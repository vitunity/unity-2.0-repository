({
	getRecords : function(cmp, evt, helper) {
        console.log('-----getRecords--'+cmp.get("v.defaultValue"));
        //cmp.set("v.currentUserId",$A.get("$SObjectType.CurrentUser.Id"));
        var showNextRecords = false;
        var showPreviousRecords = false;
        console.log("---keys--"+helper.softwareSystemsKeyValues["keys"].length);
        //if(helper.softwareSystemsKeyValues["keys"].length == 0){
        	helper.getPicklistValues(cmp);
        //}
        helper.setAdminFlag(cmp);
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords);
        helper.getApiStatusList(cmp, evt);
    },
    nextRecords:function(cmp,event,helper){
        console.log('-----nextRecords--'+cmp.get("v.offst"));
        var showNextRecords = true;
        var showPreviousRecords = false;
        var offset = cmp.get("v.offst");
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords,offset); 
    },
    previousRecords:function(cmp,event,helper){
        var showNextRecords = false;
        var showPreviousRecords = true;
        var offset = cmp.get("v.offst");
        console.log('----offset previous'+offset);
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords,offset); 
    },
    
    loadAttachmentsModal : function(component, event, helper){
        var apiRequestId = event.currentTarget.getAttribute("data-selected-Index");
        console.log("-----openModalToUpload--"+apiRequestId);
        component.set("v.selectedAPIRequest", apiRequestId);
        helper.getSFAPIKeyAttachments(component, apiRequestId);
    },
    
    showAPIDetails : function(component, event, helper){
        var apiRequestId = event.currentTarget.getAttribute("data-selected-Index");
        console.log("-----apiRequestId"+apiRequestId);
        component.set("v.selectedAPIRequest", apiRequestId);
        component.find("apiRequestLoader").reloadRecord();
        helper.getSFAPIKeyAttachments(component, apiRequestId);
        helper.getSFAPIKeyFieldHistory(component, apiRequestId);
    },
	
    openApproveRejectModal : function(component, event, helper){
        var apiApprovalDetails = event.currentTarget.getAttribute("data-selected-Index");
        var approvalDetails = apiApprovalDetails.split("-");
        component.set("v.selectedAPIRequest", approvalDetails[0]);
        component.set("v.approveRequest", approvalDetails[1]);
        component.set("v.approvalRejectionReason","");
        helper.toggleClass(component,true,"approvalWindow", "slds-show", "slds-hide");
    },
    
    closeApprovalWindow : function(component, event, helper){
        helper.toggleClass(component,false,"approvalWindow", "slds-show", "slds-hide");
    },
    
    approveRequest : function(component, event, helper){
        var apiRequestObjectId = event.currentTarget.getAttribute("data-selected-Index");
        helper.approveOrRejectAPIRequest(component, apiRequestObjectId, 'Approve', component.get("v.approvalRejectionReason"));
    },
    
    rejectRequest : function(component, event, helper){
        var apiRequestObjectId = event.currentTarget.getAttribute("data-selected-Index");
        helper.approveOrRejectAPIRequest(component, apiRequestObjectId, 'Reject', component.get("v.approvalRejectionReason"));
    },
    
    removeAttachment : function(component, event, helper){
        var attachmentDetails = event.currentTarget.getAttribute("data-selected-Index");
        var attachmentArray = attachmentDetails.split("-");
        var attId = attachmentArray[0];
        var apiRequestId = attachmentArray[1];
        
        var removeAttachment = component.get("c.deleteAttachment");
        removeAttachment.setParams({
            "attachmentId" : attId,
            "apiRequestId" : apiRequestId
        });
        removeAttachment.setCallback(this, function(response){
            var requestResult = response.getState();
            console.log("-----requestResult--"+requestResult);
            if(requestResult == "SUCCESS"){
                component.set("v.apiAttachments", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(removeAttachment);
    },
    
    attachFilesToAPIRequest : function(component, event, helper){
        var apiRequestId = component.get("v.selectedAPIRequest");
        helper.uploadFiles(component, apiRequestId);
    },
    
    handleUploadFinished : function(component, event, helper){
        console.log("------handleUploadFinished");
        var apiRequestId = component.get("v.selectedAPIRequest");
        helper.getSFAPIKeyAttachments(component, apiRequestId);
    },
    
    updateAPITypeOptions : function(component, event, helper){
        var selectedSoftwareSystemValue = component.get("v.selectedSoftwareSystem");
        var apiTypeOptions = helper.apiScopesMap[selectedSoftwareSystemValue];
        console.log("-----updateAPITypeOptions"+JSON.stringify(apiTypeOptions));
        var apiTypeValues = [];
        apiTypeValues.push({
            label: "Select API Type",
            value: ""
        });
        for (var key in apiTypeOptions) {
            apiTypeValues.push({
                label: key,
                value: key
            });
        }
        component.find("APIType").set("v.options",apiTypeValues);
        console.log("---apiTypeValues"+JSON.stringify(apiTypeValues));
    },
    
    updateThirdPartyOptions : function(component, event, helper){
        var selectedSoftwareSystemValue = component.get("v.selectedSoftwareSystem");
        var selecteAPITypeValue = component.get("v.selectedAPIType");
        var thirdPartyOptions = helper.apiScopesMap[selectedSoftwareSystemValue][selecteAPITypeValue];
        console.log("-----thirdPartyOptions"+JSON.stringify(thirdPartyOptions));
        var thirdPartyOptionValues = [];
        thirdPartyOptionValues.push({
            label: "Select third party software",
            value: ""
        });
        for (var key in thirdPartyOptions) {
            thirdPartyOptionValues.push({
                label: thirdPartyOptions[key],
                value: thirdPartyOptions[key]
            });
        }console.log("-----thirdPartyOptions2"+JSON.stringify(thirdPartyOptionValues));
        component.find("3rdPartyType").set("v.options",thirdPartyOptionValues);
    },
    
    closeAlertBox : function(component, event, helper){
        helper.toggleClass(component,false,"messageWindow", "slds-show", "slds-hide");
        if(component.get("v.approvalError")){
            helper.toggleClass(component,true,"approvalWindow", "slds-show", "slds-hide");
        }
    },
    
    changeAPIRequestOwner : function(component, event, helper){
        console.log("---LOGGEDINUSERID1--");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
		console.log("---LOGGEDINUSERID2--"+userId);
        var apiRequestObject = component.get("v.apiRequestDetail");
        apiRequestObject.fields.OwnerId.value = userId;
        component.set("v.apiRequestDetail", apiRequestObject);
        
        component.find("apiRequestLoader").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                component.find("apiRequestLoader").reloadRecord();
                console.log("Save completed successfully.");
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving record, error: ' + 
                           JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            }
        }));
    },
    
    changeAPIRequestOwnerTable : function(component, event, helper){
        var apiRequestId = event.currentTarget.getAttribute("data-selected-Index");
        var changeOwner = component.get("c.takeOwnership");
        changeOwner.setParams({
            "apiRequestId" : apiRequestId
        });
        changeOwner.setCallback(this,function(res){
            var state = res.getState(); 
            console.log('-----state-- '+state+"----"+JSON.stringify(res.getError()));
            if(state=="SUCCESS"){
                component.set("v.approvalError", false);
                console.log("---Owner Updated---");
                component.set("v.message","Owner Updated.");
                helper.toggleClass(component,true,"messageWindow", "slds-show", "slds-hide");
                helper.getObjectRecords(component,false, false, component.get("v.offst"));
            }else{
                component.set("v.message","Error Occured While "+approvalAction+"ing Request.");
                console.log("---Request Approval Failed");
            }
        });
        $A.enqueueAction(changeOwner);
    },
    
    handleAPIRequestUpdate : function(component, event, helper){
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            console.log("---API REQUEST LOADED---"+JSON.stringify(component.get("v.apiRequestDetail")));
            var apiReqObj = component.get("v.apiRequestDetail");
            var loggedInUserId = $A.get("$SObjectType.CurrentUser.Id");
            var apiOwnerId = apiReqObj.fields.OwnerId.value;
            var defaultAccountId = component.get("v.defaultValue");
            if(!$A.util.isEmpty(loggedInUserId)){
                loggedInUserId = loggedInUserId.substring(0, 15);
            }
			if(!$A.util.isEmpty(apiOwnerId)){
                apiOwnerId = apiOwnerId.substring(0, 15);
            }
            console.log("---loggedInUserId--"+loggedInUserId);
            console.log("---apiOwnerId------"+apiOwnerId);
            console.log("---defaultAccountId"+defaultAccountId);
            console.log("---apiReqObj.Approval_Status__c--"+apiReqObj.Approval_Status__c);
            var showButton = true;
            if(loggedInUserId == apiOwnerId || !$A.util.isUndefined(defaultAccountId) ||  
               apiReqObj.fields.Approval_Status__c.value == 'Approved' || apiReqObj.fields.Approval_Status__c.value == 'Rejected'){
                showButton = false;
            }
            component.set("v.showOwnRequestButton", showButton);
            helper.getObjectRecords(component,false, false, component.get("v.offst"));
        } else if(eventParams.changeType === "ERROR") {
            console.log("- ERROR WHILE LOADING API Request RECORD");
        }
    },
    
    refreshData : function(component, event, helper){
        helper.getObjectRecords(component,false, false, component.get("v.offst"));
    },
    
    clearFilters : function(cmp, evt, helper){
        cmp.set("v.APIRequestId","");
        cmp.set("v.customerNameValue","");
        cmp.set("v.selectedSoftwareSystem","");
        cmp.set("v.selectedAPIType","");
        cmp.set("v.selected3rdParty","");
        cmp.set("v.selectedApprovalStatus","");
        document.getElementById('startDateId').value='';
        document.getElementById('endDateId').value='';
        helper.getObjectRecords(cmp,false,false);
    }
})
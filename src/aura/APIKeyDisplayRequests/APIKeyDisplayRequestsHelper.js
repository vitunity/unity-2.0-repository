({
    softwareSystemsKeyValues : {"keys" : []},
    apiScopesMap : {},
    externalSoftwareMap : {},
    
	getObjectRecords : function(cmp,next,prev,offset) {
        //console.log('----received offset'+off);
        console.log('-----getRecords--'+cmp.get("v.defaultValue"));
        this.showSpinner(cmp, true);
        var off = offset || 0;
        console.log('----passing offset'+off);

        var startDate = null; 
        var endDate = null;

        if(document.getElementById('startDateId') != null)
        {
            startDate =  document.getElementById('startDateId').value;
        }
        if(document.getElementById('endDateId') != null)
        {
            endDate =  document.getElementById('endDateId').value;
        }

        var lookupController = new Object();
        lookupController.startDate = startDate;
        lookupController.endDate = endDate;
        lookupController.hasPrev = prev;
        lookupController.hasNext = next;
        lookupController.objectFields = cmp.get("v.fieldNames");
        lookupController.defaultFieldNames = cmp.get("v.conditionFieldNames");
        lookupController.defaultFieldValues = [
            cmp.get("v.APIRequestId"),
            cmp.get("v.defaultValue"),  
            cmp.get("v.customerNameValue"),  
            cmp.get("v.selectedSoftwareSystem"),  
            cmp.get("v.selectedAPIType"),  
            cmp.get("v.selected3rdParty"),  
            cmp.get("v.selectedApprovalStatus")
        ];
        lookupController.offset = off;
        lookupController.sObjectName = "API_Request__c";
        lookupController.pageSize = cmp.get("v.recordsPerPage");
        console.log('----lookupController.searchText'+lookupController.searchText+'----'+(!lookupController.searchText));
        console.log('----lookupController.objectFields'+lookupController.objectFields+'----'+lookupController.objectFields.length);
        var action = cmp.get("c.getAPIRequests");
        action.setParams({
            "lookupControllerString" : JSON.stringify(lookupController)     
        });
        console.log('-----lookupControllerString'+JSON.stringify(lookupController));
        action.setCallback(this,function(res){
            var state = res.getState(); 
            console.log('-----state-- '+state+"----");
            if(state=="SUCCESS"){
                var result = res.getReturnValue();
                cmp.set('v.next',result.hasNext);
                cmp.set('v.prev',result.hasPrev);
                cmp.set('v.offst',result.offset);
                cmp.set('v.totalNumberOfPages', result.totalPages);
                this.showSpinner(cmp, false);
                console.log('---'+JSON.stringify(result.objectRecords));
                cmp.set('v.sObjectRecords',result.objectRecords);
            }   
        });
        $A.enqueueAction(action);
    },
    
    showSpinner : function(cmp, isLoading){
        var cmpTarget = cmp.find('statusSpinner');
        if(isLoading){
            $A.util.addClass(cmpTarget, 'slds-show');
        	$A.util.removeClass(cmpTarget, 'slds-hide');
    	}else{
			$A.util.addClass(cmpTarget, 'slds-hide');
        	$A.util.removeClass(cmpTarget, 'slds-show');
		}    
	},
    
    approveOrRejectAPIRequest : function(component, requestId, approvalAction, userComments){
        this.toggleClass(component,false,"approvalWindow", "slds-show", "slds-hide");
        component.set("v.message","Processing...");
        this.toggleClass(component,true,"messageWindow", "slds-show", "slds-hide");
        console.log("-----userComments"+userComments);
        if(!$A.util.isEmpty(userComments)){
            component.set("v.approvalError", false);
            var approveOrRejectRequest = component.get("c.approveRejectAPIRequest");
            approveOrRejectRequest.setParams({
                "apiRequestId" : requestId,
                "actionName" : approvalAction,
                "comments" : userComments
            });
            approveOrRejectRequest.setCallback(this,function(res){
                var state = res.getState(); 
                console.log('-----state-- '+state+"----"+JSON.stringify(res.getError()));
                if(state=="SUCCESS"){
                    console.log("---Request Approved---");
                    component.set("v.message","Request approved successfully.");
                    this.toggleClass(component,true,"messageWindow", "slds-show", "slds-hide");
                    this.getObjectRecords(component,false, false, component.get("v.offst"));
                }else{
                    component.set("v.approvalError", true);
                    component.set("v.message","Error Occured While "+approvalAction+"ing Request.");
                    console.log("---Request Approval Failed");
                }
            });
            $A.enqueueAction(approveOrRejectRequest);
        }else{
            this.toggleClass(component,false,"approvalWindow", "slds-show", "slds-hide");
            component.set("v.message","Please enter Reason for Approval/Rejection.");
            component.set("v.approvalError", true);
        }
	},
    
    uploadFiles : function(component, parentRecordId) {
        var fileInput = component.find("file").getElement();
    	var uploadedFiles = fileInput.files;
        
        this.setupReader(component, uploadedFiles[0], parentRecordId);
    },
        
    upload: function(component, file, fileContents, parentRecordId) {
        var action = component.get("c.attachFile"); 
		
        action.setParams({
            parentId: parentRecordId,
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type
        });

        action.setCallback(this, function(a) {
            console.log("----status--upload--"+a.getState()+"---"+JSON.stringify(a.getReturnValue()));
            component.set("v.apiAttachments", a.getReturnValue());
        });
        $A.enqueueAction(action); 
    },
    
    setupReader : function(component, file, parentRecordId) {
        console.log("----setupReader-");
        var fr = new FileReader();
        var self = this;
       	fr.onload = $A.getCallback(function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

            fileContents = fileContents.substring(dataStart);
    	    self.upload(component, file, fileContents, parentRecordId);
        });
        fr.readAsDataURL(file);
    },
    
    getSFAPIKeyAttachments : function(component, apiKeyObjectId){
        var loadAttachments = component.get("c.getAPIRequestAttachments");
        loadAttachments.setParams({
            "apiRequestId" : apiKeyObjectId
        });
        loadAttachments.setCallback(this, function(response){
            var requestResult = response.getState();
            console.log("-----requestResult--"+requestResult);
            if(requestResult == "SUCCESS"){
                component.set("v.apiAttachments", response.getReturnValue());
                component.find("file").set("v.value", "");
            }
        });
        $A.enqueueAction(loadAttachments);
    },
    getSFAPIKeyFieldHistory : function(component, apiKeyObjectId){
        var loadFieldHistory = component.get("c.getApiRequestHistory");
        loadFieldHistory.setParams({
            "apiRequestId" : apiKeyObjectId
        });
        loadFieldHistory.setCallback(this, function(response){
            var requestResult = response.getState();
            console.log("-----requestResult--"+requestResult);
            if(requestResult == "SUCCESS"){
                component.set("v.apiRequestHistory", response.getReturnValue());
            }
        });
        $A.enqueueAction(loadFieldHistory);
    },
    
	getPicklistValues : function(component) {
        console.log("---in getPicklistValues");
		var action = component.get("c.getAPIScopes");
        action.setCallback(this, function(response) {
            var result = response.getState();
            console.log("----state"+result);
            if(result == "SUCCESS") {
                this.apiScopesMap = response.getReturnValue();
                console.log("in get picklist success"+JSON.stringify(this.apiScopesMap));
                
                var softwareSystemValues = [];
                softwareSystemValues.push({
                    label: "Select Software System",
                    value: ""
                });
                for (var key in this.apiScopesMap) {
                    if(key != 'REQUIREDREQUESTREASON'){
                        this.softwareSystemsKeyValues["keys"].push(key);
                        softwareSystemValues.push({
                            label: key,
                            value: key
                        });
                    }
             	}
                component.find("softwareSystem").set("v.options",softwareSystemValues);
                console.log("---softwareSystemPicklistValues"+JSON.stringify(this.softwareSystemsKeyValues));
            }
        });
        $A.enqueueAction(action);
	},
    
    toggleClass: function(component, showModal, modalName, showClass, hideClass) {
        console.log('---in toggle class'+modalName);
        var modalComponent = component.find(modalName);
        console.log("----toggle--"+JSON.stringify(modalComponent));
        if(showModal){
            $A.util.removeClass(modalComponent,hideClass);
            $A.util.addClass(modalComponent,showClass);
        }else{
            $A.util.removeClass(modalComponent,showClass);
            $A.util.addClass(modalComponent,hideClass);
        }
    },
    
    setAdminFlag : function(component){
        var adminUserCheck = component.get("c.isLoggedInUserAdmin");
        adminUserCheck.setCallback(this, function(response){
            var requestResult = response.getState();
            console.log("-----adminUserCheck--"+requestResult);
            if(requestResult == "SUCCESS"){
                component.set("v.adminUser", response.getReturnValue());
            }
        });
        $A.enqueueAction(adminUserCheck);
    },

    getApiStatusList : function(component, event) {
        var apiStatusList = $A.get("$Label.c.API_Request_Interface_Status");
        console.log('apiStatusList==harry=='+apiStatusList);
        component.set("v.listApiStatus", apiStatusList)
    }
})
({
	openHomePage : function(component, event, helper) {
        //console.log("------openHomePage--"+JSON.stringify(component.get("v.myVarianContact")));
        var loggedInSFUser = component.get("v.myVarianContact");
        var sfAccountId = "";
        if(loggedInSFUser){
            sfAccountId = loggedInSFUser.AccountId;
        }
        component.set("v.myVarianAccountId", sfAccountId);
        //helper.getAPIKeyUrlParameter(component, event);
        //helper.getApiKeyCustomLabels(component, event);
	},
    
    openApiRequestFormComponent : function(component, event, helper){
        //console.log('openApiRequestFormComponent');
        $A.createComponent(
            "c:APIKeyRequestForm",
            {
                "sfContact":component.get("v.myVarianContact")
            },
            function(newCmp){
                //console.log('in new request form function');
                if (component.isValid()) {
                   component.set("v.body", newCmp);
                }
             }
		);
    },
    
    openDisplayAPIRequest : function(component, event, helper){
        
        helper.changeTabStyle(component, "displayRequestTab");
        //console.log('openDisplayAPIRequest');
        var loggedInSFUser = component.get("v.myVarianContact");
        var sfAccountId = "";
        if(loggedInSFUser){
            sfAccountId = loggedInSFUser.AccountId;
        }
        //console.log("-----openDisplayAPIRequest--"+sfAccountId);
        $A.createComponent(
            "c:APIKeyDisplayRequests",
            {
                "defaultField" : "AccountId",
                "defaultValue" : sfAccountId,
                "languageString" : component.get("v.apiKeyLanguage"),
                "apiKeyCustomLabelMap" : component.get("v.apiKeyLabelsMap")
            },
            function(newCmp){
                //console.log('in display function');                
                if (component.isValid()) {
                   component.set("v.body", newCmp);
                }
             }
		);
    },
    
    openAPIDocumentation : function(component, event, helper){
        $A.createComponent(
            "c:APIDocumentation",
            {
     
            },
            function(newCmp){
                if (component.isValid()) {
                   component.set("v.body", newCmp);
                }
             }
		);
    },
	
    openDisplayTab : function(component, event, helper){
        console.log("-----openDisplayTab1");
        var displayTab = component.find("displayRequestTab")
        $A.util.addClass(displayTab, 'showActive');
        $A.util.removeClass(component.find("newRequestTab"), 'showActive');
        $('#displayRequestHTMLId').tab('show');
        component.showDisplayAPIRequestTab();
    },
    
    changeTab : function(component, event, helper){
        var selectedTabAuraId = event.currentTarget.getAttribute("data-selected-Index");
        helper.changeTabStyle(component, selectedTabAuraId);
    }
})
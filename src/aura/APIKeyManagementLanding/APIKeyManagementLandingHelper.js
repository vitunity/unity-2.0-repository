({
	/* get url parameter */
    getAPIKeyUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam,fParam) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    data.lang = sParameterName[1] === undefined ? true : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.myfav = sParameterName[1] === 'true' ? true : false;
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('lang','myFav');
        component.set("v.apiKeyLanguage",bdata.lang);
    },
    
    /* Load custome labels */
	getApiKeyCustomLabels : function(component, event) {
       	console.log("----getApiKeyCustomLabels----");
		var action = component.get("c.getCustomLabelMap");
       	action.setParams({"languageObj":component.get("v.apiKeyLanguage")});
       	action.setCallback(this, function(response) {
           var state = response.getState();
            console.log("----getApiKeyCustomLabels-state--"+state);
           if (component.isValid() && state === "SUCCESS") {
               	var labelsMap = response.getReturnValue();
            	component.set("v.apiKeyLabelsMap", labelsMap);
            	//console.log("----apiKeyLabelsMap---"+JSON.stringify(labelsMap));
           }else if(response.getState() == "ERROR"){
               	$A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    
    changeTabStyle : function(component, tabId){
        console.log("------changeTabStyle1---");
        console.log("------selectedTabAuraId---"+tabId);
        if(tabId != "homeTab"){             
        	$A.util.removeClass(component.find("homeTab"), "showActive");
        }
        if(tabId != "newRequestTab"){             
        	$A.util.removeClass(component.find("newRequestTab"), "showActive");
        }
        if(tabId != "displayRequestTab"){             
        	$A.util.removeClass(component.find("displayRequestTab"), "showActive");
        }
        if(tabId != "apiKeyDocumentation"){             
        	$A.util.removeClass(component.find("apiKeyDocumentation"), "showActive");
        }
        $A.util.addClass(component.find(tabId), "showActive");
        console.log("------changeTabStyle2---");
    }
})
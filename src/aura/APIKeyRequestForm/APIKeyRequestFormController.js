({
    initializeRequestForm : function(component, event, helper) {
        component.set("v.customerDetails",{
            'sobjectType': 'API_Request__c',
            'Customer_Name__c': '',
            'Account__c': '',
            'Customer__c': '',
            'Contact__c': '',
            'Customer_Contact_Name__c': '', 
            'Customer_Contact_Phone__c': '',
            'Customer_Contact_Email__c': '',
            'Requestro_Name__c': '',
            'Requestor_Email__c': '', 
            'Software_System__c': '',
            'API_Type__c': '',
            'X3rd_party_Software__c': '',
            'Request_Reason__c':''       
        });        
        helper.loadContact(component);
        helper.getPicklistValues(component);
                
    },
        loadSFContact : function(component, event, helper){
        console.log("--CHANGE CONTACT--"+JSON.stringify(component.get("v.sfContact")));
        helper.loadContact(component);
    },
    
    submitAPIRequest : function(component, event, helper){
        component.set("v.message","Processing...")
        helper.toggleClass(component,true,"APIKeySubmitPopup", "showBootstrapModal", "hideBootstrapModal");
        if(helper.validateAPIRequestForm(component)) {
            
            var customerDetailsObject = component.get('v.customerDetails');
            var isVarianUser = component.get("v.isInternalUser");
            customerDetailsObject.Requestro_Name__c = component.find("contactToRequest").getElement().value;    
            customerDetailsObject.Requestor_Email__c = document.getElementById("requestorEmail").value;
            customerDetailsObject.Request_Reason__c = document.getElementById("requestReasonTestArea").value;
            if(isVarianUser){
            	customerDetailsObject.Customer_Contact_Name__c = document.getElementById("APIRequestContactName").value;
            }
            if(isVarianUser){
                customerDetailsObject.Customer_Name__c = document.getElementById("APIRequestCustomerName").value;
            }
            var saveAPIRequest = component.get("c.saveAPIRequest");
            saveAPIRequest.setParams({
                'apiRequest' : JSON.stringify(customerDetailsObject)
            });
            saveAPIRequest.setCallback(this, function(response){
                var statusMessage = response.getReturnValue();
                console.log("---statusMessage--"+statusMessage);
                 
                if (statusMessage.includes("SUCCESS")) {
                    // record is saved successfully
                    var recordId = statusMessage.split('-')[1];
                    component.set("v.message","API Request submitted successfully.You would be redirected to Display API request tab now. If request is approved and key is not available, please refresh table, it may take upto 60 seconds");
                    helper.uploadFiles(component, recordId);
                    $(".chooseFile").val("").trigger("change");
                } //Updated by Harshal
                else if(statusMessage!= "" && statusMessage.includes("DUPLICATE_VALUE")){
                        var apiTypeKey =  component.get("v.customerDetails.Software_System__c")+", "+component.get("v.customerDetails.API_Type__c");
                        if(!$A.util.isEmpty(component.get("v.customerDetails.X3rd_party_Software__c"))){
                            apiTypeKey = apiTypeKey + ", "+ component.get("v.customerDetails.X3rd_party_Software__c");
                        }                            
                        component.set("v.message","You have already submitted request for "+apiTypeKey);
                    }
                else {
                    component.set("v.message"," Error occurred while submitting request, Please contact varian helpdesk.");
                    component.set("v.hasErrors", true);
                    console.log('Unknown problem, state: '+statusMessage);
                }
            });
            $A.enqueueAction(saveAPIRequest);
        }
        console.log("------in submitAPIRequest");
    },
    
    updateAPITypeOptions : function(component, event, helper){
        var selectedSoftwareSystemValue = component.get("v.customerDetails.Software_System__c");
        var apiTypeOptions = helper.apiScopesMap[selectedSoftwareSystemValue];
        console.log("-----updateAPITypeOptions"+JSON.stringify(apiTypeOptions));
        var apiTypeValues = [];
        apiTypeValues.push({
            label: "Select API Type",
            value: ""
        });
        for (var key in apiTypeOptions) {
            apiTypeValues.push({
                label: key,
                value: key
            });
        }
        component.find("APIType").set("v.options",apiTypeValues);
        console.log("---apiTypeValues"+JSON.stringify(apiTypeValues));
    },
    
    updateThirdPartyOptions : function(component, event, helper){
        var selectedSoftwareSystemValue = component.get("v.customerDetails.Software_System__c");
        var selecteAPITypeValue = component.get("v.customerDetails.API_Type__c");
        var thirdPartyOptions = helper.apiScopesMap[selectedSoftwareSystemValue][selecteAPITypeValue];
        console.log("-----thirdPartyOptions"+JSON.stringify(thirdPartyOptions));
        var thirdPartyOptionValues = [];
        thirdPartyOptionValues.push({
            label: "Select third party software",
            value: ""
        });
        for (var key in thirdPartyOptions) {
            thirdPartyOptionValues.push({
                label: thirdPartyOptions[key],
                value: thirdPartyOptions[key]
            });
        }
        if(thirdPartyOptionValues.length > 1){
            component.set("v.disableThirdPartySoftware", false);
        }else{
            component.set("v.disableThirdPartySoftware", true);
        }
        component.find("3rdPartyTpe").set("v.options",thirdPartyOptionValues);
    },
    
    showSpinner: function(component, event, helper) {
        //var spinnerComponent = component.find("spinnerId"); 
        //$A.util.addClass(spinnerComponent, 'slds-show');
        //$A.util.removeClass(spinnerComponent, 'slds-hide');
        helper.toggleClass(component,true,"spinnerId", "slds-show", "slds-hide");
        console.log("----showSpinner");
    },
 
    hideSpinner : function(component,event,helper){
        //var spinnerComponent = component.find("spinnerId"); 
        //$A.util.addClass(spinnerComponent, 'slds-hide');
        //$A.util.removeClass(spinnerComponent, 'slds-show');
        helper.toggleClass(component,false,"spinnerId", "slds-show", "slds-hide");
        console.log("----hideSpinner");
    },
    
    showAlertModal : function(component,event,helper){
        helper.toggleClass(component,true,"APIKeySubmitPopup", "showBootstrapModal", "hideBootstrapModal");
    },
    
    hideAlertModal : function(component,event,helper){
        helper.toggleClass(component,false,"APIKeySubmitPopup", "showBootstrapModal", "hideBootstrapModal");
        var formHasErrors = false;
        formHasErrors = component.get("v.hasErrors");
        console.log("----formHasErrors--"+formHasErrors);
        if(!formHasErrors){
            component.set("v.copySFContact", "false");
            component.reInitializeAPIRequest();
            var displayRequestsTab = component.getEvent("showDisplayTab");
            displayRequestsTab.fire(); 
        }
    },
    
    //Opens custom lookup component popup dynamically based of object type
    showCustomLookupModal: function(component, event, helper) {
        //Toggle CSS styles for opening Modal
        
        //var recordLoopupKey = event.getSource().get("v.name");
        var recordLoopupKey = event.currentTarget.getAttribute("data-selected-Index");
        var recordLookupArray = recordLoopupKey.split("-");
        var recordIdField = recordLookupArray[0];
        var recordNameField = recordLookupArray[1];
        var sObjectLabel = recordLookupArray[2];
        var sObjectName = recordLookupArray[3];
        
        //var recordNameComponent = component.find(recordNameField);    
        //var recordNameValue = recordNameComponent.get("v.value");

        var recordNameValue = document.getElementById(recordNameField).value;
        console.log('recordNameValue is----'+recordNameValue+'--recordIdField'+recordIdField+'--recordNameField'+recordNameField+'---sObjectName'+sObjectName);
        var objectFieldLabels = [];
        var objectFieldNames = [];
        if(sObjectName == "Account"){
            console.log('----Account--'+sObjectName);
            objectFieldLabels = ['Name', 'Account Type', 'Street', 'City', 'State', 'Zip/Postal Code', 'Country', 
                                 'ERP Customer Id', 'Account Record Type'];
            
            objectFieldNames = ['Name', 'Account_Type__c', 'BillingStreet', 'BillingCity', 'BillingState', 'BillingPostalCode',
                                'BillingCountry', 'Ext_Cust_Id__c', 'RecordType.Name'];
            component.set("v.defaultFieldName", "");
            component.set("v.defaultFieldValue", "");
                  
        }else if(sObjectName == "Contact"){
            console.log('----Contact--'+sObjectName+"-----account Id"+component.get("v.customerDetails.Account__c"));
            objectFieldLabels = ['Name', 'Account Name', 'Phone', 'Email', 'City', 'State', 'Postal Code', 'Country'];
            objectFieldNames = ['Name', 'Account.Name', 'Phone', 'Email', 'MailingCity', 'MailingState', 'MailingPostalCode', 
                                'MailingCountry'];
            component.set("v.defaultFieldName", "AccountId");
            component.set("v.defaultFieldValue", component.get("v.customerDetails.Account__c"));
            
        }
        console.log('----objectFieldLabels'+objectFieldLabels+'-----objectFieldNames'+objectFieldNames);
        component.set("v.fieldLabels", objectFieldLabels);
        component.set("v.fieldNames", objectFieldNames);
        component.set("v.selectedLookupSearchKey", recordNameValue);  
        component.set("v.selectedLookupIdField", recordIdField);
        component.set("v.selectedLookupNameField", recordNameField);
        component.set("v.objectLabelValue", sObjectLabel);
        component.set("v.objectAPIName", sObjectName);
        
        helper.toggleClass(component,true,"APIKeyCustomLookupPopUp", "showBootstrapModal", "hideBootstrapModal");
    },
    
    setSelectedLookupRecord : function(component, event, helper){
        var sfId = event.getParam("recordId");
        var sfName = event.getParam("recordName");
        var nameField = event.getParam("recordNameField");
        var idField = event.getParam("recordIdField");
        console.log('-----sfId1'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        helper.toggleClass(component,false,"APIKeyCustomLookupPopUp", "showBootstrapModal", "hideBootstrapModal");
        console.log('-----sfId2'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        document.getElementById(nameField).value = sfName;
        component.find(idField).set("v.value",sfId);
        console.log('-----sfId3'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        
        if(sfId.startsWith("003")){
            component.find("contactLoader").reloadRecord();
        }else{
            component.find("accountLoader").reloadRecord();
        }
        component.set("v.   ", "");
    },
    
    handleAccountUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        console.log("------handleAccountUpdated--"+component.get("v.customerDetails.Account__c"));
        if(eventParams.changeType === "LOADED") {
            var accountObject = component.get("v.accountRecord");
            console.log("-----account JSON"+JSON.stringify(accountObject));
            component.set("v.customerDetails.Customer__c", accountObject.fields.Ext_Cust_Id__c.value);
            component.set("v.customerDetails.Customer_Contact_Name__c", "");
            component.set("v.customerDetails.Customer_Contact_Phone__c", "");
        } else if(eventParams.changeType === "ERROR") {
            console.log("- ERROR WHILE LOADING ACCOUNT RECORD");
        }
    },
    
    handleContactUpdated: function(component, event, helper) {
        console.log("------handleContactUpdated--"+component.get("v.customerDetails.Contact__c"));
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var contactObject = component.get("v.contactRecord");
            console.log("-----account JSON"+JSON.stringify(contactObject));
            component.set("v.customerDetails.Customer_Contact_Phone__c", contactObject.fields.Phone.value);
        } else if(eventParams.changeType === "ERROR") {
            console.log("- ERROR WHILE LOADING CONTACT RECORD--"+component.get("v.recordError"));
        }
    },
    
    copyContactToRequestor : function(component, event, helper){
        if(component.find("contactToRequest").getElement().checked){
            console.log("-------copyContactToRequestor----");
            component.set("v.customerDetails.Requestro_Name__c", component.get("v.customerDetails.Customer_Contact_Name__c")/*document.getElementById("APIRequestContactName").value*/);
            var contactObject = component.get("v.sfContact");
            var contactObjectRecord = component.get("v.contactRecord");
            console.log("-----contactObject----"+JSON.stringify(contactObjectRecord)+'---'+!$A.util.isUndefined(contactObject.Email));
            if(!$A.util.isUndefined(contactObject.Email)){
            	component.set("v.customerDetails.Requestor_Email__c", contactObject.Email);
                component.set("v.customerDetails.Requestro_Name__c", contactObject.Name);
            }else{
                component.set("v.customerDetails.Requestor_Email__c", contactObjectRecord.fields.Email.value);
                component.set("v.customerDetails.Requestro_Name__c", contactObjectRecord.fields.Name.value);
            }
        }
        else{
            component.set("v.customerDetails.Requestor_Email__c", "");
            component.set("v.customerDetails.Requestro_Name__c", "");
        }
    },
    
    closeLookupModal : function(component, event, helper){
        helper.toggleClass(component,false,"APIKeyCustomLookupPopUp", "showBootstrapModal", "hideBootstrapModal");
    },
    
    removeFile: function(component,event){
        console.log("----removeFile--1--");
        $(".chooseFile").val("").trigger("change");
        console.log("----removeFile--2--");
    },

    resetAPIRequest: function(component,event){
        console.log("----in the reset method--");
        $A.enqueueAction(component.get('c.initializeRequestForm'));
        $(".chooseFile").val("").trigger("change");
        component.find("contactToRequest").getElement().checked=false;       
    }
})
({
    softwareSystemsKeyValues : {"keys" : [], "scopesWithRequiredReason": []},
    apiScopesMap : {},
    externalSoftwareMap : {},
    getPicklistValues : function(component) {
        // console.log("---in getPicklistValues");
        var action = component.get("c.getAPIScopes");
        action.setCallback(this, function(response) {
            var result = response.getState();
            //console.log("----state"+result);
            if(result == "SUCCESS") {
                this.apiScopesMap = response.getReturnValue();
                console.log("in get picklist success"+JSON.stringify(this.apiScopesMap));
                
                var softwareSystemValues = [];
                softwareSystemValues.push({
                    label: "Select Software System",
                    value: ""
                });
                for (var key in this.apiScopesMap) {
                    if(key != 'REQUIREDREQUESTREASON'){
                        this.softwareSystemsKeyValues["keys"].push(key);
                        softwareSystemValues.push({
                            label: key,
                            value: key
                        });
                    }
                }
                
                for(var key in this.apiScopesMap["REQUIREDREQUESTREASON"]){
                    this.softwareSystemsKeyValues["scopesWithRequiredReason"].push(key);
                }
                component.find("softwareSystem").set("v.options",softwareSystemValues);
                console.log("---requiredReasonScopes"+JSON.stringify(this.softwareSystemsKeyValues["scopesWithRequiredReason"]));
            }
        });
        $A.enqueueAction(action);
    },
    
    loadContact : function(component) {
        var vmContact = component.get("v.sfContact");
        //console.log("contact"+JSON.stringify(vmContact));
        if(vmContact == null || !vmContact.Name){
            component.set("v.isInternalUser", true);
            // console.log("-----1");
        }else{
            //console.log("-----2");
            component.set("v.isInternalUser", false);
            component.set("v.customerDetails.Customer_Name__c", vmContact.Account.Name);
            component.set("v.customerDetails.Account__c", vmContact.AccountId);
            component.set("v.customerDetails.Customer__c", vmContact.Account.Ext_Cust_Id__c);
            component.set("v.customerDetails.Contact__c", vmContact.Id);
            component.set("v.customerDetails.Customer_Contact_Name__c", vmContact.Name);
            component.set("v.customerDetails.Customer_Contact_Phone__c", vmContact.Phone);
        }
    },
    
    toggleClass: function(component, showModal, modalName, showClass, hideClass) {
        // console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,hideClass);
            $A.util.addClass(modalComponent,showClass);
        }else{
            $A.util.removeClass(modalComponent,showClass);
            $A.util.addClass(modalComponent,hideClass);
        }
    },
    
    MAX_FILE_SIZE: 2000000, /* 1 000 000 * 3/4 to account for base64 */
    
    validateAPIRequestForm : function(component){
        console.log("----validateAPIRequestForm-1"+JSON.stringify(this.softwareSystemsKeyValues["scopesWithRequiredReason"]));
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        //var fileInput = component.find("file").getElement();
        var fileInput = component.find("file").getElement();
        var uploadedFiles = fileInput.files;
        var messages = "";
        var isInternalUser = component.get("v.isInternalUser");
        console.log("----validateAPIRequestForm-2"+JSON.stringify(uploadedFiles)+"--internal User"+isInternalUser+"---"+(!isInternalUser));
        if(isInternalUser && $A.util.isEmpty(document.getElementById("APIRequestCustomerName").value)){
            $A.util.addClass(document.getElementById("APIRequestCustomerName"), 'glowing-border');
            messages += 'Please select customer.';  
        }else{
            $A.util.removeClass(document.getElementById("APIRequestCustomerName"), 'glowing-border');
        }
        if(isInternalUser && $A.util.isEmpty(document.getElementById("APIRequestContactName").value)){
            $A.util.addClass(document.getElementById("APIRequestContactName"), 'glowing-border');
            if(messages != ""){messages += "<br/>";}
            messages += 'Please select customer contact.';  
        }else{
            $A.util.removeClass(document.getElementById("APIRequestContactName"), 'glowing-border');
        }
        if($A.util.isEmpty(component.get("v.customerDetails.Customer__c"))){
            $A.util.addClass(component.find("ApiRequestCustomerId"), 'glowing-border');
            if(messages != ""){messages += "<br/>";}
            messages += 'Please enter customer ID.';  
        }else{
            $A.util.removeClass(component.find("ApiRequestCustomerId"), 'glowing-border');
        }
        if($A.util.isEmpty(component.get("v.customerDetails.Customer_Contact_Phone__c"))){
            $A.util.addClass(component.find("APIRequestContactPhone"), 'glowing-border');
            if(messages != ""){messages += "<br/>";}
            messages += 'Please enter customer contact phone.';  
        }else{
            $A.util.removeClass(component.find("APIRequestContactPhone"), 'glowing-border');
        }
        if($A.util.isEmpty(document.getElementById("requestorName").value)){
            $A.util.addClass(document.getElementById("requestorName"), 'glowing-border');
            if(messages != ""){messages += "<br/>";}
            messages += 'Please enter requestor name.';  
        }else{
            $A.util.removeClass(document.getElementById("requestorName"), 'glowing-border');
        }
        
        var requestorEmail = document.getElementById("requestorEmail").value;
        if($A.util.isEmpty(requestorEmail)){
            $A.util.addClass(document.getElementById("requestorEmail"), 'glowing-border');
            if(messages != ""){messages += "<br/>";}
            messages += 'Please enter requestor email.';  
        }else{
            $A.util.removeClass(document.getElementById("requestorEmail"), 'glowing-border');
        }
        
        if(!$A.util.isEmpty(requestorEmail)){   
            if(requestorEmail.match(emailRegex)){
                $A.util.removeClass(document.getElementById("requestorEmail"), 'glowing-border');
            }else{
                $A.util.addClass(document.getElementById("requestorEmail"), 'glowing-border');
                if(messages != ""){messages += "<br/>";}
                messages += 'Please enter a valid requestor email.';  
            }
        }
        
        if($A.util.isEmpty(component.get("v.customerDetails.Software_System__c"))){
            $A.util.addClass(component.find("softwareSystem"), 'glowing-border'); 
            if(messages != ""){messages += "<br/>";}
            messages += 'Please select software system.';  
        }else{
            $A.util.removeClass(component.find("softwareSystem"), 'glowing-border');
        }
        var apiType = component.get("v.customerDetails.API_Type__c");
        if($A.util.isEmpty(apiType)){
            $A.util.addClass(component.find("APIType"), 'glowing-border'); 
            if(messages != ""){messages += "<br/>";}
            messages += 'Please select API Type.';  
        }else{
            $A.util.removeClass(component.find("APIType"), 'glowing-border');
        }
        
        if($A.util.isEmpty(component.get("v.customerDetails.X3rd_party_Software__c")) 
           && !$A.util.isUndefined(component.find("3rdPartyTpe"))
           && component.find("3rdPartyTpe").get("v.options").length > 1){
            $A.util.addClass(component.find("3rdPartyTpe"), 'glowing-border'); 
            if(messages != ""){messages += "<br/>";}
            messages += 'Please select 3rd party software.';  
        }else{
            $A.util.removeClass(component.find("3rdPartyTpe"), 'glowing-border');
        }
        var requestReasonValue = document.getElementById("requestReasonTestArea").value;
        console.log("----requestReasonValue---"+requestReasonValue);
        if(this.validateRequiredReason(component) && $A.util.isEmpty(requestReasonValue)){
            //debugger;
            //$A.util.addClass(component.find("requestReason"), 'glowing-border');
            $("textarea").addClass("glowing-border");
            if(messages != ""){messages += "<br/>";}
            messages += 'Please enter request reason.'; 
        }else{
            //$A.util.removeClass(component.find("requestReason"), 'glowing-border');
            $("textarea").removeClass("glowing-border");
        }
        
        console.log("----validateAPIRequestForm-3");
        for (var i = 0; i < uploadedFiles.length; i++) {
            if(messages != ""){
                messages += "<br/>"
            }
            console.log("----Files-"+uploadedFiles[i].size);
            if (uploadedFiles[i].size > this.MAX_FILE_SIZE) {
                messages += 'File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes. Selected file size: ' + uploadedFiles[i].size;  
            }
        }
        
        console.log("----validateAPIRequestForm-4"+messages);
        if(messages == ""){
            component.set("v.hasErrors", false);
            return true;
        }else{
            component.set("v.hasErrors", true);
            component.set("v.message",messages);  
            console.log("-----messages----"+messages);
            helper.toggleClass(component,true,"APIKeySubmitPopup", "showBootstrapModal", "hideBootstrapModal");
            return false;
        }
    },
    
    validateRequiredReason : function(component){
        var softwareSystem = component.get("v.customerDetails.Software_System__c");
        var apiType = component.get("v.customerDetails.API_Type__c");
        var thirdPartySoftware = component.get("v.customerDetails.X3rd_party_Software__c");
        var apiScopeKey = "";
        if(!$A.util.isEmpty(softwareSystem) && !$A.util.isEmpty(apiType)){
            apiScopeKey = softwareSystem+""+apiType;
            if(!$A.util.isEmpty(thirdPartySoftware)){
                apiScopeKey = apiScopeKey+""+thirdPartySoftware;
            }
        }
        var requiredReasonScopes = this.softwareSystemsKeyValues["scopesWithRequiredReason"];
        var requestReasonRequired = false;
        for (var i = 0; i < requiredReasonScopes.length; i++) {
            if(apiScopeKey == requiredReasonScopes[i]){
                requestReasonRequired = true;
            }
        }
        return requestReasonRequired;
    },
    
    uploadFiles : function(component, parentRecordId) {
        console.log("----uploadFiles");
        var fileInput = component.find("file").getElement();
        var uploadedFiles = fileInput.files;
        if(uploadedFiles ==0){
            $A.util.addClass(component.find("removeFile").getElement(), 'ShowRemoveFile');
        }
        for (var i = 0; i < uploadedFiles.length; i++) {
            this.setupReader(component, uploadedFiles[i], parentRecordId);
        }
    },
    
    upload: function(component, file, fileContents, parentRecordId) {
        var action = component.get("c.saveTheFile"); 
        //console.log("----upload-"+JSON.stringify(file));
        action.setParams({
            parentId: parentRecordId,
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type
        });
        
        action.setCallback(this, function(a) {
            console.log("----status--"+a.getState());
            attachId = a.getReturnValue();
            //console.log(attachId);
        });
        $A.enqueueAction(action); 
        
    },
    
    setupReader : function(component, file, parentRecordId) {
        // console.log("----setupReader-");
        var fr = new FileReader();
        var self = this;
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            
            fileContents = fileContents.substring(dataStart);
            
            self.upload(component, file, fileContents, parentRecordId);
        };
        fr.readAsDataURL(file);
    },
})
({
	doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        if(recordId===null || recordId===undefined)
        	alert('Record Id not exist.');
        
        helper.accessPermission(component);
        helper.checkFolderExist(component);
	},
    visibleComponent: function(component, event, helper) {
        helper.resetAllMsgs(component);
        console.log('here');
        var selectedValue = component.find("categoryPicklist").get("v.value");
        var userNameId = component.find('userNameId').getElement();
        var userRoleId = component.find('userRoleId').getElement();
        var userEmailId = component.find('userEmailId').getElement();
        var buttonId = component.find('buttonId').getElement();

        if(selectedValue === "") {
			$A.util.addClass(userNameId, 'slds-hide');
        	$A.util.addClass(userRoleId, 'slds-hide');
        	$A.util.addClass(userEmailId, 'slds-hide');
            $A.util.addClass(buttonId, 'slds-hide');
        } else if(selectedValue==="1") {
        	$A.util.addClass(userEmailId, 'slds-hide');
			$A.util.removeClass(userNameId, 'slds-hide');
        	$A.util.removeClass(userRoleId, 'slds-hide');
            $A.util.removeClass(buttonId, 'slds-hide');
        } else if(selectedValue==="2") {
			$A.util.addClass(userNameId, 'slds-hide');
        	$A.util.removeClass(userRoleId, 'slds-hide');
        	$A.util.removeClass(userEmailId, 'slds-hide');
            $A.util.removeClass(buttonId, 'slds-hide');
        }
    },
    save: function(component, event, helper) {
        helper.resetAllMsgs(component);
        var recordId = component.get("v.recordId");
		var selectedValue = component.find("categoryPicklist").get("v.value");
        var errorMsg = null;
        var roleNameList = {"1":"Co-owner", "2":"Viewer", "3":"Uploader", "4":"Preview-Uploader", "5":"Editor", "6":"Previewer"};
		var roleVal = component.find("roleList").get("v.value");
        var roleName = roleNameList[roleVal];
        
        if(roleName===null || roleName===undefined) {
            errorMsg = "Please, Select user role";
        }
        
		if(selectedValue==="1") {
            var selectedUserRecId = component.get("v.selectedLookUpRecord").Id;
            if(selectedUserRecId===null || selectedUserRecId===undefined) {
                if(errorMsg===null) {
                    errorMsg = "Please, Select user name";
                } else {
                    errorMsg += " and user name";
                }
            }
        } else if(selectedValue==="2") {
            var email = component.find("emailVal").get("v.value");
            var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
            
            if((email === null || email === undefined) && errorMsg === null) {
				errorMsg = "Please, enter user email";
			} else if ((email === null || email === undefined) && errorMsg !== null) {
				errorMsg += " and enter user email";
            } else if(email !== null && email !== undefined && !email.match(regExpEmailformat) && errorMsg === null) {
				errorMsg = "Please, enter valid user email";
            } else if(email !== null && email !== undefined && !email.match(regExpEmailformat) && errorMsg !== null) {
                errorMsg += " and enter valid user email";
            }
        }
        
        if(errorMsg===null) {
            if(selectedValue==="1") { // Varian User
                helper.addBoxUserPermission(component, true, roleName, selectedUserRecId, email);
            }
            if(selectedValue==="2") { // Non-Varian User
                helper.addBoxUserPermission(component, false, roleName, selectedUserRecId, email);
            }
		} else if(errorMsg !== null) {
			helper.setMsg(component, 'errMsg', errorMsg);
		}
    }
})
({
    accessPermission: function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.checkPermission");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });
            
        action.setCallback(this, function(a) {
            var isUserAuthorised = a.getReturnValue();
            if(isUserAuthorised) {
                component.set('v.isAuthorised', true);
            } else {
                component.set('v.showErrMsg', true);
                component.set("v.errMsg", "<span class='TxtAlign'>Not authorized</span>");
            }
        	var spinnerElem = component.find('spinnerId').getElement();
        	$A.util.addClass(spinnerElem, 'slds-hide');
        });

        $A.enqueueAction(action);
    },
    checkFolderExist: function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.checkFolderIdExist");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });
		var self = component;
        action.setCallback(this, function(a) {
            var isFolderIDExist = a.getReturnValue();
            var isAuthorised = self.get('v.isAuthorised');
            if(!isFolderIDExist && isAuthorised) {
                component.set('v.isAuthorised', false);
                component.set('v.showErrMsg', true);
                component.set("v.errMsg", '<ul style="padding:0px 20px;list-style: circle;"><li>There is no folder created in BOX.</li><li>To resolve this error, close this error message and upload your data to BOX by clicking "Upload To Varian Secure Drop" button in this PHI Log.</li></ul>');
            }
        	var spinnerElem = component.find('spinnerId').getElement();
        	$A.util.addClass(spinnerElem, 'slds-hide');
        });

        $A.enqueueAction(action);        
    },
    resetAllMsgs: function(component) {
        
        var spinnerElem = component.find('spinnerId').getElement();
        var errElem = component.find('errMsg').getElement();
        var infoElem = component.find('infoMsg').getElement();

		$A.util.addClass(errElem, 'slds-hide');
        $A.util.addClass(infoElem, 'slds-hide');
        $A.util.addClass(spinnerElem, 'slds-hide');
        
        infoElem.innerText = '';
        errElem.innerText = '';
    },
    setMsg: function(component, strText, msg) {
        var elem = component.find(strText).getElement();
        $A.util.removeClass(elem, 'slds-hide');
        elem.innerText = msg;
    },
    showSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.removeClass(spinnerElem, 'slds-hide');
    },
    hideSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.addClass(spinnerElem, 'slds-hide');
    },
    addBoxUserPermission: function(component, isVarianUser, roleName, userId, userEmail) {
		console.log('Calling addBoxUserPermission--------');
        var action = component.get("c.addUserToBox");
    
        action.setParams({
            recordId: component.get("v.recordId"),
            isVarianUser: isVarianUser,
            roleName: roleName, 
            userId: userId,
            userEmail: userEmail
        });
            
        action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            if(result[0]==='true') {
				this.setMsg(component, 'infoMsg', result[1]);

                window.setTimeout(function(){
					$A.get("e.force:closeQuickAction").fire();
					$A.get('e.force:refreshView').fire();
                }, 3000);
            } else if(result[0]==='false') {
                this.setMsg(component, 'errMsg', result[1]);
            }
            this.hideSpinner(component);
        });
            
        this.showSpinner(component);
        $A.enqueueAction(action);
    }
})
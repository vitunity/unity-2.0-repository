({
	myAction : function(component, event, helper) {
		var cmpTarget = component.find('listBox');
        
        $A.util.removeClass(cmpTarget, 'divSuggestionCSS');
        $A.util.addClass(cmpTarget, 'hideSuggestionDiv');
	}, 
    searchVal : function(component, event, helper) {
      	var searchText = component.find("recordSearchText").get('v.value');
        var minLen = searchText.length;
        var cmpTarget = component.find('listBox');
        if(minLen < 2) {
            component.set("v.suggestionList", []);
            //component.set("v.inputVal", '');
            $A.util.removeClass(cmpTarget, 'divSuggestionCSS');
            $A.util.addClass(cmpTarget, 'hideSuggestionDiv');
            return;
        } else {
            
            $A.util.addClass(cmpTarget, 'divSuggestionCSS');
            $A.util.removeClass(cmpTarget, 'hideSuggestionDiv');
            helper.searchDetails(component, event, searchText);
        }
    },
    
    selected : function(component, event, helper) {
        //console.log('selected' + event.srcElement.id);
        var cmpTarget = component.find('listBox');
        var val = event.target.id;
        var res = [];
        res = val.split("_");
        var setSelectedRecordEvent = component.getEvent("replacementselection");
        var recId = component.get("v.compId");
        setSelectedRecordEvent.setParams({"vendorName": res[0]});
        setSelectedRecordEvent.setParams({"recId": recId});
        
        component.set("v.inputVal", res[0]);
        component.set("v.suggestionList", []);
        $A.util.removeClass(cmpTarget, 'divSuggestionCSS');
        $A.util.addClass(cmpTarget, 'hideSuggestionDiv');
        setSelectedRecordEvent.fire();
        //component.set("v.suggestionList", []);
        //$A.util.removeClass(cmpTarget, 'divSuggestionCSS');
        //$A.util.addClass(cmpTarget, 'hideSuggestionDiv');
    }
})
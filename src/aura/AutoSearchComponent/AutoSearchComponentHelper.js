({
	searchDetails : function(component, event, searchText) {
		console.log(' == helper method called == ');
        //var action = component.get("c.getSuggestions");
        var action = component.get("c.getSuggestValues");
        var fieldsToGet = component.get("v.fields");
        
        action.setParams({
            "sObjectType": component.get("v.sObject"),
            "fieldName":component.get("v.fieldName"),
            "searchText": searchText,
            "fieldsToGet": fieldsToGet.join(),
            "limitSize": component.get("v.limit")
        });
        
        action.setCallback(this, function(res) {
            var state = res.getState(); 
            if(state=="SUCCESS"){
            	var suggestions = res.getReturnValue();
                console.log(' === return === ' + JSON.stringify(res.getReturnValue()) );
                var vendorList = [];
                for(var i = 0; i < suggestions.length; i ++) {
                    if(!vendorList.includes(suggestions[i])) {
                    	vendorList.push(suggestions[i]);
                    }
                }
                component.set("v.suggestionList", vendorList);
                component.set("v.listSize", suggestions.length);
            }
        });
        $A.enqueueAction(action);
	}
})
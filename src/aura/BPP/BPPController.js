({


    handleComponentEvent : function(component, event) 
    {
        
        if(component.get("v.bundledProfile") == true)
        {
            component.set("v.bundledProfile", false);
            component.set("v.fromMainCmp", true);
        }
        else
        {
           component.set("v.bundledProfile", true);

        }
       
        component.set("v.cp",event.getParam("cp"));
    }
})
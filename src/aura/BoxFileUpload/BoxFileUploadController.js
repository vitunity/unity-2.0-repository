({
    doInit: function (component, event, helper) {
        var recordId = component.get("v.recordId");
        if(recordId===null || recordId===undefined)
        	alert('Record Id not exist.');
        
        helper.accessPermission(component);
    },
    uploadFileToBox: function (component, event, helper) {
        // Reset All Msgs
        helper.resetAllMsgs(component);

        var fileInput = component.find("file").getElement();
    	var file = fileInput.files[0];
        if(file === undefined || file === null) {
            helper.setMsg(component, 'errMsg', 'Please, upload a file.');
            return;
        }
        if(file.size===0) {
            helper.setMsg(component, 'errMsg', 'File size is 0. Please, upload a file.');
            return;
        }
        
        var fr = new FileReader();
        var self = this;
       	fr.onload = function() {
            var fileContents = fr.result;
    	    var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

            fileContents = fileContents.substring(dataStart);

            console.log('----Here');
    	    helper.upload(component, file, fileContents);
        };

        fr.readAsDataURL(file);
    }
})
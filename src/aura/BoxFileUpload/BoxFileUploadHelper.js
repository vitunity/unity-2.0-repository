({
    upload: function(component, file, fileContents) {
        console.log('--------Check');
        var action = component.get("c.uploadBoxFile");

        if(file.size > 3145728) {
            fileContents = ''; // Large string not possible to send 
        }
        
        action.setParams({
            recordId: component.get("v.recordId"),
            uploadFilename: file.name,
            uploadContent: encodeURIComponent(fileContents), 
            contentType: file.type,
            fileSize: String(file.size)
        });
            
        action.setCallback(this, function(a) {
            var result = a.getReturnValue();
            if(result[0]==='true') {
				this.setMsg(component, 'infoMsg', result[1]);

                window.setTimeout(function(){
					$A.get("e.force:closeQuickAction").fire();
					$A.get('e.force:refreshView').fire();
                }, 3000);
            } else if(result[0]==='false') {
                this.setMsg(component, 'errMsg', result[1]);
                window.setTimeout(function(){
					$A.get("e.force:closeQuickAction").fire();
					$A.get('e.force:refreshView').fire();
				}, 10000);
            }
            this.hideSpinner(component);
        });
            
        this.showSpinner(component);
        $A.enqueueAction(action);
    },
    resetAllMsgs: function(component) {
        
        var spinnerElem = component.find('spinnerId').getElement();
        var errElem = component.find('errMsg').getElement();
        var infoElem = component.find('infoMsg').getElement();

		$A.util.addClass(errElem, 'slds-hide');
        $A.util.addClass(infoElem, 'slds-hide');
        $A.util.addClass(spinnerElem, 'slds-hide');
        
        infoElem.innerText = '';
        //errElem.innerText = '';
        component.set('v.errMsgText', '');
    },
    setMsg: function(component, strText, msg) {
        var elem = component.find(strText).getElement();
        $A.util.removeClass(elem, 'slds-hide');
        //elem.innerText = msg;
        if(strText === 'errMsg') {
            component.set('v.errMsgText', msg);
        }
    },
    showSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.removeClass(spinnerElem, 'slds-hide');
    },
    hideSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.addClass(spinnerElem, 'slds-hide');
    },
    accessPermission: function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.checkPermission");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });
            
        action.setCallback(this, function(a) {
            var isUserAuthorised = a.getReturnValue();
            if(isUserAuthorised) {
                component.set('v.isAuthorised', true);
            } else {
                component.set('v.showErrMsg', true);
            }
        	var spinnerElem = component.find('spinnerId').getElement();
        	$A.util.addClass(spinnerElem, 'slds-hide');
        });

        $A.enqueueAction(action);
    }
})
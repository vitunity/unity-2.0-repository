({
    
    doInit : function(component, event, helper) 
    {
        var action = component.get('c.initMethod');
        action.setParams({
            "recordId": component.get("v.currentRecordId")
        });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set('v.obj', response.getReturnValue());
                //console.log('obj:'+ JSON.stringify(response.getReturnValue()));
            }
        });
		
		var saveButton = component.find("saveButton");
        $A.util.addClass(saveButton, 'slds-hide');
		
        $A.enqueueAction(action);
    },


    onCountryChange : function (component,event,helper)
    {
        var selectedCountry = component.find("countryId").get("v.value");
        var obj = component.get("v.obj");
        if(selectedCountry != 'None') 
        {
            obj.selectedCountryId =  selectedCountry;
            var action = component.get('c.fetchClearanceStatus');
            action.setParams({
                "obj": JSON.stringify(obj)
            });

            action.setCallback(this,function(response)
            {
        //store state of response
        var state = response.getState();
        if (state === "SUCCESS") 
        {
          //set response value in objClassController attribute on component
          component.set('v.obj', response.getReturnValue());
        }
            });
          $A.enqueueAction(action);

        }
        else
        {
            obj.selectedCountryId =  selectedCountry;
            obj.childClearanceEntries = null;
            component.set('v.obj', obj);
            component.set("v.result", "");
        }
    },

	// update parent clearance entries
	updateParentClrEntries: function(component, event, helper) {
		// Call Backend method
        var action = component.get('c.updateParentClearanceEntries');
        var obj = component.get("v.obj");
        action.setParams({
            "obj": JSON.stringify(obj)
        });

        try {
            action.setCallback(this,function(response)
            {
                //store state of response
                var state = response.getState();
    
                if (state === "SUCCESS") 
                {
                    //set response value in objClassController attribute on component
                    var rflag = response.getReturnValue();
                    if(rflag) {
                        // refresh same page
                        $A.get('e.force:refreshView').fire();
                    } else {
                        // Stay on same page
                        console.log('Result not getting updated due to error');
                    }
                }
			});
        } catch (e) {
            // Stay on same page
            console.log('Result not getting updated due to error');
		}
        $A.enqueueAction(action);
	},
	
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) 
    {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
  
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper)
    {
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    simulate :  function (component,event,helper)
    {
        var selectedCountry = component.find("countryId").get("v.value");
        if(selectedCountry!=null && selectedCountry!='None') {
            var saveButton = component.find("saveButton");
            var acc = component.find('clearanceStatus');
            var filterLogic = component.find("logicId").get("v.value");
            var result;
            var counter = 0;
            for(var cmp in acc) 
            {
                counter= counter + 1;
               
                if(acc[cmp].get("v.value").includes("Permitted"))
                {
                   filterLogic= filterLogic.replace(counter, 'true');
                }
                else
                {
                    filterLogic = filterLogic.replace(counter, 'false');
                }
    
                filterLogic = filterLogic.replace('AND', '&&');
                filterLogic = filterLogic.replace('and', '&&');
                filterLogic = filterLogic.replace('OR', '||');
                filterLogic = filterLogic.replace('or', '||');
            }

			var newFilterLogic = filterLogic.replace(/&&/g,'');
            newFilterLogic = newFilterLogic.replace(/\||/g,'');
            newFilterLogic = newFilterLogic.replace(/false/gi,'');
            newFilterLogic = newFilterLogic.replace(/true/gi,'');
            newFilterLogic = newFilterLogic.replace(/!/g,'');
            newFilterLogic = newFilterLogic.replace(/\(/g,'');
            newFilterLogic = newFilterLogic.replace(/\)/g,'');
            newFilterLogic = newFilterLogic.trim()
            if(newFilterLogic!=='') {
				component.set("v.result", "Please enter valid clearance logic");
				$A.util.addClass(saveButton, 'slds-hide');
                return;
            }

            try {
                var action = component.get('c.evaluateString');
                var obj = component.get("v.obj");
                action.setParams({
                    "filterLogic": filterLogic,
                    "obj": JSON.stringify(obj)
                });
    
                action.setCallback(this,function(response)
                {
                    //store state of response
                    var state = response.getState();
        
                    if (state === "SUCCESS") 
                    {
                        //set response value in objClassController attribute on component
                        obj = response.getReturnValue();
                        component.set('v.obj', obj);
                        component.set("v.obj.bundledResults", obj.bundledResults);
						if(obj.clearanceLogicResult) {
							component.set("v.result", "Parent product profile's clearance entry will be updated to PERMITTED");
						} else {
							component.set("v.result", "Parent product profile's clearance entry will be updated to BLOCKED");                 
						}
                        if(obj.isCommercializationMember) {
							$A.util.removeClass(saveButton, 'slds-hide');
                        }
                    }
                });
            } catch (e) {
                if (e instanceof SyntaxError) {
                    component.set("v.result", "Please enter valid clearance logic");
                }
                $A.util.addClass(saveButton, 'slds-hide');
            }
            $A.enqueueAction(action);
        }
    },
  
    relatedTab: function(component, event, helper) 
    {
        var tab1 = component.find('relatedId');
        var TabOnedata = component.find('relatedDataId');
 
        var tab2 = component.find('detailsId');
        var TabTwoData = component.find('detailsDataId');
 
        //show and Active fruits tab
        $A.util.addClass(tab1, 'slds-is-active');
        $A.util.addClass(TabOnedata, 'slds-show');
        $A.util.addClass(tab1, 'slds-has-focus');
        $A.util.removeClass(TabOnedata, 'slds-hide');
        // Hide and deactivate others tab
        $A.util.removeClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
         $A.util.removeClass(tab2, 'slds-has-focus');
        $A.util.addClass(TabTwoData, 'slds-hide');
 
    },

    detailsTab: function(component, event, helper) 
    {
        var tab1 = component.find('detailsId');
        var TabOnedata = component.find('detailsDataId');
 
        var tab2 = component.find('relatedId');
        var TabTwoData = component.find('relatedDataId');
 
        //show and Active fruits tab
        $A.util.addClass(tab1, 'sslds-is-active');
        $A.util.addClass(TabOnedata, 'slds-show');
        $A.util.addClass(tab1, 'slds-has-focus');
        $A.util.removeClass(TabOnedata, 'slds-hide');
        // Hide and deactivate others tab
        $A.util.removeClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
         $A.util.removeClass(tab2, 'slds-has-focus');
        $A.util.addClass(TabTwoData, 'slds-hide');
 
    },

    icSelect : function(component, event, helper) 
    {
        
        helper.handleMenuSelect(component, event, helper,'Input_Clearance__c');
    },

    ppaSelect : function(component, event, helper) 
    {
        helper.handleMenuSelect(component, event, helper,'Product_Profile_Association__c');
    },

  createRecordForIC : function (component, event, helper) 
  {
      
      helper.createRecord(component, event, helper,'Input_Clearance__c');
  },

  createRecordForPPA: function (component, event, helper) 
  {
      
      helper.createRecord(component, event, helper,'Product_Profile_Association__c');
  },

  redirectEditPage : function (component, event, helper) 
  {
      
      var cmpEvent = component.getEvent("bundledEvent");
      cmpEvent.setParams({
            "cp" : component.get("v.obj").cp
        });
      cmpEvent.fire();
  },

  onDelete : function (component,event,helper)
  {

    var action = component.get('c.deletePP');
    action.setParams({
            "recordId": component.get("v.currentRecordId"),
        });
    action.setCallback(this,function(response)
    {
          //store state of response
        var state = response.getState();
        if (state === "SUCCESS") 
        {
           var result = response.getReturnValue();
           if(result != 'Success') 
           {
                component.set("v.errorMSG",result);
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                var erroFooterSectn = component.find("errorFooterBtn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
           }
           else
           {

              sforce.one.navigateToURL(component.get("v.obj").delURL,true);
           }

        }

    });
        $A.enqueueAction(action);
  },


  refreshRelated : function (component, event, helper) 
  {
      
      var action = component.get('c.refreshRelatedList');
        action.setParams({
            "recordId": component.get("v.currentRecordId"),
            "obj"  :  JSON.stringify(component.get("v.obj"))
        });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set('v.obj', response.getReturnValue());
                //console.log('obj:'+ JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(action);
   },

   // this function for displaying out product family helptext
    erroFooterBtn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
        $A.util.addClass(erroFooterSectn, 'slds-show');
        $A.util.removeClass(erroFooterSectn, 'slds-hide');
    },

     // this function for displaying out product family helptext
    erroFooterSectn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
        $A.util.addClass(erroFooterSectn, 'slds-hide');
        $A.util.removeClass(erroFooterSectn, 'slds-show');
    },

})
({
	toggleSection : function(component,event,secId) {
	  var acc = component.find(secId);
        	for(var cmp in acc) {
        	$A.util.toggleClass(acc[cmp], 'slds-show');  
        	$A.util.toggleClass(acc[cmp], 'slds-hide');  
       }
	},

	handleMenuSelect : function(component, event, helper,objName) 
    {
        var recordSel = event.getParam("value");

        if(recordSel.includes("Edit"))
        {
           recordSel = recordSel.replace("Edit", "");
           sforce.one.editRecord(recordSel);
        }
        else
        {
            
          var action = component.get('c.deleteRecord');
          action.setParams({
            "recordId": component.get("v.currentRecordId"),
            "obj"  :  JSON.stringify(component.get("v.obj")),
            "ObjName" : objName,
            "delId" : recordSel
           });
          
          action.setCallback(this,function(response)
          {
              //store state of response
              var state = response.getState();
              if (state === "SUCCESS") 
              {
                  //set response value in objClassController attribute on component
                  component.set('v.obj', response.getReturnValue());
              }
          });
          $A.enqueueAction(action);
        }
    },
    
    createRecord :  function (component,event,helper,objName)
    {
        sforce.one.createRecord(objName);
    }
})
({
	doInit : function(component, event, helper) 
    {
        var action = component.get('c.initMethod');
        //console.log('path:'+decodeURIComponent(window.location.search.substring(1)));
        action.setParams({
            "recordId": component.get("v.currentRecordId"),
            "cp" : component.get("v.cp")
       });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                var obj = response.getReturnValue();
                component.set('v.cp', obj.cp);
                component.set('v.obj', obj);

                //console.log('obj:'+ JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(action);
    },

    onBusinessUnit : function(component, event, helper)
    {
        var obj= component.get("v.obj");
        var bV = component.find("businessId").get("v.value");

        component.set("v.clearanceFamily",obj.controllingInfo[bV]);  

    },

    cancel: function(component,event,helper)
    {
    	var val = component.get("v.fromMainCmp"); 

        if( val == false)
        { 
        	
            sforce.one.navigateToURL(component.get("v.obj").cancelURL,true);
        }
        else
        {
    	    var cmpEvent = component.getEvent("bundledEvent");
            cmpEvent.setParams({
               "cp" : component.get("v.cp")
            });
            cmpEvent.fire();
       } 
    },

    save : function(component,event,helper)
    {
    	helper.saveMethod(component,event,helper,false);
       
    },

    saveAndNew :function(component,event,helper)
    {
        helper.saveMethod(component,event,helper,true);
    },


    showSpinner: function(component, event, helper) 
    {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
  
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper)
    {
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
})
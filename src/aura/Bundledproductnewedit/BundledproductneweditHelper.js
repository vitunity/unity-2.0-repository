({
	saveMethod : function(component,event,helper,saveNew) 
	{
	   
	    var val = component.get("v.currentRecordId");
        var cp = component.get("v.cp");
        var businessId = component.find("businessId").get("v.value");
        var clearanceFamilyId = component.find("clearanceFamilyId").get("v.value");
        var action = component.get('c.saveRecord');
        action.setParams({
            "recordId": val,
            "cp" : JSON.stringify(cp),
            "businessId"    : businessId,
            "clearanceFamilyId"    : clearanceFamilyId,
       });
       action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                var returnId = response.getReturnValue();
                if(returnId.includes("Error"))
                {
                	 component.set("v.errors", returnId);
                }
                else if(saveNew == false)
                {
                   sforce.one.navigateToSObject(returnId,"refresh");
                   
                }
                else
                {
                	sforce.one.navigateToURL('/apex/Bundledproductnewedit',true);
                }
            }
        });
        $A.enqueueAction(action);	
	}
})
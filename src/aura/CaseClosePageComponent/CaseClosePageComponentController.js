({
    doInit : function (component, event, helper)
    {
        //alert("in Init");
         helper.showSpinner(component, event, helper);
         var recordId = component.get("v.recordId");
         //alert(recordId);
         var caseType = component.get("v.caseType");
         //alert(caseType);
         var createArticle = component.get("v.createArticle");
            //alert(createArticle);
         component.find('cstypeid').set('v.value', caseType);
         setTimeout(function(){helper.hideSpinner(component, event, helper); }, 1000);
    },
    closeModel: function(component, event, helper) 
    {
        component.set("v.showPopup", false);
        //$A.get('e.force:refreshView').fire();
    },
	
    saveData : function(component, event, helper) 
    {
        component.set("v.showPopup", false);
        //$A.get('e.force:refreshView').fire();
    },
   
    onPicklistLoad : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");                        
        component.set("v.refreshFlag",true);
    },

    onChangeCaseType : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var createArticle = component.get("v.createArticle");
           // alert(createArticle);
        var caseObject = component.get("v.caseObject");
        caseObject.Case_Type__c = value;
        component.set("v.caseObject",caseObject);
    },
    onChangeCL1 : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var caseObject = component.get("v.caseObject");
        caseObject.Classification_1__c = value;
        component.set("v.caseObject",caseObject);
    },
    onChangeCL2 : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var caseObject = component.get("v.caseObject");
        caseObject.Classification_2__c = value;
        component.set("v.caseObject",caseObject);
    },
    onChangeCL3 : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var caseObject = component.get("v.caseObject");
        caseObject.Classification_3__c = value;
        component.set("v.caseObject",caseObject);
    },
    onChangeCloseCaseReason : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var caseObject = component.get("v.caseObject");
        caseObject.Closed_Case_Reason__c = value;
        component.set("v.caseObject",caseObject);
    },
    onChangeCaseActivity : function(component, event, helper) 
    {
        var value = event.getSource().get("v.value");
        var caseObject = component.get("v.caseObject");
        caseObject.Local_Case_Activity__c = value;
        component.set("v.caseObject",caseObject);
        
    },
    saveCaseData : function(component, event, helper) 
    {
       debugger;
       var createArticle = event.getSource().get("v.createArticle");
       console.log(createArticle);
       var caseObject = component.get("v.caseObject");
       var message = 'True';
        var caseRec1;
       caseObject.Local_Case_Activity__c = component.find("recId").get("v.value");
       caseObject.Case_Type__c = component.find("cstypeid").get("v.value");
       if(caseObject.Case_Type__c == '' || caseObject.Case_Type__c == 'undefined' || caseObject.Case_Type__c == undefined)
       {
           message = 'Please select case type.'
       }

       if(message =='True' && caseObject.Classification_1__c == '')
       {
           message = 'Please select Classification 1.'
       }

       if(message =='True' && caseObject.Classification_2__c  == '')
       {
           message = 'Please select Classification 2.'
       }

       if(message =='True' && caseObject.Closed_Case_Reason__c  == '')
       {
           message = 'Closed Case Reason: You must enter a value.'
       }
      
        if(message != 'True')
        {
            component.set("v.message",message);
            var cmpTarget = component.find('headerNotify');
            $A.util.removeClass(cmpTarget, 'slds-theme_success');
            $A.util.removeClass(cmpTarget, 'slds-theme_error');
            $A.util.addClass(cmpTarget, 'slds-theme_error');
            helper.toggleClass(component,true,"SubmitPopup");
        }

        if(message == 'True')
        {

            helper.showSpinner(component, event, helper);
			var createArticle = component.get("v.createArticle");
            var recordId = component.get("v.recordId");
			var fetchCase = component.get("c.getCase");
	        fetchCase.setParams({
	            "caseId" :  recordId
	         });
            fetchCase.setCallback(this, function(response)
	        {
            	debugger;
                //alert('1');
                var state = response.getState();
		        if (state === "SUCCESS") 
		        {
                    caseRec1 = response.getReturnValue();
                    component.set("v.caseRecord",caseRec1);
                   // alert('caserec'+caseRec1);
					var caseInfo = component.get("c.updateCloseCase");
					caseInfo.setParams({
						"caseId" :  recordId,
						"caseObject" : caseObject
					 });

					caseInfo.setCallback(this, function(response)
					{
						
						debugger;
						var state = response.getState();
						if (state === "SUCCESS") 
						{
						   if(response.getReturnValue().includes("Failure"))
							{
								message = response.getReturnValue();
								message = message.replace("Failure :","");
								component.set("v.message",message);
								var cmpTarget = component.find('headerNotify');
								$A.util.removeClass(cmpTarget, 'slds-theme_success');
								$A.util.removeClass(cmpTarget, 'slds-theme_error');
								$A.util.addClass(cmpTarget, 'slds-theme_error');
								helper.toggleClass(component,true,"SubmitPopup");
								helper.hideSpinner(component, event, helper); 
							}
							else
							{   
                                //alert(createArticle);
								if (createArticle == true)
								{
									helper.toggleClass(component,true,"SubmitPopup");
									helper.hideSpinner(component, event, helper); 
									  var action = component.get("c.getRecordTypeId");
									  action.setParams({ 
										"recTypeName" : component.find("levels").get("v.value") 
									   });
									  action.setCallback(this, function(response) 
									  {
										 var state = response.getState();
										 if (state === "SUCCESS") 
										 {
											var recid = response.getReturnValue();
											 var caseObject = component.get("v.caseRecord");
											var articleRecTypeName = component.find("levels").get("v.value");
        									if (articleRecTypeName === 'Q&A')
                                            {    
												var createRecordEvent = $A.get("e.force:createRecord");
												createRecordEvent.setParams({
													"entityApiName": "Service_Article__kav",
													"recordTypeId" : recid,
													"defaultFieldValues": {
														'Answer__c' : caseObject.Local_Case_Activity__c,
                                                    	'Title' : caseObject.Subject,
														'Manager_Name__c' : caseObject.Applies_To__c,
														'Properties__c' : caseObject.ECF_Investigation_Notes__c,
														'Solution__c' : caseObject.Local_Case_Activity__c,
														'Symptoms__c' : caseObject.Symptoms__c,
														'SourceId': caseObject.Id
													}
												});
                                                
											 	var workspaceAPI = component.find("workspace");
												workspaceAPI.getFocusedTabInfo().then(function(response) {
												 var focusedTabId = response.tabId;
												 workspaceAPI.closeTab({tabId: focusedTabId}).then(function(response){ sObjectEvent.fire();   $A.get('e.force:refreshView').fire();});
												})
												createRecordEvent.fire();
											}
											else if (articleRecTypeName === 'How To')
                                            {    
												var createRecordEvent = $A.get("e.force:createRecord");
												createRecordEvent.setParams({
													"entityApiName": "Service_Article__kav",
													"recordTypeId" : recid,
													"defaultFieldValues": {
														'Process__c' : caseObject.Local_Case_Activity__c,
                                                    	'Title' : caseObject.Subject,
														'Manager_Name__c' : caseObject.Applies_To__c,
														'Properties__c' : caseObject.ECF_Investigation_Notes__c,
														'Solution__c' : caseObject.Local_Case_Activity__c,
														'Symptoms__c' : caseObject.Symptoms__c,
														'SourceId': caseObject.Id
													}
												});
											 	var workspaceAPI = component.find("workspace");
												workspaceAPI.getFocusedTabInfo().then(function(response) {
												 var focusedTabId = response.tabId;
												 workspaceAPI.closeTab({tabId: focusedTabId}).then(function(response){ sObjectEvent.fire();   $A.get('e.force:refreshView').fire();});
												})
												createRecordEvent.fire();
											}
											else
											{
												var createRecordEvent = $A.get("e.force:createRecord");
												createRecordEvent.setParams({
													"entityApiName": "Service_Article__kav",
													"recordTypeId" : recid,
													"defaultFieldValues": {
                                                    	'Title' : caseObject.Subject,
														'Manager_Name__c' : caseObject.Applies_To__c,
														'Properties__c' : caseObject.ECF_Investigation_Notes__c,
														'Solution__c' : caseObject.Local_Case_Activity__c,
														'Symptoms__c' : caseObject.Symptoms__c,
														'SourceId': caseObject.Id
													}
												});
											 	var workspaceAPI = component.find("workspace");
												workspaceAPI.getFocusedTabInfo().then(function(response) {
												 var focusedTabId = response.tabId;
												 workspaceAPI.closeTab({tabId: focusedTabId}).then(function(response){ sObjectEvent.fire();   $A.get('e.force:refreshView').fire();});
												})
												createRecordEvent.fire();
											}
										 }
									 });
                                    $A.enqueueAction(action);
								}
								else
								{    
                                    //alert('1');
                                   
									var sObjectEvent = $A.get("e.force:navigateToSObject");
                                    //alert('2');
									sObjectEvent.setParams({
										"recordId": component.get("v.recordId"),
									})
                                    //alert('3');
									var workspaceAPI1 = component.find("workspace");
                                    //alert('4');
									workspaceAPI1.getFocusedTabInfo().then(function(response) {
										var focusedTabId = response.tabId;
										workspaceAPI1.closeTab({tabId: focusedTabId}).then(function(response){ sObjectEvent.fire();   $A.get('e.force:refreshView').fire();});
									})
									.catch(function(error) {
										console.log(error);
									});
                                    //alert('5');
								}
                                
							}
							
						 
						}
					});
					$A.enqueueAction(caseInfo);
                }
            });
            $A.enqueueAction(fetchCase);   
           // alert(2);
	        

         }
    },

    hideAlertModal : function(component,event,helper)
    {
        helper.toggleClass(component,false,"SubmitPopup");
    },

    backToCase: function(component,event,helper)
    {
    	var sObjectEvent = $A.get("e.force:navigateToSObject");
	    sObjectEvent.setParams({
	        "recordId": component.get("v.recordId"),
	    })
	    sObjectEvent.fire();
    },
})
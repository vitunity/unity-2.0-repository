({
	showSpinner : function(component, event, helper) 
	{
		$A.util.removeClass(component.find('spinner'), "slds-hide");
	},

    hideSpinner : function(component, event, helper) 
	{
		 $A.util.addClass(component.find('spinner'), "slds-hide");  
	},

	toggleClass: function(component,showModal, modalName) {
       // console.log('---in toggle class');
		var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
	},

})
({
	doInit : function(component, event, helper) {
	var action = component.get("c.fetchUserProfile");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    
    navigateToUpdateCaseTeamMember : function(component, event, helper) {
    	var evt = $A.get("e.force:navigateToComponent");
    	evt.setParams({
        	componentDef : "c:UpdateCaseTeamMember",
        	componentAttributes: {
            	recordId : component.get("v.recordId")
       	 	}
    	});
    	evt.fire();
	},

    navigateToPHI :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
           "url": "/apex/SVMXC__ServiceMaxConsole?SVMX_recordId="+caseRecord.Id+"&SVMX_action=SFM&SVMX_processId=Create_PHI_from_Case&SVMX_retURL=/"+caseRecord.Id
        });
         urlEvent.fire();
         
    },

    navigateToSCAL :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        window.open("https://apoc.varian.com/drm/actions/service/quick-search?searchText="+caseRecord.ProductSystem__r.Name+"&searchType=3","_blank");
         
    },
    
    navigateToSCBL :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        window.open("https://apoc.varian.com/drm/actions/service/quick-search?searchText="+caseRecord.ProductSystem__r.Name+"&searchType=3","_blank");
         
    },
    
    navigateToPSE :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        window.open("http://pse.oscs.varian.com/SiteSearch.asp?searchfor="+caseRecord.Subject,"_blank");
         
    },

    navigateToCWO :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/SVMXC__ServiceMaxConsole?SVMX_recordId="+caseRecord.Id+"&SVMX_action=SFM&SVMX_processId=CreateWorkOrderFromCase&SVMX_retURL=/"+caseRecord.Id
        });
         urlEvent.fire();  
    },
    
    navigateToUpdateInstallationCase :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/CreateInstallationPlan?caseid="+caseRecord.Id
        });
        urlEvent.fire();
    },
    
    navigateToCreatePWO :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        if(caseRecord.SVMXC__Site__c == null || caseRecord.SVMXC__Site__c == undefined) 
        { 
            alert('There is no location on your case. Please add location to the case or check with FOA to see if the functional location has been created.'); 
        } 
        else{
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/apex/SR_CreatePWOPage?id="+caseRecord.Id+"&cid=cs"
            });
            urlEvent.fire();
        }
        
    },
    
    navigateToPTUpdateInstallationCase :function(component, event, helper) 
    {
        var caseRecord= component.get('v.simpleRecord');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/PT_CreateInstallationPlan?caseid="+caseRecord.Id
        });
        urlEvent.fire();
    },
    // navigateToCaseCloseTesting : function(component, event, helper) {
    //     var evt = $A.get("e.force:navigateToComponent");
    //     evt.setParams({
    //         componentDef : "c:CaseClosePageComponent",
    //         componentAttributes: {
    //             recordId : component.get("v.recordId")
    //         }
    //     });
    //     evt.fire();
    // },
    
   navigateToCancelCase : function(component, event, helper) {
        //component.set('v.caseCancellation',true);
        helper.caseCancellationHelper(component, event);
    },

    navigateToCloseCase : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.simpleRecord")));
        component.set('v.closeCase',true);
        /*var caseRecord= component.get('v.simpleRecord');
        alert(caseRecord.Id);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/sr_close_case_page?id="+caseRecord.Id+"_self"
        }); 
        urlEvent.fire();*/
    },

    navigateToECF : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.simpleRecord")));
        //component.set('v.ecf',true);
        var caseObject  = component.get("v.simpleRecord");
        var urlEvent = $A.get("e.force:navigateToURL");
        if(caseObject.Is_escalation_to_the_CLT_required__c == 'Yes' 
           || caseObject.Is_escalation_to_the_CLT_required__c == 'Yes from Work Order'
           && caseObject.Is_This_a_Complaint__c =='Yes' 
           || caseObject.Was_anyone_injured__c == 'Yes'){
            if(caseObject.Case_ECF_Count__c == 0){
                urlEvent.setParams({
                "url": "/apex/SR_CreateL2848?CF00NE0000005Uovj=00646669&CF00NE0000005Uovj_lkid="+caseObject.Id+"&scontrolCaching=1&retURL=%2F"+caseObject.Id+"&save_new=1&sfdc.override=1"
            	});
            	urlEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
            	toastEvent.setParams({
                title : 'Error Message',
                message:'You cannot create ECF form as one already exists againt this Case.',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            	});
            	toastEvent.fire();
            }
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error Message',
                message:'You cannot create ECF form because Escalated Complaint Required is not set to Yes.',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'pester'
            });
            toastEvent.fire();
        	//alert('You cannot create ECF form because Escalated Complaint Required is not set to Yes.');
        }
    },

    navigateToKA : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.simpleRecord")));
        component.set('v.knowledgeArticle',true);
    },

    navigateToPWO : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.simpleRecord")));
        component.set('v.PWO',true);
    },
    navigateToTeamMember : function(component, event, helper) {
        //console.log(JSON.stringify(component.get("v.simpleRecord")));
        component.set('v.teamMember',true);
    },

    navigateToIWI :function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "http://internal.varian.com/os/CRM/End%20User%20Training/Site%20Solutions/Work%20Instructions%20-%20Installation%20Case.pdf"
        });
        urlEvent.fire();
    },

    showPopup : function(component, event, helper) {
    	component.set('v.showPopup',true);
    }
    
})
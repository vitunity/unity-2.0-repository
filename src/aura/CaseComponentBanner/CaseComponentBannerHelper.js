({
	caseCancellationHelper : function (component, event){
        var caseInfo = component.get("c.checkCaseCancellation");
        var caseObject = component.get("v.simpleRecord");
        caseInfo.setParams({
            "caseObject" :  caseObject
        });
        caseInfo.setCallback(this, function(response){
            var cmpTarget = component.find('headerNotify');
            $A.util.removeClass(cmpTarget, 'slds-theme_success');
            $A.util.removeClass(cmpTarget, 'slds-theme_error');
            var updateStatus = response.getReturnValue();
            debugger;
            if(updateStatus.includes("success"))
            { 
                //updateStatus = updateStatus.replace("success", "");
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success Message',
                    message: updateStatus,
                    messageTemplate: 'Record {0} created! See it {1}!',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'pester'
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();

            }else{
            	var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Error Message',
                    message: updateStatus,
                    messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }

        });
        $A.enqueueAction(caseInfo);
	},
})
({
    doInit : function (component, event, helper)
    {
        helper.showSpinner(component, event, helper);
        //Get Work Order defaults
       	//alert(1);

        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) 
        {
            var cmpTarget = component.find('headerNotify');
            $A.util.removeClass(cmpTarget, 'slds-theme_success');
            $A.util.removeClass(cmpTarget, 'slds-theme_error');
            if(response)
            {
                
                var caseObject = component.get("v.caseRecord");
                //alert(caseObject);
                helper.hideSpinner(component, event, helper);
               // component.set("v.showPopup", false);
                // var urlEvent = $A.get("e.force:navigateToURL");
                // urlEvent.setParams({
                //   "url": '/knowledge/publishing/articleEdit.apexp?retURL=%2F'+caseObject.Id+'&sourceId='+caseObject.Id+'&sfdc.override=1&type=Service_Article__kav&fromPage=ARTICLE' 
                // });
                // urlEvent.fire();
            }
            else
            {
                $A.util.addClass(cmpTarget, 'slds-theme_error');
                component.set('v.message', 'You must be in the Helpdesk Console to close a case.');
                helper.hideSpinner(component, event, helper);
            }

                
        }).catch(function(error) {
            console.log(error);
        });
      
    },
    closeModel: function(component, event, helper) 
    {
        component.set("v.showPopup", false);
   },

   createArticle : function(component, event, helper) 
   {
     helper.showSpinner(component, event, helper);
       
      var action = component.get("c.getRecordTypeId");
      action.setParams({ 
        "recTypeName" : component.find("levels").get("v.value") 
       });

      action.setCallback(this, function(response) 
      {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var recid = response.getReturnValue();
                 /*var urlEvent = $A.get("e.force:navigateToURL");
   		          urlEvent.setParams({
   	             "url": "/knowledge/publishing/articleEdit.apexp?retURL=%2F"+caseid+"&sourceId="+caseid+"&sfdc.override=1&type=Service_Article__kav&rtId="+recid+"&fromPage=ARTICLE"
   	            });
                helper.hideSpinner(component, event, helper);
                component.set("v.showPopup", false);
               urlEvent.fire();*/
                var articleRecTypeName = component.find("levels").get("v.value");
        		//alert(articleRecTypeName);
				if (articleRecTypeName === 'Q&A')
				{
					var createRecordEvent = $A.get("e.force:createRecord");
					createRecordEvent.setParams({
						"entityApiName": "Service_Article__kav",
						"recordTypeId" : recid,
						"defaultFieldValues": {
							'Answer__c' : component.get("v.caseRecord").Local_Case_Activity__c,
							'Title' : component.get("v.caseRecord").Subject,
							'Manager_Name__c' : component.get("v.caseRecord").Applies_To__c,
							'Properties__c' : component.get("v.caseRecord").ECF_Investigation_Notes__c,
							'Solution__c' : component.get("v.caseRecord").Local_Case_Activity__c,
							'Symptoms__c' : component.get("v.caseRecord").Symptoms__c,
							'SourceId': component.get("v.caseRecord").Id
					  }
					});
					helper.hideSpinner(component, event, helper);
					component.set("v.showPopup", false);
					createRecordEvent.fire();
				}
				else if (articleRecTypeName === 'How To')
				{
					var createRecordEvent = $A.get("e.force:createRecord");
					createRecordEvent.setParams({
						"entityApiName": "Service_Article__kav",
						"recordTypeId" : recid,
						"defaultFieldValues": {
							'Process__c' : component.get("v.caseRecord").Local_Case_Activity__c,
							'Title' : component.get("v.caseRecord").Subject,
							'Manager_Name__c' : component.get("v.caseRecord").Applies_To__c,
							'Properties__c' : component.get("v.caseRecord").ECF_Investigation_Notes__c,
							'Solution__c' : component.get("v.caseRecord").Local_Case_Activity__c,
							'Symptoms__c' : component.get("v.caseRecord").Symptoms__c,
							'SourceId': component.get("v.caseRecord").Id
					  }
					});
					helper.hideSpinner(component, event, helper);
					component.set("v.showPopup", false);
					createRecordEvent.fire();
				}
				else
				{
					var createRecordEvent = $A.get("e.force:createRecord");
					createRecordEvent.setParams({
						"entityApiName": "Service_Article__kav",
						"recordTypeId" : recid,
						"defaultFieldValues": {
							'Title' : component.get("v.caseRecord").Subject,
							'Manager_Name__c' : component.get("v.caseRecord").Applies_To__c,
							'Properties__c' : component.get("v.caseRecord").ECF_Investigation_Notes__c,
							'Solution__c' : component.get("v.caseRecord").Local_Case_Activity__c,
							'Symptoms__c' : component.get("v.caseRecord").Symptoms__c,
							'SourceId': component.get("v.caseRecord").Id
					  }
					});
					helper.hideSpinner(component, event, helper);
					component.set("v.showPopup", false);
					createRecordEvent.fire();
				}
            }
      });
      $A.enqueueAction(action);
   }
})
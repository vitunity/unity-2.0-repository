({
	showSpinner : function(component, event, helper) 
	{
		$A.util.removeClass(component.find('spinner'), "slds-hide");
	},

    hideSpinner : function(component, event, helper) 
	{
		 $A.util.addClass(component.find('spinner'), "slds-hide");  
	},

})
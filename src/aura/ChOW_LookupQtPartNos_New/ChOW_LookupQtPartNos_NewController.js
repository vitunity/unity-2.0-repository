({
	doInit : function(component, event, helper) {
		var qtId = component.get("chowToolQtId");
		//alert('chowToolQtId in Chow_LookupfParts init = ' + component.get("v.chowToolQtId"));
		var qtNo = component.get("chowToolQtNo");
		//alert('chowToolQtNo in Chow_LookupfParts init = ' + component.get("v.chowToolQtNo"));
		//if(qtId != null && qtId != undefined) {
			helper.loadPartsNew(component);
		//}
	},

    callNext : function(component, event, helper) {
        helper.nextRecord(component, event);
    },

    callPrevious : function(component, event, helper) {
        helper.previousRecord(component, event);
    },
    
    partChange : function(component, event, helper){
        helper.loadPartsNew(component);
    },
    
    sendSelectedPart : function(component, event){
    	//alert('sendSelectedPart called');
    	var partNumber = event.getSource().get("v.label");
    	//alert('partNumber in sendSelectedPart = ' + partNumber);
        var partSelectedEvent = component.getEvent("partSelected");
        //alert('----partSelectedEvent = '+partSelectedEvent);
        partSelectedEvent.setParams({"partNumber": partNumber});
        ////partnerSelectedEvent.setParams({"partnerFunction": component.get("v.partnerFunction_LP")});
        partSelectedEvent.fire();
	},	

    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.loadPartsNew(component, false);
    },
})
({

    loadPartsNew : function(lookupComponent, resetPages){
        if(resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }

        //alert('loadParts called');
        //alert('in loadParts chowToolQtNo = ' + lookupComponent.get('v.chowToolQtNo'));
        this.showSpinner(lookupComponent, true);
        var action = lookupComponent.get("c.getParts");
        action.setParams({
            "chowToolQtId": lookupComponent.get('v.chowToolQtId'), 
            "searchText": lookupComponent.get('v.searchText_LP'),
            "recordsOffset": lookupComponent.get("v.recordsOffset"),
            "totalSize": lookupComponent.get("v.totalSize")         
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert('state in loadPartsNew = ' + state);
            if (state === "SUCCESS") {
            	var partList = response.getReturnValue();

                //lookupComponent.set("v.partList", partList.lstParts);
                if(partList.totalSize == 0 ){
                    lookupComponent.set("v.totalSize",5);
                }else{
                	lookupComponent.set("v.totalSize",partList.totalSize);
                }  

                var pageSize = lookupComponent.get("v.pageSize");
                lookupComponent.set('v.partList', partList.lstParts);
                lookupComponent.set("v.totalRecords", lookupComponent.get("v.partList").length);
                lookupComponent.set("v.startPage", 0);                
                lookupComponent.set("v.endPage", pageSize - 1);
                var PagList = [];
                for ( var i=0; i< pageSize; i++ ) {
                    if (lookupComponent.get("v.partList").length> i )
                        PagList.push(response.getReturnValue().lstParts[i]);    
                }
                lookupComponent.set('v.PaginationList', PagList);             
            }
            this.showSpinner(lookupComponent, false);
        });
        // Invoke the service
        $A.enqueueAction(action);
    },

	nextRecord: function (component, event, helper) {
	  var sObjectList = component.get("v.partList");
	        var end = component.get("v.endPage");
	        var start = component.get("v.startPage");
	        var pageSize = component.get("v.pageSize");
	        var PagList = [];
	        var counter = 0;
	        for ( var i = end + 1; i < end + pageSize + 1; i++ ) {
	            if ( sObjectList.length > i ) {
	                PagList.push(sObjectList[i]);
	            }
	            counter ++ ;
	        }
	        start = start + counter;
	        end = end + counter;
	        component.set("v.startPage", start);
	        component.set("v.endPage", end);
	        component.set('v.PaginationList', PagList);
	 },

	 previousRecord: function (component, event, helper) {
	  var sObjectList = component.get("v.partList");
	        var end = component.get("v.endPage");
	        var start = component.get("v.startPage");
	        var pageSize = component.get("v.pageSize");
	        var PagList = [];
	        var counter = 0;
	        for ( var i= start-pageSize; i < start ; i++ ) {
	            if ( i > -1 ) {
	                PagList.push(sObjectList[i]);
	                counter ++;
	            } else {
	                start++;
	            }
	        }
	        start = start - counter;
	        end = end - counter;
	        component.set("v.startPage", start);
	        component.set("v.endPage", end);
	        component.set('v.PaginationList', PagList);
	 },

    showSpinner : function(cmp, isLoading){
        var cmpTarget = cmp.find('statusSpinner');
        if(isLoading){
            $A.util.addClass(cmpTarget, 'slds-show');
        	$A.util.removeClass(cmpTarget, 'slds-hide');
    	}else{
			$A.util.addClass(cmpTarget, 'slds-hide');
        	$A.util.removeClass(cmpTarget, 'slds-show');
		}    
	}
})
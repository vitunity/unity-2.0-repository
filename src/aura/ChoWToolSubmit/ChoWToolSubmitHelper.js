({
    validateAssignEmail : function(component, event) {
        var Id1 = component.get("v.recordId");
        //alert('Id1 = ' + Id1);
        var action = component.get("c.getChowInfo");
       // console.log('action-----'+action)
        action.setParams({
            "chowId" : Id1
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();    
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                var assignEmail = returnValue.Assignee_Email__c;
                if(assignEmail == null || assignEmail == undefined) {
                    component.set("v.messageStr", "Form cannot be submitted without Active Assignee.");
                    //$A.get("e.force:closeQuickAction").fire();
                    //return;
                } else {
                   this.callSendEmail(component, event);
                }
            }
        });
        $A.enqueueAction(action);        
    },
    
    callSendEmail : function(component, event) {
          var Id = component.get("v.recordId");
          var action = component.get("c.sendEmail");       
          action.setParams({ chowId : Id});
		      action.setCallback(this, function(response) {
              var state = response.getState();
              console.log('state1'+state)
              if(state === "SUCCESS") {
				  component.set("v.messageStr", "ChoW Details updated Successfully!");                 
                  //$A.get("e.force:closeQuickAction").fire();
                  $A.get('e.force:refreshView').fire();
              }else if (state == "ERROR"){
                  var errors = response.getError();
                  var toastEvent = $A.get("e.force:showToast");
                  toastEvent.setParams({
                      "title": "ERROR!",
                      "message": errors[0].message
                  });
                toastEvent.fire();
                  if (errors[0] && errors[0].message) {
                    // Did not catch on the Server Side
                    component.set("v.message", errors[0].message);
                    component.set("v.messageError", true);
                }
                  console.log('Problem getting chow tool, response state: ' + state);
              }
          });
          $A.get("e.force:navigateToComponent");
        
          $A.enqueueAction(action);        
    }
})
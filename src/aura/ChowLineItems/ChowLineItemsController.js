({
    doInit : function(component, event, helper) {
        // var Id = component.get("v.chowToolId");
        // alert('recordId = ' + Id);       
        helper.getChowToolQtId(component, event);
        helper.initializeChowLine(component, event);
    },

    showPartModal: function(component, event, helper) {
        var partAuraId = event.getSource().get("v.name");
        //alert('partAuraId = '+ partAuraId);
        var partComponent = component.find(partAuraId);
        //alert('partnerComponent = '+ partComponent);
        var partValue = partComponent.get("v.value");
        //alert('partValue = ' + partValue);
        helper.setChowToolQtNo(component, event);
                
        component.set("v.searchText", partValue);
        helper.toggleClass(component,true,"partPopUp");
    },   

    createChowLineItem : function(component, event, helper) {
        var qty = component.find("quantity").get("v.value");
        //alert('qty = ' + qty);
        if(qty == null || qty == undefined){
            alert('Please enter a quantity before saving.');
            return;
        }

        helper.saveChowLineItem(component, event);
    },

    hidePartModal : function(component, event, helper) {
        console.log('----in hideModel');
        //Toggle CSS styles for hiding Modal
        helper.toggleClass(component,false, "partPopUp");
    },    

    setSelectedPart : function(component, event, helper){
        //alert('setSelectedPartner called');
        var partNumber = event.getParam("partNumber");
        //alert('partNumber in setSelectedPart = ' + partNumber);
        //var partnerFunction = event.getParam("partnerFunction");
        console.log('--in setSelectedPart'+partNumber);
        //component.set("v.prepareOrderController.soldToNumber", partnerNumber);

        component.find("partNoValue").set("v.value", partNumber); 
        helper.updatePartDetails(component, partNumber);

        helper.toggleClass(component, false, "partPopUp");
    }, 

    doCancel : function(component, event, helper) {
        var chowToolId = component.get("v.chowToolId");
        var chowRecUrl = $A.get("$Label.c.Chow_Record_URL"); //'https://varian--sfqa.lightning.force.com/lightning/r/Chow_Tool__c/';
        var url = chowRecUrl + chowToolId +'/view?0.source=alohaHeader';
        window.location.href  = url;         
    }, 
    
})
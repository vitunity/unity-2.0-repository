({
    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
    },

    initializeChowLine : function(component, event) {
        var chowToolId = component.get("v.chowToolId");
        //alert('chowToolId = ' + chowToolId);

        var action = component.get("c.initChowLineItem");
        action.setParams({
            "chowToolId" : chowToolId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();        
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.chowLineItem", returnValue);
            }
        });
        $A.enqueueAction(action);
    },

    getChowToolQtId: function(component, event) {
        var chowToolId = component.get("v.chowToolId");
        //alert('recordId in getChowToolQtId = ' + chowToolId);  

        var action = component.get("c.getChowToolInfo");
       // console.log('action-----'+action)
        action.setParams({
            "chowToolId" : chowToolId
        });//
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            //alert('state in getChowToolQtId = ' + state);        
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                //alert('returnValue in getChowToolQtId '+ JSON.stringify(returnValue));
                //var partnerObject = JSON.parse(JSON.stringify(returnValue));
                //component.set(componentName, partnerObject);
                component.set("v.chow", returnValue);

                component.set("v.chowToolId", returnValue.Id);
                component.set("v.chowToolQtId", returnValue.Quote_Number__c);
            }
        });
        $A.enqueueAction(action);               
    },

    setChowToolQtNo: function(component, event) {

        var qtId = component.get("v.chowToolQtId");
        //alert('Quote id to pass to lookup comp = ' + qtId);

        var action = component.get("c.getQuoteInfo");
       // console.log('action-----'+action)
        action.setParams({
            "quoteId" : qtId
        });//
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            //alert('state in setChowToolQtNo = ' + state);        
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                //alert('quote no = ' + returnValue.Name);
                component.set("v.chowToolQtNo", returnValue.Name);
            }
        });
        $A.enqueueAction(action);               
    },

    updatePartDetails : function(component, partNumber){
        var quoteId = component.get("v.chowToolQtId");
        var action = component.get("c.getPartDetails");
        action.setParams({
            "partNumber" : partNumber,
            "quoteId" : quoteId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();        
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.chowLineItem.Quote_part_number__c", returnValue.Name);
                component.set("v.partNumberSelectedId", returnValue.Id);
            }
        });
        $A.enqueueAction(action);
    },

    saveChowLineItem : function(component, event) {
        var chowToolId = component.get("v.chowToolId");
        //alert('chowToolId = ' + chowToolId);

        var quoteId = component.get("v.chowToolQtId");
        var quoteProductId = component.get("v.partNumberSelectedId");

        var action = component.get("c.saveChowLineItemDelete");
        action.setParams({
            "chowToolId" : chowToolId,
            "quoteId" : quoteId,
            "quoteProductId" : quoteProductId,
            "chowLineRec": component.get("v.chowLineItem")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();        
            if(state == "SUCCESS" && component.isValid()) {
                var recordId = response.getReturnValue().Id;
                //alert('record id returned = ' + recordId);
                //var url = 'https://varian--sfdev.lightning.force.com/lightning/r/Chow_Tool__c/'+recordId+'/view';
                var chowRecUrl = $A.get("$Label.c.Chow_Record_URL"); //'https://varian--sfdev.lightning.force.com/lightning/r/Chow_Tool__c/';
                var url = chowRecUrl + chowToolId +'/view';
                window.location.href  = url; 
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);



    }






})
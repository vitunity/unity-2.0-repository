({
    doInit: function(component, event, helper) {     

        
        //var Id = component.get("v.recordId");
        // console.log(' === ' + component.get("v.masterQuoteId"));
        //alert('Id = ' + Id);
        helper.fetchPickListVal(component, 'Priority_Level__c', 'PriorityLevel');
        component.find("PriorityLevel").set("v.value", "Routine");
        helper.fetchPickListVal(component, 'Category__c', 'TypeofChange');
        helper.fetchPickListVal(component, 'Priority_Reason__c', 'PriorityReason');
        helper.fetchPickListVal(component, 'Request_Status__c', 'RequestStatus');
        component.find("RequestStatus").set("v.value", "Pending");

        component.find("Comments").set("v.disabled", true); 

        var masterQtId = component.get("v.masterQuoteId"); //component.get("v.masterQuoteId");//'a3544000000QOja';  
        console.log('==masterQtId=='+masterQtId); 
        if(masterQtId != null && masterQtId != undefined) {
            component.find("quoteId").set("v.value", masterQtId);
            helper.getQuoteInfo(component, event);
        }
		
        console.log('==masterQtId=='+masterQtId); 
        var masterOrdId = component.get("v.masterOrderId"); //component.get("v.masterQuoteId");//'a3544000000QOja';
        if(masterOrdId != null && masterOrdId != undefined) {
            component.find("sOrderId").set("v.value", masterOrdId);
            helper.getSalesOrderInfo(component, event);
        }  

        //helper.callPopulateActiveTeam(component, event);        
    },

    populateActiveTeam : function(component, event, helper) {
        helper.callPopulateActiveTeam(component, event);
    },

    partnerAutoPopulate:function(component, event, helper) { 
        
        //alert('partnerAutoPopulate called partner id = ' + component.get("v.partnerId"));
		
        var partnerId = component.get("v.partnerId");
        console.log(JSON.stringify(component.get('v.accountLookUpRecord.Name')));
        var action = component.get("c.getPartnerAddress");
         action.setParams({ "partnerId" : partnerId});
        action.setCallback(this,function(response) {            
             var state = response.getState();   
             //alert('state = ' + state);
             //alert('return = ' + JSON.stringify(response.getReturnValue()));       
            if (state === "SUCCESS") {                
                component.set("v.chow.State__c", response.getReturnValue().State_Province_Code__c);
                ////component.set("v.chow.Geo_Region__c", response.getReturnValue().Region__c);
               component.set("v.chow.City__c", response.getReturnValue().City__c);  

                component.find("city").set("v.value", response.getReturnValue().City__c);

                console.log(response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
         $A.enqueueAction(action);
    },	

    teamMemberAutoPopulate:function(component, event, helper) {
        var teamMember =component.get("v.teamMemeberLookUpRecord.Name");
        console.log('teamMemeberLookUpRecord'+teamMember);
    	var action = component.get("c.getChowTeamMember");
         action.setParams({ "teamMem" : teamMember});
         action.setCallback(this,function(response) {            
             var state = response.getState();            
            if (state === "SUCCESS") {                
                 component.set("v.chow.Active_Teams__c", response.getReturnValue().Active_Team__c); 
                component.set("v.chow.Active_Task_Assigne__c", response.getReturnValue().Team_Member_Name__r.Name); 
                console.log(response.getReturnValue());
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    salesOrderAutoPopulate:function(component, event, helper) {
        helper.clearFields(component, event);
        var soSelected = component.find("sOrderId").get("v.value");
        //alert('soSelected = ' + soSelected);
        var soSelectedData = component.get("v.chow.Sales_Order__c");
        //alert('soSelectedData = ' + soSelectedData);
        helper.getSalesOrderInfo(component, event);
    },     

    quoteAutoPopulate:function(component, event, helper) {
        //alert('quoteAutoPopulate called');
        helper.clearFields(component, event);
        console.log(' Quote Auto Populate Called ');
        helper.getQuoteInfo(component, event);
    },      

    onPicklistChange: function(component, event, helper) {
        // get the value of select option
       // alert(event.getSource().get("v.value"));
    },

    solToAccountChanged : function(component, event, helper) {
        component.find("sitePartnerValue").set("v.value", null);
        //component.find("geoRegion").set("v.value", null);
        component.find("city").set("v.value", null);
        //component.find("erpSitePartnerId").set("v.value", null);
        //component.find("erpSoldToId").set("v.value", null);
        debugger;
        helper.getSoldToAccountInfo(component, event);
    },
   
    changeTeam : function(component, event, helper) {
        helper.getTaskAssignee(component, event);
    },

    createChowTool : function(component, event, helper) {
     // debugger;
        var selectedvalues = component.get("v.selectedLookUpRecords");
        var soSelected = component.find("sOrderId").get("v.value");
        var soSelected1 = component.get("v.chow.Sales_Order__c");
        var quoteSelected = component.find("quoteId").get("v.value"); 
        var partnerId = component.get("v.partnerId"); 
        var accId = component.find("accountId").get("v.value"); 
        var unknown1 = component.find("unknown").get("v.value"); 
        var comments = component.find("commentsId").get("v.value");

        var activeTeam = component.find("ActiveTeam").get("v.value");
        var activeAssignee = document.getElementById('TaskAssigneeId').value; //component.find("TaskAssignee").get("v.value");

        console.log('unknown = ' + unknown1);
        console.log('soSelected = ' + soSelected);
        // alert('soSelected1 = ' + soSelected1);
        // alert('quoteSelected = ' + quoteSelected);
		console.log(' quoteSelected => ' + quoteSelected);
        if(unknown1 == undefined || unknown1 == false) {
            unknown1 = false;
        }

        //alert('activeTeam = ' + activeTeam); 
        //alert('activeAssignee = ' + activeAssignee); 

        // alert('soSelected = ' + soSelected);
        // alert('quoteSelected = ' + quoteSelected);   
        // alert('comments = ' + comments);  

        if(soSelected == null && quoteSelected == null && unknown1 == false) {
            //confirm('Please enter either Quote or Order or check Sales Order/Quote unknown before saving the form.');
            
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            component.set("v.errors", 
                          'Please enter either Quote or Order or check Sales Order/Quote unknown before saving the form.');
            return;
           
        }

        if(activeTeam == null || activeTeam == '--None--' || activeTeam == undefined
            || activeTeam == ''
          ) 
        {
            //confirm('Please enter either Quote or Order or check Sales Order/Quote unknown before saving the form.');
            
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            component.set("v.errors", 
                          'Please enter Active Team before saving the form.');
            return;
           
        }  

        if(activeAssignee == null || activeAssignee == '--None--' 
            || activeAssignee == undefined
            || activeAssignee == ''
          ) 
        {
            //confirm('Please enter either Quote or Order or check Sales Order/Quote unknown before saving the form.');
            
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            component.set("v.errors", 
                          'Please enter Active Task Assignee before saving the form.');
            return;
           
        }  

        if(comments == null || comments == undefined) {
            //confirm('Please enter either Quote or Order or check Sales Order/Quote unknown before saving the form.');
            
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            component.set("v.errors", 
                          'Please enter comments before saving the form.');
            return;
           
        }  
               
        // alert('selectedvalues = ' + selectedvalues);
        // alert('soSelected = ' + soSelected);
        // alert('quoteSelected = ' + quoteSelected);
        // alert('partnerId = ' + partnerId);
        // alert('sold to Account = ' + accId);

        
        var action = component.get("c.saveChowTool"); 
        console.log('selectedvalues'+selectedvalues);
        action.setParams({
            "chowRec": component.get("v.chow"),
            "sitePartnerId": partnerId,
            "sOrderId" : soSelected,
            "quoteId" : quoteSelected,
            "taskAssignee" : activeAssignee,
            "soldToActId" : accId,
            "selectedvalues": selectedvalues 
        });
         action.setCallback(this,function(response) {            
            
            var state = response.getState();    
            //('state = ' + state);    
            var recordId1 = response.getReturnValue();
            //alert('recordId1 = ' + recordId1);
            //console.log('record id 1 = ' + recordId1); 
            //console.log(' === record id === ' + JSON.stringify(response.getReturnValue()));
            //sforce.one.back(true); 
            //window.location.href  = 'https://varian--sfqa.lightning.force.com/lightning/r/Chow_Tool__c/aBW0v0000004J2KGAU/view';   
            
            // if(state == "SUCCESS" && component.isValid()) {
            if(component.isValid()) {
                //console.log(' === inside state === ' + JSON.stringify(response.getReturnValue()));
                var recordId = response.getReturnValue();
                //alert('record id returned = ' + recordId);

                //alert('The form is not Submitted yet !');

                //var url = 'https://varian--sfdev.lightning.force.com/lightning/r/Chow_Tool__c/'+recordId+'/view';
                var chowRecUrl = $A.get("$Label.c.Chow_Record_URL"); //'https://varian--sfqa.lightning.force.com/lightning/r/Chow_Tool__c/';
                var url = chowRecUrl + recordId +'/view?0.source=alohaHeader';
                window.location.href  = url; 
              
                 
                /*var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId":recordId
                });
                navEvt.fire();*/
                
                
                /*
                if(typeof sforce !== 'undefined') {
                    sforce.one.back(true);  
                }
                */
                
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);

    },    
      
    /*  
    createChowTool : function(component, event, helper) {
     // debugger;
        var selectedvalues = component.get("v.selectedLookUpRecords");       
        var action = component.get("c.saveChowTool"); 
        console.log('selectedvalues'+selectedvalues);
        action.setParams({
            "chowRec":component.get("v.chow"),
            "sitePartnerId":component.get("v.partnerId"),
            "selectedvalues":selectedvalues
        });
        $A.enqueueAction(action);
        alert('action1'+action.getState()); 
     /*   action.setCallback(this, function(response) {
            if (action.getState() === "SUCESS"){
                var recordId =response.getReturnValue();
                alert('responseId'+responseId);
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId":recordId
                });
    			navEvt.fire();
        });*/

        /*
        var intervalId;
        var checkTheAction = function(){
            console.log(action.getState());
            alert('action2'+action.getState());
            if (action.getState() === "SUCESS"){ // || action.getState() === "ERROR" || action.getState() === "INCOMPLETE"){
                var responseId =response.getReturnValue();
                alert('responseId'+responseId);
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId":responseId
                });
    			navEvt.fire();
                console.log(action.getReturnValue());
                window.clearInterval(intervalId);
            }
        };
        intervalId = window.setInterval(checkTheAction,1000);
    },

    */

    
    showPartnerModal: function(component, event, helper) {
        //Toggle CSS styles for opening Modal
        //alert('showPartnerModal called');

        var soldTo = component.find("accountId").get("v.value");    // component.get("v.soldToAccount");
        //alert('soldTo = ' + soldTo);
        if(soldTo == null || soldTo == undefined) {
            //$('#partner-error-modal').modal('show');
            alert('Please select Sold To Account first before selecting its Partner Account.')
            return;
        } 
        
        var partnerFunctionKey = event.getSource().get("v.name");
        //alert('partnerFunctionKey = ' + partnerFunctionKey);
        var partnerFunctionArray = partnerFunctionKey.split("-");
        var partnerFunction = partnerFunctionArray[0];
        //alert('partnerFunction = '+ partnerFunction);
        var partnerAuraId = partnerFunctionArray[1];
        //alert('partnerAuraId = '+ partnerAuraId);
        
        
        var partnerComponent = component.find(partnerAuraId);
        //alert('partnerComponent = '+ partnerComponent);
        var partnerValue = partnerComponent.get("v.value");
        //alert('partnerValue = ' + partnerValue);
        var soldToNumber = component.get("v.soldToAccount"); // component.find('accountId').get("v.value");
        //alert('soldToNumber = '+soldToNumber);

        // var salesOrgComponent = component.find("slsOrgPcklst");
        // var salesOrg = helper.getPricebookName(salesOrgComponent.get("v.value"));
        // console.log('----partnerFunction'+partnerFunction+'--partnerValue'+partnerValue+'--salesOrg'+salesOrg);
        // if(salesOrg){
        //     salesOrg = salesOrg.substring(0, 4);
        //     salesOrgComponent.set("v.errors", null);
        // }else{
        //     salesOrgComponent.set("v.errors", [{message:"Please select sales org."}]);
        //     return;
        // }
        var partnerGlobalId = partnerComponent.getGlobalId();
        //alert('partnerGlobalId = '+partnerGlobalId);
        
        //component.set("v.selectedPartnerFunction", partnerFunction);
        component.set("v.searchText", partnerValue);
        //component.set("v.selectedSalesOrgLabel", salesOrg);
        //console.log('----partnerFunction'+partnerFunction+'--partnerValue'+partnerValue+'--salesOrg'+salesOrg);
        helper.toggleClass(component,true,"partnerPopUp");
    },   

    hidePartnerModal : function(component, event, helper) {
        console.log('----in hideModel');
        //Toggle CSS styles for hiding Modal
        helper.toggleClass(component,false, "partnerPopUp");
    },    

    setSelectedPartner : function(component, event, helper){
        //alert('setSelectedPartner called');
        var partnerNumber = event.getParam("partnerNumber");
        var partnerFunction = event.getParam("partnerFunction");
        console.log('--in setSelectedPartner'+partnerNumber+'--'+partnerFunction);
        //component.set("v.prepareOrderController.soldToNumber", partnerNumber);

        component.find("sitePartnerValue").set("v.value", partnerNumber); 
        helper.updatePartnerDetails(component, partnerNumber);

        helper.toggleClass(component, false, "partnerPopUp");
    },


    onblur : function(component,event,helper){
        // on mouse leave clear the listOfSeachRecords & hide the search result component 
        component.set("v.listOfSearchRecords", null );
        component.set("v.SearchKeyWord", '');
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    onfocus : function(component,event,helper){
        // show the spinner,show child search result component and call helper function
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        component.set("v.listOfSearchRecords", null ); 
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC 
        var getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
    },
    
    keyPressController : function(component, event, helper) {
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        // get the search Input keyword   
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if(getInputkeyWord.length > 0){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        var selectedPillId = event.getSource().get("v.name");
        var AllPillsList = component.get("v.lstSelectedRecords"); 
        
        for(var i = 0; i < AllPillsList.length; i++){
            if(AllPillsList[i].Id == selectedPillId){
                AllPillsList.splice(i, 1);
                component.set("v.lstSelectedRecords", AllPillsList);
            }  
        }
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );      
    },
    
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        component.set("v.SearchKeyWord",null);
        // get the selected object record from the COMPONENT event   
        var listSelectedItems =  component.get("v.lstSelectedRecords");
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        listSelectedItems.push(selectedAccountGetFromEvent);
        component.set("v.lstSelectedRecords" , listSelectedItems); 
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open'); 
    },
    doCancel: function(component, event) {
         
        var soSelected = component.find("sOrderId").get("v.value");
       
        var quoteSelected = component.find("quoteId").get("v.value"); 
        
        var retValue = component.get('v.recordId');
        
        
        
        /*if(soSelected != null && soSelected != undefined) {
        
            var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": soSelected,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
        }else if(quoteSelected != null && quoteSelected != undefined) {
        
            var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": quoteSelected,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
        }else{
            var url = 'https://varian--sfdev.lightning.force.com/lightning/o/Chow_Tool__c/list?filterName=00B0v000001YFyxEAG'
            window.location.href  = url;
        }*/
        
        
        
        var ssoId = component.get("v.renderSID");
        var qqId = component.get("v.renderQID");
        
         if(ssoId != null && ssoId != undefined) {
        
            var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": ssoId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
        }else if(qqId != null && qqId != undefined) {
        
            var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": qqId,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
        }else{
            var url = 'https://varian--sfqa.lightning.force.com/lightning/o/Chow_Tool__c/list?filterName=00Be0000001lx6qEAA&0.source=alohaHeader'
            window.location.href  = url;
        }
        
        
    },
	
    // this function for displaying out product family helptext
    erroFooterSectn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
        var errorFooterBtn = component.find("errorFooterBtn");
        $A.util.addClass(erroFooterSectn, 'slds-hide');
        $A.util.removeClass(erroFooterSectn, 'slds-show');
        
        $A.util.addClass(errorFooterBtn, 'slds-hide');
        $A.util.removeClass(errorFooterBtn, 'slds-show');
    },
    
})
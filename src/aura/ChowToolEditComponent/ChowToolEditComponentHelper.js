({
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },

    /* load Active Team Picklist */
    getActiveTeam : function(component,event) {
        var action = component.get("c.getActiveTeamPicklistOptions");
        var region = component.find("geoRegion").get("v.value");

        action.setParams({
            "geoRegion": region
        });        
        var options = [];
        
        options.push({
            label : '--None--',
            value : ''
        });
        
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                //console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }  
                component.find("ActiveTeam").set("v.options",options);
                /*
                if(options.length > 1){
                    component.find("ActiveTeam").set("v.value",options[1].value); 
                }            
                */                       
            }
        }); 
        $A.enqueueAction(action);
    },

    getTaskAssignee : function(component,event) {
        var action = component.get("c.getTaskAssigneeList");
        var options = [];
        options.push({
            label : '--None--',
            value : ''
        });        
         action.setParams({ "teamName" : component.find('ActiveTeam').get("v.value")});

         component.set("v.selectedTeam", component.find('ActiveTeam').get("v.value"));

         action.setCallback(this,function(response) {            
             var state = response.getState();           
            if (state === "SUCCESS") {    
                var picklistresult = response.getReturnValue();
                //alert('Assignee list retrn = ' + JSON.stringify(picklistresult));
                //console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                        
                component.find("TaskAssignee").set("v.options",options);
                component.set("v.listAssignee", options);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getSoldToAccountInfo : function(component, event) {
        var accId = component.find("accountId").get("v.value");
        //renderSID
        //alert('accId in getAccountFields = ' + accId);
        if (accId != null && accId != '' && accId != undefined) {

            var action = component.get("c.getAccountFieldsInfo");
            action.setParams({
                "accId": accId
            });
            action.setCallback(this, function(response){
                if(response.getState()==='SUCCESS'){
                    //alert('res = ' + JSON.stringify(response.getReturnValue()));
                    //console.log(response.getReturnValue());
                    var acc = response.getReturnValue();
                    //alert('acc.ERP_Site_Partner_Code__c = ' + acc.ERP_Site_Partner_Code__c);
                    component.set("v.soldToAccount", acc.Ext_Cust_Id__c);       //ERP_Site_Partner_Code__c);
                }
            });
            $A.enqueueAction(action);
        }
    },
/*
    getSalesOrderInfo : function(component, event) {
        var soSelected = component.find("sOrderId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        //alert('soSelected = ' + soSelected);
        console.log(' === soSelected === ' + soSelected);
        //console.log(' === soSelected === ' + soSelected);

        var action = component.get("c.getSalesOrderInfoWrap");
        action.setParams({ "soId" : soSelected});
        action.setCallback(this,function(response) {            
            var state = response.getState();
            console.log('state = ' + state + ' == ' + JSON.stringify(response.getReturnValue()));
            
            if (state === "SUCCESS") {
                var soInfoWrap = response.getReturnValue();
                //alert('==soInfoWrap.Sold_To__c=='+soInfoWrap.sOrder.Sold_To__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);
                
                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                  ) {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }
                
                //component.find("sitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                
                component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerName);
                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);
                component.find("quoteId").set("v.value", soInfoWrap.quote.Id);
                // var accountName = component.get("v.accountLookUpRecord"); 
                // accountName = soInfoWrap.sitePartner;
                component.find("geoRegion").set("v.value", soInfoWrap.quote.Quote_Region__c);       //BMI_Region__c);
                
                //alert('city = ' + soInfoWrap.city);
                component.find("city").set("v.value", soInfoWrap.city);
                
                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
            
        });
        $A.enqueueAction(action);
       // this.getSoldToAccountInfo(component,event);
       
    },

*/
    getQuoteInfo : function(component, event) {
        var quoteSelected = component.find("quoteId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        //alert('quoteSelected = ' + quoteSelected);
        
        var action = component.get("c.getQuoteInfoWrap");
         action.setParams({ "quoteId" : quoteSelected});   ////2017-95512-A2
         action.setCallback(this,function(response) {            
             var state = response.getState();    
             //alert('state = ' + state);

            if (state === "SUCCESS") {                
                //alert('res = ' + JSON.stringify(response.getReturnValue()));
                //console.log(response.getReturnValue());
                var soInfoWrap = response.getReturnValue();
                //  alert(component.find("accountId"));
                //  alert('Account = ' + soInfoWrap.sOrder.Sold_To__c);
                //  alert('region = ' + soInfoWrap.quote.BMI_Region__c);
                //  alert('city = ' + soInfoWrap.city);
                //  alert('quote id = ' + soInfoWrap.quote.Id);
                //  alert('partner no = ' + soInfoWrap.sitePartnerNo);
                //  alert('partner id = ' + soInfoWrap.sOrder.ERP_Partner__c);
                //  alert('partner comp = ' + component.find("SitePartnerValue"));


                //component.find("SitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                //component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerNo);
                component.set("v.sitePartnerNameAtr", soInfoWrap.sitePartnerName);

                //component.set("v.chow.Site_partner_End_User__r.Name", soInfoWrap.sOrder.ERP_Partner__r.Name);

                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);

                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                    )
                {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }

                //alert('order field = ' + component.find("sOrderId"));
                component.find("sOrderId").set("v.value", soInfoWrap.sOrder.Id);
                component.find("geoRegion").set("v.value", soInfoWrap.geoRegion);   // quote.Quote_Region__c);       //BMI_Region__c);
                //component.find("city").set("v.value", soInfoWrap.city);
                //alert('quote field = ' + component.find("quoteId"));
                component.find("quoteId").set("v.value", soInfoWrap.quote.Id);
                component.find("city").set("v.value", soInfoWrap.city);

                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);

                this.callPopulateActiveTeam(component, event);

            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },  
/////
    getSalesOrderInfo : function(component, event) {
        var soSelected = component.find("sOrderId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        component.set("v.renderSID", soSelected);
        //alert('soSelected = ' + soSelected);
        
        var action = component.get("c.getSalesOrderInfoWrap");
        action.setParams({ "soId" : soSelected});
        action.setCallback(this,function(response) {            
            var state = response.getState();
            console.log('state = ' + state + ' == ' + JSON.stringify(response.getReturnValue()));
            
            if (state === "SUCCESS") {
                var soInfoWrap = response.getReturnValue();
                //alert('==soInfoWrap.Sold_To__c=='+soInfoWrap.sOrder.Sold_To__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);
                
                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                  ) {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }
                
                //component.find("sitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                
                //component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerName);
                component.set("v.sitePartnerNameAtr", soInfoWrap.sitePartnerName);

                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);

                //alert('soInfoWrap.quote.Id return = ' + soInfoWrap.quote.Id);
                //alert('soInfoWrap.extQtNo return = ' + soInfoWrap.extQtNo);
                
                if(soInfoWrap.quote.Id != null) {
                    component.find("quoteId").set("v.value", soInfoWrap.quote.Id);
                    component.set("v.chow.Quote_Number__c", soInfoWrap.quote.Id);
                } else {
                    component.find("extQuoteId").set("v.value", soInfoWrap.extQtNo);
                    component.set("v.chow.Ext_Quote_Number__c", soInfoWrap.extQtNo);
                }
                // var accountName = component.get("v.accountLookUpRecord"); 
                // accountName = soInfoWrap.sitePartner;
                component.find("geoRegion").set("v.value", soInfoWrap.geoRegion);       // quote.Quote_Region__c);   // BMI_Region__c);
                
                //alert('city = ' + soInfoWrap.city);
                component.find("city").set("v.value", soInfoWrap.city);
                
                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
            
        });
        $A.enqueueAction(action);
       // this.getSoldToAccountInfo(component,event);
       
    },


    clearFields : function(component, event) {
        console.log('in clearFields');
        component.find("geoRegion").set("v.value", null);
        component.find("city").set("v.value", null);
        if(component.find("quoteId") != undefined) {
            component.find("quoteId").set("v.value", null);
            component.set("v.chow.Quote_Number__c", null);
        }
        if(component.find("extQuoteId") != undefined) {
            component.find("extQuoteId").set("v.value", null);
            component.set("v.chow.Ext_Quote_Number__c", null);
        }     
        component.set("v.chow.Site_partner_End_User__c", null);
        component.set("v.partnerId", null);
        component.set("v.sitePartnerNameAtr", null);
        component.find("accountId").set("v.value", null);
        component.set("v.soldToAccount", null);

        component.set("v.chow.ERP_Sold_To__c", null);
        component.set("v.chow.ERP_Site_Partner__c", null);
    },

/*
    getQuoteInfo : function(component, event) {
        var quoteSelected = component.find("quoteId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        //alert('quoteSelected = ' + quoteSelected);
        
        var ssId = component.get("v.renderSID");
        if(ssId == null || ssId == undefined){
             component.set("v.renderQID", quoteSelected);
        }
        
        var action = component.get("c.getQuoteInfoWrap");
         action.setParams({ "quoteId" : quoteSelected});   ////2017-95512-A2
         action.setCallback(this,function(response) {            
             var state = response.getState();    
             //alert('state = ' + state);

            if (state === "SUCCESS") {                
                //alert('res = ' + JSON.stringify(response.getReturnValue()));
                //console.log(response.getReturnValue());
                var soInfoWrap = response.getReturnValue();
                //  alert(component.find("accountId"));
                //  alert('Account = ' + soInfoWrap.sOrder.Sold_To__c);
                //  alert('region = ' + soInfoWrap.quote.BMI_Region__c);
                //  alert('city = ' + soInfoWrap.city);
                //  alert('quote id = ' + soInfoWrap.quote.Id);
                //  alert('partner no = ' + soInfoWrap.sitePartnerNo);
                //  alert('partner id = ' + soInfoWrap.sOrder.ERP_Partner__c);
                //  alert('partner comp = ' + component.find("SitePartnerValue"));


                //component.find("SitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerNo);
                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);

                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                    )
                {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }

                //alert('order field = ' + component.find("sOrderId"));
                component.find("sOrderId").set("v.value", soInfoWrap.sOrder.Id);
                component.find("geoRegion").set("v.value", soInfoWrap.quote.Quote_Region__c);       //BMI_Region__c);
                //component.find("city").set("v.value", soInfoWrap.city);
                //alert('quote field = ' + component.find("quoteId"));
                component.find("quoteId").set("v.value", soInfoWrap.quote.Id);
                component.set("v.chow.Quote_Number__c", soInfoWrap.quote.Id);


                component.find("city").set("v.value", soInfoWrap.city);

                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);

            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },   

    */

    updatePartnerDetails : function(component, partnerNumber){
        var action = component.get("c.getPartnerDetails");
       // console.log('action-----'+action)
        action.setParams({
            "partnerNo" : partnerNumber
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();          
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                console.log('returnValue'+returnValue);
                //var partnerObject = JSON.parse(JSON.stringify(returnValue));
                //component.set(componentName, partnerObject);
                component.set("v.partnerId", returnValue.Id);
                //alert('citys = ' + returnValue.City__c);
                //alert('states = ' + returnValue.State_Province_Code__c);                
                component.set("v.chow.Site_Partner_City__c", returnValue.City__c + ' , ' + returnValue.State_Province_Code__c);
                console.log('setvalues'+component.get("v.chow.Site_Partner_City__c"));
                component.find("city").set("v.value", returnValue.City__c + ' , ' + returnValue.State_Province_Code__c);
                component.find("sitePartnerValue").set("v.value", returnValue.Partner_Number__c);
            }
        });
        $A.enqueueAction(action);
    },

    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
    },


    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchLookUpValues");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName"),
            'ExcludeitemsList' : component.get("v.lstSelectedRecords")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Records Found...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                component.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },

    populateActiveTeam : function(component, event, helper) {
        component.find('ActiveTeam').set("v.value", null);
        component.find("TaskAssignee").set("v.value", null);
        helper.getActiveTeam(component, event);
        helper.getTaskAssignee(component, event);

        var chowRegion = component.get("v.chow.Geo_Region__c");
        //alert('chowRegion = ' + chowRegion);

        if(chowRegion == 'EMEA' || chowRegion == 'emea') {
            var action = component.get("c.getDefaultTeam");
            action.setParams({
                "region": chowRegion
            });

                
             
             
            action.setCallback(this, function(response){
                if(response.getState()==='SUCCESS'){
                    //alert('res = ' + JSON.stringify(response.getReturnValue()));
                    //console.log(response.getReturnValue());
                    var defTeam = response.getReturnValue();
                    //alert('acc.ERP_Site_Partner_Code__c = ' + acc.ERP_Site_Partner_Code__c);
                    component.set("v.defaultChowTeam", defTeam);       //ERP_Site_Partner_Code__c);
                    //alert('defTeam.Default_Active_Team__c = ' + defTeam.Default_Active_Team__c);
                    component.find('ActiveTeam').set("v.value", defTeam.Default_Active_Team__c);
                    helper.getTaskAssignee(component, event);

                    //alert('acive assigne in chowtooledit ctrl = ' + defTeam.Default_Active_Assignee__c);

                    component.find('TaskAssignee').set("v.value", defTeam.Default_Active_Assignee__c);
                    component.set("v.chow.Active_Task_Assignee__c", defTeam.Default_Active_Assignee__c);
                    //document.getElementById("TaskAssigneeId").value = defTeam.Default_Active_Assignee__c;
                }
            });
            $A.enqueueAction(action);            
        }
    },

    callPopulateActiveTeam : function(component, event) {
        component.find('ActiveTeam').set("v.value", null);
        component.find("TaskAssignee").set("v.value", null);
        this.getActiveTeam(component, event);
        this.getTaskAssignee(component, event);

        var chowRegion = component.get("v.chow.Geo_Region__c");
        //alert('chowRegion = ' + chowRegion);

        var geo = component.find("geoRegion").get("v.value");
        //alert('geo = ' + geo);

        if(chowRegion == 'EMEA' || chowRegion == 'emea') {
            var action = component.get("c.getDefaultTeam");
            action.setParams({
                "region": chowRegion
            });

            action.setCallback(this, function(response){
                if(response.getState()==='SUCCESS'){
                    //alert('res = ' + JSON.stringify(response.getReturnValue()));
                    //console.log(response.getReturnValue());
                    var defTeam = response.getReturnValue();
                    //alert('defTeam = ' + defTeam);
                    //alert('acc.ERP_Site_Partner_Code__c = ' + acc.ERP_Site_Partner_Code__c);
                    component.set("v.defaultChowTeam", defTeam);       //ERP_Site_Partner_Code__c);
                    //alert('defTeam.Default_Active_Team__c = ' + defTeam.Default_Active_Team__c);
                    component.find('ActiveTeam').set("v.value", defTeam.Default_Active_Team__c);
                    this.getTaskAssignee(component, event);

                    //alert('acive assigne in chowtooledit ctrl = ' + defTeam.Default_Active_Assignee__c);

                    component.find('TaskAssignee').set("v.value", defTeam.Default_Active_Assignee__c);
                    component.set("v.chow.Active_Task_Assignee__c", defTeam.Default_Active_Assignee__c);
                    //document.getElementById("TaskAssigneeId").value = defTeam.Default_Active_Assignee__c;
                }
            });
            $A.enqueueAction(action);            
        }
    },
})
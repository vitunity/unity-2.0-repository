({
    fetchPickListVal: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },

    /* load Active Team Picklist */
    getActiveTeam : function(component,event) {
        var action = component.get("c.getActiveTeamPicklistOptions");
        var region = component.find("geoRegion").get("v.value");

        action.setParams({
            "geoRegion": region
        });        
        var options = [];
        
        options.push({
            label : '--None--',
            value : ''
        });
        
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                //console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }  
                component.find("ActiveTeam").set("v.options",options);
                /*
                if(options.length > 1){
                    component.find("ActiveTeam").set("v.value",options[1].value); 
                }            
                */                       
            }
        }); 
        $A.enqueueAction(action);
    },

    getTaskAssignee : function(component,event) {
        var action = component.get("c.getTaskAssigneeList");
        var options = [];
        options.push({
            label : '--None--',
            value : ''
        });        
         action.setParams({ "teamName" : component.find('ActiveTeam').get("v.value")});

         component.set("v.selectedTeam", component.find('ActiveTeam').get("v.value"));

         action.setCallback(this,function(response) {            
             var state = response.getState();           
            if (state === "SUCCESS") {    
                var picklistresult = response.getReturnValue();
                //alert('Assignee list retrn = ' + JSON.stringify(picklistresult));
                //console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                        
                component.find("TaskAssignee").set("v.options",options);
                component.set("v.listAssignee", options);
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },

    getSoldToAccountInfo : function(component, event) {
        var accId = component.find("accountId").get("v.value");
        //alert('accId in getAccountFields = ' + accId);
        if (accId != null && accId != '' && accId != undefined) {

            var action = component.get("c.getAccountFieldsInfo");
            action.setParams({
                "accId": accId
            });
            action.setCallback(this, function(response){
                if(response.getState()==='SUCCESS'){
                    //alert('res = ' + JSON.stringify(response.getReturnValue()));
                    //console.log(response.getReturnValue());
                    var acc = response.getReturnValue();
                    //alert('acc.ERP_Site_Partner_Code__c = ' + acc.ERP_Site_Partner_Code__c);
                    component.set("v.soldToAccount", acc.Ext_Cust_Id__c);       //ERP_Site_Partner_Code__c);
                }
            });
            $A.enqueueAction(action);
        }
    },

    getSalesOrderInfo : function(component, event) {
        var soSelected = component.find("sOrderId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        //alert('soSelected = ' + soSelected);
        console.log(' === soSelected === ' + soSelected);
        //console.log(' === soSelected === ' + soSelected);
        
        /*var acc = component.find("accountId").get("v.value");
        alert('==acc=='+acc);
        component.find("accountId").set("v.value", "");
        var acc1 = component.find("accountId").get("v.value");
        alert('==acc1=='+acc1);*/
        
        var action = component.get("c.getSalesOrderInfoWrap");
        action.setParams({ "soId" : soSelected});
        action.setCallback(this,function(response) {            
            var state = response.getState();
            console.log('state = ' + state + ' == ' + JSON.stringify(response.getReturnValue()));
            
            if (state === "SUCCESS") {
                var soInfoWrap = response.getReturnValue();
                //alert('==soInfoWrap.Sold_To__c=='+soInfoWrap.sOrder.Sold_To__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);
                
                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                  ) {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }
                
                //component.find("sitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                
                component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerName);
                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);
                component.find("quoteId").set("v.value", soInfoWrap.quote.Id);
                // var accountName = component.get("v.accountLookUpRecord"); 
                // accountName = soInfoWrap.sitePartner;
                component.find("geoRegion").set("v.value", soInfoWrap.geoRegion);       // quote.Quote_Region__c);       //BMI_Region__c);
                
                //alert('city = ' + soInfoWrap.city);
                component.find("city").set("v.value", soInfoWrap.city);
                
                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);

                /*
                var city = soInfoWrap.city;
                var substr = " , ";
                var result = city.indexOf(substr) > -1;
                console.log(' == result == ' + result);
                if(result) {
                    component.find("city").set("v.value", '');
                } else {
                    component.find("city").set("v.value", soInfoWrap.city);
                }
                */
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
            
        });
        $A.enqueueAction(action);
       // this.getSoldToAccountInfo(component,event);
       
    },

    getQuoteInfo : function(component, event) {
        var quoteSelected = component.find("quoteId").get("v.value"); ////component.get("v.salesOrderLookUpRecord.Name");
        //alert('quoteSelected = ' + quoteSelected);
        
        var action = component.get("c.getQuoteInfoWrap");
         action.setParams({ "quoteId" : quoteSelected});   ////2017-95512-A2
         action.setCallback(this,function(response) {            
             var state = response.getState();    
             //alert('state = ' + state);

            if (state === "SUCCESS") {                
                //alert('res = ' + JSON.stringify(response.getReturnValue()));
                //console.log(response.getReturnValue());
                var soInfoWrap = response.getReturnValue();
                //  alert(component.find("accountId"));
                //  alert('Account = ' + soInfoWrap.sOrder.Sold_To__c);
                //  alert('region = ' + soInfoWrap.quote.BMI_Region__c);
                //  alert('city = ' + soInfoWrap.city);
                //  alert('quote id = ' + soInfoWrap.quote.Id);
                //  alert('partner no = ' + soInfoWrap.sitePartnerNo);
                //  alert('partner id = ' + soInfoWrap.sOrder.ERP_Partner__c);
                //  alert('partner comp = ' + component.find("SitePartnerValue"));


                //component.find("SitePartnerValue").set("v.value", soInfoWrap.sitePartnerNo);
                component.set("v.chow.Site_partner_End_User__c", soInfoWrap.sitePartnerNo);
                component.set("v.partnerId", soInfoWrap.sOrder.ERP_Partner__c);
                component.find("accountId").set("v.value", soInfoWrap.sOrder.Sold_To__c);
                //component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__c);

                if(soInfoWrap.sOrder != null 
                    && soInfoWrap.sOrder != undefined
                    && soInfoWrap.sOrder.Sold_To__c != null 
                    && soInfoWrap.sOrder.Sold_To__c != undefined
                    )
                {
                    component.set("v.soldToAccount", soInfoWrap.sOrder.Sold_To__r.Ext_Cust_Id__c);      //ERP_Site_Partner_Code__c);
                }

                //alert('order field = ' + component.find("sOrderId"));
                component.find("sOrderId").set("v.value", soInfoWrap.sOrder.Id);
                component.find("geoRegion").set("v.value", soInfoWrap.geoRegion);   // quote.Quote_Region__c);       //BMI_Region__c);
                //component.find("city").set("v.value", soInfoWrap.city);
                //alert('quote field = ' + component.find("quoteId"));
                component.find("quoteId").set("v.value", soInfoWrap.quote.Id);

                /*
                var city = soInfoWrap.city;
                var substr = " , ";
                var result = city.indexOf(substr) > -1;
                console.log(' == result == ' + result);
                if(result) {
                    component.find("city").set("v.value", '');
                } else {
                    component.find("city").set("v.value", soInfoWrap.city);
                } 
                */
                component.find("city").set("v.value", soInfoWrap.city);

                component.set("v.chow.ERP_Sold_To__c", soInfoWrap.sOrder.ERP_Sold_To__c);
                component.set("v.chow.ERP_Site_Partner__c", soInfoWrap.sOrder.ERP_Site_Partner__c);
                
                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);

            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },   

    updatePartnerDetails : function(component, partnerNumber){
        var action = component.get("c.getPartnerDetails");
       // console.log('action-----'+action)
        action.setParams({
            "partnerNo" : partnerNumber
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();          
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                console.log('returnValue'+returnValue);
                //var partnerObject = JSON.parse(JSON.stringify(returnValue));
                //component.set(componentName, partnerObject);
                component.set("v.partnerId", returnValue.Id);
                //alert('citys = ' + returnValue.City__c);
                //alert('states = ' + returnValue.State_Province_Code__c);                
                component.set("v.chow.Site_Partner_City__c", returnValue.City__c + ' , ' + returnValue.State_Province_Code__c);
                console.log('setvalues'+component.get("v.chow.Site_Partner_City__c"));
                component.find("city").set("v.value", returnValue.City__c + ' , ' + returnValue.State_Province_Code__c);
                component.find("sitePartnerValue").set("v.value", returnValue.Partner_Number__c);
            }
        });
        $A.enqueueAction(action);
    },

    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
    },


    searchHelper : function(component,event,getInputkeyWord) {
        // call the apex class method 
        var action = component.get("c.fetchLookUpValues");
        // set param to method  
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName"),
            'ExcludeitemsList' : component.get("v.lstSelectedRecords")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Records Found... message on screen.                }
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Records Found...');
                } else {
                    component.set("v.Message", '');
                    // set searchResult list with return value from server.
                }
                component.set("v.listOfSearchRecords", storeResponse); 
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },

    initializeChow : function(component, event) {
        var Id = component.get("v.recordId");
        //alert('Id = ' + Id);
        var action = component.get("c.getchOwrecords");
        // set param to method  
        action.setParams({
            'chOWId': Id
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            //alert('state = ' + state);
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                //alert('response = ' + JSON.stringify(response));
                component.set("v.chow", response);

                //var status = response.Request_Status__c;
                //alert('chow status = ' + status);
                var Id = response.Id;
                //alert('==Id=='+Id);
                if(response.Request_Status__c == 'Duplicate'
                    || response.Request_Status__c == 'Denied'
                    || response.Request_Status__c == 'Withdrawn'
                    || response.Request_Status__c == 'Comepleted-Updated in SAP'
                  ) 
                {
                    alert('The Chow record is not editable.');
                    
                    var chowRecUrl = $A.get("$Label.c.Chow_Record_URL"); //'https://varian--sfdev.lightning.force.com/lightning/r/Chow_Tool__c/';
                    var url = chowRecUrl + Id +'/view';
                    window.location.href  = url;                     
                }

                component.set("v.partnerId", response.Site_partner_End_User__c);
                //component.set("v.chow.Site_partner_End_User__c", response.Site_partner_End_User__r.Name);
                //alert('partner id retrn = ' + response.Site_partner_End_User__c);
                
                //alert('account comp = ' + component.find("accountId"));
                //component.find("accountId").set("v.value", response.Account__r.Name);
                component.find("accountId").set("v.value", response.Account__c);
                component.find("Comments").set("v.value", response.Chronological_Comments_Archive__c);

                //alert('soldto in initializeChow = ' + response.Account__r.Ext_Cust_Id__c);
                component.set("v.soldToAccount", response.Account__r.Ext_Cust_Id__c);

                this.getActiveTeam(component, event);
                this.getTaskAssignee(component, event);

                /*
                var TaskAssigneeValue = response.Active_Task_Assignee__c;
                alert('task Assignee = ' + TaskAssigneeValue);
                document.getElementById('TaskAssigneeId').value = TaskAssigneeValue;
                component.find("TaskAssignee").set("v.value", TaskAssigneeValue);
                */
                //component.set("v.selectedAssignee", TaskAssigneeValue);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);           
    },
})
({
	doInit : function(component, event, helper) {
	    //alert('in action component = ' + component.get('v.recordId'));
		var navEvt = $A.get("e.force:navigateToComponent");
        navEvt.setParams({
            componentDef: "c:ChowLineItems",
            componentAttributes :{"chowToolId":component.get('v.recordId')}
        });
        navEvt.fire();
	}
})
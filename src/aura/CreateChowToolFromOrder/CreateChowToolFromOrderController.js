({
	doInit : function(component, event, helper) {
		var navEvt = $A.get("e.force:navigateToComponent");
        navEvt.setParams({
            componentDef: "c:ChowToolEditComponent",
            componentAttributes :{"masterOrderId":component.get('v.recordId')}
        });
        navEvt.fire();
	}
})
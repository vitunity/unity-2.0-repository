({
	doInit : function(component, event, helper) {
		var navEvt = $A.get("e.force:navigateToComponent");
        navEvt.setParams({
            componentDef: "c:ChowToolEditComponent",
            componentAttributes :{"masterQuoteId":component.get('v.recordId')}
        });
        navEvt.fire();
	}
})
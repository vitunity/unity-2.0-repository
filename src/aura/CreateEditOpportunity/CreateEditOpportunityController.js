/**
 * @author		:		Puneet Mishra
 * @created		:		22 august 2017
 * @description	:		component controller
 */

({
    doInit : function(component, event, helper) {
        /*var urlEvent = $A.get("e.force:navigateToURL");
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then(function(response) {
            workspaceAPI.getFocusedTabInfo().then(function(response) {
                var focusedTabId = response.tabId;
                workspaceAPI.openTab({
                    url: '#/sObject/'+component.get("v.recordId")+'/view',
                    focus: true
                }).then(function(response) {
                    workspaceAPI.openSubtab({
                        parentTabId: response,
                        url: '#/sObject/Opportunity/new?accId='+component.get("v.recordId"),
                        focus: true
                    }).then(function(response){ 
                        var dismissActionPanel = $A.get("e.force:closeQuickAction");
                        dismissActionPanel.fire();
                    });
                });
            });
        });
        
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CreateOpportunityComponent",
            componentAttributes: {
                accountId : component.get("v.recordId")
            }
        });
        evt.fire();
        
        /*
    	evt.fire();
        urlEvent.setParams({
            "url":"#/n/New_Contact"
        });
        urlEvent.fire(); 
        */
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/opportunity?accId="+component.get("v.recordId")
        });
        urlEvent.fire();
       
        }
})
({
    doInit : function(component, event, helper) {
        var createQuoateJs = component.get("c.createQuote");
        var oppId = component.get("v.recordId");
        var workspaceAPI = component.find("workspace");
        //alert('Opportunity Id = '+oppId);
        createQuoateJs.setParams({
            "oppId": oppId
        });
        var urlEvent = $A.get("e.force:navigateToURL");
        createQuoateJs.setCallback(this, function(response) {
            var state = response.getState();
            //alert("In registerData call back function::"+state);
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.resultObj",response.getReturnValue());
                var result = component.get("v.resultObj");
                if(result.status == 'success'){
                    urlEvent.setParams({
                        "url": result.url
                    });
                    urlEvent.fire();
                }
            }else if (response.getState() == "ERROR") {
                console.log(response);
            }
        });
        $A.enqueueAction(createQuoateJs);
    }
})
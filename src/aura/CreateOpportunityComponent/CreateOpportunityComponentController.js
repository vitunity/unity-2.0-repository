/**
 * @author		:		Puneet Mishra
 * @created		:		22 august 2017
 * @description	:		component controller
 */

({
    doInit : function(component, event, helper) {        
        component.set("v.Spinner", true);
        var oppName = component.find("oppname");
        $A.util.removeClass(oppName, 'input');
        
        var getOpp = component.get('c.fetchOpportunity');
        var accountId = component.get("v.accountId");
        getOpp.setParams({
            "opportunityId":component.get("v.recordId"),
            "accId":component.get("v.accountId")
        });
        getOpp.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' #### state #### ' + state);
            if (component.isValid() && state === "SUCCESS") {
                var obj = response.getReturnValue();
                console.log('------obj.opportunity'+JSON.stringify(obj));
                component.set("v.opportunity", obj.opportunity);
                if(obj.opportunity.AccountId ==  null){
                    obj.opportunity.AccountId = accountId;
                }
                var oppType = component.get('v.opportunity.Type');
                if(oppType == 'Replacement') {
                    helper.applyCSS(component, event);
                    component.find("replacedQuoteLoader").reloadRecord();
                } else {
                    helper.removeCSS(component, event);
                }
                
                var replacement = component.get('v.opportunity.Existing_Equipment_Replacement__c');
                console.log(' ==== replacement ==== ' + replacement);
                if(replacement == 'Yes') {
                    helper.existingProductCSS(component, event);
                } else {
                    helper.removeExistingProductCSS(component, event);
                }
                
                component.set("v.opportunityController", obj);
                // 22 Feb 2018, not required helper.primaryWonReasonChange(component, true);
                // 22 Feb 2018, not required helper.secondaryWonReasonChange(component, true);
                //helper.primaryLossReasonChange(component, true);
                //helper.secondaryLossReasonChange(component, true);
                // 22 Feb 2018, not required helper.lostSoftwareChange(component, true);
                console.log(' === 2:Execute == ');
                //Added by Shital
                component.set("v.CompetitorDetails",obj.nonInstalledVarianProducts);
                //Added by PUNEET
                component.set("v.productNames", obj.nonInstalledVarianProducts.productNameListTEST);
                component.set("v.RecordTypes",obj.nonVarianProductRecordTypes);
                console.log(' === 3:Execute == ');
                //component.set("v.Spinner", false);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(getOpp);
    },
    
    // function called on Primary Won Reason Change
    onPrimaryWonReasonChange: function(component, event, helper) {
        helper.primaryWonReasonChange(component, false);
    },
    // function called on Secondary Won Reason change
    onSecondaryWonReasonChange: function(component, event, helper) {
        helper.secondaryWonReasonChange(component, false);
    },
    // function called on Primary Loss Reason change
    onPrimaryLossReasonChange: function(component, event, helper) {
        helper.primaryLossReasonChange(component, false);
    },
    // function called on Secondary Loss Reason change
    onSecondaryLossReasonChange: function(component, event, helper) {
        helper.secondaryLossReasonChange(component, false);
    },
    // function automatically call by aura:waiting event
    showSpinner: function(component, event, helper) {
        // make spinner attribute true for display loading spinner
        component.set("v.Spinner", true);
    },
    
    // function called on Loss software change
    onLostSoftwareChange: function(component, event, helper) {
        helper.lostSoftwareChange(component, false);
    },
    
    // function automatically call by aura:doneWaiting event
    hideSpinner: function(component, event, helper) {
        // make spinner attribute true for display loading spinner
        component.set("v.Spinner", false);
    },
    
    // function called on click of Cancel and close the popup
    cancelDialog: function(component, event, helper) {
        var recId = component.get("v.recordId");
        var accountId = component.get("v.accountId");
        if( accountId != '' && accountId != null ) {
            if(typeof sforce !== 'undefined') {
                sforce.one.navigateToSObject(accountId,"DETAIL");  
            }
        }else if( recId != '' && recId != null ) {
            if(typeof sforce !== 'undefined') {
                sforce.one.navigateToSObject(recId,"DETAIL");  
            }
            //sforce.one.createRecord('Contact',null,{'retURL':'/006','nooverride':'1'});
            //sforce.one.navigateToSObject('/'+recId);
            /*global sforce:true*/
            //if(typeof sforce !== 'undefined') {
            //sforce.one.navigateToSObject(recId,"DETAIL"); 
            //}
        }else{
            if(typeof sforce !== 'undefined') {
                sforce.one.navigateToURL('/006'); 
            }                
        }
        /*
        var workspaceAPI = component.find("workspace");
        var isConsole = false;
        workspaceAPI.isConsoleNavigation().then(function(response) {
        	alert('Test');
            workspaceAPI.getFocusedTabInfo().then(function(response) {
                var focusedTabId = response.tabId;
                alert(focusedTabId);
                workspaceAPI.closeTab({tabId: focusedTabId});
            })
            .catch(function(error) {
                console.log(error);
            });
        });
        */
        
    },
    
    hideModal : function(component, event, helper) {
        helper.toggleModalClass(component, false);
    },
    handlePrimaryReasonChange: function(component, event, helper) {
        // Get a reference to the dependent picklist
        //var selectContact = component.find("primarylostdetail");
        
        // Call the helper function to refresh the
        // dependent picklist, based on the new controlling value
        //component.set("v.opportunityController.primaryLostDetail",
        //selectContact.optionsByControllingValue[event.target.value]);
        
        var parentValue = component.find('primarylossreason').get('v.value');
        console.log(' ParentVal => ' + parentValue);
        //console.log(' ParentVal 2 => ' + component.get('v.primarylostdetail')[parentValue]);
        console.log(' ParentVal => ' + parentValue);
        //component.set('v.opportunityController.primaryLostDetail', component.get('v.primarylostdetail')[parentValue]);
        
        //if(parentValue != '')
        //  component.set('v.disabledPick',false);
        //else
        //  component.set('v.disabledPick',true);
        
    }, 
    //hide show on value change of Type picklist
    toggleQuote: function (component, event, helper) {
        var typeValue = component.find('accountType').get('v.value');
        //obj.accountType = typeValue;
        var changeElement = component.find("divID");
        if(typeValue == 'Replacement') {
            //$A.util.toggleClass(changeElement, "slds-hide");
            //component.set('v.opportunityController', obj);
            helper.applyCSS(component, event);
        } else {
            //$A.util.toggleClass(changeElement, "slds-hide");
            helper.removeCSS(component, event);
        }
    }, 
    
    handleMyComponentEvent : function(component, event, helper) {
        console.log(' EVENT FIRED ');
    },
    
    onCheckBoxChange:function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var selectedHeaderCheckField = event.getSource().get("v.name");
        component.set(selectedHeaderCheckField,selectedHeaderCheck);
        //alert(selectedHeaderCheckField);
    },
    onOrderInHouseChange:function(component, event, helper) {
    	/*var getOpp = component.get("c.getCheckboxValue");
        var accountId = component.get("v.accountId");
        getOpp.setParams({
            "opportunityId":component.get("v.recordId"),
           
        });
        
        getOpp.setCallback(this, function(response) {
            var state = response.getReturnValue();
            
            if (state === true) { 
            	component.set("v.showInHouseOrder",true);	     
            } 
        });
        $A.enqueueAction(action);*/
        //component.set("v.showInHouseOrder",true);	 
    },
    
    
    saveRecord : function(component, event, helper) {
        
        if(!helper.requiredfieldValidation(component, event)){
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            return;
        }
        
        var opport1 = component.get("v.opportunity ");
        var recId = component.get("v.recordId");
        helper.deleteCustomLookupReferences(opport1);
        component.set("v.Spinner", true);
        var getOpp = component.get('c.saveOpprotunity');
        console.log(' == after delete call ==');
        var existArray1 = []; //Added by Shital
        existArray1 = component.get("v.CompetitorDetails");
        console.log(opport1.Booking_Value__c + ' == ' + opport1.Net_Booking_Value__c + ' = JSON.stringify(opport1) => ' + JSON.stringify(opport1));
        getOpp.setParams({
            "opport":opport1,//opport.opportunity
            "nonInstalledVarianProducts":JSON.stringify(existArray1)
        });
        getOpp.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' === state === ' + state);
            if(state == "SUCCESS" && component.isValid()) {
                console.log(' === inside state === ' + response.getReturnValue());
                //sforce.one.navigateToSObject(response.getReturnValue(),"refresh");
                var recordId = response.getReturnValue();
                
                if(typeof sforce !== 'undefined') {
                    sforce.one.back(true);
                    
                }
            } else if( component.isValid() && state == "ERROR") {
                //STRY0059057: INC4796235 : SFA-00096043, TriStar Centennial Medical Center - Cannot save opportunity
                var oppValidation = 'You cannot create or update an opportunity that is pending removal, or is flagged as a Strategic Account';
                var valdationerr = response.getError()[0].message;
                
                console.log("Exception caught successfully");
                console.log("Error object", response);
                console.log("Error Message 1", response.getError()[0]);
                console.log("Error Message 2", response.getError()[0].message);
                console.log("Error Message 3", response.getState());
                console.log("Error object", JSON.stringify(response));
                
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                //INC4796235 : SFA-00096043, TriStar Centennial Medical Center - Cannot save opportunity
                if(valdationerr.includes(oppValidation)) {
                    component.set("v.errors", oppValidation);
                } else {
                	component.set("v.errors", response.getError()[0].message);
                }
                //helper.toggleModalClass(component, true, response.getError()[0].message);
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(getOpp);
    },
    
    // this function for displaying product family helptext
    display : function(component, event, helper) 
    {
        var toggleText = component.find("tooltip");
        $A.util.addClass(toggleText, 'slds-show');
        $A.util.removeClass(toggleText, 'slds-hide');
        
    },
    
    // this function for displaying out product family helptext
    displayOut : function(component, event, helper) 
    {
        var toggleText = component.find("tooltip");
        $A.util.addClass(toggleText, 'slds-hide');
        $A.util.removeClass(toggleText, 'slds-show');
    },
    
    // this function for displaying out product family helptext
    erroFooterSectn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
        $A.util.addClass(erroFooterSectn, 'slds-hide');
        $A.util.removeClass(erroFooterSectn, 'slds-show');
    },
    
    // this function for displaying out product family helptext
    erroFooterBtn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
        $A.util.addClass(erroFooterSectn, 'slds-show');
        $A.util.removeClass(erroFooterSectn, 'slds-hide');
    },
    
    //Opens custom lookup component popup dynamically based of object type
    showCustomLookupModal: function(component, event, helper) {
        //Toggle CSS styles for opening Modal
        
        var recordLoopupKey = event.getSource().get("v.name");
        var recordLookupArray = recordLoopupKey.split("-");
        var recordIdField = recordLookupArray[0];
        var recordNameField = recordLookupArray[1];
        var sObjectLabel = recordLookupArray[2];
        var sObjectName = recordLookupArray[3];
        
        var recordNameComponent = component.find(recordNameField);
        var recordNameValue = recordNameComponent.get("v.value");
        
        console.log('----recordNameValue'+recordNameValue+'--recordIdField'+recordIdField+
                    '--recordNameField'+recordNameField+'---sObjectName'+sObjectName);
        var objectFieldLabels = [];
        var objectFieldNames = [];
        if(sObjectName == "Account"){
            console.log('----Account--'+sObjectName);
            objectFieldLabels = ['Name', 'Account Type', 'Street', 'City', 'State', 'Zip/Postal Code', 'Country', 
                                 'ERP Customer Id', 'Account Record Type'];
            
            objectFieldNames = ['Name', 'Account_Type__c', 'BillingStreet', 'BillingCity', 'BillingState', 'BillingPostalCode',
                                'BillingCountry', 'Ext_Cust_Id__c', 'RecordType.Name'];
            component.set("v.defaultFieldName", "");
            component.set("v.defaultFieldValue", "");
        }else if(sObjectName == "Contact"){
            console.log('----Contact--'+sObjectName);
            objectFieldLabels = ['Name', 'Account Name', 'Phone', 'Email', 'City', 'State', 'Postal Code', 'Country'];
            objectFieldNames = ['Name', 'Account.Name', 'Phone', 'Email', 'MailingCity', 'MailingState', 'MailingPostalCode', 
                                'MailingCountry'];
            component.set("v.defaultFieldName", "AccountId");
            component.set("v.defaultFieldValue", component.get("v.opportunity.AccountId"));
        }else if(sObjectName == "BigMachines__Quote__c"){
            objectFieldLabels = ['Name', 'Opportunity', 'Description', 'Account','Is Primary?'];
            objectFieldNames = ['Name', 'BigMachines__Opportunity__r.Name', 'BigMachines__Description__c', 'BigMachines__Account__r.Name',
                               'BigMachines__Is_Primary__c'];
            
            component.set("v.defaultFieldName", "");
            component.set("v.defaultFieldValue", "");
            
        }
        console.log('----objectFieldLabels'+objectFieldLabels+'-----objectFieldNames'+objectFieldNames);
        component.set("v.fieldLabels", objectFieldLabels);
        component.set("v.fieldNames", objectFieldNames);
        component.set("v.selectedLookupIdField", recordIdField);
        component.set("v.selectedLookupNameField", recordNameField);
        component.set("v.selectedLookupSearchKey", recordNameValue);
        component.set("v.objectLabelValue", sObjectLabel);
        component.set("v.objectAPIName", sObjectName);
        
        helper.toggleClass(component,true,"customLookupPopUp");
    },
    
    hideCustomLookupModal : function(component, event, helper) {
        console.log('----in hideModel');
        //Toggle CSS styles for hiding Modal
        helper.toggleClass(component,false, "customLookupPopUp");
    },
    
    setSelectedLookupRecord : function(component, event, helper){
        var sfId = event.getParam("recordId");
        var sfName = event.getParam("recordName");
        var nameField = event.getParam("recordNameField");
        var idField = event.getParam("recordIdField");
        helper.toggleClass(component, false, "customLookupPopUp");
        component.find(nameField).set("v.value",sfName);
        component.find(idField).set("v.value",sfId);
        console.log('-----sfId'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        component.set("v.selectedLookupSearchKey", "");
        
        if(nameField == "oppQuoteReplacedName"){
            component.find("replacedQuoteLoader").reloadRecord();
        }
    },
    
    handleAccountChange : function(component, event, helper){
        var instance =  component.get("v.opportunityController");
        if(instance ==  null){
            return;
        }
        var accountId = component.get("v.opportunity.AccountId");
        if(accountId){
            console.log('------handleAccountChange'+accountId);
            helper.getSFAccountDetails(component, accountId, "OppAccount");
        }    
    },
    
    handleSitePartnerChange : function(component, event, helper){
        var sitePartnerId = component.get("v.opportunity.Site_Partner__c");
        if(sitePartnerId){
            console.log('------handleSitePartnerChange'+sitePartnerId);
            helper.getSFAccountDetails(component, sitePartnerId, "SitePartner");
        }
    },
    
	/** Custom Replacement Lookup Modal Logic **/
    showCustomLookupModalForReplacement: function(component, event, helper) {
        //oppAccountId-oppAccountName-HW-SVMXC__Installed_Product__c-IP-linacsystakeout, oppAccountId-oppAccountName-HW-SVMXC__Installed_Product__c
        
        var recordLoopupKey = event.getSource().get("v.name");
        var recordLookupArray = recordLoopupKey.split("-");
        var recordIdField = recordLookupArray[0];
        var recordNameField = recordLookupArray[1];
        var productFamily = recordLookupArray[2];
        var hiddenId = recordLookupArray[6];//INC4873200:INC4904281
        console.log('==== event.getSource().get("v.name") === ' + hiddenId);//INC4873200:INC4904281
        component.set("v.hiddenId", hiddenId);//INC4873200:INC4904281
        component.set("v.productFamily", productFamily);
        component.set("v.recId", recordIdField);
        var elementId = recordLookupArray[5];
        var sObjectName;
        var sObjectLabel;
        var VALEU;
        
        if(elementId == 'linacsystakeout') {
            VALEU = component.find("linacsystakeout").get("v.value");
            console.log(' == VALEU == ' + VALEU);
            if(typeof(VALEU) == 'undefined' || VALEU == 'None'|| VALEU == '') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }
            if(VALEU == 'Varian') {
                sObjectLabel = 'Install Products';
                sObjectName = 'SVMXC__Installed_Product__c';
            } else {
                sObjectLabel = 'Competitors';
                sObjectName = 'Competitor__c';
            }
        } else if(elementId == 'oissysTakeout') {
            VALEU = component.find("oissysTakeout").get("v.value");
            if(typeof(VALEU) == 'undefined' || VALEU == 'None'|| VALEU == '') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }
            if(VALEU == 'Varian') {
                sObjectLabel = 'Install Products';
                sObjectName = 'SVMXC__Installed_Product__c';
            } else {
                sObjectLabel = 'Competitors';
                sObjectName = 'Competitor__c';
            }
        } else if(elementId == 'serviceTakeout') {
            VALEU = component.find("serviceTakeout").get("v.value");
            
            if(typeof(VALEU) == 'undefined' || VALEU == 'None'|| VALEU == '') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }
            sObjectLabel = 'Service Takeout';
            sObjectName = 'Competitor__c';
        }
        console.log(' === sObjectLabel === ' + sObjectLabel);
        console.log(' === String(VALEU) === ' + String(VALEU));
        console.log(' === component.get("v.opportunity.AccountId") === ' + component.get("v.opportunity.AccountId"));
        console.log(' === sObjectName === ' + sObjectName);
        
        var oppAccountId = '';
        var sitePartnerAcc = component.get("v.opportunity.Site_Partner__c");
        var accountId = component.get("v.opportunity.AccountId")
        if(sitePartnerAcc != undefined){
            oppAccountId = sitePartnerAcc;
        }
        else if(accountId != ''){
            oppAccountId = accountId;
        }
          
        component.set("v.sctakeout", String(VALEU));
        component.set("v.oppAccId", oppAccountId);
        component.set("v.sObjectLabel", sObjectLabel);
        component.set("v.sObjectAPIName", sObjectName);
        helper.toggleClass(component,true,"customProdReplacementLookupPopUp");
    },
    
    // Hide custom replacement Modal
    hideCustomReplacementModalModal : function(component, event, helper) {
        //Toggle CSS styles for hiding Modal
        helper.toggleClass(component,false, "customProdReplacementLookupPopUp");
        component.set("v.sctakeout", '');
        component.set("v.oppAccId", '');
        component.set("v.sObjectLabel", '');
        component.set("v.sObjectAPIName", '');
    },
    
    // hide/show existing product replacement section
    toggleExistingProdReplacement : function(component, event, helper) {
        var replacement = component.find('existingproductreplacement').get('v.value');
        if(replacement == 'Yes') {
            helper.existingProductCSS(component, event);
        } else {
            helper.removeExistingProductCSS(component, event);
        }
    },
    
    //Replacement Logic
    setSelectedReplacementRecord : function(component, event, helper) {
        var values = event.getParam("prodNames");
        var recId = event.getParam("recordId");
        
        var hiddenVal = event.getParam("hiddenRecVal");
        var hiddenId = event.getParam("hiddenRecId");
        
        var final;
        //if(component.find(recId).get("v.value") != '') {
        console.log(' 9999999 ' + typeof(component.find(recId).get("v.value")));
        if(typeof(component.find(recId).get("v.value")) != 'undefined'){
            final = component.find(recId).get("v.value") + ',' + values;
        } else {
            final = values;
        }
        /*component.find(recId).set("v.value",values);*/
        component.find(recId).set("v.value",final);
        //component.set("sObjectAPIName", '');
        //INC4873200:INC4904281
        var finalVendor;
        //if(component.find(recId).get("v.value") != '') {
        console.log(' 9999999 ' + typeof(component.find(hiddenId).get("v.value")));
        if(typeof(component.find(hiddenId).get("v.value")) != 'undefined'){
            finalVendor = component.find(hiddenId).get("v.value") + ',' + hiddenVal;
        } else {
            finalVendor = hiddenVal;
        }
        
        component.find(hiddenId).set("v.value",finalVendor.toString());
        helper.toggleClass(component, false, "customProdReplacementLookupPopUp");
    },
    
    // Adds Competitor record in List
    add : function(component, event, helper) {      
        console.log(' selected method - add** '+component.get("v.selectedRecordType"));
        var action12 = component.get('c.addRemoveInstalledProduct'); 
        var existingLength =[];
        existingLength = component.get("v.CompetitorDetails");
        console.log(' == existing length == ' + existingLength.length);
        action12.setParams({
            "selectedRecordType":component.get("v.selectedRecordType"),
            "addRemoveFlag":'add',
            "counter1": existingLength.length
        });
        action12.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' ==== state ==== ' + state);
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                var count = component.get("v.counter");
                var recType = component.get("v.selectedRecordType");
                console.log(' ==== res ==== ' + JSON.stringify(res) );
				component.set("v.productNames", res.productNameListTEST);                
                //console.log(' ==== res ==== ' + JSON.stringify(res.productNameListTEST) );
                var exist = [];
                exist = component.get("v.CompetitorDetails");
                console.log(' ==== res ==== ' + exist.nonVarianProduct + ' == ' + JSON.stringify(exist));
                var count = exist.length;//component.get("v.counter");
                console.log(' == count == ' + count);
                /*if(count == 0) {
                    exist[0] = res;
                } else {
                    exist.push(res);
                }*/
                exist.push(res);
                component.set("v.CompetitorDetails", exist);
                
                //var count = component.get("v.counter");
                component.set("v.counter", count+1);
                
            } else {
                console.log('  ERROR ');
            }
        });
        $A.enqueueAction(action12);
    }, 
    
    // Removes Competitor record in List
    remove : function(component, event, helper) {
        
        var rowNumberRemove = event.getSource().get("v.name");// take rowID   
        var existArray = []; 
        existArray = component.get("v.CompetitorDetails");
        
        var removeRow = component.get('c.removeInstalledProduct'); 
        removeRow.setParams({
            "rowNumber": rowNumberRemove,     
            "existingList":JSON.stringify(existArray)
        });        
        removeRow.setCallback(this, function(response) {
            var state = response.getState(); 
            if(state == "SUCCESS") {
                var res = response.getReturnValue();
                console.log(' === JSON === ' + JSON.stringify(res) + ' == ' + res.length);
                var comp = [];
                if(res.length == 0) {
                    console.log(' inside if ');
                	component.set("v.CompetitorDetails", comp);
                    
                } else {
                    component.set("v.CompetitorDetails", res);
                }
                component.set("v.CompetitorDetails", res);
                
                var existingLength =[];
                existingLength = component.get("v.CompetitorDetails");
                console.log(' == existing length == ' + existingLength.length+"-----"+component.get("v.CompetitorDetails").length);
                
                var count = component.get("v.counter");
                console.log(' === copounter === ' + count);
                if(count>0){
                    component.set("v.counter", count-1);
                }
            } else {
                console.log('  ERROR ');
            }
        });
        $A.enqueueAction(removeRow);
    },
    
    // Model__c picklist fetched when Vendor changes, 
    onVendorChange: function(component, event, helper) {
        //Added on 2 FEB START
        console.log(' == VENDOR CHANGE');
        var compititorList = component.get("v.CompetitorDetails");
        var selectedRowNumber = event.getSource().get("v.name");// take rowID 
        var obj = component.get('v.opportunityController');
        
        var recType = component.get("v.selectedRecordType");
        console.log(' selected rec type ' + recType);
        for(var counter in compititorList){
            if(counter!='remove'){
                if(compititorList[counter].rowNumber==selectedRowNumber){
                    console.log(' NOT Empty ' + JSON.stringify(component.get('v.productNames')));
                    var productNames = compititorList[counter].productNameListTEST;//INC4873200:INC4904281:component.get('v.productNames');
                    if(!($A.util.isEmpty(compititorList[counter].nonVarianProduct.Vendor__c))){
                        console.log('Inside otherrrrr' + compititorList[counter].nonVarianProduct.Vendor__c);
                        var vendorKey = compititorList[counter].nonVarianProduct.Vendor__c;
                        console.log(' === recType === ' + recType);
                        if(recType != 'Image Management Systems') {
                        	//compititorList[counter].vendorDependentProductNameList = obj.productNameList[vendorKey];
                        	compititorList[counter].vendorDependentProductNameList = productNames[vendorKey];
                            console.log(' == vendor List == ' + JSON.stringify(compititorList[counter].vendorDependentProductNameList));
                        } else {
                            compititorList[counter].vendorDependentProductNameList = obj.vendorProductNameList[vendorKey];
                        }
                    } else { // if vendor selected value is empty is empty
                        compititorList[counter].vendorDependentProductNameList = productNames[vendorKey];
                    }
                }
            }
        }
        console.log(' === compititorList === ' + JSON.stringify(compititorList) + JSON.stringify(obj.productNameList) );
        component.set("v.CompetitorDetails", compititorList);
        //END
    },
    
    // 6 march 2018
    vendorSelected : function(component, event, helper) {
        var values = event.getParam("vendorName");
        var recId = event.getParam("recId");
        console.log(' == values == ' + values );
        console.log(' == recId == ' + recId );
        component.find(recId).set("v.value",values);
    },
    onOppCategoryChange : function(component, event, helper) {
        
        var selectedVals = new Array();
		selectedVals.push(event.getSource().get("v.value"));
    	console.log(' ==selectedVals== ' + selectedVals);
        //console.log(' === ' + component.get('v.opportunity.Opportunity_Category__c')) ;
        //console.log(' === ' + JSON.stringify(component.get('v.opportunity.Opportunity_Category__c')) );
    },
    
    handleQuoteReplacedUpdated: function(component, event, helper) {
        console.log("------handleQuoteReplacedUpdated--"+component.get("v.opportunity.Quote_Replaced__c"));
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
            var quoteReplacedObject = component.get("v.quoteReplacedRecord");
            console.log("-----quote replaced JSON"+JSON.stringify(quoteReplacedObject));
            component.find("replacedQuoteAmount").set("v.value", quoteReplacedObject.fields.BigMachines__Amount__c.displayValue);
        } else if(eventParams.changeType === "ERROR") {
            console.log("- ERROR WHILE LOADING QUOTE REPLACED RECORD--");
        }
    },
    clearSelection: function(component, event, helper) {
        var replacementName = event.getSource().get("v.name");
        console.log(' == replacementName == ' + replacementName);
        if(replacementName == 'VarianPCSNNonVarianIB') {
            component.find("VarianPCSNNonVarianIB").set("v.value",'');
            component.set("v.opportunity.Varian_PCSN_Non_Varian_IB__c", '');
            
            component.find("pcsnVendor").set("v.value",'');
            component.set("v.opportunity.Varian_PCSN_Non_Varian_IBwVendor__c", '');
            
        } else if(replacementName == 'VarianSWSerialNoNonVarianIB') {
            component.find("VarianSWSerialNoNonVarianIB").set("v.value",'');
            component.set("v.opportunity.Varian_SW_Serial_No_Non_Varian_IB__c", '');
            
            component.find("swVendor").set("v.value",'');
            component.set("v.opportunity.Varian_SW_Serial_No_Non_Varian_IBwVendor__c", '');
            
            
        } else if(replecementName == 'VarianSCSerialNoNonVarianIB') {
            component.find("VarianSCSerialNoNonVarianIB").set("v.value",'');
            component.set("v.opportunity.Varian_SC_Serial_No_Non_Varian_IB__c", '');
            
            
            component.find("scVendor").set("v.value",'');
            component.set("v.opportunity.Varian_SC_Serial_No_Non_Varian_IBwVendor__c", '');
            
        }
    }
})
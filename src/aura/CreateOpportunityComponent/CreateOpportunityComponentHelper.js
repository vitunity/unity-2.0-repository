/**
 * @author		:		Puneet Mishra
 * @created		:		22 august 2017
 * @description	:		component helper
 */
({
    // navigate user to record detail page on click of Cancel
    navigateTo: function(component, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":recId 
        });
        navEvt.fire();
    },
    
    //Applies Style class when Account Type is Replacement
    applyCSS: function(component, event) {
        console.log('APPLY');
        var cmpTarget = component.find('divID');
        $A.util.addClass(cmpTarget, 'divCSS');
        $A.util.removeClass(cmpTarget, 'hideDiv');
    },
    
    // Existing Replacement Product section Add Styling
    existingProductCSS : function(component, event) {
        var cmpTarget = component.find('existingProdId');
        var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.addClass(cmpTarget, 'divCSS');
        $A.util.addClass(cmpHeaderTarget, 'divCSS');
        $A.util.removeClass(cmpTarget, 'hideDiv');
        $A.util.removeClass(cmpHeaderTarget, 'hideDiv');
    },
    
    // Existing Replacement Product section Remove Styling
    removeExistingProductCSS: function(component, event) {
        console.log('REMOVE');
        var cmpTarget = component.find('existingProdId');
        var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.removeClass(cmpTarget, 'divCSS');
        $A.util.removeClass(cmpHeaderTarget, 'divCSS');
        $A.util.addClass(cmpTarget, 'hideDiv');
        $A.util.addClass(cmpHeaderTarget, 'hideDiv');
    },
    
    toggleModalClass: function(component,showModal, message) {
        console.log('---in toggle class');
        var modal = component.find("modalDiv");
        var parent = component.find("parentDiv");
        component.find("messages").set("v.value", message);
        console.log(' - parentDIV - ' + parent + ' - modalDiv - ' + modal);
        if(showModal){
            $A.util.removeClass(modal,'LockOff');
            $A.util.addClass(modal,'LockOn');
        } else {
            $A.util.removeClass(modal,'LockOn');
            $A.util.addClass(modal,'LockOff');
            component.find("messages").set("v.value", '');
        }
        
    },
    
    //Removes Style class when Account Type is other than Replacement
    removeCSS: function(component, event) {
        console.log('REMOVE');
        var cmpTarget = component.find('divID');
        $A.util.removeClass(cmpTarget, 'divCSS');
        $A.util.addClass(cmpTarget, 'hideDiv');
    },
    
    // Method verify if the Required fields are populated with alues if not then give a appropriate error message
    requiredfieldValidation: function(component, event) {
        var allerrors = '';
        //Opportunity Name Validation
        var oppName = component.find("oppname");
        var oppNameValue = oppName.get("v.value");
        if($A.util.isEmpty(oppNameValue)){
            $A.util.addClass(oppName, 'slds-has-error');
            allerrors+="Opportunity name cannot be blank";
        } else {
            $A.util.removeClass(oppName, 'slds-has-error');
            oppName.set("v.errors", [{message:""}]);
        }
        
        //Acount sector validation
        var accountSector = component.find("accountSector");
        var accountSectorValue = accountSector.get("v.value");
        if($A.util.isEmpty(accountSectorValue)){
            $A.util.addClass(accountSector, 'slds-has-error');
            //accountSector.set("v.errors", [{message:"Account sector cannot be blank"}]);
            allerrors+="<br/>Account sector cannot be blank.";
        } else {
            $A.util.removeClass(accountSector, 'slds-has-error');
            accountSector.set("v.errors", [{message:""}]);
        }
        
        //Payer Same as Customer
        var paySCust = component.find("paySCust");
        var paySCustValue = paySCust.get("v.value");
        if($A.util.isEmpty(paySCustValue)){
            $A.util.addClass(paySCust, 'slds-has-error');
            //paySCust.set("v.errors", [{message:"Payer Same as Customer cannot be blank"}]);
            allerrors+="<br/>Payer Same as Customer cannot be blank.";
        } else {
            $A.util.removeClass(paySCust, 'slds-has-error');
            paySCust.set("v.errors", [{message:""}]);
        }
        
		 //Competior details section validation START- Added by Shital
		// Vendor validation
		
		var count = component.get("v.counter");
        console.log('***count**'+count);
        if(count > 0){
            console.log('***count inside if**'+count);
            //var vendor = component.find("Vendor");    
            var compititorList = component.get("v.CompetitorDetails");
            console.log("-----compititorList--BEFORE"+JSON.stringify(compititorList)+'----'+compititorList.length);
            for(var counter in compititorList){
            	console.log('---out count IN== ' + counter);
                if (counter != 'remove') {
                	if ($A.util.isEmpty(compititorList[counter].nonVarianProduct.Vendor__c)) {
                        compititorList[counter].hasErrorVendor = true;
                        allerrors += "<br/>Vendor cannot be blank.";
                    } else {
                        compititorList[counter].hasErrorVendor = false;
                        //vendor.set("v.errors", [{message:""}]);
                        console.log("----in else");
                    }
                    
                    if(compititorList[counter].nonVarianProduct.RecordType.Description != 'Image Management Systems' &&
                       compititorList[counter].nonVarianProduct.RecordType.Description != '3rd Party Service Contracts'
                      ) {
                        if ($A.util.isEmpty(compititorList[counter].nonVarianProduct.Model__c)) {
                            console.log('----in if Model >' + compititorList[counter].nonVarianProduct.Model__c);
                            compititorList[counter].hasErrorModel = true;
                            allerrors += "<br/>Product Name cannot be blank.";
                        } else {
                            compititorList[counter].hasErrorModel = false;
                            //vendor.set("v.errors", [{message:""}]);
                            console.log("----in else");
                        }
                    }
                    
                    var recordType = compititorList[counter].nonVarianProduct.RecordType.Description;
                    if ($A.util.isEmpty(compititorList[counter].nonVarianProduct.Isotope_Source__c)
                       			&& recordType=="Brachytherapy Afterloaders" 
                       ) {
                        compititorList[counter].hasErrorIsotopeSource = true;
                        allerrors += "<br/>Isotope Source cannot be blank.";
                    }else {
                        compititorList[counter].hasErrorIsotopeSource = false;
                        //vendor.set("v.errors", [{message:""}]);
                        console.log("----in else");
                    }
                }
            }
            
            console.log('----compititorList--AFTER'+JSON.stringify(compititorList));
            component.set("v.CompetitorDetails", compititorList);
        }
        //Opportunity Currency Validation
        var oppCurrency = component.find("oppCurrency");
        var oppCurrencyValue = component.get('v.opportunity.CurrencyIsoCode');
        console.log(' $A.util.isEmpty(oppCurrencyValue) ' + $A.util.isEmpty(oppCurrencyValue));
        if($A.util.isEmpty(oppCurrencyValue)){
            $A.util.addClass(oppCurrency, 'slds-has-error');
            allerrors+="<br/>Please select opportunity currency.";
        } else {
            $A.util.removeClass(oppCurrency, 'slds-has-error');
            oppCurrency.set("v.errors", [{message:""}]);
        }
        
        // Deliver To Country Validation
        var deliverToField = component.find("delivertocountry");
        var deliverToFieldValue = deliverToField.get("v.value");
        console.log(' $A.util.isEmpty(deliverToFieldValue) ' + $A.util.isEmpty(deliverToFieldValue));
        if($A.util.isEmpty(deliverToFieldValue)){
            $A.util.addClass(deliverToField, 'slds-has-error');
            allerrors+="<br/>Deliver to cannot be blank";
        } else {
            $A.util.removeClass(deliverToField, 'slds-has-error');
            deliverToField.set("v.errors", [{message:""}]);
        }
        
        //Expected Close Date Validation
        var expectedCoseDate = component.find("expectedclosedate");
        var expectedCoseDateValue = expectedCoseDate.get("v.value");
        console.log(' $A.util.isEmpty(expectedCoseDateValue) ' + $A.util.isEmpty(expectedCoseDateValue));
        if($A.util.isEmpty(expectedCoseDateValue)){
            $A.util.addClass(expectedCoseDate, 'dateRequired');
            allerrors+="<br/>Please select expected close date.";
        } else {
            $A.util.removeClass(expectedCoseDate, 'dateRequired');
            expectedCoseDate.set("v.errors", [{message:""}]);
        }
        
        var primaryContact = component.find("oppPrimaryContactName");
        var primaryContactValue = primaryContact.get("v.value");
        var primaryContactId = component.get('v.opportunity.Primary_Contact_Name__c');
        //STSK0014202
        //if($A.util.isEmpty(primaryContactValue)){
        //    $A.util.addClass(primaryContact, 'requiredField');
        if($A.util.isEmpty(primaryContactId)){
            $A.util.addClass(primaryContact, 'requiredField');
        	//expectedCoseDate.set("v.errors", [{message:"Please select expected close date"}]);
            //component.set("v.errors", 'Please select expected close date');
            allerrors+="<br/>Please select primary contact";
            //return false;
        } else {
            $A.util.removeClass(primaryContact, 'dateRequired');
            expectedCoseDate.set("v.errors", [{message:""}]);
            //return true;
        }
        
        //Opportunity Stage Name Validation
        
        var oppObj = component.get("v.opportunity");
        
        var opportunityStageName = component.find("opportunitystagename");
        var opportunityStageNameValue = opportunityStageName.get("v.value");
        console.log(' $A.util.isEmpty(opportunityStageNameValue) ' + $A.util.isEmpty(opportunityStageNameValue));
        if($A.util.isEmpty(opportunityStageNameValue)){
            $A.util.addClass(opportunityStageName, 'slds-has-error');
            //opportunityStageName.set("v.errors", [{message:"Please select opportunity stage"}]);
            allerrors+="<br/>Please select opportunity stage.";
            //component.set("v.errors", 'Please select opportunity stage');
            //return false;
        } else {
            $A.util.removeClass(opportunityStageName, 'slds-has-error');
            opportunityStageName.set("v.errors", [{message:""}]);
            allerrors = allerrors +this.opportunityStageValidations(component, oppObj, opportunityStageNameValue);
            //return true;
        }
        
        //Payer Same as Customer
        var existingEquip = component.find("existingproductreplacement");
        var existingEquipValue = existingEquip.get("v.value");
        if($A.util.isEmpty(existingEquipValue)){
            $A.util.addClass(existingEquip, 'slds-has-error');
            //paySCust.set("v.errors", [{message:"Payer Same as Customer cannot be blank"}]);
            allerrors+="<br/>Existing Product Replacement cannot be blank.";
            //component.set("v.errors", 'Payer Same as Customer cannot be blank');
            //return false;
        } else {
            $A.util.removeClass(existingEquip, 'slds-has-error');
            existingEquip.set("v.errors", [{message:""}]);
        } 
        
        var existingProdCheck = false;
        if(oppObj.Existing_Equipment_Replacement__c == 'Yes'){
            existingProdCheck = true;
        }
        //alert(existingProdCheck);
        if(existingProdCheck == true){
            var velocityTakeOutVal = component.get('v.opportunity.Velocity_Takeout__c'); // not required
            var compTakeOutVendorVal = component.get('v.opportunity.Competitive_Takeout_Vendor__c');
            var compSysLostVal = component.get('v.opportunity.Competitive_System_Lost__c');
            var compSoftLostVal = component.get('v.opportunity.Competitive_Software_Lost__c'); // not required
            var collLostBusiReasons = component.get('v.opportunity.Collection_of_Lost_Business_reason__c'); // not required
            var varianPCSNNonVarianIB = component.find("VarianPCSNNonVarianIB").get("v.value");
           	var Varian_SW_Serial_No_NonVarianIB = component.find("VarianSWSerialNoNonVarianIB").get("v.value");
            
            // Service Contracts takeout addition
            var servicetakeoutVal = component.get('v.opportunity.Service_Takeout__c');
            
            console.log(' ##compTakeOutVendorVal## ' + compTakeOutVendorVal);
            console.log(' ##compSysLostVal## ' + compSysLostVal);
            console.log(' ##servicetakeoutVal## ' + servicetakeoutVal);
            // Puneet: extending logic for Servic Contracts
            if( (compTakeOutVendorVal == 'None' || $A.util.isEmpty(compTakeOutVendorVal)) && 
               	(compSysLostVal == 'None' || $A.util.isEmpty(compSysLostVal)) &&
                (servicetakeoutVal == 'None' || $A.util.isEmpty(servicetakeoutVal))
              ) {
                //this.toggleModalClass(component, true, 'Please select atleast one takeout from Existing Equipment Replacement');
                allerrors+="<br/>Please select atleast one takeout from Existing Equipment Replacement.";
                //component.set("v.errors", 'Please select atleast one takeout from Existing Equipment Replacement');
                //return false;
            } 
            //Added by Shital 5 April 2018
            if((typeof(varianPCSNNonVarianIB) == 'undefined' || varianPCSNNonVarianIB == 'None') &&
               (compTakeOutVendorVal != 'None' && !($A.util.isEmpty(compTakeOutVendorVal)))){ 
                allerrors+="<br/>Please select Varian PCSN/Non Varian IB.";
            }
            if((typeof(Varian_SW_Serial_No_NonVarianIB) == 'undefined' || Varian_SW_Serial_No_NonVarianIB == 'None') &&
               (compSysLostVal != 'None' && !($A.util.isEmpty(compSysLostVal)))){ 
                allerrors+="<br/>Please select Varian SW Serial No./Non Varian IB.";
            }
        } 
        console.log("--checkAllErrors"+(allerrors != '')+"---"+allerrors);
        if(allerrors != ''){
            component.set("v.errors",allerrors+'<br/>');
            return false;
        }
        
        
        var isChinaRegion = component.get('v.opportunityController.isChinaRegion');
        if(isChinaRegion) {
            this.toggleModalClass(component, true, 'You do not have permission to create opportunity for this account');
            return false;
        }
        return true;
    },
    
    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
    },
    
    deleteCustomLookupReferences : function(sfOpp){
        delete sfOpp.Primary_Contact_Name__r;
        delete sfOpp.Site_Partner__r;
        delete sfOpp.Quote_Replaced__r;
        delete sfOpp.Account;
    },
    
    getSFAccountDetails : function(component, accountRecordId, accountType){
        console.log('----getAccountDetails');
        var getAccountDetails = component.get("c.getAccount");
        getAccountDetails.setParams({
            "accountId":accountRecordId
        });
        getAccountDetails.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' === state === ' + state);
            if(state == "SUCCESS" && component.isValid()) {
                console.log(' === inside state === ' + response.getReturnValue());
                var accountObject = response.getReturnValue();
                this.updateAccountFields(component, accountObject, accountType);
            } else if( component.isValid() && state == "ERROR") {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", response.getError()[0].message);
            }
        });
        $A.enqueueAction(getAccountDetails);
    },
    
    updateAccountFields : function(component, accountObj, accountField){
        console.log('---updateAccountFields'+accountObj+'----'+accountField);
        if(accountField == "SitePartner"){
            component.set("v.opportunityController.sitePartnerAddress", this.getAccountAddress(accountObj));
        }else if(accountField == 'OppAccount'){
            component.set("v.opportunityController.accAddress", this.getAccountAddress(accountObj));
            if(accountObj.Account_Sector__c){
                component.set("v.opportunity.Account_Sector__c", accountObj.Account_Sector__c);
            }
            var currencyCode = component.get("v.opportunity.CurrencyIsoCode");
            component.set("v.opportunity.CurrencyIsoCode", accountObj.CurrencyIsoCode);
            component.set("v.opportunity.Deliver_to_Country__c", accountObj.Country__c);
        }
    },
    
    getAccountAddress : function(oppAccount){
        console.log('---getAccountAddress'+JSON.stringify(oppAccount));
        var acctAddress = (oppAccount.BillingStreet?(oppAccount.BillingStreet+',\r\n'):'')+
            (oppAccount.BillingCity?(oppAccount.BillingCity+','):'')+
            (oppAccount.BillingState?(oppAccount.BillingState+','):'')+
            (oppAccount.BillingPostalCode?(oppAccount.BillingPostalCode+',\r\n'):'')+
            (oppAccount.BillingCountry?(oppAccount.BillingCountry+''):'');
        return acctAddress;      
    },
    
    opportunityStageValidations : function(component, oppInstance, opportunityStageNameValue){
        var errors = "Please enter ";
        if(opportunityStageNameValue == '8 - CLOSED LOST' || opportunityStageNameValue == '7 - CLOSED WON'){

            if($A.util.isEmpty(oppInstance.Competitive_Price__c)){
                $A.util.addClass(component.find("compitivedealprice"), 'slds-has-error');
                errors +=  "Competitive Deal Price, "
            }
            /*
            if($A.util.isEmpty(oppInstance.Comments_for_Win__c)){
                $A.util.addClass(component.find("commentsforwin"), 'slds-has-error');
                errors +=  "Comments For Win/Loss, "
            }*/
            
            if($A.util.isEmpty(oppInstance.Primary_Won_Reason__c)){
                $A.util.addClass(component.find("primarywonreason"), 'slds-has-error');
                errors +=  "Primary Won/Loss Reason, "
            }
            /*if($A.util.isEmpty(oppInstance.Primary_Won_Detail__c)){
                $A.util.addClass(component.find("primarywondetail"), 'slds-has-error');
                errors +=  "Primary Won/Loss Detail, "
            }*/
            if($A.util.isEmpty(oppInstance.Secondary_Won_Reason__c)){
                $A.util.addClass(component.find("secwonreason"), 'slds-has-error');
                errors +=  "Secondary Won/Loss Reason, "
            }
            /*if($A.util.isEmpty(oppInstance.Secondary_Won_Detail__c)){
                $A.util.addClass(component.find("secwondetail"), 'slds-has-error');
                errors +=  "Secondary Won/Loss Detail, "
            }*/
        }
        
        if(opportunityStageNameValue == '8 - CLOSED LOST' ){
            if($A.util.isEmpty(oppInstance.Actions_to_win_future_business1__c)){
                $A.util.addClass(component.find("actionstowinfuture"), 'slds-has-error');
                errors +=  "Actions to win future business, "
            }
            /*
            if($A.util.isEmpty(oppInstance.Comments_for_Win__c)){
                $A.util.addClass(component.find("commentsforwin"), 'slds-has-error');
                errors +=  "Comments For Win/Loss, "
            }
            */
            var compititorListVal = component.get("v.CompetitorDetails");
            var isSynced = false; // variable get set when atleast one Competitor rec synced to Account
            
            for (var i = 0; i < compititorListVal.length; i++) { 
                if(compititorListVal[i].nonVarianProduct.isLost__c) {
                    isSynced = true;
                    console.log('== inside if == ' + isSynced);
                }
            }
            if(isSynced != true && compititorListVal.length > 0) {
                errors +=  "atleast one competitor in the \"Competitor Details\" "
            }
            
        }        
        
        if(errors!="Please enter "){
            errors = errors.slice(0, -2)+".";
            return "<br/>"+errors;
        }
        return "";
    },
    
    // 2 feb 2018function called on Lost software change
    lostSoftwareChange: function(component, initEvent) {
        var lostSoftwareValue = component.find("lostsoftware").get("v.value");
        console.log(' === lostSoftwareValue ==== ' + lostSoftwareValue);
        var obj = component.get('v.opportunityController');
        obj.lostOISDetail = obj.lostOIS[lostSoftwareValue];
        obj.lostTPSDetail = obj.lostTPS[lostSoftwareValue];
        console.log(' === obj.lostSoftwareValue ==== ' + lostSoftwareValue);
        console.log(' === obj.lostOISDetail ==== ' + obj.lostOISDetail);
        console.log(' === obj.lostTPSDetail ==== ' + obj.lostTPSDetail);
        component.set('v.opportunityController', obj);
        if(!initEvent){
            //component.set('v.opportunity.Secondary_Won_Detail__c','');
        }
    },
    
    // function called on Primary Won Reason Change
    primaryWonReasonChange: function(component, initEvent) {
        var primaryWonReason = component.find("primarywonreason").get("v.value");
        var obj = component.get('v.opportunityController');
        
        obj.primaryWonDetail = obj.wonBusinessPrimaryWon[primaryWonReason];
        component.set('v.opportunityController', obj);
        if(!initEvent){
            component.set('v.opportunity.Primary_Won_Detail__c','');
        }
    },
    // function called on Secondary Won Reason change
    secondaryWonReasonChange: function(component, initEvent) {
        var secWonReason = component.find("secwonreason").get("v.value");
        var obj = component.get('v.opportunityController');
        
        obj.secWonDetail = obj.wonBusinessSecondaryWon[secWonReason];
        component.set('v.opportunityController', obj);
        if(!initEvent){
            component.set('v.opportunity.Secondary_Won_Detail__c','');
        }
    },
    // function called on Primary Loss Reason change
    primaryLossReasonChange: function(component, initEvent) {
        var primarylossreason = component.find("primarylossreason").get("v.value");
        var obj = component.get('v.opportunityController');
        console.log(' === primarylossreason === ' + primarylossreason);
        console.log("---obj.lossBusinessPrimaryLoss"+JSON.stringify(obj.lossBusinessPrimaryLoss));
        obj.primaryLostDetail = obj.lossBusinessPrimaryLoss[primarylossreason];
        component.set('v.opportunityController', obj);
        if(!initEvent){
            component.set('v.opportunity.Primary_reason__c','');
        }
    },
    // function called on Secondary Loss Reason change
    secondaryLossReasonChange: function(component, initEvent) {
        var secondarylossreason = component.find("secondarylossreason").get("v.value");
        var obj = component.get('v.opportunityController');
        
        obj.secLostDetail = obj.lossBusinessSecondaryLoss[secondarylossreason];
        component.set('v.opportunityController', obj);
        console.log('----before init event'+component.get("v.opportunity.Secondary_Reason__c"));
        console.log("-----"+component.get("v.opportunityController.secLostDetail"));
        if(!initEvent){
            console.log('----in init event');
            component.set('v.opportunity.Secondary_Reason__c','');
        }
    }
})
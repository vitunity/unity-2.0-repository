/**
 * @author		:		Puneet Mishra
 * @created		:		22 august 2017
 * @description	:		component controller
 */

({
    doInit : function(component, event, helper) {   
        //alert('Test');
        //component.set("v.Spinner", true); 
        //helper.getUrlParameter(component, event);
        //var oppId = helper.getURL(component, event,'recordId');
        
        //var accId = helper.getURL(component, event,'accRecordId');
        //alert('^^^^oppID^^'+oppId);
        //component.set("v.recordId",oppId);
        
        var recId = component.get("v.recordId");
        var oppName = component.find("oppname");
        $A.util.removeClass(oppName, 'input');
        
        var getOpp = component.get('c.fetchOpportunity');
        var accountId = component.get("v.accountId");
        getOpp.setParams({
                "opportunityId":component.get("v.recordId"),
                "accId":component.get("v.accountId")
        });
        getOpp.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var obj = response.getReturnValue();
                console.log('------obj.opportunity'+JSON.stringify(obj));
                component.set("v.opportunity", obj.opportunity);
                if(obj.opportunity.AccountId ==  null){
                   obj.opportunity.AccountId = accountId;
                }
                var oppType = component.get('v.opportunity.Type');
                if(oppType == 'Replacement') {
                    helper.applyCSS(component, event);
                } else {
                    helper.removeCSS(component, event);
                }
                var replacement = component.get('v.opportunity.Existing_Equipment_Replacement__c');
                if(replacement == 'Yes') {
                    helper.existingProductCSS(component, event);
                } else {
                    helper.removeExistingProductCSS(component, event);
                }
                
                var oppStage = component.get('v.opportunity.StageName');
                if(oppStage == '8 - CLOSED LOST') {
                    helper.opportunityWonLostCSS(component, event);
                } else {
                    helper.removeOpportunityWonLostCSS(component, event);
                }
                component.set("v.opportunityController", obj);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(getOpp);
    },
    
    // function called on Primary Won Reason Change
    onPrimaryWonReasonChange: function(component, event, helper) {
        var primaryWonReason = component.find("primarywonreason").get("v.value");
		var obj = component.get('v.opportunityController');
    	
		obj.primaryWonDetail = obj.wonBusinessPrimaryWon[primaryWonReason];
		component.set('v.opportunityController', obj);
        
	},
    // function called on Secondary Won Reason change
    onSecondaryWonReasonChange: function(component, event, helper) {
        var secWonReason = component.find("secwonreason").get("v.value");
		var obj = component.get('v.opportunityController');
    	
        obj.secWonDetail = obj.wonBusinessSecondaryWon[secWonReason];
		component.set('v.opportunityController', obj);
        
	},
    // function called on Primary Loss Reason change
    onPrimaryLossReasonChange: function(component, event, helper) {
        var primarylossreason = component.find("primarylossreason").get("v.value");
		var obj = component.get('v.opportunityController');
    	console.log(' === primarylossreason === ' + primarylossreason);
		obj.primaryLostDetail = obj.lossBusinessPrimaryLoss[primarylossreason];
		component.set('v.opportunityController', obj);
        
	},
    // function called on Secondary Loss Reason change
    onSecondaryLossReasonChange: function(component, event, helper) {
        var secondarylossreason = component.find("secondarylossreason").get("v.value");
		var obj = component.get('v.opportunityController');
    	
        obj.secLostDetail = obj.wonBusinessSecondaryWon[secondarylossreason];
		component.set('v.opportunityController', obj);
        
	},
    // function automatically call by aura:waiting event
    showSpinner: function(component, event, helper) {
        // make spinner attribute true for display loading spinner
        component.set("v.Spinner", true);
	},
    
    // function automatically call by aura:doneWaiting event
    hideSpinner: function(component, event, helper) {
        // make spinner attribute true for display loading spinner
    	component.set("v.Spinner", false);
    },
    
    // function called on click of Cancel and close the popup
    cancelDialog1: function(component, event, helper) {
        var recId = component.get("v.recordId");
        var accountId = component.get("v.accountId");

        if( accountId != '' && accountId != null ) {
            if(typeof sforce !== 'undefined') {
                sforce.one.navigateToSObject(accountId,"DETAIL");  
            }
        }else if( recId != '' && recId != null ) { 
            /*if(typeof sforce !== 'undefined') {
                  sforce.one.navigateToSObject(recId,"DETAIL");  
                }*/
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recId,
                "slideDevName": "DETAIL"
            });
            navEvt.fire();
            
        }else{
            //if(typeof sforce !== 'undefined') {
                //sforce.one.navigateToURL('/006'); 
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": "/006"
                });
                urlEvent.fire();
            //}                
        }
        /*
        var workspaceAPI = component.find("workspace");
        var isConsole = false;
        workspaceAPI.isConsoleNavigation().then(function(response) {
        	alert('Test');
            workspaceAPI.getFocusedTabInfo().then(function(response) {
                var focusedTabId = response.tabId;
                alert(focusedTabId);
                workspaceAPI.closeTab({tabId: focusedTabId});
            })
            .catch(function(error) {
                console.log(error);
            });
        });
        */
        
    },
    
    hideModal : function(component, event, helper) {
        helper.toggleModalClass(component, false);
    },
    handlePrimaryReasonChange: function(component, event, helper) {
        // Get a reference to the dependent picklist
        //var selectContact = component.find("primarylostdetail");
        
        // Call the helper function to refresh the
        // dependent picklist, based on the new controlling value
        //component.set("v.opportunityController.primaryLostDetail",
            //selectContact.optionsByControllingValue[event.target.value]);
    	
        var parentValue = component.find('primarylossreason').get('v.value');
        console.log(' ParentVal => ' + parentValue);
        //console.log(' ParentVal 2 => ' + component.get('v.primarylostdetail')[parentValue]);
        console.log(' ParentVal => ' + parentValue);
        //component.set('v.opportunityController.primaryLostDetail', component.get('v.primarylostdetail')[parentValue]);
        
        //if(parentValue != '')
          //  component.set('v.disabledPick',false);
        //else
          //  component.set('v.disabledPick',true);
        
    }, 
    //hide show on value change of Type picklist
    toggleQuote: function (component, event, helper) {
        var typeValue = component.find('accountType').get('v.value');
    	//obj.accountType = typeValue;
        var changeElement = component.find("divID");
        if(typeValue == 'Replacement') {
        	//$A.util.toggleClass(changeElement, "slds-hide");
			//component.set('v.opportunityController', obj);
			helper.applyCSS(component, event);
        } else {
            //$A.util.toggleClass(changeElement, "slds-hide");
        	helper.removeCSS(component, event);
        }
    }, 
    
    handleMyComponentEvent : function(component, event, helper) {
        console.log(' EVENT FIRED ');
    },
    
    onCheckBoxChange:function(component, event, helper) {
    	var selectedHeaderCheck = event.getSource().get("v.value");
        var selectedHeaderCheckField = event.getSource().get("v.name");
        component.set(selectedHeaderCheckField,selectedHeaderCheck);
        //alert(selectedHeaderCheckField);
    },
    
    saveRecord : function(component, event, helper) {
        //alert('before validation');
        if(!helper.requiredfieldValidation(component, event)){
            var errorFooterBtn = component.find("errorFooterBtn");
            $A.util.addClass(errorFooterBtn, 'slds-show');
            $A.util.removeClass(errorFooterBtn, 'slds-hide');
            var erroFooterSectn = component.find("erroFooterSectn");
            $A.util.addClass(erroFooterSectn, 'slds-show');
            $A.util.removeClass(erroFooterSectn, 'slds-hide');
            return;
        }
        
        var opport1 = component.get("v.opportunity ");
        var recId = component.get("v.recordId");
        helper.deleteCustomLookupReferences(opport1);
        component.set("v.Spinner", true);
        var getOpp = component.get('c.saveOpprotunity');
        getOpp.setParams({
            "opport":opport1//opport.opportunity
        });
       	getOpp.setCallback(this, function(response) {
			var state = response.getState();
            console.log('**opport1**'+JSON.stringify(opport1));
            console.log('**recId**'+recId);
            console.log(' === state === ' + state);
            if(state == "SUCCESS" && component.isValid()) {
                console.log(' === inside state === ' + response.getReturnValue());
            	//sforce.one.navigateToSObject(response.getReturnValue(),"refresh");
                var recordId = response.getReturnValue();
                //try{
                    //var navEvtO = $A.get("e.force:navigateToSObject");
                    //navEvtO.setParams({
                       // "recordId": recordId
                    //});
                    //navEvtO.fire();
                //}catch(error){
                ////Commented by Shital
                    /*if(typeof sforce !== 'undefined') {
                      sforce.one.navigateToSObject(recordId,"DETAIL");  
                    }*/
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recId,
                        "slideDevName": "DETAIL"
                    });
                    navEvt.fire();
                //}
            } else if( component.isValid() && state == "ERROR") {
                console.log("Exception caught successfully");
                console.log("Error object", response);
                console.log("Error Message", response.getError()[0]);
                console.log("Error Message", response.getError()[0].message);
                console.log("Error Message", response.getState());
                console.log("Error object", JSON.stringify(response));
                
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", response.getError()[0].message);
                //helper.toggleModalClass(component, true, response.getError()[0].message);
            }
           	component.set("v.Spinner", false);
        });
		$A.enqueueAction(getOpp);
    },
    // this function for displaying product family helptext
	display : function(component, event, helper) 
	{
		var toggleText = component.find("tooltip");
	    $A.util.addClass(toggleText, 'slds-show');
	    $A.util.removeClass(toggleText, 'slds-hide');

    },

    // this function for displaying out product family helptext
    displayOut : function(component, event, helper) 
    {
		var toggleText = component.find("tooltip");
	    $A.util.addClass(toggleText, 'slds-hide');
	    $A.util.removeClass(toggleText, 'slds-show');
    },
    
    // this function for displaying out product family helptext
    erroFooterSectn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
	    $A.util.addClass(erroFooterSectn, 'slds-hide');
	    $A.util.removeClass(erroFooterSectn, 'slds-show');
    },
    
    // this function for displaying out product family helptext
    erroFooterBtn : function(component, event, helper) 
    {
        var erroFooterSectn = component.find("erroFooterSectn");
	    $A.util.addClass(erroFooterSectn, 'slds-show');
	    $A.util.removeClass(erroFooterSectn, 'slds-hide');
    },
    
    showCustomLookupModalForReplacement: function(component, event, helper) {
        //oppAccountId-oppAccountName-HW-SVMXC__Installed_Product__c-IP-linacsystakeout, oppAccountId-oppAccountName-HW-SVMXC__Installed_Product__c
        
        var recordLoopupKey = event.getSource().get("v.name");
        var recordLookupArray = recordLoopupKey.split("-");
        var recordIdField = recordLookupArray[0];
        var recordNameField = recordLookupArray[1];
        var productFamily = recordLookupArray[2];
        
        component.set("v.productFamily", productFamily);
        component.set("v.recId", recordIdField);
        var elementId = recordLookupArray[5];
        var sObjectName;
        var sObjectLabel;
        var VALEU;
        
        if(elementId == 'linacsystakeout') {
            VALEU = component.find("linacsystakeout").get("v.value");
            //console.log(' ==== VALEU ===== ' + VALEU);
            /*if(typeof(VALEU) == 'undefined' || VALEU == 'None') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }*/
            if(VALEU == 'Varian') {
                sObjectLabel = 'Install Products';
                sObjectName = 'SVMXC__Installed_Product__c';
            } else {
                sObjectLabel = 'Competitors';
                sObjectName = 'Competitor__c';
            }
        } else if(elementId == 'oissysTakeout') {
            VALEU = component.find("oissysTakeout").get("v.value");
            if(typeof(VALEU) == 'undefined' || VALEU == 'None') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }
            if(VALEU == 'Varian') {
                sObjectLabel = 'Install Products';
                sObjectName = 'SVMXC__Installed_Product__c';
            } else {
                sObjectLabel = 'Competitors';
                sObjectName = 'Competitor__c';
            }
        }else if(elementId == 'serviceTakeout') {
            VALEU = component.find("serviceTakeout").get("v.value");
            
            if(typeof(VALEU) == 'undefined' || VALEU == 'None') {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", 'Please Select ' + productFamily +' Takeout');
                return;   
            }
            sObjectLabel = 'Service Takeout';
            sObjectName = 'Competitor__c';
        }
        console.log(VALEU + ' === sObjectLabel === ' + sObjectLabel);
        component.set("v.sctakeout", String(VALEU));
		console.log(' ==sObjectLabel== ' + sObjectLabel);
		console.log(' ==sObjectName== ' + sObjectName);
        console.log(' ==accountId== ' + component.get("v.opportunity.AccountId"));
        component.set("v.oppAccId", component.get("v.opportunity.AccountId"));
        component.set("v.sObjectLabel", sObjectLabel);
        component.set("v.sObjectAPIName", sObjectName);
        helper.toggleClass(component,true,"customProdReplacementLookupPopUp");
    },
    
    //Replacement Logic
    setSelectedReplacementRecord : function(component, event, helper) {
        var values = event.getParam("prodNames");
        var recId = event.getParam("recordId");
        component.find(recId).set("v.value",values);
        helper.toggleClass(component, false, "customProdReplacementLookupPopUp");
    },
    
    // Hide custom replacement Modal
    hideCustomReplacementModalModal : function(component, event, helper) {
        //Toggle CSS styles for hiding Modal
        helper.toggleClass(component,false,"customProdReplacementLookupPopUp");
    },
    
    //Opens custom lookup component popup dynamically based of object type
    showCustomLookupModal: function(component, event, helper) {
		//Toggle CSS styles for opening Modal
		
        var recordLoopupKey = event.getSource().get("v.name");
        var recordLookupArray = recordLoopupKey.split("-");
        var recordIdField = recordLookupArray[0];
        var recordNameField = recordLookupArray[1];
        var sObjectLabel = recordLookupArray[2];
        var sObjectName = recordLookupArray[3];
        
        var recordNameComponent = component.find(recordNameField);
        var recordNameValue = recordNameComponent.get("v.value");
        
        console.log('----recordNameValue'+recordNameValue+'--recordIdField'+recordIdField+
                    '--recordNameField'+recordNameField+'---sObjectName'+sObjectName);
        var objectFieldLabels = [];
        var objectFieldNames = [];
        if(sObjectName == "Account"){
            console.log('----Account--'+sObjectName);
            //objectFieldLabels = ['Name', 'Account Type', 'Street', 'City', 'State', 'Zip/Postal Code', 'Country', 
                                //'ERP Customer Id', 'Account Record Type'];
            objectFieldLabels = ['Name', 'Account Type'];
                                
            objectFieldNames = ['Name', 'Account_Type__c'];
            
            component.set("v.defaultFieldName", "");
        	component.set("v.defaultFieldValue", "");
        }else if(sObjectName == "Contact"){
            console.log('----Contact--'+sObjectName);
            //objectFieldLabels = ['Name', 'Account Name', 'Phone', 'Email', 'City', 'State', 'Postal Code', 'Country'];
            //objectFieldNames = ['Name', 'Account.Name', 'Phone', 'Email', 'MailingCity', 'MailingState', 'MailingPostalCode', 
                                //'MailingCountry'];
            objectFieldLabels = ['Name', 'Account Name'];
            objectFieldNames = ['Name', 'Account.Name'];
            component.set("v.defaultFieldName", "AccountId");
        	component.set("v.defaultFieldValue", component.get("v.opportunity.AccountId"));
        }
        console.log('----objectFieldLabels'+objectFieldLabels+'-----objectFieldNames'+objectFieldNames);
        component.set("v.fieldLabels", objectFieldLabels);
        component.set("v.fieldNames", objectFieldNames);
        component.set("v.selectedLookupIdField", recordIdField);
        component.set("v.selectedLookupNameField", recordNameField);
        component.set("v.selectedLookupSearchKey", recordNameValue);
        component.set("v.objectLabelValue", sObjectLabel);
        component.set("v.objectAPIName", sObjectName);
        
		helper.toggleClass(component,true,"customLookupPopUp");
	},
    
    hideCustomLookupModal : function(component, event, helper) {
        console.log('----in hideModel');
		//Toggle CSS styles for hiding Modal
		helper.toggleClass(component,false, "customLookupPopUp");
	},
    
    setSelectedLookupRecord : function(component, event, helper){
        var sfId = event.getParam("recordId");
        var sfName = event.getParam("recordName");
        var nameField = event.getParam("recordNameField");
        var idField = event.getParam("recordIdField");
        helper.toggleClass(component, false, "customLookupPopUp");
        component.find(nameField).set("v.value",sfName);
        component.find(idField).set("v.value",sfId);
        console.log('-----sfId'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        component.set("v.selectedLookupSearchKey", "");
    },
    
    handleAccountChange : function(component, event, helper){
        var accountId = component.get("v.opportunity.AccountId");
        if(accountId){
            console.log('------handleAccountChange'+accountId);
        	helper.getSFAccountDetails(component, accountId, "OppAccount");
        }    
    },
    
    handleSitePartnerChange : function(component, event, helper){
        var sitePartnerId = component.get("v.opportunity.Site_Partner__c");
        if(sitePartnerId){
            console.log('------handleSitePartnerChange'+sitePartnerId);
        	helper.getSFAccountDetails(component, sitePartnerId, "SitePartner");
        }
    },
    
    // hide/show existing product replacement section - Added by Shital
    toggleExistingProdReplacement : function(component, event, helper) {
        var replacement = component.find('existingproductreplacement').get('v.value');
        if(replacement == 'Yes') {
            helper.existingProductCSS(component, event);
        } else {
            helper.removeExistingProductCSS(component, event)
        }
    },
    
    // hide/show existing Opportunity won/loss section - Added by Shital
    toggleOpportunityWonLoss : function(component, event, helper) {
        var oppStage = component.find('opportunitystagename').get('v.value');
        if(oppStage == '8 - CLOSED LOST') {
            helper.opportunityWonLostCSS(component, event);
        } else {
            helper.removeOpportunityWonLostCSS(component, event);
        }
    },
    
    vendorSelected : function(component, event, helper) {
        var values = event.getParam("vendorName");
        var recId = event.getParam("recId");
        console.log(' == values == ' + values );
        console.log(' == recId == ' + recId );
        component.find(recId).set("v.value",values);
    }
})
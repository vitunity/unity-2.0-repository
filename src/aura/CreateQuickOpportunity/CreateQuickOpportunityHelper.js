/**
 * @author		:		Puneet Mishra
 * @created		:		22 august 2017
 * @description	:		component helper
 */
({
    // navigate user to record detail page on click of Cancel
    navigateTo: function(component, recId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":recId 
        });
        navEvt.fire();
    },
    
    //Applies Style class when Account Type is Replacement
    applyCSS: function(component, event) {
        console.log('APPLY');
        var cmpTarget = component.find('divID');
        $A.util.addClass(cmpTarget, 'divCSS');
        $A.util.removeClass(cmpTarget, 'hideDiv');
    },
    
    toggleModalClass: function(component,showModal, message) {
    	console.log('---in toggle class');
        var modal = component.find("modalDiv");
        var parent = component.find("parentDiv");
        component.find("messages").set("v.value", message);
        console.log(' - parentDIV - ' + parent + ' - modalDiv - ' + modal);
        if(showModal){
            $A.util.removeClass(modal,'LockOff');
            $A.util.addClass(modal,'LockOn');
        } else {
            $A.util.removeClass(modal,'LockOn');
            $A.util.addClass(modal,'LockOff');
            component.find("messages").set("v.value", '');
        }
            
    },
    
    //Removes Style class when Account Type is other than Replacement
    removeCSS: function(component, event) {
        console.log('REMOVE');
        var cmpTarget = component.find('divID');
        $A.util.removeClass(cmpTarget, 'divCSS');
        $A.util.addClass(cmpTarget, 'hideDiv');
    },
    
    // Method verify if the Required fields are populated with alues if not then give a appropriate error message
    requiredfieldValidation: function(component, event) {
        var allerrors = '';
        //Opportunity Name Validation
        var oppName = component.find("oppname");
        var oppNameValue = oppName.get("v.value");
        if($A.util.isEmpty(oppNameValue)){
            $A.util.addClass(oppName, 'slds-has-error');
            //oppName.set("v.errors", [{message:"Opportunity name cannot be blank"}]);
            allerrors+="Opportunity name cannot be blank";
            //component.set("v.errors", 'Opportunity name cannot be blank');
            //return false;
        } else {
            $A.util.removeClass(oppName, 'slds-has-error');
            oppName.set("v.errors", [{message:""}]);
        	//return true;
        }
                
        //Acount sector validation
        var accountSector = component.find("accountSector");
        var accountSectorValue = accountSector.get("v.value");
        if($A.util.isEmpty(accountSectorValue)){
            $A.util.addClass(accountSector, 'slds-has-error');
            //accountSector.set("v.errors", [{message:"Account sector cannot be blank"}]);
            allerrors+="<br/>Account sector cannot be blank";
            //component.set("v.errors", 'Account sector cannot be blank');
            //component.set("v.errors", allerrors);
            //return false;
        } else {
            $A.util.removeClass(accountSector, 'slds-has-error');
            accountSector.set("v.errors", [{message:""}]);
        }
        
        //Payer Same as Customer
        var paySCust = component.find("paySCust");
        var paySCustValue = paySCust.get("v.value");
        if($A.util.isEmpty(paySCustValue)){
            $A.util.addClass(paySCust, 'slds-has-error');
            //paySCust.set("v.errors", [{message:"Payer Same as Customer cannot be blank"}]);
            allerrors+="<br/>Payer Same as Customer cannot be blank";
            //component.set("v.errors", 'Payer Same as Customer cannot be blank');
            //return false;
        } else {
            $A.util.removeClass(paySCust, 'slds-has-error');
            paySCust.set("v.errors", [{message:""}]);
        }
        
        
        //Opportunity Currency Validation
        var oppCurrency = component.find("oppCurrency");
        var oppCurrencyValue = component.get('v.opportunity.CurrencyIsoCode');
        console.log(' $A.util.isEmpty(oppCurrencyValue) ' + $A.util.isEmpty(oppCurrencyValue));
        if($A.util.isEmpty(oppCurrencyValue)){
            $A.util.addClass(oppCurrency, 'slds-has-error');
            //oppCurrency.set("v.errors", [{message:"Please select opportunity currency"}]);
            allerrors+="<br/>Please select opportunity currency";
        	//component.set("v.errors", 'Please select opportunity currency');
            //return false;
        } else {
            $A.util.removeClass(oppCurrency, 'slds-has-error');
            oppCurrency.set("v.errors", [{message:""}]);
        	//return true;
        }
        
        // Deliver To Country Validation
        var deliverToField = component.find("delivertocountry");
        var deliverToFieldValue = deliverToField.get("v.value");
        console.log(' $A.util.isEmpty(deliverToFieldValue) ' + $A.util.isEmpty(deliverToFieldValue));
        if($A.util.isEmpty(deliverToFieldValue)){
            $A.util.addClass(deliverToField, 'slds-has-error');
            //deliverToField.set("v.errors", [{message:"Deliver to cannot be blank"}]);
            //component.set("v.errors", 'Deliver to cannot be blank');
            //return false;
            allerrors+="<br/>Deliver to cannot be blank";
        } else {
            $A.util.removeClass(deliverToField, 'slds-has-error');
            deliverToField.set("v.errors", [{message:""}]);
        	//return true;
        }
        
        //Expected Close Date Validation
        var expectedCoseDate = component.find("expectedclosedate");
        var expectedCoseDateValue = expectedCoseDate.get("v.value");
        console.log(' $A.util.isEmpty(expectedCoseDateValue) ' + $A.util.isEmpty(expectedCoseDateValue));
        //alert('close date');
        if($A.util.isEmpty(expectedCoseDateValue)){
            $A.util.addClass(expectedCoseDate, 'dateRequired');
            //expectedCoseDate.set("v.errors", [{message:"Please select expected close date"}]);
            //component.set("v.errors", 'Please select expected close date');
            allerrors+="<br/>Please select expected close date";
            //return false;
        } else {
            $A.util.removeClass(expectedCoseDate, 'dateRequired');
            expectedCoseDate.set("v.errors", [{message:""}]);
        	//return true;
        }
        
        //Opportunity Stage Name Validation
        var opportunityStageName = component.find("opportunitystagename");
        var opportunityStageNameValue = opportunityStageName.get("v.value");
        console.log(' $A.util.isEmpty(opportunityStageNameValue) ' + $A.util.isEmpty(opportunityStageNameValue));
        if($A.util.isEmpty(opportunityStageNameValue)){
            $A.util.addClass(opportunityStageName, 'slds-has-error');
            //opportunityStageName.set("v.errors", [{message:"Please select opportunity stage"}]);
        	allerrors+="<br/>Please select opportunity stage";
            //component.set("v.errors", 'Please select opportunity stage');
            //return false;
        } else {
            $A.util.removeClass(opportunityStageName, 'slds-has-error');
            opportunityStageName.set("v.errors", [{message:""}]);
        	//return true;
        }
        /*
        // Existing Product Validation
        var existingProd = component.find("existingproductreplacement");
        var existingProdValue = existingProd.get("v.value");
        console.log(' $A.util.isEmpty(existingProdValue) ' + $A.util.isEmpty(existingProdValue));
        var oppObj = component.get("opportunity");
		var existingProdCheck = false;
        if($A.util.isEmpty(existingProdValue) && 
           oppObj.Existing_Equipment_Replacement__c == 'Yes'){
            //$A.util.addClass(existingProd, 'slds-has-error');
            //existingProd.set("v.errors", [{message:"Please select existing product replacement"}]);
            //component.set("v.errors", 'Please select existing product replacement');
            //return false;
            existingProd = true;
        } else {
            //existingProd.set("v.errors", [{message:""}]);
        	//return true;
            if(oppObj.Existing_Equipment_Replacement__c == 'Yes'){
                 existingProd = true;
            }
        }
        */
        
        //Payer Same as Customer
        var existingEquip = component.find("existingproductreplacement");
        var existingEquipValue = existingEquip.get("v.value");
        if($A.util.isEmpty(existingEquipValue)){
            $A.util.addClass(existingEquip, 'slds-has-error');
            allerrors+="<br/>Existing Product Replacement cannot be blank";
        } else {
            $A.util.removeClass(existingEquip, 'slds-has-error');
            existingEquip.set("v.errors", [{message:""}]);
        } 
        
        if(allerrors != ''){
            component.set("v.errors",allerrors+'<br/>');
            return false;
        }
        
        
        var isChinaRegion = component.get('v.opportunityController.isChinaRegion');
        if(isChinaRegion) {
            this.toggleModalClass(component, true, 'You do not have permission to create opportunity for this account');
        	return false;
        }
        return true;
    },
    
    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
		var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
	},
    
    deleteCustomLookupReferences : function(sfOpp){
        delete sfOpp.Primary_Contact_Name__r;
        delete sfOpp.Site_Partner__r;
        delete sfOpp.Quote_Replaced__r;
        delete sfOpp.Account;
    },
    
    getSFAccountDetails : function(component, accountRecordId, accountType){
        console.log('----getAccountDetails');
    	var getAccountDetails = component.get("c.getAccount");
        getAccountDetails.setParams({
            "accountId":accountRecordId
        });
       	getAccountDetails.setCallback(this, function(response) {
			var state = response.getState();
            console.log(' === state === ' + state);
            if(state == "SUCCESS" && component.isValid()) {
                console.log(' === inside state === ' + response.getReturnValue());
                var accountObject = response.getReturnValue();
                this.updateAccountFields(component, accountObject, accountType);
            } else if( component.isValid() && state == "ERROR") {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", response.getError()[0].message);
            }
        });
		$A.enqueueAction(getAccountDetails);
	},
    
    updateAccountFields : function(component, accountObj, accountField){
        console.log('---updateAccountFields'+accountObj+'----'+accountField);
        if(accountField == "SitePartner"){
            component.set("v.opportunityController.sitePartnerAddress", this.getAccountAddress(accountObj));
        }else if(accountField == 'OppAccount'){
            component.set("v.opportunityController.accAddress", this.getAccountAddress(accountObj));
            if(accountObj.Account_Sector__c){
            	component.set("v.opportunity.Account_Sector__c", accountObj.Account_Sector__c);
        	}
            component.set("v.opportunity.CurrencyIsoCode", accountObj.CurrencyIsoCode);
            component.set("v.opportunity.Deliver_to_Country__c", accountObj.Country__c);
        }
    },
    
    getAccountAddress : function(oppAccount){
        console.log('---getAccountAddress'+JSON.stringify(oppAccount));
        var acctAddress = (oppAccount.BillingStreet?(oppAccount.BillingStreet+',\r\n'):'')+
                (oppAccount.BillingCity?(oppAccount.BillingCity+','):'')+
                (oppAccount.BillingState?(oppAccount.BillingState+','):'')+
                (oppAccount.BillingPostalCode?(oppAccount.BillingPostalCode+',\r\n'):'')+
                (oppAccount.BillingCountry?(oppAccount.BillingCountry+''):'');
    	return acctAddress;      
    },
    
    getURL : function(component, event, parameter){
        //alert("####param##"+parameter);
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;  
            //alert("####sURLVariables##"+sPageURL+"!!!@@@");
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        return getUrlParameter(parameter);
    },
    
    // Existing Replacement Product section Add Styling
    existingProductCSS : function(component, event) {
        var cmpTarget = component.find('existingProdId');
        var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.addClass(cmpTarget, 'divCSS');
        $A.util.addClass(cmpHeaderTarget, 'divCSS');
        $A.util.removeClass(cmpTarget, 'hideDiv');
        $A.util.removeClass(cmpHeaderTarget, 'hideDiv');
    },
    
    // Existing Replacement Product section Remove Styling
    removeExistingProductCSS: function(component, event) {
        console.log('REMOVE');
        var cmpTarget = component.find('existingProdId');
        var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.removeClass(cmpTarget, 'divCSS');
        $A.util.removeClass(cmpHeaderTarget, 'divCSS');
        $A.util.addClass(cmpTarget, 'hideDiv');
        $A.util.addClass(cmpHeaderTarget, 'hideDiv');
    },
    
    // Existing Replacement Product section Add Styling
    opportunityWonLostCSS : function(component, event) {
        var cmpTarget = component.find('opptyWonLossSection');
        //var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.addClass(cmpTarget, 'divCSS');
        //$A.util.addClass(cmpHeaderTarget, 'divCSS');
        $A.util.removeClass(cmpTarget, 'hideDiv');
        //$A.util.removeClass(cmpHeaderTarget, 'hideDiv');
    },
    
    // Existing Replacement Product section Remove Styling
    removeOpportunityWonLostCSS: function(component, event) {
        //console.log('REMOVE');
        var cmpTarget = component.find('opptyWonLossSection');
        //var cmpHeaderTarget = component.find('existingProdHeaderId');
        $A.util.removeClass(cmpTarget, 'divCSS');
        //$A.util.removeClass(cmpHeaderTarget, 'divCSS');
        $A.util.addClass(cmpTarget, 'hideDiv');
        //$A.util.addClass(cmpHeaderTarget, 'hideDiv');
    },
})
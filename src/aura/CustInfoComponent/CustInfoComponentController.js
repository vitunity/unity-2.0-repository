({   
    doInit: function(component, event, helper){
        var action = component.get("c.getCase");
        action.setParams({ caseId : component.get('v.recordId')});
        action.setCallback(this, function(response) {
       		debugger;
            var state = response.getState();
            var caseObj = response.getReturnValue()
            var accountObj = caseObj.Account;
            component.set('v.accountObj',accountObj);
            component.set('v.caseObj',caseObj);
            var conObj = component.get("v.conObj");
            conObj.Id = caseObj.ContactId;
            if(caseObj.Contact != undefined){
				conObj.Name = caseObj.Contact.Name;
                conObj.Phone = caseObj.Contact.Phone;
            }
            
            conObj.AccountId = accountObj.Id;
            component.set('v.conObj',conObj);
            var ipObj = component.get("v.ipObj");
            if(caseObj.ProductSystem__r != undefined)
            ipObj.Name= caseObj.ProductSystem__r.Name;
            ipObj.Id = caseObj.ProductSystem__c;
            component.set('v.ipObj',ipObj);
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
	editCase: function(component, event, helper) {      
		component.set('v.editMode', true);
        debugger;
        var editReset = component.find("custInfoEditComponent");
        var action = component.get("c.getCase");
        action.setParams({ caseId : component.get('v.recordId')});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            var caseObj = response.getReturnValue()
            var accountObj = caseObj.Account;
            component.set('v.accountObj',accountObj);
            component.set('v.caseObj',caseObj);
            var conObj = component.get("v.conObj");
            conObj.Id = caseObj.ContactId;
            if(caseObj.Contact != undefined)
            conObj.Name = caseObj.Contact.Name;
            conObj.AccountId = accountObj.Id;
            component.set('v.conObj',conObj);
            var ipObj = component.get("v.ipObj");
            if(caseObj.ProductSystem__r != undefined)
            ipObj.Name= caseObj.ProductSystem__r.Name;
            ipObj.Id = caseObj.ProductSystem__c;
            component.set('v.ipObj',ipObj);
            console.log(response.getReturnValue());
            editReset.initializeBack(component.get('v.accountObj'),
                                     component.get('v.caseObj'),
                                     component.get('v.conObj'),
                                     component.get('v.ipObj'));
        });
        $A.enqueueAction(action);
        
	},     
	
    handleCustInfoEvent: function(cmp, event, helper) {     
   		cmp.set('v.editMode', false);
        $A.get('e.force:refreshView').fire();
    },
    
	submitCase: function(cmp, event, helper) {     
   		cmp.set('v.editMode', false);
    }, 
})
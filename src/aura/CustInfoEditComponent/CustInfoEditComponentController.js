({
	doInit : function(component, event, helper) {
		helper.initHelper(component, event);
        helper.setIPdata(component, event);
	},
    
    doInitializeBack : function(component, event, helper) {
        debugger;
        var params = event.getParam('arguments');
        if (params) {
            component.set('v.caseObj',params.caseObj);
            component.set('v.conObj',params.conObj);
            component.set('v.accountObj',params.accountObj);
            component.set('v.ipObj',params.ipObj);
            helper.initHelper(component, event);
        	helper.setIPdata(component, event);
        }
	},    
    
    handleLookupChooseEventQuickCase : function(component, event, helper) {
        helper.handleLookupChooseEventQuickCaseHelper(component,event);
    },
    
    handleQuickCaseCreateContactEvent : function(component, event, helper) {
        helper.handleQuickCaseCreateContactEventHelper(component,event);
    },
    saveCaseRecord : function(component, event, helper) {
    	helper.caseCreateHelper(component, event);
    },
    
    backCaseRecord : function(component, event, helper) {
    	var caseObj = component.get("v.caseObj");
        var custInfoEvent = component.getEvent("custInfoEvent");
        custInfoEvent.setParams({
            "data" : caseObj
        });
        $A.get('e.force:refreshView').fire();
        custInfoEvent.fire();
         
    },
    
    accountChanged : function(component, event, helper) {
        helper.getCasedata(component, event);
	},
    
    updateSelectedRows : function(component, event, helper) {
    	helper.updateSelectedRowsHelper(component, event);
    },
    
    updateContactSelectedRows : function(component, event, helper) {
    	helper.updateSelectedContactRowsHelper(component, event);
    },
    
    searchIPComp: function(component, event, helper) {
        helper.setIPdata(component,event);
    },
    
    handleExpandClick: function(component, event, helper) {
    },
    
    handleCollapseClick: function(component, event, helper) {
    },
    
    closeIPComp: function(component, event, helper) {
        var modal = component.find('iPComp');
        $A.util.addClass(modal,'slds-hide');
    },
    
    closeContactComp: function(component, event, helper) {
        var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');
    },
    
    openContactComp: function(component, event, helper) {
        var modal = component.find('contactCmp');
        $A.util.removeClass(modal,'slds-hide');
        helper.setContactdata(component,event);
    },
    
    searchContactComp: function(component, event, helper) {
        helper.setContactdata(component,event);
    },
    
    closeCreateContact: function(component, event, helper) {
        var modal = component.find('createContact');
        $A.util.addClass(modal,'slds-hide');
    },
    
    openIPComp: function(component, event, helper) {
        var modal = component.find('iPComp');
        $A.util.removeClass(modal,'slds-hide');
        helper.setIPdata(component,event);
    },
    
    handleCreatedContactEvent: function(component, event, helper) {
        helper.handleCreatedContactEventHelper(component, event);
    },
    updateContactColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortContactData(cmp, fieldName, sortDirection);
    },
    
    createContact : function (component,event,helper) {
		var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');        
        var createContactEvent = component.getEvent("createContact");
        createContactEvent.setParams({
            "sObjectType" : "Contact",
            "actionType"  : "Create"
        });
        createContactEvent.fire();
        console.log('event fired');
    },
    
    editContact : function (component,event,helper) {
        var createContactEvent = component.getEvent("createContact");
        createContactEvent.setParams({
            "sObjectType" : "Contact",
            "actionType"  : "Edit"
        });
        createContactEvent.fire();
        console.log('event fired');
    }
 
})
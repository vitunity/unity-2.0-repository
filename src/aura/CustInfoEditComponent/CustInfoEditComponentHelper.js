({
    initHelper : function(component, event) {
        debugger;
        component.set('v.caseColumns', [
            {label: 'Created', fieldName: 'CreatedDate', type: 'date'},
            {label: 'Case#', fieldName: 'caseUrl', target: '_self', type: 'url',
                  typeAttributes: { label: { fieldName: 'CaseNumber' } }},
            {label: 'Subject', fieldName: 'Subject', type: 'test'},
            {label: 'Product/System', fieldName: 'productName', type: 'text'},
            {label: 'Status', fieldName: 'Status', type: 'test'},
            {label: 'Contact', fieldName: 'contactName', type: 'lookup'},
            {label: 'Case Owner', fieldName: 'ownerName', type: 'lookup'},
            {label: 'Priority', fieldName: 'Priority', type: 'text'}
        ]);
        component.set('v.gridContactColumns', [
            {label: 'Name', fieldName: 'name', sortable: true,type: 'text'},
            {label: 'Title', sortable: true, fieldName: 'Title', type: 'text'},
            {label: 'Phone', sortable: true, fieldName: 'Phone', type: 'text'},
            {label: 'Email', sortable: true, fieldName: 'Email', type: 'text'},
            {label: 'Email Declined', sortable: true, fieldName: 'EmailDeclined', type: 'text'}
        ]);
        var caseObj = component.get('v.caseObj');
        var conObj = component.get('v.conObj');
        var contactTag = component.find('contactObjId');
        if(contactTag != undefined)
        contactTag.set("v.value",conObj.Name);
        var ipTag = component.find('installedProduct'); 
        var ipObj = component.get('v.ipObj');
        if(ipObj != undefined && ipTag != undefined)
        ipTag.set("v.value",ipObj.Name);
    },
    
    setIPdata : function(component, event) {
         debugger;
		 var columns = [
            {type: 'text',fieldName: 'name', label: 'Name',initialWidth: 200},
            {type: 'text',fieldName: 'productName',label: 'Product Name'},
            {type: 'text',fieldName: 'status',label: 'Status'},
            {type: 'text',fieldName: 'location',label: 'Location'},
            {type: 'text',fieldName: 'billingType',label: 'Billing Type'},
            {type: 'text',fieldName: 'contractName',label: 'Contract'},
            {type: 'date',fieldName: 'expDate',label: 'Exp Date'}
        ];
        component.set('v.gridColumns', columns);
        var accountId = component.get('v.caseObj').AccountId;
        var action = component.get("c.getIPs");
        var ipSearchStrTag =  component.find('ipSearchStr');
        var ipSearchStr  = '';
        if(ipSearchStrTag != undefined){
			ipSearchStr = ipSearchStrTag.get('v.value');
        }
        action.setParams({ accountId : accountId,searchStr:ipSearchStr});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            component.set('v.gridData',JSON.parse(response.getReturnValue()));
            console.log(response.getReturnValue());
            
        });
        $A.enqueueAction(action);
	},
    
     setContactdata : function(component, event) {
		debugger;
        var accountId = component.get('v.caseObj').AccountId;
        var action = component.get("c.getContacts");
        var contactSearchStrTag =  component.find('contactSearchStr');
        var contactSearchStr  = '';
        if(contactSearchStrTag != undefined){
			contactSearchStr = contactSearchStrTag.get('v.value');
        }
        action.setParams({ accountId : accountId,searchStr:contactSearchStr});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            debugger;
            var responseWrap = response.getReturnValue(); 
            for(var i = 0; i < responseWrap.length; i++) {
                var contactObj = responseWrap[i];
                contactObj.account = contactObj.Account.Name;
                contactObj.name = contactObj.FirstName + ' '+contactObj.LastName;
                if(contactObj.Email_Declined__c == true){
                    contactObj.EmailDeclined = 'Yes';
                }else{
                    contactObj.EmailDeclined = 'No';
                }
            }
            component.set('v.gridContactData',responseWrap);
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    
    getCasedataQuery : function(component,accountId) {
    	//debugger;
        var action = component.get("c.getCases");
    	action.setParams({accountId : accountId});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            var responseWrap = response.getReturnValue(); 
            for(var i = 0; i < responseWrap.length; i++) {
                var caseObj = responseWrap[i];
                caseObj.caseUrl="/"+caseObj.Id;
                if(caseObj.Contact != undefined)
                caseObj.contactName = caseObj.Contact.Name;
                if(caseObj.Owner != undefined)
                caseObj.ownerName = caseObj.Owner.Name;
                if(caseObj.ProductSystem__r != undefined)
                caseObj.productName = caseObj.ProductSystem__r.Name;
            }
            //component.set('v.caseList',responseWrap);
        });
        $A.enqueueAction(action);
    },
    
    getCasedata : function(component, event) {
        debugger;
        var caseObj = component.get("v.caseObj");
        var accountId = caseObj.AccountId;
        if(accountId != undefined){
            caseObj.AccountId = accountId;
    		this.getCasedataQuery(component,accountId);
        }else{
            caseObj.AccountId = null;
        }
        component.set('v.caseObj',caseObj);
    },
    
    caseCreateHelper : function(component, event) {
        debugger; 
        var craRole;
        var craPhone;
        var craTag = component.find("isCRA");
        var isCRA = false;
        if(craTag != undefined){
            var craRole = component.find("craRole").get('v.value');
            var craPhone = component.find("craPhone").get('v.value');
            isCRA = craTag.get('v.checked');
            if(craTag.get('v.checked') && (craRole == '' || (craPhone == '' || craPhone == null))){
                component.set('v.isError',true);
                component.set('v.errorMsg','Please select CRA Role and Phone.');
                return;
            }else{
                component.set('v.isError',false);
                component.set('v.errorMsg','');
            }
        }
        
        var action = component.get("c.saveCase");
        var caseObj = component.get("v.caseObj");
        var accountId = caseObj.AccountId;
        var ipObj = component.get('v.ipObj');
        caseObj.ProductSystem__c = ipObj.Id;
        component.set('v.caseObj',caseObj);
        action.setParams({ caseData : caseObj,
                          isCRA: isCRA,
                          craRole:craRole,
                          craPhone:craPhone
                         });
        action.setCallback(this, function(response) {
       		var state = response.getState();
            var caseId = response.getReturnValue();
            console.log(state+'='+caseId);
            $A.get('e.force:refreshView').fire();
            var createContactEvent = component.getEvent("custInfoEvent");
            createContactEvent.setParams({
                "data" : caseObj
            });
            createContactEvent.fire();
        });
        $A.enqueueAction(action);
    },
    
    updateSelectedRowsHelper : function(component, event) {
        var selectedRows = event.getParam('selectedRows');  
        console.log(JSON.stringify(event));
        var setRows = [];
        for (var i = 0; i < selectedRows.length; i++){
           //alert(selectedRows[i].productName);
           //var caseObj = component.get("v.caseObj");
           //caseObj.Subject = selectedRows[i].name;
           component.find("installedProduct").set("v.value",selectedRows[i].name); 
           var ipObj = component.get('v.ipObj');
           ipObj.Id = selectedRows[i].ipId;
           ipObj.Name = selectedRows[i].name;
           component.set('v.ipObj',ipObj);
        }
        component.set("v.gridSelectedRows", setRows);
        var modal = component.find('iPComp');
        $A.util.addClass(modal,'slds-hide');
        
    },
    
    updateSelectedContactRowsHelper : function(component, event) {
        debugger;
        var selectedRows = event.getParam('selectedRows');  
        console.log(JSON.stringify(event));
        var setRows = [];
        for (var i = 0; i < selectedRows.length; i++){
           component.find("contactObjId").set("v.value",selectedRows[i].name); 
           var conObj = component.get('v.conObj');
           conObj.Id = selectedRows[i].Id;
           conObj.Name = selectedRows[i].name;
           conObj.AccountId = selectedRows[i].AccountId;
           component.set('v.conObj',conObj);
           var caseObj = component.get("v.caseObj");
           caseObj.ContactId = selectedRows[i].Id;
           component.set('v.caseObj',caseObj);
        }
        component.set("v.gridContactSelectedRows", setRows);
        var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');
        
    },
    
    handleLookupChooseEventQuickCaseHelper : function(component, event, helper) {
        try{
            debugger;
            var data = event.getParam("lookUpData");
            console.log(JSON.stringify(data));
            //alert(JSON.stringify(data));
            if(data.Id.startsWith("001")){
                //console.log(JSON.stringify(component.get('v.conObj')));
                //console.log(JSON.stringify(data));
                component.set("v.accountObj",data);
                this.getCasedataQuery(component, data.Id);
            }
            if(data.Id.startsWith("005")){
                component.set('v.groupStr','');
            }
            if(data.Id.startsWith("00G")){
                component.set('v.userStr','');
            }
            
            if(data.AccountId == undefined) return;
            //alert(JSON.stringify(data));
            //var account = component.find('accId');
            //account.set('v.value',data.AccountId);
            var caseObj = component.get("v.caseObj");
            caseObj.AccountId = data.AccountId;
            var accountObj = component.get("v.accountObj");
            accountObj.Name = data.Account_Name__c;
            accountObj.Id = data.AccountId;
            component.set("v.accountObj",accountObj);
            component.set("v.caseObj",caseObj);
            if(data.SVMXC__Company__c != undefined){
                component.find("installedProduct").set("v.value",data.Name); 
                var ipObj = component.get('v.ipObj');
                ipObj.Id = data.Id;
                ipObj.Name = data.Name;
                component.set('v.ipObj',ipObj);            
            }
            if(data.Id.startsWith("003")){
                component.set("v.conObj",data);
                component.find("contactObjId").set("v.value",data.Name); 
            }
            this.getCasedataQuery(component,data.AccountId); 
        }catch(error){
        }
    },
    
    handleQuickCaseCreateContactEventHelper : function(component, event) {
        debugger; 
        //open contact screen to create
        var sObject = event.getParam("sObjectType");
        var actionType = event.getParam("actionType");
        //alert(sObject+'='+actionType);
        var modal = component.find('createContact');
        var caseObj = component.get("v.caseObj");
        modal.set("v.accountId",caseObj.AccountId);
        var copcon = component.find("createConID");
        debugger;
        if(actionType == 'Clean'){
            component.set('v.conObj',{}); 
            component.set('v.ipObj',{});
            component.set('v.accountObj',{});
            if(component.find("contactObjId") != undefined)
            component.find("contactObjId").set("v.value",'');
            if(component.find("installedProduct") != undefined)
            component.find("installedProduct").set("v.value",'');
            var caseObj = component.get('v.caseObj');
            caseObj.AccountId = null;
            component.set('v.caseObj',caseObj);
        }else if(actionType == 'Edit'){
            copcon.setContactId(caseObj.ContactId);
            $A.util.removeClass(modal,'slds-hide');
        }else{
            copcon.setContactId(null);
            $A.util.removeClass(modal,'slds-hide');
        }
    },
     handleCreatedContactEventHelper: function(component, event) {
        debugger;
        var data = event.getParam("contactRecord");
        var caseObj = component.get("v.caseObj");
        var conObj = component.get("v.conObj");
        caseObj.ContactId = data.Id;
        caseObj.AccountId = data.AccountId;
        conObj.Name = data.Name;
        conObj.Id = data.Id;
        conObj.AccountId = data.AccountId;
        component.set("v.caseObj",caseObj);
        component.set("v.conObj",conObj);
        component.find("contactObjId").set("v.value",data.Name);
        var modal = component.find('createContact');
        $A.util.addClass(modal,'slds-hide');
        this.getCasedataQuery(component,data.AccountId); 
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.caseList");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.caseList", data);
    },
    
    sortContactData: function (component, fieldName, sortDirection) {
        var data = component.get("v.gridContactData");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.gridContactData", data);
    },
    
    sortIpData: function (component, fieldName, sortDirection) {
        var data = component.get("v.caseList");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.caseList", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})
({   
    doInit: function(component, event, helper){
        var action = component.get("c.getCase");
        action.setParams({ caseId : component.get('v.recordId')});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            var caseObj = response.getReturnValue()
            component.set('v.caseObj',caseObj);
            var conObj = component.get("v.conObj");
            conObj.Id = caseObj.ContactId;
            conObj.Name = caseObj.Contact.Name;
            component.set('v.conObj',conObj);
            var ipObj = component.get("v.ipObj");
            ipObj.Name= caseObj.ProductSystem__r.Name;
            ipObj.Id = caseObj.ProductSystem__c;
            component.set('v.ipObj',ipObj);
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
	editCase: function(cmp, event, helper) {      
		cmp.set('v.editMode', true);    
	},     
	
    handleCustInfoEvent: function(cmp, event, helper) {     
   		cmp.set('v.editMode', false);
    },
    
	submitCase: function(cmp, event, helper) {     
   		cmp.set('v.editMode', false);
    }, 
})
({
    //check and download epot
    doInit: function(component, event, helper) {
      	var action = component.get("c.checkEpotAction");
        action.setParams({"quoteId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isValid",response.getReturnValue());
                var isvalidObj = response.getReturnValue();
                if(isvalidObj == true){
                	window.open('/apex/SR_PrepareOrderPdf?Id='+component.get("v.recordId"));
                }
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    // function called on click of Cancel and close the popup
    ok: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
})
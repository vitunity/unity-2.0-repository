({
    doInit : function(component, event, helper ) {
        var action = component.get('c.validProfile');
        console.log(' === state === ');
        //action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var obj = response.getReturnValue();
                console.log(' === state === ' + obj);
                var cmpTarget = component.find('quotesDiv');
                if(obj == 'true' || obj == true) {
                    console.log(' == adding css == true == ');
                    $A.util.addClass(cmpTarget, 'divCSS');
                    $A.util.removeClass(cmpTarget, 'hideDiv');
                } else {
                    console.log(' == adding css == false == ');
                    $A.util.removeClass(cmpTarget, 'divCSS');
                    $A.util.addClass(cmpTarget, 'hideDiv');
                }
            }
        });
        $A.enqueueAction(action);
    },
    doChatterRequest : function(component, event, helper ) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var url = '/apex/NewChatterform';
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    handleClick : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://varian.my.salesforce.com/services/oauth2/authorize?response_type=code&client_id=3MVG9y6x0357HledS1eOFVsrVIRaNEBiqRGgmWMIyTJi6lQPIQz8roEgnAb2xANeVfW01XdjUD1kUzbik2Sm5&redirect_uri=https://varian.bigmachines.com/admin/oauth2/salesforce/oauth_callback.jsp&state=/commerce/buyside/commerce_manager.jsp?bm_cm_process_id=4653759&from_hp=true&_bm_trail_refresh_=true"
        });
        urlEvent.fire();
    },
    handleMachineClick : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           // "url": "https://varian-staging.cloud.thingworx.com/Thingworx/Mashups/VMS.District.TrueBeam.Status?appKey=20a1fd58-9e3f-4b78-bd1d-8f0d1a8cad19&x-thingworx-session=true"
            "url": "/apex/CustomLinkTracking?app=MHD"    
        });
        urlEvent.fire();
    
    }
})
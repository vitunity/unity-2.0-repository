({
	getRecords : function(cmp, evt, helper) {
        console.log('-----getRecords');
        var showNextRecords = false;
        var showPreviousRecords = false;
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords);
    },
    nextRecords:function(cmp,event,helper){
        var showNextRecords = true;
        var showPreviousRecords = false;
        var offset = cmp.get("v.offst");
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords,offset); 
    },
    previousRecords:function(cmp,event,helper){
        var showNextRecords = false;
        var showPreviousRecords = true;
        var offset = cmp.get("v.offst");
        console.log('----offset previous'+offset);
        helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords,offset); 
    },
    
    onSearchKeyUp : function(cmp,event,helper){
    	var searchString = cmp.get("v.searchValue");
        if(searchString && searchString.length >= 3){
        	var showNextRecords = false;
            var showPreviousRecords = false;
            helper.getObjectRecords(cmp,showNextRecords,showPreviousRecords);
        }
	},
    
    sendSelectedValue : function(cmp, evt, helper){
        var setSelectedRecordEvent = cmp.getEvent("recordSelected");
        
        setSelectedRecordEvent.setParams({"recordName": evt.getSource().get("v.label")});
        setSelectedRecordEvent.setParams({"recordId": evt.getSource().get("v.target")});
        setSelectedRecordEvent.setParams({"recordIdField": cmp.get("v.fieldToSetRecordId")});
        setSelectedRecordEvent.setParams({"recordNameField": cmp.get("v.fieldToSetRecordName")});
        console.log('----partnerSelectedEvent'+setSelectedRecordEvent);
        setSelectedRecordEvent.fire();
    }
})
({
	
    doInit : function(component, event, helper) {
        // Get a reference to the bigmachines Quote function defined in the Apex controller
        console.log('---partners section');
		helper.loadPartners(component);
    }, 
    
    nextRecords : function(component, event, helper) {
		
	},
    
    previousRecords : function(component, event, helper) {
		
	},
    
    partnerChange : function(component, event, helper){
        console.log("Old value: " + event.getParam("oldValue"));
        console.log("Current value: " + event.getParam("value"));
        helper.loadPartners(component);
    },
    
    sendSelectedPartner : function(component, event){
    	var partnerNumber = event.getSource().get("v.label");
        var partnerSelectedEvent = component.getEvent("partnerSelected");
        console.log('----partnerSelectedEvent'+partnerSelectedEvent);
        partnerSelectedEvent.setParams({"partnerNumber": partnerNumber});
        partnerSelectedEvent.setParams({"partnerFunction": component.get("v.partnerFunction_LP")});
        partnerSelectedEvent.fire();
	}
})
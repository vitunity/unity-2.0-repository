({
	
    loadPartners : function(lookupComponent){
         this.showSpinner(lookupComponent, true);
    	var action = lookupComponent.get("c.initializeLookupPartners");
        action.setParams({
            "partnerFunction": lookupComponent.get('v.partnerFunction_LP'),
            "soldToNumber": lookupComponent.get('v.soldToNumber_LP'),
            "salesOrg": lookupComponent.get('v.salesOrg_LP'),
            "searchText": lookupComponent.get('v.searchText_LP')
        });
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                //helper.checkPartners(returnValue);
                var looupPartnerObject = JSON.parse(JSON.stringify(returnValue));
                console.log('---partners'+looupPartnerObject);
                lookupComponent.set("v.lookupPartner", looupPartnerObject);
            }
            this.showSpinner(lookupComponent, false);
        });
        // Invoke the service
        $A.enqueueAction(action);
	},
    
    showSpinner : function(cmp, isLoading){
        var cmpTarget = cmp.find('statusSpinner');
        if(isLoading){
            $A.util.addClass(cmpTarget, 'slds-show');
        	$A.util.removeClass(cmpTarget, 'slds-hide');
    	}else{
			$A.util.addClass(cmpTarget, 'slds-hide');
        	$A.util.removeClass(cmpTarget, 'slds-show');
		}    
	}
})
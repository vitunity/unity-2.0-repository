({
	redirectToPrepareOrder : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/Lightning_PrepareOrder?quoteId="+component.get("v.recordId")
        });
        urlEvent.fire();
	}
})
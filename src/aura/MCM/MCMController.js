({
    handleComponentEvent : function(component, event) {
        component.set("v.reviewProductId",event.getParam("reviewProductId"));
        component.set("v.inputClearanceId",event.getParam("inputClearanceId"));
        component.set("v.marketClearance", false);
    },
    back : function(component, event) {
        component.set("v.marketClearance", true);
    }
})
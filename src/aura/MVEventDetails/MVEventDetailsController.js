({
    doInit : function(component, event, helper) {        
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getEventDetails(component, event);
    },
    
    home: function(component, event, helper) {
         helper.goHome(component, event);
    }
    
})
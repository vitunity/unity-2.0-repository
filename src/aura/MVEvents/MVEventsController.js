({
    doInit : function(component, event, helper) 
    { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getUpcomingEvent(component, event);
    },

    recentEvent : function(component, event, helper) 
    { 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvrecentevents?lang="+component.get('v.language')
        });
        urlEvent.fire();
    },
    
    openEventDetail : function(component, event, helper) { 
        var urlEvent = $A.get("e.force:navigateToURL");
        var eventId = event.currentTarget.getAttribute("data-craid");
        urlEvent.setParams({
            "url": "/eventdetail?lang="+component.get('v.language')+"&Id="+eventId
        });
        urlEvent.fire();
    },
    
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
})
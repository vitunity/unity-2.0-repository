({
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang = getUrlParameter('lang');
        component.set("v.language",lang);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    getUpcomingEvent : function(component,event) 
    {        
        var self = this;
        var action = component.get("c.getUpcomingEvents");
        var options = [];
        action.setCallback(this, function(response) 
    	{            
            var state = response.getState();
            if(state == "SUCCESS"){
                var events = response.getReturnValue();
                console.log(events);
                for(var i =0;i<events.length;i++)
                {
                    options.push({
                        strMnthDate : events[i].strMnthDate,
                        value : events[i].listEvents
                    });
                }                
            }
            component.set("v.upcomingEventsMap",options);
        });	
        $A.enqueueAction(action); 
    },
})
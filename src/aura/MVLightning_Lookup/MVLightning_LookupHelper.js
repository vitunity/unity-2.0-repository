({
    
    getObjectRecords : function(cmp,next,prev,offset) {
        console.log('----received offset'+off);
        this.showSpinner(cmp, true);
        var off = offset || 0;
        console.log('----passing offset'+off);
        var lookupController = new Object();
        lookupController.hasPrev = prev;
        lookupController.hasNext = next;
        lookupController.objectFields = cmp.get("v.fieldNames");
        var objectFieldLabels = cmp.get("v.fieldLabels");
        lookupController.defaultFieldName = cmp.get("v.defaultField");
        lookupController.defaultFieldValue = cmp.get("v.defaultValue");
        lookupController.offset = off;
        lookupController.sObjectName = cmp.get("v.objectName");
        lookupController.searchText = cmp.get("v.searchValue");
        lookupController.pageSize = cmp.get("v.recordsPerPage");
        console.log('----lookupController.searchText'+lookupController.searchText+'----'+(!lookupController.searchText));
        console.log('----lookupController.objectFields'+lookupController.objectFields+'----'+lookupController.objectFields.length);
        var action = cmp.get("c.initializeLookupController");
        action.setParams({
            "lookupControllerString" : JSON.stringify(lookupController)     
        });
        action.setCallback(this,function(res){
            var state = res.getState();            
            if(state=="SUCCESS"){
                //document.getElementById("tableBody").innerHTML ='';
                var result = res.getReturnValue();
                cmp.set('v.offst',result.offset);
                console.log('---'+JSON.stringify(result.objectRecords));
                cmp.set('v.sObjectRecords',result.objectRecords);
                
                var retRecords = result.objectRecords;
                var tableBodyComponent = cmp.find("tableBody");
                tableBodyComponent.set("v.body", []);
				var tableBody = tableBodyComponent.get("v.body");
                var newComponents = [];
                var columnCounter = 0;
                var matchingRecords = [];
                retRecords.forEach(function(s) {
                    var stringRecord = JSON.stringify(s).toLowerCase();
                    
                    console.log("-----stringRecord---1--"+stringRecord+"---"+lookupController.searchText);
                    console.log("-----stringRecord---2--"+stringRecord.includes(lookupController.searchText));
                    if((lookupController.searchText != "" && stringRecord.includes(lookupController.searchText.toLowerCase())) || lookupController.searchText == ""){
                        matchingRecords.push(s);
                        //var tableRow = document.createElement('tr');
                        newComponents.push(["aura:html", {
                            "tag": "tr"
                        }]);
                        columnCounter = 0;
                        lookupController.objectFields.forEach(function(field){
                            var className = '';
                            if(field == 'Email'){
                                className = 'wordBreakClass';
                            }
                            newComponents.push(["aura:html", {
                                "tag": "td",
                                "HTMLAttributes": {
                                    "class" : className
                                }
                            }]);
                            if(field == 'Name'){
                                newComponents.push(["ui:outputURL", {
                                    "label": s[field],
                                    "iconClass": s['Id'], 
                                    "value":"",
                                    "click": cmp.getReference("c.sendSelectedValue")
                                }]);
                            }else{
                                if(field.includes(".")){
                                    var referenceFieldvalues = field.split(".");
                                    var parentObjectName = referenceFieldvalues[0];
                                    var parentObjectValue = referenceFieldvalues[1];
                                    var parentValue = "";
                                    if(s[parentObjectName]){
                                        parentValue = s[parentObjectName][parentObjectValue];
                                    }
                                    newComponents.push(["ui:outputText",{
                                        "value" : parentValue
                                    }]);
                                }else{
                                    var fieldValue = "";
                                    if(s[field]){
                                        fieldValue = s[field];
                                    }
                                    newComponents.push(["ui:outputText",{
                                        "value" : fieldValue
                                    }]);
                                    //console.log('-----field'+field+"---"+s[field]);
                                }
                            }
                            columnCounter = columnCounter + 1;                        
                        });
                	}
                 });
                //console.log('----newComponents'+JSON.stringify(newComponents));
                var rowCounter = 0;
                columnCounter = 1;
                var numberOfColumns = lookupController.objectFields.length;
                
                $A.createComponents(
                    newComponents,
                    function(components, status, errorMessage){
                        console.log('-----status--'+status);
                        if (status === "SUCCESS") {
                        	matchingRecords.forEach(function(s) {
                                var tr = components[rowCounter];
                                var trBody = components[rowCounter].get("v.body");
                                lookupController.objectFields.forEach(function(field){
                                    //if(field != 'Id'){
                                        var td = components[columnCounter];
                                        columnCounter = columnCounter + 1;
                                        var tdContent = components[columnCounter];
                                        columnCounter = columnCounter + 1;
                                        //console.log('----createComponentSuccess--'+columnCounter);
                                        var tdBody = td.get("v.body");
                                        tdBody.push(tdContent);
                                        td.set("v.body", tdBody);
                                        trBody.push(td);
                                    //}
                                });
                                tr.set("v.body", trBody);
                                tableBody.push(tr);
                                //console.log('----rowCounter--'+rowCounter);
                                rowCounter = columnCounter;
                                columnCounter = rowCounter + 1;
                            });
                            tableBodyComponent.set("v.body", tableBody);
                        }else if (status === "INCOMPLETE") {
                            console.log("No response from server or client is offline.")
                        }
                            else if (status === "ERROR") {
                                console.log("Error: " + JSON.stringify(errorMessage));
                        }
                    }
                );
                console.log('----next'+result.hasNext+'----prev'+result.hasPrev+'---off'+result.offset);
                cmp.set('v.next',result.hasNext);
                cmp.set('v.prev',result.hasPrev);
                cmp.set('v.offst',result.offset);
                cmp.set('v.totalNumberOfPages',result.totalPages);
                this.showSpinner(cmp, false);
            }
        });        
        $A.enqueueAction(action);
    },
    
    showSpinner : function(cmp, isLoading){
        var cmpTarget = cmp.find('statusSpinner');
        if(isLoading){
            $A.util.addClass(cmpTarget, 'slds-show');
        	$A.util.removeClass(cmpTarget, 'slds-hide');
    	}else{
			$A.util.addClass(cmpTarget, 'slds-hide');
        	$A.util.removeClass(cmpTarget, 'slds-show');
		}    
	}
})
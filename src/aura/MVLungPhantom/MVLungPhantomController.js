({
    /* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getLungPhantRecs(component, event);
    },
    /* redirect to another page */
    handleVoucherClick : function(component, event, helper){
        helper.handleVoucherClick(component,event);
    },
    
    /* Save the data */
    savePhantom : function(component, event, helper){
        helper.savePhantom(component,event);
    },
    toggleTabs : function(component, event, helper){
        helper.doToggleTabs(component,event);
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },    
})
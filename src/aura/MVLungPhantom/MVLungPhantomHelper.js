({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
                $("#tab_b").hide();
            	$("#tab_a").show();
                $("#Voucher").hide();
                $("#PhantomText").hide();
                $('.loading').hide();
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Load data */
    getLungPhantRecs : function(component, event) {
        debugger;
        var action = component.get("c.getlLPHRec");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var rpcLngRecords = response.getReturnValue();
                for(var i = 0; i < response.getReturnValue().length; i++) {
                    if( rpcLngRecords[i].Date_Received__c != undefined){
                        var dt = rpcLngRecords[i].Date_Received__c.split('-',-2);
                    	rpcLngRecords[i].Date_Received__c = dt[1]+'/'+dt[2]+'/'+dt[0];
                    }
                	
                    //alert(rpcLngRecords[i].Date_Received__c);
                }
                component.set("v.lstLungPhantRecs",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /* Load data */
    savePhantom : function(component, event) {
        var rpcPhantomRecords = component.get("v.lstLungPhantRecs");
        var dates = $(".datepickerRPC");
        for(var i=0; i<dates.length; i++){
            var element = dates.eq(i);
            //alert($( element ).parent().attr( "id" ));
            var recordIndex = $( element ).parent().attr( "id" );
            var record = rpcPhantomRecords[recordIndex];
            var dt = $(element).val();
            var dtArray = dt.split("/",-2);
            record.Date_Received__c = dtArray[2]+'-'+dtArray[0]+'-'+dtArray[1];
            //alert($( element ).parent().attr( "id" ));
            alert(record.Date_Received__c);
        }
        var action = component.get("c.saveLungPhantomRec");
        action.setParams({"lungPhantRecs":component.get("v.lstLungPhantRecs")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //component.set("v.lstLungPhantRecs",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    handleVoucherClick : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var lstLungPhantRecs = component.get("v.lstLungPhantRecs");
            if(!$A.util.isEmpty(lstLungPhantRecs)){
                var rec = lstLungPhantRecs[index];
                if($A.util.isEmpty(rec.Redeemed_By__c)){
                    var recId = rec.Id;
                    this.setConfirmPopup(component, event, recId);                    
                    url = "http://rpc.mdanderson.org/mdadl/Varian_Phantom_Request.aspx";
                }else{
                    url = "http://rpc.mdanderson.org/mdadl/Varian_Phantom_Request.aspx";
                }
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": url,
                    isredirect: "true"
                });
                urlEvent.fire();
            }
        }
    },
    setConfirmPopup : function(component, event, voucherID) {
        var action = component.get("c.confirmPopup");
        action.setParams({"voucherID":voucherID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lstLungPhantRecs",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    doToggleTabs: function(component, event) {
        var index = event.currentTarget.id;
        if(index  == 'Voucher'){
            $("#tab_b").hide();
            $("#tab_a").show();
            $("#Voucher").hide();
            $("#PhantomText").hide();
            $("#Phantom").show();
            $("#VoucherText").show();
        }else if(index  == 'Phantom'){
            $("#tab_b").show();
            $("#tab_a").hide();
            $("#Phantom").hide();
            $("#PhantomText").show();
            $("#VoucherText").hide();
            $("#Voucher").show();
            
        }
    }
    
    
})
({
	
    /* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getLungPhantRecs(component, event);
    },
    
    /* Page load method. It will load data in intial load.*/
    search : function(component, event, helper) {
        helper.getLungPhantRecs(component, event);
    },
    /* Page load method. It will load data in intial load.*/
    searchOpen : function(component, event, helper) {
        component.set('v.tab','open');
        helper.getLungPhantRecs(component, event);
    },
    /* Page load method. It will load data in intial load.*/
    searchClosed : function(component, event, helper) {
        component.set('v.tab','closed');
        helper.getLungPhantRecs(component, event);
    },
    /* Save the data */
    savePhantom : function(component, event, helper){
        helper.savePhantom(component,event);
    }
})
({
	/* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /* Load data */
    getLungPhantRecs : function(component, event) {
        var action = component.get("c.getlLPHRec");
        action.setParams({"searchKeyWord":component.get('v.searchText'),
                          "tab":component.get('v.tab')});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(JSON.stringify(response.getReturnValue()));
                var lstLungPhantRecs = response.getReturnValue();
                for(var i = 0; i < lstLungPhantRecs.length; i++) {
                	var dt = lstLungPhantRecs[i].dateExposed;
                    if(dt != undefined){
                        var dta = dt.split('-',-2);
            			lstLungPhantRecs[i].dateExposed = dta[1]+'/'+dta[2]+'/'+dta[0];
                    }
                    var dt2 = lstLungPhantRecs[i].dateResults;
                    if(dt2 != undefined){
                        var dta2 = dt2.split('-',-2);
            			lstLungPhantRecs[i].dateResults = dta2[1]+'/'+dta2[2]+'/'+dta2[0];
                    }
                }
                component.set("v.lstLungPhantRecs",lstLungPhantRecs);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /* Load data */
    savePhantom : function(component, event) {       
        debugger;
        var rpcPhantomRecords = component.get("v.lstLungPhantRecs");
        var dates = $(".datepickerMDADSDT");
        
        for(var i=0; i<dates.length; i++){
            var element = dates.eq(i);
            //alert($( element ).parent().attr( "id" ));
            var recordIndex = $( element ).parent().attr( "id" );
            var record = rpcPhantomRecords[recordIndex];
            var dt = $(element).val();
            if(dt != ''){                
                var dtArray = dt.split("/",-2);
                record.dateExposed = dtArray[2]+'-'+dtArray[0]+'-'+dtArray[1];
                record.lung.Date_Exposed_Phantom_Received_by_MDADL__c = record.dateExposed;
                //alert($( element ).parent().attr( "id" ));
                //alert(record.lung.Date_Exposed_Phantom_Received_by_MDADL__c);
            }
        }
        var dates2 = $(".datepickerMDADEDT");
        for(var i=0; i<dates2.length; i++){
            var element = dates2.eq(i);
            //alert($( element ).parent().attr( "id" ));
            var recordIndex = $( element ).parent().attr( "id" );
            var record = rpcPhantomRecords[recordIndex];
            var dt = $(element).val();
            if(dt != ''){
                var dtArray = dt.split("/",-2);
                record.dateResults = dtArray[2]+'-'+dtArray[0]+'-'+dtArray[1];
                record.lung.Date_Results_Reported_by_MDADL__c = record.dateExposed;
                //alert($( element ).parent().attr( "id" ));
                //alert(record.lung.Date_Results_Reported_by_MDADL__c);
            }
        }
        var rpcUpdate = [];
        for(var i=0; i<rpcPhantomRecords.length; i++){
        	rpcUpdate.push(rpcPhantomRecords[i].lung);
        }
        //debugger;
        var action = component.get("c.saveLungPhantomRecNew");
        //JSON.parse(JSON.stringify(component.get("v.lstLungPhantRecs")))
        action.setParams({"wLungPhantListNew":rpcUpdate});
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                alert('Saved');
                //component.set("v.lstLungPhantRecs",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
})
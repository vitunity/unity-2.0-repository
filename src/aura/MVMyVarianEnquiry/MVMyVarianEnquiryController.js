({
    doInit : function(component, event, helper) {
         helper.getCustomLabels(component, event);
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(response) {
            var user = response.getReturnValue();
            //console.log('== + '+ user.Id);
            if(user == true){
            	component.set("v.isAuthenticated", true);
                component.set("v.isNotAuthenticated", false);
            }else{
                component.set("v.isAuthenticated", false);
                component.set("v.isNotAuthenticated", true);
            }
        })
        $A.enqueueAction(action);
        helper.getCountry(component, event);
       
    },
    /* Toogle the page based on logged in user */
    toggle : function(component, event, helper) {
    
     var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      helper.resetValidation(component,event);
      var counter = 0;
      var fName = component.find("firstNameId").get("v.value");
      var lName = component.find("lastNameId").get("v.value");
      var instiVar = component.find("institutionId").get("v.value");
      var telVar = component.find("telephoneId").get("v.value");
      var addressVar = component.find("addressId").get("v.value");
      var cityVar = component.find("cityId").get("v.value");
      var stateVar = component.find("stateId").get("v.value");
      var postalCodeVar = component.find("postalCodeId").get("v.value");
      var cEmail = component.find("emailId").get("v.value");
      	if(fName == undefined || fName == null || fName == ""){
        	  component.set("v.firstNameId", "true");
              counter=1;
              
         }if(lName == undefined || lName == null){
              component.set("v.lastNameId", "true");
              counter=1;
              
         }if(instiVar == undefined || instiVar == null){
              component.set("v.institutionId", "true");
              counter=counter+1;
          }if(telVar == undefined || telVar == null){
              component.set("v.telephoneId", "true");
              counter=1;
              
          }if(cityVar == undefined || cityVar == null){
              component.set("v.cityId", "true");
              counter=1;
              
          }if(stateVar == undefined || stateVar == null){
              component.set("v.stateId", "true");
              counter=1;
              
          }if(postalCodeVar == undefined || postalCodeVar == null){
              component.set("v.postalCodeId", "true");
              counter=1;
              
          }if(cEmail == undefined || cEmail == null ||  !cEmail.match(regExpEmailformat)){
          		component.set("v.emailId", "true");
              counter=1;
          
          }if(addressVar == undefined || addressVar == null){
          		component.set("v.addressId", "true");
              counter=1;
          
          }if(counter==0){
          
        component.set("v.showMe","false"); 
        component.set("v.showMe1","True");
        
  		}
  },
  /* Send email to user with attachment */  
  sendEmailContact: function(component, event, helper) { 
        
      //MAX_FILE_SIZE: 750 000
        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];
    	var fr = new FileReader();
    	var self = this;
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            helper.upload(component,file,fileContents);
                        
        };
    	fr.readAsDataURL(file);
    
    },
     
    onChangeCountry: function(component, event, helper) {
        helper.setCountry(component, event);
    }

})
({
    /* Upload file attachment */
	upload:function(component,file,fileContents){
        debugger;
		var registerMData = component.get("c.registerMData");
            var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");
            var subj = component.get("v.mvSubject");
            var longD = component.get("v.mvLongDesc");
            registerMData.setParams({
                "registerDataObj": rdata,
                "accDataObj": adata,
                "subject": subj,
                "longDes": longD,
                "fileName": file.name,
                "base64Data": fileContents,
                "contentType": file.type,
                "languageObj": component.get("v.language")
    
            });
        	var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
                var state = response.getState();
                $A.util.toggleClass(component.find("afterEnquirySubmit"), 'slds-hide');
                $A.util.toggleClass(component.find("enquirySubmit"), 'slds-hide');
            });
            $A.enqueueAction(registerMData);
        	urlEvent.setParams({
                "url": "/my-varian-enquiry"
            });
            component.set("v.mvSuccess","true");
        	component.set("v.mvSubject", "");
        	component.set("v.mvLongDesc", "");
        	component.find("file").getElement().value='';
        	//wait(1000);
        	
        	//urlEvent.fire();
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Reset validations on page reload */
    resetValidation : function(component, event){ 
        
        component.set("v.firstNameId", "false");
        component.set("v.lastNameId", "false");
        component.set("v.emailId", "false");
        component.set("v.telephoneId", "false");
        component.set("v.addressId", "false");
        component.set("v.institutionId", "false");
        component.set("v.cityId", "false");
        component.set("v.stateId", "false");
        component.set("v.postalCodeId", "false");

        
    },
    /* Load country picklist values */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var productGroup = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.accData.Country1__c"))
                    {
                        productGroup.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", productGroup);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action); 
    },
    
    setCountry: function(component, event) 
    {
		 var selectCmp = component.find("country");
         component.set("v.registerData.MailingCountry", selectCmp.get("v.value"));
	}
    
})
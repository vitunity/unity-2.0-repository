({
  doInit: function(component, event, helper) {
       // debugger;
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
    },

    loadPreLoginContent : function (component,event, helper)
    {
        helper.getOnDemandWebinars(component, event);
        helper.loadFeaturedStories(component, event);
        //helper.getUser(component, event);
        helper.getSalutation(component, event);      
        helper.getPreferredLanguage(component, event);
        helper.getProfessionalRole(component, event);
        helper.getSpecialty(component, event);
        helper.getAccountType(component, event);
        helper.getCountry(component, event);
        helper.initialiseCaptcha(component, event);
    },
    /* Login action*/
    handleLoginClick: function(component, event, helper) {
       helper.customLogin(component, event);
    },
    /* Register action*/
    handleRegisterClick: function(component, event, helper) {
        var url="/registration";
        helper.navigateToURL(component, event,url);
    },
    /* Forgot Password action*/
    handleForgoPasswordClick: function(component, event, helper) {
        var url="/forgot-password";
        helper.navigateToURL(component, event,url);
    },
    /* open Oncopeer Community Link*/
    openOncopeerCommunityLink: function(component, event, helper) {
        //var url="https://www.oncopeer.com/";
        //var url="https://varian.force.com/OCSUGC";
        var url="http://www.oncopeer.com";
        helper.navigateToURL(component, event,url);
    },
    /* open Certificate Manager Link*/
    openCertificateManagerLink: function(component, event, helper) {
        debugger;
        //var url="https://webapps.varian.com/certificate/en";
        var url = component.get("v.customLabelMap.MV_CertificateManger_Guest_URL");
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Marketplace Link*/
    openVarianMarketplaceLink: function(component, event, helper) {
        var url="https://sfqa1-varian.cs77.force.com/vMarketLogin";
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Medical affairs Link*/
    openVarianMedicalaffairsLink: function(component, event, helper) {
        var url="http://medicalaffairs.varian.com/";
        helper.navigateToURL(component, event,url);
    },
    /* open All Webinars  Link*/
    openWebinarsLink: function(component, event, helper) {
        var url="/webinars";
        helper.navigateToURL(component, event,url);
    },
     /* open Webiars Link*/
    openWebinarLink: function(component, event, helper) {
         helper.openWebinarLink(component, event);     
    },
    /* open Centerline Link*/
    openCenterlineLink: function(component, event, helper) {
        var url="https://www.varian.com/oncology/events-resources/centerline";
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Medical affairs Link*/
    openSportonLink: function(component, event, helper) {
        var url="https://www.varian.com/proton-therapy/spot-blog/home";
        helper.navigateToURL(component, event,url);
    },
    /* open Featured Story Link*/
    openFeaturedStoryLink: function(component, event, helper) {
         helper.openFeaturedStoryLink(component, event);     
    },
    /* Reset captcha */
    resetCaptcha: function(component, event, helper){
      helper.initialiseCaptcha(component, event);
        component.set("v.captchaData", "");
    },
  
    /* set functional location */
    onChangeRole: function(component, event, helper){
      helper.setRole(component, event);
    },
  
    /* set preferred language */
    onChangeLanguage: function(component, event, helper){
      helper.setLanguage(component, event);
    },
    
    /* set speciality value */
    onUserNameChange: function(component, event, helper){
      helper.setUserNameChange(component, event);
    },

    onUserNameChangeEmail: function(component, event, helper){
        helper.setUserNameChangeEmail(component, event);
        helper.validateDupEmail(component, event);
    },

    onUserNameChangeEmailMgr: function(component, event, helper){
        helper.setUserNameChangeEmailMgr(component, event);
    },

    /* set speciality value */
    onChangeSpeciality: function(component, event, helper){
      helper.setSpeciality(component, event);
    },
    
    onChangeAccountType: function(component, event, helper){
      helper.setAccountType(component, event);
    },
  
    /* set contact salutation */
    onChangeSalutation: function(component, event, helper){
      helper.setSalutation(component, event);
    },
  
    /* set country value */
    onChangeCountry: function(component, event, helper){
      helper.setCountry(component, event);
    },
  
    /* toggle sections based on logged in user */
    validate : function(component, event, helper) {
      
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var counter = 0;
        var fName = component.find("firstNameId").get("v.value");
        var lName = component.find("lastNameId").get("v.value");
        var titleVar = component.find("titleId").get("v.value");
        var telVar = component.find("telephoneId").get("v.value");
        var faxVar = component.find("faxId").get("v.value");
        var cEmail = component.find("conEmail").get("v.value");
        var sal = component.find("Salutation").get("v.value");
        var roleVar = component.find("Role").get("v.value");
        var specialtyVar = component.find("Specialty").get("v.value");
        var langVar = component.find("Language").get("v.value");
        var showContact = false;
        
        var action = component.get('c.showContact');
        
        action.setParams({
        "conPresentemail": cEmail
        });
        action.setCallback(this,function(response){
        showContact = response.getReturnValue();
        if(showContact == true){
          counter=1;
            debugger;
            component.set("v.varianError","false")
            component.set("v.alreadyRegistertedError","true");
            component.set("v.registerData.Email","");
            component.startMyPopUp();
            component.set("v.showMe","True");
            component.set("v.showMe1","false");
            component.set("v.alreadyRegistertedError","false");
        }if(cEmail == null || !cEmail.match(regExpEmailformat)){
            debugger;
            counter = 1;
            component.set("v.conEmail", "true");
          
        }if(cEmail.includes("@varian.com") ) {
          debugger;
            counter=1;
            component.set("v.alreadyRegistertedError","False");
            component.set("v.varianError","true");
            component.set("v.registerData.Email","");
            component.startMyPopUp();
            component.set("v.showMe","True"); 
            component.set("v.showMe1","false");
            //component.set("v.varianError","false");

              
        }
        if(fName == undefined || fName == null){
          counter = 1;
            component.set("v.firstNameId", "true");
              
        }if(lName == undefined || lName == null){
          counter=1;
            component.set("v.lastNameId", "true");
              
        }if(titleVar == undefined || titleVar == null){
            counter=1;
            component.set("v.titleId", "true");
        }if(telVar == undefined || telVar == null){
            counter=1;
            component.set("v.telephoneId", "true");
            
        }if(faxVar == undefined || faxVar == null){
            counter=1;
            component.set("v.faxId", "true");
            
        }if(sal == undefined || sal == null){
            counter=1;
            component.set("v.salutationId", "true");
        }if(langVar == undefined || langVar == null){
            counter=1;
            component.set("v.languageId", "true");
        }if(specialtyVar == undefined || specialtyVar == null){
            counter=1;
            component.set("v.specialtyId", "true");
        }if(roleVar == undefined || roleVar == null){
            counter=1;
            component.set("v.roleId", "true");
        }
        else if(counter==0){
          component.set("v.showMe","false");
            component.set("v.showMe1","True"); 
        }
        });
        $A.enqueueAction(action);
    },
    /* Reset captcha */
  toggleCaptcha : function(component, event, helper) {
      var counter = 0;
        var instiVar = component.find("institutionId").get("v.value");
        var addVar = component.find("institutionAddId").get("v.value");
        var cityVar = component.find("institutionCityId").get("v.value");
        var stateVar = component.find("institutionStateId").get("v.value");
        var countryVar = component.find("country").get("v.value");
        var zipVar = component.find("institutionZipId").get("v.value");
        var managerVar = component.find("managerId").get("v.value");
        var mEmailVar = component.find("managerEmailId").get("v.value");
      
        if(instiVar == undefined || instiVar == null){
          counter=1;
            component.set("v.institutionId", "true");
              
        }if(addVar == undefined || addVar == null){
            counter=1;
            component.set("v.institutionAddId", "true");
              
        }if(cityVar == undefined || cityVar == null){
            counter=1;
            component.set("v.institutionCityId", "true");
        }if(stateVar == undefined || stateVar == null){
            counter=1;
            component.set("v.institutionStateId", "true");
              
        }if(countryVar == undefined || countryVar == null){
            counter=1;
            component.set("v.country", "true");
              
        }if(zipVar == undefined || zipVar == null){
            counter=1;
            component.set("v.institutionZipId", "true");
        }if(managerVar == undefined || managerVar == null){
            counter=1;
            component.set("v.managerId", "true");
        } if(mEmailVar == undefined || mEmailVar == null){
            counter=1;
            component.set("v.managerEmailId", "true");
        }else if(counter==0){
          var Canada = component.find("isCanada").get("v.value");
            var country1 = component.find("country").get("v.value");
            if(country1 =='Canada' && Canada == false) 
            {
              component.startCanadaPopUp();
                component.set("v.isCanada","true"); 
            }
            else{      
              component.set("v.showMe1","false");
                component.set("v.showCaptcha","True");         
            }
        }
  },
    
   startMyPopUp : function(component, event, helper) {
      var cmpTarget = component.find('popUpHandle');
        $A.util.removeClass(cmpTarget, 'dntShowMeClass');
        $A.util.addClass(cmpTarget, 'showMeClass');
    },
    /* canada pop up handler */
    startCanadaPopUp : function(component, event, helper) {
        var cmpTarget = component.find('canadaPopUpHandle');
        $A.util.removeClass(cmpTarget, 'dntShowMeClass1');
        $A.util.addClass(cmpTarget, 'showMeClass');
    },
  
    handleMe : function(component, event, helper) {
      var cmpTarget = component.find('popUpHandle');
        $A.util.removeClass(cmpTarget, 'showMeClass');
        $A.util.addClass(cmpTarget, 'dntShowMeClass');
    },
    /* Show pop up if not canadian user */
    handleMeCanadaNo : function(component, event, helper) {
      var cmpTarget = component.find('canadaPopUpHandle');
        component.set("v.consentForCanada","false");
        component.set("v.isCanada","true");
        $A.util.removeClass(cmpTarget, 'showMeClass');
        $A.util.addClass(cmpTarget, 'dntShowMeClass');
    },
     /* Show pop up if the user is a canadian user */
    handleMeCanadaYes : function(component, event, helper) {
        var cmpTarget = component.find('canadaPopUpHandle');
        component.set("v.consentForCanada","true");
        component.set("v.isCanada","true");
        $A.util.removeClass(cmpTarget, 'showMeClass');
        $A.util.addClass(cmpTarget, 'dntShowMeClass');
    },
  
    /* Save contact data */
    callMyVarianRegister: function(component,event,helper) { 
        //Set to default - errors
        component.set("v.captchaError", "false");

        //Validate captcha error
        var checkVal = helper.validateCaptcha(component,event);
        //alert(checkVal);
        if(checkVal){
            var registerMData = component.get("c.registerMData");

            var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");

            registerMData.setParams({
              "registerDataObj": rdata,
              "accDataObj": adata
            });
          
            var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
              var state = response.getState();
              //alert('state = ' + state);
              $('#register-1').modal('hide');
                if(component.isValid() && state === "SUCCESS") { 
                    var rtnVal = response.getReturnValue();
                    //alert('rtnVal = ' + rtnVal);

                    if(rtnVal === 'Error Found') {
                      $('#reg-error-1').modal('show');
                    } else {
                      $('#reg-complete-1').modal('show');
                    }
                }else if (response.getState() == "ERROR") {
                    alert('state is error');
                    $A.log("callback error", response.getError());
                    component.set("v.regError",response.getError());
                    //alert(response.getError());
                    $('#reg-error-1').modal('show');
                }
            });
            $A.enqueueAction(registerMData);
          
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "/registration-completed"
            });
            
            
            //urlEvent.fire();      
        }else{
            component.set("v.captchaError", "true");
            component.set("v.captchaData", "");
        }
  },
    /* Check for canada user  */
    callMyVarianRegisterCanada: function(component,event,helper) {
      //Set to default - errors
      component.set("v.captchaError", "false");
      //Validate captcha error
      var checkVal = helper.validateCaptcha(component,event);
      debugger;
      if(checkVal){
          debugger;
          var sendNotification = component.get("c.sendNotification"); 
          debugger;
          var rdata = component.get("v.registerData");
          debugger;
          var adata = component.get("v.accData");
          debugger;
          var consent = component.get("v.consentForCanada");
      debugger;           
          sendNotification.setParams({
              "registerDataObj": rdata,
              "accDataObj": adata,
              "ConsentAction": consent
          });
          
          var urlEvent = $A.get("e.force:navigateToURL");
          sendNotification.setCallback(this, function(response) {
              var state = response.getState(); 
          });
          $A.enqueueAction(sendNotification);
          
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
              "url": "/vhome"
          });
          urlEvent.fire();      
      }else{
          component.set("v.captchaError", "true");
          component.set("v.captchaData", "");
      }
  },
          
  callFunction : function(component,event,helper){
        var selectedCountry = component.get("v.registerData.MailingCountry"); 
        var japanAgrChkVal;
        if(component.find("japanAgrId") != undefined) {
            japanAgrChkVal = component.find("japanAgrId").get("v.value");
            var jpnDivId = component.find("jpnDivId");
            if(selectedCountry == 'Japan' && japanAgrChkVal == false) {
                $A.util.addClass(jpnDivId , 'errorClassChk');
            } else {
                //alert('there');
                $A.util.removeClass(jpnDivId, 'errorClassChk');
            }
        }

        
        if(helper.validateRequiredFieldsNew(component,event) == true 
              || component.get("v.showError") == true
              || (selectedCountry == 'Japan' && japanAgrChkVal == false)
           )
        {
          //alert('return called');
          return;   
        }
        /*
        if(helper.validateRequiredFields(component,event) == true ||
                component.get("v.showError") == true){
          return;   
        } */

        component.callMyVarianRegister();
    },
     
    forgotPassClear: function(component,event,helper){
      helper.doFrgotPassClear(component,event);
    },
    
    forgotPass: function(component,event,helper){
        component.set("v.captchaError", "false");
        var checkCaptha = helper.validateCaptcha(component,event);
        if(checkCaptha){
            var checkVal = helper.checkSubmitValidations(component,event);
            if(checkVal == true){
                component.set("v.showError",false);
                helper.submitForgotPass(component,event);
            }else{
                component.set("v.showError",true);
                //component.set("v.errorMessage",component.get("v.customLabelMap.MV_ValidInputs"));
                component.set("v.errorMessage",component.get("v.customLabelMap.MV_AnswertoQuestion"));
            }
        }else{
            component.set("v.captchaError",true);
        }
    },
    
    resetPassWord: function (component, event, helper) {
        debugger;
        var checkVal = helper.checkPasswordValidations(component,event);
        if(checkVal){
            component.set("v.showError",false);
            helper.resetPasswordClick(component,event);
        }else{
            
        }
    },
    
    onRecAnsChange: function(component, event, helper){
        helper.checkAnsRequired(component, event);
    },
    onChangeVerifypasswrd: function(component, event, helper){
        helper.verifypasswrd(component, event);
    },
    onChangeConfirmpass: function(component, event, helper){
        helper.confirmpass(component, event);
    },
    keyPress: function(component, event, helper) {
        if (event.keyCode == 13) {
            var spinner = component.find("mySpinner");
            $A.util.toggleClass(spinner, "slds-hide");            
            helper.customLogin(component, event);
        }
    },    
})
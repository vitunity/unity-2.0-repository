({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        if(lang == undefined )
        {
            var staticLabel = $A.get("$Label.c.MV_WebStorage");
            if(undefined != localStorage.getItem('languageKey') && 'undefined' != localStorage.getItem('languageKey') && staticLabel.toUpperCase() == 'TRUE')
            {
                component.set("v.language",localStorage.getItem('languageKey')); 
            }
            else
            {
                component.set("v.language",'en');
            }
        }
        else
        {
            component.set("v.language",lang);
        }
        
        var startURL = getUrlParameter('startURL');     
        if(startURL != undefined ){
            //alert(startURL);
            var dId = getUrlParameter('Id');
            if(dId != undefined){
                component.set("v.startURL",startURL+"?Id="+dId);
            }else{
                component.set("v.startURL",startURL);
            }
            //alert(component.get("v.startURL"));
        }
        //varian unite auth page issue fix.
        var hrefVar = window.location.href.split('=');
        //alert(decodeURIComponent(hrefVar[1]));
        if(hrefVar.length > 0 && hrefVar[1].includes('setup')){
            component.set("v.startURL",hrefVar[1]); 
        }
        /*
        if(lang){
            component.set("v.selectedLanguage",lang);  
        }*/
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        // var action = component.get("c.getCustomLabelMap");
        // action.setParams({"languageObj":component.get("v.language")});
        // action.setCallback(this, function(response) {
        //     var state = response.getState();
        //     if (component.isValid() && state === "SUCCESS") {
        //         component.set("v.customLabelMap",response.getReturnValue());
        //         component.loadPreLoginContent();
        //     }else if(response.getState() == "ERROR"){
        //         $A.log("callback error", response.getError());
        //     }      
        // } );
        // $A.enqueueAction(action);

        var staticLabel = $A.get("$Label.c.MV_WebStorage");
        var lang = component.get("v.language");
		console.log(' ###1## ' + component.get("v.language"));
        if (typeof(Storage) !== "undefined" && staticLabel.toUpperCase() == 'TRUE') 
        {
            if(undefined != localStorage.getItem(lang) && localStorage.getItem(lang).length!=0)
            {
                component.set("v.customLabelMap",JSON.parse(localStorage.getItem(lang)));
                localStorage.setItem('languageKey',lang);
                component.loadPreLoginContent();
            }
            else
            {
                console.log(' ##### ' + component.get("v.language"));
                var action = component.get("c.getCustomLabelMap");
                action.setParams({"languageObj":component.get("v.language")});
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS") {
                        component.set("v.customLabelMap",response.getReturnValue());
                        component.loadPreLoginContent();
                        localStorage.setItem(lang, JSON.stringify(response.getReturnValue()));
                        localStorage.setItem('languageKey',lang);
                        if (component.get("v.selectedLanguage") != '') {
                            $('.selectpicker').val(component.get("v.selectedLanguage"));
                        }
                    }else if(response.getState() == "ERROR"){
                        $A.log("callback error", response.getError());
                    }      
                } );
                $A.enqueueAction(action);
            }
            // Store
        } 
        else 
        {
            console.log(' ###2## ' + component.get("v.language"));
            console.log(' ###2## ' + JSON.stringify(component.get("c.getCustomLabelMap")) );
            var action = component.get("c.getCustomLabelMap");
            action.setParams({"languageObj":component.get("v.language")});
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log(' == PUNEET 3 ==> + ' + JSON.stringify(response.getState()) );
                if (component.isValid() && state === "SUCCESS") {
                    console.log(' == PUNEET 3 ==> + ' + JSON.stringify(response.getReturnValue()) );
                    component.set("v.customLabelMap",response.getReturnValue());
                    component.loadPreLoginContent();
                    localStorage.setItem(lang, JSON.stringify(response.getReturnValue()));
                    localStorage.setItem('languageKey',lang);
                }else if(response.getState() == "ERROR"){
                    $A.log("callback error", response.getError());
                }      
            } );
            $A.enqueueAction(action);
        }

    },
    
    /* Login action*/
    /*
    customLogin: function(component, event) {
        //alert('Getting Logged In.......');
        console.log('Login---');

        var Email = component.get("v.username");
        var password = component.get("v.password");

        //Validation
        if ($A.util.isEmpty(Email) || $A.util.isUndefined(Email)) {
            //alert('Email is Required');
            component.set("v.showError",true);
            component.set("v.loginError", component.get("{!v.customLabelMap.Invalid_Username_Error_Message}"));                      
            return;
        } else {
            component.set("v.showError",false);
            component.set("v.loginError","");            
        }
        if ($A.util.isEmpty(password) || $A.util.isUndefined(password)) {
            //alert('Password is Rqquired');
            component.set("v.showError",true);
            component.set("v.loginError",component.get("{!v.customLabelMap.Invalid_Username_Error_Message}"));            
            return;
        } else {
            component.set("v.showError",false);
            component.set("v.loginError","");              
        }       

        
        this.getPartnerContact(component,event);
        console.log(Email+'##'+password+'##'+component.get("v.language")+'##'+component.get("v.startURL")+'##'+component.get("v.okatAppLoginURL"));
        var pageName = component.get("v.pageName");
        var actionLogin = component.get("c.loginCommunityWithApp");
        actionLogin.setParams({
            "username": Email,
            "password": password,
            "language": component.get("v.language"),
            "startUrl": component.get("v.startURL"),
            "okatLoginAppURL": component.get("v.okatAppLoginURL")
        });
        actionLogin.setCallback(this, function(response) {
            var state = response.getState();
            // alert(response.getReturnValue());
            if (component.isValid() && state === "SUCCESS") {

                var isMDADL = component.get("v.loggedInUser.Is_MDADL__c");

                if(isMDADL == true) {
                    pageName = 'mdadllungphantom';
                } else {
                    pageName = "homepage";
                }

                var url = response.getReturnValue();         
                var newUrl = url.replace("/homepage", '/'+pageName);
                window.open(newUrl,'_self');

            } else if (response.getState() == "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.loginError",component.get('v.customLabelMap.Invalid_Username_Error_Message'));
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                
                //alert(response.getError());
                $A.log("callback error", response.getError());
                //window.open(response.getReturnValue(),'_self');
            }
        });
        $A.enqueueAction(actionLogin);
    },
    */

    customLogin: function(component, event) {
        //alert('Getting Logged In.......');
        console.log('Login---');

        var Email = component.get("v.username");
        var password = component.get("v.password");
        
        //Validation
        if ($A.util.isEmpty(Email) || $A.util.isUndefined(Email)) {
            //alert('Email is Required');
            component.set("v.showError",true);
            component.set("v.loginError", component.get("{!v.customLabelMap.Invalid_Username_Error_Message}"));                      
            return;
        } else {
            component.set("v.showError",false);
            component.set("v.loginError","");            
        }
        if ($A.util.isEmpty(password) || $A.util.isUndefined(password)) {
            //alert('Password is Rqquired');
            component.set("v.showError",true);
            component.set("v.loginError",component.get("{!v.customLabelMap.Invalid_Username_Error_Message}"));            
            return;
        } else {
            component.set("v.showError",false);
            component.set("v.loginError","");              
        }   
        
        //alert('Email1 = ' + Email);

        //Check for frozen user - if frozen show message and exit
        var action = component.get("c.isUserFreezedMethod"); 
        action.setParams({
            "userEmail": Email
        });       
        //alert('Email2 = ' + Email);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var isFrozen = response.getReturnValue();
                if (isFrozen == true) {
                    $('#new-reg-email-change').modal('show');
                    return;
                } else {


                    this.getPartnerContact(component,event);
                    console.log(Email+'##'+password+'##'+component.get("v.language")+'##'+component.get("v.startURL")+'##'+component.get("v.okatAppLoginURL"));
                    var pageName = component.get("v.pageName");
                    var actionLogin = component.get("c.loginCommunityWithApp");
                    actionLogin.setParams({
                        "username": Email,
                        "password": password,
                        "language": component.get("v.language"),
                        "startUrl": component.get("v.startURL"),
                        "okatLoginAppURL": component.get("v.okatAppLoginURL")
                    });
                    actionLogin.setCallback(this, function(response) {
                        var state = response.getState();
                        // alert(response.getReturnValue());
                        if (component.isValid() && state === "SUCCESS") {
							window.localStorage.setItem('recentLogin', true);
                            var isMDADL = component.get("v.loggedInUser.Is_MDADL__c");

                            if(isMDADL == true) {
                                pageName = 'mdadllungphantom';
                            } else {
                                pageName = "homepage";
                            }

                            var url = response.getReturnValue();         
                            var newUrl = url.replace("/homepage", '/'+pageName);
                            window.open(newUrl,'_self');

                        } else if (response.getState() == "ERROR") {
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    component.set("v.loginError",component.get('v.customLabelMap.Invalid_Username_Error_Message'));
                                    console.log("Error message: " + errors[0].message);
                                }
                            } else {
                                console.log("Unknown error");
                            }
                            
                            //alert(response.getError());
                            $A.log("callback error", response.getError());
                            //window.open(response.getReturnValue(),'_self');
                        }
                    });
                    $A.enqueueAction(actionLogin);


                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    /* navigate To URL*/
    navigateToURL: function(component, event, url) {
        // var url = "https://webapps.varian.com/certificate/en";
        if(url){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                isredirect: "true"
            });
            urlEvent.fire(); 
        }        
    },
    /* Load Webinars */
    getOnDemandWebinars : function(component, event) {
        var action = component.get("c.getOnDemandWebinars");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lstWebinars = response.getReturnValue();
               // alert(lstWebinars);
                component.set("v.lstWebinars",lstWebinars);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Load Featured Stories */
    loadFeaturedStories : function(component, event) {
        var action = component.get("c.getFeaturedStories");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lstFeaturedStories = response.getReturnValue();
                //alert(lstFeaturedStories);
                component.set("v.lstFeaturedStories",lstFeaturedStories);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* open Featured Story Link*/
    openFeaturedStoryLink: function(component, event) {
        var index= event.currentTarget.id;
        var url;
        
        var lstFeaturedStories = component.get("v.lstFeaturedStories");
        if(!$A.util.isEmpty(lstFeaturedStories)){
            var item = lstFeaturedStories[index];
            if(item){
               url = item.link; 
            }
        }
        if(url){
          this.navigateToURL(component, event,url);  
        }        
    },
    /* open Webinar Link*/
    openWebinarLink: function(component, event) {
        var id = event.currentTarget.id;
        var url;
        
        if(id){
            url="/mvwebsummary?Id="+id; // mvwebsummary?Id=a0O0n0000007LTeEAM
        }       
        if(url){
          this.navigateToURL(component, event,url);  
        }        
    },
    /* Get User */
    getUser : function(component, event) {
        var getUser = component.get("c.isLoggedIn");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isloggedIn", response.getReturnValue());
                var isLoggedIn = component.get("v.isloggedIn");
                if(isLoggedIn == true){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/homepage"
                    });
                    //urlEvent.fire();
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },
    /* Load Salutations */
    getSalutation : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Salutation"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var salutations = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Salutation"))
                    {
                        salutations.push(returnArray[i]);
                    }
                }
                component.set("v.mvSalutation", salutations);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Set Salutation value */
    setSalutation: function(component, event) 
    {
        var selectCmp = component.find("Salutation");
        component.set("v.registerData.Salutation", selectCmp.get("v.value"));
    },
    
    /* Load Speciality picklist value */
    getSpecialty : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Specialty__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Specialties = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Specialty__c"))
                    {
                        Specialties.push(returnArray[i]);
                    }
                }
                component.set("v.mvSpecialty", Specialties);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Load Account Type picklist value */
    getAccountType : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForAccountType");
        action.setParams({"fieldName":"Account_Type__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Specialties = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Account_Type__c"))
                    {
                        Specialties.push(returnArray[i]);
                    }
                }
                component.set("v.mvAccountType", Specialties);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setSpeciality: function(component, event) 
    {
        var selectCmp = component.find("Specialty");
        component.set("v.registerData.Specialty__c", selectCmp.get("v.value"));
    },
    setAccountType: function(component, event) 
    {
        var selectCmp = component.find("AccountType");
        component.set("v.registerData.Account_Type__c", selectCmp.get("v.value"));
    },
    
    /* Get Preferred Languager picklist value */
    getPreferredLanguage : function(component, event) {
        var action = component.get("c.getLanguagePicklistOptn");
        action.setParams({"fieldName":"Preferred_Language1__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Languages = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    Languages.push({
                        label: returnArray[i].split(',')[0],
                        value: returnArray[i].split(',')[1]
                    });
                    /*
                    if(returnArray[i] != component.get("v.registerData.Preferred_Language1__c"))
                    {
                        Languages.push(returnArray[i]);
                    }*/
                }
                component.set("v.mvPreferredLanguage", Languages);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setLanguage: function(component, event) 
    {
        var selectCmp = component.find("Language");
        component.set("v.registerData.Preferred_Language1__c", 
                      selectCmp.get("v.value"));
    },
    
    /* Get Professional role picklist value */
    getProfessionalRole : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Functional_Role__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var roles = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Functional_Role__c"))
                    {
                        roles.push(returnArray[i]);
                    }
                }
                component.set("v.mvProfessionalRole", roles);
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setRole: function(component, event) {
        var selectCmp = component.find("Role");
        component.set("v.registerData.Functional_Role__c", selectCmp.get("v.value"));
    },
    
    /* Get Country picklist value */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForCountry");
        action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.MailingCountry"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    setCountry: function(component, event) 
    {
        var selectCmp = component.find("country");
        //alert("test=="+selectCmp.get("v.value"));
        component.set("v.registerData.MailingCountry", selectCmp.get("v.value"));
    },
    
    /*Initialise captcha value */
    initialiseCaptcha: function(component,event) {
        component.set("v.char1", this.randomNumber());
        component.set("v.char2", this.randomNumber());
        component.set("v.char3", this.randomNumber());
        component.set("v.char4", this.randomNumber());
        component.set("v.char5", this.randomNumber());
        component.set("v.char6", this.randomNumber());
    },
    /* Generate ramdom number for captcha */
    randomNumber: function(){
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var random = possible.charAt(Math.floor(Math.random() * possible.length))
        return random;
    },
    
    /* Captcha match Validation */
    validateCaptcha: function(component,event) {        
        //debugger;
        var char1 = component.get("v.char1");
        var char3 = component.get("v.char3");
        var char5 = component.get("v.char5");
        var captchaData = component.get("v.captchaData");
        if(captchaData == undefined || captchaData == null){    
            this.initialiseCaptcha(component,event);
            return false;
            
        }else if(captchaData.length != 3){
             this.initialiseCaptcha(component,event);
            return false;
        }else if(captchaData[0].localeCompare(char1) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[1].localeCompare(char3) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[2].localeCompare(char5) != 0){
            return false;
             
        }else{
            return true;
        }
    },

    validateRequiredFields: function(component,event) {  
        //debugger;
        var errorFlag = false;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var fName = component.find("firstNameId").get("v.value");
        var lName = component.find("lastNameId").get("v.value");
        var titleVar = component.find("titleId").get("v.value");
        var telVar = component.find("telephoneId").get("v.value");
        var faxVar = component.find("faxId").get("v.value");
        var cEmail = component.find("conEmail").get("v.value");
        var sal = component.find("Salutation").get("v.value");
        var roleVar = component.find("Role").get("v.value");
        var specialtyVar = component.find("Specialty").get("v.value");
        var langVar = component.find("Language").get("v.value");
        var instiVar = component.find("institutionId").get("v.value");
        var addVar = component.find("institutionAddId").get("v.value");
        var cityVar = component.find("institutionCityId").get("v.value");
        var stateVar = component.find("institutionStateId").get("v.value");
        var countryVar = component.find("country").get("v.value");
        var zipVar = component.find("institutionZipId").get("v.value");
        var managerVar = component.find("managerId").get("v.value");
        var mEmailVar = component.find("managerEmailId").get("v.value");
       
        if(cEmail == null || !cEmail.match(regExpEmailformat)){
            errorFlag = true;
            component.set("v.conEmail", "true");
        }else{
            component.set("v.conEmail", "false");
        }
        if(fName == undefined || fName == null){
            errorFlag = true;
            component.set("v.firstNameId", "true");
        }else{
            component.set("v.firstNameId", "false");
        }
        if(lName == undefined || lName == null){
            errorFlag = true;
            component.set("v.lastNameId", "true");
        }else{
            component.set("v.lastNameId", "false");
        }
        if(titleVar == undefined || titleVar == null){
            errorFlag = true;
            component.set("v.titleId", "true");
        }else{
            component.set("v.titleId", "false");
        }
        if(telVar == undefined || telVar == null){
            errorFlag = true;
            component.set("v.telephoneId", "true");
        }else{
            component.set("v.telephoneId", "false");
        }
        /*
        if(faxVar == undefined || faxVar == null){
            errorFlag = true;
            component.set("v.faxId", "true");
        }else{
            component.set("v.faxId", "false");
        }
        */
        if(sal == undefined || sal == null){
            errorFlag = true;
            component.set("v.salutationId", "true");
        }else{
            component.set("v.salutationId", "false");
        }
        if(langVar == undefined || langVar == null){
            errorFlag = true;
            component.set("v.languageId", "true");
        }else{
            component.set("v.languageId", "false");
        }
        if(specialtyVar == undefined || specialtyVar == null){
            errorFlag = true;
            component.set("v.specialtyId", "true");
        }else{
            component.set("v.specialtyId", "false");
        }
        if(roleVar == undefined || roleVar == null){
            errorFlag = true;
            component.set("v.roleId", "true");
        }else{
            component.set("v.roleId", "false");
        }
        
        if(instiVar == undefined || instiVar == null){
            errorFlag = true;
            component.set("v.institutionId", "true");
        }else{
            component.set("v.institutionId", "false");
        }
        
        if(addVar == undefined || addVar == null){
            errorFlag = true;
            component.set("v.institutionAddId", "true");
        }else{
            component.set("v.institutionAddId", "false");
        }
        
        if(cityVar == undefined || cityVar == null){
            errorFlag = true;
            component.set("v.institutionCityId", "true");
        }else{
            component.set("v.institutionCityId", "false");
        }
        if(stateVar == undefined || stateVar == null){
            errorFlag = true;
            component.set("v.institutionStateId", "true");
        }else{
            component.set("v.institutionStateId", "false");
        }
        if(countryVar == undefined || countryVar == null){
            errorFlag = true;
            component.set("v.country", "true");
        }else{
            component.set("v.country", "false");
        }
        if(zipVar == undefined || zipVar == null){
            errorFlag = true; 
            component.set("v.institutionZipId", "true");
        }else{
            component.set("v.institutionZipId", "false");
        }
        

        if(managerVar == undefined || managerVar == null){
            errorFlag = true;
            component.set("v.managerId", "true");
        } else{
            component.set("v.managerId", "false");
        }
        
        if(mEmailVar == undefined || mEmailVar == null){
            errorFlag = true;
            component.set("v.managerEmailId", "true");
        }else{
            component.set("v.managerEmailId", "false");
        }
        return errorFlag;
    },


    validateRequiredFieldsNew: function(component,event) {  
        //debugger;
        var errorFlag = false;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var fName = component.find("firstNameId").get("v.value");
        var lName = component.find("lastNameId").get("v.value");
        var titleVar = component.find("titleId").get("v.value");
        var telVar = component.find("telephoneId").get("v.value");
        var faxVar = component.find("faxId").get("v.value");
        var cEmail = component.find("conEmail").get("v.value");
        var sal = component.find("Salutation").get("v.value");
        var roleVar = component.find("Role").get("v.value");
        var specialtyVar = component.find("Specialty").get("v.value");
        var langVar = component.find("Language").get("v.value");
        var instiVar = component.find("institutionId").get("v.value");
        var addVar = component.find("institutionAddId").get("v.value");
        var cityVar = component.find("institutionCityId").get("v.value");
        var stateVar = component.find("institutionStateId").get("v.value");
        var countryVar = component.find("country").get("v.value");
        var zipVar = component.find("institutionZipId").get("v.value");
        var managerVar = component.find("managerId").get("v.value");
        var mEmailVar = component.find("managerEmailId").get("v.value");
        var lang = component.get("v.language");

        //alert('lang = ' + lang);
       
        if(cEmail == null || !cEmail.match(regExpEmailformat)){
            errorFlag = true;
            //component.set("v.conEmail", "true");
            $A.util.addClass(component.find("conEmail"), 'errorClass');
        }else{
            component.set("v.conEmail", "false");
            $A.util.removeClass(component.find("conEmail"), 'errorClass');
        }
        if(fName == undefined || fName == null){
            errorFlag = true;
            //component.set("v.firstNameId", "true");
            $A.util.addClass(component.find("firstNameId"), 'errorClass');
        }else{
            component.set("v.firstNameId", "false");
            $A.util.removeClass(component.find("firstNameId"), 'errorClass');
        }
        if(lName == undefined || lName == null){
            errorFlag = true;
            //component.set("v.lastNameId", "true");
            $A.util.addClass(component.find("lastNameId"), 'errorClass');
        }else{
            component.set("v.lastNameId", "false");
            $A.util.removeClass(component.find("lastNameId"), 'errorClass');
        }
        if(titleVar == undefined || titleVar == null){
            errorFlag = true;
            //component.set("v.titleId", "true");
            $A.util.addClass(component.find("titleId"), 'errorClass');
        }else{
            component.set("v.titleId", "false");
            $A.util.removeClass(component.find("titleId"), 'errorClass');
        }
        if(telVar == undefined || telVar == null){
            errorFlag = true;
            //component.set("v.telephoneId", "true");
            $A.util.addClass(component.find("telephoneId"), 'errorClass');
        }else{
            component.set("v.telephoneId", "false");
            $A.util.removeClass(component.find("telephoneId"), 'errorClass');
        }
        /*
        if(faxVar == undefined || faxVar == null){
            errorFlag = true;
            component.set("v.faxId", "true");
        }else{
            component.set("v.faxId", "false");
        }
        */
        if(sal == undefined || sal == null){
            errorFlag = true;
            //component.set("v.salutationId", "true");
            $A.util.addClass(component.find("Salutation"), 'errorClass');
        }else{
            component.set("v.salutationId", "false");
            $A.util.removeClass(component.find("Salutation"), 'errorClass');
        }

        if(langVar == undefined || langVar == null || langVar == ''){

            errorFlag = true;
            //component.set("v.languageId", "true");
            $A.util.addClass(component.find("Language"), 'errorClass');
        }else{
            component.set("v.languageId", "false");
            $A.util.removeClass(component.find("Language"), 'errorClass');
        }
        if(specialtyVar == undefined || specialtyVar == null){
            errorFlag = true;
            //component.set("v.specialtyId", "true");
            $A.util.addClass(component.find("Specialty"), 'errorClass');
        }else{
            component.set("v.specialtyId", "false");
            $A.util.removeClass(component.find("Specialty"), 'errorClass');
        }
        if(roleVar == undefined || roleVar == null){
            errorFlag = true;
            //component.set("v.roleId", "true");
            $A.util.addClass(component.find("Role"), 'errorClass');
        }else{
            component.set("v.roleId", "false");
            $A.util.removeClass(component.find("Role"), 'errorClass');
        }
        
        if(instiVar == undefined || instiVar == null){
            errorFlag = true;
            //component.set("v.institutionId", "true");
            $A.util.addClass(component.find("institutionId"), 'errorClass');
        }else{
            component.set("v.institutionId", "false");
            $A.util.removeClass(component.find("institutionId"), 'errorClass');
        }
        
        if(addVar == undefined || addVar == null){
            errorFlag = true;
            //component.set("v.institutionAddId", "true");
            $A.util.addClass(component.find("institutionAddId"), 'errorClass');
        }else{
            component.set("v.institutionAddId", "false");
            $A.util.removeClass(component.find("institutionAddId"), 'errorClass');
        }
        
        if(cityVar == undefined || cityVar == null){
            errorFlag = true;
            //component.set("v.institutionCityId", "true");
            $A.util.addClass(component.find("institutionCityId"), 'errorClass');
        }else{
            component.set("v.institutionCityId", "false");
            $A.util.removeClass(component.find("institutionCityId"), 'errorClass');
        }
        if(stateVar == undefined || stateVar == null){
            errorFlag = true;
            //component.set("v.institutionStateId", "true");
            $A.util.addClass(component.find("institutionStateId"), 'errorClass');
        }else{
            component.set("v.institutionStateId", "false");
            $A.util.removeClass(component.find("institutionStateId"), 'errorClass');
        }
        if(countryVar == undefined || countryVar == null){
            errorFlag = true;
            //component.set("v.country", "true");
            $A.util.addClass(component.find("country"), 'errorClass');
        }else{
            component.set("v.country", "false");
            $A.util.removeClass(component.find("country"), 'errorClass');
        }
        if(zipVar == undefined || zipVar == null){
            errorFlag = true; 
            //component.set("v.institutionZipId", "true");
            $A.util.addClass(component.find("institutionZipId"), 'errorClass');
        }else{
            component.set("v.institutionZipId", "false");
            $A.util.removeClass(component.find("institutionZipId"), 'errorClass');
        }
        
        if(lang != 'ja' && lang != 'Japanese') {
            if(managerVar == undefined || managerVar == null){
                errorFlag = true;
                //component.set("v.managerId", "true");
                $A.util.addClass(component.find("managerId"), 'errorClass');
            } else{
                component.set("v.managerId", "false");
                $A.util.removeClass(component.find("managerId"), 'errorClass');
            }
            
            if(mEmailVar == undefined || mEmailVar == null){
                errorFlag = true;
                //component.set("v.managerEmailId", "true");
                $A.util.addClass(component.find("managerEmailId"), 'errorClass');
            }else{
                component.set("v.managerEmailId", "false");
                $A.util.removeClass(component.find("managerEmailId"), 'errorClass');
            }
        }

        //alert('errorFlag = ' + errorFlag);
        return errorFlag;
    },
    
    verifypasswrd: function(component, event) {
        var Exp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
        var pwd = component.find("newPwd").get("v.value"); 
        if ($A.util.isEmpty(pwd) || $A.util.isUndefined(pwd)) {
            document.getElementById('newPwd').style.display = 'block';
            
        }else{
            document.getElementById('newPwd').style.display = 'none';
        }
        if(!Exp.test(pwd) || pwd.length < 8)
        {
           alert('Password must contain at least one uppercase letter, one lowercase letter, one number,and 8 characters .');
           return false;
        }
    },
    
    confirmpass: function(component, event) 
    {
        var oldpwd = component.find("newPwd").get("v.value");
        var newpwd = component.find("confirmPwd").get("v.value");
        
        if ($A.util.isEmpty(oldpwd) || $A.util.isUndefined(oldpwd)) {
           document.getElementById('confirmPwd').style.display = 'block';
        }
        else
        {
            document.getElementById('confirmPwd').style.display = 'none';
        }
        
        if(oldpwd != newpwd)
        {
            document.getElementById('confirmpass').style.display = 'block';
        }
        else
        {
            document.getElementById('confirmpass').style.display = 'none';
        }
     },
        
     checkAnsRequired: function(component, event) {
        var ansre = component.find("recAnswer").get("v.value");
        if ($A.util.isEmpty(ansre) || $A.util.isUndefined(ansre)) {
           document.getElementById('recAnswer').style.display = 'block';
        }
        else
        {
            document.getElementById('recAnswer').style.display = 'none';
        }
    },
    
    submitForgotPass: function (component, event, helpler) {
        var userName = component.find("userNameId").get("v.value");
        var lastName = component.find("lastNameIdforPassword").get("v.value");
        var action = component.get("c.getRecovQuestion");
        action.setParams({"Emailid":userName, "Lastname":lastName});
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();

            if (!$A.util.isEmpty(rtnValue)) {
                if(rtnValue == "NoQuestion"){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_NoSecurityQuestion"));
                }else if(rtnValue == "WrongInputs" || rtnValue == "NotMatched"){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_Notmatch"));
                }else{
                    component.set("v.recoverQuestion",rtnValue);
                    $('#forgot').modal('hide');
                    $('#reset').modal('show');
                    this.doFrgotPassClear(component,event);
                }                
            }
        });
        $A.enqueueAction(action);
    },
    
    resetPasswordClick: function (component, event, helpler) {
        debugger;
        var userName = component.find("userNameId").get("v.value");
        var lastName = component.find("lastNameIdforPassword").get("v.value");
        var Answer = component.find("recAnswer").get("v.value");
        var NewPassWord = component.find("newPwd").get("v.value");
        
        var action = component.get("c.changePassword");
        action.setParams({
            "Emailid":userName,
            "Lastname":lastName,
            "Answer":Answer,
            "NewPassWord":NewPassWord
        });
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            if (!$A.util.isEmpty(rtnValue)) {
                if(rtnValue == "Success"){  
                    $('#passreset-complete-1').modal('show');
                    $('#reset').modal('hide');
                    this.doFrgotPassClear(component,event);
                    //this.navigateToURL(component, event,'/');
                }else if(rtnValue == "WrongAnswer"){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_AnswertoQuestion"));
                }else if(rtnValue == "PasswordMatched"){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_DiffPassword"));
                }
                else if(rtnValue == "password.value: Password cannot be your current password"){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_DiffPassword"));
                }
                else if(rtnValue == "recoveryQuestion.answer: The credentials provided were incorrect."){
                    component.set("v.showError",true);
                    component.set("v.errorMessage",component.get("v.customLabelMap.MV_AnswertoQuestion"));
                }
                    
                    else{
                    component.set("v.showError",true);
                    component.set("v.errorMessage",rtnValue);
                }                  
            }else{
                component.set("v.showError",true);
                //component.set("v.errorMessage",component.get("v.customLabelMap.MV_ValidInputs"));
                component.set("v.errorMessage",component.get("v.customLabelMap.MV_AnswertoQuestion"));
            }
        });
        $A.enqueueAction(action);
    },
    
    checkSubmitValidations:function(cmp,event) {
        var validFlag = true;
        var userNameCmp = cmp.find("userNameId").get('v.value');
        var lastNameCmp = cmp.find("lastNameIdforPassword").get('v.value');
        if ($A.util.isEmpty(userNameCmp) || $A.util.isUndefined(userNameCmp)) {
            document.getElementById('userNameId').style.display = 'block';
            validFlag = false;
        }else{
            document.getElementById('userNameId').style.display = 'none';
        }
        
        if ($A.util.isEmpty(lastNameCmp) || $A.util.isUndefined(lastNameCmp)) {
            document.getElementById('lastNameIdforPassword').style.display = 'block';
            validFlag = false;
        }else{
            document.getElementById('lastNameIdforPassword').style.display = 'none';
        }
        return validFlag;
    },
    
    checkPasswordValidations:function(cmp,event) {        
        
        debugger;
        var validFlag = true;
        var recAnswerCmp = cmp.find("recAnswer").get('v.value');
        var newPwdCmp = cmp.find("newPwd").get('v.value');
        var confirmPwdCmp = cmp.find("confirmPwd").get('v.value');
        
        
        if ($A.util.isEmpty(recAnswerCmp) || $A.util.isUndefined(recAnswerCmp)) {
            document.getElementById('recAnswer').style.display = 'block';
            validFlag = false;
        }else{
            document.getElementById('recAnswer').style.display = 'none';
        }
        if ($A.util.isEmpty(newPwdCmp) || $A.util.isUndefined(newPwdCmp)) {
            document.getElementById('newPwd').style.display = 'block';
            validFlag = false;
        }else{
            document.getElementById('newPwd').style.display = 'none';
        }
        if ($A.util.isEmpty(confirmPwdCmp) || $A.util.isUndefined(confirmPwdCmp)) {
            document.getElementById('confirmPwd').style.display = 'block';
            validFlag = false;
        }else{
            document.getElementById('confirmPwd').style.display = 'none';
        }

        var pwdMatch = false;
        
        if(validFlag == true ){
            pwdMatch = (newPwdCmp == confirmPwdCmp ? true : false);
            if(!pwdMatch){
                cmp.set("v.showError",true);
                cmp.set("v.errorMessage",cmp.get("v.customLabelMap.MV_CorrectPassword"));
            }            
        }else{
            cmp.set("v.showError",true);
            //cmp.set("v.errorMessage",cmp.get("v.customLabelMap.MV_ValidInputs"));
            console.log("here in else");
            component.set("v.errorMessage",component.get("v.customLabelMap.MV_AnswertoQuestion"));
        }
        return pwdMatch;
    },
  
    setUserNameChange:function(component,event) {
        //debugger;
         //Todo - remove later - commented just for testing in QA
        var matchString = "varian.com";
        var email = component.get("v.username");
        if (email.toLowerCase().indexOf(matchString) != -1){
            $("#employeeMessage").modal("show");
            component.set("v.username",'');
        }
        
    },

    setUserNameChangeEmail:function(component,event) {
        //debugger;
         //Todo - remove later - commented just for testing in QA
        var matchString = "varian.com";
        //var email = component.get("v.username");
        var email = component.find("conEmail").get("v.value");
        if (email.toLowerCase().indexOf(matchString) != -1){
            $("#employeeMessage").modal("show");
            //component.set("v.username",'');
            component.find("conEmail").set("v.value", null);
        }
        
    },      

    setUserNameChangeEmailMgr:function(component,event) {
        //debugger;
         //Todo - remove later - commented just for testing in QA
        var matchString = "varian.com";
        //var email = component.get("v.username");
        var email = component.find("managerEmailId").get("v.value");
        if (email.toLowerCase().indexOf(matchString) != -1){
            $("#employeeMessage").modal("show");
            //component.set("v.username",'');
            component.find("managerEmailId").set("v.value", null);
        }
        
    },   

    doFrgotPassClear: function(component,event){
        //debugger;
        $('.key-box').val('');
        component.set('v.showError',false);
        component.set('v.errorMessage','');
        
        /*
        conponent.find("userNameId").put('v.value','');
        conponent.find("lastNameIdforPassword").put('v.value','');
        conponent.find("captchaId").put('v.value','');
        conponent.find("recAnswer").put('v.value','');
        conponent.find("newPwd").put('v.value','');
        conponent.find("confirmPwd").put('v.value','');
        */
    },
    getPartnerContact: function(component, event) {
        var Email1 = component.get("v.username");
        var action = component.get("c.getPartnerContactInfo"); 
        action.setParams({
            "userEmail": Email1
        });        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var usr = response.getReturnValue();
                component.set("v.loggedInUser", usr);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    validateDupEmail: function(component, event) {
        var Email = $('.conEmailClass').val();
        var action = component.get("c.duplicateContactEmail"); 
        action.setParams({
            "email": Email
        });        

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var dupEmail = response.getReturnValue();
                if(dupEmail == true) {
                    component.set("v.showError", true);
                } else {
                    component.set("v.showError", false);
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);        
    }
    
})
({
    doInit : function(component, event, helper) { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getRegionValues(component,event);
        helper.getProductValues(component,event);
        helper.getDocumentTypes(component,event);
        //helper.getMaterialTypes(component,event);
        //helper.getTreatmentTechniqueValues(component,event);
        helper.getContentVersionList(component,event);
    },
    changeRegion : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },    
    changeProduct : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    changeDocType : function(component, event, helper){
        if(component.find("docTypeId").get("v.value") === "Product Material"){
            $A.util.removeClass(component.find("divMaterialType"),"slds-hide");
            $A.util.removeClass(component.find("divTreatment"),"slds-hide");
        }else{
            $A.util.addClass(component.find("divMaterialType"),"slds-hide");
            $A.util.addClass(component.find("divTreatment"),"slds-hide");
            component.find("materialTypeId").set("v.value","");
            component.find("treatmentId").set("v.value","");
        }                          
        helper.getContentVersionList(component, event);
    },
    changeMaterialType : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    changeTreatmentTechnique : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    onApply : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    onReset : function(component, event, helper){
        component.find("refineKeyword").set("v.value","");
        //component.find("treatmentId").set("v.value","");
        //component.find("materialTypeId").set("v.value","");
        component.find("docTypeId").set("v.value","");
        component.find("productId").set("v.value","");
        component.find("regionId").set("v.value","");
        helper.getContentVersionList(component,event);
    },
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    /* download documet or redirect to another page */
    home : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage?lang="+component.get('v.language')
        });
        urlEvent.fire();
    }
})
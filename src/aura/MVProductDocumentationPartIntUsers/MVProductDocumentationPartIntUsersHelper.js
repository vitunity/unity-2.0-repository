({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getRegionValues : function(component,event){
        var action = component.get("c.getRegions");
        
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("regionId").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    getProductValues : function(component,event){
        var action = component.get("c.getProdOptions");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("productId").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    getDocumentTypes : function(component){
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        
        var action = component.get("c.getDocTypes");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("docTypeId").set("v.options",options);
        });	
        $A.enqueueAction(action); 
    },
    getMaterialTypes : function(component,event){
        var self = this;
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        var action = component.get("c.getMaterialTypes");
        // var action = component.get("c.getPicklistValues");
        // action.setParams({"fieldName":"Material_Type__c","sObjectName":"ContentVersion"});
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                if(!$A.util.isEmpty(picklistresult)){
                    for(var i =0;i<picklistresult.length;i++){
                        options.push({
                            label : picklistresult[i].label,
                            value : picklistresult[i].pvalue
                        });
                    } 
                }
            }
            component.find("materialTypeId").set("v.options",options);
        });	
        $A.enqueueAction(action); 
    },
    getTreatmentTechniqueValues : function(component,event){
        var self = this;
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        var action = component.get("c.getTreatmentTechniques");
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                if(!$A.util.isEmpty(picklistresult)){
                    for(var i =0;i<picklistresult.length;i++){
                        options.push({
                            label : picklistresult[i].label,
                            value : picklistresult[i].pvalue
                        });
                    } 
                }
            }
            component.find("treatmentId").set("v.options",options);
        });	
        $A.enqueueAction(action); 
    },
    getContentVersionList : function(component,event){
        var action = component.get("c.getContentVersions");
        action.setParams({"RegionAll":component.find("regionId").get("v.value"),
                          "Productvalue":component.find("productId").get("v.value"),
                          "DocumentTypes":component.find("docTypeId").get("v.value"),
                          "MaterialTypeInfo":'',
                          "TreatmentTechniquesInfo":'',//component.find("treatmentId").get("v.value")
                          "Document_TypeSearch":component.find("refineKeyword").get("v.value")
                         }); 
        
        action.setCallback(this, function(response) {            
            var state = response.getState();            
            if(state == "SUCCESS"){
                var contentVersionList = response.getReturnValue();              
                component.set("v.contentVersionList",contentVersionList);
            }            
        });	
        $A.enqueueAction(action); 
    },
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var contentVersionList = component.get("v.contentVersionList");
            if(!$A.util.isEmpty(contentVersionList)){
                var doc = contentVersionList[index];
                if(doc.Mutli_Languages__c){
                    var docId = doc.ContentDocumentId;
                    url = "/productdocumentationdetail/?Id="+docId;
                    
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": url,
                        isredirect: "true"
                    });
                    urlEvent.fire();
                    
                }else{                    
                    var docId = doc.Id;
                    var sPageURL = window.location.href;
                    var sURLVariables = sPageURL.split('/');
                    var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
                    //  strRemove = strRemove + '/' 
                    // strRemove = strRemove + sURLVariables[sURLVariables.length - 1];
                    var newUrl = sPageURL.replace(strRemove, '');
                    
                    //  url = "https://sfdev2-varian.cs61.force.com/varian/sfc/servlet.shepherd/version/download/"+docId ;
                    url = newUrl + "sfc/servlet.shepherd/version/download/"+docId;
                    window.location.replace(url) ;                   
                } 
            }
        }
    },    
})
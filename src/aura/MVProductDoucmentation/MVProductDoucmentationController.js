({
    doInit : function(component, event, helper) { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.isCurrentUserInEuro(component, event);
        helper.getProductGroupValues(component,event);
		helper.getContentVersionList(component, event);
    },    
    changeProduct : function(component, event, helper){
        helper.getDocumentType(component, event);
        //helper.getContentVersionList(component, event);
    },
    changeDocType : function(component, event, helper){
        //helper.getDocumentVersions(component, event);
        helper.getContentVersionList(component, event);
    },
    changeVersion : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    onApply : function(component, event, helper){  
        helper.getContentVersionList(component, event);
    },
    callApply : function(component, event, helper){
        debugger; 
         if(event.getParams().keyCode == 13){
            //alert('Enter key');
            helper.getContentVersionList(component, event);
         }
        
    },
    onReset : function(component, event, helper){
        component.find("refineKeyword").set("v.value","");
        helper.getProductGroupValues(component,event);
    },
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getContentVersionList(component,event,true);
    },
     home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    contactUs: function(component, event, helper) {
        var url = "/contactus?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    
})
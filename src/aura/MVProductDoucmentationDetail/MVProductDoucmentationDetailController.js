({
    doInit : function(component, event, helper) {        
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getContentVersionData(component, event);
        helper.getContentVersionList(component, event);
    },
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    home : function(component, event, helper){
        helper.goHome(component,event);
    },
 	docLibrary : function(component, event, helper){
        helper.goDocLibrary(component,event);
    }
})
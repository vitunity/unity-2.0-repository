({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
        var Id =  getUrlParameter('Id');
        component.set("v.Id",Id);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getContentVersionList : function(component,event) 
    {
        var self = this;
        var action = component.get("c.getlContentVersionsDetail");
        //var action = component.get("c.getDocuments");
        //debugger;

        var docId = component.get("v.Id");
        if(docId){
            action.setParams({
                "Document_TypeSearch":"",
                "docId":docId
            }); 
            
            action.setCallback(this, function(response) {                
                var state = response.getState();
                if(state == "SUCCESS")
                {
                    var cvlist = response.getReturnValue();
                    component.set("v.contentVersionList",cvlist);
                }
                
            });	
            $A.enqueueAction(action); 
        }
    },
    getContentVersionData : function(component,event){
        var self = this;
        var action = component.get("c.getlContentVersionsData");
        var docId = component.get("v.Id");
        if(docId){
            action.setParams({
                "docId":docId
            });
            action.setCallback(this, function(response) {                
                var state = response.getState();
                if(state == "SUCCESS"){
                    var contentVersion = response.getReturnValue();
                    if(!$A.util.isEmpty(contentVersion)){
                        component.set("v.contentVersion",contentVersion);
                    }                    
                }                
            });	
            $A.enqueueAction(action); 
        }
    },
    openDocument : function(component, event){
        var docId = event.currentTarget.id;
        if(docId){
            var url='';
            var sPageURL = window.location.href;
            var sURLVariables = sPageURL.split('/');
            // var strRemove =  '/' +sURLVariables[sURLVariables.length - 3] + '/' + sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
            // var newUrl = sPageURL.replace(strRemove, '');
            var newUrl = sURLVariables[0] +"//"+ sURLVariables[2] + "/" + sURLVariables[3];
            
            ///sfc/servlet.shepherd/version/download/{!DocInfo.Id}?{!$Label.ContentDocTag}">{!DocInfo.Title}
            url = newUrl + "/sfc/servlet.shepherd/version/download/"+ docId +"?" + $A.get("$Label.c.ContentDocTag");
            
            window.location.replace(url);
        }
    },
    goHome : function(component, event){
		var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();        
    },
    goDocLibrary : function(component, event){
    	var url = "/productdocumentation?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    }
})
({
    doInit : function(component, event, helper) 
    { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getRecentEvent(component, event);
    },
    
    openEventDetail : function(component, event, helper) { 
        var urlEvent = $A.get("e.force:navigateToURL");
        var eventId = event.currentTarget.getAttribute("data-craid");
        urlEvent.setParams({
            "url": "/eventdetail?lang="+component.get('v.language')+"&Id="+eventId
        });
        urlEvent.fire();
    },
    upcomingEvent : function(component, event, helper) 
    { 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/events?lang="+component.get('v.language')
        });
        urlEvent.fire();
    },
    
    home: function(component, event, helper) {
        var url = "/";
        if(component.get("v.isloggedIn")){
            url = "/homepage?lang="+component.get('v.language');
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
})
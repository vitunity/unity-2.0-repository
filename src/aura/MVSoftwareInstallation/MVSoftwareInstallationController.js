({
    /* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
       // alert("test");
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getSoftwareInstallationDocs(component, event);
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplay : function(component, event, helper){
        helper.toggleCardDisplay(component, event);
    },
    /* download documet or redirect to another page */
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    }
})
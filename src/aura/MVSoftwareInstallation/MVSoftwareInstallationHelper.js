({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* to go back to previous page */
    openProductAccessUpdateRequest : function() {
        // Salesforce event for navigation
        var urlEvent = $A.get("e.force:navigateToURL");
        // set url
        urlEvent.setParams({
            "url": '/productaccessupdate',
            isredirect: "true"
        });
        // Fire the event
        urlEvent.fire();
    },
    /* get Software Installation Documents by Category */
    getSoftwareInstallationDocs : function(component,event){
        // find Apex method to get list of  docs
        var action = component.get("c.getSoftwareInstallation");
        
        // Call the apex method.
        action.setCallback(this, function(response) {            
            // get the status
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                // get the marketing dos data
                var lstSoftwareInstall =response.getReturnValue(); 
                // Check list is empty or not.
                if(!$A.util.isEmpty(lstSoftwareInstall)){
                    // set  list  data into attribute.
                    component.set("v.lstSoftwareInstall",lstSoftwareInstall);
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        // set actio in queue. Lightning actions are asynchronous.
        $A.enqueueAction(action);
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplay : function(component, event){
        var index = event.currentTarget.id;
        
        var cardBodyId = component.find("bodyId");
        if($A.util.isArray(cardBodyId)){
            $A.util.toggleClass(cardBodyId[index], "slds-hide");
        }else{
            // It will add or remove slds-hide css class for html.
            $A.util.toggleClass(cardBodyId, "slds-hide");
        }
    },
    /* download documet or redirect to another page */
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var indexes = index.split('-');
            var url='';
            var contentVersionList = component.get("v.lstSoftwareInstall");
            if(!$A.util.isEmpty(contentVersionList)){
                var doc = contentVersionList[indexes[0]].lstSoftwareInstallation[indexes[1]];
                if(!doc.Multi_Languages__c && doc.ExternalUrl__c){
                    url = doc.URL__c;
                    if(url){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": url,
                            isredirect: "true"
                        });
                        urlEvent.fire();
                    }
                    
                }
                else if(doc.Multi_Languages__c){
                    var docId = doc.Content_Id__c;
                    if(docId){
                        url = "/productdocumentationdetail/?Id="+docId;
                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": url,
                            isredirect: "true"
                        });
                        urlEvent.fire();
                    }
                    
                }else if(!doc.Multi_Languages__c && !doc.ExternalUrl__c){
                    var docId = doc.ContentID__c;
                    if(docId){
                        var sPageURL = window.location.href;
                        var sURLVariables = sPageURL.split('/');
                        var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
                        var newUrl = sPageURL.replace(strRemove, '');
                        
                        url = newUrl + "sfc/servlet.shepherd/version/download/"+docId;
                        window.location.replace(url) ;  
                    }
                } 
            }
        }
        
    }
})
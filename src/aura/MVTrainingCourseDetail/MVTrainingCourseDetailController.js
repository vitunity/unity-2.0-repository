({
	doInit : function(component, event, helper) { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getTrainingCourse(component,event);
    },
    
    openTrainingCourses : function(component, event, helper){
        helper.openTrainingCourses(component,event);
    },
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    registerTraining : function(component, event, helper){
        helper.doRegisterTraining(component,event);
    },
    home: function(component, event, helper) {
        helper.doHome(component,event);
       
    },
})
({
    
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
        var TitleId =  getUrlParameter('Id');
        component.set("v.TitleId",TitleId);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    
    getTrainingCourse : function(component, event) {
        var action = component.get("c.getTrainingandCourses");
        action.setParams({"TitleId":component.get("v.TitleId"),
                          "SelectedMenu":component.get("v.region")
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") { 
                var TrainingCourse = response.getReturnValue();
                if(!$A.util.isEmpty(TrainingCourse)){
                    var lstTrainingCourses = TrainingCourse.lstTrCourse;               
                    component.set("v.lstTrainingCourses",lstTrainingCourses);
                    var lstTrainingDates = TrainingCourse.lstTrdate;               
                    component.set("v.lstTrainingDates",lstTrainingDates);
                    component.set("v.isLms",TrainingCourse.isLmsUser);
                    component.set("v.registerUrl",TrainingCourse.registerUrl);

                    component.set("v.Attachments", TrainingCourse.lstAtts);
                    component.set("v.Notes", TrainingCourse.lstNotes); 
                }
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    openTrainingCourses : function(component, event){ 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/trainingcourses?lang="+component.get('v.language'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    openDocument : function(component, event){
        var docId = event.currentTarget.id;
        if(docId){
            var sPageURL = window.location.href;
            var sURLVariables = sPageURL.split('/');
            var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
           
            var newUrl = sPageURL.replace(strRemove, '');
           // https://sfdev1-varian.cs67.force.com/servlet/servlet.FileDownload?retURL=%2Fapex%2FCpTrainingCourseDetail%3Fid%3Da2a0n00000031TWAAY&file=00P0n000000eFFXEA2&_CONFIRMATIONTOKEN=VmpFPSxNakF4Tnkwd09TMHhObFF3TkRvME9UbzBNeTQyTnpOYSwzaXpsa3p5VDhjVDROQkRjX1VuUlFOLFlUQmhOamht&common.udd.actions.ActionsUtilORIG_URI=%2Fservlet%2Fservlet.FileDownload
            var url = newUrl + "sfc/servlet.FileDownload?file="+docId;
            window.location.replace(url) ;
        }
    },
    
    doRegisterTraining : function(component, event){
        var url = component.get('v.registerUrl');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    doHome : function(component, event){
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    }
})
({
	doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);        
        helper.getCustomLabels(component, event);
        ////helper.getLogUserTerritory(component,event);
        helper.getCookieParameter(component, event);
        helper.getLogUserTerritory(component,event);        
        helper.getContentVersionList(component,event);
    },
    onApply : function(component, event, helper){
        helper.getContentVersionList(component, event);
    },
    onRegionClick : function(component, event, helper){
        var region = event.currentTarget.id;
        component.set("v.region",region);
        document.cookie = "UTerritory=" + escape(region) + "; path=/";
        // var UTerritory =  'LSKey[c]UTerritory';
        helper.getContentVersionList(component, event);
    },
    /* redirect to another page */
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getContentVersionList(component,event,true);
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    keyPress: function(component, event, helper) {
        if (event.keyCode == 13) {
            helper.getContentVersionList(component, event);
        }
    },

})
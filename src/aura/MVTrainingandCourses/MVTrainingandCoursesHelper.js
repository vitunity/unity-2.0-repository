({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },

    /* Load data */
    getContentVersionList : function(component, event,resetPages) {
        if(!resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        
        var action = component.get("c.getContentVersions");
        action.setParams({"Document_TypeSearch":component.find("refineKeyword").get("v.value"),
                          "SelectedMenu":component.get("v.region"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "pageSize":component.get("v.pageSize")
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {                
                //var contentVersionList = response.getReturnValue();
                //component.set("v.contentVersionList",contentVersionList);
                
                var responseWrap = response.getReturnValue(); 
                component.set("v.contentVersionList",responseWrap.lstTrainingCourses);
                component.set("v.totalSize",responseWrap.totalSize);
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* redirect to another page */
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        
        if(index){
            var url='';
            var contentVersionList = component.get("v.contentVersionList");
            if(!$A.util.isEmpty(contentVersionList)){                
                var doc = contentVersionList[index];
                if(doc){
                    var docId = doc.Id;
                    if(docId){
                        url = "/trainingcoursedetail?Id="+docId+"&lang="+component.get('v.language');                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": url,
                            isredirect: "true"
                        });
                        urlEvent.fire();
                    }                    
                } 
            }
        }        
    },
    
    
    setCookie: function(component, event) {
        var expires;
        var days;
        var namespace ='';
        
      /*  if(days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }*/
        
        // Note: the name would usually be escaped when setting a cookie but
        // Locker Service uses braces in the cookie name.
        document.cookie = "UTerritory=" + escape(value) + expires + "; path=/";
    },

    getLogUserTerritory : function(component, event) {
        var logUsrTerr;
        var action = component.get("c.getLoggedInUserTerritory");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                logUsrTerr = response.getReturnValue();
                component.set("v.logUserTerritory", logUsrTerr);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },

    getCookieParameter : function(component, event) {
        var getCookieParameter = function getCookieParameter(sParam) {
            var sPageURL = decodeURIComponent(document.cookie),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
                //alert('sPageURL = ' + sPageURL);
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var region =  getCookieParameter('UTerritory');
        if(!$A.util.isEmpty(region)){
            component.set("v.region",region);
        } 
    },

    /* Load data */
    getContentVersionListTest : function(component, event,resetPages) {

        console.log('reached here 1 = ' + component.find("refineKeyword").get("v.value"));
        console.log('reached here 2 = ' + component.get("v.region"));
        console.log('reached here 3 = ' + component.get("v.recordsOffset"));
        console.log('reached here 4 = ' + component.get("v.totalSize"));
        console.log('reached here 5 = ' + component.get("v.pageSize"));

        var action = component.get("c.getContentVersions");
        action.setParams({"Document_TypeSearch":component.find("refineKeyword").get("v.value"),
                          "SelectedMenu":component.get("v.region"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "pageSize":component.get("v.pageSize")
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {                
                //var contentVersionList = response.getReturnValue();
                //component.set("v.contentVersionList",contentVersionList);
                
                var responseWrap = response.getReturnValue(); 
                component.set("v.contentVersionList",responseWrap.lstTrainingCourses);
                component.set("v.totalSize",responseWrap.totalSize);
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },    
    
})
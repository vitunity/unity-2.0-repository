({
	/* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
       // alert("test");
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);        
        helper.getAgreementShow(component, event);
        helper.getTrueBeamValues(component, event);
        // helper.toggleTermsDialog(component, event);
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplay : function(component, event, helper){
        helper.toggleCardDisplay(component, event, "bodyId");
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplayStatic : function(component, event, helper){
        helper.toggleCardDisplay(component, event, "bodyIdFL");
    },
    toggleCardDisplayFaq : function(component, event, helper){
       helper.toggleCardDisplay(component, event, "bodyIdFaq");
    },
    /* redirect to another page */
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    toggleTermsDialog : function(component, event, helper) {
        helper.toggleTermsDialog(component, event);       
    },
    saveAgreement : function(component, event, helper) {
        helper.saveAgreement(component, event);       
    },
     home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },    
})
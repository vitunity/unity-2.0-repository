({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplay : function(component, event, bodyId){
        var index = event.currentTarget.id;
        
        var cardBodyId = component.find(bodyId);
        if($A.util.isArray(cardBodyId)){
            $A.util.toggleClass(cardBodyId[index], "slds-hide");
        }else{
            // It will add or remove slds-hide css class for html.
            $A.util.toggleClass(cardBodyId, "slds-hide");
        }
    },    
    /* download documet or redirect to another page */
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        
        if(index){
            var indexes = index.split('-');
            var url='';
            var lstTrueBeamDevMode = component.get("v.lstTrueBeamDevMode");
            if(!$A.util.isEmpty(lstTrueBeamDevMode)){
                
                var doc = lstTrueBeamDevMode[indexes[0]].lstDevMode[indexes[1]];
                var TBtype = lstTrueBeamDevMode[indexes[0]].label;
                if(doc){
                    var docId = doc.Id;
                    if(docId){
                        url = "/tbdevsummary/?Id="+docId+"&TBtype="+TBtype;
                        // cpTBSummary?TBtype={!TBtype}&recid={!l.id}
                        
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": url,
                            isredirect: "true"
                        });
                        urlEvent.fire();
                    }
                    
                } 
            }
        }        
    },
    getAgreementShow : function(component,event){ 
        var action = component.get("c.getAgreementShow");
        action.setCallback(this, function(response) {            
            var state = response.getState();           
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                var agrModal = component.find("agrId");
                if(!result){
                    //this.toggleTermsDialog(component, event);
                    $A.util.addClass(agrModal, "show");
                } else {
                    $A.util.removeClass(agrModal, "show");
                }                     
            }            
        });	
        $A.enqueueAction(action); 
    },
    saveAgreement : function(component,event){ 
        var action = component.get("c.registerAgreement");
        action.setCallback(this, function(response) {            
            var state = response.getState();
            
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                //alert(result); 
                if(result){
                    //this.toggleTermsDialog(component, event);
                    var agrModal = component.find("agrId");
                    $A.util.removeClass(agrModal, "show");
                }else{
                   // alert("Error in saving Agreement");
                }                
            }            
        });	
        $A.enqueueAction(action); 
    },
    getTrueBeamValues : function(component,event){ 
        var action = component.get("c.getTBDevModeValues");
        action.setCallback(this, function(response) {            
            var state = response.getState();           
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                if(!$A.util.isEmpty(response.getReturnValue())){
                    component.set("v.lstTrueBeamDevMode",response.getReturnValue());
                }   


            }            
        });	
        $A.enqueueAction(action);      
    },
    toggleTermsDialog : function(component, event) {
        //alert("test");
        $A.util.toggleClass(component.find("divTermsDialog"), 'slds-hide');
        $A.util.toggleClass(component.find("divBackGroundGray"), 'slds-hide');       
    },
})
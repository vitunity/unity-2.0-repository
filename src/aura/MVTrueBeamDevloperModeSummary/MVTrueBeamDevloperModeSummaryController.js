({
	/* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
    },
    /* download documet */
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    }
})
({
	
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
        var recId =  getUrlParameter('Id');
        component.set("v.recId",recId);
        var TBtype =  getUrlParameter('TBtype');
        component.set("v.TBType",TBtype);
        
        this.getTrueBeamValues(component, event);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getTrueBeamValues : function(component,event){ 
        var action = component.get("c.getTBDevModeSummaryValues");
        
        // Set parameters to apex method.
        action.setParams({"RecID":component.get("v.recId"),
                          "TBtype":component.get("v.TBType")
                         });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue();                
                if(!$A.util.isEmpty(result)){
                    if(!$A.util.isEmpty(result.TBDevMode)){
                        component.set("v.TBDevMode",result.TBDevMode);
                    }
                    if(!$A.util.isEmpty(result.lstDevMode)){
                        component.set("v.lstDevMode",result.lstDevMode);
                    }
                    
                }                
            }            
        });	
        $A.enqueueAction(action); 
    },
    /* download documet */
    openDocument : function(component, event){
        var docId = event.currentTarget.id;
        var url='';
        if(docId){
            var sPageURL = window.location.href;
            var sURLVariables = sPageURL.split('/');
            var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
            
            //var newUrl = sPageURL.replace(strRemove, '');
            var siteURL = $A.get("$Label.c.MvSiteURL");

			//url = newUrl + "sfc/servlet.shepherd/version/download/"+docId;
			url = siteURL + "/sfc/servlet.shepherd/version/download/"+docId;
			window.location.replace(url);
        }
    },
})
({
    doInit : function(component, event, helper)  { 
        helper.getUrlParameter(component, event);
        helper.getUrlParameter2(component, event);
        helper.getCustomLabels(component, event);
        helper.getProductGroupValues(component,event);
        helper.getLiveEventWebinars(component,event,true);
        helper.queryOnDemandWebinars(component,event,true);
    }, 
    onApply : function(component, event, helper){
        helper.getContentVersionList(component, event);
        
    },
    onReset : function(component, event, helper){
        debugger;
        component.find("refineKeyword").set("v.value","");
        component.find("productGroupId").set("v.value","");
        helper.queryOnDemandWebinars(component,event,true);
    },
	onkeyDownWebinars: function(component, event, helper){
		if(event.getParams().keyCode == 13){
            helper.queryOnDemandWebinars(component,event);
            helper.queryLiveWebinars(component,event);
            
        }
    },
    onsetWebinars : function(component, event, helper){
        helper.queryOnDemandWebinars(component,event,true);
    },
    openDocumentEvent : function(component, event, helper){
        helper.openDocument(component,event);
    },
    openDocumentOnDemandEvent : function(component, event, helper){
        helper.openDocumentOnDemand(component,event);
    },
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.queryOnDemandWebinars(component,event,true);
    },
    home: function(component, event, helper) {
        url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
})
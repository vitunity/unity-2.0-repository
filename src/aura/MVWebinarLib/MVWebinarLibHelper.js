({
    
     getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var Id = getUrlParameter('Id');
        component.set("v.Id",Id);
        var lang = getUrlParameter('lang');
        component.set("v.language",lang);
    },
    
    getUrlParameter2 : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam,fParam) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    data.lang = sParameterName[1] === undefined ? true : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.searchText = sParameterName[1];
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('Id','searchText');
        component.set("v.Id",bdata.Id);
        component.find("refineKeyword").set("v.value", bdata.searchText);
        var Id = getUrlParameter('Id');
        component.set("v.Id",Id);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    getProductGroupValues : function(component,event) 
    {        
        var self = this;
        var action = component.get("c.getProds");
        var options = [];
        options.push({
            label : '--Any--',
            value : '1Any'
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++)
                {
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("productGroupId").set("v.options",options);
        });	
        $A.enqueueAction(action); 
    },
    
    getLiveEventWebinars : function(component,event,resetPages) 
    {
        
        var self = this;
        var action = component.get("c.fetchLiveWebinars");
        
        action.setParams({
                          	"recordsOffset":component.get("v.recordsOffset"),
                          	"totalSize":component.get("v.totalSizeLive"),
                          	"pageSize":component.get("v.pageSizeLive")
                         }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var cvlist = response.getReturnValue();
                if(cvlist)
                {
                    component.set("v.liveEventWebinars",cvlist);
                }
            }
        });	
        $A.enqueueAction(action);  
    },
    
    queryOnDemandWebinars : function(component,event,resetPages) 
    {
        if(!resetPages)
        {
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        
        var self = this;
        var action = component.get("c.getOnDemandWebinars");

        action.setParams({
            				"Product":component.find("productGroupId").get("v.value"),
                          	"Document_TypeSearch":component.find("refineKeyword").get("v.value"),
                          	"recordsOffset":component.get("v.recordsOffset"),
                          	"totalSize":component.get("v.totalSize"),
                          	"pageSize":component.get("v.pageSize")
                         }); 
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var cvlist = response.getReturnValue();
                //alert(cvlist);
                //if(cvlist)
                //{
                    for(var i = 0; i < cvlist.lstWebinarWrap.length; i++) {
                    	var doc = cvlist.lstWebinarWrap[i];
                        doc.url = '/s/mvwebsummary?Id='+doc.eid+"&lang="+component.get('v.language');
                    }
                    component.set("v.onDemandEventWebinars",cvlist.lstWebinarWrap);
                    //component.set("v.totalSizeOnDemand",cvlist.totalSize);
                	component.set("v.totalSize",cvlist.totalSize);
                //}
            }
        });		
        $A.enqueueAction(action); 
    },
    
    queryLiveWebinars : function(component,event,resetPages) 
    {
        var self = this;
        var action = component.get("c.getLiveWebinars");

        action.setParams({
            				"Product":component.find("productGroupId").get("v.value"),
                          	"Document_TypeSearch":component.find("refineKeyword").get("v.value")
                         }); 
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var cvlist = response.getReturnValue();
                component.set("v.liveEventWebinars",cvlist);
            }
            
        });		
        $A.enqueueAction(action); 
    },
    
    getOnDemandEventWebinars : function(component,event,resetPages) 
    {
        if(!resetPages)
        {
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        
        var self = this;
        var action = component.get("c.fetchOnDemandWebinars");
        
        action.setParams({
                          	"recordsOffset":component.get("v.recordsOffset"),
                          	"totalSize":component.get("v.totalSize"),
                          	"pageSize":component.get("v.pageSize")
                         }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var cvlist = response.getReturnValue();
                if(cvlist)
                {
                    for(var i = 0; i < cvlist.lstWebinarWrap.length; i++) {
                    	var doc = cvlist.lstWebinarWrap[i];
                        doc.url = '/s/mvwebsummary?Id='+doc.eid+"&lang="+component.get('v.language');
                    }
                    component.set("v.onDemandEventWebinars",cvlist.lstWebinarWrap);
                    component.set("v.totalSize",cvlist.totalSize);
                }
            }
        });	
        $A.enqueueAction(action); 
    },
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var liveEventWebinars = component.get("v.liveEventWebinars");
            if(!$A.util.isEmpty(liveEventWebinars))
            {
                var doc = liveEventWebinars[index];
                var docId = doc.eid;
                url = "/mvwebsummary/?Id="+docId;
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams
                ({
                    "url": url,
                    isredirect: "true"
                });
                urlEvent.fire(); 
            }
        }
    },
    
    openDocumentOnDemand : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var onDemandEventWebinars = component.get("v.onDemandEventWebinars");
            if(!$A.util.isEmpty(onDemandEventWebinars))
            {
                var doc = onDemandEventWebinars[index];
                var docId = doc.eid;
                url = "/mvwebsummary?Id="+docId;
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams
                ({
                    "url": url,
                    isredirect: "true"
                });
                urlEvent.fire(); 
            }
        }
    }
})
({
	doInit : function(component, event, helper) 
    {
		var action = component.get('c.showClearanceItemRecords');
        var inputClearanceId = component.get("v.inputClearanceId");
        var reviewProductId = component.get("v.reviewProductId");
        action.setParams({
            "inputClearanceId": inputClearanceId,
            "reviewProductId": reviewProductId
        });
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set('v.objClassController', response.getReturnValue());
                console.log('obj:'+JSON.stringify(response.getReturnValue()));
            }
        });
        $A.enqueueAction(action);
	},
    
    sectionOne : function(component, event, helper) 
    {
        var acc = component.find('articleOne');
        for(var cmp in acc) 
        {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
     sectionTwo : function(component, event, helper) 
    {
        var acc = component.find('articleTwo');
        for(var cmp in acc) 
        {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
})
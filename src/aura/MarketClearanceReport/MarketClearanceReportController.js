({
	doInit : function(component, event, helper) 
    {
		var action = component.get('c.initClass');
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in objClassController attribute on component
                component.set('v.objClassController', response.getReturnValue());
                component.find("productProfileId").set("v.value",'All');
                component.find("countryId").set("v.value",'All');
                 component.set("v.ProductName","test");
            }
        });
        $A.enqueueAction(action);
	},
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
       hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    getReport : function(component, event, helper) 
    {

        var action = component.get('c.showClearanceData');
        var pp = component.find("productProfileId").get("v.value");
        var country = component.find("countryId").get("v.value");
    

        if(pp == 'All' && country == 'All')
        {

            component.set("v.bothSelected",true);
            
        }
        else
         { 
            
            var obj = component.get("v.objClassController");
            
            action.setParams({
                "productProfileId": pp,
                "countryId": country,
                "ppMap"   : obj.ppMap
            });
            action.setCallback(this,function(response)
            {
                //store state of response
                var state = response.getState();

                if (state === "SUCCESS") 
                {
                    //set response value in objClassController attribute on component
                    component.set('v.inputClearance', response.getReturnValue());
                    component.set("v.bothSelected",false);
                    if(country == 'All')
                    {

                        //component.set("v.multiplecountry",true);
                        component.set("v.multipleProduct",false);
                        component.set("v.ProductName",obj.ppMap[pp]);
                    }
                    if(pp == 'All')
                    {
                       component.set("v.multipleProduct",true);
                       component.set("v.CountryName",obj.ccMap[country]);
                       //component.set("v.multiplecountry",false);
                       var ppList =[];
                        for(var key in obj.ppMap)
                        {
                            ppList.push(obj.ppMap[key]);
                        }
                        component.set("v.AllProductProfile",ppList);
                    }


                    if(pp != 'All' && country != 'All')
                    {
                       component.set("v.ProductName",obj.ppMap[pp]);
                       component.set("v.multipleProduct",false);
                    }
                }
            });
            $A.enqueueAction(action);
         }   
	},

    openModal:function(component,event,helper)
    {    
        
       var id = event.target.id ;
        var cmpEvent = component.getEvent("itemEvent");

        cmpEvent.setParams({
            "reviewProductId" : component.find("productProfileId").get("v.value"),
            "inputClearanceId" : id
        });
        cmpEvent.fire();
    },

    sectionOne : function(component, event, helper) 
    {
        var acc = component.find('articleOne');
        for(var cmp in acc) 
        {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
})
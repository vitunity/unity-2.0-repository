({
	/* get url parameter */
	getUrlParameter : function(component, event) {
		var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   var lang =  getUrlParameter('lang');
       var mtypeVar =  getUrlParameter('mtype')
       component.set("v.mtype",mtypeVar);
	},
    /* Load custome labels */
	getCustomLabels : function(component, event) {
	   //var action = component.get("c.getCustomLabelMap");
       //action.setParams({"languageObj":component.get("v.language")});
       //action.setCallback(this, function(response) {
           //var state = response.getState();
           //if (component.isValid() && state === "SUCCESS") {
            //component.set("v.customLabelMap",response.getReturnValue());
        $("#tab_b").hide();
        $("#tab_a").show();
        $("#Voucher").hide();
        $("#PhantomText").hide();
           //}else if(response.getState() == "ERROR"){
              // $A.log("callback error", response.getError());
           //}      
       //} );
       //$A.enqueueAction(action);
	},
    
     /* Load custome labels */
	getDocuments : function(component, event) {
	   var action = component.get("c.accessDocs");
       action.setParams({"pageName":component.get("v.mtype")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           alert(response.getReturnValue()[0].TitleVal);
           if (component.isValid() && state === "SUCCESS") {
               component.set("v.documents",response.getReturnValue());
               
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    
    /* Load custome labels */
	getMyVarianAgreementHelper : function(component, event) {
	   var action = component.get("c.getMyVarianAgreement");
       action.setParams({
           "name":"MONTE_CARLO",
           "languageCode":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.agreementList",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    
    
    doToggleTabs: function(component, event) {
        var index = event.currentTarget.id;
        alert(index);
        if(index  == 'Voucher'){
            $("#tab_b").hide();
            $("#tab_a").show();
            $("#Voucher").hide();
            $("#PhantomText").hide();
            $("#Phantom").show();
            $("#VoucherText").show();
        }else if(index  == 'Phantom'){
            $("#tab_b").show();
            $("#tab_a").hide();
            $("#Phantom").hide();
            $("#PhantomText").show();
            $("#VoucherText").hide();
            $("#Voucher").show();
            
        }
    },
    changeTabStyle : function(component, tabId){
        console.log("------changeTabStyle1---");
        console.log("------selectedTabAuraId---"+tabId);
        var tabDetails = tabId.split('-');
        var tabAuraId = tabDetails[0];
        var tabContentId = tabDetails[1];
        if(tabAuraId != "homeTab"){             
        	$A.util.removeClass(component.find("homeTab"), "showActive");
            $A.util.removeClass(component.find("APIHomeContent"), "in");
            $A.util.removeClass(component.find("APIHomeContent"), "active");
            
        }
        if(tabAuraId != "newRequestTab"){             
        	$A.util.removeClass(component.find("newRequestTab"), "showActive");
            $A.util.removeClass(component.find("APInewContent"), "in");
            $A.util.removeClass(component.find("APInewContent"), "active");
        }
        $A.util.addClass(component.find(tabId), "showActive");
        $A.util.addClass(component.find(tabContentId), "in");
        $A.util.addClass(component.find(tabContentId), "active");
        console.log("------changeTabStyle2---");
    }
    
})
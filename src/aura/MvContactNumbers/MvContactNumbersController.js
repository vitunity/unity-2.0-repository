({
    
    doInit: function(component, event, helper) {      
      //Fetch the account list from the Apex controller   
      helper.getUrlParameter(component, event);
      helper.getCustomLabels(component, event);
      helper.getSupportContactList(component);
      helper.getPacificSupportContactList(component);
      helper.getAustraliaSupportContactList(component);
      helper.getLatinAmericaSupportContactList(component);
    },
    
    contactUs : function(component, event, helper) {
    	var url = "/contactus?lang=en_US";
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url,
            redirect: true
        });
        urlEvent.fire();  
    },
    
    sectionOne : function(component, event, helper) {
       helper.helperFun(component,event,'articleOne');
    },
    
   sectionTwo : function(component, event, helper) {
      helper.helperFun(component,event,'articleTwo');
    },
   
   sectionThree : function(component, event, helper) {
      helper.helperFun(component,event,'articleThree');
   },
   
   sectionFour : function(component, event, helper) {
      helper.helperFun(component,event,'articleFour');
   },
})
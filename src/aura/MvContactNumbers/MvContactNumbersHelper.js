({
	/* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    helperFun : function(component,event,secId) {
	  var acc = component.find(secId);
        	for(var cmp in acc) {
        	$A.util.toggleClass(acc[cmp], 'slds-show');  
        	$A.util.toggleClass(acc[cmp], 'slds-hide');  
       }
	},
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
                var spinner = component.find("mySpinner");
                $A.util.toggleClass(spinner, "slds-hide");
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },

    // Fetch the accounts from the Apex controller
    getSupportContactList: function(component) {
        var action = component.get('c.getContacts');
    
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.supportContacts', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
  	},
    getPacificSupportContactList: function(component) {
        var action = component.get('c.getPacificContacts');
    
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.supportContactsPacific', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
  	},
    getAustraliaSupportContactList: function(component) {
        var action = component.get('c.getAustraliaContacts');
    
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.supportContactsAustralia', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
  	},
    getLatinAmericaSupportContactList: function(component) {
        var action = component.get('c.getLatinAmericaContacts');
    
        // Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
         component.set('v.supportContactsLatinAmerica', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
  	},
    
    
})
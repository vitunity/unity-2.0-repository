({
	doInit : function(component, event, helper) {
       helper.getCachedCustomLabels(component, event);
        var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   var lang =  getUrlParameter('lang');
       component.set("v.language",lang);
       var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":lang});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
           	component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
		   }      
       } );
       $A.enqueueAction(action);
        
        component.set("v.registerData", new Object);
       
        var action = component.get("c.getCurrentUser");
        action.setCallback(this, function(response) {
            component.set("v.isAuthenticated", response.getReturnValue());
        })
        $A.enqueueAction(action);
        
        helper.isCurrentUserInEuro(component, event);
        helper.getCountry(component, event);
        //helper.getRegCountry(component, event);
        helper.getProd(component, event);
        helper.getProduct(component, event);
        helper.getLanguage(component, event);
        helper.initialiseCaptcha(component, event);
       
    },
    
    /* Set Product Value */
    onChangeProduct: function(component, event, helper){
        component.set("v.contentVersion", null);  
        helper.setProduct(component, event);
    },
    
    /* Reset captcha */
    resetCaptcha: function(component, event, helper){
    	helper.initialiseCaptcha(component, event);
        component.set("v.captchaData", "");
    },
   
     /*onChangeRegCountry: function(component, event, helper){
      helper.setCountry(component, event);
    },8?
    /* Set Preferred Language Value */
    onChangeLanguage: function(component, event, helper){
        component.set("v.contentVersion", null);
        helper.setLanguage(component, event);
    },
    
    setModal : function(component, event, helper) {
    	//debugger;
        var nextModal = event.currentTarget.getAttribute("data-nextModal");
        //alert('nextModal');
        component.set('v.currentModal',nextModal);
    },
    
     /* Toogle the page based on logged in user */
    nextContactRequest : function(component, event, helper) {
		console.log('===='+JSON.stringify(component.get("v.registerData"))); 
        if(helper.validatePersonalFields(component,event) == true){
            //component.set('v.validationMessage','Please fill required fields details');
            return;   
        }

    	var currentModal = component.get('v.currentModal');
        //alert(currentModal);
        /*
    	debugger;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        helper.resetValidation(component,event);
        var counter = 0;
        var fName = component.find("firstNameId").get("v.value");
        var lName = component.find("lastNameId").get("v.value");
        var instiVar = component.find("institutionId").get("v.value");
        var telVar = component.find("telephoneId").get("v.value");
        var addressVar = component.find("addressId").get("v.value");
        var cityVar = component.find("cityId").get("v.value");
        var stateVar = component.find("stateId").get("v.value");
        var postalCodeVar = component.find("postalCodeId").get("v.value");
        var cEmail = component.find("emailId").get("v.value");
        if(fName == undefined || fName == null || fName == ""){
            component.set("v.firstNameId", "true");
            counter=1;
            
        }if(lName == undefined || lName == null){
            component.set("v.lastNameId", "true");
            counter=1;
            
        }if(instiVar == undefined || instiVar == null){
            component.set("v.institutionId", "true");
            counter=counter+1;
        }if(telVar == undefined || telVar == null){
            component.set("v.telephoneId", "true");
            counter=1;
            
        }if(cityVar == undefined || cityVar == null){
            component.set("v.cityId", "true");
            counter=1;
            
        }if(stateVar == undefined || stateVar == null){
            component.set("v.stateId", "true");
            counter=1;
            
        }if(postalCodeVar == undefined || postalCodeVar == null){
            component.set("v.postalCodeId", "true");
            counter=1;
            
        }if(cEmail == undefined || cEmail == null ||  !cEmail.match(regExpEmailformat)){
            component.set("v.emailId", "true");
            counter=1;
            
        }if(addressVar == undefined || addressVar == null){
            component.set("v.addressId", "true");
            counter=1;
            
        }*/
        /*if(helper.validateRequiredFields(component,event) == true){
         	component.set('v.validationMessage','Please fill required fields details');
        	return;   
        }*/
        $('#pInfo').modal('hide');
        $('#'+currentModal).modal('show');
    },
    
    
    
    /*  Redirect to mvPaperDocumentationRequest page*/
    openPaperDoc: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvpaperdocumentrequest"
        });
        urlEvent.fire();
        
    },
    
     /* Get all selected documents */
    checkboxSelect: function(component, event, helper) {
        // get the selected radio value 

        var docSelected = event.currentTarget.dataset.value;
        component.set("v.selectedDocId", docSelected);
        /*

        if (event.source.get("v.value")) {
            // Checkbox is checked - add id to checkedContacts
            if (component.get("v.checkedDocuments").indexOf(id) < 0) {
                component.get("v.checkedDocuments").push(id);
            }
        } else {
        // Checkbox is unchecked - remove id from checkedDocuments
            var index = component.get("v.checkedDocuments").indexOf(id);
            if (index > -1) {
                component.get("v.checkedDocuments").splice(index, 1);
            }
        }
        */
    },
    
    /* Submit request for paper documentation*/
    submit: function(component, event, helper) { 
        
        if(helper.validateDocumentFields(component,event) == true){
            return;   
        }

        /*
        //debugger;
        // create var for store record id's for selected checkboxes  
        var selectedId = [];
        //selectedId = component.get("v.checkedDocuments");
        // get all checkboxes 
        var getAllId = component.find("boxPack");

        //alert(getAllId);
        // play a for loop and check every checkbox values 
        // if value is checked(true) then add those Id (store in Text attribute on checkbox) in selectedId var.
        for (var i = 0; i < getAllId.length; i++) {

            if (getAllId[i].get("v.value") == true) {
                //alert(getAllId[i].get("v.value"));
                selectedId.push(getAllId[i].get("v.text"));
            }
        }
        */
       
        //alert('selectedDocId = ' + component.get("v.selectedDocId"));

        /*
        if(selectedDocId == null || selectedDocId == '') {
            alert('Please select a ducument before submit');
            return;
        }
        */

        //debugger;
        // call the helper function and pass all selected record id's.    
        //helper.PassSelectedContentHelper(component, event, selectedId);
        helper.PassSelectedContentHelper(component, event); //, selectedDocId);
        
    },

    /* Reset button click */
    reset: function(component, event, helper) { 
        component.set("v.mvProduct", null);
        component.set("v.mvLanguage", null);

        //component.find("productValue").set("v.value","");
        //component.find("languageValue").set("v.value","");        
        component.set("v.contentVersion", null);
        component.set("v.validationMessage" , null);
        helper.getProduct(component, event);
        helper.getLanguage(component, event);
    },
        
    sendEmailContact: function(component, event, helper) { 
        
        if(helper.validateEnquiryFields(component,event) == true){
         	component.set('v.validationMessage','Please fill required fields details');
        	return;   
        }
        component.set("v.captchaError", "false"); 
        //Validate captcha error
        var checkVal = helper.validateCaptcha(component,event);
        
        if(checkVal){
            var self = this;
            var fileInput = component.find("file").getElement();
            if(fileInput.files.length > 0){
                var file = fileInput.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    debugger;
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    helper.upload(component,event,file,fileContents);
                    //helper.initialiseCaptcha(component, event);
        			//component.set("v.captchaData", "");
                };
                debugger;
                fr.readAsDataURL(file);
            }else{
                //debugger;
                helper.upload(component,event,"","",false);
                //helper.initialiseCaptcha(component, event);
       		    //component.set("v.captchaData", "");
            }
        }else{
            component.set("v.captchaError", "true");
            component.set("v.captchaData", "");
        }
        
    },
    
    sendWebsiteEmailContact: function(component, event, helper) { 
        
        if(helper.validateWebsiteFields(component,event) == true){
         	component.set('v.validationMessage','Please fill required fields details');
        	return;   
        }
        
        component.set("v.captchaError", "false");

        //Validate captcha error
        var checkVal = helper.validateCaptcha(component,event);
        
        //debugger;
        if(checkVal){
            var self = this;
            var fileInput = component.find("fileEn").getElement();
            if(fileInput.files.length > 0){
                var file = fileInput.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    //debugger;
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    helper.websiteUpload(component,event,file,fileContents);
                    //helper.initialiseCaptcha(component, event);
        			//component.set("v.captchaData", "");
                };
                //debugger;
                fr.readAsDataURL(file);
            }else{
                //debugger;
                helper.websiteUpload(component,event,"","",false);
                //helper.initialiseCaptcha(component, event);
        		//component.set("v.captchaData", "");
            }
        }else{
            component.set("v.captchaError", "true");
            component.set("v.captchaData", "");
        }
        
    },
    
    
    
    sendRequestEmailContact: function(component, event, helper) {
        if(helper.validateRequestFields(component,event) == true){
         	component.set('v.validationMessage','Please fill required fields details');
        	return;   
        }
         component.set("v.captchaError", "false");
        //Validate captcha error
        var checkVal = helper.validateCaptcha(component,event);
        if(checkVal){
            var self = this;
            var fileInput = component.find("fileRequest").getElement();
            if(fileInput.files.length > 0){
                var file = fileInput.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                   // debugger;
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    helper.requestUpload(component,event,file,fileContents);
                    //helper.initialiseCaptcha(component, event);
        			//component.set("v.captchaData", "");
                };
                //debugger;
                fr.readAsDataURL(file);
            }else{
                //debugger;
                helper.requestUpload(component,event,"","",false);
               // helper.initialiseCaptcha(component, event);
        		//component.set("v.captchaData", "");
            }
        }else{
            component.set("v.captchaError", "true");
            component.set("v.captchaData", "");
        }
    },
    
    
    /* Learning center send email to user with  attachment */
	sendLearningEmailContact: function(component, event, helper) { 
        if(helper.validateLearningFields(component,event) == true){
         	component.set('v.validationMessage','Please fill required fields details');
        	return;   
        }
         component.set("v.captchaError", "false"); 
        //Validate captcha error
        var checkVal = helper.validateCaptcha(component,event);
        if(checkVal){
            //debugger;
            var self = this;
             var fileInput = component.find("fileLearn").getElement();
            if(fileInput.files.length > 0){
                var file = fileInput.files[0];
                var fr = new FileReader();
                fr.onload = function() {
                    //debugger;
                    var fileContents = fr.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    helper.learningUpload(component,event,file,fileContents);
                   // helper.initialiseCaptcha(component, event);
        			//component.set("v.captchaData", "");
                };
                //debugger;
                fr.readAsDataURL(file);
            }else{
                debugger;
                helper.learningUpload(component,event,"","",false);
               // helper.initialiseCaptcha(component, event);
        		//component.set("v.captchaData", "");
            }
        }else{
            component.set("v.captchaError", "true");
            component.set("v.captchaData", "");
        }
    
    },
    
    
    /*  Redirect to RequestforQuoteUpdrade page*/ 
   	openRequestForQuote: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/requestforquoteupgrade"
        });
        urlEvent.fire();
    },
    
    /*  Redirect to myVarianWebsiteFeedback page*/ 
    openWebsiteFeedback: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/my-varian-website-feedback"
        });
        urlEvent.fire();
    },
    /*  Redirect to myVarianEnquiry page*/ 
    openMyVarianEnquiry: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/my-varian-enquiry"
        });
        urlEvent.fire();
    },
    
    /*  Redirect to myVarianLearningCenter page*/
    openLearningCenter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/my-varian-learning-center"
        });
        urlEvent.fire();
        isredirect: "true"
    },
    
        /*  Redirect to myVarianLearningCenter page*/
    openContactNumbers: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contactnumbers?lang="+component.get('v.language')
        });
        urlEvent.fire();
        isredirect: "true"
    },
    onChangeCountry: function(component, event, helper) {
        helper.setCountry(component, event);
    },
     onChangeRole: function(component, event, helper) {
        helper.setProd(component, event);
    },
    
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    hideSpinner : function (component, event) {
       component.set("v.Spinner", false); 
    },
    showSpinner : function (component, event) {
       component.set("v.Spinner", true); 
    },
    
},
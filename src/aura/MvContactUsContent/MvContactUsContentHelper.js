({
	/* Load country picklist values */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                //debugger;
                var productGroup = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.accData.Country1__c"))
                    {
                        productGroup.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", productGroup);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError()); 
            }
        });
        $A.enqueueAction(action); 
    },

    isCurrentUserInEuro : function(component, event) {
        var action1 = component.get("c.isCurrentUserInEuro");
        action1.setCallback(this, function(response) {
            component.set("v.isCurrentUserInEuro", response.getReturnValue());
        })
        $A.enqueueAction(action1);
    },   

    /* Load custome labels */
    getCachedCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelCachedMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
     /*Initialise captcha value */
    initialiseCaptcha: function(component,event) {
        component.set("v.char1", this.randomNumber());
        component.set("v.char2", this.randomNumber());
        component.set("v.char3", this.randomNumber());
        component.set("v.char4", this.randomNumber());
        component.set("v.char5", this.randomNumber());
        component.set("v.char6", this.randomNumber());
    },
    /* Generate ramdom number for captcha */
    randomNumber: function(){
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var random = possible.charAt(Math.floor(Math.random() * possible.length))
        return random;
    },
    
     /* Captcha match Validation */
    validateCaptcha: function(component,event) {        
        //debugger;
        var char1 = component.get("v.char1");
        var char3 = component.get("v.char3");
        var char5 = component.get("v.char5");
        var captchaData = component.get("v.captchaData");
        if(captchaData == undefined || captchaData == null){	
            this.initialiseCaptcha(component,event);
            return false;
            
        }else if(captchaData.length != 3){
             this.initialiseCaptcha(component,event);
            return false;
        }else if(captchaData[0].localeCompare(char1) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[1].localeCompare(char3) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[2].localeCompare(char5) != 0){
            return false;
             
        }else{
            return true;
        }
    },

    
    /* Upload file attachment */
	upload:function(component,event,file,fileContents){
            
			var registerMData = component.get("c.registerMData");
            var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");
            var subj = component.get("v.mvSubject");
            var longD = component.get("v.mvLongDesc");
            registerMData.setParams({
                "registerDataObj": rdata,
                "accDataObj": adata,
                "subject": subj,
                "longDes": longD,
                "fileName": file.name,
                "base64Data": encodeURIComponent(fileContents),
                "contentType": file.type,
                "languageObj": component.get("v.language")
    
            });
        	debugger;
        	var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                 	debugger;
                $("#myVarianInquiryNext").modal('hide'); 
                    component.set("v.mvSuccess","true");
					component.set("v.mvSubject", "");
					component.set("v.mvLongDesc", "");
                    component.set("v.Likedisable",false);
					$(".chooseFile").val("").trigger("change");
					this.initialiseCaptcha(component, event);
					component.set("v.captchaData", ""); 
                $("#requestSubmitted").modal('show'); 
                }
               
            });
        
           $A.getCallback(function() {
				$A.enqueueAction(registerMData);
			})();
            
    },
    
    /* upload file attachment for sending email */
    websiteUpload:function(component,event,file,fileContents){
		//debugger;
		
        var registerMData = component.get("c.registerMDataWebsite"); 
        console.log('===='+JSON.stringify(component.get("v.registerData")));    
        var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");
            //var prodVer = component.find("mvProdVersion").get("v.value");
        	var subj = component.get("v.mvSubjectWebsite");
            var longD = component.get("v.mvLongDescWebsite");
            registerMData.setParams({
                "registerDataObj": rdata,
                "accDataObj": adata,
                "subject": subj,
                "longDes": longD,
                "fileName": file.name,
                "base64Data": encodeURIComponent(fileContents),
                "contentType": file.type 
    
            });
        	var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
                var state = response.getState();
                
                 $("#websiteFeedbackNext").modal('hide');
                 component.set("v.mvSuccess","true");
        	     component.set("v.mvSubjectWebsite", "");
                component.set("v.Likedisable",false);
        	     component.set("v.mvLongDescWebsite", "");
        	     $(".chooseFile").val("").trigger("change");
                 this.initialiseCaptcha(component, event);
        		 component.set("v.captchaData", "");
                 $("#requestSubmitted").modal('show');
            });
            $A.getCallback(function() {
				$A.enqueueAction(registerMData);
			})();
        	
        	
    },
    
    /* upload file attachment to send with email */
    requestUpload:function(component,event,file,fileContents){
        
		var registerMData = component.get("c.registerMDataRequest");
            var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");
            var prodVer =  component.find("prodversion").get("v.value");
            var pdata = component.find("prod").get("v.value");   
       		var subj = component.get("v.mvSubjectRequest");
            var longD = component.get("v.mvLongDescRequest");
        
           
            registerMData.setParams({
                "registerDataObj": rdata,
                "accDataObj": adata,
                "prodObj":pdata,
                "prodV": prodVer,
                "subject": subj,
                "longDes": longD,
                "fileName": file.name,
                "base64Data": encodeURIComponent(fileContents),
                "contentType": file.type,
                "languageObj": component.get("v.language")
    		});
        	var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
                var state = response.getState();
                //var errors = response.getError();
                //console.log('-----in ERROR Message'+JSON.stringify(errors));
                //debugger;
                $("#upgradeQouteNext").modal('hide');
                component.set("v.mvSuccess","true");
        	    component.set("v.mvSubjectRequest", "");
        	    component.set("v.mvLongDescRequest", "");
                component.set("v.Likedisable",false);
        	    $(".chooseFile").val("").trigger("change");
                component.find("prodversion").set("v.value", "");
        	    component.find("prod").set("v.value", "");
                this.initialiseCaptcha(component, event);
        	    component.set("v.captchaData", "");
                $("#requestSubmitted").modal('show');
            });
            $A.getCallback(function() {
				 $A.enqueueAction(registerMData);
			})();
        	
			
            
       
    },
    
    
    
    
    /* upload file for attachment in email */
    learningUpload:function(component,event,file,fileContents){
       //debugger;
		var registerMData = component.get("c.registerMDataLearning");
            var rdata = component.get("v.registerData");
            var adata = component.get("v.accData");
            var subj = component.get("v.mvSubjectLearning");
            var longD = component.get("v.mvLongDescLearning");
            //alert(subj);
            //alert(longD);
            //debugger;
            registerMData.setParams({
                "registerDataObj": rdata,
                "accDataObj": adata,
                "subject": subj,
                "longDes": longD,
                "fileName": file.name,
                "base64Data": encodeURIComponent(fileContents),
                "contentType": file.type,
                "languageObj": component.get("v.language")
    
            });
        	var urlEvent = $A.get("e.force:navigateToURL");
            registerMData.setCallback(this, function(response) {
                var state = response.getState();
                  $("#learningCenterNext").modal('hide');
                   component.set("v.mvSuccess","true");
        	       component.set("v.mvSubjectLearning", "");
        	       component.set("v.mvLongDescLearning", "");
                component.set("v.Likedisable",false);
        	       $(".chooseFile").val("").trigger("change");
                   this.initialiseCaptcha(component, event);
        		   component.set("v.captchaData", "");
                  $("#requestSubmitted").modal('show');
            });
            
        	$A.getCallback(function() {
				$A.enqueueAction(registerMData);
			})();
        	
    },
    
     /* Pass selected content documents to controller */
    PassSelectedContentHelper: function(component, event) {//, RecordsIds) {
      //alert(RecordsIds);
        //call apex class method
      var action = component.get('c.registerDocumentMData');
      // pass the all selected record's Id's to apex method
      var rdata = component.get("v.registerData");
      var adata = component.get("v.accData"); 
      //var pdata = component.get("v.prodDataDocument"); 
      var language = component.find("languageValue").get("v.value");

      //debugger;
      action.setParams({
          "registerDataObj": rdata,
          "accDataObj": adata,
          //"pDataObj":pdata,
          "lstRecordId": component.get("v.selectedDocId"), //RecordsIds,
          "Languagevalue": language
      });
    
        var urlEvent = $A.get("e.force:navigateToURL");
        action.setCallback(this, function(response) {
            var state = response.getState();
            $("#paperDocumentNext").modal('hide');
            $("#requestSubmitted").modal('show');
            component.find("productValue").set("v.value","");
            component.find("languageValue").set("v.value","");
            component.set("v.contentVersion", null);
            component.set("v.selectedDocId", null);
        });  
        
      $A.enqueueAction(action);
      
        
     },
    
    validateRequestFields: function(component,event) {
    	var errorFlag = false;
        var prodVar = component.find("prod").get("v.value");
      	var prodversionVar = component.find("prodversion").get("v.value");
      	var subjectIdRequestVar = component.find("subjectIdRequest").get("v.value");
      	var longDescIdRequestVar = component.find("longDescIdRequest").get("v.value");  
        //var requestCaptchaVar =  component.find("requestCaptcha").get("v.value"); 
        
        if(prodVar == undefined || prodVar == null){
        	errorFlag = true;
            $A.util.addClass(component.find("prod"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("prod"), 'errorClass');
        }
        
        if(prodversionVar == undefined || prodversionVar == null){
        	errorFlag = true;
            $A.util.addClass(component.find("prodversion"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("prodversion"), 'errorClass');
        }
        
        
        if(subjectIdRequestVar == undefined || subjectIdRequestVar == null || subjectIdRequestVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("subjectIdRequest"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("subjectIdRequest"), 'errorClass');
        }
        
        if(longDescIdRequestVar == undefined || longDescIdRequestVar == null  || longDescIdRequestVar ==''){
        	errorFlag = true;
            $A.util.addClass(component.find("longDescIdRequest"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("longDescIdRequest"), 'errorClass');
        }
        
        return errorFlag;
        
    },
    
    
    validateWebsiteFields: function(component,event) {
    	var errorFlag = false;
        var subjectIdWebsiteVar = component.find("subjectIdWebsite").get("v.value");
      	var longDescIdWebsiteVar = component.find("longDescIdWebsite").get("v.value");
      	
      
        
        
        
        
        if(subjectIdWebsiteVar == undefined || subjectIdWebsiteVar == null || subjectIdWebsiteVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("subjectIdWebsite"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("subjectIdWebsite"), 'errorClass');
        }
        
        if(longDescIdWebsiteVar == undefined || longDescIdWebsiteVar == null  || longDescIdWebsiteVar ==''){
        	errorFlag = true;
            $A.util.addClass(component.find("longDescIdWebsite"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("longDescIdWebsite"), 'errorClass');
        }
        
        
        
        return errorFlag;
        
    },
    
    
     validateEnquiryFields: function(component,event) {
    	var errorFlag = false;
        var subjectIdVar = component.find("subjectId").get("v.value");
      	var longDescIdVar = component.find("longDescId").get("v.value");
      	
      
        
        
        
        
        if(subjectIdVar == undefined || subjectIdVar == null || subjectIdVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("subjectId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("subjectId"), 'errorClass');
        }
        
        if(longDescIdVar == undefined || longDescIdVar == null  || longDescIdVar ==''){
        	errorFlag = true;
            $A.util.addClass(component.find("longDescId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("longDescId"), 'errorClass');
        }
        
        
        return errorFlag;
        
    },
    
      validatePersonalFields: function(component,event) {

        var errorFlag = false;

        var fName = component.find("firstNameId").get("v.value");
        var lName = component.find("lastNameId").get("v.value");
        var email = component.find("emailId").get("v.value");
        
        if(fName == undefined || fName == null || fName == ''){
            errorFlag = true;
            $A.util.addClass(component.find("firstNameId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("firstNameId"), 'errorClass');
        }        

        if(lName == undefined || lName == null || lName == ''){
            errorFlag = true;
            $A.util.addClass(component.find("lastNameId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("lastNameId"), 'errorClass');
        }    

        if(email == undefined || email == null || email == ''){
            errorFlag = true;
            $A.util.addClass(component.find("emailId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("emailId"), 'errorClass');
        }    

        return errorFlag;
        
    },   
        
    
    validateDocumentFields: function(component,event) {
        var errorFlag = false;
        var productValueVar = component.find("productValue").get("v.value");
        var languageValueVar = component.find("languageValue").get("v.value");
        var selectedDocId = component.get("v.selectedDocId");
        
        if(productValueVar == undefined || productValueVar == null || productValueVar == ''){
            errorFlag = true;
            $A.util.addClass(component.find("productValue"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("productValue"), 'errorClass');
        }
        
        if(languageValueVar == undefined || languageValueVar == null  || languageValueVar ==''){
            errorFlag = true;
            $A.util.addClass(component.find("languageValue"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("languageValue"), 'errorClass');
        }

        if(component.find("boxPack") != undefined && component.find("boxPack").length > 0) {
            if(selectedDocId == undefined || selectedDocId == null  || selectedDocId ==''){
                errorFlag = true;
                component.set("v.validationMessage", "Please select one document before submission.");
                //$A.util.addClass(component.find("tableId"), 'errorClass');
               
            }else{
                //$A.util.removeClass(component.find("tableId"), 'errorClass');
                component.set("v.validationMessage", "");
            }       
        } 
        
        return errorFlag;
        
    },
    
    
    
    
    
    validateLearningFields: function(component,event) {
    	var errorFlag = false;
        var subjectIdLearningVar = component.find("subjectIdLearning").get("v.value");
      	var longDescIdLearningVar = component.find("longDescIdLearning").get("v.value");
      	//var fileLearningVar = component.find("fileLearning").get("v.value");
      
        
        
        
        
        if(subjectIdLearningVar == undefined || subjectIdLearningVar == null || subjectIdLearningVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("subjectIdLearning"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("subjectIdLearning"), 'errorClass');
        }
        
        if(longDescIdLearningVar == undefined || longDescIdLearningVar == null  || longDescIdLearningVar ==''){
        	errorFlag = true;
            $A.util.addClass(component.find("longDescIdLearning"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("longDescIdLearning"), 'errorClass');
        }
        
       
        
        return errorFlag;
        
    },
    
    
    validateRequiredFields: function(component,event) {  
        //debugger;
        var errorFlag = false;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      	var fName = component.find("firstNameId").get("v.value");
      	var lName = component.find("lastNameId").get("v.value");
      	var eVar = component.find("emailId").get("v.value");
      	var telVar = component.find("telephoneId").get("v.value");
      	var insVar = component.find("institutionId").get("v.value");
      	var addvar = component.find("addressId").get("v.value");
      	var cityVar = component.find("cityId").get("v.value");
        var stateVar = component.find("stateId").get("v.value");
        var postVar = component.find("postalCodeId").get("v.value");
        var conVar = component.find("country").get("v.value");
       
       
       
        if(eVar == null || !eVar.match(regExpEmailformat)){
            errorFlag = true;
             $A.util.addClass(component.find("emailId"), 'errorClass');
        }else{
             $A.util.removeClass(component.find("emailId"), 'errorClass');
        }
        if(fName == undefined || fName == null){
        	errorFlag = true;
            $A.util.addClass(component.find("firstNameId"), 'errorClass');
            //component.set("v.firstNameId", "true");
        }else{
            $A.util.removeClass(component.find("firstNameId"), 'errorClass');
            //component.set("v.firstNameId", "false");
        }
        if(lName == undefined || lName == null){
        	errorFlag = true;
             $A.util.addClass(component.find("institutionId"), 'errorClass');
            //component.set("v.lastNameId", "true");
        }else{
            $A.util.removeClass(component.find("institutionId"), 'errorClass');
            //component.set("v.lastNameId", "false");
        }
        /*if(conVar == undefined || conVar == null){
            errorFlag = true;
            $A.util.addClass(component.find("country"), 'errorClass');
        }else{
           $A.util.removeClass(component.find("country"), 'errorClass');
        }*/
        if(insVar == undefined || insVar == null){
            errorFlag = true;
            $A.util.addClass(component.find("lastNameId"), 'errorClass');
        }else{
             $A.util.removeClass(component.find("lastNameId"), 'errorClass');
        }
        /*if(addvar == undefined || addvar == null){
            errorFlag = true;
            //component.set("v.addressId", "true");
        }else{
            //component.set("v.addressId", "false");
        }*/
        if(cityVar == undefined || cityVar == null){
            errorFlag = true;
            $A.util.addClass(component.find("cityId"), 'errorClass');
        }else{
            $A.util.removeClass(component.find("cityId"), 'errorClass');
        }
        if(stateVar == undefined || stateVar == null){
            errorFlag = true;
             $A.util.addClass(component.find("stateId"), 'errorClass');
        }else{
             $A.util.removeClass(component.find("stateId"), 'errorClass');
        }
        if(postVar == undefined || postVar == null){
            errorFlag = true;
             $A.util.addClass(component.find("postalCodeId"), 'errorClass');
        }else{
            $A.util.removeClass(component.find("postalCodeId"), 'errorClass');
        }
        
        return errorFlag;
    },
    
    
    /* Load country picklist values */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var productGroup = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.accData.Country1__c"))
                    {
                        productGroup.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", productGroup);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action); 
    },
    
    /* Load country picklist values */
    getRegCountry : function(component, event) {
        //debugger;
        var action = component.get("c.getDynamicPicklistOptionsForCountry");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.countryData.Name"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* load product value */
    getProd : function(component, event) {
        var action = component.get("c.getDynamicProductPicklistOptions");
        action.setParams({"fieldName":"Product_Group__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var productGroup = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.prodData.Product_Group__c"))
                    {
                        productGroup.push(returnArray[i]);
                    }
                }
                component.set("v.mvProduct", productGroup);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* load product value paper documentation */
    getProduct : function(component, event) {
        //debugger;
        var action = component.get("c.getDynamicPicklistOptionsForProduct");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.prodDataDocument.Product_Name__c"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvProduct", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* Set product value */
    setProduct: function(component, event) 
    {
        component.set("v.contentVersion", null);
        var selectCmp = component.find("productValue");
        component.set("v.prodDataDocument.Product_Name__c", selectCmp.get("v.value"));

        this.getDocuments(component, event);
    },
    
    /* Get language picklist value*/
    getLanguage : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForDocumentLanguage");
        //action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.langData.Document_Language__c"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvLanguage", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
        
    },
    
    
     /* Set language picklist value*/
    setLanguage: function(component, event) 
    {
        component.set("v.contentVersion", null);
        var selectCmp = component.find("languageValue");
        //var selectCmp = 'en';
        component.set("v.langData.Document_Language__c", selectCmp);

        this.getDocuments(component, event);
       
        /*
        var registerMData = component.get("c.ContentVersions"); 
        var product = component.find("productValue").get("v.value");
        //var language = component.find("languageValue").get("v.value");
        //var language = selectCmp.get("v.value");
        
        registerMData.setParams({
              
            "Productvalue": product,
            "Languagevalue": selectCmp
              
        });
        debugger;  
        var urlEvent = $A.get("e.force:navigateToURL");
        registerMData.setCallback(this, function(response) {
        var state = response.getState();
        var returnArray = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.contentVersion", response.getReturnValue());
            }
        });
        $A.enqueueAction(registerMData);
        */
    },

    getDocuments: function(component, event)
    {
        var registerMData = component.get("c.ContentVersions"); 
        var product = component.find("productValue").get("v.value");
        var selectCmp = component.find("languageValue").get("v.value");
        //var language = component.find("languageValue").get("v.value");
        //var language = selectCmp.get("v.value");
        
        registerMData.setParams({
              
            "Productvalue": product,
            "Languagevalue": selectCmp
              
        });
        //debugger;  
        var urlEvent = $A.get("e.force:navigateToURL");
        registerMData.setCallback(this, function(response) {
        var state = response.getState();
        var returnArray = response.getReturnValue();
            if (state === "SUCCESS") {
                component.set("v.contentVersion", response.getReturnValue());
                if(response.getReturnValue().length == 0) {
                    component.set("v.validationMessage", "There is no document found for this Product and Language combination.");
                } else {
                    component.set("v.validationMessage", "");
                }
            }
        });
        $A.enqueueAction(registerMData);        
    },
    
    setCountry: function(component, event) 
    {
		 var selectCmp = component.find("country");
         component.set("v.registerData.MailingCountry", selectCmp.get("v.value"));
	},
    
     /* set product values */
    setProd: function(component, event) 
    {
		 var selectCmp = component.find("prod");
         component.set("v.prodData.Product_Group__c", selectCmp.get("v.value"));
	},
   
})
({
	/* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);              
        helper.getAccountList(component, event);
        helper.getCusDisputeListValues(component, event);    
    },

    onApply : function(component, event, helper){
        helper.getInvoiceList(component,event,true);
    },
    
    callApply : function(component, event, helper){
        if(event.getParams().keyCode == 13){
           helper.getInvoiceList(component,event,true);
         }
    },

    onReset : function(component, event, helper){
        //alert('called');
        component.find("AccountNameId").set("v.value","Select");
        component.find("refineKeyword").set("v.value","");
        $(".statusClass").val("All");
        $(".inputDateFrom").val("");
        $(".inputDateTo").val("");
        helper.getInvoiceList(component,event,true);
    },

	checkAll: function(component, event, helper) {
	  var master = component.find("master");
	  var boxPack = component.find("checkId");
	  if (!Array.isArray(boxPack)) {
	    boxPack = [boxPack];
	  }

	  var val = master.get("v.value");
	  for (var i = 0; i < boxPack.length; i++) {
	    boxPack[i].set("v.value", val);
	  }
	},   

    handleExcelClick : function(component, event, helper){        
       helper.handleExcelClick(component,event);
    },	 

    handleOpenClick : function(component, event, helper){        
       helper.handleOpen(component,event);
    },	

    handlePrintClick : function(component, event, helper){ 
        var invRecords = component.get("v.invoiceList");
        var checkBoxes = $(".checkBoxClass");
        var invIds =[];
        for(var i=0; i<checkBoxes.length; i++) {

            var chkVal = component.find("checkId")[i].get("v.value");
            if(chkVal == true) {
                invIds.push(invRecords[i].invId);
            }
        }

        if(invIds != null && invIds != undefined && invIds != '') {
            helper.handlePrint(component,event);
        } else {
            alert('Please select an invoice to use this functionality.');
        }
    },	

    openPopup : function(component, event, helper) {
    	var invid = event.currentTarget.id;
        var chwin = window.open('/apex/CpCustomerBilling?Id='+invid,'_blank', 'width=800,height=500,scrollbars=1');
        chwin.focus();
    }, 

    handleSaveClick : function(component, event, helper) {
    	helper.saveInvoiceRecords(component, event);
        ////helper.getInvoiceList(component, event, true);
        ////helper.getCusDisputeListValues(component, event); 

        /*
        setTimeout(function(){ 
            var invRecords = component.get("v.invoiceList");
            for(var i=0; i<invRecords.length; i++) {
                component.find("CusDisputeId")[i].set("v.value", invRecords[i].invoice.Reason_Code__c);
            }
        }, 2000);   
        */      
    },

    handleAgingBtnClick : function(component, event, helper) {
        var siteURL = $A.get("$Label.c.MvSiteURL"); 
        var agingUrl = siteURL+'/servlet/servlet.FileDownload?file='+ component.get("v.agingCustomerStatement");

        //alert('agingUrl = ' + agingUrl);
        window.open(agingUrl);
            
    },

    onChangeCode : function(component, event, helper) {
        //alert('called');
        helper.setDisputeCode(component, event);  
    },

    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getInvoiceList(component,event,false);
    },    
})
({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },

    getAgingCustomer : function(component, event) {
        var accid = component.find("AccountNameId") == undefined ? 'All' : component.find("AccountNameId").get("v.value");
        var action = component.get("c.getageingstatement");
        action.setParams({"acctid": accid});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.agingCustomerStatement",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },

    getAccountList : function(component,event){
        var action = component.get("c.getAccountList");
        var options = [];     
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("AccountNameId").set("v.options",options);
        }); 
        $A.enqueueAction(action);
    }, 

    getInvoiceList : function(component,event, resetPages){
        if(resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }

        var action = component.get("c.getInvoices");
        
        //alert(component.find("invoiceStatus").get("v.value"));

        var status = $(".statusClass").val();
         action.setParams({"invoiceStatus": status,
                           "acctid": component.find("AccountNameId").get("v.value"),
                           "frmdate": $(".inputDateFrom").val(),
                           "todate": $(".inputDateTo").val(),
                           "Document_TypeSearch": component.find("refineKeyword").get("v.value"),
                           "recordsOffset":component.get("v.recordsOffset"),
                           "totalSize":component.get("v.totalSize")                           
                          });
        //String invoiceStatus, String acctid, String frmdate, String todate, String Document_TypeSearch
        action.setCallback(this, function(response) {            
            var state = response.getState();   
            //alert(state);
            if(state == "SUCCESS"){
                var invListWrp = response.getReturnValue();
                component.set("v.invoiceList", invListWrp.lstInvoiceWrapper); 
                component.set("v.totalSize", invListWrp.totalSize);
                if(component.find("AccountNameId").get("v.value") != undefined) {
                    this.getAgingCustomer(component, event);
                }
            }             
        }); 
        $A.enqueueAction(action); 
    },   

    getCusDisputeListValues : function(component,event){
        var action = component.get("c.getCusDisputeList");
        var options = [];
        options.push({
            label : '',
            value : ''
        });           
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }     
                component.set("v.lstOfCode",options);    
                this.getInvoiceList(component, event);       
            }

            var invRecords = component.get("v.invoiceList");
            for(var i=0; i<invRecords.length; i++) {
                component.find("CusDisputeId")[i].set("v.options",options);
            }
        }); 
        $A.enqueueAction(action);
    },  

    saveInvoiceRecords : function(component, event) {
        var action = component.get("c.saveRecords");
        var strInvoice = JSON.stringify(component.get("v.invoiceList"));
        console.log(strInvoice);
        //alert(strInvoice);
        var invoices = component.get("v.invoiceList");
        action.setParams({"strInvoiceWrap": strInvoice });        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                alert('Invoice is successfully Saved'); 
                var inv = response.getReturnValue();
                //component.set("v.invoiceList", inv);   
                //this.getInvoiceList(component, event);                  
            }

        }); 
        $A.enqueueAction(action);

    },

    handleExcelClick : function(component, event, helper){
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('/');
        var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
        var newUrl = sPageURL.replace(strRemove, '');
        var url = newUrl + 'apex/CpCustomerBillingExcel';
        window.location.replace(url) ; 
    },   

    handleOpen : function(component, event, helper){

        var invRecords = component.get("v.invoiceList");
        var checkBoxes = $(".checkBoxClass");
        var invIds =[];
        for(var i=0; i<checkBoxes.length; i++) {

            var chkVal = component.find("checkId")[i].get("v.value");
            if(chkVal == true) {
                invIds.push(invRecords[i].invId);
            }
        }

        if(invIds != null && invIds != undefined && invIds != '') {
            var action = component.get("c.getAttIds");
            action.setParams({"invIds": invIds,
                              "openOrPrint": 'Open'
                             });
            action.setCallback(this, function(response) {            
                var state = response.getState();   
                if(state == "SUCCESS"){
                    var attIds = response.getReturnValue(); 
                    //alert('retunred attids from server = ' + attIds);
                       var label = $A.get("$Label.c.Heroku_App_URL");

                      if (attIds != null && attIds != '')
                      {  
                        var ids = attIds.split("%");
                         for (i = 0 ; i < ids.length; i++)
                         {
                            if (ids[i] != null)
                            {  
                                //alert('att id = ' + ids[i]);
                                window.open(label +ids[i]);
                            }
                         }
                      }
                      else 
                      {
                        alert('Please select a PDF file to use this functionality.');
                      }
                                           
                }             
            }); 
            $A.enqueueAction(action); 
        } else {
            alert('Please select an invoice to use this functionality.');
        }        
    },  

    handlePrint : function(component, event, helper){

        var invRecords = component.get("v.invoiceList");
        var checkBoxes = $(".checkBoxClass");
        var invIds =[];
        for(var i=0; i<checkBoxes.length; i++) {

            var chkVal = component.find("checkId")[i].get("v.value");
            if(chkVal == true) {
                invIds.push(invRecords[i].invId);
            }
        }

        if(invIds != null && invIds != undefined && invIds != '') {
            var action = component.get("c.getAttIds");
            action.setParams({"invIds": invIds,
                              "openOrPrint": 'Print'
                             });
            action.setCallback(this, function(response) {            
                var state = response.getState();   
                if(state == "SUCCESS"){
                    var attIds = response.getReturnValue(); 
                    //alert('attIds in print = ' + attIds);
                    var label = $A.get("$Label.c.Heroku_App_URL");

                      if (attIds != null && attIds != '')
                      {  
                         var chwin = window.open(label + attIds);
                      }
                      else 
                      {
                        alert('Please select a PDF file to use this functionality.');
                      }
                                           
                }             
            }); 
            $A.enqueueAction(action); 
        } else {
            alert('Please select an invoice to use this functionality.');
            return null;
        }             
    },  

    setDisputeCode : function(component, event) 
    {
        var codes = $(".reasonCodeClass");
        var invRecords = component.get("v.invoiceList");
        for(var i=0; i<codes.length; i++) {
            var changedValue = $("#CusDisputeId"+i).val();
            invRecords[i].invoice.Reason_Code__c = changedValue;
        }
    },
})
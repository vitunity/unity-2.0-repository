({
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.pageLoad(component, event);   
        helper.handleSearch(component,event);
    },
    
    handleSearch : function(component,event,helper){
        component.set('v.searchType','');
        helper.handleSearch(component,event);
    },
   
    home:function(component,event,helper){
    	helper.goHome(component,event);
    },
    
    productDocumentation: function(component,event,helper){
    	helper.goProductDocumentation(component,event);
    },
    webinar: function(component,event,helper){
    	helper.goWebinar(component,event);
    },
    
    onMultiSelectChange: function(component,event,helper){
    	helper.handleSearch(component,event);
    }, 
        
    
})
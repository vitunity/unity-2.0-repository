({
    pageLoad : function(component, event){
        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var urlSearch=getUrlParameter('searchText');
        var urlRedirect=getUrlParameter('redirectFlag');
        
        if(!$A.util.isEmpty(urlSearch) && !$A.util.isUndefined(urlSearch))
        {
            component.set("v.searchText", urlSearch);
        }
        else{
            component.set("v.searchText", '');
        }
        if(!$A.util.isEmpty(urlRedirect) && !$A.util.isUndefined(urlRedirect))
        {
            component.set("v.redirectFlag", urlRedirect);            
        }
        else
        {
            component.set("v.redirectFlag", false); 
        }    
        
        var action = component.get("c.pageLoad");
        action.setParams({ searchText : component.get("v.searchText"),
                          redirectFlag: component.get("v.redirectFlag")  });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:', state);
            if ( state == "SUCCESS") {
                var resp = response.getReturnValue();
                console.log('The response is ::::'+resp +'The response size is:::'+resp.length);
                if(!$A.util.isEmpty(resp) && !$A.util.isUndefined(resp)){             
                    component.set('v.searchResponseMap', resp);
                    var myMap = component.get("v.searchResponseMap");
                    for(Object.key in myMap)
                    {                        
                        debugger;
                        if(Object.key==="Documentations")
                        {                                
                            component.set('v.docResponse',myMap[Object.key].respObj);
                            console.log(myMap[Object.key].respObj);
                        }
                        if(Object.key==="ProductIdeas")
                        {                               
                            component.set('v.prodResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="Webinars")
                        {                                
                            component.set('v.webinarResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="Events")
                        {                              
                            component.set('v.eventResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="selectOptions")
                        {                            
                            component.set('v.selOptions',myMap[Object.key].selOpt);
                        }
                    }
                }
                
            }
            else
            {
                console.log('The response is :::'+state);
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    
    /* get url parameter */
	getUrlParameter : function(component, event) {
		var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   var lang =  getUrlParameter('lang');
       component.set("v.language",lang);
   	},
	
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    handleSearch: function(component,event){  
        component.set("v.searchResponseMap", []);
        component.set("v.docResponse", []);
        component.set("v.prodResponse" , []);
        component.set("v.webniarResponse" , []);
        component.set("v.eventResponse" , []);
        
        var resp ;
        var action = component.get("c.globalSearchResult");
        var all = component.find("InputSelectMultiple").get("v.value");
        if(all == undefined){
            all = 'All';
        }
        action.setParams({ searchText : component.find("searchTxtID").get("v.value"),
                           selOption:all});
        action.setCallback(this, function(response){
            var state = response.getState();
            debugger;
            console.log('state:', state);
            if ( state == "SUCCESS") {                
                resp = response.getReturnValue();
                console.log('The response is :::'+resp);
                if(!$A.util.isEmpty(resp) && !$A.util.isUndefined(resp)){
                    console.log('The response is One more time :::'+resp);                    
                    component.set('v.searchResponseMap', resp);
                    component.set('v.searchType',all);
                    console.log(component.get('v.searchResponseMap'));
                    var myMap = component.get("v.searchResponseMap");
                    for(Object.key in myMap)
                    {
                        console.log('The key is ::'+Object.key);
                        if(Object.key==="Documentations")
                        {
                            component.set('v.docResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="ProductIdeas")
                        {
                            component.set('v.prodResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="Webinars")
                        {
                            component.set('v.webniarResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="Events")
                        {
                            component.set('v.eventResponse',myMap[Object.key].respObj);
                        }
                        if(Object.key==="selectOptions")
                        {
                            component.set('v.selOptions',myMap[Object.key].selOpt);                                  
                        }                        
                    }                    
                }                
            }            
        });
        $A.enqueueAction(action);        
    },	
    
    goHome :function(component,event){
    	var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    goProductDocumentation :function(component,event){
    	var url = "/productdocumentation?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    docSelected : function(component, event) {
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/productdocumentationdetail?Id=",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    goWebinar :function(component,event){
    	var url = "/webinars?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
})
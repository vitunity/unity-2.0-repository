({
	docViewMore : function(component, event) {
        console.log('Inside docViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            //"url": "/productdocumentation?lang="+component.get("v.language"),
            "url": "/productdocumentation?lang="+component.get("v.language")+"&searchText="+component.get("v.searchText"),
            isredirect: "true"
        });
        urlEvent.fire();
		
	},
    
    docSelected : function(component, event) {
        console.log('Inside docViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        var docId = event.currentTarget.getAttribute("data-craid");
        urlEvent.setParams({
            "url": "/productdocumentationdetail?Id="+docId+"&lang="+component.get("v.language"),
            isredirect: "true"
        });
        urlEvent.fire();
    }
})
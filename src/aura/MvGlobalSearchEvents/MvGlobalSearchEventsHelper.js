({
	eventViewMore : function(component, event) {
        console.log('Inside docViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/events?lang="+component.get('v.language'),
            isredirect: "true"
        });
        urlEvent.fire();
		
	},
    
    eventSelected : function(component, event) {
        var eventId = event.currentTarget.getAttribute("data-craid");
        console.log('Inside docViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/mvrecentevents?Id="+eventId+"&lang="+component.get('v.language'),
            isredirect: "true"
        });
        urlEvent.fire();
    }
})
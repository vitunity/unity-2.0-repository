({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
	productViewMore : function(component, event) {
        console.log('Inside product Mored');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/productideas?lang="+component.get("v.language")+"&searchText="+component.get("v.searchText"),
            isredirect: "true"
        });
        urlEvent.fire();
		
	},
    
    productSelected : function(component, event) {
        var ideaID = event.currentTarget.getAttribute("data-craid");
        console.log('Inside Product Selected');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        console.log('ideaID = ' + ideaID);
        urlEvent.setParams({
            //"url": "/error",
            "url": "/productideas?Id="+ideaID+"&lang="+component.get("v.language"),
            isredirect: "true"
        });
        urlEvent.fire();
    },

})
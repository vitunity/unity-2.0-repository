({
	webinarViewMore : function(component, event) {
        console.log('Inside webinarViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/webinars?lang="+component.get("v.language")+"&searchText="+component.get("v.searchText"),
            isredirect: "true"
        });
        urlEvent.fire();
		
	},
    
    webinarSelected : function(component, event) {
        var webinarId = event.currentTarget.getAttribute("data-craid");
        console.log('Inside webinarViewMore');
        var urlEvent = $A.get("e.force:navigateToURL");
        console.log('The url Event is :::'+urlEvent);
        urlEvent.setParams({
            "url": "/mvwebsummary?Id="+webinarId,
            isredirect: "true"
        });
        urlEvent.fire();
    }
})
({
    doInit: function(component, event, helper) {
       helper.getUrlParameter(component, event);
       helper.getCustomLabels(component, event);
       helper.getUser(component, event); 
       helper.getUserDetails(component, event);
       helper.getContact(component, event);
    },
	
    Yes: function(component, event, helper) 
    {
        helper.updateContact(component, event);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myaccount?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
        var cmpBack = component.find('divBackGroundGray');
        var cmpExpiredBox = component.find('expiredBox');
        $A.util.removeClass(cmpExpiredBox,'slds-fade-in-open');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
    },
    
    No: function(component, event, helper) 
    {
        helper.updateContact(component, event);
        var cmpBack = component.find('divBackGroundGray');
        var cmpExpiredBox = component.find('expiredBox');
        $A.util.removeClass(cmpExpiredBox,'slds-fade-in-open');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
    },
    
    events: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    
    register: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/userregistration?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    
    contactus: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contactus?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    
    myaccount: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myaccount?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    
    home: function(component, event, helper) {
        var url = "/";
        if(component.get("v.isloggedIn")){
            url = "/homepage?lang="+component.get('v.selectedLanguage');
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    openMyAccount: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myaccount?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    openProductDocumentation: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/productdocumentation?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    }, 
    openInstallationSupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/softwareinstallation?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openHardwareRequirements: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.varian.com/oncology/products/software/information-systems/aria-ois-radiation-oncology?cat=resources#hardwarespecs",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openPartnerChannel: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/partnerchannel?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    openSupportCases: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/supportcases?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openProductIdeas: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/productideas?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openRPCLungPhantom: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvrpclungphantom?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openMDADLLungPhantom: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mdadllungphantom?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openOncoPeerCommunity: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/OCSUGC/apex/OCSUGC_Home",
            isredirect: "true"
        });
        //urlEvent.fire();
        window.open('/OCSUGC/apex/OCSUGC_Home','_blank');
    }, 
    
    openEclipseapisupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://variandeveloper.codeplex.com/",
            isredirect: "true"
        });
        urlEvent.fire();
    },    
 
    handleResearchMenuSelect : function (component, event,helper) {
        var menuValue = event.detail.menuItem.get("v.value");
        switch(menuValue) {
            case "MonteCarlo": helper.openMonteCarlo(component, event); break;
            case "DeveloperMode": helper.openDeveloperMode(component, event); break;
            case "Eclipse": helper.openEclipseApiSupport(component, event); break;
        }
    },
    
    openCourses: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/trainingcourses?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    }, 
    openLearningCenter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://varian.oktapreview.com/api/v1/groups/",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openPeerTraining: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvpeertopeer?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openCertificateManager: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://webapps.varian.com/certificate/en",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    openEvents: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/events?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    }, 
    openWebinars: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/webinars?lang=",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    openMarketplace: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://sfqa1-varian.cs77.force.com/vMarketLogin",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openMarketingYourCenter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/marketingkit?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    
    registrationPage: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvregistration?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },    
    cpMonteCarlo: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarlo?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    handleGlobalSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var searchTxt=component.get('v.globalSearchText');
        console.log('Deepak :::The search text from Header is :::'+searchTxt);
        urlEvent.setParams({
            "url": "/globalsearch?searchText="+searchTxt+"&redirectFlag=true",
           
        });
        urlEvent.fire();
    },
    /* Login action*/
    customLogin: function(component, event, helper) {
        //alert('Getting Logged In.......');
        console.log('Login---');

        var username = component.get("v.username");
        var password = component.get("v.password");
        //Validation
        if ($A.util.isEmpty(username) || $A.util.isUndefined(username)) {
            alert('Username is Required');
            return;
        }
        if ($A.util.isEmpty(password) || $A.util.isUndefined(password)) {
            alert('Password is Rqquired');
            return;
        }
        var actionLogin = component.get("c.loginCommunity");
        actionLogin.setParams({
            "username": username,
            "password": password
        });
        actionLogin.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var urlEvent = $A.get("e.force:navigateToURL");
                //alert('Login==' + response.getReturnValue());
                window.open(response.getReturnValue(),'_self');
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getState());
			    window.open(response.getReturnValue(),'_self');
            }
        });
        $A.enqueueAction(actionLogin);
    },
    /* Logout action*/
    logout: function(component, event, helper) {
        var logoutCustom = component.get("c.logoutCustom");
        logoutCustom.setCallback(this, function(response) {
            //alert("Logout="+response.getReturnValue());
            var urltest = response.getReturnValue();
            alert(urltest);
            window.location.replace('/apex/MvLogoutPage');
        });
        $A.enqueueAction(logoutCustom);
    },
    /* Logout action*/
    pickLanguage: function(component, event, helper) {
        var url = window.location.href; 
        //alert(url);
        //var value = url.substr(0,url.lastIndexOf('/') + 1);
        //alert(value);
        //window.history.back();
        //window.location.href = url+"?lang="+component.get("v.language");
        var paramName='lang';
        var paramValue = component.get("v.selectedLanguage");
        var pattern = new RegExp('(\\?|\\&)('+paramName+'=).*?(&|$)')
        var newUrl=url;
        if(url.search(pattern)>=0){
            newUrl = url.replace(pattern,'$1$2' + paramValue + '$3');
        }
        else{
            newUrl = newUrl + (newUrl.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
        }
        window.location.href = newUrl;
   },
    savePreferredLanguages : function(component, event, helper) {
        helper.savePreferredLanguages(component, event);
    },
    openLanguageDialog : function(component, event, helper) {
        helper.toggleLanguageDialog(component, event);
        helper.getPreferredLanguageOptions(component, event);
    },
})
({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        //alert(lang);
        component.set("v.language",lang);
        if(lang){
          component.set("v.selectedLanguage",lang);  
        }
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /* Get User */
    getUser : function(component, event) {
        var getUser = component.get("c.isLoggedIn");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isloggedIn", response.getReturnValue());
                var isLoggedIn = component.get("v.isloggedIn");
                if(isLoggedIn == true){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/homepage"
                    });
                    //urlEvent.fire();
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },
    openMonteCarlo: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarlo?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openDeveloperMode: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/truebeamdevmode?lang="+component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openEclipseApiSupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://variandeveloper.codeplex.com/discussions",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    toggleLanguageDialog : function(component, event) {
        //alert("test");
        $A.util.toggleClass(component.find("divLangDialog"), 'slds-hide');
        $A.util.toggleClass(component.find("divBackGroundGray"), 'slds-hide');       
    },
    /* Get Contact prefered Language*/
    getUserDetails : function(component, event) {
        var getUser = component.get("c.getLoggedInUser");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var usr = response.getReturnValue();
                component.set("v.userObj",usr);
                if(!$A.util.isEmpty(usr)){                    
                    if(!$A.util.isEmpty(usr.Contact)){
                        if(usr.Contact.Is_Preferred_Language_selected__c){
                            if($A.util.isEmpty(usr.Contact.Preferred_Language1__c)){
                                this.toggleLanguageDialog(component, event);
                                this.getPreferredLanguageOptions(component, event);
                            }else{
                                if($A.util.isEmpty(component.get("v.language")) || component.get("v.language") == "en"){
                                    this.translateLanguage(component, event,usr.Contact.Preferred_Language1__c); 
                                }                                
                            }                            
                        }else{
                            this.toggleLanguageDialog(component, event);
                            this.getPreferredLanguageOptions(component, event, usr.Contact.Preferred_Language1__c);
                        }
                    }
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },    
    getPreferredLanguageOptions : function(component,event,defaultValue){
        var action = component.get("c.getPreferredLanguages");
        
        var options = [];
        options.push({
            label : 'Please Select',
            value : ''
        });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("PreferredLanguagesId").set("v.options",options);
            if(! $A.util.isEmpty(defaultValue)){
                component.find("PreferredLanguagesId").set("v.value",defaultValue);  
            }            
        });	
        $A.enqueueAction(action);
    },
    savePreferredLanguages : function(component,event){
        var action = component.get("c.savePreferredLanguage");
        
        action.setParams({
            "language":component.get("v.preferredLanguage")
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var saveResult = response.getReturnValue();
                if(saveResult){
                    this.toggleLanguageDialog(component, event);
                    this.translateLanguage(component, event,component.get("v.preferredLanguage"));
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Error."
                    });
                    toastEvent.fire();
                }
            }
        });	
        $A.enqueueAction(action);
    },
    translateLanguage: function(component, event, language) {
        var url = window.location.href;
        var supportedLanguages = $A.get("$Label.c.MV_SupportedLanguages");
        var lstSupportedLanguages  = []
        lstSupportedLanguages = supportedLanguages.split(",");
        var paramValue ;
        lstSupportedLanguages.forEach(function(item){
            var langs  = []
            langs = item.trim().split(":");
            if (langs.indexOf(language) > -1) {                
                if(langs.length > 1){
                    paramValue = langs[1];   
                }                    
            }
        });
        
        var paramName='lang';
        if(!paramValue){
            paramValue = "en_US";
        }
        if(component.get("v.language") == paramValue ){//
            return;
        }
       // component.set("v.language",paramValue);
       // var paramValue = component.get("v.language");
        var pattern = new RegExp('(\\?|\\&)('+paramName+'=).*?(&|$)')
        var newUrl=url;
        if(url.search(pattern)>=0){
            newUrl = url.replace(pattern,'$1$2' + paramValue + '$3');
        }
        else{
            newUrl = newUrl + (newUrl.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
        }
        window.location.href = newUrl;
   },
    getContact : function(component, event)
    {
        var action = component.get("c.getContactInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var usr = response.getReturnValue();
                component.set("v.loggedInUser", usr);
                if(usr.Id && !usr.ShowAccPopUp__c)
                {
                    var cmpTarget = component.find('expiredBox');
                    var backGroundGray = component.find('divBackGroundGray');
                    $A.util.addClass(cmpTarget,'slds-fade-in-open'); 
                    $A.util.removeClass(backGroundGray, 'slds-hide'); 
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    updateContact : function(component, event)
    {
        var action = component.get("c.saveMyContact");
        action.setCallback(this, function(response) 
                           {
                               var state = response.getState();
                               if(state == "SUCCESS")
                               {
                                   var str = response.getReturnValue();
                               }
                               else if (state == "ERROR") 
                               {
                                   var errors = response.getError();
                               }
                           });	
        $A.enqueueAction(action);
    },    
})
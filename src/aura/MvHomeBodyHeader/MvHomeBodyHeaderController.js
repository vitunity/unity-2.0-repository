({
	doInit: function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getOnDemandWebinars(component, event);
        helper.loadFeaturedStories(component, event);
        helper.getUser(component, event);
    },
    /* Login action*/
    handleLoginClick: function(component, event, helper) {
       helper.customLogin(component, event);
    },
    /* Register action*/
    handleRegisterClick: function(component, event, helper) {
        var url="/registration";
        helper.navigateToURL(component, event,url);
    },
    /* Forgot Password action*/
    handleForgoPasswordClick: function(component, event, helper) {
        var url="/forgot-password";
        helper.navigateToURL(component, event,url);
    },
    /* open Oncopeer Community Link*/
    openOncopeerCommunityLink: function(component, event, helper) {
        var url="/oncopeer";
        helper.navigateToURL(component, event,url);
    },
    /* open Certificate Manager Link*/
    openCertificateManagerLink: function(component, event, helper) {
        var url="https://webapps.varian.com/certificate/en";
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Marketplace Link*/
    openVarianMarketplaceLink: function(component, event, helper) {
        var url="https://sfqa1-varian.cs77.force.com/vMarketLogin";
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Medical affairs Link*/
    openVarianMedicalaffairsLink: function(component, event, helper) {
        var url="http://medicalaffairs.varian.com/";
        helper.navigateToURL(component, event,url);
    },
    /* open All Webinars  Link*/
    openWebinarsLink: function(component, event, helper) {
        var url="/webinars";
        helper.navigateToURL(component, event,url);
    },
     /* open Webiars Link*/
    openWebinarLink: function(component, event, helper) {
         helper.openWebinarLink(component, event);     
    },
    /* open Centerline Link*/
    openCenterlineLink: function(component, event, helper) {
        var url="https://www.varian.com/oncology/events-resources/centerline";
        helper.navigateToURL(component, event,url);
    },
    /* open Varian Medical affairs Link*/
    openSportonLink: function(component, event, helper) {
        var url="https://www.varian.com/proton-therapy/spot-blog/home";
        helper.navigateToURL(component, event,url);
    },
    /* open Featured Story Link*/
    openFeaturedStoryLink: function(component, event, helper) {
         helper.openFeaturedStoryLink(component, event);     
    },
})
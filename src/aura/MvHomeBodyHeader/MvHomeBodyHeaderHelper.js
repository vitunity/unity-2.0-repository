({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        //alert(lang);
        component.set("v.language",lang);
        if(lang){
            component.set("v.selectedLanguage",lang);  
        }
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Login action*/
    customLogin: function(component, event) {
        //alert('Getting Logged In.......');
        console.log('Login---');
        
        var Email = component.get("v.username");
        var password = component.get("v.password");
        
        //Validation
        if ($A.util.isEmpty(Email) || $A.util.isUndefined(Email)) {
            alert('Email is Required');
            return;
        }
        if ($A.util.isEmpty(password) || $A.util.isUndefined(password)) {
            alert('Password is Rqquired');
            return;
        }			
        
        var pageName = component.get("v.pageName");
        var actionLogin = component.get("c.loginCommunity");
        actionLogin.setParams({
            "username": Email,
            "password": password
        });
        actionLogin.setCallback(this, function(response) {
            var state = response.getState();
            // alert(response.getReturnValue());
            if (component.isValid() && state === "SUCCESS") {
                var url = response.getReturnValue();                
                var newUrl = url.replace("/homepage", '/'+pageName);
                window.open(newUrl,'_self');
            } else if (response.getState() == "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        component.set("v.loginError",errors[0].message);
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                
                //alert(response.getError());
                $A.log("callback error", response.getError());
                //window.open(response.getReturnValue(),'_self');
            }
        });
        $A.enqueueAction(actionLogin);
    },
    
    /* navigate To URL*/
    navigateToURL: function(component, event, url) {
        // var url = "https://webapps.varian.com/certificate/en";
        if(url){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                isredirect: "true"
            });
            urlEvent.fire(); 
        }        
    },
    /* Load Webinars */
    getOnDemandWebinars : function(component, event) {
        var action = component.get("c.getOnDemandWebinars");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lstWebinars = response.getReturnValue();
               // alert(lstWebinars);
                component.set("v.lstWebinars",lstWebinars);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Load Featured Stories */
    loadFeaturedStories : function(component, event) {
        var action = component.get("c.getFeaturedStories");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lstFeaturedStories = response.getReturnValue();
                //alert(lstFeaturedStories);
                component.set("v.lstFeaturedStories",lstFeaturedStories);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* open Featured Story Link*/
    openFeaturedStoryLink: function(component, event) {
        var index= event.currentTarget.id;
        var url;
        
        var lstFeaturedStories = component.get("v.lstFeaturedStories");
        if(!$A.util.isEmpty(lstFeaturedStories)){
            var item = lstFeaturedStories[index];
            if(item){
               url = item.link; 
            }
        }
        if(url){
          this.navigateToURL(component, event,url);  
        }        
    },
    /* open Webinar Link*/
    openWebinarLink: function(component, event) {
        var id = event.currentTarget.id;
        var url;
        
        if(id){
            url="/mvwebsummary?Id="+id; // mvwebsummary?Id=a0O0n0000007LTeEAM
        }       
        if(url){
          this.navigateToURL(component, event,url);  
        }        
    },
    /* Get User */
    getUser : function(component, event) {
        var getUser = component.get("c.isLoggedIn");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isloggedIn", response.getReturnValue());
                var isLoggedIn = component.get("v.isloggedIn");
                if(isLoggedIn == true){
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/homepage"
                    });
                    urlEvent.fire();
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },
})
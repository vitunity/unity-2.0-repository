/*
 * Author: 
 */
({
    //doInit: function(component, event, helper) {
        //helper.searchAction(component, '0060n000002ANbXAAW')
    //},
    /*
     * Executes the search server-side action when the c:InputLookupEvt is thrown
     */
    handleInputLookupEvt: function(component, event, helper){
        //alert(' =====  '+ component.get("v.value"));
		helper.searchAction(component, event.getParam('searchString')); 
        //helper.searchAction(component, component.get("v.value"));
    },
    
    /*
    	Loads the typeahead component after JS libraries are loaded
    */
    initTypeahead : function(component, event, helper){
        //first load the current value of the lookup field and then
        //creates the typeahead component
        helper.loadFirstValue(component);
    },
    handleValueChange : function (component, event, helper) {
        // handle value change
        console.log("old value: " + event.getParam("v.accaddress"));
        console.log("current value: " + event.getParam("value"));
        console.log("accountSector current value: " + event.getParam("accountSector"));
    }
})
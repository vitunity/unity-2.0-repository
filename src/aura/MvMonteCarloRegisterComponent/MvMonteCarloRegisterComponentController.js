({
	doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
		var getContact = component.get("c.getContact");
        getContact.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.contactObj", response.getReturnValue());
                component.set("v.contactId", response.getReturnValue().Id);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getContact);
	},
    
    onGroup: function(component, event) {
        var selected = event.getSource().get("v.text");
        component.set("v.projectLenghtType",selected);
        //alert(selected);
    },
    
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    openmodal: function(component,event,helper) {
        //debugger;
        var requestedData = component.find("requestedData");
        var requestedDataLabel = component.find("requestedDataLabel");
        var valueRequestedData = requestedData.get("v.value");
        var labelRequestedData = requestedDataLabel.get("v.value");
        if ($A.util.isEmpty(valueRequestedData) || $A.util.isUndefined(valueRequestedData)) {
            alert(labelRequestedData+' is Required');
            return;
        }
        
        var instentToUse = component.find("instentToUse");
        var instentToUseLabel = component.find("instentToUseLabel");
        var valueIntent = instentToUse.get("v.value");
        var labelIntent = instentToUseLabel.get("v.value");
        if ($A.util.isEmpty(valueIntent) || $A.util.isUndefined(valueIntent)) {
            alert(labelIntent+' is Required');
            return;
        }
        
        var investigator = component.find("investigator");
        var investigatorLabel = component.find("investigatorLabel");
        var valueinvestigator = investigator.get("v.value");
        var labelinvestigator = investigatorLabel.get("v.value");
        if ($A.util.isEmpty(valueinvestigator) || $A.util.isUndefined(valueinvestigator)) {
            alert(labelinvestigator+' is Required');
            return;
        }
        
        var investigatorCo = component.find("investigatorCo");
        var investigatorCoLabel = component.find("investigatorCoLabel");
        var valueinvestigatorCo = investigatorCo.get("v.value");
        var labelinvestigatorCo = investigatorCoLabel.get("v.value");
        if ($A.util.isEmpty(valueinvestigatorCo) || $A.util.isUndefined(valueinvestigatorCo)) {
            alert(labelinvestigatorCo+' is Required');
            return;
        }
        
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    
    callRregisterMonteCarlo: function(component,event,helper) {
        var registerMData = component.get("c.registerMData");
        var contactId = component.get("v.contactId");
        var rdata = component.get("v.registerData");
        var projectLenght = component.get("v.projectLenght");
        var projectLenghtType = component.get("v.projectLenghtType");
        registerMData.setParams({
            "contactId": contactId,
            "registerDataObj": rdata,
            "projectLenght": projectLenght,
            "projectLenghtType": projectLenghtType,
            "mtype":component.get('v.mtype')
        });
        var urlEvent = $A.get("e.force:navigateToURL");
        registerMData.setCallback(this, function(response) {
            var state = response.getState();
            //alert("In registerData call back function::"+state);
            if (component.isValid() && state === "SUCCESS") {
                //alert('Save==' + response.getReturnValue());
                var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.removeClass(cmpBack,'slds-backdrop--open');
                $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
                urlEvent.setParams({
                    "url": "/"+response.getReturnValue()
                });
                urlEvent.fire();
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getState());
			    var cmpTarget = component.find('Modalbox');
                var cmpBack = component.find('Modalbackdrop');
                $A.util.removeClass(cmpBack,'slds-backdrop--open');
                $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
            }
        });
        $A.enqueueAction(registerMData);
    } 

})
({
	/* get url parameter */
	getUrlParameter : function(component, event) {
		var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   var lang =  getUrlParameter('lang');
       var mtypeVar =  getUrlParameter('mtype')
       component.set("v.mtype",mtypeVar);
	},
    /* Load custome labels */
	getCustomLabels : function(component, event) {
	   var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    closeModalSubmit:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    }
})
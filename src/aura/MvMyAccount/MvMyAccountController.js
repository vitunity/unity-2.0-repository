({
	doInit : function(component, event, helper) 
    {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        
        helper.getRemoteServiceTools(component, event);
        //helper.getMyFavoritesOld(component, event);
        helper.getMyFavorites(component, event);
        helper.getLoggedInUser(component, event);
        helper.getSalutation(component, event);
        helper.getPreferredLanguage(component, event);
        helper.getSpecialty(component, event);
        helper.getFunctionalRole(component, event);
        helper.getCountry(component, event);
        helper.getRecoveryQuestion(component, event);
        helper.getProdList1(component, event);
        helper.getProdList2(component, event);
        helper.getInstitution(component, event);
        helper.getContactUpdate(component, event);
        helper.getOptionsCountry(component, event);
        helper.setSubscribe(component, event);
        

        component.find("RecoveryQuestion").set("v.value","In what city did you meet your spouse/significant other?");

        setTimeout(function(){ 
        var cmpTarget = component.find('contentId');
        $A.util.removeClass(cmpTarget, 'hide'); 

        }, 400);

        setTimeout(function(){ 

        var b = component.get('c.hideSpinner');
        $A.enqueueAction(b);

        }, 700);
	},

    hideSpinner : function (component, event, helper) {
        var cmpTarget = component.find('spinnerId');
        $A.util.addClass(cmpTarget, 'hide'); 
    },
    showSpinner : function (component, event, helper) {
        var cmpTarget = component.find('spinnerId');
        $A.util.removeClass(cmpTarget, 'hide');  
    },
    
    uploadUserProfileImage : function(component, event, helper) {
        helper.uploadImage(component, event);
	},
	saveContactUpdate : function(component, event, helper) 
    {
        helper.saveContactU(component, event);
	},

	saveContact : function(component, event, helper) 
    {
        helper.saveContact(component, event);
	},
    
    saveOptions : function(component, event, helper){
        helper.saveOptions(component, event);
    },
    
    onChangeSalutation: function(component, event, helper){
        helper.setSalutation(component, event);
    },
    
    onChangeLanguage: function(component, event, helper){
        helper.setLanguage(component, event);
    },
    onChangeRole: function(component, event, helper){
        helper.setRole(component, event);
    },
    
    onChangeSpeciality: function(component, event, helper){
        helper.setSpeciality(component, event);
    },
    
    onChangeCountry: function(component, event, helper){
        helper.setCountry(component, event);
    },
    
    onChangeVerifypasswrd: function(component, event, helper){
        helper.verifypasswrd(component, event);
    },
    
    onChangeConfirmpass: function(component, event, helper){
        helper.confirmpass(component, event);
    },
    onChangeCancelreset: function(component, event, helper){
        helper.cancelreset(component, event);
    },
    onChangePassvaluechk: function(component, event, helper){
        helper.passvaluechk(component, event);
    },
    onChangeQuestion: function(component, event, helper){
        helper.recoveryvaluechk(component, event);
    },
    onChangeFnPONsubs: function(component, event, helper){
        helper.fnPONsubs(component, event);
    },
    onChangeFnPONUNsubs: function(component, event, helper){
        helper.fnPONUNsubs(component, event);
    },
    openInstDialog: function(component, event, helper){
        helper.instDialog(component, event);
    },
    closeModalWindow: function(component, event, helper){
        helper.closeModal(component, event);
    },
    onChangeCountry1: function(component, event, helper){
        helper.setCountry1(component, event);
    },
    onRegeditNow: function(component, event, helper){
        helper.regeditNow(component, event);
    },
    handleAuthorizeClick: function(component, event, helper){
        helper.saveAuthorization(component, event);
    },
    onChangeSetSubscribe: function(component, event, helper){
        helper.setSubscribe(component, event);
    },
    popupPhoto: function(component, event, helper){
        helper.popupPhotoWindow(component, event);
    },
    homeRedirect: function(component, event, helper){
       helper.goHome(component, event);
    },
    removeCRAClick:function(component, event, helper){
    	helper.doRemoveCRA(component, event);
    },

    onClickDoResearch:function(component, event, helper){
       var doResearch = document.getElementById("box").checked;
       component.set("v.loggedInUser.Research_Contact__c", doResearch);
    },

    onClickMyProfile:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('myProfileIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('myProfileSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","myProfile");
        
    },

    onClickMyNotification:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('notificationIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('notificationSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","notification");
        
    },
    onClickSecurity:function(component, event, helper)
    {
       var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('securityIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('securitySpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","security");
        
    },
    onClickAPIKey:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('apiIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('apiSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","api");
    },
    onClickLMS:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('LmsIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('LmsSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","Lms");
    },
    onClickSuppIns:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('supportedIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('supportedSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","supported");
    },
    onClickRemote:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('remoteIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('remoteSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","remote");
    },
    onClickMyFav:function(component, event, helper)
    {
        var currentSelection = component.get("v.CurrentSelection");

        var favBoldTag = component.find('favIconId');
        $A.util.addClass(favBoldTag, 'active');

        var favSpanTag = component.find('favSpanId');
        $A.util.addClass(favSpanTag, 'li-color-toggle');

        var myProfileSpanTag = component.find(currentSelection+'SpanId');
        $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

        var myProfileBoldTag = component.find(currentSelection+'IconId');
        $A.util.removeClass(myProfileBoldTag, 'active');

        component.set("v.CurrentSelection","fav");
    }


})
({
    doInit: function(component, event, helper) {
        helper.getUrlParameter(component, event);
        ////helper.getRecoveryQuestion(component, event);
        
        helper.getCustomLabels(component, event);
        helper.getUser(component, event);
        helper.getUserDetails(component, event);
        helper.getParterChannelUsr(component, event);
        helper.getContact(component, event);
        helper.getParterChannelUsr(component, event);
        helper.getPTInstalltionUsr(component, event);
        helper.isCurrentUserInSancCountry(component, event);
        helper.isCurrentUserInApprovedCountry(component, event);

        helper.getMyQuestions(component, event);
        helper.getPreferredLanguage(component, event);
    },

    onChangeVerifypasswrd: function(component, event, helper){
        helper.verifypasswrd(component, event);
    },
    
    onChangeConfirmpass: function(component, event, helper){
        helper.confirmpass(component, event);
    },
	
    resetPasswordFirstTime: function(component, event, helper){
        helper.resetPasswordHelper(component, event);
    },
    
    onChangeQuestion: function(component, event, helper){
        helper.getRecoveryQuestion(component, event);
        helper.recoveryvaluechk(component, event);
    },
    
    confirmAccountInfo: function(component, event, helper) {
        helper.updateContact(component, event);
    },

    callUpdateUserLMS: function(component, event, helper) {
        helper.updateUserLMS(component, event);
    },

    confirmAccountInfoNo: function(component, event, helper) {
        helper.updateContactNo(component, event);
    },

    events: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    register: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/userregistration?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    contactus: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contactus?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    myaccount: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myaccount?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    home: function(component, event, helper) {
        var url = "/";
        if (component.get("v.isloggedIn")) {
            url = "/homepage?lang=" + component.get('v.selectedLanguage');
        }
        //alert('in home of new header cmp controller');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },

    openMyAccount: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myaccount?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },

    openProductDocumentation: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/productdocumentation?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openInstallationSupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/softwareinstallation?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openPTInstallationSupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/ptInstallation?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },    
    openHardwareRequirements: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.varian.com/oncology/products/software/information-systems/aria-ois-radiation-oncology?cat=resources#hardwarespecs",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openPartnerChannel: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/partnerchannel?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },

    openSupportCases: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/supportcases?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openProductIdeas: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/productideas?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openRPCLungPhantom: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvrpclungphantom?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openMDADLLungPhantom: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mdadllungphantom?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openOncoPeerCommunity: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/OCSUGC/apex/OCSUGC_Home",
            isredirect: "true"
        });
        //urlEvent.fire();
        window.open('/OCSUGC/apex/OCSUGC_Home', '_blank');
    },

    openEclipseapisupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://variandeveloper.codeplex.com/",
            isredirect: "true"
        });
        urlEvent.fire();
    },

    handleResearchMenuSelect: function(component, event, helper) {
        var menuValue = event.detail.menuItem.get("v.value");
        switch (menuValue) {
            case "MonteCarlo":
                helper.openMonteCarlo(component, event);
                break;
            case "DeveloperMode":
                helper.openDeveloperMode(component, event);
                break;
            case "Eclipse":
                helper.openEclipseApiSupport(component, event);
                break;
        }
    },

    developerMode: function(component, event, helper) {
        helper.openDeveloperMode(component, event);
    },

    eclipse: function(component, event, helper) {
        helper.openEclipseApiSupport(component, event);
    },

    monteCarlo: function(component, event, helper) {
        helper.openMonteCarlo(component, event);
    },

    openCourses: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/trainingcourses?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openTPaas: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({            
            "url": "/treatmentplanningservices?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openLearningCenter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://varian.oktapreview.com/api/v1/groups/",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openPeerTraining: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvpeertopeer?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openCertificateManager: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://webapps.varian.com/certificate/en",
            isredirect: "true"
        });
        urlEvent.fire();
    },

    openEvents: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/events?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openWebinars: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/webinars?lang=",
            isredirect: "true"
        });
        urlEvent.fire();
    },

    openMarketplace: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://sfqa1-varian.cs77.force.com/vMarketLogin",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openMarketingYourCenter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/marketingkit?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },

    registrationPage: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvregistration?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    cpMonteCarlo: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarlo?lang=" + component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
    handleGlobalSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var searchTxt = $('#serachText').val();
        urlEvent.setParams({
            "url": "/globalsearch?searchText=" + searchTxt + "&redirectFlag=true"+"&lang="+component.get("v.language")
        });
        urlEvent.fire();
    },
    formGlobalSearchPress: function(component, event, helper) {
        if (event.keyCode === 13) {
            var urlEvent = $A.get("e.force:navigateToURL");
            var searchTxt = $('#serachText').val(); //component.get('v.globalSearchText');
            urlEvent.setParams({
                "url": "/globalsearch?searchText=" + searchTxt + "&redirectFlag=true"+"&lang="+component.get("v.language")
            });
            urlEvent.fire();
        }
    },
    /* Login action*/
    customLogin: function(component, event, helper) {
        //alert('Getting Logged In.......');
        console.log('Login---');
        var username = component.get("v.username");
        var password = component.get("v.password");
        //Validation
        if ($A.util.isEmpty(username) || $A.util.isUndefined(username)) {
            alert('Username is Required');
            return;
        }
        if ($A.util.isEmpty(password) || $A.util.isUndefined(password)) {
            alert('Password is Rqquired');
            return;
        }
        var actionLogin = component.get("c.loginCommunity");
        actionLogin.setParams({
            "username": username,
            "password": password
        });

        actionLogin.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                window.localStorage.setItem('recentLogin', true);
                var urlEvent = $A.get("e.force:navigateToURL");
                alert('Login==' + response.getReturnValue() + '&lang=' + component.get("v.language"));
                window.open(response.getReturnValue() + '&lang=' + component.get("v.language"), '_self');
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getState());
                window.open(response.getReturnValue(), '_self');
            }
        });
        $A.enqueueAction(actionLogin);
    },
    /* Logout action*/
    logout: function(component, event, helper) {
        var logoutCustom = component.get("c.logoutCustom");
        logoutCustom.setCallback(this, function(response) {
            //alert("Logout="+response.getReturnValue());
            var urltest = response.getReturnValue();
            //alert(urltest);
            window.location.replace('/apex/MvLogoutPage');
        });
        $A.enqueueAction(logoutCustom);
    },
    /* Logout action*/
    pickLanguage: function(component, event, helper) {
        helper.doPickLanguage(component, event);
    },
    savePreferredLanguages: function(component, event, helper) {
        helper.savePreferredLanguages(component, event);
    },

    preferredLanguageChange: function(component, event, helper) {
        ////var pLanguage = $('#custom-drop-2').val();
        ////component.set("v.preferredLanguage", pLanguage);

        var selectCmp = component.find("Language");
        component.set("v.loggedInUser.Preferred_Language1__c", selectCmp.get("v.value"));   
        component.set("v.preferredLanguage", selectCmp.get("v.value"));     
    },

    handleCheck: function(component, event, helper) {
		var a = event.getSource();
		var id = a.getLocalId();
        if(id=="emailOptInId") {
			component.find("emailOptInId").set("v.checked", true);
            component.find("emailOptOutId").set("v.checked", false);
        } 

        if(id=="emailOptOutId") {
			component.find("emailOptInId").set("v.checked", false);
            component.find("emailOptOutId").set("v.checked", true);            
        }
    },

    saveMarCom: function(component, event, helper) {
        helper.saveMarComData(component, event);
    },
    hideMarcomDialog: function(component, event, helper) {
        $('#MarComDialog').modal('hide');
        var usr = component.get("v.userObj");
        if (usr.Contact.ShowAccPopUp__c == false) {
            $('#contactAddress').modal('show');
        }
    }
    

    /*
    openLanguageDialog: function(component, event, helper) {
        helper.toggleLanguageDialog(component, event);
        helper.getPreferredLanguageOptions(component, event);
    },
    */

})
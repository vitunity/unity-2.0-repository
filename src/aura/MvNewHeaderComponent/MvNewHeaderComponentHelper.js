({
    /* get url parameter */
    getUrlParameter: function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang = getUrlParameter('lang');
        if (lang) {
            component.set("v.language", lang);
            component.set("v.selectedLanguage", lang);
        }else{
            component.set("v.language", "en");
        }
    },
    /* Load custome labels */
    getCustomLabels: function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({
            "languageObj": component.get("v.language")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap", response.getReturnValue());
                if(component.get("v.selectedLanguage") != '' )
                $('.selectpicker').val(component.get("v.selectedLanguage"));
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* get Recovery Questions */
    getMyQuestions : function(component, event) {
        var action = component.get("c.getMyRecQuestions");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
               // component.set("v.myFavorites", response.getReturnValue());
               //alert('return = ' + JSON.stringify(response.getReturnValue()));
                this.translateMyQuestions(component, event, response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* translate Recovery Questions using custom label MV_RecoveryQuestions*/
    translateMyQuestions: function(component, event, myFavorites) {

        if(myFavorites.length > 0){
            var allMyFavorites = component.get("v.customLabelMap.MV_RecoveryQuestions");
            
            var lstAllFavorites  = []
            lstAllFavorites = allMyFavorites.split(",");
            if(myFavorites.length > 0){            
                lstAllFavorites.forEach(function(item){
                    var favs  = []
                    favs = item.trim().split(":");
                    //alert('favs = ' + favs);
                    if(favs.length > 1){
                        var i;
                        for (i = 0; i < myFavorites.length; i++) {
                            //alert('favs = ' + favs[0]);
                            if(favs[0] == myFavorites[i].optionValue) {
                                myFavorites[i].optionValue = favs[1];
                                myFavorites[i].optionLabel = favs[1];
                                myFavorites[i].optionTranslatedLabel = favs[1];
                                break;
                            }
                        }
                    }                    
                });
            } 
        }

        //alert('final myQuestions = ' + JSON.stringify(myFavorites));
        component.set("v.myQuestions", myFavorites);
    },
    /* Get User */
    getUser: function(component, event) {
		/*Get User */
        var getUser = component.get("c.isLoggedIn");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isloggedIn", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
        /*Get Marketing User */
        var marUser = component.get("c.isMarketingUser");
        marUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isMarketing", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(marUser);
    },

    isCurrentUserInSancCountry : function(component, event) {
        var action1 = component.get("c.isCurrentUserInSancCountries");
        action1.setCallback(this, function(response) {
            component.set("v.isCurrentUserInSancCountry", response.getReturnValue());
        })
        $A.enqueueAction(action1);
    },   

    isCurrentUserInApprovedCountry : function(component, event) {
        var action1 = component.get("c.isCurrentUserInCaseApprovCountries");
        action1.setCallback(this, function(response) {
            component.set("v.isCurrentUserInAprvCountry", response.getReturnValue());
        })
        $A.enqueueAction(action1);
    },   

    /* Get Channel Partner Group user */
    getParterChannelUsr : function(component, event, helper) {
        var action = component.get("c.isPartnerChannelUsr");
        action.setCallback(this,function(response){
            var status = response.getState();
            if(status === "SUCCESS"){
                component.set("v.isPartnerChannelUsr", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    /* Get PT Installation Group user */
    getPTInstalltionUsr : function(component, event, helper) {
        var action = component.get("c.isCurUserPTInstGrp");
        action.setCallback(this,function(response){
            var status = response.getState();
            if(status === "SUCCESS"){
                component.set("v.isPTInstallationUsr", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    openMonteCarlo: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarlo?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openDeveloperMode: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/truebeamdevmode?lang=" + component.get('v.selectedLanguage'),
            isredirect: "true"
        });
        urlEvent.fire();
    },
    openEclipseApiSupport: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://variandeveloper.codeplex.com/discussions",
            isredirect: "true"
        });
        urlEvent.fire();
    },
    toggleLanguageDialog: function(component, event) {
        //alert("test");
        $A.util.toggleClass(component.find("divLangDialog"), 'slds-hide');
        $A.util.toggleClass(component.find("divBackGroundGray"), 'slds-hide');
    },
    /* Get Contact prefered Language*/
    getUserDetails: function(component, event) {
        var getUser = component.get("c.getLoggedInUser");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var usr = response.getReturnValue();
                component.set("v.userObj", usr);
                //alert(usr.Contact.Is_Preferred_Language_selected__c);
                //alert(usr.Contact.ShowAccPopUp__c);
                //alert(usr.Contact.PasswordReset__c);
                if (!$A.util.isEmpty(usr)) {
                    if (!$A.util.isEmpty(usr.Contact)) {
                        if (usr.Contact.PasswordReset__c == false) {
                            $('#securityReset').modal('show');
                        }else if (usr.Contact.Is_Preferred_Language_selected__c == false) {
                            ////$('.custom-drop-2').val(usr.Contact.Preferred_Language1__c);
                            ////component.find("Language").set("v.options", usr.Contact.Preferred_Language1__c);
                            $('#prefferedLanguage').modal('show');
                        } else if ((usr.Contact.Email_Opt_in__c == false &&  
                                   	 usr.Contact.HasOptedOutOfEmail == false) || 
                                  	(usr.Contact.Email_Opt_in__c == undefined && 
                                     usr.Contact.HasOptedOutOfEmail == undefined)) {
                            var recentLogin = window.localStorage.getItem('recentLogin');
                            if(recentLogin === "true" || usr.Contact.ShowAccPopUp__c == false) {
                                window.localStorage.removeItem('recentLogin');
                            	$('#MarComDialog').modal('show');    
                            }
                        } else if (usr.Contact.ShowAccPopUp__c == false) {
                            $('#contactAddress').modal('show');
                        }
                    }else {
                        //$('#employeeMessage').modal('show');
                    }
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },

    /*
    getPreferredLanguageOptions: function(component, event, defaultValue) {
        var action = component.get("c.getPreferredLanguages");

        var options = [];
        options.push({
            label: 'Please Select',
            value: ''
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var picklistresult = response.getReturnValue();
                for (var i = 0; i < picklistresult.length; i++) {
                    options.push({
                        label: picklistresult[i].label,
                        value: picklistresult[i].pvalue
                    });
                }
            }
            component.find("PreferredLanguagesId").set("v.options", options);
            if (!$A.util.isEmpty(defaultValue)) {
                component.find("PreferredLanguagesId").set("v.value", defaultValue);
            }
        });
        $A.enqueueAction(action);
    },
    */

    getPreferredLanguage : function(component, event) {
        var action = component.get("c.getLanguagePicklistOptn");
        action.setParams({"fieldName":"Preferred_Language1__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Languages = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    Languages.push({
                        label: returnArray[i].split(',')[0],
                        value: returnArray[i].split(',')[1]
                    });
                    /*
                    if(returnArray[i] != component.get("v.registerData.Preferred_Language1__c"))
                    {
                        Languages.push(returnArray[i]);
                    }*/
                }
                component.set("v.mvPreferredLanguage", Languages);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },


    savePreferredLanguages: function(component, event) {
        var action = component.get("c.savePreferredLanguage");
        action.setParams({
            "language": component.find("Language").get("v.value")  //$("#custom-drop-2").val()
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var saveResult = response.getReturnValue();
                if (saveResult) {
                    $('#prefferedLanguage').modal('hide');
                    ////$('#custom-drop').val($('#custom-drop-2').val());
                    $('#custom-drop').val(component.find("Language").get("v.value"));
                    this.doPickLanguage(component, event);
                    /*var usr = component.get("v.userObj");
                    if (!$A.util.isEmpty(usr)) {
                        //$('#contactAddress').modal('hide');
                        if (!$A.util.isEmpty(usr.Contact)) {
                            if (usr.Contact.ShowAccPopUp__c == false) {
                                $('#contactAddress').modal('show');
                            }
                        }
                    }*/
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": "Error."
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    doPickLanguage: function(component, event) {
        var url = window.location.href;
        var languageCode = $('#custom-drop').val();
        component.set("v.selectedLanguage", languageCode);
        var paramName = 'lang';
        var paramValue = component.get("v.selectedLanguage");
        var pattern = new RegExp('(\\?|\\&)(' + paramName + '=).*?(&|$)')
        var newUrl = url;
        if (url.search(pattern) >= 0) {
            newUrl = url.replace(pattern, '$1$2' + paramValue + '$3');
        } else {
            newUrl = newUrl + (newUrl.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
        }
        window.location.href = newUrl;
    },
    
    translateLanguage: function(component, event, language) {
        var url = window.location.href;
        var supportedLanguages = $A.get("$Label.c.MV_SupportedLanguages");
        var lstSupportedLanguages = []
        lstSupportedLanguages = supportedLanguages.split(",");
        var paramValue;
        lstSupportedLanguages.forEach(function(item) {
            var langs = []
            langs = item.trim().split(":");
            if (langs.indexOf(language) > -1) {
                if (langs.length > 1) {
                    paramValue = langs[1];
                }
            }
        });

        var paramName = 'lang';
        if (!paramValue) {
            paramValue = "en_US";
        }
        if (component.get("v.language") == paramValue) { //
            return;
        }
        // component.set("v.language",paramValue);
        // var paramValue = component.get("v.language");
        var pattern = new RegExp('(\\?|\\&)(' + paramName + '=).*?(&|$)')
        var newUrl = url;
        if (url.search(pattern) >= 0) {
            newUrl = url.replace(pattern, '$1$2' + paramValue + '$3');
        } else {
            newUrl = newUrl + (newUrl.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
        }
        window.location.href = newUrl;
    },

    getContact: function(component, event) {
        var action = component.get("c.getContactInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var usr = response.getReturnValue();
                component.set("v.loggedInUser", usr);

                var isMDADL = component.get("v.loggedInUser.Is_MDADL__c");
                var role = component.get("v.loggedInUser.Functional_Role__c");

                component.find("emailOptInId").set("v.checked", usr.Email_Opt_in__c);
                component.find("emailOptOutId").set("v.checked", usr.HasOptedOutOfEmail);

                if(role == 'Account Payable Capital'
                    || role == 'Account Payable Capital & Service'
                    || role == 'Account Payable Service') {

                    component.set("v.isAccountPayable", true);
                }
                
                //if (usr.Id && !usr.ShowAccPopUp__c) {
                    var cmpTarget = component.find('expiredBox');
                    var backGroundGray = component.find('divBackGroundGray');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.removeClass(backGroundGray, 'slds-hide');
                //}

                /*
                var tarPage;
                if(isMDADL == true) {
                    tarPage = "mdadllungphantom";
                } else {
                    tarPage = "homepage";
                }

                alert('tarPage = ' + tarPage);

                if(tarPage == "mdadllungphantom") {
                    alert('true return');
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/" + tarPage
                    });
                    urlEvent.fire();
                
                }
                */

            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /*
    getContact: function(component, event) {
        var action = component.get("c.getContactInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var usr = response.getReturnValue();
                component.set("v.loggedInUser", usr);
                if (usr.Id && !usr.ShowAccPopUp__c) {
                    var cmpTarget = component.find('expiredBox');
                    var backGroundGray = component.find('divBackGroundGray');
                    $A.util.addClass(cmpTarget, 'slds-fade-in-open');
                    $A.util.removeClass(backGroundGray, 'slds-hide');
                }
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    */

    updateContactNo: function(component, event) {
        var action = component.get("c.saveMyContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var str = response.getReturnValue();
                var usr = component.get("v.userObj");
                if (!$A.util.isEmpty(usr)) {
                    $('#contactAddress').modal('hide');
                    window.open("/s/myaccount?lang=" + component.get('v.selectedLanguage'),"_self");
                    /*
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": "/myaccount?lang=" + component.get('v.selectedLanguage')
                    });
                    urlEvent.fire();
                    */
                }
            } else if (state == "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },
    
    updateContact: function(component, event) {
        var action = component.get("c.saveMyContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var str = response.getReturnValue();
                var usr = component.get("v.userObj");
                if (!$A.util.isEmpty(usr)) {
                    $('#contactAddress').modal('hide');
                }
            } else if (state == "ERROR") {
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },
    
    getRecoveryQuestion : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Recovery_Question__c"});
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var returnArray = response.getReturnValue();
                component.set("v.mvRecoveryQuestion", returnArray);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    }, 
    
    verifypasswrd: function(component, event) 
    {
        var Exp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
        var pwd = component.find("newPassword").get("v.value");
        if(pwd == undefined){
        	document.getElementById('newpass').style.display = 'block';
            validate = false;
        }else{
            document.getElementById('newpass').style.display = 'none';
        }
        if(!Exp.test(pwd) || pwd.length < 8)
        {
           alert('Password must contain at least one uppercase letter, one lowercase letter, one number,and 8 characters .');
           return false;
        }
    },
    
    
    confirmpass: function(component, event) 
    {
        var oldpwd = component.find("newPassword").get("v.value");
        var newpwd = component.find("confirmPassword").get("v.value");
        if(oldpwd != newpwd)
        {
            document.getElementById('confirmpass').style.display = 'block';
        }
        else
        {
            document.getElementById('confirmpass').style.display = 'none';
        }
     },
    
    recoveryvaluechk : function(component, event) {
        var recoveryQuestion = document.getElementById("RecoveryQuestionNew").value;   //// component.find('recoveryQuestion').get('v.value');
	},
    
    resetPasswordHelper : function(component, event) {
		
		
		
        var validate = true;
        //alert('here 23232');
        var recoveryQuestion = document.getElementById("RecoveryQuestionNew").value;   //// component.find('recoveryQuestion').get('v.value');
        //alert('recoveryQuestion = ' + recoveryQuestion);
        // if(recoveryQuestion == undefined){
        //     recoveryQuestion = component.get('v.mvRecoveryQuestion')[0];
        // }

        //alert('recoveryQuestion = ' + recoveryQuestion);
        var recoveryAnswer = component.find('recoveryAnswer').get('v.value');
        //alert('recoveryAnswer = ' + recoveryAnswer);
        /*
        if(recoveryAnswer == undefined){
        	document.getElementById('qAns').style.display = 'block';
            validate = false;
        }else{
            document.getElementById('qAns').style.display = 'none';
        }
        */

        if ($A.util.isEmpty(recoveryAnswer) || $A.util.isUndefined(recoveryAnswer)) {
            $A.util.addClass(component.find("recoveryAnswer"), 'errorClass');
            validate = false;
        }else{
            $A.util.removeClass(component.find("recoveryAnswer"), 'errorClass');
        } 

        var pwd = component.find("newPassword").get("v.value");
        /*
        if(pwd == undefined){
        	document.getElementById('newpass').style.display = 'block';
            validate = false;
        }else{
            document.getElementById('newpass').style.display = 'none';
        }
        */
        if ($A.util.isEmpty(pwd) || $A.util.isUndefined(pwd)) {
            $A.util.addClass(component.find("newPassword"), 'errorClass');
            validate = false;
        }else{
            $A.util.removeClass(component.find("newPassword"), 'errorClass');
        }             
        var oldpwd = component.find("newPassword").get("v.value");
        var newpwd = component.find("confirmPassword").get("v.value");
        if(oldpwd != newpwd)
        {
            document.getElementById('confirmpass').style.display = 'block';
            validate = false;
        }
        else
        {
            document.getElementById('confirmpass').style.display = 'none';
        }
        
		
        if(validate == true){
			
        	    
            //document.getElementById('activeresetbtn').style.display = 'none';
            //document.getElementById('inactiveresetbtn').style.display = '';
			
            var action = component.get("c.resetpassword");
            action.setParams({"recoveryQuestion":recoveryQuestion,
                              "recoveryAnswer":recoveryAnswer,
                              "newPassword":pwd});
            action.setCallback(this, function(response){
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") { 
                    $('#securityReset').modal('hide');
					var usr = component.get("v.userObj");		
					
                    if (!$A.util.isEmpty(usr.Contact)) {
						
                        if (usr.Contact.Is_Preferred_Language_selected__c == false) {
                            //alert('PasswordReset is true');
                            $('#prefferedLanguage').modal('show');
                            
                        } /* else if (usr.Contact.PasswordReset__c == false) {	
   							//alert('here 123');	
							$('#securityReset').modal('show');					
						} */						
                    }
                } else if (response.getState() == "ERROR") {
                    $A.log("callback error", response.getError()[0].message);
                    alert('Error');
                    alert(response.getError()[0].message);
                    
                }
            });
            $A.enqueueAction(action);
        }
        
	},
    saveMarComData: function(component, event) {
		var emailOptIn = false;
        var emailOptOut = false;
        if(component.find("emailOptInId").get("v.checked")===true) {
			emailOptIn = true;
        } 
        if(component.find("emailOptOutId").get("v.checked")===true) {
            emailOptOut = true;
        }

        var action = component.get("c.saveMarCommData");
        action.setParams({"emailOptIn": emailOptIn,
                          "emailOptOut": emailOptOut
                         });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var usr = component.get("v.userObj");
                $('#MarComDialog').modal('hide');
                if (usr.Contact.ShowAccPopUp__c == false) {
                	$('#contactAddress').modal('show');
            	}
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError()[0].message);
                alert('Error');
                alert(response.getError()[0].message);
                
            }
        });
        $A.enqueueAction(action);
    },

    updateUserLMS: function(component, event) {
        var action = component.get("c.updateUserLMSMethod");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
})
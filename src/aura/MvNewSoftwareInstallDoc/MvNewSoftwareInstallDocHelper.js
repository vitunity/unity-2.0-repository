({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
                 var spinner = component.find("mySpinner");
				 $A.util.toggleClass(spinner, "slds-hide");
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getProductGroupValues : function(component,event) 
    {        
        // var action = component.get("c.getPicklistValues");
        var self = this;
        // action.setParams({"fieldName" : "Product_Group__c", "sObjectName" : "Product2"});
        var action = component.get("c.getProdOptions");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                console.log(picklistresult);
                
                for(var i =0;i<picklistresult.length;i++)
                {
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("productGroupId").set("v.options",options);
            self.getDocumentType(component);
            //self.getDocumentVersions(component,event);
            //self.getContentVersionList(component,event);
            
        });	
        $A.enqueueAction(action); 
        //self.getDocumentType(component,event);
        //self.getDocumentVersions(component,event);
    },
    getDocumentType : function(component) 
    {   
        var self = this;
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        // 
        //var action = component.get("c.getPicklistValues");
        //action.setParams({"fieldName" : "Document_Type__c", "sObjectName" : "ContentVersion"});
        var action = component.get("c.getDocTypes");
        action.setParams({"Productvalue":component.find("productGroupId").get("v.value")});
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var picklistresult = response.getReturnValue();
                console.log(picklistresult);
                
                for(var i =0;i<picklistresult.length;i++)
                {
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            //component.find("docTypeId").set("v.options",options);
            self.getDocumentVersions(component,event);
        });	
        $A.enqueueAction(action); 
    },
    getDocumentVersions : function(component,event) 
    {
        var self = this;
        var options = [];
        var optionArr = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        // var action = component.get("c.getPicklistValues");
        // action.setParams({"fieldName" : "Document_Version__c", "sObjectName" : "ContentVersion"});
        
        var action = component.get("c.getVersions");
        action.setParams({"Productvalue":component.find("productGroupId").get("v.value")});
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var picklistresult = response.getReturnValue();
                if(!$A.util.isEmpty(picklistresult)){
                    for(var i =0;i<picklistresult.length;i++)
                    {
                        optionArr.push(parseFloat(picklistresult[i].pvalue));
                    }
                    optionArr.sort(function(a,b) { return a-b; });
                    for(var i =0;i<optionArr.length;i++)
                    {
                        options.push({
                            label : optionArr[i],
                            value : optionArr[i]
                        });
                    }
                }
            }
            component.find("versionId").set("v.options",options);
            self.getContentVersionList(component,event);
        });	
        $A.enqueueAction(action); 
    },
    getContentVersionList : function(component,event,resetPages){
        if(!resetPages){
            debugger;
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
            debugger;
        }
       
        var self = this;
        var action = component.get("c.getSwlContentVersions");
        
        action.setParams({"Productvalue":component.find("productGroupId").get("v.value"),
                          "DocumentTypes":component.find("projectTypeId").get("v.value"),
                          "versioninfo":component.find("versionId").get("v.value"),
                          "Document_TypeSearch":component.find("categoryId").get("v.value"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "pageSize":component.get("v.pageSize")
                         }); 
        debugger;
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            debugger;
            if(state == "SUCCESS"){
                var responseWrap = response.getReturnValue(); 
                if(responseWrap){
                     for(var i = 0; i < responseWrap.lstContentVersions.length; i++) {
                    	var doc = responseWrap.lstContentVersions[i];
						var siteURL = $A.get("$Label.c.MvSiteURL");
                        if(doc.Mutli_Languages__c){
                            doc.url = siteURL+'/s/productdocumentationdetail/?Id='+doc.ContentDocumentId+"&lang="+component.get('v.language');
                        }else{
                            doc.url = siteURL+'/sfc/servlet.shepherd/version/download/'+doc.Id;
                        }
                    }
                    component.set("v.contentVersionList",responseWrap.lstContentVersions);
                    component.set("v.totalSize",responseWrap.totalSize);
                    
                    //component.set("v.totalSize",responseWrap.totalSize ? responseWrap.totalSize : 0);
                }
               // var cvlist = response.getReturnValue();
               // component.set("v.contentVersionList",cvlist);
            }
            
        });	
        $A.enqueueAction(action); 
    },
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var contentVersionList = component.get("v.contentVersionList");

            if(!$A.util.isEmpty(contentVersionList)){
                var doc = contentVersionList[index];
                if(doc.Mutli_Languages__c){
                    var docId = doc.ContentDocumentId;

                    var onlyId = doc.Id;

                    url = "/productdocumentationdetail/?Id="+docId+"&lang="+component.get('v.language');
                    
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": url,
                        isredirect: "true"
                    });
                    urlEvent.fire();
                    
                }else{                    
                    var docId = doc.Id;
                    //var sPageURL = window.location.href;
                    //var sURLVariables = sPageURL.split('/');
                    //var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
                    //  strRemove = strRemove + '/' 
                    // strRemove = strRemove + sURLVariables[sURLVariables.length - 1];
                    //var newUrl = sPageURL.replace(strRemove, '');
                    var siteURL = $A.get("$Label.c.MvSiteURL");
                    //  url = "https://sfdev2-varian.cs61.force.com/varian/sfc/servlet.shepherd/version/download/"+docId ;
                    url = siteURL + "sfc/servlet.shepherd/version/download/"+docId;
                    window.location.replace(url) ;                   
                } 
            }
        }
    }
    
})
({
    /* Load custome labels */
	getCustomLabels : function(component, event) {
	   var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
})
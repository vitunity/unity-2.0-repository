({
   jsLoaded: function(component, event, helper) {
       var map = L.map('map', {zoomControl: true})
                  .setView([0, 0], 2.0);
      L.tileLayer(
       'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}',
       {
              attribution: 'Tiles © Esri'
       }).addTo(map);
      component.set("v.map", map);
  },
    
    accountsLoaded: function(component, event, helper) {

        // Add markers
        var map = component.get('v.map');
        var wrapAccounts = event.getParam('accountWrappers');
        for (var i=0; i<wrapAccounts.length; i++) {
            var account = wrapAccounts[i].objAct;
            var latLng = [account.Latitude__c, account.Longitude__c];
            var accountName=account.Name;
            var webSite=account.Website;
            var street=account.BillingStreet;
            var phone=account.Phone;
            var state=account.BillingState;
            var city=account.Billingcity;
            var country=account.BillingCountry;
            var postalCode=account.BillingPostalCode;
            var self='_self';
            L.marker(latLng, {name: account.name},{draggable:false}).bindPopup("<b>"+accountName+"</b><br>"+street+","+city+","+state+","+country+","+postalCode+"</br><br><a href="+webSite+" target="+self+">"+webSite+"</a></br>").openPopup().addTo(map);
                             
        }  
    },
    accountSelected: function(component, event, helper) {
        // Center the map on the account selected in the list
        var map = component.get('v.map');
        var accountWrap = event.getParam("accountWrap");
        map.panTo([accountWrap.objAct.Latitude__c, accountWrap.objAct.Longitude__c]);
        map.setView([accountWrap.objAct.Latitude__c, accountWrap.objAct.Longitude__c], 12);
    }
})
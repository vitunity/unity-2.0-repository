({
    accountSelected : function(component) {
        var event = $A.get("e.c:MvPTPAccountSelected");
        event.setParams({"accountWrap": component.get("v.accountWrap")});
        event.fire();
    }
})
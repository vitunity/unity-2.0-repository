({
    getAccountWrapperList: function(component, helper){
        var action = component.get("c.getAccountWrappers");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:', state);
            if ( state == "SUCCESS") {
               
                var resp = response.getReturnValue();
                if(!$A.util.isEmpty(resp) && !$A.util.isUndefined(resp)){
               
                component.set('v.wrapAccs', resp);
                }
                   var event = $A.get("e.c:PeerToPeerLoadEvent");
                   event.setParams({"accountWrappers": resp});
                   event.fire();
            }

        });
        $A.enqueueAction(action);
    }
    
})
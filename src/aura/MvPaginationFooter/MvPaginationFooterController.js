({
	
    firstPage: function(component, event, helper) {
        var totalSize = component.get("v.totalSize");
        var pageSize = component.get("v.pageSize");
        var totalPages = Math.ceil(totalSize/pageSize);
        component.set("v.totalPages", totalPages);
        var counters = new Array();//= Array(totalPages).fill(0).map((e,i)=>i+1);
        for(var x = 1; x <= totalPages; x++){
            counters.push(x);
            if(x==10) {
                break;
            }
        }
        component.set("v.pageCounter",counters);
        component.set("v.currentPageNumber", 1);
        helper.firePaginationEvent(component, event);
    },
    handlePrevPage: function(component, event, helper) {
        debugger;
        var counters = component.get("v.pageCounter");
        var currentPage = component.get("v.currentPageNumber");
        if(currentPage == 1){
            helper.firePaginationEvent(component, event);
            return;
        }
        alert(counters.length);
        for(var x = 0; x < counters.length; x++){
            counters[x] -= 1;
            
        }
        component.set("v.pageCounter",counters);
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber")-1, 1));
        helper.firePaginationEvent(component, event);
    },
    handleNextPage: function(component, event, helper) {
        debugger;
        var counters = component.get("v.pageCounter");
        alert(counters.length);
        for(var x = 0; x < counters.length; x++){
            counters[x] += 1;
        }
        component.set("v.pageCounter",counters);
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber")+1, component.get("v.totalPages")));
        helper.firePaginationEvent(component, event);
    },
    
    lastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.ceil(component.get("v.totalPages")));
        helper.firePaginationEvent(component, event);
    },
    
    handleSizeChange: function(component, event, helper) {        
        var totalSize = component.get("v.totalSize");
        var pageSize = component.get("v.pageSize");
        var totalPages = Math.ceil(totalSize/pageSize);
        component.set("v.totalPages", totalPages);
        $(".pagination").bootpag({
            total: totalPages,
            page: 1,
            maxVisible: 5,
            leaps: true,
            firstLastUse: true,
            first: '←',
            last: '→',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, num){
            component.set("v.currentPageNumber", num); 
            helper.firePaginationEvent(component, event);
            //$(this).bootpag({total:totalPages, maxVisible: 10});
        });
        
    },

    navigate: function(component, event, helper) { 
        var counter = event.currentTarget.getAttribute("data-count");
        component.set("v.currentPageNumber", counter);
        helper.firePaginationEvent(component, event);
    }
    
})
({
    doInit : function(component, event, helper) {
        helper.getCustomLabels(component, event);
        helper.getContactInfo(component, event);
        helper.getProduct(component, event);
        helper.getCountry(component, event);
        helper.getLanguage(component, event);
       
    },
    /* Set Product Value */
    onChangeProduct: function(component, event, helper){
      helper.setProduct(component, event);
    },
    /* Set Country Value */
    onChangeCountry: function(component, event, helper){
      helper.setCountry(component, event);
    },
    /* Set Preferred Language Value */
    onChangeLanguage: function(component, event, helper){
      helper.setLanguage(component, event);
    },
    
  
    /* Toggle based on logged in user */
	toggle : function(component, event, helper) {
    	component.set("v.showMe","false");
    	component.set("v.showMe1","True");
    },
    
    /* Get all selected documents */
    checkboxSelect: function(component, event, helper) {
    	// get the selected checkbox value 
      	var id = event.source.get("v.text");
      	if (event.source.get("v.value")) {
        	// Checkbox is checked - add id to checkedContacts
        	if (component.get("v.checkedDocuments").indexOf(id) < 0) {
          		component.get("v.checkedDocuments").push(id);
            }
      	} else {
        // Checkbox is unchecked - remove id from checkedDocuments
            var index = component.get("v.checkedDocuments").indexOf(id);
            if (index > -1) {
                component.get("v.checkedDocuments").splice(index, 1);
            }
      	}
	},
    
    /* Submit request for paper documentation*/
    submit: function(component, event, helper) { 
    	// create var for store record id's for selected checkboxes  
      	var selectedId = [];
      	//selectedId = component.get("v.checkedDocuments");
      	// get all checkboxes 
      	var getAllId = component.find("boxPack");
       	// play a for loop and check every checkbox values 
      	// if value is checked(true) then add those Id (store in Text attribute on checkbox) in selectedId var.
        for (var i = 0; i <= getAllId.length; i++) {
            if (getAllId[i].get("v.value") == true) {
                selectedId.push(getAllId[i].get("v.text"));
            }
        }
       
      	// call the helper function and pass all selected record id's.    
      	helper.PassSelectedContentHelper(component, event, selectedId);
        var urlEvent = $A.get("e.force:navigateToURL");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/vhome"
        });
        urlEvent.fire();   
	}
})
({
	getProduct : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForProduct");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.prodData.Product_Name__c"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvProduct", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /* Load country picklist values */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForCountry");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.countryData.Name"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* Set product value */
    setProduct: function(component, event) 
    {
        var selectCmp = component.find("productValue");
        component.set("v.prodData.Product_Name__c", selectCmp.get("v.value"));
    },
    
    /* Set Country picklist value */
    setCountry: function(component, event) 
    {
        var selectCmp = component.find("countryValue");
        component.set("v.registerData.MailingCountry", selectCmp.get("v.value"));
    },
    
    /* Fetch logged in user information */
    getContactInfo: function(component, event){
        var action = component.get("c.getContact");
        action.setCallback(this, function(response) {
        var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.registerData", response.getReturnValue());
            	component.set("v.isAuthenticated", true);
            }
            else{
            }
        })
        $A.enqueueAction(action);
    },
    /* Set Product value */
    setProduct: function(component, event) 
    {
        var selectCmp = component.find("productValue");
        component.set("v.prodData.Product_Name__c", selectCmp.get("v.value"));
        
    },
    
    /* Get language picklist value*/
    getLanguage : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForDocumentLanguage");
        //action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.langData.Document_Language__c"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvLanguage", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Set language picklist value*/
    setLanguage: function(component, event) 
    {
        var selectCmp = component.find("languageValue");
        component.set("v.langData.Document_Language__c", selectCmp.get("v.value"));
        if(selectCmp!=null){
            
            component.set("v.showTable", true);
        }
        
        var registerMData = component.get("c.ContentVersions"); 
        var product = component.find("productValue").get("v.value");
        var language = component.find("languageValue").get("v.value");
        
        registerMData.setParams({
              
        	"Productvalue": product,
            "Languagevalue": language
              
        });
          
        var urlEvent = $A.get("e.force:navigateToURL");
        registerMData.setCallback(this, function(response) {
        var state = response.getState();
        var returnArray = response.getReturnValue();
        	if (state === "SUCCESS") {
                component.set("v.contentVersion", response.getReturnValue());
            }
        });
        $A.enqueueAction(registerMData);
    },
    
    /* Pass selected content documents to controller */
    PassSelectedContentHelper: function(component, event, RecordsIds) {
      //call apex class method
      var action = component.get('c.registerMData');
      // pass the all selected record's Id's to apex method
      var rdata = component.get("v.registerData");
      var adata = component.get("v.accData");  
      var language = component.find("languageValue").get("v.value");
        debugger;
      action.setParams({
          "registerDataObj": rdata,
          "accDataObj": adata,
          "lstRecordId": RecordsIds,
          "Languagevalue": language
      });
      action.setCallback(this, function(response) {
       //store state of response
       var state = response.getState();
       
      });
      $A.enqueueAction(action);
     },
})
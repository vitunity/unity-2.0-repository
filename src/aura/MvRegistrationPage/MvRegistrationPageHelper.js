({
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* Load Salutations */
    getSalutation : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Salutation"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var salutations = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Salutation"))
                    {
                        salutations.push(returnArray[i]);
                    }
                }
                component.set("v.mvSalutation", salutations);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    /* Set Salutation value */
    setSalutation: function(component, event) 
    {
        var selectCmp = component.find("Salutation");
        component.set("v.registerData.Salutation", selectCmp.get("v.value"));
    },
    
    /* Load Speciality picklist value */
    getSpecialty : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Specialty__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Specialties = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Specialty__c"))
                    {
                        Specialties.push(returnArray[i]);
                    }
                }
                component.set("v.mvSpecialty", Specialties);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setSpeciality: function(component, event) 
    {
        var selectCmp = component.find("Specialty");
        component.set("v.registerData.Specialty__c", selectCmp.get("v.value"));
    },
    
    /* Get Preferred Languager picklist value */
    getPreferredLanguage : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Preferred_Language1__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Languages = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Preferred_Language1__c"))
                    {
                        Languages.push(returnArray[i]);
                    }
                }
                component.set("v.mvPreferredLanguage", Languages);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setLanguage: function(component, event) 
    {
        var selectCmp = component.find("Language");
        component.set("v.registerData.Preferred_Language1__c", selectCmp.get("v.value"));
    },
    
    /* Get Professional role picklist value */
    getProfessionalRole : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Functional_Role__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var roles = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.Functional_Role__c"))
                    {
                        roles.push(returnArray[i]);
                    }
                }
                component.set("v.mvProfessionalRole", roles);
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    setRole: function(component, event) 
    {
        var selectCmp = component.find("Role");
        component.set("v.registerData.Functional_Role__c", selectCmp.get("v.value"));
    },
    
    /* Get Country picklist value */
    getCountry : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptionsForCountry");
        action.setParams({"fieldName":"Country__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countries = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    if(returnArray[i] != component.get("v.registerData.MailingCountry"))
                    {
                        countries.push(returnArray[i]);
                    }
                }
                component.set("v.mvCountry", countries);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    setCountry: function(component, event) 
    {
        var selectCmp = component.find("country");
        component.set("v.registerData.MailingCountry", selectCmp.get("v.value"));
    },
    
    /*Initialise captcha value */
    initialiseCaptcha: function(component,event) {
        component.set("v.char1", this.randomNumber());
        component.set("v.char2", this.randomNumber());
        component.set("v.char3", this.randomNumber());
        component.set("v.char4", this.randomNumber());
        component.set("v.char5", this.randomNumber());
        component.set("v.char6", this.randomNumber());
    },
    /* Generate ramdom number for captcha */
    randomNumber: function(){
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var random = possible.charAt(Math.floor(Math.random() * possible.length))
        return random;
    },
    
    /* Captcha match Validation */
    validateCaptcha: function(component,event) {        
        debugger;
        var char1 = component.get("v.char1");
        var char3 = component.get("v.char3");
        var char5 = component.get("v.char5");
        var captchaData = component.get("v.captchaData");
        if(captchaData == undefined || captchaData == null){	
             this.initialiseCaptcha(component,event);
            return false;
            
        }else if(captchaData.length != 3){
             this.initialiseCaptcha(component,event);
            return false;
        }else if(captchaData[0].localeCompare(char1) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[1].localeCompare(char3) != 0){
             this.initialiseCaptcha(component,event);
            return false;
             
        }else if(captchaData[2].localeCompare(char5) != 0){
            return false;
             
        }else{
            return true;
        }
    }
    
})
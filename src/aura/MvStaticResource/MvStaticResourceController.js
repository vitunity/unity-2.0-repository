({
	doInit1 : function(component, event, helper) {
        $(".auraCss").attr("disabled", "disabled");
        $(".auraCss").remove();
       
        console.log('*************** Inside do Init1 Method *****************')
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        jQuery("document").ready(function(){
            console.log('Inside jquery'); 
            var lang =  getUrlParameter('lang');
            if(lang){
                $('.selectpicker').val(lang); 
            	$('.selectpicker').selectpicker('render'); 
            }
            
            $(document).on('change', '.chooseFile', function() {
                var filename = $(this).val();
                if (/^\s*$/.test(filename)) {
                $(this).parent().parent().removeClass('active');
                $( this ).siblings( ".noFile" ).text("No file chosen..."); 
                } else {
                $(this).parent().parent().addClass('active');
                $( this ).siblings( ".noFile" ).text(filename.replace("C:\\fakepath\\", ""));
                }
			});
            
            $(document).on('click', '#accessDocAccordianId .header', function() {            
                //$('#accessDocAccordianId tbody tr').not('.header').hide();
            	$(this).nextUntil('tr.header').slideToggle(100);
            });
            
            setInterval(function(){ 
            	$('.selectpicker').selectpicker('render'); 
                $(".datepicker").datepicker({
                    numberOfMonths: 1
                });
                //format: "MM dd, yyyy"
                $(".datepickerRPC").datepicker({
                    numberOfMonths: 1,
                    format: "dd-mm-yyyy"
                });
                $(".datepickerMDADEDT").datepicker({
                    numberOfMonths: 1,
                    format: "dd-mm-yyyy"
                });
                $(".datepickerMDADSDT").datepicker({
                    numberOfMonths: 1,
                    format: "dd-mm-yyyy"
                });
            }, 1000);
            
            setInterval(function(){ 
            	$('.selectpicker').selectpicker('render'); 
            }, 100);
            
        });
	}
})
({
	doInit : function(component, event, helper) {
		//helper.getUrlParameter(component, event);
        //helper.getCustomLabels(component, event);
        //helper.getDocuments(component, event);
       
    },
    changeTab : function(component, event, helper){
        var selectedTabAuraId = event.currentTarget.getAttribute("data-selected-Index");
        helper.changeTabStyle(component, selectedTabAuraId);
    },
    
    changeToPhoton :  function(component, event, helper){
		component.set("v.activeTab","Photon");    	
    },
    changeToElectron :  function(component, event, helper){
    	component.set("v.activeTab","Electron");
    }
})
({
	 /* Load custome labels */
	
    
    changeTabStyle : function(component, tabId){
        console.log("------changeTabStyle1---");
        console.log("------selectedTabAuraId---"+tabId);
        var tabDetails = tabId.split('-');
        var tabAuraId = tabDetails[0];
        var tabContentId = tabDetails[1];
        if(tabAuraId != "homeTab"){             
        	$A.util.removeClass(component.find("homeTab"), "showActive");
            $A.util.removeClass(component.find("HomeContent"), "in");
            $A.util.removeClass(component.find("HomeContent"), "active");
            
        }
        if(tabAuraId != "newRequestTab"){             
        	$A.util.removeClass(component.find("newRequestTab"), "showActive");
            $A.util.removeClass(component.find("newContent"), "in");
            $A.util.removeClass(component.find("newContent"), "active");
        }
        $A.util.addClass(component.find(tabId), "showActive");
        $A.util.addClass(component.find(tabContentId), "in");
        $A.util.addClass(component.find(tabContentId), "active");
        console.log("------changeTabStyle2---");
    },
   
    
})
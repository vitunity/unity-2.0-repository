({
	mvnewregist : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarloregister?mtype=TrueBeam"
        });
        urlEvent.fire();
	}
})
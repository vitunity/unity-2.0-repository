({
    doInit : function(component, event, helper) {        
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getLiveEventWebinar(component, event);
    },
    register : function(component, event, helper) {        
        helper.doRegister(component, event);
    }, 
    
    home: function(component, event, helper) {
         helper.goHome(component, event);
    },
    
    webinarHome: function(component, event, helper) {
        helper.goWebinar(component, event);
    },
})
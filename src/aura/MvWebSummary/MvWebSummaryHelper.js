({
    
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var Id = getUrlParameter('Id');
        component.set("v.Id",Id);
        var lang = getUrlParameter('lang');
        component.set("v.language",lang);
    },
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        debugger;
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getLiveEventWebinar : function(component,event) {
        var docId = component.get("v.Id");
        var action = component.get("c.getLiveWebinar");
        action.setParams({"Id": docId}); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                debugger;
                var cvlist = response.getReturnValue();
                component.set("v.liveEventWebinar",cvlist);
            }
        }); 
        $A.enqueueAction(action); 
    },
    
    doRegister : function(component,event) {
       	var webinarObj = component.get('v.liveEventWebinar');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": webinarObj.URL__c
        });
        urlEvent.fire();

    },
    
    goHome : function(component,event) {
       	var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    goWebinar : function(component,event) {
       	 var url = "/webinars?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
})
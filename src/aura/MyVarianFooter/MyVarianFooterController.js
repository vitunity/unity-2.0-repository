({
	doInit: function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
    },
})
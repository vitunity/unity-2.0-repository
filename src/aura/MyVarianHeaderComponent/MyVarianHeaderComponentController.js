({
    doInit : function(component, event) {
       var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   //set the src param value to my src attribute
       //alert( getUrlParameter('contactus'));
        component.set("v.contactus", getUrlParameter('contacts'));
        
    },
	
    myAction : function(component, event, helper) {
		
	},
    events : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage"
        });
        urlEvent.fire();
    },
    contactus : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contactus"
        });
        urlEvent.fire();
    },
    myaccount : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/myAccount"
        });
        urlEvent.fire();
    },
    home : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage"
        });
        urlEvent.fire();
    },    
    
    marketingYourCenter : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvmarketingkit"
        });
        urlEvent.fire();
    },
    
    productDocumentation : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvproductideas"
        });
        urlEvent.fire();
    },
    cpMonteCarlo : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/mvcpmontecarlo"
        });
        urlEvent.fire();
	}
})
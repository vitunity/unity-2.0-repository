({
	doInit : function(component, event, helper) {         
        helper.getUrlParameter(component, event);
        helper.getLoggedInUser(component, event);
        helper.getCustomLabels(component, event);

        helper.getDocTypeValues(component,event);
        //helper.getContentVersionList(component,event);
        helper.getMyHomeFavorites(component,event);
        helper.getUserObj(component,event);
        helper.getOnDemandWebinars(component, event);
	},
    changeDocType : function(component, event, helper){
       helper.getContentVersionList(component,event);
    },
    openProductDocumentation: function(component, event, helper){
        helper.openProductDocumentation(component, event);
    },
    openDocument : function(component, event, helper){
        helper.openDocument(component,event);
    },
    
    handleMyFavoritesClick : function(component, event, helper){
       helper.handleMyFavoritesClick(component,event);
    },
    openWebinarLink: function(component, event, helper) {
         helper.openWebinarLink(component, event);     
    },
    myaccount: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var langVal = component.get("v.language");
        var loggedInUserVal = component.get("v.loggedInUser.Id");
        var myFavVal;

        if ($A.util.isUndefined(langVal)) {
            langVal = 'en';
        } 

        if ($A.util.isUndefined(loggedInUserVal)) {
            myFavVal = false;
        } else {
            myFavVal = true;
        }
        urlEvent.setParams({
             "url": "/myaccount?lang="+langVal+"&myFav="+myFavVal
        });
        window.open("/s/myaccount?lang="+langVal+"&myFav="+myFavVal,"_self");
        //urlEvent.fire();
    },
    webinarTab: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/webinars?lang="+component.get('v.selectedLanguage')
        });
        urlEvent.fire();
    },
})
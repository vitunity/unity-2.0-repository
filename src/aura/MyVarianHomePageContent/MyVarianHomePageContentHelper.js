({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getDocTypeValues : function(component,event) {        
        var action = component.get("c.getDocumentOptionsList");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++)
                {
                    
                    if(picklistresult[i].label == 'Safety Notifications'){
                        options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue,
                        selected : true
                    	});
                    }else{
                        options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                        
                    	});
                    }
                }                
            }
            component.find("docTypeId").set("v.options",options);
            this.getContentVersionList(component,event);
        });	
        $A.enqueueAction(action); 
    },
    
    getMyHomeFavorites : function(component,event) 
    {
		var action = component.get("c.getMyHomeFavorites");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.myFavorites", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    
    /* handle MyFavorites Navigation */
    handleMyFavoritesClick : function(component, event, helper){
        var fav =  event.currentTarget.id;
        var url;
        
        if(fav){
            switch(fav) {
                case "Certificate Manager":
                    url = "https://webapps.varian.com/certificate";
                    break;
                case "Course Listing":
                    url = "/trainingcourses";
                    break;
                case "Events":
                    url = "/events";
                    break;
                case "Learning Center":
                    url = "https://varian.oktapreview.com/api/v1/groups";
                    break;
                case "OncoPeer":
                    url = "/oncopeer";
                    break;
                case "Product Ideas":
                    url = "/productideas";
                    break;
                case "Support Case":
                    url = "/supportcases";
                    break;
                case "Webinars":
                    url = "/webinars";
                    break;
                case "vMarket":
                    url = "";
                    break;
                default:
                    url="";
            }
            this.navigateToURL(component, event, url)
        }
    },
    /* navigate To URL*/
    navigateToURL: function(component, event, url) {
        if(!$A.util.isEmpty(url)){
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                isredirect: "true"
            });
            urlEvent.fire(); 
        }        
    },
   
    /* open Webinar Link*/
    openWebinarLink: function(component, event) {
        var id = event.currentTarget.id;
        var url;
        
        if(id){
            url="/mvwebsummary?Id="+id; // mvwebsummary?Id=a0O0n0000007LTeEAM
        }       
        if(url){
          this.navigateToURL(component, event,url);  
        }        
    },

    getContentVersionList : function(component,event)  {
        var action = component.get("c.getlstCntntVersion");
        var docType = component.find("docTypeId").get("v.value");
        action.setParams({"selectedDocument":docType}); 
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var cvlist = response.getReturnValue();
                for(var i = 0; i < cvlist.length; i++) {
                    var doc = cvlist[i];
                    doc.url = '/s/productdocumentationdetail/?Id='+doc.cont.ContentDocumentId+"&lang="+component.get('v.language');
                }
                component.set("v.contentVersionList",cvlist);
            }            
        });	
        $A.enqueueAction(action); 
    },
    
    openProductDocumentation : function(component, event){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/productdocumentation?lang="+component.get('v.language'),
            isredirect: "true"
        });
        urlEvent.fire();        
    },
    openDocument : function(component, event){
        var index = event.currentTarget.id;
        if(index){
            var url='';
            var contentVersionList = component.get("v.contentVersionList");
            if(!$A.util.isEmpty(contentVersionList)){
                var doc = contentVersionList[index];
                if(doc.cont.Mutli_Languages__c){
                    var docId = doc.cont.ContentDocumentId;
                    url = "/productdocumentationdetail/?Id="+docId;
                    
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": url,
                        isredirect: "true"
                    });
                    urlEvent.fire();
                    
                }else{                    
                    var docId = doc.cont.Id;
                    var sPageURL = window.location.href;
                    var sURLVariables = sPageURL.split('/');
                    var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
                    var newUrl = sPageURL.replace(strRemove, '');
                    
                    //  url = "https://sfdev2-varian.cs61.force.com/varian/sfc/servlet.shepherd/version/download/"+docId ;
                    url = newUrl + "sfc/servlet.shepherd/version/download/"+docId;
                    window.location.replace(url) ;                   
                } 
            }
        }
    },
    getUserObj : function(component, event){
        var action = component.get("c.getSubscribeInfo");
        action.setCallback(this, function(response) {
        	var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set('v.userObj',response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* Load Webinars */
    getOnDemandWebinars : function(component, event) {
        var action = component.get("c.getOnDemandWebinars");
        // action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lstWebinars = response.getReturnValue();
               // alert(lstWebinars);
                component.set("v.lstWebinars",lstWebinars);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getLoggedInUser : function(component, event) {
        var action = component.get("c.getLoggedInUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.loggedInUser", response.getReturnValue());             
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },        
})
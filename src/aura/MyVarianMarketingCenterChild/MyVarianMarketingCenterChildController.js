({
    /* Page load method. It will load data in intial load.*/
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getMarketingKitsProgDocs(component, event);
        
    },
    
     /* Redirect to Previous page. used form Cancel button in Terms window*/
    close : function(component, event, helper){
       component.set('v.imgPreviewTitle','Loading');
       component.set('v.imgPreviewUrl','');
    },
    /* Redirect to Previous page. used form Cancel button in Terms window*/
    openMarketKitPage : function(component, event, helper){
        helper.openMarketKitPage(component,event);
    },
    /* Expand or hide the each section clicking on the name.*/
    toggleCardDisplay : function(component, event, helper){
        var index = event.currentTarget.id;
        
        var progId = component.find("progId");
        if($A.util.isArray(progId)){
            $A.util.toggleClass(progId[index], "slds-hide");
        }else{
            // It will add or remove slds-hide css class for html.
            $A.util.toggleClass(progId, "slds-hide");
        }
    },
    
    previewImage:function(component, event, helper){
    	helper.imageResize(component, event);
    },
    
    /* Set image size when it is loading.*/
    imageResize : function(component, event, helper){
        helper.showImgPreview(component, event);       
    },
    /* Download the file*/
    downloadFile : function(component, event, helper){
        helper.openDocument(component, event);
    },
    /* Open image preview window */
    showImgPreview : function(component, event, helper){        
        helper.showImgPreview(component, event);
    },
    /* Show Terms widow dialog */
    toggleTermsDialog : function(component, event, helper){
        helper.toggleTermsDialog(component, event);
    },
    /* Save License agreement from Terms window agree button */
    saveLicenseAgreement : function(component, event, helper){
        helper.registerLicenseAgreement(component, event);
       // hide the terms window.
      // helper.toggleTermsDialog(component, event);
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
})
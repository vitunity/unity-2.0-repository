({
    /* to go back to previous page */
    openMarketKitPage : function() {
        // Salesforce event for navigation
        var urlEvent = $A.get("e.force:navigateToURL");
        // set url
        urlEvent.setParams({
            "url": '/marketingkit',
            isredirect: "true"
        });
        // Fire the event
        urlEvent.fire();
    },
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)).replace(/\+/g, " "),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // get param values
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
        // get param values
        var marktProg =  getUrlParameter("prog");
        //alert(marktProg);
        component.set("v.marktProg",marktProg);
    },
    
    
    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
                var header = component.get("v.customLabelMap.MV_MKChildPageHeader");
                
                // Replace "marktProg" straing with dynamic marktProg value(this will ome from previous page)
                var res = header.replace("marktProg", component.get("v.marktProg"));
                component.set("v.customLabelMap.MV_MKChildPageHeader",res);
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getMarketingKitsProgDocs : function(component,event){
        // find Apex method to get list of marketing docs
        var action = component.get("c.getMarkProgDocs");
        
        // Set parameters to apex method.
        action.setParams({"marktProg":component.get("v.marktProg"),
                          "userIdFromSurvey":component.get("v.userIdFromSurvey")
                         });
        // Call the apex method.
        action.setCallback(this, function(response) {
            
            // get the status
            var state = response.getState();
            console.log(' === State marktKitdocs ===> ' + state + ' == ' + JSON.stringify(response.getReturnValue()));
            if (component.isValid() && state === "SUCCESS") {
                // get the marketing dos data
                var objMarketingKit =response.getReturnValue(); 
                // Check marketing docs list is empty or not.
                if(!$A.util.isEmpty(objMarketingKit)){
                    // set marketing list docs data into attribute.
                    component.set("v.marktKitdocs",objMarketingKit.lstMarkProgDocs);
                    
                    // set Terms accepted data into attribute.
                    component.set("v.termsAccepted",objMarketingKit.mkTermsAccepted);
                    
                    // Show the terms window if terms not accpeted.
                    if(!objMarketingKit.mkTermsAccepted){
                       this.toggleTermsDialog(component, event); 
                    }                    
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        // set actio in queue. Lightning actions are asynchronous.
        $A.enqueueAction(action);
    },
    /* To download the doc.*/
    openDocument : function(component, event){
        var docId = event.currentTarget.id;
        if(docId){
            var url='';
            // get the page url
            var sPageURL = window.location.href;
            // split page url
            var sURLVariables = sPageURL.split('/');
            // reconstruct the url without "Varian/s"
            var newUrl = sURLVariables[0] +"//"+ sURLVariables[2] + "/" + sURLVariables[3];
            
            // prepare url to download file. "Varian/s" needs to remove from url to download file.
            url = newUrl + "/sfc/servlet.shepherd/version/download/"+ docId +"?" + $A.get("$Label.c.ContentDocTag");
            // open the download url.
            window.location.replace(url);
        }
    },
    /* Set the Image size while loading */
    imageResize : function(component, event, helper){
        debugger;
        var index = event.currentTarget.id;
        if(index){
            var indexes = index.split('-');
            // get the all Marketing docs list.
            var marktKitdocs = component.get("v.marktKitdocs");
            // get the current record.
            var marktKitdoc = marktKitdocs[indexes[0]].content[indexes[1]].subGroupList[indexes[2]];
            var imgwidth = marktKitdoc.imgWidth;
            var imgheight = marktKitdoc.imgHeight;            

            var coImages =  component.find("coImage1");
            var coImage = coImages;
            
            if(!$A.util.isEmpty(coImages)){
                if($A.util.isArray(coImages)){
                    coImage = coImages[indexes[2]];
                }
                
                if(imgheight == '0'){
                    $A.util.addClass(coImage, 'imageHeight');
                    $A.util.addClass(coImage, 'imageWidth');
                }
                if (imgheight > imgwidth){
                    $A.util.addClass(coImage, 'imageHeight');
                }
                if (imgwidth > imgheight){
                    $A.util.addClass(coImage, 'imageWidth');
                }                
            }
        }        
    },
    /* Show or hide image preview usig preview button click or image click. */
    showImgPreview : function(component, event, helper){
        // get the image url which is set as Id for anchor tag.      
        var imageUrl = event.currentTarget.id;
        //alert(imageUrl);
        // get the title which is set to anchor tag title.
        var title = event.currentTarget.title;
        // set image url for image preview popup window
        if(imageUrl){
            component.set("v.imgPreviewUrl", imageUrl);
        }else{
            component.set("v.imgPreviewUrl", "");
        }
        // set the image title in image preview popup.
        if(title){
            component.set("v.imgPreviewTitle", title);
        }else{
            component.set("v.imgPreviewTitle", "");
        }
        
        // to show or hide Image preview dialogue. 
        $A.util.toggleClass(component.find("divpreviewDialog"), 'displayNone');
        $A.util.toggleClass(component.find("divBackGroundGray"), 'displayNone');
    },
    /* to show or hide Terms dialogue. */
    toggleTermsDialog : function(component, event){
        $A.util.toggleClass(component.find("divTermsDialog"), "displayNone");
        $A.util.toggleClass(component.find("divBackGroundGray"), 'displayNone');
    },
    /* */
    registerLicenseAgreement : function(component,event){
        // find Apex method to get list of marketing docs
        var action = component.get("c.registerLicenseAgreement");
        
        // Set parameters to apex method.
        action.setParams({"marketProg":component.get("v.marktProg")});
        // Call the apex method.
        action.setCallback(this, function(response) {
            
            // get the status
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                // get the TermsAccepted data
                var termsAccepted = response.getReturnValue();
                //alert(termsAccepted);
                if(termsAccepted){
                    // set Terms accepted data into attribute.
                    component.set("v.termsAccepted",termsAccepted);
                    
                    // Show the terms window if terms not accpeted.
                    console.log(' === termsAccepted ');
                    if(termsAccepted){
                       this.toggleTermsDialog(component, event); 
                    }                    
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        // set actio in queue. Lightning actions are asynchronous.
        $A.enqueueAction(action);
    },
})
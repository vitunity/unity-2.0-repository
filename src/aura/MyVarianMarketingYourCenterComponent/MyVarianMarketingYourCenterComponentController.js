({
    // Page load method. It will load data in intial load.
    doInit : function(component, event, helper) {
        // Action to get Url parameters values
        helper.getUrlParameter(component, event);
        // Action to get custom labels values
        helper.getCustomLabels(component, event);
        // Action to get Marketing text with images.
        helper.getMarketingKitsList(component,event);
        helper.getMyVarianAgreementHelper(component, event);
        //helper.getMarketingKitsProgDocs(component, event);
        //helper.getMarketingKitsProgDocs(component, event);
    },
    // Action to Redirect to next page. this Action execute when clicking on image text.
    openDocument : function(component, event, helper){
        //var productName = event.srcElement.title;
        //var index = event.srcElement.id;
        //console.log(' === ' + productName);
        //component.set("v.productName",productName);
        //component.set("v.productIndex",index);
        helper.getMarketingKitsProgDocs(component, event);
        // Action to Redirect to next page.
        //helper.openDocument(component,event);
    },
    // Action to Redirect to next page. this Action execute when clicking on image text.
    checkAgreement : function(component, event, helper){
        debugger;
        // Action to Redirect to next page.
        //var pId = event.currentTarget.getAttribute("data-pid");
        //alert(pId);
        var productName = event.srcElement.title;
        var index = event.srcElement.id;
        console.log(' === ' + productName);
        component.set("v.productName",productName);
        component.set("v.productIndex",index);
        helper.getMarketingKitsProgDocs(component, event);
        //ale	rt(productName);
        //helper.openDocument(component,event);
    },
    /* Redirect to Previous page. used form Cancel button in Terms window*/
    openMarketKitPage : function(component, event, helper){
        helper.openMarketKitPage(component,event);
    },
    /* Save License agreement from Terms window agree button */
    saveLicenseAgreement : function(component, event, helper){
        console.log(' === saveLicenseAgreement === ');
        helper.registerLicenseAgreement(component, event);
    },   
    
})
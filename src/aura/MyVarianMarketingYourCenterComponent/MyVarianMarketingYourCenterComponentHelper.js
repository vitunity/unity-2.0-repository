({
    /* get url parameters */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        // Get lang value from Url if it has.
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load getMyVarianAgreement */
	getMyVarianAgreementHelper : function(component, event) {
	   var action = component.get("c.getMyVarianAgreement");
       action.setParams({
           "name":"MARKETING_KIT",
           "languageCode":component.get("v.language")});
       action.setCallback(this, function(response) {
           debugger;
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.agreementList",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    
    /* Load custom labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /* get Marketing kit Text with images list data*/
    getMarketingKitsList : function(component,event){
        console.log(' getMarketingKits ');
        // find Apex action to call from this method. 
        var action = component.get("c.getMarketingKits");
        // Call Apex method using setCallback function.
        action.setCallback(this, function(response) {
            // Get response Status
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // Get Images list from Apex.
                var objMarketingKit = response.getReturnValue();
                // Check Non Internal user Marketing data list is empty
                if(!$A.util.isEmpty(objMarketingKit.lstMarketingKit)){
                    // Set Non Internal user Marketing data to Attribute data.
                    component.set("v.marketingKitsList",objMarketingKit.lstMarketingKit);  
                }else if(!$A.util.isEmpty(objMarketingKit.lstMarketingKitInternalUser)){
                    // Set Internal user Marketing data to Attribute data.
                    component.set("v.marketingKitsInternalUserList",objMarketingKit.lstMarketingKitInternalUser);  
                }
                // Set Terms accepted Attribute data.
                if(!$A.util.isEmpty(objMarketingKit.mkTermsAccepted)){
                    //alert(objMarketingKit.mkTermsAccepted);
                    component.set("v.termsAccepted",objMarketingKit.mkTermsAccepted);  
                }
                // Set Survey Url Attribute data.
                if(!$A.util.isEmpty(objMarketingKit.mkSurveyURL)){
                    component.set("v.surveyURL",objMarketingKit.mkSurveyURL); 
                }
                
            }else if(response.getState() == "ERROR"){
                // it will Log error in chrome console.
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    openDocument : function(component, event){
        // Get current row index. which is set as Anchor link id in component.
        var index = event.currentTarget.id;
        debugger;
        if(index){
            var url='';
            var marketingKitsList = component.get("v.marketingKitsList");
            var marketingKitsInternalUserList = component.get("v.marketingKitsInternalUserList");
            
            // Condition for Non Internal User Data.
            if(!$A.util.isEmpty(marketingKitsList)){
                // get current row(User clicked image) from list.
                var doc = marketingKitsList[index];
                
                // Check for survey Url if it  has to rediret.
                if(doc.Product__r.Name === 'Premier Marketing Program' && component.get("v.termsAccepted")){
                    url = component.get("v.surveyURL");
                    //alert(url);
                }
                else{
                    // Next page Url
                    var prodName = doc.Product__r.Name;
                    url = "/mvmarketingkitchild?prog="+escape(prodName)+"&lang="+component.get('v.language');; 
                }
            }
            else if(!$A.util.isEmpty(marketingKitsInternalUserList)){ // Condition for INternal user
                
                var doc = marketingKitsInternalUserList[index];
                if(doc.Product_Name__c){
                    // Next page Url
                    var prodName = doc.Product_Name__c;
                    url = "/mvmarketingkitchild?prog="+prodName+"&lang="+component.get('v.language');
                }
            }
            
            // check url
            if(!$A.util.isEmpty(url)){
                // redirect to next page using salesforce standatrd event "e.force:navigateToURL" 
                // Get Url Event
                var urlEvent = $A.get("e.force:navigateToURL");
                // Set the params to Url Event
                urlEvent.setParams({
                    "url": url,
                    isredirect: "true"
                });
                //execute the event. it will redirect to given url.
                urlEvent.fire();
            }
        }
    },
    openMarketKitPage : function() {
        // Salesforce event for navigation
        var urlEvent = $A.get("e.force:navigateToURL");
        // set url
        urlEvent.setParams({
            "url": '/marketingkit',
            isredirect: "true"
        });
        // Fire the event
        urlEvent.fire();
    },
    
    registerLicenseAgreement : function(component,event){
        // find Apex method to get list of marketing docs
        var action = component.get("c.registerLicenseAgreement");
        
        // Set parameters to apex method.
        action.setParams({"marketProg":component.get("v.productName")});//"Premier Marketing Program" });//
        // Call the apex method.
        action.setCallback(this, function(response) {
            // get the status
            var state = response.getState();
            console.log(' === state === ' + JSON.stringify(response.getState()));
            console.log(' === response === ' + JSON.stringify(response.getReturnValue()));
            console.log(' === component.isValid() === ' + component.isValid());
            if (component.isValid() && state === "SUCCESS") {
                // get the TermsAccepted data
                var termsAccepted = response.getReturnValue();
                //alert(termsAccepted);
                if(termsAccepted){
                    //alert(termsAccepted);
                    // set Terms accepted data into attribute.
                    component.set("v.termsAccepted",termsAccepted);
                    
                    $("#toc").modal('hide');
                    this.getMarketingKitsProgDocs(component, event);
                    // Show the terms window if terms not accpeted.
                    if(termsAccepted){
                        console.log(' == toggleTermsDialog == ');
                        this.toggleTermsDialog(component, event); 
                    }                    
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        // set actio in queue. Lightning actions are asynchronous.
        $A.enqueueAction(action);
    },
    
    getMarketingKitsProgDocs : function(component,event ){
        // find Apex method to get list of marketing docs
        var action = component.get("c.getMarkProgDocs");
        //alert(component.get("v.productName"));
        //alert(component.get("v.productIndex"));
        // Set parameters to apex method.
        action.setParams({"marktProg":component.get("v.productName"),
                          "userIdFromSurvey":component.get("v.userIdFromSurvey")
                         });
        // Call the apex method.
        action.setCallback(this, function(response) {
            
            // get the status
            var state = response.getState();
            console.log(' === state === ' + state);
            if (component.isValid() && state === "SUCCESS") {
                // get the marketing dos data
                var objMarketingKit =response.getReturnValue(); 
                // Check marketing docs list is empty or not.
                if(!$A.util.isEmpty(objMarketingKit)){
                    // set marketing list docs data into attribute.
                    //component.set("v.marktKitdocs",objMarketingKit.lstMarkProgDocs);
                    // set Terms accepted data into attribute.
                    if(component.get("v.termsAccepted") == false)
                    component.set("v.termsAccepted",objMarketingKit.mkTermsAccepted);
                    debugger;
                    console.log(' === state === ' + objMarketingKit.mkTermsAccepted);
                    // Show the terms window if terms not accpeted.
                    if(component.get("v.termsAccepted") == false){
                        $("#toc").modal('show');
                    }else{
                       //this.openDocument(component,event);
                       var index = component.get("v.productIndex");
                       //alert(index);
                       if(index){
                            var url='';
                            var marketingKitsList = component.get("v.marketingKitsList");
                            var marketingKitsInternalUserList = component.get("v.marketingKitsInternalUserList");
                            
                            // Condition for Non Internal User Data.
                            if(!$A.util.isEmpty(marketingKitsList)){
                                // get current row(User clicked image) from list.
                                var doc = marketingKitsList[index];
                                
                                // Check for survey Url if it  has to rediret.
                               /*if(doc.Product__r.Name === 'Premier Marketing Program' && component.get("v.termsAccepted")){
                                    url = component.get("v.surveyURL");
                                    alert(url);
                                }
                                else{*/
                                    // Next page Url
                                    var prodName = doc.Product__r.Name;
                                    url = "/mvmarketingkitchild?prog="+escape(prodName)+"&lang="+component.get('v.language');; 
                                //}
                            }
                            else if(!$A.util.isEmpty(marketingKitsInternalUserList)){ // Condition for INternal user
                                
                                var doc = marketingKitsInternalUserList[index];
                                if(doc.Product_Name__c){
                                    // Next page Url
                                    var prodName = doc.Product_Name__c;
                                    url = "/mvmarketingkitchild?prog="+prodName+"&lang="+component.get('v.language');
                                }
                            }
                            
                            // check url
                            if(!$A.util.isEmpty(url)){
                                // redirect to next page using salesforce standatrd event "e.force:navigateToURL" 
                                // Get Url Event
                                var urlEvent = $A.get("e.force:navigateToURL");
                                // Set the params to Url Event
                                urlEvent.setParams({
                                    "url": url,
                                    isredirect: "true"
                                });
                                //execute the event. it will redirect to given url.
                                urlEvent.fire();
                            }
                        }
                    }                  
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        // set actio in queue. Lightning actions are asynchronous.
        $A.enqueueAction(action);
    },
    
})
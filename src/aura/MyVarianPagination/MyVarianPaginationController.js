({
    firstPage: function(component, event, helper) {
        component.set("v.currentPageNumber", 1);
        helper.firePaginationEvent(component, event);
    },
    handlePrevPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber")-1, 1));
        helper.firePaginationEvent(component, event);
    },
    handleNextPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber")+1, component.get("v.totalPages")));
        helper.firePaginationEvent(component, event);
    },
    lastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", Math.ceil(component.get("v.totalPages")));
        helper.firePaginationEvent(component, event);
    },
    handleSizeChange: function(component, event, helper) {        
        var totalSize = component.get("v.totalSize");
        var pageSize = component.get("v.pageSize");
        component.set("v.totalPages", Math.ceil(totalSize/pageSize));
        // console.log("old value: " + event.getParam("oldValue"));
        // console.log("current value: " + event.getParam("value"));
    }
})
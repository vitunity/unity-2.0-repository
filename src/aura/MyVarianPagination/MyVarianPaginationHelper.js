({
    firePaginationEvent : function(component, event) {
        var compEvent = component.getEvent("paginationEvent");
        // set  data for the event 
        compEvent.setParams({"recordsOffset" : (Math.max(component.get("v.currentPageNumber")-1)) * component.get("v.pageSize") });
        compEvent.fire(); 
    }
})
({
    doInit : function(component, event, helper) {
       var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	   //set the src param value to my src attribute
       //alert( getUrlParameter('lang'));
       var lang =  getUrlParameter('lang');
       //alert(lang);
       
       component.set("v.language",lang);
       var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":lang});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
           	//alert(JSON.stringify(response.getReturnValue()));
            component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
			   alert(response.getError());
           }      
       } );
       $A.enqueueAction(action);
    },
	
    myAction : function(component, event, helper) {
		
	},
    events : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage"
        });
        urlEvent.fire();
    },
    contactus : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contactus"
        });
        urlEvent.fire();
    },
    home : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/homepage"
        });
        urlEvent.fire();
    }
})
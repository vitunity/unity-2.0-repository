({
    initRecords: function(component, event, helper) {
        var action = component.get("c.getOpportunities");
        action.setParams({
                    'accountId': component.get("v.recordId")
                  });
             action.setCallback(this, function(response) {
              var state = response.getState();
              if (state === "SUCCESS") {
                  var storeResponse = response.getReturnValue();
                  console.log(JSON.stringify(storeResponse));
               // set AccountList list with return value from server.
                  component.set("v.oppList", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    sortByName: function(component, event, helper) {
        helper.sortBy(component, "Name");
    },
    sortByAmount: function(component, event, helper) {
        helper.sortBy(component, "Amount");
    },
    sortByDate: function(component, event, helper) {
        helper.sortBy(component, "CloseDate");
    },
    sortByStage: function(component, event, helper) {
        helper.sortBy(component, "StageName");
    },
    sortByRef: function(component, event, helper) {
        helper.sortBy(component, "SFDC_Oppty_Ref_number__c");
    },   
    
    Save: function(component, event, helper) {
      // Check required fields(Name) first in helper method which is return true/false
        if (helper.requiredValidation(component, event)){
            // call the saveAccount apex method for update inline edit fields update 
            var action = component.get("c.saveOpps");
            action.setParams({
                'oppList': component.get("v.oppList")
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    // set AccountList list with return value from server.
                    component.set("v.oppList", storeResponse);
                    // Hide the save and cancel buttons by setting the 'showSaveCancelBtn' false 
                    component.set("v.showSaveCancelBtn",false);
                    //alert('Updated...');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "The records has been updated successfully."
                    });
                    toastEvent.fire();
                	}
            });
            $A.enqueueAction(action);
        } 
    },
    
    cancel : function(component,event,helper){
       // on cancel refresh the view (This event is handled by the one.app container. It’s supported in Lightning Experience, the Salesforce app, and Lightning communities. ) 
        $A.get('e.force:refreshView').fire(); 
    },
    // function called on click of Cancel and close the popup
    ok: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":component.get("v.recordId")
        });
        navEvt.fire();
        $A.get("e.force:closeQuickAction").fire();
    },
     applyColumnResizing: function (cmp, event, helper) 
    {      
      var thElm;
      var startOffset;

      Array.prototype.forEach.call(
        document.querySelectorAll("table th"),
        function (th) 
        {
          th.style.position = 'relative';

          var grip = document.createElement('div');
          grip.innerHTML = "&nbsp;";
          grip.style.top = 0;
          grip.style.right = '-4px';
          grip.style.bottom = 0;
          grip.style.width = '13px';
          grip.style.position = 'absolute';
          grip.style.cursor = 'col-resize';
          grip.addEventListener('mousedown', function (e) {
              thElm = th;
              startOffset = th.offsetWidth - e.pageX;
          });

          th.appendChild(grip);
        });

      document.addEventListener('mousemove', function (e) {
        if (thElm) {
          thElm.style.width = startOffset + e.pageX + 'px';
        }
      });

      document.addEventListener('mouseup', function () {
          thElm = undefined;
      });
  },
    
})
({
   doInit : function(component, event, helper) 
    {
        var action = component.get('c.fetchQuotes');
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                component.set('v.obj', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
  
    refreshRelated : function(component,event,helper)
    {

       var action = component.get('c.refreshSection');
        action.setParams({
            "recordId": component.get("v.recordId"),
            "obj": JSON.stringify(component.get("v.obj"))
        });

        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                component.set('v.obj', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    selectAll: function(component, event, helper) 
    {
       var selectedHeaderCheck = event.getSource().get("v.value");
       var getAllId = component.find("boxPack");

        if (selectedHeaderCheck == true) 
        {
            if(getAllId.length == undefined)
            {
               getAllId.set("v.value",true);
            }
            else
            {
                for (var i = 0; i < getAllId.length; i++) 
                {
          component.find("boxPack")[i].set("v.value", true);
                }
           }

        } 
        else 
        {
            if(getAllId.length == undefined)
            {
        getAllId.set("v.value",false);
            }
            else
            {
                for (var i = 0; i < getAllId.length; i++) 
                {
          component.find("boxPack")[i].set("v.value", false);
                }
            }
        }
    },

    handleMenuSelect : function(component, event, helper,objName) 
    {
       var obj = component.get("v.obj");
        var id_str = event.currentTarget.getAttribute("data-quoteId");
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
              "url": "/apex/BigMachines__QuoteEdit?id="+id_str+"&siteId="+obj.siteID
          });
        urlEvent.fire();
    },

    newQuote : function (component, event, helper,objName)
    {
        var recId = component.get("v.recordId");
        var obj = component.get("v.obj");
        var action = component.get('c.newQuoteAction');
        action.setParams({
            "recordId": component.get("v.recordId")
        });
        var urlEvent = $A.get("e.force:navigateToURL");
        action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
               var rObj =  response.getReturnValue();
               if(rObj.status == 'success'){
                   urlEvent.setParams({
                       "url": rObj.url
                   });
                   urlEvent.fire();
               }else{
                   alert(rObj.message);
               }
                
            }
        });
        $A.enqueueAction(action);
    },
    
    cloneQuote : function(component, event, helper,objName)
    {
      var getAllId = component.find("boxPack");
      if(getAllId == undefined)
      {
        alert('No quote is available for this opportunity.');  
      }
      else
      {
        var count  = 0;
        var quoteId = null;

        if(!Array.isArray(getAllId))
        {
          if (getAllId.get("v.value") == true) {
              count ++;
              quoteId = getAllId.get("v.text"); 
          }
        }
        else
        {
          for (var i = 0; i < getAllId.length; i++)
          {
            var val = component.find("boxPack")[i].get("v.value");
            if(val == true){
              count++;
            }
          }
        }
        
        if(count > 1 ){
           alert('Please select only one quote to clone.');  
        }else if(count == 0){
           alert('Please select one quote to clone.');  
        }else if (count == 1 && quoteId == null) 
        {
          for (var i = 0; i < getAllId.length; i++)
          {
            var val = component.find("boxPack")[i].get("v.value");
            //alert(val);
            if(val == true){
               quoteId = component.find("boxPack")[i].get("v.text");
            }
          }
        }
        if(quoteId != null)
        {
          var action = component.get('c.cloneQuoteAction');
          action.setParams({
            "recordId": quoteId,
             "oppId" : component.get("v.recordId")
          });
          var urlEvent = $A.get("e.force:navigateToURL");
          action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
               var rObj =  response.getReturnValue();
               if(rObj.status == 'success'){
                 urlEvent.setParams({
                   "url": rObj.url
                 });
                 urlEvent.fire();
               }else{
                 alert(rObj.message);
               }
            }
          });
          $A.enqueueAction(action);
        }
      } 
    },
    
  setAsPrimary : function(component, event, helper,objName)
  {
    var getAllId = component.find("boxPack");
   
    if(getAllId == undefined)
    {
      alert('No quote is available for this opportunity.');  
    }
    else
    { 
      var count  = 0;
      var quoteId = null;

      if(!Array.isArray(getAllId))
      {
        if (getAllId.get("v.value") == true) {
            count ++;
            quoteId = getAllId.get("v.text"); 
        }
      }
      else
      {
        for (var i = 0; i < getAllId.length; i++)
        {
          var val = component.find("boxPack")[i].get("v.value");
          if(val == true){
            count++;
          }
        }
      }
     
      if(count > 1){
         alert("Please select only one quote to be the primary quote");  
      }else if (count == 0) {
        alert("Please check the box next to the quote that you would like to be the primary quote before clicking the 'Set as Primary' button.");
      }
      else if (count == 1  && quoteId == null) 
      {
        for (var i = 0; i < getAllId.length; i++)
        {
          var val = component.find("boxPack")[i].get("v.value");
          if(val == true){
            quoteId = component.find("boxPack")[i].get("v.text");
          }
        }
      }
    
      if(quoteId != null)
      {
        var setAsPrimary  = confirm("Setting a new quote as the primary quote will delete all of the opportunity products on the opportunity that this quote belongs to which are associated with the current primary quote. Do you want to continue?");

        if(setAsPrimary == true)
        {
          var action = component.get('c.setAsPrimaryAction');
          action.setParams({
            "recordId": quoteId
          });
          var navEvt = $A.get("e.force:navigateToSObject");
          action.setCallback(this,function(response){
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
               var rObj =  response.getReturnValue();
               if(rObj.status == 'success'){
                 navEvt.setParams({
                  "recordId": component.get("v.recordId"),
                  
                 });
                 navEvt.fire();
               }else{
                 alert(rObj.message);
               }
            }
          });
          $A.enqueueAction(action);
        }
      }
    } 
  },
    
  moveQuote : function(component, event, helper,objName)
  {
    var getAllId = component.find("boxPack");
 
    if(getAllId == undefined)
    {
      alert('No quote is available for this opportunity.');  
    }
    else
    {
      var count  = 0;
      var quoteId = null;

      if(!Array.isArray(getAllId))
      {
        if (getAllId.get("v.value") == true) {
            count ++;
            quoteId = getAllId.get("v.text"); 
        }
      }
      else
      {
        for (var i = 0; i < getAllId.length; i++)
        {
          var val = component.find("boxPack")[i].get("v.value");
          if(val == true){
            count++;
          }
        }
      }
    
      if(count > 1){
         alert('Please select only one quote to clone.');  
      }else if (count == 0) {
        alert("Please check the box next to the quote that you would like to move before clicking the 'Move' button.");    
      }else if (count == 1 && quoteId == null) 
      {
        for (var i = 0; i < getAllId.length; i++)
        {
          var val = component.find("boxPack")[i].get("v.value");
          if(val == true){
            var quoteId = component.find("boxPack")[i].get("v.text");
          }
        }
      }

      if(quoteId != null)
      {
        var action = component.get('c.moveQuoteAction');
        action.setParams({
          "recordId": quoteId
        });
        var urlEvent = $A.get("e.force:navigateToURL");
        action.setCallback(this,function(response){
          //store state of response
          var state = response.getState();
          if (state === "SUCCESS") {
             var rObj =  response.getReturnValue();
             if(rObj.status == 'success'){
               urlEvent.setParams({
                 "url": rObj.url
               });
               if(rObj.message != ''){
                 if(confirm(rObj.message)){
                   urlEvent.fire();
                 }
               }else{
                 urlEvent.fire();
               }
             }else{
               alert(rObj.message);
             }
          }
        });
        $A.enqueueAction(action);
      }
    } 
  },
    
  openQuote : function(component, event, helper,objName)
  {
      var selectedItem = event.currentTarget;
      var quoteId = selectedItem.dataset.record;
      var workspaceAPI = component.find("workspace");
      var isConsole = false;
      workspaceAPI.isConsoleNavigation().then(function(response) {
          isConsole = true;
          workspaceAPI.getFocusedTabInfo().then(function(response) {
              var focusedTabId = response.tabId;
              workspaceAPI.openSubtab({
                  parentTabId: focusedTabId,
                  url: '#/sObject/'+quoteId+'/view',
                  focus: true
              }).then(function(response){ 
                  var dismissActionPanel = $A.get("e.force:closeQuickAction");
                  dismissActionPanel.fire();
              });
          });
      });
      if(isConsole == false){
          var urlEvent = $A.get("e.force:navigateToSObject");
          urlEvent.setParams({
              "recordId": quoteId,
          });
          urlEvent.fire();
      }
  },

  applyColumnResizing: function (cmp, event, helper) 
  {      
      var thElm;
      var startOffset;

      Array.prototype.forEach.call(
        document.querySelectorAll("table th"),
        function (th) 
        {
          th.style.position = 'relative';

          var grip = document.createElement('div');
          grip.innerHTML = "&nbsp;";
          grip.style.top = 0;
          grip.style.right = '-4px';
          grip.style.bottom = 0;
          grip.style.width = '13px';
          grip.style.position = 'absolute';
          grip.style.cursor = 'col-resize';
          grip.addEventListener('mousedown', function (e) {
              thElm = th;
              startOffset = th.offsetWidth - e.pageX;
          });

          th.appendChild(grip);
        });

      document.addEventListener('mousemove', function (e) {
        if (thElm) {
          thElm.style.width = startOffset + e.pageX + 'px';
        }
      });

      document.addEventListener('mouseup', function () {
          thElm = undefined;
      });
  },

  viewQuote : function(cmp, event, helper)
  {
      var urlEvent = $A.get("e.force:navigateToURL");
      urlEvent.setParams({
        "url": component.get("v.obj").viewAllQuote
      });
       urlEvent.fire();


  },
    
})
({
    //check and download epot
    doInit: function(component, event, helper) {
      	var action = component.get("c.resetQuoteStatus");
        action.setParams({"quoteId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.result",response.getReturnValue());
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    // function called on click of Cancel and close the popup
    ok: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":component.get("v.recordId")
        });
        navEvt.fire();
        $A.get("e.force:closeQuickAction").fire();
    },
    
})
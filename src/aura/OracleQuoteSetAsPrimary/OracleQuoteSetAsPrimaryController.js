({
    //check and download epot
    doInit: function(component, event, helper) {
      	var action = component.get("c.canQuoteEditCheck");
        action.setParams({"recordId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isEdit",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    // function called on click of Cancel and close the popup
    ok: function(component, event, helper) {
        /*var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":component.get("v.recordId")
        });
        navEvt.fire();
        $A.get("e.force:closeQuickAction").fire();*/
        $A.get("e.force:closeQuickAction").fire();
    },
    
    //check and download epot
    setAsPrimary: function(component, event, helper) {
      	var action = component.get("c.setAsPrimaryAction");
        action.setParams({"recordId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //component.set("v.isResult",true);
                //component.set("v.isResult",response.getReturnValue().msg);
                //if(response.getReturnValue().msg == ''){
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId":component.get("v.recordId")
                    });
                    $A.get('e.force:refreshView').fire();
                    navEvt.fire();
                    $A.get("e.force:closeQuickAction").fire();
                //}else{
                    
                //}
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
})
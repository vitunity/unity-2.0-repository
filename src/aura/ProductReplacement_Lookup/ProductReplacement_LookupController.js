({
    getRecords : function(component, event, helper) {
        var prodFamily = component.get("v.prodFamily");
        var objName = component.get("v.objName");
        var objLabel = component.get("v.objLabel");
        var accId = component.get("v.accId");
        //var vendorVal = component.get("v.vendor");
        var vendor = component.get("v.objVen");
        
        if(objLabel == 'Competitors' || objLabel == 'Service Takeout')
        	component.set("v.isVarianProd", false);
        else
            component.set("v.isVarianProd", true);
        var action = component.get("c.getReplacementProducts");
        
        action.setParams({
            "accId": accId,
            "productFamily": prodFamily,
            "objName":objName,
            "objLabel": objLabel,
            "vendor":vendor
        });
        
        action.setCallback(this,function(res){
            var state = res.getState();
            if(state == "SUCCESS") {
                var result = res.getReturnValue();
                console.log(' ==== result ==== ' + JSON.stringify(result));
                component.set('v.sObjRecords',result);
                if(result.length == 0) {
                	component.set('v.hasRec', false);
                } else {
                    component.set('v.hasRec', true);
                }
                //helper.showSpinner(component, false);
            }
        });
        $A.enqueueAction(action);
    	
    },
    
    addRemoveValues : function(component, event, helper) {
        
    	var selectedVal = [];
        selectedVal = component.get("v.selectedText");
        //var prodName = event.getSource().get("v.name");
        var prodName = event.getSource().get("v.name");
        
        var prodwoVendor = prodName.split("(");
        
        var selectedVendor = [];
        selectedVendor = component.get("v.hiddenSelText");
        
        if(selectedVal.indexOf(prodwoVendor[0]) == -1) {
            selectedVal.push(prodwoVendor[0]);
        } else {
            var index = selectedVal.indexOf(prodwoVendor[0]);
            selectedVal.splice(index, 1);
        }
        
        if(selectedVendor.indexOf(prodName) == -1) {//if(selectedVal.indexOf(prodName) == -1) {
            selectedVendor.push(prodName);
        } else {
            var index1 = selectedVendor.indexOf(prodName);
            selectedVendor.splice(index1, 1);
        }
        console.log(' ### selectedVal ### ' + selectedVal);
        console.log(' ### selectedVendor ### ' + selectedVendor);
        component.set("v.hiddenSelText", selectedVendor);//P
        component.set("v.selectedText", selectedVal);
        
    },
    
    addRecords : function(component, event, helper) {
        var setSelectedRecordEvent = component.getEvent("replacementRecordSelected");
        var selectedText = component.get("v.selectedText");
        console.log(' === > selectedRecords ==> ' + selectedText.toString());
        setSelectedRecordEvent.setParams({"prodNames": selectedText.toString()});
        setSelectedRecordEvent.setParams({"recordId": component.get("v.searchTxt")});
        //INC4873200:INC4904281
        setSelectedRecordEvent.setParams({"hiddenRecVal":component.get("v.hiddenSelText")});
        setSelectedRecordEvent.setParams({"hiddenRecId":component.get("v.hidRecId")});
        var selectedVal = [];
    	component.set("v.selectedText", selectedVal);
        setSelectedRecordEvent.fire();
    },
    
})
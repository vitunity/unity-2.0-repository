({
	doInit : function(component, event, helper) {
		helper.initHelper(component, event);
        helper.setIPdata(component, event);
        debugger;
        var value = helper.getParameterByName(component , event, 'inContextOfRef');
        if(value != undefined){
            var context = JSON.parse(window.atob(value));
            if(context.attributes.recordId!= undefined && context.attributes.recordId.startsWith("003")){
                helper.getContactHelper(component, event,context.attributes.recordId);
            }
            if(context.attributes.recordId!= undefined && context.attributes.recordId.startsWith("001")){
                helper.getAccountHelper(component, event,context.attributes.recordId); 
            }
        }
        debugger;
        var relatedToId = component.get('v.relatedToId');
        
        if(relatedToId != undefined && relatedToId.startsWith("003")){
            helper.getContactHelper(component, event,relatedToId);
        }
        if(relatedToId != undefined && relatedToId.startsWith("001")){
            helper.getAccountHelper(component, event,relatedToId); 
        }
        
	},
    
    resetAll : function(component, event, helper) {
        $A.get("e.force:refreshView").fire();
    },
    
    handleLookupChooseEventQuickCase : function(component, event, helper) {
        helper.handleLookupChooseEventQuickCaseHelper(component,event);
    },
    
    handleQuickCaseCreateContactEvent : function(component, event, helper) {
        helper.handleQuickCaseCreateContactEventHelper(component,event);
    },
    
    saveCaseRecord : function(component, event, helper) {
    	var contactCheck = component.find('contactObjId').get('v.value');
        var ipCheck = component.find('installedProduct').get('v.value');
        console.log('contactCheck: '+contactCheck+', ipCheck: '+ipCheck);
        //Check fields to make sure user hasn't cleared object values.
        if(contactCheck && ipCheck){
            helper.caseCreateHelper(component, event);
        }else{
            component.set('v.isError',true);
            component.set('v.errorMsg','Please select Contact and Product.');
        }
    },
    
    accountChanged : function(component, event, helper) {
        helper.getCasedata(component, event);
	},
    
    updateSelectedRows : function(component, event, helper) {
    	helper.updateSelectedRowsHelper(component, event);
    },
    
    updateContactSelectedRows : function(component, event, helper) {
    	helper.updateSelectedContactRowsHelper(component, event);
    },
    
    closeIPComp: function(component, event, helper) {
        var modal = component.find('iPComp');
        $A.util.addClass(modal,'slds-hide');
    },
    
    openIPComp: function(component, event, helper) {
        var modal = component.find('iPComp');
        $A.util.removeClass(modal,'slds-hide');
        helper.setIPdata(component,event);
    },
    
    searchIPComp: function(component, event, helper) {
        helper.setIPdata(component,event);
    },
    handleExpandClick: function(component, event, helper) {
    },
    handleCollapseClick: function(component, event, helper) {
    },
    
    closeContactComp: function(component, event, helper) {
        var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');
    },
    
    openContactComp: function(component, event, helper) {
        var modal = component.find('contactCmp');
        $A.util.removeClass(modal,'slds-hide');
        helper.setContactdata(component,event);
    },
    
    searchContactComp: function(component, event, helper) {
        helper.setContactdata(component,event);
    },
        
    closeCreateContact: function(component, event, helper) {
        var modal = component.find('createContact');
        $A.util.addClass(modal,'slds-hide');
    },   
    
    handleCreatedContactEvent: function(component, event, helper) {
        helper.handleCreatedContactEventHelper(component, event);
    },
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    updateContactColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortContactData(cmp, fieldName, sortDirection);
    },
    createContact : function (component,event,helper) {
		var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');        
        var createContactEvent = component.getEvent("createContact");
        createContactEvent.setParams({
            "sObjectType" : "Contact",
            "actionType"  : "Create"
        });
        createContactEvent.fire();
        console.log('event fired');
    },
    
    editContact : function (component,event,helper) {
        var createContactEvent = component.getEvent("createContact");
        createContactEvent.setParams({
            "sObjectType" : "Contact",
            "actionType"  : "Edit"
        });
        createContactEvent.fire();
        console.log('event fired');
    }
 
})
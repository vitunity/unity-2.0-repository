({
    getParameterByName: function(component, event, name) {
        debugger;
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var regex = new RegExp("[?&]" + name + "(=1\.([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    },
    
    getContactHelper: function(component, event,contactId) {
        debugger;
        var action = component.get("c.getContact");
        action.setParams({contactId : contactId});
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var conObj = response.getReturnValue();
            var caseObj = component.get('v.caseObj');
            var accountObj = component.get('v.accountObj');
            accountObj.Name = conObj.Account.Name;
            accountObj.Id = conObj.AccountId;
            conObj.Name = conObj.FirstName+ ' '+conObj.LastName;
            caseObj.AccounId = conObj.AccountId;
            component.set('v.accountObj',accountObj);
            component.set('v.caseObj',caseObj);
            component.set('v.conObj',conObj);
            this.getCasedata(component, event);
            component.find("contactObjId").set("v.value",conObj.Name);
        });
        $A.enqueueAction(action);
    },
    
    getAccountHelper: function(component, event,accountId) {
        debugger;
        var action = component.get("c.getAccount");
        action.setParams({accountId : accountId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var accObj = response.getReturnValue();
            var caseObj = component.get('v.caseObj');
            caseObj.AccounId = accObj.Id;
            component.set('v.accountObj',accObj);
            component.set('v.caseObj',caseObj);
            this.getCasedata(component, event);
        });
        $A.enqueueAction(action);
    },
    
    initHelper : function(component, event) {
        component.set('v.caseColumns', [
            {label: 'Created', fieldName: 'CreatedDate', sortable: true,type: 'date'},
            {label: 'Case#', fieldName: 'caseUrl',  sortable: true, type: 'url',
                  typeAttributes: { label: { fieldName: 'CaseNumber' }, target: '_blank' }},
            {label: 'Subject', sortable: true, fieldName: 'Subject', type: 'test'},
            {label: 'Product/System', sortable: true, fieldName: 'productURL', width:400, type: 'url',
            		typeAttributes: { label: { fieldName: 'productName' },target: '_blank' }},
            {label: 'Status', sortable: true, fieldName: 'Status', type: 'test'},
            {label: 'Contact', sortable: true, fieldName: 'contactName', type: 'lookup'},
            {label: 'Case Owner', sortable: true, fieldName: 'ownerName', type: 'lookup'},
            {label: 'Priority', sortable: true, fieldName: 'Priority', type: 'text'}
        ]);
        component.set('v.gridContactColumns', [
            {label: 'Name', fieldName: 'name', sortable: true,type: 'text'},
            {label: 'Title', sortable: true, fieldName: 'Title', type: 'text'},
            {label: 'Phone', sortable: true, fieldName: 'Phone', type: 'text'},
            {label: 'Email', sortable: true, fieldName: 'Email', type: 'text'},
            {label: 'Email Declined', sortable: true, fieldName: 'EmailDeclined', type: 'text'}
        ]);
    },
    
    setIPdata : function(component, event) {
		 var columns = [
            {type: 'text',fieldName: 'name', label: 'Name',initialWidth: 200},
            {type: 'text',fieldName: 'productName',label: 'Product Name'},
            {type: 'text',fieldName: 'status',label: 'Status'},
            {type: 'text',fieldName: 'location',label: 'Location'},
            {type: 'text',fieldName: 'pcodeRef',label: 'PCode is Reference Only'},
            {type: 'text',fieldName: 'billingType',label: 'Billing Type'},
            {type: 'text',fieldName: 'contractName',label: 'Contract'},
            {type: 'date',fieldName: 'expDate',label: 'Exp Date'}
        ];
        component.set('v.gridColumns', columns);
        var accountId = component.get('v.caseObj').AccountId;
        var action = component.get("c.getIPs");
        var ipSearchStrTag =  component.find('ipSearchStr');
        var ipSearchStr  = '';
        if(ipSearchStrTag != undefined){
			ipSearchStr = ipSearchStrTag.get('v.value');
        }
        action.setParams({ accountId : accountId,searchStr:ipSearchStr});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            component.set('v.gridData',JSON.parse(response.getReturnValue()));
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    
    setContactdata : function(component, event) {
		debugger;
        var accountId = component.get('v.caseObj').AccountId;
        var action = component.get("c.getContacts");
        var contactSearchStrTag =  component.find('contactSearchStr');
        var contactSearchStr  = '';
        if(contactSearchStrTag != undefined){
			contactSearchStr = contactSearchStrTag.get('v.value');
        }
        action.setParams({ accountId : accountId,searchStr:contactSearchStr});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            debugger;
            var responseWrap = response.getReturnValue(); 
            for(var i = 0; i < responseWrap.length; i++) {
                var contactObj = responseWrap[i];
                if(contactObj.Account != undefined )
                contactObj.account = contactObj.Account.Name;
                contactObj.name = contactObj.FirstName + ' '+contactObj.LastName;
                if(contactObj.Email_Declined__c == true){
                    contactObj.EmailDeclined = 'Yes';
                }else{
                    contactObj.EmailDeclined = 'No';
                }
            }
            component.set('v.gridContactData',responseWrap);
            console.log(response.getReturnValue());
        });
        $A.enqueueAction(action);
	},
    
    getCasedataQuery : function(component,accountId) {
    	debugger;
        var copcon = component.find("createConID");
        copcon.setAccountId(accountId);
        var action = component.get("c.getCases");
        var spInstruction = '';
        var englishIsOK = false;
    	action.setParams({accountId : accountId});
        action.setCallback(this, function(response) {
       		var state = response.getState();
            var responseWrap = response.getReturnValue(); 
            for(var i = 0; i < responseWrap.length; i++) {
                var caseObj = responseWrap[i];
                caseObj.caseUrl="/"+caseObj.Id;
                if(caseObj.Contact != undefined)
                caseObj.contactName = caseObj.Contact.Name;
                if(caseObj.Owner != undefined)
                caseObj.ownerName = caseObj.Owner.Name;
                if(caseObj.ProductSystem__r != undefined){
                    caseObj.productName = caseObj.ProductSystem__r.Name + ' - '+ caseObj.ProductSystem__r.SVMXC__Product__r.Name;
                    caseObj.productURL = "/"+caseObj.ProductSystem__c;
                }
                spInstruction = caseObj.Account.Special_Care_Instruction_New__c;
                if(caseObj.Account.Prefered_Language__c != undefined 
                   && (caseObj.Account.Prefered_Language__c != 'EN')){
                	englishIsOK = 'No';
                }
            }
            debugger;
            component.find("specialInfo").set('v.value',spInstruction);
            if(englishIsOK == 'No'){
                var engOkTag = component.find("englishOK");
                var engOkTagP = component.find("englishOKP");
                engOkTag.set('v.value','false');
                $A.util.removeClass(engOkTagP,'slds-hide');
                $A.util.removeClass(engOkTag,'slds-hide');
            }else{
                var engOkTag = component.find("englishOK");
                var engOkTagP = component.find("englishOKP");
                $A.util.addClass(engOkTagP,'slds-hide');
                $A.util.addClass(engOkTag,'slds-hide');
            }
            
            component.set('v.caseList',responseWrap);
            this.sortData(component, component.get("v.sortedBy"), component.get("v.sortedDirection"));
        });
        $A.enqueueAction(action);
    },
    
    getCasedata : function(component, event) {
        debugger;
        var caseObj = component.get("v.caseObj");
        var accountId = component.get('v.accountObj').Id;
        if(accountId != undefined){
            caseObj.AccountId = accountId;
            var copcon = component.find("createConID");
            copcon.setAccountId(accountId);
    		this.getCasedataQuery(component,accountId);
        }else{
            caseObj.AccountId = null;
        }
        component.set('v.caseObj',caseObj);
    },
   
    caseCreateHelper : function(component, event) {
        debugger; 
        var craRole;
        var craPhone;
        var craTag = component.find("isCRA");
        var isCRA = false;
        if(craTag != undefined){
            var craRole = component.find("craRole").get('v.value');
            var craPhone = component.find("craPhone").get('v.value');
            isCRA = craTag.get('v.checked');
            if(craTag.get('v.checked') && (craRole == '' || (craPhone == '' || craPhone == null))){
                component.set('v.isError',true);
                component.set('v.errorMsg','Please select CRA Role and Phone.');
                return;
            }else{
                component.set('v.isError',false);
                component.set('v.errorMsg','');
            }
        }
        var action = component.get("c.saveCase");
        var caseObj = component.get("v.caseObj");
        var accountId = caseObj.AccountId;
        var subject = component.find('subject').get('v.value');
        var desc = component.find('desc').get('v.value');
        var caseReason = component.find('caseReason').get('v.value');
		var priority = component.find('priority').get('v.value');
        var specialInfo = component.find('specialInfo').get('v.value');
        var ipObj = component.get('v.ipObj');
        var conObj = component.get('v.conObj');
        caseObj.ProductSystem__c = ipObj.Id;
        caseObj.ContactId = conObj.Id;
        component.set('v.caseObj',caseObj);
        console.log(accountId+' '+subject+' '+desc+' '+caseReason+' '+priority+' '+specialInfo);
        caseObj.AccountId = accountId;
        caseObj.Subject = subject;
        caseObj.Description = desc;
        caseObj.Case_Special_Care_Instructions__c = specialInfo;
        caseObj.Priority = priority;
        caseObj.Reason = caseReason;
        var isEnglishOKTag = component.find('englishOK');
        if(isEnglishOKTag != undefined){
            caseObj.English_OK__c = isEnglishOKTag.get('v.value')
        }
        
        if(accountId == '' || accountId == null){
             component.set('v.isError',true);
             component.set('v.errorMsg','Please select account.');
             return;
        }else if(caseObj.ContactId == '' || caseObj.ContactId == null){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please select contact.');
            return;
        }else if(caseObj.ProductSystem__c == '' || caseObj.ProductSystem__c == null){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please select Product PCSN.');
            return;
        }else if(subject == '' ||subject == null){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please enter subject.');
            return;
        }/*else if(desc == '' || desc == null){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please enter description.');
            return;
        }*/else if(caseReason == ''){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please select Case Reason.');
            return;
        }else if(priority == ''){
            component.set('v.isError',true);
            component.set('v.errorMsg','Please select Priority');
            return;
        }else{
			component.set('v.isError',false);
            component.set('v.errorMsg','');
            component.set('v.disabled',true);
        }
        
        
        action.setParams({ caseData : caseObj,
                          isCRA: isCRA,
                          craRole:craRole,
                          craPhone:craPhone
                         });
        action.setCallback(this, function(response) {
            debugger;
            component.set('v.disabled',false);
       		var state = response.getState();
            if (state === "SUCCESS") {
                var caseId = response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Case created successfully!"
                });
                toastEvent.fire();
            	this.closeFocusedTabAndOpenNewTab(component, caseId);
            }else if (state === "ERROR") {
                // Process error returned by server
                var error = response.getError()[0];
                console.log(JSON.stringify(response.getError()));
                // Parse custom error data & report it
                var errorData = JSON.parse(error.message);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": errorData.message
                });
                toastEvent.fire();
            }else {
                // Handle other reponse states
            }
            
        });
        $A.enqueueAction(action);
    },
    
    closeFocusedTabAndOpenNewTab : function(component, caseId) {
        debugger;
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            console.log(focusedTabId);
            //Opening New Tab
            workspaceAPI.openTab({
                url: '#/sObject/'+caseId+'/view'
            }).then(function(response) {
                workspaceAPI.focusTab({tabId : response});
            })
            .catch(function(error) {
                console.log(error);
            });

            //Closing old one
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            var event = $A.get( 'e.force:navigateToSObject' );
            event.setParams({
                'recordId' : caseId
            }).fire();
            console.log(error);
        });
    },
    
    updateSelectedRowsHelper : function(component, event) {
        var selectedRows = event.getParam('selectedRows');  
        console.log(JSON.stringify(event));
        var setRows = [];
        for (var i = 0; i < selectedRows.length; i++){
           component.find("installedProduct").set("v.value",selectedRows[i].name); 
           var ipObj = component.get('v.ipObj');
           ipObj.Id = selectedRows[i].ipId;
           ipObj.Name = selectedRows[i].name;
           component.set('v.ipObj',ipObj);
        }
        component.set("v.gridSelectedRows", setRows);
        var modal = component.find('iPComp');
        $A.util.addClass(modal,'slds-hide');
        
    },
    
    updateSelectedContactRowsHelper : function(component, event) {
        debugger;
        var selectedRows = event.getParam('selectedRows');  
        console.log(JSON.stringify(event));
        var setRows = [];
        for (var i = 0; i < selectedRows.length; i++){
           component.find("contactObjId").set("v.value",selectedRows[i].name); 
           var conObj = component.get('v.conObj');
           conObj.Id = selectedRows[i].Id;
           conObj.Name = selectedRows[i].name;
           conObj.AccountId = selectedRows[i].AccountId;
           component.set('v.conObj',conObj);
           var caseObj = component.get("v.caseObj");
           caseObj.ContactId = selectedRows[i].Id;
           component.set('v.caseObj',caseObj);
        }
        component.set("v.gridContactSelectedRows", setRows);
        var modal = component.find('contactCmp');
        $A.util.addClass(modal,'slds-hide');
        
    },
    
    handleLookupChooseEventQuickCaseHelper : function(component, event, helper) {
        try{
            debugger;
            var data = event.getParam("lookUpData");
            console.log(JSON.stringify(data));
            //alert(JSON.stringify(data));
            if(data.Id.startsWith("001")){
                //console.log(JSON.stringify(component.get('v.conObj')));
                //console.log(JSON.stringify(data));
                component.set("v.accountObj",data);
                this.getCasedataQuery(component, data.Id);
            }
            if(data.Id.startsWith("005")){
                component.set('v.groupStr','');
            }
            if(data.Id.startsWith("00G")){
                component.set('v.userStr','');
            }
            
            if(data.AccountId == undefined) return;
            //alert(JSON.stringify(data));
            //var account = component.find('accId');
            //account.set('v.value',data.AccountId);
            var caseObj = component.get("v.caseObj");
            caseObj.AccountId = data.AccountId;
            var accountObj = component.get("v.accountObj");
            accountObj.Name = data.Account_Name__c;
            accountObj.Id = data.AccountId;
            component.set("v.accountObj",accountObj);
            component.set("v.caseObj",caseObj);
            if(data.SVMXC__Company__c != undefined){
                component.find("installedProduct").set("v.value",data.Name); 
                var ipObj = component.get('v.ipObj');
                ipObj.Id = data.Id;
                ipObj.Name = data.Name;
                component.set('v.ipObj',ipObj);            
            }
            if(data.Id.startsWith("003")){
                component.set("v.conObj",data);
                component.find("contactObjId").set("v.value",data.Name); 
            }
            this.getCasedataQuery(component,data.AccountId); 
        }catch(error){
        }
    },
    
    handleQuickCaseCreateContactEventHelper : function(component, event) {
        debugger; 
        //open contact screen to create
        var sObject = event.getParam("sObjectType");
        var actionType = event.getParam("actionType");
        //alert(sObject+'='+actionType);
        var modal = component.find('createContact');
        var caseObj = component.get("v.caseObj");
        modal.set("v.accountId",caseObj.AccountId);
        var copcon = component.find("createConID");
        debugger;
        if(actionType == 'Clean'){
            /*component.set('v.conObj',{}); 
            component.set('v.ipObj',{});
            component.set('v.accountObj',{});
            if(component.find("contactObjId") != undefined)
            component.find("contactObjId").set("v.value",'');
            if(component.find("installedProduct") != undefined)
            component.find("installedProduct").set("v.value",'');
            var caseObj = component.get('v.caseObj');
            caseObj.AccountId = null;
            component.set('v.caseObj',caseObj);*/
        }else if(actionType == 'Edit'){
            copcon.setContactId(caseObj.ContactId);
            $A.util.removeClass(modal,'slds-hide');
        }else{
            copcon.setContactId(null);
            $A.util.removeClass(modal,'slds-hide');
        }
    },
    handleCreatedContactEventHelper: function(component, event) {
        debugger;
        var data = event.getParam("contactRecord");
        var caseObj = component.get("v.caseObj");
        var conObj = component.get("v.conObj");
        caseObj.ContactId = data.Id;
        caseObj.AccountId = data.AccountId;
        conObj.Name = data.Name;
        conObj.Id = data.Id;
        conObj.AccountId = data.AccountId;
        component.set("v.caseObj",caseObj);
        component.set("v.conObj",conObj);
        component.find("contactObjId").set("v.value",data.Name);
        var modal = component.find('createContact');
        $A.util.addClass(modal,'slds-hide');
        this.getCasedataQuery(component,data.AccountId); 
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.caseList");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.caseList", data);
    },
    
    sortContactData: function (component, fieldName, sortDirection) {
        var data = component.get("v.gridContactData");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.gridContactData", data);
    },
    
    sortIpData: function (component, fieldName, sortDirection) {
        var data = component.get("v.caseList");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.caseList", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})
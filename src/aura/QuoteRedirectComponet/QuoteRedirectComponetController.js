({
    doInit : function(component, event) {
        // the function that reads the url parameters
        //alert(window.location.href);
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.href.substring(2)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        //set the src param value to my src attribute
        //component.set("v.src", getUrlParameter('src'));
        var id = getUrlParameter('id');
        var oppId = getUrlParameter('oppId');
        var clone = getUrlParameter('clone');
        component.set("v.quoteId", id);
        component.set("v.oppId", oppId );
        component.set("v.isClone", clone);
        var urlref = '/apex/Bigmachines__QuoteEdit?clone=true&oppId='+oppId+'&id='+id+'&_bm_trail_refresh_=true';
        component.set("v.url", urlref);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({"url": urlref});
        urlEvent.fire();
    }
})
({
	doInit : function(component, event, helper) {
        console.log('Here');
        var recordId = component.get("v.recordId");
        if(recordId===null || recordId===undefined)
        	alert('Record Id not exist.');
        
        helper.accessPermission(component);
	},
    removeUser: function(component, event, helper) {
        helper.resetAllMsgs(component);
        //var collabId = event.getSource().getLocalId();
        var collabId = event.target.id;
        helper.removeUserFromLst(component, collabId);
    }
})
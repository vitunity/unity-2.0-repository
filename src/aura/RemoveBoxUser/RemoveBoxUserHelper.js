({
    accessPermission: function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.checkPermission");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });

        var self = this;
        action.setCallback(this, function(a) {
            var isUserAuthorised = a.getReturnValue();
        	var spinnerElem = component.find('spinnerId').getElement();
        	$A.util.addClass(spinnerElem, 'slds-hide');

            if(isUserAuthorised) {
                component.set('v.isAuthorised', true);
                self.getAllBoxUser(component);
            } else {
                component.set('v.showErrMsg', true);
            }

        });

        $A.enqueueAction(action);
    },
	getAllBoxUser: function(component) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAllBoxUserFromBoxLst");
    
        action.setParams({
            recordId: component.get("v.recordId")
        });
            
        action.setCallback(this, function(a) {
            var userList = a.getReturnValue();
            if(userList) {
                component.set('v.LstUser', userList);
            }
        });

        $A.enqueueAction(action);
    }, 
    removeUserFromLst: function(component, collabId) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.RemoveCollaboration");
    
        action.setParams({
            CollaborationId: collabId
        });

		var self = this;
        action.setCallback(this, function(a) {
            var isDeleted = a.getReturnValue();
            if(isDeleted) {
                self.setMsg(component, 'infoMsg', 'User removed');
                self.getAllBoxUser(component);
            } else {
                self.setMsg(component, 'errMsg', 'Unexpected error');
            }
            self.hideSpinner(component);
        });

        this.showSpinner(component);
        $A.enqueueAction(action);        
    },
    resetAllMsgs: function(component) {
        
        var spinnerElem = component.find('spinnerId').getElement();
        var errElem = component.find('errMsg').getElement();
        var infoElem = component.find('infoMsg').getElement();

		$A.util.addClass(errElem, 'slds-hide');
        $A.util.addClass(infoElem, 'slds-hide');
        $A.util.addClass(spinnerElem, 'slds-hide');
        
        infoElem.innerText = '';
        errElem.innerText = '';
    },
    setMsg: function(component, strText, msg) {
        var elem = component.find(strText).getElement();
        $A.util.removeClass(elem, 'slds-hide');
        elem.innerText = msg;
    },
    showSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.removeClass(spinnerElem, 'slds-hide');
    },
    hideSpinner: function(component) {
        var spinnerElem = component.find('spinnerId').getElement();
        $A.util.addClass(spinnerElem, 'slds-hide');
    }
})
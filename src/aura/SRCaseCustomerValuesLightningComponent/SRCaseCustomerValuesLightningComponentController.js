({
	doInit : function(component, event, helper) 
	{        
       //
    },

    saveData : function(component, event, helper) 
	{    
       helper.showSpinner(component, event, helper);
        //Get input date for Malfunction Start, and convert back to UTC from User timezone
        var d1Prox = new Date(component.find("mlStartDateId").get("v.value"));
        d1Prox.setTime(d1Prox.getTime() - (d1Prox.getTimezoneOffset()*60000));
        d1Prox = d1Prox.toISOString();
        //Get input date for Preferred Start, and convert back to UTC from User timezone
        var d2Prox = new Date(component.find("prStartDateId").get("v.value"));
        d2Prox.setTime(d2Prox.getTime() - (d2Prox.getTimezoneOffset()*60000));
        d2Prox = d2Prox.toISOString();
        //Get input date for Preferred End, and convert back to UTC from User timezone
        var d3Prox = new Date(component.find("prEndDateId").get("v.value"));
        d3Prox.setTime(d3Prox.getTime() - (d3Prox.getTimezoneOffset()*60000));
        d3Prox = d3Prox.toISOString();
        
        //Update to check for individual pieces of date formatting.
       var d1 = helper.dateWrapper(component, event, helper,d1Prox);
       var d2 = helper.dateWrapper(component, event, helper,d2Prox);
       var d3 = helper.dateWrapper(component, event, helper,d3Prox);
     
       if( isNaN(d3.hour) || isNaN(d2.hour) || isNaN(d3.min) || isNaN(d2.min) || isNaN(d1.hour) || isNaN(d1.min))
       {
            component.find('notifLib').showNotice({
		            "variant": "error",
		            "header": "Please enter the hour and minute values!",
            closeCallback: function() {
              }
           });
           helper.hideSpinner(component, event, helper);
       }else if(component.find('prStartDateId').get("v.value") < component.find('mlStartDateId').get("v.value"))
       {
           component.find('notifLib').showNotice({
               "variant": "error",
               "header": "Preferred Start cannot be Before Malfunction Start",
           closeCallback: function(){ 
               }
           });
           helper.hideSpinner(component, event, helper);
       }else if(component.find('prStartDateId').get("v.value") > component.find('prEndDateId').get("v.value"))
       {
           component.find('notifLib').showNotice({
               "variant": "error",
               "header": "Preferred End cannot be before Preferred Start",
           closeCallback: function(){ 
               }
           });
           helper.hideSpinner(component, event, helper);
       }else if(component.find('mlStartDateId').get("v.value") > component.find('prEndDateId').get("v.value"))
       {
           component.find('notifLib').showNotice({
               "variant": "error",
               "header": "Preferred End cannot be before Malfunction Start",
           closeCallback: function(){ 
               }
           });
           helper.hideSpinner(component, event, helper);
       }
       else
       { 
            var action = component.get("c.saveDates");
	        action.setParams({
	            "d1": JSON.stringify(d1),
	            "d2": JSON.stringify(d2),
	            "d3": JSON.stringify(d3),
	            "caseId" : component.get("v.recordId"),
	        });
           	console.log('action: '+JSON.stringify(d1)+', '+JSON.stringify(d2)+', '+JSON.stringify(d3));
            action.setCallback(this,function(response){
	            var state = response.getState();
	            if (state === "SUCCESS"){
                    if(response.getReturnValue().includes("Success"))
                    {
                    	$A.get('e.force:refreshView').fire();
                    	var toastEvent = $A.get("e.force:showToast");
					    toastEvent.setParams({
					        "title": "Success!",
					        "message": "The record has been updated successfully.",
					        "type" : "success"
					    });
					    toastEvent.fire();
                    }
                    else
                    {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
					        "title": "Error!",
					        "message": "Internal error. Please try again.",
					        "type" : "error"
					    });
					    toastEvent.fire();

                    }
	                helper.hideSpinner(component, event, helper);
		            helper.toggleClass(component,false,'dateTimePopup');
                }
           });
           $A.enqueueAction(action);
	    }
    },
    
    onMlChange : function(component, event, helper) 
	{        
       var mlSDChange = component.find("mlStartDateId").get("v.value");
       component.find("prStartDateId").set("v.value",mlSDChange);
    },

    openModal : function(component, event, helper) 
	{        
        //IMPORTANT:  Salesforce does Locale offset AND local browser offset.  We're accounting for browser and assuming SFDC is the same.
        var caseRec = component.get("v.simpleRecord");
       //get Date from helper and create Date object to account for offset
        var mlSD = new Date(helper.dateFormat(component, event, helper,caseRec.Customer_Malfunction_Start_Date_time_Str__c));
       var prSD = new Date(helper.dateFormat(component, event, helper,caseRec.Customer_Requested_Start_Date_Time__c));
       var prED = new Date(helper.dateFormat(component, event, helper,caseRec.Customer_Requested_End_Date_Time__c)); 
        //have to convert by user timezone offset
        mlSD.setTime(mlSD.getTime() + (mlSD.getTimezoneOffset()*60000));
        prSD.setTime(prSD.getTime() + (prSD.getTimezoneOffset()*60000));
        prED.setTime(prED.getTime() + (prED.getTimezoneOffset()*60000));
        console.log('mlSD: '+mlSD+', prSD: '+prSD+', prED: '+prED);
        
        //note existing bug in lightning component where cannot pass date to inputDateTime
       if(mlSD != undefined && mlSD != null){
         	component.find("mlStartDateId").set("v.value",mlSD.toISOString());
       }

        if(prSD != undefined && prSD != null){
          	component.find("prStartDateId").set("v.value",prSD.toISOString());
        }

        if(prED != undefined && prED != null){
          	component.find("prEndDateId").set("v.value",prED.toISOString());
        } 

        helper.toggleClass(component,true,'dateTimePopup');
    },

    hideModal : function(component, event, helper) 
	 {        
       helper.toggleClass(component,false,'dateTimePopup');
    },

    handleRecordUpdated : function(component, event, helper) 
	{    
     	//
    }  
})
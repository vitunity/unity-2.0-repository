({
	toggleClass: function(component,showModal, modalName) {

		var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
	},
	showSpinner : function(component, event, helper) 
	{
		$A.util.removeClass(component.find('spinner'), "slds-hide");
	},

    hideSpinner : function(component, event, helper) 
	{
		 $A.util.addClass(component.find('spinner'), "slds-hide");  
	},
	
    dateFormat : function(component, event, helper, inDate) 
	{
        //convert from string to acceptable input string with zulu time to prevent timezone use.
        const SECONDS = ':00.000Z';
        var split_full = inDate.split(' ');  
        var split_date = split_full[0];
        var split_time = split_full[1];
        var split_AMPM = split_full[2];
        var split_dayMonthYear = split_date.split('-');
        let [hours, minutes] = split_time.split(':');
        if (hours === '12'){
            hours = '00';
        }
        if (split_AMPM === 'PM'){
            hours = parseInt(hours, 10) + 12;
        }
        var newTime = hours+':'+minutes;
        var day = split_dayMonthYear[0];
        var monthText = split_dayMonthYear[1];
        var year = split_dayMonthYear[2];
        var monthNumeral = (function(){
            if(monthText == 'Jan'){
                return '01';
            }else if(monthText == 'Feb'){
                return '02';
            }else if(monthText == 'Mar'){
                return '03';
        	}else if(monthText == 'Apr'){
                return '04';
        	}else if(monthText == 'May'){
                return '05';
        	}else if(monthText == 'Jun'){
                return '06';
        	}else if(monthText == 'Jul'){
                return '07';
        	}else if(monthText == 'Aug'){
                return '08';
        	}else if(monthText == 'Sep'){
                return '09';
        	}else if(monthText == 'Oct'){
                return '10';
        	}else if(monthText == 'Nov'){
                return '11';
        	}else if(monthText == 'Dec'){
                return '12';
        	}
        })();
        var finalDate = year+'-'+monthNumeral+'-'+day+'T'+newTime+SECONDS;
        return finalDate;
	},
    
    /*dateWrapper : function(component, event, helper, inp)
	{
        var checkD = new Object();
        var event = new Date(inp);
        checkD.fullYear = '';
        checkD.month = '';
        checkD.dt = '';
        checkD.hour = '';
        checkD.min = '';
        checkD.timeFormat = ''; 

        if(event != null && event != undefined)
        {
	        checkD.fullYear = event.getFullYear();
	        checkD.month = event.getMonth() + 1;
	        checkD.dt = event.getDate();
	        //checkD.hour = event.getHours();
	        checkD.min = event.getMinutes(); 

	        if(event.getHours() != null && event.getHours() != undefined)
	        {

	        	if(event.getHours() > 12)
	        	{
                    checkD.hour = event.getHours() - 12;
                    checkD.timeFormat = 'PM';
	        	}
	        	else
	        	{

                    checkD.hour = event.getHours() ;
                    checkD.timeFormat = 'AM';
	        	}
	        }
        }
       return checkD;
	}*/
    //New date wrapper to parse date to individual fields for reconstruction - do not use new date object.
    dateWrapper: function(component, event, helper, inp){
        var checkD = new Object();
        checkD.fullYear = inp.slice(0,4);
        checkD.month = inp.slice(5,7);
        checkD.dt = inp.slice(8,10);
        checkD.hour = inp.slice(11,13);
        checkD.min = inp.slice(14,16);
        checkD.min = ('00'+checkD.min).slice(-2);
        checkD.timeFormat = '';
        if(checkD.hour != null && checkD.hour != undefined){
            if(checkD.hour > 12){
                checkD.hour = checkD.hour - 12;
                checkD.timeFormat = 'PM';
            }else if(checkD.hour == '12'){
                checkD.timeFormat = 'PM'; 
            }else{
                checkD.timeFormat = 'AM';
            }
            checkD.hour = ('00'+checkD.hour).slice(-2);
            if(checkD.hour == '00'){
                checkD.hour = '12';
            }
        }
        return checkD;
    }

})
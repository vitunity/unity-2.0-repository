({
    doInit : function(component, event, helper) {
        // Get a reference to the bigmachines Quote function defined in the Apex controller
		var action = component.get("c.initializePrepareOrderForm");
        action.setParams({
            "quoteId": component.get("v.recordId")
        });
        helper.showMessages(component, "", "prepareOrderTabValidation", "prepareOrderValidationMessage", false, "", "");
        helper.toggleClass(component,true,"orderSpinner");
        // Register the callback function
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                helper.checkPartners(returnValue);
                var prepareOrderObject = JSON.parse(JSON.stringify(returnValue));
                component.set("v.prepareOrderController", prepareOrderObject);
                component.set("v.saasProducts",prepareOrderObject.sAASQuoteProducts);
                
                
                console.log('----saasproducts--'+JSON.stringify(component.get("v.saasProducts")));
                helper.getSalesOrgsValues(prepareOrderObject.salesOrgs);
                console.log('----'+JSON.stringify(returnValue));
                component.set("v.bmiQuote", returnValue.objQuote);
                 console.log('----***v.bmiQuote****'+JSON.stringify(returnValue.objQuote));

                var prepareOrderValidationMessage = helper.setPrepareOrderVisibility(component, returnValue);
                if(prepareOrderValidationMessage != ""){
                    helper.showMessages(component, prepareOrderValidationMessage, "prepareOrderTabValidation", "prepareOrderValidationMessage", true, "slds-theme--error", "slds-theme--success");
                }
            }
            
            helper.toggleClass(component,false,"orderSpinner");

			/* Set POS Details */
			if(returnValue.objQuote.Order_Type__c=='Services') {
				component.find('is_pos_contract').set("v.value",returnValue.objQuote.Is_POS_Contract__c);
				component.find('verify_pos_checkbox').set("v.value",returnValue.objQuote.Is_POS_Verified__c);
				if(returnValue.objQuote.POS_Capital_Quote_Number__c!=null) {
					component.find('lookup_latest_pos_revision').set("v.value",returnValue.objQuote.POS_Capital_Quote_Number__c);
				} else {
					component.find('lookup_latest_pos_revision').set("v.value",returnValue.objQuote.Quote_Reference__c);
					if(returnValue.objQuote.Quote_Reference__c!=null && returnValue.objQuote.Quote_Reference__c!='') {
						component.find('is_pos_contract').set("v.value",true);
						//component.find('verify_pos_checkbox').set("v.value",true);
					} else {
						component.find('is_pos_contract').set("v.value",false);
						component.find('verify_pos_checkbox').set("v.value",false);
					}
				}
			} else if(returnValue.objQuote.Order_Type__c!='Services') {
				component.find('is_pos_contract_in_house').set("v.value",returnValue.objQuote.Is_POS_IN_HOUSE__c);
				if(returnValue.objQuote.POS_Contract_Quote_Number__c!=null) {
					component.find('lookup_latest_pos_revision1').set("v.value",returnValue.objQuote.POS_Contract_Quote_Number__c);
                } 
                /*else if(returnValue.capitalServiceQuote!=null) {
                    component.find('is_pos_contract_in_house').set("v.value",true);
                    component.find('lookup_latest_pos_revision1').set("v.value",returnValue.capitalServiceQuote);
                } 
                */
                else {
					component.find('lookup_latest_pos_revision1').set("v.value",returnValue.objQuote.Quote_Reference__c);
					if(returnValue.objQuote.Quote_Reference__c!=null && returnValue.objQuote.Quote_Reference__c!='') {
						component.find('is_pos_contract_in_house').set("v.value",true);
					} else {
						component.find('is_pos_contract_in_house').set("v.value",false);
					}
				}
			}
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    clearAllMessages: function(component, event, helper) {
        helper.showMessages(component, '', "salesMsgDiv", "salesMsgBox", false,  "", "");
        helper.showMessages(component, '', "serviceMsgDiv", "serviceMsgBox", false,  "", "");
        helper.showMessages(component, "", "messageDiv", "messageBox", false,  "", "");
    },
    openPartnerLookup : function(component, event, helper){
        
        var partnerFunctionKey = event.getSource().get("v.name");
        var partnerFunctionArray = partnerFunctionKey.split("-");
        var partnerFunction = partnerFunctionArray[0];
        var partnerAuraId = partnerFunctionArray[1];
        
        
        var partnerComponent = component.find(partnerAuraId);
        var partnerValue = partnerComponent.get("v.value");
        var soldToNumber = component.find('soldToValue').get("v.value");
        var salesOrg = helper.getPricebookName(component.find("slsOrgPcklst").get("v.value"));
        salesOrg = salesOrg.substring(0, 4);
        var partnerGlobalId = partnerComponent.getGlobalId();

        console.log('--partnerFunction'+partnerFunction);
        console.log('--soldToNumber'+soldToNumber);
        console.log('--salesOrg'+salesOrg);
        console.log('--partnerGlobalId'+partnerGlobalId);
        
    	var newwindow = window.open("/apex/Lightning_OrderPartners?partnerGlobalId="+partnerGlobalId+"&partnerFunction="+partnerFunction+"&salesOrg="+salesOrg+"&soldToNumber="+soldToNumber+"&srcText="+partnerValue,"new",'height = 590,width = 1000,left = 420,top =250, menubar = false, scrollbars = yes,resizable =yes');         
        
		//if (window.focus) {newwindow.focus()}
	},
    
    showPartnerModal: function(component, event, helper) {
		//Toggle CSS styles for opening Modal
		
        var partnerFunctionKey = event.getSource().get("v.name");
        var partnerFunctionArray = partnerFunctionKey.split("-");
        var partnerFunction = partnerFunctionArray[0];
        var partnerAuraId = partnerFunctionArray[1];
        
        
        var partnerComponent = component.find(partnerAuraId);
        var partnerValue = partnerComponent.get("v.value");
        var soldToNumber = component.find('soldToValue').get("v.value");
        var salesOrgComponent = component.find("slsOrgPcklst");
        var salesOrg = helper.getPricebookName(salesOrgComponent.get("v.value"));
        console.log('----partnerFunction'+partnerFunction+'--partnerValue'+partnerValue+'--salesOrg'+salesOrg);
        if(salesOrg){
        	salesOrg = salesOrg.substring(0, 4);
            salesOrgComponent.set("v.errors", null);
        }else{
            salesOrgComponent.set("v.errors", [{message:"Please select sales org."}]);
            return;
        }
        var partnerGlobalId = partnerComponent.getGlobalId();
        
        component.set("v.selectedPartnerFunction", partnerFunction);
        component.set("v.searchText", partnerValue);
        component.set("v.selectedSalesOrgLabel", salesOrg);
        console.log('----partnerFunction'+partnerFunction+'--partnerValue'+partnerValue+'--salesOrg'+salesOrg);
		helper.toggleClass(component,true,"partnerPopUp");
	},
	
    showContactModal: function(component, event, helper) {
		//Toggle CSS styles for opening Modal
		
        var recordLoopupKey = event.getSource().get("v.name");
        var recordLookupArray = recordLoopupKey.split("-");
        console.log("----lookuparray"+JSON.stringify(recordLookupArray));
        var objectLabelValue = recordLookupArray[0];
        var objectAPIName = recordLookupArray[1];
        var recordIdField = recordLookupArray[2];
        var recordNameField = recordLookupArray[3];
		var objFieldNames =  []; 
        var objFieldLabels =  [];  
        var defaultObjField = "";
        var defaultObjFieldValue = "";
        
        if(objectAPIName == 'Contact'){
            objFieldLabels = ['Name','Account Name'];
            objFieldNames = ['Name','Account.Name'];
            defaultObjField = "AccountId";
            defaultObjFieldValue = component.get("v.bmiQuote.BigMachines__Account__c");
            console.log("----in contact"+defaultObjField+"---"+defaultObjFieldValue);
        }else if(objectAPIName == "SVMXC__Site__c"){
            objectLabelValue = "Functional Location";
            objFieldLabels = ['Location Name', 'ERP Functional Location', 'ERP site partner ID', 'Street', 'City', 'State',
                              'Zip', 'Country'];
            objFieldNames = ['Name', 'ERP_Functional_Location__c', 'ERP_Site_Partner_Code__c',
                             'SVMXC__Street__c', 'SVMXC__City__c', 'SVMXC__State__c', 'SVMXC__Zip__c', 'SVMXC__Country__c'];
            defaultObjField = "ERP_Site_Partner_Code__c";
            defaultObjFieldValue = component.get("v.prepareOrderController.sitePartnerNumber");
            console.log("----in location"+defaultObjField+"---"+defaultObjFieldValue);
        }
        
        var recordNameComponent = component.find(recordNameField);
        var recordNameValue = recordNameComponent.get("v.value");
        
        console.log('----recordNameValue'+recordNameValue+'--recordIdField'+recordIdField+'--recordNameField'+recordNameField);
        console.log('----objectName'+objectAPIName+'--objectLabel'+objectLabelValue);
        
        component.set("v.selectedLookupIdField", recordIdField);
        component.set("v.selectedLookupNameField", recordNameField);
        component.set("v.selectedLookupSearchKey", recordNameValue);
        component.set("v.lookupDefaultFieldName", defaultObjField);
        component.set("v.lookupDefaultFieldValue", defaultObjFieldValue);
        component.set("v.lookupFieldNames", objFieldNames);
        component.set("v.lookupFieldLabels", objFieldLabels);
		component.set("v.lookupObjectLabel", objectLabelValue);
        component.set("v.lookupObjectName", objectAPIName);
		helper.toggleClass(component,true,"customLookupPopUp");
	},
    
    //Show service org list in case of combined quote
    showServiceOrgs : function(component, event, helper){
        var action = component.get("c.updateSalesServiceOrgList");
        var prepareOrderObject = component.get("v.prepareOrderController");
        var bigmachinesQuote = component.get("v.bmiQuote");
        prepareOrderObject.objQuote = bigmachinesQuote;
        
        action.setParams({
            "prepareOrderObject" : JSON.stringify(prepareOrderObject)
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                component.set("v.prepareOrderController.salesOrgs", returnValue);
                helper.getSalesOrgsValues(returnValue);
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
	hidePartnerModal : function(component, event, helper) {
        console.log('----in hideModel');
		//Toggle CSS styles for hiding Modal
		helper.toggleClass(component,false, "partnerPopUp");
	},
    
    hideContactModal : function(component, event, helper) {
        console.log('----in hideModel');
		//Toggle CSS styles for hiding Modal
		helper.toggleClass(component,false, "customLookupPopUp");
	},
    
    setSelectedPartner : function(component, event, helper){
    	var partnerNumber = event.getParam("partnerNumber");
        var partnerFunction = event.getParam("partnerFunction");
        console.log('--in setSelectedPartner'+partnerNumber+'--'+partnerFunction);
        if(partnerFunction == 'SP'){
            component.set("v.prepareOrderController.soldToNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'SP=Sold-to party', partnerNumber, 'v.prepareOrderController.soldToParty');
        }else if(partnerFunction == 'Z1'){
            component.set("v.prepareOrderController.sitePartnerNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'Z1=Site Partner', partnerNumber, 'v.prepareOrderController.sitePartner');
        }else if(partnerFunction == 'PY'){
            component.set("v.prepareOrderController.payerNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'PY=Payer', partnerNumber, 'v.prepareOrderController.payer');
        }else if(partnerFunction == 'EU'){
            component.set("v.prepareOrderController.endUserNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'EU=End User', partnerNumber, 'v.prepareOrderController.endUser');
        }else if(partnerFunction == 'BP'){
            component.set("v.prepareOrderController.billToNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'BP=Bill-to Party', partnerNumber, 'v.prepareOrderController.billToParty');
        }else if(partnerFunction == 'SH'){
            component.set("v.prepareOrderController.shipToNumber", partnerNumber);
            helper.updatePartnerDetails(component, 'SH=Ship-to party', partnerNumber, 'v.prepareOrderController.shipToParty');
        }
        helper.toggleClass(component, false, "partnerPopUp");
	},
    
    setSelectedLookupRecord : function(component, event, helper){
        var sfId = event.getParam("recordId");
        var sfName = event.getParam("recordName");
        var nameField = event.getParam("recordNameField");
        var idField = event.getParam("recordIdField");
        if(nameField != "functionalLocationName"){
            component.find(nameField).set("v.value",sfName);
            component.find(idField).set("v.value",sfId);
        }else{
            helper.getFunctionalLocationName(component, sfId, nameField);
        }
        console.log('-----sfId'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        helper.toggleClass(component, false, "customLookupPopUp");
    },
    
    //Set select order reason method into attribute
    setOrderReasonMethod : function(component, event){    	
        var selectedOrderReason = event.getSource().get("v.text");
        component.set("v.selectedOrderReasonMethod" , selectedOrderReason);    
        console.log('-----setOrderReasonMethod'+selectedOrderReason);
	},
    
    applyOrderReason : function(component){
        var setOneOrderReasonForAll = component.get("v.selectedOrderReasonMethod");
        if(setOneOrderReasonForAll == 'true'){
            
            var selectedOrderReason = component.find("orderReason").get("v.value");
            var quoteProductList = component.get("v.prepareOrderController.quoteProductWrapperList");
            for(var counter in quoteProductList){
                console.log('----applyOrderReason'+counter);
                quoteProductList[counter].orderReason = selectedOrderReason;
            }
            component.set("v.prepareOrderController.quoteProductWrapperList" , quoteProductList);
        }
	},

    //Saves user selections and entries but wont submit to SAP
    saveOrder : function(component, event, helper){
        var clearMsg = component.get('c.clearAllMessages');$A.enqueueAction(clearMsg);
        helper.toggleClass(component,true,"orderSpinner");
        var prepareOrderObject = component.get("v.prepareOrderController");
        var bigmachinesQuote = component.get("v.bmiQuote");
        
        //INC4935277 : Puneet, Limiting character length of this field ( to 35) to comply with the mapping to SAP Purchase Order field
        var prepareOrderLength = bigmachinesQuote.Purchase_Order_Number__c ;
        var errorMessages = '';
        if(prepareOrderLength != null && prepareOrderLength.length > 35) {
            helper.addErrorBorder(component, 'purchaseOrderNumber');
            errorMessages += 'Purchase Order Number cannot exceed 35 characters.<br/>';
            console.log(' #### error #### ');
            helper.showMessages(component, errorMessages, "errorButtonIcon", "errorMessageBox", true, "slds-theme--error", "slds-theme--success");
            helper.toggleClass(component,false,"orderSpinner");
            return;
        }
        
        helper.deleteContactReferences(bigmachinesQuote);
        prepareOrderObject.objQuote = bigmachinesQuote;
        
        //delete duplicate references
        delete prepareOrderObject.payer;

		/* Start - POS details Apended */
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
			prepareOrderObject.posQuote.isPosContract = component.find('is_pos_contract').get("v.value");
			prepareOrderObject.posQuote.posCapitalQuoteNumber = component.find('lookup_latest_pos_revision').get("v.value");
			prepareOrderObject.posQuote.verifyPosContract = component.find('verify_pos_checkbox').get("v.value");	
		} else if(prepareOrderObject.objQuote.Order_Type__c!='Services') {
			prepareOrderObject.posQuote.isPosInHouse = component.find('is_pos_contract_in_house').get("v.value");
			prepareOrderObject.posQuote.posContractQuoteNumber = component.find('lookup_latest_pos_revision1').get("v.value");
		}
		/* End - POS details Apended */

        var prepareOrderInstance = JSON.stringify(prepareOrderObject);
        console.log('-----saveOrder--'+prepareOrderInstance);

        var action = component.get("c.save");
        action.setParams({
            "prepareOrderObject" : prepareOrderInstance
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('-----save state'+state);
            if (state === "SUCCESS") {
				var returnValue = response.getReturnValue();
				if(returnValue.isPosError) {
					if(returnValue.objQuote.Order_Type__c=='Services') {
						// Set POS service both checbox to false
						component.find('is_pos_contract').set("v.value",false);
						component.find('verify_pos_checkbox').set("v.value",false);

						// Make blank POS service related details
						component.set('v.prepareOrderController.posQuote.preBookDate', '');
						component.set('v.prepareOrderController.posQuote.preBookNumber', '');
						component.set('v.prepareOrderController.posQuote.bookDate', '');
						component.set('v.prepareOrderController.posQuote.soNumber', '');

						// Show service error message
						helper.showPOSErrorMsg(component);
						//helper.showMessages(component, 'Capital Sales Quote does not exist', "serviceMsgDiv", "serviceMsgBox", true,  "slds-theme--error", "slds-theme--success");
					} else {
						// Show sales error message
						helper.showPOSErrorMsg(component);
						//helper.showMessages(component, 'Contract Sales Quote does not exist', "salesMsgDiv", "salesMsgBox", true,  "slds-theme--error", "slds-theme--success");
					}
				} else {
					helper.showMessages(component, "Order Saved Successfully!", "messageDiv", "messageBox", true,  "slds-theme--success", "slds-theme--error");
					helper.showMessages(component, "", "errorButtonIcon", "errorMessageBox", false, "", "");				
				}
            }else if(state === "ERROR"){
                console.log('-----in ERROR'+state);
                var errors = response.getError();
                console.log('-----in ERROR Message'+JSON.stringify(errors));
                if (errors) {
                    if(errors[0] && errors[0].message){
                        helper.showMessages(component, errors[0].message, "messageDiv", "messageBox", true,  "slds-theme--error", "slds-theme--success");
                    }else if (errors[0].pageErrors[0] && errors[0].pageErrors[0].message) {
                        helper.showMessages(component, errors[0].pageErrors[0].message, "messageDiv", "messageBox", true,  "slds-theme--error", "slds-theme--success");
                    }else{
                        helper.showMessages(component, "Error occured while saving order. Please contact system administrator.", "messageDiv", "messageBox", true,  
                                        "slds-theme--error", "slds-theme--success");
                    }
                } else {
                    console.log('-----errors else');
                    helper.showMessages(component, "Error occured while saving order. Please contact system administrator.", "messageDiv", "messageBox", true,  
                                        "slds-theme--error", "slds-theme--success");
                }
            }
            helper.toggleClass(component,false,"orderSpinner");
        });
        
        // Invoke the service
        $A.enqueueAction(action);
    },
    
    //Validate eorder and show confirm box
    validateAndConfirm : function(component, event, helper){
        var prepareOrderObject = component.get("v.prepareOrderController");
        var bigmachinesQuote = component.get("v.bmiQuote");
        //INC4935277 : Puneet, Limiting character length of this field ( to 35) to comply with the mapping to SAP Purchase Order field
        var prepareOrderLength = bigmachinesQuote.Purchase_Order_Number__c ;
        var errorMessages = '';
        if(prepareOrderLength != null && prepareOrderLength.length > 35) {
            helper.addErrorBorder(component, 'purchaseOrderNumber');
            errorMessages += 'Purchase Order Number cannot exceed 35 characters.<br/>';
            console.log(' #### error #### ');
            helper.showMessages(component, errorMessages, "errorButtonIcon", "errorMessageBox", true, "slds-theme--error", "slds-theme--success");
            helper.toggleClass(component,false,"orderSpinner");
            return;
        }
        
        prepareOrderObject.objQuote = bigmachinesQuote;

        var errorMessages = helper.validateOrder(component, prepareOrderObject);
        
        if(errorMessages != ''){
            helper.showMessages(component, errorMessages, "errorButtonIcon", "errorMessageBox", true, "", "");
            helper.toggleClass(component,false,"orderSpinner");
        }else{
            helper.toggleClass(component,true,"confirmBox");
        }
    },
    
    //Submits order to SAP after validation is successful 
    submitOrderToSAP : function(component, event, helper){
        var clearMsg = component.get('c.clearAllMessages');$A.enqueueAction(clearMsg);
        var bigmachinesQuote = component.get("v.bmiQuote");
        //INC4935277 : Puneet, Limiting character length of this field ( to 35) to comply with the mapping to SAP Purchase Order field
        var prepareOrderLength = bigmachinesQuote.Purchase_Order_Number__c ;
        var errorMessages = '';
        if(prepareOrderLength != null && prepareOrderLength.length > 35) {
            helper.addErrorBorder(component, 'purchaseOrderNumber');
            errorMessages += 'Purchase Order Number cannot exceed 35 characters.<br/>';
            console.log(' #### error #### ');
            helper.showMessages(component, errorMessages, "errorButtonIcon", "errorMessageBox", true, "slds-theme--error", "slds-theme--success");
            helper.toggleClass(component,false,"orderSpinner");
            return;
        }
        
        helper.toggleClass(component,false,"confirmBox");
        helper.toggleClass(component,true,"orderSpinner");
        var prepareOrderObject = component.get("v.prepareOrderController");
        
        prepareOrderObject.objQuote = bigmachinesQuote;
        
        console.log("----submitOrderToSAP");

		/* Start - POS details Apended */
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
			prepareOrderObject.posQuote.isPosContract = component.find('is_pos_contract').get("v.value");
			prepareOrderObject.posQuote.posCapitalQuoteNumber = component.find('lookup_latest_pos_revision').get("v.value");
			prepareOrderObject.posQuote.verifyPosContract = component.find('verify_pos_checkbox').get("v.value");	
		} else if(prepareOrderObject.objQuote.Order_Type__c!='Services') {
			prepareOrderObject.posQuote.isPosInHouse = component.find('is_pos_contract_in_house').get("v.value");
			prepareOrderObject.posQuote.posContractQuoteNumber = component.find('lookup_latest_pos_revision1').get("v.value");
		}
		/* End - POS details Apended */
        
        //delete duplicate references
        delete prepareOrderObject.payer;        
        var prepareOrderInstance = JSON.stringify(prepareOrderObject);
        var action = component.get("c.submitOrder");
        action.setParams({
            "prepareOrderObject" : prepareOrderInstance
        });
        helper.showMessages(component, "", "errorButtonIcon", "errorMessageBox", false, "", "");
        helper.showMessages(component, "", "errorMessagesDiv", "errorMessageBox", false, "", "");
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('-----save state'+state);
            if (state === "SUCCESS") {
				var errorMessages = response.getReturnValue();
				if(errorMessages=="PosError") {
					// POS details not valid
					if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
						// Set POS service both checbox to false
						component.find('is_pos_contract').set("v.value",false);
						component.find('verify_pos_checkbox').set("v.value",false);
						
						// Make blank POS service related details
						component.set('v.prepareOrderController.posQuote.preBookDate', '');
						component.set('v.prepareOrderController.posQuote.preBookNumber', '');
						component.set('v.prepareOrderController.posQuote.bookDate', '');
						component.set('v.prepareOrderController.posQuote.soNumber', '');
						// Show service error Message
						helper.showPOSErrorMsg(component);
						//helper.showMessages(component, 'Capital Sales Quote does not exist', "serviceMsgDiv", "serviceMsgBox", true,  "slds-theme--error", "slds-theme--success");
					} else {
						// Show sales error Message
						helper.showPOSErrorMsg(component);
						//helper.showMessages(component, 'Contract Sales Quote does not exist', "salesMsgDiv", "salesMsgBox", true,  "slds-theme--error", "slds-theme--success");
					}
				} else {
					var navEvt = $A.get("e.force:navigateToSObject");
					var quoteRecordId = component.get("v.bmiQuote.Id");
				
					console.log("-----errorMessages"+errorMessages);
					if(errorMessages!=""){
						helper.showMessages(component, errorMessages, "errorButtonIcon", "errorMessageBox", true, "", "");
						helper.toggleClass(component,false,"orderSpinner");
					}else{
						helper.showMessages(component, "Order Submitted Successfully!", "messageDiv", "messageBox", true,  "slds-theme--success", "slds-theme--error");
						if(typeof sforce !== 'undefined') {
							sforce.one.navigateToSObject(quoteRecordId,"DETAIL");
						}    
					}
				}				
            }else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {                                                
                    if(errors[0] && errors[0].message){
                        helper.showMessages(component, errors[0].message, "messageDiv", "messageBox", true,  "slds-theme--error", "slds-theme--success");
                    }else if (errors[0].pageErrors[0] && errors[0].pageErrors[0].message) {
                        helper.showMessages(component, errors[0].pageErrors[0].message, "messageDiv", "messageBox", true,  "slds-theme--error", "slds-theme--success");
                    }else{
                        helper.showMessages(component, "Error occured while saving order. Please contact system administrator.", "messageDiv", "messageBox", true,  
                                            "slds-theme--error", "slds-theme--success");
                    }
                } else {
                    helper.showMessages(component, "Error occured while saving order. Please contact system administrator.", "messageDiv", "messageBox", true,  
                                        "slds-theme--error", "slds-theme--success");
                }
            }
            helper.toggleClass(component,false,"orderSpinner");
        });
        // Invoke the service
        $A.enqueueAction(action);
        
    },
    
    clearErrorBorder : function(component, event){
        var cmpTarget = event.getSource();
        $A.util.removeClass(cmpTarget, 'errorBorder');
    },
    
    showErrorPopOver : function(component, event, helper){
        helper.showMessages(component, document.getElementById("errorMessageBox").innerHTML, "errorMessagesDiv", "errorMessageBox", true, "", "");
    },
    
    hideErrorPopOver : function(component, event, helper){
        helper.showMessages(component, document.getElementById("errorMessageBox").innerHTML, "errorMessagesDiv", "errorMessageBox", false, "", "");
    },
    
    closeConfirmBox : function(component, event, helper){
        helper.toggleClass(component,false,"confirmBox");
    },
    
    cancelBtn: function(component, event, helper) {
        var recId = component.get("v.recordId");
		sforce.one.navigateToSObject(recId,"DETAIL");           
    },
    
    epotBtn: function(component, event, helper) {
      	/*var action = component.get("c.checkEpotAction");
        action.setParams({"quoteId":component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isValid",response.getReturnValue());
                var isvalidObj = response.getReturnValue();
                if(isvalidObj == true){
                	window.open('/apex/SR_PrepareOrderPdf?Id='+component.get("v.recordId"));
                }else{
                    alert('You cannot generate the Epot document as this quote(Order) is not submitted to SAP');
                }
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);*/
        window.open('/apex/SR_PrepareOrderPdf?Id='+component.get("v.recordId"));
    },
    openERPCustomerRequestForm : function(component, event, helper) {
        
        console.log('-----openERPCustomerRequestForm');
        //helper.toggleClass(component,true,"orderSpinner");
        //var recId = component.get("v.recordId");
		//sforce.one.navigateToSObject(recId,"DETAIL");        
        var billTo = component.get("v.prepareOrderController.billToNumber");
        var payer = component.get("v.prepareOrderController.payerNumber");
        var soldTo = component.get("v.prepareOrderController.soldToNumber");
        var shipTo = component.get("v.prepareOrderController.shipToNumber");
        var sitePartner = component.get("v.prepareOrderController.sitePartnerNumber");
        var salesOrg = helper.getPricebookName(component.find("slsOrgPcklst").get("v.value"));
        
        sforce.one.navigateToURL("/apex/Lightning_ERPCustomerRequestForm?quoteId="+component.get("v.recordId")+"&salesOrg="+salesOrg+"&BP="+billTo+"&PY="+payer+"&SP="+soldTo+"&SH="+shipTo+"&Z1="+sitePartner);  
        
		/*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/Lightning_ERPCustomerRequestForm?quoteId="+component.get("v.recordId")+"&salesOrg="+salesOrg+"&BP="+billTo+"&PY="+payer+"&SP="+soldTo+"&SH="+shipTo+"&Z1="+sitePartner
        });
        urlEvent.fire();*/
	},
    /**********************************************************************************************************
     * Start - Below is the POS Section methods
     **********************************************************************************************************/
    /**
     * Handler for receiving the updateLookupIdEvent event
     */
    handleQuoteIdUpdate : function(cmp, event, helper) {
        // Get the Id from the Event
        var accountId = event.getParam("sObjectId");

        // Get the Instance Id from the Event
        var instanceId = event.getParam("instanceId");

        // Determine the instance Id of the component that fired the event
        if (instanceId == "MyQuote")
        {
            // Set the Id bound to the View
            cmp.set('v.recordId', accountId);
        }
        else
        {
            console.log('Unknown instance id: ' + instanceId);
        }
    },

    /**
     * Handler for receiving the clearLookupIdEvent event
     */
	handleQuoteIdClear : function(cmp, event, helper) {
        // Get the Instance Id from the Event
        var instanceId = event.getParam("instanceId");

        // Determine the instance Id of the component that fired the event
        if (instanceId == "MyQuote")
        {
            // Clear the Id bound to the View
            cmp.set('v.recordId', null);
        }
        else
        {
            console.log('Unknown instance id: ' + instanceId);
        }
	},
    /**
     * Search an SObject for a match
     */
	search : function(cmp, event, helper) {
		helper.doSearch(cmp);        
    },

    /**
     * Select an SObject from a list
     */
    select: function(cmp, event, helper) {
    	helper.handleSelection(cmp, event);
    },
    
    /**
     * Clear the currently selected SObject
     */
    clear: function(cmp, event, helper) {
    	helper.clearSelection(cmp);    
    },
	// TODO - Has to be removed onPosCheck method
	onPosCheck : function(component, event, helper){
		var value = component.find('is_pos_contract').get("v.value");
		if(value==true) {
			component.find('is_pos_contract').set("v.value",true);
			component.find('verify_pos_checkbox').set("v.value",true);
		} else {
			component.find('is_pos_contract').set("v.value",false);
			component.find('verify_pos_checkbox').set("v.value",false);
		}
        return false;
	},
    onSalesPosCheckbox : function(component, event, helper) {
		var value = component.find('is_pos_contract_in_house').get("v.value");
        if(value==false) {
            component.find('lookup_latest_pos_revision1').set("v.value",'');
        }
    },
	getLatestRevisionForService: function(component, event, helper){
		var clearMsg = component.get('c.clearAllMessages');$A.enqueueAction(clearMsg);
		var prepareOrderObject = component.get("v.prepareOrderController");
		var capital_sales_quote = component.find('lookup_latest_pos_revision').get("v.value");

        var action = component.get("c.getLastesRevisionOfQuoteByName");
        var bigmachinesQuote = component.get("v.bmiQuote");
        prepareOrderObject.objQuote = bigmachinesQuote;
        action.setParams({
            "prepareOrderObject" : JSON.stringify(prepareOrderObject),
			"bmQuoteName" : capital_sales_quote
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
				if(returnValue!=null) {
					var obj = JSON.parse(returnValue);
					//component.set("v.prepareOrderController", obj);
					component.find('lookup_latest_pos_revision').set('v.value', obj.posQuote.name);

					component.set('v.prepareOrderController.posQuote.preBookDate', obj.posQuote.preBookDate);
					component.set('v.prepareOrderController.posQuote.preBookNumber', obj.posQuote.preBookNumber);
					component.set('v.prepareOrderController.posQuote.bookDate', obj.posQuote.bookDate);
					component.set('v.prepareOrderController.posQuote.soNumber', obj.posQuote.soNumber);
					
					component.find('is_pos_contract').set("v.value",true);
					component.find('verify_pos_checkbox').set("v.value",true);

                    // Show message 
                    var latestQuoteMessageId = component.find("latestQuoteIdMessage");
                    if(capital_sales_quote != obj.posQuote.name) {
                    	$A.util.removeClass(latestQuoteMessageId,'slds-hide');
                    }
					/*window.setTimeout(
        				$A.getCallback(function() {
							$A.util.addClass(latestQuoteMessageId,'slds-hide');
        				}), 60000
    				);*/
				} else {
					component.find('is_pos_contract').set("v.value",false);
					component.find('verify_pos_checkbox').set("v.value",false);
					helper.showPOSErrorMsg(component);
					//helper.showMessages(component, 'Capital Sales Quote does not exist', "serviceMsgDiv", "serviceMsgBox", true,  "slds-theme--error", "slds-theme--success");

					component.set('v.prepareOrderController.posQuote.preBookDate', '');
					component.set('v.prepareOrderController.posQuote.preBookNumber', '');
					component.set('v.prepareOrderController.posQuote.bookDate', '');
					component.set('v.prepareOrderController.posQuote.soNumber', '');
				}
            }
            return false;
        });
        // Invoke the service
        $A.enqueueAction(action);
		
	},
	getLatestRevisionForSales: function(component, event, helper){
        var clearMsg = component.get('c.clearAllMessages');$A.enqueueAction(clearMsg);
		var prepareOrderObject = component.get("v.prepareOrderController");
		var contract_sales_quote = component.find('lookup_latest_pos_revision1').get("v.value");
		
        var action = component.get("c.getLastesRevisionOfQuoteByName");
        var bigmachinesQuote = component.get("v.bmiQuote");
        prepareOrderObject.objQuote = bigmachinesQuote;
        
        action.setParams({
            "prepareOrderObject" : JSON.stringify(prepareOrderObject),
			"bmQuoteName" : contract_sales_quote
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
				if(returnValue!=null) {
					var obj = JSON.parse(returnValue);
					//component.set("v.prepareOrderController", obj);
					component.find('lookup_latest_pos_revision1').set("v.value",obj.posQuote.name);
					component.find('is_pos_contract_in_house').set("v.value",true);
				} else {
					component.find('lookup_latest_pos_revision1').set("v.value",'');
					component.find('is_pos_contract_in_house').set("v.value",false);
                    helper.showPOSErrorMsg(component);
					//helper.showMessages(component, 'Contract Sales Quote does not exist', "salesMsgDiv", "salesMsgBox", true,  "slds-theme--error", "slds-theme--success");
				}
            }
            return false;
        });
        // Invoke the service
        $A.enqueueAction(action);
	},
    hideLookup: function(component,event, helper) {
        // Dont Remove below SetTimeout, As it is useful to focus out input text to disappear the lookup
        // And, Dont affect to selection method.
		setTimeout(function(){
            var lookupList = component.find('lookuplist');
            var lookupList1 = component.find('lookuplist1');
			$A.util.addClass(lookupList, 'slds-hide');
            $A.util.addClass(lookupList1, 'slds-hide');
		}, 100);
    }
	/**********************************************************************************************************
     * End - Below is the POS Section methods
     **********************************************************************************************************/
})
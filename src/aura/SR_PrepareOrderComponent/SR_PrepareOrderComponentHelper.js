({
	checkPartners : function(prepareOrderObj){
        if(!prepareOrderObj.shipToNumber){
			prepareOrderObj.shipToNumber = '';
		}
        if(!prepareOrderObj.soldToNumber){
			prepareOrderObj.soldToNumber = '';
		}
        if(!prepareOrderObj.payerNumber){
			prepareOrderObj.payerNumber = '';
		}
        if(!prepareOrderObj.sitePartnerNumber){
			prepareOrderObj.sitePartnerNumber = '';
		}
        if(!prepareOrderObj.billToNumber){
			prepareOrderObj.billToNumber = '';
		}
        if(!prepareOrderObj.endUserNumber){
			prepareOrderObj.endUserNumber = '';
		}
	},
    salesOrgOptionsMap: {},
    getSalesOrgsValues: function(salesOrgValues) {
        var self = this;
		console.log('---getSalesOrgsValues');
        for (var index in salesOrgValues) {
            self.salesOrgOptionsMap[salesOrgValues[index].value] = salesOrgValues[index].label;
        	
            console.log('---in for'+salesOrgValues[index].value);
        }
    },
    getPricebookName: function(id) {
        return this.salesOrgOptionsMap[id];
    },
    
    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
		var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
	},
    
    updatePartnerDetails : function(component, partnerFunction, partnerNumber, componentName){
        console.log('---fetchPartnerDetails'+component.get("v.prepareOrderController.selectedSalesOrg"));
        console.log('---fetchPartnerDetails'+partnerFunction);
        console.log('---fetchPartnerDetails'+component.get("v.bmiQuote.Order_Type__c"));
        console.log('---fetchPartnerDetails'+partnerNumber);
        var action = component.get("c.getPartnerDetails");
        action.setParams({
            "partnerFunction" : partnerFunction,
            "partnerNumber" : partnerNumber,
            "orderType" : component.get("v.bmiQuote.Order_Type__c"),
            "salesOrgId" : component.get("v.prepareOrderController.selectedSalesOrg")
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
                var partnerObject = JSON.parse(JSON.stringify(returnValue));
                component.set(componentName, partnerObject);
            }
        });
        // Invoke the service
        $A.enqueueAction(action);
    },
    
    //Required Fields Validation
    validateOrder : function(component, prepareOrderObj){
    	var errors = '';
        
        if(prepareOrderObj.billToNumber == ''){
            this.addErrorBorder(component, 'billToValue');
            errors += 'Please select Bill To Party.<br/>';
        }
        if(prepareOrderObj.shipToNumber == ''){
            this.addErrorBorder(component, 'shipToValue');
            errors += 'Please select Ship To Party.<br/>';
        }
        if(prepareOrderObj.payerNumber == ''){
            this.addErrorBorder(component, 'payerValue');
            errors += 'Please select Payer.\n<br/>';
        }
        if(prepareOrderObj.soldToNumber == ''){
            this.addErrorBorder(component, 'soldToValue');
            errors += 'Please select Sold To Party.<br/>';
        }
        if((prepareOrderObj.sitePartnerNumber == '' && prepareOrderObj.endUserNumber == '') || 
        (prepareOrderObj.sitePartnerNumber != '' && prepareOrderObj.endUserNumber != '')){
            this.addErrorBorder(component, 'sitePartnerValue');
            this.addErrorBorder(component, 'endUserValue');
            errors += 'Please select either Site Partner or End User but not both.<br/>';
        }
        if(prepareOrderObj.objQuote.Purchase_Order_Number__c == '' || !prepareOrderObj.objQuote.Purchase_Order_Number__c){
            this.addErrorBorder(component, 'purchaseOrderNumber');
            errors += 'Please enter Purchase Order Number.<br/>';
        }
        if(prepareOrderObj.objQuote.Projected_Ship_Date__c == '' || !prepareOrderObj.objQuote.Projected_Ship_Date__c){
            this.addErrorBorder(component, 'projectedShipDate');
            errors += 'Please select Projected Ship Date.<br/>';
        }
        if(prepareOrderObj.objQuote.Purchase_Order_Date__c == '' || !prepareOrderObj.objQuote.Purchase_Order_Date__c){
            this.addErrorBorder(component, 'purchaseOrderDate');
            errors += 'Please select Purchase Order Date.<br/>';
        }
        if(prepareOrderObj.objQuote.Requested_Delivery_Date__c == '' || !prepareOrderObj.objQuote.Requested_Delivery_Date__c){
            this.addErrorBorder(component, 'requestedDeliveryDate');
            errors += 'Please select Requested Delivery Date.<br/>';
        }
        if(prepareOrderObj.objQuote.Order_Type__c == 'Sales' && 
           (prepareOrderObj.selectedProjectManager == '' || !prepareOrderObj.selectedProjectManager)){
            this.addErrorBorder(component, 'projectManagerService');
            errors += 'Please select Site Project Manager.<br/>';
        }
        if(prepareOrderObj.selectedContractAdmin == '' || !prepareOrderObj.selectedContractAdmin){
            this.addErrorBorder(component, 'contractAdmin');
            errors += 'Please select Contract Administrator.<br/>';
        }
        console.log('----prepareOrderObj.salesOperationSpecialists'+prepareOrderObj.salesOperationSpecialists);
        if(prepareOrderObj.selectedOpSpecialist == ''  || !prepareOrderObj.selectedOpSpecialist){
            this.addErrorBorder(component, 'soSpecialist');
            errors += 'Please select Sales Operations Specialist.<br/>';
        }
        console.log('----prepareOrderObj.selectedSalesManager'+prepareOrderObj.selectedSalesManager);
        if(prepareOrderObj.selectedSalesManager == ''  || !prepareOrderObj.selectedSalesManager){
            this.addErrorBorder(component, 'salesManager');
            errors += 'Please select Sales/Services Manager.<br/>';
        }
        if(prepareOrderObj.objQuote.Order_Type__c != 'Services' && 
        (prepareOrderObj.objQuote.Distribution_Channel__c == '' || !prepareOrderObj.objQuote.Distribution_Channel__c)){
            this.addErrorBorder(component, 'salesDistributionChannel');
            errors += 'Please select Distribution Channel.<br/>';	
        }
        if(prepareOrderObj.objQuote.Order_Type__c != 'Sales' && 
        (prepareOrderObj.objQuote.Service_Distribution_list__c == '' || !prepareOrderObj.objQuote.Service_Distribution_list__c)){
            this.addErrorBorder(component, 'serviceDistributionChannel');
            errors += 'Please select Service Distribution Channel.<br/>';
        }
        console.log('----errors'+errors);
        return errors;
	},
    
    getErrorObject : function(errorString){
        var errorMsg = new Object();
        errorMsg.message = errorString;
        return errorMsg;
    },
    
    showMessages : function(component, message, messageAuraId, messageHTMLId, showMessage, addClass, removeClass){
        var errorComponent = component.find(messageAuraId);
        console.log("---messageAuraId"+messageAuraId);
        console.log("---messageHTMLId"+messageHTMLId);
        console.log("---message"+message);
        console.log("---showMessage"+showMessage);
        if(showMessage){
            $A.util.removeClass(errorComponent,'slds-hide');
            $A.util.addClass(errorComponent,'slds-show');
            $A.util.removeClass(errorComponent,removeClass);
            $A.util.addClass(errorComponent,addClass);
            
            document.getElementById(messageHTMLId).innerHTML = message;
            console.log('-----innerhtml'+document.getElementById(messageHTMLId).innerHTML);
        }else{
            $A.util.removeClass(errorComponent,'slds-show');
            $A.util.addClass(errorComponent,'slds-hide');
        }
    },
    
    toggleSpinner: function(component) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        
        if(!$A.util.hasClass(spinner, 'hideEl')){
            evt.setParams({ isVisible : false });
         }		
        else {
            evt.setParams({ isVisible : true });
        }
        evt.fire();
    },
    
    setPrepareOrderVisibility : function(component, prepareOrderObj){
    	component.set("v.showPrepareOrderPage", prepareOrderObj.showPrepareOrderPage);
        var errorMsg = "";
        if(!prepareOrderObj.errorMessage){
            console.log('--prepareOrderObj.errorMessage1'+prepareOrderObj.errorMessage);
            errorMsg = "";
        }else{       
            console.log('--prepareOrderObj.errorMessage2'+prepareOrderObj.errorMessage);
            errorMsg = prepareOrderObj.errorMessage;
        }
        return errorMsg;
    },
    
    deleteContactReferences : function(bmQuote){
        delete bmQuote.Purchasing_Contact__r;
        delete bmQuote.Principal_Contact__r;
        delete bmQuote.Medical_Contact__r;
        delete bmQuote.Physics_Contact__r;
        delete bmQuote.Technical_Contact__r;
    },
    
    addErrorBorder : function(component, componentName){
        var cmpTarget = component.find(componentName);
        $A.util.addClass(cmpTarget, 'errorBorder');
    },
    
    getFunctionalLocationName : function(component, recordId, functionalLocationNameField){
        console.log("-----getFunctionalLocationName"+recordId+"----"+functionalLocationNameField);
		var functionalLocationAction = component.get("c.getFunctionLocationName");
        functionalLocationAction.setParams({
            "functionalLocationId": recordId
        });
        
        // Register the callback function
        functionalLocationAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log("----getFunctionalLocationName--state"+state);
            if (state === "SUCCESS") {
                var returnValue = response.getReturnValue();
				console.log("--returnValue--"+returnValue);
                component.find(functionalLocationNameField).set("v.value",returnValue);
            }else{
                component.find(functionalLocationNameField).set("v.value","");
            }
        });
        // Invoke the service
        $A.enqueueAction(functionalLocationAction);
    },
	
	
    /**********************************************************************************************************
     * Start - Below is the POS Section methods
     **********************************************************************************************************/
    /**
     * Perform the SObject search via an Apex Controller
     */
    doSearch : function(cmp) {
		var prepareOrderObject = cmp.get("v.prepareOrderController");
		var searchString = '';
        var lookupList;
        var inputElement;
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
			searchString = cmp.find('lookup_latest_pos_revision').get("v.value");
			cmp.set("v.searchString", searchString);
            inputElement = cmp.find('lookup_latest_pos_revision');
            lookupList = cmp.find('lookuplist');
            inputElement.set('v.errors', null);
        } else {
			searchString = cmp.find('lookup_latest_pos_revision1').get("v.value");
			cmp.set("v.searchString1", searchString);
            inputElement = cmp.find('lookup_latest_pos_revision1');
            lookupList = cmp.find('lookuplist1');
            inputElement.set('v.errors1', null);
        }

        // We need at least 7 characters for an effective search
        if (typeof searchString === 'undefined' || searchString.length < 7)
        {
            // Hide the lookuplist
            $A.util.addClass(lookupList, 'slds-hide');
            return;
        }

        // Show the lookuplist
        $A.util.removeClass(lookupList, 'slds-hide');

        // Get the API Name
        var sObjectAPIName = cmp.get('v.sObjectAPIName');

        // Create an Apex action
        var action = cmp.get('c.lookup');

        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();

        // Set the parameters
        action.setParams({ 
            "prepareOrderObject" : JSON.stringify(prepareOrderObject),
            "searchString" : searchString, 
            "sObjectAPIName" : sObjectAPIName
        });
                          
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();

            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();

                // If we have no matches, return nothing
                if (matches.length == 0)
                {
                    if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
                    	cmp.set('v.matches', null);
                    } else {
						cmp.set('v.matches1', null);
                    }
                    return;
                }
                
                // Store the results
                if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
                	cmp.set('v.matches', matches);
                } else {
                    cmp.set('v.matches1', matches);
                }
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();
                
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        this.displayToast('Error', errors[0].message);
                    }
                }
                else
                {
                    this.displayToast('Error', 'Unknown error.');
                }
            }
        });
        
        // Enqueue the action                  
        $A.enqueueAction(action);
    },

    /**
     * Handle the Selection of an Item
     */
    handleSelection : function(cmp, event) {
        // Resolve the Object Id from the events Element Id (this will be the <a> tag)
        var objectId = this.resolveId(event.currentTarget.id);

        // The Object label is the inner text)
        var objectLabel = event.currentTarget.innerText;

        // Log the Object Id and Label to the console
        console.log('objectId=' + objectId);
        console.log('objectLabel=' + objectLabel);
                
        // Create the UpdateLookupId event
        var updateEvent = cmp.getEvent("updateLookupIdEvent");
        
        // Get the Instance Id of the Component
        var instanceId = cmp.get('v.instanceId');

        // Populate the event with the selected Object Id and Instance Id
        //updateEvent.setParams({
        //    "sObjectId" : objectId, "instanceId" : instanceId
        //});

        // Fire the event
        //updateEvent.fire();

        // Update the Searchstring with the Label
		var prepareOrderObject = cmp.get("v.prepareOrderController");
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
            cmp.set("v.searchString", objectLabel);
            cmp.find('lookup_latest_pos_revision').set("v.value", objectLabel);
            //cmp.find('is_pos_contract').set("v.value",true);
            //cmp.find('verify_pos_checkbox').set("v.value",true);
            var lookupList = cmp.find("lookuplist");

        	var action = cmp.get('c.getQuoteInfo');    
			action.setParams({ "quoteId" : objectId});
            // Define the callback
            action.setCallback(this, function(response) {
                var state = response.getState();
    
                // Callback succeeded
                if (state === "SUCCESS") {
                    // Get the search matches
                    var returnValue = response.getReturnValue();
                    if(returnValue.length > 0) {
                        cmp.set('v.prepareOrderController.posQuote.preBookNumber', returnValue[0]);
                        cmp.set('v.prepareOrderController.posQuote.soNumber', returnValue[1]);
                        cmp.set('v.prepareOrderController.posQuote.bookDate', returnValue[2]);
                        cmp.set('v.prepareOrderController.posQuote.preBookDate', returnValue[3]);
                        var clearMsg = cmp.get('c.clearAllMessages');$A.enqueueAction(clearMsg);
                    }
                }
            });
        	// Enqueue the action
        	$A.enqueueAction(action);
        } else {
            cmp.set("v.searchString1", objectLabel);
            cmp.find('lookup_latest_pos_revision1').set("v.value", objectLabel);
            cmp.find('is_pos_contract_in_house').set("v.value",true);
            var lookupList = cmp.find("lookuplist1");
        }
        // Hide the Lookup List
        $A.util.addClass(lookupList, 'slds-hide');

    },

    /**
     * Clear the Selection
     */
    clearSelection : function(cmp) {
        // Create the ClearLookupId event
        var clearEvent = cmp.getEvent("clearLookupIdEvent");

        // Get the Instance Id of the Component
        var instanceId = cmp.get('v.instanceId');

        // Populate the event with the Instance Id
        clearEvent.setParams({
            "instanceId" : instanceId
        });
        
        // Fire the event
        clearEvent.fire();

        // Clear the Searchstring
		var prepareOrderObject = cmp.get("v.prepareOrderController");
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
        	cmp.set("v.searchString", '');
        } else {
            cmp.set("v.searchString1", '');
        }
    },

    /**
     * Resolve the Object Id from the Element Id by splitting the id at the _
     */
    resolveId : function(elmId)
    {
        var i = elmId.lastIndexOf('_');
        return elmId.substr(i+1);
    },

    /**
     * Display a message
     */
    displayToast : function (title, message) 
    {
        var toast = $A.get("e.force:showToast");

        // For lightning1 show the toast
        if (toast)
        {
            //fire the toast event in Salesforce1
            toast.setParams({
                "title": title,
                "message": message
            });

            toast.fire();
        }
        else // otherwise throw an alert
        {
            alert(title + ': ' + message);
        }
    },
    showPOSErrorMsg: function(cmp) {
		var prepareOrderObject = cmp.get("v.prepareOrderController");
		if(prepareOrderObject.objQuote.Order_Type__c=='Services') {
            this.showMessages(cmp, 'Capital Sales Quote does not exist. If this cannot be resolved, you may leave this blank and continue. Please notify the quote owner of the issue.', "serviceMsgDiv", "serviceMsgBox", true,  "slds-theme--error", "slds-theme--success");
        } else if(prepareOrderObject.objQuote.Order_Type__c=='Combined') {
            this.showMessages(cmp, 'Contract Sales Quote does not exist. If this cannot be resolved, you may leave this blank and continue. Please notify the quote owner of the issue.', "salesMsgDiv", "salesMsgBox", true,  "slds-theme--error", "slds-theme--success");
        } else {
            this.showMessages(cmp, 'Contract Sales Quote does not exist. If this cannot be resolved, you may leave this blank and continue. Please notify the quote owner of the issue.', "salesMsgDiv", "salesMsgBox", true,  "slds-theme--error", "slds-theme--success"); 
        }
    }
    /**********************************************************************************************************
     * End - Below is the POS Section methods
     **********************************************************************************************************/
})
({
    getSoiDeliviries : function(component) 
    {
        var action = component.get('c.getSoiVerifications'); 
        //alert('record id = ' + component.get("v.recordId"));
        var recId = component.get("v.recordId");   //a1r0n00000082lL
        action.setParams({"recId":recId});    
        action.setCallback(this, function(actionResult) 
           {
               var Results=actionResult.getReturnValue();
               component.set("v.soiDelVerifications",Results);
               
           });
        $A.enqueueAction(action);
    },

    submitConfirm : function(component) 
    {
        var action = component.get('c.submitSoiVerifications'); 
        //alert('record id = ' + component.get("v.recordId"));
        var listDelVerRecs = component.get("v.soiDelVerifications");  
        var wrkOrdId = component.get("v.recordId");
        action.setParams({"listDelVerRecs": listDelVerRecs,"wrkOrdId": wrkOrdId});    
        action.setCallback(this, function(actionResult) 
           {
               var Results=actionResult.getReturnValue();
               component.set("v.soiDelVerifications",Results);   
           });
        $A.enqueueAction(action);
    }      
})
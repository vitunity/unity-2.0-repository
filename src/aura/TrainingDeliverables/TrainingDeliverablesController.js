({
	doInit : function(component, event, helper ) {
        var accountId = component.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        var url = '/apex/SR_ViewEntitlement?id='+accountId+'&cid=ac&status=In-Progress,Backlog&ItemCategory=Z014,Z018,Z024&WT=Training';
        urlEvent.setParams({
          "url": url
        });
        urlEvent.fire();
    }
  
})
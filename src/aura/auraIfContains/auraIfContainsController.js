({
	doInit: function(component, event, helper) {
        
        var getList = component.get('v.items'); 
        var getElement = component.get('v.element');
        var getElementIndex = getList.indexOf(getElement);
        var getStatus = component.get('v.status'); 
        var getAttachment = component.get('v.attachment'); 
       
       // if getElementIndex is not equal to -1 it's means list contains this element. 
       /* if(getElementIndex != -1){ 
          component.set('v.condition',true);
        }else{
          component.set('v.condition',false);
        }*/
        // Updated by Harshal
        if(getStatus=='Approved'){
            if(getElementIndex != -1 && (getAttachment!==undefined || getAttachment!=='')){ 
              component.set('v.message','Success');
            }else{
              component.set('v.message','Failure');
            }
        }
        else{
            component.set('v.message','');
        }
    }
})
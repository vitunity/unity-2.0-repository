({
    
    //this is a init method will be invoked on component load
    doInit : function(component, event, helper) 
    {
        var action = component.get('c.initClass');
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set('v.objClassController', response.getReturnValue());
				var priceBook = response.getReturnValue().PricebookOptions;
				var myMap = new Map();
				for(var temp in priceBook)
				{
					myMap.set(priceBook[temp].Name,priceBook[temp].Id);
				}	
				component.set("v.pricebookMap",myMap);
				component.set("v.ConversionRate",response.getReturnValue().selectedConversionRate);
            }
        });
        $A.enqueueAction(action);
    },
	
	
	// this function will be invoked on product line changed 
	onProductLineChange: function(component,event, helper) 
	{
        var selectedFamily = component.find("productFamilyId").get("v.value");
		var obj = component.get('v.objClassController');
		
		if(selectedFamily === undefined || selectedFamily == null || selectedFamily =='')
		{
		    selectedFamily = obj.selectedFamily;		
		}
		else
		{
		    obj.selectedFamily = selectedFamily;	
		}
		
		var selectedLine = component.find("productLineId").get("v.value");
		
		if(selectedLine != null && obj.modelValues[selectedFamily+' '+selectedLine] != undefined )
		{
			var modelSelection = selectedFamily + ' ' + selectedLine;
			obj.modelOptions = obj.modelValues[modelSelection];
		}
		else
		{
			obj.modelOptions = [];
		}
        component.set('v.objClassController', obj);
    },

	// this function will be invoked on product family changed 
	onProductFamilyChange :  function (component,event, helper)
	{
		var selectedFamily = component.find("productFamilyId").get("v.value");
		var obj = component.get('v.objClassController');
		if( selectedFamily != undefined &&  obj.lineValues[selectedFamily] != undefined)
		{
		    obj.lineOptions = obj.lineValues[selectedFamily];	
		}	
		else
		{
		    obj.lineOptions = [];
		}
		component.set('v.objClassController', obj);
	},
	
	// this function will be invoked when user change currency selection
	onCurrencyChange : function (component,event, helper)
	{
		var selectedModel = component.find("modelId").get("v.value");
		var selectedLine = component.find("productLineId").get("v.value");
		
		var counter = 0;
		
		if( (selectedLine === undefined || selectedLine == null || selectedLine.length <= 0) || (selectedModel === undefined || selectedModel == null || selectedModel.length <= 0) )
		{
			var catalogPartSearch = component.find("searchId").get("v.value");
			if(catalogPartSearch != undefined && catalogPartSearch != null && catalogPartSearch != '')
			{
                helper.quickSearch(component,event,counter);
            }		
		}	
		else
		{
		    helper.quickSearch(component,event,counter);
		}
	},	
	


   // this function will be invoked when user change country selection
	onCountryChange : function (component,event, helper)
	{
		var selectedModel = component.find("modelId").get("v.value");
		var selectedLine = component.find("productLineId").get("v.value");
		
		var counter = 4;
		
		if( (selectedLine === undefined || selectedLine == null || selectedLine.length <= 0) || (selectedModel === undefined || selectedModel == null || selectedModel.length <= 0) )
		{
			var catalogPartSearch = component.find("searchId").get("v.value");
			if(catalogPartSearch != undefined && catalogPartSearch != null && catalogPartSearch != '')
			{
                helper.quickSearch(component,event,counter);
            }		
		}	
		else
		{
		    helper.quickSearch(component,event,counter);
		}
	},	


	// this function will be invoked when user used search catalog option
	onSearchCatalog : function(component,event,helper)
	{
	    var catalogPartSearch = component.find("searchId").get("v.value");
		var counter = 3;
		
		if( catalogPartSearch != undefined && catalogPartSearch != null && catalogPartSearch != '')
		{
		    component.set("v.groupValue",false);
			component.find("productFamilyId").set("v.value",'');
		    component.find("productLineId").set("v.value",'');
		    component.find("modelId").set("v.value",'');
			helper.quickSearch(component,event,counter);
            
			var pillTarget = component.find("clearSearch");
            $A.util.addClass(pillTarget, 'slds-show');
            $A.util.removeClass(pillTarget, 'slds-hide');			
		}
	},	
	
	// this function will be invoked when user used clear search for clearing out search catalog value
	onClearSearch : function(component,event,helper)
	{
	    var pillTarget = component.find("clearSearch");
		component.find("searchId").set("v.value",'');
		$A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
		component.set("v.groupValue",false);
        component.set("v.searchCatalog",false);
		var pillTarget = component.find("clearSearch");
	},	
	
	// this function will be invoked when user change pricebook selection
	onPriceBookChange : function(component,event, helper)
	{
		var selectedModel = component.find("modelId").get("v.value");
		var selectedLine = component.find("productLineId").get("v.value");
		var counter = 1;
		
		if(  (selectedLine === undefined || selectedLine == null || selectedLine.length <= 0) || (selectedModel === undefined || selectedModel == null || selectedModel.length <= 0)  )
		{
			var catalogPartSearch = component.find("searchId").get("v.value");
			if(catalogPartSearch != undefined && catalogPartSearch != null && catalogPartSearch != '')
			{
                helper.quickSearch(component,event,counter);
            }		
		}	
		else
		{
		    helper.quickSearch(component,event,counter);
		}
	},	
	
	// this function will be invoked on model change
	onModelChange :  function (component,event, helper)
	{
        var selectedModel = component.find("modelId").get("v.value");
		var counter = 2;
        if(!(selectedModel == null || selectedModel == ''))
        {
            var catalogPartSearch = component.find("searchId").get("v.value");
		    if(catalogPartSearch != undefined && catalogPartSearch != null &&  catalogPartSearch != '')
		    {
			    component.find("productFamilyId").set("v.value",'');
				component.find("productLineId").set("v.value",'');
				component.find("modelId").set("v.value",'');
		    }
			else
			{
				helper.quickSearch(component,event,counter);	
			}
        }
		
	},
	
	// this function will be invoked changing group selection, it can be multiple as well
	onGroupChange : function(component, event, helper) 
	{

        var obj = component.get('v.objClassController');
		var arr = new Array();
		arr.push(event.getSource().get("v.value"));
		var pushGroup = arr[0].split(";")
		obj.selectedGroup = [];
		for(var temp in pushGroup)
		{
            var sg = new Object();
            sg.selectedGroup = pushGroup[temp];
            obj.selectedGroup.push(sg);
		}
		component.set('v.objClassController', obj);
    },		
	
	// this function will be used to open product modal when user selected any particular row
	openModel: function(component, event, helper) 
	{
		var catalogPartSearch = component.find("searchId").get("v.value");
		var obj = component.get('v.objClassController');

		if(catalogPartSearch != undefined)
		{
			obj.catalogPartSearch = catalogPartSearch;
		}
		obj.selectedCatalogPartId = event.getSource().get("v.alt");
	    var action = component.get('c.selectRow');
		action.setParams({
            "catLogObj": JSON.stringify(obj)
        });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set('v.objClassController', response.getReturnValue());
				component.set("v.isOpen", true);
            }
        });
        $A.enqueueAction(action);
    },
	
	// this function will be used to close product modal
	closeModel: function(component, event, helper) 
	{
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
	},
	
	// this function will be used to open  product modal, this event will be fired from child component 
	handleComponentEvent : function(component, event) 
	{
        var prodId = event.getParam("prodId");
		var obj = component.get('v.objClassController');
		
		var groupCmp = component.find("groupId");
		if(groupCmp != undefined)
		{
			var arr = new Array();
			arr.push(groupCmp.get("v.value"));
		
			if(arr.size >0  )
			{
				var pushGroup = arr[0].split(";")
				obj.selectedGroup = [];
				for(var temp in pushGroup)
				{
		            var sg = new Object();
		            sg.selectedGroup = pushGroup[temp];
		            obj.selectedGroup.push(sg);
				}
			}	
		}	
		
		obj.selectedCatalogPartId = prodId;
	    var action = component.get('c.selectRow');
		action.setParams({
            "catLogObj": JSON.stringify(obj)
        });
        action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //set response value in objClassController attribute on component
                component.set("v.objClassController", response.getReturnValue());
				component.set("v.isOpen", true);
            }
        });
        $A.enqueueAction(action);
    },

    // this function will be used to open new tab for editing product details(product which is currently in modal)
	openEditWindow : function(component, event, helper)
	{
        window.open('/'+event.getSource().get("v.value"),'_blank');
	},

	// this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) 
    {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
	
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper)
    {
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
	
	// this function for displaying product family helptext
	display : function(component, event, helper) 
	{
		var toggleText = component.find("tooltip");
	    $A.util.addClass(toggleText, 'slds-show');
	    $A.util.removeClass(toggleText, 'slds-hide');

    },

    // this function for displaying out product family helptext
    displayOut : function(component, event, helper) 
    {
		var toggleText = component.find("tooltip");
	    $A.util.addClass(toggleText, 'slds-hide');
	    $A.util.removeClass(toggleText, 'slds-show');
    },

    // this function for displaying Search catalog helptext
    searchDisplay : function(component, event, helper) 
    {
		var toggleText = component.find("tooltipSearch");
	    $A.util.addClass(toggleText, 'slds-show');
	    $A.util.removeClass(toggleText, 'slds-hide');

    },

    // this function for displaying out Search catalog helptext
    searchDisplayOut : function(component, event, helper) 
    {
		var toggleText = component.find("tooltipSearch");
	    $A.util.addClass(toggleText, 'slds-hide');
	    $A.util.removeClass(toggleText, 'slds-show');
    },

    // this function is used for exporting currently selected group
    exportToExcel : function(component, event, helper)
    {
        var counter = 1;
        helper.excelExport(component, event,counter);
    },

    // this function is used for exporting all the group 
    exportAllToExcel : function(component, event, helper)
    {
        var counter = 2;
    	helper.excelExport(component, event,counter);
    },
  
    // this function is used for setting flag for Search by price
    onCheck: function(component, event, helper) 
    {
        var checkCmp = component.find("priceSelect");
        var obj = component.get('v.objClassController');
        obj.searchByPrice = checkCmp.get("v.value");
        component.set("v.objClassController", obj);
    },

    // this function is used for setting flag for Discountable only
    onDiscountCheck : function(component, event, helper)
    {
        var checkCmp = component.find("discountCheck");
        var obj = component.get('v.objClassController');
        obj.showDiscountable  = checkCmp.get("v.value");
        component.set("v.objClassController", obj);
    },

})
({
	quickSearch :  function (component,event,counter)
	{
		var selectedFamily = component.find("productFamilyId").get("v.value");
		var selectedLine = component.find("productLineId").get("v.value");
		var selectedModel = component.find("modelId").get("v.value");
		var catalogPartSearch = component.find("searchId").get("v.value");
        var selectedCurrency = component.find("currencyId").get("v.value");	
		var selectedPriceBook = component.find("pricebookId").get("v.value");
		var selectedCountry = component.find("countryId").get("v.value");
        var obj = component.get('v.objClassController');

		if(catalogPartSearch != undefined)
		{
			obj.catalogPartSearch = catalogPartSearch;
		}
     
		if(selectedFamily != undefined && selectedFamily !='')
		{
			obj.selectedFamily = selectedFamily;
		}
        if(selectedCurrency != undefined)
        {
            obj.selectedCurrency = selectedCurrency;
        }
		
		if(selectedPriceBook != undefined)
        {
            var pricebookMap = component.get("v.pricebookMap");
			obj.selectedPricebook = pricebookMap.get(selectedPriceBook);
        }

        if(selectedCountry != undefined)
		{
			obj.selectedCountry = selectedCountry;
		}
		
		obj.selectedLine = selectedLine;
		obj.selectedModel = selectedModel;
		
		var action = component.get('c.quickSearch');
		action.setParams({
            "catLogObj": JSON.stringify(obj)
        });
		action.setCallback(this,function(response)
        {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var obj = response.getReturnValue();
				component.set("v.objClassController", obj);
				console.log('selectedModel:'+obj.selectedModel);
          
				if(counter == 2)
				{
					component.set("v.groupValue",true);
					component.set("v.groupOption",obj.groupOptions);
					
				}
				else if(counter == 3)
				{
				    component.set("v.searchCatalog",true);	
				}	
				else if(counter == 0)
				{
				    component.set("v.ConversionRate",response.getReturnValue().selectedConversionRate);	
				}	
            }
        });
        $A.enqueueAction(action);
	},

	excelExport : function(component, event, counter)
    {
	    var obj = component.get('v.objClassController');
	    var sg = new Object();
	    sg.selectedModel = obj.selectedModel;
	    sg.selectedGroup = obj.selectedGroup;
	    sg.selectedFamily = obj.selectedFamily;
	    sg.selectedLine = obj.selectedLine;
	    sg.selectedCurrency = obj.selectedCurrency;
	    sg.selectedConversionRate = obj.selectedConversionRate;
	    sg.showDiscountable = obj.showDiscountable;
	    sg.selectedRegion = obj.selectedRegion;
	    sg.selectedCountry = obj.selectedCountry;
	    sg.selectedPricebook = obj.selectedPricebook;
	    sg.standardPricebook = obj.standardPricebook;
        //console.log(sg);
	    if(counter ==1)
	    {
            window.open('/apex/Catalog_View_Export_Lightning?catalogJSON='+encodeURIComponent(JSON.stringify(sg)),'_parent');
	    }
	    
	    else if (counter ==2)
	    {
	        window.open('/apex/Catalog_View_ExportALL_Lightning?catalogJSON='+encodeURIComponent(JSON.stringify(sg)),'_parent');
	    }
   },

})
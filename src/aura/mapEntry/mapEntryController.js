({
	doInit : function(component, event, helper) {
        var key = component.get("v.key");
        var map = component.get("v.map");
        component.set("v.value" , map[key]);
	},
    
    
    fireComponentEvent : function(cmp, event) 
    {
        var cmpEvent = cmp.getEvent("cmpEvent");

        cmpEvent.setParams({
            "prodId" : event.getSource().get("v.alt") 
        });
        cmpEvent.fire();
    }
})
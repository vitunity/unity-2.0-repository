({
    doInit : function(component, event, helper) { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getAccountListValues(component,event);
        helper.getPriorityListValues(component,event);
        helper.getproductListValues(component,event);
        helper.getStatusListValues(component,event);
    },
    
    onApply : function(component, event, helper){
    	helper.getCasesList(component,event,true);
    },
    
    callApply : function(component, event, helper){
        if(event.getParams().keyCode === 13){
           helper.getCasesList(component,event,true);
         }
    },
    
    onReset : function(component, event, helper){
        component.find("SelectedInstitution").set("v.value","");
        component.find("ProductGroupId").set("v.value","");
        component.find("StatusId").set("v.value","");
        component.find("refineKeyword").set("v.value","");
        $(".inputDateFrom").val("");
        $(".inputDateTo").val("");
        component.find("viewmyCases").set("v.value",false);
        component.find("casesWithWo").set("v.value",false);
        component.find("casesWithSD").set("v.value",false);
        component.set("v.caseRec.Date_of_Event__c","");
        component.set("v.caseRec.Date_Reported_to_Varian__c","");
        helper.getCasesList(component,event,true);
    },
    
    changeRegion : function(component, event, helper){
        helper.getCasesList(component,event,true);        
    },
    changeProduct : function(component, event, helper){
        helper.getCasesList(component,event,true);        
    },
    onChangeAccount : function(component, event, helper){
        helper.getCasesList(component,event,true);        
    },
    
    handleStatusClick : function(component, event, helper){
        var link = event.currentTarget.id;
        component.set("v.CaseStatusLink",link);
        helper.getCasesList(component,event,true);        
    },
    
    onChangeMyCases : function(component, event, helper){
        helper.getCasesList(component,event,true);        
    },
  
    
    handleExcelClick : function(component, event, helper){        
       helper.handleExcelClick(component,event);
    },
    
    toggleNewCaseDiv : function(component, event, helper) {
        helper.resetNewCase(component, event);
        helper.toggleNewCaseDiv(component, event);
    },
    
    uploadCaseFile:function(component, event, helper) {
        helper.doUploadCaseFile(component,event);
    },
    
    uploadCaseEditFile:function(component, event, helper) {
        helper.doUploadEditCaseFile(component,event);
    },
    
    saveNewCase : function(component, event, helper) {
       debugger;
        if(helper.validateEnquiryFields(component,event) == true){
        	return;   
        }
        if(component.get("v.disableButton") == false){
       	    helper.saveNewCaseWithFile(component, event);    
        }
       
        component.set("v.disableButton",true); 
    },
    
    cancelNewCase : function(component, event, helper) {
        helper.toggleNewCaseDiv(component, event);
        helper.toggleNewCaseDivOnCancel(component, event);
        helper.resetNewCase(component, event);
    },
    
    backToIdeasFromComment : function(component, event, helper) {
        helper.toggleCaseDetails(component, event);
        helper.resetCommentSection(component, event);
    },
    
    openCaseDetails : function(component, event,helper) {
        $('#caseDetailId').removeClass('slds-hide');
        helper.loadCaseDetailsSection(component, event);
    },
    
    backToCaseFromDetails : function(component, event, helper) {
        helper.toggleBackCaseDetails(component, event);
    },
    
    backToCasDetails : function(component, event, helper) {
        helper.toggleCDetails(component, event);
    },
    
    openCaseReportPDF : function(component, event, helper) {
        helper.openCaseReportPDF(component, event);        
    },
    
    handleCaseDetailLinkClick : function(component, event, helper){
        var link = event.currentTarget.id;
        component.set("v.CaseDetailLink",link);
        helper.toggleCaseDetailSections(component,event);
    },
    
    openActivity : function(component, event, helper) {
       $('#divEmailSectionId').removeClass('slds-hide');
        helper.toggleCaseDivSections(component, event,"divCaseDetails","divCaseActivity");
        helper.loadCaseActivityDetails(component, event);
    },
    openCaseFromActivity : function(component, event, helper) {
        helper.toggleCaseDivSections(component, event,"divCaseDetails","divCaseActivity");
    },
    
    openCaseClose : function(component, event, helper) {
        helper.toggleCaseDivSections(component, event,"divCaseDetails","divCaseClose");
    },
    
    openCaseEdit : function(component, event, helper) {
       $A.util.toggleClass(component.find("divCaseDetailButtons"), 'slds-hide');
    },
    
    handleCaseCloseSaveClick : function(component, event, helper) {
        helper.saveCaseClose(component, event);
    },
    
    handleCaseCloseCancelClick : function(component, event, helper) {
        helper.toggleCaseDivSections(component, event,"divCaseClose","divCaseDetails");
    },
    
    handleCaseEditSaveClick : function(component, event, helper) {
        $("#commentSection").modal('hide');
        helper.saveEditCaseWithFile(component, event);
    },
   
    openWOAttachement : function(component, event, helper) {
        helper.openAttachment(component, event);
    },
    
    openEmailAttachement : function(component, event, helper) {
        helper.openAttachment(component, event);
    },
    
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getCasesList(component,event,false);
    },
    
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
})
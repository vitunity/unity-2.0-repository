({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    /* Load custome labels */
   getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
                $("#divNewCaseSection").hide();
                $("#divCaseDetails").hide();
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    /*Get account values */
    getAccValues : function(component,event){
        var action = component.get("c.getAccOptions");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                console.log(picklistresult);
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("SelectedInstitution").set("v.options",options);
            if(options.length > 1){
                component.find("SelectedInstitution").set("v.value",options[1].value); 
            }            
            this.getProductGroupValues(component,event);
        });	
        $A.enqueueAction(action);
    },
    getStatusValues : function(component,event){
        var action = component.get("c.getStatusOptions");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setParams({
            "CaseStatusLink":component.get("v.CaseStatusLink")
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("StatusId").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    getProductGroupValues : function(component,event){
        var action = component.get("c.getProductGroupOptions");
        
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setParams({
            "Accountvalue":component.find("SelectedInstitution").get("v.value")
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("ProductGroupId").set("v.options",options);
             
        });	
        $A.enqueueAction(action);
    },
    getCasesList : function(component,event,resetPages){
        if(resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        var action = component.get("c.getcaseList");
        var dateFrom = $('.inputDateFrom').val();
        var dateTo = $('.inputDateTo').val();
        console.log('Here');
        action.setParams({"CaseStatusLink":component.get("v.CaseStatusLink"),
                          "Accountvalue":component.find("SelectedInstitution").get("v.value"),
                          "StatusVal":component.find("StatusId").get("v.value"),        
                          "Productvalue":component.find("ProductGroupId").get("v.value"),
                          "Searchcasenum":component.find("refineKeyword").get("v.value"),
                          "Mycases":component.find("viewmyCases").get("v.value"),
                          "casesWithWO": component.find("casesWithWo").get("v.value"),
                          "casesWithSD": component.find("casesWithSD").get("v.value"),
                          "caseRecord":component.get("v.caseRec"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "dateFrom":dateFrom,
                          "dateTo":dateTo
                         });
        action.setCallback(this, function(response) {             
            var state = response.getState();   
            if(state == "SUCCESS"){
                var caseList = response.getReturnValue();
                component.set("v.caseList",caseList.lstCases);
                if(caseList.totalSize == 0 ){
                    component.set("v.totalSize",5);
                }else{
                	component.set("v.totalSize",caseList.totalSize);
                }
            }             
        });	
        $A.enqueueAction(action); 
    },
    
    getDefaultAccValues :function(component,event){
    	 var action = component.get("c.getDefaultAccountValue"); 
    	 action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var accValue = response.getReturnValue();
            	component.set("v.defaultValue",accValue);  
                component.find("SelectedInstitution").set("v.value",accValue); 
                this.getCasesList(component,event,true);
            }
        });	
        $A.enqueueAction(action);
    },
    
    checkIsExternalUser : function(component,event){
        var action = component.get("c.checkIsExternalUser");
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var isExternalUser = response.getReturnValue();
                // alert(isExternalUser);
                if(! isExternalUser){
                    component.set("v.isExternalUser",isExternalUser);
                    $A.util.removeClass(component.find("divContact"),"slds-hide");
                }
            }
        });	
        $A.enqueueAction(action);
    },
    
    handleExcelClick : function(component, event, helper){
        var CaseStatusLink=component.get("v.CaseStatusLink");
        var Accountvalue=component.find("SelectedInstitution").get("v.value");
        var StatusVal=component.find("StatusId").get("v.value");        
        var Productvalue=component.find("ProductGroupId").get("v.value");
        var Searchcasenum=component.find("refineKeyword").get("v.value")?component.find("refineKeyword").get("v.value"):'';
        var Mycases=component.find("viewmyCases").get("v.checked");
        var casesWithSD=component.find("casesWithSD").get("v.checked");
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('/');
        var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
        var newUrl = sPageURL.replace(strRemove, '');
        var excelurl = "apex/CpCasePageExcel?link="+CaseStatusLink+"&Productvalue="+Productvalue+"&casesWithSD="+casesWithSD;
        excelurl = excelurl + "&Statusvalue="+StatusVal+"&Searchvalue="+Searchcasenum+"&Account="+Accountvalue+"&Mycases="+Mycases;
        var url = newUrl + excelurl;
        window.location.replace(url) ; 
    },
    
    getproductListValues : function(component,event){
        var action = component.get("c.getproductList");
        var options = [];
        options.push({
            label : 'Select',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("ProductGroupId").set("v.options",options);
            component.find("ProductGroupIdNew").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    getPriorityListValues : function(component,event){
        var action = component.get("c.getPriorityList");
        var options = [];
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("PriorityNew").set("v.options",options);
            component.find("PriorityNew").set("v.value","Medium");
        });	
        $A.enqueueAction(action);
    },
    
    getAccountListValues : function(component,event){
        var action = component.get("c.getAccountList");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }
				           
            }
            component.find("SelectedInstitution").set("v.options",options);
            component.find("SelectedInstitutionNew").set("v.options",options);
            this.getDefaultAccValues(component,event);
        });	
        $A.enqueueAction(action);
    },
    
    getStatusListValues : function(component,event){
        var action = component.get("c.getStatusList");
        var options = [];
        options.push({
            label : '--Any--',
            value : ''
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("StatusId").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    
    
    resetNewCase : function(component, event) {
        debugger;
        var account = component.find("SelectedInstitution").get("v.value");
        component.set("v.newCase.AccountId",account);
        component.set("v.newCase.Priority","Medium");
        component.set("v.newCase.Product_System__c","");
        component.find("SerialNumberdNew").set("v.value","");
        component.set("v.newCase.Subject","");
        component.set("v.newCase.Product_Version_Com__c",""); 
        component.set("v.newCase.Description",""); 
        component.find("describeId").set("v.value",""); 
        component.set("v.disableButton",false); 
        $(".chooseFile").val("").trigger("change");
        var radios = document.getElementsByName('optionsRadios');

       for (var i = 0, length = radios.length; i < length; i++) {
           if(radios[i].value == 'Email')
               radios[i].checked =true;
           else
               radios[i].checked=false;
       }
        
    },
    
    saveNewCase : function(component,event,file,fileContents,isFile){
        debugger;
        /*Dipali : STRY0016236: Start*/  
        var radios = document.getElementsByName('optionsRadios');

       for (var i = 0, length = radios.length; i < length; i++) {
         if (radios[i].checked) {
            var radioValue= radios[i].value;
            component.set("v.newCase.Primary_Contact_Number__c",radioValue);
           }
         }
         /*Dipali : STRY0016236: End*/ 
        $('.loading').show();
        var showCaseFilter = true;
        var newCase = component.get("v.newCase");
        var fileName ="";
        var base64Data = "";
        var contentType = "";
        if(isFile){
            base64Data = encodeURIComponent(fileContents);
            fileName  =  file.name;
            contentType = file.type
        }
        var action = component.get("c.saveCase");
        var serialnumber = component.find("SerialNumberdNew").get("v.value");
        var SubjectNew = component.find("SubjectNew").get("v.value");
        var versionIdNew = component.find("versionIdNew").get("v.value");
        var describeId = component.find("describeId").get("v.value");
        var ProductGroupIdNew = component.find("ProductGroupIdNew").get("v.value");
      
        //alert(serialnumber);
        //alert(SubjectNew);
        //alert(versionIdNew);
        //alert(describeId);
        //alert(ProductGroupIdNew);
        console.log(' ### JSON #### ' + component.get("v.newCase"));
        console.log(' ### JSON #### ' + JSON.stringify(component.get("v.newCase")));
        console.log("==serialnumber=="+serialnumber); 
        action.setParams({"newCase":newCase,
                          SerialName:component.find("SerialNumberdNew").get("v.value"),
                          SubjectNew: component.find("SubjectNew").get("v.value"),
                          versionIdNew: component.find("versionIdNew").get("v.value"),
                          describeId: component.find("describeId").get("v.value"),
                          product: component.find("ProductGroupIdNew").get("v.value"),
                          fileName: file.name,
                          base64Data: base64Data,
                          contentType: contentType
                          
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();  
            if(state == "SUCCESS"){
                 $('.loading').hide();
                var caseID = response.getReturnValue();
                if(!$A.util.isEmpty(caseID)){
                    this.getCasesList(component,event);
                    $("#divCasesFilterSection").show();
                    $("#divCasesSection").show();
                    $("#divNewCaseSection").hide();
                    this.resetNewCase(component, event);
                }
            }            
        });	
        $A.enqueueAction(action); 
    },
    
    doUploadCaseFile : function(component, event){ 
         var fileInput = component.find("file").getElement();
         if(fileInput.files.length > 0){
            var file = fileInput.files[0];
            var fr = new FileReader();
            fr.onload = function() {
                var fileContents = fr.result;
                component.set('v.fileContents',fileContents);
            };
            fr.readAsDataURL(file);
         }
    },
    
    doUploadEditCaseFile : function(component, event){ 
         var fileInput = component.find("fileEditCase").getElement();
         if(fileInput.files.length > 0){
            var file = fileInput.files[0];
            var fr = new FileReader();
            fr.onload = function() {
                var fileContents = fr.result;
                component.set('v.fileContents',fileContents);
            };
            fr.readAsDataURL(file);
         }
    },
    
    saveNewCaseWithFile : function(component, event){ 
        var self = this;
        var fileInput = component.find("file").getElement();
        if(fileInput.files.length > 0){
            var file = fileInput.files[0];
            var fileContents = component.get('v.fileContents');
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.saveNewCase(component,event,file,fileContents,true);            
        }else{
            self.saveNewCase(component,event,"","",false);
    	}
    },
    
    loadCaseDetailsSection : function(component,event){
        var index = event.currentTarget.id;
        var caseId;
        var caseobj;
        var caseList;
        if(index){
            caseList = component.get("v.caseList");
            if(!$A.util.isEmpty(caseList)){
                caseobj = caseList[index];
                if(caseobj){                    
                    caseId = caseobj.Id;
                    component.set("v.caseId",caseId);
                    component.set("v.caseRecDetails",caseobj);
                    component.set("v.CaseDetailLink","Details");   
                    this.getCaseNotesSection(component, event);
                    this.getWOSection(component, event);
                }
            }            
            $("#divCasesFilterSection").hide();
            $("#divCasesSection").hide();
            $("#divCaseDetails").show();
            
        }
    },
    
    getCaseNotesSection : function(component,event){
        var action = component.get("c.getCaseNotes");
        //alert(component.get("v.CaseDetailLink"));
        action.setParams({"caseId":component.get("v.caseId"),
                          "caseType":component.get("v.CaseDetailLink")
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
           // alert(state);
            if(state == "SUCCESS"){
                var caseList = response.getReturnValue(); 
                //alert(caseList);
                //debugger;
                component.set("v.commentsList",caseList);
            }
			this.getCaseAttachments(component,event); 
             
        });	
        $A.enqueueAction(action); 
    },
    
    getCaseAttachments : function(component,event){
        //debugger;
        var action = component.get("c.getCaseAttachments");
        //alert(component.get("v.CaseDetailLink"));
        action.setParams({"caseId":component.get("v.caseId"),
                          "caseType":component.get("v.CaseDetailLink")
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
           	console.log("----state"+state);
            console.log("----response"+JSON.stringify(response));
            if(state == "SUCCESS"){
                var caseList = response.getReturnValue(); 
                //alert(caseList);
                //debugger;
                component.set("v.attachmentsList",caseList);
            }else if(state === "ERROR"){
                console.log('-----in ERROR'+state);
                var errors = response.getError();
                console.log('-----in ERROR Message'+JSON.stringify(errors));
            }
        });	
        $A.enqueueAction(action); 
    },
    
     
    
    
    getWOSection : function(component,event){
        var action = component.get("c.getWorkOrders");
       // var caseType = component.get("v.CaseDetailLink");
        var caseType = "WorkOrder";
        action.setParams({"caseId":component.get("v.caseId"),
                          "caseType": caseType
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
           // alert(state);
            if(state == "SUCCESS"){
                var woList = response.getReturnValue(); 
                //alert(woList);
                component.set("v.woList",woList);
            }            
        });	
        $A.enqueueAction(action); 
    },
    
    openCaseReportPDF : function(component,event){
        var caseId = component.get("v.caseId");
        if(caseId){
            var sPageURL = window.location.href;
            var sURLVariables = sPageURL.split('/');
            var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
            
            var newUrl = sPageURL.replace(strRemove, '');
            
            var pdfurl = "apex/CpCaseReportPDF?id="+caseId;
            //window.open("/apex/CpCaseReportPDF?id={!currCase.Id}");
            var url = newUrl + pdfurl;
           // window.location.replace(url) ;
            
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                isredirect: "true"
            });
            urlEvent.fire();
        }
    },
    
    loadCaseActivityDetails : function(component,event){
        var index = event.currentTarget.id;
        var taskId;
        var comment;
        var commentsList;
        
        if(index){
            commentsList = component.get("v.commentsList");
            if(!$A.util.isEmpty(commentsList)){
                comment = commentsList[index];
                if(comment){
                    taskId = comment.objectId;
                    component.set("v.taskId",taskId);
                   // component.set("v.caseRecDetails",comment);
                    // component.set("v.commentBody",idea.Description);
                }
            }
            this.getCaseActivityDetails(component,event);
            //debugger;
            $("#divCasesFilterSection").hide();
            $("#divCasesSection").hide();
            $("#divCaseDetails").hide();     
            $("#divEmailSection").show();
        }
    },
    getCaseActivityDetails : function(component,event){
        var action = component.get("c.getCaseActivity");
        
        action.setParams({"taskId":component.get("v.taskId")});
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
           // alert(state);
            if(state == "SUCCESS"){
                var caseActivity = response.getReturnValue(); 
              //  alert(caseActivity.TaskDetail.Attachments.length);
                component.set("v.caseActivity",caseActivity.TaskDetail);
                component.set("v.TaskComment",caseActivity.TaskComment);
                component.set("v.emailAttachments",caseActivity.TaskDetail.Attachments);
                component.set("v.TaskSubject",caseActivity.TaskSubject);
            }
            
        });	
        $A.enqueueAction(action); 
    },
    openAttachment : function(component,event){
        var docId = event.currentTarget.id;        
        if(docId){
            var sPageURL = window.location.href;
            var sURLVariables = sPageURL.split('/');
            var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
            
            var newUrl = sPageURL.replace(strRemove, '');
            var docurl = "servlet/servlet.FileDownload?file="+docId;
            var url = newUrl + docurl;
            
           // window.location.replace(url) ;
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": url,
                isredirect: "true"
            });
            urlEvent.fire();        
        }
    },
       
    saveCaseClose : function(component,event){
        var action = component.get("c.closeCase");
        //alert(component.find("closeCaseReasonId").get("v.value"));
        action.setParams({"caseId":component.get("v.caseId"),
                          //"closeReason":component.get("v.caseRecDetails.Closed_Case_Reason__c")
                          "closeReason":component.find("closeCaseReasonId").get("v.value")
                         });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue(); 
                if(result){ 
                    //debugger;
                    //this.toggleCaseDiv1Sections(component, event,"divCaseClose","divCaseDetails");
                    this.getCaseNotesSection(component,event);
                    $("#requestToClose").modal('hide');
                }
               // component.set("v.commentsList",caseList);
            }            
        });	
        $A.enqueueAction(action); 
    },
    validateEnquiryFields: function(component,event) {
    	var errorFlag = false;
        var SubjectNewVar = component.find("SubjectNew").get("v.value");
      	var SerialNumberdNewVar = component.find("SerialNumberdNew").get("v.value");
        
        var versionIdNewVar = component.find("versionIdNew").get("v.value");
      	var describeIdVar = component.find("describeId").get("v.value");
      	var ProductGroupIdNewVar = component.find("ProductGroupIdNew").get("v.value");
        var PriorityNewVar = component.find("PriorityNew").get("v.value");
      
        
         if(ProductGroupIdNewVar == undefined || ProductGroupIdNewVar == null || ProductGroupIdNewVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("ProductGroupIdNew"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("ProductGroupIdNew"), 'errorClass');
        }
        
        
        if(PriorityNewVar == undefined || PriorityNewVar == null || PriorityNewVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("PriorityNew"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("PriorityNew"), 'errorClass');
        }
        
        
        if(SubjectNewVar == undefined || SubjectNewVar == null || SubjectNewVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("SubjectNew"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("SubjectNew"), 'errorClass');
        }

        if(describeIdVar == undefined || describeIdVar == null || describeIdVar == ''){
        	errorFlag = true;
            $A.util.addClass(component.find("describeId"), 'errorClass');
           
        }else{
            $A.util.removeClass(component.find("describeId"), 'errorClass');
        }
        
        return errorFlag;
    },
    
    saveEditCaseWithFile : function(component, event){ 
        var self = this;
        var fileInput = component.find("fileEditCase").getElement();
        if(fileInput.files.length > 0){
            var file = fileInput.files[0];
            var fileContents = component.get('v.fileContents');
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.saveCaseEdit(component,event,file,fileContents,true);            
        }else{
            self.saveCaseEdit(component,event,"","",false);
    	}
        
    },
    saveCaseEdit : function(component,event,file,fileContents,isFile){
        var caseId = component.get("v.caseId");
        var fileName ="";
        var base64Data = "";
        var contentType = "";
        //debugger;
        if(isFile){
            base64Data = encodeURIComponent(fileContents);
            fileName  =  file.name;
            contentType = file.type
        }
        var action = component.get("c.saveEditCase");
        
        action.setParams({"caseId":caseId,
                          CommentInput:component.find("CaseEditCommentId").get("v.value"),
                          fileName: file.name,
                          base64Data: base64Data,
                          contentType: contentType
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
            //alert(state);
            if(state == "SUCCESS"){
                //debugger;
                var caseID = response.getReturnValue();
                // alert(ideaID);
                if(!$A.util.isEmpty(caseID)){
                    this.resetEditCase(component,event);
                    /*this.toggleCaseDivSections(component, event,"divCaseEdit","divCaseDetailSections");
                    $A.util.toggleClass(component.find("divCaseDetailButtons"), 'slds-hide');
                    $A.util.addClass(component.find("caseEditModal"), 'slds-hide');*/
                    this.getCaseNotesSection(component,event);
                    
                    $("#commentSection").modal('hide');
                    //this.getCaseNotesSection(component, event);
                    //this.getWOSection(component,event);
                    //$("#contentWrapper").modal('hide');
                }
            }            
        });	
        $A.enqueueAction(action); 
    },
    resetEditCase : function(component, event) {
        var account = component.find("CaseEditCommentId").set("v.value","");
         $(".chooseFile").val("").trigger("change");
    },
    
    toggleNewCaseDiv : function(component, event) {
        $("#divCasesFilterSection").hide();
        $("#divCaseDetails").hide();
        $("#divCasesSection").hide();
        $("#divNewCaseSection").show();
        $('#newCaseSectionId').removeClass('slds-hide'); 
       
    },
    toggleNewCaseDivOnCancel :function(component, event){ 
        $("#divCasesFilterSection").show();
        $("#divCasesSection").show();
        $("#divNewCaseSection").hide();
    },
    
    toggleBackCaseDetails : function(component, event) {
        $("#divCasesFilterSection").show();
        $("#divCasesSection").show();
        $("#divCaseDetails").hide();
        $("#divEmailSection").hide();
    },
    
    toggleCDetails : function(component, event) {
        $("#divCasesFilterSection").hide();
        $("#divCasesSection").hide();
        $("#divCaseDetails").show();
        $("#divEmailSection").hide();
    },
    
    
   /* toggleCaseDetailSection : function(component, event) {
        $A.util.toggleClass(component.find("divComments"), 'slds-hide');
        $A.util.toggleClass(component.find("divWODetails"), 'slds-hide');      
    },*/
    
    toggleCaseDivSections : function(component, event, hideDiv, showDiv) {
        $A.util.toggleClass(component.find(hideDiv), 'slds-hide');
        $A.util.toggleClass(component.find(showDiv), 'slds-hide');      
    },
    
	toggleCaseDiv1Sections : function(component, event, hideDiv, showDiv) {
        //debugger;
        //this.getCasesList();
        $A.util.toggleClass(component.find("divCaseClose"), 'slds-hide');
        var divCaseDetails = true;
        component.set("v.divCaseDetails", divCaseDetails);
        //$A.util.toggleClass(component.find(divCaseClose), 'slds-hide');
        //debugger;
        //this.resetNewCase();
        //$A.util.toggleClass(component.find(showDiv), 'slds-show'); 
        
     }, 
    
    
    toggleCaseActivity : function(component, event) {
        $A.util.toggleClass(component.find("divCaseDetails"), 'slds-hide');
        $A.util.toggleClass(component.find("divCaseActivity"), 'slds-hide');      
    },
    
    toggleCaseDetailSections : function(component, event) {
        //debugger;
        if(component.get("v.CaseDetailLink") =="Details"){
            $A.util.removeClass(component.find("divComments"), 'slds-hide');
            //$A.util.addClass(component.find("divWODetails"), 'slds-hide');
            //$A.util.addClass(component.find("divRecommandation"), 'slds-hide');
        }else if(component.get("v.CaseDetailLink") =="WorkOrder"){
            $A.util.addClass(component.find("divComments"), 'slds-hide');
            $A.util.removeClass(component.find("divWODetails"), 'slds-hide');
            $A.util.addClass(component.find("divRecommandation"), 'slds-hide');
        }else if(component.get("v.CaseDetailLink") =="Recommendation"){
            $A.util.addClass(component.find("divComments"), 'slds-hide');
            $A.util.addClass(component.find("divWODetails"), 'slds-hide');
            $A.util.removeClass(component.find("divRecommandation"), 'slds-hide');
        }        
    },
})
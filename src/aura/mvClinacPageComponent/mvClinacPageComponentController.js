({
	doInit : function(component, event, helper) 
    {
        helper.getCustomLabels(component, event);
	},
	mvnewregist : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/montecarloregister?mtype="+component.get('v.mtype')
        });
        urlEvent.fire();
	}
})
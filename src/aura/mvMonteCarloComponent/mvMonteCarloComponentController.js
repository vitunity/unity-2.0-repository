({
	doInit : function(component, event, helper) {
       
        helper.getUrlParameter(component, event);
       
        helper.getCustomLabels(component, event);
        
        helper.getContactHelper(component, event);
        
        helper.getMyVarianAgreementHelper(component, event);
        
	},
    
    onGroup: function(component, event) {
        var selected = event.getSource().get("v.text");
        component.set("v.projectLenghtType",selected);
    },
    
    registerMonteCarlo: function(component, event, helper) {
        var validate = helper.validateAgreement(component,event)
        if(validate == false)
        {
            helper.callRegisterAction(component, event);
            helper.getDocuments(component, event);
            var myType = component.get("v.mtype");
            if(myType == 'Clinac')
            {
                $('#second-section-doc').css('visibility','visible');
                $('#second-section').hide();
                $('#second-section-doc').show();

            }
            else if(myType == 'Truebeam')
            {
                $('#second-section-trueBeam-doc').css('visibility','visible');
                $('#second-section-trueBeam').hide();
                $('#second-section-trueBeam-doc').show();

            }
            $("#Agreement").modal('hide');
        }
    },
    
    agreementRegisterClinac: function(component, event, helper) {
        component.set('v.mtype','Clinac');
        var validate = helper.validateInputs(component,event);
        if(validate == false){
            $("#Agreement").modal('show');
            $("#trueBeamDoc").hide();
         	$("#register-clinac").modal('hide');
        }
	},
    
    agreementRegisterTrubeam: function(component, event,helper) {
         component.set('v.mtype','Truebeam');
         var validate = helper.validateInputs(component,event);
         if(validate == false){
            $("#Agreement").modal('show');
             $("#clinacDoc").hide();
         	$("#register-truebeam").modal('hide');
         }
	},
    showTrubeamDocuments: function(component, event,helper) {
        component.set('v.mtype','Truebeam');
        helper.getDocuments(component, event);
        component.set("v.showTruebeam", true);
        component.set("v.showHome", false);
    },
    
    showClinacDocuments: function(component, event,helper) {
        component.set('v.mtype','Clinac');
        helper.getDocuments(component, event);
        component.set("v.showClinac", true);
        component.set("v.showHome", false);
    },
    
     showTrubeam: function(component, event,helper) {
        component.set('v.mtype','Truebeam');
        component.set("v.showTruebeam", true);
        component.set("v.showHome", false);
    },
    
    showClinac: function(component, event,helper) {
        component.set('v.mtype','Clinac');
        component.set("v.showClinac", true);
        component.set("v.showHome", false);
    },
    
    
    home: function(component, event, helper) {
        var url = "/";
        if (component.get("v.isloggedIn")) {
            url = "/homepage?lang=" + component.get('v.language')
        }

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url+"homepage?lang=" + component.get('v.language')
        });
        urlEvent.fire();
    },

    monteCarloPage: function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           "url": "/montecarlo?lang=" + component.get('v.language')
        });
        urlEvent.fire();
    },
        
})
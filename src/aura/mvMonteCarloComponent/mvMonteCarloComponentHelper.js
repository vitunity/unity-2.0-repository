({
	/* get url parameter */
	getUrlParameter : function(component, event) {
       
        var getUrlParameter = function getUrlParameter(sParam) {
           var sPageURL = decodeURIComponent(window.location.search.substring(1)),
               sURLVariables = sPageURL.split('&'),
               sParameterName,
               i;
           for (i = 0; i < sURLVariables.length; i++) {
               sParameterName = sURLVariables[i].split('=');
               if (sParameterName[0] === sParam) {
                   return sParameterName[1] === undefined ? true : sParameterName[1];
               }
           }
       };
	     var lang =  getUrlParameter('lang');
       component.set("v.language", lang);
       var mtypeVar =  getUrlParameter('mtype')
       component.set("v.mtype",mtypeVar);
	},
    /* Load custome labels */
	getCustomLabels : function(component, event) {
	   var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);

	},
    
    
    
     /* Load custome labels */
	getDocuments : function(component, event) {
	   var action = component.get("c.accessDocs");
       action.setParams({"pageName":component.get("v.mtype")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           //alert(response.getReturnValue()[0].TitleVal);
           if (component.isValid() && state === "SUCCESS") {
               component.set("v.documents",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
    
    /* Load custome labels */
	getMyVarianAgreementHelper : function(component, event) {
	   var action = component.get("c.getMyVarianAgreement");
       action.setParams({
           "name":"MONTE_CARLO",
           "languageCode":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.agreementList",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
	},
   
    getContactHelper:function(component,event,helper){    
        var getContact = component.get("c.getContact");
        getContact.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var contact = response.getReturnValue();
                component.set("v.contactObj", contact);
                component.set("v.contactId", contact.Id);
                if(contact.Id == null){
                     $('#register-clinac-btn').hide();
                     $('#register-truebeam-btn').hide();
                     $('#without-truebeam-access').hide();
                     $('#without-clinac-access').hide();
                     $('#with-clinac-access').removeClass('hide');
                     $('#with-truebeam-access').removeClass('hide');
                     $('#show-agreement').hide();
                }else{
                   if ($A.util.isEmpty(contact.Monte_Carlo_Registered__c) 
                         || $A.util.isUndefined(contact.Monte_Carlo_Registered__c)) {
                   		$('#with-clinac-access').hide();
                        $('#with-truebeam-access').hide();
                        $('#show-agreement').hide();
                        $('#without-clinac-access').removeClass('hide');
                        $('#without-truebeam-access').removeClass('hide');
                   }else{
                       debugger;
                       /* Clinac Check */
                       if(contact.Monte_Carlo_Registered__c.includes("Clinac")){
                            $('#register-clinac-btn').hide();
                            $('#without-clinac-access').hide();
                            $('#with-clinac-access').removeClass('hide');
                        }else{
                            $('#with-clinac-access').hide();
                            $('#without-clinac-access').removeClass('hide');
                        }
                        /* Truebeam Check */
                        if(contact.Monte_Carlo_Registered__c.includes("TrueBeam")){
                            $('#register-truebeam-btn').hide();
                            $('#without-truebeam-access').hide();
                            $('#with-truebeam-access').removeClass('hide');
                        }else{
                            $('#with-truebeam-access').hide();
                            $('#without-truebeam-access').removeClass('hide');
                        }
                   }
                }
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getContact);
    },
    
    validateInputs: function(component,event){  
        var data = component.get("v.registerData");
        if ($A.util.isEmpty(data.Requested_Data__c) || $A.util.isUndefined(data.Requested_Data__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(data.Intended_Use__c) || $A.util.isUndefined(data.Intended_Use__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(data.Principal_Investigator__c) || $A.util.isUndefined(data.Principal_Investigator__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(data.Co_Investigators__c) || $A.util.isUndefined(data.Co_Investigators__c)) {
            alert('Please fill required fields');
            return;
        }
        
        return false;
    },


    validateAgreement: function(component,event){  
        var data = component.get("v.registerData");
        if ($A.util.isEmpty(data.Rule_8_Initial__c) || $A.util.isUndefined(data.Rule_8_Initial__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(data.Rule_9_Initial__c) || $A.util.isUndefined(data.Rule_9_Initial__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(data.Rule_10_Initial__c) || $A.util.isUndefined(data.Rule_10_Initial__c)) {
            alert('Please fill required fields');
            return;
        }
        
        return false;
    },
    
    callRegisterAction: function(component,event){  
        var registerMData = component.get("c.registerMData");
        var contactId = component.get("v.contactId");
        var rdata = component.get("v.registerData");
        var projectLenght = component.get("v.projectLenght");
        var projectLenghtType = component.get("v.projectLenghtType");
        registerMData.setParams({
            "contactId": contactId,
            "registerDataObj": rdata,
            "projectLenght": projectLenght,
            "projectLenghtType": projectLenghtType,
            "mtype":component.get('v.mtype')
        });
        var urlEvent = $A.get("e.force:navigateToURL");
        registerMData.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                urlEvent.setParams({
                    "url": "/montecarlo?lang=" + component.get('v.language')
                });
                //urlEvent.fire();
            } else if (response.getState() == "ERROR") {
               
            }
        });
        $A.enqueueAction(registerMData);
	}
    
})
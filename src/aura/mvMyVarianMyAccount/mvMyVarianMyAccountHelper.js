({
    /* get url parameter */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam,fParam) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    ////data.lang = sParameterName[1] === undefined ? true : sParameterName[1]; //UB: Commented April 19
                    data.lang = sParameterName[1] === undefined ? 'en_US' : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.myfav = sParameterName[1] === 'true' ? true : false;
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('lang','myFav');
       component.set("v.CurrentSelection","myProfile");
       component.set("v.myFavDefault",'profile');

        if(bdata.myfav == true)
        {
            
            // var favBoldTag = component.find('favIconId');
            // $A.util.addClass(favBoldTag, 'active');


            // var favSpanTag = component.find('favSpanId');
            // $A.util.addClass(favSpanTag, 'li-color-toggle');

            // var myProfileSpanTag = component.find('myProfileSpanId');
            // $A.util.removeClass(myProfileSpanTag, 'li-color-toggle');

            // var myProfileBoldTag = component.find('myProfileIconId');
            // $A.util.removeClass(myProfileBoldTag, 'active');
          

            component.set("v.myFavDefault",'true');
            var myfavDivTag = component.find('myfavDetails');
            $A.util.addClass(myfavDivTag, 'active');

            var myProfileDivTag = component.find('myProfileDetails');
            $A.util.removeClass(myProfileDivTag, 'active');

            component.set("v.CurrentSelection","fav");
        }
        component.set("v.language",bdata.lang);
    },
    /* Load custome labels */
    getCustomLabels : function(component, event) {
       var action = component.get("c.getCustomLabelMap");
       action.setParams({"languageObj":component.get("v.language")});
       action.setCallback(this, function(response) {
           var state = response.getState();
           if (component.isValid() && state === "SUCCESS") {
            component.set("v.customLabelMap",response.getReturnValue());
           }else if(response.getState() == "ERROR"){
               $A.log("callback error", response.getError());
           }      
       } );
       $A.enqueueAction(action);
    },
    
    getOptionsCountry : function(component, event) {
        var action = component.get("c.getOptionsCountry");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.OptionsCountry", response.getReturnValue());
                var countValues = component.get("v.OptionsCountry");

                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                    ControllerField.push({
                      class: "optionClass",
                      label: countValues[i],
                      value: countValues[i]
                    });
                }
                component.find('Country1').set("v.options", ControllerField);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    getSalutation : function(component, event) {

        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Salutation"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            { 
                component.set("v.mvSalutation", response.getReturnValue());
                var countValues = component.get("v.mvSalutation");
                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                   if(component.get("v.loggedInUser.Salutation") == countValues[i])
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i],
                          selected : "true"
                        });
                    }
                    else
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i]
                        });
                    }
                }
                component.find('Salutation').set("v.options", ControllerField);
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getPreferredLanguage : function(component, event) {
        var action = component.get("c.getLanguagePicklistOptn");
        action.setParams({"fieldName":"Preferred_Language1__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var Languages = new Array();
                var returnArray = response.getReturnValue();
                for(var i=0; i < returnArray.length; i++)
                {
                    Languages.push({
                        label: returnArray[i].split(',')[0],
                        value: returnArray[i].split(',')[1]
                    });
                    /*
                    if(returnArray[i] != component.get("v.registerData.Preferred_Language1__c"))
                    {
                        Languages.push(returnArray[i]);
                    }*/
                }
                component.set("v.mvPreferredLanguage", Languages);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getSpecialty : function(component, event) {
        
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Specialty__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            { 
                component.set("v.mvSpecialty", response.getReturnValue());
                var countValues = component.get("v.mvSpecialty");
                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                   if(component.get("v.loggedInUser.Specialty__c") == countValues[i])
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i],
                          selected : "true"
                        });
                    }
                    else
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i]
                        });
                    }
                }
                component.find('Specialty').set("v.options", ControllerField);
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getFunctionalRole : function(component, event) {

        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Functional_Role__c"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            { 
                component.set("v.mvFunctionalRole", response.getReturnValue());
                var countValues = component.get("v.mvFunctionalRole");
                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                   if(component.get("v.loggedInUser.Functional_Role__c") == countValues[i])
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i],
                          selected : "true"
                        });
                    }
                    else
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i]
                        });
                    }
                }
                component.find('Role').set("v.options", ControllerField);
                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getCountry : function(component, event) {
        var action = component.get("c.getCountryOptions");
       // action.setParams({"fieldName":"MailingCountry"});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.mvCountry", response.getReturnValue());
                var countValues = component.get("v.mvCountry");
                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                   if(component.get("v.loggedInUser.MailingCountry") == countValues[i])
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i],
                          selected : "true"
                        });
                    }
                    else
                    {
                        ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i]
                        });
                    }
                }
                component.find('CountryId').set("v.options", ControllerField);
                component.set("v.listCountries", ControllerField);
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getRecoveryQuestion : function(component, event) {
        var action = component.get("c.getDynamicPicklistOptions");
        action.setParams({"fieldName":"Recovery_Question__c"});
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var countValues = response.getReturnValue();
                component.set("v.mvRecoveryQuestion", countValues);
                var ControllerField = [];
                for (var i = 0; i < countValues.length; i++) 
                {
                    ControllerField.push({
                          class: "optionClass",
                          label: countValues[i],
                          value: countValues[i]
                        });
                }
                component.find('RecoveryQuestion').set("v.options", ControllerField);

            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },    
    
    getInstitution : function(component, event) {
        var action = component.get("c.getCRAs");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.cra", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },    
    
    getContactUpdate : function(component, event) {
        var action = component.get("c.getContactUpdate");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.cu", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    }, 
    
    getLoggedInUser : function(component, event) {
        var action = component.get("c.getLoggedInUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.loggedInUser", response.getReturnValue());
                if(response.getReturnValue().Id == null){
                    var int = setInterval(function(){ 
                        $("#myNoti").click();
                        $("#myNotiLi").click();
                        clearInterval(int);
                    }, 100);
                    
                }                
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },    
    
    getUserObject : function(component, event) {
        var action = component.get("c.getSubscribeInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.userObject", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    setTab : function(component, event) 
    {
        component.set("v.tabId", "myInformation");
    },
    
    setSubscribe : function(component, event) {
        var actionSubscribe = component.get("c.getSubscribeInfo");
        actionSubscribe.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                var subscribeCom = component.find('subscribeTag');
                var unsubscribeCom = component.find('unsubscribeTag');
                component.set("v.subscribedUser", response.getReturnValue());
                if(component.get("v.subscribedUser").Subscribed_Products__c)
                {
                    $A.util.removeClass(unsubscribeCom, 'slds-hide'); 
                }
                else
                {
                    $A.util.removeClass(subscribeCom, 'slds-hide'); 
                }
            } 
            else if (response.getState() == "ERROR") 
            {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(actionSubscribe);
    },

    regeditNow: function(component, event) {
        ////var LMSMsg1Com = component.find('LMSMsg1');   //UB: commented 19 April
        ////var regeditNowCom = component.find('regeditNow');   //UB: commented 19 April
        var actionRegister = component.get("c.registerNow");
        actionRegister.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set('v.userObject', response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(actionRegister);
    },
    
    getMyFavoritesOld : function(component, event) {
        var action = component.get("c.getMyFavorites");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.myFavorites", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* get My  Favorites*/
    getMyFavorites : function(component, event) {
        var action = component.get("c.getMyFavorites");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
               // component.set("v.myFavorites", response.getReturnValue());
                this.translateMyFavorites(component, event, response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    /* translate Contact My Favorites using custom label MV_Favorites*/
    translateMyFavorites: function(component, event, myFavorites) {
        
        if(myFavorites.length > 0){
            var allMyFavorites = component.get("v.customLabelMap.MV_Favorites");
            
            var lstAllFavorites  = []
            lstAllFavorites = allMyFavorites.split(",");
            if(myFavorites.length > 0){            
                lstAllFavorites.forEach(function(item){
                    var favs  = []
                    favs = item.trim().split(":");
                    if(favs.length > 1){
                        var i;
                        for (i = 0; i < myFavorites.length; i++) {
                            if(favs[0] == myFavorites[i].optionValue) {
                                //myFavorites[i].optionValue = favs[1];
                                myFavorites[i].optionLabel = favs[1];
                                myFavorites[i].optionTranslatedLabel = favs[1];
                                break;
                            }
                        }
                        /*
                        var val = myFavorites.filter(item=>item.optionValue==favs[0]);
                        if(val){
                           val.optionLabel = favs[1];
                        }
                        */
                    }                    
                });
            } 
        }
        component.set("v.myFavorites", myFavorites);
    },
    
    getProdList1 : function(component, event) {
        var action = component.get("c.fetchProds1");
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.prodList1", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    getProdList2 : function(component, event) {
        var action = component.get("c.fetchProds2");
        action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") { 
                component.set("v.prodList2", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    
    createNewRegistration : function(component, event) {

    },
    

    saveContact : function(component, event) 
    {
        ////var action = component.get("c.saveMyInfomation");
        var loggedInUser = component.get("v.loggedInUser");
        //alert('loggedInUser = ' + loggedInUser);

        if ($A.util.isEmpty(loggedInUser.FirstName) || $A.util.isUndefined(loggedInUser.FirstName)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.LastName) || $A.util.isUndefined(loggedInUser.LastName)) {
            alert('Please fill required fields');
            return ;
        }

        if ($A.util.isEmpty(loggedInUser.Title) || $A.util.isUndefined(loggedInUser.Title)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.Email) || $A.util.isUndefined(loggedInUser.Email)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.Phone) || $A.util.isUndefined(loggedInUser.Phone)) {
            alert('Please fill required fields');
            return;
        }

        /*
        if ($A.util.isEmpty(loggedInUser.Fax) || $A.util.isUndefined(loggedInUser.Fax)) {
            alert('Please fill required fields');
            return;
        }
        */

        if ($A.util.isEmpty(loggedInUser.Manager__c) || $A.util.isUndefined(loggedInUser.Manager__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.Manager_s_Email__c) || $A.util.isUndefined(loggedInUser.Manager_s_Email__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.Account.AccountNumber) || $A.util.isUndefined(loggedInUser.Account.AccountNumber)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.MailingStreet) || $A.util.isUndefined(loggedInUser.MailingStreet)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.MailingCity) || $A.util.isUndefined(loggedInUser.MailingCity)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.MailingState) || $A.util.isUndefined(loggedInUser.MailingState)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(loggedInUser.MailingPostalCode) || $A.util.isUndefined(loggedInUser.MailingPostalCode)) {
            alert('Please fill required fields');
            return;
        }
        




        var newInstituteName = document.getElementById("text-input-id-2").value;
        var oldInstituteName = component.get("v.loggedInUser.Account.Name");

        var newEmail = document.getElementById("text-input-email").value;
        var oldEmail = component.get("v.loggedInUser.Email");
        
        var oldCity = component.get("v.loggedInUser.MailingCity");
        var newCity = document.getElementById("text-input-city").value;

        var oldState = component.get("v.loggedInUser.MailingState");
        var newState = document.getElementById("text-input-state").value;

        var oldCountry = component.get("v.loggedInUser.MailingCountry");
        var newCountry = document.getElementById("CountrySelectId").value; 

        // alert('new value = ' + newCountry);
        // alert('old value = ' + oldCountry);        

        var oldPostal = component.get("v.loggedInUser.MailingPostalCode");
        var newPostal = document.getElementById("text-input-postal").value;  

        var oldStreet = component.get("v.loggedInUser.MailingStreet");
        var newStreet = document.getElementById("text-input-street").value;                    

        var errorFound = component.get("v.showError");
        //alert('errorFound = ' + errorFound);
        if(errorFound == true) {
            return;
        }

        var cmpTarget = component.find('spinnerId');
        $A.util.removeClass(cmpTarget, 'hide'); 

        var action;

        if(newInstituteName != oldInstituteName
            //|| newEmail != oldEmail
            || newCity != oldCity
            || newCountry != oldCountry
            || newPostal != oldPostal
            || newStreet != oldStreet
            || newState != oldState)
        {
            //alert('institue changed.');
            //this.createNewRegistration(component, event);
            action = component.get("c.registerMDataIntituteChange");
            action.setParams({"registerDataObj": loggedInUser,
                              "insName" : newInstituteName,
                              "newEmail": newEmail,
                              "newCity": newCity,
                              "newState": newState,
                              "newCountry": newCountry,
                              "newPostal": newPostal,
                              "newStreet": newStreet                              
                            });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                //alert('ins chaanged state = ' + state);
                if(state == "SUCCESS")
                {
                    var str = response.getReturnValue();
                    $A.util.addClass(cmpTarget, 'hide');                                             
                    $('#new-reg-ins-change').modal('show');   
                    ////location.reload();            
                }
                else if (state == "ERROR") 
                {
                   var errors = response.getError();
                   alert(errors[0].message);
                   $A.util.addClass(cmpTarget, 'hide');                   
                }
            }); 
            ////$A.util.addClass(cmpTarget, 'hide');
            
            $A.enqueueAction(action);

            //$('#new-reg-ins-change').modal('show');

        } else if(newEmail != oldEmail) {
            //alert('email changed.');
            //this.createNewRegistration(component, event);
            action = component.get("c.registerMDataEmailChange");
            action.setParams({"registerDataObj": loggedInUser,
                              "newEmail" : newEmail});
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                //alert('ins chaanged state = ' + state);
                if(state == "SUCCESS")
                {
                    var str = response.getReturnValue();
                    $('#new-reg-email-change').modal('show');
                }
                else if (state == "ERROR") 
                {
                   var errors = response.getError();
                   alert(errors[0].message);
                   $A.util.addClass(cmpTarget, 'hide');
                   
                }
            }); 


            $A.util.addClass(cmpTarget, 'hide');
            ////$('#new-reg-email-change').modal('show');
            $A.enqueueAction(action);
            

        } 
        else {

            action = component.get("c.saveMyInfomation");
            action.setParams({"newCon": loggedInUser,
                              "newEmail": newEmail,
                              "newCity": newCity,
                              "newState": newState,
                              "newCountry": newCountry,
                              "newPostal": newPostal,
                              "newStreet": newStreet
                             });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();

                if(state == "SUCCESS")
                {
                    var str = response.getReturnValue();
                    location.reload();
                }
                else if (state == "ERROR") 
                {
                   var errors = response.getError();
                   alert(errors[0].message);
                   $A.util.addClass(cmpTarget, 'hide');
                   
                }
            }); 
            $A.enqueueAction(action);

        }
        
        ////$A.enqueueAction(action);
    },
    
    saveContactU : function(component, event) 
    {
        var cu = component.get("v.cu");
      
        if ($A.util.isEmpty(cu.New_Account__c) || $A.util.isUndefined(cu.New_Account__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(cu.Address__c) || $A.util.isUndefined(cu.Address__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(cu.State__c) || $A.util.isUndefined(cu.State__c)) {
            alert('Please fill required fields');
            return;
        }


        if ($A.util.isEmpty(cu.City__c) || $A.util.isUndefined(cu.City__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(cu.Country__c) || $A.util.isUndefined(cu.Country__c)) {
            alert('Please fill required fields');
            return;
        }

        if ($A.util.isEmpty(cu.Postal_Code__c) || $A.util.isUndefined(cu.Postal_Code__c)) {
            alert('Please fill required fields');
            return;
        }

        var action = component.get("c.createContactUpdate");
        action.setParams
        ({
            "AcctName": component.get("v.cu").New_Account__c,
            "StreetAddress": component.get("v.cu").Address__c,  
            "City": component.get("v.cu").City__c, 
            "State": component.get("v.cu").State__c,
            "Country": component.get("v.cu").Country__c, 
            "PostalCode": component.get("v.cu").Postal_Code__c, 
            "Phone": component.get("v.cu").Telephone__c    
         });
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var str = response.getReturnValue();
                $('#register-1').modal('hide');
            }
            else if (state == "ERROR") 
            {
               $('#register-1').modal('hide');
               var errors = response.getError();
            }
        });  
        $A.enqueueAction(action);
    },

    setSalutation: function(component, event) 
    {
         var selectCmp = component.find("Salutation");
         component.set("v.loggedInUser.Salutation", selectCmp.get("v.value"));
    },
    setLanguage: function(component, event) 
    {
         var selectCmp = component.find("Language");
         component.set("v.loggedInUser.Preferred_Language1__c", selectCmp.get("v.value"));
         //alert(selectCmp.get("v.value"));
         //component.set("v.loggedInUser.Preferred_Language1__c", selectCmp.get("v.value"));
    },
    setSpeciality: function(component, event) 
    {
         var selectCmp = component.find("Specialty");
         component.set("v.loggedInUser.Specialty__c", selectCmp.get("v.value"));
    },
    setRole: function(component, event) 
    {
         var selectCmp = component.find("Role");
         component.set("v.loggedInUser.Functional_Role__c", selectCmp.get("v.value"));
    },
    setCountry: function(component, event) 
    {
         var selectCmp = component.find("CountryId");
         component.set("v.loggedInUser.MailingCountry", selectCmp.get("v.value"));
    },
    setCountry1: function(component, event) 
    {
         var selectCmp = component.find("Country1");
         component.set("v.cu.Country__c", selectCmp.get("v.value"));
    },
    setQuestion: function(component, event) 
    {
         var selectCmp = component.find("RecoveryQuestion");
         component.set("v.loggedInUser.Recovery_Question__c", selectCmp.get("v.value"));
    },
    saveOptions : function(component, event) 
    {
        var hobbies = document.getElementsByName("myFavorites");  
        var value;
        var selectNum = 0;
        for (var i=0; i< hobbies.length; i++)
        {  
            if (hobbies[i].checked)
            {
                selectNum += 1;
                if (!value)
                {  
                    value = hobbies[i].value;
                } 
                else 
                {  
                    value += ";" + hobbies[i].value;
                } 
            }
        }
        if(selectNum > 5)
        {
            component.set("v.myFavError",true);
            return;
        }
        var action = component.get("c.saveMyFavorite");
        action.setParams({"myFavoriteStr":value});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS")
            {
              var str = response.getReturnValue();
            }
            else if (state == "ERROR") 
            {
               var errors = response.getError();
            }
            location.reload();
        }); 
        $A.enqueueAction(action);
    },
    
    verifypasswrd: function(component, event) 
    {
        var Exp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
        var pwd = component.find("newPassword").get("v.value");
       if(!Exp.test(pwd) || pwd.length < 8)
       {
           alert('Password must contain at least one uppercase letter, one lowercase letter, one number,and 8 characters .');
           return false;
       }
    },
    
    
    confirmpass: function(component, event) 
    {
        var oldpwd = component.find("newPassword").get("v.value");
        var newpwd = component.find("confirmPassword").get("v.value");
        if(oldpwd != newpwd)
        {
            document.getElementById('confirmpass').style.display = 'block';
        }
        else
        {
            document.getElementById('confirmpass').style.display = 'none';
        }
     },
    
    cancelreset: function(component, event) 
    {
        document.getElementById('newPassword').value ="";
        document.getElementById('confirmPassword').value ="";
        document.getElementById('currentPassword').value = "";
        document.getElementById('currentPassword1').value = "";
        document.getElementById('recoveryAnswer').value = "";
    },
    
    /*////UB: Commented 19 April
    instDialog: function(component, event) 
    {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },

    
    popupPhotoWindow: function(component, event) 
    {
        var cmpTarget = component.find('photoDialog');
        var cmpBack = component.find('photobackdrop');
        $A.util.addClass(cmpTarget,'slds-fade-in-open'); 
        $A.util.removeClass(cmpBack, 'slds-hide'); 
    },

    
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    */
    
    fnPONsubs: function(component, event) 
    {
        var action = component.get("c.subscribe");
       
        action.setCallback(this, function(response) {
            var state = response.getState();
            var userObj = component.get("v.subscribedUser");
            userObj.Subscribed_Products__c = true;
            component.set("v.subscribedUser",userObj);
            if(state == "SUCCESS")
            {
                var str = response.getReturnValue();
                
            }
            else if (state == "ERROR") 
            {
                var errors = response.getError();
            }
        }); 
        $A.enqueueAction(action);
    },
    
    fnPONUNsubs: function(component, event) 
    {
        var action = component.get("c.unSubscribe");
        
      
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            var userObj = component.get("v.subscribedUser");
                userObj.Subscribed_Products__c = false;
                component.set("v.subscribedUser",userObj);
            if(state == "SUCCESS"){
                var str = response.getReturnValue();
                
            }
            else if (state == "ERROR") 
            {
                
                var errors = response.getError();
            }

        }); 
        $A.enqueueAction(action);
    },
    
    passvaluechk: function(component, event) {
        //var newpwd = component.find("newPassword").get("v.value");
        //var confrmpwd = component.find("confirmPassword").get("v.value");
        //var oldpwd = component.find("currentPassword").get("v.value");
        var newpwd =  document.getElementById('newPassword').value;
        var confrmpwd =  document.getElementById('confirmPassword').value;
        var oldpwd =  document.getElementById('currentPassword').value;

        document.getElementById('CurrPas').style.display = 'none';
        
        var cmpTarget = component.find('newPassLi');
        $A.util.addClass(cmpTarget, 'hide');
        
        var cmpTarget = component.find('confirmpassLi');
        $A.util.addClass(cmpTarget, 'hide');
        
        if(oldpwd === undefined || oldpwd == null || oldpwd.length <= 0)
        {
            document.getElementById('CurrPas').style.display = 'block';
            return false;
        }
        else if(newpwd === undefined || newpwd == null || newpwd.length <= 0)
        {
            var cmpTarget = component.find('newPassLi');
            $A.util.removeClass(cmpTarget, 'hide');
            return false;
        }
        else if(confrmpwd === undefined || confrmpwd == null || confrmpwd.length <= 0)
        {
            var cmpTarget = component.find('confirmpassLi');
            $A.util.removeClass(cmpTarget, 'hide');
            return false;
        }
 
        if(newpwd == oldpwd)
        {
            alert('New Password and Current Password Can\'t be same');
            return false;
        }

        else if (newpwd != confrmpwd)
        {
            alert('The passwords provided do not match.');
            return false;
        }
       
        else 
        {
            var Exp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
                
            if(!Exp.test(newpwd) || newpwd.length < 8)
            {
                alert('Password must contain at least one uppercase letter, one lowercase letter, one number,and 8 characters .');
                return false;
            }
            else
            {
                var cmpTarget = component.find('spinnerId');
                $A.util.removeClass(cmpTarget, 'hide');

                var action = component.get("c.Resetpass");
                action.setParams
                ({
                    "Newpass": newpwd,
                    "ConfirmPass": confrmpwd,
                    "OldPassword": oldpwd,
                });
                action.setCallback(this, function(response) 
                {
                    var state = response.getState();
                    if(state == "SUCCESS")
                    {
                        var str = response.getReturnValue();
                        if(str.includes("Failure"))
                        {
                            alert(str);
                            $A.util.addClass(cmpTarget, 'hide');
                            return false;
                        }
                        else
                        {
                            alert('Password has been changed successfully.');
                            location.reload();
                        }
                    }
                    else if (state == "ERROR") 
                    {
                       var errors = response.getError();
                       alert(errors);
                    }
                    //location.reload();
                });  
                $A.enqueueAction(action);
            }
        }
    },
    
    recoveryvaluechk: function(component, event) {
        var currentpwd =  document.getElementById('currentPassword1').value;;
        var RecoveryA = document.getElementById('recoveryAnswer').value;
        var RecoveryQ = component.find("RecoveryQuestion").get("v.value");
       
        if(currentpwd == '' )
        {
            document.getElementById('CurrPass1').style.display = 'block';
        }
        else if(currentpwd != '') 
        {
            if(RecoveryQ != '')
            {
                if(RecoveryA != '')
                {
                    if(RecoveryA.length < 4)
                    {
                        alert('The recover answer must contain at least 4 characters.');
                        return false;
                    }
                    else
                    {
                       
                        var cmpTarget = component.find('spinnerId');
                        $A.util.removeClass(cmpTarget, 'hide');

                        var action = component.get("c.ResetRcvryQus");
                        action.setParams
                        ({
                            "Curntpass": currentpwd,
                            "RecoveryQ": RecoveryQ,
                            "RecoveryA": RecoveryA,
                        });
                        action.setCallback(this, function(response) 
                        {
                            var state = response.getState();
                                               
                            if(state == "SUCCESS")
                            {
                               var str = response.getReturnValue();
                                if(str.includes("Failure"))
                                {
                                    alert(str);
                                    $A.util.addClass(cmpTarget, 'hide');
                                    return false;
                                }
                                else
                                {
                                    alert('Reset security question has been changed successfully.');
                                    location.reload();
                                }
                                
                            }
                            else if (state == "ERROR") 
                            {
                                var errors = response.getError();
                            }
                            location.reload();
                        }); 
                        $A.enqueueAction(action);
                        
                        document.getElementById('edit-profile-currentPassword').value = '';
                        document.getElementById('edit-profile-RecoveryQ').value = '';
                        document.getElementById('edit-profile-RecovryAns').value = '';
                        document.getElementById('CurrPass').style.display = 'none';
                        // function for resetting recovery question 
                    }
                }else
                {
                    alert('Please Enter Recovery Answer!!');
                    return false;
                }
            }
            else {
                  alert('Enter Recovery Question');
                  return false;
                }
             }
          
    },    
    
    /* Load Remote Service Tools tab */
    getRemoteServiceTools : function(component, event) {
        var actionNew = component.get("c.initializeCRA");
        //action.setParams({"languageObj":component.get("v.language")});
        actionNew.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var lstWCRA ;
                var wCRAWrapper = response.getReturnValue();
                //alert(wCRAWrapper.isIConsent);
                if(!$A.util.isEmpty(wCRAWrapper)){
                    lstWCRA = wCRAWrapper.lstWCRA;
                    component.set("v.isIConsent",wCRAWrapper.isIConsent);
                    component.set("v.ConsentContent",wCRAWrapper.ConsentContentText);
                    component.set("v.lstWCRA",lstWCRA);
                }
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        });
        
        $A.enqueueAction(actionNew);
    },
    
    saveAuthorization : function(component, event) {
        var action = component.get("c.iConsentClick");
        action.setParams({"lstWCRA":JSON.stringify(component.get("v.lstWCRA"))});
        action.setCallback(this, function(response) {
            var state = response.getState();
            // alert(JSON.stringify(component.get("v.lstWCRA")));
            if (component.isValid() && state === "SUCCESS") {
                //alert(response.getReturnValue());
                //component.set("v.lstWCRA",response.getReturnValue());
                this.getRemoteServiceTools(component, event);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    /*////UB: Commented 19 April
    uploadImage : function(component, event) {
        debugger;
        var fileInput = component.find("userImgFile").getElement();
        var file = fileInput.files[0];
        if (file.size > this.MAX_FILE_SIZE) {
            alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
                  'Selected file size: ' + file.size);
            return;
        }
        var fr = new FileReader();
        var self = this;
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.uploadHandler(component, file, fileContents);
        };
 
        fr.readAsDataURL(file);
    },
    */
        
    uploadHandler: function(component, file, fileContents) {
        var action = component.get("c.saveTheImageUserFile"); 
        action.setParams({
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type
        });
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            console.log(attachId);
        });
        //$A.run(function() {
            $A.enqueueAction(action); 
        //});
    },
    
    goHome:function(component, event){
         url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    doRemoveCRA: function(component, event) {
        var craId = event.currentTarget.getAttribute("data-craid");
        var action = component.get("c.removeCRA"); 
        action.setParams({"craIDObj" : craId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                 this.getInstitution(component, event);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action); 
    }
})
({
	doInit : function(component, event, helper) { 
        helper.checkIsExternalUser(component,event);
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getNonVarianUser(component, event);
        helper.getNewIdeaProductGroupValues(component,event);
        helper.getIdeaForEdit(component,event);
    },
    
    handlePostIdeaClick : function(component, event, helper){ 
        helper.saveProductIdeaWithFile(component, event);
    },
    
    productIdeas: function(component, event, helper){ 
        var url = "/productideas?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire(); 
	},
    
    setSelectedLookupRecord : function(component, event, helper){
        var sfId = event.getParam("recordId");
        var sfName = event.getParam("recordName");
        var nameField = event.getParam("recordNameField");
        var idField = event.getParam("recordIdField");
        helper.toggleClass(component, false, "customLookupPopUp");
        component.find(nameField).set("v.value",sfName);
        component.find(idField).set("v.value",sfId);
        console.log('-----sfId'+sfId+'---sfName'+sfName+'----recordNameField'+nameField+'----recordIdField'+idField);
        component.set("v.selectedLookupSearchKey", "");
    },
    
    home: function(component, event, helper){ 
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire(); 
	},
    
    uploadFile:function(component, event, helper) {
        helper.doUploadFile(component,event);
    },
})
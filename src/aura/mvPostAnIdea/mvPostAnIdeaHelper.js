({
	

    getUrlParameter : function(component, event) {
        
        var getUrlParameter = function getUrlParameter(sParam,fParam,ideaId) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    data.lang = sParameterName[1] === undefined ? true : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.ext = sParameterName[1] === 'true' ? true : false;
                }
                else if (sParameterName[0] === ideaId){
                    data.ideaId = sParameterName[1];
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('lang','ext','Id');
        component.set("v.isExternalUserParam",bdata.ext);
        component.set("v.ideaId",bdata.ideaId);
        //alert(bdata.ideaId);
		component.set("v.language",bdata.lang);
    },

    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    getIdeaForEdit : function(component, event) {
        var action = component.get("c.getIdea");
        action.setParams({"hdnIdeaId":component.get("v.ideaId")});
        action.setCallback(this, function(response) {
            debugger;
            var selItem2;
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //console.log(JSON.stringify(response.getReturnValue()));
                try {
                    var idea = response.getReturnValue();
                    selItem2 = idea.Contact__r;
                    selItem2.name = idea.Contact__r.Name;
                    selItem2.val = idea.Contact__c;
                    component.set("v.selItem2",selItem2);
                    component.set("v.newIdea",idea);
                }catch(err) {
                   console.log(err.message);
                }
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    
    checkIsExternalUser : function(component,event){
        var action = component.get("c.checkIsExternalUser");
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var isExternalUser = response.getReturnValue();
                component.set("v.isExternalUser",isExternalUser);
                if(! isExternalUser){
                    component.set("v.isExternalUser",isExternalUser);
                    $A.util.removeClass(component.find("divContact"),"slds-hide");
                }
            }
        });	
        $A.enqueueAction(action);
    },
    
    
    getNewIdeaProductGroupValues : function(component,event){
        var action = component.get("c.getNewIdeaProductGroup");
        var options = [];
        options.push({
            label : '',
            value : ''
        },
        {
            label : 'All',
            value : 'All'
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                    

                    
                }                
            }
            component.find("ProductGroupIdNew").set("v.options",options);
        }); 
        $A.enqueueAction(action);
    },
    
    //   MAX_FILE_SIZE: 750 000, /* 1 000 000 * 3/4 to account for base64 */
    saveProductIdeaWithFile : function(component, event){ 
        debugger;
        var errorFlag = false;
        var prodGrp = component.find("ProductGroupIdNew").get("v.value");
        //component.find("contactInputField").set("v.value",component.get("v.selItem2"));
        //console.log("value"+value.name);
        var titleId = component.find("titleId").get("v.value");
        var describeId = component.find("describeId").get("v.value");
        
        if ($A.util.isEmpty(prodGrp) || $A.util.isUndefined(prodGrp)) {
            $A.util.addClass(component.find("ProductGroupIdNew"), 'errorClass');
            errorFlag = true;
        } else {
            $A.util.removeClass(component.find("ProductGroupIdNew"), 'errorClass');
        }

        if ($A.util.isEmpty(titleId) || $A.util.isUndefined(titleId)) {
            $A.util.addClass(component.find("titleId"), 'errorClass');
            errorFlag = true;
        } else {
            $A.util.removeClass(component.find("titleId"), 'errorClass');
        }

        if ($A.util.isEmpty(describeId) || $A.util.isUndefined(describeId)) {
            $A.util.addClass(component.find("describeId"), 'errorClass');
            errorFlag = true;
        } else {
             $A.util.removeClass(component.find("describeId"), 'errorClass');   
        }

        if(errorFlag == true) {
            return;
        }

        var self = this;
        var fileInput = component.find("file").getElement();
        if(fileInput.files.length > 0){ 
            var file = fileInput.files[0];
            var fileContents = component.get('v.fileContents');
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            self.saveProductIdea(component,event,file,fileContents,true); 
        }else{
            self.saveProductIdea(component,event,"","",false);
        }
    },

    doUploadFile : function(component, event){ 
         var fileInput = component.find("file").getElement();
         if(fileInput.files.length > 0){
            var file = fileInput.files[0];
            var fr = new FileReader();
            fr.onload = function() {
                var fileContents = fr.result;
                component.set('v.fileContents',fileContents);
            };
            fr.readAsDataURL(file);
         }
    },

    saveProductIdea : function(component,event,file,fileContents,isFile){
        var newIdeaObj = component.get("v.newIdea");
        var fileName ="";
        var base64Data = "";
        var contentType = "";
        debugger;
        if(isFile){
            //debugger;
            base64Data = encodeURIComponent(fileContents);
            fileName  =  file.name;
            contentType = file.type
        }
        var action = component.get("c.saveProductIdea");
        var contactObj = component.get('v.selItem2');
        
        if(contactObj != undefined && contactObj.val != undefined && contactObj.val != null)
        	newIdeaObj.Contact__c = contactObj.val;

        action.setParams({"newIdeaObj":newIdeaObj,
                          fileName: file.name,
                          base64Data: base64Data,
                          contentType: contentType
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
            debugger;
            if(state == "SUCCESS"){
                var ideaID = response.getReturnValue();
                // alert(ideaID);
                if(!$A.util.isEmpty(ideaID)){
                   
                }
                
                
                var url = "/productideas?lang="+component.get('v.language');
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": url
                });
                urlEvent.fire();
                
                
                
            }            
        });	
        $A.enqueueAction(action); 
    },
    toggleClass: function(component,showModal, modalName) {
        console.log('---in toggle class');
        var modalComponent = component.find(modalName);
        
        if(showModal){
            $A.util.removeClass(modalComponent,'LockOff');
            $A.util.addClass(modalComponent,'LockOn');
        }else{
            $A.util.removeClass(modalComponent,'LockOn');
            $A.util.addClass(modalComponent,'LockOff');
        }
    },
    
    getSFContactDetails : function(component, contactRecordId){
        console.log('----getSFContactDetails');
        var getAccountDetails = component.get("c.getAccount");
        getAccountDetails.setParams({
            "contactId":contactRecordId
        });
        getAccountDetails.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' === state === ' + state);
            if(state == "SUCCESS" && component.isValid()) {
                console.log(' === inside state === ' + response.getReturnValue());
                var contactObject = response.getReturnValue();
                //this.updateContactFields(component, contactObject);
            } else if( component.isValid() && state == "ERROR") {
                var errorFooterBtn = component.find("errorFooterBtn");
                $A.util.addClass(errorFooterBtn, 'slds-show');
                $A.util.removeClass(errorFooterBtn, 'slds-hide');
                var erroFooterSectn = component.find("erroFooterSectn");
                $A.util.addClass(erroFooterSectn, 'slds-show');
                $A.util.removeClass(erroFooterSectn, 'slds-hide');
                component.set("v.errors", response.getError()[0].message);
            }
        });
        $A.enqueueAction(getAccountDetails);
    },
   
   
    /* Get Non Varian User */
    getNonVarianUser: function(component, event) {
        var getUser = component.get("c.checkNonVarianUser");
        getUser.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isNonVarianUser", response.getReturnValue());
            } else if (response.getState() == "ERROR") {
                $A.log("callback error", response.getError());
            }
        });
        $A.enqueueAction(getUser);
    },  
})
({
    doInit : function(component, event, helper) { 

    	helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);

        var ideaId = component.get("v.commentIdeaId");  // "0870n00000002eC";
        var action = component.get("c.getIdeaComments");   
        action.setParams({"hdnIdeaId": ideaId});
        action.setCallback(this, function(response) {       
            var state = response.getState();
            if(state == "SUCCESS"){
                var lstwIdeaComments = response.getReturnValue();
                component.set("v.lstwIdeaComments",lstwIdeaComments);
            }            
        });	
        $A.enqueueAction(action);         
        
    },
    
    onApply : function(component, event, helper){
        helper.getProductIdeaList(component,event);
    },
    onChangeMyIdeas : function(component, event, helper){
        helper.getProductIdeaList(component,event);      
    },
    
    onReset : function(component, event, helper){
        component.find("SortById").set("v.value","Recent");
        component.find("ProductGroupId").set("v.value","All");
        component.find("VersionId").set("v.value","All");
        component.find("refineKeyword").set("v.value","");
        component.find("ViewmyIdeas").set("v.value",false);
        helper.getProductIdeaList(component,event);
    },
    toggleTermsDialog : function(component, event, helper) {
        helper.toggleTermsDialog(component, event);       
    },
    dialogContinueClick : function(component, event, helper) {

        helper.checkIsExternalUser(component,event);

        //alert('isExternalUserParam passing = ' + component.get('v.isExternalUserParam'));


        $("#post-an-idea").modal('hide');
        //var url = "/post-an-idea?lang="+component.get('v.language')+"&ext="+component.get('v.isExternalUserParam');
        var url = "/post-an-idea?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire(); 
    },
    backToIdeas : function(component, event, helper) {
        helper.toggleIdeasDiv(component, event);
        helper.resetNewIdea(component,event);
    },
    handlePostIdeaClick : function(component, event, helper){ 
       // helper.saveProductIdea(component, event);
        helper.saveProductIdeaWithFile(component, event);
        
    },
    handleLikeClick : function(component, event, helper){ 
       helper.saveVote(component, event);
    },
    handleCommentsClick : function(component, event, helper) {
        //helper.toggleDivIdeasComment(component, event);
        helper.loadCommentSection(component, event);
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/product-idea-comment?lang="+component.get('v.language'),
            isredirect: "true"
        });
        urlEvent.fire();*/
        
    },
    handleCommentSaveClick : function(component, event, helper){ 
        debugger;
        helper.saveIdeaComment(component, event);
        //helper.toggleDivIdeasComment(component, event);
    },
    backToIdeasFromComment : function(component, event, helper) {
        helper.toggleDivIdeasComment(component, event);
        helper.resetCommentSection(component, event);
    },
    showMoreDescription : function(component, event, helper){ 
        var ideaId = event.currentTarget.id;
        //alert(ideaId);
        component.set("v.readMoreIdeaId",ideaId);
    },
    handleFilesChange : function(component, event, helper){ 
       // helper.handleFilesChange(component, event);
    },
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getProductIdeaList(component,event,true);
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    goToIdeas: function(component, event, helper) {
        var url = "/productideas?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },    
})
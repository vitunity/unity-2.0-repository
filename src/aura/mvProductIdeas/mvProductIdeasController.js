({
    doInit : function(component, event, helper) { 
        helper.getUrlParameter(component, event);
        helper.getCustomLabels(component, event);
        helper.getIdeaSortByValues(component,event);
        helper.getIdeaProductGroupValues(component,event);
        helper.getIdeaStatusValues(component,event);
        helper.getNewIdeaProductGroupValues(component,event);
        helper.getProductIdeaList(component,event);
        //alert('in init of productideas 1');
        helper.checkIsExternalUser(component,event);
        //alert('in init of productideas 2');
    },
    
    onApply : function(component, event, helper){
        helper.getProductIdeaList(component,event);
    },
    onChangeMyIdeas : function(component, event, helper){
        helper.getProductIdeaList(component,event);      
    },
    
    onReset : function(component, event, helper){
        component.find("SortById").set("v.value","Recent");
        component.find("ProductGroupId").set("v.value","All");
        component.find("VersionId").set("v.value","All");
        component.find("refineKeyword").set("v.value","");
        component.find("ViewmyIdeas").set("v.value",false);
        helper.getProductIdeaList(component,event);
    },
    toggleTermsDialog : function(component, event, helper) {
        helper.toggleTermsDialog(component, event);       
    },
    dialogContinueClick : function(component, event, helper) {

        helper.checkIsExternalUser(component,event);

        //alert('isExternalUserParam passing = ' + component.get('v.isExternalUserParam'));


        $("#post-an-idea").modal('hide');
        //var url = "/post-an-idea?lang="+component.get('v.language')+"&ext="+component.get('v.isExternalUserParam');
        var url = "/post-an-idea?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire(); 
    },
    backToIdeas : function(component, event, helper) {
        helper.toggleIdeasDiv(component, event);
        helper.resetNewIdea(component,event);
    },
    handlePostIdeaClick : function(component, event, helper){ 
       // helper.saveProductIdea(component, event);
        helper.saveProductIdeaWithFile(component, event);
        
    },
    handleLikeClick : function(component, event, helper){ 
       helper.saveVote(component, event);
       helper.getProductIdeaList(component,event);
    },
    handleCommentsClick : function(component, event, helper) {

        var index = event.currentTarget.id;
        var IdeaId;
        var upVote;
        var Type;
        var idea;
        var lstwProductIdea;
        
        if(index){
            lstwProductIdea = component.get("v.lstwProductIdea");
            if(!$A.util.isEmpty(lstwProductIdea)){
                idea = lstwProductIdea[index];
                if(idea){
                    IdeaId = idea.ideaObj.Id;
                    upVote = idea.upVote;
                }
            }
        }

        //helper.toggleDivIdeasComment(component, event);
        //helper.loadCommentSection(component, event);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/product-idea-comment?lang="+component.get('v.language')+"&ideaIdParam="+IdeaId,
            isredirect: "true"
        });
        urlEvent.fire();
        
    },
    
    handleIdeaEdit : function(component, event, helper) {
        var index = event.currentTarget.id;
        var IdeaId;
        var idea;
        var lstwProductIdea;
        
        if(index){
            lstwProductIdea = component.get("v.lstwProductIdea");
            if(!$A.util.isEmpty(lstwProductIdea)){
                idea = lstwProductIdea[index];
                if(idea){
                    IdeaId = idea.ideaObj.Id;
                }
            }
        }
        var url = "/post-an-idea?lang="+component.get('v.language')+'&Id='+IdeaId;
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
        
    },
    
    handleCommentSaveClick : function(component, event, helper){ 
        debugger;
        helper.saveIdeaComment(component, event);
        //helper.toggleDivIdeasComment(component, event);
    },
    backToIdeasFromComment : function(component, event, helper) {
        helper.toggleDivIdeasComment(component, event);
        helper.resetCommentSection(component, event);
    },
    showMoreDescription : function(component, event, helper){ 
        var ideaId = event.currentTarget.id;
        //alert(ideaId);
        component.set("v.readMoreIdeaId",ideaId);
    },
    handleFilesChange : function(component, event, helper){ 
       // helper.handleFilesChange(component, event);
    },
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getProductIdeaList(component,event,true);
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
})
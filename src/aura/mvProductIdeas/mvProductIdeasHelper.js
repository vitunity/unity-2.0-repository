({
    
    /* get url parameter */
    /*
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };
        var lang =  getUrlParameter('lang');
        component.set("v.language",lang);
    },
    */
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam,fParam) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] === sParam) {
                    data.lang = sParameterName[1] === undefined ? true : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.searchText = sParameterName[1];
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('lang','searchText');
        component.set("v.language",bdata.lang);
        component.find("refineKeyword").set("v.value", bdata.searchText);
    },

    /* Load custome labels */
    getCustomLabels : function(component, event) {
        var action = component.get("c.getCustomLabelMap");
        action.setParams({"languageObj":component.get("v.language")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.customLabelMap",response.getReturnValue());
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    checkIsExternalUser : function(component,event){

        //alert('in checkIsExternalUser of productideas');
        var action = component.get("c.checkIsExternalUser");
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var isExternalUser = response.getReturnValue();
                //alert('isExternalUser found finally 1 = ' + isExternalUser);
                
                component.set("v.isExternalUserParam",isExternalUser);

                //alert('isExternalUser found finally 2 = ' + component.get('v.isExternalUserParam'));

                
                if(! isExternalUser){
                    component.set("v.isExternalUser",isExternalUser);
                    $A.util.removeClass(component.find("divContact"),"slds-hide");
                }
            }
        });	
        $A.enqueueAction(action);
    },
    getIdeaSortByValues : function(component,event){
        var action = component.get("c.getIdeaSortBy");
        var options = [];
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("SortById").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    getIdeaProductGroupValues : function(component,event){
        var action = component.get("c.getIdeaProductGroup");
        
        var options = [];
        options.push({
            label : 'All',
            value : 'All'
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("ProductGroupId").set("v.options",options);
            this.getProductIdeaList(component,event);
        });	
        $A.enqueueAction(action);
    },
    getIdeaStatusValues : function(component,event){
        var action = component.get("c.getIdeaStatus");
        
        var options = [];
        options.push({
            label : 'All',
            value : 'All'
        });
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("VersionId").set("v.options",options);
        });	
        $A.enqueueAction(action);
    },
    
    getProductIdeaList : function(component,event,resetPages){        

        if(! resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        
        var action = component.get("c.getProductIdea");
        var sortby = component.find("SortById").get("v.value");
        if ($A.util.isEmpty(sortby) || $A.util.isUndefined(sortby)) {
            sortby = 'Recent';
        }
        
        action.setParams({"sortBy":sortby,
                          "ideaPG":component.find("ProductGroupId").get("v.value"),
                          "ideaStatus":component.find("VersionId").get("v.value"),                          
                          "Idea_Search":component.find("refineKeyword").get("v.value"),
                          "viewMyIdeas":component.find("ViewmyIdeas").get("v.value"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "pageSize":component.get("v.pageSize")
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
            if(state == "SUCCESS"){
                var ProductIdeaWrap = response.getReturnValue();
                component.set("v.lstwProductIdea",ProductIdeaWrap.lstwProductIdea);
                if(ProductIdeaWrap.totalSize == 0){
                    component.set("v.totalSize",4);
                }else{
                    component.set("v.totalSize",ProductIdeaWrap.totalSize);
                }
            }            
        });	
        $A.enqueueAction(action); 
    },
    getNewIdeaProductGroupValues : function(component,event){
        var action = component.get("c.getNewIdeaProductGroup");
        
        var options = [];
        options.push({
            label : 'All',
            value : 'All'
        });
        /*
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var picklistresult = response.getReturnValue();
                for(var i =0;i<picklistresult.length;i++){
                    options.push({
                        label : picklistresult[i].label,
                        value : picklistresult[i].pvalue
                    });
                }                
            }
            component.find("ProductGroupIdNew").set("v.options",options);
        });	
        $A.enqueueAction(action);
        */
    },
    
    saveVote : function(component,event){
        var index = event.currentTarget.id;
        // alert(index);
        var IdeaId;
        var upVote;
        var Type;
        var idea;
        var lstwProductIdea;
        
        if(index){
            lstwProductIdea = component.get("v.lstwProductIdea");
            if(!$A.util.isEmpty(lstwProductIdea)){
                idea = lstwProductIdea[index];
                if(idea){
                    IdeaId = idea.ideaObj.Id;
                    upVote = idea.upVote;
                }
            }
        }
        
        if(!IdeaId || upVote){
            return ;
        }
        if(!upVote){
            Type = 'up' ;
        }else{
            //  Type = 'downVote' ; 
        }
        
        var action = component.get("c.makeVote");
        action.setParams({"IdeaId":IdeaId,
                          "Type":Type
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(state == "SUCCESS"){
                var status = response.getReturnValue(); 
                if(status){
                    lstwProductIdea[index].upVote = true;
                    component.set("v.lstwProductIdea", lstwProductIdea);
                }
            }            
        });	
        $A.enqueueAction(action); 
    },
    saveProductIdea : function(component,event,file,fileContents,isFile){
        var newIdeaObj = component.get("v.newIdea");
        var fileName ="";
        var base64Data = "";
        var contentType = "";
        if(isFile){
            base64Data = encodeURIComponent(fileContents);
            fileName  =  file.name;
            contentType = file.type
        }
        var action = component.get("c.saveProductIdea");
        
        action.setParams({"newIdeaObj":newIdeaObj,
                          fileName: file.name,
                          base64Data: base64Data,
                          contentType: contentType
                         });
        
        action.setCallback(this, function(response) {            
            var state = response.getState();   
            //alert(state);
            if(state == "SUCCESS"){
                var ideaID = response.getReturnValue();
                // alert(ideaID);
                if(!$A.util.isEmpty(ideaID)){
                    this.resetNewIdea(component,event);
                    this.toggleIdeasDiv(component, event);
                    this.getProductIdeaList(component,event);
                }
            }            
        });	
        $A.enqueueAction(action); 
    },
    saveIdeaComment : function(component,event){
        debugger;
        var action = component.get("c.addComment");        
        action.setParams({"ideaId":component.get("v.commentIdeaId"),
                          "commentBody":component.find("commentBodyId").get("v.value")
                         });
        debugger;
        action.setCallback(this, function(response) {            
            var state = response.getState();
             $('#post-comment').modal('hide'); 
            if(state == "SUCCESS"){
                var status = response.getReturnValue();
                if(!$A.util.isEmpty(status)){
                    //  component.find("commentBodyId").set("v.value","");
                    // this.getIdeaComments(component,event);
                    //this.resetCommentSection(component,event);
                    //this.toggleDivIdeasComment(component, event);
                    this.getProductIdeaList(component, event);
                }
            }            
        });	
        $A.enqueueAction(action); 
        debugger;
        /*var url = "/productideas?lang="+component.get('v.language');
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": url
                });
                urlEvent.fire(); */
                
        
        
    },
    
    toggleTermsDialog : function(component, event) {
        $A.util.toggleClass(component.find("divTermsDialog"), 'slds-hide');
        $A.util.toggleClass(component.find("divBackGroundGray"), 'slds-hide');       
    },
    
    toggleIdeasDiv : function(component, event) {
        $A.util.toggleClass(component.find("divNewIdea"), 'slds-hide');
        $A.util.toggleClass(component.find("divIdeas"), 'slds-hide');       
    },
    resetNewIdea : function(component, event) {
        if(!component.get("v.isExternalUser")){
            component.set("v.newIdea.Contact__c",""); 
            // component.find("contactId").set("v.value","");
            // component.find("contactId").set("v.address","");
            // component.find("contactId").set("v.nameValue","");
            
        }
        component.set("v.newIdea.Categories","");
        component.set("v.newIdea.Product_Version__c","");
        component.set("v.newIdea.Title","");
        component.set("v.newIdea.Body",""); 
        component.find("file").getElement().value= "";
    },
    resetCommentSection : function(component, event) {
        component.set("v.commentIdeaId","");
        component.set("v.commentTitle","");
        component.set("v.commentBody","");
        component.find("commentBodyId").set("v.value","");
    },
    toggleDivIdeasComment : function(component, event) {
        $A.util.toggleClass(component.find("divIdeasComment"), 'slds-hide');
        $A.util.toggleClass(component.find("divIdeas"), 'slds-hide');       
    },
    loadCommentSection : function(component,event){
        var index = event.currentTarget.id;
        var IdeaId;
        var idea;
        var lstwProductIdea;
        
        if(index){
            lstwProductIdea = component.get("v.lstwProductIdea");
            if(!$A.util.isEmpty(lstwProductIdea)){
                idea = lstwProductIdea[index];
                if(idea){
                    IdeaId = idea.ideaObj.Id;
                    component.set("v.commentIdeaId",IdeaId);
                    //component.set("v.commentTitle",idea.title);
                    //component.set("v.commentBody",idea.Description);
                }
            }
            this.getIdeaComments(component,event);
        }
    },
    getIdeaComments : function(component,event){
        var action = component.get("c.getIdeaComments");        
        action.setParams({"hdnIdeaId":component.get("v.commentIdeaId")});
        
        action.setCallback(this, function(response) {       
            var state = response.getState();
            if(state == "SUCCESS"){
                var lstwIdeaComments = response.getReturnValue();
                component.set("v.lstwIdeaComments",lstwIdeaComments);
            }            
        });	
        $A.enqueueAction(action); 
    },
    //   MAX_FILE_SIZE: 750 000, /* 1 000 000 * 3/4 to account for base64 */
    saveProductIdeaWithFile : function(component, event){ 
        var self = this;
        // var fileInput = event.getSource().get("v.files");
        
        var fileInput = component.find("file").getElement();
        // alert(fileInput.files.length);
        
        if(fileInput.files.length > 0){ 
            // if(!$A.util.isEmpty(fileInput.files)){ 
            // var file = fileInput[0];
            var file = fileInput.files[0];
            
            /*  if (file.size > this.MAX_FILE_SIZE) {
    alert('File size cannot exceed ' + this.MAX_FILE_SIZE + ' bytes.\n' +
    'Selected file size: ' + file.size);
    return;
        }*/
            
            var fr = new FileReader();
            fr.onload = function() {
                var fileContents = fr.result;
                var base64Mark = 'base64,';
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                
                fileContents = fileContents.substring(dataStart);
                // alert(fileContents);
                // self.upload(component, file, fileContents);
                self.saveProductIdea(component,event,file,fileContents,true);
            };
            
            fr.readAsDataURL(file);
        }else{
            self.saveProductIdea(component,event,"","",false);
        }
        
    }, 
    upload: function(component, file, fileContents) {
        var action = component.get("c.saveTheFile"); 
        
        action.setParams({
            fileName: file.name,
            base64Data: encodeURIComponent(fileContents), 
            contentType: file.type
        });
        
        action.setCallback(this, function(a) {
            attachId = a.getReturnValue();
            console.log(attachId);
        });
        
        $A.run(function() {
            $A.enqueueAction(action); 
        });
    },
    
})
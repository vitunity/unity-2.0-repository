({
	doInit : function(component, event, helper) {
        
        helper.getFilterPlanStage(component);
		helper.getFilterPriorities(component); 
        //helper.getDosiometristInfo(component,event,"load");
		helper.getTPassList(component,event,true);
                
        var action = component.get("c.getDosiometristInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
				component.set("v.IsDosiometrist",response.getReturnValue());                 
                if(response.getReturnValue())
                {
                    window.setInterval(
                        $A.getCallback(function() {
                            console.log('calling....');
                            helper.getTPassList(component,event,true)
                        }),
                        10000
                    );
                }
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
	},
    
    handleStatusClick : function(component, event, helper){
        var link = event.currentTarget.id;
        component.set("v.TPaasStatusLink",link);
		helper.getFilterPlanStage(component);
        helper.getTPassList(component,event,true);        
    },
    onTPaasApply : function(component, event, helper){
        helper.getTPassList(component,event,true);
    },
    refreshComment : function(component, event, helper){
        helper.RefreshTPassComments(component,event,component.get("v.packageId"));
    },
    openTPassDetails : function(component, event,helper) {
        var index = event.currentTarget.id;
 
		//var url = "/treatmentplanningservicesdetail?lang="+component.get('v.language')+"&id="+index;
        var url = "/treatmentplanningservicesdetail?lang="+component.get('v.language')+"&id="+index;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
	onCollapse : function(component, event,helper) {
        var tpassid = event.currentTarget.id;
		console.log('tpassid:'+tpassid);
		console.log('tpassid:'+tpassid+'::'+document.getElementById(tpassid+'plus'));
		document.getElementById(tpassid+'plus').style.display = 'none';
		document.getElementById(tpassid+'minus').style.display = '';
		
		var matches = document.getElementsByClassName(tpassid+'detail');

		for (var i=0; i<matches.length; i++) {
		  matches[i].style.display = '';
		}
    },
	onExpand : function(component, event,helper) {
        var tpassid = event.currentTarget.id;
		document.getElementById(tpassid+'plus').style.display = '';
		document.getElementById(tpassid+'minus').style.display = 'none';
		var matches = document.getElementsByClassName(tpassid+'detail');

		for (var i=0; i<matches.length; i++) {
			matches[i].style.display = 'none';
		}
    },
	
	
	onRadioClick : function(component, event,helper) {
		var tpassid = event.currentTarget.id;
        
		document.getElementById("recreateplanrequestdiv2").style.display = 'none';
		component.set("v.SelectedTpass","");
		var matches = document.getElementsByClassName('checkbx');

		for (var i=0; i<matches.length; i++) {
			if(matches[i].id == tpassid && matches[i].checked) {
				document.getElementById("recreateplanrequestdiv2").style.display = '';
				component.set("v.SelectedTpass",tpassid);
			}
			if(matches[i].id != tpassid){
				matches[i].checked=false;
			}
			console.log(matches[i].id +':'+ tpassid+':'+matches[i].checked);
		}
    },
	
	onCreateOrder : function(component, event,helper) {
        helper.createTpassDetail(component,'null');
		component.set("v.tpassheaderval",$A.get("$Label.c.MV_PlanRequest"));
		component.find("tpasspatient").set("v.disabled",false);
		component.find("tpassclientname").set("v.disabled",false);
        component.find("tpasssite").set("v.disabled",false);
    },
	
	
	
	onCreateRecreateOrder : function(component, event,helper) {
        helper.createTpassDetail(component,component.get('v.SelectedTpass'));
		component.set("v.tpassheaderval",$A.get("$Label.c.MV_RePlanRequest"));
		component.find("tpasspatient").set("v.disabled",true);
		component.find("tpassclientname").set("v.disabled",true);
        component.find("tpasssite").set("v.disabled",true);
		
    },
	
	onSaveTpassDetail : function(component, event,helper) {
        helper.saveTpassDetail(component,event);
    },
    openCommentpopup : function(component, event, helper) {
        helper.showPopupHelper(component, 'newmodaldialog', 'slds-fade-in-');
		helper.showPopupHelper(component,'backdrop','slds-backdrop--');
    },
    onPriorityChange : function(component, event, helper) {
         component.set("v.inputpriority",component.find("TPassPriorityText").get("v.value")); 
         document.getElementById('priorityoutput').style.display = 'none';
         document.getElementById('priorityinput').style.display = '';
    },
    onPrioritySave : function(component, event, helper) {
         document.getElementById('priorityoutput').style.display = '';
         document.getElementById('priorityinput').style.display = 'none';
         helper.TPassPrioritySave(component,event,component.find("TPassPriority").get("v.value"),component.find("TPassPlanStageText").get("v.value"),component.find("TPassPlanStateText").get("v.value"),component.find("TPassDosiometristText").get("v.value"));
    },
    
    onPlanStageChange : function(component, event, helper) {
         component.set("v.inputPlanStage",component.find("TPassPlanStageText").get("v.value")); 
         document.getElementById('PlanStageoutput').style.display = 'none';
         document.getElementById('PlanStageinput').style.display = '';
    },
    onPlanStageSave : function(component, event, helper) {
         document.getElementById('PlanStageoutput').style.display = '';
         document.getElementById('PlanStageinput').style.display = 'none';
         helper.TPassPrioritySave(component,event,component.find("TPassPriorityText").get("v.value"),component.find("TPassPlanStage").get("v.value"),component.find("TPassPlanStateText").get("v.value"),component.find("TPassDosiometristText").get("v.value"));
    },
		
	
	onPlanStateChange : function(component, event, helper) {
         component.set("v.inputPlanState",component.find("TPassPlanStateText").get("v.value")); 
         document.getElementById('PlanStateoutput').style.display = 'none';
         document.getElementById('PlanStateinput').style.display = '';
    },
    onPlanStateSave : function(component, event, helper) {
         document.getElementById('PlanStateoutput').style.display = '';
         document.getElementById('PlanStateinput').style.display = 'none';
         helper.TPassPrioritySave(component,event,component.find("TPassPriorityText").get("v.value"),component.find("TPassPlanStageText").get("v.value"),component.find("TPassPlanState").get("v.value"),component.find("TPassDosiometristText").get("v.value"));
    },
    
	onDosiometristChange : function(component, event, helper) {  
         
         component.set("v.TPassDosiometrist",component.find("TPassDosiometristText").get("v.value")); 
         document.getElementById('Dosiometristoutput').style.display = 'none';
         document.getElementById('Dosiometristinput').style.display = '';
    },	
	onDosiometristSave : function(component, event, helper) {
         document.getElementById('Dosiometristoutput').style.display = '';
         document.getElementById('Dosiometristinput').style.display = 'none';
         helper.TPassPrioritySave(component,event,component.find("TPassPriorityText").get("v.value"),component.find("TPassPlanStageText").get("v.value"),component.find("TPassPlanStateText").get("v.value"),component.find("TPassDosiometrist").get("v.value"));
    },	
	
    newHidePopUp : function(component, event, helper) {        
        helper.hidePopupHelper(component, 'newmodaldialog', 'slds-fade-in-');
		helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');        
    },
    handleExcelClick : function(component, event, helper){        
       helper.handleExcelClick(component,event);
    },
    handleStartDateChange : function(component, event, helper) {
        console.log('v.startdate'+event.getParam("value"));
    },
    dateChange: function(component, event, helper) {
        console.log("dateChange");
        var date = component.get("v.startdate");
        console.log("date is: ", date);
    },
    handleCommentSaveClick: function(component, event,helper) {   
        //debugger;
        helper.SaveComments(component, event);
    },
    
    handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.getTPassList(component,event);
    },
    
    treatmentplanning: function(component, event, helper) {
        var url = "/treatmentplanning?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    onReset : function(component, event, helper){
        component.find("refineKeyword").set("v.value","");
        component.find("TPassFilterPlanStage").set("v.value","--Any--");
        component.find("TPassFilterPriority").set("v.value","--Any--");
		component.set("v.TPaasStatusLink","Open");
        $('.inputDateFrom').val('');
        $('.inputDateTo').val('');
        helper.getTPassList(component,event,true);
    },
    
    
    showChat : function(component, event,helper){
        var bChatOpen = component.get("v.bActiveChatClass");
        var bChatActive = component.get("v.bOpenChatClass");
        
        if(bChatOpen == '' && bChatActive == ''){
            component.set("v.bOpenChatClass" , 'slds-is-open');
             component.set("v.bActiveChatClass" , 'slds-is-active');
        }else{
            component.set("v.bOpenChatClass" , '');
             component.set("v.bActiveChatClass" , '');
        } 
        
    },    
    closeChat : function(component, event,helper){
        component.set("v.bOpenChatClass" , '');
        component.set("v.bActiveChatClass" , '');
    }
    
    
})
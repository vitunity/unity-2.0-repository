({
	/* Load data */
    getTPassList : function(component, event,resetPages) {
        console.log('##:'+$('.inputDateFrom')+':::'+$('.inputDateFrom').val());
        if(resetPages){
            component.find("paginationCmp").set("v.currentPageNumber",1);
            component.set("v.recordsOffset",0);
        }
        if(document.getElementById("recreateplanrequestdiv2") != null){
            document.getElementById("recreateplanrequestdiv2").style.display = 'none';
        }
        var action = component.get("c.getTPassList");
		
        var ViewMyOrders = false;
        try{
            ViewMyOrders=component.find("ViewMyOrders").get("v.value");
        }catch(err) {console.log(err.message);}
        
		console.log('0035:'+component.find("TPassFilterPriority").get("v.value")+'#####'+component.find("TPassFilterPlanStage").get("v.value"));
        action.setParams({"Document_TypeSearch":component.find("refineKeyword").get("v.value"),
                          "TPaasStatusLink":component.get("v.TPaasStatusLink"),
						  "Priority":component.find("TPassFilterPriority").get("v.value"),
						  "PlanStage":component.find("TPassFilterPlanStage").get("v.value"),
                          "recordsOffset":component.get("v.recordsOffset"),
                          "totalSize":component.get("v.totalSize"),
                          "pageSize":component.get("v.pageSize"),                          
                          "frmDate":$('.inputDateFrom').val(),
                          "toDate":$('.inputDateTo').val(),
                          "ViewMyOrders":ViewMyOrders
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {                
                
                var responseWrap = response.getReturnValue();                 
                component.set("v.TPAASList",responseWrap.TPAASList);
                component.set("v.totalSize",responseWrap.totalSize);
                component.set("v.isInternalUser",responseWrap.isInternalUser);
                component.set("v.SelectedTpass","");				
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
		
		
    },
    
	getFilterPlanStage : function(component){
       
		var TPaasStatusLink = component.get("v.TPaasStatusLink");
		var action = component.get("c.getPlanStages");
		action.setParams({"TPaasStatusLink":TPaasStatusLink});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
				component.set("v.filterplanstage","--Any--");   
				component.set("v.PlanStages",response.getReturnValue());				
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
	},
	getFilterPriorities: function(component){
       
		var action = component.get("c.getPriorities");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
				component.set("v.filterpriority","--Any--");  
				component.set("v.Priorities",response.getReturnValue());                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
	},
	TPassPrioritySave : function(component, event,priority,planstage,planstate,dosiometrist) {
        var action = component.get("c.savePriority");
        action.setParams({"packageId":component.get("v.packageId"),
                          "priority":priority,
						  "planstage":planstage,
						  "planstate":planstate,
						  "dosiometrist":dosiometrist
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState();            
            if (component.isValid() && state === "SUCCESS") {                
                component.set("v.tdetail.Priority__c",priority); 
				component.set("v.tdetail.Plan_Stage__c",planstage); 
				component.set("v.tdetail.PlanState__c",planstate); 
                component.set("v.Assigned_DosiometristName",dosiometrist);                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	SaveComments: function(component,event){

        console.log('PackageId---' + component.get("v.packageId"));
        var action = component.get("c.saveComment");        
        action.setParams({"PackageId":component.get("v.packageId"),"body":component.find("commentBodyId").get("v.value")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            this.HidePopup(component);
            if (state === "SUCCESS") { 
                this.loadTPassDetailsSection(component,event,component.get("v.packageId"));
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action); 
    },  
	HidePopup:function(component){       
       
        var bodyelements = document.getElementsByClassName('modal-open');
        while(bodyelements.length > 0){
            bodyelements[0].classList.remove("modal-open");
        }
        //console.log(component.find("commentBodyId"));
        component.find("commentBodyId").set("v.value","");
        var clcelements = document.getElementsByClassName('modal-backdrop');
        while(clcelements.length > 0){
            clcelements[0].classList.remove("modal-backdrop");
        }
        document.getElementById('tpass-comment').setAttribute("aria-hidden", "true");
        document.getElementById('divTPassDetails').classList.add("in");
        document.getElementById('tpass-comment').style.display = 'none';
    },
    handleExcelClick : function(component, event, helper){
        
                          
        var st=component.find("refineKeyword").get("v.value");
        var frmDate=$('.inputDateFrom').val();
        var toDate=$('.inputDateTo').val();        
        
		var Priority = component.find("TPassFilterPriority").get("v.value");
		var PlanStage= component.find("TPassFilterPlanStage").get("v.value");
		var TPaasStatusLink = component.get("v.TPaasStatusLink");
        var ViewMyOrders = false;
        try{
            ViewMyOrders=component.find("ViewMyOrders").get("v.value");
        }catch(err) {console.log(err.message);}
		
        var sPageURL = window.location.href;
        var sURLVariables = sPageURL.split('/');
                 
        var strRemove =  sURLVariables[sURLVariables.length - 2] + '/' + sURLVariables[sURLVariables.length - 1];
        var newUrl = sPageURL.replace(strRemove, '');
        var excelurl = "apex/TreatmentPlanningExcel?st="+st+"&Priority="+Priority+"&PlanStage="+PlanStage+'&fdate='+frmDate+'&tdate='+toDate+'&ViewMyOrders='+ViewMyOrders+'&TPaasStatusLink='+TPaasStatusLink;
        
        var url = newUrl + excelurl;
        console.log('url:'+url);
        //alert(url);
        window.location.replace(url) ; 
    },
	createTpassDetail : function(component,parentId){       

		console.log('Reset Compo:'+parentId);
		var action = component.get("c.getNewTPass");
		action.setParams({"parentId":parentId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
				console.log('CreateTPassDetail:'+response.getReturnValue().prescribeddose);
				
				component.set("v.TPassDetailObj",response.getReturnValue()); 
				
				component.set("v.inputNewpriority",response.getReturnValue().priority);		
                component.set("v.inputNewqarmetrics",response.getReturnValue().oarmetrics);
				component.set("v.inputNewSite",response.getReturnValue().siteId);	
				
				component.find("tpassprescribeddose").set("v.value",response.getReturnValue().prescribeddose);
				component.find("tpassacceptabledoseattenuation").set("v.value",response.getReturnValue().acceptabledoseattenuation);
				component.find("tpassoarmetrix").set("v.value",response.getReturnValue().oarmetrics);
				component.find("tpassoarnotes").set("v.value",response.getReturnValue().oarnotes);
                component.find("tpasspatient").set("v.value",response.getReturnValue().Patient);
                component.find("tpassclientname").set("v.value",response.getReturnValue().ClientName);                
				component.find("tpassplannotes").set("v.value",response.getReturnValue().plannotes);
				
				
			
				
				component.set("v.SelectedTpass","");
				
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	saveTpassDetail : function(component,event){
       
        
	    var pprt='Priority Replan(1 day)';
		try{
            pprt = component.find("tpasspriority").get("v.value");
        }catch(err) {console.log(err.message);}
		
		var action = component.get("c.SaveNewTPassDetail");		
		action.setParams({ 
            "wObj" : JSON.stringify(component.get("v.TPassDetailObj")),
			"priority" : pprt,
			"qarmetrics" : component.find("tpassqarmetrics").get("v.value"),
			"site" : component.get("v.inputNewSite")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
			
            if (state === "SUCCESS") {
                this.getTPassList(component,event,true);
                this.HideNewCreatePopup(component);
				component.set("v.SelectedTpass","");
				document.getElementById("recreateplanrequestdiv2").style.display = 'none';
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	HideNewCreatePopup:function(component){       
       
        var bodyelements = document.getElementsByClassName('modal-open');
        while(bodyelements.length > 0){
            bodyelements[0].classList.remove("modal-open");
        }
        var clcelements = document.getElementsByClassName('modal-backdrop');
        while(clcelements.length > 0){
            clcelements[0].classList.remove("modal-backdrop");
        }
        document.getElementById('tpass-comment').setAttribute("aria-hidden", "true");
        document.getElementById('divTPassDetails').classList.add("in");
        document.getElementById('tpass-comment').style.display = 'none';
    },
	RefreshTPassComments : function(component,event,index){
       
		
		var action = component.get("c.getTPassComments");
        action.setParams({"PackageId":index});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            
            if (state === "SUCCESS") { 
                component.set("v.tcomment",response.getReturnValue());  
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	getDosiometristInfo : function(component,event, caller){
       
		var action = component.get("c.getDosiometristInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
				component.set("v.IsDosiometrist",response.getReturnValue());                 
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
	},
	loadTPassDetailsSection : function(component,event,index){
       
		document.getElementById('divTPassFilterSection').style.display = 'none';
        document.getElementById('divTpassSection').style.display = 'none';
        document.getElementById('divTPassDetails').style.display = 'block';
          
        
        var action = component.get("c.getTPassDetails");
        action.setParams({"PackageId":index});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
                //console.log(JSON.stringify(response.getReturnValue()));
                
                component.set("v.tdetail",response.getReturnValue()); 
                component.set("v.packageId",response.getReturnValue().Id);
				component.set("v.inputDosiometrist",response.getReturnValue().Assigned_Dosiometrist__r.Name);
                component.set("v.Assigned_DosiometristName",response.getReturnValue().Assigned_Dosiometrist__r.Name);
                component.set("v.tcomment",response.getReturnValue().TPassPackage_Comments__r);
				component.set("v.headerLabel","tpaasdetail");
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);


		action = component.get("c.getDosiometrists");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.TPAASDosiometristList",response.getReturnValue());  
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
		
		/*action = component.get("c.getTPassComments");
        action.setParams({"PackageId":index});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            
            if (state === "SUCCESS") { 
                component.set("v.tcomment",response.getReturnValue());  
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);*/
    },
    
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    }, 
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
    }	
})
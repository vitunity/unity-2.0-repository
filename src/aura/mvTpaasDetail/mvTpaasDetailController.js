({
    doInit : function(component, event, helper) {
        helper.getUrlParameter(component, event);        //alert('::'+component.get("v.packageId"));
        helper.loadTPassDetailsSection(component, event,component.get("v.packageId"));
		helper.getDosiometristInfo(component,event,"load");
		helper.RefreshTPassComments(component,event,component.get("v.packageId"),true);
        
		var intervalId =  window.setInterval(
            $A.getCallback(function() { 
                //document.getElementById('refreshEnable').style.display = 'none';
				//document.getElementById('refreshDisable').style.display = '';
				helper.RefreshTPassComments(component,event,component.get("v.packageId"),false);
                //window.clearInterval(intervalId);
            }),
            10000
        );  
    },
    backToList: function(component, event,helper) {        
        var url = "/treatmentplanningservices?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    home: function(component, event, helper) {
        var url = "/homepage?lang="+component.get('v.language');
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    },
    
	onPriorityChange : function(component, event, helper) {
         component.set("v.inputpriority",component.find("TPassPriorityText").get("v.value")); 
         document.getElementById('priorityoutput').style.display = 'none';
         document.getElementById('priorityinput').style.display = '';
    },
    onPrioritySave : function(component, event, helper) {
        console.log('####onPrioritySave####');
        document.getElementById('priorityoutput').style.display = '';
        document.getElementById('priorityinput').style.display = 'none';
        helper.TPassPrioritySave(component,event,
                                 component.find("TPassPriority").get("v.value"),
                                 component.find("TPassPlanStageText").get("v.value"),
                                 component.find("TPassDosimetristNotesText").get("v.value"),
                                 component.find("TPassDosiometristText").get("v.value")
                                );
    },
    
    onPlanStageChange : function(component, event, helper) {
         component.set("v.inputPlanStage",component.find("TPassPlanStageText").get("v.value")); 
         document.getElementById('PlanStageoutput').style.display = 'none';
         document.getElementById('PlanStageinput').style.display = '';
    },
    onPlanStageSave : function(component, event, helper) {
		
		var isError = false; 
		var errordiv = document.getElementById('dosiopnotediv');
		errordiv.style.display= 'none';
		var planstageVal = component.find("TPassPlanStage").get("v.value");
		if(planstageVal == 'Completed'){		     
			var noteval = component.get("v.inputDosimetristNotes");			
			if(noteval == '' || noteval == 'undefined' || noteval == null){            
				errordiv.style.display= '';
				isError = true;
				document.getElementById('DosimetristNotesoutput').style.display = 'none';
				document.getElementById('DosimetristNotesinput').style.display = '';        
				component.find("TPassDosimetristNotes").set("v.value",component.find("TPassDosimetristNotesText").get("v.value"));
			}         
			else {
				errordiv.style.display= 'none';
			}
		}
		
		if(isError == false){
		
			document.getElementById('PlanStageoutput').style.display = '';
			document.getElementById('PlanStageinput').style.display = 'none';
			helper.TPassPrioritySave(component,event,
			component.find("TPassPriorityText").get("v.value"),
			component.find("TPassPlanStage").get("v.value"),
			component.find("TPassDosimetristNotesText").get("v.value"),
			component.find("TPassDosiometristText").get("v.value"));
		}
    },
		
	
	onDosimetristNotesChange : function(component, event, helper) {  
         
        document.getElementById('DosimetristNotesoutput').style.display = 'none';
        document.getElementById('DosimetristNotesinput').style.display = '';        
		component.find("TPassDosimetristNotes").set("v.value",component.find("TPassDosimetristNotesText").get("v.value"));
       
    },	
	onDosimetristNotesSave : function(component, event, helper) {
		
		var DosimetristNotes = component.find("TPassDosimetristNotes").get("v.value");
		var errordiv = document.getElementById('dosiopnotediv');
		var planstageVal=component.find("TPassPlanStageText").get("v.value");
		
		
		var PlanStageinput = document.getElementById('PlanStageinput');		
		if(PlanStageinput != 'undefined' && PlanStageinput.style.display != 'none'){
			planstageVal = component.find("TPassPlanStage").get("v.value");
		}
		
		console.log(planstageVal+'@@@@@@@@@'+DosimetristNotes);
		if(planstageVal != 'Completed' || (planstageVal == 'Completed' && DosimetristNotes != '' && DosimetristNotes != 'undefined' && DosimetristNotes != null)){		
			
			document.getElementById('PlanStageoutput').style.display = '';
			document.getElementById('PlanStageinput').style.display = 'none';
			errordiv.style.display = 'none';
				
			document.getElementById('DosimetristNotesoutput').style.display = '';
			document.getElementById('DosimetristNotesinput').style.display = 'none';
			helper.TPassPrioritySave(component,event,
			component.find("TPassPriorityText").get("v.value"),
			planstageVal,
			component.find("TPassDosimetristNotes").get("v.value"),
			component.find("TPassDosiometristText").get("v.value"));
		}else if(planstageVal == 'Completed'){
			errordiv.style.display = '';
		}
		
    },	
	
    
	onDosiometristChange : function(component, event, helper) {  
         
        document.getElementById('Dosiometristoutput').style.display = 'none';
        document.getElementById('Dosiometristinput').style.display = '';
        
		component.find("TPassDosiometrist").set("v.value",component.find("TPassDosiometristText").get("v.value"));
        
    },	
	onDosiometristSave : function(component, event, helper) {
         document.getElementById('Dosiometristoutput').style.display = '';
         document.getElementById('Dosiometristinput').style.display = 'none';
         helper.TPassPrioritySave(component,event,
		 component.find("TPassPriorityText").get("v.value"),
		 component.find("TPassPlanStageText").get("v.value"),
		 component.find("TPassDosimetristNotesText").get("v.value"),
		 component.find("TPassDosiometrist").get("v.value"));
    },	
	
	refreshComment : function(component, event, helper){		
		document.getElementById('refreshEnable').style.display = 'none';
		document.getElementById('refreshDisable').style.display = '';
        helper.RefreshTPassComments(component,event,component.get("v.packageId"),true);
    },
	handlepaginationEvent : function(component, event, helper) {
        var recordsOffset = event.getParam("recordsOffset");
        component.set("v.recordsOffset",recordsOffset);
        helper.RefreshTPassComments(component,event,component.get("v.packageId"),false);
    },
	handleCommentSaveClick: function(component, event,helper) {   
        helper.SaveComments(component, event);
    },
	onSaveTpassDetail : function(component, event,helper) {
        helper.saveTpassDetail(component,event);
    }
})
({
    getUrlParameter : function(component, event) {
        var getUrlParameter = function getUrlParameter(sParam,fParam) {

            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i,
                data = {};

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    data.lang = sParameterName[1] === undefined ? true : sParameterName[1];
                }
                else if (sParameterName[0] === fParam){
                    data.id = sParameterName[1];
                }
            }
            return data;
        };
        var bdata =  getUrlParameter('lang','id');
        //alert(bdata.lang+'---'+bdata.id);
        component.set("v.packageId", bdata.id);
        component.set("v.language",bdata.lang);
        
    },
    
    
	TPassPrioritySave : function(component, event,priority,planstage,dosiometristnotes,dosiometrist) {
        console.log('###############TPassPrioritySave################');
        var action = component.get("c.savePriority");
        action.setParams({"packageId":component.get("v.packageId"),
                          "priority":priority,
						  "planstage":planstage,
						  "dosiometristnotes":dosiometristnotes,
						  "dosiometrist":dosiometrist
                         });
        
        action.setCallback(this, function(response) {
            var state = response.getState(); 
            console.log('state:'+state);
            if (component.isValid() && state === "SUCCESS") { 
                console.log('res:'+response.getReturnValue());
				component.set("v.tdetail",response.getReturnValue()); 
                component.set("v.tdetail.Priority__c",priority); 
				component.set("v.tdetail.Dosimetrist_Notes__c",dosiometristnotes); 
				component.set("v.tdetail.Plan_Stage__c",response.getReturnValue().Plan_Stage__c); 
                component.set("v.Assigned_DosiometristName",dosiometrist);                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	SaveComments: function(component,event){

        console.log('PackageId---' + component.get("v.packageId"));
        var action = component.get("c.saveComment");        
        action.setParams({"PackageId":component.get("v.packageId"),"body":component.find("commentBodyId").get("v.value")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            this.HidePopup(component);
            if (state === "SUCCESS") { 
                this.loadTPassDetailsSection(component,event,component.get("v.packageId"));
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action); 
    },  
	HidePopup:function(component){       
       
        var bodyelements = document.getElementsByClassName('modal-open');
        while(bodyelements.length > 0){
            bodyelements[0].classList.remove("modal-open");
        }
        //console.log(component.find("commentBodyId"));
        component.find("commentBodyId").set("v.value","");
        var clcelements = document.getElementsByClassName('modal-backdrop');
        while(clcelements.length > 0){
            clcelements[0].classList.remove("modal-backdrop");
        }
        document.getElementById('tpass-comment').setAttribute("aria-hidden", "true");
        document.getElementById('divTPassDetails').classList.add("in");
        document.getElementById('tpass-comment').style.display = 'none';
    },
	loadTPassDetailsSection : function(component,event,index){
       
        component.find("paginationCmp").set("v.currentPageNumber",1);
        component.set("v.recordsOffset",0);
		
        var action = component.get("c.getTPassDetails");
        action.setParams({"PackageId":index});
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
                //console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.packageId",response.getReturnValue().Id);
                component.set("v.tdetail",response.getReturnValue()); 
				component.set("v.inputDosiometrist",response.getReturnValue().Assigned_Dosiometrist__r.Name);
                component.set("v.Assigned_DosiometristName",response.getReturnValue().Assigned_Dosiometrist__r.Name);
				component.set("v.inputDosimetristNotes",response.getReturnValue().Dosimetrist_Notes__c);
				
                //component.set("v.tcomment",response.getReturnValue().TPassPackage_Comments__r);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);


		action = component.get("c.getDosiometrists");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.TPAASDosiometristList",response.getReturnValue());  
                
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
	RefreshTPassComments : function(component,event,index,resetPages){
      
		
		var action = component.get("c.getTPassComments");
        action.setParams({
			"PackageId":index,
			"recordsOffset":component.get("v.recordsOffset"),
			"totalSize":component.get("v.totalSize"),
			"pageSize":component.get("v.pageSize")
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.tcomment",response.getReturnValue().comments);
                component.set("v.totalSize",response.getReturnValue().totalComments);
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }     
            document.getElementById('refreshEnable').style.display = '';
			document.getElementById('refreshDisable').style.display = 'none';
        } );
        $A.enqueueAction(action);
    },
	saveTpassDetail : function(component,event){
       
	   
		var action = component.get("c.SaveNewTPassDetail");		
		action.setParams({ 
            "wObj" : JSON.stringify(component.get("v.TPassDetailObj")),
			"priority" : component.find("tpasspriority").get("v.value"),
			"qarmetrics" : component.find("tpassqarmetrics").get("v.value"),
			"site" : component.get("v.inputNewSite")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
			
            if (state === "SUCCESS") {
                this.getTPassList(component,event,true);
                this.HideNewCreatePopup(component);
				component.set("v.SelectedTpass","");
				document.getElementById("recreateplanrequestdiv2").style.display = 'none';
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
    },
    getDosiometristInfo : function(component,event, caller){
       
		var action = component.get("c.getDosiometristInfo");
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") { 
				component.set("v.IsDosiometrist",response.getReturnValue());                 
            }else if(response.getState() == "ERROR"){
                $A.log("callback error", response.getError());
            }      
        } );
        $A.enqueueAction(action);
	},
	HideNewCreatePopup:function(component){       
       
        var bodyelements = document.getElementsByClassName('modal-open');
        while(bodyelements.length > 0){
            bodyelements[0].classList.remove("modal-open");
        }
        var clcelements = document.getElementsByClassName('modal-backdrop');
        while(clcelements.length > 0){
            clcelements[0].classList.remove("modal-backdrop");
        }
        document.getElementById('tpass-comment').setAttribute("aria-hidden", "true");
        document.getElementById('divTPassDetails').classList.add("in");
        document.getElementById('tpass-comment').style.display = 'none';
    },
	getTPassList : function(component, event,resetPages) {
		alert('call tpaas list page');
	}
})
({
    doInit : function(component, event, helper)
    {
        helper.getSoiDeliviries(component);
    },
    openSalesOrder: function(component, event, helper) {
    	var id_str = event.currentTarget.getAttribute("data-soId");
    	//sforce.one.navigateToSObject(id_str);

		if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
		    // Salesforce app navigation
		    sforce.one.navigateToSObject(id_str);
		}
		else {
		    // Set the window's URL using a Visualforce expression

            window.open("/"+id_str, target="_blank");
		    //window.location.href = "/" + id_str+"_blank";
		    // '/varian--sfdev1.cs67.my.salesforce.com/'+ id_str ; 
		        //'{!URLFOR($Action.Sales Orders.View, id_str)}';
		}

    	/*
        var id_str = event.currentTarget.getAttribute("data-soId");
        alert(id_str);
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": id_str
        });
        $A.get('e.force:refreshView').fire();
        navEvt.fire();
        */
    },   
    openDelVer: function(component, event, helper) {
    	var id_str = event.currentTarget.getAttribute("data-DelId");
    	//sforce.one.navigateToSObject(id_str);

		if( (typeof sforce != 'undefined') && sforce && (!!sforce.one) ) {
		    // Salesforce app navigation
		    sforce.one.navigateToSObject(id_str);
		}
		else {
		    // Set the window's URL using a Visualforce expression
            window.open("/"+id_str, target="_blank");
		}
    }, 
})
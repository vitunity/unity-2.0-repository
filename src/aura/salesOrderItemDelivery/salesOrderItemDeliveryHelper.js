({
    getSoiDeliviries : function(component) 
    {
        var action = component.get('c.getSoiDeliveries'); 
        //alert('record id = ' + component.get("v.recordId"));
        var recId = component.get("v.recordId");   //a1r0n00000082lL
        action.setParams({"recId":recId});    
        action.setCallback(this, function(actionResult) 
           {
               var Results=actionResult.getReturnValue();
               component.set("v.soiDeliveries",Results);
               
           });
        $A.enqueueAction(action);
        
    }  
})
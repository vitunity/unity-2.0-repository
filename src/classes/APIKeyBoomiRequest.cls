public with sharing class APIKeyBoomiRequest implements Queueable, Database.AllowsCallouts {
    Id apiKeyRequestId;
    
    public APIKeyBoomiRequest(Id apiKeyRequestId){
        this.apiKeyRequestId = apiKeyRequestId;
    }
    
    public void execute(QueueableContext context) 
    {
        System.debug('-----generateAPIKey---');
        API_Request__c apiReq = [SELECT Name, Allow_Access_To__c, Licenses__c, Valid_for_No_of_days__c, New_Key_Format__c,Customer_Name__c FROM API_Request__c WHERE id =:apiKeyRequestId];
        system.debug('apiReq ---'+apiReq);
        BOOMIAPIBODY__mdt apiKeyBody = [SELECT APIKeyRequestBody__c FROM BOOMIAPIBODY__mdt LIMIT 1];
        String requestBody = '';
        requestBody = apiKeyBody.APIKeyRequestBody__c.replace('APIKEYIDVALUE',apiKeyRequestId);
        requestBody = requestBody.replace('APINEWKEYFORMATVALUE',String.valueOf(apiReq.New_Key_Format__c));
        requestBody = requestBody.replace('APIJSONNAME',apiReq.Name);
        //requestBody = requestBody.replace('APIJSONNAME','API-01648');
        requestBody = requestBody.replace('APIJSONISSUEDTO',apiReq.Customer_Name__c);
        if(apiReq.Valid_for_No_of_days__c  != null)
            requestBody = requestBody.replace('APIJSONEXPIRESON',system.now().addDays(apiReq.Valid_for_No_of_days__c.intValue()).format());
        else
             requestBody = requestBody.replace('APIJSONEXPIRESON','');

        if(apiReq.Licenses__c  != null)
               requestBody = requestBody.replace('APIJSONLICENSES',apiReq.Licenses__c);
        else
            requestBody = requestBody.replace('APIJSONLICENSES','');
        
        if(apiReq.Allow_Access_To__c!= null)
            requestBody = requestBody.replace('APIJSONALLOWACCESSTO',apiReq.Allow_Access_To__c);
         else
            requestBody = requestBody.replace('APIJSONALLOWACCESSTO','');
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:BOOMI_API');
        req.setMethod('POST');        
        System.debug('-----req---'+requestBody);
        req.setBody(requestBody);
        
        Http http = new Http();
       
        //if(!Test.isRunningTest())
        //{ 
           HTTPResponse res = http.send(req);
           System.debug('-----res---'+res);
           System.debug('-----res---'+res.getBody());
           if(res.getbody().contains('error'))
           {
              apiReq.Interface_Status__c ='401';
              update apiReq;
           }
        //}
    }
}
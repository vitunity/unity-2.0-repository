public with sharing class APIKeyDisplayRequests {
 
	@AuraEnabled
    public static Lightning_TableWithSearch getAPIRequests(String lookupControllerString){
    	Lightning_TableWithSearch tableData = Lightning_TableWithSearch.initializeTable(lookupControllerString);
    	updateApproverComments(tableData.objectRecords);
    	System.debug('--tableData--'+tableData);
    	return tableData;
    } 
    @TestVisible
    private static void updateApproverComments(List<API_Request__c> apiRequests){
    	Map<Id, API_Request__c> approverActions = new Map<Id, API_Request__c>([
    		Select Id,
			(
				Select Id, IsPending, TargetObjectId, StepStatus,Comments, CreatedDate,  CreatedById 
				FROM ProcessSteps 
				WHERE StepStatus = 'Approved' OR StepStatus = 'Rejected'
				ORDER BY CreatedDate DESC LIMIT 1
			) 
			FROM API_Request__c 
			WHERE Id IN:apiRequests
		]);
		System.debug('----approverActions--'+approverActions);
		
    	for(API_Request__c apiRequest : apiRequests){
    		if(approverActions.containsKey(apiRequest.Id)){
    			API_Request__c apiReqApprovalAction = approverActions.get(apiRequest.Id);
    			if(!apiReqApprovalAction.ProcessSteps.isEmpty()){
    				apiRequest.Approver_Comment__c = apiReqApprovalAction.ProcessSteps[0].Comments;
    			}
    		}
    	}
    }
    
    
    @AuraEnabled 
    public static List<Attachment> getAPIRequestAttachments(String apiRequestId){
    	String apiKeyId = '';
    	List<API_Request__c> apiRequest = [SELECT Id, API_Key_Attachment_Id__c FROM API_Request__c WHERE Id =:apiRequestId];
    	if(!apiRequest.isEmpty()){
    		apiKeyId = apiRequest[0].API_Key_Attachment_Id__c;
    	}
    	return [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId =: apiRequestId AND Id!=:apikeyId];
    }

    @AuraEnabled 
    public static List<API_Request_Status__mdt> getApiRequestMetaData(String status){
        list<API_Request_Status__mdt> list1
            = [SELECT Id, Masterlabel, Developername, Status__c FROM API_Request_Status__mdt
                    WHERE Developername = :status];

        return list1;
    }    

    @AuraEnabled 
    public static List<HistoryWrapper> getApiRequestHistory(String apiRequestId){
    	List<HistoryWrapper> apiFieldHistory = new List<HistoryWrapper>();
    	for(API_Request__History fieldHistory : [
    		SELECT ParentId, OldValue, NewValue, Field, CreatedBy.Name, CreatedDate 
    		FROM API_Request__History 
    		WHERE ParentId =:apiRequestId ORDER BY CreatedDate DESC
    	]){
    		Boolean isSFId = false;
    		String oldValue = '';
    		String newValue = '';
    		if(fieldHistory.OldValue !=null){
    			oldValue = String.valueOf(fieldHistory.OldValue);
    		}
    		if(fieldHistory.NewValue !=null){
    			newValue = String.valueOf(fieldHistory.NewValue);
    		}
    		if(Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(oldValue).matches()){
    			isSFId = true;
    		}
    		if(Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(newValue).matches()){
    			isSFId = true;
    		}
    		if(!isSFId){
	    		String action = (fieldHistory.Field != 'created' && fieldHistory.Field != 'locked')?
            	('Changed '+fieldHistory.Field+' From '+fieldHistory.OldValue+' to '+fieldHistory.NewValue):fieldHistory.Field;
    			apiFieldHistory.add(new HistoryWrapper(fieldHistory.CreatedDate.date(), fieldHistory.CreatedBy.Name, action));
			}
    	}
    	return apiFieldHistory;
    }
    
    @AuraEnabled 
    public static void approveRejectAPIRequest(String apiRequestId, String actionName, String comments){
    	
    	List<Group> apiApprovalGroup =[SELECT Id FROM Group WHERE Type='Queue' and DeveloperName =: System.Label.API_Key_Approval_Queue];
    	
        List<ProcessInstanceWorkitem> approvalItems = [
            SELECT Id From ProcessInstanceWorkitem p
			WHERE p.ProcessInstance.TargetObjectId = :apiRequestId
			AND p.OriginalActorId =:apiApprovalGroup[0].Id
		];
    	system.debug('approvalItems@@'+approvalItems);
    	// Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest aprovalActionItem = new Approval.ProcessWorkitemRequest();
        aprovalActionItem.setComments(comments);
        aprovalActionItem.setAction(actionName);
        aprovalActionItem.setWorkitemId(approvalItems.get(0).Id);
        Approval.ProcessResult result2 =  Approval.process(aprovalActionItem);
    }
    
    @AuraEnabled
    public static void takeOwnership(Id apiRequestId) {
    	update new API_Request__c(Id=apiRequestId, OwnerId=UserInfo.getUserId());
    } 
    
    @AuraEnabled
    public static List<Attachment> attachFile(Id parentId, String fileName, String base64Data, String contentType) {
    	APIKeyRequestFormController.saveTheFile(parentId, fileName, base64Data, contentType);
    	return getAPIRequestAttachments(parentId);
    } 
    
    @AuraEnabled
    public static Map<String,Map<String,List<String>>> getAPIScopes() {
    	return APIKeyRequestFormController.getAPIScopes();
    } 
    
    @AuraEnabled
    public static List<Attachment> deleteAttachment(String attachmentId, String apiRequestId){
    	delete new Attachment(Id=attachmentId);
    	return [SELECT Id, Name FROM Attachment WHERE ParentId =: apiRequestId];
    }
    
    
    @AuraEnabled 
    public static Boolean isLoggedInUserAdmin(){
    	List<Group> adminGroup = [SELECT Id FROM Group WHERE Type='Queue' and DeveloperName =: System.Label.API_Key_Approval_Queue];
    	List<GroupMember> adminMember = [SELECT Id FROM GroupMember WHERE GroupId =:adminGroup[0].Id AND UserOrGroupId=:UserInfo.getUserId()];
    	return !adminMember.isEmpty();
    }
    
    public class HistoryWrapper{
    	@AuraEnabled public Date createdDate;
    	@AuraEnabled public String userName;
    	@AuraEnabled public String action;
    	public HistoryWrapper(Date createdDate, String userName, String action){
    		this.createdDate = createdDate;
    		this.userName = userName;
    		this.action = action;
    	}
    }
}
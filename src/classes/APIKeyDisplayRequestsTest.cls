@IsTest
public class APIKeyDisplayRequestsTest {
	
    public static testMethod void getAPIRequestsTest(){
    	Lightning_TableWithSearchTest.insertAPIRequestData();
    	System.debug('---insertData--'+Lightning_TableWithSearchTest.salesAccount.Id);
    	
    	Date startDateValue = Date.today().addDays(-1);
        Date endDateValue = Date.today().addDays(2);
        String startDate = String.valueOf(startDateValue.month())+'/'+String.valueOf(startDateValue.day())+'/'+String.valueOf(startDateValue.year());
        String endDate = String.valueOf(endDateValue.month())+'/'+String.valueOf(endDateValue.day())+'/'+String.valueOf(endDateValue.year());
        
        Lightning_TableWithSearch lightningTable = new Lightning_TableWithSearch();
        lightningTable.sObjectName = 'API_Request__c';
        lightningTable.pageSize = 10;
        lightningTable.offset = 0;
        lightningTable.objectFields = new List<String>{
        	'Name', 'API_Request_Id__c', 'Software_System__c', 'API_Type__c', 'OwnerId',
        	'Request_Reason__c', 'Customer_Name__c', 'Customer_Contact_Name__c', 'Owner_Approver_Name__c',
        	'Request_Status__c', 'Approval_Status__c', 'CreatedDate', 'Approver_Name__c',
        	'X3rd_party_Software__c', 'API_Key_Attachment_Id__c', 'Approver_Comment__c'
        };
        lightningTable.defaultFieldNames = new List<String>{
        	'Name', 'Account__c', 'Customer_Name__c', 'Software_System__c', 'API_Type__c',
             'X3rd_party_Software__c', 'Approval_Status__c'
        };
        lightningTable.defaultFieldValues = new List<String>{
        	'', Lightning_TableWithSearchTest.salesAccount.Id, '', 'TEST', '',
             '', ''
        };
        lightningTable.startDate = startDate;
		lightningTable.endDate = endDate;
        lightningTable.hasPrev = false;
        lightningTable.hasNext = false;
        lightningTable.pageSize = 10;
        Lightning_TableWithSearch tableData = APIKeyDisplayRequests.getAPIRequests(JSON.serialize(lightningTable));
        System.assertEquals(10, tableData.objectRecords.size());
        
        
        for(API_Request__c apiRequest : Lightning_TableWithSearchTest.apiRequests){
       		if(apiRequest.Id != Lightning_TableWithSearchTest.apiRequests[0].Id){
       			APIKeyRequestTriggerHandler.sendForApproval(apiRequest);
       		}
        }
        
        List<Group> apiApprovalGroup =[SELECT Id FROM Group WHERE Type='Queue' and DeveloperName =: System.Label.API_Key_Approval_Queue];
    	
        List<ProcessInstanceWorkitem> approvalItems = [
            SELECT Id From ProcessInstanceWorkitem p
			WHERE p.ProcessInstance.TargetObjectId = :Lightning_TableWithSearchTest.apiRequests[1].Id
			AND p.OriginalActorId =:apiApprovalGroup[0].Id
		];
    	system.debug('approvalItems@@--TEST--'+approvalItems+'---'+[SELECT Id, Approval_Status__c, Name FROM API_Request__c WHERE Id =:Lightning_TableWithSearchTest.apiRequests[1].Id][0].Approval_Status__c);
    	
		APIKeyDisplayRequests.takeOwnership(Lightning_TableWithSearchTest.apiRequests[0].Id);
		List<Attachment> attachments = APIKeyDisplayRequests.attachFile(Lightning_TableWithSearchTest.apiRequests[0].Id, 'TEST', 'TESTING', 'application/xml');
		APIKeyDisplayRequests.deleteAttachment(attachments[0].Id, Lightning_TableWithSearchTest.apiRequests[0].Id);
		APIKeyDisplayRequests.isLoggedInUserAdmin();
		APIKeyDisplayRequests.approveRejectAPIRequest(Lightning_TableWithSearchTest.apiRequests[0].Id, 'Approve', 'TESTING');
		System.assertEquals(2, [SELECT Id, Approval_Status__c FROM API_Request__c WHERE Approval_Status__c = 'Approved'].size());
		APIKeyDisplayRequests.approveRejectAPIRequest(Lightning_TableWithSearchTest.apiRequests[1].Id, 'Reject', 'TESTING');
		System.assertEquals(1, [SELECT Id, Approval_Status__c FROM API_Request__c WHERE Approval_Status__c = 'Rejected'].size());
		APIKeyDisplayRequests.getApiRequestHistory(Lightning_TableWithSearchTest.apiRequests[0].Id);
		APIKeyDisplayRequests.getAPIScopes();
        APIKeyDisplayRequests.getApiRequestMetaData('TEST');
    }
}
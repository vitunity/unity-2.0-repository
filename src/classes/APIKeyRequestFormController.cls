public without sharing class APIKeyRequestFormController {
    
    private static final String REQUIRED_REASON = 'REQUIREDREQUESTREASON';
    
    @AuraEnabled  
    public static Map<String,Map<String,List<String>>> getAPIScopes(){
        
        Boolean isInternalUser = false;
        User loggedInUser = [SELECT Id,ContactId FROM User WHERE Id=:UserInfo.getUserId()];
        
        if(loggedInUser.ContactId==null){
            isInternalUser = true;
        }
        
        Map<String,Map<String,List<String>>> apiScopesMap = new Map<String,Map<String,List<String>>>();
        Map<String, List<String>> requestReasonrequiredScopes = new Map<String,List<String>>();
        String apiKeyDetailsQuery = 'SELECT Software_System__c, API_Types__c, X3rd_Party_Software__c, Approval_Required__c, Request_Reason_Required__c,Allow_Duplicate_API_Req__c'
            +' FROM API_Key_Details__c ';
        
        if(!isInternalUser){
            apiKeyDetailsQuery+=' WHERE Available_For_Internal_Users_Only__c = false';
        }
        
        for(API_Key_Details__c apiScope : Database.query(apiKeyDetailsQuery)){
            if(apiScope.Request_Reason_Required__c){
                String apiScopeKey = apiScope.Software_System__c+''+apiScope.API_Types__c;
                if(!String.isBlank(apiScope.X3rd_Party_Software__c)){
                    apiScopeKey += apiScope.X3rd_Party_Software__c;
                }
                requestReasonrequiredScopes.put(apiScopeKey, new List<String>());
            }
            if(apiScopesMap.containsKey(apiScope.Software_System__c)){
                Map<String, List<String>> dependentAPITypes = apiScopesMap.get(apiScope.Software_System__c);
                if(dependentAPITypes.containsKey(apiScope.API_Types__c)){
                    if(!String.isBlank(apiScope.X3rd_Party_Software__c)){
                        dependentAPITypes.get(apiScope.API_Types__c).add(apiScope.X3rd_Party_Software__c);
                    }
                    apiScopesMap.put(apiScope.Software_System__c, dependentAPITypes);
                    continue;
                }
                dependentAPITypes.put(apiScope.API_Types__c, !String.isBlank(apiScope.X3rd_Party_Software__c)?new List<String>{apiScope.X3rd_Party_Software__c}:new List<String>());
                apiScopesMap.put(apiScope.Software_System__c, dependentAPITypes);
                continue;
            }
            apiScopesMap.put(apiScope.Software_System__c, new Map<String, List<String>>{apiScope.API_Types__c => new List<String>{apiScope.X3rd_Party_Software__c}});
        }
        apiScopesMap.put(REQUIRED_REASON, requestReasonrequiredScopes);
        System.debug('---apiScopesMap--'+apiScopesMap);
        return apiScopesMap;
    }
    
    @AuraEnabled
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;
        
        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    /* @AuraEnabled
public static String saveAPIRequest(String apiRequest){
System.debug('---saveAPIRequest--'+apiRequest);
API_Request__c apiRequestObject = (API_Request__c)JSON.deserialize(apiRequest, API_Request__c.class);
try{
insert apiRequestObject;
return 'SUCCESS-'+apiRequestObject.Id;
}catch(Exception e){
return e.getMessage();
}
}*/
    //Updated by Harshal
    @AuraEnabled
    public static String saveAPIRequest(String apiRequest){
        System.debug('---saveAPIRequest--'+apiRequest);
        List<API_Request__c> lstAPIrequest =new List<API_Request__c>();
        API_Request__c apiRequestObject = (API_Request__c)JSON.deserialize(apiRequest, API_Request__c.class);
        //Added by Harshal for Duplicate
        string apiKeyCombinationStr = '';
        string con =  apiRequestObject.Contact__c;
        con =  con.substring(0,con.length()-3);
        system.debug('con=='+con);

        if(String.isBlank(apiRequestObject.X3rd_party_Software__c) ){            
            apiKeyCombinationStr=con+''+apiRequestObject.Software_System__c+''+apiRequestObject.API_Type__c;
            system.debug('apiKeyCombinationStr=='+apiKeyCombinationStr);  
        }
        else{            
            apiKeyCombinationStr=con+''+apiRequestObject.Software_System__c+''+apiRequestObject.API_Type__c+''+apiRequestObject.X3rd_party_Software__c;
            system.debug('apiKeyCombinationStr=='+apiKeyCombinationStr);  
        }
        
        Boolean isAPIKeyDuplicate=[select Allow_Duplicate_API_Req__c from API_Key_Details__c where API_Types__c=:apiRequestObject.API_Type__c AND Software_System__c=:apiRequestObject.Software_System__c AND X3rd_party_Software__c=:apiRequestObject.X3rd_party_Software__c].Allow_Duplicate_API_Req__c;
        system.debug('isAPIKeyDuplicate=='+isAPIKeyDuplicate);
        if(string.isNotBlank(apiKeyCombinationStr)){
            lstAPIrequest=[select id,API_Key_Request_Key__c from API_Request__c where API_Key_Request_Key__c=:apiKeyCombinationStr];
        }
        system.debug('lstAPIrequest=='+lstAPIrequest);
       /*
        try{
            if(!isAPIKeyDuplicate){
                system.debug('In isAPIKeyDuplicate if==');
                if(lstAPIrequest.size()>0){
                    system.debug('In lstAPIrequest if before insert==');
                    insert apiRequestObject;  
                    system.debug('In lstAPIrequest if after insert==');
                    return 'SUCCESS-'+apiRequestObject.Id;
                }
                else{
                    system.debug('In lstAPIrequest else==');
                    return 'DUPLICATE_VALUE';               
                }
            }
            else{
                system.debug('In isAPIKeyDuplicate else before insert===');
                insert apiRequestObject;
                system.debug('In isAPIKeyDuplicate else after insert===');
                return 'SUCCESS-'+apiRequestObject.Id;
                
            }
        }catch(Exception e){
            return e.getMessage();
        }
*/
         try{
            if(lstAPIrequest.size()>0){
                system.debug('In lstAPIrequest if==');
                if(isAPIKeyDuplicate){
                    system.debug('In isAPIKeyDuplicate if before insert==');
                    insert apiRequestObject;  
                    system.debug('In isAPIKeyDuplicate if after insert==');
                    return 'SUCCESS-'+apiRequestObject.Id;
                }
                else{
                    system.debug('In lstAPIrequest else==');
                    return 'DUPLICATE_VALUE';               
                }
            }
            else{
                system.debug('In lstAPIrequest else before insert===');
                insert apiRequestObject;
                system.debug('In lstAPIrequest else after insert===');
                return 'SUCCESS-'+apiRequestObject.Id;
                
            }
        }catch(Exception e){
            return e.getMessage();
        }
        
        
        
    }
}
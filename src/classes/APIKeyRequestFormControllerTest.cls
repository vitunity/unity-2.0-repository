@IsTest
public class APIKeyRequestFormControllerTest {
    
    public static testMethod void APIKeyRequestForm(){
        
        insert new Regulatory_Country__c(
            Name = 'USA'
        );
        
        Group testGroup = new Group(Name='API Key Request Queue', Type='Queue');
        insert testGroup;
        QueuesObject testQueue;
        //Creating QUEUE
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            //Associating queue with group AND to the API Request object
            testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'API_Request__c');
            insert testQueue;
        }
        
        List<API_Key_Details__c> apiKeyList = new List<API_Key_Details__c>();        
        
        API_Key_Details__c apiKey = APIKeyRequestTestMethodHelper.getApiKeyDetails('ARIA', '3rd Party Access', 'Equicare (v15.5)');
        apiKey.Approval_Required__c=true;
        apiKey.Request_Reason_Required__c=true;
        apikey.Available_For_Internal_Users_Only__c=true;
        apiKey.OwnerId=userInfo.getUserId();        
        apiKeyList.add(apiKey);       
        
        API_Key_Details__c apiKey1 = APIKeyRequestTestMethodHelper.getApiKeyDetails('ARIA', '3rd Party Access', 'Equicare (v15.5)');        
        apiKey1.Approval_Required__c=true;
        apiKey1.Request_Reason_Required__c=true;
        apikey1.Available_For_Internal_Users_Only__c=false;        
        apiKeyList.add(apiKey1);        
        
        API_Key_Details__c apiKey2 = APIKeyRequestTestMethodHelper.getApiKeyDetails('ARIA', 'ARIA Access', '');       
        apiKey2.Approval_Required__c=true;
        apiKey2.Request_Reason_Required__c=true;
        apikey2.Available_For_Internal_Users_Only__c=false;        
        apiKeyList.add(apiKey2);
        insert apiKeyList;
        
        Account acc = TestUtils.getAccount();
        acc.name='Test-Acc';
        insert acc;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acc.Id;
        insert con;
        
        API_Request__c ApiReq;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            ApiReq = new API_Request__c();
            ApiReq.Account__c=acc.Id;
            ApiReq.Software_System__c='ARIA';
            ApiReq.API_Type__c='ARIA Access';
            ApiReq.Approval_Status__c='Approved';
            ApiReq.Requestor_Email__c='test@mail.com';
            ApiReq.Customer_Name__c = '1234';
            ApiReq.Allow_Access_To__c = 'Varian/ARIA/aria-access/LP';
            insert ApiReq;
            
            Attachment attach=new Attachment();     
            attach.Name='Test Attachment';
            Blob bodyBlob=Blob.valueOf('Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=ApiReq.id;
            insert attach;
            String rawParams = '{"Id":"ApiReq.id"}';
            
        }            
        
        API_Request__c ApiReq1 = new API_Request__c();
        ApiReq1.Account__c = acc.Id;
        ApiReq1.Contact__c = con.Id;
        ApiReq1.Software_System__c = 'ARIA';
        ApiReq1.API_Type__c = 'ARIA Access';
        ApiReq1.Requestor_Email__c = 'test@mail.com';
        ApiReq1.Customer_Name__c = '12345';
        ApiReq1.Allow_Access_To__c = 'Varian/ARIA/aria-access/LP';
        ApiReq1.ownerid=UserInfo.getUserId();
        	
        	APIKeyRequestFormController.getAPIScopes();
            APIKeyRequestFormController.saveTheFile(ApiReq.id, 'Test Attachment', 'base64Data', 'text');
            APIKeyRequestFormController.getCustomLabelMap('en_US');
            APIKeyRequestFormController.saveAPIRequest(JSON.serialize(ApiReq1));
        
        Test.startTest();
        	 
            Test.setMock(HttpCalloutMock.class, new APIKeyRequestHttpMock());
            System.enqueueJob(new APIKeyBoomiRequest(ApiReq.Id));
        Test.stopTest();
        
    }
}
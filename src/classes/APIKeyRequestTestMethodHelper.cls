@isTest
public class APIKeyRequestTestMethodHelper {
    public static API_Key_Details__c getApiKeyDetails(string softwaresystem, string ApiType, string thirdrdpartysoftware){
        
        API_Key_Details__c apiKey = new API_Key_Details__c();
        apiKey.Software_System__c =softwaresystem;
        apiKey.API_Types__c=ApiType;
        apiKey.X3rd_Party_Software__c=thirdrdpartysoftware;
        apiKey.Approval_Required__c=true;
        apiKey.Request_Reason_Required__c=true;
        apikey.Available_For_Internal_Users_Only__c=true;
        return apiKey;
        
    }
    
    public static API_Request__c getapiRequest(string requestorname,string softwaresystem,string thirdrdpartysoftware,string ApiType,string status){
        API_Request__c ApiReq = new API_Request__c();
        ApiReq.Requestro_Name__c=requestorname;
        ApiReq.Software_System__c=softwaresystem;
        ApiReq.API_Type__c=ApiType;
        ApiReq.X3rd_party_Software__c=thirdrdpartysoftware;
        ApiReq.Approval_Status__c=status;     
        
        return ApiReq;
    }
    
}
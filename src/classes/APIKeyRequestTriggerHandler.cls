public without sharing class APIKeyRequestTriggerHandler {
    
    public static Boolean stopAPIKeyRequestTrigger = false;
    /**
     * Check if api request needs to be sent for approval based on matrix maintained in Api Scopes custom metadata type
     */
    public static void sendForApproval(API_Request__c apiRequest){
        
        String query = 'SELECT Id '
        +'FROM API_Key_Details__c '
        +'WHERE Software_System__c = \''+apiRequest.Software_System__c+'\''
        +'AND API_Types__c = \''+apiRequest.API_Type__c+'\''
        +'AND Approval_Required__c = true ';
        
        if(!String.isBlank(apiRequest.X3rd_party_Software__c)){
            query += ' AND X3rd_Party_Software__c = \''+apiRequest.X3rd_party_Software__c+'\'';
        }
        System.debug('----query--'+query);
        List<API_Key_Details__c> apiScopes = Database.query(query);
        System.debug('-----apiScopes'+apiScopes);
        if(!apiScopes.isEmpty()){
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Approval needed for API Key Request Software System :'+apiRequest.Software_System__c+', API Type : '+apiRequest.API_Type__c+', 3rd Prarty : '+apiRequest.X3rd_party_Software__c);
            req1.setObjectId(apiRequest.Id);
            req1.setSubmitterId(UserInfo.getUserId()); 
            req1.setProcessDefinitionNameOrId('API_Key_Request_Approval_Management');
            req1.setSkipEntryCriteria(true);
            Approval.ProcessResult result = Approval.process(req1);
        }else{
            
            List<Group> apiApprovalGroup = [SELECT Id FROM Group WHERE Type='Queue' and DeveloperName =: System.Label.API_Key_Approval_Queue];
        
            List<ProcessInstanceWorkitem> approvalItems = [
                SELECT Id From ProcessInstanceWorkitem p
                WHERE p.ProcessInstance.TargetObjectId = :apiRequest.Id
                AND p.OriginalActorId =:apiApprovalGroup[0].Id
            ];
            
            APIKeyRequestTriggerHandler.stopAPIKeyRequestTrigger = true;
            if(!approvalItems.isEmpty()){
                APIKeyDisplayRequests.approveRejectAPIRequest(apiRequest.Id, 'Approve', 'Auto Approved.');
            }else{
                update new API_Request__c(Id=apiRequest.Id, Approval_Status__c = 'Approved', Approver_Name__c = 'Auto Approved');
                System.enqueueJob(new APIKeyBoomiRequest(apiRequest.Id));
            }
        }
    }
    
    /**
     * Share api request records with api key request customer contact
     */
    public static void shareAPIRequestsWithContacts(List<API_Request__c> apiRequests, Map<Id, API_Request__c> oldApiRequests){
        List<API_Request__Share> apiRequestSharesToInsert = new List<API_Request__Share>();
        for(API_Request__c apiRequest : apiRequests){
            if(oldApiRequests!=null && oldApiRequests.get(apiRequest.Id).OwnerId != apiRequest.OwnerId && apiRequest.CreatedById != apiRequest.OwnerId){
                if(apiRequest.Created_By_MyVarian_User__c){
                    apiRequestSharesToInsert.add(
                        new API_Request__Share(ParentId = apiRequest.Id, UserOrGroupId = apiRequest.CreatedById, AccessLevel = 'Edit')
                    );
                }
            }
        }
        List<Database.SaveResult> sr = Database.insert(apiRequestSharesToInsert,false);
    }
    
    /**
     * Auto update from api key details object*/
    public static void updateAPIKeyRequestFields(List<Api_Request__c> apiRequests){
        List<String> apiKeys = new List<String>();
        for(Api_Request__c apiRequest : apiRequests){
            String apiKey = apiRequest.Software_System__c+''+apiRequest.API_Type__c;
            if(!String.isBlank(apiRequest.X3rd_Party_Software__c)){
                apiKey += apiRequest.X3rd_Party_Software__c;
            }
            apiKeys.add(apiKey);
        }
        Map<String, API_Key_Details__c> apiKeyDetailsMap = new Map<String,API_Key_Details__c>();
        for(API_Key_Details__c apiScope : [SELECT Id, API_Scope_Key__c, Allow_Access_To__c, Licenses__c, New_Key_Format__c, Valid_for_No_of_days__c FROM API_Key_Details__c WHERE API_Scope_Key__c IN:apiKeys]){
            apiKeyDetailsMap.put(apiScope.API_Scope_Key__c, apiScope);
        }
        
        for(Api_Request__c apiRequest : apiRequests){
            String apiKey = apiRequest.Software_System__c+''+apiRequest.API_Type__c;
            if(!String.isBlank(apiRequest.X3rd_Party_Software__c)){
                apiKey += apiRequest.X3rd_Party_Software__c;
            }
            if(apiKeyDetailsMap.containsKey(apiKey))
            {
                apiRequest.Allow_Access_To__c = (apiKeyDetailsMap.get(apiKey)).Allow_Access_To__c;
                apiRequest.Licenses__c = (apiKeyDetailsMap.get(apiKey)).Licenses__c;
                apiRequest.New_Key_Format__c = (apiKeyDetailsMap.get(apiKey)).New_Key_Format__c;
                apiRequest.Valid_for_No_of_days__c = (apiKeyDetailsMap.get(apiKey)).Valid_for_No_of_days__c;
            } 
        }
    }
}
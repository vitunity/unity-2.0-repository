/*
Project No	  :	5142
Name          : APIKeySAPIntegrationController controller
Created By    : Jay Prakash (@varian)
Date          : 15th March, 2017
Purpose       : An Apex Controller to to get connect to SAP System using REST API Callout.
Updated By    : Yogesh Nandgadkar
Last Modified Reason  :   Updated to get the proper list of API Types
*/

global class APIKeySAPIntegrationController {	
	public List<String> apiTypeList = new List<String>();
	public List<String> softwareSystemList = new List<String>();
	public List<String> thirdPartySoftwareList = new List<String>();
    public static string authorizationHeader = sapAuthorizationHeader();

    public APIKeySAPIntegrationController() {        
    }
    
    public HTTPResponse sapAPICall(String sapFM){
    	HttpRequest sapRequestObj = new HttpRequest();
    	Http httpObj = new Http();
    	
    	// set the request method
        sapRequestObj.setMethod('GET');
        
        //Custom Settings -  Proxy PHP URL
        //String url = 'https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        PROXY_PHP_URL_DEV2__c proxyPHPUrl =  PROXY_PHP_URL_DEV2__c.getValues('Proxy PHP URL');  
        String url = proxyPHPUrl.ProxyURL__c; 
                
        system.debug('****url***'+url);
        
        // Adding the endpoint to the request
        sapRequestObj.setEndpoint(url);
        
        // Setting of Authorization Token
        sapRequestObj.setHeader('Authorization', authorizationHeader);
        
        //Custom Settting -  SAP Endpoint URL
        //String sapApiUrl =  'https://ehd.cis.varian.com:8251/zcallfmviarest/'+sapFM+'?format=json';
        SAP_ENDPOINT_URL_DEV2__c sapURL =  SAP_ENDPOINT_URL_DEV2__c.getValues('SAP Endpoint URL');         
        String sapApiUrl=sapURL.SAP_URL__c+sapFM+'?format=json';        
        
        system.debug('****sapFM***'+sapFM);
        system.debug('****sapApiUrl***'+sapApiUrl);
        
        sapRequestObj.setHeader('X-SAPWebService-URL', sapApiUrl);
        
        sapRequestObj.setHeader('Content-Type','application/json');
        sapRequestObj.setHeader('Accept','application/json');
        
        HTTPResponse httpResponse = httpObj.send(sapRequestObj);
    	return httpResponse;
    }  
    
    public List<String> getApiTypes(){
    	apiTypeList.clear();
        
        // Custom Settings for SAPFM Call for API Type
        INVOKE_SAP_FM_SETTINGS__c apiType =  INVOKE_SAP_FM_SETTINGS__c.getValues('API Type'); 
        String sapFM = apiType.FM_Call__c; //String sapFM = 'ZSAAS_PROVIDE_APITYPES';
                
    	HTTPResponse httpResponse = sapAPICall(sapFM);    	 
        System.debug('******httpResponse.getBody()******'+httpResponse.getBody());
    	JSONParser parser = JSON.createParser(httpResponse.getBody());
    	while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'APITYPES')) {
                     parser.nextToken();
                    
                    // Getting the value of API Names                 
                    String apiName=parser.getText();                                       
                    apiTypeList.add(apiName);
                    System.debug('******apiTypeList******'+apiTypeList);
            }                      
        }
        return apiTypeList;
    }
    
    public List<String> getSoftwareSystemList(){
    	softwareSystemList.clear();
        
        // Custom Settings for SAPFM Call for System Software
        INVOKE_SAP_FM_SETTINGS__c apiType =  INVOKE_SAP_FM_SETTINGS__c.getValues('API Type'); 
        String sapFM = apiType.FM_CAll__c; //String sapFM = 'ZSAAS_PROVIDE_APITYPES';
        //String sapFM ='ZSAAS_PROVIDE_APITYPES';
        
    	HTTPResponse httpResponse = sapAPICall(sapFM);    	
    	JSONParser parser = JSON.createParser(httpResponse.getBody());
    	while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'SOFTNAME')) {
                     parser.nextToken();                    
                    // Getting the value of API Names                 
                    String softwareName=parser.getText();                                       
                    softwareSystemList.add(softwareName);  
            }                      
        }        
    	return softwareSystemList;
    }
    
    public List<String> getThirdPartySoftwareList(){
    	thirdPartySoftwareList.clear();
        INVOKE_SAP_FM_SETTINGS__c thirdPartySoft =  INVOKE_SAP_FM_SETTINGS__c.getValues('THIRD PARTY SOFTWARE'); 
        String sapFM = thirdPartySoft.FM_Call__c; //String sapFM = 'ZSAAS_PROVIDE_3RDPARTYSOFT';
    	HTTPResponse httpResponse = sapAPICall(sapFM);
    	    	
    	JSONParser parser = JSON.createParser(httpResponse.getBody());
    	while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'SW_NAME')) {
                     parser.nextToken();                    
                    // Getting the value of API Names                 
                    String thirdPartySoftName=parser.getText(); 
                    thirdPartySoftwareList.add(thirdPartySoftName);
            }                      
        }
        System.debug(thirdPartySoftwareList);
    	return thirdPartySoftwareList;
    }
    // Method to return AuthToken Header used while API CallOut
    global static String sapAuthorizationHeader(){
        SAP_Login_Authorization__c uName =  SAP_Login_Authorization__c.getValues('Username'); 
        String userName = uName.LoginDetails__c; 
        
        SAP_Login_Authorization__c pwd =  SAP_Login_Authorization__c.getValues('Password'); 
        String password = pwd.LoginDetails__c; 
        
        Blob headerValue=Blob.valueOf(userName+':'+ password);         
        String authorizationHeaderValue = 'BASIC ' +EncodingUtil.base64Encode(headerValue);
        
		return authorizationHeaderValue;
    }
}
/*
Project No	:  5142
Name        :  APIKeySAPIntegrationController_Test Test Class
Created By  :  Jay Prakash
Date        :  11th April,2017
Purpose     :  Test class - To test the SFDC<=> SAP Callout to get the values for API Key Details
Updated By  :  Jay Prakash
*/
@isTest(SeeAllData = false)
private class APIKeySAPIntegrationController_Test{
   
    public static INVOKE_SAP_FM_SETTINGS__c csObj ; 
    public static PROXY_PHP_URL_DEV2__c proxyPHPURL ; 
    public static SAP_ENDPOINT_URL_DEV2__c sapURL ; 
    public static SAP_Login_Authorization__c sapLogInAuthUserName ; 
    public static SAP_Login_Authorization__c sapLogInAuthPwd ;     
    
    static testMethod void validatesapAPICall()    
    {
         //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
         //SAP Endopoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'THIRD PARTY SOFTWARE';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_3RDPARTYSOFT';
        
         insert proxyPHPURL;
         insert sapURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;        
         insert csObj ;         
        
         test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockGetThirdPartySoftwareList ());
         APIKeySAPIntegrationController obj = new APIKeySAPIntegrationController();
         obj.getThirdPartySoftwareList();
        // obj.sapAuthorizationHeader();
         test.stopTest();  
                 
    }
    
     static testMethod void validateGetSoftwareSystemList()    
    {
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
         //SAP Endopoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
        
         insert proxyPHPURL;
         insert sapURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;        
         insert csObj ;  
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
         insert csObj ;        
        
    	 test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockGetSoftwareSystemList());
         APIKeySAPIntegrationController obj = new APIKeySAPIntegrationController();
         obj.getSoftwareSystemList();
         test.stopTest();  
                 
    }
    
    static testMethod void validateGetApiTypes()    
    {
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
         //SAP Endopoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
        
         insert proxyPHPURL;
         insert sapURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;        
         insert csObj ;
         
    	 test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockGetApiTypes());
         APIKeySAPIntegrationController obj = new APIKeySAPIntegrationController();
         obj.getApiTypes();
         test.stopTest();  
                 
    }
    
    

}
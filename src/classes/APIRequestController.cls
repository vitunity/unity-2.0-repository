/*
Project No    : 5142
Name          : APIRequestController Controller
Created By    : Jay Prakash (@varian)
Date          : 10th March, 2017
Purpose       : An Apex Controller to save the API Request created by the myVarian Community Log In User
Updated By    : Yogesh Nandgadkar
Last Modified Reason  :   Updated to dislay the proper messageg at UI 
*/

public with sharing class APIRequestController {
    APIKeySAPIntegrationController apiKeySAPObj = new APIKeySAPIntegrationController();
    public static string authorizationHeader = APIKeySAPIntegrationController.sapAuthorizationHeader();
    public String softwareSystem{get;set;}
    public String apiType{get;set;}
    public String thirdPartySoft{get;set;}
    
    public Id LoggedInUserId{get;set;} 
    public String contactName{get;set;}     
    public String contactPhone{get;set;}
    public String contactEmail{get;set;}
    public String requesterName{get;set;}
    public String requesterEmail{get;set;}
    public String apiRequestReason{get;set;}
    
    Public Id conId{get;set;} 
    
    Public String accName{get;set;} 
    Public Id AccId{get;set;}     
    Public String customerID{get;set;}
    Public String apiKeyNumber{get;set;}

	public Boolean isGuest{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}

	// Added as part of New Request
	public String apiPurpose ='For Customer' ;  
    
    public APIRequestController(){
        getUserDetails();
        shows='none';
        isGuest = SlideController.logInUser();
        
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }
    }
    
    public Id getUserAuthId() {
        return UserInfo.getUserId();
    }
    
    public String getUserContactEmail() {
        String LoggedInUserId = getUserAuthId(); 
               
        // Retrieving ContactId from USER        
        List<User> userData = [ SELECT Id,contactId from USER where id=: LoggedInUserId];
        if (userData.size() > 0) {
            Id conId = userData[0].contactId;
        
            // Retrieving Contact Data from Contact based on ContactId
            List<Contact> contactData = [ SELECT Id, AccountId, Name, Phone, Email from CONTACT where id=: conId]; 
            if(!Test.isRunningTest()){
             return contactData[0].Email;
            }else{
              return  'test@varian.com';
            } 
        }
        return null;
    }
    
    public void getUserDetails(){
        // Retrieving User ID and Name
        LoggedInUserId = UserInfo.getUserId();  
        
        // Retrieving ContactId from USER        
        List<User> userData = [ SELECT Id,contactId from USER where id=: LoggedInUserId]; 
        conId = userData[0].contactId;
        
        // Retrieving Contact Data from Contact based on ContactId
       
        List<Contact> contactData = [ SELECT Id, AccountId, Name, Phone, Email from CONTACT where id=: conId]; 
        
           if(!contactData.isEmpty()){
               contactName = contactData[0].Name;                         
               AccId = contactData[0].AccountId;
               contactPhone = contactData[0].Phone;
               contactEmail = contactData[0].Email;
           }
            
          // Retrieving Account Name based on
           if(AccId !=NULL){
                List<Account> accountData = [SELECT Name,Ext_Cust_Id__c from Account where id =: AccId];
                accName = accountData[0].Name;
                customerID =accountData[0].Ext_Cust_Id__c;
           }
       
    }
  
    public List<SelectOption> getSoftwareSystemOptions() {
        List<String> softwareSystems = apiKeySAPObj.getSoftwareSystemList();        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select Software System'));
        for(String str:softwareSystems) {
            options.add(new SelectOption(str,str));
        }
        return options;    
    }
    
    public List<SelectOption> getApiTypesOptions() {
        List<String> apiTypeList = apiKeySAPObj.getApiTypes();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select API Type'));
        for(String str:apiTypeList) {
            options.add(new SelectOption(str,str));
        }     
        return options;        
    } 
    
    public List<SelectOption> getThirdPartSoftNameOptions() {
        List<String> thirdPartySoftwareList = apiKeySAPObj.getThirdPartySoftwareList();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select 3rd Party Software'));
        for(String str:thirdPartySoftwareList) {
            options.add(new SelectOption(str,str));
        }        
        return options;        
    } 
    
    public PageReference getAPIRequest(){ 
        //Added for to pass as param to SAP System
       ID recordID=LoggedInUserId;
       
        HttpRequest httpsReq = new HttpRequest();
        httpsReq.getBody();
        
        // set the request method
        httpsReq.setMethod('POST');

        //Custom Settings -  Proxy PHP URL
        //String url = 'https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        PROXY_PHP_URL_DEV2__c proxyPHPUrl =  PROXY_PHP_URL_DEV2__c.getValues('Proxy PHP URL');  
        String url = proxyPHPUrl.ProxyURL__c; 
        
        // Adding the endpoint to the request
        httpsReq.setEndpoint(url);
        
        // Setting of Authorization Token    
        httpsReq.setHeader('Authorization', authorizationHeader);
        
        //Custom Settings for "ZSAAS_CREATE_APIREQUEST"
        INVOKE_SAP_FM_SETTINGS__c downloadAPIKey =  INVOKE_SAP_FM_SETTINGS__c.getValues('Create API Request'); 
       	String sapFM = downloadAPIKey.FM_Call__c; //String sapFM = 'ZSAAS_CREATE_APIREQUEST'; 
        
        //Custom Setting -  SAP Endpoint URL       
        SAP_ENDPOINT_URL_DEV2__c sapURL =  SAP_ENDPOINT_URL_DEV2__c.getValues('SAP Endpoint URL');         
        String sapApiUrl=sapURL.SAP_URL__c+sapFM+'?format=json';  
        
        httpsReq.setHeader('X-SAPWebService-URL', sapApiUrl);
        
        httpsReq.setHeader('Content-Type','application/json');
        httpsReq.setHeader('Accept','application/json');
        
        String IM_SOFT3RDPART_Value = '';
        String IM_APIREQREASON_Value = '';
        
        if( '3RD PARTY ACCESS' == apiType ){
            IM_SOFT3RDPART_Value = thirdPartySoft;
        }
        
        if( ( apiType == 'FULL ACCESS TO DB' ) || ( apiType == '3RD PARTY ACCESS'  && thirdPartySoft == 'OTHERS' ) ){           
            IM_APIREQREASON_Value = apiRequestReason;
        }
        
       String jsonBody = '{ "IM_CHANGEDBY": "'+contactEmail+'","IM_CUSTOMNAME": "'+accName+'", "IM_CUSTOMERID": "'+customerID +'", "IM_CUCONNAME": "'+ contactName +'", "IM_CUSTOMPHONE": "'+ contactPhone +'", "IM_CUSTEMAIL": "'+ contactEmail +'", "IM_REQNAME": "'+ requesterName +'", "IM_REQEMAIL": "'+ requesterEmail +'", "IM_SOFTNAME": "'+ softwareSystem +'", "IM_APITYPES": "'+ apiType +'", "IM_SOFT3RDPART": "'+ IM_SOFT3RDPART_Value +'", "IM_APIREQREASON": "'+ IM_APIREQREASON_Value +'", "IM_APIPURPOSE": "'+ apiPurpose +'"}'; 
       // String jsonBody ='accName=value+&customerID=value+&contactName=value+&LoggedInUserEmail=value+&softwareSystem=value+&apiType=value+&thirdPartySoft';                     
             
         //After Header - Add these code
         if(jsonBody != null )
         { 
             httpsReq.setBody(jsonBody);
             httpsReq.setHeader('Content-length',string.valueOf(jsonBody.length()));
         }        
        HttpResponse res = new http().send(httpsReq);        
        JSONParser parser = JSON.createParser(res.getBody());
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'EX_APIREQID')) {
                     parser.nextToken();
                    
                    // Getting the value of API Key                 
                    apiKeyNumber=parser.getText(); 
            } 
        }       
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your New API Request is registred. API Request ID is: ' + apiKeyNumber )); 
        return new pagereference('/apex/APIRequestForm?apiKeyNumber='+apiKeyNumber);         
    }
    
    public Boolean getDoesErrorExist() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR);
    } 

    public Boolean getDoesInfoExist() {
        return ApexPages.hasMessages(ApexPages.Severity.INFO);
    }
}
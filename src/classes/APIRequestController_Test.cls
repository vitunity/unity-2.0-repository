/*
Project No    : 5142
Name          : APIRequestController_Test
Created By  : Jay Prakash 
Date        : 11th April, 2017
Purpose     : Test class - To Verify the Functionality of Creating New API Request for Varian Community Users 
Updated By  : Jay Prakash [ To Improve the code Coverage ]
Updated Date : 05th May, 2017  
*/
@isTest(SeeAllData = false)
private class APIRequestController_Test{
   
    public static INVOKE_SAP_FM_SETTINGS__c csObj ; 
    public static PROXY_PHP_URL_DEV2__c proxyPHPURL ;
    public static SAP_ENDPOINT_URL_DEV2__c sapURL;
    public static SAP_Login_Authorization__c sapLogInAuthUserName ; 
    public static SAP_Login_Authorization__c sapLogInAuthPwd ;
       
    static testMethod void validatesapUnit()    
    {    
          Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];        
           
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'Create API Request';
         csObj.FM_Call__c = 'ZSAAS_CREATE_APIREQUEST';
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert csObj;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;
        
          System.RunAs(userInfo ) {
            test.startTest();              
              APIRequestController  obj = new APIRequestController ();    
              obj.getDoesErrorExist();
              obj.getDoesInfoExist();         
             test.stopTest(); 
          }   
    }    
  
    static testMethod void validateGetAPIRequest()    
    {
       Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
       User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];          
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'Create API Request';
         csObj.FM_Call__c = 'ZSAAS_CREATE_APIREQUEST';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';   
        
         insert csObj;
         insert proxyPHPURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;  
         insert sapURL;
        
          System.RunAs(userInfo ) {
             test.startTest();
              Test.setMock(HttpCalloutMock.class, new MockGetAPIRequest());
              APIRequestController  obj = new APIRequestController ();
              obj.getAPIRequest();
              
              test.stopTest();              
               }     
    }
    
    static testMethod void validateGetUserContactEmail()    
    {    
          Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
          
          //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;
        
          System.RunAs(userInfo ) {
            test.startTest();

              Account accObj = new Account();
              accObj.name = 'Test';
              accObj.Country__c= 'India';
              accObj.Ext_Cust_Id__c = 'test' ;
              insert accobj ; 
              
              Contact  conObj = new Contact (AccountId = accObj.id,FirstName='FName',LastName = 'portalTestUser',Phone='1234567890', Email='test123@varian.com', MobilePhone='123114567890');
              insert  conObj ;
              
             APIRequestController  obj = new APIRequestController ();
             obj.getUserContactEmail();
             
             test.stopTest();               
          }   
    }
    
    static testMethod void validateGetSoftwareSystemOptions()    
    {
    
          Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
          
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
         
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';                
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert csObj ;
         insert proxyPHPURL ;
         insert sapURL;
         insert sapLogInAuthUserName ;
         insert sapLogInAuthPwd ;
          
          System.RunAs(userInfo ) {
            test.startTest();

              Test.setMock(HttpCalloutMock.class, new MockGetSoftwareSystemList());
              
             APIRequestController  obj = new APIRequestController ();
             
             obj.getSoftwareSystemOptions();
             test.stopTest();  
             
          }   
    }
     
    static testMethod void validateGetApiTypesOptions()    
    {    
          Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
          
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';                
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert csObj ;
         insert proxyPHPURL ;
         insert sapURL;
         insert sapLogInAuthUserName ;
         insert sapLogInAuthPwd ;        
          
          System.RunAs(userInfo ) {
            test.startTest();

              Test.setMock(HttpCalloutMock.class, new MockGetApiTypes());
              
             APIRequestController  obj = new APIRequestController ();
             
             obj.getApiTypesOptions();
             test.stopTest();  
             
          }   
    }
    
    static testMethod void validategetThirdPartSoftNameOptions()    
    {    
          Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
          
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'THIRD PARTY SOFTWARE';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_3RDPARTYSOFT';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';                
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert csObj ;
         insert proxyPHPURL ;
         insert sapURL;
         insert sapLogInAuthUserName ;
         insert sapLogInAuthPwd ;
          
          System.RunAs(userInfo ) {
             test.startTest();

             Test.setMock(HttpCalloutMock.class, new MockGetThirdPartySoftwareList());
              
             APIRequestController  obj = new APIRequestController ();
             
             obj.getThirdPartSoftNameOptions();
             test.stopTest();  
             
          }   
    }
        
    static testMethod void validateAPIRequestController()    
    {    
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profileObj = [Select Id from Profile where name = 'System Administrator'];
        
        User portalAccountOwner = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profileObj.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner);     
        
        System.RunAs(portalAccountOwner) {
            Test.startTest();  
            
                 csObj = new INVOKE_SAP_FM_SETTINGS__c();
                 csObj.name = 'THIRD PARTY SOFTWARE';
                 csObj.FM_Call__c = 'ZSAAS_PROVIDE_3RDPARTYSOFT';
                
                //Proxy PHP URL
                 proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
                 proxyPHPURL.name='Proxy PHP URL';
                 proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
                
                // For SAP Endpoint URL
                 sapURL = new SAP_ENDPOINT_URL_DEV2__c();
                 sapURL.name='SAP Endpoint URL';
                 sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';                
                
                //SAP Log In Auth Token
                 sapLogInAuthUserName = new SAP_Login_Authorization__c();
                 sapLogInAuthUserName.name='Username';
                 sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
                
                 sapLogInAuthPwd = new SAP_Login_Authorization__c();
                 sapLogInAuthPwd.name='Password';           
                 sapLogInAuthPwd.LoginDetails__c='Api@5142';
                
                 insert csObj ;
                 insert proxyPHPURL ;
                 insert sapURL;
                 insert sapLogInAuthUserName ;
                 insert sapLogInAuthPwd ;
            
                Account portalAccount = new Account(
                    Name = 'TestAccount',
                    Country__c= 'India',
                    Ext_Cust_Id__c = 'test' ,
                    OwnerId = portalAccountOwner.Id
                );
                Database.insert(portalAccount);
                        
                //Create contact
                Contact contactObj = new Contact(
                    FirstName = 'Test',
                        Lastname = 'McTesty',
                    AccountId = portalAccount.Id,
                        Email = System.now().millisecond() + 'test@test.com'
                );
                Database.insert(contactObj);
                
               APIRequestController  obj = new APIRequestController (); 
               obj.thirdPartySoft  = 'test';
               obj.apiRequestReason = 'test';
               obj.AccId      = portalAccount.id ;
                
            Test.stopTest();  
         }           
    }  
}
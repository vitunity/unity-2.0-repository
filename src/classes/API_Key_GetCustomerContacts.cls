@RestResource(urlMapping='/SitePartnerAndSoldToContacts/*')
global with sharing class API_Key_GetCustomerContacts {
    @Httpget
    global static List<Contact> doGet() {    
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String soldToCode = req.params.get('soldToCode');
        String sitePartnerCode = req.params.get('sitePartnerCode');
          
        List<Contact> soldToContacts = new List<Contact>();
        List<Contact> sitePartnerContacts = new List<Contact>();
        
        if(!String.isBlank(soldToCode)){
            soldToContacts = [
                SELECT Id,Name,EMail,Phone
                FROM Contact
                WHERE Account.Ext_Cust_Id__c =:soldToCode 
            ];
        }
        
        if(!String.isBlank(sitePartnerCode) && sitePartnerCode!=soldToCode){
            sitePartnerContacts = [
                SELECT Id,Name,EMail,Phone
                FROM Contact
                WHERE Account.ERP_Site_Partner_Code__c =:sitePartnerCode 
            ];
        } 
        
        soldToContacts.addAll(sitePartnerContacts);
        
        return soldToContacts;        
    }
}
@isTest 
private class API_Key_GetCustomerContactsTest {
    static testMethod void testGetCustomerContacts() {
	   
		Account acct = TestUtils.getAccount();
		acct.Name = 'APIKEY MGMT ACCOUNT';
		acct.Ext_Cust_Id__c = 'TESTSOLDTOID';
		
		Account sitePartnerAccount = TestUtils.getAccount();
		sitePartnerAccount.Name = 'APY KEY SITE PARTNER ACCOUNT';
		sitePartnerAccount.ERP_Site_Partner_Code__c = 'TESTSITEPARTNERID';
	    insert new List<Account>{acct,sitePartnerAccount};
		
		Contact contact1 = TestUtils.getContact();
		contact1.AccountId = acct.Id;
		
		Contact contact2 = TestUtils.getContact();
		contact2.AccountId = sitePartnerAccount.Id;
		insert new List<Contact>{contact1,contact2};
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
    
		req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/SitePartnerAndSoldToContacts?soldToCode=TESTSOLDTOID&sitePartnerCode=TESTSITEPARTNERID';
		req.httpMethod = 'GET';
		req.params.put('soldToCode','TESTSOLDTOID');
		req.params.put('sitePartnerCode','TESTSITEPARTNERID');
		RestContext.request = req;
		RestContext.response = res;
		
		API_Key_GetCustomerContacts.doGet();
    }
}
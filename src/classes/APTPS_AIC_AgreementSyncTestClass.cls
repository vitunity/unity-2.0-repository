@istest(SeeAllData=true)
public class APTPS_AIC_AgreementSyncTestClass {
	public static testMethod void runPositiveTestCases() {
        Test.setMock(HttpCalloutMock.class, new APTPS_SyncSFDCtoAICMock());
        
        User u1 = [SELECT Id, Name FROM User LIMIT 1];
        System.debug('=========> Run As ' + u1.Name);
               
        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Indirect Procurement Request' and sObjectType = 'Apttus__APTS_Agreement__c' LIMIT 1];
		System.debug('=========> Record Type ' + recordType.Name);
                    
        //Run As U1
        //System.RunAs(u1){
            
            Cost_Center__c costCenter = new Cost_Center__c(Name='Test cost center');
            insert costCenter;
            
            Vendor__c vendor = new Vendor__c(Name='Test cost center');
            insert vendor;
                       
            Apttus__APTS_Agreement__c agrrInst = new Apttus__APTS_Agreement__c(
                Request_Status__c='Active', 
                AIC_Record_ID__c='9c3a669c-5d27-e811-80c2-0004ffb079d5',
            	Planned_Closure_Date__c = Date.valueOf('2008-03-31 00:00:00'), 
                Cost_Center__c = costCenter.Id,
                Vendor__c = vendor.Id,
                Business_Owner__c = u1.Id,
                Varian_Contact__c = u1.Id,
                //Procurement_Assigned_To__c = u1.Id,
                RecordTypeId= recordType.Id
            );
            insert agrrInst;
            
            List<Apttus__APTS_Agreement__c> agreements = [SELECT Id From Apttus__APTS_Agreement__c WHERE id = :agrrInst.Id ];//Where AIC_Record_ID__c != null and Request_Status__c = 'Active'];
            System.debug('=========> Agreement Selected = ' + agreements.size());
            
            string recordId = agreements[0].Id;
            
            List<Apttus__APTS_Agreement__c> agreementsToUpdate = new List<Apttus__APTS_Agreement__c>();
            Apttus__APTS_Agreement__c agrr = new Apttus__APTS_Agreement__c(id=agreements[0].Id, Request_Status__c='In_Process');
            agreementsToUpdate.add(agrr);
        
            System.debug('=========> Agreement Updated Called ');
            update agreementsToUpdate;
            System.debug('=========> Agreement Updated ');
            
            List<Apttus__APTS_Agreement__c> updatedAgreements = [Select ID, Request_Status__c from Apttus__APTS_Agreement__c Where Id = :recordId];
            for(Apttus__APTS_Agreement__c a : updatedAgreements){
                System.assertEquals('In_Process', a.Request_Status__c, 'This was supposed to match if TickerSymbol changed');
            }        	
    	//}
 	}
}
public with sharing class APTPS_Constants {
    public static final string AIC_TOKEN_URL = 'https://login.windows.net/varian.com/oauth2/token';
    public static final string AIC_UPSERT_AGREEMENT_ACTION = '/api/generic/v1/objects/clm_Agreement/upsert?UpsertKey=Id';
    public static final string AIC_GET_OBJECT_ID_BY_SFDC_ID_ACTION_URL = '/api/generic/v1/search/advance/{0}?query=';
    public static final string AIC_GET_OBJECT_ID_BY_SFDC_ID_ACTION_QUERY_PARAMETER = '\'{\'"select":["Id"],"offset":1,"limit":1,"orderby":[], "filter":[\'{\'"field":"ExternalId","operator":"Equal","value":"{0}"\'}\'], "getcount":false\'}\'';
    
    
    public static string AIC_APP_URL = '';
    public static string AIC_CLIENT_ID = '';
    public static string AIC_CLIENT_SECRET = '';    
    
    public static final string CONTENT_TYPE_URL_ENCODED = 'application/x-www-form-urlencoded';
    public static final string CONTENT_TYPE_JSON = 'application/json;charset=utf-8';
    
    public static final string REST_GET_METHOD = 'GET';
    public static final string REST_POST_METHOD = 'POST';
    
    public static final string AIC_USER_OBJECT = 'crm_User';
    public static final string AIC_COST_CENTER_OBJECT = 'CP01_CostCenter';
    public static final string AIC_VENDOR_OBJECT = 'CP01_Vendor';
    public static final string AIC_RECORD_TYPE_OBJECT = 'cmn_RecordType';
    
    public static void Init(){
        system.debug('APTPS_Constants.Init Called');
        AIC_API_Settings__c aicSetting = AIC_API_Settings__c.getValues('AIC API Settings');
		APTPS_Constants.AIC_APP_URL = aicSetting.AIC_APP_URL__c;
        APTPS_Constants.AIC_CLIENT_ID = aicSetting.AIC_CLIENT_ID__c;
        APTPS_Constants.AIC_CLIENT_SECRET = aicSetting.AIC_CLIENT_SECRET__c;
    }
}
public class APTPS_FutureAgreementSyncToAIC {
    static string TAG = 'AICSync APTPS_FutureAgreementSyncToAIC';
    
    @future(callout=true)
    public static void send(String jsonDataToSend)
    {
        system.debug(TAG + '.send jsonDataToSend: ' + jsonDataToSend);
        Apttus__APTS_Agreement__c agreement = (Apttus__APTS_Agreement__c)JSON.deserialize(jsonDataToSend, Apttus__APTS_Agreement__c.class);
        
        APTPS_SyncSFDCtoAIC sync = new APTPS_SyncSFDCtoAIC();
      	APTPS_SyncSFDCtoAIC.AgreementWrapperForAIC dataToSend = new APTPS_SyncSFDCtoAIC.AgreementWrapperForAIC(agreement);
		system.debug(TAG + '.send dataToSend: ' + dataToSend);
      	
        List<Vendor__c> vendors = new List<Vendor__c>();
        List<Cost_Center__c> costCenters = new List<Cost_Center__c>();
        List<User> usersToUpdate = new List<User>();
        
        List<Id> lstUserIds = new List<Id>{agreement.Apttus__Requestor__c, agreement.Business_Owner__c, agreement.Varian_Contact__c, agreement.Procurement_Assigned_To__c};
        Map<Id, User> users = new Map<Id, User>([SELECT Id, Apttus_ExternalId__c FROM User WHERE ID IN :lstUserIds]);
        
        /*if (String.isNotBlank(agreement.Apttus__Requestor__c)) {
        	dataToSend.RequestorId = getUserAicID(users.get(agreement.Apttus__Requestor__c), sync, usersToUpdate);
        } */
        string businessOwner='', varianContact='', procurementAssignedTo='', vendorId='', costCenterId='';
        if (String.isNotBlank(agreement.Business_Owner__c)) {
            businessOwner = getUserAicID(users.get(agreement.Business_Owner__c), sync, usersToUpdate);
        	dataToSend.CP01_BusinessOwner = businessOwner;
        }
        
        if (String.isNotBlank(agreement.Varian_Contact__c)) {
            varianContact = getUserAicID(users.get(agreement.Varian_Contact__c), sync, usersToUpdate);
        	dataToSend.CP01_VarianContact = varianContact;
        }
        
        if (String.isNotBlank(agreement.Procurement_Assigned_To__c)) {
            procurementAssignedTo  = getUserAicID(users.get(agreement.Procurement_Assigned_To__c), sync, usersToUpdate);
        	dataToSend.CP01_ProcurementAssignedTo = procurementAssignedTo;
        }
        
        if (string.isNotEmpty(agreement.Vendor__c)) {
            vendorId =  String.valueOf(agreement.Vendor__c);
            Vendor__c vendor = [SELECT AIC_Record_ID__c FROM Vendor__c WHERE ID =: vendorId];
            if (string.isBlank(vendor.AIC_Record_ID__c)) {
                vendor.AIC_Record_ID__c = sync.getObjectIdFomAIC(APTPS_Constants.AIC_VENDOR_OBJECT,  vendorId);
                vendors.add(vendor);
            }
            vendorId = vendor.AIC_Record_ID__c;
            dataToSend.CP01_Vendor = vendorId;
        }
        
        if (string.isNotEmpty(agreement.Cost_Center__c)) {
            costCenterId =  String.valueOf(agreement.Cost_Center__c);
            Cost_Center__c costCenter = [SELECT AIC_Record_ID__c FROM Cost_Center__c WHERE ID =: costCenterId];
            if (string.isBlank(costCenter.AIC_Record_ID__c)) {
                costCenter.AIC_Record_ID__c = sync.getObjectIdFomAIC(APTPS_Constants.AIC_COST_CENTER_OBJECT,  costCenterId);
                costCenters.add(costCenter);
            }
            costCenterId = costCenter.AIC_Record_ID__c;
            dataToSend.CP01_CostCenter = costCenterId;
        }
        
        if(businessOwner == '' && agreement.Business_Owner__c != null)
        {
       		system.debug(TAG + '.send couldnot sync the record to AIC Portal. selected BusinessOwner ' + String.valueOf(agreement.Business_Owner__c) + ' is not present in AIC Users.');
        }else if(varianContact == '' && agreement.Varian_Contact__c != null)
        {
       		system.debug(TAG + '.send couldnot sync the record to AIC Portal. selected VarianContact ' + String.valueOf(agreement.Varian_Contact__c) + ' is not present in AIC Users.');
        }else if(procurementAssignedTo == '' && agreement.Procurement_Assigned_To__c != null)
        {
       		system.debug(TAG + '.send couldnot sync the record to AIC Portal. selected Procurement Assigned To ' + String.valueOf(agreement.Procurement_Assigned_To__c) + ' is not present in AIC Users.');
        }else if(vendorId == '' && agreement.Vendor__c != null)
        {
       		system.debug(TAG + '.send couldnot sync the record to AIC Portal. selected Vendor ' + String.valueOf(agreement.Vendor__c) + ' is not present in AIC Vendors.');
        }else if(costCenterId == '' && agreement.Cost_Center__c!=null)
        {
       		system.debug(TAG + '.send couldnot sync the record to AIC Portal. selected Cost Center ' + String.valueOf(agreement.Cost_Center__c) + ' is not present in AIC Cost Centers.');
        }else{
        	sync.sendAgreementData(dataToSend);
        }
        
        if(!Test.isRunningTest()){
            if (usersToUpdate.size() > 0) update usersToUpdate;
            if (vendors.size() > 0) update vendors;
            if (costCenters.size() > 0) update costCenters;
    	}
    }
    
    private static String getUserAicID (User user, APTPS_SyncSFDCtoAIC sync, List<User> usersToUpdate) { 
        if(user!=null){
            if (string.isBlank(user.Apttus_ExternalId__c)) {
                user.Apttus_ExternalId__c = sync.getObjectIdFomAIC(APTPS_Constants.AIC_USER_OBJECT,  String.valueOf(user.Id));
                usersToUpdate.add(user);
            }
            return user.Apttus_ExternalId__c;
        }
        return null;
    }
}
public with sharing class APTPS_SyncSFDCtoAIC {
    static string TAG = 'AICSync APTPS_SyncSFDCtoAIC';
    
    public String Token { get; set; }
    private Http Http { get; set; }
    
	public APTPS_SyncSFDCtoAIC() {
        APTPS_Constants.Init();
        this.Http = new Http();
        this.getToken();
    }
    
    public void getToken() {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(APTPS_Constants.AIC_TOKEN_URL);
        request.setMethod(APTPS_Constants.REST_POST_METHOD);
        request.setHeader('Content-Type', APTPS_Constants.CONTENT_TYPE_URL_ENCODED);
        request.setBody('grant_type=client_credentials'
                        + '&client_id=' + APTPS_Constants.AIC_CLIENT_ID 
                        + '&client_secret=' + APTPS_Constants.AIC_CLIENT_SECRET
                        + '&resource=' + APTPS_Constants.AIC_CLIENT_ID);
        system.debug(TAG + '.getToken request:' + request);
        HttpResponse response = this.Http.send(request);
        system.debug(TAG + '.getToken response:' + response);
        
        if (response.getStatusCode() == 200 || response.getStatusCode() == 202) {
            Map<String, Object> jsonResponse = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            this.Token = 'Bearer ' + jsonResponse.get('access_token');
            //system.debug(TAG + '.getObjectIdFomAIC Token:' + this.Token);
        } else {
            system.debug(TAG + '.getToken ErrorStatusCode:' + response.getStatusCode() + ' ErrorStatus:' + response.getStatus());
            system.debug(TAG + '.getToken Message:' + response.getBody());
        }
    }
    
    public void sendAgreementData(AgreementWrapperForAIC toSend) {
        HttpRequest request = new HttpRequest();
        request.setEndpoint(APTPS_Constants.AIC_APP_URL + APTPS_Constants.AIC_UPSERT_AGREEMENT_ACTION);
        request.setMethod(APTPS_Constants.REST_POST_METHOD);
		request.setHeader('Content-Type', APTPS_Constants.CONTENT_TYPE_JSON);
        request.setHeader('Authorization', this.Token);
        request.setHeader('App-Url', APTPS_Constants.AIC_APP_URL);
		request.setHeader('Accept', 'text/html');
        
        system.debug(TAG + '.getToken request:' + request);
        // Set the body as a JSON
        string body = '[' + json.serialize(toSend) + ']';
        system.debug(TAG + '.sendAgreementData Body:' + body);
        request.setBody(body);
        HttpResponse response = this.Http.send(request);
        system.debug(TAG + '.getToken response:' + response);

        // Parse the JSON response
        if (response.getStatusCode() == 200 || response.getStatusCode() == 202) {
            system.debug(TAG + '.sendAgreementData Record Updated in AIC:' + response.getBody());
        } else {
            system.debug(TAG + '.sendAgreementData ErrorStatusCode:' + response.getStatusCode() + ' ErrorStatus:' + response.getStatus());
            system.debug(TAG + '.sendAgreementData Message:' + response.getBody());
        }
    }
    
    public String getObjectIdFomAIC(string objectName, string sfdcId) {
        HttpRequest request = new HttpRequest();
        string url = String.format(APTPS_Constants.AIC_GET_OBJECT_ID_BY_SFDC_ID_ACTION_URL, new String[]{objectName});
        string queryParamasEncoded = EncodingUtil.urlEncode(String.format(APTPS_Constants.AIC_GET_OBJECT_ID_BY_SFDC_ID_ACTION_QUERY_PARAMETER, new String[]{sfdcId}), 'UTF-8');
        
        request.setEndpoint(APTPS_Constants.AIC_APP_URL + url + queryParamasEncoded);
        request.setMethod(APTPS_Constants.REST_GET_METHOD);
        request.setHeader('Content-Type', APTPS_Constants.CONTENT_TYPE_JSON);
        request.setHeader('Authorization', this.Token);
        request.setHeader('App-Url', APTPS_Constants.AIC_APP_URL);
        request.setHeader('App-Url', APTPS_Constants.AIC_APP_URL);
		request.setHeader('Accept', 'text/html');

        Http http = new Http();
        system.debug(TAG + '.getObjectIdFomAIC request:' + request);
        HttpResponse response = http.send(request);
		system.debug(TAG + '.getObjectIdFomAIC response:' + response);
        // Parse the JSON response
        if (response.getStatusCode() == 200 || response.getStatusCode() == 202) {
            System.debug(response.getBody());
            Map <String, Object> deserilized = (Map <String, Object>)JSON.deserializeUntyped(response.getBody());
            try {
                return (String)((Map<String, Object>)((List<Object>)deserilized.get('SerializedResultEntities')).get(0)).get('Id');
            }
            catch (exception e){
                system.debug(TAG + '.getObjectIdFomAIC Exception:' + e.getMessage());
                return '';
            }
        }else{
            system.debug(TAG + '.getObjectIdFomAIC ErrorStatusCode:' + response.getStatusCode() + ' ErrorStatus:' + response.getStatus());
            system.debug(TAG + '.getObjectIdFomAIC Message:' + response.getBody());
        }
        return '';
    }
    
    public class AgreementWrapperForAIC {
        //public string RequestorId { get; set; }
        //public string CP01_RequestorIdForSFDC { get; set; }
        public string CP01_BusinessOwner { get; set; }
        public string CP01_BusinessOwnerForSFDC { get; set; }
        public string CP01_VarianContact { get; set; }
        public string CP01_VarianContactForSFDC { get; set; }
        public string CP01_CostCenter { get; set; }
        public string CP01_CostCenterForSFDC { get; set; }
        public string CP01_Vendor { get; set; }
        public string CP01_ProcurementAssignedTo { get; set; }
        
        public string Id { get; set; }
        public Id ExternalId { get; set; }
        public DateTime ExternalLastUpdatedOn { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Date CP01_StartDate { get; set; }
        public string CP01_ReportingGroup { get; set; }
        public string CP01_VarianGroup { get; set; }
        public string CP01_RequestStatus { get; set; }
        public Date CP01_PlannedClosureDate { get; set; }
        public Decimal CP01_EstimatedSpendAtIntake { get; set; }
        public string CP01_CompanyCode { get; set; }
        public string CP01_GeoCoverage { get; set; }
        public string CP01_Comments { get; set; }
        public string CP01_RequestorLocation { get; set; }
        
        public string CP01_PurchaseElements { get; set; }
        public string CP01_PurchaseElementsForSFDC { get; set; }
        public string CP01_IntellectualProperty { get; set; }
		public string CP01_IntellectualPropertyForSFDC { get; set; }
        public string CP01_RequestNumberFromSFDC { get; set; }

        public AgreementWrapperForAIC(Apttus__APTS_Agreement__c agreement) {    
            //this.RequestorId = agreement.Apttus__Requestor__c;
            //this.CP01_RequestorIdForSFDC = agreement.Apttus__Requestor__c;
            this.CP01_BusinessOwner = agreement.Business_Owner__c;
            this.CP01_BusinessOwnerForSFDC = agreement.Business_Owner__c;
            this.CP01_VarianContact = agreement.Varian_Contact__c;
           	this.CP01_VarianContactForSFDC = agreement.Varian_Contact__c;
           	this.CP01_CostCenter = agreement.Cost_Center__c;           
            this.CP01_CostCenterForSFDC = agreement.Cost_Center__c;           
            this.CP01_Vendor = agreement.Vendor__c;
            this.CP01_ProcurementAssignedTo = agreement.Procurement_Assigned_To__c;
            
            this.ExternalId = agreement.Id;
            this.ExternalLastUpdatedOn = agreement.LastModifiedDate;
            this.Name = agreement.Name;
            this.Description = agreement.Apttus__Description__c;
            this.CP01_ReportingGroup = agreement.Reporting_Group__c;
            this.CP01_VarianGroup = agreement.Varian_Group__c;
            this.CP01_StartDate = agreement.Start_Date__c;
            this.CP01_RequestStatus = agreement.Request_Status__c;
            this.CP01_PlannedClosureDate = agreement.Planned_Closure_Date__c;
            if(agreement.Purchase_Elements__c != null){
            	this.CP01_PurchaseElements = agreement.Purchase_Elements__c.replace(';', ',');
        	}
            this.CP01_PurchaseElementsForSFDC = agreement.Purchase_Elements__c;
            this.CP01_EstimatedSpendAtIntake = agreement.Estimated_Spend_at_Intake__c;
            this.CP01_CompanyCode = agreement.Company_Code__c;
            this.CP01_GeoCoverage = agreement.Geo_Coverage__c;
            this.CP01_Comments = agreement.Comments__c;
            this.CP01_RequestorLocation = agreement.Requestor_Location__c;
            if(agreement.This_vendor_have_access_to_restrict_info__c != null){
            	this.CP01_IntellectualProperty = agreement.This_vendor_have_access_to_restrict_info__c.replace(';', ',');
            }
            this.CP01_IntellectualPropertyForSFDC = agreement.This_vendor_have_access_to_restrict_info__c;
            this.CP01_RequestNumberFromSFDC = agreement.Apttus__FF_Agreement_Number__c;
            this.Id = agreement.AIC_Record_ID__c;
        }
    }
}
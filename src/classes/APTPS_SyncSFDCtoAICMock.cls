@isTest
global class APTPS_SyncSFDCtoAICMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest request) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        // System.assertEquals('http://example.com/example/test', request.getEndpoint());
        // System.assertEquals('GET', request.getMethod());
        
        HttpResponse res = new HttpResponse();
        if (request.getEndpoint().contains('oauth2/token')) {
            // Create a fake getTOKEN response
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"token_type": "Bearer","expires_in": "3599","ext_expires_in": "0","expires_on": "1522254791","not_before": "1522250891","resource": "371f725c-a97d-4024-af98-123db451f289","access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkZTaW11RnJGTm9DMHNKWEdtdjEzbk5aY2VEYyIsImtpZCI6IkZTaW11RnJGTm9DMHNKWEdtdjEzbk5aY2VEYyJ9.eyJhdWQiOiIzNzFmNzI1Yy1hOTdkLTQwMjQtYWY5OC0xMjNkYjQ1MWYyODkiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC9jNDlkOWM0OS00YjExLTRjY2QtYjEzNy03MmY4OGM2OGEyNTIvIiwiaWF0IjoxNTIyMjUwODkxLCJuYmYiOjE1MjIyNTA4OTEsImV4cCI6MTUyMjI1NDc5MSwiYWlvIjoiWTJOZ1lIaS83ZmJSbklzcDhjNkxpKzk5dU5xUkNnQT0iLCJhcHBpZCI6IjM3MWY3MjVjLWE5N2QtNDAyNC1hZjk4LTEyM2RiNDUxZjI4OSIsImFwcGlkYWNyIjoiMSIsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0L2M0OWQ5YzQ5LTRiMTEtNGNjZC1iMTM3LTcyZjg4YzY4YTI1Mi8iLCJvaWQiOiJlZDVhZDc5Yi1mZGEzLTQ3MjctOGQ2ZC1hNDM5Y2RkNTY2NzUiLCJzdWIiOiJlZDVhZDc5Yi1mZGEzLTQ3MjctOGQ2ZC1hNDM5Y2RkNTY2NzUiLCJ0aWQiOiJjNDlkOWM0OS00YjExLTRjY2QtYjEzNy03MmY4OGM2OGEyNTIiLCJ1dGkiOiJUN1A5ZGMwN3pFaTBORDZVZENBQ0FBIiwidmVyIjoiMS4wIn0.ZWARrIJcuj8dJ9pwdMp7p-SokUsdUC1lFHz-kCZKWvJPe22gEi1vY5lod4txgjDhTWsxMINy_wIzkh-np_xvLjryBywRTwjQ_k0Zx6VeK-9v533rEvqWzg9UhthA8PJGIUeelmQPCwDrZimoPUvHjiGOBXwO24TALIj3w7Oe7akOHWWy4NU3zwqApxQ5KwBo4-qK2Rd7v1KUoMUPDZ1F8AMlEjWh6o8Flfh_FdMorxiDOVz42JzVqcBj_r9MPO8XKUlpbhl3evJ3G6IYlx4NwSED8fLL9wjCV0Ak5wO0hrfW9mXcYtVRRqAPH0oIv_PgRFaVSMlVRbaczMNyCPcdeg"}');
            res.setStatusCode(200);
        }
        else if(request.getEndpoint().contains('clm_Agreement')) {
            // Create a fake sendAgreementData response
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}');
            res.setStatusCode(200);
		}
        else if(request.getEndpoint().contains(APTPS_Constants.AIC_VENDOR_OBJECT)) {
            // Create a fake sendAgreementData response
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"SerializedResultEntities": [{"Id": "cbf9c483-9018-e811-80c2-0004ffb079d5","Name": "Standav","RowVersion": "AAAAAAACp4A="}],"TotalNumberOfRecords": 19512,"MoreRecords": false}');
            res.setStatusCode(200);
		}
        else if(request.getEndpoint().contains(APTPS_Constants.AIC_COST_CENTER_OBJECT)) {
            // Create a fake sendAgreementData response
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"SerializedResultEntities": [{"Id": "cbf9c483-9018-e811-80c2-0004ffb079d5","Name": "Standav","RowVersion": "AAAAAAACp4A="}],"TotalNumberOfRecords": 19512,"MoreRecords": false}');
            res.setStatusCode(200);
		}
        else if(request.getEndpoint().contains(APTPS_Constants.AIC_USER_OBJECT)) {
            // Create a fake sendAgreementData response
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"SerializedResultEntities": [{"Id": "cbf9c483-9018-e811-80c2-0004ffb079d5","Name": "Standav","RowVersion": "AAAAAAACp4A="}],"TotalNumberOfRecords": 19512,"MoreRecords": false}');
            res.setStatusCode(200);
		}
        
        return res;
    }
}
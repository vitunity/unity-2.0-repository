// ===========================================================================
// Component: APTS_AddRecipientController 
//    Author: Asif Muhammad
// Copyright: 2017 by Standav
//   Purpose: Controller for AddRecipient Visual Force Page to 
//            select the recipients.
// ===========================================================================
// Created On: 21-12-2017
// ChangeLog:  
// ==========================================================================
public with sharing class APTS_AddRecipientController {
    //Public Variable Declarations
    public String agreementId{get;set;}
    public Apttus__APTS_Agreement__c agreementObj{get;set;}
    public list<Apttus_DocuApi__DocuSignDefaultRecipient2__c> docusignRecipientList{get;set;}
    public list<RecipientWrapper> recipientWrapperList{get;set;}
    public RecipientWrapper newRecipient{get;set;}
    public Contact contactObj{get;set;}
    public Integer index{get;set;}
    public Integer deleteIndex{get;set;}
    public Integer moveUpIndex{get;set;}
    public Integer moveDownIndex{get;set;}
    public Boolean selectAllRecipients{get;set;}
    public String hiddenEmailId{get;set;}
    public Integer maxRoutingOrder{get;set;}
    public List<SelectOption> signerPickvals{get;set;}

    //Constructor Class
    public APTS_AddRecipientController () {
        //Getting parameters from the URL
        agreementId = apexpages.currentpage().getparameters().get('agreementId');
        selectAllRecipients = true;
        newRecipient = New RecipientWrapper();
        //Initialising Contact record used for holding search term in the autocomplete search box
        contactObj = New Contact();
        maxRoutingOrder = 0;
        index = 0;
        //Populating values for Answer checklist
        Schema.DescribeFieldResult signerValue = Apttus_DocuApi__DocuSignDefaultRecipient2__c.Apttus_DocuApi__RecipientType__c.getDescribe();
        List < Schema.PicklistEntry > signerValues = new List < Schema.PicklistEntry > ();
        signerValues = signerValue.getPicklistValues();
        signerPickvals = new List < SelectOption > ();
        if(signerValues != null && !signerValues.isEmpty()){
            for (Schema.PicklistEntry a: signerValues) {
                signerPickvals.add(new SelectOption(a.getValue(), a.getValue()));
            }
        }
        agreementObj = [SELECT Id FROM Apttus__APTS_Agreement__c WHERE Id =: agreementId LIMIT 1];
        docusignRecipientList = [SELECT Id, Apttus_DocuApi__ContactId__c, Apttus_DocuApi__ReadOnlyFirstName__c, Apttus_DocuApi__Email__c,
            Apttus_DocuApi__FirstName__c, Apttus_DocuApi__LastName__c, Apttus_DocuApi__RecipientType__c, Apttus_DocuApi__IsRequired__c,
            Apttus_DocuApi__SigningOrder__c, Vendor_Contact__c FROM Apttus_DocuApi__DocuSignDefaultRecipient2__c
            WHERE Apttus_CMDSign__AgreementId__c =: agreementObj.Id LIMIT 999
        ];

        //Populating Recipient Wrapper List
        recipientWrapperList = New list < RecipientWrapper > ();
        if (docusignRecipientList != NULL && !docusignRecipientList.isEmpty()) {
            for (Apttus_DocuApi__DocuSignDefaultRecipient2__c recipient: docusignRecipientList) {
                RecipientWrapper newRecipient = New RecipientWrapper();
                newRecipient.recipientId = recipient.Id;
                newRecipient.fullName = '';
                if (recipient.Apttus_DocuApi__FirstName__c != NULL) {
                    newRecipient.firstName = recipient.Apttus_DocuApi__FirstName__c;
                    newRecipient.fullName += recipient.Apttus_DocuApi__FirstName__c;
                } else
                    newRecipient.firstName = '';
                if (recipient.Apttus_DocuApi__LastName__c != NULL) {
                    newRecipient.lastName = recipient.Apttus_DocuApi__LastName__c;
                    newRecipient.fullName += ' ' + recipient.Apttus_DocuApi__LastName__c;
                } else
                    newRecipient.lastName = '';
                if (recipient.Apttus_DocuApi__Email__c != NULL)
                    newRecipient.emailId = recipient.Apttus_DocuApi__Email__c;
                if (recipient.Apttus_DocuApi__SigningOrder__c != NULL)
                    newRecipient.routingOrder = Integer.valueOf(recipient.Apttus_DocuApi__SigningOrder__c);
                else
                    newRecipient.routingOrder = maxRoutingOrder + 1;
                if (recipient.Apttus_DocuApi__RecipientType__c != NULL)
                    newRecipient.signerType = recipient.Apttus_DocuApi__RecipientType__c;
                newRecipient.flag = true;
                newRecipient.createdFromPage = false;
                newRecipient.index = index;
                recipientWrapperList.add(newRecipient);
                maxRoutingOrder++;
                index++;
            }
        }
    }

    //Link to redirect user back to Agreement Page
    public PageReference cancelButton() {
        PageReference pageRef = New PageReference('/' + agreementObj.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    //Function to redirect to Apttus Send Esignature Page
    public PageReference sendForEsignature() {
        //Getting Redirect URL from Custom Setting 
        Map < String, Docusign_Custom_Integration__c > docusignCustom = Docusign_Custom_Integration__c.getAll();
        //Updating Existing Recipients
        List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > recipientUpdateList = New List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > ();
        List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > recipientInsertList = New List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > ();
        List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > recipientDeleteList = New List < Apttus_DocuApi__DocuSignDefaultRecipient2__c > ();
        if (recipientWrapperList != NULL && !recipientWrapperList.isEmpty()) {
            for (RecipientWrapper recipientWrapperObj: recipientWrapperList) {
                if (recipientWrapperObj.hiddenName != NULL && recipientWrapperObj.hiddenName != '')
                    recipientWrapperObj.fullName = recipientWrapperObj.hiddenName;
                if (recipientWrapperObj.hiddenEmail != NULL && recipientWrapperObj.hiddenEmail != '')
                    recipientWrapperObj.emailId = recipientWrapperObj.hiddenEmail;
                if (recipientWrapperObj.hiddenRouting != NULL && recipientWrapperObj.hiddenRouting != '')
                    recipientWrapperObj.routingOrder = Integer.valueOf(recipientWrapperObj.hiddenRouting);
                if (recipientWrapperObj.flag == true) {
                    if (recipientWrapperObj.createdFromPage == false) {
                        Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientObj = New Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                        recipientObj.Id = recipientWrapperObj.recipientId;
                        recipientObj.Apttus_DocuApi__FirstName__c = recipientWrapperObj.firstName;
                        recipientObj.Apttus_DocuApi__LastName__c = recipientWrapperObj.lastName;
                        recipientObj.Apttus_DocuApi__Email__c = recipientWrapperObj.emailId;
                        recipientObj.Apttus_DocuApi__SigningOrder__c = recipientWrapperObj.routingOrder;
                        recipientObj.Apttus_DocuApi__RecipientType__c = recipientWrapperObj.signerType;
                        recipientUpdateList.add(recipientObj);
                    } else {
                        Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientObj = New Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                        recipientObj.Apttus_DocuApi__FirstName__c = recipientWrapperObj.fullName.substringBefore(' ');
                        recipientObj.Apttus_DocuApi__LastName__c = recipientWrapperObj.fullName.substringAfter(' ');
                        recipientObj.Apttus_DocuApi__Email__c = recipientWrapperObj.emailId;
                        recipientObj.Apttus_DocuApi__SigningOrder__c = recipientWrapperObj.routingOrder;
                        recipientObj.Apttus_DocuApi__RecipientType__c = recipientWrapperObj.signerType;
                        recipientObj.Apttus_CMDSign__AgreementId__c = agreementId;
                        recipientObj.Apttus_DocuApi__IsTransient__c = false;
                        recipientInsertList.add(recipientObj);
                    }
                } else if (recipientWrapperObj.createdFromPage == false) {
                    Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientObj = New Apttus_DocuApi__DocuSignDefaultRecipient2__c();
                    recipientObj.Id = recipientWrapperObj.recipientId;
                    recipientDeleteList.add(recipientObj);
                }
            }
            try {
                if (recipientInsertList.size() > 0)
                    insert recipientInsertList;
                if (recipientUpdateList.size() > 0)
                    update recipientUpdateList;
                if (recipientDeleteList.size() > 0)
                    delete recipientDeleteList;
            } catch (Exception e) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(e));
                ApexPages.addMessage(myMsg);
                System.Debug('Le Error: ' + myMsg);
                return NULL;
            }
        }
        //Commmentting out URL used, to avoid error in Production 
        PageReference pageRef = New PageReference('/apex/apttus_cmdsign__CMDocuSignCreateEnvelope?id=' + agreementObj.Id);
        //PageReference pageRef = New PageReference('/apex/CMDocuSignCreateEnvelope?id=' + agreementObj.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    //Function to delete a Recipient from the list
    public void deleteRecipient() {
        if (deleteIndex != NULL) {
            Integer deleteElementIndex;
            for (Integer i = 0; i < recipientWrapperList.size(); i++) {
                if (recipientWrapperList.get(i).index == deleteIndex)
                    deleteElementIndex = i;
            }
            if (recipientWrapperList.get(deleteElementIndex).routingOrder != NULL && recipientWrapperList.get(deleteElementIndex).routingOrder == maxRoutingOrder)
                maxRoutingOrder--;
            recipientWrapperList.remove(deleteElementIndex);
        }
    }

    //Function to add a new Recipient
    public void addAnotherRecipient(){
        //Finding max index
        Integer x = 0;
        if(recipientWrapperList != null && !recipientWrapperList.isEmpty()){
            for (RecipientWrapper rw: recipientWrapperList) {
                if (rw.Index > x)
                    x++;
            }
            //Finding the hidden values from recipientWrapperList 
            for (RecipientWrapper recipientWrapper: recipientWrapperList) {
                if (recipientWrapper.hiddenName != NULL && recipientWrapper.hiddenName != '')
                    recipientWrapper.fullName = recipientWrapper.hiddenName;
                if (recipientWrapper.hiddenEmail != NULL && recipientWrapper.hiddenEmail != '')
                    recipientWrapper.emailId = recipientWrapper.hiddenEmail;
                if (recipientWrapper.hiddenRouting != NULL && recipientWrapper.hiddenRouting != '')
                    recipientWrapper.routingOrder = Integer.valueOf(RecipientWrapper.hiddenRouting);
            }
            RecipientWrapper recipientWrapperObj = New RecipientWrapper();
            recipientWrapperObj.flag = true;
            recipientWrapperObj.createdFromPage = true;
            recipientWrapperObj.index = x;
            recipientWrapperObj.routingOrder = maxRoutingOrder + 1;
            maxRoutingOrder++;
            index++;
            recipientWrapperList.add(recipientWrapperObj);
        }
    }

    //Function to add to List from auto complete textbox
    public void addToList() {
        //Finding max index
        Integer x = 0;
        if(recipientWrapperList != null && !recipientWrapperList.isEmpty()){
            for (RecipientWrapper rw: recipientWrapperList) {
                if (rw.Index > x)
                    x++;
            }
        }
        if (contactObj.firstName != NULL) {
            RecipientWrapper recipientWrapperObj = New RecipientWrapper();
            recipientWrapperObj.flag = true;
            recipientWrapperObj.createdFromPage = true;
            recipientWrapperObj.index = index;
            index++;
            recipientWrapperObj.fullName = contactObj.firstName;
            recipientWrapperObj.firstName = recipientWrapperObj.fullName.substringBefore(' ');
            recipientWrapperObj.lastName = recipientWrapperObj.fullName.substringAfter(' ');
            recipientWrapperObj.emailId = hiddenEmailId;
            recipientWrapperObj.routingOrder = maxRoutingOrder + 1;
            maxRoutingOrder++;
            recipientWrapperObj.signerType = 'Signer';
            recipientWrapperList.add(recipientWrapperObj);
            contactObj = New Contact();
        }
    }
    
    public void moveUp(){
        list < RecipientWrapper > recipientWrapperListTemp = new List < RecipientWrapper > ();
        list < RecipientWrapper > recipientWrapperListToLoop = new List < RecipientWrapper > ();
        for (RecipientWrapper sObj: recipientWrapperList) {
            recipientWrapperListToLoop.add(sObj);
        }

        Set < Id > RecipientIdSet = new Set < Id > ();
        if (moveUpIndex != NULL && moveUpIndex > 0) {
            Map < Integer, RecipientWrapper > tempMap = New Map < Integer, RecipientWrapper > ();
            if(recipientWrapperList != null && !recipientWrapperList.isEmpty()){
                for (recipientWrapper rw: recipientWrapperList){
                    tempMap.put(rw.index, rw);
                }
            }
            RecipientWrapper moveUpElement = New RecipientWrapper();
            RecipientWrapper moveDownElement = New RecipientWrapper();
            Integer RoutingOrder1;
            Integer RoutingOrder2;
            Integer RoutingOrder3;
            boolean flag = false;
            Integer indexswap;
            for (Integer i = 0; i < recipientWrapperListToLoop.size(); i++) {
                RoutingOrder1 = recipientWrapperListToLoop.get(i).routingOrder;
                if (i != 0)
                    RoutingOrder2 = recipientWrapperListToLoop.get(i - 1).routingOrder;
                if (i != recipientWrapperListToLoop.size() - 1)
                    RoutingOrder3 = recipientWrapperListToLoop.get(i + 1).routingOrder;
                if (recipientWrapperList.get(i).index == moveUpIndex) {
                    if (i != 0) {
                        moveUpElement = recipientWrapperListToLoop.get(i);
                        moveUpElement.routingOrder = RoutingOrder2;
                        moveUpElement.Index = RoutingOrder2 - 1;
                        
                        moveDownElement = recipientWrapperListToLoop.get(i - 1);
                        moveDownElement.routingOrder = RoutingOrder1;
                        moveDownElement.Index = RoutingOrder1 - 1;
                    } else {
                        moveUpElement = recipientWrapperListToLoop.get(i);
                        moveUpElement.routingOrder = RoutingOrder3;
                        moveUpElement.Index = RoutingOrder3 - 1;

                        moveDownElement = recipientWrapperListToLoop.get(i + 1);
                        moveDownElement.routingOrder = RoutingOrder1;
                        moveDownElement.Index = RoutingOrder1 - 1;
                    }
                    recipientWrapperListTemp.add(moveUpElement);
                    recipientWrapperListTemp.add(moveDownElement);
                    indexswap = i + 1;
                    flag = true;
                    break;

                } else {

                    if (i != moveUpIndex - 1)
                        recipientWrapperListTemp.add(recipientWrapperListToLoop.get(i));
                }

            }

            if (flag) {
                for (integer i = indexswap; i < recipientWrapperList.size(); i++) {
                    recipientWrapperListTemp.add(recipientWrapperListToLoop.get(i));
                }
            }
            recipientWrapperList = new list < RecipientWrapper > ();
            recipientWrapperList = recipientWrapperListTemp;

        }
    }

    //Function to move a recipient down the list
    public void moveDown() {
        list < RecipientWrapper > recipientWrapperListTemp = new List < RecipientWrapper > ();
        list < RecipientWrapper > recipientWrapperListToLoop = new List < RecipientWrapper > ();
        if(recipientWrapperList != null && !recipientWrapperList.isEmpty()){
            //recipientWrapperListToLoop = recipientWrapperList;
            for (RecipientWrapper sObj: recipientWrapperList) {
                recipientWrapperListToLoop.add(sObj);
            }
        }
        Set < Integer > RecipientIdSet = new Set < Integer > ();
        if (moveDownIndex != NULL && moveDownIndex < (recipientWrapperList.size() - 1)) {
            RecipientWrapper moveUpElement = New RecipientWrapper();
            RecipientWrapper moveDownElement = New RecipientWrapper();
            Integer RoutingOrder1;
            Integer RoutingOrder2;
            Integer RoutingOrder3;
            boolean flag = false;
            Integer indexswap;
            for (Integer i = 0; i < recipientWrapperListToLoop.size(); i++) {
                //2
                RoutingOrder1 = recipientWrapperListToLoop.get(i).routingOrder;
                if (i != 0)
                    RoutingOrder2 = recipientWrapperListToLoop.get(i - 1).routingOrder; //1
                if (i != recipientWrapperListToLoop.size() - 1)
                    RoutingOrder3 = recipientWrapperListToLoop.get(i + 1).routingOrder; //3
                if (recipientWrapperList.get(i).index == moveDownIndex) {
                    if (i != recipientWrapperListToLoop.size() - 1) {
                        moveDownElement = recipientWrapperListToLoop.get(i);
                        moveDownElement.routingOrder = RoutingOrder3;
                        moveDownElement.Index = RoutingOrder3 - 1;

                        moveUpElement = recipientWrapperListToLoop.get(i + 1);
                        moveUpElement.routingOrder = RoutingOrder1;
                        moveUpElement.Index = RoutingOrder1 - 1;
                    } else if (i == recipientWrapperListToLoop.size() - 1) {
                        moveDownElement = recipientWrapperListToLoop.get(i - 1);
                        moveDownElement.routingOrder = RoutingOrder1;
                        moveDownElement.Index = RoutingOrder1;

                        moveUpElement = recipientWrapperListToLoop.get(i);
                        moveUpElement.routingOrder = RoutingOrder2;
                        moveUpElement.Index = RoutingOrder2;
                    }
                    recipientWrapperListTemp.add(moveUpElement);
                    recipientWrapperListTemp.add(moveDownElement);
                    indexswap = i + 2;
                    flag = true;
                    break;

                } else {
                    recipientWrapperListTemp.add(recipientWrapperListToLoop.get(i));
                }

            }
            if (flag) {
                for (integer i = indexswap; i < recipientWrapperListToLoop.size(); i++) {
                    recipientWrapperListTemp.add(recipientWrapperListToLoop.get(i));
                }
            }
            recipientWrapperList = new list < RecipientWrapper > ();
            recipientWrapperList = recipientWrapperListTemp;
        }
    }

    //Wrapper class to display recipients
    public with sharing class RecipientWrapper{
        public String firstName{get;set;}
        public String lastName{get;set;}
        public String fullName{get;set;}
        public String emailId{get;set;}
        public String hiddenName{get;set;}
        public String hiddenEmail{get;set;}
        public String hiddenRouting{get;set;}
        public Integer routingOrder{get;set;}
        public String signerType{get;set;}
        public Boolean flag{get;set;}
        public Id recipientId{get;set;}
        public Integer index{get;set;}
        public Boolean createdFromPage{get;set;}
        
        public RecipientWrapper() {
            this.firstName = '';
            this.lastName = '';
            this.fullName = '';
            this.emailId = '';
            this.hiddenName = '';
            this.hiddenEmail = '';
            this.hiddenRouting = '';
        }
    }
}
// ===========================================================================
// Component: APTS_AddRecipientControllerTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: 
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================

@isTest
public with sharing class APTS_AddRecipientControllerTest
{
    // create object records
    @testSetup
    private static void testSetupMethod()
    {
        
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('Steve');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        insert vendor;
        System.assert(vendor.Id!=NULL);
        
        //Inserting Agreement Record
        Apttus__APTS_Agreement__c agreementObj = APTS_TestDataUtility.createAgreement(vendor.Id);
        agreementObj.Planned_Closure_Date__c =System.today();
        insert agreementObj;
        System.assert(agreementObj.Id!=NULL);

        //Creating Account Record
        Account accountObj = APTS_TestDataUtility.createAccount('Steve');
        insert accountObj;
        System.assert(accountObj.Id!=NULL);

        //Creating Contact record 
        Contact contactObj = APTS_TestDataUtility.createContact(accountObj.id);
        insert contactObj;
        System.assert(contactObj.Id!=NULL);

        //Inserting Docusign recipient linked to contact Record
        List<Apttus_DocuApi__DocuSignDefaultRecipient2__c> recipientContactList = new List<Apttus_DocuApi__DocuSignDefaultRecipient2__c>();
        Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientContact = APTS_TestDataUtility.createDocuSignDefaultRecipient(contactObj.id,agreementObj.id);
        recipientContact.Apttus_DocuApi__FirstName__c = 'John';
        recipientContactList.add(recipientContact);
        
        Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientContact1 = APTS_TestDataUtility.createDocuSignDefaultRecipient(contactObj.id,agreementObj.id);
        recipientContactList.add(recipientContact1);
        insert recipientContactList;
        System.assert(recipientContactList!=NULL);
        
        Docusign_Custom_Integration__c docusignCustom = APTS_TestDataUtility.createDocuSignCustomIntegration();
        insert docusignCustom;

    }
    
    // tests for all the methods under the  APTS_AddRecipientController
    @isTest
    private static void test()
    {

        Test.startTest();
            Docusign_Custom_Integration__c docusignCustom = [Select Id From Docusign_Custom_Integration__c Where Name = 'Setting'];
            Apttus__APTS_Agreement__c agreementObj = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
            PageReference myVfPage = Page.APTS_AddRecipient;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('agreementId',agreementObj.Id);
            ApexPages.currentPage().getParameters().put('Setting',docusignCustom.Id);
            APTS_AddRecipientController controller = New APTS_AddRecipientController();
            controller.contactObj.FirstName = 'Jason';
            controller.addToList();
            controller.addAnotherRecipient();
            controller.sendForEsignature();
            controller.deleteIndex = 0;
            controller.deleteRecipient();
            controller.moveUpIndex = 1;
            controller.moveDownIndex = 0;
            controller.cancelButton();
            controller.moveUp();
            controller.moveDown();
        Test.stopTest();
    }

}
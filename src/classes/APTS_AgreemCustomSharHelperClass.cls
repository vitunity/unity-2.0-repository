//===========================================================================
// Component: APTS_AgreemCustomSharHelperClass 
// Author: Sadhana Sadu,Rujuta Mokashi
// Purpose: Helper class for trigger APTS_ApexCustomSharing for custom sharing on Agreement object based on user lookup fields.
// ===========================================================================
// Created On: May-11-2018
// ChangeLog:  
// ==========================================================================
public class APTS_AgreemCustomSharHelperClass {

 //Method is update the custom sharing rules based on Varian contact, Business Owner and Procurement assigned to fields
 public static void customSharingRules(List < Apttus__APTS_Agreement__c > agreementList, Map < ID, Apttus__APTS_Agreement__c > oldMapObject , Set<id> agreementIdSet) {

  // Create a new list of sharing records for Apttus__APTS_Agreement__c
  // Organization Wide Default sharing setting was set to "Private".
  List < Apttus__APTS_Agreement__Share > customShrs = new List < Apttus__APTS_Agreement__Share > ();

  // Declare variables for Varian Contact, Business Owner and Prourement Assigned 
  Apttus__APTS_Agreement__Share varianShr;
  Apttus__APTS_Agreement__Share BusinessOwnerShr;
  Apttus__APTS_Agreement__Share ProcurMemberShr;
  Apttus__APTS_Agreement__Share CreatedByIdShr;
  Apttus__APTS_Agreement__Share RecordTypeShr;
  Apttus__APTS_Agreement__Share RecordTypeShrParent;
  
  //
   RecordType recordTypeObj = [SELECT Id, Name FROM RecordType WHERE Name = 'Indirect Procurement Request' and sObjectType = 'Apttus__APTS_Agreement__c'];
   Map<id,Apttus__APTS_Agreement__c> agreementMap =new Map<id,Apttus__APTS_Agreement__c>( [SELECT Id FROM Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__r.RecordTypeid =: recordTypeObj.id and id IN :agreementIdSet]); 
   system.debug(' Insert Logic 1:' + agreementMap );
   Group grp = [select Id from Group where  Type = 'Queue' and Name= 'Indirect Procurement Team'];
  // For each of the Agreement records being updated, do the following logic:           
  for (Apttus__APTS_Agreement__c agreeNew: agreementList) {

   //Check for if old version of record exists then excute delete logic
   if (oldMapObject != null) {

    // Get the old version of the current record
    Apttus__APTS_Agreement__c agreeOld = oldMapObject.get(agreeNew.Id);
    system.debug(' Old RDType ---' + agreeOld.RecordTypeId );
    system.debug(' New RDType---' + agreeNew.RecordTypeId );
    // Compare the Varian contact, Business Owner and Procurement assigned to look up field in both old and new version
    //Excute below code only value differs
    if (agreeOld.Varian_Contact__c != agreeNew.Varian_Contact__c || agreeOld.Business_Owner__c != agreeNew.Business_Owner__c
        || agreeOld.Procurement_Assigned_To__c != agreeNew.Procurement_Assigned_To__c || agreeOld.RecordTypeId != agreeNew.RecordTypeId || agreementMap.get(agreeNew.Apttus__Parent_Agreement__c) == null) {
    system.debug(' Old RDType' + agreeOld.RecordTypeId );
    system.debug(' New RDType' + agreeNew.RecordTypeId );
     // Get the IDs of update new record                               
     set < id > agreeold1 = new set < ID > ();
     agreeold1.add(agreeNew.Id);

     //Query the records with the RowCause = 'Apex Sharing' and IDs of updated record
     List < Apttus__APTS_Agreement__Share > sharesToDelete = [select id from Apttus__APTS_Agreement__Share
                                          where ParentId IN: agreeold1 and RowCause =: 'Apex_Sharing__c' ];
      system.debug(' Delete Logic 1:' + sharesToDelete );
     //Delete the all the query records                            
     if (!sharesToDelete.isEmpty()) {
      Database.Delete(sharesToDelete, false);
     }
    }
}
    // Creating new records
    // Instantiate the sharing objects
    varianShr = new Apttus__APTS_Agreement__Share();
    BusinessOwnerShr = new Apttus__APTS_Agreement__Share();
    ProcurMemberShr = new Apttus__APTS_Agreement__Share();
    RecordTypeShr = new Apttus__APTS_Agreement__Share();
    RecordTypeShrParent = new Apttus__APTS_Agreement__Share();
    CreatedByIdShr = new Apttus__APTS_Agreement__Share();
    // Populate the Share record with the ID of the record to be shared.
    varianShr.ParentId = agreeNew.Id;
    BusinessOwnerShr.ParentId = agreeNew.Id;
    ProcurMemberShr.ParentId = agreeNew.Id;
    CreatedByIdShr.ParentId = agreeNew.Id;
        // Then, set the ID of user or group being granted access. In this case,
    // we’re setting the Id of the Varian Contact,Business Owner and Procurement assigned to
    // the Varian Contact,Business Owner  are lookup fields on the agreeNew record.
    varianShr.UserOrGroupId = agreeNew.Varian_Contact__c;
    BusinessOwnerShr.UserOrGroupId = agreeNew.Business_Owner__c;
    ProcurMemberShr.UserOrGroupId = agreeNew.Procurement_Assigned_To__c;
    CreatedByIdShr.UserOrGroupId = agreeNew.CreatedById;
    // Specify that the access for this particular Share record. .
    varianShr.AccessLevel = 'Read';
    BusinessOwnerShr.AccessLevel = 'Edit';
    ProcurMemberShr.AccessLevel = 'Edit';
    CreatedByIdShr.AccessLevel = 'Edit';
    // Set the Apex sharing reason for varian contact, Business Owner and Procurement assigned to sharing             
    varianShr.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c;
    BusinessOwnerShr.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c;
    ProcurMemberShr.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c; 
    CreatedByIdShr.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c; 
    //Create Share Records for Child having Parent with a IP Request Recordtype
        if(!agreementMap.isEmpty()){
        for(Id obj:agreementMap.keySet()){
            RecordTypeShr.ParentId = agreementMap.get(obj).id;
        }
        RecordTypeShr.UserOrGroupId = grp.id;
        RecordTypeShr.AccessLevel = 'Edit';
        RecordTypeShr.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c;
        }
    //Create Share Records for Parent having IP Request Recordtype
    if(agreeNew.RecordTypeId == recordTypeObj.id) {
        RecordTypeShrParent.ParentId = agreeNew.Id;
        RecordTypeShrParent.UserOrGroupId = grp.Id;
        RecordTypeShrParent.AccessLevel = 'Edit';
        RecordTypeShrParent.RowCause = Schema.Apttus__APTS_Agreement__Share.RowCause.Apex_Sharing__c;
        }   
    // Add objects to list for insert           
    customShrs.add(varianShr);
    customShrs.add(BusinessOwnerShr);
    customShrs.add(ProcurMemberShr);
    customShrs.add(CreatedByIdShr);
    customShrs.add(RecordTypeShr);
    customShrs.add(RecordTypeShrParent);

   

   // Insert sharing records and capture save result
   // The false parameter allows for partial processing if multiple records are passed
   // into the operation
   Database.SaveResult[] lsr = Database.insert(customShrs, false);
   system.debug(' Insert Logic 2:' + customShrs);
  }
 }
}
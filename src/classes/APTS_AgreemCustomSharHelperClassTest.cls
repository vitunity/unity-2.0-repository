// ===========================================================================
// Component: APTS_AgreemCustomSharHelperClassTest
//    Author: Sadhana S
// Copyright: 2018 by Standav
//   Purpose: Test Class for  APTS_AgreemCustomSharHelperClass

// ===========================================================================
// Created On: May 9th -2018
// ChangeLog:  
// ===========================================================================

@isTest
public class APTS_AgreemCustomSharHelperClassTest {

 // create object records in test setupmethod
 @testSetup
 private static void testSetupMethod() {
  // Select users for the test.
  User user1 = APTS_TestDataUtility.createUser('James Bond');
  User user2 = APTS_TestDataUtility.createUser('James Bond');
  User user3 = APTS_TestDataUtility.createUser('James Bond');
  insert user1;
  insert user2;
  System.assert(user1.Id != null);
  System.assert(user2.Id != null);

  //Inserting Vendor record
  Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
  Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
  vendor.RecordTypeId = recordTypeId;
  insert vendor;
  System.assert(vendor.Id != null);

  //Inserting Agreement Records
  Apttus__APTS_Agreement__c agreementObj1 = APTS_TestDataUtility.createAgreement(vendor.Id);
  agreementObj1.Name = 'Test MSA1';
  agreementObj1.Planned_Closure_Date__c = System.today();
  agreementObj1.Varian_Contact__c = user1.Id;
  agreementObj1.Business_Owner__c = user2.Id;
  insert agreementObj1;
  System.assert(agreementObj1.Id != NULL);

  Apttus__APTS_Agreement__c agreementObj2 = APTS_TestDataUtility.createAgreement(vendor.Id);
  agreementObj2.Name = 'Test MSA2';
  agreementObj2.Planned_Closure_Date__c = System.today();
  agreementObj2.Varian_Contact__c = user1.Id;
  agreementObj2.Business_Owner__c = user2.Id;
  insert agreementObj2;
  System.assert(agreementObj2.Id != NULL);
  
  Apttus__APTS_Agreement__c agreementObj3 = APTS_TestDataUtility.createAgreementRequestParent(vendor.Id);
  agreementObj3.Name = 'Test Agreement Request';
  agreementObj3.Planned_Closure_Date__c = System.today();
  agreementObj3.Varian_Contact__c = user1.Id;
  agreementObj3.Business_Owner__c = user2.Id;
  insert agreementObj3 ;
  System.assert(agreementObj3.Id != NULL);
  
  Apttus__APTS_Agreement__c agreementObj4 = APTS_TestDataUtility.createAgreement(vendor.Id);
  agreementObj4.Name = 'Test Agreement Child';
  agreementObj4.Planned_Closure_Date__c = System.today();
  agreementObj4.Varian_Contact__c = user1.Id;
  agreementObj4.Business_Owner__c = user2.Id;
  agreementObj4.Apttus__Parent_Agreement__c = agreementObj3.id;
  insert agreementObj4;
  System.assert(agreementObj4.Id != NULL);
  
  Id agrRecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('2. MSA').getRecordTypeId();
  agreementObj3.RecordTypeId = agrRecordTypeId;
  update agreementObj3;

 }

 // tests for all the methods under the  APTS_AddRecipientController
 @isTest
 private static void testcustomSharingRules() {

  list < user > User = [SELECT Id FROM User WHERE Name = 'James Bond'];
  
  
  Apttus__APTS_Agreement__c agreementObj1 = [Select Id, Name, Varian_Contact__c, Business_Owner__c
                                                        From Apttus__APTS_Agreement__c Where Name = 'Test MSA1'];

  Apttus__APTS_Agreement__c agreementObj2 = [Select Id, Name, Varian_Contact__c, Business_Owner__c
                                                        From Apttus__APTS_Agreement__c Where Name = 'Test MSA2'];

  Test.startTest();

  //Check the list has entires if so perform below logic
  if (User.size() > 0) {
   agreementObj2.Varian_Contact__c = user[0].id;
   agreementObj2.Business_Owner__c = user[1].id;
   update agreementObj2;

   system.assertEquals(agreementObj2.Varian_Contact__c, user[0].id);
   system.assertEquals(agreementObj2.Business_Owner__c, user[1].id);
   System.assert(agreementObj2.Id != NULL);
   System.assert(agreementObj1.Id != NULL);
  }

  if (agreementObj2.Varian_Contact__c != agreementObj1.Varian_Contact__c ||
   agreementObj2.Business_Owner__c != agreementObj1.Business_Owner__c) {
   
   //Query the records with the RowCause = 'Apex Sharing' and IDs of updated record
   list<Apttus__APTS_Agreement__Share> sharesToDelete = [select id, AccessLevel, RowCause, ParentId,UserOrGroupId
                                                                        from Apttus__APTS_Agreement__Share 
                                                                        where ParentId = :agreementObj2.Id 
                                                                        AND RowCause= 'Apex_Sharing__c' LIMIT 1];

   // Test for only one manual share on job.
   //System.assertEquals(sharesToDelete[0].size(), 1, 'Set the object\'s sharing model to Private.');

   // Test attributes of manual share.
   System.assertEquals(sharesToDelete[0].AccessLevel,'Edit');
   System.assertEquals(sharesToDelete[0].RowCause, 'Apex_Sharing__c');
   System.assertEquals(sharesToDelete[0].ParentId, agreementObj2.Id);
   System.assertEquals(sharesToDelete[0].UserOrGroupId, user[0].id);
   System.assert(sharesToDelete[0].Id != NULL);
   System.assert(sharesToDelete[0].AccessLevel != NULL);
   System.assert(sharesToDelete[0].RowCause != NULL);
   System.assert(sharesToDelete[0].ParentId != NULL);
   System.assert(sharesToDelete[0].UserOrGroupId != NULL);
   
   update sharesToDelete;
   
   // Test invalid agreement Id.
    delete agreementObj1;

   //Delete the all the query records                            
    if(!sharesToDelete.isEmpty()){
    
       // Delete the records
        Database.DeleteResult[] deleteResults1 = Database.delete(sharesToDelete, false);
        deleteResults1 = Database.delete(sharesToDelete, false);
      }                                                  

   //Check the list has entires if so perform below logic
   if (User.size() > 0) {

    // Query agreement sharing records.
    List < Apttus__APTS_Agreement__Share > aShrs = [SELECT Id, UserOrGroupId, AccessLevel,
                                                     RowCause FROM Apttus__APTS_Agreement__Share
                                                    WHERE ParentId = : agreementObj1.Id AND UserOrGroupId = : user[0].id];

    // Test for only one manual share on job.
    System.assertEquals(aShrs.size(), 1, 'Set the object\'s sharing model to Private.');

    // Test attributes of Apex share.
    System.assertEquals(aShrs[0].AccessLevel, 'Read');
    System.assertEquals(aShrs[0].RowCause, 'Apex_Sharing__c');
    System.assertEquals(aShrs[0].UserOrGroupId, user[0].id);
    System.assert(aShrs[0].Id != NULL);
    System.assert(aShrs[0].UserOrGroupId != NULL);
    System.assert(aShrs[0].AccessLevel != NULL);
    System.assert(aShrs[0].RowCause != NULL);

    // Test invalid agreement Id.
    delete agreementObj1;

    //Delete the records
    Database.DeleteResult[] deleteResults = Database.delete(aShrs, false);
    deleteResults = Database.delete(aShrs, false);
   }
   Test.stopTest();

  }

 }
}
// ===========================================================================
// Component: APTS_AgreementStatusChange
//    Author: Sadhana Sadu
// Copyright: 2018 by Standav
//   Purpose: Change the status,Status category and ready to queue field on Agreement obj.This class is called by two VF pages APTS Agreement Sign-Status and 
//            APTS Agreement Queue Assignment which used are by Formula field Add to Queue and Ready  for E-Signature 
// ===========================================================================
// Created On: 25-01-2018
// ChangeLog:  31-01-2018 Revision - 1 
// ===========================================================================

public class APTS_AgreementStatusChange {
    
    private final Apttus__APTS_Agreement__c agreementId; // The constructor passes in the standard controller defined
    public APTS_AgreementStatusChange(ApexPages.StandardController controller){
        this.agreementId = (Apttus__APTS_Agreement__c) Controller.getRecord();
    }
    
    //This method changes Status to 'Ready for Signature', Status Category to 'In signature' on  click of the Send to Signature
    public PageReference ChangeStatusToInSignature(){ 
        
        //access controller for information about the page 
        Apttus__APTS_Agreement__c agreementStatus = new Apttus__APTS_Agreement__c();
        agreementStatus.id = agreementId.id;
        agreementStatus.Apttus__Status_Category__c = Label.Status_Category; // Update Status Category  to 'In Signatures'
        agreementStatus.Apttus__Status__c = Label.Status; // Update Status Category  to 'Ready for Signatures'
        update agreementStatus; // Update the agreement record
        
        PageReference editPage = new PageReference('/' + this.agreementId.id);
        editPage.setRedirect(True);
        return editpage; // Return to record page 
    }

    //This method changes Request status to In process and change owner to Indirect Procurement Team Queue 
    public PageReference RequestStatusAndQueueAssignment(){
        
        Apttus__APTS_Agreement__c agreementStatus = new Apttus__APTS_Agreement__c();
        agreementStatus.id = agreementId.id;
        agreementStatus.Apttus__Status_Category__c = Label.Status_Category; // Update Status Category  to 'In Signatures'
        agreementStatus.Request_Status__c = label.Request_Status; // Update Status Category  to 'In_Process'
        Group queue = [SELECT Id FROM Group WHERE Name = :label.Indirect_ProcuTeam and Type = 'Queue'];
        if(queue != null){
            agreementStatus.OwnerId = queue.id; // Change owner to Indirect Procurement Team 
        }
        
        try {
            update agreementStatus; // Update the agreement record
            PageReference editPage = new PageReference('/' + this.agreementId.id);
            editPage.setRedirect(True);
            return editpage; // Return to record page 
        } catch (Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(e));
            ApexPages.addMessage(myMsg);
            System.Debug('Le Error: ' + myMsg);
            return NULL;
        }
    }

}
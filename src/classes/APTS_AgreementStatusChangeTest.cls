// ===========================================================================
// Component: APTS_AgreementStatusChangeTest
//    Author: Sadhana Sadu
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class APTS_AgreementStatusChange used in APTS Agreement Sign-Status and APTS Agreement Queue Assignment
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest

public class APTS_AgreementStatusChangeTest {
    
    // create object records
    @testSetup
    private static void testSetupMethod()
    {
        //Inserting Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        insert vendor;
        System.assert(vendor.Id!=NULL);
        
        //Inserting Cost center
        Cost_Center__c TestCostCenter = new Cost_Center__c();
        TestCostCenter.Name = 'Test Cost Center';
        insert TestCostCenter;
        
        //Inserting Agreement Record
        Apttus__APTS_Agreement__c agreementObj = APTS_TestDataUtility.createAgreement(vendor.Id);
        agreementObj.Name = 'Test MSA';       
        agreementObj.Cost_Center__c = TestCostCenter.ID ;
        agreementObj.Planned_Closure_Date__c = date.parse('12/27/2009');
        agreementObj.Start_Date__c= date.parse('12/27/2009');
        insert agreementObj;
        System.assert(agreementObj.Id!=NULL);
    }
    
    public static testMethod void APTS_AgreementStatusChange() {

        //Inserting Agreement Record

        Apttus__APTS_Agreement__c agreementObj = [Select Id, Name, PurchasingOrg__c From Apttus__APTS_Agreement__c Where Name = 'Test MSA'];

        // start the test execution context
        Test.startTest();

            // set the test's page to your VF page (or pass in a PageReference) 
            PageReference myVfPage = Page.APTS_AddRecipient;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id', agreementObj.Id);
    
            ApexPages.currentPage().getParameters().put('agreementId', agreementObj.Id);
            // call the constructor
    
            ApexPages.StandardController sc = new ApexPages.StandardController(agreementObj);
            APTS_AgreementStatusChange controller = new APTS_AgreementStatusChange(sc);
    
            controller.ChangeStatusToInSignature();
            controller.RequestStatusAndQueueAssignment();
            // stop the test

        Test.stopTest();

    }
    
    //  Tests ChangeStatusToInSignature method in APTS_AgreementStatusChange
    public static testMethod void testChangeStatusToInSignature() {
        Test.startTest();
            Apttus__APTS_Agreement__c agreementId = [Select Id, Name, PurchasingOrg__c From Apttus__APTS_Agreement__c Where Name = 'Test MSA'];
            //Logic - Change ststus and status category on  click of the Send to Signature
            agreementId.Apttus__Status_Category__c = Label.Status_Category; // Update Status Category  to 'In Signatures'
            agreementId.Apttus__Status__c = Label.Status; // Update Status Category  to 'Ready for Signatures'
            update agreementId; // Update the agreement record
    
            PageReference editPage = new PageReference('/');
            Test.setCurrentPage(editPage);
            
            ApexPages.StandardController sc = new ApexPages.standardController(agreementId);
            APTS_AgreementStatusChange controller = new APTS_AgreementStatusChange(sc);
            System.assertNotEquals(null, controller.ChangeStatusToInSignature());
        Test.stopTest();
    }
    
    //  Tests RequestStatusAndQueueAssignment method in APTS_AgreementStatusChange
    public static testMethod void TestRequestStatusAndQueueAssignment() {
        Test.startTest();
            Apttus__APTS_Agreement__c agreementId = [Select Id, Name, PurchasingOrg__c From Apttus__APTS_Agreement__c Where Name = 'Test MSA'];
            Apttus__APTS_Agreement__c agreementStatus = [select Apttus__Status_Category__c, Apttus__Status__c from Apttus__APTS_Agreement__c where id =: agreementId.Id];
            agreementStatus.Apttus__Status_Category__c = Label.Status_Category; // Update Status Category  to 'In Signatures'
            agreementStatus.Apttus__Status__c = Label.Status; // Update Status Category  to 'Ready for Signatures'
            Group gp = new Group(Name = 'Indirect Procurement Team', Type = 'Queue');
            insert gp;
    
            QueueSobject mappingObject = new QueueSobject(QueueId = gp.Id, SobjectType = 'Apttus__APTS_Agreement__c');
            System.runAs(new User(Id = UserInfo.getUserId())) {
                insert mappingObject;
            }
            
            Id agrRecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('2. MSA').getRecordTypeId();
            agreementStatus.OwnerId = gp.id; // Change owner to Indirect Procurement Team
            agreementStatus.RecordTypeId = agrRecordTypeId;
            update agreementStatus;
            PageReference editPage = new PageReference('/');
            Test.setCurrentPage(editPage);
            ApexPages.StandardController sc = new ApexPages.standardController(agreementStatus);
            APTS_AgreementStatusChange controller = new APTS_AgreementStatusChange(sc);
        Test.stopTest();
    }
}
// ===========================================================================
// Component: APTS_AutoCompleteComponenetControllerTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for APTS_AutoCompleteComponentController used in AutoCompleteComponenet
// ===========================================================================
// Created On: 30-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_AutoCompleteCompControllerTest {
    // create object records
    @testSetup
    private static void testSetupMethod() {

        //Creating User Record
        User user = APTS_TestDataUtility.createUser('James');
        insert user;
        System.assert(user.Id != NULL);

        //Creating Account Record
        Account accountObj = APTS_TestDataUtility.createAccount('Her Majesty\'s Secret Service');
        insert accountObj;
        System.assert(accountObj.Id != NULL);

        //Creating Contact record 
        Contact contactObj = APTS_TestDataUtility.createContact(accountObj.Id);
        insert contactObj;
        System.assert(contactObj.Id != NULL);

        //Creating Lead Record
        Lead leadObj = APTS_TestDataUtility.createLead();
        insert leadObj;
        System.assert(leadObj.Id != NULL);

        //Creating Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        insert vendor;
        System.assert(vendor.Id != NULL);

        //Creating Vemdor Contact Record
        Vendor_Contact__c vendorObj = APTS_TestDataUtility.createVendorContact(vendor.Id);
        insert vendorObj;
        System.assert(vendorObj.Id != NULL);
    }

    // Test for method findSObjects that find records with the name Bond
    @isTest
    private static void testmethod1() {

        User user = [Select Id From User Where id!=null and isActive=true limit 1];
        Test.startTest();
        System.runAs(user) {
            
                APTS_AutoCompleteComponenetController.findSObjects('Bond');
        }
        Test.stopTest();
    }
}
// ===========================================================================
// Component: APTS_AutoVendorContactMergeScheduler 
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Batch for automatically merging Vendor Contact Records coming in from VSAP and existing
//            records in Salesforce   
// ===========================================================================
// Created On: 30-01-2018
// ===========================================================================

global class APTS_AutoVendorContactMergeScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        //Calling batch class to automatically merge matching 
        APTS_AutomaticVendorContactMerge batchInstance = New APTS_AutomaticVendorContactMerge();
        Database.executeBatch(batchInstance);
    }
}
// ===========================================================================
// Component: APTS_AutomaticVendorContactMerge
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Batch for automatically merging Vendor Contact Records coming in from VSAP and existing
//            records in Salesforce
// ===========================================================================
// Created On: 30-01-2018
// ===========================================================================
global with sharing class APTS_AutomaticVendorContactMerge implements Database.Batchable < sObject > , Database.Stateful {
    global Integer recordsProcessed = 0;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set < Id > masterVendorsIdSet = New Set < Id > ();
        //Querying for VendorContacts that have an VSAP_ID__c 
        List < Vendor_Contact__c > vendorContactList = [SELECT ID, VSAP_ID__c, Email__c, Parent_VendorContact__c FROM Vendor_Contact__c WHERE (VSAP_ID__c != NULL AND Email__c != NULL AND Parent_VendorContact__c = TRUE) LIMIT 200];
        system.debug('vendorContactList  ----> '+vendorContactList );
        if (!vendorContactList.isEmpty() && vendorContactList != null) {
            for (Vendor_Contact__c vendorObj: vendorContactList) {
                masterVendorsIdSet.add(vendorObj.Id);
            }
        }
        return Database.getQueryLocator('SELECT ID, VSAP_ID__c, Email__c FROM Vendor_Contact__c WHERE (VSAP_ID__c != NULL AND Email__c != NULL AND Parent_VendorContact__c = TRUE AND ID IN :masterVendorsIdSet) LIMIT 200');

    }
    
    global void execute(Database.BatchableContext bc, List < Vendor_Contact__c > scope) {
        List < Vendor_Contact__c > potentialChildVendorList = New List < Vendor_Contact__c > ();
        List < Id > masterVendorIdList = New List < Id > ();
        Map < String, Vendor_Contact__c > masterVendorIdMap = New Map < String, Vendor_Contact__c > ();
        Map < String, Vendor_Contact__c > masterVendorEmailMap = New Map < String, Vendor_Contact__c > ();
        Map < String, Vendor_Contact__c > childVendorIdMap = New Map < String, Vendor_Contact__c > ();
        //Map to store to one to one correspondence between Master and child records
        Map < Id, Id > masterChildIdMap = New Map < Id, Id > ();
        for (Vendor_Contact__c vendorObj: scope)
            masterVendorIdList.add(vendorObj.Id);
        //Populating a map with keyset containing the SAP Id of all the potential Master Vendor Records
        for (Vendor_Contact__c vendorObj: scope) {
            masterVendorIdMap.put(vendorObj.VSAP_ID__c, vendorObj);
            masterVendorEmailMap.put(vendorObj.Email__c, vendorObj);
        }
        potentialChildVendorList = [SELECT ID, VSAP_ID__c, Email__c FROM Vendor_Contact__c WHERE(ID NOT IN: masterVendorIdList AND VSAP_ID__c IN: masterVendorIdMap.keyset() AND Parent_VendorContact__c = FALSE AND Email__c IN: masterVendorEmailMap.keyset())];
        if (potentialChildVendorList.size() > 0) {
            for (Vendor_Contact__c vendorObj: potentialChildVendorList)
                childVendorIdMap.put(vendorObj.VSAP_ID__c, vendorObj);
            for (String stringObj: masterVendorIdMap.keyset()) {
                if (childVendorIdMap.containsKey(stringObj))
                    masterChildIdMap.put(masterVendorIdMap.get(stringObj).Id, childVendorIdMap.get(stringObj).Id);
            }
            //Calling Utility Function to do the Merge Function 
            APTS_AutomaticVendorContactMergeUtility.doMerge(masterChildIdMap);
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE ID =: bc.getJobId()
        ];
    }
}
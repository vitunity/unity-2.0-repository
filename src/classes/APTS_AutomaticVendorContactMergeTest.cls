// ===========================================================================
// Component: APTS_AutomaticVendorContactMergeTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class AutomaticVendorContactMerge used for merging two Vendor Contacts
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_AutomaticVendorContactMergeTest {
    @testSetup
    private static void testSetupMethod() {
    
        User u = APTS_TestDataUtility.createUser('VarianUser');
        insert u;
        
        system.runAs(u){
        //Creating Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        vendor.SAP_Vendor_ID__c = '111';
        insert vendor;
        System.assert(vendor.Id != NULL);

        //Creating Vendor Contact Record
        Vendor_Contact__c vendorObj = APTS_TestDataUtility.createVendorContact(vendor.id);
        vendorObj.Vendor__c = vendor.id;
        Id recordTypeId1 = Schema.SObjectType.Vendor_Contact__c.getRecordTypeInfosByName().get('Vendor Contact').getRecordTypeId();
        vendorObj.RecordTypeId = recordTypeId1;
        insert vendorObj;
        System.assert(vendorObj.Id != NULL);

        Vendor_Contact__c vendorObj1 = APTS_TestDataUtility.createVendorContact(vendor.id);
        Id recordTypeId2 = Schema.SObjectType.Vendor_Contact__c.getRecordTypeInfosByName().get('Prospect Vendor Contact').getRecordTypeId();
        vendorObj1.RecordTypeId = recordTypeId2;
        vendorObj1.Vendor__c = vendor.id;
        insert vendorObj1;
        System.assert(vendorObj1.Id != NULL);

        }
    }
    // tests the batch class
    @isTest
    private static void testmethod1() {
        Test.startTest();
        APTS_AutomaticVendorContactMerge obj = new APTS_AutomaticVendorContactMerge();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }


    @isTest
    private static void testmethod2() {
        Test.startTest();
        APTS_AutoVendorContactMergeScheduler sh1 = new APTS_AutoVendorContactMergeScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }
}
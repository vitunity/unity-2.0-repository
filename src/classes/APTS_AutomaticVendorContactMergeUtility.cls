// ===========================================================================
// Component: APTS_AutomaticVendorContactMergeUtility
//    Author: Asif Muhammad
// Copyright: 2017 by Standav
//   Purpose: This is a utility class for containing methods used by
//            AutomaticVendorContactMerge Batch class
// ===========================================================================
// Created On: 30-01-2018
// ChangeLog:  
// ===========================================================================
global with sharing class APTS_AutomaticVendorContactMergeUtility{
    //Getting Fields from the Fields for Merging Field Set of Vendor Object
    global static List < Schema.FieldSetMember > getFields() {
        return SObjectType.Vendor_Contact__c.FieldSets.Fields_For_Merging.getFields();
    }
    //Function to generate query to retrieve Vendor Contacts taking a list of Id's as parameter 
    //and choosing the fields based on Field Set
    //Function Overloading used for taking both Set and List of Id's as parameter
    global static List < Vendor_Contact__c > getVendorList(Set < Id > vendorIdList){
        String query = 'SELECT ';
        for (Schema.FieldSetMember f: getFields())
            query += f.getFieldPath() + ', ';
        query += 'Id, VSAP_ID__c FROM Vendor_Contact__c WHERE ID IN :vendorIdList AND VSAP_ID__c != NULL AND Email__c != NULL';
        return Database.query(query);
    }
    
    global static List < Vendor_Contact__c > getVendorList(List < Id > vendorIdList){
        String query = 'SELECT ';
        for (Schema.FieldSetMember f: getFields())
            query += f.getFieldPath() + ', ';
        query += 'Id, VSAP_ID__c FROM Vendor_Contact__c WHERE ID IN :vendorIdList AND VSAP_ID__c != NULL AND Email__c != NULL';
        return Database.query(query);
    }
    
    //Function to do the Merge Process
    global static void doMerge(Map < Id, Id > childMasterIdMap){
        List < Vendor_Contact__c > masterVendorList = New List < Vendor_Contact__c > ();
        List < Vendor_Contact__c > childVendorList = New List < Vendor_Contact__c > ();
        //Mapping Master record to Child Record
        Map < Vendor_Contact__c, Vendor_Contact__c > masterChildVendorMap = New Map < Vendor_Contact__c, Vendor_Contact__c > ();
        masterVendorList = getVendorList(childMasterIdMap.keyset());
        childVendorList = getVendorList(childMasterIdMap.values());
        if (masterVendorList.size() > 0 && childVendorList.size() > 0) {
            //Nested For Loop used, to create map between Child and Master record
            for (Vendor_Contact__c masterVendorObj: masterVendorList) {
                for (Vendor_Contact__c childVendorObj: childVendorList) {
                    if (masterVendorObj.VSAP_ID__c == childVendorObj.VSAP_ID__c && masterVendorObj.Email__c == childVendorObj.Email__c)
                        masterChildVendorMap.put(masterVendorObj, childVendorObj);
                }
            }
            //Processing Map.......
            if (masterChildVendorMap.size() > 0){
                //Creating List for DML Operation 
                List < Vendor_Contact__c > masterVendorUpdateList = New List < Vendor_Contact__c > ();
                List < Vendor_Contact__c > childVendorDeleteList = New List < Vendor_Contact__c > ();
                for (Vendor_Contact__c masterVendorObj: masterChildVendorMap.keyset()) {
                    Vendor_Contact__c newMasterVendorObj = New Vendor_Contact__c();
                    newMasterVendorObj.Id = masterVendorObj.Id;
                    for (Schema.FieldSetMember f: getFields()) {
                        //Checking if master record fields are NULL and corresponding child record has value
                        if (!(masterVendorObj.get(f.getFieldPath()) != NULL) && masterChildVendorMap.get(masterVendorObj).get(f.getFieldPath()) != NULL)
                            newMasterVendorObj.put(f.getFieldPath(), masterChildVendorMap.get(masterVendorObj).get(f.getFieldPath()));
                    }
                    masterVendorUpdateList.add(newMasterVendorObj);
                    childVendorDeleteList.add(masterChildVendorMap.get(masterVendorObj));

                }
                try {
                    update masterVendorUpdateList;
                    System.Debug('masterVendorUpdateList: ' + masterVendorUpdateList);
                    delete childVendorDeleteList;
                } catch (DmlException e) {
                    System.Debug('Le Exception on DML Opoeration: ' + e);
                }
            }
        }
    }
}
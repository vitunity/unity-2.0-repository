// ===========================================================================
// Component: APTS_AutomaticVendorMerge
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Batch for automatically merging Vendor Records coming in from SAP and existing
//            records in Salesforce
// ===========================================================================
// Created On: 24-01-2018
// ===========================================================================
global with sharing class APTS_AutomaticVendorMerge implements Database.Batchable<sObject>, Database.Stateful
{
    global Integer recordsProcessed = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        List<Vendor__c> vendorsWithAgreementList = New List<Vendor__c>();
        List<Vendor__c> vendorsWithScoringMatrixList = New List<Vendor__c>();
        Set<Id> masterVendorsIdSet = New Set<Id>();

        //Querying for Vendors that have an SAP_Vendor_Id__c and atleast one related Agreement or Scoring Matrix
        vendorsWithAgreementList = [SELECT ID,SAP_Vendor_ID__c FROM Vendor__c WHERE SAP_Vendor_Id__c != NULL AND ID IN (SELECT Vendor__c FROM Apttus__APTS_Agreement__c) LIMIT 200];
        for(Vendor__c vendorObj : [SELECT ID,SAP_Vendor_ID__c FROM Vendor__c WHERE SAP_Vendor_Id__c != NULL AND ID IN (SELECT Vendor__c FROM Apttus__APTS_Agreement__c) LIMIT 200])
            masterVendorsIdSet.add(vendorObj.Id);

        for(Vendor__c vendorObj : [SELECT ID,SAP_Vendor_ID__c FROM Vendor__c WHERE SAP_Vendor_Id__c != NULL AND ID IN (SELECT Vendors__c FROM Scoring_Matrix__c) LIMIT 200])
            masterVendorsIdSet.add(vendorObj.Id);

        return Database.getQueryLocator('SELECT ID,SAP_Vendor_ID__c FROM Vendor__c WHERE SAP_Vendor_Id__c != NULL AND ID IN :masterVendorsIdSet LIMIT 200');
        //return Database.getQueryLocator('SELECT ID,SAP_Vendor_ID__c FROM Vendor__c WHERE SAP_Vendor_Id__c != NULL AND (ID IN (SELECT Vendor__c FROM Apttus__APTS_Agreement__c) OR ID IN(SELECT Vendor__c FROM Apttus__APTS_Agreement__c)) LIMIT 200');
    }
    global void execute(Database.BatchableContext bc,List<Vendor__c> scope)
    {
        System.Debug('Le Scope: '+scope);
        List<Vendor__c> potentialChildVendorList = New List<Vendor__c>();
        List<Id> masterVendorIdList = New List<Id>();
        Map<String,Vendor__c> masterVendorIdMap = New Map<String,Vendor__c>();
        Map<String,Vendor__c> childVendorIdMap = New Map<String,Vendor__c>();

        //Map to store to one to one correspondence between Master and child records
        Map<Id,Id> masterChildIdMap = New Map<Id,Id>();

        for(Vendor__c vendorObj : scope)
            masterVendorIdList.add(vendorObj.Id);
        //Populating a map with keyset containing the SAP Id of all the potential Master Vendor Records
        for(Vendor__c vendorObj : scope)
            masterVendorIdMap.put(vendorObj.SAP_Vendor_Id__c,vendorObj);

        potentialChildVendorList = [SELECT ID,SAP_Vendor_ID__c, Vendor_Status__c FROM Vendor__c WHERE ID NOT IN :masterVendorIdList AND SAP_Vendor_ID__c IN :masterVendorIdMap.keyset()];

        if(potentialChildVendorList.size()>0)
        {
            for(Vendor__c vendorObj : potentialChildVendorList)
                childVendorIdMap.put(vendorObj.SAP_Vendor_ID__c,vendorObj);

            for(String stringObj : masterVendorIdMap.keyset())
            {
                if(childVendorIdMap.containsKey(stringObj))
                    masterChildIdMap.put(masterVendorIdMap.get(stringObj).Id,childVendorIdMap.get(stringObj).Id);
            }

            //Calling Utility Function to do the Merge Function 
            APTS_AutomaticVendorMergeUtility.doMerge(masterChildIdMap);
        }
    }
    global void finish(Database.BatchableContext bc)
    {
        System.Debug(recordsProcessed + ' Vendors Eliminated so far!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems,CreatedBy.Email 
                        FROM AsyncApexJob WHERE ID=:bc.getJobId() ];
        System.Debug('Job Details: '+ job);
    }
}
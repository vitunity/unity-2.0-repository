// ===========================================================================
// Component: APTS_AutomaticVendorMergeScheduler
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: 
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================

global class APTS_AutomaticVendorMergeScheduler implements Schedulable
{
    global void execute(SchedulableContext ctx) 
    {
        //Calling batch class to automatically merge matching 
        APTS_AutomaticVendorMerge batchInstance = New APTS_AutomaticVendorMerge();
        Database.executeBatch(batchInstance);
    }
}
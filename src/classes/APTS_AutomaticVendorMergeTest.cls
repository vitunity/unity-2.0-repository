// ===========================================================================
// Component: APTS_AutomaticVendorMergeTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class AutomaticVendorMerge used for merging two Vendors
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_AutomaticVendorMergeTest {
    
    // create object records
    @testSetup
    private static void testSetupMethod() {
    User u = APTS_TestDataUtility.createUser('VarianUser');
        insert u;
        
        system.runAs(u) {
        //Creating Vendor record
        Vendor__c vendor1 = APTS_TestDataUtility.createMasterVendor('James Bond1');
        vendor1.Name = 'James Bond';
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor1.RecordTypeId = recordTypeId;
        vendor1.SAP_Vendor_ID__c = '1112';
        insert vendor1;
        System.assert(vendor1.Id != NULL);

        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('Agent 0071');
        Id recordVTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordVTypeId;
        vendor.SAP_Vendor_ID__c = '1112';
        vendor.Vendor_to_be_Merged__c = vendor1.Id;
        insert vendor;
        System.assert(vendor.Id != NULL);

        //Inserting Agreement Record
        Apttus__APTS_Agreement__c agreementObj = APTS_TestDataUtility.createAgreement(vendor.Id);
        agreementObj.Planned_Closure_Date__c =System.today();
        insert agreementObj;
        System.assert(agreementObj.Id != NULL);

        //Inserting Scoring Matrix record
        Scoring_Matrix__c matrix = APTS_TestDataUtility.createScoringMatrix(vendor.Id, agreementObj.id);
        insert matrix;
        System.assert(matrix.Id != NULL);
        }
    }

    // test for the batch class
    @isTest
    private static void testmethod1() {
        Test.startTest();
        APTS_AutomaticVendorMerge obj = new APTS_AutomaticVendorMerge();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
    
    // tests the scheduler
    @isTest
    private static void testmethod2() {
        Test.startTest();
        APTS_AutomaticVendorMergeScheduler sh1 = new APTS_AutomaticVendorMergeScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }
    
    /*@isTest
    private static void testmethod3() {
        Test.startTest();
            Map<Id,Id> childMasterMap = New Map<Id,Id>();
            List<Vendor__c> vendorList = New List<Vendor__c>();
            vendorList = [SELECT Id FROM Vendor__c LIMIT 2];
            childMasterMap.put(vendorList[0].Id,vendorList[1].Id);
            APTS_AutomaticVendorMergeUtility.doMerge(childMasterMap);
        Test.stopTest();
    }*/
}
// ===========================================================================
// Component: APTS_AutomaticVendorMergeUtility
//    Author: Asif Muhammad
// Copyright: 2017 by Standav
//   Purpose: This is a utility class for containing methods used by
//            AutomaticVendorMerge Batch class
// ===========================================================================
// Created On: 21-12-2017
// ChangeLog:  
// ===========================================================================
global with sharing class APTS_AutomaticVendorMergeUtility
{
    //Getting Fields from the Fields for Merging Field Set of Vendor Object
    global static List<Schema.FieldSetMember> getFields() 
    {
        return SObjectType.Vendor__c.FieldSets.Fields_For_Merging.getFields();
    }
    //Function to generate query to retrieve Vendors taking a list of Id's as parameter 
    //and choosing the fields based on Field Set
    //Function Overloading used for taking both Set and List of Id's as parameter
    global static List<Vendor__c> getVendorList(Set<Id> vendorIdList)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getFields()) 
            query += f.getFieldPath() + ', ';

        query += 'Id FROM Vendor__c WHERE ID IN :vendorIdList AND SAP_Vendor_Id__c != NULL';
        return Database.query(query);
    }
    global static List<Vendor__c> getVendorList(List<Id> vendorIdList)
    {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : getFields()) 
            query += f.getFieldPath() + ', ';

        query += 'Id FROM Vendor__c WHERE ID IN :vendorIdList AND SAP_Vendor_Id__c != NULL';
        return Database.query(query);
    }
    //Function to do the Merge Process
    global static void doMerge(Map<Id,Id> childMasterIdMap)
    {
        List<Vendor__c> masterVendorList = New List<Vendor__c>();
        List<Vendor__c> childVendorList = New List<Vendor__c>();
        //Mapping Master record to Child Record
        Map<Vendor__c,Vendor__c> masterChildVendorMap = New Map<Vendor__c,Vendor__c>();

        masterVendorList = getVendorList(childMasterIdMap.keyset());
        childVendorList = getVendorList(childMasterIdMap.values());

        if(masterVendorList.size()>0 && childVendorList.size()>0)
        {
            //Nested For Loop used, to create map between Child and Master record
            for(Vendor__c masterVendorObj : masterVendorList)
            {
                for(Vendor__c childVendorObj : childVendorList)
                {
                    if(masterVendorObj.SAP_Vendor_Id__c == childVendorObj.SAP_Vendor_Id__c)
                        masterChildVendorMap.put(masterVendorObj,childVendorObj);
                }
            }
            //Processing Map.......
            if(masterChildVendorMap.size()>0)
            {
                System.Debug('Le masterChildVendorMap: '+masterChildVendorMap);
                //Creating List for DML Operation 
                List<Vendor__c> masterVendorUpdateList = New List<Vendor__c>();
                List<Vendor__c> childVendorDeleteList = New List<Vendor__c>();

                for(Vendor__c masterVendorObj : masterChildVendorMap.keyset())
                {
                    Vendor__c newMasterVendorObj = New Vendor__c();
                    newMasterVendorObj.Id = masterVendorObj.Id;
                    
                    for(Schema.FieldSetMember f : getFields())
                    {
                        //Checking if master record fields are NULL and corresponding child record has value
                        if(!(masterVendorObj.get(f.getFieldPath())!=NULL) && masterChildVendorMap.get(masterVendorObj).get(f.getFieldPath())!=NULL)
                            newMasterVendorObj.put(f.getFieldPath(),masterChildVendorMap.get(masterVendorObj).get(f.getFieldPath()));
                    }
                    masterVendorUpdateList.add(newMasterVendorObj);
                    //if(masterChildVendorMap.get(masterVendorObj).Vendor_Status__c == 'Inactive'){
                        childVendorDeleteList.add(masterChildVendorMap.get(masterVendorObj));
                    //}
                }
                try{
                    update masterVendorUpdateList;
                    delete childVendorDeleteList;
                }catch(DmlException e)
                {
                    System.Debug('Le Exception on DML Opoeration: '+ e);
                }
            }
        }
    }
}
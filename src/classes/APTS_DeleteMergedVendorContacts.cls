// ===========================================================================
// Component: APTS_DeleteMergedVendorContacts 
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Batch for automatically delete Vendor Contact Records coming in from VSAP and existing records in Salesforce
// ===========================================================================
// Created On: 30-01-2018
// ===========================================================================



global class APTS_DeleteMergedVendorContacts implements Database.Batchable < sObject > , Database.Stateful {
    global Integer recordsProcessed = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT ID,Soft_Delete__c FROM Vendor_Contact__c WHERE Soft_Delete__c = TRUE OR Vendor_Contact_Status__c = \'Inactive\'');
    }
    
    global void execute(Database.BatchableContext bc, List < Vendor_Contact__c > scope) {
        recordsProcessed = recordsProcessed + scope.size();
        delete scope;
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE ID =: bc.getJobId()
        ];
    }
}
// ===========================================================================
// Component: APTS_DeleteMergedVendorContactsScheduler 
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: 
// ===========================================================================
// Created On: 24-01-2018
// ===========================================================================

global class APTS_DeleteMergedVendorContactsScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        //Calling batch class to delete all Vendor Contacts with soft delete checkbox ticked
        APTS_DeleteMergedVendorContacts batchInstance = New APTS_DeleteMergedVendorContacts();
        Database.executeBatch(batchInstance);
    }
}
// ===========================================================================
// Component: APTS_DeleteMergedVendorContactsTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class DeleteMergedVendorContacts
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_DeleteMergedVendorContactsTest {
    // create object records
    @testSetup
    private static void testSetupMethod() {

        //Creating Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        vendor.SAP_Vendor_ID__c = '111';
        insert vendor;
        System.assert(vendor.Id != NULL);

        //Creating Vendor Contact Record
        Vendor_Contact__c vendorObj = APTS_TestDataUtility.createVendorContact(vendor.Id);
        vendorObj.Soft_Delete__c = true;
        vendorObj.Primary_Contact__c = true;
        insert vendorObj;
        System.assert(vendorObj.Id != NULL);

    }

    // tests the batch class
    @isTest
    private static void testmethod1() {
        Test.startTest();
        APTS_DeleteMergedVendorContacts obj = new APTS_DeleteMergedVendorContacts();
        DataBase.executeBatch(obj);
        Test.stopTest();

    }

    // tests the scheduler
    @isTest
    private static void testmethod2() {
        Test.startTest();
        APTS_DeleteMergedVendorContactsScheduler sh1 = new APTS_DeleteMergedVendorContactsScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }

}
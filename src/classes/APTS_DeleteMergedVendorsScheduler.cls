// ===========================================================================
// Component: APTS_DeleteMergedVendorsScheduler 
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: 
// ===========================================================================
// Created On: 30-01-2018
// ===========================================================================

global class APTS_DeleteMergedVendorsScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        //Calling batch class to delete all Vendors with soft delete checkbox ticked
        APTS_DeleteMergedVendors batchInstance = New APTS_DeleteMergedVendors();
        Database.executeBatch(batchInstance);
    }
}
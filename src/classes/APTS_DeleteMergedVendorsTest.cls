// ===========================================================================
// Component: APTS_DeleteMergedVendorsTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class APTS_DeleteMergedVendors
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_DeleteMergedVendorsTest {
    @testSetup
    private static void testSetupMethod() {

        //Creating Vendor record
        Vendor__c vendor1 = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor1.RecordTypeId = recordTypeId;
        vendor1.SAP_Vendor_ID__c = '01p3B000000Ofhl';
        vendor1.Soft_Delete__c = true;
        insert vendor1;
        System.assert(vendor1.Id != NULL);

    }

    @isTest
    private static void testmethod1() {
        Test.startTest();
        APTS_DeleteMergedVendors obj = new APTS_DeleteMergedVendors();
        DataBase.executeBatch(obj);
        Test.stopTest();

    }

    @isTest
    private static void testmethod2() {
        Test.startTest();
        APTS_DeleteMergedVendorsScheduler sh1 = new APTS_DeleteMergedVendorsScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, sh1);
        Test.stopTest();
    }

}
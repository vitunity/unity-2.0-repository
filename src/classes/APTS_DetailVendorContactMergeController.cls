// ===========================================================================
// Component: APTS_DetailVendorContactMergeController
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Controller for DetailedVendorMerge Visual Force Page to 
//            select the fields for Merging.
// ===========================================================================
// Created On: 17-01-2018
// ChangeLog:  
// ===========================================================================
public with sharing class APTS_DetailVendorContactMergeController{
    public String vendorContactId{get;set;}
    public Vendor_Contact__c vendorContactObj { get; set; }
    public Vendor_Contact__c childVendorContactObj { get; set; }
    public list<displayWrapper> displayWrapperList{get;set;}
    public String mainVendorContactName{get;set;}
    public String childVendorContactName{get;set;}

       public APTS_DetailVendorContactMergeController() {
        //Getting parameters from the URL 
        vendorContactId = apexpages.currentpage().getparameters().get('vendorContactId');
        //Initialisation of Object Variables
        vendorContactObj = New Vendor_Contact__c();
        childVendorContactObj = New Vendor_Contact__c();
        displayWrapperList = New list < displayWrapper > ();
        //Querying for the Vendor Contact records
        vendorContactObj = getVendorContact(vendorContactId);
        if (vendorContactObj.Vendor_Contact_to_be_Merged__c != NULL)
            childvendorContactObj = getVendorContact(String.valueOf(vendorContactObj.Vendor_Contact_to_be_Merged__c));
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This Vendor contact doesnt have the Vendor to be merged field populated');
            ApexPages.addMessage(myMsg);
        }
        //Populating the display Wrapper
        if (vendorContactObj.Id != NULL && childvendorContactObj.Id != NULL) {
            mainVendorContactName = vendorContactObj.Name;
            childVendorContactName = childvendorContactObj.Name;
            for (Schema.FieldSetMember f: this.getFields()) {
                displayWrapper displayWrapperObj = New displayWrapper();
                displayWrapperObj.displayName = f.getLabel();
                displayWrapperObj.fieldApi = f.getFieldPath();
                if (String.valueOf(vendorContactObj.get(f.getFieldPath())) != NULL)
                    displayWrapperObj.masterFieldValue = String.valueOf(vendorContactObj.get(f.getFieldPath()));
                else
                    displayWrapperObj.masterFieldValue = '';
                if (String.valueOf(childvendorContactObj.get(f.getFieldPath())) != NULL)
                    displayWrapperObj.childFieldValue = String.valueOf(childvendorContactObj.get(f.getFieldPath()));
                else
                    displayWrapperObj.childFieldValue = '';
                displayWrapperObj.radioValue = displayWrapperObj.masterFieldValue;
                //Logic to not show Radio button to choose the value to be used if the master and child values are same
                //Or if the field being compared is the Record Name(since it doesnt allow records with duplicate names in the database)
                if ((displayWrapperObj.masterFieldValue == displayWrapperObj.childFieldValue) || f.getFieldPath() == 'Name')
                    displayWrapperObj.renderRadioButton = false;
                //Logic to add the Selectoptions values for the Radio Button
                if (vendorContactObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.options.add(New selectOption(String.valueOf(vendorContactObj.get(f.getFieldPath())), String.valueOf(vendorContactObj.get(f.getFieldPath()))));
                else
                    displayWrapperObj.options.add(New selectOption('', ''));
                if (childvendorContactObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.options.add(New selectOption(String.valueOf(childvendorContactObj.get(f.getFieldPath())), String.valueOf(childvendorContactObj.get(f.getFieldPath()))));
                else
                    displayWrapperObj.options.add(New selectOption('', ''));
                displayWrapperList.add(displayWrapperObj);
            }
        }
    }
    
    //Function to Merge the field values and redirect back to the record detail page
    public PageReference mergeRecords() {
        Vendor_Contact__c vendorContactUpdateObj = New Vendor_Contact__c();
        vendorContactUpdateObj.Id = vendorContactId;
        //Populating the merged field values on the record
        for (displayWrapper dwObj: displayWrapperList) {
            vendorContactUpdateObj.put(dwObj.fieldApi, dwObj.radioValue);
        }
        try {
            update vendorContactUpdateObj;
            if (childVendorContactObj != NULL && vendorContactObj.Vendor_Contact_to_be_Merged__c != NULL) {
                childVendorContactObj.Soft_Delete__c = true;
                childVendorContactObj.Soft_Delete_Timestamp__c = System.Now();
                update childVendorContactObj;
            }
            PageReference pageRef = New PageReference('/' + vendorContactId);
            pageRef.setRedirect(true);
            return pageRef;
        } catch (DmlException e) {
            //Throwing Exception on the page
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An Error has occured while merging: ' + String.valueOf(e));
            ApexPages.addMessage(myMsg);
            return NULL;
        }
    }
    
    //Function to redirect user back to Vendor Contact Page on clicking cancel
    public PageReference cancelButton() {
        PageReference pageRef = New PageReference('/' + vendorContactId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    //Getting Fields from the Fields for Merging Field Set of Vendor Contact Object
    public List < Schema.FieldSetMember > getFields() {
        return SObjectType.Vendor_Contact__c.FieldSets.Fields_For_Merging.getFields();
    }
    //Function to return Vendor Record with input parameter Id of vendor by generating SOQL Query
    private Vendor_Contact__c getVendorContact(String vendorContactId) {
        String query = 'SELECT ';
        for (Schema.FieldSetMember f: this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id,Vendor_Contact_to_be_Merged__c, Name FROM Vendor_Contact__c WHERE ID=\'' + vendorContactId + '\' LIMIT 1';
        return Database.query(query);
    }


    //Wrapper used for displaying the Fields to be compared
    public with sharing class displayWrapper{
        public String displayName{get;set;}
        public String fieldApi{get;set;}
        public String masterFieldValue{get;set;}
        public String childFieldValue{get;set;}
        public String radioValue{get;set;}
        public list<SelectOption> options{get;set;}
        public Boolean renderRadioButton{get;set;}

        //Constructor for diplay Wrapper
        public displayWrapper(){
            this.fieldApi = '';
            this.displayName = '';
            this.masterFieldValue = '';
            this.childFieldValue = '';
            this.radioValue = '';
            this.options = New list<SelectOption>();
            this.renderRadioButton = true;
        }
    }
}
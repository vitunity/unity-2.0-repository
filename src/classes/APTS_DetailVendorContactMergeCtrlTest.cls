// ===========================================================================
// Component: APTS_DetailVendorContactMergeCtrlTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for batch class APTS_DetailVendorContactMergeController used in DetailedVendorContactMerge page
// ===========================================================================
// Created On: 31-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_DetailVendorContactMergeCtrlTest {

    // create object records
    @testSetup
    private static void testSetupMethod() {

        //Creating Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        vendor.SAP_Vendor_ID__c = '111';
        insert vendor;
        System.assert(vendor.Id != NULL);

        //Creating Vendor Contact Record
        Vendor_Contact__c vendorObj = APTS_TestDataUtility.createVendorContact(vendor.Id);
        vendorObj.First_Name__c = '';
        insert vendorObj;
        System.assert(vendorObj.Id != NULL);

        Vendor_Contact__c vendorObj1 = APTS_TestDataUtility.createVendorContact(vendor.Id);
        vendorObj1.Last_Name__c = '';
        vendorObj1.Vendor_Contact_to_be_Merged__c = vendorObj.Id;
        vendorObj1.Email__c = '';
        insert vendorObj1;
        System.assert(vendorObj1.Id != NULL);
    }

    //  Tests mergeRecords and cancelButton method in APTS_DetailVendorContactMergeController
    @isTest
    private static void testMethod1() {
        Test.startTest();

        Vendor_Contact__c vendorObj = [SELECT Id FROM Vendor_Contact__c WHERE First_Name__c = 'James'
            LIMIT 1
        ];

        PageReference myVfPage = Page.APTS_DetailedVendorContactMerge;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('vendorContactId', vendorObj.Id);
        APTS_DetailVendorContactMergeController controller = New APTS_DetailVendorContactMergeController();
        controller.mergeRecords();
        controller.cancelButton();

        Test.stopTest();
    }

}
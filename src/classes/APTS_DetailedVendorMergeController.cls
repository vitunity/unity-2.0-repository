// ===========================================================================
// Component: APTS_DetailedVendorMergeController
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Controller for DetailedVendorMerge Visual Force Page to 
//            select the fields for Merging.
// ===========================================================================
// Created On: 17-01-2018
// ChangeLog:  
// ===========================================================================
public with sharing class APTS_DetailedVendorMergeController {
    public String vendorId{get;set;}
    public Vendor__c vendorObj { get; set; }
    public Vendor__c childVendorObj { get; set; }
    public list<displayWrapper> displayWrapperList{get;set;}
    public String mainVendorName{get;set;}
    public String childVendorName{get;set;}

    public APTS_DetailedVendorMergeController() {

        //Getting parameters from the URL 
        vendorId = apexpages.currentpage().getparameters().get('vendorId');

        //Initialisation of Object Variables
        vendorObj = New Vendor__c();
        childVendorObj = New Vendor__c();
        displayWrapperList = New list < displayWrapper > ();
        //Querying for the Vendor records
        vendorObj = getVendor(vendorId);
        if (vendorObj.Vendor_to_be_Merged__c != NULL)
            childVendorObj = getVendor(String.valueOf(vendorObj.Vendor_to_be_Merged__c));
        else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This Vendor doesnt have the Vendor to be merged field populated');
            ApexPages.addMessage(myMsg);
        }
        //Populating the display Wrapper
        if (vendorObj.Id != NULL && childVendorObj.Id != NULL) {
            mainVendorName = vendorObj.Name;
            childVendorName = childVendorObj.Name;
            for (Schema.FieldSetMember f: this.getFields()) {
                displayWrapper displayWrapperObj = New displayWrapper();
                displayWrapperObj.displayName = f.getLabel();
                displayWrapperObj.fieldApi = f.getFieldPath();
                if ((String) vendorObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.masterFieldValue = (String) vendorObj.get(f.getFieldPath());
                else
                    displayWrapperObj.masterFieldValue = '';
                if ((String) childVendorObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.childFieldValue = (String) childVendorObj.get(f.getFieldPath());
                else
                    displayWrapperObj.childFieldValue = '';
                displayWrapperObj.radioValue = displayWrapperObj.masterFieldValue;
                //Logic to not show Radio button to choose the value to be used if the master and child values are same
                //Or if the field being compared is the Record Name(since it doesnt allow records with duplicate names in the database)
                if ((displayWrapperObj.masterFieldValue == displayWrapperObj.childFieldValue) || f.getFieldPath() == 'Name')
                    displayWrapperObj.renderRadioButton = false;
                //Logic to add the Selectoptions values for the Radio Button
                if (vendorObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.options.add(New selectOption((String) vendorObj.get(f.getFieldPath()), (String) vendorObj.get(f.getFieldPath())));
                else
                    displayWrapperObj.options.add(New selectOption('', ''));
                if (childVendorObj.get(f.getFieldPath()) != NULL)
                    displayWrapperObj.options.add(New selectOption((String) childVendorObj.get(f.getFieldPath()), (String) childVendorObj.get(f.getFieldPath())));
                else
                    displayWrapperObj.options.add(New selectOption('', ''));
                displayWrapperList.add(displayWrapperObj);
            }
        }
    }

    //Function to Merge the field values and redirect back to the record detail page
    public PageReference mergeRecords() {
        Vendor__c vendorUpdateObj = New Vendor__c();
        vendorUpdateObj.Id = vendorId;
        //Populating the merged field values on the record
        for (displayWrapper dwObj: displayWrapperList) {
            vendorUpdateObj.put(dwObj.fieldApi, dwObj.radioValue);
        }
        try {
            update vendorUpdateObj;
            if (childVendorObj != NULL && vendorObj.Vendor_to_be_Merged__c != NULL) {
                childVendorObj.Soft_Delete__c = true;
                childVendorObj.Soft_Delete_Timestamp__c = System.Now();
                update childVendorObj;
            }
            PageReference pageRef = New PageReference('/' + vendorId);
            pageRef.setRedirect(true);
            return pageRef;
        } catch (DmlException e) {
            //Throwing Exception on the page
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An Error has occured while merging: ' + String.valueOf(e));
            ApexPages.addMessage(myMsg);
            return NULL;
        }
    }

    //Function to redirect user back to Vendor Page on clicking cancel
    public PageReference cancelButton() {
        PageReference pageRef = New PageReference('/' + vendorId);
        pageRef.setRedirect(true);
        return pageRef;
    }
    //Getting Fields from the Fields for Merging Field Set of Vendor Object
    public List < Schema.FieldSetMember > getFields() {
        return SObjectType.Vendor__c.FieldSets.Fields_For_Merging.getFields();
    }
    //Function to return Vendor Record with input parameter Id of vendor by generating SOQL Query
    private Vendor__c getVendor(String vendorId) {
        String query = 'SELECT ';
        for (Schema.FieldSetMember f: this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        query += 'Id,Vendor_to_be_Merged__c FROM Vendor__c WHERE ID=\'' + vendorId + '\' LIMIT 1';
        return Database.query(query);
    }

    //Wrapper used for displaying the Fields to be compared
    public with sharing class displayWrapper {
        public String displayName{get;set;}
        public String fieldApi{get;set;}
        public String masterFieldValue{get;set;}
        public String childFieldValue{get;set;}
        public String radioValue{get;set;}
        public list<SelectOption> options{get;set;}
        public Boolean renderRadioButton{get;set;}

        //Constructor for diplay Wrapper
        public displayWrapper() {
            this.fieldApi = '';
            this.displayName = '';
            this.masterFieldValue = '';
            this.childFieldValue = '';
            this.radioValue = '';
            this.options = New list < SelectOption > ();
            this.renderRadioButton = true;
        }
    }
}
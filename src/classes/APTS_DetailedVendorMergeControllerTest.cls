/**
 *  Class Name: APTS_DetailedVendorMergeControllerTest  
 *  Description: This is a Test class for APTS_DetailedVendorMergeController.
 *  Company: Standav
 *  CreatedDate:28/02/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Nitha T S             28/02/2018               Modified
 */
@isTest
public with sharing class APTS_DetailedVendorMergeControllerTest
{
    @testSetup
    private static void testSetupMethod()
    { 
        Vendor__c childVendor = APTS_TestDataUtility.createMasterVendor('Test Child');
        childVendor.SAP_Vendor_ID__c = '1231231';
        insert childVendor;
        System.assert(childVendor.Id!=NULL);
        
        Vendor__c masterVendor = APTS_TestDataUtility.createMasterVendor('Test Master');
        masterVendor.Vendor_to_be_Merged__c = childVendor.Id;
        insert masterVendor;
        System.assert(masterVendor.Id!=NULL);
    
    }
    @isTest
    private static void testMethod1()
    {
        Test.startTest();
            Vendor__c vendorObj = [SELECT Id FROM Vendor__c WHERE NAME ='Test Master' LIMIT 1];
            PageReference myVfPage = Page.APTS_DetailedVendorMerge;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('vendorId',vendorObj.Id);
            APTS_DetailedVendorMergeController controller = New APTS_DetailedVendorMergeController();
            controller.mergeRecords();
            controller.cancelButton();
        Test.stopTest();
    }
}
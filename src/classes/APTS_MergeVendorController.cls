// ===========================================================================
// Component: APTS_MergeVendorController
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: 
// ===========================================================================
// Created On: 17-01-2018
// ChangeLog:  
// ===========================================================================
public with sharing class APTS_MergeVendorController {
    public String vendorId{get;set;}
    public Vendor__c vendorObj{get;set;}
    public Vendor__c slaveVendorObj{get;set;}
    public APTS_MergeVendorController() {

        //Getting parameters from the URL 
        if (apexpages.currentpage() != NULL &&
            apexpages.currentpage().getparameters().get('vendorId') != NULL)
            vendorId = apexpages.currentpage().getparameters().get('vendorId');
        else
            vendorId = NULL;
    }

    public Pagereference doMerge() {

        Vendor__c vendorObj = New Vendor__c();
        Vendor__c slaveVendorObj = New Vendor__c();
        vendorObj = [SELECT Id, City__c, Country__c, Primary_Vendor_Contact__c, SAP_Vendor_ID__c, Soft_Delete__c, State__c, Street__c, Vendor_Status__c, Vendor_to_be_Merged__c, Zip_Code__c
            FROM Vendor__c WHERE ID =: vendorId
        ];
        if (!(vendorObj.Vendor_to_be_Merged__c != NULL)) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This Vendor doesnt have the Vendor to be merged field populated');
            ApexPages.addMessage(myMsg);
            return NULL;
        }
        slaveVendorObj = [SELECT Id, City__c, Country__c, Primary_Vendor_Contact__c, SAP_Vendor_ID__c, Soft_Delete__c, State__c, Street__c, Vendor_Status__c, Vendor_to_be_Merged__c, Zip_Code__c
            FROM Vendor__c WHERE ID =: vendorObj.Vendor_to_be_Merged__c
        ];

        //Updating the fields to be merged with priority to Orignal record fields
        if ((vendorObj.City__c == NULL || vendorObj.City__c == '') && slaveVendorObj.City__c != NULL)
            vendorObj.City__c = slaveVendorObj.City__c;
        if ((vendorObj.Country__c == NULL || vendorObj.Country__c == '') && slaveVendorObj.Country__c != NULL)
            vendorObj.Country__c = slaveVendorObj.Country__c;

        //Throwing invalid Id error when trying to set Primary Vendor Contact
        if ((vendorObj.SAP_Vendor_ID__c == NULL || vendorObj.SAP_Vendor_ID__c == '') && slaveVendorObj.SAP_Vendor_ID__c != NULL)
            vendorObj.SAP_Vendor_ID__c = slaveVendorObj.SAP_Vendor_ID__c;
        if ((vendorObj.State__c == NULL || vendorObj.State__c == '') && slaveVendorObj.State__c != NULL)
            vendorObj.State__c = slaveVendorObj.State__c;
        if ((vendorObj.Street__c == NULL || vendorObj.Street__c == '') && slaveVendorObj.Street__c != NULL)
            vendorObj.Street__c = slaveVendorObj.Street__c;
        if ((vendorObj.Vendor_Status__c == NULL || vendorObj.Vendor_Status__c == '') && slaveVendorObj.Vendor_Status__c != NULL)
            vendorObj.Vendor_Status__c = slaveVendorObj.Vendor_Status__c;
        if ((vendorObj.Zip_Code__c == NULL || vendorObj.Zip_Code__c == '') && slaveVendorObj.Zip_Code__c != NULL)
            vendorObj.Zip_Code__c = slaveVendorObj.Zip_Code__c;
        slaveVendorObj.Soft_Delete__c = true;

        try {
            System.Debug('Le trying to Update');
            update vendorObj;
            update slaveVendorObj;
        } catch (DMLException e) {
            System.Debug('Le Exception on DML Operation: ' + String.ValueOf(e));
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has ocurred during the Merge process');
            ApexPages.addMessage(myMsg);
            return NULL;
        }
        PageReference pageRef = New PageReference('/' + vendorId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}
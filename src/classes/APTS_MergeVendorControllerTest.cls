// ===========================================================================
// Component: APTS_MergeVendorControllerTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for MergeVendorController used in AutoCompleteComponenet
// ===========================================================================
// Created On: 30-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class APTS_MergeVendorControllerTest {
    
    // create object records
    @testSetup
    private static void testSetupMethod() {

        //Creating Vendor record
        Vendor__c vendor1 = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor1.RecordTypeId = recordTypeId;
        vendor1.SAP_Vendor_ID__c = '01p3B000000Ofhl';
        insert vendor1;
        System.assert(vendor1.Id != NULL);

        Vendor__c vendor = New Vendor__c();
        vendor.Name = 'Bond';
        Id recordVTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordVTypeId;
        vendor.Vendor_to_be_Merged__c = vendor1.Id;
        insert vendor;
        System.assert(vendor.Id!=NULL);

    }
    
    //  Tests doMerge method in APTS_MergeVendorController when vendor to be merged is not null
    @isTest
    private static void testmethod1() {

        Vendor__c vendorRecord = [Select Id From Vendor__c Where Name = 'Bond' And Vendor_to_be_Merged__c != null];

        Test.startTest();
            PageReference pageRef = Page.APTS_VendorMergePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('vendorId', vendorRecord.Id);
    
            APTS_MergeVendorController controller = New APTS_MergeVendorController();
            controller.doMerge();
        Test.stopTest();
    }

    //  Tests doMerge method in APTS_MergeVendorController when vendor to be merged is null
    @isTest
    private static void testmethod2() {
        Vendor__c vendorRecord = [Select Id From Vendor__c Where Name = 'James Bond' And Vendor_to_be_Merged__c = null];

        Test.startTest();
            PageReference pageRef = Page.APTS_VendorMergePage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('vendorId', vendorRecord.Id);
    
            APTS_MergeVendorController controller = New APTS_MergeVendorController();
            controller.doMerge();
        Test.stopTest();
    }
}
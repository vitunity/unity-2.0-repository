/**
 *  Class Name: APTS_TestDataUtility  
 *  Description: This is a Utility class for creating Object Records.
 *  Company: Standav
 *  CreatedDate:28/02/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------  
 *  Asif Muhammad         28/02/2018              Original version
 *  Sadhana Sadu          March 15 2018            
 */
public class APTS_TestDataUtility{
    
    //  Create User Record
    public static User createUser(String name) {
        User user = New User();
        user.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
        String orgId = UserInfo.getOrganizationId();
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + randomInt;
        user.LastName = name+'Bond'+ uniqueName;
        user.Email = uniqueName+'jamesBond@gmail.com';
        user.Username = 'jamesBond'+randomInt+'@gmail.com';
        user.CompanyName = 'test';
        user.Title = 'title';
        user.Alias = 'Agent007';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_Us';
        return user;
    }
    
    //  Creating Account record 
    public static Account createAccount(String name) {
        Account accountObj = New Account();
        accountObj.Name = name;
        if(accountObj.Bill_To__c != null)
        accountObj.Bill_To__r.BillingCity = 'Anytown';
        accountObj.BillingCity = 'Anytown';
        accountObj.State_Province_Is_Not_Applicable__c=true;
        accountObj.Country__c = 'Algeria';
        return accountObj;
    }
    
    //  Creating Contact record 
    public static Contact createContact(Id accountId) {
        Contact contactObj = New Contact();
        contactObj.LastName = 'Bond';
        contactObj.FirstName = 'James';
        contactObj.Email = 'james@mi5.com';
        contactObj.Functional_Role__c = 'Consultant';
        contactObj.AccountId = accountId;
        return contactObj;
    }
    
    //  Creating Lead Record
    public static Lead createLead(){
        Lead leadObj = New Lead();
        leadObj.LastName = 'Bond';
        leadObj.FirstName = 'James';
        leadObj.status = 'New';
        leadObj.Company = 'Company';
        return leadObj;
    }
    
    //  Create Master Vendor Record
    public static Vendor__c createMasterVendor(String name) {
        Vendor__c vendor = New Vendor__c();
        vendor.Name = name;
        vendor.City__c = 'Dubai';
        vendor.Country__c = 'UAE';
        vendor.State__c = 'Dubai';
        vendor.Street__c = '6A';
        vendor.Vendor_Status__c = 'Vendored';
        vendor.Zip_Code__c = '502131';
        return vendor;

    }
    
    //  Creating Agreement Record
    public static Apttus__APTS_Agreement__c createAgreement(Id vendorId) {
        Apttus__APTS_Agreement__c agreementObj = New Apttus__APTS_Agreement__c();
        agreementObj.Request_Name__c = 'Agree to Disagree';
        agreementObj.Vendor__c = vendorId;
        Id agrRecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('2. MSA').getRecordTypeId();
        agreementObj.RecordTypeId = agrRecordTypeId;
        agreementObj.CurrencyIsoCode = 'CAD';
        return agreementObj;
    }
    
    //  Creating Agreement Record IP Request
    public static Apttus__APTS_Agreement__c createAgreementRequestParent(Id vendorId) {
        Apttus__APTS_Agreement__c agreementObj = New Apttus__APTS_Agreement__c();
        //agreementObj.Request_Name__c = 'Test Request Agreement';
        agreementObj.Vendor__c = vendorId;
        Id agrRecordTypeId = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByName().get('Indirect Procurement Request').getRecordTypeId();
        agreementObj.RecordTypeId = agrRecordTypeId;
        agreementObj.CurrencyIsoCode = 'CAD';
        return agreementObj;
    }
    
    //  Creating Vemdor Contact Record
    public static Vendor_Contact__c createVendorContact(Id vendorId) {
        Vendor_Contact__c vendorObj = New Vendor_Contact__c();
        vendorObj.First_Name__c = 'James';
        vendorObj.Last_Name__c = 'Bond';
        vendorObj.Email__c = 'jamesBond@mi5.com';
        vendorObj.Vendor__c = vendorId;
        vendorObj.Contact_Person_ID__c = 'test'+ Integer.valueOf(math.rint(math.random()*1000000));
        return vendorObj;
    }
    
    //Inserting Purchase Org record
    public static Purchasing_Org__c createPurchasingOrg(Id vendorId){
        Purchasing_Org__c purchaseObj = new Purchasing_Org__c();
        purchaseObj.Name = 'Purchase Org Test';
        purchaseObj.Vendor__c = vendorId;
        purchaseObj.CurrencyIsoCode = 'USD';
        return purchaseObj;
    }
    
    //  Creating Scoring Matrix record
    public static Scoring_Matrix__c createScoringMatrix(Id vendorId, Id agreementId){
        Scoring_Matrix__c matrix = new Scoring_Matrix__c();
        matrix.Request__c = agreementId;
        matrix.Vendors__c = vendorId;
        return matrix;
    }
    
    //  Creating Docusign Custom Integration record
    public static Docusign_Custom_Integration__c createDocuSignCustomIntegration(){
        Docusign_Custom_Integration__c docusignCustom = new Docusign_Custom_Integration__c();
        docusignCustom.Name = 'Setting';
        docusignCustom.Add_Recipient_URL__c = 'https://varian--sfdev2--apttus-cmdsign.cs50.visual.force.com';
        return docusignCustom;
    }
    
    //Inserting Docusign recipient linked to contact Record
    public static Apttus_DocuApi__DocuSignDefaultRecipient2__c createDocuSignDefaultRecipient(Id contactId, Id agreementId){
        Apttus_DocuApi__DocuSignDefaultRecipient2__c recipientContact = New Apttus_DocuApi__DocuSignDefaultRecipient2__c();
        recipientContact.Apttus_DocuApi__FirstName__c = 'James';
        recipientContact.Apttus_DocuApi__LastName__c = 'Bond';
        recipientContact.Apttus_DocuApi__Email__c = 'james@mi5.com';
        recipientContact.Apttus_DocuApi__SigningOrder__c = 1;
        recipientContact.Apttus_DocuApi__ContactId__c = contactId;
        recipientContact.Apttus_DocuApi__RecipientType__c = 'Signer';
        recipientContact.Apttus_CMDSign__AgreementId__c = agreementId;
        return recipientContact;
    }
}
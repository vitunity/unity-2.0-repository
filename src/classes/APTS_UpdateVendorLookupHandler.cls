// ===========================================================================
// Component: APTS_UpdateVendorLookupHandler
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: TriggerHandler for APTS_UpdateVendorLookup Trigger on Agreement
// ===========================================================================
// Created On: 15-02-2018
// ===========================================================================
public class APTS_UpdateVendorLookupHandler {
    // Method to update Master Vendor field in Agreement object
    public static void updateAgreementMasterVendor(List < Apttus__APTS_Agreement__c > agreementList) {
        //Set of ids of purchase org ids
        Set < Id > purchaseOrgIds = new Set < Id > ();
        for (Apttus__APTS_Agreement__c agreement: agreementList) {
            if (agreement.PurchasingOrg__c != null) {
                purchaseOrgIds.add(agreement.PurchasingOrg__c);
            }
        }
        //Map Purchase org Ids with the purchase org records
        Map < Id, Purchasing_Org__c > purchaseOrgMap = new Map < Id, Purchasing_Org__c > ([SELECT Id, Name, Vendor__c FROM Purchasing_Org__c WHERE Id In: purchaseOrgIds]);
        for (Apttus__APTS_Agreement__c agreement: agreementList) {
            if (agreement.PurchasingOrg__c != null && purchaseOrgMap.containsKey(agreement.PurchasingOrg__c)) {
                agreement.Vendor__c = purchaseOrgMap.get(agreement.PurchasingOrg__c).Vendor__c;
            }
        }
    }
}
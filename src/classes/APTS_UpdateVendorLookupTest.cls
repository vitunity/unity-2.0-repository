// ===========================================================================
// Component: APTS_UpdateVendorLookupTest
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for UpdateVendorLookup trigger
// ===========================================================================
// Created On: 15-02-2018
// ChangeLog:  
// ===========================================================================
@isTest
private class APTS_UpdateVendorLookupTest {
    
    // create object records
    @testSetup
    private static void testSetupMethod()
    {
        //Inserting Vendor record
        Vendor__c vendor = APTS_TestDataUtility.createMasterVendor('James Bond');
        Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        vendor.RecordTypeId = recordTypeId;
        insert vendor;
        System.assert(vendor.Id!=NULL);
        
        //Inserting Purchase Org record
        Purchasing_Org__c purchaseObj = APTS_TestDataUtility.createPurchasingOrg(vendor.Id);
        insert purchaseObj;
        System.assert(purchaseObj.Id!=NULL);
        
        //Inserting Agreement Record
        Apttus__APTS_Agreement__c agreementObj = APTS_TestDataUtility.createAgreement(vendor.Id);
        agreementObj.Name = 'Test MSA';
        agreementObj.Planned_Closure_Date__c =System.today();
        insert agreementObj;
        System.assert(agreementObj.Id!=NULL);
    }
    
    // Update agreement field and test the same
    private static testMethod void test() {
        
        Apttus__APTS_Agreement__c agreementObj = [Select Id, Name, PurchasingOrg__c From Apttus__APTS_Agreement__c Where Name = 'Test MSA'];
        Purchasing_Org__c purchaseObj = [Select Id, Name From Purchasing_Org__c Where Name = 'Purchase Org Test'];
        Test.startTest();
            agreementObj.PurchasingOrg__c = purchaseObj.Id;
            update agreementObj;
            system.assertEquals(agreementObj.PurchasingOrg__c, purchaseObj.Id);
        Test.stopTest();

    }

}
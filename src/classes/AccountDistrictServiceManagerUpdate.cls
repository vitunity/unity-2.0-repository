public class AccountDistrictServiceManagerUpdate{
     public static void updateAccountFields(List<User> lstUser, map<Id,User> oldMap){
        map<Id,user> mapUser = new map<Id,user>();
        for(User u: lstUser){
            
                if(oldMap <> null && (OldMap.get(u.Id).email <> u.email || OldMap.get(u.Id).phone <> u.phone)){
                    mapUser.put(u.Id,u);
                }
           
        }
        if(mapUser.size()>0){
            List<Account> lstAccount = new List<Account>();
            map<Id,Account> mapAccount = new map<Id,Account>([select Id,District_Service_Manager_Email__c,District_Service_Manager_Phone__c,District_Service_Manager__c from Account where District_Service_Manager__c IN: mapUser.keyset()]);
            for(Account Acc : mapAccount.values()){
                if(mapUser.containsKey(Acc.District_Service_Manager__c)){
                    Acc.District_Service_Manager_Phone__c = mapUser.get(Acc.District_Service_Manager__c).phone; 
                    Acc.District_Service_Manager_Email__c= mapUser.get(Acc.District_Service_Manager__c).Email; 
                    lstAccount.add(Acc);                    
                }
            }
            update lstAccount;
        }
    }
}
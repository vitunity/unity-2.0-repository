@isTest
public class AccountDistrictServiceManagerUpdateTest {
    public static testmethod void unittest(){
        
        //insert Country record
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        
        Profile pr = [Select id from Profile where name = 'VMS Service - User'];    
        User u = new user(alias = 'standt', email='standardtestusqq2@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standarqqqdtestuse92@testclass.com');
        insert u;
        
        User u2 = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u2;
        
        //insert ERP Timezone Record
        ERP_Timezone__c objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
        insert objERP;

        //insert account Record
        Account objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id);
        objAcc.District_Service_Manager__c = u.id;
        insert objAcc;
        System.Test.startTest();
        AccountDistrictServiceManagerUpdate.updateAccountFields(new list<User> {u},new map<id,User>{u.id =>u2});
        System.Test.stopTest();
    }
    
    public static testmethod void unittest2(){
        
        //insert Country record
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        
        Profile pr = [Select id from Profile where name = 'VMS Service - User'];    
        User u = new user(alias = 'standt', email='standardtestusqq2@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standarqqqdtestuse92@testclass.com');
        insert u;
        
        User u2 = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u2; 
        
        //insert ERP Timezone Record
        ERP_Timezone__c objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
        insert objERP;

        //insert account Record
        Account objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id);
        objAcc.District_Sales_Manager__c = u.Id;
        insert objAcc;
        objAcc.District_Sales_Manager__c = u.Id;
        update objAcc;
        System.Test.startTest();
        AccountDistrictSalesManagerUpdate.updateAccountFields(new list<User> {u},new map<id,User>{u.id =>u2});
        System.Test.stopTest();
    }
    
    public static testmethod void unittest3(){
        
        //insert Country record
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        
        Profile pr = [Select id from Profile where name = 'VMS Service - User'];    
        User u = new user(alias = 'standt', email='standardtestusqq2@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standarqqqdtestuse92@testclass.com');
        insert u;
        
        User u2 = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', 
    		lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u2; 
        
        //insert ERP Timezone Record
        ERP_Timezone__c objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
        insert objERP;

        //insert account Record
        Account objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id);
        objAcc.Software_Sales_Manager__c = u.Id;
        insert objAcc;
        
        System.Test.startTest();
        AccountSoftwareSalesManagerUpdate.updateAccountFields(new list<User> {u},new map<id,User>{u.id =>u2});
        System.Test.stopTest();
    }
}
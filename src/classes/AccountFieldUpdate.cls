public class AccountFieldUpdate{
    public static void findCSSDistrict(List<SVMXC__Territory__c > lstTerritory, map<Id,SVMXC__Territory__c > oldMap){
        
        set<Id> setParentTerritory = new set<Id>();
        for(SVMXC__Territory__c t: lstTerritory){
            if(oldMap == null || (oldMap <> null && t.SVMXC__Description__c <> oldmap.get(t.id).SVMXC__Description__c)){
                setParentTerritory.add(t.id);
            }
        }
        system.debug('== setParentTerritory =='+ setParentTerritory + '===oldMap=== '+oldMap +'==lstTerritory=='+lstTerritory );
           System.Debug('QUERRY ON TERRITORY'+ [select Id,SVMXC__Parent_Territory__c ,Service_Team__r.SVMXC__Group_Code__c,SVMXC__Parent_Territory__r.SVMXC__Description__c from SVMXC__Territory__c]);
        if(setParentTerritory.size()>0){
            map<string,string> mapServiceTeam = new map<string,string>();
            for(SVMXC__Territory__c t : [select Id,Service_Team__r.SVMXC__Group_Code__c,SVMXC__Parent_Territory__r.SVMXC__Description__c from SVMXC__Territory__c where 
                SVMXC__Parent_Territory__c IN: setParentTerritory and Service_Team__r.SVMXC__Group_Code__c <> null]){
                mapServiceTeam.put(t.Service_Team__r.SVMXC__Group_Code__c,t.SVMXC__Parent_Territory__r.SVMXC__Description__c);
            } 
            if(mapServiceTeam.size()>0){
                List<account> lstAccount = [select Id,CSS_District__c,Service_Region__c from Account where CSS_District__c IN: mapServiceTeam.keyset()];
                for(Account A: lstAccount){
                    A.Service_Region__c = mapServiceTeam.get(A.CSS_District__c);
                }
                update lstAccount;
            }
        }
    }
}
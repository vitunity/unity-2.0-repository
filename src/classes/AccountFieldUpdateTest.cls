@isTest(seealldata= True)
private class AccountFieldUpdateTest{

    static testmethod void fieldUpdatTest(){
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='USA' ; 
        insert reg ;
        
        Account a = new Account();
        a.Name = 'My Varian GPO 1926';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'GPO';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';
        a.CSS_District__c = 'SMBJ';
        insert a ;
        
        
        list<SVMXC__Territory__c> territiryList = new list<SVMXC__Territory__c >();

        SVMXC__Territory__c territory1 = new SVMXC__Territory__c(
            Name = '[U2C] WEST-CENTRAL WEST',
            SVMXC__Description__c = 'TEST1'
        );
        insert territory1;

        SVMXC__Service_Group__c ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(), SVMXC__Group_Code__c = 'SMBJ');
        insert ObjSvcTeam;

        SVMXC__Territory__c territory2 = new SVMXC__Territory__c(
            Name = '[U2J] WEST-SOUTH TX HOUS (OLD)',
            SVMXC__Parent_Territory__c = territory1.Id,
            SVMXC__Description__c = 'TEST DESC',
            Service_Team__c = ObjSvcTeam.id  
        );
        insert territory2;
        
        test.startTest();
        territory1 = [select Id,SVMXC__Description__c from SVMXC__Territory__c where Id=: territory2.SVMXC__Parent_Territory__c];
        system.assertEquals(territory1.SVMXC__Description__c , 'TEST1');        
        territory1.SVMXC__Description__c = 'TEST2';
        update territory1;
        test.stopTest();
        
        Account Acc = [select Id,Service_Region__c  from Account where Id=: a.Id];
        system.assertEquals(Acc.Service_Region__c  , 'TEST2');
        
    }
}
public class AccountTestData 
{

    public AccountTestData()
    {
        
    }
    
    public static Account createAccount()
    {
        Account a = new Account();
        a.Name = 'My Varian Test Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createNationalAccount()
    {
        Account a = new Account();
        a.Name = 'My Varian National Account';
        a.BillingCountry='USA';
        a.BillingCity= 'Los Angeles';
        a.Country__c= 'USA';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'National';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';
        
        return a;
    }
    
    public static Account createStrategicAccount()
    {
        Account a = new Account();
        a.Name = 'My Varian Strategic Account';
        a.BillingCountry='USA';
        a.BillingCity= 'Los Angeles';
        a.Country__c= 'USA';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'Strategic';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createGPOAccount()
    {
        Account a = new Account();
        a.Name = 'My Varian GPO Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'GPO';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createIDNAccount()
    {
        Account a = new Account();
        a.Name = 'My Varian IDN Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'IDN';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createNationalAffiliationAccount(Id nationalAccountId)
    {
        Account a = new Account();
        a.Name = 'My Varian National Affiliation Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliated_to_National_Account__c = nationalAccountId;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createStrategicAffiliationAccount(Id strategicAccountId)
    {
        Account a = new Account();
        a.Name = 'My Varian Strategic Affiliation Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliated_to_Strategic_Account__c = strategicAccountId;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createGPOAffiliationAccount(Id gpoAccountId)
    {
        Account a = new Account();
        a.Name = 'My Varian GPO Affiliation Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliated_to_GPO_Account__c = gpoAccountId;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createIDNAffiliationAccount(Id idnAccountId)
    {
        Account a = new Account();
        a.Name = 'My Varian IDN Affiliation Account';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliated_to_IDN_Account__c = idnAccountId;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';

        return a;
    }
    
    public static Account createAccount(String strName, String strCountry, String strZip) 
    {
        Account a = new Account();
        a.Name = strName;
        a.BillingCountry=strCountry;
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = strZip;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = strCountry;  
        a.OMNI_Postal_Code__c = strZip;
        
        return a;
    }
    
    public static Account createAccount(String strName, String strCountry, String strZip, String strState) 
    {
        Account a = new Account();
        a.Name = strName;
        a.BillingCountry=strCountry;
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState = strState;
        a.BillingPostalCode = strZip;
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c = strState;
        a.OMNI_Country__c = strCountry;  
        a.OMNI_Postal_Code__c = strZip;
        
        return a;
    }    
    
 /*   public static List<Account> createAccounts(Integer numAccounts)
    {
        List<Account> accountList = new List<Account>();
        for(Integer i = 0; i < numAccounts; i++)
        {
            Account a = new Account();
            a.Name = 'My Varian Test Account' + i;
            a.BillingCountry='USA';
            a.BillingCity= 'Orem';
            a.Country__c= 'USA';
            a.BillingStreet ='1024 W 245 N';
            a.BillingState ='UT';
            a.BillingPostalCode = '6056'+i;
            a.Agreement_Type__c = 'Master Pricing';
            a.OMNI_Address1__c ='1024 W 245 N';
            a.OMNI_City__c ='Orem';
            a.OMNI_State__c ='UT';
            a.OMNI_Country__c = 'USA';  
            a.OMNI_Postal_Code__c = '6056'+i;

            accountList.add(a);
        }
        return accountList;
    } */
}
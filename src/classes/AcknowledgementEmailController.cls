/*
Test Class - Test Class - SR_SuccessEmailRestControllerTest
*/
global class AcknowledgementEmailController {                 
    
    map<string,string> emailmap = new map<string,string>();
    
    public AcknowledgementEmailController(){    
        list<EmailTemplate> lstEmailTemplates = [select Id,
                                                 Name,
                                                 DeveloperName from EmailTemplate 
                                                 where(Name= 'Quote SAP PreBookedSales - ERROR VF' OR
                                                       Name= 'Quote SAP PreBookedService - Error VF' OR 
                                                       Name= 'Addendum_Quote SAP PreBookedCombined - ERROR VF' OR 
                                                       Name= 'Addendum Quote SAP PreBookedSales - ERROR VF' OR
                                                       Name = 'Quote SAP PreBookedCombined - ERROR VF')];        
        
        
        if(lstEmailTemplates!=null && lstEmailTemplates.size()>0)
        {
            for(emailtemplate e: lstEmailTemplates){
                emailmap.put(e.name,e.id);
            }
        } 
    }
    
    //To send email when order submit
    
    public void SendAcknowledgementEmail( BigMachines__Quote__c pobjQuote, string pSalesMgr){    
         
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String functionality = '';
        
        if(pobjQuote.Order_Type__c == 'Services') 
            functionality = 'Prebooked Service';
        
        if(pobjQuote.Order_Type__c == 'Sales') 
            functionality = 'Prebooked Sales';
        
        if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.Fulfillment_Quote__c){
            functionality = 'Fullfilment';
        }
        
        Set<String> emailsSet = new Set<String>();
        
        if(pobjQuote.Order_Type__c == 'Sales' || pobjQuote.Order_Type__c == 'Combined' ){
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Prebooked Sales'; 
            
            emailsSet.addAll(getEmails(functionality, pobjQuote));
        }
        
        if((pobjQuote.Order_Type__c == 'Services' || pobjQuote.Order_Type__c == 'Combined') &&(pobjQuote.Interface_Status__c == 'Process')){
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Prebooked Service'; 
            
            List<Email_Dlists__c> DListRecs = [Select Id,
                                               Name,
                                               Primary_Dlist__c,
                                               secondary_Dlist__c,
                                               Admin_Dlist__c,
                                               Brachy_Dlist__c,
                                               Business_Dlist__c,
                                               Functionality__c,
                                               Region__c,
                                               Support_Dlist__c ,
                                               SalesOrg__c,
                                               DocType__c
                                               from Email_Dlists__c where Functionality__c = :functionality AND SalesOrg__c = :pobjQuote.Service_Org__c limit 1]; 
            
            system.debug('DListRecs---'+DListRecs);
            for(Email_Dlists__c DListRec : DListRecs){
                if(DListRec.DocType__c == 'ZCSS' || DListRec.DocType__c == 'ZSQT'){
                    if(DListRec.Primary_Dlist__c != null && DListRec.Primary_Dlist__c != '')
                        emailsSet.add(DListRec.Primary_Dlist__c);
                    
                    if(DListRec.Secondary_Dlist__c != null && DListRec.Secondary_Dlist__c != '')
                        emailsSet.add(DListRec.Secondary_Dlist__c);
                }
            }    
        }
        
        // Add to send email to user as per country for acknowledgement
        emailsSet.addAll(getUserEmailByCountry(pobjQuote, 'ACKNOWLEDGMENT'));
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toaddress = new List<String>(); 
        emailsSet.add('matt.carvalho@varian.com');
        //emailsSet.add('David.Biller@varian.com');
        //emailsSet.add('ani.Sinnarkar@varian.com');
        //emailsSet.add('Jovanny.Perez@varian.com');
        emailsSet.addAll(getUserEmails(pobjQuote));
        System.debug('-----emailsSet'+emailsSet);
        for(String s : emailsSet)
            toaddress.add(s.trim());
        
        if( pSalesMgr == '' || pSalesMgr == null )
            pSalesMgr = '' ;
        
        string emailSubject = '**Epot/SAP ORDER Submitted : '+pobjQuote.Name+ '(Mgr: '+ pSalesMgr  + '- 1st acknowledgement email the admin processing the order, receives' ;
        
        email.setSubject( emailSubject ) ;
        email.setToAddresses(toaddress);
        mail.setOrgWideEmailAddressId(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c);
        email.setHtmlBody('<html>' + '<br/>'+ '1st acknowledgement email the admin processing the order, receives.') ;
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}) ; 
    }
    
    
    // ERROR email will be fired from Trigger  
    public void SendErrorEmail( BigMachines__Quote__c pobjQuote, String emailTemplate){           
        if(emailmap.get(emailTemplate) != null){   
            Set<String> emailsSet = new Set<String>(); 
            emailsSet = returnDlistEmails(pobjQuote);

            // Add to send email to user as per country for error
            emailsSet.addAll(getUserEmailByCountry(pobjQuote, 'ERROR'));

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string [] toaddress = new List<String>();
            emailsSet.add('matt.carvalho@varian.com');
            //emailsSet.add('David.Biller@varian.com');
            //emailsSet.add('ani.Sinnarkar@varian.com');  
            //emailsSet.add('Jovanny.Perez@varian.com');
            //emailsSet.add('Florence.Jullian@varian.com');
            emailsSet.addAll(getUserEmails(pobjQuote)); 
            system.debug('emailsSet---'+emailsSet);               
            for(String s : emailsSet)
                toaddress.add(s.trim());
            mail.setToAddresses(toaddress);   
            mail.setTargetObjectId(pobjQuote.Submitted_To_SAP_By__c);
            mail.setOrgWideEmailAddressId(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c);
            mail.setWhatId(pobjQuote.Id);
            mail.setTemplateId(emailmap.get(emailTemplate));
            mail.setSaveAsActivity(false);
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            List<Attachment> aList  = [select Name, Body from Attachment where ParentId = :pobjQuote.Id limit 10];
            
            for(Attachment a : aList){    
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);                   
            }
            if(!fileAttachments.isEmpty())     
                mail.setFileAttachments(fileAttachments);
            
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }  
    }
    
    public Set<String> returnDlistEmails(BigMachines__Quote__c pobjQuote){
        String functionality = '';
        
        if(pobjQuote.Order_Type__c == 'Service') 
            functionality = 'Prebooked Service';
        
        if(pobjQuote.Order_Type__c == 'Sales') 
            functionality = 'Prebooked Sales';
        
        if(pobjQuote.Order_Type__c == 'Service' && pobjQuote.SAP_Booked_Service__c != null) 
            functionality = 'Booked Service';
        
        if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.SAP_Booked_Sales__c != null) 
            functionality = 'Booked Sales';
        
        if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.Fulfillment_Quote__c){
                functionality = 'Fullfilment';
        }
        
        Set<String> emailsSet = new Set<String>();
        
        if(pobjQuote.Order_Type__c == 'Sales' || pobjQuote.Order_Type__c == 'Combined'){
            
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Prebooked Sales';      
            
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Booked Sales';
            
            emailsSet.addAll(getEmails(functionality, pobjQuote));
        }
        
        if(pobjQuote.Order_Type__c == 'Services' || pobjQuote.Order_Type__c == 'Combined'){
            
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Prebooked Service';    
            
            if(pobjQuote.Order_Type__c == 'Combined') 
                functionality = 'Booked Service';
            
            List<Email_Dlists__c> DListRecs = [Select Id,
                                               Name,
                                               Primary_Dlist__c,
                                               secondary_Dlist__c,
                                               Admin_Dlist__c,
                                               Brachy_Dlist__c,
                                               Business_Dlist__c,
                                               Functionality__c,
                                               Region__c,
                                               Support_Dlist__c ,
                                               SalesOrg__c,
                                               DocType__c
                                               from Email_Dlists__c where Functionality__c = :functionality AND SalesOrg__c = :pobjQuote.Service_Org__c limit 1]; 
            
            system.debug('DListRecs---'+DListRecs);
            for(Email_Dlists__c DListRec : DListRecs){
                if(DListRec.DocType__c == 'ZCSS' || DListRec.DocType__c == 'ZSQT'){
                    if(DListRec.Primary_Dlist__c != null && DListRec.Primary_Dlist__c != '')
                        emailsSet.add(DListRec.Primary_Dlist__c);
                    
                    if(DListRec.Secondary_Dlist__c != null && DListRec.Secondary_Dlist__c != '')
                        emailsSet.add(DListRec.Secondary_Dlist__c);
                }
            }    
        }
        
        return emailsSet;
    }
    
    private Set<String> getEmails(String functionality, BigMachines__Quote__c objQuote){
        List<Email_Dlists__c> DListRecs = [Select Id,Name,Primary_Dlist__c,secondary_Dlist__c,Admin_Dlist__c,
                                                Brachy_Dlist__c,Business_Dlist__c,Functionality__c,Region__c,
                                                Support_Dlist__c,SalesOrg__c,Fulfillment_DList__c
                                                From Email_Dlists__c 
                                                Where Functionality__c =:functionality 
                                                And Region__c =:objQuote.Quote_Region__c]; 
            
        system.debug('DListRecs---'+DListRecs);
        Set<String> emails = new Set<String>();
        for(Email_Dlists__c DListRec : DListRecs){
            
            if(!String.isBlank(DListRec.Business_Dlist__c))
                emails.add(DListRec.Business_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Primary_Dlist__c))
                emails.add(DListRec.Primary_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Brachy_Dlist__c) && objQuote.Brachy_Sales__c)
                emails.add(DListRec.Brachy_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Support_Dlist__c))
                emails.add(DListRec.Support_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Admin_Dlist__c))
                emails.add(DListRec.Admin_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Secondary_Dlist__c))
                emails.add(DListRec.Secondary_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Fulfillment_DList__c) && objQuote.Fulfillment_Quote__c && objQuote.Order_Type__c == 'Sales'){
                emails.add(DListRec.Fulfillment_DList__c.trim());
            }
            
        }
        return emails;
    }

    private Set<String> getUserEmailByCountry(BigMachines__Quote__c objQuote, String category) {
        Set<String> emailsSet = new Set<String>();

        String Country;
        if(objQuote.BigMachines__Account__r.Country__c != null)
            Country = objQuote.BigMachines__Account__r.Country__c;
        else if (objQuote.Ship_To_Country__c != null)
            Country = objQuote.Ship_To_Country__c;
        else
            Country = objQuote.BigMachines__Account__r.BillingCountry;

        if (Country != null)
            Country = String.valueOf(Country).trim();
        Country = '%'+ Country +'%';
        
        List<Email_Dlists__c> email_dlist_list = [Select Id, Email_List__c From Email_Dlists__c Where Order_Status__c=: category and Country__c LIKE : Country limit 1];
        if(email_dlist_list.size() > 0) {
            for(Email_Dlists__c dlist : email_dlist_list) {
                List<String> e_list = dlist.Email_List__c.split(';');
                for (String u_email : e_list) {
                    system.debug('-----EMAIL-------'+u_email.trim());
                    emailsSet.add(u_email.trim());
                }
            }
        }
        return emailsSet;
    }

    private Set<String> getUserEmails(BigMachines__Quote__c objQuote){
        Set<String> emails = new Set<String>();
        List<Id> userIds = new List<Id>{objQuote.ownerId, objQuote.Submitted_To_SAP_By__c};
        Map<Id,User> users = new Map<Id,User>([Select Id, Email 
                                                    from User 
                                                    where Id IN:userIds]);
        for(User sfUser : users.values()){
            emails.add(sfUser.Email);
        }
        System.debug('-----emails'+emails+'---userIds'+userIds);
        return emails;
    }
}
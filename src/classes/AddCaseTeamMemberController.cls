public class AddCaseTeamMemberController 
{
    private String caseId = ApexPages.currentPage().getParameters().get('CaseId');
    public List<CaseTeamMember> existingCaseTeamMember;
    public List<Case_Team_Member__c> existingCustomCaseTeamMember;
    public List<Case_Team_Member__c> caseTeamMemberList;
    public Integer memberIndex {get; set;}    
    public Map<String, String> optionNameToId; 
    public Map<String, String> optionIdToName;
    public List<Case_Team_Member__c> caseTeamMembers
    {
        get
        {
            return caseTeamMemberList;
        }
        set;
    }

    public AddCaseTeamMemberController() 
    {
        caseTeamMemberList = new List<Case_Team_Member__c>();
        existingCaseTeamMember = [select Id, MemberId, ParentId, TeamRoleId from CaseTeamMember where ParentId = :caseId];
        existingCustomCaseTeamMember = [select Id, Case__c, MemberId__c from Case_Team_Member__c where Case__c = :caseId];
        optionNameToId = new Map<String, String>();
        optionIdToName = new Map<String, String>();
        for(CaseTeamRole role : [SELECT Name, Id FROM CaseTeamRole])
        {
            optionNameToId.put(role.Name, role.Id);
            optionIdToName.put(role.Id, role.Name);
        }          
        for(CaseTeamMember caseTeamMember : existingCaseTeamMember)
        {
            Case_Team_Member__c newCaseTeamMember = new Case_Team_Member__c();
            newCaseTeamMember.Case__c = caseTeamMember.ParentId;
            newCaseTeamMember.MemberId__c = caseTeamMember.MemberId;
            newCaseTeamMember.TeamRoleId__c = optionIdToName.get(caseTeamMember.TeamRoleId);
            caseTeamMemberList.add(newCaseTeamMember);
        }

        Case_Team_Member__c member1 = new Case_Team_Member__c();
        Case_Team_Member__c member2 = new Case_Team_Member__c();
        Case_Team_Member__c member3 = new Case_Team_Member__c();  
        member1.Case__c = caseId;
        member2.Case__c = caseId;
        member3.Case__c = caseId;
        caseTeamMemberList.add(member1);   
        caseTeamMemberList.add(member2);   
        caseTeamMemberList.add(member3);         
    }
    public void deleteTeamMember()
    {
        caseTeamMemberList.remove(memberIndex);
    }
    public PageReference Save()
    {
        Map<String, Case_Team_Member__c> customCaseTeamMemberToBeSaved = new Map<String, Case_Team_Member__c>();
        Map<String, CaseTeamMember> teamMemberToBeSaved = new Map<String, CaseTeamMember>();
        for(Case_Team_Member__c teamMemberCustom : caseTeamMemberList)
        {
            if(teamMemberCustom.MemberId__c != null)
            {
                if(String.isBlank(teamMemberCustom.TeamRoleId__c))
                {
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a team role');
                    ApexPages.addMessage(errorMsg);
                    return null;                    
                }            
                CaseTeamMember teamMember = new CaseTeamMember();
                teamMember.ParentId = caseId;
                teamMember.MemberId = teamMemberCustom.MemberId__c;
                teamMember.TeamRoleId = optionNameToId.get(teamMemberCustom.TeamRoleId__c);
                teamMemberToBeSaved.put(teamMember.MemberId, teamMember);
            }            
        }
        /*
        for(User user : [select Id, Name, Manager.Id from User where Id in :teamMemberToBeSaved.keySet()])
        {
            if(user.Manager.Id != null && !teamMemberToBeSaved.keySet().contains(user.Manager.Id))
            {
                CaseTeamMember teamMember = new CaseTeamMember();
                teamMember.ParentId = caseId;
                teamMember.MemberId = user.Manager.Id;
                teamMember.TeamRoleId = teamMemberToBeSaved.get(user.Id).TeamRoleId;
                teamMemberToBeSaved.put(teamMember.MemberId, teamMember);  
                Case_Team_Member__c newCaseTeamMember = new Case_Team_Member__c();
                newCaseTeamMember.Case__c = caseId;
                newCaseTeamMember.MemberId__c = teamMember.MemberId;
                newCaseTeamMember.TeamRoleId__c = optionNameToId.get(teamMember.TeamRoleId);
                caseTeamMemberList.add(newCaseTeamMember);               
            }
        }
        */
        if(existingCaseTeamMember.size() > 0)
        {
            delete existingCaseTeamMember;
        }
        upsert teamMemberToBeSaved.values();
        if(existingCustomCaseTeamMember.size() > 0)
        {
            delete existingCustomCaseTeamMember;
        }
        for(Case_Team_Member__c teamMemberCustom : caseTeamMemberList)
        {
            if(teamMemberCustom.MemberId__c != null)
            {
                customCaseTeamMemberToBeSaved.put(teamMemberCustom.MemberId__c, teamMemberCustom);
            }            
        }        
        upsert customCaseTeamMemberToBeSaved.values();
        return new PageReference('/' + caseId);
    }
    
    public PageReference AddOneRow()
    {
        Case_Team_Member__c member = new Case_Team_Member__c();
        caseTeamMemberList.add(member); 
        return null;
    }
    public PageReference Cancel()
    {
        return new PageReference('/' + caseId);      
    }       
}
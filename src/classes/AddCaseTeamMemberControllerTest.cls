@isTest
public class AddCaseTeamMemberControllerTest
{
    public static testMethod void AddCaseTeamMemberControllerTest()
    {
        String CaseRecType = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 
        String recTypeHD= Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 
        //Create Account
        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = 'AA787799';
        testAcc.CSS_District__c = 'Test';
        insert testAcc;
    
       //Create Location
        SVMXC__Site__c loc = new SVMXC__Site__c(SVMXC__Account__c = testAcc.Id, Name = 'Test Location');
        insert loc;
       //Create Product
        Product2 objProd2=new Product2();
        objProd2.Name = 'technical Test Product';
        objProd2.Material_Group__c= 'H:TRNING';
        objProd2.Budget_Hrs__c =10.0;
        objProd2.Credit_s_Required__c=10;
        objProd2.UOM__c='EA';
        insert objProd2;
    
       //Insert Case
        case cs = new case();
        cs.subject='test';
        cs.RecordTypeId=recTypeHD;
        cs.AccountId=testAcc.id;
        cs.SVMXC__Site__c= loc.id;
        insert cs;
        Case_Team_Member__c customCaseTeam = new Case_Team_Member__c();
        customCaseTeam.Case__c = cs.Id;
        customCaseTeam.TeamRoleId__c = 'GTS';
        customCaseTeam.MemberId__c = Userinfo.getUserId();
        insert customCaseTeam;
        List<CaseTeamRole> roles = [SELECT Name, Id FROM CaseTeamRole];
        CaseTeamMember standardCaseTeamMember = new CaseTeamMember();
        standardCaseTeamMember.ParentId = cs.Id;
        standardCaseTeamMember.MemberId = Userinfo.getUserId();
        standardCaseTeamMember.TeamRoleId = roles[0].Id;     
        insert standardCaseTeamMember;
        PageReference addCaseTeamMember = Page.AddCaseTeamMember;
        Test.setCurrentPage(addCaseTeamMember);
        ApexPages.currentPage().getParameters().put('CaseId', cs.Id);
        AddCaseTeamMemberController addCaseMember = new AddCaseTeamMemberController();
        addCaseMember.memberIndex = 2;
        addCaseMember.AddOneRow();
        addCaseMember.deleteTeamMember();
        addCaseMember.save();
        addCaseMember.Cancel();
    }
}
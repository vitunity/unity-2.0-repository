public class AddendumBookedProductController{
    public string QuoteId{get;set;}
    public boolean isBookedServiceEmail{get;set;}
    public class WrapperBookedProduct{
        public string ProductName{get;private set;}
        public string Message{get;private set;}
        public decimal ZPCP{get;private set;}
        public decimal ZPTH{get;private set;}
        public decimal ZTAR{get;private set;}
        public decimal SectionNumber{get;private set;}
        public decimal LineNumber{get;private set;}
        public string Description{get;private set;}
        public decimal GroupNumber{get;private set;} 
        
        public WrapperBookedProduct(string pname, string msg, decimal zpcp, decimal zpth, decimal ztar, decimal SectionNumber,decimal GroupNumber, decimal LineNumber, string Description){
            ProductName = pname;Message = msg;this.ZPCP = zpcp;this.zpth = zpth;this.ztar = ztar;this.SectionNumber = SectionNumber;this.GroupNumber = GroupNumber;this.LineNumber = LineNumber;this.Description = Description;
        }
    }
    public AddendumBookedProductController(){
        isBookedServiceEmail = true;
    }
    
    public list<WrapperBookedProduct> getBookedProduct(){
        list<WrapperBookedProduct> BPlist = new list<WrapperBookedProduct>();

        set<string> setBQMPartNumbers = new set<string>();
        List<BigMachines__Quote_Product__c> lstBMP = [select Id,Name,BigMachines__Sales_Price__c,BigMachines__Total_Price__c,Target_Price__c,Section_Number__c,Grp__c,Line_Number__c,BigMachines__Description__c from BigMachines__Quote_Product__c where 
        BigMachines__Quote__c =: QuoteId];
        
        for(BigMachines__Quote_Product__c p: lstBMP){
            setBQMPartNumbers.add((p.Name).tolowerCase());
        }

        map<string,string> mapAddendumPartNumbers = new map<string,string>();
        for(Addendum_Data__c p: [select Id,Product__r.BigMachines__Part_Number__c,Message__c  from Addendum_Data__c 
        where Product__r.BigMachines__Part_Number__c IN: setBQMPartNumbers]){
            mapAddendumPartNumbers.put((p.Product__r.BigMachines__Part_Number__c).tolowerCase(),p.Message__c );
        }
        
        for(BigMachines__Quote_Product__c p: lstBMP){
            string pname = (p.Name).tolowerCase();
            if(mapAddendumPartNumbers.containsKey(pname)){
                BPlist.add(new WrapperBookedProduct(p.Name,mapAddendumPartNumbers.get(pname),p.BigMachines__Sales_Price__c,p.BigMachines__Total_Price__c,p.Target_Price__c,p.Section_Number__c,p.Grp__c,p.Line_Number__c, p.BigMachines__Description__c));
            }
        }       
        return BPlist;
    }
}
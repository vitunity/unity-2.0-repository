@isTest(seealldata=true)
private class AddendumBookedProductController_Test {
    
    static testMethod void TestBookedProduct(){
        
        Account subscriptionAccount = TestUtils.getAccount();
        subscriptionAccount.Name = 'AUTO CREATE BILLING PLAN';
        subscriptionAccount.AccountNumber = 'Sales1212';
        subscriptionAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert subscriptionAccount;
        
        Contact con = TestUtils.getContact();
        con.AccountId = subscriptionAccount.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = subscriptionAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        Product2 installationProduct = new Product2();
        installationProduct.Name = 'Sample Sales Product';
        installationProduct.IsActive = true;
        installationProduct.ProductCode = 'Test Code123';
        installationProduct.Family = 'Subscription';
        installationProduct.Subscription_Product_Type__c = 'Installation';
        installationProduct.BigMachines__Part_Number__c = 'TESTWP1';
        insert installationProduct;
        
        
        BigMachines__Quote__c subscriptionQuoteAddOn = TestUtils.getQuote();
        subscriptionQuoteAddOn.BigMachines__Account__c = opp.AccountId;
        subscriptionQuoteAddOn.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteAddOn.National_Distributor__c = '121212';
        subscriptionQuoteAddOn.Order_Type__c = 'sales';
        subscriptionQuoteAddOn.Price_Group__c = 'Z2';
        insert subscriptionQuoteAddOn;
        
        BigMachines__Quote_Product__c quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.Name = 'TESTWP1';
        quoteProduct.BigMachines__Quote__c = subscriptionQuoteAddOn.Id;
        quoteProduct.BigMachines__Product__c = installationProduct.Id;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = 'Sales';
        quoteProduct.Standard_Price__c = 40000;
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.Line_Number__c= 12;
        quoteProduct.EPOT_Section_Id__c = 'TEST';
        quoteProduct.Subscription_Start_Date__c = Date.today();
        quoteProduct.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
        quoteProduct.Number_Of_Years__c = '3';
        quoteProduct.Subscription_End_Date__c = Date.today().addYears(Integer.valueOf(3));
        quoteProduct.Billing_Frequency__c = 'Annually';
        quoteProduct.Subscription_Sales_Type__c = 'Add-On';
        quoteProduct.Installation_Price__c = 0.0;
        quoteProduct.Subscription_Unit_Price__c = 2900;
        quoteProduct.Subscription_Implementation_At_Booking__c= 50;
        quoteProduct.SAP_Contract_Number__c = 'TEST1234';        
        insert quoteProduct;
        
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new AddMock()); 
            
            
            Addendum_Data__c Add = new Addendum_Data__c ();
            Add.Start_Date__c = system.today();
            Add.End_Date__c = system.today();
            Add.Product__c = installationProduct.id;
            Add.Region__c = 'NA';
            Add.Message__c = 'TEST MSG';
            insert Add; 
            
            AddendumBookedProductController conObj = new AddendumBookedProductController();
            conObj.QuoteId = subscriptionQuoteAddOn.Id;
            conObj.getBookedProduct();
            
        Test.Stoptest();
    }
    
    public class AddMock implements HTTpCalloutMock{
        
        public HTTPResponse respond(HTTPRequest req) {
            String bmSessionID = '12345';
            String response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
            HTTPResponse res=new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setBody(response);            
            res.setStatusCode(200);
            return res;            
        }
    }
}
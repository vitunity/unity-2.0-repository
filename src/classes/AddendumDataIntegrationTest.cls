@isTest(SeeAllData=true)
private class AddendumDataIntegrationTest {
    
    static testMethod void InsertAddendumData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new AddMock()); 

            Product2 p = new Product2();
            p.Name = 'TestAdd';
            insert p;
            
            Addendum_Data__c Add = new Addendum_Data__c ();
            Add.Start_Date__c = system.today();
            Add.End_Date__c = system.today();
            Add.Product__c = p.id;
            //Add.Product_Replacement_Part__c = p.id;
            Add.Region__c = 'NA';
            insert Add; 
            
      
        Test.Stoptest();
    } 
    static testMethod void UpdateAddData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new AddMock()); 

            Product2 p = new Product2();
            p.Name = 'TestAdd';
            insert p;
            
            Addendum_Data__c Add = new Addendum_Data__c ();
            Add.Start_Date__c = system.today();
            Add.End_Date__c = system.today();
            Add.Product__c = p.id;
            //Add.Product_Replacement_Part__c = p.id;
            Add.Region__c = 'NA';
            insert Add; 

            Addendum_Data__c Addobj = [select Id,Region__c from Addendum_Data__c where Id =: Add.Id];
            AddObj.Region__c = 'AUS';
            update Addobj;
            
      
        Test.Stoptest();
    } 
  
    static testMethod void DeleteAddData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new AddMock()); 

            Product2 p = new Product2();
            p.Name = 'TestAdd';
            insert p;
            
            Addendum_Data__c Add = new Addendum_Data__c ();
            Add.Start_Date__c = system.today();
            Add.End_Date__c = system.today();
            Add.Product__c = p.id;
            //Add.Product_Replacement_Part__c = p.id;
            Add.Region__c = 'WW';
            insert Add; 
            
            delete Add;
            
      
        Test.Stoptest();
    }
    
    public class AddMock implements HTTpCalloutMock{
        
        public HTTPResponse respond(HTTPRequest req) {
            String bmSessionID = '12345';
            String response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
            HTTPResponse res=new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setBody(response);            
            res.setStatusCode(200);
            return res;            
        }
    }
}
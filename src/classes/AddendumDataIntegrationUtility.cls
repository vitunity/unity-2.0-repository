/**************************************************************************\
Test Class - AddendumDataIntegrationTest
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
public class AddendumDataIntegrationUtility{
    /**************************************************************************\
    @Description - INC4202831 - Get session ID from the response.
    **************************************************************************/
    public Static string getSessionId(String response, String startTag, String endTag){
        String[] arr = response.split(startTag);
        arr = arr[1].split(endTag);
        String sessionID = arr[0];
        return sessionID;
    }
    /**************************************************************************\
    @Description - INC4202831 - Login request for BigMachines.
    **************************************************************************/
    public Static String getLoginRequest(string username, string password, string location){
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        '<soapenv:Header>'+
        '<bm:category xmlns:bm="urn:soap.bigmachines.com">Security</bm:category>'+
        '<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:schemaLocation>'+location+'</bm:schemaLocation></bm:xsdInfo>'+
        '</soapenv:Header>'+
        '<soapenv:Body>'+
        '<bm:login xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:userInfo>'+
        '<bm:username>'+username+'</bm:username>'+
        '<bm:password>'+password+'</bm:password>'+
        '<bm:sessionCurrency/>'+
        '</bm:userInfo>'+
        '</bm:login>'+
        '</soapenv:Body>'+
        '</soapenv:Envelope>';
    }
    /**************************************************************************\
    @Description - INC4202831 - Making a request to insert PLC Data.
    **************************************************************************/
    public Static string getAddPLCRequest(string sessionId, List<Addendum_Data__c> lstAddendumData ){
        String xmlreq ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        '<soapenv:Header>'+
        '<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:sessionId>'+sessionId+'</bm:sessionId>'+
        '</bm:userInfo>'+
        '<bm:category xmlns:bm="urn:soap.bigmachines.com">Data Tables</bm:category>'+
        '<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:schemaLocation>https://devvarian.bigmachines.com/bmfsweb/devvarian/schema/v1_0/datatables/AddendumParts.xsd</bm:schemaLocation>'+
        '</bm:xsdInfo>'+
        '</soapenv:Header>'+
        '<soapenv:Body>'+
        '<bm:add xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:DataTables bm:table_name="AddendumParts">';

        for(Addendum_Data__c p: lstAddendumData ){
            xmlreq += '<bm:each_record>'+
            '<bm:AddendumRecordID>'+p.Name+'</bm:AddendumRecordID>'+
            '<bm:PartNumber>'+p.Product__r.BigMachines__Part_Number__c+'</bm:PartNumber>'+
            '<bm:SalesRegion>'+((p.Region_code__c <> null)?'/'+p.Region_code__c +'/':'')+'</bm:SalesRegion>'+
            '<bm:Country>'+((p.Country_Code__c <> null)?'/'+p.Country_Code__c+'/':'')+'</bm:Country>'+
            //'<bm:StartDate>'+validDateformat(p.Start_Date__c)+'</bm:StartDate>'+
            //'<bm:EndDate>'+validDateformat(p.End_Date__c)+'</bm:EndDate>'+
            '<bm:Message>'+p.Message__c+'</bm:Message>'+
            '</bm:each_record>';
        }

        xmlreq += '</bm:DataTables></bm:add></soapenv:Body></soapenv:Envelope>';
        xmlreq  = xmlreq.replaceAll('null','');
        return xmlreq;
    }
    /**************************************************************************\
    @Description - INC4202831 - Making a request to update PLC Data..
    **************************************************************************/
    public static String getUpdatePLCData(String bmSessionID,string location){
      return '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
            +'<soapenv:Header>'
            +'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'
            +'<bm:sessionId>'+bmSessionID+'</bm:sessionId>'
            +'</bm:userInfo>'
            +'<bm:category xmlns:bm="urn:soap.bigmachines.com">Commerce</bm:category>'
            +'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'
            +'<bm:schemaLocation>'+location+'</bm:schemaLocation>'
            +'</bm:xsdInfo>'
            +'</soapenv:Header>'
            +'<soapenv:Body>'
            +'<bm:updateTransaction xmlns:bm="urn:soap.bigmachines.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
                +'<bm:transaction>'
                //+'<bm:id></bm:id>'
                +'<bm:AddendumRecordID>AD-0066</bm:AddendumRecordID>'
                +'<bm:process_var_name>Message</bm:process_var_name>'
                +'<bm:action_data>'
                +'<bm:action_var_name>updated successfull</bm:action_var_name>'
                +'</bm:action_data>'
                +'</bm:transaction>'
            +'</bm:updateTransaction>'
            +'</soapenv:Body>'
            +'</soapenv:Envelope>';
    }
    /**************************************************************************\
    @Description - INC4202831 - Deploy request to BigMachines.
    **************************************************************************/
    public Static String getDeployRequest(String location, String bmSessionID){
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        '<soapenv:Header>'+
        '<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:sessionId>'+bmSessionID+'</bm:sessionId>'+
        '</bm:userInfo>'+
        '<bm:category xmlns:bm="urn:soap.bigmachines.com">Data Tables</bm:category>'+
        '<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:schemaLocation>'+location+'</bm:schemaLocation>'+
        '</bm:xsdInfo>'+
        '</soapenv:Header>'+
        '<soapenv:Body>'+
        '<bm:deploy xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:DataTables bm:table_name="AddendumParts"/>'+
        '</bm:deploy>'+
        '</soapenv:Body>'+
        '</soapenv:Envelope>';
      }
    /**************************************************************************\
    @Description - INC4202831 - Request to delete PLC data from BigMachines.
    **************************************************************************/ 
    public Static String getDeletePLCRequest(String bmSessionID, set<string> ListPLCName, string location){
        string  xmlreq = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        '<soapenv:Header>'+
        '<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:sessionId>'+bmSessionID+'</bm:sessionId>'+
        '</bm:userInfo>'+
        '<bm:category xmlns:bm="urn:soap.bigmachines.com">Data Tables</bm:category>'+
        '<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:schemaLocation>'+location+'</bm:schemaLocation>'+
        '</bm:xsdInfo>'+
        '</soapenv:Header>'+
        '<soapenv:Body>'+
        '<bm:delete xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:DataTables bm:table_name="AddendumParts">'+
        
        '<bm:criteria>';
        for(String PLCName: ListPLCName){           
            xmlreq +='<bm:field>AddendumRecordID</bm:field>'+
            '<bm:value>'+(String.isBlank(PLCName)?'':PLCName)+'</bm:value>'+
            '<bm:comparator><![CDATA['+(String.isBlank(PLCName)?'<>':'=')+']]></bm:comparator>';
            
        }
        xmlreq += '</bm:criteria>'+
        '</bm:DataTables>'+
        '</bm:delete>'+
        '</soapenv:Body>'+
        '</soapenv:Envelope>';
        
        system.debug(':Delete XML: '+xmlreq);
        return xmlreq;
    }
    public static string validDateformat(Date d){
        string dd;
        if(d <> null){
            dd = d.month() +'/'+d.day()+'/'+d.year();
        }
        return dd;
    }
    /**************************************************************************\
    @Description - INC4202831 - Execute HTTP call-out all operations to BigMachines.
    **************************************************************************/
    public Static String makeRequest(String xml, String EndPoint){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('SOAPAction','Retrieve');
        req.setHeader('Accept-Encoding','gzip,deflate');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setBody(xml);
        req.setTimeout(120000);
        req.setEndpoint(EndPoint);
    
        String bodyRes = '';
        try{
            HttpResponse res = h.send(req);
            bodyRes = res.getBody();
            System.debug('bodyRes: '+ bodyRes);
        }catch(System.CalloutException e) {
            System.debug('Callout error: '+ e+'----'+e.getLineNumber());          
        }
        return bodyRes;
    }
}
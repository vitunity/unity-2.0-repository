/**************************************************************************\
Test Class - AddendumDataIntegrationTest 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
public class AddendumDataSynchronization{
    @future(callout=true)
    /**************************************************************************\
    @Description - STRY0068550 - Add new Addendum Data  in BigMachine.
    **************************************************************************/
    public static void Synchronization(set<Id> SetAddendumData){
        List<Addendum_Data__c> lstAddendumData = new List<Addendum_Data__c>();
        for(Addendum_Data__c p:[select id ,Name,BMId__c,End_Date__c,Message__c,Product__r.BigMachines__Part_Number__c,Country_Code__c,
        Region__c,Region_code__c ,Start_Date__c from Addendum_Data__c  where Id IN: SetAddendumData]){ 
            lstAddendumData.add(p);
        }
        if(lstAddendumData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;         
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = AddendumDataIntegrationUtility.getAddPLCRequest(sessionID,lstAddendumData);
            String responseAddPLC = AddendumDataIntegrationUtility.makeRequest(PLDReq,Endpoint );
            
            if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                //deploy PLC_Data
                String deployReq = AddendumDataIntegrationUtility.getDeployRequest(location,sessionID);
                String responseDeployPLC = AddendumDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
            }
        } 
    }
    /**************************************************************************\
    @Description - STRY0068550 - Update Addendum Data in BigMachine.
    **************************************************************************/
    @future(callout=true)
    public static void UpdatePLCData(set<Id> setPLCIds){
        List<Addendum_Data__c> lstAddendumData = new List<Addendum_Data__c>();
        set<string> SetAddendumData = new set<string>();
        for(Addendum_Data__c p:[select id ,Name,BMId__c,End_Date__c,Message__c,Product__r.BigMachines__Part_Number__c,Country_Code__c,
        Region__c,Region_code__c ,Start_Date__c from Addendum_Data__c  where Id IN: setPLCIds]){ 
            lstAddendumData.add(p);
            SetAddendumData.add(p.Name);
        }
        if(lstAddendumData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;         
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = AddendumDataIntegrationUtility.getAddPLCRequest(sessionID,lstAddendumData);
            
            
            String PLCDeleteReq = AddendumDataIntegrationUtility.getDeletePLCRequest(sessionID,SetAddendumData,location);
            String responseDeletePLC = AddendumDataIntegrationUtility.makeRequest(PLCDeleteReq,Endpoint );
            
            if(!String.isBlank(responseDeletePLC) && !responseDeletePLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                String responseAddPLC = AddendumDataIntegrationUtility.makeRequest(PLDReq,Endpoint );            
                if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                    //If fail then do something
                }else{
                    //deploy PLC_Data
                    String deployReq = AddendumDataIntegrationUtility.getDeployRequest(location,sessionID);
                    String responseDeployPLC = AddendumDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
                }
            }
        } 
    }
    /**************************************************************************\
    @Description - STRY0068550 - Delete Addendum Data in BigMachine.
    **************************************************************************/
    @future(callout=true)
    public static void DeletePLCData(set<string> SetAddendumData){
        if(SetAddendumData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;        
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = AddendumDataIntegrationUtility.getDeletePLCRequest(sessionID,SetAddendumData,location);
            String responseAddPLC = AddendumDataIntegrationUtility.makeRequest(PLDReq,Endpoint );
            
            if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                //deploy PLC_Data
                String deployReq = AddendumDataIntegrationUtility.getDeployRequest(location,sessionID);
                String responseDeployPLC = AddendumDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
            }
        } 
    }
    /**************************************************************************\
    @Description - STRY0068550 - Get session ID of BigMachines.
    **************************************************************************/
    public static string getSessionId(string username, string password, string location, string Endpoint){
        String loginXML = AddendumDataIntegrationUtility.getLoginRequest(username,password,location);
        String response = AddendumDataIntegrationUtility.makeRequest(loginXML,Endpoint);    
        String sessionID = AddendumDataIntegrationUtility.getSessionId(response,'<bm:sessionId>','</bm:sessionId>');
        system.debug('sessionID:'+sessionID);
        return sessionID;
    }
}
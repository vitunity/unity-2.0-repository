/***********************************************************************
* Author         : Varian
* Description	 : Any comparable object which can be extend to support sorting for any objects
* User Story     : 
* Created On     : 4/28/16
************************************************************************/

global without sharing virtual class AnyComparable implements Comparable, AnyObjectI {
	
	global Integer compareTo(Object compareTo) {
		AnyObjectI compareToObj = (AnyObjectI) compareTo;
		return this.getCompareField().toLowerCase().CompareTo(compareToObj.getCompareField().toLowerCase());
	}
    
    public virtual String getCompareField() {
    	return '';
    }
}
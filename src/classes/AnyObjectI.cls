/***********************************************************************
* Author         : Varian
* Description	 : Any comparable interface which can be extend to support sorting for any objects
* User Story     : 
* Created On     : 4/28/16
************************************************************************/

public interface AnyObjectI {
	String getCompareField();
}
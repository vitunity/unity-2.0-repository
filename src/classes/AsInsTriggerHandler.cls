public class AsInsTriggerHandler {

    public static void pouplateFromBoomi(map<String, As_Installed__c> mapVerIdAsIns) {
        
        if(mapVerIdAsIns.size() > 0) {
            for(SOI_Delivery_Verification__c ver : [SELECT Installed_Product__c, Work_Order__c, Sales_Order__c, 
                                                        Sales_Order_Item__c, Material__c, Work_Order__r.SVMXC__Company__c
                                                        FROM SOI_Delivery_Verification__c  WHERE Id IN :mapVerIdAsIns.keySet()]) {
                
                As_Installed__c asInsLocal = mapVerIdAsIns.get(ver.Id);

                system.debug('### debug install customer= ' + ver.Work_Order__r.SVMXC__Company__c);
                asInsLocal .Installed_Product__c = ver.Installed_Product__c;
                asInsLocal .Work_Order__c = ver.Work_Order__c;
                asInsLocal .SO_Number__c= ver.Sales_Order__c;
                asInsLocal .SO_Item__c = ver.Sales_Order_Item__c;
                /*move Material to separate method to write directly from record
                asInsLocal .Material__c = ver.Material__c;*/
                asInsLocal .Customer__c = ver.Work_Order__r.SVMXC__Company__c;            
            
            }
        }
    }
    
    //Add method to populate material from ERP_Material__c on As Installed and not from SOI Delivery Verification
    public static void populateMaterial(map<String, As_Installed__c>mapMatAsIns){
        if(mapMatAsIns.size() > 0){
            for(Product2 prod : [SELECT Id, Name, ProductCode FROM Product2 WHERE ProductCode IN :mapMatAsIns.keySet()]){
                As_Installed__c asInsLocal = mapMatAsIns.get(prod.ProductCode);
                asInsLocal.Material__c = prod.Id;
            }
        }
    }
}
public class AssignUserToGroupController {
    public Boolean showPGBln {get;set;}
    public final static string VMS_MyVarian_AdminPermissionsSet = '0PSE0000000HXgx'; // This multipicklist should be editable only to this permission set (Portal Admins)
    public final static string SystemAdminProfile ='00eE0000000mvyb';
    public Account acc{get;set;}
    public Contact_Role_Association__c contactRoleAssoc{get;set;}
    
    public AssignUserToGroupController(){
        contactRoleAssoc = new Contact_Role_Association__c();
    }
    
    public void changeAccount(){
        contactRoleAssoc.Account__c = null;
        showPGBln = false;
    }
    
    public void MultipicklistGroupData(){
        system.debug('---------------contactRoleAssoc.Account__c------------' + contactRoleAssoc.Account__c);
        if(contactRoleAssoc.Account__c != null){
            
            acc = [Select Id,Selected_Groups__c,Name,IsPartner,IsCustomerPortal,Quality__c,Peer_to_Peer__c,RapidArc__c,IGRT__c,RPM__c,IMRT__c,SRS_SBRT__c,Paperless__c,Latitude__c,Longitude__c from Account where Id =: contactRoleAssoc.Account__c];
            system.debug('****IsPArtner**'+acc.IsPartner);
            if(acc.IsPartner){
                showPGBln= false;
                String userid = UserInfo.getUserId();    
                Id profileId = UserInfo.getProfileId();
                system.debug ('USER Info ---> '+ userId);
                //show public group list on account page only if it is sys admin profile or Portal Admin users with VMS_MyVarian_AdminPermissionsSet
                if (ProductUtils.isPortalAdmin(userId) || ProductUtils.isSystemAdmin(profileId)) showPGBln = true; 

            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Partner Account.');
                ApexPages.addMessage(myMsg);
                showPGBln = false;
            }
         }
    }
}
@RestResource(urlMapping='/Attachment/*')
global class AttachmentController {
    @HttpGet
    global static void getBlob() {
    RestRequest req = RestContext.request;
    String Id= RestContext.request.params.get('AttachmentId') ;
    Attachment a = [SELECT Id,Name,ParentId,Body,BodyLength,ContentType FROM Attachment where id=:Id];
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type',a.ContentType);
    res.responseBody = a.Body; 
    } 
}
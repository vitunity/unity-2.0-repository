@istest
   private class AttachmentControllerTestClass 
   {
    static testMethod void myAttachmentControllerTest2() 
    {
        VU_Products__c pr= new VU_Products__c();
                   pr.Product_Description__c= 'test' ;
                   pr.Product_Family__c= 'Oncology ';
                   pr.Playlist_Id__c = '1234err567y';
                   insert pr;
        Attachment attach = new Attachment(Name='test', ParentId=pr.ID, body=EncodingUtil.base64Decode('testtest'));
        insert attach;
        String AttachId = attach.id;
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/Attachment'; 
        req.addParameter('AttachmentId', AttachId);
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        AttachmentController.getBlob();
    }
}
public with sharing class AttachmentTriggerHandler extends TriggerDispatcher.TriggerDispatcherBaseImpl 
{
    public override void beforeInsert(List<sObject> newValues) 
    { 
        Set<Id> woIds = new Set<Id>();
        Map<Id, Id> attachmentIdToWOId = new Map<Id, Id>();
        Map<Id, String> woIdToName = new Map<Id, String>();
        for(sObject s : newValues)
        {
            Attachment att = (Attachment)s;
            if(att.Name.contains('.pdf') && att.Name.contains('FSR'))
            {
                woIds.add(att.parentId);
            }
        }

        for(SVMXC__Service_Order__c wo : [select Id, Name from SVMXC__Service_Order__c where Id in: woIds])
        {
            woIdToName.put(wo.Id, wo.Name + '.pdf');
        }

        for(sObject s : newValues)
        {
            Attachment att = (Attachment)s;
            if(woIdToName.keySet().contains(att.parentId))
            {
                att.Name = woIdToName.get(att.parentId);
            }
        }
    }
}
@isTest
public with sharing class Attachmentcounttrigger_test{
	public static ID TechRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Group_Members__c').get('Technician'); 
	public static Attachment att;
	public static testmethod void test1()
	{
	    CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
		SVMXC__Service_Group__c ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(),
        SVMXC__Group_Code__c = 'ABC');
        insert ObjSvcTeam;
        //insert technician Record
        SVMXC__Service_Group_Members__c ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true,
        SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com',
        SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId());
        insert ObjTech; 
		SVMXC__Service_Order__c pwo = new SVMXC__Service_Order__c();
	    pwo.recordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
	    pwo.Event__c = true;
	    pwo.SVMXC__Group_Member__c = ObjTech.Id;
	    insert pwo;
		att = new Attachment();
	    att.Name = 'test.pdf';
	    att.parentId = pwo.id;
	    att.body = EncodingUtil.base64Decode('testtest');
	    insert att;
	    delete att;
   	}
}
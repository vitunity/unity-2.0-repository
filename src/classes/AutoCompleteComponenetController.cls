// ===========================================================================
// Component: AutoCompleteComponenetController
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Controller for AutoCompleteComponent used in Docusign
// ===========================================================================
// Created On: 02-01-2018
// ChangeLog:  
// ===========================================================================
global class AutoCompleteComponenetController
{
    private static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();

    //Function that will run the query and return the query back to the component
    @RemoteAction
    global static string findSObjects(String qry)
    {
        //String Filter for appending to WHERE clause
        String filter = 'LIKE \'%'+String.escapeSingleQuotes(qry)+'%\' ';

        //Wrapper List
        List<ResponseWrapper> responseWrapperList = New List<ResponseWrapper>();

        //Querying for Users
        String userSoqlBegin = 'SELECT Id,Name,Email FROM User WHERE ';
        String filterUserName = 'Name ' + filter + ' AND isActive = TRUE';
        String usersSoql = userSoqlBegin + filterUserName;
        System.Debug('Le User SOQL: '+usersSoql);

        List<User> userList = New List<User>();
        userList = Database.query(usersSoql);
        system.Debug('Le User List: '+userList);

        if(userList.size()>0)
        {
            for(User userObj : userList)
            {
                ResponseWrapper responseWrapperObj = New ResponseWrapper();
                responseWrapperObj.id = userObj.Id;
                responseWrapperObj.name = userObj.Name;
                responseWrapperObj.emailId = userObj.Email;
                responseWrapperObj.objectApi = 'User';
                responseWrapperObj.DisplayText = userObj.Name + ' (' + userObj.Email +') User';
                responseWrapperList.add(responseWrapperObj);
            }
        }

        //Querying for Contacts
        String contactsSoqlBegin = 'SELECT Id,FirstName,LastName,Email FROM Contact WHERE ';
        String filterFirstName = 'FirstName ' + filter;
        String filterLastName = 'LastName ' + filter;
        String contactsSoql = contactsSoqlBegin + filterFirstName + ' OR ' + filterLastName;
        System.Debug('Le Contacts SOQL: '+contactsSoql);

        List<Contact> contactList = New List<Contact>();
        contactList = Database.query(contactsSoql);
        System.Debug('Le Contacts List: '+contactList);

        if(contactList.size()>0)
        {
            for(Contact contactObj : contactList)
            {
                ResponseWrapper responseWrapperObj = New ResponseWrapper();
                responseWrapperObj.id = contactObj.Id;
                responseWrapperObj.name = contactObj.FirstName + ' ' + contactObj.LastName;
                responseWrapperObj.emailId = contactObj.Email;
                responseWrapperObj.objectApi = 'Contact';
                responseWrapperObj.DisplayText = responseWrapperObj.name + ' (' + contactObj.Email + ') Contact';
                responseWrapperList.add(responseWrapperObj);
            }
        }

        //Querying For Leads
        String leadSoqlBegin = 'SELECT Id,FirstName,LastName,Email FROM Lead WHERE ';
        String filterLeadFirstName = 'FirstName ' + filter;
        String filterLeadLastName = 'LastName ' + filter;
        String leadsSoql = leadSoqlBegin + filterLeadFirstName + ' OR ' + filterLeadLastName;
        System.Debug('Le Leads SOQL: '+leadsSoql);

        List<Lead> leadList = New List<Lead>();
        leadList = Database.query(leadsSoql);
        System.Debug('Le Leads List: '+leadList);

        if(leadList.size()>0)
        {
            for(Lead leadObj : leadList)
            {
                ResponseWrapper responseWrapperObj = New ResponseWrapper();
                responseWrapperObj.id = leadObj.Id;
                responseWrapperObj.name = leadObj.FirstName + ' ' + leadObj.LastName;
                responseWrapperObj.emailId = leadObj.Email;
                responseWrapperObj.objectApi = 'Lead';
                responseWrapperObj.DisplayText = responseWrapperObj.name + ' (' + responseWrapperObj.emailId + ') Lead';
                responseWrapperList.add(responseWrapperObj);
            }
        }

        //Querying for Vendor Contacts
        String vendorContactSoqlBegin = 'SELECT Id,First_Name__c,Last_Name__c,Email__c FROM Vendor_Contact__c WHERE ';
        String filterVendorContactFirstName = 'First_Name__c ' + filter;
        String filterVendorContactLastName = 'Last_Name__c ' + filter;
        String vendorContactSoql = vendorContactSoqlBegin + filterVendorContactFirstName + ' OR ' + filterVendorContactLastName;
        System.Debug('Le Vendor Contact SOQL: '+vendorContactSoql);

        List<Vendor_Contact__c> vendorContactList = New List<Vendor_Contact__c>();
        vendorContactList = Database.query(vendorContactSoql);
        System.Debug('Le Vendor Contact List: '+vendorContactList);

        if(vendorContactList.size()>0)
        {
            for(Vendor_Contact__c vendorContactObj : vendorContactList)
            {
                ResponseWrapper responseWrapperObj = New ResponseWrapper();
                responseWrapperObj.Id = vendorContactObj.Id;
                responseWrapperObj.Name = vendorContactObj.First_Name__c + ' ' + vendorContactObj.Last_Name__c;
                responseWrapperObj.EmailId = vendorContactObj.Email__c;
                responseWrapperObj.ObjectApi = 'Vendor_Contact__c';
                responseWrapperObj.DisplayText = responseWrapperObj.Name + ' (' + responseWrapperObj.EmailId + ') Vendor Contact';
                responseWrapperList.add(responseWrapperObj);
            }
        }

        System.Debug('Le Response Wrapper List: '+responseWrapperList);

        //Generating final response
        String response;
        if(responseWrapperList.size()>0)
            response = JSON.serialize(responseWrapperList);
        else
            return NULL;

        System.Debug('Le Final Response: '+response);
        return response;
    }

    //Wrapper class to send back response
    global with sharing class ResponseWrapper
    {
        String Id{get;set;}
        String Name{get;set;}
        String EmailId{get;set;}
        String ObjectApi{get;set;}
        String DisplayText{get;set;}
    }





}
// ===========================================================================
// Component: AutoCompleteComponenetController
//    Author: Asif Muhammad
// Copyright: 2018 by Standav
//   Purpose: Test Class for for AutoCompleteComponenetController used in AutoCompleteComponenet
// ===========================================================================
// Created On: 30-01-2018
// ChangeLog:  
// ===========================================================================
@isTest
public with sharing class AutoCompleteComponenetControllerTest
{
  @testSetup
  private static void testSetupMethod()
  {
    
    //Creating User Record
    User user = New User();
    user.ProfileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
    user.LastName = 'Bond';
    user.Email = 'jamesBond@gmail.com';
    user.Username = 'jamesBond@mi6.com';
    user.CompanyName = 'test';
    user.Title = 'title';
    user.Alias = 'Agent007';
    user.TimeZoneSidKey = 'America/Los_Angeles';
    user.EmailEncodingKey = 'UTF-8';
    user.LanguageLocaleKey = 'en_US';
    user.LocaleSidKey = 'en_Us';
    insert user;
    System.assert(user.Id!=NULL);

    //Creating Account Record
    Account accountObj = New Account();
    accountObj.Name = 'Her Majesty\'s Secret Service';
    accountObj.Bill_To__r.BillingCity = 'Anytown';
    accountObj.Country__c = 'Algeria';
    insert accountObj;
    System.assert(accountObj.Id!=NULL);

    //Creating Contact record 
    Contact contactObj = New Contact();
    contactObj.LastName = 'Bond';
    contactObj.FirstName = 'James';
    contactObj.Email = 'james@mi5.com';
    contactObj.Functional_Role__c = 'Consultant';
    contactObj.AccountId = accountObj.Id;
    insert contactObj;
    System.assert(contactObj.Id!=NULL);
    
    //Creating Lead Record
    Lead leadObj = New Lead();
    leadObj.LastName = 'Bond';
    leadObj.FirstName = 'James';
    leadObj.status = 'New';
    leadObj.Company = 'Company';
    insert leadObj;
    System.assert(leadObj.Id!=NULL);
    
    //Creating Vendor record
    Vendor__c vendor = New Vendor__c();
    vendor.Name = 'James Bond';
    Id recordTypeId = Schema.SObjectType.Vendor__c.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
    vendor.RecordTypeId = recordTypeId;
    insert vendor;
    System.assert(vendor.Id!=NULL);
    
    //Creating Vemdor Contact Record
    Vendor_Contact__c vendorObj = New Vendor_Contact__c();
    vendorObj.First_Name__c = 'James';
    vendorObj.Last_Name__c = 'Bond';
    vendorObj.Email__c = 'jamesBond@mi5.com';
    vendorObj.Vendor__c = vendor.Id;
    insert vendorObj; 
    System.assert(vendorObj.Id!=NULL);
  }
  
  @isTest
  private static void testmethod1()
  {
    
    User user = [Select Id From User Where Alias='Agent007'];
    System.runAs(user){
        Test.startTest();
            AutoCompleteComponenetController.findSObjects('Bond');
        Test.stopTest();
        }
    }
}
public with sharing class AutoCreateBillingPlan {
    
    public static Boolean STOP_BILLING_PLAN_CRERATION = false;
    
    /**
     * For subscription family products create billing plan with due date using billing frequency and subscription start date and end date
     */
    public static void createSubscriptionLines(List<BigMachines__Quote_Product__c> quoteProducts, Map<Id,BigMachines__Quote_Product__c> oldQuoteProducts){
        
        List<BigMachines__Quote_Product__c> subscriptionProducts = [
            SELECT Id, BigMachines__Product__r.Family, Standard_Price__c, Subscription_Start_Date__c,
            Subscription_End_Date__c,Number_Of_Years__c,Billing_Frequency__c,BigMachines__Total_Price__c,
            BigMachines__Quote__r.BigMachines__Opportunity__r.Primary_Contact_Name__r.Email,Add_On_Start_Date__c,
            Subscription_Sales_Type__c,Installation_Price__c,Subscription_Unit_Price__c,BigMachines__Quantity__c,
            SAP_Contract_Number__c,Group_Sequence__c,BigMachines__Product__r.Subscription_Product_Type__c,
            Subscription_Implementation_At_Booking__c, BigMachines__Quote__r.CurrencyISOCode
            FROM BigMachines__Quote_Product__c
            WHERE BigMachines__Product__r.Family = 'Subscription'
            AND Subscription_Start_Date__c != null
            AND Subscription_End_Date__c != null
            AND Id IN:quoteProducts
            //AND Header__c = true
        ];
        
        List<BigMachines__Quote_Product__c> updatedQuoteProducts;
        if(oldQuoteProducts != null)
            updatedQuoteProducts = getUpdatedQuoteProducts(subscriptionProducts, oldQuoteProducts);
        else
            updatedQuoteProducts = subscriptionProducts;
        
        
        List<Subscription_Line_Item__c> subscriptionLines = new List<Subscription_Line_Item__c>();
        
        List<BigMachines__Quote_Product__c> newSubscriptions = new List<BigMachines__Quote_Product__c>();
        List<BigMachines__Quote_Product__c> addOnSubscriptions = new List<BigMachines__Quote_Product__c>();
         
        for(BigMachines__Quote_Product__c quoteProduct : updatedQuoteProducts)
        {
            if(quoteProduct.Subscription_Sales_Type__c != 'Add-On'){
                newSubscriptions.add(quoteProduct);
            }else{
                addOnSubscriptions.add(quoteProduct);
            }
        }   
        if(oldQuoteProducts != null){
            deleteSubscriptionLines(updatedQuoteProducts);
        }
        subscriptionLines.addAll(createNewSubscriptionBillingPlan(newSubscriptions));
        subscriptionLines.addAll(createAddOnBillingPlan(addOnSubscriptions));
        
        if(!subscriptionLines.isEmpty()){ 
            insert subscriptionLines;
        }                                                       
    }
    
    
    /**
     * Create billing plan for subscription part for Add on
    */
    public static List<Subscription_Line_Item__c> createAddOnBillingPlan(List<BigMachines__Quote_Product__c> quoteProducts)
    {
        Map<String, Integer> billingFrequencyMap = new Map<String, Integer>{'Annually' => 1,'Bi-Annual' => 2,'Quarterly' => 4,'Monthly' => 12};
        Map<String, Integer> billingFrequencyMonthMap = new Map<String, Integer>{'Annually' => 12,'Bi-Annual' => 6,'Quarterly' => 3,'Monthly' => 1};
        List<Subscription_Line_Item__c> subscriptionLines = new List<Subscription_Line_Item__c>();
        
        //Subscription part billing
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts)
        {
            Double billingAmount = 0.0;
            String productType = quoteProduct.BigMachines__Product__r.Subscription_Product_type__c;
            Date cycleStartDate = quoteProduct.Add_On_Start_Date__c;
            Integer months = billingFrequencyMonthMap.get(quoteProduct.Billing_Frequency__c);
            
            if(productType == 'Subscription')
            {
                //Billing Cycle For Original Quote
                Integer noOfBillingCycles = Integer.valueOf(quoteProduct.Number_Of_Years__c) * billingFrequencyMap.get(quoteProduct.Billing_Frequency__c);  
                Date cycleOrigStartDate = quoteProduct.Subscription_Start_Date__c;
              
    
                
                //End Billing Cycle For Original Quote
                
                
                //Billing Plan calculation for Add on : Start
                Integer numberOfDays = quoteProduct.Add_On_Start_Date__c.DaysBetween(quoteProduct.Subscription_End_Date__c);
                Decimal pricePerDay = (quoteProduct.BigMachines__Total_Price__c /numberOfDays);
                Date addOnCycleDate;
                Date addOnStartDate = quoteProduct.Add_On_Start_Date__c;
                Integer addOnCounter = noOfBillingCycles;
                Integer startDateDay = quoteProduct.Subscription_Start_Date__c.day();
                boolean addonCurrentDate = false;
                
                for(Integer counter = 0; counter <= noOfBillingCycles-1; counter++)
                {
                    cycleOrigStartDate = cycleOrigStartDate.addMonths(months);
                    if(counter > 0)
                        cycleOrigStartDate = AutoCreateBillingPlan.monthEndCalculation(startDateDay,cycleOrigStartDate);
                    
                    if(addonCurrentDate)
                    {
                        addOnCycleDate = cycleOrigStartDate;
                        break;
                    }
                    
                    if(quoteProduct.Add_On_Start_Date__c < cycleOrigStartDate)
                    {
                        addOnCycleDate = cycleOrigStartDate;
                        break;
                    }
                    else if (quoteProduct.Add_On_Start_Date__c == cycleOrigStartDate)
                    {
                        addonCurrentDate = true;
                    }
                    addOnCounter--;
                }
                
                Decimal installmentAmount = (quoteProduct.Add_On_Start_Date__c.DaysBetween(addOnCycleDate)* pricePerDay );
                Decimal netAmount = quoteProduct.BigMachines__Total_Price__c - installmentAmount ;
                Integer wholeTotalPrice = Integer.valueOf(netAmount);
                Decimal fractionPart = netAmount - wholeTotalPrice;
                Decimal modAmount = Math.mod(wholeTotalPrice,(addOnCounter -1));
                Decimal subsequentAmount = (wholeTotalPrice - modAmount)/(addOnCounter-1);
                
                for(Integer counter = 0; counter <= addOnCounter-1; counter++)
                {
                    Subscription_Line_Item__c billingCycle = getSubscriptionLineItem(
                        quoteProduct, 
                        addOnStartDate, 
                        addOnCycleDate, 
                        Double.valueOf(counter != (addOnCounter-1)?installmentAmount:(installmentAmount + modAmount + fractionPart)),
                        Integer.valueOf(quoteProduct.Group_Sequence__c), 
                        productType == 'Installation'?1:counter+1
                    );
                    subscriptionLines.add(billingCycle);
                    addOnStartDate = billingCycle.Billing_End_Date__c;
                    addOnCycleDate = addOnStartDate.addMonths(months);
                    addOnCycleDate = monthEndCalculation(startDateDay,addOnCycleDate);
                    installmentAmount = subsequentAmount;
                }
                
                //End Billing Plan calculation for Add on 
            }
            else if(productType == 'Installation')
            {
                Double implAmount = 0.0;
                Integer implCounter = 0;
                Double implementationPercent = quoteProduct.Subscription_Implementation_At_Booking__c>0?quoteProduct.Subscription_Implementation_At_Booking__c:100.0;
                implAmount = quoteProduct.BigMachines__Total_Price__c * implementationPercent/100;
                Subscription_Line_Item__c implAtBooking = getSubscriptionLineItem(
                    quoteProduct, 
                    cycleStartDate, 
                    cycleStartDate.addMonths(months), 
                    implAmount, 
                    Integer.valueOf(quoteProduct.Group_Sequence__c), 
                    1
                );
                implAtBooking.Implementation_Sequence__c = implCounter +=1;
                subscriptionLines.add(implAtBooking);
                
                if(implementationPercent < 100.0){
                    Subscription_Line_Item__c implAtAcceptance = getSubscriptionLineItem(
                        quoteProduct, 
                        cycleStartDate, 
                        cycleStartDate.addMonths(months), 
                        Double.valueOf(quoteProduct.BigMachines__Total_Price__c - implAmount), 
                        Integer.valueOf(quoteProduct.Group_Sequence__c), 
                        1
                    );
                    implAtAcceptance.Implementation_Sequence__c = implCounter +=1;
                    subscriptionLines.add(implAtAcceptance);
                }
            }
        }
        return subscriptionLines;
    }
    
   
    
    /**
     * Create billing plan for subscription part based of billing frequeny and number of years
     */
    public static List<Subscription_Line_Item__c> createNewSubscriptionBillingPlan(List<BigMachines__Quote_Product__c> quoteProducts){
        Map<String, Integer> billingFrequencyMap = new Map<String, Integer>{'Annually' => 1,'Bi-Annual' => 2,'Quarterly' => 4,'Monthly' => 12};
        Map<String, Integer> billingFrequencyMonthMap = new Map<String, Integer>{'Annually' => 12,'Bi-Annual' => 6,'Quarterly' => 3,'Monthly' => 1};
        List<Subscription_Line_Item__c> subscriptionLines = new List<Subscription_Line_Item__c>();
        
        //Subscription part billing
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            Double billingAmount = 0.0;
            String productType = quoteProduct.BigMachines__Product__r.Subscription_Product_type__c;
            Date cycleStartDate = quoteProduct.Subscription_Start_Date__c;
            Integer months = billingFrequencyMonthMap.get(quoteProduct.Billing_Frequency__c);
            Date cycleEndDate = cycleStartDate.addMonths(months);
            
            if(productType == 'Subscription')
            {
                Integer noOfBillingCycles = Integer.valueOf(quoteProduct.Number_Of_Years__c) * billingFrequencyMap.get(quoteProduct.Billing_Frequency__c);  
                Integer wholeTotalPrice = Integer.valueOf(quoteProduct.BigMachines__Total_Price__c);
                Decimal fractionPart = quoteProduct.BigMachines__Total_Price__c - wholeTotalPrice;
                Decimal modAmount = Math.mod(wholeTotalPrice,noOfBillingCycles);
                Decimal installmentAmount = (wholeTotalPrice - modAmount)/noOfBillingCycles;
                Integer startDateDay = quoteProduct.Subscription_Start_Date__c.day();
                for(Integer counter = 0; counter <= noOfBillingCycles-1; counter++)
                {
                    Subscription_Line_Item__c billingCycle = getSubscriptionLineItem(
                        quoteProduct, 
                        cycleStartDate, 
                        cycleEndDate, 
                        //Double.valueOf(quoteProduct.BigMachines__Total_Price__c/noOfBillingCycles), 
                        Double.valueOf(counter != noOfBillingCycles-1?installmentAmount:(installmentAmount + modAmount + fractionPart)),
                        Integer.valueOf(quoteProduct.Group_Sequence__c), 
                        productType == 'Installation'?1:counter+1
                    );
                    subscriptionLines.add(billingCycle);
                    cycleStartDate = billingCycle.Billing_End_Date__c;
                    cycleEndDate = cycleStartDate.addMonths(months);
                    cycleEndDate = monthEndCalculation(startDateDay,cycleEndDate);
                }
            }
            else if(productType == 'Installation')
            {
                Double implAmount = 0.0;
                Integer implCounter = 0;
                Double implementationPercent = quoteProduct.Subscription_Implementation_At_Booking__c>0?quoteProduct.Subscription_Implementation_At_Booking__c:100.0;
                implAmount = quoteProduct.BigMachines__Total_Price__c * implementationPercent/100;
                Subscription_Line_Item__c implAtBooking = getSubscriptionLineItem(
                    quoteProduct, 
                    cycleStartDate, 
                    cycleStartDate.addMonths(months), 
                    implAmount, 
                    Integer.valueOf(quoteProduct.Group_Sequence__c), 
                    1
                );
                implAtBooking.Implementation_Sequence__c = implCounter +=1;
                subscriptionLines.add(implAtBooking);
                
                if(implementationPercent < 100.0){
                    Subscription_Line_Item__c implAtAcceptance = getSubscriptionLineItem(
                        quoteProduct, 
                        cycleStartDate, 
                        cycleStartDate.addMonths(months), 
                        Double.valueOf(quoteProduct.BigMachines__Total_Price__c - implAmount), 
                        Integer.valueOf(quoteProduct.Group_Sequence__c), 
                        1
                    );
                    implAtAcceptance.Implementation_Sequence__c = implCounter +=1;
                    subscriptionLines.add(implAtAcceptance);
                }
            }
        }
        return subscriptionLines;
    }
    
    @TestVisible
    private static Map<String,List<Subscription_Line_Item__c>> getOldSubscriptionLineItems(List<BigMachines__Quote_Product__c> quoteProducts){
        Set<String> contractNumbers = new set<String>();
        Map<String,List<Subscription_Line_Item__c>> subscriptionLineItems = new Map<String,List<Subscription_Line_Item__c>>();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            contractNumbers.add(quoteProduct.SAP_Contract_Number__c);
        }
        
        List<Subscription_Line_Item__c> lineItems = [
            SELECT Id,Due_date__c,Amount__c,Year__c,Quote_Product__c,
            Quote_Product__r.SAP_Contract_Number__c
            FROM Subscription_Line_Item__c
            WHERE Quote_Product__r.SAP_Contract_Number__c IN:contractNumbers
            AND Quote_Product__r.Subscription_Sales_Type__c = 'New'
            ORDER BY Year__c ASC
        ];
        
        for(Subscription_Line_Item__c subscriptionLine : lineItems){
            if(subscriptionLineItems.containsKey(subscriptionLine.Quote_Product__r.SAP_Contract_Number__c)){
                subscriptionLineItems.get(subscriptionLine.Quote_Product__r.SAP_Contract_Number__c).add(subscriptionLine);
            }else{
                subscriptionLineItems.put(subscriptionLine.Quote_Product__r.SAP_Contract_Number__c, new List<Subscription_Line_Item__c>{subscriptionLine});
            }
        }
        return subscriptionLineItems;
    }
    
    /**
     * Create subscription line item instance using given data
     */
    private static Subscription_Line_Item__c getSubscriptionLineItem(
        BigMachines__Quote_Product__c quoteProduct, 
        Date startDate, 
        Date endDate, 
        Double amount,
        Integer lineNumber,
        Integer year
    ){
        Subscription_Line_Item__c subscriptionLine = new Subscription_Line_Item__c();
        subscriptionLine.Quote_Product__c = quoteProduct.Id;
        subscriptionLine.Billing_Start_Date__c = startDate;
        subscriptionLine.Billing_End_Date__c = endDate;
        subscriptionLine.Due_Date__c = startDate;
        subscriptionLine.Payment_Status__c = 'Not Paid';
        subscriptionLine.Amount__c = Decimal.valueOf(amount).setScale(2);
        subscriptionLine.Email__c = quoteProduct.BigMachines__Quote__r.BigMachines__Opportunity__r.Primary_Contact_Name__r.Email;
        subscriptionLine.Line_Number__c = lineNumber;
        subscriptionLine.year__c = year;
        subscriptionLine.CurrencyISOCode = quoteProduct.BigMachines__Quote__r.CurrencyISOCode;
        return subscriptionLine;
    }
    
    private static void deleteSubscriptionLines(List<BigMachines__Quote_Product__c> quoteProducts){
        Set<Id> quoteProductIds = new Set<Id>();
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            quoteProductIds.add(quoteProduct.Id);
        }
        
        List<Subscription_Line_Item__c> subscriptionLines = [SELECT Id 
                                                                FROM Subscription_Line_Item__c 
                                                                WHERE Quote_Product__c IN:quoteProductIds
                                                                LIMIT 10000];
        
        if(!subscriptionLines.isEmpty()) delete subscriptionLines;       
    }
    
    /**
     * Check if the quote product has been updated with new subscription information and return only updated quote products to process
     */
    private static List<BigMachines__Quote_Product__c> getUpdatedQuoteProducts(List<BigMachines__Quote_Product__c> quoteProducts,Map<Id,BigMachines__Quote_Product__c> oldQuoteProducts){
        
        List<BigMachines__Quote_Product__c> updatedQPs = new List<BigMachines__Quote_Product__c>();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            BigMachines__Quote_Product__c oldQuoteProduct = oldQuoteProducts.get(quoteProduct.Id);
            if(quoteProduct.Subscription_Start_Date__c != oldQuoteProduct.Subscription_Start_Date__c ||
            quoteProduct.Subscription_End_Date__c != oldQuoteProduct.Subscription_End_Date__c ||
            quoteProduct.BigMachines__Quantity__c != oldQuoteProduct.BigMachines__Quantity__c ||
            quoteProduct.BigMachines__Total_Price__c != oldQuoteProduct.BigMachines__Total_Price__c ||
            quoteProduct.Billing_Frequency__c != oldQuoteProduct.Billing_Frequency__c ||
            quoteProduct.Subscription_Implementation_At_Booking__c != oldQuoteProduct.Subscription_Implementation_At_Booking__c ){
                updatedQPs.add(quoteProduct);
            }
        }
        
        return updatedQPs;
    }
    
    /**
     * Update group sequence field on add-on quote product based on previous sequence for new quote
     */
    public static void updateGroupSequence(List<BigMachines__Quote_Product__c> quoteProducts){
        Set<String> contractNumbers = new Set<String>();
        Set<String> subscriptionQuotes = new Set<String>();
        Map<String, Set<Id>> quotesLinkedToSubscriptions = new Map<String, Set<Id>>();
        List<BigMachines__Quote_Product__c> addOnNewProducts  = new List<BigMachines__Quote_Product__c>();
        Map<String, Integer> groupSequence = new Map<String, Integer>();
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            if(quoteProduct.Subscription_Sales_Type__c == 'Add-On'){
                contractNumbers.add(quoteProduct.SAP_Contract_Number__c);
                addOnNewProducts.add(quoteProduct);
            }else{
                if(quoteProduct.Grp__c != null){
                    quoteProduct.Group_Sequence__c = Integer.valueOf(String.valueOf(quoteProduct.Grp__c).right(2) + '0');
                }
            }
        }
        
        //Get old and new quote products associated with contract
        List<BigMachines__Quote_Product__c> subscriptionQuoteProducts = [
            SELECT Id, BigMachines__Quote__c, SAP_Contract_Number__c
            FROM BigMachines__Quote_Product__c 
            WHERE SAP_Contract_Number__c IN:contractNumbers 
            AND SAP_Contract_Number__c!=null
            ////AND BigMachines__Quote__r.Subscription_Status__c = 'Success'
        ];
        
        for(BigMachines__Quote_Product__c quoteProduct : subscriptionQuoteProducts){
            subscriptionQuotes.add(quoteProduct.BigMachines__Quote__c);
            if(quotesLinkedToSubscriptions.containsKey(quoteProduct.SAP_Contract_Number__c)){
                quotesLinkedToSubscriptions.get(quoteProduct.SAP_Contract_Number__c).add(quoteProduct.BigMachines__Quote__c);
                continue;
            }
            quotesLinkedToSubscriptions.put(quoteProduct.SAP_Contract_Number__c, new Set<Id>{quoteProduct.BigMachines__Quote__c});
        }
        
        List<AggregateResult> addOnQuoteProducts = [
            SELECT MAX(Group_Sequence__c) grpSequence,BigMachines__Quote__c
            FROM BigMachines__Quote_Product__c
            WHERE BigMachines__Quote__c IN:subscriptionQuotes
            GROUP BY BigMachines__Quote__c
        ];
        
        for(AggregateResult quoteProduct : addOnQuoteProducts){
            groupSequence.put(String.valueOf(quoteProduct.get('BigMachines__Quote__c')),Integer.valueOf(quoteProduct.get('grpSequence')));
        }
        
        for(String contractNumber : quotesLinkedToSubscriptions.keySet()){
            Integer maxNumber = 0;
            for(Id quoteId : quotesLinkedToSubscriptions.get(contractNumber)){
                if(groupSequence.containsKey(quoteId)){
                    if(maxNumber < groupSequence.get(quoteId)){
                        maxNumber = groupSequence.get(quoteId);
                    }
                }
            }
            groupSequence.put(contractNumber, maxNumber);
        }
        
        for(BigMachines__Quote_Product__c quoteProduct : addOnNewProducts)
        {
            if(groupSequence.containsKey(quoteProduct.SAP_Contract_Number__c)){
                quoteProduct.Group_Sequence__c = groupSequence.get(quoteProduct.SAP_Contract_Number__c) + 10;
                groupSequence.put(quoteProduct.SAP_Contract_Number__c, Integer.valueOf(quoteProduct.Group_Sequence__c));
            }else{
                quoteProduct.Group_Sequence__c = 40;
            }
        }
    }
    
    private static Date monthEndCalculation(Integer startDateDay,Date endDate)
    {
        Set<integer> days31Month = new Set<integer>{1,3,5,7,8,10,12};
        if(endDate.month() != 2 )
        {
            if(startDateDay == 29 && endDate.day() != 29)
               endDate = endDate.addDays(29-endDate.day());
            
            else if (startDateDay == 30 && endDate.day() != 30)
               endDate = endDate.addDays(30-endDate.day());
            
            else if (startDateDay == 31 && endDate.day() != 31)
            {
                if(days31Month.contains(endDate.month()))
                {
                    endDate = endDate.addDays(31-endDate.day());
                }
            }
            
        }
        return endDate;
    }
}
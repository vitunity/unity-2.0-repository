@isTest(seeAllData=true)
private class AutoCreateBillingPlanTest {
  
    public static BigMachines__Quote__c subscriptionQuoteNew;
    public static BigMachines__Quote__c subscriptionQuoteRenewal;
    public static BigMachines__Quote__c subscriptionQuoteAddOn;
  
     /**
     * Test method for creating billing cycles after subscription quote product gets created for quote
     */
    static testMethod void testCreateSubscriptionLines(){
        
        Account subscriptionAccount = TestUtils.getAccount();
        subscriptionAccount.Name = 'AUTO CREATE BILLING PLAN';
        subscriptionAccount.AccountNumber = 'Sales1212';
        subscriptionAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert subscriptionAccount;
        
        Contact con = TestUtils.getContact();
        con.AccountId = subscriptionAccount.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = subscriptionAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        
        Test.startTest();
            setUpSubscriptionTestData(opp);
            List<Subscription_Line_Item__c> subscriptionLines = [
                SELECT Id,Billing_Start_Date__c,Due_Date__c,Billing_End_Date__c,Quote_Product__c
                FROM Subscription_Line_Item__c
                WHERE Quote_Product__r.BigMachines__Quote__c =:subscriptionQuoteNew.Id 
                OR Quote_Product__r.BigMachines__Quote__c =:subscriptionQuoteRenewal.Id 
                OR Quote_Product__r.BigMachines__Quote__c =:subscriptionQuoteAddOn.Id 
                ORDER BY Billing_Start_Date__c ASC
            ];
            //System.assertEquals(9, subscriptionLines.size());
          
            Map<Id,List<Subscription_Line_Item__c>> quoteProductSubscriptionLines = new Map<Id,List<Subscription_Line_Item__c>>();
          
            for(Subscription_Line_Item__c subscriptionLine : subscriptionLines){
                if(!quoteProductSubscriptionLines.containsKey(subscriptionLine.Quote_Product__c)){
                    quoteProductSubscriptionLines.put(subscriptionLine.Quote_Product__c, new List<Subscription_Line_Item__c>{subscriptionLine});
                    continue;
                }
                quoteProductSubscriptionLines.get(subscriptionLine.Quote_Product__c).add(subscriptionLine); 
            }
          
            List<BigMachines__Quote_Product__c> quoteProducts = [
                SELECT Id,Billing_Frequency__c,Subscription_Sales_Type__c,Subscription_Start_Date__c,
                Subscription_End_Date__c,Add_On_Start_Date__c,BigMachines__Product__r.Subscription_Product_Type__c
                FROM BigMachines__Quote_Product__c
                WHERE BigMachines__Quote__c =:subscriptionQuoteNew.Id
                OR BigMachines__Quote__c =:subscriptionQuoteAddOn.Id 
                OR BigMachines__Quote__c =:subscriptionQuoteRenewal.Id 
            ];
          
            BigMachines__Quote_Product__c quoteProductToUpdate;
          
            //Assertions
            for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            
                List<Subscription_Line_Item__c> subscriptionLineItems = quoteProductSubscriptionLines.get(quoteProduct.Id);
            
                if(quoteProduct.Subscription_Sales_Type__c.equals('New') && quoteProduct.BigMachines__Product__r.Subscription_Product_Type__c == 'Subscription'){
                    //System.assertEquals(3,subscriptionLineItems.size());
                    quoteProductToUpdate = quoteProduct;
                }else if(quoteProduct.Subscription_Sales_Type__c.equals('New') && quoteProduct.BigMachines__Product__r.Subscription_Product_Type__c == 'Installation'){
                    //System.assertEquals(1,subscriptionLineItems.size());
                }else if(quoteProduct.Subscription_Sales_Type__c.equals('Add-On')){
                    //System.assertEquals(2,subscriptionLineItems.size());
                    for(Subscription_Line_Item__c subscriptionLine : subscriptionLineItems){
                        System.debug('----QuoteProduct'+quoteProduct.Subscription_Start_Date__c+'--'+quoteProduct.Subscription_End_date__c+'--'+quoteProduct.Add_On_Start_Date__c);
                        System.debug('---subscriptionLine'+subscriptionLine.Due_Date__c);
                    }
                }else{
                    //System.assertEquals(3,subscriptionLineItems.size());
                }
            }
          
            //test update subscription plan
            quoteProductToUpdate.Subscription_Start_Date__c = quoteProductToUpdate.Subscription_Start_Date__c.addMonths(1);
            quoteProductToUpdate.Subscription_End_Date__c = quoteProductToUpdate.Subscription_End_Date__c.addMonths(1);
            update quoteProductToUpdate;
        Test.stopTest();
    }
    /**
     * Utility method to create test subscription quote
     */
    public static void setUpSubscriptionTestData(Opportunity opp){
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 subscriptionProduct = new Product2();
        subscriptionProduct.Name = 'Sample Sales Product';
        subscriptionProduct.IsActive = true;
        subscriptionProduct.ProductCode = 'Test Code123';
        subscriptionProduct.Family = 'Subscription';
        subscriptionProduct.Subscription_Product_Type__c = 'Subscription';
        //insert subscriptionProduct;
        
        Product2 installationProduct = new Product2();
        installationProduct.Name = 'Sample Sales Product';
        installationProduct.IsActive = true;
        installationProduct.ProductCode = 'Test Code123';
        installationProduct.Family = 'Subscription';
        installationProduct.Subscription_Product_Type__c = 'Installation';
        insert new List<Product2>{subscriptionProduct,installationProduct};
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = subscriptionProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 2100;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        //insert pbEntry;
        PricebookEntry pbEntry1 = new PricebookEntry();
        pbEntry1.Product2Id = installationProduct.Id;
        pbEntry1.Pricebook2Id = standardPricebookId;
        pbEntry1.UnitPrice = 3500;
        pbEntry1.CurrencyISOCode = 'USD';
        pbEntry1.IsActive = true;
        insert new List<PricebookEntry>{pbEntry, pbEntry1};
        
        subscriptionQuoteNew = TestUtils.getQuote();
        subscriptionQuoteNew.Name = 'QUOTE-TEST1';
        subscriptionQuoteNew.BigMachines__Account__c = opp.AccountId;
        subscriptionQuoteNew.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteNew.National_Distributor__c = '121212';
        subscriptionQuoteNew.Order_Type__c = 'sales';
        subscriptionQuoteNew.Price_Group__c = 'Z2';
        
        subscriptionQuoteRenewal = TestUtils.getQuote();
        subscriptionQuoteRenewal.BigMachines__Account__c = opp.AccountId;
        subscriptionQuoteRenewal.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteRenewal.National_Distributor__c = '121212';
        subscriptionQuoteRenewal.Order_Type__c = 'sales';
        subscriptionQuoteRenewal.Price_Group__c = 'Z2';
        
        subscriptionQuoteAddOn = TestUtils.getQuote();
        subscriptionQuoteAddOn.BigMachines__Account__c = opp.AccountId;
        subscriptionQuoteAddOn.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteAddOn.National_Distributor__c = '121212';
        subscriptionQuoteAddOn.Order_Type__c = 'sales';
        subscriptionQuoteAddOn.Price_Group__c = 'Z2';
        insert new List<BigMachines__Quote__c>{subscriptionQuoteNew, subscriptionQuoteRenewal, subscriptionQuoteAddOn};
        
        insert new List<BigMachines__Quote_Product__c>{
            getQuoteProduct(subscriptionQuoteNew.Id,subscriptionProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteNew.Id,installationProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteRenewal.Id,subscriptionProduct.Id, '3', 'Annually','Renewal')
        };
      
        Subscription__c subscription1 = new Subscription__c();
        subscription1.Name = 'TEST1234';
        subscription1.Ext_Quote_Number__c = 'QUOTE-TEST1';
        subscription1.ERP_Site_Partner__c = 'Sales11111';
        insert subscription1;
      
        List <BigMachines__Quote_Product__c> lstQuoteProd= new List<BigMachines__Quote_Product__c>();
         lstQuoteProd.add(getQuoteProduct(subscriptionQuoteAddOn.Id,subscriptionProduct.Id, '3', 'Annually','Add-On'));
         lstQuoteProd.add(getQuoteProduct(subscriptionQuoteAddOn.Id,installationProduct.Id, '3', 'Annually','Add-On'));
         insert lstQuoteProd;
    }
    
    
    
    private static BigMachines__Quote_Product__c getQuoteProduct(Id quoteId,Id subscriptionProductId, String numberOfYears, String billingFrequency, String salesType){
        BigMachines__Quote_Product__c quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.BigMachines__Quote__c = quoteId;
        quoteProduct.BigMachines__Product__c = subscriptionProductId;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = 'Sales';
        quoteProduct.Standard_Price__c = 40000;
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.Line_Number__c= 12;
        quoteProduct.EPOT_Section_Id__c = 'TEST';
        quoteProduct.Subscription_Start_Date__c = Date.today();
        quoteProduct.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
        quoteProduct.Number_Of_Years__c = numberOfYears;
        quoteProduct.Subscription_End_Date__c = Date.today().addYears(Integer.valueOf(numberOfYears));
        quoteProduct.Billing_Frequency__c = billingFrequency;
        quoteProduct.Subscription_Sales_Type__c = salesType;
        quoteProduct.Installation_Price__c = 0.0;
        quoteProduct.Subscription_Unit_Price__c = 2900;
        quoteProduct.Subscription_Implementation_At_Booking__c= 50;
        if(salesType != 'Renewal'){
          quoteProduct.SAP_Contract_Number__c = 'TEST1234';
        }
        return quoteProduct;
    }
    
    
    
}
/**
 * Autocreates clearance by product for every new product
 */
public with sharing class AutoCreateClearanceByProduct {
    
    public static void createClearanceByProduct(List<Product2> products){
        List<Clearance_by_Product__c> clearances = new List<Clearance_by_Product__c>();
        
        ProductProfile__c productProfile = ProductProfile__c.getValues('ProfileId');
        
        if(productProfile!=null){
            for(Product2 product : products){
                if(product.Product_Type__c == 'Sellable'){
                    Clearance_by_Product__c productClearance = new Clearance_by_Product__c();
                    productClearance.Product__c = product.Id;
                    productClearance.Product_Profile__c = productProfile.ProfileId__c;
                    clearances.add(productClearance);
                }
            }
            insert clearances;
        }
    }
}
@isTest
private class AutoCreateClearanceByProductTest {

    static testMethod void createClearanceByProductTest() {
        
        Review_Products__c productProfile = new Review_Products__c();
        productProfile.Product_Profile_Name__c = 'Test Prodile';
        insert productProfile;
        
        ProductProfile__c productProfileId = new ProductProfile__c();
        productProfileId.Name = 'ProfileId';
        productProfileId.ProfileId__c = productProfile.Id;
        insert productProfileId;
        
        List<Product2> products = new List<Product2>();
        for(Integer counter=0; counter < 201; counter++){
            Product2 product = new Product2();
            product.Name = 'Test'+counter;
            product.Product_Type__c = 'Sellable';
            products.add(product);
        }
        insert products;
        
        List<Clearance_by_Product__c> clearances = [Select Id From Clearance_by_Product__c];
        System.assertEquals(201, clearances.size());
    }
}
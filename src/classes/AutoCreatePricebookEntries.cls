global class AutoCreatePricebookEntries implements Database.Batchable<sObject>,Schedulable{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Id standardPricebookId;
        if(!Test.isRunningTest()){
            standardPricebookId = [Select Id from Pricebook2 where IsStandard = true][0].Id;
        }else{
            standardPricebookId = Test.getStandardPricebookId();
        }
        
        String query = 'Select Id,(Select Id,CurrencyISOCode '
                                    +' From PricebookEntries'
                                    +' Where Pricebook2Id =: standardPricebookId) '
                        +' From Product2';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Product2> products){
        Schema.DescribeFieldResult fieldResult = PricebookEntry.CurrencyISOCode.getDescribe();
        List<Schema.PicklistEntry> currencyCodes = fieldResult.getPicklistValues();
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        
        Id standardPricebookId;
        if(!Test.isRunningTest()){
            standardPricebookId = [Select Id from Pricebook2 where IsStandard = true][0].Id;
        }else{
            standardPricebookId = Test.getStandardPricebookId();
        }
        
        for(Product2 product : products){
            System.debug('------product'+product+'----product.PricebookEntries.size()'+product.PricebookEntries.size());
            if(product.PricebookEntries.size() == 1){
                System.debug('------product'+product.PricebookEntries);
                for(Schema.PicklistEntry currencyCode : currencyCodes){
                    if(currencyCode.getValue() != product.PricebookEntries[0].CurrencyISOCode){
                        pricebookEntries.add(
                                                new PricebookEntry(Product2Id = product.Id,
                                                                    Pricebook2Id = standardPricebookId,
                                                                    UnitPrice = 0,
                                                                    CurrencyISOCode = currencyCode.getValue(),
                                                                    IsActive = true)
                                            );
                    }
                }
            }
        }
        
        System.debug('------priceBookEntries'+priceBookEntries.size());
        if(!pricebookEntries.isEmpty()) insert priceBookEntries;
    }
    
    global void execute(SchedulableContext SC){        
        AutoCreatePricebookEntries createPricebookEntries = new AutoCreatePricebookEntries ();
        Database.executeBatch(createPricebookEntries);
    }
    global void finish(Database.BatchableContext BC){
    }
}
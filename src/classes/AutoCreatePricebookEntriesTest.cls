@isTest
private class AutoCreatePricebookEntriesTest {

    static testMethod void testAutoCreatePricebookEntriesBatch() {
        insertProductsAndPricebookEntries();
        Test.startTest();
        	Database.executeBatch(new AutoCreatePricebookEntries());
        Test.stopTest();
        Schema.DescribeFieldResult fieldResult = PricebookEntry.CurrencyISOCode.getDescribe();
   		List<Schema.PicklistEntry> currencyCodes = fieldResult.getPicklistValues();
   		
        List<PricebookEntry> pricebookEntries = [Select Id From PricebookEntry];
        System.assertEquals(200*currencyCodes.size(),pricebookEntries.size());
    }
    
    private static void insertProductsAndPricebookEntries(){
    	
    	Id standardPricebookId = Test.getStandardPricebookId();
    	
    	System.debug('----standardPricebookId'+standardPricebookId);
    	System.debug('----standardPricebook'+[Select Id,Name From Pricebook2 Where Id =: standardPricebookId]);
    	
    	List<Product2> products = new List<Product2>();
        for(Integer counter = 0; counter < 200; counter++){
        	Product2 product = new Product2();
        	product.Name = 'Test Product'+counter;
        	product.IsActive = true;
        	products.add(product);
        }
        insert products;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Integer counter = 0; counter < 200; counter++){
        	if(counter<100){
        		PricebookEntry pbEntry = new PricebookEntry();
        		pbEntry.Product2Id = products[counter].Id;
   				pbEntry.Pricebook2Id = standardPricebookId;
   				pbEntry.UnitPrice = 111;
   				pbEntry.CurrencyISOCode = 'USD';
   				pricebookEntries.add(pbEntry);
        	}else{
        		PricebookEntry pbEntry = new PricebookEntry();
        		pbEntry.Product2Id = products[counter].Id;
   				pbEntry.Pricebook2Id = standardPricebookId;
   				pbEntry.UnitPrice = 111;
   				pbEntry.CurrencyISOCode = 'NZD';
   				pricebookEntries.add(pbEntry);
        	}
        }
        
        insert pricebookEntries;
    }
}
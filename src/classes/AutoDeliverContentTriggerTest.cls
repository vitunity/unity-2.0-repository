@isTest(SeeAllData=true)
private class AutoDeliverContentTriggerTest
{
    @isTest
    public static void testAutoDeliverContentTrigger()
    {
        ContentWorkspace workspace = [select Id from ContentWorkspace where Name = 'Charket – documentation'];

        ContentVersion version = new ContentVersion();
        version.Title = '[test]UnitTestTitle';
        version.VersionData = Blob.valueOf('UnitTestData');
        version.PathOnClient = 'UnitTest.pdf';
        version.FirstPublishLocationId = workspace.Id;
        insert version;
    }
}
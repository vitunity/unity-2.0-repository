public class AutoFillAccountfield{
    public static void fillSurvey(List<HD_Case_Closed_Survey_Results__c> lstSR){
        set<Id> setContactId = new set<Id>();
        for(HD_Case_Closed_Survey_Results__c s : lstSR){
            if(s.Contact__c <> null){
                setContactId.add(s.Contact__c);
            }
        }
        
        if(setContactId.size() > 0){
            map<Id,Contact> mapContact = new map<Id,Contact>([select id,Account.CSS_District__c,Account.District__c,Account.Region1__c,Account.Service_Region__c 
            from Contact where Id IN: setContactId and AccountId <> null]);
            for(HD_Case_Closed_Survey_Results__c s : lstSR){
                if(s.Contact__c <> null){
                    if(mapContact.ContainsKey(s.Contact__c)){
                        Contact con = mapContact.get(s.Contact__c);
                        s.District__c = con.Account.District__c;
                        s.Region__c = con.Account.Region1__c;
                        s.CSS_District__c = con.Account.CSS_District__c;
                        s.Service_Region__c = con.Account.Service_Region__c;
                    }
                }
            }
        }
    }
}
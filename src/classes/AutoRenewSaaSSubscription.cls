global class AutoRenewSaaSSubscription {
  @future(callout=true)
  public static void renewSubscription(Id subscriptionId){
    Subscription__c subscription = [
      SELECT Id, Name, Account__r.Name, Account__c, Qumulate_Group_Id__c, Number_of_Licences__c
      FROM Subscription__c
      WHERE Id =:subscriptionId
    ];
    
    BigMachinesTransactions BMITxn = new BigMachinesTransactions();
    
    //send login request
    String loginXML = BMITxn.getLoginRequest();
    String response = sendRequest(loginXML, BMITxn.bmiConfig.Endpoint__c, subscription, BMITxn.bmiConfig.Error_Notification_Email__c);
    System.debug('----response'+response);
    if(!String.isBlank(response) && !response.contains('<bm:success>')){
      sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, subscription, 'Failed to send login request');
      return;
    }
    String sessionId = extractElementValue(response,'<bm:sessionId>','</bm:sessionId>');
    
    //Send create transaction request
    String createTransactionXML = BMITxn.getCreateTransactionRequest(sessionId);
    response = '';
    response = sendRequest(createTransactionXML.replace('&','&amp;'), BMITxn.bmiConfig.Endpoint__c, subscription, BMITxn.bmiConfig.Error_Notification_Email__c);
    System.debug('----requestcreateTransactionXML'+createTransactionXML);
    System.debug('----responsecreateTransactionXML'+response);
    if(!String.isBlank(response) && !response.contains('<bm:success>')){
      sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, subscription, 'Failed to send create transaction request');
      return;
    }
    String bmId = extractElementValue(response,'<bm:id>','</bm:id>');
    
    //Send addConfiguration request
    String addConfigurationXML = BMITxn.getAddConfigurationRequest(sessionId,bmId,String.valueOf(subscription.Number_of_Licences__c),String.valueOf(Date.today()),'Annually','1','Renewal');
    response = '';
    response = sendRequest(addConfigurationXML.replace('&','&amp;'), BMITxn.bmiConfig.Endpoint__c, subscription, BMITxn.bmiConfig.Error_Notification_Email__c);
    System.debug('----requestaddConfigurationXML'+addConfigurationXML);
    System.debug('----responseaddConfigurationXML'+response);
    if(!String.isBlank(response) && !response.contains('<bm:success>')){
      sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, subscription, 'Failed to send add configuration request');
      return;
    }
    
    //public String getUpdateTransactionRequest(String bmSessionID, String bmID, String sfdcAccountID, String accountName, String qumulateGroupUUID){
    //Send updateTransaction request
    String updateTransactionXML = BMITxn.getUpdateTransactionRequest(sessionId,bmId,subscription.Account__c,subscription.Account__r.Name,subscription.Qumulate_Group_Id__c);
    response = '';
    response = sendRequest(updateTransactionXML.replace('&','&amp;'), BMITxn.bmiConfig.Endpoint__c, subscription, BMITxn.bmiConfig.Error_Notification_Email__c);
    System.debug('----requestupdateTransactionXML'+updateTransactionXML);
    System.debug('----responseupdateTransactionXML'+response);
    if(!String.isBlank(response) && !response.contains('<bm:success>')){
      sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, subscription, 'Failed to send update transaction request');
      return;
    }
  }
  
  private static String extractElementValue(String response, String startTag, String endTag) {
        String[] arr = response.split(startTag);
        arr = arr[1].split(endTag);
        String sessionID = arr[0];
        return sessionID;
    }
  
  private static String sendRequest(String xmlRequest, String endPoint, Subscription__c subscription, String email){
    Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setBody(xmlRequest);
        req.setTimeout(120000);
        req.setEndpoint(endPoint);
        String response = '';
        try{
            HttpResponse res = h.send(req);
            response = res.getBody();
      }catch(System.CalloutException e) {
        sendErrorEmail(email, subscription, 'Failed to send http request');
      }
      return response;
  }
  
  private static void sendErrorEmail(String email, Subscription__c subscription, String message){
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {email};
    mail.setToAddresses(toAddresses);
    mail.setSenderDisplayName('Salesforce Support');
    mail.setSubject('Error occured while auto renewing subscription:'+subscription.Name);
    mail.setPlainTextBody('URL:'+URL.getSalesforceBaseUrl()+'/'+subscription.Id+'\n '+message);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }
  
  /**
   * Close saas subscription after related renewal contract is created
   */
  public static void closeSaasSubscription(List<BigMachines__Quote_Product__c> quoteProducts){
    List<Subscription__c> closedSubscriptions = new List<Subscription__c>();
    Set<String> subscriptionNumbers = new Set<String>();
    for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
      if(quoteProduct.Subscription_Sales_Type__c == 'Renewal'){
        if(!String.isBlank(quoteProduct.SAP_Contract_Number__c)){
          subscriptionNumbers.add(quoteProduct.SAP_Contract_Number__c);
        }
      }
    }
    
    for(Subscription__c saasSubscription : [SELECT Id,Name,Status__c FROM Subscription__c WHERE Status__c='Processed' AND Name IN:subscriptionNumbers]){
      saasSubscription.Status__c = 'Closed';
      closedSubscriptions.add(saasSubscription);
    }
    
    if(!closedSubscriptions.isEmpty()){
      update closedSubscriptions;
    }
  }
}
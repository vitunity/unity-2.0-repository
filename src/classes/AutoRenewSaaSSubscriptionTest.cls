@isTest
private class AutoRenewSaaSSubscriptionTest {

  private static BigMachines__Quote__c subscriptionQuote;
    static testMethod void testRenewalSubscription() {
        setUpSubscriptionData();
        Subscription__c subscription = [SELECT Id FROM Subscription__c WHERE Quote__c =:subscriptionQuote.Id];
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SyncDataWithBMISessionIdMock(200,'OK',getTestResponse(),new Map<String,String>()));
      AutoRenewSaaSSubscription.renewSubscription(subscription.Id);
    Test.stopTest();
    }
    
    static testMethod void testRenewalError() {
        setUpSubscriptionData();
        Subscription__c subscription = [SELECT Id FROM Subscription__c WHERE Quote__c =:subscriptionQuote.Id];
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SyncDataWithBMISessionIdMock(200,'OK',getErrorResponse(),new Map<String,String>()));
      AutoRenewSaaSSubscription.renewSubscription(subscription.Id);
    Test.stopTest();
    }
    
    static testMethod void testRenewSubscriptionUsingTrigger(){
      setUpSubscriptionData();
      Subscription__c subscription = [SELECT Id FROM Subscription__c WHERE Quote__c =:subscriptionQuote.Id];
      
      Test.startTest();
        Test.setMock(HttpCalloutMock.class, new SyncDataWithBMISessionIdMock(200,'OK',getErrorResponse(),new Map<String,String>()));
        update new Subscription__c(Id = subscription.Id, Renew_Subscription__c = true);
        Test.stopTest();
    }
    private static String getTestResponse(){
      return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
            '<soapenv:Body>'+
                          '<bm:loginResponse xmlns:bm="urn:soap.bigmachines.com">'+
                             '<bm:status>'+
                                '<bm:success>true</bm:success>'+
                                '<bm:message>Successfully processed API for devvarian at Thu Apr 09 05:06:02 EDT 2015</bm:message>'+
                             '</bm:status>'+
                             '<bm:userInfo>'+
                                '<bm:sessionId>TESTSessionID</bm:sessionId>'+
                                '<bm:id>testid</bm:id>'+
                             '</bm:userInfo>'+
                          '</bm:loginResponse>'+
                       '</soapenv:Body>'+
          '</soapenv:Envelope>';
    }
    
    private static String getErrorResponse(){
      return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
           +'<soapenv:Body>'
              +'<soapenv:Fault>'
                 +'<faultcode>soapenv:Server</faultcode>'
                 +'<faultstring>COMMERCE-ERR:Could not validate the sessionId. Please provide a valid session Id or try to login again.</faultstring>'
                 +'<detail xmlns:bm="urn:soap.bigmachines.com">'
                    +'<bm:fault>'
                       +'<bm:exceptionCode>COMMERCE-ERR</bm:exceptionCode>'
                       +'<bm:exceptionMessage>Could not validate the sessionId. Please provide a valid session Id or try to login again.</bm:exceptionMessage>'
                    +'</bm:fault>'
                 +'</detail>'
              +'</soapenv:Fault>'
           +'</soapenv:Body>'
        +'</soapenv:Envelope>';
    }
    
    private static void setUpSubscriptionData(){
      
      insert new BigMachinesInfo__c(
        Name = 'Connection Details',
        Endpoint__c = 'https://varian.bigmachines.com',
        Username__c='TESTUSERNAME',
        Password__c='TESTPASSWORD',
        Organisation_Name__c='TESTORG',
        Error_Notification_Email__c = 'test@test.com'
      );
      
    Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'Sales1212';
        insert salesAccount;
        
        subscriptionQuote = TestUtils.getQuote();
        subscriptionQuote.Name = 'TEST Subscription QUOTE';
        subscriptionQuote.BigMachines__Account__c = salesAccount.Id;
        subscriptionQuote.National_Distributor__c = '121212';
        subscriptionQuote.Order_Type__c = 'sales';
        subscriptionQuote.Price_Group__c = 'Z2';
        
        Subscription__c subscription = new Subscription__c();
        subscription.Account__c = salesAccount.Id;
        subscription.Name = 'TESTSUBSCRIPTION';
        subscription.Ext_Quote_Number__c = subscriptionQuote.Name;
        subscription.Number_Of_Licences__c = 4;
        insert subscription;
  }
}
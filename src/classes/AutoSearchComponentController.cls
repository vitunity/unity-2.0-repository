/**
 * 	@author		:		Puneet Mishra
 * 	@description:		controller search the type characters and returned the suggested option on AutoLookupComponent
 */
public class AutoSearchComponentController {
   /* 
    @AuraEnabled
    public static List<String> getSuggestions(String sObjectType, String fieldName, String searchText, String fieldsToGet, Integer limitSize ) {
        Schema.DescribeFieldResult fieldResult = Competitor__c.Vendor__c.getDescribe();
        
        List<String> vendorsList = new List<String>();
        List<String> finalList = new List<String>();
        for(Schema.PicklistEntry entry : fieldResult.getPicklistValues()) {
            if(entry.isActive()) {
                vendorsList.add(entry.getLabel());
            }
        }
        for(String vendor : vendorsList) {
            if( (vendor.toLowerCase()).contains(searchText.toLowerCase()) ) {
                finalList.add(vendor);
            }
        }
        finalList.add('Varian');
        return finalList;
    }
    */
    @auraEnabled
    public static List<String> getSuggestValues(String sObjectType, String fieldName, String searchText, String fieldsToGet, Integer limitSize) {
        List<String> suggestions = new List<String>();
        List<String> finalList = new List<String>();
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new List<String>{sObjectType});
        List<Schema.SObjectField> val = new List<Schema.SObjectField>();
        for(schema.sObjectField fields : results[0].fields.getMap().values()) {
            Schema.DescribeFieldResult fieldDescObj = fields.getDescribe();
            system.debug('== fieldDescObj ==' + fieldDescObj.getName());
            if(fieldDescObj.getName() == fieldName) {
                for(Schema.PicklistEntry pickVal : fieldDescObj.getPicklistValues()) {
                    system.debug(' === pickVal === ' + pickVal);
                    if(pickVal.isActive())
                        suggestions.add(pickVal.getLabel());
                }
            }
        }
        system.debug('== suggestions ==' + suggestions);
        for(String vendor : suggestions) {
            system.debug('== vendor ==' + vendor);
            if( (vendor.toLowerCase()).contains(searchText.toLowerCase()) ) {
                finalList.add(vendor);
            }
        }
        
        return finalList;
    }
    
}
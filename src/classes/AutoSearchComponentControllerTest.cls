/**
 *   @author    :    Puneet Mishra
 *   @description:    test Class for AutoSearchComponentController
 */
@isTest
public class AutoSearchComponentControllerTest {
  
    public static testMethod void getSuggestValues_Test() {
        test.startTest();
        
        String sObjectType = 'Opportunity';
        String fieldName = 'Competitive_System_Lost__c';
        String searchText = 'Accu';
        String fieldsToGet = '';
        Integer limitSize = 0;
        
        List<String> suggestions = AutoSearchComponentController.getSuggestValues(sObjectType, fieldName, searchText, fieldsToGet, limitSize);
        
        test.stopTest();
    }
    
}
global class BatchAddUserToSelectedPG implements Database.Batchable<sObject>{
   global final String Query;
   public Set<Id> userIdsets;
   public set<String> groupSet;
   global BatchAddUserToSelectedPG(set<Id> userIdSet,Id accId){
        groupSet = new set<String>();
        String str = [select Id,Selected_Groups__c from Account where Id =: accId].Selected_Groups__c;
        if(str != null){    
            groupSet.addAll(str.split('; '));
        }
        userIdsets = userIdSet;
        Query = 'select Id,Name from Group where Name in : groupSet';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      system.debug('=========Query'+Database.getQueryLocator(query));
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC,List<Group> scope){
      try{
          List<GroupMember> grpMemInsert = new List<GroupMember>();
          for(Id urId : userIdsets){
              for(Group gp : scope){
                  GroupMember Obj= new Groupmember();
                    Obj.GroupID= gp.Id;
                    Obj.UserOrGroupId= urId;
                    grpMemInsert.add(Obj); 
              }
          }
          system.debug('--------------grpMemInsert-------------------' + grpMemInsert);
          insert grpMemInsert;
      }catch(Exception ex){
        system.debug('############# EXCEPTION '+ex);
      }
   }

   global void finish(Database.BatchableContext BC){

   }

}
global class BatchCoveredProductSync implements Database.Batchable<sObject>,Database.Stateful{   
    global string query; 
    string emailBody='';
    integer TotalFailedIP = 0;
    integer TotalSucessIP = 0;
    integer totalIP = 0;
    set<Id> setSuccessfullIP = new set<Id>();
    global BatchCoveredProductSync(string batchQuery){
        query = batchQuery;  
    }


    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query); 
    }


    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Contract_Products__c> scope){
        
        
        List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
        List<SVMXC__Installed_Product__c> lstScopeInstalledProduct;
    if(test.isRunningTest()){
      lstScopeInstalledProduct = [select Id,(select Id,SVMXC__Service_Contract__c,SVMXC__Start_Date__c,SVMXC__End_Date__c from SVMXC__R00N70000001hzdkEAA__r where status__c = true limit 1),SVMXC__Service_Contract_Start_Date__c,SVMXC__Service_Contract_End_Date__c,SVMXC__Service_Contract__c,SVMXC__Service_Contract_Line__c from SVMXC__Installed_Product__c where SVMXC__Service_Contract_Line__c IN: scope];
        }else{lstScopeInstalledProduct = [select Id,(select Id,SVMXC__Service_Contract__c,SVMXC__Start_Date__c,SVMXC__End_Date__c from SVMXC__R00N70000001hzdkEAA__r where status__c = true limit 1),SVMXC__Service_Contract_Start_Date__c,SVMXC__Service_Contract_End_Date__c,SVMXC__Service_Contract__c,SVMXC__Service_Contract_Line__c from SVMXC__Installed_Product__c where SVMXC__Service_Contract_Line__c IN: scope and Service_Contract_Line_IsActive__c = false]; }
    
        for(SVMXC__Installed_Product__c i : lstScopeInstalledProduct){
            if(i.SVMXC__R00N70000001hzdkEAA__r <> null && i.SVMXC__R00N70000001hzdkEAA__r.size()>0){
                SVMXC__Service_Contract_Products__c obj = i.SVMXC__R00N70000001hzdkEAA__r[0];
                i.SVMXC__Service_Contract__c = obj.SVMXC__Service_Contract__c;
                i.SVMXC__Service_Contract_Start_Date__c = obj.SVMXC__Start_Date__c;
                i.SVMXC__Service_Contract_End_Date__c = obj.SVMXC__End_Date__c;
                i.SVMXC__Service_Contract_Line__c = obj.Id;
            }else{
                i.SVMXC__Service_Contract__c = null;
                i.SVMXC__Service_Contract_Start_Date__c = i.SVMXC__Service_Contract_End_Date__c = null;
                i.SVMXC__Service_Contract_Line__c = null;
            }
            lstInstalledProduct.add(i);
        }
        totalIP += lstInstalledProduct.size();
        Database.SaveResult[] srList = Database.update(lstInstalledProduct, false); 
        BuildEmailBody(scope,srList);       
    }
    global void finish(Database.BatchableContext BC){
        
        string succIP;
        for(SVMXC__Installed_Product__c i: [select Id,PCSN__c from SVMXC__Installed_Product__c where id IN: setSuccessfullIP]){
            if(succIP == null)  succIP = '['+i.PCSN__c;
            else succIP += ', '+i.PCSN__c;          
        }
        if(succIP <> null) succIP += ']';
        emailBody = 'Total Instaled Product processed: ' + totalIP + '. Total Installed Product Update successfull: ' + TotalSucessIP+''+succIP+ '.\r\nTotal Installed Product failed: ' + TotalFailedIP + '.\r\n' + emailBody;  
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        string toAddress= Label.BatchOnCoveredProduct;       
        mail.setToAddresses(new String[] {toAddress});               
        
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject('Batch Installed Product Synch');
        
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody(emailBody);
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });            
        }
        catch(exception exp){
            system.debug(exp.getMessage());
        }        
        
    }
    private void BuildEmailBody(List<SVMXC__Service_Contract_Products__c> InstPro, List<Database.SaveResult> results){
        Integer errCounter = 0; 
        Integer successfullcount = 0;       
        
        for(integer index=0;index<results.size();index++){
            Database.SaveResult result=results[index]; 
            SVMXC__Service_Contract_Products__c Ins = InstPro[index];
            if (result.isSuccess()){
                successfullcount++;
                setSuccessfullIP.add(Ins.Id);
            }
            else if(!result.isSuccess()){ errCounter++;                
                emailBody += 'Installed Product ID: ' + Ins.id + '.';
                for (Database.Error err : result.getErrors()) emailBody += 'Error: '+ err.getStatusCode() + ' ' + err.getMessage() + '.';
                emailBody += '\r\n';
            }
        }
        TotalFailedIP += errCounter;  
        TotalSucessIP += successfullcount;          
    }
}
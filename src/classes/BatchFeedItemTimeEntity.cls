global class BatchFeedItemTimeEntity implements Database.Batchable<sObject>{
    global string query;
    global string errorMessage;
    //global string batchstatus;
    global BatchFeedItemTimeEntity (string  q){
        query  = q;
        errorMessage = '';
        //batchstatus = '';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope){
        try{
            //batchstatus += '# record found on Account : '+scope.size();
            List<SVMXC_Time_Entry__c> lstEntity = new  List<SVMXC_Time_Entry__c>();
            List<ERP_Partner_Association__c> lstERPAssociation = new  List<ERP_Partner_Association__c>();
            List<SVMXC__RMA_Shipment_Order__c> lstPartsOrder = new  List<SVMXC__RMA_Shipment_Order__c>();
            List<Contact_Role_Association__c> lstCRA = new  List<Contact_Role_Association__c>();
            List<SVMXC__Product_Stock__c> lstProductStock = new  List<SVMXC__Product_Stock__c>();
            List<SVMXC__Stock_History__c> lstStockHistory = new  List<SVMXC__Stock_History__c>();
            List<SVMXC__Service_Contract_Products__c> lstServiceContractProduct = new  List<SVMXC__Service_Contract_Products__c>();
            List<Contact> lstContact = new  List<Contact>();
            List<SVMXC__Installed_Product__c> lstIP = new  List<SVMXC__Installed_Product__c>();
            
            
            for(SVMXC_Time_Entry__c TE: [select Id from  SVMXC_Time_Entry__c where Associated_Account__c IN: scope]){
                lstEntity.add(TE);
            }
            for(ERP_Partner_Association__c  ERP: [select Id from  ERP_Partner_Association__c  where Customer_Account__c IN: scope]){
                lstERPAssociation.add(ERP);
            }
            for(SVMXC__RMA_Shipment_Order__c  PO: [select Id from  SVMXC__RMA_Shipment_Order__c where SVMXC__Company__c IN: scope]){
                lstPartsOrder.add(PO);
            }
            for(Contact_Role_Association__c  PO: [select Id from  Contact_Role_Association__c where Account__c IN: scope]){
                lstCRA.add(PO);
            }
            for(SVMXC__Product_Stock__c  PO: [select Id from  SVMXC__Product_Stock__c where SVMXC__Partner_Account__c IN: scope]){
                lstProductStock.add(PO);
            }
            for(SVMXC__Stock_History__c  PO: [select Id from  SVMXC__Stock_History__c where SVMXC__Partner_Account__c IN: scope]){
                lstStockHistory.add(PO);
            }
            for(SVMXC__Service_Contract_Products__c  PO: [select Id from  SVMXC__Service_Contract_Products__c where Site_Partner__c IN: scope]){
                lstServiceContractProduct.add(PO);
            }
            for(Contact  PO: [select Id from  Contact where AccountId IN: scope]){
                lstContact.add(PO);
            }
            for(SVMXC__Installed_Product__c  PO: [select Id from  SVMXC__Installed_Product__c where SVMXC__Company__c IN: scope]){
                lstIP.add(PO);
            }
            
        
            Database.DeleteResult[] drEntryList;
            Database.DeleteResult[] drERPAssociationList;
            Database.DeleteResult[] drPartsOrderList;
            Database.DeleteResult[] drCRAList;
            Database.DeleteResult[] drProductStockList;
            Database.DeleteResult[] drStockHistoryList;
            Database.DeleteResult[] drServiceContractProductList;
            Database.DeleteResult[] drContactList;
            Database.DeleteResult[] drIPList;
            
            List<FeedItem> lstFeedItem = [select Id from FeedItem where ParentId IN: lstEntity limit 10000];
            if(!lstFeedItem.isempty()){
                drEntryList = Database.delete(lstFeedItem, false);
            }
            
            List<FeedItem> lstAssociationFeedItem = [select Id from FeedItem where ParentId IN: lstERPAssociation limit 10000];
            if(!lstAssociationFeedItem.isempty()){
                drERPAssociationList = Database.delete(lstAssociationFeedItem, false);
            }
            
            List<FeedItem> lstPartsOrderFeedItem = [select Id from FeedItem where ParentId IN: lstPartsOrder limit 10000];
            if(!lstPartsOrderFeedItem.isempty()){
                drPartsOrderList = Database.delete(lstPartsOrderFeedItem, false);
            }
            
            List<FeedItem> lstCRAFeedItem = [select Id from FeedItem where ParentId IN: lstCRA limit 10000];
            if(!lstCRAFeedItem.isempty()){
                drCRAList = Database.delete(lstCRAFeedItem, false);
            }
            
            List<FeedItem> lstPStockFeedItem = [select Id from FeedItem where ParentId IN: lstProductStock limit 10000];
            if(!lstPStockFeedItem.isempty()){
                drProductStockList = Database.delete(lstPStockFeedItem, false);
            }
            
            List<FeedItem> lstSHistoryFeedItem = [select Id from FeedItem where ParentId IN: lstStockHistory limit 10000];
            if(!lstSHistoryFeedItem.isempty()){
                drStockHistoryList = Database.delete(lstSHistoryFeedItem, false);
            }
            
            List<FeedItem> lstServiceContractFeedItem = [select Id from FeedItem where ParentId IN: lstServiceContractProduct limit 10000];
            if(!lstServiceContractFeedItem.isempty()){
                drServiceContractProductList = Database.delete(lstServiceContractFeedItem, false);
            }
            
            List<FeedItem> lstContactFeedItem = [select Id from FeedItem where ParentId IN: lstContact limit 10000];
            if(!lstContactFeedItem.isempty()){
                drContactList = Database.delete(lstContactFeedItem, false);
            }
            
            List<FeedItem> lstIPFeedItem = [select Id from FeedItem where ParentId IN: lstIP limit 10000];
            if(!lstIPFeedItem.isempty()){
                drIPList = Database.delete(lstIPFeedItem, false);
            }
            
            /*    
            batchstatus += '\n-------------------------------------------\n';
            batchstatus += '\nEntry Feed Items found : '+lstFeedItem.size();
            batchstatus += '\nERP Association Feed Items found : '+lstAssociationFeedItem.size();
            batchstatus += '\nPart Order Feed Items found : '+lstPartsOrderFeedItem.size();
            batchstatus += '\n-------------------------------------------\n';
            
            
            
            Integer success=0;
            string DBError = '';            
            if(drEntryList<>null)for(Database.DeleteResult dr : drEntryList) {  
                if (dr.isSuccess())success++;
                else{for(Database.Error err : dr.getErrors()) { DBError+=err.getStatusCode() + ': ' + err.getMessage()+': on Field :'+ err.getFields(); }}
            }
            
            batchstatus += '\nEntry Feed Items Report : sucess('+success+')  '+',Issues : '+DBError;
            
            success=0;
            DBError = '';           
            if(drERPAssociationList<>null)for(Database.DeleteResult dr : drERPAssociationList) {    
                if (dr.isSuccess())success++;
                else{for(Database.Error err : dr.getErrors()) { DBError+=err.getStatusCode() + ': ' + err.getMessage()+': on Field :'+ err.getFields(); }}
            }
            
            batchstatus += '\nEntry Feed Items Report : sucess('+success+')  '+',Issues : '+DBError;
            
            success=0;
            DBError = '';           
            if(drPartsOrderList<>null)for(Database.DeleteResult dr : drPartsOrderList) {    
                if (dr.isSuccess())success++;
                else{for(Database.Error err : dr.getErrors()) { DBError+=err.getStatusCode() + ': ' + err.getMessage()+': on Field :'+ err.getFields(); }}
            }
            
            batchstatus += '\nEntry Feed Items Report : sucess('+success+')  '+',Issues : '+DBError;
            
           Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
           String[] toAddresses = new String[] {'rakesh.basani@varian.com'};
           mail.setToAddresses(toAddresses);
           mail.setSubject('Record Clean Up Status: ');
           mail.setPlainTextBody(batchstatus);
           Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
           */

        }catch(Exception e){   errorMessage += e.getLineNumber()+'::'+e.getMessage()+', ';         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();      String[] toAddresses = new String[] {Label.EmailFeedDelete};       mail.setToAddresses(toAddresses);         mail.setSubject('Record Clean Up Status: ');      mail.setPlainTextBody(errorMessage);       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
    }

    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {Label.EmailFeedDelete};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Record Clean Up Status: ' + a.Status);
       //mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.  Errors : '+errorMessage +'\nDetails:'+batchstatus);
       mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.  Errors : '+errorMessage);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
}
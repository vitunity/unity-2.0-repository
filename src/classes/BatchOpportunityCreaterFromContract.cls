/*************************************************************************\
    @ Author        : Mohit Sharma
    @ Date      : 20-Aug-2013
    @ Description   : Class handling inbound email and create article.
    @ Last Modified By  :   Ritika Kaushik
    @ Last Modified On  :    29-Aug-2013
    @ Last Modified Reason  :   Adding the code for saving the record
****************************************************************************/

global class BatchOpportunityCreaterFromContract implements Database.Batchable<sobject>
{
    global  date dt1=System.Today();
    global  date dt2 = System.Today()+90;  
    global final String Query;
    map<string,SVMXC__Service_Contract__c> mapServiceContract=new Map<string,SVMXC__Service_Contract__c>();
    list<SVMXC__SLA_Detail__c> lstSLADetail =new list<SVMXC__SLA_Detail__c>();     // List To Hold Available services
    Map<string,SVMXC__Service_Contract__c> mapContractSrvcPlanSrvcPriceBook;    //Map Containing Service contract service Plan Service Price Book
    list<SVMXC__Service_Contract__c > varServiceContract=new list<SVMXC__Service_Contract__c > (); // List To Hold Contract To Update
    list<Opportunity> lstOpportunity=new list<Opportunity>();
    List<SVMXC__Service_Contract_Products__c> lstCoveredProductTemp;  // Temp List Of Covered Product
    Map<string,string> mapOpportCotractAccount = new Map<string,string>();
    Map<string,list<SVMXC__Service_Contract_Products__c>> MapContractCoveredProd=new Map<string,list<SVMXC__Service_Contract_Products__c>>(); //Map To Hold Contract Id with Covered Product
    set<string> stContractIDs=new set<string>();
    set<string> strproduct=new set<string>(); // set To hold the product
    Set<string> stSLACoveredProduct;
    set<string> stallSlA=new set<string>();
    Map<string,set<string>> mapContractSLA=new Map<string,set<string>>();
    list<SVMXC__SLA_Detail__c> lstTempSlaDetail=new list<SVMXC__SLA_Detail__c>();
    Map<string,list<SVMXC__SLA_Detail__c>> mapSLAtermSLADetail=new Map<string,list<SVMXC__SLA_Detail__c>>();
    map<string,string> mapPriceBook=new map<string,string>();
    Set<string> stPBE=new set<string>();
    list<SVMXC__Service_Contract__c> lstServiceContract=new list<SVMXC__Service_Contract__c>();
    set<string> stproductTemp=new set<string>(); // This set Holding the Product Id which has not been added to Standard Price Book
    set<string> sttempchk=new set<string>();
    Map<string,pricebookentry> mapToInsertStPBE=new Map<string,pricebookentry>();
    Map<string,pricebookentry> mapToInsertStPBECustom=new Map<string,pricebookentry> ();
    Set<string> strCSSDistrict=new set<string>();
    list<SVMXC__Service_Group__c> lstTerrTeam=new list<SVMXC__Service_Group__c>();
    map<string,SVMXC__Service_Group__c> mapTerrTeam=new Map<string,SVMXC__Service_Group__c>();
    /*********************Constr**********************************/
    global BatchOpportunityCreaterFromContract (String q)
    {
        Query=q;
    }
    /*********************Initialize query**********************************/
    global Database.QueryLocator start(Database.BatchableContext BC) //Database.BatchableContext
    {
        return Database.getQueryLocator(query);
    }
    /*********************Initialize Code Execution**********************************/

    global void execute(Database.BatchableContext BC,List<Sobject> sc) //Database.BatchableContext BC
    {
        mapContractSrvcPlanSrvcPriceBook=new Map<string,SVMXC__Service_Contract__c> ();
        Map<Id, SVMXC__Service_Contract__c> expiredContracts = new Map<Id, SVMXC__Service_Contract__c> ();
        
        for(Sobject s : sc)
        {   
            SVMXC__Service_Contract_Products__c varSC = (SVMXC__Service_Contract_Products__c)s;
            stContractIDs.add(varSC.SVMXC__Service_Contract__c);
            //system.debug('++++++++++++++++SMC ID' + stContractIDs);
            //DE7264
            if (varSC != null && varSC.SVMXC__Service_Contract__c != null && varSC.SVMXC__Service_Contract__r.SVMXC__End_Date__c != null 
            		&& varSC.SVMXC__Service_Contract__r.SVMXC__End_Date__c < System.Today() && varSC.SVMXC__Service_Contract__r.SVMXC__Active__c ) {
            	expiredContracts.put(varSC.SVMXC__Service_Contract__c, new SVMXC__Service_Contract__c (Id = varSC.SVMXC__Service_Contract__c, SVMXC__Active__c = false));			
            }
        }
        ////DE7264 - Set contracts to inactive
        //System.debug(logginglevel.info, 'expiredContracts ========= '+ expiredContracts);
        if (expiredContracts.size() > 0) {
        	LIST<database.SaveResult> ctrResults = Database.update(expiredContracts.values());
        	//System.debug(logginglevel.info, 'expiredContracts results === '+ ctrResults);
        }
        
        list<pricebook2> lstPricebook2=new list<pricebook2>();
        lstServiceContract=[select id ,SVMXC__Company__c,SVMXC__Company__r.ownerId,SVMXC__Company__r.owner.Id,SVMXC__Company__r.CSS_District__c,SVMXC__End_Date__c,Renewal_Opportunity__c,Name, SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__c,SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name, SVMXC__Company__r.Country__c  from SVMXC__Service_Contract__c where Id In : stContractIDs ];
        for( SVMXC__Service_Contract__c varSerMainteContract:lstServiceContract )
        {
            if(varSerMainteContract.SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__c!=null)
            {//stPBE.add(varSerMainteContract.SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__c);
                stPBE.add(varSerMainteContract.SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name);
            }
            if(varSerMainteContract.SVMXC__Company__r.CSS_District__c!=null)
            {
                strCSSDistrict.add(varSerMainteContract.SVMXC__Company__r.CSS_District__c);
            }
            mapContractSrvcPlanSrvcPriceBook.put(varSerMainteContract.id,varSerMainteContract);   //Map Containing Service contract service Plan Service Price Book
            mapServiceContract.put(varSerMainteContract.id,varSerMainteContract);
            
        }
        
        //system.debug('Debug------123-----'+mapServiceContract);
        //system.debug('Debug-----456------'+mapContractSrvcPlanSrvcPriceBook);
        //system.debug('---------Mohit233'+stPBE);
        lstPricebook2=[select id , name from pricebook2 where name IN :stPBE];
        map<string,pricebook2> mapPriceBookNameId=new map<string,pricebook2> ();
        set<string> stPriceBookId=new set<string>();
        for(pricebook2 varPriceBook2: lstPricebook2)
        {
            stPriceBookId.add(varPriceBook2.id);
            mapPriceBookNameId.put(varPriceBook2.name,varPriceBook2);
        }
        //Start of Code added by Ritika
        //system.debug('strCSSDistrict>>>>'+strCSSDistrict  );
        lstTerrTeam = [SELECT Id,SVMXC__Group_Code__c,District_Manager__c FROM SVMXC__Service_Group__c where SVMXC__Group_Code__c IN :strCSSDistrict AND District_Manager__c!=null ];
        for(SVMXC__Service_Group__c varTerrTeam : lstTerrTeam )
        {      
           mapTerrTeam.put(varTerrTeam.SVMXC__Group_Code__c,varTerrTeam );       
        }
        //system.debug('mapTerrTeam>>>>>>'+mapTerrTeam);
        //End of Code added by Ritika
        //system.debug('---pricebookentrymap----------->'+mapPriceBookNameId);
        for(string varContractId : stContractIDs)
        {   
            stSLACoveredProduct=new set<string>();
            lstCoveredProductTemp=new List<SVMXC__Service_Contract_Products__c>();
            for(Sobject s : sc)
            {   
                SVMXC__Service_Contract_Products__c varSC = (SVMXC__Service_Contract_Products__c)s;   //Casting generic Object To Covered Product Object
                if(varContractId==varSC.SVMXC__Service_Contract__c)   
                {
                    lstCoveredProductTemp.add(varSC);   
                    stSLACoveredProduct.add(varSC.SVMXC__SLA_Terms__c);                                     // Adding CoveredProduct-SLA Ids        // Adding Covered Product To temp List 
                    if(varSC.SVMXC__SLA_Terms__c!=null)
                    {
                        stallSlA.add(varSC.SVMXC__SLA_Terms__c);
                    }
                    if(varSC.SVMXC__Product__c!=null)
                    {
                        strproduct.add(varSC.SVMXC__Product__c);
                    }       
               }  
            }      
            MapContractCoveredProd.put(varContractId,lstCoveredProductTemp);    //Adding Mapping of All Covered Product To Contract
            mapContractSLA.put(varContractId,stSLACoveredProduct); // Adding SLA To Map  
        }
        //System.debug('----map------->'+MapContractCoveredProd);
        //System.debug('----------->MapContractSLA'+mapContractSLA);
        list<product2> lstProduct =[select id,SVMXC__Product_Line__c, name from Product2 where SVMXC__Product_Line__c='Renewal'];// Froduct for Scenario 3
        lstSLADetail = [Select id , SVMXC__Available_Services__r.Product__c,SVMXC__SLA_Terms__c   from SVMXC__SLA_Detail__c where  SVMXC__SLA_Terms__c IN : stallSlA and Status__c != 'Cancelled']; // SLA Product // added  Status check fr de3515 By Bushra on 13-02-2015
        list<pricebookentry> lstPriceBookEntry=new list<pricebookentry>();
        for(product2 varstr: lstProduct) // Inserting Price Book entry for Renewal Product
        { 
        //for(pricebook2 varPricebook2: mapPriceBookNameId.values())   
        //{ 
        //Pricebookentry objPriceBookEntry=new pricebookentry(product2id=varstr.id,UnitPrice =0,Pricebook2Id=varPricebook2.id);
             strproduct.add(varstr.id);
             //lstPriceBookEntry.add(objPriceBookEntry);
        //}
        }
        //insert lstPriceBookEntry;
        //system.debug('---------lstSLADetail-->'+lstSLADetail);
        for(string varstr: stallSlA)
        {   
            lstTempSlaDetail=new list<SVMXC__SLA_Detail__c>();
            for(SVMXC__SLA_Detail__c varSlaDetail: lstSLADetail)
            {
                if(varSlaDetail.SVMXC__SLA_Terms__c==varstr)
                {
                    lstTempSlaDetail.add(varSlaDetail);
                }
                if(varSlaDetail.SVMXC__Available_Services__r.Product__c!=null)
                {
                    system.debug('-----------SLAproduct->'+strproduct+varSlaDetail.SVMXC__Available_Services__r.Product__c);
                    strproduct.add(varSlaDetail.SVMXC__Available_Services__r.Product__c);
                }
            }
            mapSLAtermSLADetail.put(varstr,lstTempSlaDetail); // map Sla-slaDetail
        }
        //system.debug('------SLA Term SLA Details-------->'+mapSLAtermSLADetail);
        /*********************Code For PriceBook-PriceBook-Entery-Map **********************************/
        //======================================Code added By mohit=================================
        list<pricebookentry> lstPBEStandard=[select id, name,Product2Id,pricebook2.name,Pricebook2id from pricebookentry where Pricebook2.isStandard=true AND product2id In: strproduct];
        list<pricebookentry> lstPBEStandardCustom=[select id, pricebook2.name,name,Product2Id,Pricebook2id from pricebookentry where Pricebook2.Name In : stPBE AND product2id In: strproduct];
        list<pricebookentry> lstPBEStandardToInsert=new list<pricebookentry>(); // list is used to create product with Standard pricebook
        list<pricebook2> varPBStandard=[select id , Name from pricebook2 where isStandard=true]; // geting Standard Price Book
        for(pricebookentry varTemp: lstPBEStandard)
        {
            sttempchk.add(varTemp.product2id);
        }

        if(lstPBEStandard.size()>0)
        {
            for(string var: strproduct)
            {

                for( pricebookentry varPbe :lstPBEStandard )
                {
                  if(!sttempchk.contains(var) )
                  {
                    string keyPbe=var+varPBStandard[0].id;
                    pricebookentry varStPriceookentry= new pricebookentry(product2id=var ,pricebook2Id=varPBStandard[0].id,unitprice=1,isactive=true);
                    mapToInsertStPBE.put(keyPbe,varStPriceookentry);
                  //lstPBEStandardToInsert.add(varStPriceookentry); // list to Create Standard priceBook Entry
                  }
                  else  {}
                }
            }
        }
        else
        {
            for(string var: strproduct)
            {
                string key2=var+varPBStandard[0].id;
                pricebookentry varStPriceookentry= new pricebookentry(product2id=var ,pricebook2Id=varPBStandard[0].id,unitprice=1,isactive=true);
                mapToInsertStPBE.put(key2,varStPriceookentry);
              //lstPBEStandardToInsert.add(varStPriceookentry);
            }
        }
        lstPBEStandardToInsert.addall(mapToInsertStPBE.values());
        //Custom pricebook Entry Check
        set<string> strPriceProduct=new set<string>();   //Set to Hold Product, PriceBook Id as key-Standard
        set<string> strPricePriceCustom=new set<string>();  ////Set to Hold Product ,PriceBook Id as key-Custom
        for(string varStr: strproduct)
        {
            for(string varstrPBE : stPBE)
            { 
                string strkey=varStr+'@'+varstrPBE;
                strPriceProduct.add(strkey);
            }
        }
        for(pricebookentry varPriceCustom : lstPBEStandardCustom)  // Iterating over Existing Price Book and Existing Product
        {
            string keyCustom=varPriceCustom.product2id+'@'+varPriceCustom.pricebook2.name;
            strPricePriceCustom.add(keyCustom);   // adding key To set to Filter Out Existing and non-Existing Price book Entry For Product
        }
        system.debug('-------Mohit----strPriceProduct>'+strPriceProduct);
        Map<string,pricebookentry> mapPricebookcustom=new Map<string,pricebookentry> ();  // Map To Holding key with pricebook entry
        for(string varstr: strPriceProduct)
        {
            if(lstPBEStandardCustom.size()>0)  // Checking the Pricebook entry for Custom price Book
            {
                for(pricebookentry varPBE: lstPBEStandardCustom )
                {
                    if(!strPricePriceCustom.contains(varstr))
                    {
                        list<string> lst=varstr.split('@');
                        if(mapPriceBookNameId.get(lst[1]).id!=null)
                        {
                            pricebookentry varCustmPBE= new pricebookentry(product2id=lst[0] ,pricebook2Id=mapPriceBookNameId.get(lst[1]).id,unitprice=0,isactive=true);
                            mapPricebookcustom.put(varstr,varCustmPBE);
                        }
                    }
                    else{}
                }
            }
            else
            {
                list<string> lst=varstr.split('@');
                if(mapPriceBookNameId.get(lst[1]).id!=null)
                {
                    pricebookentry varCustmPBE= new pricebookentry(product2id=lst[0] ,pricebook2Id=mapPriceBookNameId.get(lst[1]).id,unitprice=0,isactive=true);
                    mapPricebookcustom.put(varstr,varCustmPBE);
                }
            }
        }
        list<pricebookentry> lstPBECustomToInsert=new list<pricebookentry> ();
        lstPBECustomToInsert.addall(mapPricebookcustom.values());
        Insert lstPBEStandardToInsert; // Insert Product in to Standard PriceBookEntery
        insert lstPBECustomToInsert;   // Inserting Product in to Custom Price Book Entry
        //======================================Code added By mohit=================================
        string strkeyPriceBook='';
        //list<PricebookEntry> mapPBE=[select id, product2id,pricebook2id from PricebookEntry where product2id IN: strproduct AND pricebook2id IN : stPBE];
        list<PricebookEntry> mapPBE=[select id, product2id,pricebook2id from PricebookEntry where product2id IN: strproduct AND pricebook2id IN : stPriceBookId];
        //System.debug('------PriceBook'+mapPBE);
        for(PricebookEntry varPriceBookEntry:mapPBE )
        {
            strkeyPriceBook=string.valueof(varPriceBookEntry.product2id)+string.valueof(varPriceBookEntry.pricebook2id);
            mapPriceBook.put(strkeyPriceBook,varPriceBookEntry.id);
        }
        /*********************Code For Creating Opportunity**********************************/

        for(string varContractmap : MapContractCoveredProd.keyset())
        {
            if(MapContractCoveredProd.get(varContractmap).size()>0 && mapContractSrvcPlanSrvcPriceBook.get(varContractmap).Renewal_Opportunity__c ==null)
            {
                //string optname=varSC.SVMXC__Company__r.Name+'#'+varSC.name+'#'+varSC.Id;
                string optname='Renewal Opportunity'+'#'+varContractmap;
                mapOpportCotractAccount.put(optname,varContractmap);
               if(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c !=null) //SVMXC__Company__r.CSS_District__c !=null)
               {
                //code added by Ritika to set the Account's  Css District's District Manager as the owner of the renewal opportunity
                    if(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c != null && mapTerrTeam.containsKey(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c) && mapTerrTeam.get(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c).District_Manager__c != null)
                    {
                        //system.debug('district manager>>>>>'+mapTerrTeam.get(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c).District_Manager__c);
                        Opportunity opt=new Opportunity(name=optname,ownerid = mapTerrTeam.get(mapServiceContract.get(varContractmap).SVMXC__Company__r.CSS_District__c).District_Manager__c,accountid=mapServiceContract.get(varContractmap).SVMXC__Company__c,closedate=mapContractSrvcPlanSrvcPriceBook.get(varContractmap).SVMXC__End_Date__c,StageName='Prospecting',amount=0,Type='Service Contract - Renewal', Service_Maintenance_Contract__c = mapServiceContract.get(varContractmap).ID, Deliver_to_Country__c = mapServiceContract.get(varContractmap).SVMXC__Company__r.Country__c);
                        system.debug('opportunity>>>>>>'+opt.ownerid);
                        system.debug('Contract ID @@@@@@@@@@@@'+varContractmap);
                        system.debug('Covered Prod Contract ID ###########'+opt.Service_Maintenance_Contract__c);
                        lstOpportunity.add(opt);
                    }
                }
                else// if CSS District is null then set the Account Owner as the Renawal Opportunity Owner
                {
                   system.debug('opportunitytest>>>>>>'+mapServiceContract.get(varContractmap).SVMXC__Company__r.owner.id);
                    Opportunity opt=new Opportunity(name=optname,ownerid=mapServiceContract.get(varContractmap).SVMXC__Company__r.ownerid,accountid=mapServiceContract.get(varContractmap).SVMXC__Company__c,closedate=mapContractSrvcPlanSrvcPriceBook.get(varContractmap).SVMXC__End_Date__c,StageName='Prospecting',amount=0,Type='Service Contract - Renewal', Service_Maintenance_Contract__c = mapServiceContract.get(varContractmap).ID, Deliver_to_Country__c = mapServiceContract.get(varContractmap).SVMXC__Company__r.Country__c);
                    system.debug('opportunity>>>>>>'+opt.ownerid);
                    system.debug('Contract ID @@@@@@@@@@@@'+varContractmap);
                    system.debug('Covered Prod Contract ID ###########'+opt.Service_Maintenance_Contract__c);
                    lstOpportunity.add(opt);
                }
            }
        }        
        LIST<database.SaveResult> results = Database.Insert(lstOpportunity,false); 
        System.debug('SaveResult_Opportunity Insert'+results); 
        
        set<string> st_OpptyID = new set<string>();  // Set holding inserted  Oppty Id 
        
        for (Database.SaveResult sr : results) // Iterating Success Result
        {
            if (sr.isSuccess()) 
            {
                st_OpptyID.add(sr.getId());     // Adding Oppty Id 
            }
        }
       
       
       

        /*********************Code For Updating Contract,Opportunity Line item**********************************/
        
        list<OpportunityLineItem> listLineItem = new list<OpportunityLineItem>();                        // Need to Remove Unnecessary Code
        Map<string,OpportunityLineItem> mapOpportunityLineitem=new Map<string,OpportunityLineItem> ();   //Map to hold Opportunity Line Items
        
        if(st_OpptyID.size() > 0)
        {
            for(Opportunity varOpt: [select id, ownerid,name from Opportunity where id IN : st_OpptyID]) // Query to reterive inserted Oppty Data
            {   
                list<string> lsttempContract=new list<string>();
                lsttempContract=varOpt.name.split('#');
                system.debug('--Contract ID--->'+lsttempContract[1] );
                SVMXC__Service_Contract__c varSer=new SVMXC__Service_Contract__c(id=lsttempContract[1],Renewal_Opportunity__c=varOpt.id,Tech_OpportunityOwner__c=varOpt.OwnerId );
                varServiceContract.add(varSer);
                if(lsttempContract[1] !='' && lsttempContract[1] !=null)  // Checking whether Contract Id is null Or Not
                { 
                    if(MapContractCoveredProd.get( lsttempContract[1] ).size()>0)   // Checking List of Covered product with Contract
                    {
                        for (SVMXC__Service_Contract_Products__c varProd: MapContractCoveredProd.get( lsttempContract[1] ))     // Code for Creating Line Item- Scenario 1
                        {
                              //string strpbkey=string.valueof(varProd.SVMXC__Product__c)+mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]);
                            system.debug('-----msp--->'+mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name);
                            if(mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name!=null)
                            {
                                string strpbkey=string.valueof(varProd.SVMXC__Product__c)+mapPriceBookNameId.get(mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name).id;  
                                if( mapPriceBook.get(strpbkey)!=null)
                                {
                                    string key=varOpt.id+strpbkey; // temp key containing Opportunity id, Price book,Product id
                                    OpportunityLineItem varLinItem = new OpportunityLineItem (OpportunityID=varOpt.id,ServiceDate=varProd.SVMXC__End_Date__c,PricebookEntryId=mapPriceBook.get(strpbkey), quantity=1,TotalPrice=0);
                                    
                                    mapOpportunityLineitem.put(key,varLinItem);  // adding lineitem to map with key To insert
                                   // listLineItem.add(varLinItem);   
                                } 
                            }               
                        }
                        System.debug('------Contract Sla Inside opportubity--->'+mapContractSLA.get(lsttempContract[1]));
                        for(string varSTR :mapContractSLA.get(lsttempContract[1])) // Scenario 2
                        {
                            if(mapSLAtermSLADetail.get(varSTR)!=null)
                            {
                              for(SVMXC__SLA_Detail__c varSlaDetail : mapSLAtermSLADetail.get(varSTR))
                              { 
                                  if(varSlaDetail.SVMXC__Available_Services__r.Product__c!=null) 
                                  {
                                    string strpbkey1=string.valueof(varSlaDetail.SVMXC__Available_Services__r.Product__c)+mapPriceBookNameId.get(mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name).id;
                                    if(mapPriceBook.get(strpbkey1) !=null)
                                    {
                                        string key=varOpt.id+strpbkey1; // key with opportunity Product Pricebook
                                       
                                        OpportunityLineItem varLinItem = new OpportunityLineItem (OpportunityID=varOpt.id,ServiceDate=mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__End_Date__c,PricebookEntryId=mapPriceBook.get(strpbkey1), quantity=1,TotalPrice=0);
                                       
                                         mapOpportunityLineitem.put(key,varLinItem);
                                    }
                                  }
                              }
                            }
                        }
                        for(product2 varProduct: lstProduct)   // Scenario 3
                        { 
                            if(varProduct.id!=null)
                            {
                                if(mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name!=null)
                                {     
                                    string strpbkey1=string.valueof(varProduct.id)+mapPriceBookNameId.get(mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__Service_Plan__r.SVMXC__Service_Pricebook__r.name).id;  // key Containing Product Id,Pricebookid
                                   if(mapPriceBook.get(strpbkey1)!=null)
                                   {      
                                        string key=varOpt.id+strpbkey1;  
                                        OpportunityLineItem varLinItem = new OpportunityLineItem (OpportunityID=varOpt.id,Description=lsttempContract[1],ServiceDate=mapContractSrvcPlanSrvcPriceBook.get(lsttempContract[1]).SVMXC__End_Date__c+1,PricebookEntryId=mapPriceBook.get(strpbkey1), quantity=1,TotalPrice=0);
                                        mapOpportunityLineitem.put(key,varLinItem);  // Final Map to  Hold the all line items 
                                    //listLineItem.add(varLinItem); 
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        listLineItem.addall(mapOpportunityLineitem.values());
        if(varServiceContract.size() > 0)
        {
            update varServiceContract;
        }
        system.debug('------------>'+listLineItem);
        if(listLineItem.size()>0)
        insert listLineItem;//insert opprtunity line items
    }

    global void finish(Database.BatchableContext BC)
    {
        //Send an email to the User after your batch completes
        /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'sforce2009@gmail.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Batch Job is done');
        mail.setPlainTextBody('The batch Apex job processed ');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
}
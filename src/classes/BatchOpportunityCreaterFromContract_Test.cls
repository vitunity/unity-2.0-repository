@isTest(seeAllData = true)
public class BatchOpportunityCreaterFromContract_Test
{
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    public static testMethod void SC_BatchSendNotificationOnContractExpireTest()
    {
        Test.startTest();
            // Schedule the test job
            String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new SC_BatchSendNotificationOnContractExpire());

            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

            // Verify the expressions are the same
            System.assertEquals(CRON_EXP, ct.CronExpression);

            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);

            // Verify the next time the job will run
            System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));

        Test.stopTest();
    }
    public static testMethod void testRenewalOpportunity()
    {
        //Record Type Ids
        String CaseRecType = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 
        String recTypeHD = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 

        //Create Account
        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = 'AA787799';
        testAcc.CSS_District__c = 'Test';
        insert testAcc;
         // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //Create Location
        SVMXC__Site__c loc = new SVMXC__Site__c();
        loc.SVMXC__Account__c = testAcc.Id;
        loc.Name = 'Test Location';
        insert loc;

        //Create Product
        Product2 objProd2=new Product2();
        objProd2.Name = 'technical Test Product';
        objProd2.Material_Group__c = 'H:TRNING';
        objProd2.Budget_Hrs__c = 10.0;
        objProd2.Credit_s_Required__c = 10;
        objProd2.UOM__c = 'EA';
        insert objProd2;

        //Insert Case
        case cs = new case();
        cs.subject = 'test';
        cs.RecordTypeId = recTypeHD;
        cs.AccountId = testAcc.id;
        cs.SVMXC__Site__c = loc.id;
        cs.ContactId = con.id;
        insert cs;

        //Create Installed Product
        SVMXC__Installed_Product__c testInstallProduct = SR_testdata.createInstalProduct();
        testInstallProduct.ERP_Functional_Location__c = 'USA';
        testInstallProduct.SVMXC__Serial_Lot_Number__c ='test';
        insert testInstallProduct;

        //Insert Sla Term  
        list<SVMXC__Service_Level__c> lstSLaTerm = new list<SVMXC__Service_Level__c>();
        SVMXC__Service_Level__c objserviceLevel = new SVMXC__Service_Level__c();
        objserviceLevel.Name= 'TestSlaTerm001';
        lstSLaTerm.add(objserviceLevel);

        SVMXC__Service_Level__c objserviceLevel1 = new SVMXC__Service_Level__c();
        objserviceLevel1.Name = 'TestSlaTerm002';
        lstSLaTerm.add(objserviceLevel1);
        Insert lstSLaTerm;

        //Create Available service
        SVMXC__Service__c objAvailableSErvice = new SVMXC__Service__c();
        objAvailableSErvice.Name = 'TestOnsiteService';
        objAvailableSErvice.Product__c = objProd2.id;
        insert objAvailableSErvice;

        // Territory Team
        Territory_Team__c objTrr=new Territory_Team__c();
        objTrr.CSS_District__c = 'Test';  
        objTrr.CSS_District_Manager__c = UserInfo.getUserId(); 
        insert objTrr;

        //Price Book Entry
        Pricebook2 pb = new pricebook2(name = 'price1');
        insert pb;
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard = true limit 1];
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id =objProd2.id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;

        // Service Plan
        SVMXC__Service_Pricebook__c objSP = new SVMXC__Service_Pricebook__c();
        objSP.SVMXC__Active__c = true;
        objSP.Name ='price1';
        insert objSP;      

        SVMXC__Service_Plan__c objPlan= new SVMXC__Service_Plan__c();
        objPlan.SVMXC__Service_Pricebook__c = objSP.id;
        insert objPlan;

        //Create Service Contract  
        SVMXC__Service_Contract__c testServiceContract = Sr_testdata.createServiceContract();
        testServiceContract.SVMXC__Service_Level__c = lstSLaTerm[0].id;
        testServiceContract.SVMXC__Company__c = testAcc.Id;
        testServiceContract.Location__c =loc.id;
        testServiceContract.SVMXC__Service_Plan__c  = objPlan.id;
        insert testServiceContract;

        // Create SLA Detail
        //SLA Detail 0  
        list<SVMXC__SLA_Detail__c> lstSLADetail=new list<SVMXC__SLA_Detail__c>();
        SVMXC__SLA_Detail__c objSlaDEtail = new SVMXC__SLA_Detail__c();
        objSlaDEtail.SVMXC__Available_Services__c = objAvailableSErvice.id;
        objSlaDEtail.Service_Maintenance_Contract__c= testServiceContract.id;
        objSlaDEtail.SVMXC__SLA_Terms__c = lstSLaTerm[0].id;
        lstSLADetail.add(objSlaDEtail);

        //SLA Detail 1
        SVMXC__SLA_Detail__c objSlaDEtail1 = new SVMXC__SLA_Detail__c();
        objSlaDEtail1.SVMXC__Available_Services__c = objAvailableSErvice.id;
        objSlaDEtail1.Service_Maintenance_Contract__c= testServiceContract.id;
        objSlaDEtail1.SVMXC__SLA_Terms__c = lstSLaTerm[1].id;
        lstSLADetail.add(objSlaDEtail1);
        insert lstSLADetail;

        //Covered Product
        SVMXC__Service_Contract_Products__c testContractProduct = new SVMXC__Service_Contract_Products__c();
        testContractProduct.SVMXC__Installed_Product__c = testInstallProduct.id;
        testContractProduct.SVMXC__Service_Contract__c = testServiceContract.id;
        testContractProduct.SVMXC__SLA_Terms__c =  lstSLaTerm[1].id;
        testContractProduct.Cancelation_Reason__c = null;
        testContractProduct.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(10);
        testContractProduct.Serial_Number_PCSN__c = 'test';
        insert testContractProduct;
        testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(7);
        update testContractProduct;
 
        Test.StartTest();
            date dt1= system.today();
            date dt2= system.today()-90;
            string str='select id ,SVMXC__SLA_Terms__c,SVMXC__Product__c,SVMXC__End_Date__c,SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVMXC__End_Date__c, SVMXC__Service_Contract__r.SVMXC__Active__c from   SVMXC__Service_Contract_Products__c where   id=\''+testContractProduct.id + '\'';
            BatchOpportunityCreaterFromContract b =new BatchOpportunityCreaterFromContract(str); 
            database.executebatch(b);
            // Datetime dt = Datetime.now().addMinutes(1);
            //String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
            // String shjobId = System.schedule('Sample_Heading', CRON_EXP, new SC_BatchSendNotificationOnContractExpire() ); 
        Test.StopTest();
    }
    public static testMethod void testTrigerPopulateEntitlonOpp(){
        ERP_Timezone__c etz = new ERP_Timezone__c();
        etz.Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)';
        etz.name='Aussa';
        insert etz;
        
         // insertAccount
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
         // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //insert Opportunity
        Opportunity opp = new  Opportunity(Name = 'testOPP', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '30%',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        insert opp;
        
        // insert parent IP var loc
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H12345',SVMXC__Serial_Lot_Number__c ='H12345',SVMXC__Status__c ='Installed',ERP_Reference__c = 'H12345');
        insert objIP;
        
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345');
        insert testServiceContractProduct;
        
        opp.CloseDate  = System.today().addDays(20);
        opp.Expected_Close_Date__c = System.today().addDays(20);
        update opp;
    }
}
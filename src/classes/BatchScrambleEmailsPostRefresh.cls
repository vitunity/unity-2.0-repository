/***************************************************************************
Author: S. Hiller
Created Date: 28-September-2017 (This version)
Project/Story/Inc/Task : N/A
Description: Post refresh sandbox email scrambling and partner user deactivation

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
28-Sept-2017 - S. Hiller - Create this class combining the object specific scrambling classes into one
*************************************************************************************/

global class BatchScrambleEmailsPostRefresh implements database.batchable<sobject>, database.stateful {

    integer currentObjectToScramble;
    static final integer FINAL_OBJECT_TO_SCRAMBLE_KEY = 6;

    // Define SQL queries

    map <integer, string> objectSQL = new map <integer, string> {
        // Scramble Case Owner Manager Email
        1 => 'select case_owner_manager_email__c, LastModifiedDate from case where case_owner_manager_email__c != null and LastModifiedDate < 2018-03-19T00:00:00Z',
        // Scramble Service Team Emails
        2 => 'select svmxc__email__c from svmxc__service_group__c where svmxc__email__c != null',
        // Scramble Technician Emails
        3 => 'select svmxc__email__c, sms_email__c, dm_email__c, foa_email__c, rd_email__c, sc_email__c, technician_email__c from svmxc__service_group_members__c where svmxc__email__c != null or sms_email__c != null or dm_email__c != null or foa_email__c != null or rd_email__c != null or sc_email__c != null or technician_email__c != null',
        // Scramble Email Distribution List Email
        4 => 'select admin_dlist__c, brachy_dlist__c, business_dlist__c, primary_dlist__c, secondary_dlist__c, support_dlist__c, fulfillment_dlist__c from email_dlists__c where admin_dlist__c != null or brachy_dlist__c != null or business_dlist__c != null or primary_dlist__c != null or secondary_dlist__c != null or support_dlist__c != null or fulfillment_dlist__c!= null',
        // Scramble Contact Emails
        5 => 'select activation_url__c, recovery_question__c, recovery_answer__c, passwordresetdate__c, passwordreset__c, oktalogin__c, oktaid__c, email, LastModifiedDate from contact where (activation_url__c != null or recovery_question__c != null or recovery_answer__c != null or passwordresetdate__c != null or passwordreset__c != false or oktalogin__c != null or oktaid__c != null or email != null) and LastModifiedDate < 2018-03-19T00:00:00Z',
        // Deactivate Customer Users
        6 => 'select isactive from user where contactid != null and isactive = true'
    };

    public batchScrambleEmailsPostRefresh(integer objectToScramble) {

        // Constructor accepting key of query for object next to be processed

        currentObjectToScramble = objectToScramble;

    }

    public database.queryLocator start(database.batchableContext bc) {

        // Execute SQL query

        return database.getQueryLocator(objectSQL.get(currentObjectToScramble));

    }

    public void execute(database.batchableContext bc, list<sobject> objectToScramble) {

        // Performs record processing in small batches avoiding governor limits

        if (currentObjectToScramble == 1) {
        
            // Cases Unscrambled Email processing
            
            list<case> casesUE = (list<case>) objectToScramble;

            for(case cUE : casesUE) {
                if(!cUE.case_owner_manager_email__c.contains('@example.com'))
                    cUE.case_owner_manager_email__c = cUE.case_owner_manager_email__c.replace('@','=') + '@example.com';

            }
            
            update casesUE;
        }
        
        else if (currentObjectToScramble == 2) {
        
            // Service Team Unscrambled Email processing
            
            list<svmxc__service_group__c> serviceTeamUE = (list<svmxc__service_group__c>) objectToScramble;

            for(svmxc__service_group__c sTUE : serviceTeamUE) {
                if(!sTUE.svmxc__email__c.contains('@example.com'))
                    sTUE.svmxc__email__c = sTUE.svmxc__email__c.replace('@','=') + '@example.com';
            }

            update serviceTeamUE;
        }
        
        else if (currentObjectToScramble == 3) {
        
            // Technician Unscrambled Email processing
            
            list<svmxc__service_group_members__c> technicianUE = (list<svmxc__service_group_members__c >) objectToScramble;

            for(svmxc__service_group_members__c tUE : technicianUE) {

                if(!string.isblank(tUE.svmxc__email__c) && !tUE.svmxc__email__c.contains('@example.com'))
                    tUE.svmxc__email__c = tUE.svmxc__email__c.replace('@','=') + '@example.com';

                if(!string.isblank(tUE.sms_email__c) && !tUE.sms_email__c.contains('@example.com'))
                    tUE.sms_email__c = tUE.sms_email__c.replace('@','=') + '@example.com';  

                if(!string.isblank(tUE.dm_email__c) && !tUE.dm_email__c.contains('@example.com'))
                    tUE.dm_email__c = tUE.dm_email__c.replace('@','=') + '@example.com'; 

                if(!string.isblank(tUE.foa_email__c) && !tUE.foa_email__c.contains('@example.com'))
                    tUE.foa_email__c = tUE.foa_email__c.replace('@','=') + '@example.com'; 

                if(!string.isblank(tUE.rd_email__c) && !tUE.rd_email__c.contains('@example.com'))
                    tUE.rd_email__c = tUE.rd_email__c.replace('@','=') + '@example.com';           

                if(!string.isblank(tUE.sc_email__c) && !tUE.sc_email__c.contains('@example.com'))
                    tUE.sc_email__c = tUE.sc_email__c.replace('@','=') + '@example.com';           

                if(!string.isblank(tUE.technician_email__c ) && !tUE.technician_email__c.contains('@example.com'))
                    tUE.technician_email__c = tUE.technician_email__c.replace('@','=') + '@example.com';   
            }

            update technicianUE;      

        }
        
        else if (currentObjectToScramble == 4) {
        
            // Email Distribution List Unscrambled Email processing
            
            list<email_dlists__c> emailDListsUE = (list<email_dlists__c>) objectToScramble;  

            for(email_dlists__c eDLUE : emailDListsUE) {

                if(!string.isblank(eDLUE.admin_dlist__c) && !eDLUE.admin_dlist__c.contains('@example.com'))
                    eDLUE.admin_dlist__c = eDLUE.admin_dlist__c.replace('@','=') + '@example.com';

                if(!string.isblank(eDLUE.brachy_dlist__c) && !eDLUE.brachy_dlist__c.contains('@example.com'))
                    eDLUE.brachy_dlist__c = eDLUE.brachy_dlist__c.replace('@','=') + '@example.com';  

                if(!string.isblank(eDLUE.business_dlist__c) && !eDLUE.business_dlist__c.contains('@example.com'))
                    eDLUE.business_dlist__c = eDLUE.business_dlist__c.replace('@','=') + '@example.com'; 

                if(!string.isblank(eDLUE.primary_dlist__c) && !eDLUE.primary_dlist__c.contains('@example.com'))
                    eDLUE.primary_dlist__c = eDLUE.primary_dlist__c.replace('@','=') + '@example.com'; 

                if(!string.isblank(eDLUE.secondary_dlist__c) && !eDLUE.secondary_dlist__c.contains('@example.com'))
                    eDLUE.secondary_dlist__c = eDLUE.secondary_dlist__c.replace('@','=') + '@example.com';           

                if(!string.isblank(eDLUE.support_dlist__c) && !eDLUE.support_dlist__c.contains('@example.com'))
                    eDLUE.support_dlist__c = eDLUE.support_dlist__c.replace('@','=') + '@example.com';           

                if(!string.isblank(eDLUE.fulfillment_dlist__c) && !eDLUE.fulfillment_dlist__c.contains('@example.com'))
                    eDLUE.fulfillment_dlist__c = eDLUE.fulfillment_dlist__c.replace('@','=') + '@example.com';   

            }

            update emailDListsUE;      

        }
        
        else if (currentObjectToScramble == 5) {
        
            // Contact Unscrambled Email processing
            
            list<contact> contactUF = (list<contact>) objectToScramble;

            for(contact cUF : contactuf) {

                if(cUF.email != null && !cUF.email.contains('@example.com'))
                    cUF.email = cUF.email.replace('@','=') + '@example.com';

                cUF.activation_url__c = null;
                cUF.recovery_question__c = null;
                cUF.recovery_answer__c = null;
                cUF.passwordresetdate__c = null;
                cUF.passwordreset__c = false;
                cUF.oktalogin__c = null;
                cUF.oktaid__c = null;

            }

            update contactUF;      

        }

        else if (currentObjectToScramble == 6) {
        
            // Active Partner User deactivation
            
            list<user> activePartnerUser = (list<user>) objectToScramble;

            for(user aPU : activePartnerUser) 
                aPU.isactive = false;

            update activePartnerUser;      

        }

    }

    public void finish(database.batchableContext bc) {

    // All batches for an object processed, call for next object or terminate

        currentObjectToScramble++;
        if(currentObjectToScramble <= FINAL_OBJECT_TO_SCRAMBLE_KEY)
            database.executebatch(new BatchScrambleEmailsPostRefresh(currentObjectToScramble));

    }   

}
/***************************************************************************
Author: Nilesh Gorle
Created Date: 25-Sept-2017
Project/Story/Inc/Task : STSK0012842 - URGENT: Deactivate MyVarian users who have not logged in since 7/1/2017
Description: 
This batch updates IsActive and IsPortalEnabled field of User have to be unchecked if last login <= 7/1/2017

Change Log:
09-Jan-2018 - Divya Hargunani - STSK0013654: STRY0048047 : MyVarian: Schedule MyVarian contact deactivation - Added logic to update the 
                                                                contact and user and to deactivate user from Okta
*************************************************************************************/
global class Batch_DeactivateUser implements Database.Batchable<SObject>, Database.AllowsCallouts{

    global string query;
    // have to filter and get all records before selected date. format below is YYYY, MM, DD
    global Date Records_Before_Date;
    global Profile profileRec;
    global Boolean updateContact;
    /************************************************************************************
    Description: 
    This is constructor for Batch_DeactivateUser to declare query string and
    fetch all User record and map it.
    *************************************************************************************/
    
    global Batch_DeactivateUser(Boolean updateContact) {
        this.updateContact = updateContact;
        profileRec = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' LIMIT 1];
        Records_Before_Date = Date.newInstance(2017, 7, 2);        
    }
    
  

    global Database.QueryLocator start(Database.BatchableContext BC){
        Date Records_Before_DateTemp = Records_Before_Date;
        Id profileId = profileRec.Id;
        if(Test.isRunningTest()) {  
            query = 'select Id, Name, ContactId, LastLoginDate, IsActive,IsPortalEnabled from User where Id = \'00544000007rOl1\'';
        } else {
            query = 'select Id, ContactId, Name, LastLoginDate, IsActive,IsPortalEnabled from User Where LastLoginDate <=: Records_Before_DateTemp And ProfileId = \'' +  string.valueOf(profileId)+'\' and isActive = true';
        }
        return Database.getQueryLocator(query);
    }

    /***************************************************************************
    Description: 
    This method uncheck IsActive and IsPortalEnabled field of account if last login < 7/1/2017
    ****************************************************************************/
    
    //Divya Hargunani - STSK0013654: MyVarian: Schedule MyVarian contact deactivation
    global void execute(Database.BatchableContext BC, List<User> usrList) {
        List<User> newUserlist = new List<User>();
        List<Contact> contactList = new List<Contact>();
        set<Id> contactIds = new set<Id>();
        for (User u: usrList) {
            u.IsActive = false;
            u.IsPortalEnabled = false;
            newUserlist.add(u);
            if(u.ContactId != null){
                contactIds.add(u.ContactId);
                contactList.add(new Contact(Id = u.ContactId, MyVarian_Member__c = false));
            }
            
        }
       
        if (!contactIds.isEmpty() && updateContact == true ) {
           update contactList;
        }
        
        if (!contactIds.isEmpty() && updateContact == false ) {
           userdeactivationmethod(contactIds);
        }
        
        if(!Test.isRunningTest()){
        if (!newUserlist.isEmpty() && updateContact == false) {
           update newUserlist;
        }
        }
    }
    
    //Divya Hargunani - STSK0013654: MyVarian: Schedule MyVarian contact deactivation
    // method for deactivating user form okta.
    @TestVisible
    private static void  userdeactivationmethod(Set<Id> userdeactivate){
        for (Contact con: [Select OktaId__c from contact where id=:userdeactivate]){
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            String strToken = label.oktatoken;
            String authorizationHeader = 'SSWS ' + strToken;
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type','application/json');
            req.setHeader('Accept','application/json');
            String EndPoint = Label.oktachangepasswrdendpoint + con.OktaId__c +'/lifecycle/deactivate';
            req.setendpoint(EndPoint);
            req.setMethod('POST');
            if(!Test.isRunningTest()){
                res = http.send(req);
                system.debug('response from okta = '+res.getBody());
            }
            else{
                res = fakeresponse.fakeresponsemethod('deactivate');
            }
           
            // http request for Deleting a user in a group
            for(CpOktaGroupIds__c cpgroup : CpOktaGroupIds__c.getall().values()){
                system.debug('remove from all groups in case of deactivation--');
                HttpRequest reqgroup = new HttpRequest();
                HttpResponse resgroup = new HttpResponse();
                Http httpgroup = new Http();
                reqgroup.setHeader('Authorization', authorizationHeader);
                reqgroup.setHeader('Content-Type','application/json');
                reqgroup.setHeader('Accept','application/json');
                String endpointgroup = Label.oktaendpointforaddinggroup + cpgroup.Id__c + '/users/' + con.OktaId__c;//Label.oktasfdcgroupid+'/users/' + oktaid;
                reqgroup.setEndPoint(endpointgroup);
                reqgroup.setMethod('DELETE');
                system.debug('aebug&&&&&&& ; ' +reqgroup.getBody());
                if(!Test.isRunningTest())
                {
                    resgroup = httpgroup.send(reqgroup);
                }
                else
                {
                    resgroup = fakeresponse.fakeresponsemethod('groupaddition');
                }
                system.debug('aebug&&&&&&& ; ' +resgroup.getBody());
            }           
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
}
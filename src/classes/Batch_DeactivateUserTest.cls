/***************************************************************************
Author: Nilesh Gorle
Created Date: 25-Sept-2017
Project/Story/Inc/Task : STSK0012842 - URGENT: Deactivate MyVarian users who have not logged in since 7/1/2017
Description: 
This is test class for Batch_DeactivateUser APex class

Change Log:
09-Jan-2018 - Divya Hargunani - STSK0013654: STRY0048047 : MyVarian: Schedule MyVarian contact deactivation - Increased test coverage
*************************************************************************************/

@isTest
public class Batch_DeactivateUserTest {
    
    public static Account acc;
    public static Contact con;
    public static User usr;
    public static Profile portalProf;
    
    public static testMethod void testUpdateContactRecord() {
        portalProf = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' LIMIT 1];
        
        acc = new Account(Name = 'TestAprRel', OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = 'test@varian.com';
        con.AccountId = acc.Id;
        insert con;
        
        usr = new User();
        usr.FirstName = 'Bruce';
        usr.LastName = 'Wayne';
        usr.Alias = 'Batman';
        usr.email = 'bruce.wayne1c@zynga.com';
        usr.languagelocalekey = 'en_US';
        usr.localesidkey = 'en_US';
        usr.emailencodingkey = 'UTF-8';
        usr.timezonesidkey = 'America/Chicago';
        usr.username = 'bruce.wayne1c@wayneenterprises.com';
        usr.ProfileId = portalProf.Id;
        usr.isActive = true;
        usr.ContactId = con.Id;
        insert usr;
        List<User> usrList = new List<User>{usr};

        test.startTest();
        Batch_DeactivateUser cb = New Batch_DeactivateUser(true);
        cb.query = 'select Id, Name, LastLoginDate, ContactId from User Where ContactId!=null limit 100';
        Database.QueryLocator ql = cb.start(null);
        cb.execute(null, usrList);
        cb.Finish(null);
        test.stopTest();
    }
	
	public static testMethod void testUpdateUserRecord() {
        portalProf = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' LIMIT 1];
        
        acc = new Account(Name = 'TestAprRel', OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = 'test@varian.com';
        con.AccountId = acc.Id;
        insert con;
        
        usr = new User();
        usr.FirstName = 'Bruce';
        usr.LastName = 'Wayne';
        usr.Alias = 'Batman';
        usr.email = 'bruce.wayne1c@zynga.com';
        usr.languagelocalekey = 'en_US';
        usr.localesidkey = 'en_US';
        usr.emailencodingkey = 'UTF-8';
        usr.timezonesidkey = 'America/Chicago';
        usr.username = 'bruce.wayne1c@wayneenterprises.com';
        usr.ProfileId = portalProf.Id;
        usr.isActive = true;
        usr.ContactId = con.Id;
        insert usr;
        List<User> usrList = new List<User>{usr};
        
        CpOktaGroupIds__c okgrp = new CpOktaGroupIds__c();
        okgrp.Name = 'test okta group';
        okgrp.AppId__c = 'test';
        okgrp.Id__c = 'test Id';
        insert okgrp;
        
        test.startTest();
        Batch_DeactivateUser cb = New Batch_DeactivateUser(false);
        cb.query = 'select Id, Name, LastLoginDate, ContactId from User Where ContactId!=null limit 100';
        Database.QueryLocator ql = cb.start(null);
        cb.execute(null, usrList);
        cb.Finish(null);
        test.stopTest();
    }
}
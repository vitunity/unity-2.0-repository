global class Batch_ERPNWAAlert implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext BC){
        string query = 'select Id,ERP_NWA__r.Name,ERP_WBS__r.Name,New_ERP_WBS__r.Name,ERP_NWA__c,ERP_WBS__c,New_ERP_WBS__c,Modify_Date_Time__c from NWA_Trace__c where Modify_Date_Time__c = LAST_N_DAYS :1 order by createdDate';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<NWA_Trace__c> scope){
        map<Id,NWA_Trace__c> mapNWATrace = new map<Id,NWA_Trace__c>();
        map<Id,List<NWA_Trace__c>> mapNWA = new map<Id,List<NWA_Trace__c>>();
        
        for(NWA_Trace__c n: scope){
            List<NWA_Trace__c> Temp = new List<NWA_Trace__c>();
            Temp.add(n);
            if(mapNWA.containsKey(n.ERP_NWA__c)){
                Temp.addAll(mapNWA.get(n.ERP_NWA__c));
            }
            mapNWA.put(n.ERP_NWA__c,Temp);
            mapNWATrace.put(n.ERP_NWA__c,n);
        }         
        try{
            
            //String[] toAddresses = new String[] {'SAP.PSADMIN@varian.com'};
            String[] toAddresses = new String[] {System.Label.ERPNWAGroup};// custom label
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            //mail.setOrgWideEmailAddressId('0D2E0000000XZCm');
            mail.setOrgWideEmailAddressId(System.Label.ERPNWAAlertFromAddress);
            mail.setSubject('NWA Notification');
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setHTMLBody(getEmailBody(mapNWATrace,mapNWA));
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception e){
            system.debug('BError:'+e.getLineNumber()+'--'+e);
        }

    }
    global string getEmailBody(map<Id,NWA_Trace__c> mapNWATrace,map<Id,List<NWA_Trace__c>> mapNWA){
        String EmailBody = '<html><body>';
        EmailBody += '<p>';
        
            EmailBody += '<p>Hello,</p>';
      
            EmailBody += '<p>';
                EmailBody += 'This email is to notify you about the ERP NWA that are associated with multiple ERP WBS:';
            EmailBody += '</p>';
            
            for(Id NWAId : mapNWA.keySet()){
                NWA_Trace__c TraceObj = mapNWATrace.get(NWAId);
                EmailBody += '<p>';
                    
                    EmailBody +='<table style="width:100%;border: 1px solid black;border-collapse: collapse;">';
                        EmailBody += '<tr style="background-color:#F1F1F1">';
                            EmailBody += '<td  colspan="5" style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">NWA : '+TraceObj.ERP_NWA__r.Name+'</td>';
                        EmailBody += '</tr>';
                        EmailBody += '<tr style="background-color:#DDDDDD">';
                          //  EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">NO.</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">NWA</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">WBS</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Latest WBS</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Update Time</th>';
                        EmailBody += '</tr>';
                        integer sno=1;
                        for(NWA_Trace__c t: mapNWA.get(NWAId)){
                            EmailBody += '<tr>';
                              //  EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+(sno++)+'.</td> ';
                                EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+t.ERP_NWA__r.Name+'</td>';
                                EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+t.ERP_WBS__r.Name+'</td>';
                                EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+t.New_ERP_WBS__r.Name+'</td>';
                                EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+t.Modify_Date_Time__c+'</td>';
                            EmailBody += '</tr>';
                        }
                    EmailBody += '</table>';
                EmailBody += '</p>';
            }
            EmailBody += '<p>';
                EmailBody += 'Thanks,';
            EmailBody += '<p>';
                EmailBody += 'Unity';
            EmailBody += '</p>';
            
        EmailBody += '<p/></body></html>';
        return EmailBody;
    }

    global void finish(Database.BatchableContext BC){
    }
}
/*************************************************************************\
Change Log:
7-Nov-2017 - Divya Hargunani - INC4701142\STSK0013311: My Notifications Email not being sent to MyVarian Users - Optimized the code
****************************************************************************/

global class Batch_EmailNotification implements  Database.Batchable<sObject>,Database.Stateful,Schedulable{
            
            global void execute(SchedulableContext sc)
            {
                Batch_EmailNotification b = new Batch_EmailNotification();
                if(Test.isRunningTest()){
                    database.executebatch(b,10);
                } else{ 
                    database.executebatch(b,1);
                }
            }  
            
            global Database.QueryLocator start(Database.BatchableContext BC) 
            { 
                String query='';
                query = 'select id , name ,contactid,Accountid, Subscribed_Products__c,Email from User where  Subscribed_Products__c = True and isActive = true ';  
                if(Test.isRunningTest()){
                    query=query+' LIMIT 10';
                }
                return Database.getQueryLocator(query) ;  
            }
            
            global void execute(Database.BatchableContext BC, List<sObject> scope)
            {
               List<Sobject> externaluser = new List<Sobject>();
               List<Sobject> internaluser = new List<Sobject>();
               Set<String> SetAccounts=new Set<String>();
               Set<String> AllPrdtSet = new Set<String>(); 
               List<string > LstProductgroups=new List<string>();
               Map<id,set<string>> ObjMapAccountHavingProducts=new Map<id,set<string>>();
               Map<String,Set<id>> ObjMapProductHavingContDocids=new Map<String,Set<id>>();
               Map<Id,Set<Id>> accountContactsInfoMap = new Map<Id,Set<Id>>();
               Set<String> documentTypes = new Set<String>{'Safety Notifications', 'CTBs', 'Release Notes'};
               List<ContentVersion > Contverids= new List<ContentVersion> ();
               List<ContentVersion > LstCntver=new List<ContentVersion> ();
               Set<id> FinalContdocids=new  Set<id>();
               List<ContentWorkspaceDoc> LstVar=new List<ContentWorkspaceDoc>();
               Map<string,integer> TempDocsSize=new  Map<string,integer> ();
               List<Contact> ListContacts=new List<Contact>();
               Set<id> SetofContids=new Set<id>();
               List<ContentVersion> documnetsInternal= new List<ContentVersion>();
               Set<string> productGroupSetInternal=new Set<string>();
               Set<string> stContentDocIdInternal=new Set<string> ();
               Schema.DescribeFieldResult fieldResult = Product2.Product_Group__c.getDescribe();
               List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

                for(Sobject s : scope) {
                    User usr = (user)s;
                    if(usr.contactid != null) {
                    SetAccounts.add(usr.Accountid);
                    }
                }
                if(!SetAccounts.isEmpty()){
                    for(SVMXC__Installed_Product__c ip :[Select s.SVMXC__Product__r.Product_Group__c,
                                                s.SVMXC__Product__c, s.Id, s.SVMXC__Company__c
                                                From SVMXC__Installed_Product__c s
                                                where s.SVMXC__Company__c in: SetAccounts and
                                                SVMXC__Product__r.Product_Group__c != null]) {
                    set<String> objPrdtSet = new set<String>();
                    if(ObjMapAccountHavingProducts.get(ip.SVMXC__Company__c) != null){
                        objPrdtSet= ObjMapAccountHavingProducts.get(ip.SVMXC__Company__c);
                    }
                    if(ip.SVMXC__Product__r.Product_Group__c!=null) {
                        objPrdtSet.addAll(ip.SVMXC__Product__r.Product_Group__c.split(';'));
                    }
                    AllPrdtSet.addall(objPrdtSet);
                    ObjMapAccountHavingProducts.put( ip.SVMXC__Company__c,objPrdtSet); 
                }
                if(!Test.isrunningtest()){
                    LstVar = [Select c.SystemModstamp, c.IsOwner, c.IsDeleted, c.Id, c.CreatedDate, 
                                c.ContentWorkspace.Name, c.ContentWorkspaceId, 
                                c.ContentDocumentId From ContentWorkspaceDoc c 
                                where ContentWorkspace.Name IN :  AllPrdtSet and 
                                (ContentDocument.ContentModifiedDate =Last_Week OR ContentDocument.CreatedDate =Last_Week)
                                limit 10000] ;
                }else{
                    LstVar=[Select c.SystemModstamp, c.IsOwner, c.IsDeleted, c.Id, c.CreatedDate,
                                c.ContentWorkspace.Name, c.ContentWorkspaceId, c.ContentDocumentId From ContentWorkspaceDoc c Limit 1]  ;                                          
                }
                for(ContentWorkspaceDoc var:LstVar) {
                    set<id> setcontids=new set<id> ();
                    if(ObjMapProductHavingContDocids.get(var.ContentWorkspace.Name)!=null) {
                        setcontids=ObjMapProductHavingContDocids.get(var.ContentWorkspace.Name);
                    }
                    setcontids.add(var.ContentDocumentId );
                    FinalContdocids.addall(setcontids);
                    ObjMapProductHavingContDocids.put(var.ContentWorkspace.Name,setcontids);  
                }
                if(!test.isrunningtest()){
                    LstCntver = [select id,Title, Document_Type__c, Date__c,ContentVersion.ContentDocumentId 
                        from ContentVersion where recordtype.name != 'Marketing Kit' 
                        and Document_Language__c = 'English' and islatest=true and Document_Type__c in : documentTypes 
                        and (ContentDocumentId in :FinalContdocids) and  (ContentModifiedDate = Last_Week 
                        OR CreatedDate = Last_Week)  Order by  Date__c Desc limit 10000];
                }else{
                    LstCntver=[select id,Title, Document_Type__c, Date__c,ContentVersion.ContentDocumentId 
                    from ContentVersion Order by  Date__c Desc LIMIT 1];
                }
            
                for(ContentVersion  varcontver:LstCntver) {
                    Contverids.add(varcontver);
                }
            
                map<string,contentversion> MapofDocidsandcontentversions=new map<string, contentversion>();
                for(contentversion varcv: Contverids) {
                    if(!MapofDocidsandcontentversions.containskey(varcv.ContentDocumentId)) {
                        MapofDocidsandcontentversions.put(varcv.ContentDocumentId,varcv);
                    }
                }

                map<string, Set<Id>> mapProd_CV=new map<string, Set<Id>>(); 
                for(String strprod:ObjMapProductHavingContDocids.keyset()) {
                    set<id> FinalConverids=new set<id>();
                    for(String str:ObjMapProductHavingContDocids.get(strprod)) {
                        if(MapofDocidsandcontentversions.get(str) != null)
                            FinalConverids.add(MapofDocidsandcontentversions.get(str).id);
                    }
                    mapProd_CV.put(strprod,FinalConverids);
                }
            
                For(id objacc: ObjMapAccountHavingProducts.keyset()) {
                    Set<String> setProdut=new  Set<String>();
                    Set<id> setcontids=new Set<id>();
                    setProdut=ObjMapAccountHavingProducts.get(objacc);
                    for(String StrProduts :setProdut) {
                        if(mapProd_CV.get(StrProduts)!=null) {
                            setcontids.addall(mapProd_CV.get(StrProduts));
                        }
                    }
                    TempDocsSize.put(objacc,setcontids.size());
                }
            }
            
            if(SetAccounts.isEmpty() || Test.isRunningTest()){
                for( Schema.PicklistEntry f : ple) {
                    productGroupSetInternal.add(f.getValue());
                }
            
                List<ContentWorkspaceDoc> Lstcntwrkspac=new List<ContentWorkspaceDoc>();

            if(!test.isrunningtest())
                    Lstcntwrkspac=[Select c.SystemModstamp, c.IsOwner, c.IsDeleted, c.Id, c.CreatedDate, c.ContentWorkspace.Name, 
                                    c.ContentWorkspaceId, c.ContentDocumentId From ContentWorkspaceDoc c 
                                    where ContentWorkspace.Name IN :  productGroupSetInternal and (ContentDocument.ContentModifiedDate = Last_Week 
                                    OR ContentDocument.CreatedDate = Last_Week) limit 10000];
            else
                    Lstcntwrkspac=[Select c.SystemModstamp, c.IsOwner, c.IsDeleted, c.Id, c.CreatedDate, c.ContentWorkspace.Name, 
                                    c.ContentWorkspaceId, c.ContentDocumentId From ContentWorkspaceDoc c limit 1];
            
            for(ContentWorkspaceDoc var:Lstcntwrkspac) {
                stContentDocIdInternal.add(var.ContentDocumentId);
            }

            if(stContentDocIdInternal.size()>0) {
                if(!test.isrunningtest())
                    documnetsInternal= [select id,Title, Document_Type__c, Date__c
                                        from ContentVersion where
                                        recordtype.name != 'Marketing Kit'
                                        and Document_Language__c = 'English'
                                        and islatest=true
                                        and Document_Type__c in : documentTypes
                                        and (ContentDocumentId in : stContentDocIdInternal)
                                        and  (ContentModifiedDate =Last_Week OR CreatedDate = Last_Week)
                                        Order by  Date__c Desc limit 10000];// limit 10];
                else
                    documnetsInternal= [select id,Title, Document_Type__c, Date__c
                                        from ContentVersion limit 1];
            }
        }
        
        for(Sobject s : scope) {
            User usr = (user)s;
            if(usr.contactid == null){
                if(documnetsInternal.size()>0) {
                    internaluser.add(s);
                }
            }
            else {
                if(TempDocsSize.get(usr.accountid)>0) {
                    SetofContids.add(usr.Contactid);
                }
            }
        }
        
        if(!internaluser.isEmpty()){
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage> ();
            EmailTemplate et = [Select id from EmailTemplate where name = 'Cp:WeekelyEmailDigest'];
            for (Sobject ss : internaluser) {
                User u = (User)ss;
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(u.Id);
                String[] toAddresses = new String[]{u.email};
                mail.setToAddresses(toAddresses);
                mail.setTemplateId(et.Id);
                mail.setSaveAsActivity(false);
                mailList.add(mail);
                }
            Messaging.sendEmail(mailList);  
        }
        if(!SetofContids.isEmpty()){
            ListContacts= [Select id,accountid,Send_WeeklyDigest__c from Contact where id in:SetofContids];
            List<Contact> FinalContacLst = new  List<Contact>();
            for (Contact sss : ListContacts) {
                contact ct=new contact(id=sss.id,Send_WeeklyDigest__c=true);
                FinalContacLst.add(ct);
            }
            Database.update(FinalContacLst,false);
        }
        
        
    }

    global void finish(Database.BatchableContext BC) {}
}
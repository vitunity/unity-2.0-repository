/*************************************************************************\
Change Log:
7-Nov-2017 - Divya Hargunani - INC4701142\STSK0013311: My Notifications Email not being sent to MyVarian Users - Increased the code coverage
****************************************************************************/

@isTest(SeeAllData=false)
private class Batch_EmailNotificationtest{
    
    public static testMethod void InternalUserTest(){
        Account acc = new Account(Name = 'TestAprRel', OMNI_Address1__c = 'address test1', 
        OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'TestAprRel1FN';
        con.LastName = 'TestAprRel1';
        con.Email = 'Test@1234APR.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Kar';
        insert con; 
        
        Test.StartTest();
        
        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        Id libId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test IP', SVMXC__Product__c = prod.Id, SVMXC__Company__c = acc.Id);
        insert ip;
        
        RecordType ContentRT = [select Id FROM RecordType WHERE DeveloperName='Product_Document' AND SobjectType = 'ContentVersion' LIMIT 1];
        ContentVersion testContentIn = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
        insert testContentIn;
        
        ContentVersion testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn.Id and IsLatest=True];
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
        ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc.ContentDocumentId= testContents.ContentDocumentId; 
        insert newWorkspaceDoc;
        
        testContentIn.Document_Language__c = 'English';
        testContentIn.Document_Type__c = 'Safety Notifications' ;
        update testContentIn;
        
        ContentVersion testContentIn1 = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
        insert testContentIn1;
        
        ContentVersion testContents1 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn1.Id and IsLatest=True];
        ContentWorkspace testWorkspace1 = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
        ContentWorkspaceDoc newWorkspaceDoc1 =new ContentWorkspaceDoc(); 
        newWorkspaceDoc1.ContentWorkspaceId = testWorkspace1.Id; 
        newWorkspaceDoc1.ContentDocumentId= testContents1.ContentDocumentId; 
        insert newWorkspaceDoc1;
        
        testContentIn1.Document_Language__c = 'English';
        testContentIn1.Document_Type__c = 'CTBs' ;
        update testContentIn1;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.RunAs(thisUser) {
            Profile p = [select id from profile where Name =: Label.VMS_Customer_Portal_User limit 1];
            
            User u = new User();
            u.LastName = con.LastName;
            u.Email = con.Email;
            u.UserName = con.Email;
            u.contactId = con.Id; 
            u.alias = 'TstApr';
            u.LocaleSidKey = 'en_US';
            u.LanguageLocaleKey = 'en_US';
            u.TimeZoneSidKey = 'America/Los_Angeles';
            u.emailencodingkey='UTF-8';
            u.profileId = p.Id;
            u.Subscribed_Products__c=true;
            insert u;
            
            set<Id> userIdSet = new set<Id>();
            userIdSet.add(u.Id);
            
            Batch_EmailNotification ben = new Batch_EmailNotification();
            database.executeBatch(ben,10); 
            SchedulableContext c = null;
            ben.execute(c);
            Test.StopTest();
        }
    }
    
    public static testMethod void PortalUserTest(){
    
        Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
        Account acc = new Account(Name = 'TestAprRel', OMNI_Address1__c = 'address test1', 
        OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'TestAprRel1FN';
        con.LastName = 'TestAprRel1';
        con.Email = 'Test@1234APR.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Kar';
        insert con; 
        
        Test.StartTest();
        
        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        Id libId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test IP', SVMXC__Product__c = prod.Id, SVMXC__Company__c = acc.Id);
        insert ip;
        
        RecordType ContentRT = [select Id FROM RecordType WHERE DeveloperName='Product_Document' AND SobjectType = 'ContentVersion' LIMIT 1];
        ContentVersion testContentIn = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
        insert testContentIn;
        
        ContentVersion testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn.Id and IsLatest=True];
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
        ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc.ContentDocumentId= testContents.ContentDocumentId; 
        insert newWorkspaceDoc;
        
        testContentIn.Document_Language__c = 'English';
        testContentIn.Document_Type__c = 'Safety Notifications' ;
        update testContentIn;
        ContentVersion testContentIn1 = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
        insert testContentIn1;
        
        ContentVersion testContents1 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn1.Id and IsLatest=True];
        ContentWorkspace testWorkspace1 = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
        ContentWorkspaceDoc newWorkspaceDoc1 =new ContentWorkspaceDoc(); 
        newWorkspaceDoc1.ContentWorkspaceId = testWorkspace1.Id; 
        newWorkspaceDoc1.ContentDocumentId= testContents1.ContentDocumentId; 
        insert newWorkspaceDoc1;
        
        testContentIn1.Document_Language__c = 'English';
        testContentIn1.Document_Type__c = 'CTBs' ;
        update testContentIn1;
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.RunAs(thisUser) {
            Profile p = [select id,name from profile where UserType in :customerUserTypes limit 1];
            
            User u = new User();
            u.LastName = 'test';
            u.Email = 'test12345@test.com';
            u.UserName = 'test12345@test.com';
            u.contactId = con.Id;
            u.alias = 'TstApr';
            u.LocaleSidKey = 'en_US';
            u.LanguageLocaleKey = 'en_US';
            u.TimeZoneSidKey = 'America/Los_Angeles';
            u.emailencodingkey='UTF-8';
            u.profileId = p.Id;
            u.Subscribed_Products__c=true;
            insert u;
            
            set<Id> userIdSet = new set<Id>();
            userIdSet.add(u.Id);
            
            Batch_EmailNotification ben = new Batch_EmailNotification();
            database.executeBatch(ben,10); 
            Test.StopTest();
        }
    }
}
/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 28-Feb-2018
    @ Description   : STSK0013576 - Update ECF Count/Submitted fields on existing case records.
    Date/Modified By Name/Task or Story or Inc # /Description of Change
****************************************************************************/
//select Id,Case_ECF_Count__c,Case_ECF_Submitted__c from Case
global class Batch_EscalatedComplaintForms implements  Database.Batchable<sObject>, Database.Stateful{

    global string query;
    global set<string> successfullCases;
    global set<string> typeofErrors;
    global Batch_EscalatedComplaintForms(string q){
        query = q;
        successfullCases = new set<string>();
        typeofErrors = new set<string>();
    }
    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
       return Database.getQueryLocator(query) ;   
    }
    global void execute(Database.BatchableContext BC, List<Case> lstTotalCase)
    {
        map<Id,List<l2848__c>> mapCaseL2848 = new map<Id,List<l2848__c>>();
        
        for(l2848__c l: [select Id,is_Submitted__c, Case__c from l2848__c where case__c IN: lstTotalCase]){   
            List<l2848__c> temp = new List<l2848__c>();
            temp.add(l);
            if(mapCaseL2848.containsKey(l.case__c)){temp.addAll(mapCaseL2848.get(l.case__c));}
            mapCaseL2848.put(l.Case__c,temp);
        }

        system.debug('before Updatable Total Cases:'+mapCaseL2848.size());
        map<Id,Case> mapCase = new map<Id,Case>();
        List<Case> ECFCase1 = new List<Case>();

        integer count = 0;
        for(Case c: lstTotalCase){
            if(mapCaseL2848.containsKey(c.id)){     
                
                integer Case_ECF_Count = 0;
                integer Case_Submitted_Count = 0;
                for(l2848__c l : mapCaseL2848.get(c.id)){
                    Case_ECF_Count++;
                    if(l.is_Submitted__c <> null && l.is_Submitted__c.equalsIgnoreCase('submitted for review'))Case_Submitted_Count++;  
                }
                c.Case_ECF_Count__c = Case_ECF_Count;
                c.Case_ECF_Submitted__c = Case_Submitted_Count;
                ECFCase1.add(c);
            }   
        }
        system.debug('Ready for Updatable total Cases 1: '+ECFCase1.size());
        
        for (Database.SaveResult sr : Database.update(ECFCase1,false)) {
            if (sr.isSuccess()) {
                successfullCases.add(string.valueOf(sr.getId()));
            }
            else {      
                for(Database.Error err : sr.getErrors()) {
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    TypeOfErrors.add(string.valueOf(err.getMessage()));
                }
            }
        }
      
    }
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        FROM AsyncApexJob WHERE Id =
        :BC.getJobId()];
        
        string ss;
        for(string s: successfullCases){
            if(ss==null)ss = s;
            else ss+= ', '+s;
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rakeshbasani.sfdc@gmail.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Escalated Complaint Forms Batch Process');
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + '<br/><br/>Type of errors: '+TypeOfErrors +'<br/><br/>Successfully updated Cases: '+ss);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
    }
}
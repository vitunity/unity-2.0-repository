global class Batch_IdeaEmailAlert implements  Database.Batchable<sObject>{
    global string query;
    global Batch_IdeaEmailAlert(string q){
        query = q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Idea> scope)
    {       
        Map<String,Set<id>> mapProductIdeas =new Map<String,Set<id>>();
        List<ProdCountEmailMapping__c> EmailMapping = [select Id,Name,ProductGroup__c,Email__c from ProdCountEmailMapping__c where Name = 'ProductImprovement'  and ProductGroup__c <> null];
        
        map<string,set<string>> mapSupportGroups = new map<string,set<string>>();
        map<string,string> mapEmails = new map<string,string>();

        for(ProdCountEmailMapping__c p : EmailMapping ){            
            if(p.Email__c <> null){
                set<string> setTemp = new set<string>();
                setTemp.add(p.ProductGroup__c);
                if(mapSupportGroups.containsKey(p.Email__c)){
                    setTemp.addAll(mapSupportGroups.get(p.Email__c));
                }
                mapSupportGroups.put(p.Email__c,setTemp);
            }
        }
        
        for(Idea s : scope)
        {           
            if(s.Categories <> null){
                   for(string c : s.Categories.split(';')){
                    set<Id> setIdeas = new set<Id>();
                    setIdeas.add(s.Id);
                    if(mapProductIdeas.containsKey(c)){
                        setIdeas.addAll(mapProductIdeas.get(c));
                    }
                    mapProductIdeas.put(c,setIdeas);
                }
            }
        }
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'donotreply@varian.com'];
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage> ();
        
        for(string s : mapSupportGroups.keyset()){
            string strProdList='<br/><ul>';
            boolean isIdeaPresent = false;
            for(string sProd : mapSupportGroups.get(s)){
                if(mapProductIdeas.containskey(sProd)){
                    strProdList += '<li>'+sProd+'</li>';
                    isIdeaPresent = true;
                }
            }
            strProdList  += '</ul>';
            if(isIdeaPresent)mapEmails.put(s,strProdList);           
        }
        
    
        for(string emailAdd : mapEmails.keyset()){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {emailAdd};  // {'Dolores.Zeledon@varian.com' };     
                if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                }
                mail.setToAddresses(toAddresses);         
                mail.setSubject('New Product Ideas are awaiting your review');      
                mail.setHtmlBody(label.Batch_IdeaEmailAlert +mapEmails.get(emailAdd));  
                mailList.add(mail);
            }
        
        
        if(mailList.size() > 0){
            Messaging.sendEmail(mailList); 
        }   
    }  
    global void finish(Database.BatchableContext BC)
    {
        /*AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email  FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rakesh.basani@varian.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Idea Email Alert Notification Report ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
}
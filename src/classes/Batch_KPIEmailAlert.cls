global class Batch_KPIEmailAlert implements  Database.Batchable<sObject>{

    string EmailSubject;
    List<string> EmailAddress;
    Datetime Last_Batch_Run;
    string errorMessage;

    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
        String query = 'select Id,MemberId,CreatedDate,CollaborationGroupId from CollaborationGroupMember  where  CollaborationGroup.Name = \'K-Chat\''; 
        
         
        map<string,KPI_Email_Notification__c> MapKPI = KPI_Email_Notification__c.getAll();
        if(MapKPI.containskey('KPI')){
            KPI_Email_Notification__c KPI = MapKPI.get('KPI');
            Last_Batch_Run = KPI.Last_Batch_Run__c;
            if(KPI.Last_Batch_Run__c <> null){
                query += ' and createdDate >: Last_Batch_Run';
            }
        }
        if(Test.isRunningTest())query += ' limit 1';
        return Database.getQueryLocator(query) ;  
    }
    
    global void execute(Database.BatchableContext BC, List<CollaborationGroupMember> scope)
    {
        system.debug('scope:'+scope);
        set<Id> setUser = new set<Id>();
        for(CollaborationGroupMember cm: scope){
            if(cm.MemberId <> null){
                setUser.add(cm.MemberId);
            }
        }
        
        map<Id,User> mapUser = new map<Id,User>([select Id,Name,Email,isActive from User where Id IN: setUser]);
        system.debug('mapUser :'+mapUser );
        string startDate = 'beginning';
        if(Last_Batch_Run <> null)startDate = Last_Batch_Run+'';
        string csvstr = 'UserID, Name, Emai, Is Active, Added Date \n';
        String EmailBody = '<html><body>';
        EmailBody += '<p>';
        
            EmailBody += '<p>Hello,</p>';
      
            EmailBody += '<p>';
                EmailBody += 'This is list of user which is added from '+startDate +' to till now';
            EmailBody += '</p>';
            
            
                EmailBody += '<p>';
                    
                    EmailBody +='<table style="width:100%;border: 1px solid black;border-collapse: collapse;">';
                       EmailBody += '<tr style="background-color:#DDDDDD">';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">UserID</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Name</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Email</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Is Active</th>';
                            EmailBody += '<th style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">Added Date</th>';
                        EmailBody += '</tr>';
                       
                        for(CollaborationGroupMember cm: scope){
                            if(cm.MemberId <> null && mapUser.containsKey(cm.MemberId)){
                                User u = mapUser.get(cm.MemberId);
                                csvstr += u.Id+','+u.Name+','+u.Email+','+u.isActive +','+cm.createdDate+'\n';  
                                
                                    EmailBody += '<tr>';
                                        EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+u.Id+'</td>';
                                        EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+u.Name+'</td>';
                                        EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+u.Email+'</td>';
                                        EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+u.isActive+'</td>';
                                        EmailBody += '<td style="border: 1px solid black;border-collapse: collapse;padding: 5px;text-align: left;">'+cm.createdDate+'</td>';
                                    EmailBody += '</tr>';
                            }
                        }
                    EmailBody += '</table>';
                EmailBody += '</p>';
            
           
            EmailBody += 'Thanks,';
            
        EmailBody += '</p></body></html>';
        
        
        system.debug('Email:'+EmailAddress+'-'+EmailSubject+'-'+EmailBody);
        map<string,KPI_Email_Notification__c> MapKPI = KPI_Email_Notification__c.getAll();
        if(MapKPI.containskey('KPI')){
            KPI_Email_Notification__c KPI = MapKPI.get('KPI');
            EmailSubject = KPI.Subject__c;
            EmailAddress = KPI.To_Addresses__c.split(',');            
        }
        if(EmailAddress <> null){
            
            Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment(); 
            csvAttcmnt.setFileName ('K-chat User.csv');
            csvAttcmnt.setBody (Blob.valueOf (csvstr));
            
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();     
            mail.setToAddresses(EmailAddress);
            mail.setSubject(EmailSubject);
            mail.setHTMLBody(EmailBody);
            mail.setFileAttachments (new Messaging.EmailFileAttachment []{csvAttcmnt});
            try{
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                system.debug('sent');
            }catch(Exception e){errorMessage = e.getMessage();}
        }

    }
    
    global void finish(Database.BatchableContext BC)
    {
        if(errorMessage == null){
            KPI_Email_Notification__c KPI = [select Id,Name,Last_Batch_Run__c from KPI_Email_Notification__c where Name = 'KPI'];
            KPI.Last_Batch_Run__c = Datetime.Now();
            update KPI ;
        }
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        FROM AsyncApexJob WHERE Id =
        :BC.getJobId()];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {system.Label.KPIEmail};
        mail.setToAddresses(toAddresses);
        mail.setSubject('KPI Group Member Status: ');
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.  Errors : '+errorMessage );
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
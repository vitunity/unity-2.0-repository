Global Class Batch_LMSAutodeactivation implements Database.Batchable<sobject>{
   
  
    global Database.Querylocator start (Database.BatchableContext BC) {
        return Database.getQueryLocator([Select id, Name,Contact.Name,IsActive,Profile.Name,LMS_Status__c,LMS_Activated_Date__c,LMS_Deactivation_Date__c,LMS_Last_Access_Date__c,LMS_Last_Course_End_date__c,LMS_Registration_Date__c from user where Profile.name = 'VMS MyVarian - Customer User' and Contact.id != null and LMS_Status__c = 'Active' and IsActive = true]);
    }
      
     
    global void execute (Database.BatchableContext BC, List<user> scope){
        list<user> userToUpdate = new list<user>();
        Map<string,MyVarian_LMS_LimitedAccess__c> Mv = new Map<string,MyVarian_LMS_LimitedAccess__c>();
        Mv= MyVarian_LMS_LimitedAccess__c.getall();
        datetime CurrentDay = system.now();
        for (user  objScope: scope){ 
             System.debug('User Name' +objscope.Name);        
             System.debug('***'+Mv.get('Limited Access').Number_of_Months__c);
            if(objscope.LMS_Last_Access_Date__c == null){
               if(LMSAutodeactivationHelper.calMonthDiff(objScope.LMS_Activated_Date__c,CurrentDay) >=  Mv.get('Limited Access').Number_of_Months__c){
                    if(objScope.LMS_Last_Course_End_date__c == null || objScope.LMS_Last_Course_End_date__c < System.today()){
                        objScope.LMS_Status__c = 'Inactive';
                         //objScope.LMS_Deactivation_Date__c = system.now();                        
                         userToupdate.add(objScope);
                    } 
               }
            }else{
                if(LMSAutodeactivationHelper.calMonthDiff(objScope.LMS_Last_Access_Date__c,CurrentDay) >= Mv.get('No Access').Number_of_Months__c){
                    if(objScope.LMS_Last_Course_End_date__c == null || objScope.LMS_Last_Course_End_date__c < System.today()){
                        objScope.LMS_Status__c = 'Inactive';
                         //objScope.LMS_Deactivation_Date__c = system.now();                        
                         userToupdate.add(objScope);
                    }
                
                }
            
            }     
        
        }
        if(!userToUpdate.isEmpty()){
            update userToUpdate;
            
        }
    }    
    
    global void finish(Database.BatchableContext BC){
    
    AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems,       
      CreatedBy.Email, CompletedDate FROM AsyncApexJob WHERE Id =:BC.getJobId()];       
      string Emailadd = system.Label.MVsupport;
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
      String[] toAddresses = new String[] {Emailadd};    
      mail.setToAddresses(toAddresses);      
      mail.setSubject('MyVarian: LMS Auto Deactivation Batch Job. Status: ' + a.Status+'; Runtime: ' +a.CompletedDate);     
      mail.setPlainTextBody('The batch Apex job is processed with ' + a.TotalJobItems + ' batches and '+ a.NumberOfErrors + ' failures.\nThis job auto deactivates the LMS access for MyVarian users.');      
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }

}
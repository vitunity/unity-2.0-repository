@isTest
public class Batch_LMSAutodeactivationTest{

   static  Id accountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
  Static TestMethod void LMSAutodeactivationTest(){
    
   
     userRole uRole = [Select Id,Name,developername,portaltype From Userrole Where Name='All' limit 1];     
     Profile P1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
     
      User thisUser= new User(Alias = 'standt', Email='standarduser@testorg.com',
     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
      LocaleSidKey='en_US', ProfileId = p1.Id,UserRoleid = uRole.id,
      TimeZoneSidKey='America/Los_Angeles', UserName='sysadmin11@testorg.com');
      insert thisUser;
     
     system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>+ user '+thisUser);
      
     //User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
     
     //insert custom settings
     system.runas(thisUser){
           MyVarian_LMS_LimitedAccess__c Setting1 = new MyVarian_LMS_LimitedAccess__c();
     Setting1.Name='Limited Access';
     Setting1.Number_of_Months__c = 2;
     insert Setting1;
     
     MyVarian_LMS_LimitedAccess__c Setting2 = new MyVarian_LMS_LimitedAccess__c();
     Setting2.Name='No Access';
     Setting2.Number_of_Months__c = 6;
     insert Setting2;
    
    //insert account records
    account acc = new account();
     acc.recordtypeid = accountRecTypeId;    
      acc.Name = 'Test Account';
      acc.Country__c = 'USA';
      acc.OMNI_Postal_Code__c = '12121';
      acc.Account_Type__c = 'Customer';
      insert acc;
      system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Acc owner+ user '+acc.ownerid);
       //insert contatcts
       list<Contact> conRecs = new list<Contact>();
      contact con =  new contact();
      //con.ownerId = thisUser.id;
      con.FirstName = 'Test';
      con.LastName = 'Test';
      con.AccountId =acc.id; 
      con.MailingCountry = 'USA';
      con.MailingState = 'CA';
      con.MailingPostalCode = '12345'; 
      con.email ='test123@gmail.com';
      conRecs.add(con);

       contact con1 =  new contact();
       //con1.ownerId = thisUser.id; 
       con1.FirstName = 'Test1';
       con1.LastName = 'Test1';
       con1.MailingCountry = 'USA';
       con1.MailingState = 'CA';
       con1.MailingPostalCode = '123456t'; 
       con1.AccountId =acc.id;
       con1.email ='test12314@gmail.com';
       conRecs.add(con1);
       
       contact con2 =  new contact();
       //con1.ownerId = thisUser.id; 
       con2.FirstName = 'Test2';
       con2.LastName = 'Test2';
       con2.MailingCountry = 'USA';
       con2.MailingState = 'CA';
       con2.MailingPostalCode = '123456t'; 
       con2.AccountId =acc.id;
       con2.email ='test123145@gmail.com';
       conRecs.add(con2);
       contact con3=  new contact();
       //con1.ownerId = thisUser.id; 
       con3.FirstName = 'Test3';
       con3.LastName = 'Test3';
       con3.MailingCountry = 'USA';
       con3.MailingState = 'CA';
       con3.MailingPostalCode = '123456t'; 
       con3.AccountId =acc.id;
       con3.email ='test1231456@gmail.com';
       conRecs.add(con3);
        insert conRecs;      
    
        
   
   //insert users 
   profile p  = [Select Id From Profile Where Name='VMS MyVarian - Customer User'];
    list<user> userRec=  new list<user>();
      user u = new user();
      u.Alias = 'standt1';
      u.Email='test14654@gmai.com';
      u.EmailEncodingKey='UTF-8';
      u.LastName='Testing1555';
      u.LanguageLocaleKey='en_US';
      u.LocaleSidKey='en_US';
      u.ProfileID = p.id;
      u.TimeZoneSidKey='America/Los_Angeles';
      u.UserName='standarduser123@testorg.com';      
     // u.UserRoleid = ur.id;     
      u.IsActive = true;
      u.LMS_Status__c = 'Active';
      u.Contactid = con.id;
      u.LMS_Last_Access_Date__c = null;
      u.LMS_Activated_Date__c = system.now()-110;
      u.LMS_Last_Course_End_date__c = null;
     userRec.add(u);
       
     user u1 = new user();
      u1.Alias = 'standt2';
      u1.Email='test15452@gmai.com';
      u1.EmailEncodingKey='UTF-8';
      u1.LastName='Testing256';
      u1.LanguageLocaleKey='en_US';
      u1.LocaleSidKey='en_US';
      u1.ProfileID = p.id;
      u1.TimeZoneSidKey='America/Los_Angeles';
      u1.UserName='standarduser234@testorg.com';     
      //u1.UserRoleid= ur.id;    

      u1.IsActive = true;
      u1.LMS_Status__c = 'Active';
      u1.Contactid = con1.id ;
      u1.LMS_Last_Access_Date__c = system.now()-200;
      u1.LMS_Activated_Date__c = system.now();
      u1.LMS_Last_Course_End_date__c = null;
       userRec.add(u1);
       
       user u2 = new user();
      u2.Alias = 'standt2';
      u2.Email='test12543@gmai.com';
      u2.EmailEncodingKey='UTF-8';
      u2.LastName='Testing1144';
      u2.LanguageLocaleKey='en_US';
      u2.LocaleSidKey='en_US';
      u2.ProfileID = p.id;
      u2.TimeZoneSidKey='America/Los_Angeles';
      u2.UserName='standarduser2561@testorg.com';     
      //u1.UserRoleid= ur.id;    

      u2.IsActive = true;
      u2.LMS_Status__c = 'Active';
      u2.Contactid = con2.id ;
      u2.LMS_Last_Access_Date__c = system.now()-700;
      u2.LMS_Activated_Date__c = system.now();
      u2.LMS_Last_Course_End_date__c = null;
       userRec.add(u2);

       user u3 = new user();
      u3.Alias = 'standt2';
      u3.Email='test1254453@gmai.com';
      u3.EmailEncodingKey='UTF-8';
      u3.LastName='Testing1144';
      u3.LanguageLocaleKey='en_US';
      u3.LocaleSidKey='en_US';
      u3.ProfileID = p.id;
      u3.TimeZoneSidKey='America/Los_Angeles';
      u3.UserName='standarduser25612321@testorg.com';     
      //u1.UserRoleid= ur.id;    

      u3.IsActive = true;
      u3.LMS_Status__c = 'Active';
      u3.Contactid = con3.id ;
      u3.LMS_Last_Access_Date__c = system.now()-365;
      u3.LMS_Activated_Date__c = system.now();
      u3.LMS_Last_Course_End_date__c = null;
       userRec.add(u3);
      insert userRec;

     }
     
      Batch_LMSAutodeactivation batchProc = new Batch_LMSAutodeactivation();
      ID batchprocessid = Database.executeBatch(batchProc);
    
      
  }
   
       
    
      
     
      
  
  }
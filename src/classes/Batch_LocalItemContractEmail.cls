global class Batch_LocalItemContractEmail implements  Database.Batchable<sObject>{

   
    List<string> EmailAddress;
   
 
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
        String query = 'select Id, Name, Account_Name__c, Address__c, With_in_Thirty_Days__c, Contract_End_Date__c, Contract_Start_Date__c from Local_Item_Contract__c '; 
        return Database.getQueryLocator(query) ;  
    }
    
    global void execute(Database.BatchableContext BC, List<Local_Item_Contract__c> scope)
    {
        string EmailBody;
        List<String> touser = new List<String>();
        List<Local_Item_Contract_Emails__c> lstEmail =Local_Item_Contract_Emails__c.getall().values();
        Local_Item_Contract_Emails__c email1 = Local_Item_Contract_Emails__c.getValues('Email 1');
        Local_Item_Contract_Emails__c email2 = Local_Item_Contract_Emails__c.getValues('Email 2');
        touser.add('email1');
        touser.add('email2');
        system.debug('touser'+touser);
       // touser.add('CSS-FieldOps-EMEA-India-FOAs@varian.com');
       // touser.add('CSS-FieldOps-EMEA-India-Managers@varian.com');
        for(Local_Item_Contract__c lic: scope){
           
            //DateTime createdDate = system.now();
            //Date createdDay = createdDate.date();

            //if(Integer.ValueOf(lic.Contract_End_Date__c) - Integer.ValueOf(createdDay)  <= 30){
            if(lic.With_in_Thirty_Days__c == true){
                system.debug('==lic.With_in_Thirty_Days__c ==');
                EmailBody = 'The contract '+lic.Name +' is expiring on '+ lic.Contract_End_Date__c.format();
                EmailBody = EmailBody +'<br><br>'+'Please take appropriate action.';
                EmailBody = EmailBody +'<br><br>'+ 'Thank You';
               
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(touser);
                mail.setSubject('Contract Expiring on '+ lic.Contract_End_Date__c.format());
                mail.setHTMLBody(EmailBody);
                try{
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    system.debug('sent');
                }catch(Exception e){}
            }
        }
        
        

    }
    
    global void finish(Database.BatchableContext BC)
    {}
        
}
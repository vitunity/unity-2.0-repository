@IsTest
global class Batch_LocalItemContractEmailTest{
    public static testmethod void batch_LocalItemEmail(){
          Database.QueryLocator QL;
          Database.BatchableContext BC;
        List<Local_Item_Contract__c> localLst =new List<Local_Item_Contract__c>();
        Account acc = new Account();
        acc.Country__c='INDIA';
        acc.BillingCity='PUNE';
        acc.State_Province_Is_Not_Applicable__c=true;
        acc.Name='test';
        
        insert acc;
        Local_Item_Contract__c item = new Local_Item_Contract__c();
        item.Account_Name__c=acc.id;
        item.Contract_End_Date__c = system.today().addDays(15);
        //item.Contract_Start_Date__c=system.today();
        
        //localLst.add(item);
        insert item;
        
        Local_Item_Contract__c item2 = new Local_Item_Contract__c ();
        item2 = [select Id, Name, Account_Name__c, Contract_End_Date__c , With_in_Thirty_Days__c from Local_Item_Contract__c  WHERE Id =: item.Id ];
        localLst.add(item2);
        
        Local_Item_Contract_Emails__c setting = new Local_Item_Contract_Emails__c();
        setting.Name = 'Email 1';
        setting.Email__c= 'css-fieldops-emea-india-foas@varian.com';
        insert setting;
        
        
        Batch_LocalItemContractEmail batch = new Batch_LocalItemContractEmail();        
        QL = batch.start(bc);
        batch.execute(BC, localLst);
        
    }
}
/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date          : 08-Aug-2017
    @ Description   : STSK0012079 - MyVarian User with Eclipse access should have Model Analytics access.
    Change Log:
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    31-Aug-2017 - Rakesh - STSK0012778 - Updated query to remove limit on Eclipse group.
    14-Nov-2017 - Rakesh - STSK0013441 - Updated class to fix HTTP callout issue.
****************************************************************************/
global class Batch_Model_Analytic_Member implements  Database.Batchable<sObject>{
    
    /**************************************************************************\
    @ Description   : Querying members in the Eclipse group.
    ****************************************************************************/   
    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
        String query = 'select Id,GroupId,UserOrGroupId from GroupMember where Group.Name = \'Eclipse\'';
        if(test.isRunningTest())query += ' limit 1';     
        return Database.getQueryLocator(query) ;  
    }
    
    /**************************************************************************\
    @ Description   : Updating is Model Analytic Member to true for all the queried users.
    ****************************************************************************/
    global void execute(Database.BatchableContext BC, List<GroupMember> scope)
    {
        set<Id> stModelAnalyticsId = new set<Id>();
        set<Id> setUser = new set<Id>();
        for(GroupMember cm: scope){            
            setUser.add(cm.UserOrGroupId);            
        }
        List<User> lstUser = new List<User>();
        for(User u: [select Id,contactId,is_Model_Analytic_Member__c from User where Id IN: setUser and isActive = true and contactId <> null]){            
            if(u.is_Model_Analytic_Member__c == false ){u.is_Model_Analytic_Member__c = true;lstUser.add(u);}
        }       
        Database.update(lstUser,false);      
    }
    /**************************************************************************\
    @ Description   : Updating is Model Analytic Member to False for all the opted out users from the group.
    ****************************************************************************/
    global void finish(Database.BatchableContext BC)
    {
        set<Id> stModelAnalyticsId = new set<Id>();
        set<Id> setEclipseMembers = new set<Id>();
        for(GroupMember gm : [select Id,GroupId,UserOrGroupId from GroupMember where Group.Name = 'Eclipse']){
            setEclipseMembers.add(gm.UserOrGroupId);
        }

        List<User> lstUser = new List<User>();
        for(User u: [select Id,is_Model_Analytic_Member__c from User where is_Model_Analytic_Member__c = true and Id NOT IN: setEclipseMembers limit 100]){
            u.is_Model_Analytic_Member__c = false;
            lstUser.add(u);            
        }
        if(lstUser.size() > 0){
            Database.update(lstUser,false);             
        }
    }
}
global class Batch_MyVarianUserInactivateUser implements Database.Batchable<sObject> {
	
	global string query;
	global list<User> listUserUpd; 
	global list<Contact> listContactUpd; 

	global Batch_MyVarianUserInactivateUser() {
		if(Test.isRunningTest()) {
			query = 'select id from Contact where Inactive_Contact__c = true LIMIT 20';
		} else {
			query = 'select id from Contact where lastmodifieddate >= YESTERDAY AND Inactive_Contact__c = true';
		}
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> scope) {
        listUserUpd = new list<User>();

        list<Id> listConId = new list<Id>();
        listContactUpd = new list<Contact>();

        for (Contact con : scope) {
        	con.Email = null;
        	listConId.add(con.Id);
        	listContactUpd.add(con);
        }

        for(User u : [SELECT Id, ContactId FROM User WHERE ContactId IN : listConId AND ContactId <> NULL]) {
        	u.isActive = false;
        	u.IsPortalEnabled = false;
        	listUserUpd.add(u);
        }

        if(listUserUpd.size() > 0) {
        	update listUserUpd;
        }

        if(listContactUpd.size() > 0) {
        	////update listContactUpd;
        }        
	}
	
	global void finish(Database.BatchableContext BC) {
  		Batch_MyVarianRemoveInactiveContactEmail  bat = new Batch_MyVarianRemoveInactiveContactEmail();
      	Database.executeBatch(bat, 200);		
	}
	
}
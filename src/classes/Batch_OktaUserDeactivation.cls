/**************************************************************************\

    @ Author        : Rakesh Basani
    @ Date      : 10-Jan-2017
    @ Description   : STSK0013441 - Deactivate Salesforce Inactive users in Okta.
    Date/Modified By Name/Task or Story or Inc # /Description of Change
****************************************************************************/
global class Batch_OktaUserDeactivation implements  Database.Batchable<sObject>, Database.AllowsCallouts {

    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
        String query = 'select Id,Email from User where isActive = false';
        
        if(test.isRunningTest())query += ' limit 1';   else query += ' and IsPortalEnabled = false and ContactId = null limit 100';
            
        return Database.getQueryLocator(query) ;   
    }
    global void execute(Database.BatchableContext BC, List<User> scope)
    {
        map<Id,string> mapContactOktas = new map<Id,string>();
        set<string> setEmails = new set<string>();
        for(User cm: scope){            
            setEmails.add(cm.email);          
        }
        map<string,Contact> mapContact = new map<string,Contact>();
        List<Contact> lstContact = new list<Contact>();
        
        if(test.isRunningTest()) lstContact = [select Id,Email,OktaId__c from Contact];
        else lstContact = [select Id,Email,OktaId__c from Contact where Email IN: setEmails and OktaId__c <> null];
        
        for(Contact c: lstContact ){
            mapContact.put(c.email,c);
            if(test.isRunningTest()){mapContactOktas.put(c.id,c.OktaId__c);}
        }
        
        for(User cm: scope){ 
            if(mapContact.containsKey(cm.email)){
                mapContactOktas.put(mapContact.get(cm.email).id,mapContact.get(cm.email).OktaId__c);
            }                        
        }
        
        for (Id conId: mapContactOktas.keyset())
        {

            string oktaId = mapContactOktas.get(conId);
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            String strToken = label.oktatoken;
            String authorizationHeader = 'SSWS ' + strToken;
            req.setHeader('Authorization', authorizationHeader);
            req.setHeader('Content-Type','application/json');
            req.setHeader('Accept','application/json');
            //Setting EndPoint
            String EndPoint = Label.oktachangepasswrdendpoint + oktaId +'/lifecycle/deactivate';
            req.setendpoint(EndPoint);
            req.setMethod('POST');
            try 
            {
                if(!Test.isRunningTest()){res = http.send(req);}
                else
                {
                    res = fakeresponse.fakeresponsemethod('deactivate');
                }
                system.debug('RRR:' +res.getBody());
            }
            catch(System.CalloutException e)  {System.debug('RRR:Error:'+e.getMessage()+'::'+res.toString()); }
            // http request for Deleting a user in a group
            for(CpOktaGroupIds__c cpgroup : CpOktaGroupIds__c.getall().values())
            {   HttpRequest reqgroup = new HttpRequest();   HttpResponse resgroup = new HttpResponse();   Http httpgroup = new Http();     reqgroup.setHeader('Authorization', authorizationHeader);
                reqgroup.setHeader('Content-Type','application/json');         reqgroup.setHeader('Accept','application/json');
                String endpointgroup = Label.oktaendpointforaddinggroup + cpgroup.Id__c + '/users/' + oktaId;   reqgroup.setEndPoint(endpointgroup);reqgroup.setMethod('DELETE');
                try  {   if(!Test.isRunningTest())resgroup = httpgroup.send(reqgroup); else resgroup = fakeresponse.fakeresponsemethod('groupaddition');                }
                catch(System.CalloutException e){System.debug('RRR:Error:'+e.getMessage()+'::'+resgroup.toString()); }
            }           

        }
      
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
}
@isTest
Public class Batch_OktaUserDeactivation_Test 
{
    
    public static testmethod void TestOkta()   
    {
        
        userRole uRole = [Select Id,Name,developername,portaltype From Userrole Where Name='All' limit 1];     
        Profile P1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        User thisUser= new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p1.Id,UserRoleid = uRole.id,
        TimeZoneSidKey='America/Los_Angeles', UserName='sysadmin11@testorg.com');
        insert thisUser;

        system.runas(thisUser){      
         
            Account a = new Account(name='testacc',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;      
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];        
            string oktaid = string.valueOf(math.random()*100000);
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='standardtest'+oktaid+'@testorg.com',OktaId__c = oktaid , AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '124445');
            insert con;
            
            User u = new User(alias = 'standt', email='standardtest'+oktaid+'@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', is_Model_Analytic_Member__c = false,
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standardtestuse92@testclass.com');
            insert u;
            
            
            User uObj = [select Id,IsPortalEnabled,Email,IsActive,contactId from User where id =: u.id];
            
            uObj.IsPortalEnabled = false;
            uObj.IsActive = false;
            update uObj;
            
            Con = [select id,Email,lastname from contact where id=: con.id];
            system.assertEquals(uObj.Email,con.Email);
            
        }
            
        Test.startTest();
            
            
        
            ScheduleBatch_OktaUserDeactivation sh1 = new ScheduleBatch_OktaUserDeactivation();
            String sch = '0 0 23 * * ?'; 
            system.schedule('TEST OKTA Deactivation', sch, sh1); 
        Test.stopTest();
        
        
        
        
    }
    
}
/***************************************************************************
Author: Nilesh Gorle
Created Date: 23-Sept-2018
Project/Story/Inc/Task : Check box folder if exist
Description: 
If box folder is deleted, update disposition type to destroyed and disposition date to current date
*************************************************************************************/
public class Batch_PHI_Log_Update implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful,Schedulable {
    public string query;
    public String accessToken;
    public List<PHI_Log__c> updatedPhiLogList = new List<PHI_Log__c>();
    public BoxFolderExist boxFolderEx;

    public void execute(SchedulableContext sc) {
        Batch_PHI_Log_Update b = new Batch_PHI_Log_Update();
        if(Test.isRunningTest()) {
            database.executebatch(b, 10);
        } else {
            database.executebatch(b, 20);
        }
    }

    public Batch_PHI_Log_Update() {
        query = 'select Id, Status__c, Case_Folder_Id__c, Folder_Id__c, Server_Location__c, BoxFolderName__c, IsFolderMovedToECT__c, Disposition_Date__c, Disposition2__c, Data_destruction_details__c, How_Destroyed__c, Date_Destroyed__c  From PHI_Log__c Where Folder_Id__c != null And Disposition2__c!=\'Destroyed\'';
        if(Test.isRunningTest()) {
            query=query+' LIMIT 10';
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        BoxPlatformApiConnection con;
        //if(!Test.isRunningTest()) {
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        BoxJwtEncryptionPreferences a1 = new BoxJwtEncryptionPreferences();
        a1.setPublicKeyId(boxCRecord.Box_Public_Key__c);
        a1.setPrivateKey(Ltng_BoxAccess.getBoxPrivateKey());
        a1.setEncryptionAlgorithm(BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256);
        a1.setPrivateKeyPassword(boxCRecord.Box_Private_Key_Password__c);
        con =  BoxPlatformApiConnection.getAppEnterpriseConnection(boxCRecord.Enterprise_Id__c, 
                                                                   boxCRecord.Client_Key__c, 
                                                                   boxCRecord.Client_Secret_Key__c, 
                                                                   a1);
        accessToken = con.accessToken;
        /*} else {
            BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm = BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256;
            BoxJwtEncryptionPreferences prefs = new BoxJwtEncryptionPreferences();
            prefs.setEncryptionAlgorithm(algorithm);
            prefs.setPrivateKey(BoxTestJsonResponseFactory.AUTH_PRIVATE_KEY);
            prefs.setPrivateKeyPassword('privateKeyPassword');
            prefs.setPublicKeyId('publicKeyId');
            con = BoxPlatformApiConnection.getAppEnterpriseConnection('enterpriseid', 'clientid', 'clientsecret', prefs);
            accessToken = 'accesstoken';
        }*/
        
        return Database.getQueryLocator(Query);
    }

    public void execute(Database.BatchableContext BC, List<PHI_Log__c> phiObjList) {
        boxFolderEx = new BoxFolderExist(accessToken);
        Boolean isFolderDeleted;
        Boolean phiFolderExist;
        Boolean caseFolderExist;
        boolean isEmptyFolder;

        for(PHI_Log__c phiObj: phiObjList) {
            isFolderDeleted = false;
            isEmptyFolder = false;

            if(phiObj.Case_Folder_Id__c != null) {
                caseFolderExist = boxFolderEx.isFolderExist(phiObj.Case_Folder_Id__c);
                if(!caseFolderExist) {
                    isFolderDeleted = true;
                }
            }
            
            if(!isFolderDeleted && phiObj.Folder_Id__c != null) {
                phiFolderExist = boxFolderEx.isFolderExist(phiObj.Folder_Id__c);
                if(phiFolderExist) {
                    isEmptyFolder = boxFolderEx.isFolderEmpty(phiObj.Folder_Id__c);
                    if(isEmptyFolder) {
                        boxFolderEx.isFolderDeleted(phiObj.Folder_Id__c);
                        isEmptyFolder = boxFolderEx.isFolderEmpty(phiObj.Case_Folder_Id__c);
                        if(isEmptyFolder) {
                            boxFolderEx.isFolderDeleted(phiObj.Case_Folder_Id__c);
                        }
                        isFolderDeleted = true;
                    }
                } else {
                    isEmptyFolder = boxFolderEx.isFolderEmpty(phiObj.Case_Folder_Id__c);
                    if(isEmptyFolder) {
                        boxFolderEx.isFolderDeleted(phiObj.Case_Folder_Id__c);
                        isFolderDeleted = true;
                    }
                }
            }

            if(isFolderDeleted) {
                phiObj.Case_Folder_Id__c = null;
                phiObj.Folder_Id__c = null;
                phiObj.Status__c = 'Closed';
                phiObj.Server_Location__c = null;
                phiObj.BoxFolderName__c = null;
                phiObj.IsFolderMovedToECT__c = false;           
                phiObj.Disposition_Date__c = date.today();
                phiObj.Disposition2__c = 'Destroyed';
                phiObj.Data_destruction_details__c = 'Purged by system as disposition type was marked Destroyed';
                phiObj.How_Destroyed__c = 'Erased';
                phiObj.Date_Destroyed__c = date.today();
                updatedPhiLogList.add(phiObj);
            }
        }
        
        if(!updatedPhiLogList.isEmpty()) {
            update updatedPhiLogList;
        }
    }

    public void finish(Database.BatchableContext BC) {
    }
}
@isTest(seeAllData = false)
public class Batch_PHI_Log_UpdateTest {

    private static String accessToken = 'accesstoken';
    private static String refreshToken = 'refreshtoken';
    private static String clientId = 'clientid';
    private static String clientSecret = 'clientsecret';
    private static String authCode = 'authcode';
    private static String entityId = 'entityid';
    private static String userId = 'userid';
    private static String enterpriseId = 'enterpriseid';
    private static String publicKeyId = 'publicKeyId';
    private static String privateKey = BoxTestJsonResponseFactory.AUTH_PRIVATE_KEY;
    private static String privateKeyPassword = 'privateKeyPassword';
    private static BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm = BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256;

    public static Box_Credential__c bxCrd;
    public static Account acc;
    public static contact cont;
    public static Country__c con;
    public static Profile pr;
    public static User u;
    public static case cs;
    public static SVMXC__Installed_Product__c testinstallprd;
    public static PHI_Log__c phi;

    private static BoxJwtEncryptionPreferences constructEncryptionPrefs() {
        BoxJwtEncryptionPreferences prefs = new BoxJwtEncryptionPreferences();
        prefs.setEncryptionAlgorithm(algorithm);
        prefs.setPrivateKey(privateKey);
        prefs.setPrivateKeyPassword(privateKeyPassword);
        prefs.setPublicKeyId(publicKeyId);
        return prefs;
    }

    testMethod static void testBatchPhiLogUpdate(){
        
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
        bxCrd.Client_Key__c = 'clientid';
        bxCrd.Client_Secret_Key__c = 'clientsecret';
        bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;
        
        
        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
        acc.BillingCountry='Test';
        acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
        acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        
        test.startTest();
        String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
        Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));
        String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
        Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
        BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
        String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
        Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));
        
        
        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'TEST';
        phi.Disposition_Date__c = Date.today();
        phi.Case_Folder_Id__c = '11446498';
        phi.Folder_Id__c = '11446498';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/11446498';
        phi.BoxFolderName__c = 'Testing';
        phi.Collab_id__c = '791293';
        insert phi;
        
    
        Batch_PHI_Log_Update sh1 = new Batch_PHI_Log_Update();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Batch_PHI_Log_Update123', sch, sh1);
        
        test.stopTest();
    }
}
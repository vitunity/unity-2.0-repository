/**
*   Author - Nilesh Gorle
*   Created date - 1st August 2018
*   Story - STRY0064591 - Send Contingent email for below criteria
*   Description - 
*       Condition 1 - If Clear date is in between 30 to 45 days, then send an email to Sales Rep
*       Condition 2 - If Clear date is less than 14 days, then send an email to Regional Manager with a copy to the Sales Rep, Guljot Grover, Heidi Long
*
*   Change Log:-
*       
**/
global class Batch_Send_Contingent_Email implements Database.Batchable<sObject>, Database.Stateful, Schedulable {

    global void execute(SchedulableContext sc){
        Batch_Send_Contingent_Email b = new Batch_Send_Contingent_Email();
        if(Test.isRunningTest()){database.executebatch(b,10);} else database.executebatch(b,200);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // TODO. remove where clause
        String query = 'Select Id, BigMachines__Account__r.Id, BigMachines__Account__r.Regional_Sales_Manager__r.Email, BigMachines__Account__r.Owner.Email, Release_Date__c, Contingency_Release_Date__c, Advance_Warning_Sent__c, Past_Due_Warning_Sent__c From BigMachines__Quote__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<BigMachines__Quote__c> scope) {
        List<BigMachines__Quote__c> updateBMQuotes = new List<BigMachines__Quote__c>();

        Boolean changeFlag;
        List<Contingent_Email_Receiver_List__c> emailRecList = Contingent_Email_Receiver_List__c.getall().values();
        List<String> additionalCCAddressList = new List<String>();
        for(Contingent_Email_Receiver_List__c e:emailRecList) {
            additionalCCAddressList.add(e.Email__c);
        }
        System.debug('-------CC LIST-----------'+additionalCCAddressList);

        Date todayDate = Date.today();

        for(BigMachines__Quote__c quote:scope) {
            changeFlag = false;

            // Reset the flag for future contingency email
            if (quote.Release_Date__c == null && quote.Contingency_Release_Date__c == null ) {
                System.debug('-------Reset Flags-----------');
                // reset flag
                quote.Advance_Warning_Sent__c = null;
                quote.Past_Due_Warning_Sent__c = null;

                changeFlag = true;
            }

            if(quote.Release_Date__c != null) {
                System.debug('-------Release Date-----------'+quote.Release_Date__c);
                // To check condition1 
                Integer pastDateDiff = quote.Release_Date__c.daysBetween(todayDate);
                Integer dateDiff = todayDate.daysBetween(quote.Release_Date__c);
                System.debug('-------dateDiff-----------'+dateDiff);

                if (quote.Release_Date__c != null && (dateDiff >= 30) && (dateDiff <= 45) &&  
                    quote.Release_Date__c > todayDate && quote.Advance_Warning_Sent__c == null &&  
                    quote.Contingency_Release_Date__c == null ) {

                    System.debug('-------Account Owner-----------'+quote.BigMachines__Account__r.Owner.Email);
                    quote.Advance_Warning_Sent__c = todayDate;
                    if(quote.BigMachines__Account__r.Owner.Email != null) {
                        string [] toAddress = new List<String>();
                        toAddress.add(String.valueOf(quote.BigMachines__Account__r.Owner.Email));
                        ContingentEmail cEmail = new ContingentEmail(quote.Id, 'Contingency Advance Warning Email', toAddress, new List<String>());
                        cEmail.sendContingentEmail();
                    }
                    changeFlag = true;
                }
    
                // To check condition2
                if (quote.Release_Date__c != null && (pastDateDiff <= 14) && quote.Past_Due_Warning_Sent__c == null &&  
                    (quote.Release_Date__c < todayDate || quote.Release_Date__c == todayDate) && 
                    quote.Contingency_Release_Date__c == null) {

                    quote.Past_Due_Warning_Sent__c = todayDate;    
                    System.debug('-------Account Regional Manager -----------'+quote.BigMachines__Account__r.Regional_Sales_Manager__r.Email);
                    string [] toAddress = new List<String>();
                    if(quote.BigMachines__Account__r.Regional_Sales_Manager__r.Email != null) {
                        toAddress.add(String.valueOf(quote.BigMachines__Account__r.Owner.Email));
                    }
                    string [] ccAddress = new List<String>{};
                    if(quote.BigMachines__Account__r.Owner.Email != null) {
                        ccAddress.add(String.valueOf(quote.BigMachines__Account__r.Regional_Sales_Manager__r.Email));
                        ccAddress.addall(additionalCCAddressList);
                    }

                    if(!toAddress.isEmpty()) {
                        ContingentEmail cEmail = new ContingentEmail(quote.Id, 'Contingency Past Due Warning Email', toAddress, ccAddress);
                        cEmail.sendContingentEmail();
                    }

                    changeFlag = true;
                }
            }
            
            if(changeFlag) 
                updateBMQuotes.add(quote);
        }

        if (!updateBMQuotes.isEmpty()) 
            update updateBMQuotes;

    }

    global void finish(Database.BatchableContext BC) {}
}
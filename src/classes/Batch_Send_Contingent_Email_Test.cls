@isTest
public with sharing class Batch_Send_Contingent_Email_Test {
    public static BigMachines__Quote__c salesQuote,salesQuote1,salesQuote2;
    public static ERP_Org__c erpOrg,erpOrg1;
    
    private static Account salesAccount;

    static testMethod void testSalesOrderEmails() {
        salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        salesQuote = TestUtils.getQuote();
        salesQuote.BigMachines__Account__c = salesAccount.Id;
        salesQuote.BigMachines__Opportunity__c = opp.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Price_Group__c = 'Z2';

        // Remove This line for production deployment
        salesQuote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        insert salesQuote;

        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: salesQuote.Id];
        
        Test.startTest();

        Batch_Send_Contingent_Email b = new Batch_Send_Contingent_Email();
        database.executebatch(b,1);   
            
        Test.stopTest();
    }

    static testMethod void testSalesOrderEmails1() {
        salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        salesQuote1 = TestUtils.getQuote();
        salesQuote1.BigMachines__Account__c = salesAccount.Id;
        salesQuote1.BigMachines__Opportunity__c = opp.Id;
        salesQuote1.National_Distributor__c = '121212';
        salesQuote1.Order_Type__c = 'sales';
        salesQuote1.Price_Group__c = 'Z2';

        Date todayDate = Date.today();
        Date releaseDate = todayDate.addDays(38);
        salesQuote1.Release_Date__c = releaseDate;

        // Remove This line for production deployment
        salesQuote1.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        insert salesQuote1;

        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: salesQuote1.Id];
        
        Test.startTest();

        Batch_Send_Contingent_Email b = new Batch_Send_Contingent_Email();
        database.executebatch(b,1);   
            
        Test.stopTest();
    }
    
    static testMethod void testSalesOrderEmails2() {
        salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        salesQuote2 = TestUtils.getQuote();
        salesQuote2.BigMachines__Account__c = salesAccount.Id;
        salesQuote2.BigMachines__Opportunity__c = opp.Id;
        salesQuote2.National_Distributor__c = '121212';
        salesQuote2.Order_Type__c = 'sales';
        salesQuote2.Price_Group__c = 'Z2';

        Date todayDate = Date.today();
        Date releaseDate = todayDate.addDays(-8);
        salesQuote2.Release_Date__c = releaseDate;

        // Remove This line for production deployment
        salesQuote2.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        insert salesQuote2;

        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: salesQuote2.Id];
        
        Test.startTest();

        Batch_Send_Contingent_Email b = new Batch_Send_Contingent_Email();
        database.executebatch(b,1);   
            
        Test.stopTest();
    }

    private static void insertERPAssociations(String partnerNumber, Id erpPartnerId, Id accountId){
        
        List<ERP_Partner_Association__c> partnerAssociations = new List<ERP_Partner_Association__c>();
        
        Set<String> partnerFunctions = new Set<String>{'SP=Sold-to party',
                                                       'Z1=Site Partner',
                                                       'BP=Bill-to Party',
                                                       'SH=Ship-to party',
                                                       'PY=Payer',
                                                       'EU=End User'};
        
        for(String partnerFunction : partnerFunctions){
            ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
            partnerAssociation.Customer_Account__c = accountId;
            partnerAssociation.Erp_Partner__c = erpPartnerId;
            partnerAssociation.Partner_Function__c = partnerFunction;
            partnerAssociation.ERP_Partner_Number__c = partnerNumber;
            partnerAssociation.Sales_Org__c = '0601';
            partnerAssociations.add(partnerAssociation);
            
            ERP_Partner_Association__c partnerAssociation2 = new ERP_Partner_Association__c();
            partnerAssociation2.Customer_Account__c = accountId;
            partnerAssociation2.Erp_Partner__c =    erpPartnerId;
            partnerAssociation2.Partner_Function__c = partnerFunction;
            partnerAssociation2.ERP_Partner_Number__c = partnerNumber;
            partnerAssociation2.Sales_Org__c = '0600';
            
            partnerAssociations.add(partnerAssociation2);
        }
        insert partnerAssociations;
    }
}
/**
*   Author - Nilesh Gorle
*   Created date - 29th March 2018
*   Story - STRY0055235-FullScale Flag on Account
*   Description - 
*       Condition 1 - Account has installed products with Name 'HFC' and 'HFO', then set 'isExistingOrProspectFSAccount' flag on account as well it's parent account too.
*       Condition 2 - Check Opps Line Items 'CDC003001123' or 'CDC003001125' and choose it's Account, then set 'isExistingOrProspectFSAccount' flag on account as well it's parent account too.
*
*   Change Log:-
*       
**/

/* Run batch to flag the 'isExistingOrProspectFSAccount’  on the basis of ERP Pcode HFC or HFO 
   And on the basis of Quote Product Part Number 'CDC003001123' and 'CDC003001125' */
global class Batch_Set_FS_Acct_Flag implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    global set<String> accts_id_list = new set<String>();
    global Set<String> erpPcodeNames = new Set<String>{'HFC','HFO'};
    global Set<String> fsCodes = new Set<String>{'CDC003001123','CDC003001125'};

    global void execute(SchedulableContext sc){
        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        if(Test.isRunningTest()){database.executebatch(b,10);} else database.executebatch(b,200);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        /*
        *  Condition 1 - Account has installed products with Name 'HFC' and 'HFO', then set 'isExistingOrProspectFSAccount' flag on account as well it's parent account too.
        */
        //  Get Acct Id from Installed product list
        for(SVMXC__Installed_Product__c ip:[Select Id,SVMXC__Company__c, ERP_Pcodes__r.Name 
            From SVMXC__Installed_Product__c Where SVMXC__Company__c!=null And ERP_Pcodes__r.Name in: erpPcodeNames]) {
            accts_id_list.add(ip.SVMXC__Company__c);
        }

        /*
        *  Condition 2 - Check Opps Line Items 'CDC003001123' or 'CDC003001125' and choose it's Account, then set 'isExistingOrProspectFSAccount' flag on account as well it's parent account too.
        */
        Set<String> opp_id_list = new Set<String>();
        for(OpportunityLineItem olt:[Select Id, OpportunityId From OpportunityLineItem 
                                        Where ProductCode in: fsCodes And OpportunityId != null]) {
            opp_id_list.add(olt.OpportunityId);
        }

        for(Opportunity op:[select ID, AccountId from Opportunity 
                            Where ID in: opp_id_list And AccountId!=null]) {
            if(op.AccountId != null)
                accts_id_list.add(op.AccountId);
        }

        /* Start - Get parentId  Account Id to set fullscale flag */
        List<String> erpPartnerNumbers = new List<String>();
        for(ERP_Partner_Association__c erpPA: [Select ID, ERP_Partner_Number__c 
                                                From ERP_Partner_Association__c 
                                                Where Customer_Account__c in: accts_id_list And Partner_Function__c='SP=Sold-to party' And Sales_Org__c='0600']) {
            erpPartnerNumbers.add(erpPA.ERP_Partner_Number__c);
        }

        for(Account acct:[Select Id, BillingState, BillingCity, BillingCountry, Account_Type__c from Account where Ext_Cust_Id__c in: erpPartnerNumbers And Ext_Cust_Id__c!=null]) {
            accts_id_list.add(acct.Id);
        }
        /* End - Get parentId  Account Id to set fullscale flag */

        String query = 'Select Id, isExistingOrProspectFSAccount__c, BillingState, BillingCity, BillingCountry, Account_Type__c From Account Where Account_Status__c != \'Pending Removal\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        /*** Start - Uncheck Logic Starts here
         * Below Code is written here to avoid 'Too Many SOQL Queries issue'
         * It will reset account flag
         ***/
        // opp Id as key and acc Id as value
        Map<Id, Id> fs_opps_acc_map = new Map<Id, Id>();

        // Get All Account with isExistingOrProspectFSAccount__c to true.
        Map<ID, Account> fs_accts_map = new Map<ID, Account>();
        for(Account actt:scope) {
            if(actt.isExistingOrProspectFSAccount__c==true) {
                fs_accts_map.put(actt.Id, actt);
            }
        }

        // Uncheck account from opps
        for(Opportunity opp:[Select Id, AccountId From Opportunity Where AccountId in: fs_accts_map.KeySet()]) {
            if(opp.AccountId != null)
                fs_opps_acc_map.put(opp.Id, opp.AccountId);
        }

        list<Account> acc_list = new list<Account>();
        map<id,account> accmap = new map<id,account>();
        set<String> uniqueAcctIdList = new set<String>();
        for(SVMXC__Installed_Product__c ip:[Select Id,SVMXC__Company__c, ERP_Pcodes__r.Name 
                                            From SVMXC__Installed_Product__c Where SVMXC__Company__c!=null And 
                                            SVMXC__Company__c in: fs_accts_map.KeySet() And 
                                            ERP_Pcodes__r.Name not in: erpPcodeNames]) {
            if(fs_accts_map.containsKey(ip.SVMXC__Company__c))
                uniqueAcctIdList.add(ip.SVMXC__Company__c);
        }

        // Opp Line Item
        Set<String> not_fs_opp_id_list = new Set<String>();
        for(OpportunityLineItem olt:[Select Id, OpportunityId From OpportunityLineItem 
                                        Where ProductCode not in: fsCodes And OpportunityId in: fs_opps_acc_map.KeySet()]) {
            not_fs_opp_id_list.add(olt.OpportunityId);
        }

        // Country List
        list<String> countryList = new list<String>{'Canada', 'Australia', 'Japan', 'China'};
        //create a map that will hold the values of the list 
        for(String fs_opp_Id:not_fs_opp_id_list) {
            if(fs_opps_acc_map.containsKey(fs_opp_Id)) {
                uniqueAcctIdList.add(fs_opps_acc_map.get(fs_opp_Id));
            }
        }

        /* Start - Get parentId  Account Id to set fullscale flag */
        List<String> erpPartnerNumbers = new List<String>();
        for(ERP_Partner_Association__c erpPA: [Select ID, ERP_Partner_Number__c 
                                                From ERP_Partner_Association__c 
                                                Where Customer_Account__c in: uniqueAcctIdList And Partner_Function__c='SP=Sold-to party' And Sales_Org__c='0600']) {
            erpPartnerNumbers.add(erpPA.ERP_Partner_Number__c);
        }

        for(Account acct:[Select Id, BillingState, BillingCity, BillingCountry, Account_Type__c from Account where Ext_Cust_Id__c in: erpPartnerNumbers And Ext_Cust_Id__c!=null And Account_Status__c != 'Pending Removal']) {
            fs_accts_map.put(acct.Id, acct);
            uniqueAcctIdList.add(acct.Id);
        }
        /* End - Get parentId  Account Id to set fullscale flag */
        
        
        for(String accId:uniqueAcctIdList) {
            if(fs_accts_map.containsKey(accId)) {
                Account acct = fs_accts_map.get(accId);
                acct.isExistingOrProspectFSAccount__c = false;
                if(countryList.contains(acct.BillingCountry) && acct.BillingState==null) {
                    acct.BillingState = ','; // Just to avoid validation error
                }
                if(acct.Account_Type__c=='Prospect' && acct.BillingCity==null) {
                    acct.BillingCity = ','; // Just to avoid validation error
                }
                acc_list.add(acct);
            }
        }
        //put all the values from the list to map. 
        accmap.putall(acc_list);
        /*** End- Uncheck Logic Ends here ***/

        /*** Start - Check Logic Starts here ***/
        System.debug('-------------Remove Flag List---------'+acc_list);
        for (Account acct : scope) {
            if(accts_id_list.contains(acct.Id) && acct.isExistingOrProspectFSAccount__c==false)
                acct.isExistingOrProspectFSAccount__c = true;
                if(countryList.contains(acct.BillingCountry) && acct.BillingState==null) {
                    acct.BillingState = ','; // Just to avoid validation error
                }
                if(acct.Account_Type__c=='Prospect' && acct.BillingCity==null) {
                    acct.BillingCity = ','; // Just to avoid validation error
                }
                accmap.put(acct.Id, acct);
        }
        /*** End - Check Logic Ends here ***/
        System.debug('-------------accmap---------'+accmap);

        if(accmap.size()>0){
            update accmap.values();
        }
        /* End */
    }

    global void finish(Database.BatchableContext BC) {}
}
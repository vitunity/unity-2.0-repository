@isTest
public with sharing class Batch_Set_FS_Acct_Flag_Test {
    public static BigMachines__Quote__c salesQuote;
    public static BigMachines__Quote__c serviceQuote;

    public static ERP_Org__c erpOrg,erpOrg1;
    
    private static Account salesAccount;
    private static Account salesAccount1;
    private static Account serviceAccount;
    private static Account combinedAccount;
    private static Account combinedAccount1;

    private final static Set<String> partnerFunctions = new Set<String>{'SP=Sold-to party',
                                                                        'Z1=Site Partner',
                                                                        'BP=Bill-to Party',
                                                                        'SH=Ship-to party',
                                                                        'PY=Payer',
                                                                        'EU=End User'};


    private static void insertERPAssociations(String partnerNumber, Id erpPartnerId, Id accountId){
        
        List<ERP_Partner_Association__c> partnerAssociations = new List<ERP_Partner_Association__c>();
        
        for(String partnerFunction : partnerFunctions){
            ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
            partnerAssociation.Customer_Account__c = accountId;
            partnerAssociation.Erp_Partner__c = erpPartnerId;
            partnerAssociation.Partner_Function__c = partnerFunction;
            partnerAssociation.ERP_Partner_Number__c = partnerNumber;
            partnerAssociation.Sales_Org__c = '0600';
            partnerAssociations.add(partnerAssociation);
            
            ERP_Partner_Association__c partnerAssociation2 = new ERP_Partner_Association__c();
            partnerAssociation2.Customer_Account__c = accountId;
            partnerAssociation2.Erp_Partner__c =    erpPartnerId;
            partnerAssociation2.Partner_Function__c = partnerFunction;
            partnerAssociation2.ERP_Partner_Number__c = partnerNumber;
            partnerAssociation2.Sales_Org__c = '0600';
            
            partnerAssociations.add(partnerAssociation2);
        }
        insert partnerAssociations;
    }

    /**
     * Setup sales quote data
     */
    public static void setUpSalesTestdata() {
        salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        salesAccount.Ext_Cust_Id__c = 'Sales11111';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'FullScale Service';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'CDC003001123';
        insert varianProduct;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        OpportunityLineItem oli = new OpportunityLineItem(
                                    OpportunityId = opp.Id,
                                    Quantity = 5,
                                    PricebookEntryId = pbEntry.Id,
                                    TotalPrice = 5 * pbEntry.UnitPrice
                                    );
        insert oli;
    }

    /**
     * Setup sales quote data
     */
    public static void setUpSalesTestdata1() {
        salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        salesAccount.Ext_Cust_Id__c = 'Sales11111';
        salesAccount.isExistingOrProspectFSAccount__c = true;
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'FullScale Service';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'CDC003001123';
        insert varianProduct;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        OpportunityLineItem oli = new OpportunityLineItem(
                                    OpportunityId = opp.Id,
                                    Quantity = 5,
                                    PricebookEntryId = pbEntry.Id,
                                    TotalPrice = 5 * pbEntry.UnitPrice
                                    );
        insert oli;
    }

    /**
     * Setup Installed product test data
     */
    public static void setUpInstalledProductTestdata() {
        combinedAccount = TestUtils.getAccount();
        combinedAccount.Name = 'Combined ACCOUNT PREPARE ORDER';
        combinedAccount.AccountNumber = 'Sales1212';
        combinedAccount.Country__c = 'Japan';
        combinedAccount.Ext_Cust_Id__c = 'Sales11111';
        combinedAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert combinedAccount;

        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, combinedAccount.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = combinedAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = combinedAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());

        SVMXC__Site__c loc1 = new  SVMXC__Site__c(Name = 'test location1', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = combinedAccount.id, ERP_Reference__c = '0975');
        loc1.ERP_Functional_Location__c = 'H140752-CLINAC';
        Insert loc1;
        
        //insert product
        product2 prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
        Insert prodObj;

        Schema.DescribeSObjectResult TR = Schema.SObjectType.SVMXC__Service_Group_Members__c;
        SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
        servcContact.Name = 'TestContract';
        servcContact.ERP_Sales_Org__c = 'salesorg';
        servcContact.Ext_Quote_number__c = 'TEST1234';
        servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
        servcContact.SVMXC__End_Date__c = system.today();
        insert servcContact;
        
        ERP_Pcode__c erpPcode = new ERP_Pcode__c();
        erpPcode.Name = 'HFC';
        erpPcode.ERP_Pcode_2__c = '19';
        erpPcode.Description__c = 'TrueBeam';
        erpPcode.IsActive__c = true;
        erpPcode.Service_Product_Group__c = 'LINAC';
        insert erpPcode;
        
        List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
        Map<string, Schema.RecordTypeInfo> techMapByName = TR.getRecordTypeInfosByName();
        // insert parent IP
        SVMXC__Installed_Product__c objIP = new  SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = null, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = combinedAccount.id);
        objIP.ERP_Distributor__c = '2009845';
        //objIP.ERP_Sold_To__c = combinedAccount.id;
        objIP.ERP_Product_Code__c = '1212';
        objIP.ERP_Reference__c = 'H140752-CLINAC';
        objIP.ERP_Pcodes__c = erpPcode.Id;
        objIP.Billing_Type__c = 'W - Warranty';
        objIP.SVMXC__Service_Contract__c = servcContact.Id;
        objIP.SVMXC__Company__c = combinedAccount.id;
        iplst.add(objIP);
        Insert iplst;   
    }

    public static void setUpSalesTestdata_StateAndCity() {
        salesAccount1 = TestUtils.getAccount();
        salesAccount1.Name = 'SALES ACCOUNT PREPARE ORDER';
        salesAccount1.AccountNumber = 'Sales1212';
        salesAccount1.Country__c = 'Japan';
        salesAccount1.BillingCountry = 'Japan';
        salesAccount1.BillingState = 'MH';
        salesAccount1.BillingCity = 'Pune';
        salesAccount1.Ext_Cust_Id__c = 'Sales11111';
        salesAccount1.Account_Type__c = 'Prospect';
        salesAccount1.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount1;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, salesAccount1.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount1.Id;
        insert con;

        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount1.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'FullScale Service';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'CDC003001123';
        insert varianProduct;

        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        OpportunityLineItem oli = new OpportunityLineItem(
                                    OpportunityId = opp.Id,
                                    Quantity = 5,
                                    PricebookEntryId = pbEntry.Id,
                                    TotalPrice = 5 * pbEntry.UnitPrice
                                    );
        insert oli;
    }


    /**
     * Setup Installed product test data
     */
    public static void setUpInstalledProductTestdata1() {
        combinedAccount1  = new Account();
        combinedAccount1.Name = 'Combined ACCOUNT PREPARE ORDER1';
        combinedAccount1.AccountNumber = 'Sales121223';
        combinedAccount1.Country__c = 'Japan';
        combinedAccount1.Account_Type__c = 'Prospect';
		combinedAccount1.BillingCountry = 'Japan';
        combinedAccount1.BillingState = 'MH';
        combinedAccount1.BillingCity = 'Pune';
        combinedAccount1.isExistingOrProspectFSAccount__c = true;
        combinedAccount1.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert combinedAccount1;

        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        insertERPAssociations('Sales11111', erpPartner.Id, combinedAccount1.Id);
        
        Contact con = TestUtils.getContact();
        con.AccountId = combinedAccount1.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = combinedAccount1.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());

        SVMXC__Site__c loc1 = new  SVMXC__Site__c(Name = 'test location1', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = combinedAccount1.id, ERP_Reference__c = '0975');
        loc1.ERP_Functional_Location__c = 'H140752-CLINAC';
        Insert loc1;
        
        //insert product
        product2 prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
        Insert prodObj;

        Schema.DescribeSObjectResult TR = Schema.SObjectType.SVMXC__Service_Group_Members__c;
        SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
        servcContact.Name = 'TestContract';
        servcContact.ERP_Sales_Org__c = 'salesorg';
        servcContact.Ext_Quote_number__c = 'TEST1234';
        servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
        servcContact.SVMXC__End_Date__c = system.today();
        insert servcContact;
        
        ERP_Pcode__c erpPcode = new ERP_Pcode__c();
        erpPcode.Name = 'HFC';
        erpPcode.ERP_Pcode_2__c = '19';
        erpPcode.Description__c = 'TrueBeam';
        erpPcode.IsActive__c = true;
        erpPcode.Service_Product_Group__c = 'LINAC';
        insert erpPcode;
        
        List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
        Map<string, Schema.RecordTypeInfo> techMapByName = TR.getRecordTypeInfosByName();
        // insert parent IP
        SVMXC__Installed_Product__c objIP = new  SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = null, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = combinedAccount1.id);
        objIP.ERP_Distributor__c = '2009845';
        //objIP.ERP_Sold_To__c = combinedAccount1.id;
        objIP.ERP_Product_Code__c = '1212';
        objIP.ERP_Reference__c = 'H140752-CLINAC';
        objIP.ERP_Pcodes__c = erpPcode.Id;
        objIP.Billing_Type__c = 'W - Warranty';
        objIP.SVMXC__Service_Contract__c = servcContact.Id;
        objIP.SVMXC__Company__c = combinedAccount1.id;
        iplst.add(objIP);
        Insert iplst;   
    }
    

    static testMethod void testQuoteProdutForFS(){
        Test.startTest();
        setUpSalesTestdata();
        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        database.executebatch(b,1);
        Test.stopTest();
    }

    
    static testMethod void testQuoteProdutForFSForStateAndCity(){
        Test.startTest();
        setUpSalesTestdata_StateAndCity();
        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        database.executebatch(b,1);
        Test.stopTest();
    }
    
    static testMethod void testUncheckFSFlagOnAcct(){
        Test.startTest();
        setUpSalesTestdata1();
        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        database.executebatch(b,1);
        Test.stopTest();
    }
    
    static testMethod void testInstalledProductForFS(){
        Test.startTest();
        setUpInstalledProductTestdata();

        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        database.executebatch(b,1);        
        Test.stopTest();
    }

    static testMethod void testInstalledProductForFS1(){
        Test.startTest();
        setUpInstalledProductTestdata1();

        Batch_Set_FS_Acct_Flag b = new Batch_Set_FS_Acct_Flag();
        database.executebatch(b,1);        
        Test.stopTest();
    }
}
/***************************************************************************
Author: Nilesh Gorle
Created Date: 16-August-2017
Project/Story/Inc/Task : Modify all HD/DISP cases created since 4/27/2015 to include Case Group Assignment
Description: 
This batch updates CaseFieldAssignment of Case Object with CaseFieldAssignment of USer Object for cases 
those are created after specified datetime.
*************************************************************************************/
global class Batch_updateCaseFieldAssignment implements Database.Batchable<SObject> {
    global string query;
    global List<Case> updateCaseList;
    global Map<Id, User> ownerInfoMap;
    // have to filter and get all records after selected date. format below is YYYY, MM, DD
    global Datetime selected_date;

    /************************************************************************************
    Description: 
    This is constructor for Batch_updateCaseFieldAssignment to declare query string and
    fetch all User record and map it.
    *************************************************************************************/
    global Batch_updateCaseFieldAssignment() {
        selected_date = Datetime.newInstance(2015, 4, 27);
        query = 'select Id, OwnerId, Case_Group_Assignment__c from Case where CreatedDate >= : selected_date AND Account.Account_Status__c!=\'Pending Removal\' AND Priority!=\'None\' ';

        // fetch all user records and mapping into ownerInfoMap 
        ownerInfoMap = new Map<Id, User>([select Id, Case_Group_Assignment__c, UserRole.Name from User]);
    }

    /************************************************************************************
    Description: 
    This method fires the query on Case object
    *************************************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator(Query);
    }

    /***************************************************************************
    Description: 
    This method assign CaseFieldAssignment value of User object to CaseFieldAssignment of Case
    object and update all case records.
    *************************************************************************************/
    global void execute(Database.BatchableContext BC, List<Case> toUpdate) {
        updateCaseList = new List<Case>();

        for (Case c:toUpdate) {
            try {
                if(c.OwnerId != null) {
                    User s_user = ownerInfoMap.get(c.OwnerId);
                    // it checks CaseGroupAssignment value of user.
                    // if it is null, then keep CaseGroupAssignment value of Case as null
                    // else update CaseGroupAssignment value of Case with CaseGroupAssignment value of user
                    if (s_user.Case_Group_Assignment__c!=null) {
                        c.Case_Group_Assignment__c = s_user.Case_Group_Assignment__c;
                    } else {
                        c.Case_Group_Assignment__c = null;
                    }

                    updateCaseList.add(c);
                }
            } catch(Exception e) {}
        }

        if (!updateCaseList.isEmpty()) {
            update updateCaseList;
        }
    }

    global void finish(Database.BatchableContext BC) {}
}
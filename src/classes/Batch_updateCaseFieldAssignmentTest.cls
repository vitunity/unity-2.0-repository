/*************************************************************************\
    @ Author        : Nilesh Gorle
    @ Date      : 16-August-2017
    @ Description   :  Test class for Batch_updateCaseFieldAssignment class.
****************************************************************************/
@isTest
public class Batch_updateCaseFieldAssignmentTest {
    static testMethod void test_cfa() {
        Test.startTest();

        Case cs = testUtils.createCase('New Case added');
        cs.Priority = 'Low';
        insert cs;


        Batch_updateCaseFieldAssignment sctq = new Batch_updateCaseFieldAssignment();
        Database.BatchableContext bc;

        sctq.query = 'select Id, OwnerId, Case_Group_Assignment__c from Case';

        List<Case> st = database.query(sctq.query);
        sctq.start(BC);
        sctq.execute(BC, st);
        sctq.finish(BC);

        Test.stopTest();
    }
}
/**
 * @Author: Ajinkya Raut
 * @Description: This class can be used as a generic utility to invoke any bigmachines action from salesforce
 */
global with sharing class BigMachinesAction{
    
    
    webService static Boolean execute(String transactionId, String actionName) {
        
         BigMachinesTransactions BMITxn = new BigMachinesTransactions();
        
        //send login request
        String loginXML = BMITxn.getLoginRequest();
        String response;
        if(!Test.isRunningTest()){
            response = sendRequest(loginXML, BMITxn.bmiConfig.Endpoint__c, BMITxn.bmiConfig.Error_Notification_Email__c, actionName);
        } else {
            String bmSessionID = '12345';
            response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
        }
        System.debug('----response'+response);
        if(!String.isBlank(response) && !response.contains('<bm:success>')){
          sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, 'Failed to send login request for '+actionName+' action'+transactionId);
          return false;
        }
        String sessionId = extractElementValue(response,'<bm:sessionId>','</bm:sessionId>');
        
        // send update transaction request to invoke generate epot action
        String updateTransactionXML = BMITxn.getExecuteActionRequest(sessionId,transactionId,actionName);
        response = '';
        response = sendRequest(updateTransactionXML.replace('&','&amp;'), BMITxn.bmiConfig.Endpoint__c, BMITxn.bmiConfig.Error_Notification_Email__c, actionName);
        System.debug('----requestupdateTransactionXML'+updateTransactionXML);
        System.debug('----responseupdateTransactionXML'+response);
        if(!String.isBlank(response) && !response.contains('<bm:success>')){
          sendErrorEmail(BMITxn.bmiConfig.Error_Notification_Email__c, 'Failed while '+actionName+' request');
          return false;
        }
        return true;
    }
    
    @testvisible
    private static String extractElementValue(String response, String startTag, String endTag) {
        
        String[] arr = response.split(startTag);
        if(!arr.isEmpty() && arr.size()>1) {
            arr = arr[1].split(endTag);
            String sessionID = arr[0];
            return sessionID;
        }
        return null;
    }
  
    @testvisible  
    private static String sendRequest(String xmlRequest, String endPoint, String email, String actionName){
          
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('SOAPAction','Retrieve');
        req.setHeader('Accept-Encoding','gzip,deflate');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setBody(xmlRequest);
        req.setTimeout(120000);
        req.setEndpoint(endPoint);
    
        String bodyRes = '';
        try{
          HttpResponse res = h.send(req);
          bodyRes = res.getBody();
        }catch(System.CalloutException e) {
          sendErrorEmail(email, 'Failed to send http request while bmi action'+ actionName);
        }
        System.debug('Soap request:' + xmlRequest);
        System.debug('Soap response:' + bodyRes);
        return bodyRes;
    }
    
    @TestVisible
    private static void sendErrorEmail(String email, String message){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {email};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Salesforce Support');
        mail.setSubject('Error occured while executing bmi action');
        mail.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
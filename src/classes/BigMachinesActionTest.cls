/**
 * TestClass : BigMachinesActionTest 
 */
@isTest(SeeAllData=true)
private class BigMachinesActionTest {
    
    static testMethod void extractElementValue(){

    Test.startTest();
    String  bodyRes =  '<bm:sessionId>'+'123456'+'</bm:sessionId>' + '<bm:success>';
    BigMachinesAction.extractElementValue(bodyRes , '<bm:sessionId>' , '</bm:sessionId>');
    
    //BigMachinesAction.sendRequest('TestxmlRequest', 'TestendPoint' , 'Testemail', 'TestactionName');
    
    Test.stopTest();    
    
    }
    
    static testMethod void extractElementValueTest(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new BigMachineActionMock());
            BigMachinesAction.execute('execute','generatee');
      
        Test.Stoptest();
    } 
    
    static testMethod void sendErrorMailTest() {
        Test.StartTest();
        BigMachinesAction.sendErrorEmail('Test@email.com', 'Testmessage');
        Test.StopTest();
    }
    
    public class BigMachineActionMock implements HTTpCalloutMock{
        
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response.
            // Set response values, and 
            // return response.
            String bmSessionID = '12345';
            String response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
            HTTPResponse res=new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            
            res.setBody(response);
            
            res.setStatusCode(200);
            return res;
            
        }
    }
}
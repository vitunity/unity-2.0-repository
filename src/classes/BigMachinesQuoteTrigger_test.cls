@isTest
public class BigMachinesQuoteTrigger_test
{
    public static testMethod void testmethod1(){
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = '121212';
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opp.Id;
        quote.National_Distributor__c = '121212'; 
        quote.SAP_Booked_Service__c = 'Booked';
        quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        insert quote;  
        update quote;
        
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.BigMachines__Account__c = acct.Id;
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.National_Distributor__c = '';
        bmQuote.SAP_Booked_Service__c = 'Reserved';
        bmQuote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        BigMachines__Quote__c bmQuote1 = TestUtils.getQuote();
        bmQuote1.BigMachines__Account__c = acct.Id;
        bmQuote1.BigMachines__Opportunity__c = opp.Id;
        bmQuote1.National_Distributor__c = 'Random Test';
        bmQuote1.SAP_Booked_Service__c = 'Booked';
        bmQuote1.BigMachines__Site__c = 'a31E0000000GL68IAG';
        
        List<BigMachines__Quote__c> sfQuotes = new List<BigMachines__Quote__c>{bmQuote,bmQuote1};
        
        QuoteTriggerHandler.updateNationalDistributorName(sfQuotes);
        
        System.assertEquals(sfQuotes[0].National_Distributor__c,'');
        System.assertEquals(sfQuotes[1].National_Distributor__c,'Random Test');
        
        insert sfQuotes;
    }
}
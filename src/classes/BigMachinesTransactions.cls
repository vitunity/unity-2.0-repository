public with sharing class BigMachinesTransactions {
  
  public BigMachinesInfo__c bmiConfig;
  public BigMachinesTransactions(){
    bmiConfig = BigMachinesInfo__c.getValues('Connection Details');
  }
    
  public String getCreateTransactionRequest(String bmSessionID){
    return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
      +'<soapenv:Header>'
      +'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'
          +'<bm:sessionId>'+bmSessionID+'</bm:sessionId>'
      +'</bm:userInfo>'
      +'<bm:category xmlns:bm="urn:soap.bigmachines.com">Commerce</bm:category>'
      +'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'
      +'<bm:schemaLocation>'+bmiConfig.Schema_Location__c+'commerce/quickstart_commermmerce_process.xsd</bm:schemaLocation>'
      +'</bm:xsdInfo>'
      +'</soapenv:Header>'
      +'<soapenv:Body>'
      +'<bm:createTransaction xmlns:bm="urn:soap.bigmachines.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
      +'<bm:items></bm:items>'
      +'<bm:transaction>'
      +'<bm:process_var_name>quickstart_commerce_process</bm:process_var_name>'
      +'<bm:_bm_cm_new_transaction_currency/>'
      +'<bm:return_specific_attributes>'
      +'<bm:documents>'
      +'<bm:document>'
      +'<bm:var_name>quote_process</bm:var_name>'
      +'<bm:attributes>'
      +'<bm:attribute>_document_number</bm:attribute>'
      +'<bm:attribute>quoteNumber_quote</bm:attribute>'
      +'</bm:attributes>'
      +'</bm:document>'
      +'</bm:documents>'
      +'</bm:return_specific_attributes>'
      +'</bm:transaction>'
      +'</bm:createTransaction>'
      +'</soapenv:Body>'
      +'</soapenv:Envelope>';
  }
    
  public String getUpdateTransactionRequest(String bmSessionID, String bmID, String sfdcAccountID, String accountName, String qumulateGroupUUID){
      return '<?xml version="1.0" encoding="UTF-8"?>'
      +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
      +'<soapenv:Header>'
      +'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'
      +'<bm:sessionId>'+bmSessionID+'</bm:sessionId>'
      +'</bm:userInfo>'
      +'<bm:category xmlns:bm="urn:soap.bigmachines.com">Commerce</bm:category>'
      +'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'
      +'<bm:schemaLocation>'+bmiConfig.Schema_Location__c+'commerce/quickstart_commermmerce_process.xsd</bm:schemaLocation>'
      +'</bm:xsdInfo>'
      +'</soapenv:Header>'
      +'<soapenv:Body>'
      +'<bm:updateTransaction xmlns:bm="urn:soap.bigmachines.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
      +'<bm:transaction>'
      +'<bm:id>'+bmID+'</bm:id>'
      +'<bm:process_var_name>quickstart_commerce_process</bm:process_var_name>'
      +'<bm:data_xml>'
      +'<bm:quote_process bm:bs_id="'+bmID+'" bm:buyer_company_name="'+bmiConfig.Organisation_Name__c+'" bm:buyer_user_name="jasonmanners" bm:currency_pref="USD" bm:data_type="0" bm:document_name="Quote" bm:document_number="1" bm:document_var_name="quote_process" bm:process_var_name="quickstart_commerce_process" bm:supplier_company_name="'+bmiConfig.Organisation_Name__c+'">'
      +'<bm:_document_number>1</bm:_document_number>'
      +'<bm:_customer_id>'+sfdcAccountID+'</bm:_customer_id>'
      +'<bm:accountID_SFDC>'+sfdcAccountID+'</bm:accountID_SFDC>'
      +'<bm:accountName_quote>'+accountName+'</bm:accountName_quote>'
      +'<bm:customerRequestedDeliveryDateOptions_quote>Specific Per Product</bm:customerRequestedDeliveryDateOptions_quote>'
      +'<bm:costCenter_Qoute bm:locked="true">Sales</bm:costCenter_Qoute>'
      +'<bm:incoterms_quote>US1: FOB: Origin</bm:incoterms_quote>' 
      +'<bm:eCommerce_Quote bm:locked="true">true</bm:eCommerce_Quote>'                             
      +'<bm:eCommerceGroupId_Quote bm:locked="true">'+qumulateGroupUUID+'</bm:eCommerceGroupId_Quote>'                                                                                                               
      +'</bm:quote_process>'
      +'</bm:data_xml>'
      +'<bm:action_data>'
      +'<bm:action_var_name>submit_quote</bm:action_var_name>'
      +'</bm:action_data>'
      +'</bm:transaction>'
      +'</bm:updateTransaction>'
      +'</soapenv:Body>'
      +'</soapenv:Envelope>';
    }
        // Ajinkya
        public String getExecuteActionRequest(String bmSessionID, String bmID, String actionName){      
          return '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'       
                +'<soapenv:Header>'     
                +'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'        
                +'<bm:sessionId>'+bmSessionID+'</bm:sessionId>'     
                +'</bm:userInfo>'       
                +'<bm:category xmlns:bm="urn:soap.bigmachines.com">Commerce</bm:category>'      
                +'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'     
                +'<bm:schemaLocation>'+bmiConfig.Schema_Location__c+'commerce/quickstart_commerce_process.xsd</bm:schemaLocation>'      
                +'</bm:xsdInfo>'        
                +'</soapenv:Header>'        
                +'<soapenv:Body>'       
                +'<bm:updateTransaction xmlns:bm="urn:soap.bigmachines.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'     
                +'<bm:transaction>'     
                +'<bm:id>'+bmID+'</bm:id>'      
                +'<bm:process_var_name>quickstart_commerce_process</bm:process_var_name>'       
                +'<bm:action_data>'     
                +'<bm:action_var_name>'+actionName+'</bm:action_var_name>'      
                +'</bm:action_data>'        
                +'</bm:transaction>'        
                +'</bm:updateTransaction>'      
                +'</soapenv:Body>'      
                +'</soapenv:Envelope>';     
        }
    
  public String getAddConfigurationRequest(String bmSessionID, String bmID, String numberOfLicenses, 
  String subscriptionStartDate, String billingFrequency, String numberOfYears, String salesType){
      return '<?xml version="1.0" encoding="UTF-8"?>'
      +'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'
      +'<soapenv:Header>'
      +'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'
      +'<bm:sessionId>'+bmSessionID+'</bm:sessionId>'
      +'</bm:userInfo>'
      +'<bm:category xmlns:bm="urn:soap.bigmachines.com">Configuration</bm:category>'
      +'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'
      +'<bm:schemaLocation>'+bmiConfig.Schema_Location__c+'commerce/quickstart_commermmerce_process.xsd</bm:schemaLocation>'
      +'</bm:xsdInfo>'
      +'</soapenv:Header>'
      +'<soapenv:Body>'
      +'<bm:configure xmlns:bm="urn:soap.bigmachines.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
      +'<bm:item>'
      +'<bm:segment>subscription</bm:segment>'
      +'<bm:product_line>qumulate</bm:product_line>'
      +'<bm:model>qumulate</bm:model>'
      +'</bm:item>'
      +'<bm:attributes>'
      +'<bm:attribute bm:_variableName="numberOfLicenses" bm:locked="true">'
      +'<bm:value>'+numberOfLicenses+'</bm:value>'
      +'</bm:attribute>'
      +'<bm:attribute bm:_variableName="salesType" bm:locked="true">'
      +'<bm:value>'+salesType+'</bm:value>'
      +'</bm:attribute>'
      +'<attribute bm:_variableName="subscriptionStartDate" dataType="Date" hiddenByRule="false" isArrayAttr="false" label="Subscription Start Date" locked="false" menuType="false" value_var_name="">'
      +'<value>'+subscriptionStartDate+'</value>'
      +'</attribute>'                                                                                   
      +'<bm:attribute bm:_variableName="numberOfYears" bm:locked="true">'
      +'<bm:value>'+numberOfYears+'</bm:value>'
      +'</bm:attribute>'                                                                    
      +'<bm:attribute bm:_variableName="billingFrenquency" bm:locked="true">'
      +'<bm:value>'+billingFrequency+'</bm:value>'
      +'</bm:attribute>'
      +'</bm:attributes>'
      +'<bm:responseIncludes>'
      +'<bm:price>true</bm:price>'
      +'<bm:spare>true</bm:spare>'
      +'<bm:bom>true</bm:bom>'
      +'<bm:attributeLabel>false</bm:attributeLabel>'
      +'<bm:previousValue>false</bm:previousValue>'
      +'<bm:displayedValue>false</bm:displayedValue>'
      +'<bm:hideInTransactionAttributes>false</bm:hideInTransactionAttributes>'
      +'<bm:ruleDetails>false</bm:ruleDetails>'
      +'<bm:transaction>'
      +'<bm:process_var_name>quickstart_commerce_process</bm:process_var_name>'
      +'<bm:document_var_name>quote_process</bm:document_var_name>'
      +'<bm:id>'+bmID+'</bm:id>'
      +'</bm:transaction>'
      +'</bm:responseIncludes>'
      +'<bm:price_book_var_name>_default_price_book</bm:price_book_var_name>'
      +'<bm:attributes/>'
      +'</bm:configure>'
      +'</soapenv:Body>'
      +'</soapenv:Envelope>';
  }
    
  public String getLoginRequest(){
    return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
      '<soapenv:Header>'+
        '<bm:category xmlns:bm="urn:soap.bigmachines.com">Security</bm:category>'+
        '<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:schemaLocation>'+bmiConfig.Schema_Location__c+'Security.xsd</bm:schemaLocation>'+
        '</bm:xsdInfo>'+
        '</soapenv:Header>'+
        '<soapenv:Body>'+
        '<bm:login xmlns:bm="urn:soap.bigmachines.com">'+
        '<bm:userInfo>'+
        '<bm:username>'+bmiConfig.Username__c+'</bm:username>'+
        '<bm:password>'+bmiConfig.Password__c+'</bm:password>'+
        '<bm:sessionCurrency/>'+
        '</bm:userInfo>'+
        '</bm:login>'+
        '</soapenv:Body>'+
      '</soapenv:Envelope>';
  }
}
@isTest
public with sharing class BigMachinesTransactionsTest {
    static testMethod void testBigMachinesTransactions(){
        insert new BigMachinesInfo__c(
            Name = 'Connection Details',
            Endpoint__c = 'https://varian.bigmachines.com',
            Username__c='TESTUSERNAME',
            Password__c='TESTPASSWORD',
            Organisation_Name__c='TESTORG',
            Error_Notification_Email__c = 'test@test.com'
          );
          
          BigMachinesTransactions bmiTXN = new BigMachinesTransactions();
          bmiTXN.getCreateTransactionRequest('SESSIONID');
          bmiTXN.getUpdateTransactionRequest('SESSIONID', 'BMID', 'ACCOUNTID', 'ACCOUNT NAME', 'QUMULATEUUID');
          bmiTXN.getExecuteActionRequest('SESSIONID', 'BMID', 'generateEpot');
          bmiTXN.getAddConfigurationRequest('SESSIONID', 'BMID', '2', '2017/02/02', '2017/02/02', '2', 'New');
          bmiTXN.getLoginRequest();
    }
}
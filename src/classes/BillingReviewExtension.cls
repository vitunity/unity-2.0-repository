/***************************************************************************
Author: Amitkumar Katre
Created Date: 15-May-2017
Project/Story/Inc/Task : 
Description: Billing Review Page Controller

Change Log: STSK0012332
Date/Modified By Name/Task or Story or Inc # /Description of Change
5-Sept-2017 - Amitkumar - STSK0012387 - Parts: Brazil - Add Quote Status Field to WO for Parts Nationalization
5-Sept-2017 - Amitkumar - STSK0012332 - Parts: Brazil - Add Quote Status Field to WO for Parts Nationalization

Change Log: STSK0012332
Date/Modified By Name/Task or Story or Inc # /Description of Change
4-Jan-2018 - Nick - STSK0013589 - Add button for launch of FOA Review Page
*************************************************************************************/
public with sharing class BillingReviewExtension {
    
    private ApexPages.StandardController sc;
    private SVMXC__Service_Order__c woObj;
    public list<SVMXC__Service_Order_Line__c> wdlList{get;set;}
    
    private Id fsRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
    private Id helpdeskRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Helpdesk');
    public list<WorkDetailWrapper> wdlWrapperList{get;set;}
    
    
    public enum SORT_BY {
            ByName,
            ByLineType,
            ByStartDate,
            BySAPBillable
    }
    private map<String, SORT_BY> sortByValues = new Map<String, SORT_BY>();
    private map<String,Integer> sortOrderMap = new map<String,Integer>{'ByName'=> 1,
                                                                       'ByLineType'=> 1,
                                                                       'ByStartDate'=> 1,
                                                                       'BySAPBillable'=> 1};
                                                                       
    private static map<String,String> coloumnsWithApiNames = new map<String,String>{'ByName'=> 'Name',
                                                                             'ByLineType'=> 'SVMXC__Line_Type__c',
                                                                             'ByStartDate'=>'SVMXC__Start_Date_and_Time__c',
                                                                             'BySAPBillable'=> 'ERP_Billable__c'};
    public static Integer sortOrderVal ;
    public static SORT_BY sortBy = SORT_BY.ByName;
    
    public PageReference sortByColumns(){
        String param = ApexPages.currentPage().getParameters().get('field');
        sortOrderVal = sortOrderMap.get(param);
        sortOrderMap.put(param,-sortOrderVal);
        sortBy = sortByValues.get(param);
        wdlWrapperList.sort();      
        return null;  
    }
        
    public class WorkDetailWrapper implements Comparable {
        public SVMXC__Service_Order_Line__c wdObj {get;set;}
        public boolean selectBox {get;set;}

        public WorkDetailWrapper(SVMXC__Service_Order_Line__c wdObj, boolean selectBox){
            this.selectBox = selectBox;
            this.wdObj = wdObj;
        }
        
        public Integer compareTo(Object obj) {
            WorkDetailWrapper wObj = (WorkDetailWrapper)(obj); 
            String fileApiName = coloumnsWithApiNames.get(String.valueOf(sortBy));
            SObject wData = wObj.wdObj;
            SObject wDataThis = this.wdObj;

            if(wDataThis.get(fileApiName) instanceof String || wDataThis.get(fileApiName) instanceof boolean){
                if (String.valueOf(wDataThis.get(fileApiName)) > String.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (String.valueOf(wDataThis.get(fileApiName)) == String.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }
            
            if(wDataThis.get(fileApiName) instanceof DateTime){
                if (DateTime.valueOf(wDataThis.get(fileApiName)) > DateTime.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (DateTime.valueOf(wDataThis.get(fileApiName)) == DateTime.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }            
            return -sortOrderVal;
        }   
    }
    
    public BillingReviewExtension(ApexPages.StandardController sc){
        if(!Test.isRunningTest()){
            sc.addFields(new list<String>{'Service_Team__r.Paid_Service_Overhead_JO__c',
                                      'Service_Team__r.Paid_Service_Overhead_JO__c',
                                      'Service_Team__r.Contract_Overhead_JO__c',
                                      'SVMXC__Order_Status__c','Parts_Quote_Status__c', 'Parts_Work_Detail_Count__c',
                                      'Parts_Quote_Number__c','Interface_Status__c', 'Sales_Org__c'});
        }
        
        this.sc = sc;
        for (SORT_BY enumValue : SORT_BY.values()){
            sortByValues.put(String.valueOf(enumValue), enumValue);
        }
        this.woObj = (SVMXC__Service_Order__c)sc.getRecord();
        initializeData();
        reset();
    }
     
    private void initializeData(){
        wdlWrapperList = new list<WorkDetailWrapper> ();
        wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c');
        for(SVMXC__Service_Order_Line__c wdObj : wdlList){
            system.debug('tes======'+wdObj.ERP_Billable__c);
            wdlWrapperList.add(new WorkDetailWrapper(wdObj,wdObj.ERP_Billable__c));
        }
        //Default sort by start date
        sortOrderVal = sortOrderMap.get('ByStartDate');
        sortOrderMap.put('ByStartDate',-sortOrderVal);
        sortBy = sortByValues.get('ByStartDate');
        wdlWrapperList.sort(); 
        swo_joset();
    }
    
    public void  swo_joset(){
        for(WorkDetailWrapper w : wdlWrapperList){
            if(w.wdObj.Review_Category__c == 'Overhead'){ 
                if(woObj.RecordTypeId == fsRecordTypeId){
                    w.wdObj.ERP_Service_Order__c = woObj.Service_Team__r.Paid_Service_Overhead_JO__c;
                }else if(woObj.RecordTypeId == helpdeskRecordTypeId){
                    w.wdObj.ERP_Service_Order__c = woObj.Service_Team__r.HelpDesk_Overhead_JO__c;
                }else{
                    //w.wdObj.ERP_Service_Order__c = woObj.Service_Team__r.Contract_Overhead_JO__c;
                }
            }else{
                //w.wdObj.ERP_Service_Order__c =  null;
            } 
        }
    }
    
    public String getIsValidWo(){
        if(woObj.SVMXC__Order_Status__c == 'Reviewed' && woObj.Interface_Status__c == 'Processed'){ 
            return '';
        }else{
            return 'none';
        }
    }
    
    public String getShowWoValidateMessage(){
        if(woObj.SVMXC__Order_Status__c == 'Reviewed' && woObj.Interface_Status__c == 'Processed'){ 
            return 'none';
        }else if(woObj.ID == null){
            return 'none';
        } 
        else{
            return '';
        }
    }
    
   //Add button to launch FOA Review Page 
    public PageReference foaReview(){
        if(woObj.id != null){
        PageReference pr = new PageReference('/apex/FOAReviewPage?id='+woObj.id);
        return pr;
    }else{
        PageReference pr = new PageReference('/apex/FOAReviewPage');
        return pr;
    	}
    }
    
    public PageReference approve(){
        try{
            system.debug('woObj:::'+woObj);
            woObj.SVMXC__Order_Status__c = 'Approved';
            woObj.Interface_Status__c = 'Process';
            if(String.isBlank(woObj.Internal_Comment__c)){
                woObj.Internal_Comment__c.addError('Comments are required on Approved.');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Comments are required on Approved.'));
                return null;
            }
            
            /*
             5-Sept-2017 - Amitkumar - STSK0012332 - Parts: Brazil - Add Quote Status Field to WO for Parts Nationalization
             */
            if(woObj.RecordTypeId == fsRecordTypeId &&
               woObj.Parts_Quote_Status__c == 'Received' &&
               String.isBlank(woObj.Parts_Quote_Number__c) ){
               woObj.Parts_Quote_Status__c.addError('Parts Quote Number must be filled when Parts Quote Status is Received. Please enter Parts Quote Number and Save.');
               ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Parts Quote Number must be filled when Parts Quote Status is Received. Please enter Parts Quote Number and Save.'));
                return null;
            }
            
            if(woObj.RecordTypeId == fsRecordTypeId &&
               woObj.Sales_Org__c == '4400'&&
               woObj.Parts_Work_Detail_Count__c > 0 &&
               (String.isBlank(woObj.Parts_Quote_Status__c) 
                || woObj.Parts_Quote_Status__c == 'Pending Request'
                || woObj.Parts_Quote_Status__c == 'Requested'
                )){
                woObj.Parts_Quote_Status__c.addError('Parts Quote must be \'Received\' or \'Not Required\' to change Work Order Status to Approved');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Parts Quote must be \'Received\' or \'Not Required\' to change Work Order Status to Approved'));
                return null;
            }
            
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
                SVMXC__Service_Order_Line__c wdl = wdWrapOBj.wdObj;
                if(wdl.Billing_Review__c == 'Bill'){
                    wdl.Billing_Type__c = 'P – Paid Service';
                }
                if(wdl.Review_Category__c == 'Contract'){
                    wdl.Billing_Type__c = 'N – Waived';
                }
                wdlListUpdate.add(wdl);
            }
            //SVMXC__Service_Order__c test = (SVMXC__Service_Order__c)sc.getRecord();
            system.debug('  Sales Org : ' + woObj.Sales_Org__c);
            system.debug('  WO Status : ' + woObj.SVMXC__Order_Status__c);
            system.debug('  WD Count : ' + woObj.Parts_Work_Detail_Count__c );
            //system.assertEquals(woObj.Parts_Quote_Status__c, 'abc');
            //update wdlListUpdate;
            //pdate woObj;
            update wdlListUpdate;
            sc.save();
            PageReference pg = Page.BillingReviewPage;
            pg.setRedirect(true);
            return pg;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
            return null;
        }
    }
    
    public String getDisplaySumulateBtn(){
        if(woObj.SVMXC__Order_Status__c == 'Reviewed' && woObj.Interface_Status__c == 'Processed' && woObj.RecordTypeId == helpdeskRecordTypeId){ 
            return '';
        }else{
            return 'none';
        }
    }
    
    public PageReference simulate(){
        try{
            woObj.SVMXC__Order_Status__c = 'Submitted';
            woObj.ERP_Status__c = null;
            woObj.Interface_Status__c = null;
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(SVMXC__Service_Order_Line__c wdWrapOBj : wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c')){
               wdWrapOBj.Billing_Type__c = null;
               wdlListUpdate.add(wdWrapOBj);
            }
            update wdlListUpdate;
            sc.save();
            PageReference pg = Page.BillingReviewPage;
            pg.setRedirect(true);
            return pg;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
            return null;
        }
    }
    
    public PageReference reject(){
        try{
            if(woObj.RecordTypeId == helpdeskRecordTypeId){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,'Cannot Reject HelpDesk Work Order.'));
                return null;
            }
            woObj.SVMXC__Order_Status__c = 'Submitted';
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
                SVMXC__Service_Order_Line__c wdl = wdWrapOBj.wdObj;
                wdl.Billing_Review__c = 'Reject';
                wdl.Review_Category__c = 'Fix Request';
                wdlListUpdate.add(wdl);
            }
            update wdlListUpdate;
            sc.save();
            PageReference pg = Page.BillingReviewPage;
            pg.setRedirect(true);
            return pg;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
            return null;
        }
    }
    
    public PageReference reset(){
        for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
            wdWrapOBj.selectBox = wdWrapOBj.wdObj.ERP_Billable__c;
            wdWrapOBj.wdObj.Billing_Review__c = 'Reviewed';
            wdWrapOBj.wdObj.Review_Category__c = 'As Is';
        }  
        swo_joset();
        return null;
    }
    
    public PageReference billAll(){
        for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
            if(wdWrapOBj.selectBox){
                wdWrapOBj.wdObj.Billing_Review__c = 'Bill';
                wdWrapOBj.wdObj.Review_Category__c = 'Standard';
            }
        }
        swo_joset();
        return null;
    }
    
    public PageReference waiveAll(){
        for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
            if(wdWrapOBj.selectBox){
                wdWrapOBj.wdObj.Billing_Review__c = 'Waive';
                if(wdWrapOBj.wdObj.ERP_Contract_Nbr__c != null){
                    wdWrapOBj.wdObj.Review_Category__c = 'Contract';
                }else if(wdWrapOBj.wdObj.SVMXC__Line_Type__c != 'Parts'){
                    wdWrapOBj.wdObj.Review_Category__c = 'Overhead';
                }else{
                    wdWrapOBj.wdObj.Review_Category__c = 'SWO/JO';
                }
            }
        }
        swo_joset();
        return null;
    }
    
    /*
     * Utility method to get child records with all fields
     */
    private list<Sobject> getSobjectChildRecords(String objectName, String recordId,String parentFieldName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        String query = 'SELECT';
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ', ';
        }
        /*
        if (query.subString(query.Length()-2,query.Length()-1) == ','){
            query = query.subString(0,query.Length()-2);
        }*/
        
        query += ' RecordType.DeveloperName '; 
        
        query += ' FROM ' + objectName;
        query += ' WHERE '+parentFieldName+' = :recordId '; 
        query += ' and SVMXC__Line_Status__c = \'Submitted\' ';
        query += ' and SVMXC__Line_Type__c != \'Miscelleaneous\' ';
        query += ' and Source_of_Parts__c != \'C\' ';
        query += ' and RecordType.DeveloperName = \'UsageConsumption\' ';
        query += ' and (Part_Disposition__c  != \'Removing\' OR Part_Disposition__c  != \'Removed\' )';
        
        system.debug('Query ==='+query);
        List<Sobject> sobjectList = database.query(query);
        return sobjectList;
    }
    
    public PageReference dummyAction(){
        return null;
    }
}
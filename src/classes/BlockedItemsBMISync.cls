/**
 * Send Blocked items data from Salesforce to BMI
 */
global with sharing class BlockedItemsBMISync implements Schedulable{
    global void execute(SchedulableContext sc) {
        String query = 'SELECT Id,'
        +'Input_Clearance__r.Model_Part_Number__c,Input_Clearance__r.Country__r.Name,Item_Part__r.Product_Code__c,Input_Clearance__r.Review_Product__c,Input_Clearance__r.Business_Unit__c,Item_Model__c '
        +'FROM Blocked_Items__c '
        +'WHERE Clearance_Status__c = \'Blocked\' and Item_Part__r.Product_Code__c!=\'\'';
        
        SalesforceBMIDataSync syncData = new SalesforceBMIDataSync(
            query,
            'Blocked_Items__c',
            'BlockedItems',
            'Country',
            ''
        );
        Database.executeBatch(syncData);
    }
}
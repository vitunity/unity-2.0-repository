/*************************************************************************************************************\
    @ Author        : Megha Arora
    @ Date          : 7-June-2013
    @ Description   : This Class is controller for visual force page to add blocked items in input clearance.
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :
    @ Apex Test Class: BlockeditemController_Test   
/**************************************************************************************************************/


public class BlockeditemController {
    
    public Input_Clearance__c InputClearance;
    public List<Blocked_Items__c> blocked{get;set;} 
    set<id> ids = new set<id>();
    List<processSelected> process_List = new  List<processSelected>();
    List<Blocked_Items__c> inserteditems = new  List<Blocked_Items__c>();
    List<alreadyselected> Insert_List = new  List<alreadyselected>();
    /*
        Constructor to initialize page with blocked items
    */
    
   public Blockeditemcontroller(ApexPages.StandardController controller)
    {       
        InputClearance = (Input_Clearance__c) controller.getRecord();
        blocked = new List<Blocked_Items__c>();
        inserteditems = [Select id,Item_Part__c,Feature_Name__c,Clearance_Entry_Form_Name__c,Input_Clearance__c from Blocked_Items__c where Input_Clearance__c=:InputClearance.id];
        for (integer i=0;i<inserteditems.size();i++)
        {
            ids.add(inserteditems[i].Item_Part__c);
        }
        List<Item_Details__c> items = new  List<Item_Details__c>();            
        InputClearance= [select id,Review_Product__c,Clearance_Entry_Form_Name__c from Input_Clearance__c where id=:InputClearance.id];     
        items = [select id,Review_Product__c from  Item_Details__c where Review_Product__c=:InputClearance.Review_Product__c ];               
        System.debug('items ******'+items );
        for (integer i=0;i<items.size();i++) {
        System.debug('(items[i].id********'+items[i].id);
            if (!(ids.contains(items[i].id)))          
            blocked.add(new Blocked_Items__c(Input_Clearance__c=InputClearance.id,Item_Part__c=items[i].id));
        }
    }
    
    
    /*
        Getter method to return wrapper of blocked item with checkbox
    */
    
   public List<processSelected> getprocessList()
    {
        for(Blocked_Items__c bi: blocked)
        process_List.add(new processSelected(bi));
        return process_List;        
    }
    public List<alreadyselected> getselectedlist()
    {
        for(Blocked_Items__c insertedbi:inserteditems)
        Insert_List.add(new alreadyselected(insertedbi));
        return Insert_List;        
    }
    
    /*
        Wrapper class to hold blocked item with checkbox
    */
    
    public class processSelected
    {
        public Blocked_Items__c item{get;set;}
        public boolean checkbox{get;set;}
        
        public processSelected (Blocked_Items__c bi)
        {
            item=bi;
            checkbox=false;           
        }       
    }
    
    public class alreadyselected
    {
        public Blocked_Items__c inserteditem{get;set;}
        
        public alreadyselected(Blocked_Items__c insertedbi)
        {
            inserteditem=insertedbi;       
        }       
    }
    
    /*
        Insert selected blocked items
    */         
    
    public PageReference save()
    {
        List<Blocked_Items__c> insertList = new List<Blocked_Items__c>();       
        
        for(processSelected ps : process_List)
        {
            if(ps.checkbox==true)
            {
                System.debug('ps.item*******'+ps.item);
                insertList.add(ps.item);            
            }
        }
        
        if (insertList.size()== 0|| insertList== null)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please Select atleast one record.' );
            ApexPages.addMessage(myMsg);
            return null;
        }
        else
        {
            insert insertList;                   
            
            // Redirecting back to input clearance page
            
            PageReference inputCL= new PageReference('/'+InputClearance.id);
            inputCL.setRedirect(true);
            return inputCL;
        }   
        
    }  

    
}
/**
 * This class contains unit tests for validating the behavior of Apex controller 'BlockeditemController'
 */


@isTest
private class BlockeditemControllerTest {

    static testmethod void constructorTest() {
    
    Review_Products__c rprod= new Review_Products__c();
    rprod.Product_Profile_Name__c= 'abc2';
    //rprod.Name= 'PP-123';
    insert rprod;
    
    Input_Clearance__c input = new Input_Clearance__c();
    input.Clearance_Entry_Form_Name__c= 'Test';
   // input.Name = 'CEF-000';
    input.Review_Product__c= rprod.id;
    insert input;
 
    Item_Details__c item= new Item_Details__c();
    item.Name= 'abc1';
    item.Review_Product__c= rprod.id;
    insert item;
    
    Item_Details__c item2= new Item_Details__c();
    item2.Name= 'abc2';
    item2.Review_Product__c= rprod.id;
    insert item2;
    
    Blocked_Items__c bitem  = new Blocked_Items__c();
    bitem.Input_Clearance__c= input.id;
    bitem.Item_Part__c= item.id;
    bitem.Feature_Name__c= 'model';
    insert bitem;
    
    
    Test.startTest();
    ApexPages.StandardController controller =new ApexPages.StandardController(input);
    BlockeditemController controller1 =new BlockeditemController (controller);
    BlockeditemController.processSelected obj=new BlockeditemController.processSelected(bitem);
    //obj.item=bitem;
    obj.checkbox=true;
    List<BlockeditemController.processSelected> lst =controller1.getprocessList();
    List<BlockeditemController.alreadyselected> lst2=controller1.getselectedlist();
    pagereference page = controller1.save();
    Test.stopTest();
    
    }
    
    }
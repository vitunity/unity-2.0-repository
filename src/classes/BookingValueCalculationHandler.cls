public class BookingValueCalculationHandler {
    
    public static boolean isExecuted = false;
    
    public static void calculateBookingValue(set<Id> quoteIds){
    /* pd commenting out to test the miscalculation of threshhold
        BookingValueCalculationHandler.isExecuted = true;
        map<Id,QuoteWrapper> quoteWrapMap = new map<Id,QuoteWrapper>();
        list<BigMachines__Quote_Product__c> updateQuoteProucts = new list<BigMachines__Quote_Product__c>();
        
        // Calculate for no discounted products
        for(BigMachines__Quote__c bq: getNonDiscountedQuoteProducts(quoteIds)){
            system.debug('booking value set method invoke');
            for(BigMachines__Quote_Product__c bqp : bq.BigMachines__BigMachines_Quote_Products__r){
                if(bqp.Relevant_Non_Relevant__c != 'not relevant'){
                    bqp.Net_Booking_Val__c = bqp.Threshold_Price__c;
                }
                else{
                    bqp.Net_Booking_Val__c = 0;
                }
            }
            Double totalBookingNonDiscount = 0;
            for(BigMachines__Quote_Product__c bqp : bq.BigMachines__BigMachines_Quote_Products__r){
                if(bqp.Threshold_Price__c != null ){
                     bqp.Booking_Value_v1__c = bqp.Threshold_Price__c;
                     
                    /**if(bqp.BigMachines__Sales_Price__c  == 0){
                        bqp.Booking_Value__c= bqp.Threshold_Price__c;
                    }else{
                        if(bqp.BigMachines__Sales_Price__c > 0){
                            bqp.Booking_Value__c= bqp.BigMachines__Sales_Price__c;
                        }else{
                            bqp.Booking_Value__c= 0;
                        }
                    }**/               
           /*pd     }                
                else{
                    bqp.Booking_Value_v1__c = 0;
                    
                }
                updateQuoteProucts.add(bqp);
                totalBookingNonDiscount += bqp.Booking_Value_v1__c;
            }
            QuoteWrapper qw = new QuoteWrapper();
            qw.bigQuote = bq;
            qw.totalBookingNonDiscount = totalBookingNonDiscount;
            quoteWrapMap.put(bq.Id,qw);
            
        }
        
        // Calculate for discounted products 
       
        for(BigMachines__Quote__c bq: getDiscountedQuoteProducts(quoteIds)){
            QuoteWrapper qw = quoteWrapMap.get(bq.Id);
             Decimal totalDiscountableThreshold = 0;
            for(BigMachines__Quote_Product__c bqp : bq.BigMachines__BigMachines_Quote_Products__r){
                if(bqp.Threshold_Price__c != null){
                    totalDiscountableThreshold += (bqp.Threshold_Price__c );}
                system.debug('totalDiscountableThreshold'+totalDiscountableThreshold);
            }
            for(BigMachines__Quote_Product__c bqp : bq.BigMachines__BigMachines_Quote_Products__r){
            if(bqp.Relevant_Non_Relevant__c != 'not relevant' && totalDiscountableThreshold != null && totalDiscountableThreshold != 0 && bqp.Threshold_Price__c != null && bqp.Threshold_Price__c != 0)
                {   
                    Decimal val1 = (bqp.Threshold_Price__c )/totalDiscountableThreshold;
                    Decimal val2 = bqp.BigMachines__Quote__r.BigMachines__Total_Amount__c - qw.totalBookingNonDiscount;
                    Decimal val3 = val1 * val2;
                    bqp.Net_Booking_Val__c = val3;
                }
                else{
                    bqp.Net_Booking_Val__c = 0;
                }
            }
            for(BigMachines__Quote_Product__c bqp : bq.BigMachines__BigMachines_Quote_Products__r){
                if(totalDiscountableThreshold != null && totalDiscountableThreshold != 0 && bqp.Threshold_Price__c != null && bqp.Threshold_Price__c != 0){
                    //totalDiscountableThreshold += (bqp.Threshold_Price__c );
                    Decimal val1 = (bqp.Threshold_Price__c )/totalDiscountableThreshold;
                    Decimal val2 = bqp.BigMachines__Quote__r.BigMachines__Total_Amount__c - qw.totalBookingNonDiscount;
                    Decimal val3 = val1 * val2;
                    bqp.Booking_Value_v1__c = val3;
                    
                }
                
                updateQuoteProucts.add(bqp);
            }
        }
        
        if(!updateQuoteProucts.isEmpty()){
            update updateQuoteProucts;
        }
        */
    }
    
    /*@testvisible
    private static list<BigMachines__Quote__c> getDiscountedQuoteProducts(set<Id> quoteIds){
        return [select Id, (Select Id,Threshold_Price__c,
                            BigMachines__Quote__r.Total_Discountable_Threshold__c,
                            BigMachines__Quote__r.BigMachines__Total_Amount__c,
                            BigMachines__Sales_Price__c,Relevant_Non_Relevant__c
                            from BigMachines__BigMachines_Quote_Products__r
                            where ((BigMachines__Product__r.Discountable__c = 'FALSE' 
                            AND  BigMachines__Product__r.Booking_Discountable__c = 'TRUE') 
                            OR BigMachines__Product__r.Discountable__c = 'TRUE')
                     AND Product_Type__c = 'Sales')
         from BigMachines__Quote__c
         where Id IN : quoteIds];
    }*/
    
    /*@testvisible
    private static list<BigMachines__Quote__c> getNonDiscountedQuoteProducts(set<Id> quoteIds){
        return [select Id, (Select Id,Threshold_Price__c,
                            BigMachines__Quote__r.Total_Discountable_Threshold__c,
                            BigMachines__Quote__r.BigMachines__Total_Amount__c,
                            BigMachines__Sales_Price__c,Relevant_Non_Relevant__c
                            from BigMachines__BigMachines_Quote_Products__r
                     where BigMachines__Product__r.Discountable__c = 'FALSE' 
                     AND  BigMachines__Product__r.Booking_Discountable__c = 'FALSE'
                     AND Product_Type__c = 'Sales')
         from BigMachines__Quote__c
         where Id IN : quoteIds];
    }*/
    
    /*public class QuoteWrapper{
        public BigMachines__Quote__c bigQuote;
        public Double totalBookingNonDiscount ;
    }*/
}
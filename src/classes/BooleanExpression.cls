public class BooleanExpression
{
    public static Boolean orJoin(String x, String y) { 
        return evaluate(x) || evaluate(y); 
    }
    public static Boolean andJoin(String x, String y) { 
        return evaluate(x) && evaluate(y); 
    }
    public static Boolean isSimpleExpression(String x) { 
        return x == 'true' || x == 'false'; 
    }
    public static String simplify(String x)
    {
        x = x.trim();
        while (x.contains('('))
        {
            String sub = x.substringAfterLast('(').substringBefore(')');
            x = x.replace('(' + sub + ')', String.valueOf(evaluate(sub)));
        }
        return x;
    }
    public static Boolean evaluate(String x)
    {
        x = simplify(x);    
        if (!isSimpleExpression(x))
        {
            
            if (x.contains('&&'))
            {
                system.debug('***CM and X : '+x);
                return andJoin(x.substringBefore('&&'),x.substringAfter('&&'));
            }
            if (x.contains('||')){
                system.debug('***CM OR x: '+x);
                return orJoin(x.substringBefore('||'),x.substringAfter('||'));
            }
            if (x.startsWith('!')) return !evaluate(x.substring(1));
        }
        return Boolean.valueOf(x);
    }
}
public class BoxAddPermission {
    public PHI_Log__c oTmpPHILog {get;set;}
    public String textfield{get;set;}
    public String emailfield{get;set;}
    public String oemailfield{get;set;}
    public String namefield{get;set;}
    public String rolefield{get;set;}
    Public string selectedname{get;set;}
    Public string selectedrole{get;set;}
    Public Boolean VarianCustomer {get; set;}
    Public Boolean NonVarianCustomer {get; set;}
    Public Boolean NonBoxCustomer {get; set;} 
    public boolean bUploaded{get;set;}
    public boolean isFolderExist{get;set;}
    String Email;
    String Name;
    public Id recid{get;set;}
    String role;
    public string infoMsg, errMsg;

    public BoxAddPermission(){
        oTmpPHILog = new PHI_Log__c();
        
        recid = ApexPages.currentPage().getParameters().get('id');
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recid);
        if(!philstc.isEmpty() && philstc[0].Case_Folder_Id__c==null && philstc[0].Folder_Id__c==null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'There is no folder created in BOX.'));
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'To resolve this error, close this error message and upload your data to BOX by clicking "Upload To Varian Secure Drop" button in this PHI Log.'));
            isFolderExist = false;
        } else {
            isFolderExist = true;
        }
    }
    
    Public List<Selectoption> getselectednamefields(){
        List<Selectoption> lstnamesel = new List<selectoption>();
        lstnamesel.add(new selectOption('None', '- None -'));
        lstnamesel.add(new selectOption('Varian_Customers', 'Unity User'));
        lstnamesel.add(new selectOption('Non_Varian_Customers', 'Non Unity/Customer'));
        return lstnamesel; 
    }
    
    Public List<Selectoption> getselectedrolefields(){
        List<Selectoption> lstrole = new List<selectoption>();
        lstrole.add(new selectOption('None', '- None -'));
        lstrole.add(new selectOption('Co_Owner', 'Co-owner'));
        lstrole.add(new selectOption('Viewer', 'Viewer'));
        lstrole.add(new selectOption('Uploader', 'Uploader'));
        lstrole.add(new selectOption('preview_Uploader', 'Preview-Uploader'));
        lstrole.add(new selectOption('Editor', 'Editor'));
        lstrole.add(new selectOption('previewer', 'Previewer'));
        return lstrole; 
    }
    
    public void setName(){
        system.debug('On picklist value change ===> ' + selectedname);
        if(selectedName == 'Varian_Customers'){
            VarianCustomer = true;
            NonVarianCustomer = false;
        } else if(selectedName == 'Non_Varian_Customers' ){
            NonVarianCustomer = true;
            VarianCustomer = false;    
        } else {
            NonVarianCustomer = false;
            VarianCustomer = false;
        }
    }
    public pagereference saveusers(){
        bUploaded = false;
        errMsg = '';
        infoMsg = '';
        recid = ApexPages.currentPage().getParameters().get('id');

        if(selectedrole == 'None') {
            errMsg = 'Please, select a role.';
        }
        if(selectedName == 'Varian_Customers' && oTmpPHILog.Director_Dept_Head__c == null) {
            errMsg += 'Please, select a user';
        }
        if(selectedName == 'Non_Varian_Customers' && emailfield == null) {
            errMsg += 'Please, select a user';
        }
        if(errMsg != '') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,errMsg));
            return null;
        }

        List<String> resultList = new List<String>();
        System.debug('=========Start==============');
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recid);
        PHI_Log__c phiLogRec;
        if(!philstc.isEmpty()) {
            phiLogRec = philstc[0];
            if(phiLogRec.Case_Folder_Id__c==null && phiLogRec.Folder_Id__c==null) {
                system.debug('-------------BOX Folder Not Exist-------');
                return null;
            }
        } else {
            system.debug('Record Not Found---'+recid);
            return null;
        }
        
        try{
            system.debug('@@@recid'+recid);
            Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
            String accessToken;
            String expires;
            String isUpdate;
            if(resultMap != null) {
                accessToken = resultMap.get('accessToken');
                expires = resultMap.get('expires');
                isUpdate = resultMap.get('isUpdate');
            }

            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            
            String caseFolderId;
            if (phiLogRec.Case_Folder_Id__c != null) {
                caseFolderId = String.valueOf(phiLogRec.Case_Folder_Id__c);
            }

            if(caseFolderId != null) {
                Boolean isUserAdded;
                if(oTmpPHILog.Director_Dept_Head__c != Null) {
                    for(User u:[select id,Name,Email from User where Id=:oTmpPHILog.Director_Dept_Head__c limit 1]) {
                        Email = String.Valueof(u.Email);
                        Name = String.Valueof(u.Name);
                    }
                    system.debug('@@role'+selectedrole+'@@varianuser'+Email+'@@api'+api);
                } else if(emailfield != null) {
                    Email = emailfield;
                    Ltng_BoxAccess.inviteUser(accessToken, Email);
                }

                if(Email==null || Email=='') {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email is required'));
                } else {
                    isUserAdded = settingRoles(phiLogRec, selectedrole, Name, Email, caseFolderId, api);

                    if(isUserAdded) {
                        bUploaded = true;
                        infoMsg = 'User updated successfully on the box folder';

                        // Update PHI Log History
                        Ltng_BoxAccess.createPHIHistoryLog('New user '+Email+' is added to Varian Secure Drop Folder', recid, 'PHI_Log__c');
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, infoMsg));
                    }
                }
                // Update Box Access Token to Custom Setting
                Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
            }
        } catch(Exception ex){
            if(ex.getMessage()=='The Box API responded with a 400 : Bad Request') {
                errMsg = 'User already has permission on the box folder';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,errMsg));
        }
        return null;
    }

    public Boolean settingRoles(PHI_Log__c phiLogRec, String role, String Name, String Email,String caseFolderId,BoxAPIConnection api) {
        String getcollab;
        String collbrtnid;
        String collaborationids;
        BoxFolder folder = new BoxFolder(api,caseFolderId);
        System.debug('collabId---------');
        String collabId = Ltng_BoxAccess.checkCollabExistForFolder(folder, Name, Email);
        System.debug('collabId---------'+collabId);
        BoxCollaboration.Info collabInfo;
        BoxCollaboration.Role boxFolderRole;
        if(role=='Co_Owner') {
            boxFolderRole = BoxCollaboration.Role.CO_OWNER;
        } else if(role=='Editor') {
            boxFolderRole = BoxCollaboration.Role.EDITOR;
        } else if(role=='Uploader') {
            boxFolderRole = BoxCollaboration.Role.UPLOADER;
        } else if(role=='Viewer') {
            boxFolderRole = BoxCollaboration.Role.VIEWER;
        } else if(role=='preview_Uploader') {
            boxFolderRole = BoxCollaboration.Role.PREVIEWER_UPLOADER;
        } else if(role=='previewer') {
            boxFolderRole = BoxCollaboration.Role.PREVIEWER;
        }

        if(collabId == null) {
            system.debug('@@@before called API');
            collabInfo = folder.collaborate(Email, boxFolderRole);
            system.debug('@@@after called API');
            String collab = (String.valueOf(BoxFolder.collabResponse));
            system.debug('@@@response'+collab);
            Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(collab);
            collbrtnid = String.valueOf(ooresults.get('id'));
            system.debug('@@@collbrtnid'+collbrtnid);
        } else {
            System.debug('Update---------');
            BoxCollaboration boxCollab = new BoxCollaboration(api, collabId);
            collabInfo = boxCollab.updateCollaboration(boxFolderRole);
            collbrtnid = collabId;
        }

        phiLogRec.User_id_s_of_owners__c += (collbrtnid+',');
        phiLogRec.User_id_s_of_owners__c = (phiLogRec.User_id_s_of_owners__c).remove('null').removeend(',');
        System.debug('==========='+phiLogRec.User_id_s_of_owners__c);
        update phiLogRec;
        return true;
        
    }
}
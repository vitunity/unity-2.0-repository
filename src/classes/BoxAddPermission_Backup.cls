public class BoxAddPermission_Backup {
    public PHI_Log__c oTmpPHILog {get;set;}
    public String textfield{get;set;}
    public String emailfield{get;set;}
    public String oemailfield{get;set;}
    public String namefield{get;set;}
    public String rolefield{get;set;}
    Public string selectedname{get;set;}
    Public string selectedrole{get;set;}
    Public Boolean VarianCustomer {get; set;}
    Public Boolean NonVarianCustomer {get; set;}
    Public Boolean NonBoxCustomer {get; set;} 
    public boolean bUploaded{get;set;}
    String oemail;
    String accky;
    String Email;
    String csfldr;
    public Id recid{get;set;}
    String role;
   String prikey='MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDam/Nt+Wot6eUG+kaKt3btsPCjjj2yWGJyY48moEHcDtK9KSlFhuqSWDcSDtV9/V+1Mzo52KP/etqg43iquZluZsAbFbbLnd0yQrpD/w412//cG8Kij5boSo8hXaMwqniAUY6roS9G0GW48FbN8zXb++Hy5/LOj1y97YeJy3W7+mT61VxgB+0Fw0yF5Yqox97zPi27tAZpwlCTyNcCtviqidBWRU/Ib0q3xB4dtzaPTUG2h0s0SeyPBiulfXW36wsMoi3NQe3MOiQ8IzctT6w55WPkgoaPJN0E9EgTsaPrg6CNnoW5eqmJSnHYs88bV0nYlGhV4tt9QL8HvAF343XlAgMBAAECggEBALXrlbZtGqk+vXMDD3UcnAHNf1P8aOdjmflrVJNmRahlRYZZHJlZXxeOBbYnVg7UrbLTAJ9m44MVr5637ZDVhxNSuodPmKD1rD/JO8yeEitL+y4mg+BX8dM5SMcRb15uXn0aYcn+BqO9hQlUKEiXHrkEcdF8qJiwwRLk+if3gvB8MmHp/I0jGnVBUAzYxasFYeQ74vSlck61I3K0IkLgMhF5U4xZQFmjoZwgjDxXXsnbK0kjnNDIRWlMo3X5Ado71d6ygrFcHWgGXNVadQ6ZF9wIEQxofWJdorrpTqYkm46cwCCD+e5ZNCR+u0QVqd0KMuABwLnGLUS5GQ0wLYfpPAECgYEA/7/IxYOytWpg7698U/axUoStpitJmWq6Bt3I+sVk1RiSPB9YOzGShxUERn2ntsKCrNVcFlgO9JtiCjdBZaWPwH/JwAuLYUwNZCtJi/CnxozRbrz8rnaYV4TQEmG4iGAcMSKHcOzpoHnYUuWhSkmREg7d2GKzcUcQWeniW2k+/YECgYEA2tLXWRn5zJxe/HqXzbreygv7lYpPyaUqBKKYtdta/ypO5p+d48lcN/G177EXlhmYJ01N6fiC2eclQh3X6bdSuDLtBPWphi6TabBKBZ6BeFuGcrAVBU3i7rMIiUAjZtbtvFsjuYclj1GeCfHgLk5hWETvuNr/ZACEM8gV40QmcmUCgYAC/D0euRvT7Er3YUgFPuLxAKV6RBUW2l0TiXE4JCe6KRBD7WW9QyXft8oV/I+BnaGi3Na3WA3Moyew0NZNlnIoIBW9zSSyXQ3m9m5kWMnMkoY7Ua9tZer/UoiPPl7GEMEjfbCxC8LqYaG5zf5k/JjZ6hyC0xwfHWI+enFu+bqHgQKBgQDWAkSIqOXsnbYsGT2kADHpysRXoTidToIEnHzbxtd9HISj+tFxOLqPID8+V6VosElljq43uEtJD04aFpPWyOsGqQ+zvQr5501WnQoX6shWzLR2MA2u7ViW+NPNX8P/zQ4fG8eZqDosq7bzpPIKd6+uo2UMFmqWBAdHmVUGut0bHQKBgQC11B1woXBFmPkrh4/5MNCczp8OGkrKtT8JPL59PptJF4SJjmXszte8rN2cZJo0UV950xpXmoR5KLw1hjo7iJso2GjK1NzqFfrtuG5GBDCr0bHWABPb2RzsAlJ6mDsgYCOpCxdbPOReglcOwFIv5BahOYUovv+A9uGHar1sMNqJ7A==';
        string key =label.Box_Client_key;
    string secret =label.Box_Client_Secret;
     String enterpriseId=label.Box_enterprise_Id;
    String pubkey=label.Box_public_key;
    String userId=label.Box_User_ID;
    public BoxAddPermission_Backup(){
        oTmpPHILog = new PHI_Log__c();
    }
    
    Public List<Selectoption> getselectednamefields(){
        List<Selectoption> lstnamesel = new List<selectoption>();
        lstnamesel.add(new selectOption('None', '- None -'));
        lstnamesel.add(new selectOption('Varian_Customers', 'Varian User'));
        lstnamesel.add(new selectOption('Non_Varian_Customers', 'Non Varian/Customer'));
        return lstnamesel; 
    }
    
    Public List<Selectoption> getselectedrolefields(){
        List<Selectoption> lstrole = new List<selectoption>();
        lstrole.add(new selectOption('None', '- None -'));
        lstrole.add(new selectOption('Co_Owner', 'Co-owner'));
        lstrole.add(new selectOption('Viewer', 'Viewer'));
        lstrole.add(new selectOption('Uploader', 'Uploader'));
        lstrole.add(new selectOption('preview_Uploader', 'Preview-Uploader'));
        lstrole.add(new selectOption('Editor', 'Editor'));
        lstrole.add(new selectOption('previewer', 'Previewer'));
        return lstrole; 
    }
    
    public void setName(){
        system.debug('On picklist value change ===> ' + selectedname);
        if(selectedName == 'Varian_Customers'){
            VarianCustomer = true;
            NonVarianCustomer = false;
           
        } else if(selectedName == 'Non_Varian_Customers' ){
            NonVarianCustomer = true;
            VarianCustomer = false;
            
        }
    }
    public pagereference saveusers(){
        bUploaded = false;
      recid=ApexPages.currentPage().getParameters().get('id');
        try{
            system.debug('@@@recordid'+recid);
            BoxJwtEncryptionPreferences a1=new BoxJwtEncryptionPreferences();
            a1.setPublicKeyId(pubkey);
            a1.setPrivateKey(prikey);
            a1.setEncryptionAlgorithm(BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256);
            a1.setPrivateKeyPassword('f64f9376101d53bf1c660764afcf7dd6');
            BoxPlatformApiConnection con = new  BoxPlatformApiConnection(enterpriseId,BoxPlatform.PlatformEntityType.ENTERPRISE,key,secret,a1);
            BoxPlatformApiConnection con1 =  BoxPlatformApiConnection.getAppEnterpriseConnection(enterpriseId,key,secret,a1);
            accky=con1.accessToken;
            BoxAPIConnection api = new BoxAPIConnection(con1.accessToken);
            for(PHI_Log__c oolstofphi:[select id,Name,Case_Folder_Id__c from PHI_Log__c where Id=:recid])
            {
                csfldr=String.valueOf(oolstofphi.get('Case_Folder_Id__c'));
            }
              if(oTmpPHILog.Director_Dept_Head__c!=Null) {
                for(User u:[select id,Name,Email from User where Id=:oTmpPHILog.Director_Dept_Head__c limit 1])
                {
                    Email=String.Valueof(u.get('Email'));
                }
                  system.debug('@@role'+selectedrole+'@@varianuser'+Email+'@@casefolder'+csfldr+'@@api'+api);
            settingRoles(selectedrole,Email,csfldr,api);
            }else if(emailfield!=null){
                settingRoles(selectedrole,emailfield,csfldr,api);
                }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'User Updated Successfully on the Box folder'));
            bUploaded = true;
        }
        catch(Exception ex){
            if(ex.getMessage()=='The Box API responded with a 400 : Bad Request')
            {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'User already has permission on the Box folder'));

            }
        }
     return null;  
    }
   public void settingRoles(String role,String Email,String casfldrid,BoxAPIConnection api)
        {
         String getcollab; 
            String collaborationids;
        if(role=='Co_Owner')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.CO_OWNER);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                }else if(role=='Editor')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.EDITOR);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                }else if(role=='Uploader')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.UPLOADER);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                }else if(role=='Viewer')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.VIEWER);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                }else if(role=='preview_Uploader')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.PREVIEWER_UPLOADER);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                }else if(role=='previewer')
                {
                    BoxFolder folder = new BoxFolder(api,casfldrid);
                    BoxCollaboration.Info collabInfo = folder.collaborate(Email,BoxCollaboration.Role.PREVIEWER);
                    list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                    getcollab = String.valueOf(BoxFolder.getcollabResponse);
                } 
 
        }  

}
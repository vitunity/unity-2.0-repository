public class BoxFolderExist {
    public string accessToken;

    public BoxFolderExist(String accToken) {
        accessToken = accToken;
    }

    public Boolean isFolderExist(String folderId) {
        http http=new http();
        HTTPResponse res=new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+accessToken); 
        String endpoint = 'https://api.box.com/2.0/folders/'+folderId;
        req.setEndpoint(endpoint);
        res = http.send(req);
        system.debug('@@@response'+res);
        Integer uploadStatusCode = res.getStatusCode();
        system.debug('--uploadStatusCode----'+uploadStatusCode);

        if(Test.isRunningTest()) {
            uploadStatusCode = 404;
        }

        if(uploadStatusCode==404) {
            return false;
        }

        if(uploadStatusCode==200 || uploadStatusCode==201) {
            return true;
        }

        return false;
    }
    
    public Boolean isFolderEmpty(String folderId) {
        http http=new http();
        HTTPResponse res=new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+accessToken);
        String endpoint = 'https://api.box.com/2.0/folders/'+ folderId +'/items';
        req.setEndpoint(endpoint);
        res = http.send(req);
        system.debug('@@@response'+res);
        Integer uploadStatusCode = res.getStatusCode();
        system.debug('--uploadStatusCode----'+uploadStatusCode);
        String jasonresp = res.getBody();
        Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);

        if(Test.isRunningTest()) {
            uploadStatusCode = 404;
        }

        if(uploadStatusCode == 404) {
            return false;
        }
        
        if(uploadStatusCode == 200 && Integer.valueOf(jsonObj.get('total_count')) == 0) {
            return true;
        }

        return false;
    }

    public Boolean isFolderDeleted(String folderId) {
        http http=new http();
        HTTPResponse res=new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setMethod('DELETE');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+accessToken);
        String endpoint = 'https://api.box.com/2.0/folders/'+folderId;
        req.setEndpoint(endpoint);
        res = http.send(req);
        system.debug('@@@response'+res);
        Integer uploadStatusCode = res.getStatusCode();
        system.debug('--uploadStatusCode----'+uploadStatusCode);

        if(Test.isRunningTest()) {
            uploadStatusCode = 404;
        }

        if(uploadStatusCode == 404) {
            return false;
        }

        if(uploadStatusCode == 200 || uploadStatusCode == 201) {
            return true;
        }

        return false;
    }
}
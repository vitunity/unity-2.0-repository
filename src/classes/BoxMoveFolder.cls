public class BoxMoveFolder {
    
    public static void afterUpdate(List<PHI_Log__c> philst,Map<Id,PHI_Log__c> phimap)
    {
       Map<String,String> mpph=new Map<String,String>();
        for(PHI_Log__c p:philst)
        {
         
            String phid=p.Id;
            mpph.put(phid,'');
            
        }
        if(mpph.size()>0)
        {
         movetoqueue(mpph);   
        } 
    }
    @future(Callout=true)
public static void movetoqueue(Map<String,String> idwisephi)
{
   String prikey='MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDam/Nt+Wot6eUG+kaKt3btsPCjjj2yWGJyY48moEHcDtK9KSlFhuqSWDcSDtV9/V+1Mzo52KP/etqg43iquZluZsAbFbbLnd0yQrpD/w412//cG8Kij5boSo8hXaMwqniAUY6roS9G0GW48FbN8zXb++Hy5/LOj1y97YeJy3W7+mT61VxgB+0Fw0yF5Yqox97zPi27tAZpwlCTyNcCtviqidBWRU/Ib0q3xB4dtzaPTUG2h0s0SeyPBiulfXW36wsMoi3NQe3MOiQ8IzctT6w55WPkgoaPJN0E9EgTsaPrg6CNnoW5eqmJSnHYs88bV0nYlGhV4tt9QL8HvAF343XlAgMBAAECggEBALXrlbZtGqk+vXMDD3UcnAHNf1P8aOdjmflrVJNmRahlRYZZHJlZXxeOBbYnVg7UrbLTAJ9m44MVr5637ZDVhxNSuodPmKD1rD/JO8yeEitL+y4mg+BX8dM5SMcRb15uXn0aYcn+BqO9hQlUKEiXHrkEcdF8qJiwwRLk+if3gvB8MmHp/I0jGnVBUAzYxasFYeQ74vSlck61I3K0IkLgMhF5U4xZQFmjoZwgjDxXXsnbK0kjnNDIRWlMo3X5Ado71d6ygrFcHWgGXNVadQ6ZF9wIEQxofWJdorrpTqYkm46cwCCD+e5ZNCR+u0QVqd0KMuABwLnGLUS5GQ0wLYfpPAECgYEA/7/IxYOytWpg7698U/axUoStpitJmWq6Bt3I+sVk1RiSPB9YOzGShxUERn2ntsKCrNVcFlgO9JtiCjdBZaWPwH/JwAuLYUwNZCtJi/CnxozRbrz8rnaYV4TQEmG4iGAcMSKHcOzpoHnYUuWhSkmREg7d2GKzcUcQWeniW2k+/YECgYEA2tLXWRn5zJxe/HqXzbreygv7lYpPyaUqBKKYtdta/ypO5p+d48lcN/G177EXlhmYJ01N6fiC2eclQh3X6bdSuDLtBPWphi6TabBKBZ6BeFuGcrAVBU3i7rMIiUAjZtbtvFsjuYclj1GeCfHgLk5hWETvuNr/ZACEM8gV40QmcmUCgYAC/D0euRvT7Er3YUgFPuLxAKV6RBUW2l0TiXE4JCe6KRBD7WW9QyXft8oV/I+BnaGi3Na3WA3Moyew0NZNlnIoIBW9zSSyXQ3m9m5kWMnMkoY7Ua9tZer/UoiPPl7GEMEjfbCxC8LqYaG5zf5k/JjZ6hyC0xwfHWI+enFu+bqHgQKBgQDWAkSIqOXsnbYsGT2kADHpysRXoTidToIEnHzbxtd9HISj+tFxOLqPID8+V6VosElljq43uEtJD04aFpPWyOsGqQ+zvQr5501WnQoX6shWzLR2MA2u7ViW+NPNX8P/zQ4fG8eZqDosq7bzpPIKd6+uo2UMFmqWBAdHmVUGut0bHQKBgQC11B1woXBFmPkrh4/5MNCczp8OGkrKtT8JPL59PptJF4SJjmXszte8rN2cZJo0UV950xpXmoR5KLw1hjo7iJso2GjK1NzqFfrtuG5GBDCr0bHWABPb2RzsAlJ6mDsgYCOpCxdbPOReglcOwFIv5BahOYUovv+A9uGHar1sMNqJ7A==';
    string key =label.Box_Client_key;
    string secret =label.Box_Client_Secret;
     String enterpriseId=label.Box_enterprise_Id;
    String pubkey=label.Box_public_key;
    String userId=label.Box_User_ID;
 String accky;
    String phifldrid;
    String csfldrid;
    String fldrnme;
    String etfldrnme;
  String endpoint;
    String body;
    String jasonresp;
     BoxJwtEncryptionPreferences a1=new BoxJwtEncryptionPreferences();
     a1.setPublicKeyId(pubkey);
     a1.setPrivateKey(prikey);
     a1.setEncryptionAlgorithm(BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256);
     a1.setPrivateKeyPassword('f64f9376101d53bf1c660764afcf7dd6');
     BoxPlatformApiConnection con = new  BoxPlatformApiConnection(enterpriseId,BoxPlatform.PlatformEntityType.ENTERPRISE,key,secret,a1);
     BoxPlatformApiConnection con1 =  BoxPlatformApiConnection.getAppEnterpriseConnection(enterpriseId,key,secret,a1);
     accky=con1.accessToken;
    List<String> fldrids=new List<String>();
       List<PHI_Log__c> ophilst=new List<PHI_Log__c>([select id,Ownerid,Current_Owners__c,Case_Folder_Id__c,Collab_id__c from PHI_Log__c where Id in:idwisephi.keySet()]);
     for(PHI_Log__c ophi:[select id,Case_Folder_Id__c,Folder_Id__c,Complaint__c,Name from PHI_Log__c where Id in:idwisephi.keySet()])
     {
         phifldrid=String.valueOf(ophi.get('Folder_Id__c'));
         csfldrid=String.valueOf(ophi.get('Case_Folder_Id__c'));
         fldrnme=String.valueOf(ophi.get('Name'));
         etfldrnme=String.valueOf(ophi.get('Complaint__c'));         
         fldrids.add(phifldrid);
         fldrids.add(csfldrid);
     }
    try{
     http http=new http();
     HTTPResponse res=new HttpResponse();
    HttpRequest req = new HttpRequest();
    req.setMethod('PUT');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+accky); 
        
    for(String n:fldrids)
    {
  if(n==csfldrid)
  {
 endpoint='https://api.box.com/2.0/folders/'+n;
      body='{"name":"'+etfldrnme+'"}';
      req.setEndpoint(endpoint);
req.setBody(body);
  }else
  {
     endpoint='https://api.box.com/2.0/folders/'+n;
      body='{"name":"'+fldrnme+'"}';
      req.setEndpoint(endpoint);
req.setBody(body);
    }
     
}
        
         res = http.send(req);
                jasonresp = res.getBody();
                System.debug(res.getstatuscode());
    }
    catch(Exception e)
    {
        system.debug('@@@exception'+e.getMessage());
    }
    
}
}
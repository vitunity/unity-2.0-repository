public class BoxScript {

	String accessToken;
	Box_Credential__c boxCRecord;
	BoxAPIConnection api;

	public BoxScript() {
		 boxCRecord = Ltng_BoxAccess.getBoxCredential();
		Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
		if(resultMap != null) {
			accessToken = resultMap.get('accessToken');
		}

		api = new BoxAPIConnection(accessToken);
	}

	public void createFolder(String FolderName) {
        String fldrid;
        try{
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders' ;
            req.setEndpoint(endpoint);
            string body = '{"name":"'+FolderName+'", "parent": {"id": "0"}}';
            req.setBody(body);
            res = http.send(req);
            String jasonresp = res.getBody();
            Integer statusCode = (Integer) res.getstatuscode();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug('JsonRP---------'+jasonresp);
            System.debug('folderName---------'+folderName);
            System.debug('statusCode---------'+statusCode);
            fldrid = String.valueOf(jsonObj.get('id'));
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }

        BoxFolder newFolder = new BoxFolder(api, fldrid);
		BoxCollaboration.Info collabInfo1 = newFolder.collaborate('Nilesh.Gorle@varian.com', BoxCollaboration.Role.CO_OWNER);
		BoxCollaboration.Info collabInfo2 = newFolder.collaborate('Rohit.Andrade@varian.com', BoxCollaboration.Role.CO_OWNER);
		BoxCollaboration.Info collabInfo3 = newFolder.collaborate('Rakesh.Basani2@varian.com', BoxCollaboration.Role.CO_OWNER);
	}

    public void deleteSpecificFolder(String folderId) {
        BoxFolder dFolder = new BoxFolder(api, folderId);
        dFolder.deleteFolder(true);
    }
    
    public void collaborateUsers(String fldrid) {
		BoxFolder newFolder = new BoxFolder(api, fldrid);
		BoxCollaboration.Info collabInfo1 = newFolder.collaborate('Nilesh.Gorle@varian.com', BoxCollaboration.Role.CO_OWNER);
		BoxCollaboration.Info collabInfo2 = newFolder.collaborate('Rohit.Andrade@varian.com', BoxCollaboration.Role.CO_OWNER);
		BoxCollaboration.Info collabInfo3 = newFolder.collaborate('Rakesh.Basani2@varian.com', BoxCollaboration.Role.CO_OWNER);
    }

    public void deleteAllBoxFolder() {
        try {
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/0' ; // will delete all folder at 0 level
            req.setEndpoint(endpoint);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            System.debug('JasonP-----------------'+jasonresp);
            System.debug('item_collection-----------------'+jsonObj.get('item_collection'));
    
            Map<String, Object> entryListObj = (Map<String, Object>) jsonObj.get('item_collection');
            System.debug('entryListObj-----------------'+entryListObj);
			String folderId;
            for(String key:entryListObj.keyset()) {
                system.debug('------------'+entryListObj.get(key));
                if(key=='entries') {
                    List<Object> entryMap = (List<Object>) entryListObj.get(key);
                    for(Object k:entryMap) {
                        Map<String, Object> nodes = (Map<String, Object>) k;
                        folderId = (String) nodes.get('id');

						try {
							http http1=new http();
							HTTPResponse res1=new HttpResponse();
							HttpRequest req1 = new HttpRequest();
							req1.setMethod('DELETE');
							req1.setHeader('Content-Type', 'application/json');
							req1.setHeader('Authorization','Bearer '+accessToken); 
							String endpoint1 = 'https://api.box.com/2.0/folders/'+folderId+'?recursive=true';
							req1.setEndpoint(endpoint1);
							res1 = http.send(req1);
							String jasonresp1 = res1.getBody();
							Map<String, Object> jsonObj1 = (Map<String, Object>)JSON.deserializeUntyped(jasonresp1);
							System.debug(res1.getstatuscode());
							System.debug('JasonP-----------------'+jasonresp1);
							System.debug('folderId-----------------'+folderId);
						} catch(Exception e) {
							system.debug('@@@delete exception'+e.getMessage());
						}
                    }
                }
            }
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
    }
    
    public void deleteBoxFolder(String pfolderId) {
        try {
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/'+pfolderId ; // will delete all folder at 0 level
            req.setEndpoint(endpoint);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            System.debug('JasonP-----------------'+jasonresp);
            System.debug('item_collection-----------------'+jsonObj.get('item_collection'));
    
            Map<String, Object> entryListObj = (Map<String, Object>) jsonObj.get('item_collection');
            System.debug('entryListObj-----------------'+entryListObj);
			String folderId;
            for(String key:entryListObj.keyset()) {
                system.debug('------------'+entryListObj.get(key));
                if(key=='entries') {
                    List<Object> entryMap = (List<Object>) entryListObj.get(key);
                    for(Object k:entryMap) {
                        Map<String, Object> nodes = (Map<String, Object>) k;
                        folderId = (String) nodes.get('id');

						try {
							http http1=new http();
							HTTPResponse res1=new HttpResponse();
							HttpRequest req1 = new HttpRequest();
							req1.setMethod('DELETE');
							req1.setHeader('Content-Type', 'application/json');
							req1.setHeader('Authorization','Bearer '+accessToken); 
							String endpoint1 = 'https://api.box.com/2.0/folders/'+folderId+'?recursive=true';
							req1.setEndpoint(endpoint1);
							res1 = http.send(req1);
							String jasonresp1 = res1.getBody();
							Map<String, Object> jsonObj1 = (Map<String, Object>)JSON.deserializeUntyped(jasonresp1);
							System.debug(res1.getstatuscode());
							System.debug('JasonP-----------------'+jasonresp1);
							System.debug('folderId-----------------'+folderId);
						} catch(Exception e) {
							system.debug('@@@delete exception'+e.getMessage());
						}
                    }
                }
            }
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
    }
}

// BoxScript bs = new BoxScript();
// bs.deleteAllBoxFolder();
// bs.createFolder('PHI Data');
// bs.createFolder('Escalated Complaint Data');
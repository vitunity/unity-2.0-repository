public class Boxgetentrprsid {
	public static void AfterInsert(List<PHI_Log__c> philst,Map<Id,PHI_Log__c> phimap) {
        Map<String,String> mpph=new Map<String,String>();
        for(PHI_Log__c p:philst) {
            String phid=p.Id;
            mpph.put(phid,'');
        }
        if(mpph.size()>0) {
            system.debug('@@map'+mpph);
            getentusr(mpph);
        }
	}
    
	@future(callout=true)
	public static void getentusr(Map<String,String> idwisephi) {
        List<PHI_Log__c> newPHIUpdateList = new List<PHI_Log__c>();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
        String accessToken;
        String expires;
        String isUpdate;
        if(resultMap != null) {
        	accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
		}

        String fname;
        String lname;
        String fulname;
        String endpoint='https://api.box.com/2.0/users?filter_term=';
        String Email;

        List<String> convertedPhiIdList = new List<String>();
        for(String key:idwisephi.keyset()) {
            convertedPhiIdList.add(key);
        }
        List<PHI_Log__c> ophilst = Ltng_BoxAccess.fetchAllPHILogRecord(convertedPhiIdList);
        if(ophilst != null) {
            for(PHI_Log__c p:ophilst) {
                String uid=String.valueOf(p.OwnerId);
                for(User u:[select id,FirstName,LastName from User where Id=:uid]) {
                    fname = String.valueOf(u.FirstName);
                    lname = String.valueOf(u.LastName);
                    fulname = fname +' '+lname;
                    endpoint += fname;
                    system.debug('@@fname'+fname);
                }
			}
        }

		system.debug('accessToken'+accessToken);
		http http=new http();
		HTTPResponse res=new HttpResponse();
		HttpRequest req = new HttpRequest();
		req.setHeader('Authorization', 'Bearer '+accessToken);
		system.debug('@@@fullname'+fulname+'@@@endpoint'+endpoint);
		req.setEndpoint(endpoint);
		req.setMethod('GET');
		try {
			res = http.send(req);
			system.debug('@@@@statuscode'+res.getStatusCode());
			if (res.getStatusCode() == 200) {
				Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
				List<object> entries= (List<Object>) results.get('entries');
				for(Object l : entries) {
					Map<String,Object> mpParsed = (Map<String,Object>) l;
					String Fullname = String.valueOf(mpParsed.get('name'));
					String lastname = Fullname.substringAfter(' ');
					system.debug('@@@lastname'+lastname+'lname'+lname);
					if(lastname == lname) {
						Email=String.ValueOf(mpParsed.get('login'));
						system.debug('@@@emailid'+Email);
					}
				}
			}

			for(PHI_Log__c op:ophilst) {
                if(op.Owners_Box_Email__c != Email && op.Populated_UserID__c != true) {
					op.Owners_Box_Email__c = Email;
					op.Populated_UserID__c=TRUE;
                	newPHIUpdateList.add(op);
                }
			}
		} Catch(Exception e) {
			system.debug('@@exception'+e);
		}
        if(!newPHIUpdateList.isEmpty()) {
			system.debug('@@@@@ophilst'+newPHIUpdateList);
			update newPHIUpdateList;
        }
	}
}
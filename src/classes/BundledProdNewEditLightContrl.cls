/***************************************************************************
Author: Anshul Goel
Created Date: 02-Feb-2018
Project/Story/Inc/Task : Sales Lightning 
Description: 
This is a Controller Class for BundledProductNewEdit
Change Log:
*************************************************************************************/

public class BundledProdNewEditLightContrl 
{
    @AuraEnabled public Map<String, List<String>> controllingInfo{get;set;}
    @AuraEnabled public List<String> businessUnit{get;set;}
    @AuraEnabled public BundledProductProfilesLtCntrl.ProductProfile cp{get;set;}
    @AuraEnabled public String  cancelURL{get;set;}

   /***************************************************************************
    Description: 
    This init method is to used to fetch product profile data
    *************************************************************************************/
    @AuraEnabled
    public static BundledProdNewEditLightContrl initMethod(String recordId, BundledProductProfilesLtCntrl.ProductProfile cp)
    {
       BundledProdNewEditLightContrl bp = new BundledProdNewEditLightContrl();
       bp.cancelURL = URL.getSalesforceBaseUrl().toExternalForm().substringBeforeLast('--') + '.lightning.force.com/one/one.app#/sObject/Review_Products__c/list?filterName=Recent';
       bp.businessUnit =  new List<String>();
       bp.cp = new BundledProductProfilesLtCntrl.ProductProfile();
       if(null == cp && String.isnotempty(recordId))
       {
            Review_Products__c currentprodprofile = [select id, name, recordtypeid,recordtype.developername,OwnerId,Owner.Name,Product_Profile_Name__c,Product_Description__c,Version_No__c,Third_Party_Product__c,CreatedById,LastModifiedById,
                           Transition_Phase_Gate__c,Design_Output_Review__c,National_Language_Support__c,Product_Bundle__c,Business_Unit__c,GMDN_Code__c,Clearance_Family__c,Profit_Center__c,RecordType.Name,CreatedBy.Name,
                           LastModifiedBy.Name,Model_Part_Number__c,Profit_Center_Upgrade__c,Bundled_Filter_Logic__c from Review_Products__c where id = : recordId];

            bp.cp.name = currentprodprofile.name;
            bp.cp.name = currentprodprofile.name;
            bp.cp.id = currentprodprofile.id;
            bp.cp.ownerName = currentprodprofile.Owner.Name;
            bp.cp.prodProfileName = currentprodprofile.Product_Profile_Name__c;
            bp.cp.recordTypeName = currentprodprofile.RecordType.Name;
            bp.cp.recordTypeDeveloperName = currentprodprofile.recordtype.developername;
            bp.cp.prodDescription = currentprodprofile.Product_Description__c;
            bp.cp.createdByName = currentprodprofile.CreatedBy.Name;
            bp.cp.lastModifiedByName = currentprodprofile.LastModifiedBy.Name;
            bp.cp.bundledFilterLogic   = currentprodprofile.Bundled_Filter_Logic__c;
            bp.cp.createdById = currentprodprofile.CreatedById;
            bp.cp.ownerId = currentprodprofile.OwnerId;
            bp.cp.lastModifiedById = currentprodprofile.LastModifiedById;
            bp.cp.versionNo       =  currentprodprofile.Version_No__c;
            bp.cp.thirdPartyProduct       =  currentprodprofile.Third_Party_Product__c;
            bp.cp.businessUnit      =  currentprodprofile.Business_Unit__c;
            bp.cp.gmdnCode       =  currentprodprofile.GMDN_Code__c;
            bp.cp.clearanceFamily      =  currentprodprofile.Clearance_Family__c;
            bp.cp.modelPartNumber       =  currentprodprofile.Model_Part_Number__c;
            bp.cp.profitCenter      =  currentprodprofile.Profit_Center__c;
            bp.cp.profitCenterUpgrade      =  currentprodprofile.Profit_Center_Upgrade__c;     
        }

        bp.controllingInfo = new Map<String, List<String>>();

	    Schema.SObjectType objType = Schema.getGlobalDescribe().get('Review_Products__c');

	    Schema.DescribeSObjectResult describeResult = objType.getDescribe();
	    Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get('Business_Unit__c').getDescribe();
	    Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get('Clearance_Family__c').getDescribe();

	    List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
	    List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();

	    for(Schema.PicklistEntry currControllingValue : controllingValues)
	    {
	        System.debug('ControllingField: Label:' + currControllingValue.getLabel());
	        bp.controllingInfo.put(currControllingValue.getLabel(), new List<String>());
	    }

	    for(Schema.PicklistEntry currDependentValue : dependentValues)
	    {
	        String jsonString = JSON.serialize(currDependentValue);

	        MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);

	        String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();

	        System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);

	        Integer baseCount = 0;

	        for(Integer curr : hexString.getChars())
	        {
	            Integer val = 0;

	            if(curr >= 65)
	            {
	                val = curr - 65 + 10;
	            }
	            else
	            {
	                val = curr - 48;
	            }

	            if((val & 8) == 8)
	                bp.controllingInfo.get(controllingValues[baseCount + 0].getLabel()).add(currDependentValue.getLabel());
	            
	            else if((val & 4) == 4)
	                bp.controllingInfo.get(controllingValues[baseCount + 1].getLabel()).add(currDependentValue.getLabel());                    
	            
	            else if((val & 2) == 2)
	                bp.controllingInfo.get(controllingValues[baseCount + 2].getLabel()).add(currDependentValue.getLabel());                    
	            
	            else if((val & 1) == 1)
	                bp.controllingInfo.get(controllingValues[baseCount + 3].getLabel()).add(currDependentValue.getLabel());                    
	            
	            baseCount += 4;
	        }            
	    } 
        bp.businessUnit.addall( bp.controllingInfo.keyset());
        bp.businessUnit.add( 0,'--None--');
        return bp;
    }


  /***************************************************************************
    Description: 
    This init method is to save record
    *************************************************************************************/
    @AuraEnabled
    public static String saveRecord(String recordId,String cp, String businessId, String clearanceFamilyId)
    {
      Type resultType = Type.forName('BundledProductProfilesLtCntrl.ProductProfile');
	    BundledProductProfilesLtCntrl.ProductProfile bcp =(BundledProductProfilesLtCntrl.ProductProfile)JSON.deserialize(cp,resultType);        
       
      String returnId; 
      try
      {
         Review_Products__c rp = new Review_Products__c();
         rp.Product_Profile_Name__c = bcp.prodProfileName;
         rp.Version_No__c = bcp.versionNo;
         if(null == bcp.thirdPartyProduct)
         {
            rp.Third_Party_Product__c = false;
         }
         else
         {
         	  rp.Third_Party_Product__c = bcp.thirdPartyProduct;
         }
        
         rp.GMDN_Code__c = bcp.gmdnCode;
         rp.Profit_Center__c = bcp.profitCenter;
         rp.Model_Part_Number__c = bcp.modelPartNumber;
         rp.Profit_Center_Upgrade__c = bcp.profitCenterUpgrade;
         rp.Bundled_Filter_Logic__c =bcp.bundledFilterLogic;
         rp.Business_Unit__c = businessId;
         rp.Clearance_Family__c = clearanceFamilyId;

          if(String.isEmpty(recordId))
          {
            insert rp;
            returnId = rp.id;
          }
         else
         {
            rp.id =  bcp.id;
            update rp;
            returnId = bcp.id;
       }

      }
      catch(Exception e)
      {
      	returnId = 'Error: '+ e.getMessage();
      }
      return returnId ;
      return 'Error: test';
   }

   public class MyPickListInfo
   {
      public String validFor;
   }

}
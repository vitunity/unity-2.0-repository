/***************************************************************************
Author: Chandramouli Vegi
Created Date: 15-May-2017
Project/Story/Inc/Task : MCM Bundled Solution
Description: 
This is a controller for BundledProductProfiles Visual Force Page, This controller proposes the Bundle
Solution. This controller simulates the logic for per country clearance status of Bundle Prod Profile.
This class is used to associate more than one Product Profiles to Bundle Prod Profiles.

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
11-Aug-2017 - Chandramouli - 
10-Aug-2017 - Chandramouli - MCM Bundled Solution - modified logic calculation based on new object: product Profile 
                             Association
*************************************************************************************/
public class BundledProductProfiles 
{

    public Review_Products__c parentProductProfile{get;set;}
    public List<Review_Products__c> childProductProfiles{get; set;}
    public Review_Products__c currentprodprofile{get;set;}
    public Id selectedCountryId{get;set;}
    public String bundleClearanceLogic{get;set;}
    public static boolean stdpage;
    public static boolean editflag{get;set;}
    public static boolean newflag{get;set;}
    public static boolean viewflag{get;set;}
    public List<BundledProductProfileResult> bundledResults{get;set;}
    
    public Map<Integer,String> productProfileSequence;
    public Map<String,String> clearanceStatusMap;
    public Map<String,Id> parentClearanceIds;
    public static boolean bundledproductprofile {get;set;}
    public Map<String,String> parentClearanceStatus;
    public ApexPages.StandardController stdController{get;set;}
    public Boolean isCommercializationMember{get;set;}
    public Map<id,decimal> childprodprofwithseq{get;set;}
    public map<String,String> mapcntry2logic {get;set;}
    public Id parentprodprofid;
    public static boolean vferror = false;

    /***************************************************************************
    Description: 
    This is constructor for BundledProductProfiles Visual Force Page, This method is used to determine if the 
    Product Profile is a new record or existing record. It determines if the product profile is a Bundled or Standalone.
    Accordingly route to appropriate page.
    *************************************************************************************/
    public BundledProductProfiles(ApexPages.StandardController stdcontroller) {
        stdpage = false;
        editflag = false;
        newflag = false;
        viewflag = false;
        Map <String,String> parameters = new Map <String,String> ();
        //parameters = ApexPages.currentPage().getparameters();
        if (!Test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Id', 'Name', 'Bundled_Filter_Logic__c','Product_Profile_Name__c','Version_No__c','Third_Party_Product__c'});
        }
        this.parentProductProfile = (Review_Products__c)stdController.getRecord();
        this.stdController = stdController;

        //Trying to get the id from the URL, if ID doesn't exist then its a new record, RETURL existing its a Edit record, else its a view record.
        if(ApexPages.currentPage().getparameters().get('id') == null){
            newflag = true;
        } else if(ApexPages.currentPage().getparameters().get('retUrl') != null){
            editflag = true;
        } else {
            viewflag = true;
        }
        if (!newflag)
        {
            //Getting the product profile record fields.
            currentprodprofile = [select id, name, recordtypeid,recordtype.developername,ownerId,Product_Profile_Name__c,Product_Description__c,Version_No__c,Third_Party_Product__c,CreatedById,LastModifiedById,
                           Transition_Phase_Gate__c,Design_Output_Review__c,National_Language_Support__c,Product_Bundle__c,Business_Unit__c,GMDN_Code__c,Clearance_Family__c,Profit_Center__c,
                           Model_Part_Number__c,Profit_Center_Upgrade__c,Bundled_Filter_Logic__c from Review_Products__c where id = :parentProductProfile.id];
        }
        else
        {
            currentprodprofile = new Review_Products__c();
        }
        
        //if the current record is Bundled record type. 
        if (currentprodprofile != null && currentprodprofile.recordtype.developername == 'Bundled')
        {
            bundledproductprofile = true;
            childprodprofwithseq = new Map<id,decimal>();
            //Fetching all the Standalone product profiles associated to the Bundled Product profile and storing the seq number.
            for (Product_Profile_Association__c ppa : [select id,Standalone_Product_Profile__c, Bundle_Product_Profile__c, Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c = :parentProductProfile.Id order by Sequence_Number__c ASC] )
            {
                childprodprofwithseq.put(ppa.Standalone_Product_Profile__c, ppa.Sequence_Number__c);
            }
            //Storing the standalone product profile details.
            if (childprodprofwithseq.size() > 0)
            {
                childProductProfiles = [SELECT Id, Name, recordtypeid, recordtype.developername FROM Review_Products__c WHERE id in :childprodprofwithseq.keySet()];
            }
            else
            {
                childProductProfiles = new list<Review_Products__c>();
            }
            productProfileSequence = new Map<Integer,String>();
            clearanceStatusMap = new Map<String,String>();
            parentClearanceIds = new Map<String,Id>();
            parentClearanceStatus = new Map<String,String>();
            
            //Checking if the logged in user is a commercialization team member.
            isCommercializationMember = ![SELECT Id FROM GroupMember WHERE UserOrGroupId =:UserInfo.getUserId() AND Group.DeveloperName = 'VMS_Commercialization_Members'].isEmpty();
            
            updateParentClearanceEntriesMaps();
            
            bundleClearanceLogic = parentProductProfile.Bundled_Filter_Logic__c;
        }
        else
        {
            stdpage = true;
        }
    }
    
    /***************************************************************************
    Description:
    Overloading constructor for batch job calling
    ***************************************************************************/
    public BundledProductProfiles(Id reviewProductId) {
        this.parentProductProfile = [select id, name, recordtypeid,recordtype.developername,ownerId,Product_Profile_Name__c,Product_Description__c,Version_No__c,Third_Party_Product__c,CreatedById,LastModifiedById,
                                        Transition_Phase_Gate__c,Design_Output_Review__c,National_Language_Support__c,Product_Bundle__c,Business_Unit__c,GMDN_Code__c,Clearance_Family__c,Profit_Center__c,
                                        Model_Part_Number__c,Profit_Center_Upgrade__c,Bundled_Filter_Logic__c from Review_Products__c where id = :reviewProductId];
    }
    
    
    /***************************************************************************
    Description: 
    This method is to determine the clearance status of Bundled Prod Prof in Batch mode.
    *************************************************************************************/
    public void batchcode()
    {
        if (parentprodprofid != null)
        {   
            //Getting the Bundled product profile fields in Batch Mode.
            currentprodprofile = [select id, name, recordtypeid,recordtype.developername,ownerId,Product_Profile_Name__c,Product_Description__c,Version_No__c,Third_Party_Product__c,CreatedById,LastModifiedById,
                               Transition_Phase_Gate__c,Design_Output_Review__c,National_Language_Support__c,Product_Bundle__c,Business_Unit__c,GMDN_Code__c,Clearance_Family__c,Profit_Center__c,
                               Model_Part_Number__c,Profit_Center_Upgrade__c,Bundled_Filter_Logic__c from Review_Products__c where id = :parentprodprofid];
            if (currentprodprofile != null && currentprodprofile.recordtype.developername == 'Bundled')
            {
                bundledproductprofile = true;
                childprodprofwithseq = new Map<id,decimal>();
                for (Product_Profile_Association__c ppa : [select id,Standalone_Product_Profile__c, Bundle_Product_Profile__c, Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c = :parentprodprofid order by Sequence_Number__c ASC] )
                {
                    childprodprofwithseq.put(ppa.Standalone_Product_Profile__c, ppa.Sequence_Number__c);
                }
                //Storing the standalone product profile details.
                if (childprodprofwithseq.size() > 0)
                {
                    childProductProfiles = [SELECT Id, Name, recordtypeid, recordtype.developername FROM Review_Products__c WHERE id in :childprodprofwithseq.keySet()];
                }
                else
                {
                    childProductProfiles = new List<Review_Products__c>();
                }
                productProfileSequence = new Map<Integer,String>();
                clearanceStatusMap = new Map<String,String>();
                parentClearanceIds = new Map<String,Id>();
                parentClearanceStatus = new Map<String,String>();
                
                //Checking if the logged in user is a commercialization team member.
                isCommercializationMember = ![SELECT Id FROM GroupMember WHERE UserOrGroupId =:UserInfo.getUserId() AND Group.DeveloperName = 'VMS_Commercialization_Members'].isEmpty();
                
                
                bundleClearanceLogic = parentProductProfile.Bundled_Filter_Logic__c;
            }
        }
    }
    /***************************************************************************
    Description: 
    This method is to get all the countries of the Bundled Product Profile
    *************************************************************************************/
    public List<SelectOption> getCountries(){
        List<SelectOption> countryOptions = new List<SelectOption>{new SelectOption('', '--NONE--')};
        List<Country__c> countries = [SELECT Id,Name FROM Country__c ORDER BY Name ASC];
        for(Country__c country: countries){
            countryOptions.add(new SelectOption(country.Id, country.Name));
        }
        return countryOptions;
    }

    /***************************************************************************
    Description: 
    This method contains the logic to determine the page redirection depending on the record's RecordType
    *************************************************************************************/
    public PageReference redirecttostandardpage()
    {

        Schema.DescribeSObjectResult R = Review_Products__c.SObjectType.getDescribe();
        Map<String,Schema.RecordTypeInfo> RT = R.getRecordTypeInfosByName();
        if (currentprodprofile != null && currentprodprofile.recordtype.developername != 'Bundled' && viewflag)
        {
            PageReference pageRef = new PageReference('/' + currentprodprofile.id);
            pageRef.getParameters().put('nooverride','1');
            pageRef.setRedirect(true);
            return pageRef;
        }
        else if (currentprodprofile != null && currentprodprofile.recordtype.developername != 'Bundled' && editflag)
        {
            PageReference pageRef = new PageReference('/'+currentprodprofile.id+'/e?retURL=%2F'+currentprodprofile.id);
            pageRef.getParameters().put('nooverride','1');
            pageRef.setRedirect(true);
            return pageRef;
        } 
        else if (newflag && RT.get('Master').isDefaultRecordTypeMapping())
        {
            String Name1 = 'Auto Generated';
            PageReference pageRef = new PageReference('/a0A' +'/e?retURL=%2F' + '?&Name=' +name1);
            pageRef.getParameters().put('nooverride','1');
            pageRef.setRedirect(true);
            return pageRef;
        }  
        else
        {
            return null;
        }
    } 


    /***************************************************************************
    Description: 
    This method is to save the record.
    *************************************************************************************/
    public PageReference doSave() 
    {
         Review_Products__c prdprofile = new Review_Products__c();
         boolean newprdpage = false;
         if(ApexPages.currentPage().getparameters().get('id') == null){
            newprdpage = true;
        }
        //Getting the record fields from VF page and creating a new object reference to save.
        prdprofile = (Review_Products__c)stdController.getRecord();
          
        prdprofile.Product_Profile_Name__c=currentprodprofile.Product_Profile_Name__c;
        //prdprofile.Product_Description__c=prdprofile.Product_Description__c;
        prdprofile.Version_No__c=currentprodprofile.Version_No__c;
        prdprofile.Third_Party_Product__c=currentprodprofile.Third_Party_Product__c;
        prdprofile.Business_Unit__c=currentprodprofile.Business_Unit__c;
        prdprofile.GMDN_Code__c=currentprodprofile.GMDN_Code__c;
        prdprofile.Clearance_Family__c=currentprodprofile.Clearance_Family__c;
        prdprofile.Profit_Center__c=currentprodprofile.Profit_Center__c;
        prdprofile.Model_Part_Number__c = currentprodprofile.Model_Part_Number__c;
        prdprofile.Profit_Center_Upgrade__c = currentprodprofile.Profit_Center_Upgrade__c;
        prdprofile.Bundled_Filter_Logic__c = currentprodprofile.Bundled_Filter_Logic__c;
        prdprofile.Transition_Phase_Gate__c = currentprodprofile.Transition_Phase_Gate__c;
        prdprofile.Design_Output_Review__c = currentprodprofile.Design_Output_Review__c;
        prdprofile.National_Language_Support__c = currentprodprofile.National_Language_Support__c;


        ApexPages.StandardController stdControllernew = new ApexPages.StandardController(prdprofile); 
        //try
        //{
            database.UpsertResult results = Database.upsert(prdprofile,false);
            system.debug(vferror);
            //return new PageReference('/' + prdprofile.id);
            
        //}
        //catch(Exception e){
        //     system.debug('in catch');
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
        //    return null;      
        //}
            /*if (vferror)
            {
                return null;
            }
            else
            {*/
                return new PageReference('/' + prdprofile.id);
           // }
        //ystem.debug('out of catch');
        //return null;
        //return new PageReference('/' + prdprofile.id);
    }

    /***************************************************************************
    Description: 
    This method is to save & create a new record.
    *************************************************************************************/
    public PageReference SaveNew()
    {
        PageReference pr = doSave();
        if(pr != null){
            
            PageReference prnew = new PageReference('/apex/BundledProductnewedit');
            prnew.setRedirect(true);
            return prnew;
        }
        return pr;  
    }  

    /***************************************************************************
    Description: 
    This method is to edit the record.
    *************************************************************************************/
    public PageReference editbundledprof(){
        pagereference pg = new Pagereference('/apex/BundledProductnewedit');
        pg.getParameters().put('nooverride','1');
        pg.getParameters().put('retURL','/'+currentprodprofile.Id);
        pg.getParameters().put('id',currentprodprofile.Id);
        pg.setredirect(true);
        return pg;
    }

    /***************************************************************************
    Description: 
    This method is to assign stand alone prod profiles to Bundled prod Profile.
    *************************************************************************************/
    public pagereference assignchildpp(){
        PageReference pageRef = new PageReference('/a8c' + '/e?CF00N0n000000Qlye='+currentprodprofile.Name+'&CF00N0n000000Qlye_lkid='+currentprodprofile.id+'&retURL=%2F'+currentprodprofile.id);
        pageRef.getParameters().put('nooverride','1');
        pageRef.setRedirect(true);
        return pageRef;
    }

    /***************************************************************************
    Description: 
    This method is to prepare the map to store bundled clearance entries.
    *************************************************************************************/
    public void updateParentClearanceEntriesMaps(){
        for(Input_Clearance__c clearance : [SELECT Id, Country__r.Name, Clearance_Status__c FROM Input_Clearance__c WHERE Review_Product__c =:parentProductProfile.Id]){
            parentClearanceIds.put(clearance.Country__r.Name, clearance.Id);
            parentClearanceStatus.put(clearance.Country__r.Name, clearance.Clearance_Status__c);
        }
    }
    
    /***************************************************************************
    Description: 
    This method is to retrieve the clearance status of standalone prod profiles depending on the country selected by User.
    *************************************************************************************/
    public List<ProductProfileClearance> childClearanceEntries{
        get{
            List<Input_Clearance__c> clearanceEntryForms = new list<Input_Clearance__c>();
            if(childClearanceEntries == null) {
                childClearanceEntries = new  List<ProductProfileClearance>();
                //Storing the standalone prod profiles clearance entries which are associated to bundled product profile
                if (selectedCountryId != null && childProductProfiles.size() > 0)
                {
                    clearanceEntryForms = [
                        SELECT Id, Clearance_Status__c, Review_Product__r.Name, Review_Product__c, Review_Product__r.Product_Profile_Name__c
                        FROM Input_Clearance__c
                        WHERE Review_Product__c IN:childProductProfiles
                        AND Country__c =:selectedCountryId
                        AND Country__c != null
                    ];
                }
                if(childprodprofwithseq.size() > 0)
                {
                    for(Input_Clearance__c clearanceEntryForm : clearanceEntryForms){
                        childClearanceEntries.add(
                            new ProductProfileClearance(
                                Integer.valueOf(childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)),
                                clearanceEntryForm.Review_Product__r.Name, 
                                clearanceEntryForm.Review_Product__r.Product_Profile_Name__c,
                                clearanceEntryForm.Review_Product__c, 
                                clearanceEntryForm.Clearance_Status__c,
                                clearanceEntryForm.Id
                            )
                        );
                        productProfileSequence.put(Integer.valueOf(childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)), clearanceEntryForm.Review_Product__r.Name);
                    }
                }
            }
            childClearanceEntries.sort();
            return childClearanceEntries;
        }
        set;
    }
    
    /***************************************************************************
    Description: 
    This method is to Reset the clearanceEntries to null after changing country
    *************************************************************************************/
    public void updateChildClearanceEntries(){
        childClearanceEntries = null;
    }
    
    /***************************************************************************
    Description: 
    This method is to simulate the per country status on button click based on the clearance filter logic entered by USER.
    *************************************************************************************/
    public void simulate(){
        //resultText = 'changed';
        if(bundleClearanceLogic == ''){
            ApexPages.addmessage(
                new ApexPages.message(ApexPages.severity.ERROR,'Please enter clearance filter logic.')
            );
        }else{
            buildBundledProductProfileResults();
        }
    }
    
    /***************************************************************************
    Description: 
    Updates clearance logic values seperated by AND operator
    *************************************************************************************/
    public void andALL(){
        buildClearanceLogic('AND');
        resultText = '';
    }
    
    /***************************************************************************
    Description: 
    Updates clearance logic values seperated by OR operator
    *************************************************************************************/
    public void orALL(){
        buildClearanceLogic('OR');
        resultText = '';
    }
    

    /***************************************************************************
    Description: 
    This method is to update the bundled clearance entries.
    *************************************************************************************/
    public PageReference updateParentClearanceEntries(){
        List<Input_Clearance__c> parentClearanceEntries = new List<Input_Clearance__c>();
        //currentprodprofile.Bundled_Filter_Logic__c = bundleClearanceLogic;
        this.parentProductProfile.Bundled_Filter_Logic__c = bundleClearanceLogic;
        
        for(BundledProductProfileResult bundledresult : bundledResults){
            if(bundledresult.clearanceId!=null){
                parentClearanceEntries.add(
                    new Input_Clearance__c(
                        Id= bundledresult.clearanceId,
                        Clearance_Status__c = bundledresult.clearanceStatus
                    )
                );
            }
        }
        try{
            Database.saveresult sr = database.update(this.parentProductProfile,false);
            system.debug(sr);
            if (vferror)
            {
                return null;
            }
            else
            {
                update parentClearanceEntries;
                updateParentClearanceEntriesMaps();
                return new PageReference('/' + this.parentProductProfile.id);
            }
        }catch(Exception e){
            System.debug('-----Exception Occured'+e.getMessage());
            return null;
        }
    }
    
    /***************************************************************************
    Description: 
    This method is to bulid the clearance entry logic.
    *************************************************************************************/
    public void buildClearanceLogic(String operator){
        bundleClearanceLogic = '';
        for(ProductProfileClearance clearanceEntry : childClearanceEntries){
            bundleClearanceLogic += clearanceEntry.sequenceNumber + ' '+operator+' ';
        }
        bundleClearanceLogic = bundleClearanceLogic.removeEnd(' '+operator+' ');
    }
    
    /***************************************************************************
    Description: 
    This method is to check if there is any change in the status.
    *************************************************************************************/
    public String resultText{
        get{
            if(resultText == 'changed'){
                //resultText = 'TESTING';
            }
            return resultText;
        }
        set;
    }
    
    /***************************************************************************
    Description: 
    This method is key to determine the percountry status depending on the logic.
    *************************************************************************************/
    public void buildBundledProductProfileResults(){
            bundledResults = new List<BundledProductProfileResult>{};
            clearanceStatusMap = new Map<String,String>();
            system.debug('***CM childProductProfiles : '+childProductProfiles);
            List<Input_Clearance__c> clearanceEntryForms = [
                SELECT Id, Clearance_Status__c, Review_Product__r.Name, Review_Product__c, Review_Product__r.Product_Profile_Name__c,
                Country__c,Country__r.Name
                FROM Input_Clearance__c
                WHERE Review_Product__c IN:childProductProfiles
                AND Country__c !=null
            ];
            Set<String> countries = new Set<String>();
            //Depending on the clearance status of the standalone prod profiles, setting the value to True/False.
            for(Input_Clearance__c clearance : clearanceEntryForms){
                system.debug('clearance : '+clearance);
                system.debug('clearance.Review_Product__r.Name : '+clearance.Review_Product__r.Name);
                system.debug('clearance.Country__r.Name : '+clearance.Country__r.Name);
                if(clearance.Clearance_Status__c == 'Permitted'){
                    clearanceStatusMap.put(clearance.Review_Product__r.Name+''+clearance.Country__r.Name, 'true');
                }else{
                    clearanceStatusMap.put(clearance.Review_Product__r.Name+''+clearance.Country__r.Name, 'false');
                }
                countries.add(clearance.Country__r.Name);
            }
            List<String> countriesList = new List<String>(countries);
            countriesList.sort();
            mapcntry2logic = new map<String,String>();
            for(String country : countriesList){
                system.debug('country: '+country);
                system.debug(parentClearanceIds.get(country));
                system.debug(buildFilterLogic(country));
                system.debug(parentClearanceStatus.get(country));
                bundledResults.add(new BundledProductProfileResult(parentClearanceIds.get(country), country, buildFilterLogic(country), parentClearanceStatus.get(country)));
            }
    }
    
    /***************************************************************************
    Description: 
    This method is to build the filter logic for the stand alone product profiles.
    *************************************************************************************/
    public String buildFilterLogic(String country){
        String filterLogic = bundleClearanceLogic;
        
        Boolean validResult = true;
        for(Integer seqNumber : productProfileSequence.keySet()){
            if(clearanceStatusMap.containsKey(productProfileSequence.get(seqNumber)+''+country)){
                String result = clearanceStatusMap.get(productProfileSequence.get(seqNumber)+''+country);
                filterLogic = filterLogic.replaceAll(String.valueOf(seqNumber),result);                
                if(mapcntry2logic.size() > 0 )
                {
                    if (mapcntry2logic.containsKey(country))
                    {
                        mapcntry2logic.put(country,filterlogic); 
                    }
                    else
                    {
                        mapcntry2logic.put(country,filterlogic);
                    }
                }
                else
                {
                    mapcntry2logic.put(country,filterlogic);
                }
            }else{
                validResult = false;
            }
        }
        if(validResult){
            return filterLogic;
        }else{
            return '';
        }
    }
    
    /***************************************************************************
    Description: 
    This is a wrapper class with comparable interface used to sort the list of child clearance entries as per seq num.
    *************************************************************************************/
    public class ProductProfileClearance implements comparable{
        public Integer sequenceNumber{get;set;}
        public String productProfile{get;set;}
        public String productProfileId{get;set;}
        public String clearanceStatus{get;set;} 
        public String profileName{get;set;}
        public String entryId{get;set;}
        public String seqnum{get;set;}
        public ProductProfileClearance(Integer sequenceNumber, String productProfile, String profileName, String productProfileId, String clearanceStatus, Id entryId){
            this.sequenceNumber = sequenceNumber;
            this.productProfile = productProfile;
            this.productProfileId = productProfileId;
            this.clearanceStatus = clearanceStatus;
            this.profileName = profileName;
            this.entryId = entryId;
            this.seqnum = String.valueOf(sequenceNumber);
        }
        public Integer compareTo(Object ObjToCompare) {
            return seqnum.CompareTo(((ProductProfileClearance)ObjToCompare).seqnum);
        }
    }
    
    /***************************************************************************
    Description: 
    This is a wrapper class which stores the clearance entry ID, Country, Status.
    *************************************************************************************/
    public class BundledProductProfileResult{
        public Id clearanceId{get;set;}
        public String filterLogic{get;set;}
        public String clearanceStatus{get;set;}
        public String country{get;set;}
        
        public BundledProductProfileResult(Id clearanceId, String country, String filterLogic, String clearanceStatus){
            this.clearanceId = clearanceId;
            this.filterLogic = filterLogic;
            this.clearanceStatus = clearanceStatus;
            this.country = country;
        }
    }
    
     /*
     * for lightning
     */
    public boolean getIsLightning(){
        User userObj = [SELECT Id, UserPreferencesLightningExperiencePreferred from User where Id =: UserInfo.getUserId() limit 001];
        return userObj.UserPreferencesLightningExperiencePreferred;
    }
}
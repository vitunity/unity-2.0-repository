/***************************************************************************
Author: Anshul Goel
Created Date: 02-Feb-2018
Project/Story/Inc/Task : Sales Lightning 
Description: 
This is a Controller Class for BundledProductProfile
Change Log:
*************************************************************************************/

public class BundledProductProfilesLtCntrl 
{
    @AuraEnabled public Review_Products__c parentProductProfile{get;set;}
    @AuraEnabled public List<Review_Products__c> childProductProfiles{get; set;}
    @AuraEnabled public ProductProfile cp{get;set;}
    @AuraEnabled  public List<CountryWrapper> countries{get;set;}
    @AuraEnabled  public  List<ProductProfileClearance> childClearanceEntries{get;set;}
    @AuraEnabled public String bundleClearanceLogic{get;set;}
    @AuraEnabled  public  List<InputClearance> inputClearance{get;set;}
    @AuraEnabled  public  List<StandalonePPA> stanPPA{get;set;}
    @AuraEnabled public Id selectedCountryId{get;set;}
    @AuraEnabled public boolean  viewAllIC{get;set;}
    @AuraEnabled public boolean  viewAllPPA{get;set;}
    @AuraEnabled public String  clearanceURL{get;set;}
    @AuraEnabled public String  ppaURL{get;set;}
    @AuraEnabled public String  delURL{get;set;}
    @AuraEnabled public Map<Integer,String> productProfileSequence;
    @AuraEnabled public Map<id,decimal> childprodprofwithseq{get;set;}
    @AuraEnabled public List<BundledProductProfileResult> bundledResults{get;set;}
    @AuraEnabled public Map<String,String> clearanceStatusMap;
    @AuraEnabled public map<String,String> mapcntry2logic {get;set;}
    @AuraEnabled public Map<String,String> parentClearanceStatus;
    @AuraEnabled public Map<String,Id> parentClearanceIds;
    @AuraEnabled public Boolean clearanceLogicResult {get;set;}
    @AuraEnabled public Boolean isCommercializationMember{get;set;}

    /***************************************************************************
    Description: 
    This init method is to used to fetch product profile data
    *************************************************************************************/
    @AuraEnabled
    public static BundledProductProfilesLtCntrl initMethod(String recordId)
    {
        //create class instance
        BundledProductProfilesLtCntrl bp = new BundledProductProfilesLtCntrl();
        bp.parentClearanceStatus = new Map<String,String>();
        bp.parentClearanceIds = new Map<String,Id>();
        bp.bundledResults = new List<BundledProductProfileResult>{};
        bp.clearanceStatusMap = new Map<String,String>();

        bp.viewAllIC = false;
        bp.clearanceURL =  URL.getSalesforceBaseUrl().toExternalForm().substringBeforeLast('--') + '.lightning.force.com/one/one.app?source=aloha#/sObject/'+recordId+'/rlName/Market_Clearance_Evidence__r/view';
        bp.ppaURL = URL.getSalesforceBaseUrl().toExternalForm().substringBeforeLast('--') + '.lightning.force.com/one/one.app?source=aloha#/sObject/'+recordId+'/rlName/Bundle_vs_Product_Profile_Associations__r/view';
        bp.delURL = URL.getSalesforceBaseUrl().toExternalForm().substringBeforeLast('--') + '.lightning.force.com/one/one.app#/sObject/Review_Products__c/list?filterName=Recent';

        if(String.isnotblank(recordId))
        {
            Review_Products__c currentprodprofile = [select id, name, recordtypeid,recordtype.developername,OwnerId,Owner.Name,Product_Profile_Name__c,Product_Description__c,Version_No__c,Third_Party_Product__c,CreatedById,LastModifiedById,
                           Transition_Phase_Gate__c,Design_Output_Review__c,National_Language_Support__c,Product_Bundle__c,Business_Unit__c,GMDN_Code__c,Clearance_Family__c,Profit_Center__c,RecordType.Name,CreatedBy.Name,
                           LastModifiedBy.Name,Model_Part_Number__c,Profit_Center_Upgrade__c,Bundled_Filter_Logic__c from Review_Products__c where id = : recordId];

            bp.cp = new ProductProfile();
            bp.cp.name = currentprodprofile.name;
            bp.cp.id = currentprodprofile.id;
            bp.cp.ownerName = currentprodprofile.Owner.Name;
            bp.cp.prodProfileName = currentprodprofile.Product_Profile_Name__c;
            bp.cp.recordTypeName = currentprodprofile.RecordType.Name;
            bp.cp.recordTypeDeveloperName = currentprodprofile.recordtype.developername;
            bp.cp.prodDescription = currentprodprofile.Product_Description__c;
            bp.cp.createdByName = currentprodprofile.CreatedBy.Name;
            bp.cp.lastModifiedByName = currentprodprofile.LastModifiedBy.Name;
            bp.cp.bundledFilterLogic   = currentprodprofile.Bundled_Filter_Logic__c;
            bp.cp.createdById = currentprodprofile.CreatedById;
            bp.cp.ownerId = currentprodprofile.OwnerId;
            bp.cp.lastModifiedById = currentprodprofile.LastModifiedById;
            bp.cp.versionNo       =  currentprodprofile.Version_No__c;
            bp.cp.thirdPartyProduct       =  currentprodprofile.Third_Party_Product__c;
            bp.cp.businessUnit      =  currentprodprofile.Business_Unit__c;
            bp.cp.gmdnCode       =  currentprodprofile.GMDN_Code__c;
            bp.cp.clearanceFamily      =  currentprodprofile.Clearance_Family__c;
            bp.cp.modelPartNumber       =  currentprodprofile.Model_Part_Number__c;
            bp.cp.profitCenter      =  currentprodprofile.Profit_Center__c;
            bp.cp.profitCenterUpgrade      =  currentprodprofile.Profit_Center_Upgrade__c;
            bp.parentProductProfile = currentprodprofile;
            bp.bundleClearanceLogic = bp.parentProductProfile.Bundled_Filter_Logic__c;
        }
       
        List<Country__c>  countries= [SELECT Id,Name FROM Country__c ORDER BY Name ASC];
        bp.countries = new List<CountryWrapper>();
        bp.countries.add(new CountryWrapper('None','--None--'));
        for(Country__c country: countries)
        {
           bp.countries.add(new CountryWrapper(country.Id, country.Name));
        }
        
        if (bp.cp != null && bp.cp.recordTypeDeveloperName == 'Bundled')
        {
            bp.childprodprofwithseq = new Map<id,decimal>();
            for (Product_Profile_Association__c ppa : [select id,Standalone_Product_Profile__c, Bundle_Product_Profile__c, Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c = :recordId order by Sequence_Number__c ASC] )
            {
                bp.childprodprofwithseq.put(ppa.Standalone_Product_Profile__c, ppa.Sequence_Number__c);
            }
            
            if (bp.childprodprofwithseq.size() > 0)
            {
                bp.childProductProfiles = [SELECT Id, Name, recordtypeid, recordtype.developername FROM Review_Products__c WHERE id in : bp.childprodprofwithseq.keySet()];
            }
         
            bp.productProfileSequence = new Map<Integer,String>();
            bp = refreshRelatedList(recordId,JSON.serialize(bp));

        }
        updateParentClearanceEntriesMaps(bp);

        //Checking if the logged in user is a commercialization team member.
        bp.isCommercializationMember = ![SELECT Id FROM GroupMember WHERE UserOrGroupId =:UserInfo.getUserId() AND Group.DeveloperName = 'VMS_Commercialization_Members'].isEmpty();
        
        return bp;
    }

    /***************************************************************************
    Description: 
    This method is to prepare the map to store bundled clearance entries.
    *************************************************************************************/
    @AuraEnabled
    public static void updateParentClearanceEntriesMaps(BundledProductProfilesLtCntrl bp){
        for(Input_Clearance__c clearance : [SELECT Id, Country__r.Name, Clearance_Status__c FROM Input_Clearance__c WHERE Review_Product__c =:bp.parentProductProfile.Id]){
            bp.parentClearanceIds.put(clearance.Country__r.Name, clearance.Id);
            bp.parentClearanceStatus.put(clearance.Country__r.Name, clearance.Clearance_Status__c);
        }
    }

  /***************************************************************************
    Description: 
    This init method is to used to refresh related list 
    *************************************************************************************/
    @AuraEnabled
    public Static  BundledProductProfilesLtCntrl refreshRelatedList(String recordId,String obj)
    {
      
      Type resultType = Type.forName('BundledProductProfilesLtCntrl');
          BundledProductProfilesLtCntrl bp =(BundledProductProfilesLtCntrl)JSON.deserialize(obj,resultType); 
      List<Input_Clearance__c> icList =  [select id, Name,Clearance_Entry_Form_Name__c, Country__c, Country__r.name, Clearance_Status__c,Clearance_Date__c, Expected_Clearance_Date__c from Input_Clearance__c where Review_Product__c =: recordId  order by lastmodifieddate desc limit 7] ;
            
      Integer count = 0;
      bp.inputClearance = new List<InputClearance>();

      if(icList != null && icList.size()>0)
      {
         if(icList.size()>6)
            bp.viewAllIC = true;

          for(Input_Clearance__c ic  :  icList)
          {
             InputClearance localIc = new InputClearance();
             localIc.id = ic.id;
             localIc.name = ic.Name;
             localIc.cleranceEntryFormName = ic.Clearance_Entry_Form_Name__c;
             localIc.countryId = ic.Country__c;
             localIc.countryName = ic.Country__r.name;
             localIc.clearanceStatus = ic.Clearance_Status__c;
             localIc.clearanceDate = ic.Clearance_Date__c;
             localIc.expectedClearanceDate = ic.Expected_Clearance_Date__c;
             bp.inputClearance.add(localIc);
             count++;
             if(count == 6)
                break;
          }  
      }

      list<Product_Profile_Association__c> ppAList =  [select id,name,Standalone_Product_Profile__r.Name, Standalone_Product_Profile__c,Sequence_Number__c  from Product_Profile_Association__c where Bundle_Product_Profile__c =: recordId  order by lastmodifieddate desc limit 7];
      count = 0;
      bp.stanPPA = new List<StandalonePPA>();
      
      if(ppAList != null && ppAList.size()>0)
      {

         if(ppAList.size()>6)
            bp.viewAllPPA = true;

          for(Product_Profile_Association__c  ppa  :  ppAList)
          {
             StandalonePPA localPPA= new StandalonePPA();
             localPPA.id = ppa.id;
             localPPA.name = ppa.Name;
             localPPA.standalonePPAId = ppa.Standalone_Product_Profile__c;
             localPPA.standalonePPAName = ppa.Standalone_Product_Profile__r.Name;
             localPPA.seqNumber = ppa.Sequence_Number__c;
             bp.stanPPA.add(localPPA);
             count++;
             if(count == 6)
                break;
          }  
      }
      return bp;
    }
   
  /***************************************************************************
    Description: 
    This init method is to used to delete record
    *************************************************************************************/
   @AuraEnabled
   public Static  BundledProductProfilesLtCntrl deleteRecord(String recordId,String obj, String ObjName, String delId)
   {
      String queryString = 'Select id from '+ ObjName + ' where Id =:delId';
      delete (Database.query(queryString));
      return refreshRelatedList(recordId,obj);
   }

  /***************************************************************************
    Description: 
    This init method is to used to delete review product
    *************************************************************************************/
   @AuraEnabled
   public static String deletePP(String recordId)
   {
      try
      {
        delete([select id from Review_Products__c where id=:recordId]);
      }
      catch(Exception e)
      {
        return e.getMessage();
      }
      return 'Success';
   }

  /***************************************************************************
    Description: 
    This init method is to used to fetch clearance status
    *************************************************************************************/
   @AuraEnabled
   public static BundledProductProfilesLtCntrl fetchClearanceStatus  (String obj)
   {
      Type resultType = Type.forName('BundledProductProfilesLtCntrl');
      BundledProductProfilesLtCntrl bp =(BundledProductProfilesLtCntrl)JSON.deserialize(obj,resultType);

      List<Input_Clearance__c> clearanceEntryForms = new list<Input_Clearance__c>();
      bp.childClearanceEntries = new  List<ProductProfileClearance>();
      
      if (bp.selectedCountryId != null && bp.childProductProfiles.size() > 0)
      {
          clearanceEntryForms =   [
                                    SELECT Id, Clearance_Status__c, Review_Product__r.Name, Review_Product__c, Review_Product__r.Product_Profile_Name__c
                                    FROM Input_Clearance__c
                                    WHERE Review_Product__c IN: bp.childProductProfiles
                                    AND Country__c =:bp.selectedCountryId
                                    AND Country__c != null
                                ];
      }
      if(bp.childprodprofwithseq.size() > 0)
      {
          for(Input_Clearance__c clearanceEntryForm : clearanceEntryForms)
          {
              bp.childClearanceEntries.add(
                                        new ProductProfileClearance(
                                            Integer.valueOf(bp.childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)),
                                            clearanceEntryForm.Review_Product__r.Name, 
                                            clearanceEntryForm.Review_Product__r.Product_Profile_Name__c,
                                            clearanceEntryForm.Review_Product__c, 
                                            clearanceEntryForm.Clearance_Status__c,
                                            clearanceEntryForm.Id
                                        )
                                    );
              bp.productProfileSequence.put(Integer.valueOf(bp.childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)), clearanceEntryForm.Review_Product__r.Name);
          }
      }
      
      bp.childClearanceEntries.sort();
      return bp;
   }


    /***************************************************************************
    Description: 
    This method is key to determine the percountry status depending on the logic.
    *************************************************************************************/
    @AuraEnabled
    public static void buildBundledProductProfileResults(BundledProductProfilesLtCntrl bp){
        bp.bundledResults.clear();
        bp.clearanceStatusMap.clear();
        system.debug('***CM childProductProfiles : '+bp.childProductProfiles);
        List<Input_Clearance__c> clearanceEntryForms = [
            SELECT Id, Clearance_Status__c, Review_Product__r.Name, Review_Product__c, Review_Product__r.Product_Profile_Name__c,
            Country__c,Country__r.Name
            FROM Input_Clearance__c
            WHERE Review_Product__c IN:bp.childProductProfiles
            AND Country__c !=null
        ];
        Set<String> countries_list = new Set<String>();
        //Depending on the clearance status of the standalone prod profiles, setting the value to True/False.
        for(Input_Clearance__c clearance : clearanceEntryForms){
            system.debug('clearance : '+clearance);
            system.debug('clearance.Review_Product__r.Name : '+clearance.Review_Product__r.Name);
            system.debug('clearance.Country__r.Name : '+clearance.Country__r.Name);
            if(clearance.Clearance_Status__c == 'Permitted'){
                bp.clearanceStatusMap.put(clearance.Review_Product__r.Name+''+clearance.Country__r.Name, 'true');
            }else{
                bp.clearanceStatusMap.put(clearance.Review_Product__r.Name+''+clearance.Country__r.Name, 'false');
            }
            countries_list.add(clearance.Country__r.Name);
        }
        List<String> countriesList = new List<String>(countries_list);
        countriesList.sort();
        bp.mapcntry2logic = new map<String,String>();
        for(String country : countriesList){
            system.debug('country: '+country);
            system.debug(bp.parentClearanceIds.get(country));
            system.debug(buildFilterLogic(country, bp));
            system.debug(bp.parentClearanceStatus.get(country));
            bp.bundledResults.add(new BundledProductProfileResult(bp.parentClearanceIds.get(country), country, buildFilterLogic(country, bp), bp.parentClearanceStatus.get(country)));
            System.debug('=========**bp.bundledResults**========'+bp.bundledResults);
        }
    }

    /***************************************************************************
    Description: 
    This method is to build the filter logic for the stand alone product profiles.
    *************************************************************************************/
    @AuraEnabled
    public static String buildFilterLogic(String country, BundledProductProfilesLtCntrl bp){
        String filterLogic = bp.cp.bundledFilterLogic;

        Boolean validResult = true;
        for(Integer seqNumber : bp.productProfileSequence.keySet()){
            if(bp.clearanceStatusMap.containsKey(bp.productProfileSequence.get(seqNumber)+''+country)){
                String result = bp.clearanceStatusMap.get(bp.productProfileSequence.get(seqNumber)+''+country);
                filterLogic = filterLogic.replaceAll(String.valueOf(seqNumber),result);                
                if(bp.mapcntry2logic.size() > 0 )
                {
                    if (bp.mapcntry2logic.containsKey(country))
                    {
                        bp.mapcntry2logic.put(country,filterlogic); 
                    }
                    else
                    {
                        bp.mapcntry2logic.put(country,filterlogic);
                    }
                }
                else
                {
                    bp.mapcntry2logic.put(country,filterlogic);
                }
            }else{
                validResult = false;
            }
        }
        if(validResult){
            return filterLogic;
        }else{
            return '';
        }
    }

   /***************************************************************************
    Description: 
    This init method is to used to evaluate expression
    *************************************************************************************/
   @AuraEnabled
   public static BundledProductProfilesLtCntrl evaluateString  (String filterLogic, String obj)
   {
      Type resultType = Type.forName('BundledProductProfilesLtCntrl');
      BundledProductProfilesLtCntrl bp =(BundledProductProfilesLtCntrl)JSON.deserialize(obj,resultType);
      
      try {
          System.debug('----------FilterLogic---------'+filterLogic);
          bp.clearanceLogicResult = BooleanExpression.evaluate(filterLogic);
          System.debug('----------Result---------'+bp.clearanceLogicResult);
      } catch(Exception e) {
          system.debug('Error-----'+e);
      }
      buildBundledProductProfileResults(bp);
      return bp;
   }

    /***************************************************************************
    Description: 
    This method is to update the bundled clearance entries.
    *************************************************************************************/
    @AuraEnabled
    public static boolean updateParentClearanceEntries(String obj){
        Type resultType = Type.forName('BundledProductProfilesLtCntrl');
        BundledProductProfilesLtCntrl bp =(BundledProductProfilesLtCntrl)JSON.deserialize(obj,resultType);
        system.debug('********Check entries1*******');
        List<Input_Clearance__c> parentClearanceEntries = new List<Input_Clearance__c>();
        system.debug('********Check entries2*******');
        //currentprodprofile.Bundled_Filter_Logic__c = bundleClearanceLogic;
        bp.parentProductProfile.Bundled_Filter_Logic__c = bp.bundleClearanceLogic;
        system.debug('********Check entries3*******');
        system.debug('********bundledResults*******'+bp.bundledResults);
 
        for(BundledProductProfileResult bundledresult : bp.bundledResults){
            system.debug('********bundledResult*********'+bundledresult);
            if(bundledresult.clearanceId!=null){
                parentClearanceEntries.add(
                    new Input_Clearance__c(
                        Id= bundledresult.clearanceId,
                        Clearance_Status__c = bundledresult.clearanceStatusResult
                    )
                );
            }
        }
        try{
            system.debug('********Check1*******');
            Database.saveresult sr = database.update(bp.parentProductProfile,false);
            system.debug('********Check2*******');
            update parentClearanceEntries;
            updateParentClearanceEntriesMaps(bp);
            system.debug('********Check3*******');
            return true;
        }catch(Exception e){
            System.debug('-----Exception Occured'+e.getMessage());
            return false;
        }
    }

    public class ProductProfileClearance implements comparable
    {
        @AuraEnabled public Integer sequenceNumber{get;set;}
        @AuraEnabled public String productProfile{get;set;}
        @AuraEnabled public String productProfileId{get;set;}
        @AuraEnabled public String clearanceStatus{get;set;} 
        @AuraEnabled public String profileName{get;set;}
        @AuraEnabled public String entryId{get;set;}
        @AuraEnabled public String seqnum{get;set;}
        
        public ProductProfileClearance(Integer sequenceNumber, String productProfile, String profileName, String productProfileId, String clearanceStatus, Id entryId)
        {
            this.sequenceNumber = sequenceNumber;
            this.productProfile = productProfile;
            this.productProfileId = productProfileId;
            this.clearanceStatus = clearanceStatus;
            this.profileName = profileName;
            this.entryId = entryId;
            this.seqnum = String.valueOf(sequenceNumber);
        }
        
        public Integer compareTo(Object ObjToCompare) {
            return seqnum.CompareTo(((ProductProfileClearance)ObjToCompare).seqnum);
        }
    }

    public class ProductProfile
    {
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String ownerName{get;set;}
        @AuraEnabled public String prodProfileName{get;set;} 
        @AuraEnabled public String recordTypeName{get;set;}
        @AuraEnabled public String recordTypeDeveloperName{get;set;}
        @AuraEnabled public String prodDescription{get;set;}
        @AuraEnabled public String createdByName{get;set;}
        @AuraEnabled public String lastModifiedByName{get;set;}
        @AuraEnabled public String bundledFilterLogic{get;set;}
        @AuraEnabled public Id lastModifiedById{get;set;}
        @AuraEnabled public Id createdById{get;set;}
        @AuraEnabled public Id ownerId{get;set;}
        @AuraEnabled public String versionNo{get;set;}
        @AuraEnabled public boolean thirdPartyProduct{get;set;}
        @AuraEnabled public String businessUnit{get;set;}
        @AuraEnabled public String gmdnCode{get;set;}
        @AuraEnabled public String clearanceFamily{get;set;}
        @AuraEnabled public String modelPartNumber{get;set;}
        @AuraEnabled public String profitCenter{get;set;}
        @AuraEnabled public String profitCenterUpgrade{get;set;}
    }
    

    public class StandalonePPA
    {
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String standalonePPAId{get;set;} 
        @AuraEnabled public String standalonePPAName{get;set;}
        @AuraEnabled public Decimal seqNumber{get;set;}
    }

    public class InputClearance
    {
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String cleranceEntryFormName{get;set;} 
        @AuraEnabled public String countryId{get;set;}
        @AuraEnabled public String countryName{get;set;}
        @AuraEnabled public String clearanceStatus{get;set;}
        @AuraEnabled public Date clearanceDate{get;set;}
        @AuraEnabled public Date expectedClearanceDate{get;set;}
    }

    public class CountryWrapper
    {
        @AuraEnabled public String Name{get;set;}
        @AuraEnabled public String Id{get;set;}

        public CountryWrapper(String id, String name)
        {

            this.Name = name;
            this.Id = id;
        }
    }

    /***************************************************************************
    Description: 
    This is a wrapper class which stores the clearance entry ID, Country, Status.
    *************************************************************************************/
    public class BundledProductProfileResult{
        @AuraEnabled public Id clearanceId{get;set;}
        @AuraEnabled public String filterLogic{get;set;}
        @AuraEnabled public String clearanceStatus{get;set;}
        @AuraEnabled public String country{get;set;}
        @AuraEnabled public Boolean clearanceStatusBooleanResult{get;set;}
        @AuraEnabled public String clearanceStatusResult{get;set;}
        
        public BundledProductProfileResult(Id clearanceId, String country, String filterLogic, String clearanceStatus){
            try {
                String newFilterLogic = filterLogic.replace('AND', '&&').replace('OR','||').replace('and', '&&').replace('or','||');
                this.clearanceStatusBooleanResult = BooleanExpression.evaluate(newFilterLogic );
                this.clearanceId = clearanceId;
                this.filterLogic = filterLogic;
                this.clearanceStatus = clearanceStatus;
                this.country = country;
                if(this.clearanceStatusBooleanResult) {
                    this.clearanceStatusResult = 'Permitted';
                } else {
                    this.clearanceStatusResult = 'Blocked';
                }
                system.debug('***********************************************');
                system.debug('--------filterLogic-----------'+filterLogic);
                system.debug('--------clearanceStatus-----------'+clearanceStatus);
                system.debug('--------clearanceStatusBooleanResult-----------'+clearanceStatusBooleanResult);
                system.debug('--------clearanceStatusResult-----------'+clearanceStatusResult);
                
            } catch(Exception e) {}
        }
    }

}
/***************************************************************************
Author: Anshul Goel
Created Date: 02-Feb-2018
Project/Story/Inc/Task : Sales Lightning 
Description: 
This is a Test Class for BundledProductProfile and BundledProductNewEdit.
Change Log:
*************************************************************************************/

@IsTest(SeeAllData=true)
public class BundledProductProfilesLtCntrlTest
{
  
   public static string bundledprodprofid = Schema.SObjectType.Review_Products__c.getRecordTypeInfosByName().get('Bundle').getRecordTypeId();
   public static list<Review_Products__c> stdprodlst = new list<Review_Products__c>();
   public static Product_Profile_Association__c ppa2;
   public static Country__c con;
   public static Country_Rules__c cntyRule;

    /***************************************************************************
    Description: 
    This method is to used to insert review Product amnd coutry and to test Lightning controller for MCM bundle
    *************************************************************************************/
    @isTest static void testMethod1() 
    {
        con = new Country__c(Name='India',Region__c='EMEA',Expiry_After_Years__c='1');
        insert con;
        cntyRule = new Country_Rules__c();
        cntyRule.Country__c=con.id;
        cntyRule.Weeks_Estimated_Review_Cycle__c=2;
        cntyRule.Weeks_to_Prepare_Submission__c=1;
        insert cntyRule;

        //Create and Insert Review Product
        Review_Products__c stdprod1 = new Review_Products__c();
        stdprod1.Product_Profile_Name__c = 'standard prod profile';
        stdprod1.Business_Unit__c = 'VBT';
        stdprodlst.add(stdprod1);
        Review_Products__c stdprod2 = new Review_Products__c();
        stdprod2.Product_Profile_Name__c = 'standard prod profile1';
        stdprod2.Business_Unit__c = 'VBT - 3rd Party';
        stdprodlst.add(stdprod2);
        Review_Products__c stdprod3 = new Review_Products__c();
        stdprod3.Product_Profile_Name__c = 'standard prod profile2';
        stdprod3.Business_Unit__c = 'VOS';
        stdprodlst.add(stdprod3);
        Review_Products__c stdprod4 = new Review_Products__c();
        stdprod4.Product_Profile_Name__c = 'standard prod profile3';
        stdprod4.Business_Unit__c = 'VOS - 3rd Party';
        stdprodlst.add(stdprod4);
        Review_Products__c bundleprodprof = new Review_Products__c();
        bundleprodprof.Product_Profile_Name__c = 'Bundled Prod Profile';
        bundleprodprof.Bundled_Filter_Logic__c = '1 AND 2 AND 3 AND 4';
        bundleprodprof.recordtypeId = bundledprodprofid;
        stdprodlst.add(bundleprodprof);
        insert stdprodlst;
       
        Product_Profile_Association__c ppa1 = new Product_Profile_Association__c();
        ppa1.Standalone_Product_Profile__c = stdprodlst[0].id;
        ppa1.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa1;
        ppa2 = new Product_Profile_Association__c();
        ppa2.Standalone_Product_Profile__c = stdprodlst[1].id;
        ppa2.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa2;
        Product_Profile_Association__c ppa3 = new Product_Profile_Association__c();
        ppa3.Standalone_Product_Profile__c = stdprodlst[2].id;
        ppa3.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa3;
        Product_Profile_Association__c ppa4 = new Product_Profile_Association__c();
        ppa4.Standalone_Product_Profile__c = stdprodlst[3].id;
        ppa4.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa4;
       
       //Test Code COverage for BundledProductProfiles
        BundledProductProfilesLtCntrl bp;
       
        Test.starttest();
        bp = BundledProductProfilesLtCntrl.initMethod(stdprodlst[4].id);
        bp = BundledProductProfilesLtCntrl.refreshRelatedList(stdprodlst[4].id,JSON.serialize(bp));
        bp.selectedCountryId   = con.id;
        bp = BundledProductProfilesLtCntrl.fetchClearanceStatus(JSON.serialize(bp));
        BundledProductProfilesLtCntrl b =  BundledProductProfilesLtCntrl.evaluateString('1 AND 2 AND 3 OR 4', JSON.serialize(bp));
        String status =  BundledProductProfilesLtCntrl.deletePP(stdprodlst[1].id);
        bp =  BundledProductProfilesLtCntrl.deleteRecord(stdprodlst[2].id,JSON.serialize(bp),'Product_Profile_Association__c',ppa4.id);
        
        //Test Code Coverage for BundledProductNewEdit
        BundledProdNewEditLightContrl bp1;
        BundledProductProfilesLtCntrl.ProductProfile cp;
        bp1 = BundledProdNewEditLightContrl.initMethod(stdprodlst[4].id,cp);
        String saveRecord = BundledProdNewEditLightContrl.saveRecord(stdprodlst[4].id,JSON.serialize(bp1.cp),null,null);
        Test.stoptest();
    }
}
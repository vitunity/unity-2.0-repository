@istest(seeAllData=false)
public class BundledProductProfilesTest
{
    public static string bundledprodprofid = Schema.SObjectType.Review_Products__c.getRecordTypeInfosByName().get('Bundle').getRecordTypeId();
    public static list<Review_Products__c> stdprodlst = new list<Review_Products__c>();
    public static Product_Profile_Association__c ppa2;
    public static Country__c con;
    public static Country_Rules__c cntyRule;
    
    static
    {
        insert new Clearance_Entry_Documents__c(Name='DoC Landing Page', Link__c='http://dms.varian.com/livelink/livelink.exe/fetch/2000/1621351/1754337/5302729/6249704/Declaration_of_Conformity_Index_September_28_2017.pdf?nodeid=34147447&vernum=-2');
        insert new Clearance_Entry_Documents__c(Name='IFU Translation', Link__c='http://dms.varian.com/livelink/livelink.exe?func=ll&objaction=overview&objid=98277322');
    
        con = new Country__c(Name='India',Region__c='EMEA',Expiry_After_Years__c='1');
        insert con;
        cntyRule = new Country_Rules__c();
        cntyRule.Country__c=con.id;
        cntyRule.Weeks_Estimated_Review_Cycle__c=2;
        cntyRule.Weeks_to_Prepare_Submission__c=1;
        insert cntyRule;

        Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';
        insert pSubItem;

        Review_Products__c stdprod1 = new Review_Products__c();
        stdprod1.Product_Profile_Name__c = 'standard prod profile';
        stdprod1.Business_Unit__c = 'VBT';
        stdprod1.Product_Model__c = pSubitem.Id;
        stdprod1.Version_No__c = '12';
        stdprod1.Clearance_Family__c = 'BrachyVision10';
        stdprod1.Regulatory_Name__c = 'Regular NAme';
        stdprod1.Model_Part_Number__c='test1';
        stdprod1.Transition_Phase_Gate__c=system.today();
        stdprod1.National_Language_Support__c=system.today();
        stdprod1.Design_Output_Review__c=system.today();
        stdprodlst.add(stdprod1);
        Review_Products__c stdprod2 = new Review_Products__c();
        stdprod2.Product_Profile_Name__c = 'standard prod profile1';
        stdprod2.Business_Unit__c = 'VBT - 3rd Party';
        stdprod2.Product_Model__c = pSubitem.Id;
        stdprod2.Version_No__c = '12';
        stdprod2.Clearance_Family__c = 'BrachyVision10';
        stdprod2.Regulatory_Name__c = 'Regular NAme';
        stdprod2.Model_Part_Number__c='test1';
        stdprod2.Transition_Phase_Gate__c=system.today();
        stdprod2.National_Language_Support__c=system.today();
        stdprod2.Design_Output_Review__c=system.today();
        stdprodlst.add(stdprod2);
        Review_Products__c stdprod3 = new Review_Products__c();
        stdprod3.Product_Profile_Name__c = 'standard prod profile2';
        stdprod3.Business_Unit__c = 'VOS';
        stdprod3.Product_Model__c = pSubitem.Id;
        stdprod3.Version_No__c = '12';
        stdprod3.Clearance_Family__c = 'BrachyVision10';
        stdprod3.Regulatory_Name__c = 'Regular NAme';
        stdprod3.Model_Part_Number__c='test1';
        stdprod3.Transition_Phase_Gate__c=system.today();
        stdprod3.National_Language_Support__c=system.today();
        stdprod3.Design_Output_Review__c=system.today();
        stdprodlst.add(stdprod3);
        Review_Products__c stdprod4 = new Review_Products__c();
        stdprod4.Product_Profile_Name__c = 'standard prod profile3';
        stdprod4.Business_Unit__c = 'VOS - 3rd Party';
        stdprod4.Product_Model__c = pSubitem.Id;
        stdprod4.Version_No__c = '12';
        stdprod4.Clearance_Family__c = 'BrachyVision10';
        stdprod4.Regulatory_Name__c = 'Regular NAme';
        stdprod4.Model_Part_Number__c='test1';
        stdprod4.Transition_Phase_Gate__c=system.today();
        stdprod4.National_Language_Support__c=system.today();
        stdprod4.Design_Output_Review__c=system.today();
        stdprodlst.add(stdprod4);
        Review_Products__c bundleprodprof = new Review_Products__c();
        bundleprodprof.Product_Profile_Name__c = 'Bundled Prod Profile';
        bundleprodprof.Bundled_Filter_Logic__c = '1 AND 2 AND 3 AND 4';
        bundleprodprof.recordtypeId = bundledprodprofid;
        stdprodlst.add(bundleprodprof);
        insert stdprodlst;
        for (Review_Products__c pp : [select id,recordtypeId,name,Product_Profile_Name__c from Review_Products__c])
        {
            system.debug('***pp : '+pp);
        }

        
        Product_Profile_Association__c ppa1 = new Product_Profile_Association__c();
        ppa1.Standalone_Product_Profile__c = stdprodlst[0].id;
        ppa1.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa1;
        ppa2 = new Product_Profile_Association__c();
        ppa2.Standalone_Product_Profile__c = stdprodlst[1].id;
        ppa2.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa2;
        Product_Profile_Association__c ppa3 = new Product_Profile_Association__c();
        ppa3.Standalone_Product_Profile__c = stdprodlst[2].id;
        ppa3.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa3;
        Product_Profile_Association__c ppa4 = new Product_Profile_Association__c();
        ppa4.Standalone_Product_Profile__c = stdprodlst[3].id;
        ppa4.Bundle_Product_Profile__c = stdprodlst[4].id;
        insert ppa4;
        //insert ppalst;
    }

    public static testMethod void testMethod1()
    {
        Apexpages.StandardController stdSetController = new Apexpages.StandardController(stdprodlst[4]);
        Apexpages.Currentpage().getparameters().put('id',stdprodlst[4].id);
        PageReference pageRef = Page.BundledProductProfiles;
        ApexPages.currentPage().getParameters().put('retUrl', 'yyyy');
        Apexpages.Currentpage().getparameters().put('id',stdprodlst[4].id);
        Test.setCurrentPage(pageRef);
        BundledProductProfiles bpp = new BundledProductProfiles(stdSetController);
        bpp.selectedCountryId = con.id;
        List<Review_Products__c> pplst = new List<Review_Products__c>();
        pplst.add(stdprodlst[0]);
        pplst.add(stdprodlst[1]);
        pplst.add(stdprodlst[2]);
        pplst.add(stdprodlst[3]);
        bpp.childProductProfiles = pplst;
        bpp.parentProductProfile = stdprodlst[4];
        bpp.parentClearanceIds = new map<String,Id>();
        bpp.parentClearanceStatus = new map<string,String>();
        bpp.productProfileSequence = new Map<Integer,String>();
        bpp.updateParentClearanceEntriesMaps();
        bpp.simulate();
        bpp.updateParentClearanceEntries();
        bpp.parentprodprofid = stdprodlst[4].id;
        bpp.batchcode();
        bpp.editbundledprof();
        bpp.assignchildpp();
        //bpp.currentprodprofile = stdprodlst[1];
        bpp.redirecttostandardpage();
        
        //stdprodlst[4].Bundled_Filter_Logic__c = '1 AND 2 AND 3';
        //bpp1.doSave();
        //bpp1.SaveNew();
    }
    public static testMethod void testMethod2()
    {
        Apexpages.StandardController stdSetController = new Apexpages.StandardController(stdprodlst[1]);
        Apexpages.Currentpage().getparameters().put('id',stdprodlst[1].id);
        BundledProductProfiles bpp = new BundledProductProfiles(stdSetController);
        bpp.currentprodprofile = stdprodlst[1];
        bpp.redirecttostandardpage();

        PageReference pageRef1 = Page.BundledProductProfiles;
        //ApexPages.currentPage().getParameters().put('retUrl', 'yyyy');
        Test.setCurrentPage(pageRef1);
        Apexpages.Currentpage().getparameters().put('id',stdprodlst[4].id);
        Apexpages.StandardController stdSetController1 = new Apexpages.StandardController(stdprodlst[4]);
        BundledProductProfiles bpp1 = new BundledProductProfiles(stdSetController1);
        bpp1.parentProductProfile = stdprodlst[4];
        bpp1.getCountries();
        bpp1.doSave();
        bpp1.SaveNew();
        bpp1.editbundledprof();
        bpp1.assignchildpp();
        bpp1.resultText = 'TEST';
        bpp1.andALL();
        bpp1.orALL();
        bpp1.buildClearanceLogic('OR');

        //bpp.updateParentClearanceEntries();
    }
    public static testMethod void testMethod3()
    {
        System.Test.StartTest();
        Review_Products__c bundleprodprof = new Review_Products__c();
        bundleprodprof.Product_Profile_Name__c = 'Bundled Prod Profile';
        bundleprodprof.Bundled_Filter_Logic__c = '1 AND 2';
        system.debug('bundledprodprofid : '+bundledprodprofid);
        bundleprodprof.recordtypeId = bundledprodprofid;
        //insert bundleprodprof;
        stdprodlst[4].Bundled_Filter_Logic__c = '1 AND 2 AND 3 OR 4';
        update stdprodlst[4];
        delete ppa2;
        System.Test.StopTest();
        //bpp.updateParentClearanceEntries();
    }
    public static testMethod void testMethod4()
    {
        test.StartTest();
        SR_Batch2SetBundleProfClearanceentries bat = new SR_Batch2SetBundleProfClearanceentries();
        database.executebatch(bat,1);
        stdprodlst[4].Bundled_Filter_Logic__c = '(1 AND 2) or (3 AND 4)';
        update stdprodlst[4];
        SR_Batch2SetBundleProfClearanceentries bat1 = new SR_Batch2SetBundleProfClearanceentries();
        database.executebatch(bat1,1);
        test.StopTest();
        //bpp.updateParentClearanceEntries();
    }
}
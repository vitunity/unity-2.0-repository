/********************************************************************************************************
@Author:Praveen Musunuru
@Date: Aug 19,2014
@Description: This Controller is used to populated the Estimated Submission Date and Expected Clearance Date
@Last Modified by: Praveen Musunuru
@Last Modified on: Aug 26,2014
@Last modified Reason: Added Design Output Review Milestone Functionality
*********************************************************************************************************/

public Class CEFUtility{

    public static void createClearanceDocumentsLink(List<Input_Clearance__c >  lstInputCls){
        
        list<Clearance_Documents_Link__c> documentListToInsert = new list<Clearance_Documents_Link__c>();
        list<Clearance_Documents_Link__c> documentList = new list<Clearance_Documents_Link__c>();
        list<Clearance_Documents_Link__c> documentListToDelete = new list<Clearance_Documents_Link__c>();
        map<Id, String> prodProfileMap = new map<Id, String>();
        map<Id, Clearance_Documents_Link__c> documentLinkMap = new map<Id, Clearance_Documents_Link__c>();
        set<Id> clearanceId = new set<Id>();
        for(Input_Clearance__c ic :lstInputCls){
            clearanceId.add(ic.Id);
        }
        system.debug('clearanceId=='+clearanceId);
        list<Review_Products__c> prodProfile = new list<Review_Products__c>();
        prodProfile = [Select Id, Business_Unit__c from Review_Products__c];
        String Link1 = Clearance_Entry_Documents__c.getValues('DoC Landing Page').link__c;
        String Link2 = Clearance_Entry_Documents__c.getValues('IFU Translation').link__c;
        
        documentList = [Select Id, Clearance_Entry_Form__c 
                                from Clearance_Documents_Link__c 
                                where Clearance_Entry_Form__c IN: clearanceId];
        
        system.debug('==documentList=='+documentList);
        if(documentList.size()>0){
            for(Clearance_Documents_Link__c dl : documentList){
                documentLinkMap.put(dl.Clearance_Entry_Form__c, dl);
            }
        }
        system.debug('==documentLinkMap=='+documentLinkMap);
        for(Review_Products__c pp :prodProfile){
            prodProfileMap.put(pp.Id, pp.Business_Unit__c);
        }
        system.debug('prodProfileMap=='+prodProfileMap);
        
        for(Input_Clearance__c ic : lstInputCls){
            system.debug('ic.Review_Product__c=='+ic.Review_Product__c);
            if((ic.Clearance_Evidence_Requirements__c == 'CE mark only' 
            || ic.Clearance_Evidence_Requirements__c == 'CE mark + NLS'
            || ic.Clearance_Evidence_Requirements__c == 'CE mark + NLS + registration' )
            && prodProfileMap.containsKey(ic.Review_Product__c) && prodProfileMap.get(ic.Review_Product__c) == 'VBT'
            && (documentLinkMap == null || !documentLinkMap.containsKey(ic.Id))){
                system.debug('==ic.Clearance_Evidence_Requirements__c =='+ic.Clearance_Evidence_Requirements__c );
                //system.debug('==prodProfileMap.get(ic.Review_Product__c)=='+ );
                Clearance_Documents_Link__c cd1 = new Clearance_Documents_Link__c();
                Clearance_Documents_Link__c cd2 = new Clearance_Documents_Link__c();
                cd1.Name = 'DoC Landing Page';
                cd1.Clearance_Documents_Link__c = Link1;
                cd1.Certificate_Id__c = 'See DoC';
                cd1.Clearance_Entry_Form__c = ic.Id;
                cd2.Name = ' IFU Translatione';
                cd2.Clearance_Documents_Link__c = Link2;
                cd2.Clearance_Entry_Form__c = ic.Id;
                documentListToInsert.add(cd1);
                documentListToInsert.add(cd2);
            }
            
            if((ic.Clearance_Evidence_Requirements__c != 'CE mark only' 
            || ic.Clearance_Evidence_Requirements__c != 'CE mark + NLS'
            || ic.Clearance_Evidence_Requirements__c != 'CE mark + NLS + registration' )
            ||  prodProfileMap.containsKey(ic.Review_Product__c) && prodProfileMap.get(ic.Review_Product__c) != 'VBT'){
            
                for(Clearance_Documents_Link__c dl : documentList){
                    if(dl.Clearance_Entry_Form__c == ic.Id ){
                        documentListToDelete.add(dl);
                    }
                }
            }
            
        }
        if(documentListToInsert.size()>0){
            insert documentListToInsert;
        }
        system.debug('==documentListToDelete=='+documentListToDelete);
        if(documentListToDelete.size()>0){
            //delete documentListToDelete;
        }
    } 

    public static void updateMCMMarketClearanceForecast(List<Input_Clearance__c >  lstInputCls)
    {
       List<Id> lstCountriesId= new List<Id>();
       Map<Id,Country_Rules__c> mapCountryRules = new Map<Id,Country_Rules__c>();
       List<ID>lstProductProfileId = new List<ID>();
       Map<Id,Review_Products__c> mapProductProfiles = new Map<Id,Review_Products__c>();
       for( Input_Clearance__c obj : lstInputCls){
            if(obj.Country__c!=null){
                lstCountriesId.add(obj.Country__c);
            }
            if(obj.Review_Product__c != null){
            lstProductProfileId.add(obj.Review_Product__c);
            }
            
       }
       System.debug('lstCountriesId###'+lstCountriesId);
       List<Country_Rules__c> lstCountryRules=[Select Country__c, Weeks_Estimated_Review_Cycle__c,Weeks_to_Prepare_Submission__c, Clearance_Evidence_Requirements__c from Country_Rules__c   where Country__c IN : lstCountriesId];
           System.debug('lstCountryRules###'+lstCountryRules);
       if(lstCountryRules != null){
       
            for(Country_Rules__c obj : lstCountryRules){
                mapCountryRules.put(obj.Country__c,obj);
            }
        }
       
       List<Review_Products__c> lstReviewProducts =[Select id,Transition_Phase_Gate__c,National_Language_Support__c,Design_Output_Review__c From Review_Products__c where ID IN :lstProductProfileId];
       
       if(lstReviewProducts != null){
       
            for(Review_Products__c obj : lstReviewProducts){
                mapProductProfiles.put(obj.id,obj);
            }
        }

                
        for( Input_Clearance__c obj : lstInputCls){
            
            if(obj.Country__c != null && mapCountryRules <> null && mapCountryRules.containsKEy(obj.country__c) && mapCountryRules.get(obj.country__c).Clearance_Evidence_Requirements__c != null )
            {
                obj.Clearance_Evidence_Requirements__c = mapCountryRules.get(obj.country__c).Clearance_Evidence_Requirements__c;
            }   
           
             if(obj.Review_Product__c!=null && obj.Milestone_Selection__c!=null && obj.Milestone_Selection__c == 'Transition Phase Gate' ){
                      
                       if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null &&
                            mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c != null && mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c !=null)
                        {
                            Integer submissionWeeks=Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c);
                            System.debug('submissionWeeks###'+submissionWeeks);
                            if(submissionWeeks >= 0){
                                obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c.addDays(submissionWeeks*7);
                            }
                        }else {
                            obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c;
                        }
                       
                        if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null && 
                        mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c !=null && mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c !=null){
                         
                          Integer submissionWeeks = Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c + mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c) ;
                           
                          System.debug('submissionWeeks###'+submissionWeeks);
                            
                            if(submissionWeeks >= 0){
                                obj.Expected_Clearance_Date__c=mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c.addDays(submissionWeeks*7);
                            }
                            
                        }else{
                                obj.Expected_Clearance_Date__c  =mapProductProfiles.get(obj.Review_Product__c).Transition_Phase_Gate__c;
                        }
                         
             
             }else if(obj.Review_Product__c!=null && obj.Milestone_Selection__c!=null && obj.Milestone_Selection__c == 'National Language Support'){
             
                if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null &&
                            mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c != null && mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c !=null )
                        {
                            Integer submissionWeeks=Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c);
                            System.debug('submissionWeeks###'+submissionWeeks);
                            if(submissionWeeks >= 0){
                                obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c.addDays(submissionWeeks*7);
                            }
                        }else {
                            obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c;
                        }
                        if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null && 
                        mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c !=null && mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c !=null){
                            
                            Integer submissionWeeks = Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c + mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c) ;
                                                    
                            System.debug('submissionWeeks###'+submissionWeeks);
                            
                            if(submissionWeeks >= 0){
                                obj.Expected_Clearance_Date__c=mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c.addDays(submissionWeeks*7);
                            }
                            
                        }else{
                                obj.Expected_Clearance_Date__c  =mapProductProfiles.get(obj.Review_Product__c).National_Language_Support__c;
                        }
                        
                }else if(obj.Review_Product__c!=null && obj.Milestone_Selection__c!=null && obj.Milestone_Selection__c == 'Design Output Review'){
             
                if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null &&
                            mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c != null && mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c !=null )
                        {
                            Integer submissionWeeks=Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c);
                            System.debug('submissionWeeks###'+submissionWeeks);
                            if(submissionWeeks >= 0){
                                obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c.addDays(submissionWeeks*7);
                            }
                        }else {
                            obj.Estimated_Submission_Date__c=mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c;
                        }
                        if(obj.Country__c != null &&  mapCountryRules.get(obj.Country__c) != null && mapProductProfiles.get(obj.Review_Product__c) !=null && 
                        mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c !=null && mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c !=null){
                            
                            Integer submissionWeeks = Integer.valueOf(mapCountryRules.get(obj.Country__c).Weeks_to_Prepare_Submission__c + mapCountryRules.get(obj.Country__c).Weeks_Estimated_Review_Cycle__c) ;
                           
                            System.debug('submissionWeeks###'+submissionWeeks);
                            
                            if(submissionWeeks >= 0){
                                obj.Expected_Clearance_Date__c=mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c.addDays(submissionWeeks*7);
                            }
                            
                        }else{
                                obj.Expected_Clearance_Date__c  =mapProductProfiles.get(obj.Review_Product__c).Design_Output_Review__c;
                        }
             }else if(obj.Milestone_Selection__c == null){
                 obj.Expected_Clearance_Date__c=null;
                 obj.Estimated_Submission_Date__c=null;
             }
             
        }
    }
    
    public static void UpdateBlockItem(List<Input_Clearance__c> lstInputCls){
    
        if(lstInputCls !=null){
        Map<Id,Input_Clearance__c> mapInputClsIds = new Map<ID,Input_Clearance__c>();
        
        for( Input_Clearance__c obj : lstInputCls){
              //if(obj.Clearance_Date_applies_to_Features__c =='Yes' ||)
              mapInputClsIds.put(obj.id,obj);
            }
            
            if(!mapInputClsIds.isEmpty()){
                
                List<Blocked_Items__c> lstBlockItems =[Select Input_Clearance__c, Expected_Clearance_Date__c from Blocked_Items__c where Input_Clearance__c IN : mapInputClsIds.keyset()];
                
                for(Blocked_Items__c obj: lstBlockItems){
                    if(mapInputClsIds.get(obj.Input_Clearance__c).Clearance_Date_applies_to_Features__c =='Yes' ){
                    obj.Expected_Clearance_Date__c=mapInputClsIds.get(obj.Input_Clearance__c).Expected_Clearance_Date__c;
                    }else if(mapInputClsIds.get(obj.Input_Clearance__c).Clearance_Date_applies_to_Features__c =='No'){
                        obj.Expected_Clearance_Date__c=null;
                    }
                }
                
                update lstBlockItems;
            }
            
        }
    }
    
    public static void setProductBundleFlag(List<Input_Clearance__c >  lstInputCls) {
        Map<Id, Review_Products__c> prodProfileMap;

        Set<Id> ppIdList = new Set<Id>();
        for(Input_Clearance__c obj : lstInputCls){
            if(obj.Review_Product__c != null)
                ppIdList.add(obj.Review_Product__c);
        }
        
        if(!ppIdList.isEmpty()) {
            prodProfileMap = new Map<Id, Review_Products__c>([Select Id, RecordType.Name from Review_Products__c Where Id in: ppIdList]);
        }
        
        for( Input_Clearance__c obj : lstInputCls){
            if(obj.Review_Product__c != null && prodProfileMap.containsKey(obj.Review_Product__c) && 
                prodProfileMap.get(obj.Review_Product__c).RecordType.Name=='Bundle' && !obj.IsProductBundle__c) {
                obj.IsProductBundle__c = True;
            } else if(obj.Review_Product__c != null && prodProfileMap.containsKey(obj.Review_Product__c) && 
                prodProfileMap.get(obj.Review_Product__c).RecordType.Name != 'Bundle' && obj.IsProductBundle__c) {
                obj.IsProductBundle__c = False;
            }
        }
    }
}
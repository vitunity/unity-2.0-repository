/**
@Summary:Used to validiate functionality of  methods within CEFUtility 
**/
@isTest()

public class CEFUtilityTest{
public static Review_Products__c rp2;
public static Country__c cntry ;
  public static void setupData(){
        insert new Clearance_Entry_Documents__c(Name='DoC Landing Page', Link__c='http://dms.varian.com/livelink/livelink.exe/fetch/2000/1621351/1754337/5302729/6249704/Declaration_of_Conformity_Index_September_28_2017.pdf?nodeid=34147447&vernum=-2');
        insert new Clearance_Entry_Documents__c(Name='IFU Translation', Link__c='http://dms.varian.com/livelink/livelink.exe?func=ll&objaction=overview&objid=98277322');
        Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';
        
        insert pSubItem;
        
        rp2 = new Review_Products__c();
        rp2.Product_Model__c = pSubitem.Id;
        rp2.Version_No__c = '12';
        rp2.Product_Profile_Name__c = pSubitem.Name;
        rp2.Clearance_Family__c = 'BrachyVision10';
        rp2.Business_Unit__c = 'VBT';
        rp2.Regulatory_Name__c = 'Regular NAme';
        rp2.Model_Part_Number__c='test1';
        rp2.Transition_Phase_Gate__c=system.today();
        rp2.National_Language_Support__c=system.today();
        rp2.Design_Output_Review__c=system.today();
        //try{
        insert rp2;
        //}catch(exception e){
        //}
        
        cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        //try{
            insert cntry;
        //}catch(exception e){
        //}
        
        
        Country_Rules__c cntyRule = new Country_Rules__c();
        cntyRule.Country__c=cntry.id;
        cntyRule.Weeks_Estimated_Review_Cycle__c=2;
        cntyRule.Weeks_to_Prepare_Submission__c=1;
        
        insert cntyRule;
        
               
        
  }
  
  private static testmethod void createClearanceDocumentsLinkTest(){
      
        setupData();
        List<Input_Clearance__c> lstInputCls = new List<Input_Clearance__c>();
        List<Blocked_Items__c> lstBlockItems = new List<Blocked_Items__c>();
        
        Input_Clearance__c  obj = new Input_Clearance__c();
        
        obj.Review_Product__c = rp2.id;
        obj.Country__c=cntry.id;
        obj.Milestone_Selection__c='Transition Phase Gate';
        obj.Clearance_Date_applies_to_Features__c='Yes';
        obj.Clearance_Evidence_Requirements__c = 'CE mark only';
        lstInputCls.add(obj);
        Input_Clearance__c  obj2 = new Input_Clearance__c();
        
        obj2.Review_Product__c = rp2.id;
        obj2.Country__c=cntry.id;
        obj2.Milestone_Selection__c='National Language Support';
        obj2.Clearance_Date_applies_to_Features__c='No';
        obj2.Clearance_Evidence_Requirements__c = 'CE mark only';
        lstInputCls.add(obj2);
        Input_Clearance__c  obj3 = new Input_Clearance__c();
        
        obj3.Review_Product__c = rp2.id;
        obj3.Country__c=cntry.id;
        obj3.Milestone_Selection__c='Design Output Review';
        obj3.Clearance_Date_applies_to_Features__c='No';
        obj3.Clearance_Evidence_Requirements__c = 'CE mark only';
        lstInputCls.add(obj3);
        insert lstInputCls;
    
        
        Blocked_Items__c objBItem = new Blocked_Items__c();
        objBItem.Input_Clearance__c=obj.id;
        lstBlockItems.add(objBItem);
        Blocked_Items__c objBItem2 = new Blocked_Items__c();
        objBItem2.Input_Clearance__c=obj2.id;
        lstBlockItems.add(objBItem2);
        insert lstBlockItems;
        Test.startTest();
            insert new Clearance_Entry_Documents__c(Name='DoC', Link__c='http://dms.varian.com/livelink/livelink.exe/fetch/2000/1621351/1754337/5302729/6249704/Declaration_of_Conformity_Index_September_28_2017.pdf?nodeid=34147447&vernum=-2');
            insert new Clearance_Entry_Documents__c(Name='IFU', Link__c='http://dms.varian.com/livelink/livelink.exe?func=ll&objaction=overview&objid=98277322');
            CEFUtility.createClearanceDocumentsLink(lstInputCls);
        Test.stopTest();
  }

  private static testmethod void updateMCMMarketClearanceForecastTest(){
    Test.startTest();
    setupData();
    List<Input_Clearance__c> lstInputCls = new List<Input_Clearance__c>();
    List<Blocked_Items__c> lstBlockItems = new List<Blocked_Items__c>();
    
    Input_Clearance__c  obj = new Input_Clearance__c();
    
    obj.Review_Product__c=rp2.id;
    obj.Country__c=cntry.id;
    obj.Milestone_Selection__c='Transition Phase Gate';
    obj.Clearance_Date_applies_to_Features__c='Yes';
    lstInputCls.add(obj);
    Input_Clearance__c  obj2 = new Input_Clearance__c();
    
    obj2.Review_Product__c=rp2.id;
    obj2.Country__c=cntry.id;
    obj2.Milestone_Selection__c='National Language Support';
    obj2.Clearance_Date_applies_to_Features__c='No';
    lstInputCls.add(obj2);
    Input_Clearance__c  obj3 = new Input_Clearance__c();
    
    obj3.Review_Product__c=rp2.id;
    obj3.Country__c=cntry.id;
    obj3.Milestone_Selection__c='Design Output Review';
    obj3.Clearance_Date_applies_to_Features__c='No';
    lstInputCls.add(obj3);
    insert lstInputCls;

    
    Blocked_Items__c objBItem = new Blocked_Items__c();
    objBItem.Input_Clearance__c=obj.id;
    lstBlockItems.add(objBItem);
    Blocked_Items__c objBItem2 = new Blocked_Items__c();
    objBItem2.Input_Clearance__c=obj2.id;
    lstBlockItems.add(objBItem2);
    insert lstBlockItems;
    
    update lstInputCls;
    Test.stopTest();
  }
}
/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 23-May-2013
    @ Description   : An Apex calss to 
    @ Last Modified By  :   Harshita Sawlani
    @ Last Modified On  :   24-May-2013
    @ Last Modified Reason  :   
****************************************************************************/
public class CPContentSubscription_Ext{
String logedinuser;
User u = new user();
//Constructor
public CPContentSubscription_Ext(){
ProductGroup = new List<String>();
Doctype = new List<String>();
logedinuser = UserInfo.getUserId();
u = [Select id,Library__c,Document_Type__c from user where id =:logedinuser];
if(u.Library__c != null)
ProductGroup.addall(u.Library__c.split(','));
if(u.Document_Type__c != null)
Doctype.addall(u.Document_Type__c.split(','));
}
/**********Function for subscription**********/
public void Subscribe(){
String Libr;
for(String s: ProductGroup){
if(Libr == null || Libr == '')
  Libr = s;
else
  Libr = Libr + ',' + s;
}
u.Library__c = Libr;
String Doctyp;
for(String s: Doctype){
if(Doctyp == null || Doctyp == '')
  Doctyp = s;
else
  Doctyp = Libr + ',' + s;
}
u.Document_Type__c = Doctyp;
update u;
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Subscribed for Library:'+ Libr + ' and for Document Type:' + Doctyp)); 
}

/*******Getter Setter function for Library(Product group) field of the content*****/
 Public List<String> ProductGroup{get;set;}
 Public List<SelectOption> options = new List<SelectOption>();
 Public void setOptions(List<SelectOption> opt)
 {
   options = opt;
 }
 Public List<SelectOption> getoptions()
        {
            options = new List<SelectOption>();
            
            Schema.Describefieldresult productfieldDesc = Product2.Product_Group__c.getDescribe();
            system.debug('%%%%%%%%'+ productfieldDesc);
            for(Schema.Picklistentry picklistvalues: productfieldDesc.getPicklistValues()) {
            options.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
           
            return options;
         }

/**********Getter Setter Function for Product Group ******************/

Public List<String> Doctype{get;set;}
 Public List<SelectOption> optionsdoc = new List<SelectOption>();
 Public void setOptionsdoc(List<SelectOption> opt)
 {
   optionsdoc = opt;
 }
 Public List<SelectOption> getoptionsdoc()
        {
            optionsdoc = new List<SelectOption>();
            Schema.Describefieldresult productfieldDesc = ContentVersion.Document_type__c.getDescribe();
            system.debug('%%%%%%%%'+ productfieldDesc);
            for(Schema.Picklistentry picklistvalues: productfieldDesc.getPicklistValues()) {
            optionsdoc.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
            return optionsdoc;
         }
         

}
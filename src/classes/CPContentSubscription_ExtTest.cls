/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 21-June-2013
    @ Description   :  Test class for CPContentSubscription_Ext class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   08-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest /*(SeeAllData = true)*/

Public class CPContentSubscription_ExtTest{

        //Method for calling CPContentSubscription_Ext class
        
        static testmethod void testCPContentSubscription_Ext(){
            Test.StartTest();
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            Integer RndNum = crypto.getRandomInteger();
            System.runAs ( thisUser ) {
            
            	
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                
                Account a; 
                
                /*Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
                insert actCntry;*/
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState ='teststate' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com'+RndNum,emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@test.com'+RndNum/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
            }
                user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com'+RndNum ];
                
                system.runAs(usr){
                
                    usr.Library__c = 'Test Library';
                    usr.Document_Type__c = 'Test Document';
                    Update usr ;
                    
                    CPContentSubscription_Ext contsub = new CPContentSubscription_Ext();
                    contsub.Subscribe();
                    List<SelectOption> selopt = contsub.getoptions();
                    List<SelectOption> selpots= contsub.getoptionsdoc();
                    List<SelectOption> obj = new List<SelectOption> ();
                    contsub.setOptions(obj);
                    contsub.setOptionsdoc(obj);
            }
            Test.StopTest();
    }
        
    
}
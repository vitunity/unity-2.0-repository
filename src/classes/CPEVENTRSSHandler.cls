global class CPEVENTRSSHandler  implements Schedulable {
 
    global CPEVENTRSSHandler() {
        
    }
    
    global void execute(SchedulableContext c) {
        updateEventsData();
    } 
@future(callout=true)
static global void updateEventsData() {
List<Dom.XMLNode> recentPosts = CPEVENTRSSHandler.getRSSFeed(Label.CpEventRSSUrl);
List<Event_Webinar__c> blogEntries = new List<Event_Webinar__c>();
List<String> webid = new List<String>();
List<Event_Webinar__c> entriesToAdd = new List<Event_Webinar__c>();
String listOfBlogs = '';
System.Debug('# OF POSTS FOUND:' +recentPosts.size());
for(Dom.XMLNode post : recentPosts) {
blogEntries.add(CPEVENTRSSHandler.convertFeedToEventData(post));
}
System.Debug('# OF ENTRIES FOUND:' +blogEntries.size());
for(Event_Webinar__c bp : blogEntries) {
webid.add(bp.Web_Id__c);
}
List<Event_Webinar__c> blogs = [SELECT Id,URL__c,Location__c,Title__c, Web_Id__c from Event_Webinar__c WHERE Web_Id__c IN :webid];
for(Event_Webinar__c blogEntry : blogEntries) {
Boolean added = false;
for(Event_Webinar__c blog : blogs) {
if(blog.Web_Id__c == blogEntry.Web_Id__c) { added = true; }
}
if(!added) { entriesToAdd.add(blogEntry); }
}
if(entriesToAdd.size()>0)
{
insert entriesToAdd;
CPEVENTRSSHandler.sendEmailNotice();
}

}
static global List<Dom.XMLNode> getRSSFeed(string URL) {
Http h = new Http();
HttpRequest req = new HttpRequest();
// url that returns the XML in the response body
req.setEndpoint(url);
req.setMethod('GET');
HttpResponse res = h.send(req);
Dom.Document doc = res.getBodyDocument();
Dom.XMLNode rss = doc.getRootElement();
System.debug('@@' + rss.getName());
List<Dom.XMLNode> rssList = new List<Dom.XMLNode>();
for(Dom.XMLNode child : rss.getChildren()) {
System.debug('@@@' + child.getName());
for(Dom.XMLNode channel : child.getChildren()) {
System.debug('@@@@' + channel.getName());
if(channel.getName() == 'item') {
rssList.add(channel);
}
}
}
return rssList;
}
static global Event_Webinar__c convertFeedToEventData(Dom.XMLNode post) {
Event_Webinar__c bp = new Event_Webinar__c();
Integer tagIndex = 0;
for(Dom.XMLNode child : post.getChildren()) {
// System.debug('@@@@ -- @' + child.getName());
if(child.getName() == 'guid') {bp.Web_Id__c = child.getText();}
if(child.getName() == 'title') { bp.Title__c = child.getText(); }
if(child.getName() == 'pubDate')
 { 
 String SplitElemet=child.getText();
 List<String> StrDate=SplitElemet.split('to');
 if(StrDate.size()>1)
 {
 System.debug('DebugLogs---->'+StrDate);
 bp.From_Date__c =Date.valueof(StrDate[0]);
 bp.To_Date__c=Date.valueof(StrDate[1]);
 }
 else
 {
  bp.From_Date__c =Date.valueof(StrDate[0]);
  bp.To_Date__c=Date.valueof(StrDate[0]);
 }
  }
  
if(child.getName() == 'creator')
 {
 
   String Splitfocus=child.getText();
    IF(Splitfocus.contains('Radiation Oncology'))
    {
     bp.Radiation_Oncology__c=True;
    }
    IF(Splitfocus.contains('Brachytherapy'))
    {
     bp.Brachytherapy__c=True;
    }
    IF(Splitfocus.contains('Medical Onc.'))
    {
     bp.Medical_Oncology__c=True;  
    }
    IF(Splitfocus.contains('Radiosurgery'))
    {
      bp.Radiosurgery__c=True;
    }
    IF(Splitfocus.contains('Proton Therapy'))
    {
     bp.Proton__c=True;
    }
    
   }  
if(child.getName() == 'link') { 
String Externalurl=child.getText();
if(Externalurl!='https://www.varian.com/')
{
bp.URL_Path_Settings__c = child.getText(); 

}

}
if(child.getName() == 'description') { bp.Location__c = child.getText(); }
}
return bp;
}
global static void sendEmailNotice() {
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
mail.setToAddresses(Label.CPRSSFEEDEMAIL.split(','));
mail.setSubject ('EVENTS RSS FEED STATUS');
mail.setHTMLBody('<h3>********Events RSS FEED UPLOADED SUCESSFULLY IN SALESFORCE*********</h3>');
Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
}
 
 }
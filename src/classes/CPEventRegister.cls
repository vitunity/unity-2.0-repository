/*************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 22-Apr-2013
    @ Description   : An Apex class to display all the required fields for Registering to a event.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
Public class CPEventRegister
{
    public String PrimaryRole {get;set;} 
    public String FMV{get;set;} 
    public String mess{get;set;} 
    public String Meals{get;set;} 
    Public String EvntName {get; set;}
    Public String EvntLanpageurl{get; set;}
    Public String AcctName {get; set;}
    Public String StateId {get; set;}
    Public String CustZip {get; set;}
    Public String CustCity {get; set;}
    Public String CustStreet {get; set;}
    Public String CustState {get; set;}
    Public String CustCountry {get; set;}       
    Public String CustEmail {get; set;}
    Public String CustFname {get; set;}
    Public String CustLname {get; set;}
    Public String CustTitle {get; set;}
    Public String CustPhone {get; set;}
    Public String CustLicense {get; set;}
    Public String CustNPI {get; set;}
    Public String CustLicenseTp {get; set;} 
    Public String EvntASRT {get; set;}
    Public String EvntMDCB {get; set;}
    Public boolean disble {get; set;}
    Public boolean disables {get; set;}
    Public boolean display{get; set;}
    Public String dspMsg {get; set;}
    Public String confirmerror{get; set;}
    public Event_Webinar__c featEventSumm{get;set;} 
    public string strBanner{get; set;}
    Public String AcctCountry {get; set;}
    Public String Regsid{get; set;}
    String EvntId = null;
    public Boolean isGuest{get;set;}
    Event_Webinar__c evnt;
    public string Usrname{get;set;}
    public string shows{get;set;}
    public Boolean attachvis{get;set;}
     public Boolean attachPresVis{get;set;}
    public List<Presentation__c> presentations{get;set;}
    Public CPEventRegister(){
        presentations = new list<Presentation__c>();
       shows='none';
       /* if(UserInfo.getUserType() == 'CspLitePortal'){
           isGuest = false;
        }else{
          isGuest = true;
        } */
        EvntId = System.currentPageReference().getParameters().get('evnt');
        FMV=System.currentPageReference().getParameters().get('FMV');
        mess=System.currentPageReference().getParameters().get('mess');
       Regsid=System.currentPageReference().getParameters().get('Regid');
        If(EvntId != null){
                //evnt = [select Id,title__c,ASRT_MDCB_ID_No_on_registration__c, URL_Path_Settings__c, Needs_MyVarian_registration__c,Reportable__c from Event_Webinar__c where id =: EvntId];
                evnt =[Select Id, e.Location__c, e.Institution__c, e.From_Date__c,ASRT_MDCB_ID_No_on_registration__c, Banner__c, Title__c, To_Date__c, Description__c,
                        URL_Path_Settings__c,Needs_MyVarian_registration__c,Reportable__c, (select id, title__c from Webinars__r where private__c = false), (SELECT Id, name FROM Attachments) From Event_Webinar__c e 
                        where recordtype.name = 'Events' and id = :EvntId];
                EvntName = evnt.title__c;
                EvntLanpageurl=evnt.URL_Path_Settings__c;
                disble = evnt.Needs_MyVarian_registration__c;
                disables=evnt.ASRT_MDCB_ID_No_on_registration__c;
        }
        if(evnt.Attachments.size()>0)
        {
        attachvis=true;
        } 
        if(mess!=null) {
              dspMsg='true';
              confirmerror='Thank you for registering for the ' + EvntName + ' event.';
        }
        else dspMsg='false';
        
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];        
        String usrContId = usr.contactid;
        if(usrContId != null)
        {
            isGuest = false;
            Contact c = [select FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
            
            CustFname = c.FirstName;
            CustLname = c.LastName;
            AcctName = c.Account.name;
            CustCity = c.MailingCity;
            CustStreet = c.MailingStreet;
            CustState = c.MailingState;
            CustCountry = c.MailingCountry;
            CustEmail = c.Email;
            CustTitle = c.title;
            CustPhone = c.phone;
            CustZip = c.MailingPostalCode;
          /*  Event_Webinar__c Eweb = [Select Id,Event_Meal_FMV__c from Event_Webinar__c where id =:evnt.Id];
            FMV=Eweb .Event_Meal_FMV__c;*/
        }
        else{
          isGuest = true;
        }
        if(EvntId !=null) {
                //Event_Webinar__c Eweb = [Select Id,Title__c,Banner__c,Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Event_Meal_FMV__c,URL__c,From_Date__c,To_Date__c,Description__c,Location__c,Reportable__c 
                                                                        //from Event_Webinar__c where id =:evnt.Id];
            Event_Webinar__c Eweb = [Select Id, e.Location__c, e.Institution__c, e.From_Date__c,ASRT_MDCB_ID_No_on_registration__c, Banner__c, Title__c, To_Date__c, Description__c,
                        URL_Path_Settings__c,Needs_MyVarian_registration__c,Reportable__c,Registration_is_closed_for_event__c,Event_Meal_FMV__c,URL__c, (select id, title__c from Webinars__r where private__c = false), (SELECT Id, name FROM Attachments) From Event_Webinar__c e 
                        where recordtype.name = 'Events' and id = :EvntId];                                                     
            FMV=Eweb.Event_Meal_FMV__c;
            featEventSumm=Eweb;
             if(Eweb.Banner__c != null && Eweb.Banner__c.length() > 93)
             {
         
        strBanner = Eweb.Banner__c.substring(Eweb.Banner__c.indexof('src')+5,Eweb .Banner__c.indexof('src')+94);
       
       }
       System.debug('@@@@Test11111'+strBanner );
        presentations = [Select p.Title__c, p.Speaker__c, p.Product_Affiliation__c, 
                       p.Name, p.Institution__c, p.Id, p.Event__r.To_Date__c, 
                       p.Event__r.From_Date__c, p.Event__r.Location__c, p.Event__r.Title__c, p.Event__r.Description__c,
                       p.Event__c, p.Description__c, p.Date__c,(SELECT Id, name FROM Attachments LIMIT 1) From Presentation__c p where Event__c = :EvntId];
                       
        }
        if(presentations.size()>0)
         {
         attachPresVis=true;
         }      
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null) {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
       }
    }
    
    Public List<SelectOption> getoptions()
    {
        List<SelectOption> options = new List<SelectOption>();
             
        options.add(new selectoption('-----','-----'));
        for(Regulatory_Country__c cn : [select name from Regulatory_Country__c order by Name]) {
        options.add(new selectoption(cn.name, cn.name)); }
        return options; 
       
    } 
    
         
    Public List<SelectOption> Role= new List<SelectOption>();
    Public List<SelectOption> getRole(){
        Schema.Describefieldresult RoleDesc = Registration__c.Primary_Role__c.getDescribe();
        system.debug('%%%%%%%%'+ RoleDesc );
        Role.add(new selectoption('Select...','Select...'));
        for(Schema.Picklistentry picklistvalues: RoleDesc.getPicklistValues()) {
        Role.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return Role;
    } 
    
    Public List<SelectOption> Mealoption= new List<SelectOption>();
    Public List<SelectOption> getMealoption(){
        Schema.Describefieldresult MealDesc = Registration__c.Meal_option__c.getDescribe();
        system.debug('%%%%%%%%'+ MealDesc );
        Mealoption.add(new selectoption('Select...','Select...'));
        for(Schema.Picklistentry picklistvalues: MealDesc.getPicklistValues()) {
        Mealoption.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return Mealoption;
    } 
    
    
    Public void eventSubmit()
    {

       Registration__c Reg = new Registration__c();
        Reg .National_Provider_Identifier_NPI__c = CustNPI;
        Reg .Medical_License__c  = CustLicense;
        Reg .License_Type__c  = CustLicenseTp;
        Reg .Event_Name__c  = evnt.title__c;
        Reg .ASRT_ID__c  = EvntASRT;
        Reg .MDCB_ID__c  = EvntMDCB;
        Reg .State_ID__c = StateId;
        Reg .Meal_option__c = Meals;
        system.debug(' ------ EventID ----- ' + evnt.Id);
        If(EvntId != null){
        Reg.Event_Webinar_Registration__c = evnt.Id;}
        Reg.id=Regsid;
        update Reg ;
        dspMsg='true';
        /*ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Thank you for registering for the ' + EvntName + ' Event' );
                ApexPages.addMessage(myMsg); */
           confirmerror='Thank you for registering for the ' + EvntName + ' Event';
    }

      Public PageReference  eventRgsSubmit()
    {
        Registration__c Rgstration = new Registration__c();
        Rgstration.First_Name__c = CustFname;
        Rgstration.Last_Name__c  = CustLname;
        Rgstration.Institution__c  = AcctName;
        Rgstration.City__c  = CustCity;
        Rgstration.Address__c = CustStreet;
        Rgstration.State__c  = CustState;
        Rgstration.Country__c  = CustCountry;
        Rgstration.E_mail__c  = CustEmail;
        Rgstration.Title__c  = CustTitle;
        Rgstration.Telephone__c  = CustPhone;
        Rgstration.Zip_Postal_Code__c  = CustZip; 
        Rgstration.State_ID__c  = StateId;
        Rgstration.National_Provider_Identifier_NPI__c = CustNPI;
        Rgstration.Medical_License__c  = CustLicense;
        Rgstration.License_Type__c  = CustLicenseTp;
       //  if(CustCountry=='USA')
       //   {
      //  Rgstration.Event_Name__c  = '0';
     //   }
     //   else
     //   {
         Rgstration.Event_Name__c  = evnt.title__c;
    //    }
        Rgstration.ASRT_ID__c  = EvntASRT;
        Rgstration.MDCB_ID__c  = EvntMDCB;
        system.debug(' ------ EventID ----- ' + EvntId);
        If(EvntId != null){
        Rgstration.Event_Webinar_Registration__c = evnt.Id;
        Rgstration.Url_Path_Setting__c = evnt.URL_Path_Settings__c;
        Rgstration.Primary_Role__c=PrimaryRole;
        Rgstration.Event_Meal_FMV__c=FMV;
        system.debug(' ------ EventID1 ----- ' + evnt.Id);}
        try
        {
            insert Rgstration;
            String Result=Rgstration.id;
            System.debug('insert reg id---->'+Result);
                Event_Webinar__c Eweb = [Select Id, Name, No_of_Registrations__c, Online_Registration_Limit__c ,Registration_is_closed_for_event__c,Reportable__c from Event_Webinar__c where id =:evnt.Id];
                     Integer RegCount  = [Select count() from Registration__c where Event_Webinar_Registration__r.id =:evnt.Id];
                     System.debug('Test Registration---->'+RegCount);
                       System.debug('***No of registrations****'+Eweb.No_of_Registrations__c);
                     /*  if(Eweb.No_of_Registrations__c==null )
                           Eweb.No_of_Registrations__c =0;
                                   
                       Eweb.No_of_Registrations__c = Eweb.No_of_Registrations__c+1;
                       System.debug('***No of registrations after click****'+Eweb.No_of_Registrations__c);
                       if(Eweb.Online_Registration_Limit__c == Eweb.No_of_Registrations__c){
                          Eweb.Registration_is_closed_for_event__c = True;  } */ 
          if(Eweb.Online_Registration_Limit__c == RegCount){
                  Eweb.Registration_is_closed_for_event__c = True;  
                  update Eweb;
          }
          
          if((CustCountry=='USA') && (Eweb.Reportable__c)) {
            PageReference pg = new PageReference('/apex/CPEventRegisterUS?evnt='+evnt.Id+'&FMV='+FMV+'&Regid='+Result);
                pg.setRedirect(true);
                return pg;
          }
          else {
                        PageReference pg = new PageReference('/apex/CPEventRegister?evnt='+evnt.Id+'&mess='+mess);
                pg.setRedirect(true);
                return pg;

         /* if(isGuest == true)
          {
                PageReference pg = new PageReference('/apex/CpMyVarianHomePage');
                pg.setRedirect(true);
                return pg;
           }
           else
           {
            PageReference pg = new PageReference('/apex/CpHomePage');
                pg.setRedirect(true);
                return pg;
           }*/
          }
         
        }
        
       
            /* This email notification is moved to workflow CPRegisterEvents
            if(Rgstration.Id != null)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'You have successfully registered for ' + EvntName + ' Event' );
                ApexPages.addMessage(myMsg); 

                EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'Event_Registration_Information' limit 1];
                String[] toaddress = new  String[]{CustEmail};
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setReplyTo(System.Label.MVsupport);
                mail.setToAddresses(toaddress);
                mail.setTemplateID(ETeplt.Id);
                mail.setTargetObjectId('003c0000007b79r');
                mail.setWhatId(Rgstration.Id);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                
            }
            */
       
        catch(Exception e)
        {   
            if(e.getmessage().contains('registration limit')){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Error Ocured: The registration limit for this event has been reached and/or the event registration is now closed. Thank you for your interest.');
             ApexPages.addMessage(myMsg);
            }else{
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Error Ocured: ' + e.getMessage() );
             ApexPages.addMessage(myMsg);
             }
                return null;
              /*  PageReference pg = new PageReference('/apex/CpMyVarianHomePage');
                pg.setRedirect(true);
                return pg;*/
        }
        
    }
    
    
    
}
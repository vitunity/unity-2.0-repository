/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CPEventRegister class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CPEventRegisterTest{

    // Method for initiating CPEventRegister class
    
    static testmethod void testCPEventRegister(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Integer RndNum = crypto.getRandomInteger();
        System.runAs ( thisUser ) {
        	
        	
                        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u;
            Account a;  
            
            /*Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
            insert actCntry;*/
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test',Title='Technical Consultant',MailingStreet= 'Test',MailingCity='Test',MailingState = 'Test',MailingPostalCode='500072',phone='0402358966'); 
            insert con;
        
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='testAK@testorg.com'+RndNum,emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@test.com'+ RndNum/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
    }
    
    
        user usr = [Select Id, ContactId, Name from User where email=:'testAK@testorg.com'+RndNum];
         
         System.runAs (usr){
        Event_Webinar__c event = new Event_Webinar__c();
        event.Title__c = 'Test Event2';
        event.Location__c = 'India';
        event.Banner__c='This banner created for test class and this data will not be commited to database because we are using starttest and stop test';
        event.No_of_Registrations__c = 0;
        event.Online_Registration_Limit__c = 2;
        event.To_Date__c = system.today()-1;
        event.From_Date__c = system.today()-2;
        event.Needs_MyVarian_registration__c=True;
        event.Registration_is_closed_for_event__c= false;
        
       
       
        Insert event;
        
         Registration__c Reg = new Registration__c();       
        Reg.Event_Name__c  = event.title__c;        
        Reg.Meal_option__c = 'Accept Meals';
        Reg.Event_Webinar_Registration__c=event.id;
        Reg.Address__c='testadd';
        Reg.City__c='testcity';
        Reg.Country__c='India';
        Reg.E_mail__c='a@bc.com';
        Reg.First_Name__c='name';
        Reg.Institution__c='testinst';
        Reg.Last_Name__c='Lanme';
        Reg.State__c='teststate';
        Reg.Telephone__c='123456789';
        Reg.Title__c='testtitle';
        Reg.Zip_Postal_Code__c='123';
         insert reg;
        System.currentPageReference().getParameters().put('Regid',Reg.id);

        update reg;
        
        system.debug('Inserted Event in Test class'+event.Id);
        ApexPages.currentPage().getParameters().put('evnt' ,event.Id);
        
        Event_Webinar__c events1 = [Select Id, Name ,No_of_Registrations__c,Online_Registration_Limit__c ,Registration_is_closed_for_event__c  from Event_Webinar__c where Id=:event.Id];

        CPEventRegister events =  new CPEventRegister();
        List<SelectOption> So = events.getoptions();
        events.eventRgsSubmit();
        events.getRole();
        events.getMealoption();
        events.eventSubmit();
        //events.eventSubmit();
        
        }
            
            
             Test.StopTest();
         }
     }
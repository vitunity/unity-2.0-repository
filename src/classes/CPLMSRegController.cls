Public class CPLMSRegController
{
 public string shows{get;set;}
  public string Usrname{get;set;}
  Public String IsErrMssgVisible{get;set;}
  Public String Isdialoguevisible{get;set;}
  public CPLMSRegController()
  {
   if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
          shows='block';
          system.debug('############' + shows);
         
       }
       else
       {
       shows='none';
       }
       Isdialoguevisible='none';
       
  }
  public void SendEmailTOVit()
  { 
  if(ApexPages.currentPage().getParameters().get('ErrSrc')=='SABA' || ApexPages.currentPage().getParameters().get('ErrSrc')=='OKTA')
       {
       if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
      User  Usrdetail=[Select Id,FirstName, LastName,Email  From User where id = : Userinfo.getUserId() limit 1];
        String[] toAddresses = new String[] {system.label.VITAccEmail};
        String    EmailBody='';
        IF(Usrdetail.FirstName!=NULL)
        {
        EmailBody='<br/>User First Name:&nbsp;'+Usrdetail.FirstName;
        }
        IF(Usrdetail.LastName!=NULL)
        {
         EmailBody=EmailBody+'<br/><br/>User Last Name:&nbsp;'+Usrdetail.LastName+'<br/><br/>Email Address:&nbsp;'+Usrdetail.Email +'<br/><br/>Error Source:&nbsp;'+ApexPages.currentPage().getParameters().get('ErrSrc');

        }
        
            Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
            MailTicket.setSubject('Error occurred in SABA/ OKTA');
           
            MailTicket.setToAddresses(toAddresses);
            MailTicket.setHTMLBody(EmailBody);
           
            MailTicket.setReplyTo(System.Label.MVsupport);       
            MailTicket.setSaveAsActivity (false);
            for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                MailTicket.setOrgWideEmailAddressId(owa.id); 
                } 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
       }
       else
       {
      id Contid = [select id,ContactId  from User where  id = : Userinfo.getUserId()].ContactId ;
       Contact c = [select FirstName, LastName,Email from contact where Id =: Contid];
       String[] toAddresses = new String[] {system.label.VITAccEmail};
       String    EmailBody='';
        IF(c.FirstName!=NULL)
        {
        EmailBody='<br/>User First Name:&nbsp;'+c.FirstName;
        }
        IF(c.LastName!=NULL)
        {
         EmailBody=EmailBody+'<br/><br/>User Last Name:&nbsp;'+c.LastName+'<br/><br/>Email Address:&nbsp;'+c.Email +'<br/><br/>Error Source:&nbsp;'+ApexPages.currentPage().getParameters().get('ErrSrc');

        }
       // String    EmailBody='<br><br><B>User First Name:&nbsp;</B>'+c.FirstName+'<br></br><br></br><B>User Last Name:&nbsp;</B>'+c.LastName+'<br></br><br></br><B>Email Address:&nbsp;</B>  '+c.Email +'<br></br><br></br><b>Error Source:&nbsp;</b> '+ApexPages.currentPage().getParameters().get('ErrSrc');
            Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
            MailTicket.setSubject('Error occurred in SABA/ OKTA');
           
            MailTicket.setToAddresses(toAddresses);
            MailTicket.setHTMLBody(EmailBody);
           
            MailTicket.setReplyTo(System.Label.MVsupport);       
            MailTicket.setSaveAsActivity (false);
            for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                MailTicket.setOrgWideEmailAddressId(owa.id); 
                } 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
        }    
     /*   EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'VIT_EmailNotification' limit 1];
        
        Messaging.SingleEmailMessage mails = new Messaging.SingleEmailMessage(); 
        mails.setReplyTo(System.Label.MVsupport);       
        mails.setTemplateID(ETeplt.Id);
        mails.setTargetObjectId(Contid);
        mails.setToAddresses(toAddresses);
        mails.setSaveAsActivity(false);
         // Use Organization Wide Address
                for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                mails.setOrgWideEmailAddressId(owa.id); 
                } 

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mails });*/
         IsErrMssgVisible='true';
       } 
       else
       {
       IsErrMssgVisible='false';
       }
 

      
  }
   public void SendEmail()
  {  
   try
        { 
         if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId != null)
       
       {
        id Contid = [select id,ContactId  from User where  id = : Userinfo.getUserId()].ContactId ;
        
         String[] toAddresses = new String[] {system.label.LMSAccEmail};
       //EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'CPLMS_Registration' limit 1];
        List<EmailTemplate> ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'CPLMS_Registration' limit 1];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setReplyTo('learningcenter@varian.com');
        if (ETeplt.size()>0)
        {      
        mail.setTemplateID(ETeplt[0].Id);
        }
        else
        {
        mail.setHTMLBody('testclass');
        }
        mail.setTargetObjectId(Contid);
        mail.setToAddresses(toAddresses);
         // Use Organization Wide Address
                for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                mail.setOrgWideEmailAddressId(owa.id); 
                } 

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
          
        }
        else
        {/*
         User  Usrdetail=[Select Id,FirstName, LastName,Email  From User where id = : Userinfo.getUserId() limit 1];
        String[] toAddresses = new String[] {system.label.LMSAccEmail};
        EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'CPLMS_RegistrationInternal' limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setReplyTo(System.Label.MVsupport);       
        mail.setTemplateID(ETeplt.Id);
       mail.setTargetObjectId(Usrdetail.Id);
       mail.setWhatId(Usrdetail.Id);
        mail.setToAddresses(toAddresses);
        mail.setSaveAsActivity(false);
         // Use Organization Wide Address
                for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                mail.setOrgWideEmailAddressId(owa.id); 
                } 

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
         
        }
        }
          catch(DMLException excp)
{
    ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, excp.getMessage() );
    ApexPages.addMessage(msg);  
                               
} 
  }
}
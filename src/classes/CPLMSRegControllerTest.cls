/*************************************************************************\
    @ Author        : 
    @ Date      : 
    @ Description   :  Test class for CPLMSRegController class.
    @ Last Modified By :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/

@isTest(SeeAllData=true)

Public class CPLMSRegControllerTest
{
 
    //Test Method for initiating CpMyAccount Class
     
    static testmethod void testCPLMSRegController() 
    {      
        Profile prfl = [select id from profile where name='VMS MyVarian - Customer User']; 
        
        Account a = new Account(name='test2',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx' ,Country__c='Test',Legal_Name__c ='Testing'); 
        insert a; 
        
        Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate'); 
        insert con;
        
        user usr1 = new User(alias = 'st123', ContactId = con.Id, Subscribed_Products__c=true,email='testuser876@testorg.com',emailencodingkey='UTF-8', lastname='lTest',languagelocalekey='en_US',localesidkey='en_US', profileid = prfl.Id, timezonesidkey='America/Los_Angeles',username='testuser876@testorg.com'); 
        insert usr1;       
        
        //Contact con2=new Contact(Lastname='test2',RAQA_Contact__c=true,FirstName='Singh2',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate'); 
        //insert con2;
        //profile sysadmn = [select id from profile where name='System Administrator']; 
        //user usr2 = new User(alias = 'st12323', ContactId = con2.Id, Subscribed_Products__c=true,email='testuser8736@testorg.com',emailencodingkey='UTF-8', lastname='lTest',languagelocalekey='en_US',localesidkey='en_US', profileid = sysadmn.Id, timezonesidkey='America/Los_Angeles',username='testuser8736@testorg.com'); 
        //insert usr2; 
        
        ApexPages.currentPage().getParameters().put('ErrSrc','SABA');
        CPLMSRegController inst2 = new CPLMSRegController();
        inst2.SendEmailTOVit();
      //  inst2.SendEmail();
        
        System.runAs (usr1)                // Running as portal user
        {
            CPLMSRegController inst = new CPLMSRegController();
            inst.SendEmailTOVit();
            inst.SendEmail();
        }
        /*System.runAs (usr1)                // Running as portal user
        {
            CPLMSRegController inst = new CPLMSRegController();
            inst.SendEmailTOVit();
            inst.SendEmail();
        }*/
        
    }
}
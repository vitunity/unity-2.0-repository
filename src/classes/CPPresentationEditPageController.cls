/*************************************************************************\
      
       
****************************************************************************/
public class CPPresentationEditPageController 
{
public String keyPrefix{get;set;}
private ApexPages.StandardController stdController;
    public CPPresentationEditPageController (ApexPages.StandardController controller) 
    {  
       Schema.DescribeSObjectResult r = Event_Webinar__c.sObjectType.getDescribe();
       keyPrefix = r.getKeyPrefix();// ends here
       this.stdController = controller;
       
    }
    public PageReference doSave() 
        {
         
         Presentation__c newPresObj = (Presentation__c)stdController.getRecord();
         system.debug('Test1--->'+newPresObj );
         newPresObj.Event__c=newPresObj.Event__c;
         newPresObj.Title__c=newPresObj.Title__c;
         newPresObj.Institution__c=newPresObj.Institution__c;
         newPresObj.Presentation_Recording_URL__c=newPresObj.Presentation_Recording_URL__c;
         newPresObj.Speaker__c=newPresObj.Speaker__c;
         newPresObj.Product_Affiliation__c=newPresObj.Product_Affiliation__c;
         newPresObj.Date__c=newPresObj.Date__c;
         newPresObj.Region__c=newPresObj.Region__c;
         newPresObj.Published__c=newPresObj.Published__c;

         
          ApexPages.StandardController stdControllernew = new ApexPages.StandardController(newPresObj); // ends here
        PageReference ret;
        try{
            ret = stdControllernew.save();
            return ret;
        }catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
              return null;
        }
         
        }
        public PageReference SaveNew(){
        
        PageReference pr = doSave();
        System.debug('aebug == '+pr);
        if(pr != null){
            
            PageReference prnew = new PageReference('/apex/CpPresentationPublish');
            prnew.setRedirect(true);
            return prnew;
        }
        return pr;  
    }  
     public pagereference docancel()
       {
       Schema.DescribeSObjectResult r = Presentation__c.sObjectType.getDescribe();
       keyPrefix = r.getKeyPrefix();// ends here
        Id issueId = stdController.getId();
        if(issueId !=null)
        {
       PageReference pagereferencealias= new PageReference('/?retURL=%2F'+issueId);
         pagereferencealias.setRedirect(true);
         return pagereferencealias;
         }
         else
         {
         PageReference pagereferencealias= new PageReference('/'+keyPrefix);
         pagereferencealias.setRedirect(true);
         return pagereferencealias;
         }
       } 
       public PageReference SaveClose() 
        {
        Presentation__c newPresObj = (Presentation__c)stdController.getRecord();
         system.debug('Test1--->'+newPresObj );
         newPresObj.Event__c=newPresObj.Event__c;
         newPresObj.Title__c=newPresObj.Title__c;
         newPresObj.Institution__c=newPresObj.Institution__c;
         newPresObj.Presentation_Recording_URL__c=newPresObj.Presentation_Recording_URL__c;
         newPresObj.Speaker__c=newPresObj.Speaker__c;
         newPresObj.Product_Affiliation__c=newPresObj.Product_Affiliation__c;
         newPresObj.Date__c=newPresObj.Date__c;
         newPresObj.Region__c=newPresObj.Region__c;
         newPresObj.Published__c=newPresObj.Published__c;
         
         
        ApexPages.StandardController stdControllernew = new ApexPages.StandardController(newPresObj); // ends here
        PageReference ret;
        ret = stdControllernew.save();
        Id issueId = stdController.getId();
        PageReference pagereferencealias= new PageReference('/?retURL=%2F'+issueId );
        pagereferencealias.setRedirect(true);
        return pagereferencealias;
        }
  }
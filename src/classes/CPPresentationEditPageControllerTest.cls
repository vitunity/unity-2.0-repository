@isTest 

Public class CPPresentationEditPageControllerTest{

    //Test Method for initiating CpPresentationDetailpageController class
    
    static testmethod void testPresentationEditPageController(){
        Test.StartTest();
           Presentation__c preseobj = new Presentation__c();
            preseobj.Title__c = 'test_09_may';
            preseobj.Speaker__c = 'Test Speaker';
            preseobj.Product_Affiliation__c = '4DITC';
            preseobj.Date__c= System.Today();
            preseobj.Region__c= 'Test,NA';
            preseobj.Published__c=true;
            preseobj.Institution__c='test';
            Insert preseobj; 
        ApexPages.StandardController controller1=new ApexPages.StandardController(preseobj); 
         CPPresentationEditPageController alias = new CPPresentationEditPageController(controller1);
         alias.doSave();
         alias.SaveNew();
         alias.docancel();
         alias.SaveClose();
         Test.StopTest();
    }
}
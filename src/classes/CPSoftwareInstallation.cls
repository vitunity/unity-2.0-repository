Public class CPSoftwareInstallation
{
 public String Usrname{get;set;}
 public String shows{get;set;}
 public String psrc{get; set;}
    public class swInstall
    {
        public list<Software_Installation__c> listSInstallation {get; set;}
        public string strCategory {get; set;}

        public swInstall(Software_Installation__c sInstallation)
        {           
            strCategory = sInstallation.Category__c;
            listSInstallation = new list<Software_Installation__c>{sInstallation};
        }
    }
    
    public list<swInstall> liSInstall {get; set;}
    private map<String, swInstall> mapSInstall = new map<String, swInstall>();
    
    Public CPSoftwareInstallation()
    {   
        shows = 'none';
        psrc = ApexPages.currentPage().getParameters().get('psrc');
        
        User un = [Select ContactId,alias from user where id =: UserInfo.getUserId() limit 1];
        if(un.ContactId == null)
        {
          shows = 'block';
          Usrname = un.alias;
        }
        liSInstall = new list<swInstall>();
        Software_Installation__c[] swIns;
        If(psrc=='vpt'){
        swIns = [Select Title__c, Content_Id__c, ContentID__c, Multi_Languages__c, URL__c,ExternalUrl__c, Category__c From Software_Installation__c where RecordType.Name = 'VPT' order by Title__c];
        }
        else{
        swIns = [Select Title__c, Content_Id__c, ContentID__c, Multi_Languages__c, URL__c,ExternalUrl__c, Category__c From Software_Installation__c where RecordType.Name != 'VPT' order by Title__c];
        }
        system.debug(' ------- size of Records ------ ' + swIns.size());
        for(Software_Installation__c sInstallation : swIns)
        {
            if(mapSInstall.get(sInstallation.Category__c) == null)
            {
                mapSInstall.put(sInstallation.Category__c, new swInstall(sInstallation));                
                liSInstall.add(mapSInstall.get(sInstallation.Category__c));
                system.debug('@@@@@@@@' + liSInstall);
            }
            else
            {                
                mapSInstall.get(sInstallation.Category__c).listSInstallation.add(sInstallation);
                system.debug(' ------- Title of Records ------ ' + mapSInstall.get(sInstallation.Category__c).listSInstallation + ' ---- ctr ---- ' + sInstallation.Category__c);
            }
        }
    }
    
}
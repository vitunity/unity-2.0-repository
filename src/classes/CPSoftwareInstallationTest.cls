/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CPSoftwareInstallation class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest(seealldata=true)

Public class CPSoftwareInstallationTest{

    //Test Method for CPSoftwareInstallation class
    
    static testmethod void testCPSoftwareInstallation(){
        Test.StartTest();
        
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
        Integer RndNum = crypto.getRandomInteger();
        
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        User u;
        Account a; 
          
        a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
        insert a;  

         Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test',MailingState ='teststate' ); 
        insert con;
        
         u = new User(alias = 'standt', Subscribed_Products__c=true,email='TestAKK@testorg.com'+RndNum,emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@test.com'+RndNum/*,UserRoleid=r.id*/); 
    
      insert u; // Inserting Portal User

        
               
           
            
            RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
             ContentVersion testContentInsert =new ContentVersion(); 
       testContentInsert.ContentURL='http://www.google.com/'; 
       testContentInsert.Title ='Google.com'; 
      testContentInsert.RecordTypeId = ContentRT.Id; 
      insert testContentInsert; 
      ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id];
     
     ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
     ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
      newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
         newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
      insert newWorkspaceDoc;   
             
              
             Document_List__c dl = new   Document_List__c();
                
            dl.Description__c = 'Test Document inserted in test class for testing ';
            dl.Title__c = 'Software Installation and Upgrades Oncology Information System';
            insert dl;
                
             Software_Installation__c  sft = new Software_Installation__c();
             sft.Document_List__c=dl.Id;
             sft.ContentID__c = testContentInsert.id;
             sft.URL__c = 'https://varian--sfpq.cs7.my.salesforce.com/sfc/#version?selectedDocumentId='+testContent.ContentDocumentId;
             insert sft;
             
               
                
            
             
                  
        CPSoftwareInstallation SI = new CPSoftwareInstallation();
        }
          Test.StopTest();
     }
 }
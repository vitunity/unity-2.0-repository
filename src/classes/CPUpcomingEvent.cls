/*************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 24-Apr-2013
    @ Description   : An Apex class to get the list of Upcoming Events.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
Public class CPUpcomingEvent
{    
    public string Usrname{get;set;}
    public string shows{get;set;}
    public class Edate
    {
        public list<Event_Webinar__c> listEvents {get; set;}
        public list<Event_Webinar__c> listEvents1 {get; set;}
        public string strMnthDate {get; set;}

        public Edate(Event_Webinar__c sEvents)
        {   //DateTime.valueOf(e.to_date__c+':00').format('MMM - yyyy')        
            strMnthDate = DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM yyyy');
            listEvents = new list<Event_Webinar__c>{sEvents};
            listEvents1 = new list<Event_Webinar__c>{sEvents};
        }
    }
    Public String RecentEvents{get; set;}
    Public String UpComingEvents{get; set;}
    public String Request{get;set;}
    public Boolean isGuest{get;set;}
    public boolean notAttchEvent {get; set;}
    public boolean presntEvent {get; set;}
    public list<Edate> liEDate {get; set;}
    public list<Edate> liEDate1 {get; set;}
    
    
    
    
    
    
     map<string, Edate> mapEvents = new map<string, Edate>();
    private map<string, Edate> mapEvents1 = new map<string, Edate>();
    Public CPUpcomingEvent()
    {
       
   shows='none';
   
   // fetchEvents();
        User usr = [Select ContactId,usertype, Profile.Name from user where Id=: UserInfo.getUserId()];        
        String usrContId = usr.contactid;
        if(usrContId != null)
            isGuest = false;
        else
            isGuest = true;
     Request = ApexPages.currentPage().getParameters().get('cid');

     if (Request == 'UpEvents')
        {
        RecentEvents='none';
        UpComingEvents='Block';
        }
        if (Request == 'RecEvents')
        {
        RecentEvents='block';
        UpComingEvents='none';
        }
        
        liEDate = new list<Edate>();
        liEDate1 = new list<Edate>();
        Event_Webinar__c[] upgEvent;
        List<String> Ids=New List<String>();
        List<GroupMember> grplmember = new List<GroupMember>();
        grplmember = [Select GroupId,UserOrGroupId,Group.Name from GroupMember where Group.Name=:Label.VMS_MyVarian_Partners and UserOrGroupId =: UserInfo.getuserId()];
        for(GroupMember gm: grplmember)
        {
        Ids.add(gm.UserOrGroupId);
        }
        SYSTEM.DEBUG('^^^^'+Ids);
        if(usrContId==null)
       {
       
        if(usr.usertype!= label.guestuserlicense)
        {
        SYSTEM.DEBUG('^^^^NotGuest');
        upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true   order by To_Date__c asc];

        }
        if(usr.usertype == label.guestuserlicense)
        {
       upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true  and Internal_Event__c=false order by To_Date__c asc];

        }
       }
       if(usrContId!=null)
       {
         if(Ids.size() > 0)
        {
        
         upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true   order by To_Date__c asc];
        }
        else
        {
        upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true  and Internal_Event__c=false order by To_Date__c asc];

        }
      }
        Event_Webinar__c[] recentEvent ;
        if(usrContId==null)
       {
       
        if(usr.usertype!= label.guestuserlicense)
        {
          SYSTEM.DEBUG('^^^^NotGuest');
          recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true order by To_Date__c desc];
        }
        if(usr.usertype == label.guestuserlicense)
        {
         recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true and Internal_Event__c=false order by To_Date__c desc];
        }
       }
       if(usrContId!=null)
       {
         if(Ids.size() > 0)
        {
        
          recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true order by To_Date__c desc];       
        }
        else
        {
          recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true and Internal_Event__c=false order by To_Date__c desc];
        }
      }
        for(Event_Webinar__c sEvents : upgEvent)
        {
            if(mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')) == null)
            {
                mapEvents.put(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy'), new Edate(sEvents));
                system.debug('----------- --------- +++++ ' + mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
                system.debug('---url--'+sEvents.URL__c );
                liEDate.add(mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
            }
            else
            {
                mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')).listEvents.add(sEvents);
            }
        }
        for(Event_Webinar__c sEvents : recentEvent)
        {
            if(mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')) == null)
            {
                mapEvents1.put(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy'), new Edate(sEvents));
                system.debug('----------- --------- +++++ ' + mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
                liEDate1.add(mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
            }
            else
            {
                mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')).listEvents1.add(sEvents);
            }
        }
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
              Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
              shows='block';
              isGuest=false;
          }
       }
    }
    public list<Event_Webinar__c> upcmgEvents{get;set;}  
    public list<Event_Webinar__c> recentEventss{get;set;}  
    public boolean recentEvntBln0{get;set;}
    public boolean recentEvntBln1{get;set;}  
    public boolean upcmgEvntBln0{get;set;}  
    public boolean upcmgEvntBln1{get;set;}
  public string upcmgEvntStr0{get;set;}
  public string upcmgEvntStr1{get;set;}
      public string recentEvntStr0{get;set;}
  public string recentEvntStr1{get;set;}
    public void fetchEvents()
    {
    
        upcmgEvents = new list<Event_Webinar__c>();
        recentEventss = new list<Event_Webinar__c>();
        
        upcmgEvents = [Select Id, e.Location__c, e.Institution__c, e.From_Date__c, Title__c, To_Date__c,CME_Event__c,
                URL_Path_Settings__c,Active__c,Description__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Online_Registration_Limit__c From Event_Webinar__c e 
                        where recordtype.name = 'Events' and To_Date__c >= today order by To_Date__c asc limit 2];
        recentEventss = [Select Id,e.Location__c, e.Institution__c, e.From_Date__c, Title__c, To_Date__c,CME_Event__c, 
                URL_Path_Settings__c,Active__c,Description__c,URL__c,Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Online_Registration_Limit__c From Event_Webinar__c e 
                        where recordtype.name = 'Events' and To_Date__c < today order by To_Date__c desc limit 2];
                        
        if(recentEventss.size() == 1)
        {
            fillBlnkEvents(recentEventss);
            recentEvntBln1 = true;
        }
        else if(recentEventss.size() == 0)
        {
            fillBlnkEvents(recentEventss);
            fillBlnkEvents(recentEventss);
            recentEvntBln0 = true;
            recentEvntBln1 = true;
        }
         else if(recentEventss.size() == 2)
    {
      recentEvntStr0 = recentEventss [0].Id;
      recentEvntStr1 = recentEventss [1].Id;
      System.debug('Testrecent'+recentEvntStr0);
    }
        if(upcmgEvents.size() == 1)
        {
            fillBlnkEvents(upcmgEvents);
            upcmgEvntBln1 = true;
        }
        else if(upcmgEvents.size() == 0)
        {
            fillBlnkEvents(upcmgEvents);
            fillBlnkEvents(upcmgEvents);
            upcmgEvntBln0 = true;
            upcmgEvntBln1 = true;
        }
    else if(upcmgEvents.size() == 2)
    {
      upcmgEvntStr0 = upcmgEvents[0].Id;
      upcmgEvntStr1 = upcmgEvents[1].Id;
    }
    
    System.debug('aebug : upcmgEvents = '+upcmgEvents);
    System.debug('aebug : recentEvents = '+recentEventss);
    }
    public void fillBlnkEvents(list<Event_Webinar__c> cmnEvents)
    {
        Event_Webinar__c rcntEvnt = new Event_Webinar__c();
        rcntEvnt.Title__c = 'No Event';
        rcntEvnt.Location__c = 'NY';
        rcntEvnt.To_Date__c = system.today();
        rcntEvnt.From_Date__c = system.today();
        rcntEvnt.Needs_MyVarian_registration__c = false;
        cmnEvents.add(rcntEvnt);
    }
          public id testeventid{get; set;}  

   /* public void countRegister() {
       
     System.debug('***Entered the countRegister***'+testeventid);
    
     Event_Webinar__c Eweb = [Select Id, Name, No_of_Registrations__c, Online_Registration_Limit__c ,Registration_is_closed_for_event__c from Event_Webinar__c where id =:testeventid];
       
       System.debug('***No of registrations****'+Eweb.No_of_Registrations__c);
       if(Eweb.No_of_Registrations__c==null )
           Eweb.No_of_Registrations__c =0;
                   
       Eweb.No_of_Registrations__c = Eweb.No_of_Registrations__c+1;
       System.debug('***No of registrations after click****'+Eweb.No_of_Registrations__c);
       if(Eweb.Online_Registration_Limit__c == Eweb.No_of_Registrations__c){
          Eweb.Registration_is_closed_for_event__c = True;
       }
       
       update Eweb; 
       
      // return null;  
   }*/
        public void countRegisters() {
       
     System.debug('***Entered the countRegister***'+testeventid);
    
     /*Event_Webinar__c Eweb = [Select Id, Name, No_of_Registrations__c, Online_Registration_Limit__c ,Registration_is_closed_for_event__c from Event_Webinar__c where id =:testeventid];
       
       System.debug('***No of registrations****'+Eweb.No_of_Registrations__c);
       if(Eweb.No_of_Registrations__c==null )
           Eweb.No_of_Registrations__c =0;
                   
       Eweb.No_of_Registrations__c = Eweb.No_of_Registrations__c+1;
       System.debug('***No of registrations after click****'+Eweb.No_of_Registrations__c);
       if(Eweb.Online_Registration_Limit__c == Eweb.No_of_Registrations__c){
          Eweb.Registration_is_closed_for_event__c = True;    
        }
       
       update Eweb; */
       
      // return null;  
   }    
    /***
    
    Public Event_Webinar__c[] getUpcmgEvent()
    {
        Event_Webinar__c[] upgEvent = [Select Id, Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c, From_Date__c, Title__c, To_Date__c From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() order by From_Date__c asc];
                        
        return upgEvent;
    }
    ***/


}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CPUpcomingEvent class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CPUpcomingEventTest{

    //Test Method for CPUpcomingEvent class
    
    static testmethod void testCPUpcomingEvent(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
            
             Regulatory_Country__c reg = new Regulatory_Country__c();
            reg.Name='Test' ; 
            insert reg ;
           
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Test', MailingState='teststate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
        }
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
        RecordType rt=[select id,name from RecordType where name=:'Events' and sobjectType=:'Event_Webinar__c'];
         
         System.runAs (usr){
           
        List<Event_Webinar__c> Event = new List<Event_Webinar__c>();
        Event.add(new Event_Webinar__c(Title__c = 'Test Event',
        Location__c = 'NY',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False,Private__c=false,Recordtypeid=rt.id));
        Event.add(new Event_Webinar__c(Title__c = 'Test Event1',
        Location__c = 'Us',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False,Private__c=false,Recordtypeid=rt.id));
        Event.add(new Event_Webinar__c(Title__c = 'Test Event2',
        Location__c = 'India',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False,Private__c=false,Recordtypeid=rt.id));
         insert Event;
            ApexPages.currentPage().getParameters().put('cid','UpEvents');
           // ApexPages.currentPage().getParameters().put('cid','RecEvents');
            CPUpcomingEvent  events =  new CPUpcomingEvent();
              events.fillBlnkEvents(Event);            
            events.fetchEvents();
           
            
        }
             Test.StopTest();
     }
     
     static testmethod void testCPUpcomingEvents(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
           
            Regulatory_Country__c reg1 = new Regulatory_Country__c();
            reg1.Name='Test' ; 
            insert reg1 ;
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Test', MailingState='teststate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
        }
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com'];
        RecordType rt=[select id,name from RecordType where name=:'Events' and sobjectType=:'Event_Webinar__c'];
         
        System.runAs (usr){         
            List<Event_Webinar__c> Event = new List<Event_Webinar__c>();
            Event.add(new Event_Webinar__c(Title__c = 'Test Event',
            Location__c = 'NY',
            To_Date__c = system.today(),
            From_Date__c = system.today(),
            Needs_MyVarian_registration__c=False,
            Private__c=false,Recordtypeid=rt.id));
            Event.add(new Event_Webinar__c(Title__c = 'Test Event1',
            Location__c = 'Us',
            To_Date__c = system.today()-1,
            From_Date__c = system.today(),
            Needs_MyVarian_registration__c=False,Varian_Event__c=true,Recordtypeid=rt.id));
            Event.add(new Event_Webinar__c(Title__c = 'Test Event2',
            Location__c = 'India',
            To_Date__c = system.today()-1,
            From_Date__c = system.today(),
            Needs_MyVarian_registration__c=False,Varian_Event__c=true,Recordtypeid=rt.id));
            insert Event;
            
            ApexPages.currentPage().getParameters().put('cid','RecEvents');
            CPUpcomingEvent  events =  new CPUpcomingEvent();
            events.fillBlnkEvents(Event);
            //events.countRegisters();
            events.fetchEvents();
            
        }
             Test.StopTest();
     }
 }
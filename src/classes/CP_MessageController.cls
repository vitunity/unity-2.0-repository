public class CP_MessageController {

    public List<MessageCenter__c> messagelist = new List<MessageCenter__c>();
    public List<MessageCenter__c> messagelist1 = new List<MessageCenter__c>();
    public List<wrapperclass> wrapperlist = new List<wrapperclass>();
    public String prdGrp{get;set;} 
    public String chkRegions{get;set;} 
    public list<String> productGrp{get;set;}
    private String soql {get;set;}
    public Map<String, Boolean> validUsers { get; private set; }
    public boolean flag{get;set;}
    public boolean tableflag{get;set;}
    public list<String> product{get;set;}
    public Id MessageId;
    public String val{get;set;}
    public map<id,MessageCenter__c> MapofMessageCenter{get;set;}
   public boolean Ispicvis{get;set;}
    public Integer TotalMess{get;set;}
    //public List<MessageCenter__c> result= new List<MessageCenter__c> ();
    public String result;
    public   User usr{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    public boolean isGuest{get;set;}
    //constructor
     public CP_MessageController (){
         usr = [select id,SmallPhotoUrl From User where id = : Userinfo.getUserId()];
         if(usr.SmallPhotoUrl.contains('profilephoto/005'))
         {
         Ispicvis=false;
         }
         else
         {
         Ispicvis=true;
         }
        prdGrp='';
        tableflag=false;
        shows='none';
       String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
        if(conCntry != null){
          chkRegions= [Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry limit 1].Portal_Regions__c;
          System.debug('CheckRegion--->'+chkRegions);   
       }
       
     soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c  from MessageCenter__c where Published__c = True order by Message_Date__c desc';

     fetchProds();
     //messageList=[select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True and  order by Message_Date__c desc];
     //runSearch();
     product= new list<String>();
     flag = True;
     
        system.debug('Current user id is---->'+Userinfo.getUserId());
        MapofMessageCenter = new map<id,MessageCenter__c>([select id,(select id,user__c from MessageViews__r where  user__c=:Userinfo.getUserId() limit 1) from MessageCenter__c]);
        system.debug('Current user id is---->'+MapofMessageCenter);
        
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }  
                                                         
    }

    //Logout method added by harshita
    public pagereference logoutmethod()
    {
        HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        String strToken = label.oktatoken;

        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
        sessionmap = usersessionids__c.getall();

        String authorizationHeader = 'SSWS ' + strToken;
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');

        try{
            String endpointgroup = Label.oktaendpointsessionkill;
            if(sessionmap!=null && sessionmap.containsKey(userinfo.getuserid()))
                endpointgroup += sessionmap.get(userinfo.getuserid()).session_id__c;

            reqgroup.setEndPoint(endpointgroup);
            reqgroup.setMethod('DELETE');
        
            if(!Test.IsRunningTest()){
                resgroup = httpgroup.send(reqgroup);
            }
        }catch(System.CalloutException e){
          system.debug(resgroup.toString());
        } 
        return new pagereference('/secur/logout.jsp'); 
    }
    
    // Method ends here
    public List<MessageCenter__c> getmessagelist(){
    return messagelist;
    
      }
    
   
 
    // Wrapper Class for styling purpose
    
      public class wrapperclass{
                public MessageCenter__c msg1{get;set;}
                public MessageCenter__c msg2{get;set;}
    
                }
    
    public List<wrapperclass> getwrapperlist()
      {
           System.debug('------messageList.size()--------'+messageList.size());
           for(integer i=0;i<messageList.size();i++)
              {
                  wrapperclass w = new wrapperclass();
                  w.msg1 =messagelist[i];
                  System.debug('MesageList1'+w.msg1);
                  i++;
              if(i<messagelist.size())
                   {
                       w.msg2 = messagelist[i];
                      }
          wrapperlist.add(w);
          System.debug('Message List'+wrapperlist);
                 }
     
         return wrapperlist;
         
        }
       
       public Pagereference redirecttodetailpage()
      {
        MessageId=System.currentPageReference().getParameters().get('abc');
        PageReference newPage = new PageReference('/apex/CpPresentationDetailpage?Id='+MessageId);
        newPage.setRedirect(true);
        return newPage;
    
    }
    
    
    //method to check if message belongs to contact region.
    public void checkMessageRegion(List<MessageCenter__c> lstMessage_List){         
          if(lstMessage_List.size() > 0){ 
                for(MessageCenter__c mc_obj:lstMessage_List){
                    if(mc_obj.User_ids__c!=null && mc_obj.Region__c!=null && (chkRegions!='' && chkRegions!=NULL) ){
                            if(!mc_obj.User_ids__c.contains(UserInfo.getUserId()) && (mc_obj.Region__c.CONTAINS('ALL')||mc_obj.Region__c.CONTAINS(chkRegions))){
                            messageList.add(mc_obj);
                            System.Debug('Test1--->'+messageList);  
                        }
                    }
                    else if(mc_obj.User_ids__c==null && mc_obj.Region__c!=null && (chkRegions!='' && chkRegions!=NULL)){
                        //if((chkRegions!='' || chkRegions!=NULL)&& mc_obj.Region__c!=null) {                           
                            if(mc_obj.Region__c.CONTAINS('ALL')|| mc_obj.Region__c.CONTAINS(chkRegions)){        
                                messageList.add(mc_obj);                                             
                            }
                        //}                     
                    }                   
                    else if(mc_obj.User_ids__c!=null && (chkRegions=='' || chkRegions==NULL)){                      
                        if(!mc_obj.User_ids__c.contains(UserInfo.getUserId())){
                            messageList.add(mc_obj);                            
                        }
                    }
                    else if(mc_obj.User_ids__c==null && (chkRegions=='' || chkRegions==NULL)){  
                            messageList.add(mc_obj);
                    }
                }
       }
    
    }
       
     public void fetchProds() {       
                  
          if(chkRegions==null ||  chkRegions=='')
          {
          chkRegions='';
          }      
          else
          {
         // chkRegions='\''+chkRegions+'\'';
           }        
         productGrp = new List<String>();
            
            
        Set<String> allProducts = new Set<String>();
        List<String> allprodlist = new list<String>();
        //for intrnal users get all the products 
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null){
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
                for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
                    allProducts.add(picklistvalues.getlabel());
                }
            }       
        }
        //else get product based on account
        else{       
            allProducts.addall(CpProductPermissions.fetchProductGroup());
            
        }
        allprodlist.addall(allProducts);
        
        for(String str: allprodlist){
        system.debug('ProductList'+allprodlist);
                if(prdGrp=='')
                prdGrp='\''+str+'\'';
                else
                prdGrp=prdGrp+', \''+str+'\'';
              //system.debug('^^^^^^^^^^^^^^^'+prdGrp);
              productGrp.add(str);
                          
       } 
       system.debug('^^^^^^^^^^^^^^^'+prdGrp);
    productGrp.sort();
          if(prdGrp!='' && chkRegions!=''){
          system.debug('testss---->');
       soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True  and Product_Affiliation__c includes ('+prdGrp+')  order by Message_Date__c desc';
      }
         
    // CountMess= [select count()  from MessageCenter__c where Published__c = True AND MessageFlag__c!=True  and Product_Affiliation__c includes (prdGrp) ];      
       system.debug('****************************'+soql);
       List<MessageCenter__c> lstMessage_List= Database.query(soql);
       checkMessageRegion(lstMessage_List);
      
       system.debug('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'+messageList);
       system.debug('@@@@prdGrp'+prdGrp);
       system.debug('@@@@prdGrp'+chkRegions);
        
       integer CountMessg=messageList.size();
       integer  countmessview   = [Select count() from MessageView__c where user__c=: UserInfo.getUserId()]; 
       system.debug('Viewmess-->'+countmessview); 
       system.debug('CountMessg-->'+CountMessg);   
        TotalMess= CountMessg-countmessview;
        
       //messageList=lstMessage_List;
       if(messageList!=null){
       tableflag=true;
       }
       //messageList=[select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True and  order by Message_Date__c desc];
    
           //system.debug('---------------'+productGrp);
    } 
       
  public PageReference runSearch() {
 
  String productGrp=Apexpages.currentPage().getParameters().get('productGrp');
  String val=Apexpages.currentPage().getParameters().get('val');
  system.debug('value selected ----- '+val);
  system.debug('ProductGrp selected ----- '+productGrp);
 
    if(productGrp==null)
    productGrp='-Any-';
    
    if(val==null)
    val='';
    
    if(productGrp =='-Any-' && val==''){
    System.debug('Test1---->');
    soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True  and Product_Affiliation__c includes ('+prdGrp+') order by Message_Date__c desc';
    }
      
   //soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True';
  
  if(productGrp =='')
      {
     
        //soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True  and Product_Affiliation__c in ('+prdGrp+') order by Message_Date__c desc';
      System.debug('Test2---->');
        soql = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True and Product_Affiliation__c includes ('+prdGrp+') order by Message_Date__c desc';
        System.debug('-----soqllist when blank value passed----'+soql);
 
      }

   if(productGrp=='-Any-' && val!='') {
        flag=false;
       // if (!productGrp.equals(''))
       //     {
       System.debug('Test3---->');
              system.debug('productGrp');
              soql= 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True and title__c LIKE \'%'+String.escapeSingleQuotes(val)+'%\' order by Message_Date__c desc';
              //soql+= ' and Product_Affiliation__c in (\''+productGrp+'\')';
       //     }  
           
        }
   
   if(productGrp!='-Any-' && productGrp!=null && val!='') {
        flag=false;
       // if (!productGrp.equals(''))
       //     {
           System.debug('Test4---->');
              system.debug('productGrp');
              soql= 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True and Product_Affiliation__c includes (\''+productGrp+'\') and title__c LIKE \'%'+String.escapeSingleQuotes(val)+'%\' order by Message_Date__c desc';
            //soql+= ' and Product_Affiliation__c in (\''+productGrp+'\')';
       //     }  
           
        }
        
         if(productGrp!='-Any-' && productGrp!=null && productGrp != '' && val=='') {
        flag=false;
       // if (!productGrp.equals(''))
       //     {
            System.debug('Test5---->');
              system.debug('**************************************');
              soql= 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c,User_ids__c,Region__c from MessageCenter__c where Published__c = True and Product_Affiliation__c includes (\''+productGrp+'\') and title__c LIKE \'%'+String.escapeSingleQuotes(val)+'%\' order by Message_Date__c desc';
            //soql+= ' and Product_Affiliation__c in (\''+productGrp+'\')';
       //     }  
           
        }
        
        
        runQuery();
  return null;
 }
 
  public void runQuery() {
      System.debug('aebug : query = '+soql);
      List<MessageCenter__c> lstMessage_List= Database.query(soql);
      system.debug('--------'+lstMessage_List.size());
      wrapperlist = new List<wrapperclass>() ;
      messagelist= new List<MessageCenter__c>();
      List<MessageCenter__c> presentationList = new List<MessageCenter__c>();
      
      if(lstMessage_List.size() > 0){              
          for(MessageCenter__c p : lstMessage_List){                
              if(p.User_ids__c!=null){
                    if(!p.User_ids__c.contains(UserInfo.getUserId())){
                //for(String s: productGrp){
                //System.debug('---s value-------'+s);
                    //if((p.Product_Affiliation__c != null && p.Product_Affiliation__c.contains(s))|| s==''){
                     presentationList.add(p);
                     system.debug('presentationList'+presentationList);
                    //}
                    
              //  }
            }
            }
            else{
            presentationList.add(p);
            }
            }
            }
           
           checkMessageRegion(presentationList);
           //messagelist = presentationList;
            system.debug('---------------'+messageList);
            
            
       
       if(messageList!=null){
       tableflag=true;
       }
       else{
       tableflag=false;
       }
  }
  
  public PageReference readmessage(){
        MessageId=System.currentPageReference().getParameters().get('abc');
        System.debug('MessageId'+MessageId);
      /*  MessageCenter__c mess=new MessageCenter__c();
        mess.MessageFlag__c=true;
        mess.id=MessageId;
        update mess;*/
       // User usr = [Select id from user where Id=: UserInfo.getUserId()];        
       // String usrId = usr.id;
        MessageView__c messview=new MessageView__c();
        messview.Message_Center__c=MessageId;
        messview.User__c=UserInfo.getUserId();
        list<MessageView__c> matchid = new list< MessageView__c>();
        matchid =[select id,Message_Center__c from MessageView__c  where Message_Center__c=:MessageId and user__c=:Userinfo.getUserId() limit 1];
       if(matchid.size()==0)
        insert messview;
        PageReference pref = new PageReference('/apex/CpMessageCenterDetail?id='+MessageId);
        pref.setRedirect(true);
        return pref;
  }
   /*public PageReference search() {

    //String strTemp=value;
    if(strTemp==null || strTemp=='null')
    strTemp='';   
    
    if(strTemp!='')
    result = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True and Message__c like \'%'+value+'%\' order by Message_Date__c desc';
    else
    result = 'select id,Message_Date__c,title__c,product_affiliation__c,Published__c,Message__c from MessageCenter__c where Published__c = True order by Message_Date__c desc';        
    soql=result;
    runQuery();
   system.debug('results'+result);
 
  return null;
 }*/
  
}
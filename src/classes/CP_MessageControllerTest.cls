/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 10-June-2013
    @ Description   :  Test class for MessageController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CP_MessageControllerTest{

    //Test Method for MessageController class when Product Affiliation and Message center both are selected
    
    static testmethod void testMessageController(){
        
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;   
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Barbados',MailingState='teststate' ); 
            insert con;
          //  UserRole r= new UserRole(DeveloperName = 'TestRole', Name = 'Test Role');
         //   insert r;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcountry=new Regulatory_Country__c();
            regcountry.name='Barbados';
            insert regcountry;
        }
        
        Test.StartTest();
         MessageCenter__c msgcenter=new MessageCenter__c(Title__c='testmessage');
         insert msgcenter;
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
         System.currentPageReference().getParameters().put('abc',msgcenter.id);
            
            CP_MessageController MessageController = new CP_MessageController();
                List<MessageCenter__c> Mc  = MessageController.getmessagelist();
                MessageController.redirecttodetailpage();
                MessageController.readmessage();
                MessageController.runQuery();
                CP_MessageController.wrapperclass  wrpcls = new CP_MessageController.wrapperclass();
                MessageController.getwrapperlist();
            
                User ur = [Select Id,Name from User where Id=:UserInfo.getUserId()];
                MessageCenter__c messagecentre = new MessageCenter__c();
                    messagecentre.Message_Date__c= Date.today();
                    messagecentre.title__c='Test';
                    messagecentre.product_affiliation__c ='4DITC';
                    messagecentre.Published__c = True;
                    messagecentre.Message__c ='Test class';
                    messagecentre.User_ids__c = ur.Id ;
                    insert messagecentre;
            
                PageReference pageRef = Page.CpMessageCenter; 
                Test.setCurrentPage(pageref);
                ApexPages.currentPage().getParameters().put('productGrp' , messagecentre.product_affiliation__c);
                CP_MessageController Mssg = new CP_MessageController ();
                Mssg.runSearch();
           
                PageReference pageRef1 = Page.CpMessageCenter; 
                Test.setCurrentPage(pageref1);
                ApexPages.currentPage().getParameters().put('val' , messagecentre.Message__c);
                CP_MessageController Mesg = new CP_MessageController ();
                Mesg.runSearch();
            }
            // for internal user
            System.currentPageReference().getParameters().put('abc',msgcenter.id);
            
            CP_MessageController MessageController = new CP_MessageController();
                List<MessageCenter__c> Mc  = MessageController.getmessagelist();
                MessageController.redirecttodetailpage();
                MessageController.readmessage();
                MessageController.runQuery();
                CP_MessageController.wrapperclass  wrpcls = new CP_MessageController.wrapperclass();
                MessageController.getwrapperlist();
                
        Test.StopTest();
     
    }
    
    //Test Method for MessageController class Product Affiliation is null and only Message center is selected
    
    static testmethod void testMessageControllermethod(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Barbados', MailingState='teststate1' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcountry=new Regulatory_Country__c();
            regcountry.name='Barbados';
            insert regcountry;
            
            
            List<Product2> prod = new List<Product2>();
             
             Product2 prods = new Product2();
                prods.Name = 'ARIA for Medical Oncology';
                prods.Product_Group__c ='ARIA for Medical Oncology';
                prod.add(prods);
               
            Product2 prods1= new Product2();
                prods1.Name = 'ARIA Radiation Oncology';
                prods1.Product_Group__c ='ARIA Radiation Oncology';
                prod.add(prods1);
                
             Product2 prods2= new Product2();
                prods2.Name = 'Eclipse';
                prods2.Product_Group__c ='Eclipse';
                prod.add(prods2);
                
             Product2 prods3= new Product2();
                prods3.Name = 'RapidArc';
                prods3.Product_Group__c ='RapidArc';
                prod.add(prods3);
                
             Product2 prods4= new Product2();
                prods4.Name = 'TrueBeam';
                prods4.Product_Group__c ='TrueBeam';
                prod.add(prods4);
              
              insert prod;
                
                    
            List<SVMXC__Installed_Product__c> Insprd = new List<SVMXC__Installed_Product__c>();
            
            SVMXC__Installed_Product__c Inprd = new SVMXC__Installed_Product__c();
             Inprd.Name = 'Testing';
             Inprd.SVMXC__Product__c = prods.Id;
             Inprd.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd);
            
            SVMXC__Installed_Product__c Inprd1 = new SVMXC__Installed_Product__c();
             Inprd1.Name = 'Testing';
             Inprd1.SVMXC__Product__c = prods1.Id;
             Inprd1.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
             Insprd.add(Inprd1);
            
            SVMXC__Installed_Product__c Inprd2 = new SVMXC__Installed_Product__c();
             Inprd2.Name = 'Testing';
             Inprd2.SVMXC__Product__c = prods2.Id;
             Inprd2.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd2.SVMXC__Status__c = 'Installed';
             Inprd2.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd2);
            
            SVMXC__Installed_Product__c Inprd3 = new SVMXC__Installed_Product__c();
             Inprd3.Name = 'Testing';
             Inprd3.SVMXC__Product__c = prods3.Id;
             Inprd3.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd3.SVMXC__Status__c = 'Installed';
             Inprd3.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd3);
            
            SVMXC__Installed_Product__c Inprd4 = new SVMXC__Installed_Product__c();
             Inprd4.Name = 'Testing';
             Inprd4.SVMXC__Product__c = prods4.Id;
             Inprd4.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd4.SVMXC__Status__c = 'Installed';
             Inprd4.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd4);
            
          insert Insprd;
            

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
            
            
            MessageCenter__c messagecentre = new MessageCenter__c();
                messagecentre.Message_Date__c= Date.today();
                messagecentre.title__c='Test';
                messagecentre.product_affiliation__c ='ARIA Radiation Oncology';
                messagecentre.Published__c = True;
                messagecentre.Message__c ='Test class';
                messagecentre.User_ids__c = usr.Id ;
                insert messagecentre;
            
            PageReference pageRef = Page.CpMessageCenter; 
            Test.setCurrentPage(pageref);
            ApexPages.currentPage().getParameters().put('productGrp' , messagecentre.product_affiliation__c);
            CP_MessageController Mssg = new CP_MessageController ();
            Mssg.runSearch();
             
            PageReference pageRef1 = Page.CpMessageCenter; 
            Test.setCurrentPage(pageref1);
            ApexPages.currentPage().getParameters().put('val', messagecentre.Message__c);
            CP_MessageController Mesg = new CP_MessageController ();
            Mesg.runSearch();
         }
         
                
        Test.StopTest();

     }
     
     
     //Test Method for MessageController class
     
     static testmethod void testMessageControllermethod1(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
                        
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;   
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Barbados', MailingState='teststate2' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcountry=new Regulatory_Country__c();
            regcountry.name='Barbados';
            insert regcountry;
            
            
            List<Product2> prod = new List<Product2>();
             
             Product2 prods = new Product2();
                prods.Name = 'ARIA for Medical Oncology';
                prods.Product_Group__c ='ARIA for Medical Oncology';
                prod.add(prods);
               
            Product2 prods1= new Product2();
                prods1.Name = 'ARIA Radiation Oncology';
                prods1.Product_Group__c ='ARIA Radiation Oncology';
                prod.add(prods1);
                
             Product2 prods2= new Product2();
                prods2.Name = 'Eclipse';
                prods2.Product_Group__c ='Eclipse';
                prod.add(prods2);
                
             Product2 prods3= new Product2();
                prods3.Name = 'RapidArc';
                prods3.Product_Group__c ='RapidArc';
                prod.add(prods3);
                
             Product2 prods4= new Product2();
                prods4.Name = 'TrueBeam';
                prods4.Product_Group__c ='TrueBeam';
                prod.add(prods4);
              
              insert prod;
                
                    
            List<SVMXC__Installed_Product__c> Insprd = new List<SVMXC__Installed_Product__c>();
            
            SVMXC__Installed_Product__c Inprd = new SVMXC__Installed_Product__c();
             Inprd.Name = 'Testing';
             Inprd.SVMXC__Product__c = prods.Id;
             Inprd.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd);
            
            SVMXC__Installed_Product__c Inprd1 = new SVMXC__Installed_Product__c();
             Inprd1.Name = 'Testing';
             Inprd1.SVMXC__Product__c = prods1.Id;
             Inprd1.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
             Insprd.add(Inprd1);
            
            SVMXC__Installed_Product__c Inprd2 = new SVMXC__Installed_Product__c();
             Inprd2.Name = 'Testing';
             Inprd2.SVMXC__Product__c = prods2.Id;
             Inprd2.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd2.SVMXC__Status__c = 'Installed';
             Inprd2.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd2);
            
            SVMXC__Installed_Product__c Inprd3 = new SVMXC__Installed_Product__c();
             Inprd3.Name = 'Testing';
             Inprd3.SVMXC__Product__c = prods3.Id;
             Inprd3.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd3.SVMXC__Status__c = 'Installed';
             Inprd3.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd3);
            
            SVMXC__Installed_Product__c Inprd4 = new SVMXC__Installed_Product__c();
             Inprd4.Name = 'Testing';
             Inprd4.SVMXC__Product__c = prods4.Id;
             Inprd4.SVMXC__Serial_Lot_Number__c = 'Test';
             Inprd4.SVMXC__Status__c = 'Installed';
             Inprd4.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd4);
            
          insert Insprd;
            
          

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
        
        MessageCenter__c messagecentre = new MessageCenter__c();
            messagecentre.Message_Date__c= Date.today();
            messagecentre.title__c='Test';
            messagecentre.product_affiliation__c ='ARIA Radiation Oncology';
            messagecentre.Published__c = True;
            messagecentre.Message__c ='Test';
            messagecentre.User_ids__c = usr.Id ;
            insert messagecentre;
        
        PageReference pageRef = Page.CpMessageCenter; 
        Test.setCurrentPage(pageref);
        ApexPages.currentPage().getParameters().put('productGrp' , messagecentre.product_affiliation__c);
        CP_MessageController Mssg = new CP_MessageController ();
        Mssg.runSearch();
         
        PageReference pageRef1 = Page.CpMessageCenter; 
        Test.setCurrentPage(pageref1);
        ApexPages.currentPage().getParameters().put('val', messagecentre.Message__c);
        CP_MessageController Mesg = new CP_MessageController ();
        Mesg.runSearch();
        
    }
        Test.StopTest();
        
     }
     
     
     static testmethod void testLogoutMethod(){
        // Create usersessionids record.
        usersessionids__c currentSession = new usersessionids__c();
        currentSession.Name = Userinfo.getUserId();
        currentSession.session_id__c = Userinfo.getSessionId();
        
        insert currentSession;
        
        CP_MessageController controller = new CP_MessageController();
        pagereference pg = controller.logoutmethod();
        system.assert(pg != null);
     }
     
     static testmethod void testCheckMessageRegions(){
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                    
        Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
        insert a;   
        
        Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Barbados', MailingState='teststate2' ); 
        insert con;
        
       // UserRole roleid = [select id from UserRole where Name ='All']; 
       //  UserRole r= 
        User newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=*/); 
        
        insert newUser;
        
        
     
      
        
        system.runAs(newUser){
            Regulatory_Country__c regcountry=new Regulatory_Country__c();
            regcountry.name=con.MailingCountry;
            regcountry.Portal_Regions__c = 'Test Region';
            
            insert regcountry;
            
            MessageCenter__c messagecentre = new MessageCenter__c();
            messagecentre.Message_Date__c= Date.today();
            messagecentre.title__c='Test';
            messagecentre.product_affiliation__c ='ARIA Radiation Oncology';
            messagecentre.Published__c = True;
            messagecentre.Message__c ='Test';
            messagecentre.User_ids__c = newUser.Id;
            messagecentre.Region__c = 'Test Region';
            
            insert messagecentre;
            
            CP_MessageController controller = new CP_MessageController();
            
            controller.checkMessageRegion(new List<MessageCenter__c>{messagecentre});
        }
     }
     
     static testmethod void testCheckMessageRegions1(){
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                    
        Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
        insert a;   
        
        Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Barbados', MailingState='teststate2' ); 
        insert con;
        //UserRole r = new UserRole(DeveloperName = 'TestRole',Name = 'Test Role');
        //Insert r;
        User newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=r.id*/); 
        
        insert newUser;
        
        system.runAs(newUser){
            Regulatory_Country__c regcountry=new Regulatory_Country__c();
            regcountry.name=con.MailingCountry;
            regcountry.Portal_Regions__c = 'Test Region';
            
            insert regcountry;
            
            MessageCenter__c messagecentre = new MessageCenter__c();
            messagecentre.Message_Date__c= Date.today();
            messagecentre.title__c='Test';
            messagecentre.product_affiliation__c ='ARIA Radiation Oncology';
            messagecentre.Published__c = True;
            messagecentre.Message__c ='Test';
            messagecentre.Region__c = 'Test Region';
            
            insert messagecentre;
            
            CP_MessageController controller = new CP_MessageController();
            
            controller.checkMessageRegion(new List<MessageCenter__c>{messagecentre});
        }
     }
     
     static testmethod void testCheckMessageRegions2(){
            
        MessageCenter__c messagecentre = new MessageCenter__c();
        messagecentre.Message_Date__c= Date.today();
        messagecentre.title__c='Test';
        messagecentre.product_affiliation__c ='ARIA Radiation Oncology';
        messagecentre.Published__c = True;
        messagecentre.Message__c ='Test';
        messagecentre.Region__c = 'Test Region';
        
        insert messagecentre;
        
        CP_MessageController controller = new CP_MessageController();
        
        controller.checkMessageRegion(new List<MessageCenter__c>{messagecentre});
     }
     
}
/***************************************************************************
Author         : N/A
Date           : N/A
Description    : N/A
Project        : N/A
Change Log:
31-Aug-2017 - Divya Hargunani - STSK0012821: Defect: Content Upload New Version Error - Changed the version of Salesforce.com API to 40.0 and re-implemented the logic to iterate through parents documents only
*************************************************************************************/

public class CP_MultiLingual_Content {
    
    public void MultiLingual_Content(map < Id, contentversion > mapofcontentver, map < Id, contentversion > oldmapofcontentversion) {

    Set < contentversion > Lstcontenttoupdate = new Set < contentversion > ();

    // STSK0012821 : Added code to iterate through parent documents only 

    set < String > parentDocuments = new set < String > ();
    for (contentversion lstcont: mapofcontentver.values()) {
        if (lstcont.Parent_Documentation__c != null)
            parentDocuments.add(lstcont.Parent_Documentation__c);
    }
    for (contentversion lstcont: oldmapofcontentversion.values()) {
        if (lstcont.Parent_Documentation__c != null)
            parentDocuments.add(lstcont.Parent_Documentation__c);
    }
	
    system.debug('parentDocuments===='+parentDocuments);
        
    map < string, contentversion > Nameandcontentmap = new map < string, contentversion > (); // map of content title and content.
    map < string, contentversion > mapofParentdocumentandcontent = new map < string, contentversion > (); // map of parent document name and content.

    List < contentversion > ContentList = new List < contentversion > ();
    if (!test.isrunningtest()) {
        contentlist = [select Id, Document_Number__c, Title, Mutli_Languages__c, Parent_Documentation__c
            from contentversion where (Document_Number__c in: parentDocuments or Parent_Documentation__c in:  parentDocuments) and isLatest = True limit 100
        ];
        system.debug('contentlist===='+contentlist);
    } else {
        contentlist = [select Id, Document_Number__c, Title, Mutli_Languages__c, Parent_Documentation__c from contentversion where Id not IN: mapofcontentver.keyset() and isLatest = True limit 100];
    }

    for (contentversion lstcont: contentlist) {
        Nameandcontentmap.put(lstcont.Document_Number__c, LstCont);
        if (lstcont.Parent_Documentation__c != null && mapofParentdocumentandcontent.get(lstcont.Parent_Documentation__c) == null) {
            mapofParentdocumentandcontent.put(lstcont.Parent_Documentation__c, lstcont);
        }
    }

    /* Below logic will execute for contents(of current context)and check/uncheck multil language checkbox
    on parent document.*/
    for (ContentVersion con: mapofcontentver.values()) {
		system.debug('con.Parent_Documentation__c ==== '+con.Parent_Documentation__c);
        if (con.Parent_Documentation__c != null && con.Parent_Documentation__c != '' && con.Parent_Documentation__c != oldmapofcontentversion.get(con.Id).Parent_Documentation__c) {

            contentversion contv = Nameandcontentmap.get(con.Parent_Documentation__c);
            system.debug('con.contv ==== '+contv);
            if (contv != null) {
                contv.Mutli_Languages__c = True;
                Lstcontenttoupdate.add(contv);
            }
        }

        if (oldmapofcontentversion.size() > 0 && oldmapofcontentversion.get(con.Id).Parent_Documentation__c != null && con.Parent_Documentation__c != oldmapofcontentversion.get(con.Id).Parent_Documentation__c) {
            if (mapofParentdocumentandcontent.get(oldmapofcontentversion.get(con.Id).Parent_Documentation__c) == null) {
                contentversion contv = Nameandcontentmap.get(oldmapofcontentversion.get(con.Id).Parent_Documentation__c);
                system.debug('con.contv old ==== '+contv);
                if (contv != null) {
                    contv.Mutli_Languages__c = False;
                    Lstcontenttoupdate.add(contv);
                }
            }
        }

    }
    List < contentversion > LstContUniqueIdUpdate = new List < contentversion > ();
    // update the multi language check box on content.
    if (Lstcontenttoupdate.size() > 0) {
        LstContUniqueIdUpdate.addall(Lstcontenttoupdate);

        update LstContUniqueIdUpdate;
    }

}

}
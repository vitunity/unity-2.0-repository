/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 23-June-2013
    @ Description   :  Test class for CP_MultiLingual_Content class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest(seealldata = true)

Public class  CP_MultiLingual_ContentTest{

    
    //Test Method for CP_MultiLingual_Content when Parent Document is not null
    
    static testMethod void testCP_MultiLingual_Content()
   {
          Test.StartTest();
        
        Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity;Argus');
        insert pr;
      Product_Version__c pv = new Product_Version__c(product__c = pr.id,Major_Release__c=2,Minor_Release__c=1);
        insert pv;
           Product_Version__c prod_ver = [SELECT Version__c FROM Product_Version__c where Id =: pv.Id];
        
        RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
        ContentVersion testContentInsert =new ContentVersion(); 
             testContentInsert.ContentURL='http://www.google.com/'; 
             testContentInsert.Title ='Google.com'; 
             testContentInsert.RecordTypeId = ContentRT.Id; 
             insert testContentInsert; 
             ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id and IsLatest=True];
              
             ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
             ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
             newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
             newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
             insert newWorkspaceDoc;
             ContentWorkspace testWorkspace2 = [SELECT Id FROM ContentWorkspace WHERE Name='Argus']; 
             ContentWorkspaceDoc newWorkspaceDoc2 =new ContentWorkspaceDoc(); 
             newWorkspaceDoc2.ContentWorkspaceId = testWorkspace2.Id; 
             newWorkspaceDoc2.ContentDocumentId = testContent.ContentDocumentId; 
             insert newWorkspaceDoc2;
             
             //testContentInsert.FirstPublishLocationId = library.id;
             //testContentInsert.Parent_Documentation__c = testContentInsert.Title;
             testContentInsert.Document_Number__c = '121345';
             testContentInsert.Document_Version__c =prod_ver.Version__c;
             Update testContentInsert;
             
             Test.StopTest();
             
         }
         
         static testMethod void testCP_MultiLingual_Contentmethod2(){
           
           //child doc
            RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
             Test.StartTest();  
            ContentVersion testContent1Insert1 =new ContentVersion(); 
             testContent1Insert1.ContentURL='http://www.google.com/'; 
             testContent1Insert1.Title ='Google.com'; 
             testContent1Insert1.RecordTypeId = ContentRT.Id; 
             insert testContent1Insert1; 
             ContentVersion testContent1 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent1Insert1.Id and IsLatest=True]; 
             ContentWorkspace testWorkspace1 = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
             ContentWorkspaceDoc newWorkspaceDoc1 =new ContentWorkspaceDoc(); 
             newWorkspaceDoc1.ContentWorkspaceId = testWorkspace1.Id; 
             newWorkspaceDoc1.ContentDocumentId = testContent1.ContentDocumentId; 
             insert newWorkspaceDoc1;
             
             //testContent1Insert1.FirstPublishLocationId = library.id;
             testContent1Insert1.Parent_Documentation__c = '121345';
             testContent1Insert1.Document_Number__c = '1213456';
             Update testContent1Insert1;
             
             ContentVersion testContent1Insert2 =new ContentVersion(); 
             testContent1Insert2.ContentURL='http://www.google.com/'; 
             testContent1Insert2.Title ='Google.com'; 
             testContent1Insert2.RecordTypeId = ContentRT.Id; 
             insert testContent1Insert2; 
             ContentVersion testContent2 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent1Insert2.Id and IsLatest=True]; 
             ContentWorkspaceDoc newWorkspaceDoc2 =new ContentWorkspaceDoc(); 
             newWorkspaceDoc2.ContentWorkspaceId = testWorkspace1.Id; 
             newWorkspaceDoc2.ContentDocumentId = testContent2.ContentDocumentId; 
             insert newWorkspaceDoc2;
             testContent1Insert2.Parent_Documentation__c = '1213456';
             testContent1Insert2.Document_Number__c = '5878789';
             Update testContent1Insert2;
             
             ContentDocument testContDoc = [Select Id, IsArchived FROM ContentDocument WHERE Id =:testContent1.ContentDocumentId];
             testContDoc.IsArchived = true;
             update testContDoc;
            Test.StopTest();
           
         }
       static testMethod void testCP_MultiLingual_Contentmethod1()
   {
          Test.StartTest();  
           
             //ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id]; 
            ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
            
            RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
            
            
          /*   ContentVersion testContentInsert =new ContentVersion(); 
             testContentInsert.ContentURL='http://www.gmail.com/'; 
             testContentInsert.Title ='gmail.com'; 
             testContentInsert.FirstPublishLocationId=testWorkspace.id;
             testContentInsert.Parent_Documentation__c='test001';
             testContentInsert.RecordTypeId = ContentRT.Id; 
             insert testContentInsert; */
             
             //testContentInsert.Parent_Documentation__c='test002';
            // update testContentInsert;
             
             ContentVersion testContentInsert1 =new ContentVersion(); 
             testContentInsert1.ContentURL='http://www.google.com/'; 
             testContentInsert1.Title ='Google.com'; 
             testContentInsert1.RecordTypeId = ContentRT.Id; 
             insert testContentInsert1;
             //testContentInsert1.FirstPublishLocationId=testWorkspace.id;
             ContentVersion testContentInsert2 = [SELECT Id, Title, ContentDocumentId, Parent_Documentation__c FROM ContentVersion WHERE Id = :testContentInsert1.Id];
             ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
             newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
             newWorkspaceDoc.ContentDocumentId = testContentInsert2.ContentDocumentId; 
             insert newWorkspaceDoc;
               
               testContentInsert1.Parent_Documentation__c='test001';

             ContentVersion testContentInsert3 =new ContentVersion(); 
             testContentInsert3.ContentURL='http://www.google.com/'; 
             testContentInsert3.Title ='Google.com'; 
             testContentInsert3.FirstPublishLocationId=testWorkspace.id;
               testContentInsert3.Document_Number__c = 'test003';
               testContentInsert3.Parent_Documentation__c='test005';
             testContentInsert3.RecordTypeId = ContentRT.Id; 
             insert testContentInsert3; 
             
             
             
                            map<Id,contentversion> cont2 = new map<Id,contentversion>();
                             map<Id,contentversion> cont3 = new map<Id,contentversion>();
              //cont3.put(testContentInsert2.id,testContentInsert2);
               
               //ContentVersion testContentInsert1 =new ContentVersion(); 
             
            // testContentInsert1.FirstPublishLocationId=testWorkspace.id;
               testContentInsert2.Parent_Documentation__c='test003';
          //   testContentInsert1.RecordTypeId = ContentRT.Id; 
             //update testContentInsert2; 
             //cont2.put(testContentInsert2.id,testContentInsert2);
            // ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id]; 
           //  ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
           //  newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
           //  newWorkspaceDoc.contentdocumentId=testContent.ContentDocumentId;
             //insert newWorkspaceDoc; 
             //CP_MultiLingual_Content cpmult=new CP_MultiLingual_Content();
             //cpmult.MultiLingual_Content(cont2,cont3);
               
          Test.StopTest();
   }  
     static testMethod void testCP_MultiLingual_ContentChildUpdateFromParent()
           {
                  Test.StartTest();
                
                
                RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
               //  ContentWorkspace testWorkspace1 = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
                ContentVersion testContentInsert =new ContentVersion(); 
                     testContentInsert.ContentURL='http://www.google.com/'; 
                     testContentInsert.Title ='Google.com'; 
                     testContentInsert.RecordTypeId = ContentRT.Id; 
                 //    testContentInsert.Document_Number__c='1213456';
                   //  testContentInsert.origin = 'C';
                   //  testContentInsert.FirstPublishLocationId=testWorkspace1.ID;
                     insert testContentInsert; 
                     
                     ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id and IsLatest=True];
                      
                    ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity' ]; 
                     ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
                     newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
                     newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
                     insert newWorkspaceDoc;
                     
                     //testContentInsert.FirstPublishLocationId = library.id;
                     //testContentInsert.Parent_Documentation__c = testContentInsert.Title;
                     testContent.Document_Type__c='End of Support';
                     testContent.Mutli_Languages__c=True ;
                     testContent.TagCsv ='Test';
                     //testContent.Document_Version__c='30';
                     testContent.Document_Number__c='121345';
                     Update testContent ;
                    
                    ContentVersion testContentParent =new ContentVersion(); 
                     testContentParent.ContentURL='http://www.google.com/'; 
                     testContentParent.Title ='Google.com'; 
                     testContentParent.RecordTypeId = ContentRT.Id; 
                     insert testContentParent;
                     ContentVersion testContentParent1 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentParent.Id and IsLatest=True];
                     ContentWorkspaceDoc parentWorkspaceDoc =new ContentWorkspaceDoc(); 
                     parentWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
                     parentWorkspaceDoc.ContentDocumentId = testContentParent1.ContentDocumentId; 
                     insert parentWorkspaceDoc;
                     testContent.Document_Type__c='End of Support';
                     testContent.Mutli_Languages__c=True ;
                     testContent.TagCsv ='Test';
                     //testContent.Document_Version__c='30';
                     testContent.Document_Number__c='12134576';
                     Update testContent ;
                    
                       ContentVersion testContentInsert1 =new ContentVersion(); 
                     testContentInsert1.ContentURL='http://www.google.com/'; 
                     testContentInsert1.Title ='Google.com'; 
                    // testContentInsert11.FirstPublishLocationId=testWorkspace.id;
                     testContentInsert1.RecordTypeId = ContentRT.Id; 
                     //testContentInsert11.Parent_Documentation__c='121345';
                     insert testContentInsert1;
                     ContentVersion testContent3 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert1.Id and IsLatest=True];
                      
                     ContentWorkspaceDoc newWorkspaceDoc2 =new ContentWorkspaceDoc(); 
                     newWorkspaceDoc2.ContentWorkspaceId = testWorkspace.Id; 
                     newWorkspaceDoc2.ContentDocumentId = testContent3.ContentDocumentId; 
                     insert newWorkspaceDoc2;
                     ContentVersion testContent2 = [SELECT Document_Number__c,ContentDocumentId FROM ContentVersion where Id = :testContentInsert1.Id and IsLatest=True];
                       testContent2.Parent_Documentation__c='121345';
                       update testContent2;
                       ContentVersion testContent5 = [SELECT Document_Number__c,ContentDocumentId FROM ContentVersion where Id = :testContent2.Id and IsLatest=True];
                       testContent5.Parent_Documentation__c='12134576';
                       update testContent5;
                       ContentVersion testContent4 = [SELECT Document_Number__c,ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];
                      testContent4.Document_Number__c='121';
                     Update testContent4;
                     
                     ContentDocument testContDoc2 = [Select Id FROM ContentDocument WHERE Id =:testContent.ContentDocumentId];
                        delete testContDoc2;
                     Test.StopTest();
                     
                 }
}
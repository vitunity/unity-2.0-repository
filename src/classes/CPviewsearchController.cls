public class CPviewsearchController{

    //List of Properties    
    List<ContentVersion> ContentVersions ;
    public String Productvalue {get;set;}
    public String DocumentTypes {get;set;}
    public String VersionInfo {get;set;} 
    public String Document_TypeSearch {get;set;}
    public Decimal Document_Version {get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    public boolean isGuest{get;set;}
    public boolean IsHitechVis{get;set;}  
    public boolean IsCpARIAICD10Vis{get;set;} 
    
    public Map<String, List<ContentVersion>> mapCntWrkSpace {get; set;}    
    // ******* New Code start VVVV

    //For Product Info.
     set<string> ProductGroupSet;   
     String userid;   
    Public List<SelectOption> options = new List<SelectOption>();
    Public List<SelectOption> getoptions()
    {
        //set<string> ProductGroupSet = CpProductPermissions.fetchProductGroup();       
        //ProductGroupSet = CpProductPermissions.fetchProductGroup();       
        options.add(new selectoption('--Any--','--Any--'));
        for(string sr : ProductGroupSet) {
        
        options.add(new selectoption(sr, sr));
        
         }
         //INC4348266.Start
        List<selectoption> AccountSelectedOption= new list<selectoption>();
        Id accountId = [Select Id, AccountId From User where id = : Userinfo.getUserId() limit 1].AccountId;
        if(accountid <> null){
            Account Acc = [select Id,Selected_Groups__c  from Account where Id =: accountId];
            if(Acc.Selected_Groups__c  <> null){
                for(string s : (Acc.Selected_Groups__c).split(';')){
                    if(s<>null){
                        AccountSelectedOption.add(new selectoption(s.trim(),s.trim()));
                    }
                }
            }
        }
        options.addAll(AccountSelectedOption);
        options.sort();
        //INC4348266.End
        
        return options;
    } 

    //For Doc Types.      
    Public List<SelectOption> DocTypes= new List<SelectOption>();
    Public List<SelectOption> getDocTypes()
    {
        Schema.Describefieldresult DocTypesDesc = ContentVersion.Document_Type__c.getDescribe();        
        DocTypes.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
        DocTypes.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return DocTypes;
    } 

    
    public CPviewsearchController() 
    {
        shows='none';
        Document_TypeSearch = Apexpages.currentpage().getParameters().get('kwrd');
        ProductGroupSet = new set<string>();
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null){
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
                for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
                    ProductGroupSet.add(picklistvalues.getlabel());
                }
            }       
        }
        else{       
            ProductGroupSet = CpProductPermissions.fetchProductGroup();
        }
       
        
        userid = UserInfo.getUserId();      
        gtlContentVersions();
         if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
              Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
              shows='block';
              isGuest=false;
          }
       }
    }
       
     
    public List<ContentVersion> lContentVersions;
   
    public PageReference showrecords1() 
    {
        gtlContentVersions();
        return null;
    }
  // wrapper class for the content type on page
    public class contenttype{
      public String Contenttypevar{get;set;}
      
      public List<Presentation__c> prsntrecord{get;set;}
      public List<Event_Webinar__c> eventrecord{get;set;}
      public List<Event_Webinar__c> webinarrecord{get;set;}
      public List<MessageCenter__c> msgrecord{get;set;}
      public contenttype(){
        prsntrecord = new List<Presentation__c>();
        eventrecord = new List<Event_Webinar__c>();
        webinarrecord = new List<Event_Webinar__c>();
        msgrecord = new List<MessageCenter__c>();
      }
    }
    
    //Read message
    
    public PageReference readmessage(){
        String MessageId=System.currentPageReference().getParameters().get('abc');
        System.debug('MessageId'+MessageId);
      /*  MessageCenter__c mess=new MessageCenter__c();
        mess.MessageFlag__c=true;
        mess.id=MessageId;
        update mess;*/
       // User usr = [Select id from user where Id=: UserInfo.getUserId()];        
       // String usrId = usr.id;
        MessageView__c messview=new MessageView__c();
        messview.Message_Center__c=MessageId;
        messview.User__c=UserInfo.getUserId();
        list<MessageView__c> matchid = new list< MessageView__c>();
        matchid =[select id,Message_Center__c from MessageView__c  where Message_Center__c=:MessageId and user__c=:Userinfo.getUserId() limit 1];
       if(matchid.size()==0)
        insert messview;
        PageReference pref = new PageReference('/apex/CpMessageCenterDetail?id='+MessageId);
        pref.setRedirect(true);
        return pref;
  }

    

    public List<contenttype> contentypelist{get;set;}// List to hold the records for the events,presentation,etc.
    public List<ContentVersion> gtlContentVersions() 
    {
        
     
     contentypelist = new List<contenttype>();
     try 
        {
     String productgroupstr = '';
     if(productgroupset != null){
     for(String str: productgroupset)
         {
           if (productgroupstr != '')
            productgroupstr = productgroupstr + ',\'' + str + '\'';      
           else
            productgroupstr = productgroupstr + '\'' + str + '\'';
         }
      }
        
            Map<ID, String> mapWrkSpace;
            lContentVersions  = new List<ContentVersion>();
            string whr='';
            String whrprodprsevent = '';
            String temp;
            set<Id> conDocID;
            if(Document_TypeSearch != '' && Document_TypeSearch!= null)
            {
                temp = Document_TypeSearch + '*';
                if(Document_TypeSearch=='hitech' || Document_TypeSearch=='arra' || Document_TypeSearch=='hitec')
                {
                 IsHitechVis=true;
                 IsCpARIAICD10Vis=false;
                }
                if(Document_TypeSearch=='icd10' || Document_TypeSearch=='icd-10')
                {
                IsCpARIAICD10Vis=true;
                IsHitechVis=false;
                }
               
                 
               
            }
            else
            {
                temp = '*s.*';
                IsCpARIAICD10Vis=false;
                IsHitechVis=false;
            }
                
            if((Productvalue != '--Any--') && (Productvalue != null))
            {
                //whr = whr + 'and FirstPublishLocation.Name =: Productvalue';
                
                conDocID = new set<Id>();
                mapWrkSpace = new Map<ID, String>();
                for(ContentWorkspaceDoc conWorDoc :[Select c.ContentDocumentId, c.ContentWorkspace.Name From ContentWorkspaceDoc c where ContentWorkspace.Name =: Productvalue])
                {                   
                    conDocID.add(conWorDoc.ContentDocumentId); 
                    mapWrkSpace.put(conWorDoc.ContentDocumentId, conWorDoc.ContentWorkspace.Name);                  
                }  
                System.debug(' ----------- ----------------  : featWebinars =' + conDocID.size());  
                whr = 'and ContentDocumentId in : conDocID'; 
                whrprodprsevent = 'and Product_Affiliation__c INCLUDES ( \'' + Productvalue + '\')';
                productgroupstr = '\'' + Productvalue + '\'';           
            }
            else
            {
                conDocID = new set<Id>();
                mapWrkSpace = new Map<ID, String>();
                for(ContentWorkspaceDoc conWorDoc :[Select c.ContentDocumentId, c.ContentWorkspace.Name From ContentWorkspaceDoc c])
                {
                    //System.debug(' ----------- ----------------  : featWebinars =' + conWorDoc);
                    conDocID.add(conWorDoc.ContentDocumentId);
                    mapWrkSpace.put(conWorDoc.ContentDocumentId, conWorDoc.ContentWorkspace.Name);
                }
                whr = 'and ContentDocumentId in : conDocID';
                
            }
            
            if((DocumentTypes != '--Any--') && (DocumentTypes != null))
            {           
                whr = whr + ' and Document_Type__c=: DocumentTypes';
            }
              //  String searchquery = 'FIND {' + temp +'} IN ALL FIELDS RETURNING Event_Webinar__c(ID,URL_Path_Settings__c,Needs_MyVarian_registration__c,Product_Affiliation__c,title__c,To_Date__c,From_Date__c,Name,Restrict_access_based_on_product_profile__c,active__c,Recordtype.Name where Private__c = false '+ whrprodprsevent +'),Presentation__c(id,name,Date__c,product_Affiliation__c,title__c where id != null ' + whrprodprsevent + '),MessageCenter__c(ID,Product_Affiliation__c,Title__c,Message_Date__c,User_ids__c where Product_Affiliation__c INCLUDES (' + productgroupstr + ' )),ContentVersion(ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c, Document_Number__c, Title where Parent_Documentation__c = null and RecordType.name = \'Product Document\' '+ whr +')';
              //  String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING Event_Webinar__c(ID,URL_Path_Settings__c,Needs_MyVarian_registration__c,Product_Affiliation__c,title__c,To_Date__c,From_Date__c,Name,Restrict_access_based_on_product_profile__c,active__c,Recordtype.Name where Private__c = false '+ whrprodprsevent +'),Presentation__c(id,name,Date__c,product_Affiliation__c,title__c where id != null ' + whrprodprsevent + '),MessageCenter__c(ID,Product_Affiliation__c,Title__c,Message_Date__c,User_ids__c where Product_Affiliation__c INCLUDES (' + productgroupstr + ' )),ContentVersion(ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c,Parent_Documentation__c, Document_Number__c, Title where  RecordType.name = \'Product Document\' '+ whr +')';
                
                
                //String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING Event_Webinar__c(ID,URL_Path_Settings__c,Needs_MyVarian_registration__c,Product_Affiliation__c,title__c,To_Date__c,From_Date__c,Name,Restrict_access_based_on_product_profile__c,active__c,Recordtype.Name where (Private__c = false and Internal_Event__c= false) '+ whrprodprsevent +'),Presentation__c(id,name,Date__c,product_Affiliation__c,title__c where id != null ' + whrprodprsevent + '),MessageCenter__c(ID,Product_Affiliation__c,Title__c,Message_Date__c,User_ids__c where Product_Affiliation__c INCLUDES (' + productgroupstr + ' )),ContentVersion(ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c,Parent_Documentation__c, Document_Number__c, Title where  RecordType.name = \'Product Document\' '+ whr +')';  
                
                String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING Event_Webinar__c(ID,URL_Path_Settings__c,Needs_MyVarian_registration__c,Product_Affiliation__c,title__c,To_Date__c,From_Date__c,Name,Restrict_access_based_on_product_profile__c,active__c,Recordtype.Name where (Private__c = false and Internal_Event__c= false) '+ whrprodprsevent +'),Presentation__c(id,name,Date__c,product_Affiliation__c,title__c where id != null ' + whrprodprsevent + '), MessageCenter__c(ID,Product_Affiliation__c,Title__c,Message_Date__c,User_ids__c';
                if(productgroupstr <> null && productgroupstr <> '')
                    searchquery  += ' where Product_Affiliation__c INCLUDES (' + productgroupstr + ')';                
                    
                searchquery  += '),ContentVersion(ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c,Parent_Documentation__c, Document_Number__c, Title where  RecordType.name = \'Product Document\' '+ whr +')';  
                
                System.debug(' ----------- ----------------  : 11 featWebinars =' + conDocID.size());   
                system.debug(' ------ ----- searchquery ----- ' + searchquery );
                List<List<SObject>> searchList = search.query(searchquery);
                system.debug(' ------ ----- searchquery ----- ' + searchquery + ' ----- ----- ---- ' + searchList.size());
                
                //Start.INC4348266
                if(Document_TypeSearch == '' || Document_TypeSearch == null)
                    lContentVersions = ((List<ContentVersion>)searchList[3]);
                else{
                    String doc1query = 'select ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c,Parent_Documentation__c, Document_Number__c, Title' +
                    ' FROM ContentVersion where Title like \'%' + Document_TypeSearch + '%\' or ' +
                    'Document_Language__c = \'' + Document_TypeSearch + '\' or ' +
                    'Description like \'%' + Document_TypeSearch + '%\' or ' +
                    'Document_Number__c  like \'%' + Document_TypeSearch + '%\'';
                    lContentVersions = Database.query(doc1query);
                }
                //End.INC4348266
                
                system.debug('#Pro1:'+lContentVersions.size()+'---'+lContentVersions);
                Set<String> SetChildRec=new Set<String>();
                                
                 Map<ID,Sobject> OrigMapRec=New  Map<ID,Sobject>();
                OrigMapRec.putall(lContentVersions);
                ContentVersion TempRec=New ContentVersion ();
                 for(ContentVersion cnt : lContentVersions)
                { 
                   if(cnt.Parent_Documentation__c!=null)
                   {
                    
                      SetChildRec.add(cnt.Parent_Documentation__c);
                      TempRec=(ContentVersion) OrigMapRec.remove(cnt.id);
                    
                      
                   }
 
                   
                }
            system.debug('#PRO2:'+SetChildRec);
                system.debug('#PRO3:'+OrigMapRec);
           List<ContentVersion> ObjParentRec=[select ID, Date__c, Mutli_Languages__c, ContentDocumentId, Document_Type__c,Parent_Documentation__c, Document_Number__c, Title FROM ContentVersion where RecordType.name ='Product Document'  and Document_Number__c in: SetChildRec];
            for(ContentVersion PartRec:ObjParentRec)
            {
               if(!OrigMapRec.containsKey(PartRec.id))
               {
               if(PartRec.Parent_Documentation__c==null)
               {
               OrigMapRec.put(PartRec.id,PartRec);
               }
               }
            }
            lContentVersions.clear();
            lContentVersions =OrigMapRec.values();
            system.debug('#PRO4:'+lContentVersions.size() +'--'+lContentVersions);
                mapCntWrkSpace = new Map<String, List<ContentVersion>>();
                list<ContentVersion> lstCnt;
                for(ContentVersion cnt : lContentVersions)
                {                
                    lstCnt = new list<ContentVersion>();
                    lstCnt.add(cnt);
                    //Start.INC4348266
                    
                    if(cnt.title <> null && mapCntWrkSpace.ContainsKey(cnt.title))
                    {                        
                        lstCnt.addAll(mapCntWrkSpace.get(cnt.title));
                    }
                    mapCntWrkSpace.put(cnt.title,lstCnt);
                     //End.INC4348266
                                          
                }
                //contentypelist.add(new contenttype('Presentation',searchList[2]));
                
               List<Event_Webinar__c> eventlist = new List<Event_Webinar__c>();
               eventlist = (List<Event_Webinar__c>)searchList[0];
               List<Presentation__c> prsntlist = new List<Presentation__c>();
               prsntlist = (List<Presentation__c>)searchList[1];
               List<MessageCenter__c> msglist = new List<MessageCenter__c>();
               msglist = (List<MessageCenter__c>)searchList[2];
               if(prsntlist != null){
               contenttype conrecord = new contenttype();
               conrecord.Contenttypevar = 'Presentation';
               conrecord.prsntrecord = prsntlist;
               contentypelist.add(conrecord);
               }
               if(eventlist != null){
                List<Event_Webinar__c> evntlist = new list<Event_Webinar__c>();
                List<Event_Webinar__c> weblist = new list<Event_Webinar__c>();
                 for(Event_Webinar__c evnt : eventlist){
                   if(evnt.Recordtype.Name == 'Events'){
                     evntlist.add(evnt);
                   }
                   if(evnt.Recordtype.name == 'Webinars' && evnt.active__c == true){
                    if(evnt.Restrict_access_based_on_product_profile__c == true){
                    system.debug('Product affiliated---' + evnt.Product_Affiliation__c.split(';'));
                    system.debug('Product group---' + ProductGroupSet );
                     for(string str : evnt.Product_Affiliation__c.split(';')){
                       if(ProductGroupSet.contains(str))
                       {
                        weblist.add(evnt);
                        break;
                       }
                     }
                    }else{
                      weblist.add(evnt);
                      }
                   }
                 }
                if(weblist != null){
                  contenttype conrecordweb = new contenttype();
                  conrecordweb.Contenttypevar = 'Webinar';
                  conrecordweb.webinarrecord = weblist;
                  contentypelist.add(conrecordweb);
                 }
                if(evntlist != null){
                  contenttype conrecordevnt = new contenttype();
                  conrecordevnt.Contenttypevar = 'Event';
                  conrecordevnt.eventrecord = evntlist;
                  contentypelist.add(conrecordevnt);
                }
               }
               
                   
               
               if(msglist != null){
               List<MessageCenter__c> messagelist = new List<MessageCenter__c>();
                for(MessageCenter__c msg: msglist){
                 if(msg.User_ids__c != null){
                  if(!msg.User_ids__c.contains(userid)){
                    messagelist.add(msg);
                  }
                 }
                }
                if(messagelist!= null){
                  contenttype conrecordmsg = new contenttype();
                  conrecordmsg.Contenttypevar = 'Message';
                  conrecordmsg.msgrecord = messagelist;
                  contentypelist.add(conrecordmsg);
                }
               }
                system.debug(' --- Check mapCntWrkSpace Column ----  --  ' + mapCntWrkSpace);
                system.debug('@@@@@@@@' + searchList[1] + '@@@@' + searchList[2]+'@@@@' + searchList[3]);
                
            //return lContentVersions;
            return null;
        }
        catch (Exception e) 
        {
            system.debug('#ERRR:'+e.getMessage()+'--'+e.getLineNumber());
            ApexPages.addMessages(e);   
            return null;
        }
    }
  

}
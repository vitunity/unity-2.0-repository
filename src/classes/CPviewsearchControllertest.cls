/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CPviewsearchController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest
    Public class CPviewsearchControllertest{
    
     //Test Method for CPviewsearchController class when DocumentTypes is not null
     
     static testmethod void testCPviewsearchController()
        {
            Test.StartTest();
                
                
             RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
             
             ContentVersion testContentInsert =new ContentVersion(); 
             testContentInsert.ContentURL='http://www.yahoo.com/'; 
             testContentInsert.Title ='Yahoo.com'; 
             testContentInsert.RecordTypeId = ContentRT.Id; 
             insert testContentInsert; 
             
             ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id];
            
            ContentWorkspace testWorkspace = [SELECT Id,Name FROM ContentWorkspace WHERE Name='Acuity']; 
             ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
             newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
             newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
             insert newWorkspaceDoc;
             
             
             MessageCenter__c msgcnter=new MessageCenter__c();
             msgcnter.Title__c='testmessage';
             insert msgcnter;
                
                PageReference pageref = Page.CPViewSearch;
                Test.setCurrentPage(pageRef); 
                ApexPages.currentPage().getParameters().put('kwrd','Testing');
                ApexPages.currentPage().getParameters().put('abc',msgcnter.id);
                CPviewsearchController view = new CPviewsearchController();
                List<SelectOption> obj =  view.getoptions();
                List<SelectOption> selpot =  view.getDocTypes();
                view.Productvalue = testWorkspace.Name;
                view.DocumentTypes = 'Safety Notification';
                view.showrecords1();
                view.readmessage();
                
                  
            Test.StopTest();
        }
        
        
         //Test Method for CPviewsearchController class when DocumentTypes is null
     
        
        static testmethod void testCPviewsearchControllermethod1()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a;  
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx', Country__c='Barbados' ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                }
            user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
             MessageCenter__c msgcnter=new MessageCenter__c();
             msgcnter.Title__c='testmessage';
             insert msgcnter;
            ApexPages.currentPage().getParameters().put('abc',msgcnter.id);
            
            System.runAs (usr){
            
                CPviewsearchController view = new CPviewsearchController();
                List<SelectOption> obj =  view.getoptions();
                List<SelectOption> selpot =  view.getDocTypes();
                view.showrecords1();
               view.readmessage();
                    }
            Test.StopTest();
        }


  }
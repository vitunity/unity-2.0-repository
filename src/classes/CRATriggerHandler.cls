/***************************************************************************
Author: Divya Hargunani
Created Date: 30-Aug-2017
Project/Story/Inc/Task : STSK0012692: MyVarian: CRA enhancement not working
Description: 
This is a handler class containing the after delete logic for CRA_Delete_Contact trigger
This simulates the logic for removing the user from product groups after deleting CRA

Change Log:
4-Oct-2017 - Divya Hargunani - STSK0013078 - can't delete CRA's
*************************************************************************************/

public class CRATriggerHandler {
    
    /*
     *Code bulkification not possible due to complexity.
     */
    public static void removeProductGroup(List<Contact_Role_Association__c> deletedCRA){
        for(Contact_Role_Association__c craObj : deletedCRA){
            set<String> productGroupsToRemove = new set<String>();
            Account accountObj = [SELECT Selected_Groups__c,
                                  (SELECT SVMXC__Product__r.Product_Group__c 
                                   FROM R00N70000001hzZ0EAI__r) 
                                  FROM Account 
                                  WHERE Id =: craObj.Account__c];
            if (accountObj.Selected_Groups__c !=null){
            productGroupsToRemove.addAll(accountObj.Selected_Groups__c.split(';',-2));
            }
            for(SVMXC__Installed_Product__c ip : accountObj.R00N70000001hzZ0EAI__r){
                if(ip.SVMXC__Product__r.Product_Group__c != null)
                productGroupsToRemove.addAll(ip.SVMXC__Product__r.Product_Group__c.split(';',-2));
            }
            //STSK0013078: changed the return type from singleton user object to list of users
            List<User> lstUser = [SELECT id FROM user WHERE contactId =: craObj.contact__c limit 1];
            
            set<String> productGroupsToValid = new set<String>();
            set<String> accountIds = new set<String>();
            for(Contact_Role_Association__c craObject : [SELECT Account__c, Contact__r.AccountId
                                                         FROM Contact_Role_Association__c 
                                                         WHERE Contact__c =: craObj.Contact__c]){
                accountIds.add(craObject.Account__c);
                accountIds.add(craObject.Contact__r.AccountId);
            }
            for(Account acc : [SELECT Selected_Groups__c,(SELECT SVMXC__Product__r.Product_Group__c 
                               FROM R00N70000001hzZ0EAI__r) 
                               FROM Account WHERE Id in : accountIds]){
                                   if(acc.Selected_Groups__c != null){
                productGroupsToValid.addAll(acc.Selected_Groups__c.split(';',-2));
                                   }
                for(SVMXC__Installed_Product__c ip : acc.R00N70000001hzZ0EAI__r){
                    if(ip.SVMXC__Product__r.Product_Group__c != null)
                    productGroupsToValid.addAll(ip.SVMXC__Product__r.Product_Group__c.split(';',-2));
                }
            }
            set<String> productGroupsForDeletGroup = new set<String>();
            for(String str : productGroupsToRemove){
                if(!productGroupsToValid.contains(str)){
                    productGroupsForDeletGroup.add(str);
                }
            }
            system.debug('productGroupsForDeletGroup====='+productGroupsForDeletGroup);
            if(lstUser.size() > 0)
            deleteGroupMembers(productGroupsForDeletGroup,lstUser[0].id);
        }
    }
    
    @future
    private static void deleteGroupMembers(set<String>productGroupsForDeletGroup, String userId){
        List < Group > publicGroupLst = [SELECT Id, Name, Type FROM Group WHERE Name in: productGroupsForDeletGroup 
                                         AND(Type = 'Regular')];
        List < GroupMember > grpMemberLst  = [SELECT Id FROM GroupMember WHERE UserOrGroupId  =: userId
                                              and GroupId in : publicGroupLst];
        delete grpMemberLst;
    }
    
}
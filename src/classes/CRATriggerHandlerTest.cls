/***************************************************************************
Author: Divya Hargunani
Created Date: 30-Aug-2017
Project/Story/Inc/Task : STSK0012692: MyVarian: CRA enhancement not working
Description: 
This is a test class for CRATriggerHandler class
*************************************************************************************/


@isTest
public class CRATriggerHandlerTest {
    public static testmethod void removeProductGroupTest(){
        
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {   
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            Account primaryAcc = new Account(name='Primary Account',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test');
            insert primaryAcc;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=primaryAcc.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '1214445');
            insert con;
            Test.startTest();
            User u = new User(alias = 'standt', email='standardtestuser29@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standardtestuser29@testclass.com');
            insert u;
            
            Account acc1 =new Account(name='Account 1',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert acc1;
            
            Account acc2 =new Account(name='Account 2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert acc2;
            
            Contact_Role_Association__c cra1 = new Contact_Role_Association__c(contact__c = con.Id, Account__c = acc1.Id);
            insert cra1; 
            
            Contact_Role_Association__c cra2 = new Contact_Role_Association__c(contact__c = con.Id, Account__c = acc2.Id);
            insert cra2;
            
            Product2 pro1 = new Product2(Name = 'Test Product 1',Product_Group__c = 'Group 1, Group 2');
            insert pro1;
            
            Product2 pro2 = new Product2(Name = 'Test Product 2',Product_Group__c = 'Group 2');
            insert pro2;
            
            SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(Name = '12345678',SVMXC__Product__c = pro1.Id, SVMXC__Company__c = acc1.Id);
            insert ip1;
            
            SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(Name = '456789',SVMXC__Product__c = pro2.Id, SVMXC__Company__c = acc1.Id);
            insert ip2;
            
            SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c(Name = '9876543',SVMXC__Product__c = pro1.Id, SVMXC__Company__c = acc2.Id);
            insert ip3;
            Test.stopTest(); 
            delete cra1;
            
        }
    }

}
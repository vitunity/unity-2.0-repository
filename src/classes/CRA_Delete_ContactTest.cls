@isTest
Public class CRA_Delete_ContactTest
{
   public static testmethod void CRA_Delete_Contact()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
            User u;
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            
            Account a;      
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '1214445');
            insert con;
            
            u = new User(alias = 'standt', email='standardtestuser29@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standardtestuser29@testclass.com');
            insert u;
            
            Contact_Role_Association__c cro = new Contact_Role_Association__c(contact__c = con.Id, Account__c = a.Id);
            insert cro;            
            try{
                //delete contact association
                delete cro;
            }
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to delete contact role association.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }               
            
        }
    }
}
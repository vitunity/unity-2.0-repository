/***********************************************************************
* Author        : Mohit Sharma
* Functionality : Tree- Like Data Selection
* Description   :  Tree- Like Data Selection for Products on click of
*                  "Search Prouduct" Button available on Case Record.
* This Apex Class is being referenced in VF Page - CS_ShowProductTree.
* 
************************************************************************/
public with sharing class CS_ProductTreeView{
    
    /* Declaration of Variables */ 
 public string str{get;set;}
set<string> stChildData=new set<string>();
public string strNodeId{get;set;}
    private static JSONGenerator gen {get; set;}
    public List<String> LstOfProductNodes{get;set;} 
    
    // Getter Setter Method for searching input text in Product Tree on VF page.   
    public String InputText{get;set;}
    list<SVMXC__Installed_Product__c> lstInstalledProduct;
    // Getter Setter Method for passing Selected Product from VF page to Apex Class. 
    public String ProductId{get;set;}  
     set<string> stIP=new set<string>();
    Map<id,List<SVMXC__Installed_Product__c>>MapProductAndLstChildren=new Map<Id,List<SVMXC__Installed_Product__c>>();
    Map<Id,SVMXC__Installed_Product__c> MapOfProducts;
    List<SVMXC__Installed_Product__c> LstOfProducts;
    public string picklistvalue{get;set;}
    String ipprodtype ;
    String keyPrefix;
    String SiteName;
    /* Controller Method Starts here.*/
    public CS_ProductTreeView(ApexPages.StandardController controller){
        LstOfProductNodes=new List<String>();
        str=apexpages.currentpage().getparameters().get('id');
        ipprodtype = apexpages.currentpage().getParameters().get('prodtype');
        if(ipprodtype == Null)
            picklistvalue = 'None';
        else
            picklistvalue = ipprodtype;
        // Fetch all Id and Products in a Map available in Org.
        //MapOfProducts=new map<Id,Product2>([Select id,Name /*CS_Parent_Product__c*/ from Product2 where isActive=true]);  

        
         Schema.DescribeSObjectResult r = SVMXC__Installed_Product__c.sObjectType.getDescribe();
         keyPrefix = r.getKeyPrefix();
         lstInstalledProduct=new list<SVMXC__Installed_Product__c>();
         /*********** For installed product *****/
         if(str.startswith(keyPrefix)){
           // Fetch the top level id of the current installed product
            List<SVMXC__Installed_Product__c> CurrentRec = new List<SVMXC__Installed_Product__c>();
            CurrentRec = [Select SVMXC__Top_Level__c,SVMXC__Site__c from SVMXC__Installed_Product__c where id =: str];//ends here
            String Toplevelid;
            if(CurrentRec != null){
               if(CurrentRec[0].SVMXC__Top_Level__c != null){
                    Toplevelid = CurrentRec[0].SVMXC__Top_Level__c;
                    SiteName = CurrentRec[0].SVMXC__Site__c;
                    }else
               Toplevelid = str;
               SiteName = CurrentRec[0].SVMXC__Site__c;
               }
            if(ipprodtype != 'None' && ipprodtype != null)
                {
                    String soql = 'select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Status__c,   SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Top_Level__c =: Toplevelid OR id =: Toplevelid)'; //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/24/2015
                    soql = soql + ' and  SVMXC__Product__r.Product_Type__c  =\'' + ipprodtype +'\'';
                    lstInstalledProduct=database.query(soql);
                }
                else
                {
                    lstInstalledProduct=[select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Status__c,   SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Top_Level__c =: Toplevelid OR id =: Toplevelid)]; //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/24/2015
                }
            }else{
        /*************Shubham*************/
        if(ipprodtype != 'None' && ipprodtype != null)
        {
            String soql = 'select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Status__c,   SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where SVMXC__Site__c =: str';
            soql = soql + ' and  SVMXC__Product__r.Product_Type__c  =\'' + ipprodtype +'\''; //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/24/2015
            lstInstalledProduct=database.query(soql);
        }
        else
        {
            lstInstalledProduct=[select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Status__c,   SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where SVMXC__Site__c =: str];
            //,Product_Version_Build__r.Name //Wave 1C - Nikita - 1/24/2015
        }
        }
        /*************Shubham*************/
        
        for(SVMXC__Installed_Product__c varIp:lstInstalledProduct)
        {
        // stParentIP.add(varIp.SVMXC__Parent__r.Name);
         stIP.add(varIp.id);
        }
        
        if(str.startswith(keyPrefix)){
            if(ipprodtype != 'None' && ipprodtype != null)
               MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Product__r.name ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (/*SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR */id IN :stIP) /*AND SVMXC__Site__c=: SiteName*/ AND SVMXC__Product__r.Product_Type__c =: ipprodtype AND((SVMXC__Top_Level__c = null AND SVMXC__Parent__c = null) OR (SVMXC__Top_Level__c != null AND SVMXC__Parent__c != null))]); //,Product_Version_Build__r.Name //Wave 1C - Nikita 1/24/2015
            else
              MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Product__r.name ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (/*SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR */ id IN :stIP) /*AND SVMXC__Site__c=: SiteName*/ AND((SVMXC__Top_Level__c = null AND SVMXC__Parent__c = null) OR (SVMXC__Top_Level__c != null AND SVMXC__Parent__c != null))]);
              //,Product_Version_Build__r.Name // Wave 1c - Nikita 1/24/2015
            
        }else{
           if(ipprodtype != 'None' && ipprodtype != null)
            {
            MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Product__r.name ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Site__c=:str AND SVMXC__Product__r.Product_Type__c =: ipprodtype]); //,Product_Version_Build__r.Name //Wave 1C -Nikita 1/24/2015
            }   else{
             MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Product__r.name ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Site__c=:str]); //,Product_Version_Build__r.Name// Wave 1C - Nikita 1/24/2015
          }
         }
        // Iterating for loop to add list of Child Products in a Map corresponding to Parent Product Id's.   

        for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) {
        
        
           if(MapProductAndLstChildren.get(pd.SVMXC__Parent__c)==null)
            {
                LstOfProducts=new List<SVMXC__Installed_Product__c>();
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }else
            {               
                LstOfProducts=MapProductAndLstChildren.get(pd.SVMXC__Parent__c);
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }
        }
         system.debug('PARENTCHILD------->' + MapProductAndLstChildren);
        Map<string,list<SVMXC__Installed_Product__c>> mapChld=new Map<string,list<SVMXC__Installed_Product__c>>();
        for(string varStr:stIP)
        {
         LstOfProducts=new List<SVMXC__Installed_Product__c>();
            for(SVMXC__Installed_Product__c pd:MapOfProducts.values())
            {
            if(varStr == pd.SVMXC__Parent__c)
            {
             LstOfProducts.add(pd);
        
            }
            
          }
        mapChld.put(varStr,LstOfProducts)   ;
        }
        system.debug('MAPCHILD---->' + mapChld);
        for(string varst:stIP)
        {
            for(SVMXC__Installed_Product__c varStr:mapChld.get(varst))
            {
              stChildData.add(varStr.id);
            }
        }
        System.debug('------MapProductAndLstChildren------------------>'+stChildData);
    } /*Controller Method Ends here.*/
    
    /* Action method (called from VF:CS_ShowProductTree) starts here.
       Below method parses all Parent Products and adds them to JSON String 
       which is rendered on VF as Tree Structure. */
    public void GenerateTreeStructureForProducts(){
        //  for static header       
       gen = JSON.createGenerator(true);
           HeadertoJSON();
           LstOfProductNodes.add(gen.getAsString()); //ends here   
        // Generating Tree Strucutre for all Parent Products.  
      List<SVMXC__Installed_Product__c> prdlist = new List<SVMXC__Installed_Product__c>();
      if(str.startswith(keyprefix)){
       if(ipprodtype != 'None' && ipprodtype != null)
        {
            prdlist = [select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__r.name   ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Parent__c,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR Id IN:stIP) AND SVMXC__Product__r.Product_Type__c =: ipprodtype AND((SVMXC__Top_Level__c = null AND SVMXC__Parent__c = null) OR (SVMXC__Top_Level__c != null AND SVMXC__Parent__c != null))]; //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/24/2015
        }else{
           prdlist = [select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__r.name   ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Parent__c,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR Id IN:stIP) AND((SVMXC__Top_Level__c = null AND SVMXC__Parent__c = null) OR (SVMXC__Top_Level__c != null AND SVMXC__Parent__c != null))]; //,Product_Version_Build__r.Name // Wave 1C -Nikita 1/24/2015
        }
     } else{
        if(ipprodtype != 'None' && ipprodtype != null)
        {
            prdlist = [select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__r.name   ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Parent__c,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR Id IN:stIP) AND SVMXC__Site__c=:str AND SVMXC__Product__r.Product_Type__c =: ipprodtype];
            //,Product_Version_Build__r.Name //Wave 1C -Nikita 1/24/2015
        }else{
           prdlist = [select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__r.name   ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Parent__c,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR Id IN:stIP) AND SVMXC__Site__c=:str];
           //,Product_Version_Build__r.Name //Wave 1C -Nikita 1/24
        }
        }
      for(SVMXC__Installed_Product__c pd: prdlist){
           
            // Initializing JSON Generator Object.
            gen = JSON.createGenerator(true);
            
            /* Calls CreateTreeStructure method to retrieve Instance of Wrapper Class 
                   which contains Parent and Child Nodes for each Product. */
                     // Calling ConvertNodeToJSON Method. 
          if( !stChildData.contains(pd.id))
         {
           WrpProductNode node = CreateTreeStructure(pd);
            

           ConvertNodeToJSON(node);
           LstOfProductNodes.add(gen.getAsString());
         
            // Creating List of All Product Nodes by adding instance of JSONGenerator as String.
          }
           
        } system.debug('------LstOfProductNodes-->'+LstOfProductNodes);
    } /* Action method ends here.*/ 
    /******** Method for generating static header of the dyna tree *******/
      public void HeadertoJSON()
    {
    
        gen.writeStartObject();
        gen.writeStringField('title', 'Installed Product Name'+'@'+'Product Name'+'@'+' Product Version Build'+'@'+'Serial Number'+'@'+'Service Contract '+'@'+' Contract Start Date'+'@'+'Contract End Date'+'@'+'City'+'@'+'Status'+'@'+'Black'+'@'+'');
        gen.writeBooleanField('unselectable', false);
        gen.writeStringField('key', '');
        gen.writeBooleanField('expand', false);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        gen.writeEndObject();  

    }
    /***** Ends here *****/
    /* CreateTreeStructure Method starts here : Below method creates Parent and Child Nodes for each Product 
       and returns instance of WrpProductNode (Wrapper class) */
    public  WrpProductNode CreateTreeStructure(SVMXC__Installed_Product__c prd){            
        WrpProductNode wrpObj=new WrpProductNode();
        wrpObj.prod=prd;
        List<WrpProductNode>lstOfChild;
        if(MapProductAndLstChildren.get(prd.id)!=null)
        {
            wrpObj.hasChildren =true;
            lstOfChild=new List<WrpProductNode >();
            for(SVMXC__Installed_Product__c pd:MapProductAndLstChildren.get(prd.id)){
              WrpProductNode temp=CreateTreeStructure(pd);
                lstOfChild.add(temp);
            }           
        }else
        {
             wrpObj.hasChildren =false;   
        }   
            
        wrpObj.prodChildNode=lstOfChild;
        return wrpObj;        
    } /*CreateTreeStrucure Method Ends here.*/
    
    /*ConvertNodeToJSON method starts here : Below method converts 
    instance of each Product Node(Wrapper class) into JSON.*/
    public void ConvertNodeToJSON(WrpProductNode prodNode){ 
        
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce. 
System.debug('--Id'+prodNode.prod.id);      
    
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod.Name+'@'+(prodNode.Prod.SVMXC__Product__r.name==null?' ': prodNode.Prod.SVMXC__Product__r.name)+'@'+
        /* Wave 1C - Nikita 1/24/2015
        (prodNode.Prod.Product_Version_Build__r.Name==null?' ':prodNode.Prod.Product_Version_Build__r.Name)+
        Wave 1C - Nikita 1/24/2015*/
        '@'+(prodNode.Prod.SVMXC__Serial_Lot_Number__c==null?'':prodNode.Prod.SVMXC__Serial_Lot_Number__c)+'@'+(prodNode.Prod.SVMXC__Service_Contract__r.Name==null?'':prodNode.Prod.SVMXC__Service_Contract__r.Name)+'@'+(string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__Start_Date__c)==null?'':string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__Start_Date__c))+'@'+(string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__End_Date__c)==null?'':string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__End_Date__c))+'@'+(prodNode.Prod.SVMXC__City__c==null?'':prodNode.Prod.SVMXC__City__c)+'@'+(prodNode.Prod.SVMXC__Status__c==null?' ':prodNode.Prod.SVMXC__Status__c)+'@'+(prodNode.Prod.id == str? 'Red' : 'Black') +'@'+prodNode.Prod.id);
        gen.writeStringField('key', prodNode.Prod.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
           if(prodNode.hasChildren ){    
            
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
       gen.writeEndObject();    
       
    }/*ConvertNodeToJSON method ends here.*/
     public void ConvertNodeToJSONChild(WrpProductNode prodNode){ 
        
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce.      
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod.Name+'@'+(prodNode.Prod.SVMXC__Product__r.name==null?' ': prodNode.Prod.SVMXC__Product__r.name)+'@'+
        /* Wave 1C - Nikita 1/24/2015
        (prodNode.Prod.Product_Version_Build__r.Name==null?' ':prodNode.Prod.Product_Version_Build__r.Name)+
        Wave 1C - Nikita 1/24/2015*/
        '@'+(prodNode.Prod.SVMXC__Serial_Lot_Number__c==null?'':prodNode.Prod.SVMXC__Serial_Lot_Number__c)+'@'+(prodNode.Prod.SVMXC__Service_Contract__r.Name==null?'':prodNode.Prod.SVMXC__Service_Contract__r.Name)+'@'+(string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__Start_Date__c)==null?'':string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__Start_Date__c))+'@'+(string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__End_Date__c)==null?'':string.valueof(prodNode.Prod.SVMXC__Service_Contract__r.SVMXC__End_Date__c))+'@'+(prodNode.Prod.SVMXC__City__c==null?'':prodNode.Prod.SVMXC__City__c)+'@'+(prodNode.Prod.SVMXC__Status__c==null?' ':prodNode.Prod.SVMXC__Status__c)+'@'+(prodNode.Prod.id == str? 'Red' : 'Black') +'@'+prodNode.Prod.id);
        gen.writeStringField('key', prodNode.Prod.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
           if(prodNode.hasChildren ){    
            
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
       gen.writeEndObject();    
      
    }
    /*Wrapper class starts here : Below class wraps Products,List of Child Products 
    and boolean variable for childs present for each Parent Product.*/
    public class WrpProductNode{        
        public List<WrpProductNode>prodChildNode{get;set;}
        public Boolean hasChildren {get; set;} 
            
        public SVMXC__Installed_Product__c prod{get;set;}        
    } /*Wrapper Class ends here*/   
    
    /*UpdateCase Method starts here : Below method is triggered by clicking Save button on VF page.*/
    public pageReference updateCase(){   
        
        // Below query fetches Case record for updating selected Product in Database.   
       // Case cs=[Select id/*,CS_Product__c*/ from case where id=:ApexPages.currentPage().getParameters().get('id')];//comment By Mohit
      //  cs.CS_Product__c=ProductId;
        try{
           // update cs;
        }catch(Exception e){
            
        }
        PageReference pg=new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        pg.setRedirect(true);
        return pg;
    }/*UpdateCase Method ends here.*/
}
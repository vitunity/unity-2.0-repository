@isTest 
Public class CS_ProductTreeView_Test {
    static testMethod void TestTreeData() 
    {




Account a; 
// Inserting account
a = new Account(name='test',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
insert a;  

//Inserting objLocation
    SVMXC__Site__c objloc=new SVMXC__Site__c();
    objloc.SVMXC__Account__c=a.id;
    objloc.Name='testLocation';
    objloc.SVMXC__Street__c='testStreet';
    objloc.SVMXC__Country__c='testCountry';
    objloc.SVMXC__Zip__c='201301';
    insert objloc;
// Inserting Product

    product2 ObjPro=new product2();
    ObjPro.name='testProd';
    insert ObjPro;
//  Inserting  installed prods
     list<SVMXC__Installed_Product__c> lstInstall=new list<SVMXC__Installed_Product__c>();
    SVMXC__Installed_Product__c objIns1=new SVMXC__Installed_Product__c();
    objIns1.SVMXC__Site__c=objloc.id;
    objIns1.SVMXC__Product__c=ObjPro.id;
    objIns1.SVMXC__Status__c='testInstalled';
    objIns1.SVMXC__Serial_Lot_Number__c='0010';
    
    insert objIns1;
    SVMXC__Installed_Product__c objIns2=new SVMXC__Installed_Product__c();
    objIns2.SVMXC__Site__c=objloc.id;
    objIns2.SVMXC__Product__c=ObjPro.id;
    objIns2.SVMXC__Status__c='testInstalled';
    objIns2.SVMXC__Serial_Lot_Number__c='0010';
    objIns2.SVMXC__Parent__c=objIns1.id;
    
    SVMXC__Installed_Product__c objIns3=new SVMXC__Installed_Product__c();
    objIns3.SVMXC__Site__c=objloc.id;
    objIns3.SVMXC__Product__c=ObjPro.id;
    objIns3.SVMXC__Status__c='testInstalled';
    objIns3.SVMXC__Serial_Lot_Number__c='0010';
   
    lstInstall.add(objIns3);
    
    Insert lstInstall;
    /*CS_ProductTreeView.WrpProductNode aa= new  CS_ProductTreeView.WrpProductNode();
    aa.prod=objIns1;
    aa.hasChildren=True;*/
    Test.setCurrentPage(Page.SR_ProductTreeViewONAccount);
    ApexPages.currentPage().getParameters().put('Id',objloc.id);
    ApexPages.StandardController sc = new ApexPages.standardController(a);
    CS_ProductTreeView sic = new CS_ProductTreeView(sc);
   sic.GenerateTreeStructureForProducts();
   //sic.ConvertNodeToJSONChild(aa);
   sic.updateCase();
    }
}
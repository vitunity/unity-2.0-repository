/***************************************************************************
Author: Rakesh Basani
Created Date: 16-Aug-2017
Project/Story/Inc/Task : STSK0012416
Description: 
Update Total Work Order Time field on Case. 
Change Log:
****************************************************************************/
 public class CalculateTotalWorkTimeClass
    {
    /***************************************************************************
    Description: 
    Calculate Total Work Time field on all case related WO's and updating Total Work Order Time field on Case.
    *************************************************************************************/
    public static void doTotal(List<SVMXC__Service_Order__c> lstWO){
        set<Id> setCaseIds = new set<Id>();
        for(SVMXC__Service_Order__c i: lstWO){ 
            setCaseIds.add(i.SVMXC__Case__c); 
        }
        if(setCaseIds.size()>0){
            updateCases(setCaseIds);
        }
    }
    @future
    public static void updateCases(set<Id> setCaseIds){
    
        if(setCaseIds.size()>0){
            map<Id,decimal> mapCaseWorkOrder = new map<Id,decimal>();
            for(SVMXC__Service_Order__c i: [select id,Total_Work_Time__c,SVMXC__Case__c from SVMXC__Service_Order__c where SVMXC__Case__c IN: setCaseIds ]){
                decimal total = i.Total_Work_Time__c;
                if(mapCaseWorkOrder.containsKey(i.SVMXC__Case__c)){
                    total += mapCaseWorkOrder.get(i.SVMXC__Case__c);
                }
                mapCaseWorkOrder.put(i.SVMXC__Case__c,total);
            }
            
            List<Case> lstCases = new List<Case>();
            for(Case c : [select Id,Total_Work_Time__c from Case where ID IN: setCaseIds and isClosed != true]){
                c.Total_Work_Time__c = (mapCaseWorkOrder.containsKey(c.Id))?mapCaseWorkOrder.get(c.Id):0;
                lstCases.add(c);
            }
            
            update lstCases;
        }
        
    }
}
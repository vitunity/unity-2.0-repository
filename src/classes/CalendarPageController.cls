/***************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
16-Nov-2017 - Pranjul Maindola - STSK0013340 - Calendar View : Maximum View State Size limit (135KB) 
*************************************************************************************/
public class CalendarPageController {

    private List<Event> events;
    private Month month;
    
    /* Below members changed to transient to avaoid view state size issue : STSK0013340 */
    private transient List<GroupWeeks> groupWeeks; 
    private transient List<GroupWeeks> monthGroupWeeks;
        
    private List<SelectOption> options;
    private Map<Id, String> ownerMap;
    private List<Integer> dayNums;
    private List<String> dayOfWeek;
    private List<Integer> monthNums;
    private Integer columnNums;
    private List<Id> userIds;


    public Event event { get; set;}
    public String groupId { get; set;}
    public Boolean swtichView { get; set; }
    public Boolean editable { get; set; }

    public List<GroupWeeks> getGroupWeeks()
    {
        return groupWeeks;
    }

    public List<GroupWeeks> getMonthGroupWeeks()
    {
        return monthGroupWeeks;
    }

    public List<Month.Week> getWeeks() 
    { 
        system.assert(month!=null,'month is null');
        return month.getWeeks();
    }

    public List<Integer> getDayNums() 
    {
        return dayNums;
    }

    public Integer getColumnNums()
    {
        return columnNums;
    }
    
    public List<String> getDayOfWeek()
    {
        return dayOfWeek;
    }
    public List<Integer> getMonthNums() 
    {
        return monthNums;
    }

    public Month getMonth() { return month; } 

    public List<SelectOption> getPublicGroup()
    {
        options = new List<SelectOption>();
        Set<Id> shareGroupId = new Set<Id>();
        for(Unity_Group__Share share : [select ParentId, UserOrGroupID from Unity_Group__Share where UserOrGroupID =: UserInfo.getUserId()])
        {
            shareGroupId.add(share.ParentId);
        }
        for(Unity_Group__c pGroup : [select Name, Id, IsDefault__c from Unity_Group__c where OwnerId =: UserInfo.getUserId() or Id in: shareGroupId])
        {
            SelectOption option = new SelectOption(pGroup.Id, pGroup.Name);
            options.add(option);
        }
        return options;
    }
    private void setGroupMap()
    {
        userIds.clear();
        if(String.isNotBlank(groupId))
        {
            for(Unity_Group__c pGroup : [select Name, Id, IsDefault__c, OwnerId from Unity_Group__c where Id =: groupId])
            {
                if(pGroup.OwnerId == UserInfo.getUserId())
                {
                    editable = true;
                }
            }
            for(Unity_Group_Member__c member : [select Id, Name, User__c, Unity_Group__c from Unity_Group_Member__c where Unity_Group__c =: groupId order by Order_Number__c asc])
            {
                userIds.add(member.User__c);
            }
        }        
        ownerMap = new Map<Id, String>();  

        for(User user : [select Id, Name from User where Id in: userIds])
        {
            ownerMap.put(user.Id, user.Name);
        }  
    }
    private void setWeek() 
    { 
        month = new Month(event.Custom_Date__c);  
        system.assert(month != null); 

        Date[] da = month.getValidDateRange();  // gather events that fall in this month
        dayNums = new List<Integer>();
        dayOfWeek = new List<String>();
        for(Integer i = 0; i < 7; i++)
        {
            dayNums.add(event.Custom_Date__c.toStartOfWeek().addDays(i).day());
            Datetime dt = datetime.newInstance(event.Custom_Date__c.toStartOfWeek().addDays(i).year(), event.Custom_Date__c.toStartOfWeek().addDays(i).month(),event.Custom_Date__c.toStartOfWeek().addDays(i).day());
            dayOfWeek.add(dt.format('EEEE').substring(0,3));
        }

        events = [ select id, Booking_Type__c, Subject, OwnerId, owner.Name, Event_Color__c, ActivityDate, ActivityDateTime, DurationInMinutes, StartDateTime, EndDateTime, IsAllDayEvent
        from Event 
        where OwnerId in: ownerMap.keySet() and ((ActivityDate >= :da[0] AND ActivityDate <= :da[da.size() - 1]) OR (ActivityDate < :da[0] AND Custom_End_Date__c >= :da[0])) order by ActivityDateTime limit 49999];
        Map<Id, List<Event>> eventsWithOwner = new Map<Id, List<Event>>();
        groupWeeks = new List<GroupWeeks>();    
        for(Event event : events)
        {
            if(!eventsWithOwner.containsKey(event.ownerId))
            {
                eventsWithOwner.put(event.ownerId, new List<Event>());
                eventsWithOwner.get(event.ownerId).add(event);
            }
            else
            {
                eventsWithOwner.get(event.ownerId).add(event);
            }
        }

        if(userIds.size() > 0)
        {
            for(Integer i = 0; i < userIds.size(); i++)
            {
                Month tempMonth = new Month(event.Custom_Date__c); 
                GroupWeeks groupWeek = new GroupWeeks(ownerMap.get(userIds[i]), tempMonth, URL.getSalesforceBaseUrl().toExternalForm() + '/' + userIds[i]);
                groupWeeks.add(groupWeek);  
                if(eventsWithOwner.containsKey(userIds[i]))
                {
                    tempMonth.setEvents(eventsWithOwner.get(userIds[i]));
                } 
            }
        }
        //month.setEvents(events);  // merge those events into the month class
    }
    public void setMonth()
    {
        month = new Month(event.Custom_Date__c, 'Month');  
        Date[] da = month.getValidMonthRange();  // gather events that fall in this month    
        monthNums = new List<Integer>();
        columnNums = 0;
        for(Integer i = 0; i < month.weeks.size()*7; i++)
        {
            monthNums.add(event.Custom_Date__c.toStartOfMonth().toStartOfWeek().addDays(i).day());
            columnNums += 1;
        }        
        System.debug('****: ' + da[0] + ' ' + da[da.size() - 1]); 
        List<Event> events = [ select id, Booking_Type__c, Subject, ownerId, owner.Name, Event_Color__c, ActivityDate, ActivityDateTime, DurationInMinutes, StartDateTime, EndDateTime, IsAllDayEvent
        from Event 
        where OwnerId in: ownerMap.keySet() and ((ActivityDate >= :da[0] AND ActivityDate <= :da[da.size() - 1]) OR (ActivityDate < :da[0] AND Custom_End_Date__c >= :da[0])) order by ActivityDateTime limit 49999];
        Map<Id, List<Event>> eventsWithOwner = new Map<Id, List<Event>>();
        monthGroupWeeks = new List<GroupWeeks>();    
        for(Event event : events)
        {
            if(!eventsWithOwner.containsKey(event.ownerId))
            {
                eventsWithOwner.put(event.ownerId, new List<Event>());
                eventsWithOwner.get(event.ownerId).add(event);
            }
            else
            {
                eventsWithOwner.get(event.ownerId).add(event);
            }
        }  

        if(userIds.size() > 0)
        {
            for(Integer i = 0; i < userIds.size(); i++)
            {
                Month tempMonth = new Month(event.Custom_Date__c, 'Month'); 
                GroupWeeks groupWeek = new GroupWeeks(ownerMap.get(userIds[i]), tempMonth, URL.getSalesforceBaseUrl().toExternalForm() + '/' + userIds[i]);
                monthGroupWeeks.add(groupWeek);  
                if(eventsWithOwner.containsKey(userIds[i]))
                {
                    tempMonth.setEvents(eventsWithOwner.get(userIds[i]));
                } 
            }
        }      
    }
    public class GroupWeeks
    {
        public Month month {get; set;}
        public String username {get; set;}
        public String userUrl {get; set;}

        public GroupWeeks(String username, Month month, String userUrl)
        {
            this.month = month;
            this.username = username;
            this.userUrl = userUrl;
        }

    }
    /*
    private void setGroupWeeks()
    {
        firstDate = d.toStartOfMonth();
        upperLeft = d.toStartOfWeek();
        groupWeeks = new Month.Week(d);

        List<Event> events = [ select id,subject,description,activitydate,activitydatetime,DurationInMinutes
        from Event 
        where activitydate];

    }
    */
    public CalendarPageController() 
    {
        editable = false;
        event = new Event();
        event.Custom_Date__c = system.today();
        userIds = new List<Id>();
        Set<Id> shareGroupId = new Set<Id>();
        for(Unity_Group__Share share : [select ParentId, UserOrGroupID from Unity_Group__Share where UserOrGroupID =: UserInfo.getUserId()])
        {
            shareGroupId.add(share.ParentId);
        }

        for(Unity_Group__c pGroup : [select Name, Id, IsDefault__c, OwnerId from Unity_Group__c where OwnerId =: UserInfo.getUserId()])
        {
            if(pGroup.OwnerId == UserInfo.getUserId())
            {
                editable = true;
            }
            if(pGroup.IsDefault__c == true)
            {
                groupId = pGroup.Id;
            }
        }
        if(String.isBlank(groupId))
        {
            for(Unity_Group__c pGroup : [select Name, Id, IsDefault__c, OwnerId from Unity_Group__c where OwnerId =: UserInfo.getUserId() or Id in:shareGroupId])
            {
                if(pGroup.OwnerId == UserInfo.getUserId())
                {
                    editable = true;
                }
                groupId = pGroup.Id;
            }
        }
        swtichView = false;
        setGroupMap();
        setWeek();
        setMonth();
    }

    public void nextWeek() 
    { 
        addWeek(7);
    }

    public void prevWeek()    
    { 
        addWeek(-7);      
    }

    public void nextMonth() 
    { 
        addMonth(1);
    }

    public void prevMonth()    
    { 
        addMonth(-1);        
    }    

    public void addMonth(Integer val) 
    { 
        event.Custom_Date__c = event.Custom_Date__c.addMonths(val);
        setMonth();
    }    
    public void addWeek(Integer val) 
    { 
        event.Custom_Date__c = event.Custom_Date__c.addDays(val);
        setWeek();
    }      

    public PageReference resetDate()
    {
        if(event.Custom_Date__c != null)
        {
            setWeek();
            setMonth();
        }
        return null;
    }
    public PageReference resetGroup()
    {
        setGroupMap();
        setWeek();
        setMonth();
        return null;
    }
    public PageReference swtichView()
    {
        if(swtichView)
        {
            swtichView = false;
        }
        else
        {
            swtichView = true;           
        }
        return resetGroup();
    }
    public PageReference deleteGroup()
    {
        List<Unity_Group__c> pGroups = [select Name, Id, IsDefault__c from Unity_Group__c where Id =: groupId];
        if(pGroups.size() > 0)
        {
            delete pGroups;
        }
        PageReference calendarPage = new PageReference('/apex/CalendarPage');        
        calendarPage.setRedirect(true);
        return calendarPage;
    }
}
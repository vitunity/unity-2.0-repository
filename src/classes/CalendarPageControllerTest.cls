@isTest(SeeAllData=true)
public class CalendarPageControllerTest
{
    static Unity_Group__c unityGroup;
    static Unity_Group_Member__c unityMember;
    public Static Profile pr = [Select id from Profile where name = 'VMS Service - User'];
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
    static Event event;
    static testMethod void CalendarPageControllerTest()
    {
        insert u;
        unityGroup = new Unity_Group__c();
        unityMember = new Unity_Group_Member__c();
        unityGroup.Name = 'Acme';
        unityGroup.IsDefault__c = true;
        Insert unityGroup;
        unityMember.Unity_Group__c = unityGroup.id;
        unityMember.User__c = u.id;
        insert unityMember;
        event = new Event();
        event.Booking_Type__c = '';
        event.Subject = 'acme';
        event.ownerId = u.id;
        event.StartDateTime = datetime.now();
        event.DurationInMinutes = 50;
        insert event;
        PageReference myVfPage1 = Page.CalendarPage;
        Test.setCurrentPage(myVfPage1);     
        CalendarPageController controller1 = new CalendarPageController();
        controller1.getGroupWeeks();
        controller1.getMonthGroupWeeks();
        controller1.getWeeks();
        controller1.getDayNums();
        controller1.getDayOfWeek();
        controller1.getMonthNums();
        controller1.getMonth();
        controller1.getPublicGroup();
        controller1.nextWeek();
        controller1.prevWeek();
        controller1.nextMonth();
        controller1.prevMonth();
        controller1.addMonth(1);
        controller1.addWeek(1);
        controller1.resetDate();
        controller1.resetGroup();
        controller1.swtichView();
        controller1.deleteGroup();
        EventItem ei = new EventItem(event);
        ei.getEv();
        ei.getFormatedDate();        
    }
    
    static testMethod void eventtest(){
       Event eObj = new Event();
       eObj.StartDateTime = System.now();
       eObj.EndDateTime = System.now();
       EventItem e = new EventItem(eObj,true);
    }
}
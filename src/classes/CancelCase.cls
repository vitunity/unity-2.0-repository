/**
  * @author cdelfattore/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Some Detailed Purpose of the Class, what it does and why. 
  *    Helpful hints for the next developer.
  *    Business logic or use cases can be used.
  * 
  * TEST CLASS 
  * 
  *    Name of the test class
  *    For Naming use the class CancelCase_TEST
  *  
  * ENTRY POINTS 
  *  
  *    How is this code called, if a VF controller which page calls it.  
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  cdelfattore/Forefront; Initial Build
  *    v2.0 8-28-15 savon/Forefront; Numerous Changes 
  *     
  * 
  **/
global with sharing class CancelCase {

  webService static void cancelCaseAndRelatedObjects(Id caseId, String recTypeName){
    System.debug(recTypeName);

    Map<Id, SObject> objectsToUpdate = new Map<Id, SObject>();

    //set this Case's Status to 'Cancelled', 
    objectsToUpdate.put(caseId, new Case(Id = caseId, Status = 'Cancelled'));

    //set the Line Status on all the Case Line's to 'Cancelled'
    for(SVMXC__Case_Line__c cl : [SELECT Id, Sales_Order_Item__c FROM SVMXC__Case_Line__c WHERE SVMXC__Case__c = :caseId]){

      //In case of Installation/Training Case, change the sales order item's status to "Available for Planning" on the case line related sales order items
      if((recTypeName == 'INST' || recTypeName == 'Training') && (cl.Sales_Order_Item__c != null)){
        Sales_Order_Item__c soi = new Sales_Order_Item__c(Id = cl.Sales_Order_Item__c, Status__c = 'Available for Planning');
        objectsToUpdate.put(soi.Id, soi);
      }

      cl.SVMXC__Line_Status__c = 'Cancelled';
      objectsToUpdate.put(cl.Id, cl);
    }

    //if there are PWO's associated with this case, set the Work Order's Order Status to 'Cancelled'
    //savon.FF removed criteria from work order query below
    Set<Id> workOrderIdSet = new Set<Id>();
    for(SVMXC__Service_Order__c wo : [SELECT Id, (Select Id, SVMXC__Line_Status__c From SVMXC__Service_Order_Line__r) FROM SVMXC__Service_Order__c WHERE SVMXC__Case__c = :caseId]){
      workOrderIdSet.add(wo.Id);
      wo.SVMXC__Order_Status__c = 'Cancelled';
      //wo.Cancel__c = true; //DE7821
      objectsToUpdate.put(wo.Id, wo);
    }

    //if there are ServiceMax Event's associated with the PWO above, set the Status to 'Cancelled'
    //on any salesforce event associated with these ServiceMax Event and then delete the ServiceMax Event's 
    //set the Status to 'Cancelled' on any salesforce event associated with these ServiceMax Event
    //use the whatid on event


    List<Event> salesforceEvents = [SELECT Id FROM Event WHERE whatid IN :workOrderIdSet];

    for (Event e : salesforceEvents) {
      e.Status__c = 'Cancelled';
    }

    if (!objectsToUpdate.isEmpty()) {
    	List<sObject> lso = objectsToUpdate.values();
    	lso.sort();
        update lso;
    }

    if (!salesforceEvents.isEmpty()) {
      update salesforceEvents;
    }
  }
}
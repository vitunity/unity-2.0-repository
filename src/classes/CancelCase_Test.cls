@isTest(seealldata=true)
public class CancelCase_Test {
     /* test data members */
    static Profile vmsserviceuserprofile;
    static User serviceuser;
    static User serviceuser1;
    static SVMXC__Service_Group_Members__c technician;
    static Technician_Assignment__c techassgn;
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    static SVMXC__Site__c location;
    static Account acc;
    static SVMXC__Service_Order_Line__c wd;
    static SVMXC__Service_Group__c servicegrp;
    static Organizational_Activities__c orgactivities;
    static Timecard_Profile__c timecardprof ;
    static Timecard_Profile__c indirecttimecardprof ;
    static ERP_Project__c  erpproject; 
    static ERP_WBS__c erpwbs;
    static Country__c con;
    static Contact c;
    static ERP_NWA__c erpnwa;
    static SVMXC_Time_Entry__c timeEntry;
    static SVMXC_Timesheet__c timeSheet;
    static Timecard_Profile__c timecardProfile;
    static Organizational_Activities__c orgActivites;
    static Product2 product;
    /* Initialize test data in static block */
    static{
        vmsserviceuserprofile = new profile();
        vmsserviceuserprofile = [Select id from profile where name = 'VMS System Admin'];
        serviceuser = [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and id!=:userInfo.getUserId() limit 1];
        serviceuser1 = [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and id!=:serviceuser.id limit 1]; 
          
        system.debug('service user' + serviceuser.id);
        system.debug('service user 1 ' + serviceuser1.id);  
        
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        
        timecardprof = new Timecard_Profile__c(First_Day_of_Week__c = 'Saturday', Name = 'Test TimeCard Profile');
        timecardprof.Normal_Start_Time__c = datetime.newInstance(2014, 12, 1, 9, 0, 0);
        insert timecardprof;
        
        indirecttimecardprof = new Timecard_Profile__c( Name = 'PM Indirect Time Entry Matrix profile'); //indirect
        insert indirecttimecardprof;
        
        orgactivities = new Organizational_Activities__c(Name = 'Allocations Prep / Reporting',    Type__c = 'Indirect', Indirect_Hours__c = indirecttimecardprof.id); //indirect
        insert orgactivities;
        
        timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        insert timecardProfile;
        
        technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician',User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician.Timecard_Profile__c = timecardProfile.Id;
        insert technician;
        
        con = new Country__c(Name = 'United States');
        insert con;
    
        acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id); 
        insert acc;
        
        c = new Contact (AccountId = acc.id, LastName = 'dssssaf');
        
        location = new SVMXC__Site__c(Sales_Org__c = '0600', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = acc.id);
        insert location;
        
        casealias = new Case(Subject = 'testsubject');
        casealias.Priority = 'High';
        insert casealias;
        
        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC');
        insert erpnwa;
        
        erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id);
        insert erpwbs;
        
        
    }
    
    public static testmethod void testancelCaase1(){
        Test.startTest();
        
        System.runAs(Serviceuser){
        wo = new SVMXC__Service_Order__c(SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id,ERP_Service_Order__c = '12345678',ERP_WBS__c=erpwbs.id);
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        wo.Event__c = true;
        //insert wo;
        }
        
        techassgn = new Technician_Assignment__c(Work_Order__c = wo.id,Technician_Name__c = technician.id);
        insert techassgn;
        
        wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Consumed_From_Location__c = location.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Completed__c=false;
        wd.SVMXC__Group_Member__c = technician.id;
        // wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.Actual_Hours__c = 5;
        //wd.SVMXC__Service_Order__c = wo.id;
        wd.NWA_Description__c = 'nwatest';
        wd.ERP_NWA__c = erpnwa.id;
        //insert wd;
        
        orgActivites = new Organizational_Activities__c();
        orgActivites.Holiday_Cash__c = true;
        insert orgActivites;
        
        product = new Product2(Name='Connect Utilities');
        insert product;
        Sales_Order_Item__c soi = new Sales_Order_Item__c(Status__c = 'Open');
        insert soi;
        
        SVMXC__Case_Line__c caseLine = SR_testdata.createCaseLine();
        caseLine.Sales_Order_Item__c = soi.Id;
        caseLine.SVMXC__Case__c = casealias.Id;
        caseLine.SVMXC__Priority__c = 'Medium';
        insert caseLine;
        
        Event ev = new Event();
        ev.WhatId = wo.Id;
        ev.Subject = 'test';
        ev.OwnerId = Userinfo.getUserId();
        ev.StartDateTime = system.now();
        ev.EndDateTime = system.now();
        insert ev;

        CancelCase.cancelCaseAndRelatedObjects(casealias.Id, 'INST');
        Test.stopTest();
        
    }
}
public class CaseButtonLightningCtrl {
	
    @AuraEnabled 
    public static String fetchUserProfile(){
        Id profileId=userInfo.getProfileId();
		String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        return profileName;
    }
    
    @AuraEnabled
    public static list<CaseMemberWrap> getCaseTeamMember(String caseId){
        //caseId = '500m0000007PwPgAAK';
        System.debug('caseId===='+caseId);
        list<CaseMemberWrap> caseMembersList = new list<CaseMemberWrap>();
        for(Case_Team_Member__c cm : [select Id, Case__c, Case__r.CaseNumber, MemberId__c, 
                                      MemberId__r.Name from Case_Team_Member__c 
                                      where Case__c = :caseId]){
            CaseMemberWrap cObj = new CaseMemberWrap();
            cObj.caseNumber = cm.Case__r.CaseNumber;
            cObj.caseMemberName = cm.MemberId__r.Name;
            cObj.caseMember = cm;
            cObj.cmId = cm.Id;
            caseMembersList.add(cObj);
        }
        
        return caseMembersList;
        
    }
    
    public class CaseMemberWrap {
       @AuraEnabled
       public String cmId {get;set;}
       @AuraEnabled
       public String caseNumber {get;set;}
       @AuraEnabled
       public String caseMemberName {get;set;}
       @AuraEnabled
       public Case_Team_Member__c caseMember {get;set;}
  
    }

    @AuraEnabled
    public static String checkCaseCancellation(Case caseObject)
    {

        String message; 
        String recTypeName = caseObject.Recordtype.Name;
        system.debug('==caseObject.Id=='+caseObject.Id);
        List<SVMXC__Service_Order__c> serviceOrder = [Select Id From SVMXC__Service_Order__c Where SVMXC__Case__c =: caseObject.Id AND Event__c = false AND SVMXC__Order_Status__c != 'Cancelled'
                                                      AND SVMXC__Order_Status__c != 'Closed'];
        List<L2848__c> ecfList = [Select Id From L2848__c Where Case__c =: caseObject.Id AND Is_Submitted__c != 'Cancelled'];
        List<SVMXC__Case_Line__c> caseLinesList = [Select Id From SVMXC__Case_Line__c Where SVMXC__Case__c =: caseObject.Id AND SVMXC__Line_Status__c != 'Cancelled'];
        List<Case> caseEcrList = [Select Id, Is_escalation_to_the_CLT_required__c from Case where Id =: caseObject.Id 
									AND Is_escalation_to_the_CLT_required__c = 'Yes'];
        if(caseObject.Status == 'Cancelled')
        {
            message = 'Case is already cancelled';
        }
        else if(serviceOrder != null && serviceOrder.size()>0)
        {
            if(recTypeName == 'HD/DISP'){
                message = 'You cannot cancel this case as there are work orders associated to them.';
            }
            else{
            message = 'IWO\'s must have status Cancelled or Closed to use the Cancel Plan button.';
            }
        }
        else if(ecfList != null && ecfList.size()>0)
        {
            message = 'You cannot cancel this case as there is an escalated complaint associated to it.';
        }
        else if(caseLinesList != null && caseLinesList.size()>0)
        {
            message = 'You cannot cancel this case as there is time associated to it.';
        }
      else if(recTypeName == 'Training' || recTypeName == 'Consulting' || recTypeName == 'INST' || recTypeName == 'Professional Services' || recTypeName == 'HD/DISP')
      {
        try
        {
            CancelCase.cancelCaseAndRelatedObjects(caseObject.Id,recTypeName);
          message = 'Cancelled successfully.';

        }
        catch(Exception e)
        {
          message = 'Error';
        }    
      }
	  else
      {
          message = 'Cancellation not supported for a Case of this Record Type.';
      }
	  system.debug('==caseEcrList=='+ caseEcrList);
	  if( recTypeName == 'HD/DISP' && caseEcrList.size()>0 ){
		message = 'This case cannot be cancelled if Complaint Required says Yes from Case. Please change or re-evaluate your option to Cancel.';
	  }
      
      return message;
    }


    @AuraEnabled
    public static boolean checkECFCount(Case caseObject)
    {
      List<L2848__c> escCompForm = [SELECT Is_Submitted__c FROM L2848__c WHERE Case__c =: caseObject.Id];
       
      //Case rec = [select id,Is_escalation_to_the_CLT_required__c,Closed_Case_Reason__c from Case where id =: caseObject.Id];
      
      boolean ecfCount = false;
      if(caseObject.Is_escalation_to_the_CLT_required__c =='Yes'&& escCompForm.size() > 0)
      {
        for(L2848__c eF: escCompForm) 
        { 
          if (eF.Is_Submitted__c != 'Rejected by Regulatory' && eF.Is_Submitted__c != 'Accepted & Closed by Regulatory' && eF.Is_Submitted__c != 'Cancelled'){
             ecfCount = true;
              System.debug('++++++++++++++ecfCount++++++++++'+ecfCount);
          }
        }
      }
      return ecfCount;
    }

   @AuraEnabled
    public static Id getRecordTypeId(String recTypeName)
    {
      Id devRecordTypeId = Schema.SObjectType.Service_Article__kav.getRecordTypeInfosByName().get(recTypeName).getRecordTypeId();
      return  devRecordTypeId;      //system.debug('Record id:'+devRecordTypeId);
    }


     @AuraEnabled
    public static Case getCaseDateTime(String caseId)
    {
      
      Case c =[select id,Customer_Malfunction_Start_Date_time_Str__c,Customer_Malfunction_Start_Date_time__c,Customer_Requested_Start_Date_Time__c,Customer_Malfunction_End_Date_Time_str__c,Customer_Requested_End_Date_Time__c from Case
                 where id=: caseId];
      return  c; 
    }

     @AuraEnabled
    public static Case_TMWrapper updateCaseTeamMember(String caseId)
    {
        List<Case_Team_Member__c> caseTeamMemberList = new List<Case_Team_Member__c>();
       
        List<Case_Team_Member__c> existingCustomCaseTeamMember = [select Id, Case__c, MemberId__c,TeamRoleId__c  from Case_Team_Member__c where Case__c = :caseId];
        Map<String, String> optionNameToId = new Map<String, String>();
        Map<String, String> optionIdToName = new Map<String, String>();

        for(CaseTeamRole role : [SELECT Name, Id FROM CaseTeamRole])
        {
              optionNameToId.put(role.Name, role.Id);
              optionIdToName.put(role.Id, role.Name);
        }   
       
          caseTeamMemberList.addall(existingCustomCaseTeamMember);
          Case_Team_Member__c member1 = new Case_Team_Member__c();
          Case_Team_Member__c member2 = new Case_Team_Member__c();
          Case_Team_Member__c member3 = new Case_Team_Member__c();  
          member1.Case__c = caseId;
          member2.Case__c = caseId;
          member3.Case__c = caseId;
          caseTeamMemberList.add(member1);   
          caseTeamMemberList.add(member2);   
          caseTeamMemberList.add(member3);

        Case_TMWrapper cTM = new Case_TMWrapper();
        cTM.caseTeamMemberList = caseTeamMemberList;
        cTM.optionNameToId = optionNameToId;
    
        return  cTM; 
    }
    
    @AuraEnabled
    public static Case_TMWrapper deleteTeamMember(String cTM, Integer memberIndex)
    {
        Case_TMWrapper ctmObject = (Case_TMWrapper)JSON.deserialize(cTM, Case_TMWrapper.class);
        ctmObject.caseTeamMemberList.remove(memberIndex);
        return ctmObject;
    }

    @AuraEnabled
    public static Case_TMWrapper addOneRow(String cTM)
    {
        Case_TMWrapper ctmObject = (Case_TMWrapper)JSON.deserialize(cTM, Case_TMWrapper.class);
        Case_Team_Member__c member = new Case_Team_Member__c();
        ctmObject.caseTeamMemberList.add(member);
        return ctmObject;

    }

    @AuraEnabled
    public static String saveTeamMember(String cTM,String caseId)
    {
      try
      {
        Case_TMWrapper ctmObject = (Case_TMWrapper)JSON.deserialize(cTM, Case_TMWrapper.class);

        delete([select Id, MemberId, ParentId, TeamRoleId from CaseTeamMember where ParentId = :caseId]);
        delete([select Id, Case__c, MemberId__c from Case_Team_Member__c where Case__c = :caseId]);

        Map<String, Case_Team_Member__c> customCaseTeamMemberToBeSaved = new Map<String, Case_Team_Member__c>();
        Map<String, CaseTeamMember> teamMemberToBeSaved = new Map<String, CaseTeamMember>();
        for(Case_Team_Member__c teamMemberCustom : ctmObject.caseTeamMemberList)
        {
            if(teamMemberCustom.MemberId__c != null)
            {  
                CaseTeamMember teamMember = new CaseTeamMember();
                teamMember.ParentId = caseId;
                teamMember.MemberId = teamMemberCustom.MemberId__c;
                teamMember.TeamRoleId = ctmObject.optionNameToId.get(teamMemberCustom.TeamRoleId__c);
                teamMemberToBeSaved.put(teamMember.MemberId, teamMember);
                teamMemberCustom.id = null;
            }            
        }
        
        upsert teamMemberToBeSaved.values();

        for(Case_Team_Member__c teamMemberCustom : ctmObject.caseTeamMemberList)
        {
            if(teamMemberCustom.MemberId__c != null)
            {
                customCaseTeamMemberToBeSaved.put(teamMemberCustom.MemberId__c, teamMemberCustom);
            }            
        }     
        upsert customCaseTeamMemberToBeSaved.values();
        return 'Success';
      }

      catch (exception e)
      {
         return 'Failure :' + e.getMessage();
      }
    }
    @AuraEnabled
    public static String saveDates(String d1, String d2, String d3, String caseId)
    {
     
      	Case updatecase = new case(id = caseId);
      	Case_DateWrapper mlSD = (Case_DateWrapper)JSON.deserialize(d1, Case_DateWrapper.class);
      	Case_DateWrapper prSD = (Case_DateWrapper)JSON.deserialize(d2, Case_DateWrapper.class);
      	Case_DateWrapper prED = (Case_DateWrapper)JSON.deserialize(d3, Case_DateWrapper.class);
      
      	if(mlSD.dt != null)
      	{   
        	updatecase.Customer_Malfunction_Start_Date_time_Str__c = mlSD.dt + '-' + convertMonthIntNumberToName(mlSD.month) + '-' + mlSD.fullYear +' '+ mlSD.hour+':'+ mlSD.min +' '+ mlSD.timeFormat;
      	}

      	if(prSD.dt != null)
      	{   
        	updatecase.Customer_Requested_Start_Date_Time__c = prSD.dt + '-' + convertMonthIntNumberToName(prSD.month) + '-' + prSD.fullYear +' '+ prSD.hour+':'+ prSD.min +' '+ prSD.timeFormat;
      	}

      	if(prED.dt != null)
      	{   
        	updatecase.Customer_Requested_End_Date_Time__c = prED.dt + '-' + convertMonthIntNumberToName(prED.month) + '-' + prED.fullYear +' '+ prED.hour+':'+ prED.min +' '+ prED.timeFormat;
      	}

      try
      	{
        	update updatecase;
        	return 'Success';
      	}
      	catch (exception e)
      	{
         	return 'Failure :' + e.getMessage();
      	}
    }
    
     @TestVisible private static String convertMonthIntNumberToName(Integer iMonthNumber)
    {
        if(iMonthNumber == 1)
            return 'Jan';
        else if(iMonthNumber == 2)
            return 'Feb';
        else if(iMonthNumber == 3)
            return 'Mar';
        else if(iMonthNumber == 4)
            return 'Apr';
        else if(iMonthNumber == 5)
            return 'May';
        else if(iMonthNumber == 6)
            return 'Jun';
        else if(iMonthNumber == 7)
            return 'Jul';
        else if(iMonthNumber == 8)
            return 'Aug';
        else if(iMonthNumber == 9)
            return 'Sep';
        else if(iMonthNumber == 10)
            return 'Oct';
        else if(iMonthNumber == 11)
            return 'Nov';
        else if(iMonthNumber == 12)
            return 'Dec';
        else
            return 'Jan';
    }

      @AuraEnabled
    public static String updateCloseCase(String caseId, Case caseObject)
    {
      system.debug('caseId=='+caseId); 
      //caseId= '500m0000007PwPg';
      Case updatecase = [SELECT Id,Local_Case_Activity__c, Closed_Case_Reason__c,Reason,Description,Status,Is_escalation_to_the_CLT_required__c, Is_This_a_Complaint__c,
                           (SELECT Id, Name, Is_Submit__c FROM L2848__r),
                           (select Id from SVMXC__Case_Lines__r )
                           FROM Case WHERE Id = :caseId];
      try
      {
        caseObject.id = caseId;

        if(caseObject.Closed_Case_Reason__c == 'Created in Error' && updatecase.SVMXC__Case_Lines__r.size() > 0 )
        {
           return 'Failure :' + 'All Field Service Work Order must be closed before you can close this case';
        }

        if(updatecase.Reason == null || updatecase.Description == null)
        {
             return 'Failure :' + 'Case Reason and Description are mandatory to close the case.';
        }  

        if(caseObject.Local_Case_Activity__c == null && (caseObject.Closed_Case_Reason__c != 'Customer Cancelled' && caseObject.Closed_Case_Reason__c != 'Created in Error' )&&(updatecase.Status != 'Closed by Work Order'))
        {
            return 'Failure :' + 'Please enter a recommendation.';
        }   

        if(updatecase.Is_escalation_to_the_CLT_required__c == 'Yes' && updatecase.Is_This_a_Complaint__c == 'Yes')
        {
            if(updatecase.L2848__r.size() == 0)
            {
                return 'Failure :' + 'The Case cannot be closed since there are no L2848 forms associated to it.';
            }
            
        } 
        if(caseObject.Case_Type__c <> null && caseObject.Case_Type__c == 'Technical')
        {
            caseObject.Classification_1__c = 'N/A';
            caseObject.Classification_2__c = 'N/A';
        } 
        updatecase.status = 'Closed';
        updatecase.Case_Type__c = caseObject.Case_Type__c;
        updatecase.Classification_1__c = caseObject.Classification_1__c;
        updatecase.Classification_2__c = caseObject.Classification_2__c;
        updatecase.Closed_Case_Reason__c = caseObject.Closed_Case_Reason__c;
        updatecase.Local_Case_Activity__c = caseObject.Local_Case_Activity__c;
        update updatecase;      
        return 'Success';
      }
      catch (exception e)
      {
         return 'Failure :' + e.getMessage();
      }
    }

    @AuraEnabled
    public static case getCase(String caseId)
    {
      system.debug('caseId=='+caseId); 
      //caseId= '500m0000007PwPg';
      Case updatecase = [SELECT Id,Local_Case_Activity__c, Closed_Case_Reason__c,Reason,Description,Status,Is_escalation_to_the_CLT_required__c, Is_This_a_Complaint__c,Applies_To__c,ECF_Investigation_Notes__c,Symptoms__c,Subject
                           FROM Case WHERE Id = :caseId];
      return updateCase;
    }

   @AuraEnabled
    public static SR_PWOWrapper loadPWO(String caseId)
    {
       SR_PWOWrapper pwoObj = new SR_PWOWrapper();
       pwoObj.objCS = [select id, Createddate,Service_Type__c, CaseNumber,Subject,SVMXC__Top_Level__c, SVMXC__Top_Level__r.SVMXC__Service_Contract__c, SVMXC__Site__c,description,ProductSystem__c,
                       ProductSystem__r.name,Accountid,Account.Name,Productid,Ownerid,Type,recordtype.developerName From case where Id =:caseId ]; 
       pwoObj.woObj = new SVMXC__Service_Order__c ();

       Set<Id> soiSize = new Set<Id>(); 
       Set<Id> soiSET = new Set<Id>(); 

       for(SVMXC__Case_Line__c caseLine : [select id,SVMXC__Location__r.ERP_Country_Code__c,Start_Date_time__c,SVMXC__Location__c,SVMXC__Case__c,SVMXC__Case__r.AccountID,End_date_time__c,name,Sales_Order_Item__r.Product_Model__c,Sales_Order_Item__r.Location__c,Sales_Order_Item__c,Sales_Order_Item__r.name,Sales_Order_Item__r.Installed_Product__c from SVMXC__Case_Line__c where SVMXC__Case__c =:caseId and Sales_Order_Item__c!=null])
       {
          if(caseLine.SVMXC__Location__c == null)
            pwoObj.locationMissing = true;                             
           
         
          if(caseLine.Sales_Order_Item__c!=null )
            soiSize.add(caseLine.Sales_Order_Item__c);

          if(caseLine.Sales_Order_Item__c!=null && caseLine.Sales_Order_Item__r.Product_Model__c!=null && caseLine.Sales_Order_Item__r.Location__c!=null)
            soiSET.add(caseLine.Sales_Order_Item__c);
           
          if(caseLine.Start_Date_time__c == null || caseLine.End_date_time__c == null)
            pwoObj.caseLineDateValuesMissing = true;                            
            
          if(pwoObj.woObj.SVMXC__Preferred_Start_Time__c == null && caseLine.Start_Date_time__c != null) 
            pwoObj.woObj.SVMXC__Preferred_Start_Time__c  = caseLine.Start_Date_time__c;            
            
           
          if (pwoObj.woObj.SVMXC__Preferred_End_Time__c == null &&  caseLine.End_date_time__c != null) 
            pwoObj.woObj.SVMXC__Preferred_End_Time__c = caseLine.End_date_time__c;             
                                    
       }
       
       for(ERP_WBS__c ObjERPWBS : [select id,name,Sales_Order_Item__c from ERP_WBS__c where Sales_Order_Item__c in : soiSize])
       {
            soiSize.remove(ObjERPWBS.Sales_Order_Item__c);              
       }
      
       if(soiSize.size()> 0)
         pwoObj.WBSmissing = true; 

       return pwoObj;
      
    }


    public class SR_PWOWrapper
    {
        @AuraEnabled public Boolean locationMissing;
        @AuraEnabled public Boolean caseLineDateValuesMissing;
        @AuraEnabled public Boolean WBSmissing;
        @AuraEnabled public case objCS;
        @AuraEnabled public SVMXC__Service_Order__c woObj;
       
   }


    public class Case_DateWrapper
    {
        @AuraEnabled public Integer fullYear;
        @AuraEnabled public Integer month;
        @AuraEnabled public String dt;
        @AuraEnabled public String hour;
        @AuraEnabled public String min;
        @AuraEnabled public String timeFormat;
   }

  public class Case_TMWrapper
  {
      @AuraEnabled public List<Case_Team_Member__c> caseTeamMemberList;
      @AuraEnabled public Map<String, String> optionNameToId ;
  }
}
@isTest
public class CaseButtonLightningCtrlTest {
    
    public static User u;
    public static Contact con;
    public static Account a;
    public static Case case1;
    public static Case_Team_Member__c caseTeamMember;
    public static Sales_Order_Item__c SalesOI;
    public static SVMXC__Case_Line__c SVMXCCase;
    public static SVMXC__Service_Order__c SVMXCService;
    static
    {        
    
        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   
        
        a = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                        BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert a;  
        
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
                          AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
                          MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        //Creating a running user with System Admin profile
        insert con;
        
        
        Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType,status = 'new',Subject= 'case Subject', Priority = 'Low',
                          Product_System__c = 'Test Prod System',  Date_of_Event__c = System.Today(), Date_Reported_to_Varian__c = System.Today(),
                        Is_escalation_to_the_CLT_required__c = 'Yes',Is_This_a_Complaint__c =  'Yes',Case_Type__c = 'Technical',
                         Closed_Case_Reason__c = 'Created in Error'
                        );
        insert case1;
        
        Case_Team_Member__c cm1 = new Case_Team_Member__c (Case__c= case1.id, MemberId__c=UserInfo.getUserId(), TeamRoleId__c = 'Clinical HD');
        insert cm1;        
        SalesOI = new Sales_Order_Item__c (Site_Partner__c = a.id);
        insert SalesOI;
                                 
    }
    
    public static testMethod void getCaseTeamMemberTest(){
        test.startTest();          
        CaseButtonLightningCtrl.fetchUserProfile();
        CaseButtonLightningCtrl.getCaseTeamMember(case1.Id);
        CaseButtonLightningCtrl.checkCaseCancellation(case1);
        CaseButtonLightningCtrl.checkECFCount(case1);
        CaseButtonLightningCtrl.getRecordTypeId('How To');
        CaseButtonLightningCtrl.getCaseDateTime(case1.Id);
        CaseButtonLightningCtrl.Case_TMWrapper objCase=CaseButtonLightningCtrl.updateCaseTeamMember(case1.Id);
        String strSobjects = JSON.Serialize(objCase);
       
        CaseButtonLightningCtrl.deleteTeamMember( strSobjects , 0);
        CaseButtonLightningCtrl.addOneRow( strSobjects);
        CaseButtonLightningCtrl.saveTeamMember( strSobjects , case1.Id);
        system.debug('obj---'+objCase);
        
        /*DateTime testDate = DateTime.now();
        String d1 =  JSON.serialize(testDate);
         system.debug('testd1'+d1);
        String d2 =  JSON.serialize(testDate);
        String d3 =  JSON.serialize(testDate);
        CaseButtonLightningCtrl.saveDates(d1, d2, d3,case1.Id);
        */
        test.stopTest();
    }
    
    public static testMethod void convertMonthIntNumberToNameTest(){  
        test.startTest(); 
        for(Integer i=1;i<= 12;i++)
        {
            CaseButtonLightningCtrl.convertMonthIntNumberToName(i);
        }
        test.stopTest();
    }
    
    public static testMethod void updateCloseCaseTest(){
        
        test.startTest(); 
        CaseButtonLightningCtrl.updateCloseCase(case1.Id,case1);
        CaseButtonLightningCtrl.loadPWO(case1.Id);
        test.stopTest();
    }
    
     public static testMethod void CaseCancellationTest(){
        
        Id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;
         
        Product2 prod = new Product2(name = 'testprod');
        insert prod;
        
        Product_Version__c prodver = new Product_Version__c(Major_Release__c = 1,Minor_Release__c = 0);
        insert prodver;
        
        Product_Version_Build__c prodverbuild = new Product_Version_Build__c(name = 'prodverbuildtest',Product__c = prod.id, Build_Number__c = 1,Product_Version__c=prodver.id);
        insert prodverbuild;
        
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id,
        Product_Version_Build__c = prodverbuild.Id);
        insert objIP;  
         
        //Id TrainingRecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'INST'].Id;
        Case case2 = new Case(ContactId = con.Id, AccountId = a.Id,status = 'new',Subject= 'case Subject', Priority = 'Low',
                          Product_System__c = 'Test Prod System', ProductSystem__c = objIP.Id, Date_of_Event__c = System.Today(), Date_Reported_to_Varian__c = System.Today(),
                             Is_escalation_to_the_CLT_required__c = 'Yes', Is_This_a_Complaint__c =  'Yes', Reason = 'test', Description = 'test' 
                             );
        insert case2;
              
         
        Case_Team_Member__c cm2 = new Case_Team_Member__c (Case__c= case2.id, MemberId__c=UserInfo.getUserId(), TeamRoleId__c = 'Clinical HD');
        insert cm2;
        test.startTest();
        SVMXCCase = new SVMXC__Case_Line__c (SVMXC__Case__c = case2.id, Sales_Order_Item__c = SalesOI.id, Hours__c = 5); 
        insert SVMXCCase;
        
        SVMXCService = new SVMXC__Service_Order__c (SVMXC__Case__c =  case2.id,Event__c = false,SVMXC__Order_Status__c = 'Open');
        insert SVMXCService;
        
        CaseButtonLightningCtrl.checkCaseCancellation(case2);
        CaseButtonLightningCtrl.checkECFCount(case2);
        String jsonInput = '{"fields":{"project":{"key":"TEST"},"summary":"REST ye merry gentlemen.","description":"Creating of an issue using project keys and issue type names using the REST API","issuetype":{"name":"Bug"}}}';
        CaseButtonLightningCtrl.saveDates(jsonInput, jsonInput, jsonInput, case2.Id);
        CaseButtonLightningCtrl.loadPWO(case2.Id);
        CaseButtonLightningCtrl.updateCloseCase(case2.Id, case2);
        case2.Local_Case_Activity__c = 'test';
        update case2;
        CaseButtonLightningCtrl.updateCloseCase(case2.Id, case2);
         
        case2.Is_escalation_to_the_CLT_required__c = 'No';
        update case2;
        CaseButtonLightningCtrl.updateCloseCase(case2.Id, case2);
        test.stopTest();
         
    }
    
}
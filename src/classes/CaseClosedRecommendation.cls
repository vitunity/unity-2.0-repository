public class CaseClosedRecommendation{
    public static void WorkPerformed(List<case> lstc, map<Id,Case> mapOld){
        map<Id,case> mapCase = new map<Id,case>();
        Id CaseHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        Id WOHD = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        for(case c : lstc){
            if(c.Status == 'Closed' && c.status <> mapOld.get(c.Id).status && c.RecordTypeId == CaseHD){
                mapCase.put(c.Id,c);
            }
        }
        if(mapCase.size() > 0 ){
            list<SVMXC__Service_Order__c> lstWORK = new List<SVMXC__Service_Order__c>();
            for(SVMXC__Service_Order__c WO : [select Id,SVMXC__Case__c,SVMXC__Work_Performed__c from SVMXC__Service_Order__c where SVMXC__Case__c IN: mapCase.keyset() 
            and RecordTypeId =: WOHD ]){
                if(mapCase.containsKey(WO.SVMXC__Case__c )){
                    //Update to strip HTML from Case -> Work Order
                    WO.SVMXC__Work_Performed__c  = mapCase.get(WO.SVMXC__Case__c).Description.replaceAll('<[^>]+>',' ');
                    //WPerformed = WPerformed.replaceAll('<[/a-zAZ0-9]*>','');
                    //WO.SVMXC__Work_Performed__c  = WPerformed;
                    lstWORK.add(WO);
                }
            }
            update lstWORK;
        }
    }
}
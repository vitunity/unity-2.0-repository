@isTest
Public class CaseClosedRecommendationTest
{
   public static testmethod void CaseClosedRecommendation()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        Id recCase_HD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        Id recWO_HD = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        
        
        Account varAcc = UnityDataTestClass.AccountData(true);
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='India' ; 
        insert reg ;
        
        Contact varCont = new Contact();
        varCont.FirstName ='firsttest';
        varCont.Account = varAcc;
        varCont.LastName = 'test contact';
        varCont.Phone = '55667688';
        varCont.Registered_Portal_User__c = True;   
        varCont.MobilePhone ='55667688';
        varCont.OtherPhone = '55667688';
        varCont.mailingcountry = 'India';
        varCont.ERP_Reference__c = 'ERP_Site_Partner_Code';
        insert varCont;
        
        SVMXC__Installed_Product__c varIP_1 = new SVMXC__Installed_Product__c(Name = 'H68876');
        insert varIP_1;
        
        
        System.runAs ( thisUser )
        {     
            Case varCase_HD = new case();
            varCase_HD.OwnerId = thisUser.Id;                                       //Owner Id
            varCase_HD.RecordTypeId = recCase_HD;                                   //Record Type
            varCase_HD.AccountiD = varAcc.id;                                       //Account
            varCase_HD.ContactId = varCont.id;                                      //Contact
            varCase_HD.ProductSystem__c = varIP_1.id;                               //IP
            varCase_HD.Status = 'In Progress';                                      //Status
            varCase_HD.Local_description__c  ='Local_description';                  //Local Description
            varCase_HD.Priority = 'Medium';                                         //Priority
            varCase_HD.Local_Subject__c = 'Test';                                   //Local subject
            varCase_HD.ERP_Reference__c = 'HERP_Reference';                         //ERP Reference
            varCase_HD.Is_This_a_Complaint__c = 'Yes';                              //Complaint
            varCase_HD.Was_anyone_injured__c = 'Yes';                               //Complaint
            varCase_HD.Case_ECF_Count__c = 1;                                       //Set Case ECF Count
            varCase_HD.Case_ECF_Submitted__c = 1;                                  //Set Case ECF Submitted.
            varCase_HD.ERP_Functional_Location__c = 'EFL';                          //ERP functional Location
            varCase_HD.ERP_Top_Level__c = 'H11111';                                 //ERP Top-Level
            varCase_HD.Reason = 'System down';                                      //Reason
            //varCase_HD.Case_Preferred_Technician__c = varTech.Id;                   //Preferred Tech
            varCase_HD.SVMXC__Actual_Onsite_Response__c = system.now();
            //varCase_HD.Test_Equipment__c = varEquip.Id;
            varCase_HD.Initial_Schedule_Date_Time__c = system.now();
            varCase_HD.SVMXC__Scheduled_Date__c = system.today();
            varCase_HD.Local_Case_Activity__c  ='Local_Case_Activity';
            varCase_HD.Local_Internal_Notes__c   ='Local_Internal_Notes';
            varCase_HD.Downtime_start__c  = system.now();
            varCase_HD.Downtime_End__c = system.now() + 1;
            varCase_HD.ERP_Site_Partner_Code__c  = 'ERP_Site_Partner_Code';
            varCase_HD.ERP_Contact_Code__c = 'Contact_Code';
            varCase_HD.Malfunction_Start__c =  system.now();
            varCase_HD.Requested_Start_Date_Time__c = system.now() + 5 ; 
            varCase_HD.Requested_End_Date_Time__c = system.now() + 10 ; 
            varCase_HD.Customer_Malfunction_Start_Date_time__c = system.today();
            varCase_HD.Customer_Preferred_Start_Date_time__c = system.today();
            varCase_HD.SVMXC__Preferred_Start_Time__c = null;
            varCase_HD.Is_escalation_to_the_CLT_required__c = 'Yes';
            varCase_HD.Customer_Malfunction_Start_Date_time_Str__c = '1-Jan-2011 5:30 AM';
            varCase_HD.Customer_Requested_Start_Date_Time__c = '1-Jan-2011 5:30 AM';
            varCase_HD.Customer_Requested_End_Date_Time__c = '3-Jan-2011 7:30 PM';
            varCase_HD.Primary_Contact_Number__c = 'Phone';
            varCase_HD.SVMXC__Top_Level__c = varIP_1.ID;
            Insert varCase_HD;
            
            
            caseComment cc = new CaseComment();
            cc.parentId = varCase_HD.id;
            cc.commentbody ='test';
            insert cc;
            
            SVMXC__Service_Order__c varWO_HD = new SVMXC__Service_Order__c();
            varWO_HD.SVMXC__Order_Status__c = 'Open';
            varWO_HD.SVMXC__Case__c = varCase_HD.Id;
            varWO_HD.recordtypeID = recWO_HD;
            insert varWO_HD;
            
            varCase_HD.Status = 'Closed';
            update varCase_HD;
            
            
        }
    }
}
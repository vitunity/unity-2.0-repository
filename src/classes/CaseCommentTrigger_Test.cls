@isTest(SeeAllData=true)
Public class CaseCommentTrigger_Test
{
   public static testmethod void CaseCommentTrigger ()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
            
            Regulatory_Country__c reg = new Regulatory_Country__c();
            reg.Name='Test' ; 
            insert reg;
           
            Account a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Test', MailingState='teststate', Phone = '1214445');
            insert con;
            
            
            Contact_Role_Association__c cro = new Contact_Role_Association__c(contact__c = con.Id, Account__c = a.Id);
            insert cro;
            
            
            Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
            Case case1 = new Case(Priority = 'Normal',ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType,status = 'new',Subject= 'case Subject',Reason='Test');
            insert case1;
            
            caseComment c = new caseComment();
            c.IsPublished = true;
            c.CommentBody = 'test';
            c.ParentId = case1.Id;
            insert c;
            
        }
    }
}
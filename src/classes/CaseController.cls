public with sharing class CaseController{

    public Case caseObj{get;set;}
    
    public CaseController(ApexPages.StandardController controller){     
        this.caseObj = (Case)controller.getRecord();
    }
    
    public String getContactId() {
        return System.currentPageReference().getParameters().get('def_contact_id');
    }
    public String getAccountId() {
        return System.currentPageReference().getParameters().get('def_account_id');
    }            
    public String getOrigin() {
        return System.currentPageReference().getParameters().get('origin');
    }  
    String userLanguage;
    public String getLanguageLocaleKey() {
        if(userLanguage == null) {
            userLanguage = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
        }
        return userLanguage;
    }
    
}
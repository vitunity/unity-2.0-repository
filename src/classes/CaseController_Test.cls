@isTest
public class CaseController_Test 
{
    public static testMethod void test() {
        test.startTest();

    Account Acc = new Account();
    //Acc.ShippingCity = 'abc';
    //Acc.ShippingCountry = 'India';
    //Acc.ShippingState = 'UP';
    Acc.BillingCity = 'abc';
    Acc.BillingCountry = 'India';
    Acc.BillingState = 'UP';
    Acc.Name = 'TestAccount';
    Acc.Country__c = 'India';
    insert Acc;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        objContact.AccountId = Acc.id;
        insert objContact;
        
        Case testcase = SR_testdata.createcase();
        testcase.ContactID = objContact.id;
        testcase.AccountId = Acc.id;
        Insert testcase ;
        
        ApexPages.currentPage().getParameters().put('id',testcase.id);
        ApexPages.StandardController testcontroller = new ApexPages.StandardController(testcase);
        CaseController controller = new CaseController(testcontroller);
        controller.getAccountId();
        controller.getContactId();
        controller.getOrigin();
        controller.getLanguageLocaleKey();
        
        test.stopTest();
    }
}
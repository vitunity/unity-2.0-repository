/********************************
Author: N/A
Created Date: N/A
Project/Story/Inc/Task : N/A
Description: 

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Sep-01-2017 : Rishabh Verma : STSK0012417: change caseline overlap error message for time overlap

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
oct-03-2017 : Rishabh Verma : STSK0012967: change caseline overlap error message for time overlap
********************************/

public class CaseLineOverlapCheck 
{
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); //CM : DFCT0011680 : Fixed to fix the overlap issues for Cases other than HD
    public static void checkForCaseLineOverlap(List<SVMXC__Case_Line__c> clList, Map<Id, SVMXC__Case_Line__c> oldMap)
    {
        Map<Integer, List<String>> errorMap = new Map<Integer, List<String>>();
        Set<Id> setEmployeeId = new Set<Id>();
        
        Set<Id> oldCLIdSet = new Set<Id>();
        Set<Id> caseids = new set<Id>();
        
        system.debug(' == oldMap == '+ oldMap);
        if(oldMap != null){
            for(SVMXC__Case_Line__c caseline : oldMap.values()){
               oldCLIdSet.add(caseline.Id);
            } 
        }

        Datetime mindate = system.now();
        for(SVMXC__Case_Line__c caseline : clList) {
            
                if(caseline.Start_Date_time__c < mindate){
                    mindate = caseline.Start_Date_time__c;
                }
                if(caseline.Case_Line_Owner__c != null){
                    setEmployeeId.add(caseline.Case_Line_Owner__c);
                }
                //CM : STSK0011683 : April 2017 : Added the below code to fix the case line overlap for non HelpDesk Caselines
                if(caseline.SVMXC__Case__c != null)
                {
                    caseids.add(caseline.SVMXC__Case__c);
                }
        }
        //CM : STSK0011683 : April 2017 : Added the below code to fix the case line overlap for non HelpDesk Caselines
        Map<id,Id> case2recordtype = new map<id,Id>();
        for (case c : [select id, RecordtypeId from Case where id in :caseids])
        {
            case2recordtype.put(c.id, c.RecordtypeId);
        }
        mindate = mindate - 1;
        system.debug('**mindate'+mindate);
        List<SVMXC__Case_Line__c> otherCLList = null;
        if(setEmployeeId.size() > 0){
            otherCLList = [SELECT Id, Name, Start_Date_time__c, Case_Line_Owner__c, End_date_time__c, SVMXC__Case__r.RecordtypeId, SVMXC__Case__r.CaseNumber
                                FROM SVMXC__Case_Line__c 
                                WHERE Case_Line_Owner__c IN :setEmployeeId AND (Start_Date_time__c >: mindate OR End_date_time__c >: mindate) AND Id NOT IN : oldCLIdSet  
                                ORDER BY Start_Date_time__c ASC];
             
        
        }
        else {
            otherCLList = new List<SVMXC__Case_Line__c>();
        }
        system.debug('==otherCLList =='+otherCLList);
        for(Integer i = 0; i < clList.size(); ++i) {
            SVMXC__Case_Line__c caseline = clList.get(i);
            
                if(!errorMap.containsKey(i)){
                    errorMap.put(i, new List<String>());
                }
                for(SVMXC__Case_Line__c othercaseline : clList){
                    if ( (case2recordtype.size() > 0 && case2recordtype.containsKey(caseline.SVMXC__Case__c) && case2recordtype.get(caseline.SVMXC__Case__c) != recHD) || othercaseline.SVMXC__Case__r.RecordtypeId != recHD) continue; //CM : DFCT0011680 : Fixed to fix the overlap issues for Cases other than HD //CM : STSK0011683 : April 2017 : Added the below code to fix the case line overlap for non HelpDesk Caselines
                    if((caseline != othercaseline) && (caseline.Case_Line_Owner__c == othercaseline.Case_Line_Owner__c)) {   
                         if (othercaseline.End_date_time__c == null || 
                        (caseline.Start_Date_time__c < othercaseline.Start_Date_time__c && caseline.End_date_time__c > othercaseline.Start_Date_time__c) 
                        || (caseline.Start_Date_time__c > othercaseline.Start_Date_time__c && caseline.End_date_time__c < othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c < othercaseline.End_date_time__c && caseline.End_date_time__c > othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c == othercaseline.Start_Date_time__c && caseline.End_date_time__c == othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c > othercaseline.Start_Date_time__c && caseline.Start_Date_time__c < othercaseline.End_date_time__c)
                        || (caseline.End_date_time__c > othercaseline.Start_Date_time__c && caseline.End_date_time__c < othercaseline.End_date_time__c)
                        || (othercaseline.Start_Date_time__c > caseline.Start_Date_time__c && othercaseline.Start_Date_time__c < caseline.End_date_time__c)
                        || (othercaseline.End_date_time__c > caseline.Start_Date_time__c && othercaseline.End_date_time__c < caseline.End_date_time__c)
                        )
                        {
                            system.debug('**errorMap**');
                            if(!errorMap.isempty() && errorMap.containsKey(i) &&  errorMap.get(i) != Null &&  othercaseline.Start_Date_time__c != Null )
                            {
                                //if(caseline.Id == null || (caseline.Id != null && caseline.Id != otherDetail.Id))
                                {
                                    //errorMap.get(i).add(othercaseline.Name 
                                        //+ ' (' + othercaseline.Start_Date_time__c.format('hh:mma') 
                                        //+ ' - ' + (othercaseline.End_date_time__c != null ? othercaseline.End_date_time__c.format('hh:mma') : '') + ')');
                                
                                  // Sep-01-2017 : Rishabh Verma : STSK0012417: change caseline overlap error message for time overlap
                                  // STSK0012967: change caseline overlap error message for time overlap
                                  errorMap.get(i).add(
                                            + ' (' + caseline.Start_Date_time__c.format('hh:mma') 
                                            + ' - ' + (caseline.End_date_time__c != null ? caseline.End_date_time__c .format('hh:mma') : '') + ')'
                                            + ' overlaps with existing caseline' 
                                            + othercaseline.Name
                                            + ' (' + othercaseline.Start_Date_time__c.format('hh:mma') 
                                            + ' - ' + (othercaseline.End_date_time__c != null ? othercaseline.End_date_time__c .format('hh:mma') : '') + ')'
                                            + ' on case '
                                            + othercaseline.SVMXC__Case__r.CaseNumber);
                                
                                }
                            }
                        }
                    }
                }
                for(SVMXC__Case_Line__c  othercaseline : otherCLList) {
                    if ( (case2recordtype.size() > 0 && case2recordtype.containsKey(caseline.SVMXC__Case__c) && case2recordtype.get(caseline.SVMXC__Case__c) != recHD) || othercaseline.SVMXC__Case__r.RecordtypeId != recHD) continue; //CM : DFCT0011680 : Fixed to fix the overlap issues for Cases other than HD //CM : STSK0011683 : April 2017 : Added the below code to fix the case line overlap for non HelpDesk Caselines
                    if((caseline != othercaseline) && (caseline.Case_Line_Owner__c == othercaseline.Case_Line_Owner__c )) {
                         if(othercaseline.End_date_time__c == null || 
                        (caseline.Start_Date_time__c < othercaseline.Start_Date_time__c && caseline.End_date_time__c > othercaseline.Start_Date_time__c) 
                        || (caseline.Start_Date_time__c > othercaseline.Start_Date_time__c && caseline.End_date_time__c < othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c < othercaseline.End_date_time__c && caseline.End_date_time__c > othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c == othercaseline.Start_Date_time__c && caseline.End_date_time__c == othercaseline.End_date_time__c)
                        || (caseline.Start_Date_time__c > othercaseline.Start_Date_time__c && caseline.Start_Date_time__c < othercaseline.End_date_time__c)
                        || (caseline.End_date_time__c > othercaseline.Start_Date_time__c && caseline.End_date_time__c < othercaseline.End_date_time__c)
                        || (othercaseline.Start_Date_time__c > caseline.Start_Date_time__c && othercaseline.Start_Date_time__c < caseline.End_date_time__c)
                        || (othercaseline.End_date_time__c > caseline.Start_Date_time__c && othercaseline.End_date_time__c < caseline.End_date_time__c)
                        )  
                        {
                            system.debug('**errorMap1**');
                            if(!errorMap.isempty() && errorMap.containsKey(i) &&  errorMap.get(i) != Null  && othercaseline.Start_Date_time__c != Null )
                            {
                                //errorMap.get(i).add(othercaseline.Name 
                                //+ ' (' + othercaseline.Start_Date_time__c.format('hh:mma') 
                                //+ ' - ' + (othercaseline.End_date_time__c != null ? othercaseline.End_date_time__c.format('hh:mma') : '') + ')');
                              
                                  // Sep-01-2017 : Rishabh Verma : STSK0012417: change caseline overlap error message for time overlap
                                  // STSK0012967: change caseline overlap error message for time overlap
                                  errorMap.get(i).add(
                                            + ' (' + caseline.Start_Date_time__c.format('hh:mma') 
                                            + ' - ' + (caseline.End_date_time__c != null ? caseline.End_date_time__c.format('hh:mma') : '') + ')'
                                            + ' overlaps with existing caseline ' 
                                            + othercaseline.Name
                                            + ' (' + othercaseline.Start_Date_time__c.format('hh:mma') 
                                            + ' - ' + (othercaseline.End_date_time__c != null ? othercaseline.End_date_time__c.format('hh:mma') : '') + ')'
                                            + ' on case '
                                            + othercaseline.SVMXC__Case__r.CaseNumber);
                            
                            
                            }
                        }
                    }
                }
                System.debug(LoggingLevel.WARN, 'Error Map Size: ' + errorMap.get(i).size());

                if (errorMap.get(i).size() > 0)
                {
                    caseline.SVMXC__Description__c= system.label.SVMXC_Activity_Overlap + String.join(errorMap.get(i), '; ');
                    caseline.addError(caseline.SVMXC__Description__c);
                } else {
                    if(oldMap != null && oldMap.get(caseline.Id).SVMXC__Description__c!= null) 
                    {
                        caseline.SVMXC__Description__c= '';
                    }
                }
            }
        }
}
public with sharing  class CasesController {

public ApexPages.StandardSetController standardController;
Public List<Case> selectedCases {get;set;}
Public List<Case> selected {get;set;}

    public CasesController(ApexPages.StandardSetController controller) 
    {
        selectedCases = new List<Case>();
        selected = new List<Case>();
        this.standardController = controller;
        system.debug('this.standardController>'+this.standardController);
    }
    
    Public Pagereference UpdateCases()
    {
    selectedCases = (List<Case>) standardController.getSelected();
        if(test.isRunningTest()){
            selectedCases = (List<Case>) standardController.getRecords();
        }
    system.debug('selectedCases >>>'+selectedCases);
    Set<Id> SetCaseId = new Set<Id>();
    List<Case> selectedCaseSet = new List<Case>();
        for(Case selectedCase : selectedCases)
        {
            SetCaseId.add(selectedCase.id);
        } 
        
        if(SetCaseId.size()>0)
        {
        selected = [select id,lastmodifiedbyId,Priority,Status,Reason,Is_This_a_Complaint__c,Local_Case_Activity__c,Closed_Case_Reason__c,Ownerid from case where id in : SetCaseId ];
            for(Case selectedCase : selected )
            {
                if(selectedCase.Status != 'Closed' ) 
                {
                    selectedCase.Reason = 'Information';
                    selectedCase.Priority = 'low';
                    selectedCase.Status = 'Closed';
                    selectedCase.Is_This_a_Complaint__c = 'No';
                    selectedCase.Is_escalation_to_the_CLT_required__c= 'No';
                    selectedCase.Was_anyone_injured__c = 'No';
                    //selectedCase.Local_Case_Activity__c = 'Created in Error by Spam email';
                    selectedCase.Local_Case_Activity__c = 'Created in Error';
                    selectedCase.Closed_Case_Reason__c = 'Created in Error';
                    selectedCase.Ownerid = userinfo.getUserId();
                    selectedCase.ClosedBy__c = userinfo.getUserId(); 
                    
                    selectedCaseSet.add(selectedCase);
                }
                
            } 
            if(selectedCaseSet.size()>0)
            {
                update selectedCaseSet;
            }
        }      
   
        return standardController.Save();
    }       
}
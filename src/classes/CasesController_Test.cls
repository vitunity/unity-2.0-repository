@isTest
Public class CasesController_Test
{
     static testmethod void CasesController() 
     {           
        Account a = new Account(name='test2',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',Country__c='Test',Legal_Name__c ='Testing'); 
        insert a;
        List<Case> lstCases = new List<Case>();
        lstCases.add(new case(Subject = 'Test Case',AccountId =a.id, Priority='Medium'));
        insert lstCases ;
        ApexPages.currentPage().getParameters().PUT('id',string.valueOf(lstCases[0] .id)); 
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstCases);      
        CasesController contr = new CasesController(sc);
        
        contr.UpdateCases();
    }

   
}
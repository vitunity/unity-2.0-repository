public class CatalogPartPrice {
  public Catalog_Part__c catalogPart {get;set;}
  public Product2 product {get;set;}
  public CPQ_Region_Mapping__c factors;

  private Double standardPrice  = 0.0;
  private Double goalPrice      = 0.0;
  private Double thresholdPrice = 0.0;
  private Double pricebookPrice = 0.0;
  
  public Double targetPrice = 0.0;
  public String country {get;set;}
  
  private Id stdPricebookId;
  private String longDescription = '';

  public Boolean isDiscountable() {
    return product.Discountable__c != null && product.Discountable__c.equals('TRUE');
  }

  public CatalogPartPrice(Catalog_Part__c catPart, Product2 newProduct, Id standardPricebookId, String selectedCountry, CPQ_Region_Mapping__c curRegionfactors, String selectedFamily, String selectedLine, String selectedCurrency) {
    catalogPart = catPart;
    factors = curRegionfactors;
    if(newProduct != null) {
      product = newProduct;
    }
    else {
      product = new Product2();
    }

    stdPricebookId = standardPricebookId;
    
    //added by shital
  if(selectedCountry != null){
    country = selectedCountry; 
    
  }else{
    country = 'USA';
  }
  }
  
  public String getCatalogPartId(){
    return catalogPart.Id;
  }

  // Calculate the Standard Price
  public Double getStandardPrice() {
    Double std_factor = 1;
    Double base_price = 0;

    if(!catalogPart.Catalog_Part_Prices__r.isEmpty()) {
      std_factor = catalogPart.Catalog_Part_Prices__r[0].Standard_Factor__c;
    }

    // get standard pricebook entry
    for(PricebookEntry pbe : product.PricebookEntries) {
      if(pbe.Pricebook2Id == stdPricebookId) {
        base_price = pbe.UnitPrice;
        break;
      }
    }
    standardPrice = std_factor * base_price;
    return std_factor * base_price;
  }

  public Double getPricebookPrice() {
    Double std_factor = 1;
    Double base_price = 0;

    if(!catalogPart.Catalog_Part_Prices__r.isEmpty()) {
      std_factor = catalogPart.Catalog_Part_Prices__r[0].Standard_Factor__c;
    }

    // get other pricebook entry
    for(PricebookEntry pbe : product.PricebookEntries) {
      if(pbe.Pricebook2Id != stdPricebookId) {
        base_price = pbe.UnitPrice;
        break;
      }
    }

    return std_factor * base_price;
  }

  // Calculate the Goal Price based on the Standard Price
  /*public Double getGoalPrice() {
    Double goal_factor = 1;

    if(isDiscountable() && !catalogPart.Catalog_Part_Prices__r.isEmpty()) {
      goal_factor = catalogPart.Catalog_Part_Prices__r[0].Goal_Factor__c;
    }

    return goal_factor * getStandardPrice();
  }*/

  //Added by Shital start
  // Calculate the Target Price 
  public Double getTargetPrice() {
    
    Double product_DASP = 0.0;
    Double DASP_Factor = 1;
    
    Double target_factor = 1;
    
    if(product.Discountable__c== 'TRUE'){
    //List<CPQ_Region_Mapping__c> factors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c where Country__c= :country];
    
      if(product.DASP__c!= NULL)
        product_DASP =  product.DASP__c;
      if(factors.DASP_Factor__c!= NULL)
        DASP_Factor = factors.DASP_Factor__c;
      if(factors.Regional_Target_Factor__c!= NULL)
        target_factor = factors.Regional_Target_Factor__c;  
    
    targetPrice = product_DASP *DASP_Factor*target_factor;
    }
    else{
     if(product.DASP__c!= NULL){
        targetPrice = product.DASP__c;
    }
    } 
    
  return targetPrice;
  }
  
  public Double getThresholdPrice(){
    Double threshold_factor = 1;
    Double productThreshold = 1;
  if(product.Discountable__c== 'TRUE'){
    //List<CPQ_Region_Mapping__c> factors = [SELECT CPQ_Region__c,Th_Factor__c FROM CPQ_Region_Mapping__c where Country__c=:country];
    
      if(product.Threshold__c!= NULL)
        productThreshold =  product.Threshold__c;
      if(factors.Th_Factor__c!= NULL)
        threshold_factor = factors.Th_Factor__c;
    
    thresholdPrice = productThreshold *threshold_factor;
  }
  else{
    if(product.Threshold__c!= NULL)
      thresholdPrice = product.Threshold__c;
  }
  return thresholdPrice;
  }
  //added by Shital end

  public String getLongDescription() {
    longDescription = '';
    List<Product_Description_Language__c> longDescriptionArr = [
      SELECT Long_Description__c, Long_Desc_RichText__c, Language_Code__c
      FROM Product_Description_Language__c 
      WHERE Product__c = :product.Id
    ];

    Map<String, String> descsMap = new Map<String, String>();
    for(Product_Description_Language__c currDesc : longDescriptionArr) {
      if(!String.isEmpty(currDesc.Language_Code__c)){
        descsMap.put(currDesc.Language_Code__c.toLowerCase(), currDesc.Long_Desc_RichText__c);
      }
    }

    String user_erp_code = UserInfo.getLocale().substring(0,2);
    if(descsMap.containskey(user_erp_code)) {
      longDescription = descsMap.get(user_erp_code);
    }
    else if(descsMap.containskey('en')) {
      longDescription = descsMap.get('en');
    }
    else {
      if(longDescriptionArr.size() > 0) {
        longDescription = longDescriptionArr[0].Long_Desc_RichText__c;
      }
    }
    
    return longDescription;
  }
}
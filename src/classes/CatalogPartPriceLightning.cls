/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning 
Description: 
This is class for fetching price and decsription based upon prodyct selection in catalog 
Change Log:

*************************************************************************************/
public class CatalogPartPriceLightning 
{
    @AuraEnabled public Catalog_Part__c catalogPart {get;set;}
    @AuraEnabled public Product2 product {get;set;}
    @AuraEnabled public CPQ_Region_Mapping__c factors;
    @AuraEnabled public Double standardPrice
    { 
      get
      {
        
        Double std_factor = 1;
        Double base_price = 0;

        if(!catalogPart.Catalog_Part_Prices__r.isEmpty()) 
          std_factor = catalogPart.Catalog_Part_Prices__r[0].Standard_Factor__c;
        
        // get standard pricebook entry
        
        for(PricebookEntry pbe : product.PricebookEntries) 
        {
          if(pbe.Pricebook2Id == stdPricebookId) 
          {
            base_price = pbe.UnitPrice;
            break;
          }
        }
          standardPrice = std_factor * base_price;
          return std_factor * base_price;
      } 
      set;  
    }

    @AuraEnabled public Double goalPrice = 0.0; 
    @AuraEnabled public Double thresholdPrice
    {
      get
      {
          thresholdPrice= 1;
          Double threshold_factor = 1;
          Double productThreshold = 1;
          if(product.Discountable__c == 'TRUE')
          {
              if(product.Threshold__c!= NULL)
                  productThreshold =  product.Threshold__c;
              if(factors.Th_Factor__c!= NULL)
                  threshold_factor = factors.Th_Factor__c;
          
              thresholdPrice = productThreshold *threshold_factor;
          }

          else
          {
              if(product.Threshold__c!= NULL)
                  thresholdPrice = product.Threshold__c;
          }
        return thresholdPrice;
      }
     set;
    }
  
    @AuraEnabled public Double pricebookPrice
    {
      get
      {
        Double std_factor = 1;
        Double base_price = 0;

        if(!catalogPart.Catalog_Part_Prices__r.isEmpty()) 
          std_factor = catalogPart.Catalog_Part_Prices__r[0].Standard_Factor__c;

        // get other pricebook entry
        for(PricebookEntry pbe : product.PricebookEntries) 
        {
          
          if(pbe.Pricebook2Id != stdPricebookId) 
          {
            base_price = pbe.UnitPrice;
            break;
          }
        }
        return std_factor * base_price;
      }
          set;
    }
  
    @AuraEnabled public Double targetPrice
    {
      get
      {
        targetPrice= 1;
        Double product_DASP = 0.0;
        Double DASP_Factor = 1;
        Double target_factor = 1;
    
        if(product.Discountable__c == 'TRUE')
        {
          if(product.DASP__c!= NULL)
            product_DASP =  product.DASP__c;
          if(factors.DASP_Factor__c!= NULL)
            DASP_Factor = factors.DASP_Factor__c;
          if(factors.Regional_Target_Factor__c!= NULL)
            target_factor = factors.Regional_Target_Factor__c;  
          
          targetPrice = product_DASP *DASP_Factor*target_factor;
       }
        
        else
        {
          if(product.DASP__c!= NULL)
            targetPrice = product.DASP__c;
        } 
       
        return targetPrice;
      }

      set;
    }

    public String country {get;set;}
    private Id stdPricebookId;
    @AuraEnabled public String longDescription {get;set;}

    public Boolean isDiscountable() 
    {
        return product.Discountable__c != null && product.Discountable__c.equals('TRUE');
    }


    public CatalogPartPriceLightning(){}

  /***************************************************************************
    Description: 
    This is a parametrized constructor for populating global variables 
  *************************************************************************************/
  public CatalogPartPriceLightning(Catalog_Part__c catPart, Product2 newProduct, Id standardPricebookId, String selectedCountry, CPQ_Region_Mapping__c curRegionfactors, String selectedFamily, String selectedLine, String selectedCurrency) 
  {
    catalogPart = catPart;
    factors = curRegionfactors;
    if(newProduct != null) 
      product = newProduct;
    
    else 
      product = new Product2();
    
    stdPricebookId = standardPricebookId;
    
    //added by shital
    if(selectedCountry != null)
      country = selectedCountry; 
      
    else
      country = 'USA';
    }
  
    public String getCatalogPartId()
    {
      return catalogPart.Id;
    }

    /***************************************************************************
       Description: 
       This is method to fetch product decsription based upon product id
    *************************************************************************************/
    public String fetchLongDescription(Id prodid) 
    {
      String longDescription = '';
      List<Product_Description_Language__c> longDescriptionArr = [
                      SELECT Long_Description__c, Long_Desc_RichText__c, Language_Code__c
                      FROM Product_Description_Language__c 
                      WHERE Product__c = :prodid
                      ];
      Map<String, String> descsMap = new Map<String, String>();
      for(Product_Description_Language__c currDesc : longDescriptionArr) 
      {
        if(!String.isEmpty(currDesc.Language_Code__c))
        descsMap.put(currDesc.Language_Code__c.toLowerCase(), currDesc.Long_Desc_RichText__c);
      }
      
      String user_erp_code = UserInfo.getLocale().substring(0,2);
      if(descsMap.containskey(user_erp_code)) 
        longDescription = descsMap.get(user_erp_code);
        
      else if(descsMap.containskey('en')) 
        longDescription = descsMap.get('en');
        
      else if(longDescriptionArr.size() > 0) 
        longDescription = longDescriptionArr[0].Long_Desc_RichText__c;
        
      return longDescription;
    }
}
/*
  Controller for Catalog View tab
*/
public class CatalogViewController {
  private final String EXT_COL_PERMISSION_SET_NAME = 'Catalog_View_Extended_Column_View';

  /*private List<String> regionValues = new List<String> {
    'AUS', 'CHN', 'EURE', 'EURW', 
    'JPN', 'LAT', 'NA', 'PAC'
  };*/

  //Added by Shital Start
  public List<String> countryValues = new List<String>();
  public Set<String> countryValuesSet = new Set<String>();
  public List<Country__c> countries = new List<Country__c>();
  public List<SelectOption> countryOptions     {get;set;}
  public String selectedCountry    {get;set;}
  //Added by SHital End
  /*
    A set of admin profile names to check against
    - After a discussion with Ani, storing these admin profiles in code will be easier to maintain than a permission set
  */
  private final Set<String> adminProfileNames = new Set<String>{
    'System Administrator',
    'VMS Sales - Admin',
    'VMS BST - Member',
    'VMS System - Support Team Member',
    'VMS Marketing - User',
    'VMS Sales - User'
  };

  private List<String> familyValues = new List<String>{''};
  private Map<String, List<String>> lineValues = new Map<String, List<String>>();
  private Map<String, List<String>> modelValues = new Map<String, List<String>>();

  private List<Pricebook2> pricebooks;
  
  private List<Long_Term_Conversion_Rate__c> currencies;
  private Map<String, Double> conversionRateMap;

  private transient List<Product2> productParts;
  private List<PricebookEntry> pricebookEntries;

  private Set<String> currentGroups = new Set<String>();
  
  public String standardPricebook {get;set;}
  // private String standardPricebookName = 'Representative and Agencies EMEA';
  private String USD_CURRENCY = 'USD';
  private String profileId;
  private String userId,userId1;
  private String userCountry;

  public List<SelectOption> pricebookOptions  {get;set;}
  public List<SelectOption> currencyOptions   {get;set;}
  //public List<SelectOption> regionOptions     {get;set;}
  public List<SelectOption> familyOptions     {get;set;}
  public List<SelectOption> lineOptions       {get;set;}
  public List<SelectOption> modelOptions      {get;set;}
  public List<SelectOption> groupOptions      {get;set;}

  public String selectedRegion    {get;set;}
  public String selectedFamily    {get;set;}
  public String selectedLine      {get;set;}
  public String selectedModel     {get;set;}
  public String selectedPricebook {get;set;}
  public String catalogPartSearch {get;set;}
  public String selectedCurrency  {get;set;}
  public String selectedPriceBookName {get;set;}

  public String userLocale {get;set;}
  public String userCurrency {get;set;}
  
  public Double selectedConversionRate  {get;set;}
  
  public Boolean showThreshold        {get;set;}
  public Boolean isCatalogAdmin       {get;set;}
  public Boolean hasMultipleRegions   {get;set;}
  public Boolean displayPricebookStd  {get;set;}
  public Boolean showDiscountable     {get;set;}
  public Boolean searchByPrice        {get;set;}
  
  public List<String> selectedGroup {get;set;}

  public transient List<Catalog_Part__c> parts        {get;set;}
  public Set<String> Gparts        {get;set;}
  public Map<String, List<CatalogPartPrice>> groupedPartsMap {get;set;}
  public List<CatalogPartPrice> groupedParts {get;set;}
  public List<CatalogPartPrice> searchedParts {get;set;}

  public Id selectedCatalogPartId {get;set;}
  public CatalogPartPrice selectedCatalogPart {get;set;}
  public CPQ_Region_Mapping__c curRegionfactors = new CPQ_Region_Mapping__c(); 

  public CatalogViewController() {
    
    //Added by Shital start
    countries = [select Id,Name from Country__c ORDER BY Name ASC NULLS LAST];
    for(Country__c country: countries){
      countryValues.add(country.Name); 
    }
    countryValuesSet.addAll(countryValues); 
    //Added by Shital end
    selectedRegion   = 'NA'; //Default based on user
    selectedCountry = 'USA';
    // selectedRegion = 'EURW';
    selectedFamily   = 'System Solutions';
    selectedCurrency = UserInfo.getDefaultCurrency();
    displayPricebookStd = false;
    
    showDiscountable = false;
    searchByPrice    = false;

    this.setPermissionFlags();
    
    userLocale   = UserInfo.getLocale();
    userCurrency = UserInfo.getDefaultCurrency();
  
    // Initialize the selected region
    
    //Added by Shital start
    userId = UserInfo.getUserId();
    if(!Test.isRunningTest())
    {
      List<User> usersInfo = [SELECT Country FROM User WHERE Id = :userId];
      for(User currentUser : usersInfo) {
        if(currentUser != null && currentUser.Country != null) {
      userCountry = currentUser.Country;
      if(countryValuesSet.contains(userCountry)){
        selectedCountry = currentUser.Country;
      }else{
        selectedCountry = 'USA';  
      }
        }
      }
      // Default the region based on the country
      if(!String.isBlank(selectedCountry)) {
        // selectedRegion = countryRegions.get(userCountry);
        List<CPQ_Region_Mapping__c> cpqRegions = [SELECT CPQ_Region__c FROM CPQ_Region_Mapping__c WHERE Country__c=:selectedCountry.toLowerCase()];
        for(CPQ_Region_Mapping__c curRegion : cpqRegions) {
          selectedRegion = curRegion.CPQ_Region__c;
        }
      }
      
      if(!this.isCatalogAdmin) {
        // Set the region values if the user has additional regions
        //List<Catalog_View_Region_Access__c> accessRegions = [SELECT CPQ_Regions__c FROM Catalog_View_Region_Access__c WHERE User__c=:userId LIMIT 1];
        /*for(Catalog_View_Region_Access__c curAccess : accessRegions) {
          if(!String.isBlank(curAccess.CPQ_Regions__c)) {
            regionValues = curAccess.CPQ_Regions__c.split(';');
            this.hasMultipleRegions = true;
          }
        } */

        // If the user has the right permission set, allow them to see threshold
        List<PermissionSetAssignment> userPermissionSets = [SELECT Id, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId=:userId AND PermissionSet.Name=:EXT_COL_PERMISSION_SET_NAME];
        if(userPermissionSets.size() > 0) {
          this.showThreshold = true;
        } 
      }
    }
    else {
      userCountry = 'USA';
    }
    
    //added by Shital End
    
    this.initializeMenuValues();

    pricebooks = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsActive = true];
    pricebookOptions = this.getPricebookOptions(pricebooks);
    this.setDefaultPricebook();

    currencies = [SELECT Conversion_Rate__c, CurrencyIsoCode FROM Long_Term_Conversion_Rate__c ORDER BY CurrencyIsoCode];
    currencyOptions = this.getCurrencyOptions(currencies);
    conversionRateMap = new Map<String, Double>();
    for(Long_Term_Conversion_Rate__c currCurrency : currencies) {
      conversionRateMap.put(currCurrency.CurrencyIsoCode, currCurrency.Conversion_Rate__c);
    }

    // Default to users profile
    countryOptions = this.getStringOptions(countryValues);
    //regionOptions = this.getStringOptions(regionValues);
    familyOptions = this.getStringOptions(familyValues);
    if(lineValues.containsKey(selectedFamily)) {
      lineOptions   = this.getStringOptions(lineValues.get(selectedFamily));
    }
    if(selectedLine != null) {
      modelOptions  = this.getStringOptions(modelValues.get(selectedLine));
    }

    groupedParts  = new List<CatalogPartPrice>();
    searchedParts = new List<CatalogPartPrice>();
    selectedGroup = new List<String>();
    groupOptions  = new List<SelectOption>();

    this.setConversionRate();
  }

  public void setPermissionFlags() {
    this.showThreshold  = false;
    this.isCatalogAdmin = false;

    profileId = UserInfo.getProfileId();
    List<Profile> profileNames = [SELECT Id, Name FROM Profile WHERE Id = :profileId];
    for(Profile currProfile : profileNames) {
      if(adminProfileNames.contains(currProfile.Name)) {
        this.isCatalogAdmin = true;
        this.showThreshold  = true;
      }
    }
    
    // Added by Ajinkya for INC4720739
    List<GroupMember> groupMembers = [
        SELECT Id, UserOrGroupId 
        FROM GroupMember 
        WHERE UserOrGroupId = :UserInfo.getUserId() 
                AND Group.DeveloperName = 'Catalog_View_Admin'
    ];
    
    if(groupMembers!=null && !groupMembers.isEmpty()){
        this.isCatalogAdmin = true;
    }
    // End
    
  }

  public PageReference exportToExcel() {
    return new Pagereference('/apex/Catalog_View_Export');
  }
  
  public pageReference exportAllToExcel() {
    return new pagereference('/apex/Catalog_View_ExportALL');
  }
  
  public PageReference searchExportToExcel() {
    return new Pagereference('/apex/Catalog_View_Search_Export');
  }

  public String xlsHeader {
    get {
      String strHeader = '';
      strHeader += '<?xml version="1.0"?>';
      strHeader += '<?mso-application progid="Excel.Sheet"?>';
      return strHeader;
    }
  }

  public PageReference quickSearch() {
    displayPricebookStd = !selectedPricebook.equals(standardPricebook);
    selectedPriceBookName = this.getPricebookName();

    if(String.isBlank(catalogPartSearch) && this.hasSearchFilters()) {
      searchedParts = new List<CatalogPartPrice>();
      searchByPrice = false;
      this.getModelParts();
      // this.updateGroups();
      this.setDefaultGroup();
      // this.updateGroupedParts();  
    }
    else if(!String.isBlank(catalogPartSearch)) {
      groupedParts = new List<CatalogPartPrice>();
      groupedPartsMap = new Map<String, List<CatalogPartPrice>>();
      groupOptions = new List<SelectOption>();
      this.clearFilters();
      this.updateSearchedParts();
    }
    else {
      groupOptions    = new List<SelectOption>();
      groupedParts    = new List<CatalogPartPrice>();
      searchedParts   = new List<CatalogPartPrice>();
      groupedPartsMap = new Map<String, List<CatalogPartPrice>>();  
    }
    
    this.setConversionRate();
    return null;
  }

  public Pagereference filterDiscountable() {
    this.quickSearch();
    return null;
  }

  public Boolean hasSearchFilters() {
    return !String.isBlank(selectedFamily) && !String.isBlank(selectedLine) && !String.isBlank(selectedModel);
  }

  public PageReference updateSelectedGroups() {
    return null;
  }

  public PageReference updateFilter() {
    if(selectedFamily != null && lineValues.containskey(selectedFamily)) {
      lineOptions = this.getStringOptions(lineValues.get(selectedFamily));  
    }
    else {
      lineOptions = this.getStringOptions(new List<String>{''});
    }

    if(selectedLine != null&& modelValues.containskey(selectedFamily+' '+selectedLine)) {
      String modelSelection = selectedFamily + ' ' + selectedLine;
      modelOptions = this.getStringOptions(modelValues.get(modelSelection));  
    }
    else {
      modelOptions = this.getStringOptions(new List<String>{''});
    }
    return null;
  }
  
  //Added by Shital Start
  public string getSelectedRegion(String selectedCountry){
    
       curRegionfactors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c WHERE Country__c=:selectedCountry.toLowerCase() limit 1];
          
            /*for(CPQ_Region_Mapping__c curRegion : [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c WHERE Country__c=:selectedCountry.toLowerCase() limit 1]) {
              selectedRegion = curRegion.CPQ_Region__c;
            }*/
            selectedRegion = curRegionfactors.CPQ_Region__c;
            
       
      system.debug('**selectedRegion***'+selectedRegion+'***selectedCountry******'+selectedCountry);
      return selectedRegion;
  }

  public void updateParts() {
    
    //Added by Shital Start
    selectedRegion = getSelectedRegion(selectedCountry);
    //Added by Shital End 
    
    parts = [
      SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c, Sort_Order__c, Group_Order__c, Credits__c,
             (SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c 
              FROM Catalog_Part_Prices__r
              WHERE Region__c = :selectedRegion)
      FROM Catalog_Part__c
      WHERE isActive__c = true
            AND Family__c = :selectedFamily
            AND Line__c = :selectedLine
            AND Model__c = :selectedModel
      ORDER BY Group_Order__c, Sort_Order__c ASC NULLS LAST
      LIMIT 1000
    ];
    // Product__r.Description, Product__r.BigMachines__Part_Number__c, Product__r.Id, Product__r.Name,
  }
  
  // Added by Ajinkya
  public Boolean getIsNARegion(){
      if(selectedCountry == 'USA' && selectedFamily == 'Professional Services' && selectedLine == 'Professional Services' && selectedModel == 'Advantage Credits'){
          return true;
      }else{
          return false;
      }
  }
  
  public void getModelParts() {
    
    selectedRegion = getSelectedRegion(selectedCountry);
    Set<Id> productIds = new Set<Id>();

    groupedPartsMap = new Map<String, List<CatalogPartPrice>>();
    groupOptions = new List<SelectOption>();
    currentGroups = new Set<String>();

    parts = [
      SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c, Sort_Order__c, Group_Order__c, Credits__c,
             (SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c 
              FROM Catalog_Part_Prices__r
              WHERE Region__c = :selectedRegion)
      FROM Catalog_Part__c
      WHERE isActive__c = true
            AND Family__c = :selectedFamily
            AND Line__c = :selectedLine
            AND Model__c = :selectedModel
      ORDER BY Group_Order__c, Sort_Order__c ASC NULLS LAST
      LIMIT 1000
    ];
    
    for(Catalog_Part__c part : parts) {
      if(!productIds.contains(part.Product__c)) {
        productIds.add(part.Product__c);
      }
    }

    List<Id> pbIds = new List<Id> {selectedPricebook, standardPricebook};

    productParts = [
      SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c, 
        (SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :USD_CURRENCY)
        // (SELECT Long_Description__c, Long_Desc_RichText__c FROM Product_Description_Languages__r)
      FROM Product2
      WHERE Id IN :productIds
      LIMIT 1000
    ];

    Map<Id, Product2> testPartMap = new Map<Id, Product2>(productParts);
    List<Id> partIds = new List<Id>(testPartMap.keySet());

    this.createGroupedPartsMap();
  }

  public void createGroupedPartsMap() {
    List<SelectOption> options = new List<SelectOption>();
    Gparts = new Set<String>();
    Map<Id, Product2> m = new Map<Id, Product2>(productParts);

    for(Catalog_Part__c part : parts) {
      Product2 product = m.get(part.Product__c);
      CatalogPartPrice currPart = new CatalogPartPrice(part, product, standardPricebook, selectedCountry, curRegionfactors, selectedFamily, selectedLine, selectedCurrency);

      List<CatalogPartPrice> tmpParts = new List<CatalogPartPrice>();
      if(groupedPartsMap.containskey(part.Group__c)) {
        tmpParts = groupedPartsMap.get(part.Group__c);
      }
      else {
        options.add(new SelectOption(part.Group__c, part.Group__c));
        currentGroups.add(part.Group__c);
      }
      if((showDiscountable && currPart.product != null &&
          (currPart.product.Discountable__c == null || currPart.product.Discountable__c.equals('TRUE')))
         || !showDiscountable) {
        tmpParts.add(currPart);  
      }
      groupedPartsMap.put(part.Group__c, tmpParts);
      Gparts.add(part.Group__c);
    }
    groupOptions = options;
  }

  public List<CatalogPartPrice> getCurrentGroupParts(String currentGroup) {
    return groupedPartsMap.get(currentGroup);
  }

  public void clearFilters() {
    selectedFamily = '';
    selectedLine   = '';
    selectedModel  = '';
  }

  public void setConversionRate() {
    selectedConversionRate = conversionRateMap.get(selectedCurrency);
  }

  public void updateSearchedParts() {
    Set<Id> productIds = new Set<Id>();
    String searchString = catalogPartSearch.trim();

    searchedParts = new List<CatalogPartPrice>();

    List<Id> pbIds = new List<Id> {selectedPricebook, standardPricebook};

    if(searchByPrice) {
      Double searchPrice = Double.valueOf(searchString);
      List<PricebookEntry> matchingPrices = [
        SELECT Id, Product2Id 
        FROM PricebookEntry WHERE UnitPrice = :searchPrice AND CurrencyIsoCode = :USD_CURRENCY 
        LIMIT 1000
      ];
      Set<Id> matchingProductIds = new Set<Id>();
      for(PricebookEntry curPBE : matchingPrices) {
        matchingProductIds.add(curPBE.Product2Id);
      }

      productParts = [
        SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c, 
          (SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :USD_CURRENCY)
          // (SELECT Long_Description__c, Long_Desc_RichText__c FROM Product_Description_Languages__r)
        FROM Product2
        WHERE Id IN :matchingProductIds
        LIMIT 1000
      ];
    }
    else {
      searchString = '%'+searchString+'%';
      productParts = [
        SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c, 
          (SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :USD_CURRENCY)
          // (SELECT Long_Description__c, Long_Desc_RichText__c FROM Product_Description_Languages__r)
        FROM Product2
        WHERE Name LIKE :searchString
             OR BigMachines__Part_Number__c LIKE :searchString
        LIMIT 1000
      ];  
    }
    

    Map<Id, Product2> m = new Map<Id, Product2>(productParts);
    productIds = m.keySet();
  
    selectedRegion = getSelectedRegion(selectedCountry);
    
    parts = [
      SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c, Credits__c,
             (SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c  
              FROM Catalog_Part_Prices__r 
              WHERE Region__c = :selectedRegion) 
      FROM Catalog_Part__c 
      WHERE Product__c IN :productIds AND isActive__c = true
      ORDER BY Model__c, Group__c
      LIMIT 1000
    ];

    for(Catalog_Part__c part : parts) {
      Product2 product = m.get(part.Product__c);
      CatalogPartPrice newPart = new CatalogPartPrice(part, product, standardPricebook, selectedCountry, curRegionfactors, selectedFamily, selectedFamily, selectedCurrency);
      if((showDiscountable && newPart.product != null &&
          (newPart.product.Discountable__c == null || newPart.product.Discountable__c.equals('TRUE')))
         || !showDiscountable) {
        searchedParts.add(newPart);
      }
    }
  }

  public void updateGroups() {
    List<SelectOption> options = new List<SelectOption>();
    Set<String> alreadyExistSet = new Set<String>();

    for (Catalog_Part__c part : parts) {
      if(!alreadyExistSet.contains(part.Group__c)) {
        options.add(new SelectOption(part.Group__c, part.Group__c));
        alreadyExistSet.add(part.Group__c);    
      }
    }    
    // groupOptions = groupedPartsMap.keyset();
  }

  public void updateGroupedParts() {
    groupedParts = this.getGroupedParts(parts);
  }

  public void setDefaultGroup() {
    if((selectedGroup.isEmpty() || this.groupsNotFound()) && !groupOptions.isEmpty()) {
      selectedGroup = new List<String>();
      selectedGroup.add(groupOptions.get(0).getValue());
    }
  }

  public Boolean groupsNotFound() {
    for(String currentGroup : selectedGroup) {
      if(!currentGroups.contains(currentGroup)) {
        return true;
      }
    }
    return false;
  }

  public void setDefaultPricebook() {
    for(Pricebook2 pb : pricebooks) {
      // If standard pricebook is Rep EMEA add back in
      // if(pb.IsStandard || pb.Name == standardPricebookName) {
      if(pb.IsStandard) {
        selectedPricebook = pb.Id;
        standardPricebook = pb.Id;
      }
    }
  }

  public String getPricebookName() {
    Map<Id, Pricebook2> pbs = new Map<Id, Pricebook2>(pricebooks);
    return pbs.get(selectedPricebook).Name;
  }

  public List<SelectOption> getStringOptions(List<String> values) {
    List<SelectOption> options = new List<SelectOption>();
    for (String value : values) {
      options.add(new SelectOption(value, value));
    }    
    return options;
  }

  public List<SelectOption> getPricebookOptions(List<Pricebook2> values) {
    List<SelectOption> options = new List<SelectOption>();
    for (Pricebook2 pricebook : pricebooks) {
      options.add(new SelectOption(pricebook.id, pricebook.name));
    }    
    return options;
  }

  public List<SelectOption> getCurrencyOptions(List<Long_Term_Conversion_Rate__c> values) {
    List<SelectOption> options = new List<SelectOption>();
    for (Long_Term_Conversion_Rate__c currCurrency : values) {
      options.add(new SelectOption(currCurrency.CurrencyIsoCode, currCurrency.CurrencyIsoCode));
    }    
    return options;
  }

  // Optimize method
  public List<CatalogPartPrice> getGroupedParts(List<Catalog_Part__c> parts) {
    List<Catalog_Part__c> newParts = new List<Catalog_Part__c>();
    List<CatalogPartPrice> catPartPrices = new List<CatalogPartPrice>();
    Set<Id> productIds = new Set<Id>();
    Set<String> selectedGroupSet = new Set<String>(selectedGroup);
    
    for (Catalog_Part__c part : parts) {
      // Change to map?
      if(selectedGroupSet.contains(part.Group__c)) {
        newParts.add(part);
      }
      // Add Ids to get from Pricebook Entries
      if(!productIds.contains(part.Product__c)) {
        productIds.add(part.Product__c);
      }
    }

    List<Id> pbIds = new List<Id> {selectedPricebook, standardPricebook};
    productParts = [
      SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c, 
        (SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :selectedCurrency)
        // (SELECT Long_Description__c, Long_Desc_RichText__c FROM Product_Description_Languages__r)
      FROM Product2
      WHERE Id IN :productIds
      LIMIT 1000
    ];


    Map<Id, Product2> m = new Map<Id, Product2>(productParts);

    for(Catalog_Part__c part : newParts) {
      Product2 product = m.get(part.Product__c);
      catPartPrices.add(new CatalogPartPrice(part, product, standardPricebook, selectedCountry, curRegionfactors, selectedFamily, selectedLine, selectedCurrency));
    }

    return catPartPrices;
  }

  public PageReference selectRow() {
    Map<Id, CatalogPartPrice> m = new Map<Id, CatalogPartPrice>();
    if(String.isBlank(catalogPartSearch)) {
      // A little slow, think about refactor
      for(String curGroup : selectedGroup) {
        for(CatalogPartPrice part : groupedPartsMap.get(curGroup)) {
          m.put(part.catalogPart.Id, part);
        }  
      }
    }
    else {
      for(CatalogPartPrice part : searchedParts) {
        m.put(part.catalogPart.Id, part);
      }
    }
    
    this.selectedCatalogPart = m.get(this.selectedCatalogPartId);
    return null;
  }

  public void initializeMenuValues() {
    Catalog_Model__c[] catalogModelRecords = [
      SELECT Family__c, Line__c, Model__c, 
             Family_Order__c, Line_Order__c, Model_Order__c
      FROM Catalog_Model__c
      WHERE isActive__c = true
      ORDER BY Family_Order__c, Line_Order__c, Model_Order__c
    ];

    for(Catalog_Model__c modelRow : catalogModelRecords) {
      String modelKey = modelRow.Family__c + ' ' + modelRow.Line__c;

      // Add the Family if its not already there
      if(!lineValues.containsKey(modelRow.Family__c)) {
        familyValues.add(modelRow.Family__c);
        lineValues.put(modelRow.Family__c, new List<String>{''});
      }
      // Add the line if its not already there
      if(!modelValues.containsKey(modelKey)) {
        modelValues.put(modelKey, new List<String>{''});

        List<String> currentLines = lineValues.get(modelRow.Family__c);
        currentLines.add(modelRow.Line__c);
        lineValues.put(modelRow.Family__c, currentLines);
      }

      List<String> currentModels = modelValues.get(modelKey);
      currentModels.add(modelRow.Model__c);
      modelValues.put(modelKey, currentModels);
    }

  }
    /*
     * for lightning
     */
    public boolean getIsLightning(){
        /*
        User userObj = [SELECT Id, UserPreferencesLightningExperiencePreferred from User where Id =: UserInfo.getUserId() limit 001];
        return userObj.UserPreferencesLightningExperiencePreferred;
		*/
        if(ApexPages.currentPage().getParameters().get('sfdcIFrameHost') != null ||
           ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ||
           ApexPages.currentPage().getParameters().get('isdtp') == 'p1') {      
            return true;
        }
        else {      
            return false;
        }           
    }
}
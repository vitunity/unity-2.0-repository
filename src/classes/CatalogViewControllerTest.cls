@isTest(seeAllData=true)
private class CatalogViewControllerTest {
    
    private static String catalogPartId;
    
    static testMethod void testCatalogViewController(){
        insertProductsAndCatalogParts();
        CatalogViewController testCatalogView = new CatalogViewController();
        
        // Search by keyword
        testCatalogView.catalogPartSearch = 'Test Product0';
        testCatalogView.selectedPricebook = Test.getStandardPricebookId();
        testCatalogView.quickSearch();
        System.assertEquals(1,testCatalogView.searchedParts.size());
        
        //reset filters
        testCatalogView.catalogPartSearch = '';
        testCatalogView.quickSearch();
        System.assertEquals(0,testCatalogView.searchedParts.size());
        
        // Search by family
        testCatalogView.selectedFamily = 'Software Solutions';
        testCatalogView.selectedLine = 'OIS (ARIA)';
        testCatalogView.selectedModel = 'Eclipse';
        testCatalogView.quickSearch();
        System.assertEquals(1,testCatalogView.groupedPartsMap.size());
        
        testCatalogView.selectedFamily = 'System Solutions';
        testCatalogView.selectedLine = 'BrachyTherapy Hardware';
        testCatalogView.selectedModel = 'VariSource';
        testCatalogView.updateFilter();
        testCatalogView.selectedPricebook = Test.getStandardPricebookId();
        testCatalogView.quickSearch();
        testCatalogView.updateGroups();
        
        System.assert(testCatalogView.groupedPartsMap.size()>0);
        
        testCatalogView.exportToExcel();
        testCatalogView.searchExportToExcel();
        String header = testCatalogView.xlsHeader;
        
        //reset filters
        testCatalogView.catalogPartSearch = '';
        testCatalogView.selectedPricebook = Test.getStandardPricebookId();
        testCatalogView.quickSearch();
        System.assertEquals(0,testCatalogView.searchedParts.size());
        
        //Search by price
        testCatalogView.catalogPartSearch = '100';
        testCatalogView.searchByPrice = true;
        testCatalogView.quickSearch();
        System.assertEquals(5,testCatalogView.searchedParts.size());
        
        //test part popup
        testCatalogView.selectedCatalogPartId = catalogPartId;
        testCatalogView.selectRow();
        System.assertEquals('Test Product4', testCatalogView.selectedCatalogPart.product.Name);
        
        //Added by Shital - coverage for catalogPartPrice class
        Catalog_Part__c tempPart = new Catalog_Part__c();
        tempPart = [SELECT Id,name FROM Catalog_Part__c WHERE Id =: catalogPartId];
        
        CPQ_Region_Mapping__c factors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c where Country__c= 'USA'];
        
        CatalogPartPrice tempCpartPrice = new CatalogPartPrice(tempPart,testCatalogView.selectedCatalogPart.product,Test.getStandardPricebookId(),'USA', factors, 'System Solutions', 'Advanced Treatment Delivery', 'USD' );

        tempCpartPrice.getPricebookPrice();
        tempCpartPrice.getTargetPrice();
        tempCpartPrice.getThresholdPrice();
        tempCpartPrice.getLongDescription();
        tempCpartPrice.getStandardPrice();
        tempCpartPrice.isDiscountable(); 
    }
    
    private static void insertProductsAndCatalogParts(){
        
        List<Product2> products = new List<Product2>();
        for(Integer counter=0; counter<5; counter++){
            Product2 product = new Product2();
            product.Name = 'Test Product'+counter;
            if(counter<2){
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Family = 'Software Solution';
                Product.SVMXC__Product_Line__c = 'Desktop';
                Product.Product_Group__c = '4D Integrated Treatment Console (4DITC)';
                product.Discountable__c = 'TRUE';
            }else if(counter<4){
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Family = 'System Solution';
                Product.SVMXC__Product_Line__c = 'Solution';
                Product.Product_Group__c = '4D Integrated Treatment Console (4DITC)';
                product.Discountable__c = 'TRUE';
            }else{
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Discountable__c = 'TRUE';
            }
            products.add(product);
        }
        
        insert products;
        
        insertCatalogParts(products);
        insertPriceBookEntries(products);
    }
    
    private static void insertPriceBookEntries(List<Product2> products){
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Product2 product : products){
            PricebookEntry pbEntry  = new PricebookEntry();
            pbEntry.Product2Id = product.Id;
            pbEntry.Pricebook2Id = Test.getStandardPricebookId();
            pbEntry.CurrencyISOCode = 'USD';
            pbEntry.UnitPrice = 100;
            pbEntry.IsActive = true;
            pricebookEntries.add(pbEntry);
        }
        insert pricebookEntries;
    }
    
    private static void insertCatalogParts(List<Product2> products){
        List<Catalog_Part__c> parts = new List<Catalog_Part__c>();
        
        for(Integer counter=0; counter<5; counter++){
            Catalog_Part__c part = new Catalog_Part__c();
            part.Name = products[counter].Name;
            part.Product__c = products[counter].Id;
            if(counter<2){
                part.Family__c = 'Software Solutions';
                part.Line__c = 'OIS (ARIA)';
                part.Model__c = 'Eclipse';
                part.Group__c = 'ABO Licenses';
            }else if(counter<4){
                part.Family__c = 'System Solutions';
                part.Line__c = 'BrachyTherapy Hardware';
                part.Model__c = 'VariSource';
                part.Group__c = 'Treatment Accessories';
            }else{
                part.Family__c = 'System Solutions';
                part.Line__c = 'BrachyTherapy Hardware';
                part.Model__c = 'VariSource';
                part.Group__c = 'Interstitial / Breast Catheter Kits';
            }
            parts.add(part);
        }
        insert parts;
        
        catalogPartId = parts[4].Id;
        
        insertPartPrices(parts);
    }
    
    private static void insertPartPrices(List<Catalog_Part__c> parts){
        
        List<Catalog_Part_Price__c> partPricing = new List<Catalog_Part_Price__c>();
        
        List<String> regions = new List<String>{'NA', 'PAC', 'LAT', 'JPN',
                                                'EURW', 'EURE', 'CHN', 'AUS'};
        
        for(Catalog_Part__c part : parts){
            for(String region : regions){
                Catalog_Part_Price__c partPrice = new Catalog_Part_Price__c();
                partPrice.Catalog_Part__c = partPrice.Id;
                partPrice.Goal_Factor__c = 1.000000000;
                partPrice.Standard_Factor__c = 1.000000000;
                partPrice.Threshold_Factor__c = 1.000000000;
                partPrice.Region__c = region;
                partPricing.add(partPrice);
            }
        }
        
        insert partPricing;
    }
}
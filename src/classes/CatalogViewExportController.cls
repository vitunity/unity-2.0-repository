/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning 
Description: 
This is controller class for export to excel. This class is used to populate selected product line,family and other details for exporting data into excel.
Change Log:

*************************************************************************************/
public class CatalogViewExportController 
{

    public String selectedFamily    {get;set;}
    public String selectedLine      {get;set;}
    public String selectedModel     {get;set;}
    public String selectedCurrency  {get;set;}
    public Double selectedConversionRate  {get;set;}
    public boolean showDiscountable    {get;set;}
    public String selectedRegion    {get;set;}
    public String selectedCountry    {get;set;}
    public String selectedPricebook {get;set;}
	public String standardPricebook {get;set;}

	public Set<String> Gparts        {get;set;}
	public String xlsHeader{get;set;} 
    public List<String> selectedGroup {get;set;}
    public Map<String, List<CatalogPartPrice>> groupedPartsMap {get;set;}
 
    

   /***************************************************************************
    Description: 
    This is a  constructor for populating global variables like selected product, family, line and currency details
    *************************************************************************************/
    public CatalogViewExportController() 
    {
	    //Getting object from URL parameters
	    String catalogJSON = ApexPages.currentPage().getParameters().get('catalogJSON');
	    JSONParser parser = JSON.createParser(catalogJSON);
	    selectedGroup = new List<String>();
	    Gparts = new Set<String>();
	    groupedPartsMap =  new Map<String,List<CatalogPartPrice>>();
	    String strHeader = '';
        strHeader += '<?xml version="1.0"?>';
        strHeader += '<?mso-application progid="Excel.Sheet"?>';
	    xlsHeader  = strHeader;
	    while (parser.nextToken() != null)
	    {
	        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) 
	        {
	             
	            if( parser.getText() == 'selectedCurrency')
	            {
	                parser.nextToken();
	                this.selectedCurrency = parser.getText();
	            }

	            else if ( parser.getText() == 'selectedLine')
	            {
	                parser.nextToken();
	                this.selectedLine = parser.getText();
	            }

	            else if ( parser.getText() == 'selectedModel') 
	            {
	                parser.nextToken();
	                this.selectedModel = parser.getText();
	            }

	            else if ( parser.getText() == 'selectedFamily')
	            {
	                parser.nextToken();
	                this.selectedFamily = parser.getText();
	            }


                else if ( parser.getText() == 'selectedConversionRate')
	            {
	                parser.nextToken();
	                this.selectedConversionRate = parser.getDoubleValue();
	            }

	            else if ( parser.getText() == 'showDiscountable')
	            {
	                parser.nextToken();
	                this.showDiscountable = parser.getBooleanValue();
	            }

	            else if ( parser.getText() == 'selectedRegion')
	            {
	                parser.nextToken();
	                this.selectedRegion = parser.getText();
	            }


	             else if ( parser.getText() == 'selectedCountry')
	            {
	                parser.nextToken();
	                this.selectedCountry = parser.getText();
	            }

	             else if ( parser.getText() == 'selectedPricebook')
	            {
	                parser.nextToken();
	                this.selectedPricebook = parser.getText();
	            }

	             else if ( parser.getText() == 'standardPricebook')
	            {
	                parser.nextToken();
	                this.standardPricebook = parser.getText();
	            }

	        }

	        else if(parser.getCurrentToken() ==  JSONToken.START_ARRAY)
	        {
	            while (parser.nextToken() != null) 
	            {
	                if (parser.getCurrentToken() == JSONToken.START_OBJECT) 
	                {
	                    parser.nexttoken();
	                    if(parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'selectedGroup')
	                    {
	                    	parser.nextToken();
	                        this.selectedGroup.add(parser.getText());
	                    }
	                    
	                }
	                break;
	            }

	        }
 
	    }
	    List<Catalog_Part__c> parts = [
									    SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c, Sort_Order__c, Group_Order__c,
									    (SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c FROM Catalog_Part_Prices__r WHERE Region__c = :selectedRegion)
									    FROM Catalog_Part__c WHERE isActive__c = true AND Family__c = :selectedFamily AND Line__c = :selectedLine AND Model__c = :selectedModel
									    ORDER BY Group_Order__c, Sort_Order__c ASC NULLS LAST LIMIT 1000
									   ];

		Set<Id> productIds = new Set<Id>();	
		
		for(Catalog_Part__c part : parts) 
		{
		    if(!productIds.contains(part.Product__c)) 
		    {
		        productIds.add(part.Product__c);
		    }
        }

        List<Id> pbIds = new List<Id> {selectedPricebook, standardPricebook};
						   

		Map<Id, Product2> m = new Map<Id, Product2>( [SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c,
												        (SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = 'USD')
												        FROM Product2 WHERE Id IN :productIds LIMIT 1000]);

		CPQ_Region_Mapping__c curRegionfactors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c WHERE Country__c=: selectedCountry.toLowerCase() limit 1];
							   

		for(Catalog_Part__c part : parts) 
		{
            
            Product2 product = m.get(part.Product__c);
            CatalogPartPrice currPart = new CatalogPartPrice(part, product, standardPricebook, selectedCountry, curRegionfactors, selectedFamily, selectedLine, selectedCurrency);
		    List<CatalogPartPrice> tmpParts = new List<CatalogPartPrice>();
		    if(groupedPartsMap.containskey(part.Group__c)) 
		        tmpParts = groupedPartsMap.get(part.Group__c);
		      
		  
		    if((showDiscountable && currPart.product != null && (currPart.product.Discountable__c == null || currPart.product.Discountable__c.equals('TRUE'))) || !showDiscountable) 
		        tmpParts.add(currPart);  
		      
		    groupedPartsMap.put(part.Group__c, tmpParts);
		    Gparts.add(part.Group__c);
		}							   

    }

}
/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning : STSK0012590
Description: 
This is a controller for catalogViewComp. This controller is used to show the catalog of different product based on model and family line selection. 
Change Log:

*************************************************************************************/

public class CatalogViewLightningController 
{
    
	// Global variables used for serializing data to the lightning component (catalogViewComp)
	@AuraEnabled public final String EXT_COL_PERMISSION_SET_NAME = 'Catalog_View_Extended_Column_View';
	@AuraEnabled public List<Country__c> countries = new List<Country__c>();
    @AuraEnabled public String selectedRegion    {get;set;}
	@AuraEnabled public String selectedCountry    {get;set;}
	@AuraEnabled public String selectedFamily    {get;set;}
	@AuraEnabled public String selectedCurrency  {get;set;}
	@AuraEnabled public Boolean displayPricebookStd  {get;set;}
	@AuraEnabled public Boolean showDiscountable     {get;set;} 
    @AuraEnabled public Boolean searchByPrice        {get;set;}	
	@AuraEnabled public Boolean showThreshold        {get;set;}
	@AuraEnabled public Boolean isCatalogAdmin       {get;set;}
	@AuraEnabled public String userLocale {get;set;}
    @AuraEnabled public String userCurrency {get;set;}
    @AuraEnabled public Map<String, List<String>> modelValues{get;set;} 
	@AuraEnabled public List<Pricebook2> PricebookOptions  {get;set;}
	@AuraEnabled public List<String> priceBookValue  {get;set;}
	@AuraEnabled public String selectedPricebook {get;set;}
	@AuraEnabled public String standardPricebook {get;set;}
	@AuraEnabled public  List<String> currencies{get;set;}
    @AuraEnabled public Map<String, Double> conversionRateMap{get;set;}  
    @AuraEnabled public List<String> familyValues { get;set;}
    @AuraEnabled public Map<String, List<String>> lineValues{get;set;}  
	@AuraEnabled public List<String> lineOptions{get;set;}
	@AuraEnabled public List<String> modelOptions{get;set;}
	@AuraEnabled public Double selectedConversionRate  {get;set;}
	@AuraEnabled public List<String> countryValuesList = new List<String>();
	@AuraEnabled public String selectedLine {get;set;}
	@AuraEnabled public String selectedModel {get;set;}
	@AuraEnabled public String catalogPartSearch {get;set;}
	@AuraEnabled public String selectedPriceBookName {get;set;}
	@AuraEnabled public List<CatalogPriceWrapper> searchedParts {get;set;}
	@AuraEnabled public List<CatalogPartPriceLightning> groupedParts {get;set;}
	@AuraEnabled public Map<String, List<CatalogPriceWrapper>> groupedPartsMap {get;set;}
	@AuraEnabled public List<GroupWrapper> groupOptions {get;set;}
	@AuraEnabled public List<SelectGroupWrapper> selectedGroup {get;set;}
	@AuraEnabled public CatalogPriceWrapper selectedCatalogPart {get;set;}
	@AuraEnabled public Id selectedCatalogPartId {get;set;}
	@AuraEnabled public CPQ_Region_Mapping__c curRegionfactors;
	
    //these variables are not needed on page side, hence not marking aura enabled
    private String userId,userId1;
	private String userCountry;
	
   /***************************************************************************
    Description: 
    This is init method for the component. It will fire for initial component load. In this method we are setting Product family, Prodcut line, default currency, country and pricebook
    *************************************************************************************/
   @AuraEnabled
   public static CatalogViewLightningController initClass()
   {
		//create class instance
        CatalogViewLightningController catLog = new CatalogViewLightningController();
        
		//initializing global parameters
		catLog.PricebookOptions = new List<Pricebook2>();
		catLog.priceBookValue = new List<String>();
		catLog.lineValues = new Map<String,List<String>>();
		catLog.familyValues = new List<String>();
		catLog.familyValues.add('');
		catLog.modelValues = new Map<String,List<String>>();
		catLog.conversionRateMap  =  new Map<String, Double>();
		catLog.curRegionfactors = new  CPQ_Region_Mapping__c(); 
		catLog.currencies =  new list<String>();
		
		//fetching all the countries supported
		catLog.countries = [select Id,Name from Country__c ORDER BY Name ASC NULLS LAST];
        
        //Creating a Set to hold country values
        Set<String> countryValuesSet = new Set<String>();
	    for(Country__c country: catLog.countries)
	    {
		    countryValuesSet.add(country.Name); 
	    }     
		
		//Setting default properties and current user properties
		catLog.selectedRegion   = 'NA'; 
		catLog.selectedCountry = 'USA';
		catLog.selectedFamily   = 'System Solutions';
		catLog.selectedCurrency = UserInfo.getDefaultCurrency();
		catLog.displayPricebookStd = false;  
		catLog.showDiscountable = false;
		catLog.searchByPrice    = false;
		
		
		// Setting permission flag
		catLog.isCatalogAdmin = false;
		catLog.showThreshold  = false;
		Set<String> adminProfileNames = new Set<String> { 'System Administrator',
														  'VMS Sales - Admin',
														  'VMS BST - Member',
														  'VMS System - Support Team Member',
														  'VMS Marketing - User'
														};
		List<Profile> profileNames = [SELECT Id, Name FROM Profile WHERE Id = : UserInfo.getProfileId()];
        for(Profile currProfile : profileNames) 
		{
			if(adminProfileNames.contains(currProfile.Name)) 
			{
				catLog.isCatalogAdmin = true;
				catLog.showThreshold  = true;
			}
        }
        
		//Fetching current user details
		catLog.userLocale   = UserInfo.getLocale();
		catLog.userCurrency = UserInfo.getDefaultCurrency();
		catLog.userId = UserInfo.getUserId();
		
	    List<User> usersInfo = [SELECT Country FROM User WHERE Id = :catLog.userId];
	    for(User currentUser : usersInfo) 
		{
			if(currentUser != null && currentUser.Country != null) 
			{
				catLog.userCountry = currentUser.Country;
				if(countryValuesSet.contains(catLog.userCountry))
					catLog.selectedCountry = currentUser.Country;
				else
					catLog.selectedCountry = 'USA';  
			}
	    }

	    // Default the region based on the country
	    if(!String.isBlank(catLog.selectedCountry)) 
		{
			List<CPQ_Region_Mapping__c> cpqRegions = [SELECT CPQ_Region__c FROM CPQ_Region_Mapping__c WHERE Country__c=:  catLog.selectedCountry.toLowerCase()];
			for(CPQ_Region_Mapping__c curRegion : cpqRegions) 
			{
			    catLog.selectedRegion = curRegion.CPQ_Region__c;
			}
	    }
	  
		if(!catLog.isCatalogAdmin) 
		{
			// If the user has the right permission set, allow them to see threshold
			List<PermissionSetAssignment> userPermissionSets = [SELECT Id, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId=: catLog.userId AND PermissionSet.Name=: catlog.EXT_COL_PERMISSION_SET_NAME];
			if(userPermissionSets.size() > 0) 
			  catLog.showThreshold = true; 
		}
		
		//Querying catalog Model
		Catalog_Model__c[] catalogModelRecords = [ SELECT Family__c, Line__c, Model__c, Family_Order__c, Line_Order__c, Model_Order__c
                                                    FROM Catalog_Model__c WHERE isActive__c = true ORDER BY Family_Order__c, Line_Order__c,
													Model_Order__c];
        
        // populating model and line values for product selection : This is to pass data to UI
        for(Catalog_Model__c modelRow : catalogModelRecords) 
		{
		    String modelKey = modelRow.Family__c + ' ' + modelRow.Line__c;

			// Add the Family if its not already there
			if(!catLog.lineValues.containsKey(modelRow.Family__c)) 
			{
			    catLog.familyValues.add(modelRow.Family__c);
				catLog.lineValues.put(modelRow.Family__c, new List<String>{''});
			}
		    // Add the line if its not already there
		    if(!catLog.modelValues.containsKey(modelKey)) 
		    {
			    catLog.modelValues.put(modelKey, new List<String>{''});
			    List<String> currentLines = catLog.lineValues.get(modelRow.Family__c);
			    currentLines.add(modelRow.Line__c);
			    catLog.lineValues.put(modelRow.Family__c, currentLines);
		    }
		    List<String> currentModels = catLog.modelValues.get(modelKey);
		    currentModels.add(modelRow.Model__c);
		    catLog.modelValues.put(modelKey, currentModels);
        }

        // setting pricebook values
		catLog.PricebookOptions = [SELECT Id, Name, IsStandard FROM Pricebook2 WHERE IsActive = true];
		
		//setting standard and selected pricebook
	    for(Pricebook2 pb : catLog.PricebookOptions) 
		{
		    catLog.priceBookValue.add(pb.Name);
			if(pb.IsStandard) 
			{
				catLog.selectedPricebook = pb.Id;
				catLog.standardPricebook = pb.Id;
		    }
		}
		 
		//Querying for currency conversion rate for global currencies
		List<Long_Term_Conversion_Rate__c> tempCurrency = [SELECT Conversion_Rate__c, CurrencyIsoCode FROM Long_Term_Conversion_Rate__c ORDER BY CurrencyIsoCode];
		String tempValue;

		for(Long_Term_Conversion_Rate__c currCurrency : tempCurrency) 
		{
		    catLog.conversionRateMap.put(currCurrency.CurrencyIsoCode, currCurrency.Conversion_Rate__c);
			if( currCurrency.CurrencyIsoCode == catLog.selectedCurrency)
				tempValue = currCurrency.CurrencyIsoCode;
			else
			    catLog.currencies.add(currCurrency.CurrencyIsoCode);
		}
		catLog.currencies.add(0,tempValue);
		
		if(catLog.lineValues.containsKey(catLog.selectedFamily)) 
		{	
		    catLog.lineOptions   = catLog.lineValues.get(catLog.selectedFamily);
		}
		
		//if(catLog.selectedLine != null) 
		//{
		    //catLog.modelOptions  = catLog.modelValues.get(catLog.selectedLine);
		//}
		catLog.selectedConversionRate = catLog.conversionRateMap.get(catLog.selectedCurrency);
		return catLog ;
    }

	/***************************************************************************
    Description: 
    This method will be invoke based on user selection (on model selection, on Search catalog, on pricebook change).
    It will populate the group and show catalog for selected family based on user input
    *************************************************************************************/
	@AuraEnabled
    public static CatalogViewLightningController quickSearch(String catLogObj)
	{
		//Type casting input JSON into controller object
		Type resultType = Type.forName('CatalogViewLightningController');
		CatalogViewLightningController catLog =(CatalogViewLightningController)JSON.deserialize(catLogObj,resultType);
	    
		catLog.displayPricebookStd = !catLog.selectedPricebook.equals(catLog.standardPricebook);
        Map<Id, Pricebook2> pbs = new Map<Id, Pricebook2>(catLog.PricebookOptions);
		catLog.selectedPriceBookName = pbs.get(catLog.selectedPricebook).Name;
		catLog.selectedConversionRate = catLog.conversionRateMap.get(catLog.selectedCurrency);

        boolean selection = !String.isBlank(catLog.selectedFamily) && !String.isBlank(catLog.selectedLine) && !String.isBlank(catLog.selectedModel);
		catLog.groupedPartsMap = new Map<String, List<CatalogPriceWrapper>>();
        catLog.groupOptions = new List<GroupWrapper>();
		catLog.searchedParts = new List<CatalogPriceWrapper>();
		Set<String> currentGroups  = new Set<String>();
	
		//If block will be excuted if user select product product line and model
		if(String.isBlank(catLog.catalogPartSearch) && selection) 
		{
		    catLog.searchByPrice = false;
		    getModelParts(catLog,currentGroups);
		    if(catLog.groupOptions != null && catLog.groupOptions.size()>0)
		        setDefaultGroup(catLog,currentGroups);
		}
		//This block will be executed based upon user Search catalog input
		else if(!String.isBlank(catLog.catalogPartSearch)) 
		{
		    clearFilters(catLog);
		    updateSearchedParts(catLog);
		}
		
		return catLog;
	}
	
	/***************************************************************************
    Description: 
    This method is used to Query CPQ region mapping based upon country selection
    *************************************************************************************/
	public static void getSelectedRegion(CatalogViewLightningController catLog)
	{
        catLog.curRegionfactors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c WHERE Country__c=:  catLog.selectedCountry.toLowerCase() limit 1];
        catLog.selectedRegion = catLog.curRegionfactors.CPQ_Region__c;
    }
	

	/***************************************************************************
    Description: 
    This method is to clear out values for product family, line and model
    *************************************************************************************/
	public static  void clearFilters(CatalogViewLightningController catLog) 
	{
		catLog.selectedFamily = '';
		catLog.selectedLine   = '';
		catLog.selectedModel  = '';
    }
	
	/***************************************************************************
    Description: 
    This method is to set default product group. It will be first element from group options
    *************************************************************************************/
	public static void setDefaultGroup(CatalogViewLightningController catLog,Set<String>currentGroups) 
	{
		catLog.selectedGroup = new List<SelectGroupWrapper>();
		SelectGroupWrapper sg = new SelectGroupWrapper();
		sg.selectedGroup = catLog.groupOptions[0].groupName;
		catLog.selectedGroup.add(sg);
    }
	
	/***************************************************************************
    Description: 
    This method is to used to get model parts based upon product line and product model
    *************************************************************************************/
	public static void getModelParts(CatalogViewLightningController catLog, Set<String>currentGroups) 
	{
        getSelectedRegion(catLog);
        Set<Id> productIds = new Set<Id>();
        List<Catalog_Part__c> parts = new List<Catalog_Part__c>();
		List<Product2> productParts = new List<Product2>();
        parts = [
				    SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c, Sort_Order__c, Group_Order__c,
				    (SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c FROM Catalog_Part_Prices__r WHERE Region__c =: catLog.selectedRegion)
				    FROM Catalog_Part__c WHERE isActive__c = true AND Family__c =: catLog.selectedFamily AND Line__c =: catLog.selectedLine 
				    AND Model__c =: catLog.selectedModel ORDER BY Group_Order__c, Sort_Order__c ASC NULLS LAST LIMIT 1000
				];
    
		for(Catalog_Part__c part : parts) 
		{
		    if(!productIds.contains(part.Product__c)) 
			    productIds.add(part.Product__c);
		}

        List<Id> pbIds = new List<Id> {catLog.selectedPricebook, catLog.standardPricebook};

		productParts =  [
						    SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c,
							(SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :'USD')	 
                            FROM Product2  WHERE Id IN :productIds LIMIT 1000
		                ];
						
		Map<Id, Product2> testPartMap = new Map<Id, Product2>(productParts);
		List<Id> partIds = new List<Id>(testPartMap.keySet());
        createGroupedPartsMap(catLog,currentGroups,parts,productParts);
    }

    /***************************************************************************
    Description: 
    This method is to used to create a Group parts Map. key will be group value. It will be used to show product decsription table based upon group selection
    *************************************************************************************/
    public static void createGroupedPartsMap(CatalogViewLightningController catLog,Set<String>currentGroups,List<Catalog_Part__c> parts,List<Product2> productParts) 
	{
		Set<String> Gparts  = new Set<String>();
		Map<Id, Product2> m = new Map<Id, Product2>(productParts);
		
		for(Catalog_Part__c part : parts) 
		{
		    Product2 product = m.get(part.Product__c);
		    //creating object of CatalogPartPriceLightning
		    CatalogPartPriceLightning currPart = new CatalogPartPriceLightning(part, product,catLog.standardPricebook, catLog.selectedCountry, catLog.curRegionfactors, catLog.selectedFamily, catLog.selectedLine, catLog.selectedCurrency);
			
		    List<CatalogPriceWrapper> tmpParts = new List<CatalogPriceWrapper>();
		    if(catLog.groupedPartsMap.containskey(part.Group__c)) 
			    tmpParts = catLog.groupedPartsMap.get(part.Group__c);

		    else 
		    {
			    GroupWrapper g = new groupWrapper();
				g.groupname = part.Group__c;
				catLog.groupOptions.add(g);
			    currentGroups.add(part.Group__c);
		    }
		  
		    if( (catLog.showDiscountable && currPart.product != null && (currPart.product.Discountable__c == null || currPart.product.Discountable__c.equals('TRUE')))
			     || !catLog.showDiscountable) 
		    {
			    
                //Population a wrapper class object. Since we cannot send original object to UI beacuse of child records in it.
                CatalogPriceWrapper catPricWr = new CatalogPriceWrapper();
                catPricWr.standardPrice = currPart.standardPrice *   catLog.selectedConversionRate;
                catPricWr.thresholdPrice = currPart.thresholdPrice * catLog.selectedConversionRate;
                catPricWr.targetPrice = currPart.targetPrice * catLog.selectedConversionRate;
                catPricWr.pricebookPrice = currPart.pricebookPrice * catLog.selectedConversionRate;
                catPricWr.prodName = currPart.product.Name;
                catPricWr.prodBigMachinePartNumber = currPart.product.BigMachines__Part_Number__c;
                catPricWr.catalogPartId = currPart.catalogPart.Id;
                catPricWr.longDescription = currPart.longDescription;
                catPricWr.prodId = currPart.product.id;
			    tmpParts.add(catPricWr);  
		    }

		    //Adding details to map and list
		    catLog.groupedPartsMap.put(part.Group__c, tmpParts);
		    Gparts.add(part.Group__c);
		}
    }

	/***************************************************************************
    Description: 
    This method is to used  show product decsription table based upon user input in Search catalog
    *************************************************************************************/
	public static void updateSearchedParts(CatalogViewLightningController catLog) 
	{
		Set<Id> productIds = new Set<Id>();
		String searchString = catLog.catalogPartSearch.trim();
		List<Id> pbIds = new List<Id> {catLog.selectedPricebook, catLog.standardPricebook};
        List<Product2> productParts = new List<Product2>();
		//User selected Search by price checkbox
		if(catLog.searchByPrice) 
		{
		    //Converting String to double
		    Double searchPrice = Double.valueOf(searchString.remove(','));
		    List<PricebookEntry> matchingPrices =   [
														SELECT Id, Product2Id 
														FROM PricebookEntry WHERE UnitPrice = : searchPrice AND CurrencyIsoCode = :'USD' 
														LIMIT 1000
		                                            ];
		    
		    Set<Id> matchingProductIds = new Set<Id>();
		    for(PricebookEntry curPBE : matchingPrices) 
		    {
			    matchingProductIds.add(curPBE.Product2Id);
		    }

		   productParts =   [
								SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c
								,(SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :'USD')
								FROM Product2 WHERE Id IN :matchingProductIds LIMIT 1000
		                    ];
		}
		// Searching based on search input
		else 
		{
		    searchString = '%'+searchString+'%';
		    productParts =  [
								SELECT Description, BigMachines__Part_Number__c, Id, Name, Discountable__c,DASP__c,Threshold__c 
								,(SELECT UnitPrice, Pricebook2Id FROM PricebookEntries WHERE Pricebook2Id IN :pbIds AND CurrencyIsoCode = :'USD')
								FROM Product2 WHERE Name LIKE :searchString
								OR BigMachines__Part_Number__c LIKE :searchString LIMIT 1000
		                    ];  
		}
		
		Map<Id, Product2> m = new Map<Id, Product2>(productParts);
		productIds = m.keySet();
	    List<pricebookentry> a = new List<pricebookentry>();
		getSelectedRegion(catlog);
		List<Catalog_Part__c> parts = new List<Catalog_Part__c>();
		parts = [
				    SELECT Id, Name, Family__c, Line__c, Model__c, Group__c, Product__c
				   ,(SELECT Standard_Factor__c, Goal_Factor__c, Threshold_Factor__c  FROM Catalog_Part_Prices__r WHERE Region__c = : catlog.selectedRegion) 
				    FROM Catalog_Part__c WHERE Product__c IN :productIds AND isActive__c = true
				    ORDER BY Model__c, Group__c LIMIT 1000
		        ];

		for(Catalog_Part__c part : parts) 
		{
		    Product2 product = m.get(part.Product__c);
		    CatalogPartPriceLightning newPart = new CatalogPartPriceLightning(part, product, catLog.standardPricebook, catLog.selectedCountry, catLog.curRegionfactors, catLog.selectedFamily, catLog.selectedFamily, catLog.selectedCurrency);
		    if( (catLog.showDiscountable && newPart.product != null &&
			    (newPart.product.Discountable__c == null || newPart.product.Discountable__c.equals('TRUE'))) || ! catLog.showDiscountable) 
			{
			    //populating wrapper class object
			    CatalogPriceWrapper catPricWr = new CatalogPriceWrapper();
                catPricWr.standardPrice = newPart.standardPrice * catLog.selectedConversionRate;
                catPricWr.thresholdPrice = newPart.thresholdPrice * catLog.selectedConversionRate;
                catPricWr.targetPrice = newPart.targetPrice * catLog.selectedConversionRate;
                catPricWr.pricebookPrice = newPart.pricebookPrice * catLog.selectedConversionRate;
                catPricWr.prodName = newPart.product.Name;
                catPricWr.prodBigMachinePartNumber = newPart.product.BigMachines__Part_Number__c;
                catPricWr.catalogModel = newPart.catalogPart.Model__c;
                catPricWr.catalogGroup = newPart.catalogPart.Group__c;
                catPricWr.catalogPartId = newPart.catalogPart.Id;
                catPricWr.longDescription = newPart.longDescription;
                catPricWr.prodId = newPart.product.Id;
			    catLog.searchedParts.add(catPricWr);
		    }
		}
    } 

	/***************************************************************************
    Description: 
    This method is to used to open a modal showing product details based upon selected row from table
    *************************************************************************************/
	@AuraEnabled
	public static CatalogViewLightningController selectRow(String catLogObj) 
	{
		Type resultType = Type.forName('CatalogViewLightningController');
		CatalogViewLightningController catLog =(CatalogViewLightningController)JSON.deserialize(catLogObj,resultType);
		
		Map<Id, CatalogPriceWrapper> m = new Map<Id, CatalogPriceWrapper>();
		if(String.isBlank(catLog.catalogPartSearch)) 
		{
		    //iteration on user selection
		    for(SelectGroupWrapper sg : catLog.selectedGroup) 
		    {
			    for(CatalogPriceWrapper part : catLog.groupedPartsMap.get(sg.selectedGroup)) 
			    {
			        m.put(part.catalogPartId, part);
			    }   
		    }
		}
		else 
		{
		    for(CatalogPriceWrapper part : catLog.searchedParts) 
			{
			    m.put(part.catalogPartId, part);
		    }
		}
		
		catLog.selectedCatalogPart = m.get(catLog.selectedCatalogPartId);
		catLog.selectedCatalogPart.longDescription =  new catalogPartPriceLightning().fetchLongDescription(catLog.selectedCatalogPart.prodId);
		return catLog;
    }
	

   /***************************************************************************
    Description: This wrapper class is created to store group values
    *************************************************************************************/
	public class GroupWrapper
	{ 
	  @AuraEnabled	public String groupName {get;set;}
		
    }
    
    /***************************************************************************
    Description: This wrapper class is created to store user selected group values
    *************************************************************************************/
    public class SelectGroupWrapper
	{ 
	   @AuraEnabled	public String selectedGroup {get;set;}
    }
    
    /***************************************************************************
    Description: This wrapper class is created to store catalog price details
    *************************************************************************************/
    public class CatalogPriceWrapper
    {
        @AuraEnabled public Double standardPrice {get;set;}
        @AuraEnabled public Double thresholdPrice {get;set;}
        @AuraEnabled public Double targetPrice {get;set;}
        @AuraEnabled public Double pricebookPrice {get;set;}
        @AuraEnabled public String prodBigMachinePartNumber {get;set;}
        @AuraEnabled public String prodName {get;set;}
        @AuraEnabled public Id catalogPartId {get;set;}
        @AuraEnabled public String catalogModel {get;set;}
        @AuraEnabled public String catalogGroup {get;set;}
        @AuraEnabled public String longDescription {get;set;}
        @AuraEnabled public Id prodId {get;set;}
    }

}
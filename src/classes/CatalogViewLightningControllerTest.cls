/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning : STSK0012590
Description: 
This is a Test Class for CatalogViewLightningController and CatalogPartPriceLightning.
Change Log:

*************************************************************************************/

@isTest (seeAllData=true)
private  class CatalogViewLightningControllerTest
{
   private static String catalogPartId;
   private static String prodId;


   /***************************************************************************
    Description: 
    This method is to used to insert product,Catalog part and part price
    *************************************************************************************/
   @isTest static void insertProductsAndCatalogParts() 
   {
        //inserting Product
        List<Product2> products = new List<Product2>();
        for(Integer counter=0; counter<5; counter++)
        {
            Product2 product = new Product2();
            product.Name = 'Test Product'+counter;
            if(counter<2){
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Family = 'Software Solution';
                Product.SVMXC__Product_Line__c = 'Desktop';
                Product.Product_Group__c = '4D Integrated Treatment Console (4DITC)';
                product.Discountable__c = 'TRUE';
            }else if(counter<4){
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Family = 'System Solution';
                Product.SVMXC__Product_Line__c = 'Solution';
                Product.Product_Group__c = '4D Integrated Treatment Console (4DITC)';
                product.Discountable__c = 'TRUE';
            }else{
                product.ProductCode = 'TEST CODE'+counter;
                product.BigMachines__Part_Number__c = 'TEST NUMBER'+counter;
                product.Discountable__c = 'TRUE';
            }
            products.add(product);
    	}
        insert products;
        prodId =  products[4].id;

        // insertCatalogParts
        List<Catalog_Part__c> parts = new List<Catalog_Part__c>();
        
        for(Integer counter=0; counter<5; counter++)
        {
            Catalog_Part__c part = new Catalog_Part__c();
            part.Name = products[counter].Name;
            part.Product__c = products[counter].Id;
            if(counter<2){
                part.Family__c = 'Software Solutions';
                part.Line__c = 'OIS (ARIA)';
                part.Model__c = 'Eclipse';
                part.Group__c = 'ABO Licenses';
            }else if(counter<4){
                part.Family__c = 'System Solutions';
                part.Line__c = 'BrachyTherapy Hardware';
                part.Model__c = 'VariSource';
                part.Group__c = 'Treatment Accessories';
            }else{
                part.Family__c = 'System Solutions';
                part.Line__c = 'BrachyTherapy Hardware';
                part.Model__c = 'VariSource';
                part.Group__c = 'Interstitial / Breast Catheter Kits';
            }
            parts.add(part);
        }
        insert parts;
        catalogPartId = parts[4].Id;

        //inserting part prices
        List<Catalog_Part_Price__c> partPricing = new List<Catalog_Part_Price__c>();
       
        List<String> regions = new List<String>{'NA', 'PAC', 'LAT', 'JPN',
                                                'EURW', 'EURE', 'CHN', 'AUS'};
        for(Catalog_Part__c part : parts)
        {
            for(String region : regions){
                Catalog_Part_Price__c partPrice = new Catalog_Part_Price__c();
                partPrice.Catalog_Part__c = part.Id;
                partPrice.Goal_Factor__c = 1.000000000;
                partPrice.Standard_Factor__c = 1.000000000;
                partPrice.Threshold_Factor__c = 1.000000000;
                partPrice.Region__c = region;
                partPricing.add(partPrice);
            }
        }
        
        insert partPricing;

        // insert Price Book Entries
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Product2 product : products){
            PricebookEntry pbEntry  = new PricebookEntry();
            pbEntry.Product2Id = product.Id;
            pbEntry.Pricebook2Id = Test.getStandardPricebookId();
            pbEntry.CurrencyISOCode = 'USD';
            pbEntry.UnitPrice = 100;
            pbEntry.IsActive = true;
            pricebookEntries.add(pbEntry);
        }
        insert pricebookEntries;

		//inserting Language data
		Language__c lang = new Language__c();
		lang.Name = 'Testing';
		lang.CurrencyIsoCode = 'USD';
		lang.BMI_Code__c ='en';
		insert lang;

        //insert Product Description language
        Product_Description_Language__c pdl = new Product_Description_Language__c();
        pdl.Product__c  = products[4].id;
        pdl.Long_Description__c = 'Test data';
        pdl.Language__c =lang.id;
        pdl.Long_Desc_RichText__c = 'Test data';
        insert pdl;
    }
    
   
  /***************************************************************************
    Description: 
    This method is to used to test Init method  and Quick Search Method of controlle for a user having defined Country
    *************************************************************************************/
	@isTest static void testMethod1() 
	{
    	insertProductsAndCatalogParts();
    	CatalogViewLightningController testCatalogView;
    	Test.starttest();

    	//Creating a running user with System Admin profile
    	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    	User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
				            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
				            LocaleSidKey='en_US', ProfileId = p.Id, 
				            TimeZoneSidKey='America/Los_Angeles', UserName='test_varian@testorg.com',Country='USA');
        System.runAs(u)
        {
            testCatalogView =  CatalogViewLightningController.initClass();

            // Search by Family
            testCatalogView.selectedFamily = 'System Solutions';
            testCatalogView.selectedLine = 'BrachyTherapy Hardware';
            testCatalogView.selectedModel = 'VariSource';
            testCatalogView.selectedPricebook = Test.getStandardPricebookId();
            testCatalogView = CatalogViewLightningController.quickSearch(JSON.serialize(testCatalogView));
            System.assert(testCatalogView.groupedPartsMap.size() >0,'No Group part Found');
            

            // Code Coverage for Open Dialog Box on part selection from group part
            testCatalogView.selectedCatalogPartId = catalogPartId ;
            testCatalogView.selectedGroup = new List<CatalogViewLightningController.SelectGroupWrapper>();
            CatalogViewLightningController.SelectGroupWrapper sg = new CatalogViewLightningController.SelectGroupWrapper();
		    sg.selectedGroup = 'Interstitial / Breast Catheter Kits'  ;
            testCatalogView.selectedGroup.add(sg);
            testCatalogView = CatalogViewLightningController.selectRow(JSON.serialize(testCatalogView));
            System.assertEquals('Test Product4', testCatalogView.selectedCatalogPart.prodName);

            //Code coverage for Search catalog when Search by price is false
            testCatalogView.catalogPartSearch = 'Test Product0';
            testCatalogView.searchByPrice = false;
            testCatalogView.selectedPricebook = Test.getStandardPricebookId();
            testCatalogView =  CatalogViewLightningController.quickSearch(JSON.serialize(testCatalogView));
            System.assertEquals(1,testCatalogView.searchedParts.size());


            //Code coverage for Dialog Box on catalog part Search
            testCatalogView.selectedCatalogPartId = catalogPartId ;
            testCatalogView.catalogPartSearch = 'Test Product';
            testCatalogView =  CatalogViewLightningController.quickSearch(JSON.serialize(testCatalogView));
            testCatalogView = CatalogViewLightningController.selectRow(JSON.serialize(testCatalogView));
            System.assertEquals('Test Product4', testCatalogView.selectedCatalogPart.prodName);

    
            //Code coverage for Search catalog when Search by price is True
            testCatalogView.catalogPartSearch = '100';
            testCatalogView.searchByPrice = true;
            testCatalogView =  CatalogViewLightningController.quickSearch(JSON.serialize(testCatalogView));
            System.assertEquals(5,testCatalogView.searchedParts.size());


            //Code Coverage for catalogPartPrice class for controller and methods
	        Catalog_Part__c tempPart = new Catalog_Part__c();
	        tempPart = [SELECT Id,name,(select id,Standard_factor__c from Catalog_Part_Prices__r) FROM Catalog_Part__c WHERE Id =: catalogPartId];
	        CPQ_Region_Mapping__c factors = [SELECT CPQ_Region__c,DASP_Factor__c,Regional_Target_Factor__c,SalesTier__c,Th_Factor__c FROM CPQ_Region_Mapping__c where Country__c= 'USA'];
	        CatalogPartPriceLightning tempCpartPrice = new CatalogPartPriceLightning(tempPart,null,Test.getStandardPricebookId(),null, factors, 'System Solutions', 'Advanced Treatment Delivery', 'USD' );
	        tempCpartPrice.getCatalogPartId();
	        tempCpartPrice.isDiscountable(); 

            
            //Code Coverage for catalogPartPrice class when product is not discountable
            Product2 prod = [select id,Discountable__c, (select id,Pricebook2Id,UnitPrice from pricebookEntries ) from product2 where Id =: prodId];
            prod.Discountable__c = 'false';
            prod.threshold__c = 1.0;
            prod.DASP__c = 0.0;
            CatalogPartPriceLightning tempCpartPrice1 = new CatalogPartPriceLightning(tempPart,prod,catalogPartId,'USA', factors, 'System Solutions', 'Advanced Treatment Delivery', 'USD' );
            Double targetPrice = tempCpartPrice1.targetPrice;
            Double thresholdPrice  =  tempCpartPrice1.thresholdPrice;
            Double pricebookPrice = tempCpartPrice1.pricebookPrice;
            

            //Code Coverage for catalogPartPrice class when product is discountable
            prod.Discountable__c = 'TRUE';
            prod.DASP__c = 0.0;
            prod.threshold__c = 1.0;
            factors.Th_factor__c = 1.0;
            factors.DASP_Factor__c = 0.0;
            factors.Regional_Target_Factor__c = 0.0;
            CatalogPartPriceLightning tempCpartPrice2 = new CatalogPartPriceLightning(tempPart,prod,Test.getStandardPricebookId(),'USA', factors, 'System Solutions', 'Advanced Treatment Delivery', 'USD' );
            targetPrice = tempCpartPrice2.targetPrice;
            thresholdPrice  =  tempCpartPrice2.thresholdPrice;
            Double standardPrice = tempCpartPrice2.standardPrice;
            
        }
        
        Test.stoptest();
	}

   
    /***************************************************************************
    Description: 
    This method is to used to test Init method for User outside defined Country vallues 
    *************************************************************************************/

   @isTest static void testMethod2()
   {
        CatalogViewLightningController testCatalogView;
        PermissionSet ps1 = [SELECT ID From PermissionSet WHERE Name = 'Catalog_View_Extended_Column_View'];
    	Profile p1 = [SELECT Id FROM Profile WHERE Name='Standard User'];
    	User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
				            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
				            LocaleSidKey='en_US', ProfileId = p1.Id, 
				            TimeZoneSidKey='America/Los_Angeles', UserName='test_varian1@testorg.com',Country='Test');
    	insert u1;
    	insert new PermissionSetAssignment(AssigneeId = u1.id, PermissionSetId = ps1.Id );
        System.runAs(u1)
        {
        	testCatalogView =  CatalogViewLightningController.initClass();

        }	
   } 
    
   /***************************************************************************
    Description: 
    This method is to used to test export controller
    *************************************************************************************/
   public static testmethod void testCatalogViewExport(){
        Test.setCurrentPage(ApexPages.currentPage());
        String json = '{ '+
                      ' "selectedModel": "Clinac iX Base",'+
                      ' "selectedGroup": "",'+
                      ' "selectedFamily": "System Solutions",'+
                      ' "selectedLine": "Treatment Delivery",'+
                      ' "selectedConversionRate": 20,'+
                      ' "showDiscountable": false,'+
                      ' "selectedRegion": "USA",'+
                      ' "selectedCountry": "USA",'+
                      ' "selectedPricebook": "01sE0000000IA4oIAG",'+
                      ' "standardPricebook": "01sE0000000IA4oIAG"'+
                      '  }';
        ApexPages.currentPage().getParameters().put('catalogJSON',json);
        CatalogViewExportController obj = new CatalogViewExportController();
    }
}
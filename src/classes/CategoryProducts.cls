/*@RestResource(urlMapping = '/Categoryproduct/*')
global class CategoryProducts {
    @HttpGet
    global static List < VU_Products__c > getBlob() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        // String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String Id = RestContext.request.params.get('CategoryName');
        String Language= RestContext.request.params.get('Language');
        List < VU_Products__c > a ;
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){        
        a= [SELECT(SELECT Id, ContentType FROM Attachments), Id, Playlist_Id__c, Language__c, Name, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND (Language__c ='English(en)' OR Language__c ='')];
            }
            else
            {
            a= [SELECT(SELECT Id, ContentType FROM Attachments), Id, Playlist_Id__c, Language__c, Name, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND Language__c =: Language];
            //system.debug('test--------' + a);
            } 
       // a= [SELECT(SELECT Id, ContentType FROM Attachments) Id, Playlist_Id__c, Name,Language__c, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND Language__c=:Language];
        system.debug('test--------' + a);
        return a;
    }
    
   
}*/

@RestResource(urlMapping = '/Categoryproduct/*')
global class CategoryProducts {
    @HttpGet
    global static List < VU_Products__c > getBlob() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        // String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String Id = RestContext.request.params.get('CategoryName');
        String Language= RestContext.request.params.get('Language');
        List < VU_Products__c > a ;
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(ja)' && Language !='Portuguese(pt-BR)' ){        
        a= [SELECT(SELECT Id, ContentType FROM Attachments), Id, Playlist_Id__c, Language__c, Name, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND (Language__c ='English(en)' OR Language__c ='')];
            }
            else
            {
            a= [SELECT(SELECT Id, ContentType FROM Attachments), Id, Playlist_Id__c, Language__c, Name, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND Language__c =: Language];
            //system.debug('test--------' + a);
            } 
       // a= [SELECT(SELECT Id, ContentType FROM Attachments) Id, Playlist_Id__c, Name,Language__c, Product_Image__c, Product_Description__c, Clinical_Benefits__c from VU_Products__c where RecordTypeId =: Id AND Language__c=:Language];
        system.debug('test--------' + a);
        return a;
    }
    
   
}
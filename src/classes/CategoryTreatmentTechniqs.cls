/*@RestResource(urlMapping='/CategoryTreatmentTechniqs/*')
     global class CategoryTreatmentTechniqs 
     {
     @HttpGet
     global static List<VU_Treatment_Technique__c> getBlob()
     {
      List<VU_Treatment_Technique__c> a ;
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Access-Control-Allow-Origin', '*');
     res.addHeader('Content-Type', 'application/json');
     res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
     String Id= RestContext.request.params.get('CategoryName') ;
     String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){  
   a= [select(SELECT Id, ContentType FROM Attachments) Id, Name,Languages__c, RecordType.Name, Description__c, Treatment_Technique_Image__c,Playlist_Id__c from VU_Treatment_Technique__c where RecordTypeId=:Id AND (Languages__c='English(en)' OR Languages__c='')];
}
else
{
    a= [select(SELECT Id, ContentType FROM Attachments) Id, Name,Languages__c, RecordType.Name, Description__c, Treatment_Technique_Image__c,Playlist_Id__c from VU_Treatment_Technique__c where RecordTypeId=:Id AND Languages__c=:Language];
     }
     return a;
     }  
     
   
     }*/
     
     @RestResource(urlMapping='/CategoryTreatmentTechniqs/*')
     global class CategoryTreatmentTechniqs 
     {
     @HttpGet
     global static List<VU_Treatment_Technique__c> getBlob()
     {
      List<VU_Treatment_Technique__c> a ;
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Access-Control-Allow-Origin', '*');
     res.addHeader('Content-Type', 'application/json');
     res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
     String Id= RestContext.request.params.get('CategoryName') ;
     String Language= RestContext.request.params.get('Language');
    if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(ja)' && Language !='Portuguese(pt-BR)' ){  
   a= [select(SELECT Id, ContentType FROM Attachments) Id, Name,Languages__c, RecordType.Name, Description__c, Treatment_Technique_Image__c,Playlist_Id__c from VU_Treatment_Technique__c where RecordTypeId=:Id AND (Languages__c='English(en)' OR Languages__c='')];
}
else
{
    a= [select(SELECT Id, ContentType FROM Attachments) Id, Name,Languages__c, RecordType.Name, Description__c, Treatment_Technique_Image__c,Playlist_Id__c from VU_Treatment_Technique__c where RecordTypeId=:Id AND Languages__c=:Language];
     }
     return a;
     }  
     
   
     }
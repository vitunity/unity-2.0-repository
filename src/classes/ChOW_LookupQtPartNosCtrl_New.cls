public with sharing class ChOW_LookupQtPartNosCtrl_New {

    //Page parameters
   @TestVisible private String chowToolQtId;
    private String chowToolQtNo;
    @TestVisible private String paginatorFilter;
    @auraEnabled public String searchText{get;set;}
    
    private static Integer RECORDS_PER_PAGE = 10; 

    //@AuraEnabled
    //public static List<PartWrapper> partWrappers{ get; set; }    
    @AuraEnabled
    public static Integer totalSizeCount { get; set; }    

    @AuraEnabled
    public static PartWrapperList getParts(String chowToolQtId,
    									   String searchText, 
    									   Integer recordsOffset, 
    									   Integer totalSize) {
        //buildQuery(invoiceStatus, acctid, frmdate, todate, Document_TypeSearch, recordsOffset, totalSize);

        String soqlQuery;
        String countQuery;
        if(searchText != null && searchText != '') {
         	soqlQuery = 'Select Name, Grp__c , BigMachines__Description__c, Header__c '
                                + ' From BigMachines__Quote_Product__c where  '
								+ ' Name LIKE :searchText AND BigMachines__Quote__c = :chowToolQtId Limit 10000';

        	countQuery = 'Select count() From BigMachines__Quote_Product__c where  '
								+ ' Name LIKE :searchText AND BigMachines__Quote__c = :chowToolQtId';
        } else {
         	soqlQuery = 'Select Name, Grp__c , BigMachines__Description__c, Header__c '
                                + ' From BigMachines__Quote_Product__c where  '
								+ ' BigMachines__Quote__c = :chowToolQtId Limit 10000';

        	countQuery = 'Select count() From BigMachines__Quote_Product__c where  '
								+ ' BigMachines__Quote__c = :chowToolQtId';
        }
                          
        System.debug('---soqlQuery = '+soqlQuery);    
        System.debug('---countQuery = '+countQuery); 
        System.debug('---searchText = '+searchText); 
        System.debug('---chowToolQtId = '+chowToolQtId);                      
        list<BigMachines__Quote_Product__c> listParts = Database.query(soqlQuery);
		totalSizeCount = Database.countQuery(countQuery);

        //system.debug('#### debug invoiceWrappers in getInvoices = ' + invoiceWrappers);
        //return invoiceWrappers;
        PartWrapperList invWrapFnl = new PartWrapperList(listParts, totalSizeCount);
        return invWrapFnl;
    }

    public class PartWrapperList
    {
        @AuraEnabled public List<BigMachines__Quote_Product__c> lstParts { get; set; }
        @AuraEnabled public Integer totalSize { get; set; }

        public PartWrapperList(list<BigMachines__Quote_Product__c> partsList, Integer totSize) {
            this.lstParts = partsList;
            this.totalSize = totSize;
        }
    }    


    @AuraEnabled
    public static ChOW_LookupQtPartNosCtrl_New initializeLookupParts(String chowToolQtId,
    															 String chowToolQtNo, 
    															 String searchText){
    //public static ChOW_PartnerLookupCtrl initializeLookupPartners(String partnerFunction, String soldToNumber, String salesOrg, String searchText){    
        ChOW_LookupQtPartNosCtrl_New lookupPart = new ChOW_LookupQtPartNosCtrl_New();
        System.debug('#### debug chowToolQtNo = ' + chowToolQtNo);
        System.debug('#### debug searchText = ' + searchText);        

        ////lookupPartner.partnerFunction = partnerFunction;
        lookupPart.chowToolQtId = chowToolQtId;
        lookupPart.chowToolQtNo = chowToolQtNo;
        ////lookupPartner.salesOrg = salesOrg;
        lookupPart.searchText = searchText;

        return lookupPart;
    }
    
    
    /**
     * Returns list of erp associations related to search text and page parameters
     */
    @AuraEnabled
    public List<BigMachines__Quote_Product__c> getQuoteProducts(){
         System.debug('#### debug list = ' + (List<BigMachines__Quote_Product__c>)quoteProductPaginator.getRecords());
        return (List<BigMachines__Quote_Product__c>)quoteProductPaginator.getRecords();
    }
    
    /**
     * Search erp associations based on page parameters and search string
     */
   
    /** reference to the paginating controller and get ERP association paginator*/
     @TestVisible
    private ApexPages.StandardSetController quoteProductPaginator {
        get {
            System.debug(Logginglevel.INFO,'searchText'+searchText);
            if(quoteProductPaginator == null && String.isBlank(searchText)) {
                
                quoteProductPaginator = getQuoteProductsBYSOQL();
                quoteProductPaginator.setPageSize(RECORDS_PER_PAGE);
            }else if(paginatorFilter != searchText && searchText!=''){
                
                if(String.isBlank(searchText)){
                    quoteProductPaginator = getQuoteProductsBYSOQL();
                }else{
                    quoteProductPaginator = getQuoteProductsBYSOSL(searchText);
                }
                paginatorFilter = searchText;
                quoteProductPaginator.setPageSize(RECORDS_PER_PAGE);
            }
            return quoteProductPaginator;
        }
        set;
    }
    
    ///////////// Private Utility Methods //////////////
    /**
     * Return query instead of sosl because we need all erp partner associations 
     * and not used sosl on ERP Partrner Association as sosl will not search formula fields
     * (most of the fields from erp partner are reffered using formula fields in ERP partner association)
     */
    @TestVisible
    private ApexPages.StandardSetController getQuoteProductsBYSOSL(String searchText){
        
        String searchString = '%'+searchText+'%';
        System.debug('#### debug searchString = ' + searchString);
        return new ApexPages.StandardSetController(Database.getQueryLocator([Select Name, Grp__c , BigMachines__Description__c, Header__c
                                                                            From BigMachines__Quote_Product__c 
                                                                            where (Name LIKE :searchString) 
                                                                            AND BigMachines__Quote__c = :chowToolQtId
                                                                                 Limit 10000]));                                                                                    
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all ERP Associations with search text
     */
    @TestVisible
    private ApexPages.StandardSetController getQuoteProductsBYSOQL(){

        String soqlQuery = 'Select Name, Grp__c , BigMachines__Description__c, Header__c '
                                + ' From BigMachines__Quote_Product__c where  ';
        
        soqlQuery += getFilters();  
        soqlQuery += ' Limit 10000';                            
        System.debug('---soqlQuery'+soqlQuery);                         
        return new ApexPages.StandardSetController(Database.getQueryLocator(soqlQuery));
    }
    
    /**
     * Utility methid for getting filters for query
     */
    @TestVisible
    private String getFilters(){
        
        String filters = '';
        
        //soldToNumber = '6051553';
        if(!String.isBlank(chowToolQtId)){
            filters += ' BigMachines__Quote__c = \''+chowToolQtId+'\' ';
        }

        /*
        if(!String.isBlank(salesOrg)){
            filters += ' Sales_Org__c = \''+salesOrg+'\' and';
        } */
        
        return filters.removeEnd('and');
    }
    
    //////////////////////////////////////////////////////////////////////////////
    ////// Pagination  Actions                                               /////
    //////////////////////////////////////////////////////////////////////////////
    
    // indicates whether there are more records after the current page set.
    @AuraEnabled
    public Boolean hasNext {
        get {
            return quoteProductPaginator.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    @AuraEnabled
    public Boolean hasPrevious {
        get {
            return quoteProductPaginator.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    @AuraEnabled
    public Integer pageNumber {
        get {
            return quoteProductPaginator.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    @AuraEnabled
    @TestVisible public Integer totalPages {
        get {
            Decimal dTotalPages = quoteProductPaginator.getResultSize()/quoteProductPaginator.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(quoteProductPaginator.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    @AuraEnabled
    public void getFirst() {
        quoteProductPaginator.first();
    }

    // returns the last page of records
    @AuraEnabled
    public void getLast() {
        quoteProductPaginator.last();     
    }

    // returns the previous page of records
    @AuraEnabled
    public void getPrevious() {
        quoteProductPaginator.previous();
    }

    // returns the next page of records
    @AuraEnabled
    public List<BigMachines__Quote_Product__c> getNext() {
    	System.debug('#### quoteProductPaginator = ' + quoteProductPaginator);
        quoteProductPaginator.next();
        System.debug('#### debug list in getNext = ' + (List<BigMachines__Quote_Product__c>)quoteProductPaginator.getRecords());
        return (List<BigMachines__Quote_Product__c>)quoteProductPaginator.getRecords();       
    }

}
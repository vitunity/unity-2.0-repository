public with sharing class ChOW_PartnerLookupCtrl {
    //Page parameters
    @TestVisible private String partnerFunction;
    @TestVisible private String soldToNumber;
    @TestVisible private String salesOrg;
    @TestVisible private String paginatorFilter;
    @auraEnabled public String searchText{get;set;}
    
   @TestVisible private static Integer RECORDS_PER_PAGE = 10; 
   @TestVisible private String sitePartner = 'Z1=Site Partner';
    
    @AuraEnabled
    public static ChOW_PartnerLookupCtrl initializeLookupPartners(String soldToNumber, String searchText){
    //public static ChOW_PartnerLookupCtrl initializeLookupPartners(String partnerFunction, String soldToNumber, String salesOrg, String searchText){    
        ChOW_PartnerLookupCtrl lookupPartner = new ChOW_PartnerLookupCtrl();
        System.debug('#### debug soldToNumber = ' + soldToNumber);
        System.debug('#### debug searchText = ' + searchText);        

        ////lookupPartner.partnerFunction = partnerFunction;
        lookupPartner.soldToNumber = soldToNumber;
        ////lookupPartner.salesOrg = salesOrg;
        lookupPartner.searchText = searchText;

        return lookupPartner;
    }
    
    @AuraEnabled
    public String getPartnerType(){
        Map<String,String> partnerTypes = new Map<String,String>{'SP' => 'Sold To Parties', 'Z1' => 'Site Partners',
                                                                'SH' => 'Ship To Parties', 'EU' => 'End Users',
                                                                'PY' => 'Payers', 'BP' => 'Bill To Parties'};
        return partnerTypes.get(partnerFunction);
    }
    
    /**
     * Returns list of erp associations related to search text and page parameters
     */
    @AuraEnabled
    public List<ERP_Partner_Association__c> getErpPartnerAssociations(){
         System.debug('#### debug list = ' + (List<ERP_Partner_Association__c>)erpAssociationPaginator.getRecords());
        return (List<ERP_Partner_Association__c>)erpAssociationPaginator.getRecords();
    }
    
    /**
     * Search erp associations based on page parameters and search string
     */
   
    /** reference to the paginating controller and get ERP association paginator*/
    @TestVisible 
    private ApexPages.StandardSetController erpAssociationPaginator {
        get {
            System.debug(Logginglevel.INFO,'searchText'+searchText);
            if(erpAssociationPaginator == null && String.isBlank(searchText)) {
                
                erpAssociationPaginator = getERPAssociationsBYSOQL();
                erpAssociationPaginator.setPageSize(RECORDS_PER_PAGE);
            }else if(paginatorFilter != searchText && searchText!=''){
                
                if(String.isBlank(searchText)){
                    erpAssociationPaginator = getERPAssociationsBYSOQL();
                }else{
                    erpAssociationPaginator = getERPAssociationsBYSOSL(searchText);
                }
                paginatorFilter = searchText;
                erpAssociationPaginator.setPageSize(RECORDS_PER_PAGE);
            }
            return erpAssociationPaginator;
        }
        set;
    }
    
    ///////////// Private Utility Methods //////////////
    /**
     * Return query instead of sosl because we need all erp partner associations 
     * and not used sosl on ERP Partrner Association as sosl will not search formula fields
     * (most of the fields from erp partner are reffered using formula fields in ERP partner association)
     */
    @TestVisible 
    private ApexPages.StandardSetController getERPAssociationsBYSOSL(String searchText){
        
        String searchString = '%'+searchText+'%';
        ////String pf = partnerFunction+'%';

        System.debug('#### debug searchString = ' + searchString);

        ////searchString = '6051553';
        ////pf = 'Z1=Site Partner';
        
        return new ApexPages.StandardSetController(Database.getQueryLocator([Select Sales_Org__c,ERP_Partner__r.Name, ERP_Partner__r.Street__c,
                                                                                ERP_Partner_Number__c,ERP_Partner__r.City__c,ERP_Partner__r.State_Province_Code__c,
                                                                                ERP_Partner__r.Country_Code__c, ERP_Partner__r.Zipcode_Postal_Code__c 
                                                                            From ERP_Partner_Association__c 
                                                                            ////where Partner_Function__c LIKE :pf
                                                                            ////And 
                                                                            where (ERP_Customer_Number__c LIKE :searchString
                                                                                    OR ERP_Partner__r.Name LIKE :searchString
                                                                                 OR ERP_Partner_Number__c LIKE :searchString) 
                                                                                 and Partner_Function__c = :sitePartner
                                                                                 Limit 10000]));                                                                            
                                                                            /*
                                                                            where (ERP_Partner__r.Name LIKE :searchString
                                                                                 OR ERP_Partner_Number__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Street__c LIKE :searchString
                                                                                 OR ERP_Partner__r.City__c LIKE :searchString
                                                                                 OR ERP_Partner__r.State_Province_Code__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Country_Code__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Zipcode_Postal_Code__c LIKE :searchString
                                                                                 OR Sales_Org__c LIKE :searchString
                                                                                 OR ERP_Customer_Number__c LIKE :searchString) 
                                                                                 and Partner_Function__c = :sitePartner
                                                                                 Limit 10000]));
                                                                            */
        
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all ERP Associations with search text
     */
    @TestVisible 
    private ApexPages.StandardSetController getERPAssociationsBYSOQL(){

        String soqlQuery = 'Select Sales_Org__c,ERP_Partner__r.Name, ERP_Partner__r.Street__c,'
                                +'ERP_Partner_Number__c,Partner_Function__c,'
                                +'ERP_Partner__r.City__c,ERP_Partner__r.State_Province_Code__c,'
                                +'ERP_Partner__r.Country_Code__c, ERP_Partner__r.Zipcode_Postal_Code__c '
                                + 'From ERP_Partner_Association__c where Partner_Function__c = :sitePartner and  ';
        
        soqlQuery += getFilters();  
        soqlQuery += ' Limit 10000';                            
        System.debug('---soqlQuery'+soqlQuery);                         
        return new ApexPages.StandardSetController(Database.getQueryLocator(soqlQuery));
    }
    
    /**
     * Utility methid for getting filters for query
     */
    @TestVisible 
    private String getFilters(){
        
        String filters = '';
        
        //soldToNumber = '6051553';
        if(!String.isBlank(soldToNumber)){
            filters += ' ERP_Customer_Number__c = \''+soldToNumber+'\' ';
        }

        /*
        if(!String.isBlank(salesOrg)){
            filters += ' Sales_Org__c = \''+salesOrg+'\' and';
        } */
        
        return filters.removeEnd('and');
    }
    
    //////////////////////////////////////////////////////////////////////////////
    ////// Pagination  Actions                                               /////
    //////////////////////////////////////////////////////////////////////////////
    
    // indicates whether there are more records after the current page set.
    @AuraEnabled
    public Boolean hasNext {
        get {
            return erpAssociationPaginator.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    @AuraEnabled
    public Boolean hasPrevious {
        get {
            return erpAssociationPaginator.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    @AuraEnabled
    public Integer pageNumber {
        get {
            return erpAssociationPaginator.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    @AuraEnabled
     @TestVisible
    public Integer totalPages {
        get {
            Decimal dTotalPages = erpAssociationPaginator.getResultSize()/erpAssociationPaginator.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(erpAssociationPaginator.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    @AuraEnabled
    public void getFirst() {
        erpAssociationPaginator.first();
    }

    // returns the last page of records
    @AuraEnabled
    public void getLast() {
        erpAssociationPaginator.last();     
    }

    // returns the previous page of records
    @AuraEnabled
    public void getPrevious() {
        erpAssociationPaginator.previous();
    }

    // returns the next page of records
    @AuraEnabled
    public void getNext() {
       erpAssociationPaginator.next();
    }

}
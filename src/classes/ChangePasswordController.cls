/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 03-Apr-2013
    @ Description   : An apex page and test class that exposes the change password functionality.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :  08-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

public with sharing class ChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}        
    
    public PageReference changePassword() {
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
    }     
    
    public ChangePasswordController() {}
    
    @IsTest(SeeAllData=true) public static void testChangePasswordController() {
        // Instantiate a new controller with all parameters in the page
        ChangePasswordController controller = new ChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';                
        
        System.assertEquals(controller.changePassword(),null);                           
    }    
}
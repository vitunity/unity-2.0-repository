global class CharketCustomAssignAgentHandler implements Charket.WeChatAgentSelector
{
    global String getAgentId(Charket.WeChatAgentSelectorContext context)
    {
        String agentSkills;
        String wechatAccountId;

        if(context.MsgType == 'SCAN' || context.MsgType == 'SUBSCRIBE')
        {
            List<Charket__WeChatQRCode__c> qrCodes = [select AgentSkills__c,
                Charket__WeChatAccount__c
                from Charket__WeChatQRCode__c
                where Charket__Ticket__c != null and Charket__IsActive__c = true
                and Charket__Ticket__c = :context.MsgContent limit 1];
            if(qrCodes.size() > 0)
            {
                agentSkills = qrCodes[0].AgentSkills__c;
                wechatAccountId = qrCodes[0].Charket__WeChatAccount__c;
            }
        }
        else if(context.MsgType == 'CLICK')
        {
            List<Charket__WeChatMenuItem__c> menuItems = [select AgentSkills__c,
                Charket__WeChatMenu__r.Charket__WeChatAccount__c
                from Charket__WeChatMenuItem__c
                where Id = :context.MsgContent limit 1];
            if(menuItems.size() > 0)
            {
                agentSkills = menuItems[0].AgentSkills__c;
                wechatAccountId = menuItems[0].Charket__WeChatMenu__r.Charket__WeChatAccount__c;
            }
        }
        else if(context.MsgType == 'TEXT')
        {
            List<Charket__WeChatResponseKeyword__c> exactMatchKeywords = [select Charket__WeChatResponse__r.AgentSkills__c,
                    Charket__WeChatResponse__r.Charket__WeChatAccount__c
                    from Charket__WeChatResponseKeyword__c
                    where Charket__WeChatResponse__r.Charket__IsActive__c = true
                    and Charket__WeChatResponse__r.Charket__UsedFor__c = 'Smart Reply'
                    and Charket__Keyword__c = :context.MsgContent
                    order by CreatedDate desc limit 1];
            if(exactMatchKeywords.size() > 0)
            {
                agentSkills = exactMatchKeywords[0].Charket__WeChatResponse__r.AgentSkills__c;
                wechatAccountId = exactMatchKeywords[0].Charket__WeChatResponse__r.Charket__WeChatAccount__c;
            }
            else
            {
                List<Charket__WeChatResponseKeyword__c> phraseMatchKeywords = [select Charket__WeChatResponse__r.AgentSkills__c,
                    Charket__WeChatResponse__r.Charket__WeChatAccount__c, Charket__Keyword__c
                    from Charket__WeChatResponseKeyword__c
                    where Charket__WeChatResponse__r.Charket__IsActive__c = true
                    and Charket__WeChatResponse__r.Charket__UsedFor__c = 'Smart Reply'
                    and Charket__MatchMode__c = 'Phrase Match'
                    order by CreatedDate desc limit 1000];
                for(Charket__WeChatResponseKeyword__c responseKeyword : phraseMatchKeywords)
                {
                    if(context.MsgContent.containsIgnoreCase(responseKeyword.Charket__Keyword__c))
                    {
                        agentSkills = responseKeyword.Charket__WeChatResponse__r.AgentSkills__c;
                        wechatAccountId = responseKeyword.Charket__WeChatResponse__r.Charket__WeChatAccount__c;
                        break;
                    }
                }
            }
        }
        else if(context.MsgType == 'TRANSFER')
        {
            List<Charket__WeChatTranscript__c> transcripts = [select
                    Charket__WeChatAgent__c, Charket__WeChatAgent__r.Skills__c
                    from Charket__WeChatTranscript__c
                    where Charket__FollowerOpenId__c = :context.FollowerOpenId
                    and Charket__WeChatAgent__c != null
                    and Charket__IsClosed__c = false limit 1];
            if(transcripts.size() > 0 && String.isNotBlank(transcripts[0].Charket__WeChatAgent__r.Skills__c))
            {
                agentSkills = transcripts[0].Charket__WeChatAgent__r.Skills__c;
            }
        }
        if(String.isNotBlank(agentSkills))
        {
            if(String.isBlank(wechatAccountId))
            {
                List<Charket__WeChatFollower__c> followers = [select Charket__WeChatAccount__c
                    from Charket__WeChatFollower__c
                    where Charket__OpenId__c = :context.FollowerOpenId limit 1];
                if(followers.size() > 0)
                {
                    wechatAccountId = followers[0].Charket__WeChatAccount__c;
                }
            }

            String agentId = getLeastActiveAgentId(wechatAccountId, agentSkills);
            if(context.MsgType == 'TRANSFER')
            {
                List<Charket__WeChatAgent__c> currentAgents = [select Id
                        from Charket__WeChatAgent__c
                        where Id = :agentId and Charket__User__c = :UserInfo.getUserId() limit 1];
                if(currentAgents.size() > 0)
                {
                    return agentId;
                }
            }
            else
            {
                return agentId;
            }
        }
        return null;
    }

    private String getLeastActiveAgentId(String wechatAccountId, String agentSkills)
    {
        List<Id> onlineUserIds = new List<Id>();
        List<Id> offlineUserIds = new List<Id>();
        String donotStatus = 'Don\'t Accept Chats';
        String skillQuery = 'select Charket__User__c, Skills__c, Charket__IsAway__c ' +
                ' from Charket__WeChatAgent__c' +
                ' where Charket__WeChatAccount__c = :wechatAccountId' +
                ' and Charket__IsActive__c = true and Charket__Status__c != :donotStatus';

        for(String skill : agentSkills.split(';'))
        {
            skillQuery += ' and Skills__c includes (\'' + skill + '\') ';
        }

        for(Charket__WeChatAgent__c agent : Database.query(skillQuery))
        {
            if(!agent.Charket__IsAway__c)
            {
                onlineUserIds.add(agent.Charket__User__c);
            }
            else
            {
                offlineUserIds.add(agent.Charket__User__c);
            }
        }

        if(onlineUserIds.size() > 0)
        {
            String query = 'select Charket__User__c user, sum(Charket__ChatCount__c) num, min(Charket__LastChatCompletedTime__c) '+
                'from Charket__WeChatAgent__c ' +
                'where Charket__User__c in :onlineUserIds and Charket__IsAway__c = false '+
                'and Charket__WeChatAccount__c = :wechatAccountId and Charket__Status__c != :donotStatus ' +
                'group by Charket__User__c order by sum(Charket__ChatCount__c), min(Charket__LastChatCompletedTime__c) limit 1';
            List<AggregateResult> agentResults = Database.query(query);
            if(agentResults.size() > 0)
            {
                String userId = String.valueOf(agentResults[0].get('user'));
                Integer count = Integer.valueOf(agentResults[0].get('num'));
                List<Charket__WeChatAgent__c> assignedAgents = [select Id, Charket__ChatCount__c, Charket__IsAway__c
                        from Charket__WeChatAgent__c
                        where Charket__User__c = :userId
                        and Charket__WeChatAccount__c = :wechatAccountId
                        and Charket__Status__c != 'Don\'t Accept Chats'
                        and Charket__IsActive__c = true limit 1];
                if(assignedAgents.size() > 0)
                {
                    return assignedAgents[0].Id;
                }
            }
        }

        List<Charket__WeChatAgent__c> offlineAgents = [select Id from Charket__WeChatAgent__c
            where Charket__IsAway__c = true and Charket__WeChatAccount__c = :wechatAccountId
            and Charket__IsActive__c = true and Charket__User__c in :offlineUserIds
            and Charket__Status__c != 'Don\'t Accept Chats'
            order by Charket__ChatCount__c, Charket__LastChatCompletedTime__c];
        if(offlineAgents.size() > 0)
        {
            return offlineAgents[0].Id;
        }
        return null;
    }
}
@isTest
private class CharketCustomAssignAgentHandlerTest
{
    private static Charket__WeChatAccount__c account;
    private Static Id accountId;
    private static String openId = 'ASSIGN_AGENT_OPENID';
    private Static Charket__WeChatQRCode__c qrCode;
    private Static Charket__WeChatMenuItem__c menuItem;

    @TestSetup
    static void init()
    {
        account = new Charket__WeChatAccount__c(
                Name = 'Test Account', Charket__Type__c = 'subscribe',
                Charket__WeChatOriginId__c = 'ASSIGN_AGENT_ORIGIN_ID'
            );
        insert account;
        accountId = account.Id;

        Charket__WeChatFollower__c follower = new Charket__WeChatFollower__c(
                Charket__OpenId__c = openId, Charket__WeChatAccount__c = account.Id,
                Charket__IsFollowing__c = true, Charket__SubscriptionDate__c = Datetime.now()
            );
        insert follower;

        Profile p = [select Id from Profile where Name = 'System Administrator'];

        List<User> users = new List<User>();
        for(Integer i = 0; i < 8; i++)
        {
            User user = new User(
                    Alias = 'user' + i, Email = 'testuseragent' + i + '@varian.sfqa.com',
                    EmailEncodingKey = 'UTF-8', LastName = 'user' + i, LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US', ProfileId = p.Id,
                    TimeZoneSidKey = 'America/Los_Angeles', UserName = 'testuseragent' + i + '@varian.sfqa.com'
                );
            users.add(user);
        }
        insert users;

        List<Charket__WeChatAgent__c> agents = new List<Charket__WeChatAgent__c>();
        for(User user : users)
        {
            Charket__WeChatAgent__c agent = new Charket__WeChatAgent__c(
                Name = user.Name, Charket__User__c = user.Id,
                Charket__WeChatAccount__c = account.Id, Charket__IsActive__c = true
            );
            agents.add(agent);
        }
        insert agents;
        DescribeFieldResult describe = Charket__WeChatAgent__c.Skills__c.getDescribe();
        List<PicklistEntry> availableValues = describe.getPicklistValues();
        agents[0].Skills__c = availableValues[0].getValue();
        agents[0].Charket__Status__c  = 'Online';
        agents[0].Charket__LastOnlineTime__c = datetime.now();
        agents[1].Skills__c = availableValues[0].getValue();
        agents[2].Skills__c = availableValues[0].getValue();
        agents[3].Skills__c = availableValues[0].getValue();
        agents[4].Skills__c = availableValues[0].getValue();
        agents[5].Skills__c = availableValues[0].getValue();
        agents[6].Skills__c = availableValues[0].getValue();
        update agents;

        qrCode = new Charket__WeChatQRCode__c(
                Charket__WeChatAccount__c = account.Id, Charket__Scene__c = '123',
                Charket__Type__c = 'Temporary', Charket__Ticket__c = 'bS9xLzAyTFMzSVptRW1lMDQxNFRwdXhxMWkAAgQ3zPZZAwQAjScA',
                AgentSkills__c = Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue()
            );
        insert qrCode;

        Charket__WeChatMenu__c menu = new Charket__WeChatMenu__c(
            Charket__Status__c = 'Active', Charket__WeChatAccount__c = account.Id
        );
        insert menu;
        menuItem = new Charket__WeChatMenuItem__c(
            Name = 'Test Menu', Charket__WeChatMenu__c = menu.Id,
            AgentSkills__c = Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue());
        insert menuItem;
    }

    @isTest
    static void getAgentIdByScanTest()
    {
        Charket__WeChatAccount__c accountScan = [select Id from Charket__WeChatAccount__c where Name = 'Test Account'];
        List<User> users = [select Id from User where Alias like 'user%'];

        Type t = Type.forName('', 'CharketCustomAssignAgentHandler');
        Charket.WeChatAgentSelector agentSelector = (Charket.WeChatAgentSelector)t.newInstance();
        Charket.WeChatAgentSelectorContext context = new Charket.WeChatAgentSelectorContext('SCAN', 'bS9xLzAyTFMzSVptRW1lMDQxNFRwdXhxMWkAAgQ3zPZZAwQAjScA', openId);
        String result = agentSelector.getAgentId(context);

        List<Charket__WeChatAgent__c> agents = [select Id from Charket__WeChatAgent__c
                where Skills__c = :Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue()
                and Charket__WeChatAccount__c = :accountScan.Id limit 1];
        System.assertEquals(result, agents[0].Id);
    }

    @isTest
    static void getAgentIdByClickTest()
    {
        Charket__WeChatAccount__c accountClick = [select Id from Charket__WeChatAccount__c where Name = 'Test Account'];
        Type t = Type.forName('', 'CharketCustomAssignAgentHandler');
        Charket.WeChatAgentSelector agentSelector = (Charket.WeChatAgentSelector)t.newInstance();
        Charket__WeChatMenuItem__c clickMenuItem = [select Id from Charket__WeChatMenuItem__c where Name = 'Test Menu' limit 1];
        Charket.WeChatAgentSelectorContext context = new Charket.WeChatAgentSelectorContext('CLICK', clickMenuItem.Id, openId);
        String result = agentSelector.getAgentId(context);

        List<Charket__WeChatAgent__c> agents = [select Id from Charket__WeChatAgent__c
                where Skills__c = :Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue()
                and Charket__WeChatAccount__c = :accountClick.Id limit 1];
        System.assertEquals(result, agents[0].Id);
    }

    @isTest
    static void getAgentIdByTextTest()
    {
        Charket__WeChatResponse__c weChatResponse = new Charket__WeChatResponse__c(
                Charket__IsActive__c = true, Charket__UsedFor__c = 'Smart Reply',
                AgentSkills__c = Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue());
        insert weChatResponse;
        Charket__WeChatResponseKeyword__c weChatResponseKeyword = new Charket__WeChatResponseKeyword__c(
                Charket__MatchMode__c = 'Phrase Match',
                Charket__WeChatResponse__c = weChatResponse.Id, Charket__Keyword__c = 'bS9xL');
        insert weChatResponseKeyword;

        Charket__WeChatAccount__c accountText = [select Id from Charket__WeChatAccount__c where Name = 'Test Account'];

        Type t = Type.forName('', 'CharketCustomAssignAgentHandler');
        Charket.WeChatAgentSelector agentSelector = (Charket.WeChatAgentSelector)t.newInstance();
        Charket.WeChatAgentSelectorContext context = new Charket.WeChatAgentSelectorContext('TEXT', 'bS9xL', openId);
        String result = agentSelector.getAgentId(context);

        List<Charket__WeChatAgent__c> agents = [select Id from Charket__WeChatAgent__c
                where Skills__c = :Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue()
                and Charket__WeChatAccount__c = :accountText.Id limit 1];
        System.assertEquals(result, agentS[0].Id);
    }

    @isTest
    static void getAgentIdByPhraseTest()
    {
        Charket__WeChatResponse__c weChatResponse = new Charket__WeChatResponse__c(
                Charket__IsActive__c = true, Charket__UsedFor__c = 'Smart Reply',
                AgentSkills__c = Charket__WeChatAgent__c.Skills__c.getDescribe().getPicklistValues()[0].getValue());
        insert weChatResponse;
        Charket__WeChatResponseKeyword__c weChatResponseKeyword = new Charket__WeChatResponseKeyword__c(
                Charket__MatchMode__c = 'Phrase Match',
                Charket__WeChatResponse__c = weChatResponse.Id, Charket__Keyword__c = 'bS9xL');
        insert weChatResponseKeyword;

        Charket__WeChatAccount__c accountText = [select Id from Charket__WeChatAccount__c where Name = 'Test Account'];

        Type t = Type.forName('', 'CharketCustomAssignAgentHandler');
        Charket.WeChatAgentSelector agentSelector = (Charket.WeChatAgentSelector)t.newInstance();
        Charket.WeChatAgentSelectorContext context = new Charket.WeChatAgentSelectorContext('TEXT', 'bS9xLtest', openId);
        String result = agentSelector.getAgentId(context);
    }

    @isTest
    static void getAgentIdForTransferTest()
    {
        Charket__WeChatFollower__c follower = [select Id, Charket__WeChatAccount__c from Charket__WeChatFollower__c
                where Charket__OpenId__c = :openId limit 1];
        Charket__WeChatAgent__c agent = [select Id from Charket__WeChatAgent__c
                where Charket__WeChatAccount__c = :follower.Charket__WeChatAccount__c limit 1];
        agent.Charket__LastDesktopActiveTime__c = Datetime.now();
        update agent;
        Charket__WeChatTranscript__c transcript = new Charket__WeChatTranscript__c(
                Charket__WeChatFollower__c = follower.Id, Charket__FollowerOpenId__c = openId,
                Charket__Status__c = 'Waiting', Charket__WeChatAgent__c = agent.Id
            );
        insert transcript;

        Type t = Type.forName('', 'CharketCustomAssignAgentHandler');
        Charket.WeChatAgentSelector agentSelector = (Charket.WeChatAgentSelector)t.newInstance();
        Charket.WeChatAgentSelectorContext context = new Charket.WeChatAgentSelectorContext('TRANSFER', '', openId);

        String result = agentSelector.getAgentId(context);
    }
}
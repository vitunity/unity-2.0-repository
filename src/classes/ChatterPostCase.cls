/***************************************************************************
Author: Nick Sauer 
Created Date: 1-March-2018
Project/Story/Inc/Task : STSK0013858 : Chatter:  Post to Chatter to Feed of Preferred Technician when Case Closed
Description: 
This is handler class for posting to chatter
*************************************************************************************/

public class ChatterPostCase {
                            
    public static void postOnCaseClose(List<case> lstc, map<Id,Case> mapOld){
        set<Id> FilteredCases = new set<Id>();
        for(case c : lstc){
            if(c.Status == 'Closed' && mapOld.get(c.id).status != c.Status){
                if(c.Status == 'Closed' 
                   && c.Closed_Case_Reason__c != 'Closed by Work Order' 
                   && c.Closed_Case_Reason__c != 'Customer Cancelled' 
                   && c.Closed_Case_Reason__c != 'Created in Error'){
                    FilteredCases.add(c.id);
                }
            }
        }
        
        if(FilteredCases.size()>0){
            List<FeedItem> posts= new List<FeedItem>();
            List<Case> updatedCase = [SELECT Id,CaseNumber,SVMXC__Top_Level__r.Name,Account.Name,Local_Subject__c,
                                      Reason,Case_Preferred_Technician__c,Case_Preferred_Technician__r.User__r.Id,
                                      Closed_Case_Reason__c,Status,Subject 
                                      FROM Case WHERE Id IN :FilteredCases];
            List<User> lstsysuser = [SELECT Id FROM user WHERE Name = 'VMS-boomi' limit 1];
            if(lstsysuser.size() > 0){
                user sysuser = lstsysuser[0];       
                for(Case ca: updatedCase){
                    if(ca.Case_Preferred_Technician__c != null && ca.Case_Preferred_Technician__r.User__r.Id != null){
                        FeedItem post = new FeedItem(
                            ParentId= ca.Case_Preferred_Technician__r.User__r.Id,
                            CreatedById= sysuser.Id,
                            Body='Case '+ca.CaseNumber+' against '+ca.SVMXC__Top_Level__r.Name+' at '+ca.Account.Name+' for which you are the Preferred Technician has been Closed.'+'\n'+'\n'
                            +'Case Reason: '+ca.Reason+'\n'
                            +'Subject: '+ca.Local_Subject__c+'\n'
                            +'Close Reason: '+ca.Closed_Case_Reason__c
                        );
                        posts.add(post);
                    }
                }
            }
            insert posts;
        }
    }

}
@isTest
private class ChatterPostCaseTest {
    public static Id ServiceTeamPersonnelRecTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    static Case objCase = SR_testdata.createCase();
    static Contact objCont = SR_testdata.createContact();
    static Account objAcc = SR_testdata.creteAccount();
    static SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c();
    static SVMXC__Service_Group__c objTeam = new SVMXC__Service_Group__c();
    static DateTime d = system.Now();
    static Country__c objCounty = new Country__c();
    static ERP_Org__c objOrg = new ERP_Org__c();
        
    static testMethod void Chatter_Post_Test() {
        Profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
        User u = new user(alias = 'standt2', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse922@testclass.com');
        insert u;
        
        objAcc.AccountNumber = '123456';
        objAcc.country__c = 'Australia';
        objAcc.ERP_Timezone__c = 'AUSSA';
        objAcc.BillingState = 'Indiana';
        objAcc.BillingCity = 'Pune';
        insert objAcc;
        
        objTeam.recordTypeId = ServiceTeamPersonnelRecTypeId;
        objTeam.Name = 'Test Team';
        insert objTeam;
        
        objTechEquip.User__c = u.Id;
        objTechEquip.SVMXC__Service_Group__c = objTeam.Id;
        insert objTechEquip;
       
        objCase.AccountID = objAcc.ID;
        objCase.ContactID = objCont.ID;
        objCase.Reason = 'System Down';
        objCase.Priority = 'Medium';
        objCase.Type ='Problem';
        objCase.Reason = 'Source Exchange';
        objCase.Case_Preferred_Technician__c = objTechEquip.Id;
        objCase.Status = 'New';
        objCase.Closed_Case_Reason__c = null;
        objCase.Is_This_a_Complaint__c = 'No';
        objCase.Was_anyone_injured__c = 'No';
        objCase.Is_escalation_to_the_CLT_required__c = 'No';
        insert objCase;
        
        test.startTest();
        objCase.Status = 'Closed';
        objCase.Closed_Case_Reason__c = 'Parts Ordered';
        update objCase;
        test.stopTest();
        
        FeedItem uf = [Select Id, ParentId, Body From FeedItem Where ParentId = :u.Id ORDER BY LastModifiedDate DESC LIMIT 1];
        System.assertNotEquals(null, uf.Body);
        
        }  
}
/**********************************************************************************************************-->
Page Name:Chatterform-->    
Developer Name:Bhanu Devaguptapu-->
Date:09/22/2013 -->

Purpose:To create an apex class  which helps users rep to submit the chatter response-->
Reference:controller-test-->
Behavior :  Saving the user data based on the type of the group and emailing to help desk. 
Scenario :  User will enter the data from custom links in home page
            Based on the type of group, the requests would be mailed to the unity help desk email id.
                
 
<!--********************************************************************************************************/
public class ChatterRequest {

   
   public String m1datedisplay { get; set; }

 
  //setting and getting the variables from user entered Chatterform page
  public Boolean isRequired=false;
  public Boolean isRequired2=false;
  public Boolean isRequired3=false;

  public string requestor {get;set;}
  public string phno {get;set;}
  public string owner {get;set;}

  public List<SelectOption> pgrp {get; set;}
  public string grp {get;set;}

  public List<SelectOption> tgrp {get; set;}
  public string grptyp {get;set;}

  public string BusinessType {get;set;}
  public List<SelectOption> busiType {get; set;} 

  public string SuperRegion {get;set;}
  public List<SelectOption> SRegion{get;set;} 

  public string Department {get;set;}
  public List<SelectOption> dept{get;set;}  

  public string requestgrpName {get;set;} 
  public string eventrequestgrpName {get;set;} 
  public string eventgrpPurpose {get;set;}
  public string eventgrpEnd {get;set;}
  public Date   eventendDate {get; set;}
  public string eventgrpdesc {get;set;}
  public string grpEnd ;
  public List<SelectOption> groupEnd {get; set;}
  public Date sampleDateField {get; set;}
  public string grpdesc {get;set;}
  public string grpPurpose {get;set;}
  public string socialrequestgrpName {get;set;}
  public string socialgrpPurpose {get;set;}
  public string socialgrpdesc {get;set;}
  public string socialgrpEnd {get;set;}
  public Date   socialendDate {get; set;}
  
  public Boolean hide = false;
  public Boolean hide1 = false;
  public Boolean hide2 = false;

  public String selectedValue{get;set;}
  public string c {get;set;}
  public Account acc {get;set;}

  public Input_Clearance__c dateGrp1 {get;set;}
  public Input_Clearance__c dateGrp2 {get;set;}
  public Input_Clearance__c dateGrp3 {get;set;}
  public Boolean showPopup {get;set;}
     
  public String getgrpEnd()
  {
    return grpEnd;
  }
    

  public void setgrpEnd(String grpEnd )
  {
    this.grpEnd=grpEnd;
  }
  
  public String geteventgrpEnd()
  {
    return eventgrpEnd;
  }
      
  
  public void seteventgrpEnd(String eventgrpEnd)
  {
    this.eventgrpEnd=eventgrpEnd;
  }
  
  public String getsocialgrpEnd()
  {
    return socialgrpEnd;
  }
      
  
  public void setsocialgrpEnd (String grpEnd )
  {
    this.socialgrpEnd=socialgrpEnd;
  }
     
     public void setMhide(Boolean b) {
       this.hide = b;
     } 
     
     public Boolean getMhide() {
       return this.hide;
     }//Passing input dynamically through another method
       
     public Boolean getM1hide() {
         return this.hide2;
     }
     
     public void setM1hide(Boolean b) {
       this.hide2 = b;  
     }
     
     public Boolean getM2hide() {
         return this.hide1;
     }
     
     public void setM2hide(Boolean b) {
         this.hide1 = b;
     }  
 
 //displaying only Selected option information and hiding other information
      
     public void grpTypeMethod()
     {
         system.debug('-----------------Called');
         if(selectedValue == 'Business')
         {
             setMhide(true);
             setM2hide(false);
             setM1hide(false);  
         }
         if(selectedValue == 'Event')
         {
             setM1hide(true);
             setM2hide(false);
             setMhide(false);
         }
         if(selectedValue == 'Social')
         {
             setM2hide(true);
             setM1hide(false);
             setMhide(false);
         }
         if(selectedValue == '')
         {
             setM2hide(false);
             setM1hide(false);
             setMhide(false);
         }
      
     }
     
    
    public ChatterRequest() {
    //  // passing list of  picklist values to VF page for User selection
        pgrp = new List<SelectOption>();
        pgrp.add(new SelectOption('Public', 'Public'));
        pgrp.add(new SelectOption('Private', 'Private'));
        
        tgrp = new List<SelectOption>();
        tgrp.add(new SelectOption('Business', 'Business'));
        tgrp.add(new SelectOption('Event', 'Event'));
        tgrp.add(new SelectOption('Social', 'Social'));
        
        busiType = new List<SelectOption>();
        busiType.add(new SelectOption('Select...', 'Select...'));
        busiType.add(new SelectOption('VMS', 'VMS'));
        busiType.add(new SelectOption('VOS', 'VOS'));
        busiType.add(new SelectOption('VPT','VPT'));
        
        SRegion = new List<SelectOption>();
        SRegion.add(new SelectOption('Select...', 'Select...'));
        SRegion.add(new SelectOption('AMS', 'AMS'));
        SRegion.add(new SelectOption('APAC', 'APAC'));
        SRegion.add(new SelectOption('EMEA', 'EMEA'));
        SRegion.add(new SelectOption('WW', 'WW'));
        
        dept = new List<SelectOption>();
        dept.add(new SelectOption('Select...', 'Select...'));
        dept.add(new SelectOption('ALL', 'ALL'));
        dept.add(new SelectOption('HR', 'HR'));
        dept.add(new SelectOption('MRKT', 'MRKT'));
        dept.add(new SelectOption('MCM', 'MCM'));
        dept.add(new SelectOption('MFG', 'MFG'));
        dept.add(new SelectOption('SALES', 'SALES'));
        dept.add(new SelectOption('SERVICE', 'SERVICE'));
        dept.add(new SelectOption('SITE SOLUTIONS', 'SITE SOLUTIONS'));
        dept.add(new SelectOption('TRAINING', 'TRAINING'));
        dept.add(new SelectOption('TTS', 'TTS'));
        dept.add(new SelectOption('VBT', 'VBT'));

        groupEnd = new List<SelectOption>();
        groupEnd.add(new SelectOption('N0', 'No'));
        groupEnd.add(new SelectOption('Yes', 'Yes'));
        
        grp = 'Public';
        
        User u = [SELECT FirstName,LastName,Phone FROM User WHERE Id =:UserInfo.getUserId()];
        requestor = u.LastName;
        phno = u.Phone;
        acc = new Account();
        showPopup = false;
        socialgrpEnd = 'Yes';
        eventgrpEnd  = 'Yes';
        grpEnd = 'Yes';
        dateGrp1  = new Input_Clearance__c();
        dateGrp2  = new Input_Clearance__c();
        dateGrp3  = new Input_Clearance__c();
    /** User c = [SELECT Phone FROM User WHERE Id =:UserInfo.getUserId()];
        phno = u.Phone; **/
    }
    
    public boolean getVal()
    {
      boolean f=true;
        if(grpEnd=='No')
        {
           grpEnd='No';
           f=false;
         }  
         return f;  
    }

	//Saving User data  and sending an email and redirecting them to home page. 
  	public pagereference doSendEmail() {
      	// First, reserve email capacity for the current Apex transaction to ensure                
    	// that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
    	pagereference pageRef;
    
    	try{
    		Messaging.reserveSingleEmailCapacity(2);

    		// Processes and actions involved in the Apex transaction occur next,
    		// which conclude with sending a single email.
    
    		// Now create a new single email message object
    		// that will send out a single email to the addresses in the To, CC & BCC list.
    		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
    		// Strings to hold the email addresses to which you are sending the email.
    		String[] toAddresses = new String[] {'unityhelp@varian.com'}; 
   			// String[] ccAddresses = new String[] {'bhanu.devaguptapu@varian.com'};
      
    
    		// Assign the addresses for the To and CC lists to the mail object.
    		mail.setToAddresses(toAddresses);
    		//mail.setCcAddresses(ccAddresses);
    
    		// Specify the address used when the recipients reply to the email. 
    		mail.setReplyTo('unity.helpdesk@varian.com');
    
    		// Specify the name used as the display name.
    		mail.setSenderDisplayName('{$User.name}');
    
    		// Specify the subject line for your email address.
    		mail.setSubject('Unity: Chatter Group Request : '+ requestgrpName );
    
    		// Set to True if you want to BCC yourself on the email.
    		mail.setBccSender(false);
    
    		// Optionally append the salesforce.com email signature to the email.
    		// The email address of the user executing the Apex Code will be used.
    		mail.setUseSignature(false);
    
    		if(selectedValue  == '')
    		{
            	apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in the mandatory fields');
            	return null;
    		}
    
    		// Specify the text content of the email.
    		if (selectedValue == 'Business') 
    		{  
      			if(BusinessType == 'Select...'|| SuperRegion == 'Select...'||Department== 'Select...'||requestgrpName == ''|| grpPurpose == '')
       			{
            		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in the mandatory fields');
            		apexpages.addmessage(msg);
            		return null;
       			}
       
       			/*if(grpEnd=='Yes'){
           		system.debug('********************'+dateGrp1.Actual_Submission_Date__c);
       
  				//     if(dateGrp1.Actual_Submission_Date__c ==null)
       			if(true)
       			{
            		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in Date fields');
            		apexpages.addmessage(msg);
        		}
        		}*/
      
      
     			mail.setPlainTextBody('Requestor\t\t: '+ requestor + '\n Is this group public or private?\t: ' + grp 
     			    				+'\n Type of group\t\t: ' + selectedValue +'\n Business Type\t\t: ' + BusinessType+'\n Which Super Region\t\t:'+SuperRegion
     							    +'\n Which Department\t\t: '+ Department+'\nRequested Group Name\t: '+ requestgrpName+'\n Group Description\t\t: '+ grpPurpose
     								+'\n Does this Group have an end date\t:' + grpEnd+ '\n If yes, when:\t\t'+dateGrp1.Actual_Submission_Date__c);
    
				// mail.setHtmlBody('<table><tr><td>Requestor</td><td>'+ requestor +'</td></tr>'
    			//				   +'<tr><td>Is this group public or private?</td>+<td>'+grp+'</td></tr>'
    			//				   +'<tr><td>Type of group</td><td>' + selectedValue +'</td></tr>'
    			//				   +'<tr><td>Business Type</td><td>' + BusinessType+'</td></tr>'
    			//				   +'<tr><td>Which Super Region</td><td>'+SuperRegion+'</td></tr>'
    			//				   +'<tr><td>Which Department</td><td>'+ Department+'</td></tr>'
    			//				   +'<tr><td>Requested Group Name</td><td>'+requestgrpName+'</td></tr>'
    			//				   +'<tr><td>Group Description</td><td>'+grpPurpose+'</td></tr>'
    			//				   +'<tr><td>Does this Group have an end date:' + grpEnd+'</td></tr>'
    			//				   +'<tr><td>If yes, when:'+dateGrp1.Actual_Submission_Date__c +'</tr></td>' ) ;
     			// }    
    
      			// mail.setHtmlBody('<b>Requestor:'+ requestor +'<b>Contact Number:' +phno+'<br><b>Is this group public or private?:' + grp 
      			//  +'<br><b>Type of group:<b> ' + selectedValue +'<br><b> Business Type :' + BusinessType+'<br> <b> Which Super Region:'+SuperRegion
      			//  +'<br> <b> Which Department:'+ Department+'<br> <b>Requested Group Name:'+ requestgrpName+'<br> <b>Group Description:'+ grpPurpose
      			//  +'<br> <b>Does this Group have an end date:' + grpEnd+ '<br> <b>  If yes, when:'+dateGrp1.Actual_Submission_Date__c);
  			}
   
  
			if (selectedValue == 'Event')
			{     
			      if(eventrequestgrpName == ''|| eventgrpPurpose == '')
			      {
			            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in mandatory fields');
			            apexpages.addmessage(msg);
			            return null;
			      }
			    
			      mail.setPlainTextBody('Requestor\t\t: '+ requestor + '\n Is this group public or private?\t: ' + grp 
			            +'\n Type of group\t\t: ' + selectedValue +'\n Business Type\t\t: ' + BusinessType+'\n Requested Group Name\t: '+ eventrequestgrpName+'\n Group Description\t:'+ eventgrpPurpose
			          	+'\n Does this Group have an end date\t:' + eventgrpEnd+ '\n  If yes, when\t\t:'+dateGrp2.Actual_Submission_Date__c);
			        
			      mail.setHtmlBody('<b>Requestor: '+ requestor +'<br><b>Is this group public or private?:' + grp 
			            + '<br><b>Type of group:<b>' + selectedValue +'<br> <b> Business Type :' + BusinessType+'<br> <b>Requested Group Name:'+ eventrequestgrpName+'<br><b>Group Description:'+ eventgrpPurpose
			          	+'<br> <b>Does this Group have an end date:' + eventgrpEnd+ '<br> <b>  If yes, when:'+dateGrp2.Actual_Submission_Date__c);
			}
			  
			if (selectedValue == 'Social')
			{
			    if(socialrequestgrpName == ''|| socialgrpdesc == ''||socialgrpPurpose =='')
			    {
			     		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in mandatory fields');
			            apexpages.addmessage(msg);
			            return null;
			     }
			     mail.setPlainTextBody('Requestor\t\t: '+ requestor +'\n Is this group public or private?\t: ' + grp 
			            +'\n Type of group\t\t: ' + selectedValue +'\n Business Type\t\t: ' + BusinessType+'\n Requested Group Name\t\t: '+ socialrequestgrpName+'\n Group Description\t: '+ socialgrpPurpose
			          	+'\n Does this Group have an end date\t: ' + socialgrpEnd + '\n  If yes, when\t: '+ dateGrp3.Actual_Submission_Date__c);
			    
			    
			      mail.setHtmlBody('<b>Requestor: '+ requestor +'<br><b>Is this group public or private?:' + grp 
			            +'<br><b>Type of group:<b> ' + selectedValue +'<br> <b>Requested Group Name:'+ socialrequestgrpName+'<br> <b>Group Description:'+ socialgrpPurpose
			          	+'<br> <b>Does this Group have an end date:' + socialgrpEnd + '<br> <b>  If yes, when:'+ dateGrp3.Actual_Submission_Date__c);
			}
          
      
		    if(selectedValue == 'Business')
		    {
		    	if(grpEnd == 'Yes' && dateGrp1.Actual_Submission_Date__c == null)
		        {
		        	apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'please Enter Date.');
		            apexpages.addmessage(msg);
		            return null;
		        } 
		    }
		         
		    if(selectedValue == 'Event')
		    {
		          if(eventgrpEnd == 'Yes' && dateGrp2.Actual_Submission_Date__c == null)
		          {
		          		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'please enter date.');
		                apexpages.addmessage(msg);
		                return null;
		          } 
		    }
		         
		    if(selectedValue == 'Social')
		    {
		          if(socialgrpEnd == 'Yes' && dateGrp3.Actual_Submission_Date__c == null)
		          {
		                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'please enter date.');
		                apexpages.addmessage(msg);
		                return null;
		          } 
		    }    
      
      		showPopup = true;
      
      		// Send the email you have created.
      		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       
       		/** pagereference pageRef = new pagereference('/home/home.jsp');
      		return pageRef.setRedirect(true); **/ 
      		pageRef= new pagereference('/apex/ChatterFormSubConfirmation');
       		return pageRef.setRedirect(true);
   }
   catch (System.EmailException E) {
   		apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please Fill the mandatory fields');
    	apexpages.addmessage(msg);
    	//pageRef=ApexPages.currentPage();
    	return null;
    }
	} 

  	public List<SelectOption> getPickValues()
    {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('Select...','Select...'));
      options.add(new SelectOption('Business','Business'));
      options.add(new SelectOption('Event','Event'));
      options.add(new SelectOption('Social','Social'));
      return options;
    }
 }
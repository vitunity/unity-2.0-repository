/**********************************************************************************************************
Author: Divya Hargunani
Created Date: 18-Sep-2017
Project/Story/Inc/Task : STRY0032751 : Modify Chatter Group Request form
Description: 
This is a controller for NewChatterRequest Visual Force Page, This controller simulates the logic when user request for chatter
********************************************************************************************************/

public class ChatterRequestFormCtrl {
    
    public String grp {get;set;}
    public String groupType {get;set;}
    public String requestedGroupName {get;set;}
    public String groupDescription {get;set;}
    public string requestor {get;set;}
    public string eventgrpEnd {get;set;}
    public Input_Clearance__c dateGrp {get;set;}
    public boolean isChatterRequestSubmitted {get;set;}
    
    public ChatterRequestFormCtrl(){
        grp = 'Public';
        groupType = 'Business';
        eventgrpEnd  = 'No';
        dateGrp  = new Input_Clearance__c();
        isChatterRequestSubmitted = false;
        User u = [SELECT FirstName,LastName,Phone FROM User WHERE Id =:UserInfo.getUserId()];
        requestor = u.LastName;
    }
 
    //Method to retrive all the available group types
    public List<SelectOption> getGroupTypes() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('Select...','Select...'));
        options.add(new SelectOption('Business','Business'));
        options.add(new SelectOption('Event','Event'));
        options.add(new SelectOption('Social','Social'));
        return options;
    }
    
    //Method to retrive all the available values for end date
    public List<SelectOption> getgroupEnd () {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes','Yes'));
        options.add(new SelectOption('No','No'));
        return options;
    }
    
    //Method to submit the chatter group request
    public PageReference submitChatterRequest(){
        // First, reserve email capacity for the current Apex transaction to ensure that we won't exceed our daily email limits when sending email after the current transaction is committed.
        pagereference pageRef;
        try{
            Messaging.reserveSingleEmailCapacity(2);
            //create a new single email message object that will send out a single email to the addresses in the To, CC & BCC list.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            // Strings to hold the email addresses to which you are sending the email.
            String[] toAddresses = new String[] {'unityhelp@varian.com'}; 
            //String[] ccAddresses = new String[] {'divya.hargunani@varian.com'};
            
            // Assign the addresses for the To and CC lists to the mail object.
            mail.setToAddresses(toAddresses);
            //mail.setCcAddresses(ccAddresses);
            
            // Specify the address used when the recipients reply to the email. 
            mail.setReplyTo('unity.helpdesk@varian.com');
            
            // Specify the name used as the display name.
            mail.setSenderDisplayName('{$User.name}');
            
            // Specify the subject line for your email address.
            mail.setSubject('Unity: Chatter Group Request : '+ requestedGroupName );
            
            // Set to True if you want to BCC yourself on the email.
            mail.setBccSender(false);
            
            // Optionally append the salesforce.com email signature to the email. The email address of the user executing the Apex Code will be used.
            mail.setUseSignature(false);
            
            if(requestedGroupName == ''|| groupDescription == ''){
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please fill in mandatory fields');
                apexpages.addmessage(msg);
                return null;
            }
            
            if (groupType == 'Event' && eventgrpEnd == 'Yes' && dateGrp.Actual_Submission_Date__c == null) {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info, 'please enter date');
                apexpages.addmessage(msg);
                return null;
            }
            
            String body = '';
            
            body += 'Requestor\t\t: ' + requestor + '\n Is this group public or private?\t: ' + grp +
                '\n Type of group\t\t: ' + groupType + '\n Requested Group Name\t\t: ' +
                requestedGroupName + '\n Group Description\t: ' +
                groupDescription;
            
            
                            
            if (groupType == 'Event') {
                body += '\n Does this Group have an end date\t:' + eventgrpEnd ;
                if(eventgrpEnd  == 'Yes'){
                 String formattedDate = dateGrp.Actual_Submission_Date__c.format();
                 body += '\n If yes, when\t:' +formattedDate;
                 }
            }
            mail.setPlainTextBody(body);

            String htmlBody = '';
            
            htmlBody+= '<b>Requestor: ' + requestor + '<br><b>Is this group public or private?:' + grp +
                '<br><b>Type of group:<b> ' + groupType + '<br> <b>Requested Group Name:' + 
                requestedGroupName + '<br> <b>Group Description:' + 
                groupDescription;
                
            if (groupType == 'Event') {
                htmlBody+= '<br><b>Does this Group have an end date:' + eventgrpEnd;
                if(eventgrpEnd  == 'Yes'){
                String formattedDate = dateGrp.Actual_Submission_Date__c.format();
                htmlBody+= '<br> <b>  If yes, when:'+formattedDate;
            }
            }
            
            mail.setHtmlBody(htmlBody);

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });            
            isChatterRequestSubmitted = true;
         }
         
         catch (System.EmailException E) {
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,'Please Fill the mandatory fields');
                apexpages.addmessage(msg);
                return null;
            }
            
        return null;
    }
    
    //Method to return user to home page after clicking home button on request submission success page
    public PageReference homepage() {
        PageReference pageRef= new pagereference('/home/home.jsp');
        return pageRef.setRedirect(true);
    }
    
}
/**********************************************************************************************************
Author: Divya Hargunani
Created Date: 22-Sep-2017
Project/Story/Inc/Task : STRY0032751 : Modify Chatter Group Request form
Description: 
This is a test class for ChatterRequestFormCtrl Apex Class
********************************************************************************************************/

@isTest
public class ChatterRequestFormCtrlTest {
    
    //Test method to validate the blank group name and blank group description
    @isTest
    public static void testGroupWithBlankNameAndDesc() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.requestedGroupName = '';
        chatter.groupDescription = '';
        chatter.submitChatterRequest();
        test.stopTest();
    }
    
    //Test method to validate the chatter request for business group
    @isTest
    public static void testBusinessGroup() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.getgroupEnd();
        chatter.getGroupTypes();
        chatter.homepage();
        chatter.requestedGroupName = 'Business Test Group';
        chatter.groupDescription = 'This is a business test group';
        chatter.submitChatterRequest();
        test.stopTest();
    }
    
    //Test method to validate the chatter request for social group
    @isTest
    public static void testSocialGroup() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.groupType = 'Social';
        chatter.requestedGroupName = 'Social Test Group';
        chatter.groupDescription = 'This is a Social test group';
        chatter.submitChatterRequest();
        test.stopTest();
    }
    
    //Test method to validate the chatter request for event group with no end date
    @isTest
    public static void testEventGroupWithoutEndDate() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.groupType = 'Event';
        chatter.requestedGroupName = 'Event Test Group without end date';
        chatter.groupDescription = 'This is a Event test group without end date';
        chatter.submitChatterRequest();
        test.stopTest();
    }
    
    //Test method to validate the chatter request for event group with blank end date
    @isTest
    public static void testEventGroupWithBlankEndDate() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.groupType = 'Event';
        chatter.requestedGroupName = 'Event Test Group with blank end date';
        chatter.eventgrpEnd = 'Yes';
        chatter.groupDescription = 'This is a Event test group with blank end date';
        chatter.submitChatterRequest();
        test.stopTest();
    }
    
    //Test method to validate the chatter request for event group with valid end date
    @isTest
    public static void testEventGroupWithValidEndDate() {
        test.startTest();
        ChatterRequestFormCtrl chatter = new ChatterRequestFormCtrl();
        chatter.groupType = 'Event';
        chatter.requestedGroupName = 'Event Test Group with valid end date';
        chatter.eventgrpEnd = 'Yes';
        chatter.dateGrp.Actual_Submission_Date__c = date.today();
        chatter.groupDescription = 'This is a Event test group with valid end date';
        chatter.submitChatterRequest();
        test.stopTest();
    }

}
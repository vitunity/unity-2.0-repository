/*************************************************************************\
    @ Author        : Ani Sinnarkar
    @ Create Date   : 14-Oct-2013
    @ Description   : This is test class for ChatterRequest class which is developed for ChatterGroup
                      request functionality.                      
                      
    @ Last Modified By  : 25-Oct-2013
    @ Updated to bump up class coverage %  
****************************************************************************/

@isTest
private class ChatterRequestTest {
    
   
      static testMethod void testChatterexception() {
          
        System.test.startTest() ;  
        ChatterRequest crController = new ChatterRequest();     
        List<SelectOption> sel = crController.getPickValues();
        crController.grpEnd = 'No' ;
        Boolean val = crController.getVal();
        crController.setMhide(true);
        Boolean hide1 = crController.getMhide();
        crController.setM1hide(true);
        Boolean hide2 = crController.getM1hide();
        crController.setM2hide(true);
        Boolean hide3 = crController.getM2hide();
        crController.selectedValue = 'Business' ;
        crController.BusinessType = 'Select...';
        crController.doSendEmail() ;
        crController.selectedValue = 'Event' ;  
        crController.eventrequestgrpName = '' ;
        crController.doSendEmail() ;
        crController.selectedValue = 'Social' ;
        crController.socialrequestgrpName= '' ;
        crController.doSendEmail() ;
        crController.selectedValue = '' ;
        crController.grpTypeMethod();
        crController.doSendEmail() ;
        System.Test.stopTest();
      }   
      
       static testMethod void testChatterexceptioncase2(){
       
            ChatterRequest crController = new ChatterRequest();  
            crController.selectedValue = 'Business' ;
            crController.grpEnd = 'Yes' ;
            Input_Clearance__c dateGrp1  = new Input_Clearance__c();
            //crController.dateGrp1.Actual_Submission_Date__c = null ; 
            crController.doSendEmail() ;
            
            crController.selectedValue = 'Event' ;
            crController.eventgrpEnd= 'Yes' ;
            crController.dateGrp2.Actual_Submission_Date__c = null ; 
            crController.doSendEmail() ;
            
            crController.selectedValue = 'Social' ;
            crController.socialgrpEnd = 'Yes' ;
            crController.dateGrp3.Actual_Submission_Date__c = null ; 
            crController.doSendEmail() ;
      }
      
     static testMethod void testChatter() {
            
            System.test.startTest() ;
            
            ChatterRequest crController = new ChatterRequest();
            crController.grp = 'Public' ;
            crController.selectedValue = 'Business' ;
            crController.BusinessType = 'VMS' ;
            crController.Department = 'ALL' ;
            crController.SuperRegion = 'AMS' ;
            crController.requestgrpName= 'Test Group' ;
            crController.grpPurpose = 'Test Group Desc';
            crController.grpEnd = 'Yes' ;
            crController.dateGrp1.Actual_Submission_Date__c = date.today() ; 
            crController.grpTypeMethod();
            crController.doSendEmail() ;

            // This is to test Public group and Group Type Event
            ChatterRequest crController1 = new ChatterRequest();
            crController1.grp = 'Public' ;
            crController1.selectedValue = 'Event' ;           
            crController1.eventrequestgrpName = 'Test Group Desc' ;
            crController1.eventgrpPurpose = 'Test Group' ;
            crController1.eventgrpEnd = 'Yes' ;
            crController1.dateGrp2.Actual_Submission_Date__c = date.today() ; 
            crController1.grpTypeMethod();
            crController1.doSendEmail() ;


            // This is to test Public group and Group Type Social
            ChatterRequest crController2 = new ChatterRequest();
            crController2.grp = 'Public' ;
            crController2.selectedValue = 'Social' ;
            crController2.socialrequestgrpName= 'Test Group' ;
            crController2.socialgrpPurpose = 'Test Group Desc' ;
            crController2.socialgrpEnd = 'Yes' ;
            crController2.dateGrp3.Actual_Submission_Date__c = date.today() ; 
            crController2.grpTypeMethod();
            crController2.doSendEmail() ;

                        
            //System.assertEquals('/apex/Chatter_form_submission_Confirmation', nextPage);
            System.Test.stopTest(); 
    }
}
/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 13-May-2013
    @ Description   : This Class is controller for visual force page to generate PDF of the approved check requests to help in printing if required.
    				  As this contoller is just to diplay VF page and no user interacion as such, so all functionality is achievable in Constructor only.
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/
public class CheckRequestForm 
{
        public Check_Request_Form__c checkRequest {get; set;} // the Check RequestForm record from where the print request initiated
        public List<Expenditure_Information__c> listExpenditure {get; set;} // list of child expenditure records
        public List<Participant_Information__c> listParticipants {get; set;} // list of participants records ... used on Health Care Provider form only
        public List<RequestApprover> RequestApproverList {get; set;} // list of approvers (people who approved the request via approval process)
        public String strDocumentId {get; set;} // varianLogo DocumentId uploaded in 'Documents'
        
        public CheckRequestForm(ApexPages.StandardController controller)
        {
            checkRequest = (Check_Request_Form__c) controller.getRecord(); // get the record with record Id
            // fetch all fields need to be displayed ont the form and also fetch list of approval history records (procesinstance) 
            checkRequest = [SELECT Id, Name, CreatedBy.Name, Account_Number__c, Account_Holder__c, Bank_Address_1__c, Bank_Address_2__c, Bank_City__c, Bank_Name__c, Bank_State__c, Bank_Zip_Code__c, Check_or_Electronic_Payment__c, Company_Name__c, Company_Name__r.Name, Description_of_Expenditure__c, Email_Address_Individual__c, Expenditure_Prescribed_Product_Name_3__c, Expenditure_Prescribed_Product_Name_2__c, Expenditure_Prescribed_Product_Name_5__c, Expenditure_Prescribed_Product_Name_4__c, Expenditure_Prescribed_Product_Name_1__c, Federal_ID_State_ID__c, Federal_Tax_ID__c, First_Name__c, HCP_or_HCO_Payment__c, If_Other_explain_Expenditure__c, If_Other_explain_License_Organization__c, If_Other_explain_License_individual__c, If_Other_explain_product1__c, If_Other_explain_product2__c, If_Other_explain_product3__c, If_Other_explain_product4__c, If_Other_explain_product5__c, Last_Name__c, Lic_State_ID__c, License_Type_Individual__c, License_Type_Organization__c, Middle_Name__c, Payment_Address_1__c, Payment_Address_2__c, Payment_Address_City__c, Payment_Address_State__c, Payment_Address_Zipcode__c, PAY_To__c, PAY_To__r.Name, Recipient_Company_Name__c, Recipient_Tax_ID__c, Recipient_title__c, Routing_SWIFT_Code__c, Status__c, Status_F__c, Submited_Date__c, Submitter_Department_Name__c, Suffix__c, Total_Amount__c, Total_Participant_Amount__c, (SELECT Id, TargetObjectId, Status, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp FROM ProcessInstances WHERE Status = 'Approved') FROM Check_Request_Form__c WHERE Id = :checkRequest.Id];
            listExpenditure = [SELECT ID, Check_Request_Form__c, Description__c, Account_Number__c, Cost_Center__c, JO_or_SO__c, Amount__c, Number_of_Participants__c, Cost_Per_Participant__c FROM Expenditure_Information__c where Check_Request_Form__c = :checkRequest.Id];
            listParticipants = [SELECT ID, Check_Request_Form__c, Participant_First_Name__c, Participant_Last_Name__c, License_Type__c, License_Number__c, E_Mail__c FROM Participant_Information__c WHERE Check_Request_Form__c = :checkRequest.Id];
            // list of approval history records
            List<ProcessInstance> objList = checkRequest.ProcessInstances;
            System.debug('APPROVER LIST === '+objList);
            Set<Id> userIds = new Set<Id>();

            for(ProcessInstance pi: objList)
            {
            	//prepare list of user ids of the people who approved the request
            	userIds.add(pi.LastModifiedById);
            }
            Map<Id, User> userMap = new Map<Id, User>([Select Id, Name from User where Id IN :userIds]);
            RequestApproverList = new List<RequestApprover>();
            for(ProcessInstance pi: objList)
            {
                String approverName = userMap.get(pi.LastModifiedById).Name; // user who approved the request
                String approveDate = pi.SystemModstamp.format('MM/dd/yyyy'); // format for display
                RequestApproverList.add(new RequestApprover(approverName, approveDate));
            }
	        Map<String, KeyValueMap_CS__c> keyValueMap = KeyValueMap_CS__c.getAll();
	        strDocumentId  = keyValueMap.get('VarianLogoDocumentId').Data_Value__c;
        }
        
        // default constructor
        public CheckRequestForm()
        {
            Id checkRequestId = Apexpages.currentPage().getParameters().get('checkRequestId'); // get the record with record Id from query parameter
            // fetch all fields need to be displayed ont the form and also fetch list of approval history records (procesinstance)
            checkRequest = [SELECT Id, Name, CreatedBy.Name, Account_Number__c, Account_Holder__c, Bank_Address_1__c, Bank_Address_2__c, Bank_City__c, Bank_Name__c, Bank_State__c, Bank_Zip_Code__c, Check_or_Electronic_Payment__c, Company_Name__c, Company_Name__r.Name, Description_of_Expenditure__c, Email_Address_Individual__c, Expenditure_Prescribed_Product_Name_3__c, Expenditure_Prescribed_Product_Name_2__c, Expenditure_Prescribed_Product_Name_5__c, Expenditure_Prescribed_Product_Name_4__c, Expenditure_Prescribed_Product_Name_1__c, Federal_ID_State_ID__c, Federal_Tax_ID__c, First_Name__c, HCP_or_HCO_Payment__c, If_Other_explain_Expenditure__c, If_Other_explain_License_Organization__c, If_Other_explain_License_individual__c, If_Other_explain_product1__c, If_Other_explain_product2__c, If_Other_explain_product3__c, If_Other_explain_product4__c, If_Other_explain_product5__c, Last_Name__c, Lic_State_ID__c, License_Type_Individual__c, License_Type_Organization__c, Middle_Name__c, Payment_Address_1__c, Payment_Address_2__c, Payment_Address_City__c, Payment_Address_State__c, Payment_Address_Zipcode__c, PAY_To__c, PAY_To__r.Name, Recipient_Company_Name__c, Recipient_Tax_ID__c, Recipient_title__c, Routing_SWIFT_Code__c, Status__c, Status_F__c, Submited_Date__c, Submitter_Department_Name__c, Suffix__c, Total_Amount__c, Total_Participant_Amount__c, (SELECT Id, TargetObjectId, Status, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp FROM ProcessInstances WHERE Status = 'Approved') FROM Check_Request_Form__c WHERE Id = :checkRequestId];
            listExpenditure = [SELECT ID, Check_Request_Form__c, Description__c, Account_Number__c, Cost_Center__c, JO_or_SO__c, Amount__c, Number_of_Participants__c, Cost_Per_Participant__c FROM Expenditure_Information__c where Check_Request_Form__c = :checkRequest.Id];
            listParticipants = [SELECT ID, Check_Request_Form__c, Participant_First_Name__c, Participant_Last_Name__c, License_Type__c, License_Number__c, E_Mail__c FROM Participant_Information__c WHERE Check_Request_Form__c = :checkRequest.Id];
            // list of approval history records
            List<ProcessInstance> objList = checkRequest.ProcessInstances;
            System.debug('APPROVER LIST === '+objList);
            Set<Id> userIds = new Set<Id>();
            for(ProcessInstance pi: objList)
            {
            	//prepare list of user ids of the people who approved the request
            	userIds.add(pi.LastModifiedById);
            }
            Map<Id, User> userMap = new Map<Id, User>([Select Id, Name from User where Id IN :userIds]);
            RequestApproverList = new List<RequestApprover>();
            for(ProcessInstance pi: objList)
            {
                String approverName = userMap.get(pi.LastModifiedById).Name;// user who approved the request
                String approveDate = pi.SystemModstamp.format('MM/dd/yyyy');// format for display
                RequestApproverList.add(new RequestApprover(approverName, approveDate));
            }
	        Map<String, KeyValueMap_CS__c> keyValueMap = KeyValueMap_CS__c.getAll();
	        strDocumentId  = keyValueMap.get('VarianLogoDocumentId').Data_Value__c;
        }
        
        // This internal wrapper class is to hold Approver data for display 
        public class RequestApprover
        {
            public String approvedby {get; set;} // name of person who approved teh request
            public String approvedDate {get; set;} // date on which request was approved

			// default constructor            
            public RequestApprover()
            {
            }

			// new constructor for initiation with parameters
            public RequestApprover(String strApprovedBy, String strApprovedDate)
            {
            	approvedby = strApprovedBy;
                approvedDate = strApprovedDate;
            }
        }
}
/**
 * This class contains unit tests for validating the behavior of Apex classe 'CheckRequestForm'

 */
@isTest
private class CheckRequestFormTest {

    static testMethod void myUnitTest() 
    {
        //create check request form record
        Check_Request_Form__c crf = new Check_Request_Form__c();
        crf.Check_or_Electronic_Payment__c = 'Check';
        crf.Payment_Address_1__c = '3333 W 2nd Street'; // if Check_or_Electronic_Payment__c = 'Check' then Payment Address field is required.
        
        insert crf;
        
        Expenditure_Information__c expInfo = new Expenditure_Information__c();
        expInfo.Check_Request_Form__c = crf.Id;
        expInfo.Description__c = 'test description';
        expInfo.Account_Number__c = 'ACNUMBER567';
        expInfo.Cost_Center__c = 'CC787';
        expInfo.Amount__c = 10000;
        expInfo.Number_of_Participants__c = 5;
        
        insert expInfo;
        
        Participant_Information__c parInfo = new Participant_Information__c();
        parInfo.Check_Request_Form__c = crf.Id; 
        parInfo.Participant_First_Name__c = 'Mack';
        parInfo.Participant_Last_Name__c = 'Mohan';
        parInfo.License_Type__c = 'Not known';
        parInfo.License_Number__c = 'LCNO5676657';
        parInfo.E_Mail__c = 'dummy@dummy.gmail.com';
        
        insert parInfo;
        
        KeyValueMap_CS__c kv = new KeyValueMap_CS__c();
        kv.Name = 'VarianLogoDocumentId';
        kv.Data_Value__c = '687686788778';
        insert kv;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(crf);
        PageReference pageRef = Page.CheckRequestFormPrintHCP;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('checkRequestId', crf.Id);
        CheckRequestForm objCRF = new CheckRequestForm();
        objCRF = new CheckRequestForm(sc);
        CheckRequestForm.RequestApprover ra = new CheckRequestForm.RequestApprover('Mack Mohan', '01-01-2014');
        Test.stopTest();        
    }
}
public Without sharing class ChowLineItemUpdatehelper {
    public ChowLineItemUpdatehelper(){
        
    }    
 @AuraEnabled 
    public static Chow_Tool__c sendEmail(id chowId) {
        List<Chow_Tool__c> chowList =[select id,name,Request_Status__c,Active_Task_Assignee__c,Assignment_Date__c,Assignee_Email__c,Add_Extra_Participant__c,Chronological_Comments_Archive__c,
                                      Comments__c,LastModifiedDate from Chow_Tool__c where id=:chowId];
       
        string commentsBody='';
        string ActiveTaskAssignee='';
        string ChronologicalCommentsArchive='';
        string comments='';
        List<string> ccSId = new List<string>();
        List<String> emailsTo=new List<String>();
        List<String> ccmail = new List<string>();
        String loginUser = UserInfo.getName();
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        
        if(chowList.size()>0){
            for(Chow_Tool__c chow : chowList){ 
                if(chow.Assignee_Email__c !=null){
                    emailsTo.add(chow.Assignee_Email__c);
                }
                if(chow.Active_Task_Assignee__c !=null){
                    ActiveTaskAssignee= chow.Active_Task_Assignee__c;
                }                
                if(chow.Comments__c !=null){
                    comments=chow.Comments__c.replaceAll('\n','<br/>');
                }
                if(chow.Chronological_Comments_Archive__c !=null){
                    commentsBody = '<html><body>'+chow.Chronological_Comments_Archive__c +'By: ' + loginUser +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ chow.LastModifiedDate+ '<table width="100%" border="1" style="border-collapse: collapse"><td>'+ comments+'</td></table></body></html>';
                    
                }else{
                    commentsBody = '<html><body>By: ' + loginUser +'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ chow.LastModifiedDate+ '<table width="100%" border="1" style="border-collapse: collapse"><td>'+ comments+'</td></table></body></html>';
                }
                if(chow.Comments__c !=null){
                    chow.Chronological_Comments_Archive__c=commentsBody;
                }               
                if(chow.Add_Extra_Participant__c !=null){
                    ccSId.addall(chow.Add_Extra_Participant__c.split(',')); 
                }                              				                
                chow.Assignment_Date__c=system.today();
                chow.AssignmentDateTest__c =system.now();
            }  
            
            Map<id,Chow_Team_Member__c> extraParticipant = new Map<id,Chow_Team_Member__c>([select id,name,Team_Member_Email__c from Chow_Team_Member__c where Team_Member_Name__r.Name in:ccSId]);
            
            ////List<user> sys_adm = [SELECT id, Email FROM User WHERE Name=:'Rishabh Verma'];
            List<user> sys_adm = [SELECT Id, Email FROM User WHERE Profile.Name = 'System Administrator' 
                    AND isActive = true LIMIT 1];
            
            EmailTemplate assignTemplate=[SELECT Description, Body, Id, Name, BrandTemplateId, Markup, NamespacePrefix, TemplateStyle, Subject,
                                          TemplateType FROM EmailTemplate where name=:'ChowToolNewAssignment'];
            EmailTemplate completeTemplate=[SELECT Description, Body, Id, Name, BrandTemplateId, Markup, NamespacePrefix, TemplateStyle, Subject,
                                            TemplateType FROM EmailTemplate where name=:'ChowToolComplete'];
            
            set<Id>contentId=new set<Id>(); 
            List<ContentDocumentLink> contentDoc =[SELECT ContentDocument.id,ContentDocumentId,ContentDocument.title FROM ContentDocumentLink WHERE LinkedEntityId =:chowId];
           
            for(ContentDocumentLink cdLnk : contentDoc){
                contentId.add(cdLnk.ContentDocumentId);
            }
            List<ContentVersion> lstContentversion =[SELECT Title,VersionData,FileType FROM ContentVersion WHERE ContentDocumentId =:contentId];
            system.debug('#### debug lstContentversion = ' + lstContentversion);

            Map<string, string> mimeTypeMap = new Map<string, string>();
            mimeTypeMap.put('POWER_POINT_X', 'application/vnd.openxmlformats-officedocument.presentationml.presentation');
            mimeTypeMap.put('POWER_POINT', 'application/vnd.ms-powerpoint');
            mimeTypeMap.put('xlsx', 'application/vnd.ms-excel');
            mimeTypeMap.put('PDF', 'application/pdf');
            mimeTypeMap.put('MSWORD', 'application/msword');
            mimeTypeMap.put('JPEG', 'image/jpeg');
            mimeTypeMap.put('PNG', 'image/png');
			mimeTypeMap.put('MSWORD', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            
            if(!extraParticipant.isEmpty()){
                for(Chow_Team_Member__c ce : extraParticipant.values()){                            
                    ccmail.add(ce.Team_Member_Email__c);                            
                } 
            }
            
            if(chowList.size() > 0){
                for(Chow_Tool__c chow : chowList){ 
                    if(chow.Request_Status__c =='Order Amendment Submitted for SAP Modification' ||
                       chow.Request_Status__c =='Completed-Updated in SAP'||
                       chow.Request_Status__c =='Denied'|| chow.Request_Status__c =='Duplicate'){
                           Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                           message.setToAddresses(emailsTo);
                           message.setCcAddresses(ccmail);
                           message.setTargetObjectId(sys_adm[0].id);
                           message.setTreatTargetObjectAsRecipient(false);
                           message.setWhatId(chow.Id);
                           message.setSaveAsActivity(false);
                           message.setTemplateId(completeTemplate.Id);
                           message.setReplyTo('noreply@salesforce.com');
                           //                             
                           
                           for(ContentVersion  cVersion : lstContentversion){
                               Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment(); 
                               efa.setFileName(cVersion.Title+'.'+cVersion.FileType);
                               efa.setBody(cVersion.VersionData);
                               efa.setContentType(mimeTypeMap.get(cVersion.FileType));
                               fileAttachments.add(efa);
                           }
                           message.setFileAttachments(fileAttachments);
                           Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                               Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                           
                           if (results[0].success) 
                           {
                               System.debug('The email was sent successfully.');
                           } else 
                           {
                               System.debug('The email failed to send: ' + results[0].errors[0].message);
                           }         
                       }else{
                           
                           Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                           message.setToAddresses(emailsTo);
                           message.setCcAddresses(ccmail);
                           message.setTargetObjectId(sys_adm[0].id);
                           message.setTreatTargetObjectAsRecipient(false);
                           message.setWhatId(chow.Id);
                           message.setSaveAsActivity(false);
                           message.setTemplateId(assignTemplate.Id);
                           message.setReplyTo('noreply@salesforce.com');
                           //                               
                           for(ContentVersion  cVersion : lstContentversion){
                               Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment(); 
                               efa.setFileName(cVersion.Title+'.'+cVersion.FileType);
                               efa.setBody(cVersion.VersionData);
                               efa.setContentType(mimeTypeMap.get(cVersion.FileType));
                               fileAttachments.add(efa);
                           }

                           system.debug('#### debug fileAttachments = ' + fileAttachments);
                           message.setFileAttachments(fileAttachments);
                           Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                               Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                           
                           
                           if (results[0].success) 
                           {
                               System.debug('The email was sent successfully.');
                           } else 
                           {
                               System.debug('The email failed to send: ' + results[0].errors[0].message);
                           }      
                             
                       }
                    chow.Comments__c=''; 
                }           
                
            }
            
            
        }
        update chowList;
        
        return null;
    }

    @AuraEnabled
    public static Chow_Tool__c getChowInfo(String chowId) {
        system.debug('#### chowId = ' + chowId);
        list<Chow_Tool__c> list1 = [SELECT Id, Assignee_Email__c FROM Chow_Tool__c 
              WHERE Id = :chowId Limit 1];
        system.debug('#### list1 = ' + list1[0].Assignee_Email__c);
        return list1[0];
    }
   
}
@IsTest
public class ChowLineItemUpdatehelperTest {
    public static testMethod void ChowLineItemUpdate(){       
     
       user u =testUtils.createUser();
       
        Chow_Team_Member__c chowTeamMember = testUtils.createChow_Team_Member('Accounting - Credit and Collections',u.id, 'NA');
        insert chowTeamMember;
        
        Chow_Tool__c chow =testUtils.createChOwTool('Accounting - Credit and Collections', 'Bruce Wayne', 'bruce.wayne1c@zynga.com');
        chow.Sales_Order_Quote_Number_Unknown__c=true;
        chow.Chronological_Comments_Archive__c='Test Chronological Coments';
        chow.Comments__c='Test Comments';

        chow.Add_Extra_Participant__c='Bruce Wayne';
        chow.Request_Status__c='Denied';
        insert chow;
        
        
        
        List<Chow_line_item__c> itemlst= new List<Chow_line_item__c>();
        string recordtypeDel =Schema.SObjectType.Chow_line_item__c.getRecordTypeInfosByName().get('Delete').getRecordTypeId();
        string recordtypeAdd =Schema.SObjectType.Chow_line_item__c.getRecordTypeInfosByName().get('Add').getRecordTypeId();
        
        Chow_line_item__c chowItem =testUtils.createChowLineItem('itemName', 2, 'Testdescription');
        chowItem.Chow_Tool__c=chow.id;
        chowItem.RecordTypeId=recordtypeDel;
        itemlst.add(chowItem);
        
        Chow_line_item__c chowItem1 =testUtils.createChowLineItem('itemName-01', 3, 'Testdescriptions');
        chowItem1.Chow_Tool__c=chow.id;
        chowItem1.RecordTypeId=recordtypeAdd;
        itemlst.add(chowItem1);
        insert itemlst;
        
          ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];        
        ContentVersion testContent = [SELECT id, ContentDocument.id,ContentDocument.title,ContentDocumentId FROM ContentVersion where Id = :contentVersion.Id];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = chow.id;
        cdl.ContentDocumentId = testcontent.ContentDocumentId;
        cdl.shareType = 'V';
        insert cdl;
       
        ChowLineItemUpdatehelper.sendEmail(chow.id);
        
    }
    public static testmethod void ChowLineItemUpdateNeg(){
        
        Chow_Tool__c chow =testUtils.createChOwTool('Accounting - Credit and Collections', 'Md Raheem', 'raheem.mohamed@varian.com');
        chow.Sales_Order_Quote_Number_Unknown__c=true;
        chow.Add_Extra_Participant__c='Bruce Wayne';
        insert chow;
        
         ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];        
        ContentVersion testContent = [SELECT id, ContentDocument.id,ContentDocument.title,ContentDocumentId FROM ContentVersion where Id = :contentVersion.Id];
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = chow.id;
        cdl.ContentDocumentId = testcontent.ContentDocumentId;
        cdl.shareType = 'V';
        insert cdl;
        
        ChowLineItemUpdatehelper.sendEmail(chow.id);
        ChowLineItemUpdatehelper.getChowInfo(chow.id);
    }
    public static testMethod void ChowLineItemsCtrl(){
        user u =testUtils.createUser();
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name='Test So';
        so.Site_Partner__c=acc.id;
        
        insert so;
        
		BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test';
        quote.Sales_Order__c=so.id;
        quote.Price_Group__c = 'Z1';
        quote.Quote_Type__c = 'Amendments';
        quote.BigMachines__Site__c =label.BigMachines_Site_Id;
      //  quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.BigMachines__Status__c='Approved';
        insert Quote;
        
        BigMachines__Quote_Product__c  BQP = new BigMachines__Quote_Product__c();
        BQP.BigMachines__Quote__c=Quote.id;
        BQP.BigMachines__Sales_Price__c=473;
        BQP.BigMachines__Quantity__c=1;
        BQP.BigMachines__Description__c='Test';
        BQP.Grp__c=1;
        BQP.Header__c=true;
        BQP.Name='FSC002100000';
        
        insert BQP;
        
        Chow_Tool__c chow =testUtils.createChOwTool('Accounting - Credit and Collections', 'Bruce Wayne', 'bruce.wayne1c@zynga.com');
        chow.Sales_Order_Quote_Number_Unknown__c=true;
        chow.Chronological_Comments_Archive__c='Test Chronological Coments';
        chow.Comments__c='Test Comments';
        chow.Add_Extra_Participant__c='Bruce Wayne';
        chow.Request_Status__c='Denied';
        insert chow;
        List<Chow_line_item__c> itemlst= new List<Chow_line_item__c>();
        string recordtypeDel =Schema.SObjectType.Chow_line_item__c.getRecordTypeInfosByName().get('Delete').getRecordTypeId();
        Chow_line_item__c chowItem1 =testUtils.createChowLineItem('itemName-01', 3, 'Testdescriptions');
        chowItem1.Chow_Tool__c=chow.id;
        chowItem1.Type__c='Delete';
        chowItem1.RecordTypeId=recordtypeDel;
       // itemlst.add(chowItem1);
       // insert itemlst;
        
        ChowLineItemsCtrl.getChowToolInfo(chow.id);
        ChowLineItemsCtrl.getQuoteInfo(Quote.id);
        ChowLineItemsCtrl.getPartDetails('FSC002100000', Quote.id);
        ChowLineItemsCtrl.initChowLineItem(chowItem1.id);
        ChowLineItemsCtrl.saveChowLineItemDelete(chow.id, Quote.id, BQP.id, chowItem1);
    }
}
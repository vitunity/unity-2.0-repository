public without sharing class ChowLineItemsCtrl {
    @AuraEnabled
    Public static Chow_Tool__c getChowToolInfo(string chowToolId){        
        Chow_Tool__c lstChOW =[select Id , name, Quote_Number__c
                                    from Chow_Tool__c where Id =:chowToolId];        
        return lstChOW;
    }

    @AuraEnabled
    Public static BigMachines__Quote_Product__c getPartDetails(string partNumber, string quoteId){        
        BigMachines__Quote_Product__c lstPart =[SELECT Id , Name , BigMachines__Description__c,
        							Grp__c, Header__c
                                    FROM BigMachines__Quote_Product__c WHERE Name = :partNumber
                                    AND BigMachines__Quote__c = :quoteId];   
        system.debug('lstPart!!'+lstPart);
        return lstPart;
    }

    @AuraEnabled
    Public static BigMachines__Quote__c getQuoteInfo(string quoteId){        
        BigMachines__Quote__c quote =[select Id , name 
        					from BigMachines__Quote__c where Id =:quoteId];        
        return quote;
    }    

    @AuraEnabled
    Public static Chow_line_item__c initChowLineItem(String chowToolId) {
        Chow_line_item__c chTool = new Chow_line_item__c();
        chTool.Chow_Tool__c = chowToolId;

        return chTool;
    }

    @AuraEnabled
    Public static Chow_line_item__c saveChowLineItemDelete(String chowToolId,
                                                           String quoteId,
                                                           String quoteProductId,
                                                           Chow_line_item__c chowLineRec)
    {
        system.debug('#### chowToolId = ' + chowToolId);
        system.debug('#### quoteId = ' + quoteId);
        system.debug('#### quoteProductId = ' + quoteProductId);
        system.debug('#### chowLineRec = ' + chowLineRec);

        chowLineRec.Quote_Number__c = quoteId;
        chowLineRec.Quote_part_number__c = quoteProductId;
        chowLineRec.Type__c = 'Delete';

        Id recTypeId = Schema.SObjectType.Chow_Line_Item__c.getRecordTypeInfosByName().get('Delete').getRecordTypeId();
        chowLineRec.RecordTypeId = recTypeId;
        insert chowLineRec;
        return chowLineRec;
    }
 
}
public without sharing class ChowToolCtrl {
    
    @AuraEnabled
    public static String saveChowTool(Chow_Tool__c chowRec,Id sitePartnerId, String sOrderId,
                                     String quoteId, String taskAssignee, String soldToActId, List<Chow_Team_Member__c> selectedvalues){
        try{
            string str ='';
            system.debug('chowRec==='+chowRec);
            system.debug('siteId----'+sitePartnerId);
            system.debug('selectedvalues---'+selectedvalues);
            system.debug('quoteId---'+quoteId);
            system.debug('soldToActId---'+soldToActId);
            system.debug('sOrderId---'+sOrderId);
            system.debug('taskAssignee---'+taskAssignee);

            Map<id,string> mapstr = new map<id,string>();
            for(Chow_Team_Member__c us : selectedvalues){
                mapstr.put(us.Team_Member_Name__c,us.Team_Member_Name__r.Name);
               str+=mapstr.get(us.Team_Member_Name__c)+',';
               
            }
            chowRec.Site_partner_End_User__c = sitePartnerId;
            if(str.endsWith(',')) {
                str = str.removeEnd(',');
            }
            chowRec.Add_Extra_Participant__c = str;
            chowRec.Sales_Order__c = sOrderId;
            chowRec.Quote_Number__c = quoteId;
            chowRec.Account__c = soldToActId;
            chowRec.Active_Task_Assignee__c = taskAssignee;
            insert chowRec;
            system.debug('chowRec++++++'+chowRec);
        }
        catch(Exception e){
            system.debug('error found -->' + e.getMessage());
        }
        system.debug('chowRec.Id = '+chowRec);
       return string.valueOf(chowRec.Id);
       
    }
        
    
     @AuraEnabled
    public static String saveChowToolEditRecord(Chow_Tool__c chowRec,Id sitePartnerId, String sOrderId,
                                     String quoteId, String taskAssignee, String soldToActId, List<Chow_Team_Member__c> selectedvalues,
                                     String PriorityReasonv,  String RequestStatusv, String PriorityLevelv, String TypeofChangev){
        try{
            string str ='';
            system.debug('chowRec==='+chowRec);
            system.debug('siteId----'+sitePartnerId);
            system.debug('selectedvalues---'+selectedvalues);

            system.debug('quoteId---'+quoteId);
            system.debug('soldToActId---'+soldToActId);
            system.debug('sOrderId---'+sOrderId);

            Map<id,string> mapstr = new map<id,string>();
            for(Chow_Team_Member__c us : selectedvalues){
                mapstr.put(us.Team_Member_Name__c,us.Team_Member_Name__r.Name);
               str+=mapstr.get(us.Team_Member_Name__c)+',';
               
            }
            chowRec.Site_partner_End_User__c = sitePartnerId;
            if(str.endsWith(',')) {
                str = str.removeEnd(',');
            }
            chowRec.Add_Extra_Participant__c = str;
            chowRec.Sales_Order__c = sOrderId;
            chowRec.Quote_Number__c = quoteId;
            chowRec.Account__c = soldToActId;
            chowRec.Priority_Reason__c = PriorityReasonv;
            chowRec.Request_Status__c = RequestStatusv;
            chowRec.Priority_Level__c = PriorityLevelv;
            chowRec.Category__c = TypeofChangev;
            ////chowRec.Active_Task_Assignee__c = taskAssignee;
            
            update chowRec;
            system.debug('chowRec++++++'+chowRec);
        }
        catch(Exception e){
            system.debug('error found -->' + e.getMessage());
        }
        system.debug('chowRec.Id = '+chowRec);
       return string.valueOf(chowRec.Id);
       
    }
    
    
    
    @AuraEnabled
    Public static Chow_Tool__c getchOwrecords(string chOWId){
        
        Chow_Tool__c lstChOW =[select id,name,Active_Task_Assignee__c,Active_Team__c,Chow_Team_Member__c,Active_Teams__c,
                                    Add_Extra_Participant__c,Assignment_Date__c,Category__c,Chronological_Comments_Archive__c,Comments__c,ERP_Site_Partner__c,
                                    ERP_Sold_To__c,Escalation_Date__c,Geo_Region__c,HIT_Numbers__c,Invoice_Number__c,KMAT__c,Meachine_S_N__c,New_POAT_SO__c,
                                    Priority_Level__c,Priority_Reason__c,Quote_Number__c,Reminder_Date__c,Request_Status__c,Sales_Order__c,
                                    Sales_Order_Quote_Number_Unknown__c,SAP_Line_Items__c,Site_Partner_City__c,Site_partner_End_User__c,Account__c,
                                    Site_partner_End_User__r.Name, Account__r.Name, Account__r.Ext_Cust_Id__c,
                                    New_Contract_Total__c, Contract_Value__c
                                    ///,RecordTypeId
                                    from Chow_Tool__c where id=:chOWId];
        
        return lstChOW;
    }
    // get picklist values
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        String strURL = String.valueof(ApexPages.currentPage());
        String strId = '';
        String check = '=a35';
        
        system.debug('strURL --->' + strURL);
        
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    
    @AuraEnabled 
    public static Account getAccount(string AccountName) {       
             return [select id,Name,BillingCity,BillingState,Region1__c from Account where Name =:AccountName];
      
    }

    @AuraEnabled
    public static Account getAccountFieldsInfo(String accId) {
        return [select id,Name,BillingCity,BillingState,Region1__c,
                    Ext_Cust_Id__c,ERP_Site_Partner_Code__c from Account where Id =:accId];
    }

    @AuraEnabled
    public static ERP_Partner__c getPartnerAddress(String partnerId) {        
           return [select id,Partner_Number__c, City__c, State_Province_Code__c, Region__c from ERP_Partner__c where Id =:partnerId];  
       
    }

    @AuraEnabled
    public static ERP_Partner__c getPartnerDetails(String partnerNo) {
        system.debug('partnerNo = ' + partnerNo);
        return [select id,Partner_Number__c, City__c, State_Province_Code__c, Region__c from ERP_Partner__c where Partner_Number__C =:partnerNo];
    }


    @AuraEnabled
    public static Chow_Team_Member__c getChowTeamMember(string teamMem){
        system.debug('teamMem'+teamMem);
           return [select id,Name,Active_Team__c,Team_Member_Name__r.Name from Chow_Team_Member__c where Name =:teamMem limit 1];
           
    }

    
    @AuraEnabled
    public static SalesOrderQuoteWrap getSalesOrderInfoWrap(string soId) {

        List<Sales_Order__c> listOrders = [SELECT Id, Name, ERP_Partner__r.City__c, 
                                            ERP_Partner__r.State_Province_Code__c, ERP_Partner__c,
                                            ERP_Partner__r.Name, Sold_To__r.Name, Sold_To__c, 
                                            ERP_Sold_To__C, ERP_Site_Partner__c,
                                            ERP_Partner__r.Partner_Number__c, Sold_To__r.ERP_Site_Partner_Code__c,
                                            Sold_To__r.Ext_Cust_Id__c, Ext_Quote_Number__c
                                            FROM Sales_Order__c WHERE Id = :soId];
        system.debug('#### listOrders = ' + listOrders);
        system.debug('#### soId = ' + soId);

        BigMachines__Quote__c quoteNeeded = new BigMachines__Quote__c();
        for(BigMachines__Quote__c qt : [select id, name, Quote_Type__c, BigMachines__Is_Primary__c, 
                BigMachines__Status__c, quote_status__c, BMI_Region__c, Quote_Region__c
                from BigMachines__Quote__c where Sales_Order__c = :soId
                and (BigMachines__Status__c like 'Approved%') 
                order by name desc]) {

            if(qt.Quote_Type__c == 'Amendments') {
                quoteNeeded = qt;
                break;
            } else if(qt.Quote_Type__c != 'Amendments' && qt.BigMachines__Is_Primary__c == true) {
                quoteNeeded = qt;
            }
        }

        //system.debug('#### listOrders[0] = ' + listOrders[0]);
        system.debug('#### quoteNeeded = ' + quoteNeeded.Id);

        //The following code is for OMNI Sales Order, where there is no Quote - SO relationship
        String extQtNo;
        SalesOrderQuoteWrap soWrap;
        system.debug('#### debug listOrders[0].Ext_Quote_Number__c = ' + listOrders[0].Ext_Quote_Number__c);

        if(quoteNeeded.Id == null) {
            soWrap = new SalesOrderQuoteWrap(listOrders[0], quoteNeeded, listOrders[0].Ext_Quote_Number__c);
        } else {
            soWrap = new SalesOrderQuoteWrap(listOrders[0], quoteNeeded, null);
        }

        //SalesOrderQuoteWrap soWrap = new SalesOrderQuoteWrap(listOrders[0], quoteNeeded);
        return soWrap;
    }

    @AuraEnabled
    public static Chow_Default_Team__c getDefaultTeam(String region) {
        Chow_Default_Team__c defTeam = Chow_Default_Team__c.getValues(region.toUpperCase());
        return defTeam;
    }

    @AuraEnabled
    public static SalesOrderQuoteWrap getQuoteInfoWrap(string quoteId) {
       
        Id OptyId = [SELECT BigMachines__Opportunity__c from BigMachines__Quote__c WHERE Id =:quoteId].BigMachines__Opportunity__c;
        
        BigMachines__Quote__c quoteNeeded = new BigMachines__Quote__c();
        Sales_Order__c sOrderNeeded = new Sales_Order__c();

        for(BigMachines__Quote__c qt : [select id, name, Quote_Type__c, BigMachines__Is_Primary__c, 
                BigMachines__Status__c, quote_status__c, BMI_Region__c, Quote_Region__c,
                Sales_Order__c
                from BigMachines__Quote__c where 
                BigMachines__Status__c like 'Approved%'
                and BigMachines__Opportunity__c = :OptyId
                order by name desc]) {

            if(qt.Quote_Type__c == 'Amendments') {
                quoteNeeded = qt;
                break;
            } else if(qt.Quote_Type__c != 'Amendments' && qt.BigMachines__Is_Primary__c == true) {
                quoteNeeded = qt;
            }
        }

        system.debug('#### quoteNeeded = ' + quoteNeeded);
        List<Sales_Order__c> listSOrderNeeded = [SELECT Id, ERP_Partner__r.City__c, ERP_Partner__r.State_Province_Code__c, Sold_To__c,
                            ERP_Partner__r.Partner_Number__c, ERP_Partner__c,Sold_To__r.Ext_Cust_Id__c,
                            ERP_Sold_To__c, ERP_Site_Partner__c, Sold_To__r.ERP_Site_Partner_Code__c,
                            ERP_Partner__r.Name, Sold_To__r.Name FROM Sales_Order__c
                            WHERE Id =:quoteNeeded.Sales_Order__c];

        system.debug('#### listSOrderNeeded = ' + listSOrderNeeded);
        
        if(listSOrderNeeded.size() > 0) {
            system.debug(' ==== ' + listSOrderNeeded[0].Sold_To__r.Name);
            system.debug(' ==== ' + listSOrderNeeded[0].Sold_To__r.Ext_Cust_Id__c);
            system.debug(' ==== ' + listSOrderNeeded[0].Sold_To__c);
            sOrderNeeded = listSOrderNeeded[0];
        }

        SalesOrderQuoteWrap qtWrap = new SalesOrderQuoteWrap(sOrderNeeded, quoteNeeded, null);
        return qtWrap;
    }


    public class SalesOrderQuoteWrap {
        @AuraEnabled String city { get; set; }
        @AuraEnabled String sitePartnerName { get; set; }
        @AuraEnabled String sitePartnerNo { get; set; }
        @AuraEnabled String accountName { get; set; }
        @AuraEnabled BigMachines__Quote__c quote { get; set; }
        @AuraEnabled Sales_Order__c sOrder { get; set; }
        @AuraEnabled String extQtNo { get; set; }
        @AuraEnabled String geoRegion { get; set; }

        String partnerCity;
        String partnerState;

        public SalesOrderQuoteWrap(Sales_Order__c so, BigMachines__Quote__c qt, String extQtNo) {
            partnerCity = so.ERP_Partner__r.City__c != null ? so.ERP_Partner__r.City__c : '';
            partnerState = so.ERP_Partner__r.State_Province_Code__c != null ? so.ERP_Partner__r.State_Province_Code__c : '';
            if(partnerCity != null && partnerState != null) {
                this.city = partnerCity + ' , ' + partnerState;
            } else {
                this.city = partnerCity + partnerState;
            }

            if(this.city.trim().endsWith(',')) {
                this.city = this.city.trim().removeEnd(',');
            }
            this.sitePartnerName = so.ERP_Partner__r.Name;
            this.sitePartnerNo = so.ERP_Partner__r.Partner_Number__c;
            this.accountName = so.Sold_To__r.Name;
            this.quote = qt;
            this.sOrder = so;
            this.extQtNo = extQtNo;
            this.geoRegion = getGeoRegionFromCusSet(qt.BMI_Region__c, qt.Quote_Region__c);
        }
    }
    
    private static String getGeoRegionFromCusSet(String bmiRegion, String qtRegion) {
        String geoRegion;
        Chow_Geo_Region__c geoSets = Chow_Geo_Region__c.getValues(bmiRegion);

        if(geoSets != null) {
           geoRegion = geoSets.Geo_Region_Name__c;
        } else {
           geoRegion = qtRegion; 
        }

        if(geoRegion == null) {
            geoRegion = bmiRegion;
        }
        return geoRegion;
    }
    


    /* Get Active Team picklist values */
    /*
    @AuraEnabled
    Public static List<picklistEntry> getActiveTeamPicklistOptions(){

        List<picklistEntry> lstTeam = new List<picklistEntry>();
        Schema.Describefieldresult fieldResult = Chow_Team_Member__c.Active_Team__c.getDescribe();       
        
        for(Schema.Picklistentry picklistvalues: fieldResult.getPicklistValues()) {
            lstTeam.add(new picklistEntry(picklistvalues.getvalue(),picklistvalues.getlabel())); 
        }
        system.debug('##### debug lstTeam = ' + lstTeam);

        return lstTeam;
    }     
    */

    @AuraEnabled
    Public static List<picklistEntry> getActiveTeamPicklistOptions(String geoRegion){
		system.debug('##### debug geoRegion = ' + geoRegion);
        List<picklistEntry> lstTeams = new List<picklistEntry>();  
        //if(!String.isEmpty(geoRegion)) {
            list<String> list1 = new list<String>();  
        	String sql;
            if(geoRegion == null || geoRegion == '') {
                sql = 'select Active_Team__c from chow_team_member__c where Active_Team__c != null ORDER BY Active_Team__c ASC';
            } else {
                sql = 'select Active_Team__c from chow_team_member__c WHERE Geo_Region__c = :geoRegion and Active_Team__c != null ORDER BY Active_Team__c ASC';
            }
        
            for(chow_team_member__c team : Database.query(sql)) {
                if(!list1.contains(team.Active_Team__c)) {
                    lstTeams.add(new picklistEntry(team.Active_Team__c, team.Active_Team__c)); 
                    list1.add(team.Active_Team__c);
                } 
            }
        //}
        system.debug('##### debug lstTeams = ' + lstTeams);
        return lstTeams;
    }     

    /* Get Active Team picklist values */
    @AuraEnabled
    Public static List<picklistEntry> getTaskAssigneeList(String teamName){

        List<picklistEntry> lstAssignees = new List<picklistEntry>();  
        list<String> list1 = new list<String>();  
        for(chow_team_member__c team : [select Team_Member_Name__r.name from chow_team_member__c
                    WHERE Active_Team__c = :teamName ORDER BY Team_Member_Name__r.name ASC]) {
            if(!list1.contains(team.Team_Member_Name__r.name)) {
                lstAssignees.add(new picklistEntry(team.Team_Member_Name__r.name, team.Team_Member_Name__r.name)); 
                list1.add(team.Team_Member_Name__r.name);
            } 
        }
        system.debug('##### debug lstAssignees = ' + lstAssignees);
        return lstAssignees;
    }    

    public class picklistEntry implements Comparable{
        @AuraEnabled public string pvalue{get;set;}
        @AuraEnabled public string label{get;set;}
        
        public picklistEntry(string pvalue,string text){
            this.pvalue = pvalue;
            this.label = text;            
        }
        
        public Integer compareTo(Object compareTo) {
            picklistEntry compareToOppy = (picklistEntry)compareTo;
            Integer returnValue = 0;
            if (pvalue > compareToOppy.pvalue)
                returnValue = 1;
            else if (pvalue < compareToOppy.pvalue)
                returnValue = -1;        
            return returnValue;       
        }
    } 
  
}
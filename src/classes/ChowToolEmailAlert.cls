public class ChowToolEmailAlert {
    public static List<Chow_Tool__c> updateEmail(List<Chow_Tool__c> chowList) {
        set<string> sId = new set<string>();
        set<string> Email = new set<string>();
        system.debug('==chowList=='+chowList);	
        if(chowList.size()>0){
            for(Chow_Tool__c chow : chowList){
                sId.add(chow.Active_Task_Assignee__c);                
            }
            system.debug('==sId=='+sId);
            Map<id,Chow_Team_Member__c> lstUser = new Map<id,Chow_Team_Member__c>([select id,name,Team_Member_Email__c from Chow_Team_Member__c where Team_Member_Name__r.Name in:sId limit 1]);
            system.debug('==lstUser=='+lstUser ); 
           
                for(Chow_Tool__c chow: chowList){
                    If(!lstUser.isEmpty()){
                        chow.Assignee_Email__c = lstUser.values().Team_Member_Email__c;
                    }
                }
            }
        
        return chowList;
    }
    
}
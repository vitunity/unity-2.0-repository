public class ClearanceEvidenceRequirement{
    public static void updateAllClearance(List<Country_Rules__c> lstCRules, map<Id,Country_Rules__c> mapOldCountryRules){
        List<Country_Rules__c> lstCountryRules = new List<Country_Rules__c>();
        map<Id,string> mapCountry = new map<Id,string>();
        for(Country_Rules__c c: lstCRules){
            
            //insert
            if(mapOldCountryRules == null) lstCountryRules.add(c);
            //update
            if(mapOldCountryRules <> null && c.Clearance_Evidence_Requirements__c <> mapOldCountryRules.get(c.Id).Clearance_Evidence_Requirements__c){
                lstCountryRules.add(c);
            }
            if(c.Country__c <> null)mapCountry.put(c.Country__c,c.Clearance_Evidence_Requirements__c);
                          
        }
        
        List<Input_Clearance__c> lstClearance = new List<Input_Clearance__c>();
        for(Country__c c : [select Id,(select Id,Clearance_Evidence_Requirements__c from Market_Clearance_Evidence__r) from Country__c where ID IN: mapCountry.keyset()]){
            for(Input_Clearance__c IC: c.Market_Clearance_Evidence__r){
                IC.Clearance_Evidence_Requirements__c = mapCountry.get(c.Id);
                lstClearance.add(IC);
            }
        }
        if(lstClearance.size()>0){
            update lstClearance;
        }
        
        
    }
}
/**
@Summary:Used to validiate functionality of  methods within CEFUtility 
**/
@isTest
public class ClearanceEvidenceRequirementTEST{
public static Review_Products__c rp2;
public static Country__c cntry ;
public static Country_Rules__c cntyRule;
  public static void setupData(){
            Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';
        
        insert pSubItem;
        
        rp2 = new Review_Products__c();
        rp2.Product_Model__c = pSubitem.Id;
        rp2.Version_No__c = '12';
        rp2.Product_Profile_Name__c = pSubitem.Name;
        rp2.Clearance_Family__c = 'BrachyVision10';
        rp2.Business_Unit__c = 'VOS';
        rp2.Regulatory_Name__c = 'Regular NAme';
        rp2.Model_Part_Number__c='test1';
        rp2.Transition_Phase_Gate__c=system.today();
        rp2.National_Language_Support__c=system.today();
        rp2.Design_Output_Review__c=system.today();
        insert rp2;
        
        cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        
        insert cntry;
        
        cntyRule = new Country_Rules__c();
        cntyRule.Country__c=cntry.id;
        cntyRule.Weeks_Estimated_Review_Cycle__c=2;     
        cntyRule.Clearance_Evidence_Requirements__c = 'Not Applicable';
        cntyRule.Weeks_to_Prepare_Submission__c=1;
        
        insert cntyRule;
        
               
        
  }

  private static testmethod void updateMCMMarketClearanceForecastTest(){
    Test.startTest();
    setupData();
    List<Input_Clearance__c> lstInputCls = new List<Input_Clearance__c>();
    List<Blocked_Items__c> lstBlockItems = new List<Blocked_Items__c>();
    
    Input_Clearance__c  obj = new Input_Clearance__c();
    
    obj.Review_Product__c=rp2.id;
    obj.Country__c=cntry.id;
    obj.Milestone_Selection__c='Transition Phase Gate';
    obj.Clearance_Date_applies_to_Features__c='Yes';
    lstInputCls.add(obj);
    Input_Clearance__c  obj2 = new Input_Clearance__c();
    
    obj2.Review_Product__c=rp2.id;
    obj2.Country__c=cntry.id;
    obj2.Milestone_Selection__c='National Language Support';
    obj2.Clearance_Date_applies_to_Features__c='No';
    lstInputCls.add(obj2);
    Input_Clearance__c  obj3 = new Input_Clearance__c();
    
    obj3.Review_Product__c=rp2.id;
    obj3.Country__c=cntry.id;
    obj3.Milestone_Selection__c='Design Output Review';
    obj3.Clearance_Date_applies_to_Features__c='No';
    lstInputCls.add(obj3);
    insert lstInputCls;
    
    cntyRule.Clearance_Evidence_Requirements__c = 'Registration required';
    update cntyRule;
    
    Test.stopTest();
  }
}
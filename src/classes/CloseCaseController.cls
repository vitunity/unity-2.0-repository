public class CloseCaseController 
{
    public String closeReason{get; set;}
    public string shows{get; set;}
    public string Usrname{get; set;}
    public boolean isGuest{get; set;}
    public string Errormsg{get;set;}
    public boolean checkcountry{get;set;}
    set<String> countryset = new set<String>();

    
    public CloseCaseController()
    {
        checkcountry = false;
        shows = 'none';
        User un = [Select Id, AccountId,ContactId,usertype,alias,Contact.Distributor_Partner__c,Contact.Account.country1__r.name From User where id = : Userinfo.getUserId() limit 1];
        if (un.ContactId == null)

        {
            shows = 'block';
            if(un.usertype == label.guestuserlicense)
            {
                Usrname = un.alias; 
                shows='none';
                isGuest=true;
            }
        }
        if(un.Contact.Distributor_Partner__c)
        {
            checkcountry = true;
        }
        for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
        {
            countryset.add(countries.name);
        }
        if(countryset.contains(un.Contact.Account.country1__r.name) && checkcountry == false)
        {
            checkcountry = true;
        }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: un.ContactId])
        {
            if(checkcountry == false && countryset.contains(contactacc.Account__r.country1__r.name))
            {
                checkcountry = true;
            }
        }

    }

    public pagereference closeCase()
    {
        try{
            Case currCase = [SELECT Id, CaseNumber, Contact.Name, Account.Name, Closed_Case_Reason__c, Status FROM Case WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];
            currCase.Closed_Case_Reason__c = 'Customer Cancelled';
            //currCase.Status = 'Closed';
            update currCase;
            system.debug('Case after update' + currCase);
            CaseComment newCaseComment = new CaseComment();
            system.debug('CaseComment declared');
            newCaseComment.ParentId = currCase.id;
            newCaseComment.CommentBody = 'Reason for request to close: '+closeReason.replaceAll('<[^>]+>','');
            insert newCaseComment;
            system.debug('CaseComment inserted'+newCaseComment);
            pagereference pref;
            pref = new pagereference('/apex/CpCaseDetail?id=' + ApexPages.currentPage().getParameters().get('Id') + '@Details');
            pref.setredirect(true);
            return pref;
        }catch(Exception e){
                Errormsg = e.getmessage();
                if(e.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION,'))
                {
                    string str = e.getmessage().substring(e.getmessage().indexof('FIELD_CUSTOM_VALIDATION_EXCEPTION,')+34);
                    Errormsg = str;
                }
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
                return null;
            
        }
        
    }
    
    public pagereference cancelClose()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCaseDetail?id='+ApexPages.currentPage().getParameters().get('Id') + '@Details');
        pref.setredirect(true);
        return pref;
    }
    
}
/*************************************************************************\
    @ Author        : Nikhil Verma
    @ Date      : 14-Nov-2014
    @ Description   :  Test class for CloseCaseController class.
    @ Last Modified By :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/


@isTest(SeeAllData=true)
Public class CloseCaseControllerTest
{
   public static testmethod void TestCloseCaseController()   
    {
        
        Profile vmsserviceuserprofile = [Select id from Profile where name = 'VMS System Admin']; //REFACTOR: Use correct users profiles
        User thisUser = [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and UserRoleId != null  limit 1];
       
        System.runAs ( thisUser )
        {     
            User u;
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            
            Account a;      
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '124554855');
            insert con;
            
            u = new User(alias = 'standt', email='standardtestuser32@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = vmsserviceuserprofile.Id, timezonesidkey='America/Los_Angeles', 
            username='standardtestuser32@testclass.com');
            insert u;
            
            Id caseRecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
            Case case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = caseRecType, Priority = 'Medium');
            insert case1;
            
            Case case2 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = caseRecType);
            insert case2;
            
            ApexPages.currentPage().getParameters().put('Id',case1.Id);
            CloseCaseController inst1 = new CloseCaseController();
            System.runAs (u)
            {
                test.startTest();
                CloseCaseController inst = new CloseCaseController();
                inst.closeReason = 'this reason for closing case is for testing';
                //pagereference pref = 
                inst.closeCase();
                ApexPages.currentPage().getParameters().put('Id','1245');
                inst.closeCase();
                ApexPages.currentPage().getParameters().put('Id',case2.Id);
                pagereference pref = inst.closeCase();
                pref = inst.cancelClose();
                CloseCaseController closeCase = new CloseCaseController();
                closeCase.cancelClose();
                closeCase.Usrname = 'abc';
                closeCase.isGuest = true;
                test.stopTest();
            }
        }
    }
}
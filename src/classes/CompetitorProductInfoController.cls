public class CompetitorProductInfoController {
  
    // fetch DS picklist values
    public static Map<String, List<String>> deliverySystem() {
        
        Map<String, List<String>> DSMap = new Map<String, List<String>> {
            //'' => new List<String>{''},
            'Accuray'=> new List<String>{'','CyberKnife', 'Radixact', 'Tomotherapy','Onrad'},
            'Best Theratronics'=> new List<String>{'','Cobalt (60)'},
            'Brainlab' => new List<String>{'','VERO'},
            'Elekta' => new List<String>{'','Axesse','Compact','GammaKnife','Infinity','Precise','Synergy','Versa HD','Unity','SLi-15'},
            'GammaStar' => new List<String>{'','GyroKnife'},
            'Hitachi' => new List<String>{'','VERO', 'Probeat-V','HL1500'},
            'IBA' => new List<String>{'','Proteus Plus', 'Proteus One'},
            'Low-End Manufacturer' => new List<String>{'','Cobalt (60)'},
            'Mitsubishi' => new List<String>{'','Proton Type', 'VERO'},
            'Mevion' => new List<String>{'','Mevion S250 Series'},
            'Neusoft' => new List<String>{'','HMSR600', 'NeuLife'},
            'None' => new List<String>{'','empty vault'},
            'Refurbisher' => new List<String>{'','Used Accuray','Used Elekta','Used Varian','Used Siemens'},
            'Shinva' => new List<String>{'','XDH 1000','XHA600C/D','XHA600E','Cobalt (60)', 'XHA1400', 'XUA 40'},
            'Siemens' => new List<String>{'','Artiste','Oncor','Primus','Mevatron','MD2'},
            'UJP Praha' => new List<String>{'','Terabalt'},
            'Unknown' => new List<String>{'','Unknown '},
            'ViewRay' => new List<String>{'','MRIdian (Cobalt)','MRIdian (Linac)','MRIdian'},
            'Vacant' => new List<String>{'','Vacant'}
        };
        
        return DSMap;
    }
    
    public static Map<String, List<String>> imageManagementSystems() {
        Map<String, List<String>> IMSMap = new Map<String, List<String>>{
            //'' => new List<String>{''},
            'Anatom-e'=> new List<String>{'','None'},
            'HealthMyne'=> new List<String>{'','None'},
            'MIM'=> new List<String>{'','None'},
            'Mirada'=> new List<String>{'','None'},
            'Oncology Systems Ltd (OSL)'=> new List<String>{'','None'}
      };
        return IMSMap;
    }
    
    public static Map<String, List<String>> oncologyInformationSystems() {
        Map<String, List<String>> OIS = new Map<String, List<String>> {
            //'' => new List<String>{''},
            'Accuray' => new List<String>{'','CK Int.','Tomo Int.'},
            'Allscripts' => new List<String>{'','Allscripts'},
            'Cerner' => new List<String>{'','Cerner'},
            'CIS Healthcare' => new List<String>{'','ChemoCare'},
            'Clinisys' => new List<String>{'','Clinisys'},
            'Elekta' => new List<String>{'','MOSAIQ','Impac','Impac Multi Access','GammaKnife Int.'},
            'Epic' => new List<String>{'','Epic Beacon'},
            'GE' => new List<String>{'','GE Centricity'},
            'GeniousDoc' => new List<String>{'','GeniousDoc Oncology'},
            'Intrinsiq (Amerisource)' => new List<String>{'','Intellidose'},
            'McKesson/USO/iKnowMed' => new List<String>{'','McKesson'},
            'Meditech' => new List<String>{'','Meditech'},
            'MedSym' => new List<String>{'','MedSym'},
            'Pegasus' => new List<String>{'','Pegasus'},
            'Rabbit Healthcare Sys' => new List<String>{'','Rabbit'},
            'HIT Care' => new List<String>{'','None'},
            'In-House' => new List<String>{'','In-House'},
            'RaySearch' => new List<String>{'','RayCare'},
            'Siemens' => new List<String>{'','LANTIS'},
            'Oncochart' => new List<String>{'','Oncochart'},
            'Prowess' => new List<String>{'','Prowess'},
            'Unknown' => new List<String>{'','Unknown'}
        };
        return OIS;
    }
    
    public static Map<String, List<String>> treatmentPlanningSolution() {
        Map<String, List<String>> TPSMap = new Map<String, List<String>>{
            //'' => new List<String>{''},
            'Accuray' => new List<String>{'','CK Int.','Tomo Int.'},
            'Bebig' => new List<String>{'','PSID','SagiPlan','HDRplus'},
            'Best Nomos' => new List<String>{'','Corvus'},
            'BrainLab' => new List<String>{'','I-plan'},
      'Elekta' => new List<String>{'','Oncentra Prostate','Monaco','Oncentra','ACE','XiO','CMS','GammaKnife Int.','ABAS'},
            'MIM' => new List<String>{'','Symphony'},
            
            'Neusoft' => new List<String>{'','NEU-PROPS'},
            'Optivus' => new List<String>{'','Odessey','Conforma 3k'},
            'Philips' => new List<String>{'','Pinnacle'},
            'Prowess' => new List<String>{'','ProArc','Prowess RTP'},
            'RaySearch' => new List<String>{'','RayStation'},
            'Shinva' => new List<String>{'','Fonics Plan'},
            'Siemens' => new List<String>{'','KonRad'},
            'ViewRay' => new List<String>{'','MRI Int.'},
            //INC4926946- Mevis and Nuclemed
            'Mevis' => new List<String>{'','CAT3D'},
            'Nuclemed' => new List<String>{'','MIRS'},
            //INC4926946- End
            'Unknown' => new List<String>{'','Unknown'}
        };
        return TPSMap;
    }

    public static Map<String, List<String>> thirdPartyItems() {
        Map<String, List<String>> TPTMap = new Map<String, List<String>>{
            //'' => new List<String>{''},
            'C-RAD' => new List<String>{'','Catalyst'},
            'Civco' => new List<String>{'','UCT'},
            'Dyn/’R' => new List<String>{'','SDX'},
            'Micropos' => new List<String>{'','RayPilot'},
            'Shimadzu' => new List<String>{'','SyncTraX'},
            'Vision RT' => new List<String>{'','AlignRT'}
        };
        return TPTMap;
    }
    
    public static Map<String, List<String>> brachytheraphyAfterLoader() {
        Map<String, List<String>> BAMap = new Map<String, List<String>>{
            //'' => new List<String>{''},
            'AGAT' => new List<String>{'','AGAT-BT'},
            'Bebig' => new List<String>{'','Ir-192 or Co-60'},
            'Best' => new List<String>{'','Ir-192 or Co-60, Tb-69'},
            'Best Medical' => new List<String>{'','Unknown'},
            'Elekta' => new List<String>{'','HDR or PDR','Digital (v3)', 'v2', 'HDR-3'},
            'Kelinzhong' => new List<String>{'','KLZ-HDR'},
            'NPIC' => new List<String>{'','GZP'},
            'Other' => new List<String>{'','Free Text'},
            'Rongli' => new List<String>{'','RL-HZJ18'},
            'Shinva' => new List<String>{'','XHDR18'},
            'Xoft/iCAD' => new List<String>{'','Axxent'}
        };
        system.debug(' === brachytheraphyAfterLoader === ' + BAMap);
        return BAMap;
    }
    
    public static Map<String, List<String>> serviceContracts() {
        Map<String, List<String>> SCMap = new Map<String, List<String>> {
            '' => new List<String>{''},
            'Oncology Service International' => new List<String>{'None'},
            'AEP International' => new List<String>{'None'},
            'Any Sayfa' => new List<String>{'None'},
            'Doruk Radyasyon Teknolojileri' => new List<String>{'None'},
            'Hizmetler - Teknik Servis  Tera (Turkey)' => new List<String>{'None'},
            'Linear Accelerator Parts - RadParts.com' => new List<String>{'None'},
            'MESA – Medical Equipment Solutions and Applications' => new List<String>{'None'},
            'Radon Medical (Products)' => new List<String>{'None'},
            'Radiology Oncology Systems ROS' => new List<String>{'None'},
            'Richard Kwon' => new List<String>{'None'},
            'Kevin Pueschel' => new List<String>{'None'},
            'TriMedx' => new List<String>{'None'},
            'Southern Linac' => new List<String>{'None'},
            'RS&A' => new List<String>{'None'},
            'Bill Lecompte' => new List<String>{'None'},
            'Crest Services' => new List<String>{'None'},
            'Acceletronics' => new List<String>{'None'},
            'COS' => new List<String>{'None'},
            'MVP' => new List<String>{'None'},
            'Rick Marshall' => new List<String>{'None'},
            'Aramark' => new List<String>{'None'},
            'King Brothers' => new List<String>{'None'},
            'JANNX' => new List<String>{'None'},
            'Linac Repair' => new List<String>{'None'},
            'Service Master' => new List<String>{'None'},
            'Tech Options' => new List<String>{'None'}
        };
        system.debug(' === 3rd Party Service Contracts === ' + SCMap);
        return SCMap;
    }
    
    //INC4852229 : EMR
    public static Map<String, List<String>> EMR() {
        Map<String, List<String>> EMRMap = new Map<String, List<String>>{
            //'' => new List<String>{''},
            'Cerner' => new List<String>{'','Cerner EMR'},
            'Epic' => new List<String>{'','Epic EMR'},
            'MedTech' => new List<String>{'','MedTech EHR'}
        };
        system.debug(' === brachytheraphyAfterLoader === ' + EMRMap);
        return EMRMap;
    }
    
}
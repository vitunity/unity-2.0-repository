@isTest
public class CompetitorProductInfoControllerTest {
	
    public static testMethod void test_Method() {
        test.startTest();
        	Map<String, List<String>> DSMap = new Map<String, List<String>>();
        	CompetitorProductInfoController.deliverySystem();
            
        	Map<String, List<String>> IMSMap = new Map<String, List<String>>();
            CompetitorProductInfoController.imageManagementSystems();
                
        	Map<String, List<String>> OISMap = new Map<String, List<String>>();
            CompetitorProductInfoController.oncologyInformationSystems();
                
        	Map<String, List<String>> TPSMap = new Map<String, List<String>>();
            CompetitorProductInfoController.treatmentPlanningSolution();
                
        	Map<String, List<String>> TPMap = new Map<String, List<String>>();
            CompetitorProductInfoController.thirdPartyItems();
                
        	Map<String, List<String>> BAMap = new Map<String, List<String>>();
        	CompetitorProductInfoController.brachytheraphyAfterLoader();
        
        	Map<String, List<String>> SCMap = new Map<String, List<String>>();
        	CompetitorProductInfoController.serviceContracts();
        
        	Map<String, List<String>> EMRMap = new Map<String, List<String>>();
        	CompetitorProductInfoController.EMR();
        
        test.stopTest();
    }
    
}
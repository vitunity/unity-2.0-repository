/**************************************************************************\
@Last Modified Date- 08/02/2017
@Last Modified By - Rakesh Basani   
@Description - STSK0012364 - Internal user adding contact when creating Product Idea 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012697 - Adding multiple attachments while posting new Idea.
/**************************************************************************/
global class Component1Cntrl {
    
    private integer counter=0;
    private integer list_size=20;
    public integer total_size{get;set;}
    public boolean newpostIdea{get;set;}
    public boolean isExternalUser{get;set;}
    set<string> setExistingProducts;
    Id ZoneId;
    String ZoneName;
    Integer IdeaDescriptionLength = 200;
    public boolean viewMyIdea{get;set;}
    public string panel{get;set;}
    global user cUser;
    global void findTotalRecord(string q){
         string cUId = userinfo.getUserId();
         total_size = database.query(q).size();
    }
    
    public List<WAttachment> lstAttachments{get;set;}
    global Component1Cntrl(){
        isExternalUser = false;
        newpostIdea = false;
        
        
        panel = 'detail';
        cUser = [select Id,Name,ContactId,AccountId,IsPortalEnabled from User where id=:userinfo.getUserId()];
        isExternalUser = cUser.IsPortalEnabled;
        
        viewMyIdea = false;
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        system.debug('::-:->'+lstc);
        if(lstc.size() > 0){
            ZoneId = lstc[0].Id;
            ZoneName = lstc[0].Name;
        }
        newIdeaObj = new Idea();
        NewIdeaAttach = new Attachment();
        newIdeaObj.CommunityId = ZoneId;
        
        
        
        getIdeaProductGroup();
        getIdeaStatus();
        getIdeaSortBy();
    }

                public PageReference Beginning() { //user clicked beginning
                counter = 0;
                return null;
                }

                public PageReference Previous() { //user clicked previous button
                counter -= list_size;
                return null;
                }

                public PageReference Next() { //user clicked next button
                counter += list_size;
                return null;
                }

                public PageReference End() { //user clicked end
                counter = total_size - math.mod(total_size, list_size);
                return null;
                }

                public Boolean getDisablePrevious() { 
                //this will disable the previous and beginning buttons
                if (counter>0) return false; else return true;
                }

                public Boolean getDisableNext() { //this will disable the next and end buttons
                if (counter + list_size < total_size) return false; else return true;
                }

                public Integer getTotal_size() {
                return total_size;
                }

                public Integer getPageNumber() {
                return counter/list_size + 1;
                }

                public Integer getTotalPages() {
                if (math.mod(total_size, list_size) > 0) {
                return total_size/list_size + 1;
                } else {
                return (total_size/list_size);
                }
                }
    
    
    public Idea newIdeaObj{get;set;}
    public Attachment NewIdeaAttach{get;set;}
    string EclipsStatus = 'New';
    public PageReference SaveIdea(){
        newIdeaObj.status = 'New';
        //STSK0012364 - Rakesh.Start
        if(isExternalUser){
            if(cUser.AccountId <> null)newIdeaObj.Account__c = cUser.AccountId;
            if(cUser.ContactId <> null)newIdeaObj.Contact__c = cUser.ContactId;
        }else{
            if(newIdeaObj.Contact__c <> null){
                Contact conObj = [select Id,AccountId from Contact where Id =: newIdeaObj.Contact__c];
                newIdeaObj.Account__c = conObj.AccountId;
            }
        }
        //STSK0012364 - Rakesh.End
        if(NewIdeaObj.categories <> null){
            set<string> setCategory = new set<string>();
            for(string t : (NewIdeaObj.categories).split(';')){
                setCategory.add(t);
            }
            if(setCategory.contains('Eclipse')){
                newIdeaObj.status = EclipsStatus;
            }
        }
        insert newIdeaObj;
 
        //Retriving all documents of new Idea and delete onnce the attachments are added to new Idea. 
        if(lstAttachments <> null && lstAttachments.size()>0){
            List<Attachment> lstAttach = new List<attachment>();
            set<string> setDocumentIds = new set<string>();
            
            for(WAttachment w: lstAttachments){     setDocumentIds.add(w.AId);   }
            map<Id,Document> mapDocument = new map<Id,Document>([select Id,Body from Document where ID IN: setDocumentIds]);
            if(mapDocument.size() > 0){ Idea_Attachment__c IdeaAttachObj = new Idea_Attachment__c(Idea__c = newIdeaObj.Id );
                insert IdeaAttachObj;for(WAttachment w: lstAttachments){ if(mapDocument.ContainsKey(w.AId)){     Attachment at = new Attachment();   at.Name = w.AName;  at.ContentType = w.AContentType;   at.Body = mapDocument.get(w.AId).Body;
                        at.parentId = IdeaAttachObj.Id;  lstAttach.add(at);      }
                }           
                insert lstAttach;
                delete mapDocument.values();
            }           
        }
        NewIdeaAttach = new Attachment();
        newIdeaObj = new Idea();
        newIdeaObj.CommunityId = ZoneId;
        panel = 'detail';
        return null;
    }
     /***************************************************************************
    Description:STSK0012697  
    Delete related document when click on remove link.
    *************************************************************************************/ 
    public void RemoveAttachmentlink(){
        string docid = apexpages.currentpage().getparameters().get('docid');
        if(docid <> null){
            List<document> lstDocument = [select Id from Document where id=:docid];
            if(lstDocument.size()>0){
                delete lstDocument;
            }
            List<WAttachment> lsttemp = new List<WAttachment>();
            for(WAttachment w : lstAttachments){
                if(w.AId <> docid){
                    lsttemp.add(w);
                }
            }
            lstAttachments = new List<WAttachment> ();
            lstAttachments = lsttemp;
        }
        
    }
    /***************************************************************************
    Description: STSK0012697 
    Create new document in public document folder when new file is selected.
    *************************************************************************************/ 
    public void addAttachment(){
        try{
            
            string  fid  = [ select id,name from folder where name = 'Public Documents'].id;
            document doc = new Document(name= string.valueOf(system.now()),body = NewIdeaAttach.body,folderId = fid);insert doc;
            WAttachment w = new WAttachment(doc.Id,NewIdeaAttach.Name,NewIdeaAttach.ContentType);
            lstAttachments.add(w);
        }catch(Exception e){system.debug('PDF:'+e.getMessage());}
        NewIdeaAttach = new Attachment();
    }
    
    //STSK0012697 - Wrapper class to maintain multiple attachments.     
    public class WAttachment{
        public String AId{get;set;}
        public String AName{get;set;}
        public String AContentType{get;set;}

        public WAttachment(String AId, string AName, String AContentType){
            this.AId = AId;
            this.AName = AName;
            this.AContentType = AContentType;
        }
    }
    
    public PageReference ResetNewIdea(){
        
        newIdeaObj = new Idea();
        newIdeaObj.CommunityId = ZoneId;
        return null;
    }
    public void addComment(){
        if(CommentObj <> null)
            insert CommentObj.CommentObj;
        panel='detail';
    }
    
    public wProductCommentDetail CommentObj{get;set;}
    public List<wProductIdeaComment> lstMIdeaComment{get;set;}
    public string hdnIdeaId{get;set;}
    public void  getIdeaComments(){
        
        lstMIdeaComment = new List<wProductIdeaComment>();
        List<IdeaComment> lstIdeaComment = [Select Id,CommentBody,createdBy.Name,CommunityId,CreatorFullPhotoUrl,CreatorName,CreatorSmallPhotoUrl,IdeaId,IsHtml,UpVotes,createdDate from IdeaComment where   IdeaId =: hdnIdeaId and createdBy.Id =: userinfo.getUserId()];

        for(IdeaComment i : lstIdeaComment){
            string dayCount = getDaysAgo(i.createdDate);
            lstMIdeaComment.add(new wProductIdeaComment(i,dayCount));
        }
        Idea i = [Select id,title,body from Idea where Id=: hdnIdeaId];
        CommentObj = new wProductCommentDetail(i.Id,i.title,i.body,lstMIdeaComment);
    }
    
    
    public List<wProductIdea> getProductIdea(){
        
       
        system.debug('RKSS:157');
         
        List<wProductIdea> lstPIdea = new List<wProductIdea>();
        string cUId = userinfo.getUserId();
        
        
        string query = 'Select id,title,body,Release_Version__c,Categories,Votescore, votetotal, recordtypeid, Status ,Numcomments, lastreferencedDate, ';
        query += ' LastviewedDate, ismerged, IsHTML ,CreatorFullPhotoUrl, AttachmentName,createdDate,createdBy.Name,';
        query += '(select Id from Comments where CreatedById =: cUId), ';
        query += '(select Id,Type from Votes where CreatedById =: cUId and isDeleted = false) from Idea where title <> null and ';
        query += '(Status <> \'Converted to Case\' and Status <> \'Internal\' and  Status <> \'Duplicate\') and communityId =: ZoneId and (Status <> \'New\' or  (Status = \'New\' and createdById =: cUId)) and (Status <> \'Pending Customer Input\' or  (Status = \'Pending Customer Input\' and createdById =: cUId)) and ';
        query +=  '(isMerged = false or  (isMerged = true and createdById =: cUId))';
        if(strIdeaPG <> 'All' && strIdeaPG <> '' && strIdeaPG <> null){
           query += ' and Categories INCLUDES (\''+strIdeaPG+'\')';
        }
        if(strIdeaStatus <> 'All'){
            query += ' and Status =: strIdeaStatus';
        }
        if(Idea_TypeSearch <> '' && Idea_TypeSearch <> null){
            //query += ' and title like \'%'+Idea_TypeSearch+'%\'';
            query += ' and Id IN : setFilteredIds ';
        }
        if(viewMyIdea){         
            query += ' and createdById =: cUId';
        }
        query += ' and title <> \'Include prescription status in Visit tab\' ';
        
        if(strSortBy == 'JustReleased')query += ' and  Status = \'Released\'';
        else if(strSortBy == 'Recent')query += ' Order By createdDate Desc ';
        else if(strSortBy == 'Popular')query += ' Order By votetotal Desc';

        system.debug('RKSS:187:'+strIdeaPG+'-----'+strIdeaStatus+'-----'+Idea_TypeSearch+'---'+setFilteredIds);
        system.debug('RKSS:188:'+query);
        
        findTotalRecord(query);
        query += ' limit :list_size offset :counter';
        
        //apexpages.addmessage(new apexpages.message(apexpages.severity.error,system.now()+'---'+strIdeaPG+'-----'+strIdeaStatus+'-----'+Idea_TypeSearch+'---'+setFilteredIds));

        
        try{
        
        
        system.debug('RKSS:199:'+query);
        List<Idea> lstIdea = database.query(query);
        system.debug('RKSS:201:'+lstIdea);
        
        
        for(Idea i :lstIdea ){
            string dayCount = getDaysAgo(i.createdDate);
            datetime tdate = system.today();
            datetime cdate = i.createdDate;
            
            boolean upVote = false;
            boolean downVote = false;
            boolean isVoteEnable = true;
            if(Idea_TypeSearch <> '' && Idea_TypeSearch <> null){
                if((i.title) <> null && (i.title).containsIgnoreCase(Idea_TypeSearch)){
                    //i.title  = (i.title).replaceAll('(?i)'+Idea_TypeSearch,'<span style="background-color:yellow">'+Idea_TypeSearch+'</span>');
                    i.title = highlightedText(i.title,Idea_TypeSearch,'<span style="background-color:yellow">','</span>');
                }
                if((i.body) <> null && (i.body).containsIgnoreCase(Idea_TypeSearch) ){
                    //i.body  = (i.body).replaceAll(Idea_TypeSearch,'<span style="background-color:yellow">'+Idea_TypeSearch+'</span>');
                    i.body = highlightedText(i.body,Idea_TypeSearch,'<span style="background-color:yellow">','</span>');
                }
            }
            system.debug('RKSS:222:'+i.title+'::'+i.body);
            string shortDescription = i.body;
            if(i.votes <> null && i.votes.size() > 0){
                isVoteEnable = false;
                Vote v = i.votes[0];
                if(v.Type == 'up')upVote = true;
                if(v.Type == 'down')downVote = true;
            }
            Integer numComment = i.Comments.size();
            
            if(i.body <> null && string.valueOf(i.body).length() > IdeaDescriptionLength)shortDescription = (i.body).substring(0,IdeaDescriptionLength)+'....&nbsp;&nbsp;<a href="javascript:void(0)" onclick="showMoreDescription(\''+i.id+'\')" style="color: #0070d2;">Show more</a>';
            lstPIdea.add(new wProductIdea(i,i.title,i.body,shortDescription,numComment,dayCount,i.votetotal,i.CreatorFullPhotoUrl,i.createdBy.Name,upVote,downVote,isVoteEnable));
        }
    }
        catch(Exception e){ 
            system.debug('RKSS:236:'+e.getLineNumber()+'--'+e.getMessage());
            apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Info,e+'--'+query);
            apexpages.addmessage(msg);
       }
       system.debug('RKSS:240:'+lstPIdea);
        return lstPIdea;
    }
    Public string highlightedText(string str, string repalcestr , string tagstart, string tagend){
     boolean isworking = true;
     string finalText = '';
     while(isworking){
      Integer startindex = str.indexOfIgnorecase(repalcestr,0);
      if(startindex == -1){finalText += str;  isworking = false;}
      else{
       finalText += str.substring(0,startindex)+tagstart+str.substring(startindex,startindex+repalcestr.length())+tagend;
       str = str.substring(startindex+repalcestr.length());
      }    
     }
     return finalText;
    }
    
    public void makeVote(){
        string IdeaId = apexpages.currentpage().getparameters().get('IdeaId');
        string Type = apexpages.currentpage().getparameters().get('Type');
        Vote v = new Vote();
        v.ParentId = IdeaId;
        v.Type = Type;
        insert v;
    }
    string IdeaId;
    public void IdeaIdRefresh(){
        panel = 'comment';
        getIdeaComments();
    }
    
    //STSK0012364 - Rakesh.Start
    private map<string,Attachment> mapMultipleAttachment;
     public void onPostIdeaClick(){
        lstAttachments = new List<WAttachment>();
        mapMultipleAttachment = new map<string,Attachment>();
        panel = 'postidea';
    }
    //STSK0012364 - Rakesh.End
    set<Id> setFilteredIds = new set<Id>();
    public void IdeaSearch(){
    setFilteredIds.clear();
        panel = 'detail'; 
        system.debug('RKSS:273: '+Idea_TypeSearch);
        if(Idea_TypeSearch <> null && Idea_TypeSearch <> ''){
            List<Idea> lstFIdea = [Select id,title,body from Idea where title <> null and Status <> 'Converted to Case' and Status <> 'Duplicate' and Status <> 'Internal' and communityId =: ZoneId and  (Status <> 'New' or  (Status = 'New' and createdById =: userinfo.getUserId())) and  (Status <> 'Pending Customer Input' or  (Status = 'Pending Customer Input' and createdById =: userinfo.getUserId())) and (isMerged = false or  (isMerged = true and createdById =: userinfo.getUserId()))];      
            system.debug('RKSS:276: '+lstFIdea);
            for(Idea i : lstFIdea){
                if(((i.title) <> null && (i.title).containsIgnoreCase(Idea_TypeSearch)) || ((i.body) <> null && (i.body).containsIgnoreCase(Idea_TypeSearch)) ){
                    setFilteredIds.add(i.id);
                }
            }
        }
        system.debug('RKSS:282: '+setFilteredIds);
    }
   
    public string getDaysAgo(Datetime cDate){
        Date tDate = date.today();

        Date yDate = date.newInstance(tDate.year(),cDate.month(),cDate.day());

        Integer year =tDate.year()-cDate.year();
        if(tDate < yDate){
            year--;
        }
        Integer month = 0;
        for(Integer m = 0;m<12;m++){
            Date tempDt = date.newInstance(cDate.year()+year,cDate.month(),cDate.day());
            tempDt = tempDt.addMonths(m);
            if(tempDt > tDate){
                month--;
                break;
            }   
            month++;
        }

        Integer day = 0;
        for(Integer m = 0;m<31;m++){
            Date tempDt = date.newInstance(cDate.year()+year,cDate.month()+month,cDate.day());
            tempDt = tempDt.addDays(m);
            if(tempDt > tDate){
                day--;
                break;
            }   
            day++;
        }
        string rstr = 'Today';
        if(year > 0){rstr = year +' year';if(year > 1)rstr +='s';rstr+=' ago';}
        else if(month > 0){rstr = month +' month';if(month > 1)rstr +='s';rstr+=' ago';}
        else if(day > 0){rstr = day +' day';if(day > 1)rstr +='s';rstr+=' ago';}

        return rstr ;
    }

    public class wProductIdea{      
        public Idea ideaObj{get;set;}
        public string title{get;set;}
        public string Description{get;set;}
        public string ShortDescription{get;set;}
        public Integer CommentCount{get;set;}
        public string Daysago{get;set;}
        public decimal Points{get;set;}
        public string userURL{get;set;}
        public string UserName{get;set;}
        public boolean upVote{get;set;}
        public boolean downVote{get;set;}
        public boolean isVoteEnable{get;set;}
        
        public wProductIdea(Idea ideaObj, string title, string Description, string ShortDescription, Integer CommentCount, string Daysago, decimal Points, string userURL, string UserName,boolean upVote,boolean downVote, boolean isVoteEnable){
            this.ideaObj = ideaObj;
            this.title = title;
            this.Description = Description;
            this.ShortDescription = ShortDescription;
            this.CommentCount = CommentCount;
            this.Daysago = Daysago;
            this.Points = Points;
            this.userURL = userURL;
            this.UserName = UserName;
            this.upVote= upVote;
            this.downVote= downVote;
            this.isVoteEnable = isVoteEnable;
        }
    }
    public string strIdeaPG{get;set;}
   
    public List<SelectOption> getIdeaProductGroup() {
        setExistingProducts = new set<string>();
        string cUId = userinfo.getUserId();
        for(Idea i : [select Id,Categories from Idea where title <> null and (Status <> 'Converted to Case' and Status <> 'Internal' and  Status <> 'Duplicate') and communityId =: ZoneId and (Status <> 'New' or  (Status = 'New' and createdById =: cUId)) and (Status <> 'Pending Customer Input' or  (Status = 'Pending Customer Input' and createdById =: cUId)) and (isMerged = false or  (isMerged = true and createdById =: userinfo.getUserId())) limit 50000]){
            if(i.Categories <> null){
                for(string s : (i.Categories).split(';')){
                    setExistingProducts.add(s);
                }
            }
        }
        
        List<SelectOption> options = new List<SelectOption>();
        List<SelectOption> options1 = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        strIdeaPG = 'All';
        for(string s : setExistingProducts){        
            options1.add(new SelectOption(s,s));
        }
        options1.sort();
        options.addAll(options1);
        return options;
    }
    public string strSortBy{get;set;}  
    public List<selectOption> getIdeaSortBy(){
        strSortBy = 'Recent';
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Recent','Recent'));
        options.add(new SelectOption('Popular','Popular'));
        options.add(new SelectOption('JustReleased','Just Released'));
        return options;
    }
    
    public class WIdeaStatus implements Comparable {
        public string name;public WIdeaStatus(string name){this.name=name;}
        public Integer compareTo(Object compareTo) {
        WIdeaStatus compareToOppy = (WIdeaStatus)compareTo;
        Integer returnValue = 0;
        if (name > compareToOppy.name)returnValue = 1;
        else if (name < compareToOppy.name)returnValue = -1;        
        return returnValue;       
    }
    }
    public string strIdeaStatus{get;set;}   
    public List<SelectOption> getIdeaStatus() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        strIdeaStatus = 'All';
        List<WIdeaStatus> lWrapper = new List<WIdeaStatus>();
        for(string s : getPicklistValues('Idea','Status')){
            if(s <> 'Duplicate' && s <> 'Converted to Case' &&  s <> 'Internal')            
            lWrapper.add(new WIdeaStatus(s));
        }
        lWrapper.sort();
        for(WIdeaStatus s:lWrapper){
            options.add(new SelectOption(s.name,s.name));
        }
        return options;
    }
    public string strNewIdeaPG{get;set;}
    public List<SelectOption> getNewIdeaProductGroup() {
        List<SelectOption> options = new List<SelectOption>();        
        for(string s : getPicklistValues('Idea','Categories')){
            if(strNewIdeaPG == null)strNewIdeaPG = s;
            options.add(new SelectOption(s,s));
        }
        return options;
    }
    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 

        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
          lstPickvals.add(a.getValue());//add the value  to our final list
        }
    
        return lstPickvals;
    }
    public string Idea_TypeSearch{get;set;}
    public pagereference reset(){
        strIdeaPG = 'All';
        strIdeaStatus = 'All';
        Idea_TypeSearch = null;
        viewMyIdea = false; 
        panel='detail';     
        return null;
    } 
    
    
    public class wProductCommentDetail{  
        public string IdeaTitle{get;set;}
        public string IdeaBody{get;set;}
        public IdeaComment CommentObj{get;set;}        
        public List<wProductIdeaComment> lstComment{get;set;}
        public wProductCommentDetail(Id IdeaId,string IdeaTitle,string IdeaBody,List<wProductIdeaComment> lstComment){
            this.CommentObj = new IdeaComment(IdeaId = IdeaId);
            this.IdeaTitle = IdeaTitle;
            this.IdeaBody = IdeaBody;
            this.lstComment = lstComment;
        }
        
    }
    public class wProductIdeaComment{      
        public IdeaComment CommentObj{get;set;}        
        public string Daysago{get;set;}
        
        public wProductIdeaComment(IdeaComment CommentObj, string Daysago){
            this.CommentObj = CommentObj;
            this.Daysago = Daysago;
        }
    }
    
    
}
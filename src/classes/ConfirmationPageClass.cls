/**
  * @author hli/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Display confirmation / error message for Timesheet page
  * 
  * TEST CLASS 
  * 
  *   
  * ENTRY POINTS 
  *  
  *    SR_TimeCard_Controler
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  hli/Forefront; Initial Build
  * 
  **/
  
  public without sharing class ConfirmationPageClass{
      
      public String errorOrSuccess {get; set;}
      public String message {get;set;}
      public String returnId {get; set;}
      
      public ConfirmationPageClass(){
        returnId = 'apex/SR_TimeCard';
        if(Apexpages.currentPage().getParameters() != null && Apexpages.currentPage().getParameters().get('success') != null){
            errorOrSuccess = Apexpages.currentPage().getParameters().get('success');
            Id currTSId = Apexpages.currentPage().getParameters().get('Id');
            if(currTSId != null){
                returnId += '?id=' + currTSId;
            }

            if(errorOrSuccess == 'true'){
                errorOrSuccess = 'confirm';
                message = 'Time Card approved successfully';
            }else if(errorOrSuccess == 'false'){
                errorOrSuccess = 'error';
                message = 'There was an error approving the Time Card. An email will be sent with the error message';
            }
         }else{
            errorOrSuccess = 'error';
            message = 'Invalid URL Parameter';
         }
      }
  }
/*************************************************************************\
    @ Author                : Sunil Bansal
    @ Date                  : 08-May-2013
    @ Description           : This Class will be used to calculate VMS factory tour attended on Accounts based on associated contacts.
    @ Last Modified By      :
    @ Last Modified On      :
    @ Last Modified Reason  :

    @ Updated By            : Sumit Tanwar on 16th July, 2014.
                              Puneet Sardana on 23 September 2014 for T-321410, T-321409
                              Naresh K Shiw  on 20 October 2014   for T-321409, T-321410
****************************************************************************/
public class ContactMaster {

    //============================================Production Code===========================================================//
    /*
      Method to retrieving account ids from contacts on delete
      @param contacts - List of contact records deleted
    */
    public void onContactDelete(List<Contact> contacts)
    {
        Set<Id> accountIds = new Set<Id>();
        for(Contact c: contacts)
        {
            if(c.AccountId != null)
                accountIds.add(c.AccountId);
        }
        if(accountIds.size() > 0)
            updateAccountForContactAttributes(accountIds);
    }

    /*
      Method to retrieving account ids from contacts on delete
      @param contacts - List of contact records updated
    */
    public void onContactUpdate(List<Contact> contacts, Map<Id, Contact> oldMap)
    {
        Set<Id> accountIds = new Set<Id>();
        for(Contact c: contacts)
        {
            if(c.AccountId != null)
                accountIds.add(c.AccountId);
        }
        if(accountIds.size() > 0)
            updateAccountForContactAttributes(accountIds);
    }

    /*
      Method to update VMS_Factory_Tour_Attended__c & At_least_one_RAQA_Contact_Assigned__c on
      account based on VMS_Factory_Tour_Attended__c, RAQA_Contact__c values on related contacts
      @param accountIds - Set of account ids related to contacts updated/deleted
    */
    private void updateAccountForContactAttributes(Set<Id> accountIds)
    {
        try
        {
            if(accountIds != null && accountIds.size() > 0)
                {
                    List<Contact> contactList = [Select Id, AccountId, VMS_Factory_Tour_Attended__c, RAQA_Contact__c from Contact where AccountId IN :accountIds];
                    Map<Id, List<Contact>> accountIdContacts = new Map<Id, List<Contact>>();
                    Map<Id, Account> accountTrueMap = new Map<Id, Account>();
                    Map<Id, Account> accountFalseMap = new Map<Id, Account>();

                    System.debug('CONTACTS LIST == '+contactList);

                    for(Contact c: contactList)
                    {
                        List<Contact> contacts = new List<Contact>();
                        if(accountIdContacts.containsKey(c.AccountId))
                            contacts = accountIdContacts.get(c.AccountId);
                        contacts.add(c);
                        accountIdContacts.put(c.AccountId, contacts);
                    }

                    for(Id accountId: accountIds)
                    {
                        System.debug('ACCOUNT IDs SIZE == '+accountIds.size());
                        Boolean VMSTourDone = false;
                        Boolean RAQAContactFound = false;
                        List<Contact> contacts = accountIdContacts.get(accountId);
                        Integer toursAttended = 0;
                        if(contacts != null)
                        {
                            for(Contact c: contacts)
                            {
                                Account acc = new Account(Id = accountId);
                                if(accountTrueMap.containsKey(accountId))
                                    acc = accountTrueMap.get(accountId);

                                if(c.VMS_Factory_Tour_Attended__c == true)
                                {
                                    toursAttended++;
                                    acc.VMS_Factory_Tours_Attended__c = toursAttended;
                                    VMSTourDone = true;
                                }
                                if(c.RAQA_Contact__c == true)
                                {
                                    acc.At_least_one_RAQA_Contact_Assigned__c = true;
                                    RAQAContactFound = true;
                                }

                                if(VMSTourDone == true || RAQAContactFound == true)
                                    accountTrueMap.put(accountId, acc);
                            }
                        }

                        if(VMSTourDone == false)
                        {
                            Account acc = new Account(Id = accountId);

                            // may be in this map due to RAQA contact found

                            if(accountTrueMap.containsKey(accountId))
                            {
                                acc = accountTrueMap.get(accountId);
                                acc.VMS_Factory_Tours_Attended__c = 0;
                                accountTrueMap.put(accountId, acc);
                            }
                            else
                            {
                                acc.VMS_Factory_Tours_Attended__c = 0;
                                accountFalseMap.put(accountId, acc);
                            }
                        }
                        if(RAQAContactFound == false)
                        {
                            Account acc = new Account(Id = accountId);

                            // may be in this map due to VMS Tour Attended count

                            if(accountTrueMap.containsKey(accountId))
                            {
                                acc = accountTrueMap.get(accountId);
                                acc.At_least_one_RAQA_Contact_Assigned__c = false;
                                accountTrueMap.put(accountId, acc);
                            }

                            // may be in this map due to VMS Tour attendance has alerady put in this map and just setting RAQA flag to false may override VMS tour count

                            else if(accountFalseMap.containsKey(accountId))
                            {
                                acc = accountFalseMap.get(accountId);
                                acc.At_least_one_RAQA_Contact_Assigned__c = false;
                                accountFalseMap.put(accountId, acc);
                            }
                            else
                            {
                                acc.At_least_one_RAQA_Contact_Assigned__c = false;
                                accountFalseMap.put(accountId, acc);
                            }
                        }
                    }

                    List<Account> updateAccounts = new List<Account>();

                    if(accountTrueMap.size() > 0)
                        updateAccounts.addAll(accountTrueMap.values());
                    if(accountFalseMap.size() > 0)
                        updateAccounts.addAll(accountFalseMap.values());

                    if(updateAccounts.size() > 0)
                        update updateAccounts;
                }
        }
        catch(Exception e)
        {
        }
    }
    //=======================================================================================================//

    //====================================OCSUGC CODE========================================================//

    // @description: This method sets the User.OCSUGC Accepted Terms of Use to False
    // @param: set of contact ids for users to be updated
    // @return: void
    private static void updateAcceptedTerms(Set<Id> contactIds) {
        List<User> lstUser = [SELECT Id,OCSUGC_Accepted_Terms_of_Use__c, OCSDC_AcceptedTermsOfUse__c
                              FROM User
                              WHERE ContactId IN :contactIds];
        List<User> lstUserUpdate = new List<User>();
        Set<ID> userIds    = new Set<ID>();
        for(User usr : lstUser) {
            if(usr.OCSUGC_Accepted_Terms_of_Use__c || usr.OCSDC_AcceptedTermsOfUse__c) {
              usr.OCSUGC_Accepted_Terms_of_Use__c = false;
              usr.OCSDC_AcceptedTermsOfUse__c = false;
              userIds.add(usr.id);
              lstUserUpdate.add(usr);
            }
        }
        if(lstUserUpdate.size() > 0) {
            update lstUserUpdate;
            OCSUGC_Utilities.removePermissionSetFromUsers(userIds, new Set<String>{OCSUGC_Constants.DC_COMMUNITY_MEMBER_SET}); 
        } 
    }

    // @description: This method sets OCSUGC User Role to be a member
    // @param: set of contact ids for users to be updated
    // @return: void
    public static void setOCSUGCUserRoleAsMember(Map<Id, Contact> newMap, Map<Id, Contact> oldMap) {
        boolean isInsert = oldMap == null;

        Set<String> contactIds = new Set<String>();
        for(Contact contact : newMap.values()) {
            if(!isInsert && (contact.OCSUGC_UserStatus__c != null) && (oldMap.get(contact.id).OCSUGC_UserStatus__c != contact.OCSUGC_UserStatus__c)){
                contactIds.add(contact.Id);
            }
        }

        List<User> lstUser = [SELECT Id,OCSUGC_User_Role__c
                              FROM User
                              WHERE IsActive = true
                              AND  ContactId IN :contactIds];

        List<User> lstUserUpdate = new List<User>();

        for(User usr : lstUser) {
            usr.OCSUGC_User_Role__c = Label.OCSUGC_Varian_Employee_Community_Member;
            lstUserUpdate.add(usr);
        }

        if(lstUserUpdate.size() > 0)
            update lstUserUpdate;
    }

    // @description: This method updates OCSUGC_UserStatus__c and other OCSUGC fields for contacts newly approved for MyVarian
    // @param:   List Trigger.NEW
    // @param:   OldMap from Trigger i.e. Trigger.oldMap
    // @return:  void
   public static void updateOCSUGCUserStatus(List<Contact> newList, Map<Id, Contact> oldMap){
        boolean isInsert = oldMap == null;

        Set<Id> contactIds = new Set<Id>();

        Set<String> ocsugcDisqualifiedFunctionalRoles = OCSUGC_Utilities.fetchOCSUGCDisqualifiedFunctionalRoles();
        Map<String, OCSUGC_Disapproved_Countries__c> ocsugcDisqualifiedCountries = OCSUGC_Disapproved_Countries__c.getAll();
        
        // updated by Puneet Mishra 21-05-2015, two new condition added to avoid status changed to approved if Status earlier was either rejected / Disabled by Self
        for(Contact contact : newList) {
            if( (contact.MyVarian_Member__c && (isInsert || contact.MyVarian_Member__c != oldMap.get(contact.Id).MyVarian_Member__c)  && 
                (contact.OCSUGC_UserStatus__c != Label.OCSUGC_Disabled_by_Self && contact.OCSUGC_UserStatus__c != Label.OCSUGC_Rejected) ) ){
                if( !ocsugcDisqualifiedFunctionalRoles.contains(contact.Functional_Role__c) ) {
                    if(ocsugcDisqualifiedCountries.containsKey(contact.Account.Country__c)) {
                        contact.OCSUGC_UserStatus__c = Label.OCSUGC_Disqualified;    
                    } else {
                        contact.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
                        contact.OCSUGC_Activation_Date__c = system.today();
                        contact.OCSUGC_Notif_Pref_EditGroup__c = true;
                        contact.OCSUGC_Notif_Pref_InvtnJoinGrp__c = true;
                        contact.OCSUGC_Notif_Pref_PrivateMessage__c = true;
                        contact.OCSUGC_Notif_Pref_ReqJoinGrp__c = true;
                    }                    
                } else {
                    contact.OCSUGC_UserStatus__c = Label.OCSUGC_Disqualified;
                }
            }
        }
    }
    
    // @description: This method removes OCSUGC Group membership from OKTA
    // @param:   NldMap from Trigger i.e. Trigger.newMap
    // @param:   OldMap from Trigger i.e. Trigger.oldMap
    // @return:  void
    public static void removeOCSUGCGroupMembershipInOkta(Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
        System.debug('in removeOCSUGCGroupMembershipInOkta');
        system.debug('#### IN CONTACTMASTER.removeOCSUGCGroupMembershipInOkta');
        boolean isInsert = oldMap == null;
        Set<String> contactIds = new Set<String>();
        Set<String> approvedContactIds = new Set<String>();
        for(Contact contact : newMap.values()) {
            if(!isInsert && ((!contact.MyVarian_Member__c && contact.MyVarian_Member__c != oldMap.get(contact.Id).MyVarian_Member__c) ||
                (contact.OCSUGC_UserStatus__c == Label.OCSUGC_Disabled_by_Admin && contact.OCSUGC_UserStatus__c != oldMap.get(contact.Id).OCSUGC_UserStatus__c) ||
                (contact.OCSUGC_UserStatus__c == Label.OCSUGC_Disabled_by_Self && contact.OCSUGC_UserStatus__c != oldMap.get(contact.Id).OCSUGC_UserStatus__c))){
                contactIds.add(contact.Id);
            }

            system.debug(' Checking User Status 1' + contact.OCSUGC_UserStatus__c);
        }

        if(contactIds.size() == 1){
            for(String contactId  : contactIds){
                if(newMap.containsKey(contactId) && newMap.get(contactId).OktaId__c != null){
                    OCSUGC_ContactCommunityUserStatusCon.updageUserGroupInOkta(newMap.get(contactId).OktaId__c, 'DELETE');
                }
            }
        }
    }

    // @description: Set the User.OCSUGC Accepted Terms of Use to False when the user is self deactivated or deactivated by Community Manager
    // @param:   List Trigger.NEW
    // @param:   OldMap from Trigger i.e. Trigger.oldMap
    // @return:  void
    public static void resetOCSUGCTermsAndPermissions(List<Contact> contacts, Map<Id, Contact> oldMap)
    {
        // Added Ref T-341525
        Set<Id> contactIds = new Set<Id>();
        //-- Added Ref T-341525
        for(Contact c: contacts)
        {            
                // Added Ref T-341525
                if(c.OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_DISABLE_SELF ||
                   (oldMap!=null && oldMap.get(c.Id)!=null && oldMap.get(c.Id).OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_APPROVED && c.OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_DISABLE_ADMIN) ||
                   (oldMap!=null && oldMap.get(c.Id)!=null && oldMap.get(c.Id).OCSUGC_UserStatus__c == OCSUGC_Constants.CONTACT_STATUS_APPROVED && c.OCSUGC_UserStatus__c == OCSUGC_Constants.OCSUGC_STATUS_REJECTED)) {
                   contactIds.add(c.Id);
                }
                //-- Added Ref T-341525
        }
      
        // Added Ref T-341525
        if(contactIds.size() > 0)
            updateAcceptedTerms(contactIds);
        //-- Added Ref T-341525
    }
}
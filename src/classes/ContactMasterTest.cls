/**
 * This class contains unit tests for validating the behavior of Apex class 'ContactMaster'
 * This test class also covers Trigger 'ContactMaster_AfterTrigger' and 'ContactMaster_BeforeTrigger'
 */


@isTest
private class ContactMasterTest {

    static testmethod void myconstructorTest() {            
        Test.startTest();       
        Account accnt= OCSUGC_TestUtility.createAccount('test account', true);
        List<Contact> contactList = new List<Contact>();
        contact con = new contact();
        con.FirstName= 'Megha';
        con.lastname= 'Arora';
        con.Accountid= accnt.id;
        con.department='Rad ONC';
        con.MailingCity='New York';
        con.MailingCountry='US';
        con.MailingStreet='New Jersey2,';
        con.MailingPostalCode='552601';
        con.MailingState='CA';
        con.Mailing_Address1__c= 'New Jersey2';
        con.Mailing_Address2__c= '';
        con.Mailing_Address3__c= '';
        con.HasOptedOutOfEmail = True;
        con.Email='sidhant16agarwal@gmail.com';
        con.VMS_Factory_Tour_Attended__c= true;
        con.RAQA_Contact__c= true;
        con.Date_of_Last_Factory_Visit__c= System.today();
        con.MyVarian_Member__c=true;
        con.Functional_Role__c='Physician';
        //con.OCSUGC_Contact_Source__c = 'Spheros';
        contactList.add(con);
        
        contact con1 = new contact();
        con1.FirstName= 'Ranjan';
        con1.lastname= 'Jha';
        con1.Accountid= accnt.id;
        con1.department='Rad ONC';
        con1.MailingCity='New York';
        con1.MailingCountry='USA';
        con1.MailingStreet='New Jersey3,MA';
        con1.MailingPostalCode='552602';
        con1.MailingState='SA';
        con1.Mailing_Address1__c= 'New Jersey3';
        con1.Mailing_Address2__c= 'MA';
        con1.Mailing_Address3__c= '';
        con1.Email='test@gmail.com';
        contactList.add(con1);
        
        contact con2 = new contact();
        con2.FirstName= 'Sunil';
        con2.lastname= 'Bansal';
        con2.Accountid= accnt.id;
        con2.department='Rad ONC';
        con2.MailingCity='New York';
        con2.MailingCountry='USA';
        con2.MailingStreet='New Jersey3,MA,City';
        con2.MailingPostalCode='552602';
        con2.MailingState='SA';
        con2.Mailing_Address1__c= 'New Jersey3';
        con2.Mailing_Address2__c= 'MA';
        con2.Email='test@gmail.com';
        con2.Mailing_Address3__c= 'City';  
        contactList.add(con2);  
        
        contact con4 = new contact();
        con4.FirstName= 'Sunil';
        con4.lastname= 'Bansal';
        con4.Accountid= accnt.id;
        con4.department='Rad ONC';
        con4.Mailing_Address1__c= '';
        con4.Mailing_Address2__c= '';
        con4.Mailing_Address3__c= ''; 
        con4.MailingCity='New York';
        con4.MailingCountry='USA';
        con4.MailingStreet='';
        con4.MailingPostalCode='552602';
        con4.MailingState='SA';
        con4.Email='test@gmail.com';
        contactList.add(con4);      
        insert contactList;     
        Test.stopTest();
        con4.Mailing_Address1__c= 'New Jersey3';
        con4.MailingStreet='New Jersey3';
        con.MailingStreet='123, 456, 789, 012';
        con.department='Med ONC';
        update new list<Contact>{con,con4};
        delete con;
    
    }
    
    static testmethod void myconstructorTest1() {              
            
            Profile admin = OCSUGC_TestUtility.getAdminProfile();
            Profile portal = OCSUGC_TestUtility.getPortalProfile();
            List<User> lstUserInsert = new List<User>();
            User adminUser = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'SomeUser_001', null);
            //User adminUser = OCSUGC_TestUtility.createUser(false, admin.Id, '1');
            lstUserInsert.add(adminUser);
            Contact con4,con3;     
            Account accnt= OCSUGC_TestUtility.createAccount('test account', true);
            List<Contact> lstContactInsert = new List<Contact>();
            con3 = OCSUGC_TestUtility.createContact(accnt.Id, false);      
            lstContactInsert.add(con3);
            con4 = OCSUGC_TestUtility.createContact(accnt.Id, false);     
            lstContactInsert.add(con4);
            insert lstContactInsert; 
            Test.startTest();
            User usr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, con4.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);
            //User usr = OCSUGC_TestUtility.createUser(false, portal.Id, con4.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);  
            lstUserInsert.add(usr);
            insert lstUserInsert;
            System.runAs(adminUser) {
              con4.OCSUGC_UserStatus__c = OCSUGC_Constants.CONTACT_STATUS_DISABLE_SELF;
              update con4;
              List<User> checkCon4 = [SELECT Id,OCSUGC_Accepted_Terms_of_Use__c
                                      FROM User
                                      WHERE ContactId=:con4.Id
                                      LIMIT 1];
              System.assert(checkCon4!=null && checkCon4.size()>0 && !checkCon4[0].OCSUGC_Accepted_Terms_of_Use__c);
          }
            Test.stopTest();
    }
    
    }
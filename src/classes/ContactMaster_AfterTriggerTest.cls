@isTest
private class ContactMaster_AfterTriggerTest {
    
    @isTest static void test_method_one() {
        Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='USA'  ); 
        insert a;  
        Test.startTest();
            Contact con = new Contact(FirstName = 'TestContact1', LastName = 'TestContact1', Email = 'Test@1234APR123.com', MvMyFavorites__c='Meetings', AccountId = a.Id, MailingCity='New York', MailingCountry='USA', MailingPostalCode='552601', MailingState='CA', Phone = '12452234');
            //Creating a running user with System Admin profile
            insert con;
            con.Email = 'new123test@email.com';
            con.MyVarian_Member__c = true;
            update con;
        Test.stopTest();
    }
    
}
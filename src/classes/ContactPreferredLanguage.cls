/*************************************************************************\
Change Log:
13-Dec-2017 - Divya Hargunani - STSK0013401: Contact Preferred language is not adopting Account language - updated the logic to set the preferred language
****************************************************************************/

public class ContactPreferredLanguage{
    public static void UpdateContact(map<Id, account> newmap, map<Id, account> oldmap){
        
        Set<Id> setAccount = new Set<Id>();
        for(Account Acc : newmap.values()){
            if((Acc.Preferred_Language__c <> null && oldmap == null) || (Acc.Preferred_Language__c <> null && 
                    oldmap <> null && Acc.Preferred_Language__c <> oldmap.get(Acc.Id).Preferred_Language__c)){
                setAccount.add(Acc.Id);
            }
        }
        
        List<Contact> lstContact = new List<contact>();
        for(Account Acc : [select Id,Preferred_Language__c,(select Id,Preferred_Language1__c from Contacts) from Account where Id IN: setAccount]){
            if(Acc.Contacts <> null && Acc.Contacts.size()>0){
                for(Contact con :  Acc.Contacts){
                    con.Preferred_Language1__c = Acc.Preferred_Language__c;
                    lstContact.add(con);
                }
            }
            
        }
        
        if(lstContact.size() > 0 )
            update lstContact;
    }
    
    public static void UpdateAllContacts(map<Id, account> newmap, map<Id, account> oldmap){
        Set<Id> setAccount = new Set<Id>();
        map<Id,Account> mapAccSC = new map<Id,Account>();
        for(Account Acc : newmap.values()){
            if((Acc.Survey_Candidate__c <> null && oldmap == null) || (Acc.Survey_Candidate__c <> null 
                        && oldmap <> null && Acc.Survey_Candidate__c <> oldmap.get(Acc.Id).Survey_Candidate__c)){
                setAccount.add(Acc.Id);
                mapAccSC.put(Acc.Id,Acc);
            }
        }
        
        set<string> setFunctionalRole = new set<string>();
        setFunctionalRole.add('Chief/Dept Administrator');
        setFunctionalRole.add('Executive Management (CXO/Owner/VP)');
        setFunctionalRole.add('IT - Management,Nurse - Manager,Pharmacist - Chief');
        setFunctionalRole.add('Physician,Physicist');
        setFunctionalRole.add('Physicist - Chief,Therapist - Chief');
        
        List<Contact> lstContact = new List<contact>();
        
        for(Contact c:  [select Id,AccountId from Contact where AccountId IN: setAccount and Functional_Role__c IN: setFunctionalRole]){
            c.Annual_Relationship_Survey__c = mapAccSC.get(c.AccountId).Survey_Candidate__c;
            if(!c.Annual_Relationship_Survey__c)
            c.Survey_Comment__c = mapAccSC.get(c.AccountId).Survey_Comment__c;
            lstContact.add(c);
        }
        if(lstContact.size() > 0 )
            update lstContact;
    }
    
    //13-Dec-2017 - Divya Hargunani - STSK0013401: Contact Preferred language is not adopting Account language
    public static void UpdatePreferredLanguage(List<Contact> lstCon){
        set<Id> setAccount = new Set<Id>();
        for(Contact con : lstCon){
            if(con.Preferred_Language1__c == null){
                setAccount.add(con.AccountId);
            }
        }
        if(!setAccount.isEmpty()){
            List<Contact> lstContact = new List<contact>();
            map<Id,Account> mapAccount = new map<Id,Account>([select Id,Preferred_Language__c from Account where Id IN: setAccount]);
            for(Contact con : lstCon){
                if(con.Preferred_Language1__c == null && mapAccount.containsKey(con.AccountId)){
                    if(mapAccount.get(con.AccountId).Preferred_Language__c <> null){
                        con.Preferred_Language1__c =  mapAccount.get(con.AccountId).Preferred_Language__c;
                    }else if(mapAccount.get(con.AccountId).Preferred_Language__c == null){
                        con.Preferred_Language1__c = 'English';
                    }
                }
                
            }
        }
    }

}
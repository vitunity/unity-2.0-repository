/*************************************************************************\
Change Log:
13-Dec-2017 - Divya Hargunani - STSK0013401: Contact Preferred language is not adopting Account language  - Increased the code coverage
****************************************************************************/

@isTest
public class ContactPreferredLanguage_test{

    static testMethod void testPreferredLanguage(){
  
        Account acc = new Account();
        acc.Name ='Test Account';
        acc.BillingCountry='USA';
        acc.Country__c= 'USA';
        acc.BillingPostalCode = '90020';
        acc.OMNI_Country__c = 'USA';  
        acc.OMNI_Postal_Code__c = '90020';
        acc.Preferred_Language__c = '';
        acc.BillingCity = 'CA';
        insert acc;    
        
        Contact con = new Contact();
        con.FirstName='Test';
        con.LastName='Test';
        con.email = 'test@varian.com';
        con.MobilePhone = '1234567891';
        con.Phone = '1234567891';
        con.MailingCity= 'Los Angeles';
        con.MailingStreet ='3333 W 2nd St';
        con.MailingState ='CA';
        con.MailingPostalCode = '90020';
        con.MailingCountry = 'USA';
        con.Preferred_Language1__c = '';
        con.Accountid= acc.id;
        insert con;
        
        acc.Preferred_Language__c = 'Japanese';
        update acc;
        
        Contact newCon = new Contact();
        newCon.FirstName='Test';
        newCon.LastName='new';
        newCon.email = 'test123@varian.com';
        newCon.MobilePhone = '1234567891';
        newCon.Phone = '1234567891';
        newCon.MailingCity= 'Los Angeles';
        newCon.MailingStreet ='3333 W 2nd St';
        newCon.MailingState ='CA';
        newCon.MailingPostalCode = '90020';
        newCon.MailingCountry = 'USA';
        newCon.Preferred_Language1__c = '';
        newCon.Accountid= acc.id;
        
        insert newCon;
    }
}
public with sharing class ContactToolPickerController {
    public Contact_Role_Association__c contactRoleAssociation{get;set;}
    public Contact contact{get;set;}
    
    public ContactToolPickerController(){
        contactRoleAssociation = new Contact_Role_Association__c();
        contact = new Contact();
    }
    
    public void collectContactInfo(){
        system.debug('****contactRoleAssociation.Contact__c***'+contactRoleAssociation.Contact__c);
        if(contactRoleAssociation.Contact__c == null){
            return;
        }
        contact = [Select Id, Name, Manager__c, Is_Lung_Phantom_Contact__c,Is_MDADL__c,Manager_s_Email__c, 
                    Monte_Carlo_Registered__c, Siemens_Employee__c, Speakers_Bureau__c, MarketingKit_Agreement__c, 
                    TrueBeam_Accepted__c, Siemens_Customer__c, Registered_Portal_User__c, OktaId__c, OktaLogin__c, 
                    activation_url__c, MyVarian_Registration_Date__c 
                    From Contact where Id =: contactRoleAssociation.Contact__c];
                    
    }
    
    public Pagereference save(){
        update contact;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Your changes have been saved.');
        ApexPages.addMessage(myMsg);
        return null;
    }
    
    public Pagereference saveAndChangeContact(){
        update contact;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Your changes have been saved.');
        ApexPages.addMessage(myMsg);
        return new Pagereference('/apex/MyVarianAdminToolTab?tabSelected=contactTool');
    }
    
    public Pagereference cancel(){
        return new Pagereference('/apex/MyVarianAdminToolTab?tabSelected=contactTool');
    }
}
@isTest
private class ContactToolPickerControllerTest {

    static testMethod void myUnitTest() {
        // Create Contact.
        Contact contact = new Contact();
        contact.FirstName = 'test firstname';
        contact.LastName = 'test lastname';
        contact.Email = 'test@demo.com';
        contact.Manager_s_Email__c = 'manager@test.com';
        contact.MailingCountry = 'Canada';
        contact.MailingState = 'state';
        
        insert contact;
        
        // Create Contact Role association.
        Contact_Role_Association__c contactRoleAssociation = new Contact_Role_Association__c();
        contactRoleAssociation.Contact__c = contact.Id;
        
        ContactToolPickerController controller = new ContactToolPickerController();
        controller.contactRoleAssociation = contactRoleAssociation;
        controller.collectContactInfo();
        contact.Manager_s_Email__c = 'manager@abc.com';
        controller.save(); 
        system.assertEquals(contact.Manager_s_Email__c, 'manager@abc.com');
        Pagereference pg = controller.saveAndChangeContact();
        system.assert(pg != null);
        Pagereference cancel = controller.cancel();
        system.assert(cancel != null);
    }
}
/********************************************************************************************************
@Author: Rajamohan Vakati
@Date: Mar 3,2017
@Description: ContactTriggerHandler is contains the logic for all trigger handler status.

Change log -
26-Dec-17 - STSK0013523 - Nilesh Gorle - Deactivating 'Pending Removal' Account's Contact. Add new method 'deactivateContactFromAcct'
*********************************************************************************************************/
public without sharing class ContactTriggerHandler extends TriggerHandler  {
    
    // public String leadOwnerName ='' ;
    private static Set<String> countyMaping = new Set<String>() ; 
    public ContactTriggerHandler() {
        for( Regulatory_Country__c c:[Select id , Name from Regulatory_Country__c]) {
            if(c.Name.toLowerCase()!=null)
                countyMaping.add(c.Name); 
        }
    }

    /**
    * This method is used to perform the action on before insert any lead record . 
    * @param None 
    * @return - Void 
    */
    public override void beforeInsert() {
        countyCheck(trigger.new) ; 
        
    }

    /**
    * This method is used to perform the action on before update any lead record . 
    * @param None 
    * @return - Void 
    */
    public override void beforeUpdate() {
        countyCheck(trigger.new) ; 
    }

    public static void countyCheck(List<Contact> con) {
        system.debug('==countyMaping=='+countyMaping);

        for(Contact c : con){
            if(c.MailingCountry!=NULL ){
                //Rishabh Verma : Added if condition to avoid test class failures
                if(!test.isRunningTest()) {
                    if(!countyMaping.contains(c.MailingCountry)){
                    
                    /* HarleenGulati.4/13/2017, error message text corrected */
                    
                        c.addError(' The country you have entered is not found. '+ matchingCountry(c.MailingCountry)); 
                        
                    } 
                }
            }
        }
    }

    public static String matchingCountry(String contactCntName) {
        String countryValues ='' ; 
        If(contactCntName.length()>2) {
            contactCntName = contactCntName.substring(0, 2) ;
        }
        /* HarleenGulati.4/13/2017
            Changes requested by Dolores. 
            Changes made : added Integer counter variable 
            corrected error text as request by Dolores */
        Integer count = 1;  
        for(String c :  countyMaping){
            if(c.startsWithIgnoreCase(contactCntName)) {
                if(count==1) {
                    countryValues = 'Possible valid options are : ';
                }
                countryValues = countryValues + c + ', ';
                count=0; 
            }
        }
        if(countryValues!='') {
            countryValues = countryValues.substring(0 , countryValues.length()-2);
        } else {
            countryValues ='' ;

        }
        return countryValues ;
    }
    
    /* STSK0013523 - Nilesh Gorle - Deactivating 'Pending Removal' Account's Contact */
    public static void deactivateContactFromAcct(List<Account> acctList) {
        List<Id> accIdList = new List<Id>();
        /* Get all account Id list if acct staus is 'Pending Removal' */
        for (Account acc : acctList) {
            if (acc.Account_Status__c == 'Pending Removal') {
                accIdList.add(acc.Id);
            }
        }

        /* Get Contact List from Account IDs */
        List<Contact> conts = [Select Id, Active__c, Inactive_Contact__c From Contact Where AccountId IN :accIdList And MyVarian_Member__c = true];
        if (conts.size() > 0) {
            for (Contact cont: conts) {
                /* Uncheck Active__c field and  Check Inactive_Contact__c field for Contact */
                cont.Active__c = false;
                cont.Inactive_Contact__c = true;
                cont.MyVarian_Member__c=false;
            }

            update conts;
        }
    }
}
/********************************************************************************************************
@Author: 
@Date: 
@Description: 

Change log -
STSK0013523 - Nilesh Gorle - testing new method 'deactivateContactFromAcct' for test coverage
*********************************************************************************************************/

@isTest
private class ContactTriggerHandler_Test {
    private static testmethod void testCase(){
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='India' ; 
        insert reg ;

        Account a = new Account();
        a.Name = 'My Varian GPO Account';
        a.BillingCountry='USA';
        a.Country__c= 'India';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'GPO';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';
        insert a ;
        
        Contact con = new Contact() ; 
        con.FirstName='Test';
        con.LastName = 'Testing ' ; 
        con.Email='test@test.com';
        con.MailingCountry='India';
        con.MailingCity= 'Los Angeles';
        con.MailingStreet ='3333 W 2nd St';
        con.MailingState ='CA';
        con.MailingPostalCode = '90020';
        con.MyVarian_Member__c = True;
        con.AccountId = a.id ;

        insert con ;
        con.MailingState ='CA';
        update con ;

        ContactTriggerHandler ct = new ContactTriggerHandler();
        try{
            ct.beforeInsert();
            ct.beforeUpdate();
        }catch(Exception e){}

        ContactTriggerHandler.matchingCountry('India');

        /* Start - STSK0013523 - Nilesh Gorle - testing new method 'deactivateContactFromAcct' for test coverage */
        List<Account> acctList = new List<Account>();
        a.Account_Status__c = 'Pending Removal';
        acctList.add(a);
        ContactTriggerHandler.deactivateContactFromAcct(acctList);
        /* End - STSK0013523 - Nilesh Gorle - testing new method 'deactivateContactFromAcct' for test coverage */
    }
}
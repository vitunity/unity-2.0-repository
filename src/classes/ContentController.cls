@RestResource(urlMapping='/ContentVersion/*')
    global class ContentController 
    {
    @HttpGet
    global static void getBlob() {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
     res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('ContentDocumentId') ;
    ContentVersion a = [SELECT Id, ContentDocumentId, FileExtension, FileType, ContentSize, ContentUrl, VersionData FROM ContentVersion where ContentDocumentId=:Id];
   // res.addHeader('Content-Type','application/pdf');
    res.addHeader('Content-Type','application/jpg');
    res.responseBody = a.VersionData;
    }
    }
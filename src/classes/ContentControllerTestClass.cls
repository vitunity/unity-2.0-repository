@istest
private class ContentControllerTestClass {
  /*
 static testMethod void myContentControllerTest() {
     ContentVersion contentVersion_1 = new ContentVersion(
      Title = 'Penguins',
      PathOnClient = 'Penguins.jpg',
      VersionData = Blob.valueOf('Test Content')   
      //IsMajorVersion = true
    );
     insert contentVersion_1;
     
     RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();
      req.requestURI = '/services/apexrest/ContentVersion'; 
        RestContext.request = req;
        RestContext.response = res;
         req.httpMethod = 'GET';
    //List<ContentVersion> n= ContentController.get('ContentDocumentId') ;
      //  List<VU_Products__c>  p= CategoryProducts.getBlob();
      ContentController.getBlob();
 }
 */
 static testMethod void myContentControllerTest2() 
    {
        ContentVersion testContentInsert =new ContentVersion(); 
            testContentInsert.ContentURL='http://www.google.com/';
            testContentInsert.Title ='Google.com'; 
        insert testContentInsert; 
        ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id];
        String contentId = testContent.ContentDocumentId;
        system.debug('---ContentID---'+contentId);
        system.debug('---ContentID latest---'+testContentInsert.isLatest);
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ContentVersion'; 
        req.addParameter('ContentDocumentId',contentId);
        RestContext.request = req;
        RestContext.response = res;
        req.httpMethod = 'GET';
        ContentController.getBlob();
        
    }   
 
}
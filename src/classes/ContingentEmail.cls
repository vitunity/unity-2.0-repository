/*
 * Send contingency email for below condition
 * in between 30 and 45 days
 * in less than or equal 14 days
 */
public class ContingentEmail {

    private Id pobjQuoteId;
    private String emailTemplate;
    private List<String> emailToList;
    private List<String> emailCCList;

    public ContingentEmail(Id pobjQuoteId, String emailTemplate, list<string> emailToList, list<string> emailCCList){
        this.pobjQuoteId = pobjQuoteId;
        this.emailTemplate = emailTemplate;
        this.emailToList = emailToList;
        this.emailCCList = emailCCList;
    }
    
    public void sendContingentEmail() {
        BigMachines__Quote__c pobjQuote = [Select Id,Name,Order_Type__c, Service_Org__c, SAP_Prebooked_Sales__c, Quote_Region__c, BigMachines__Account__r.BillingCountry, 
                                            SAP_Prebooked_Service__c, SAP_Booked_Service__c, Ship_To_Country__c, SAP_Booked_Sales__c , Fulfillment_Quote__c, Subscription_Status__c, 
                                            Brachy_Sales__c, Price_Group__c, OwnerId, Submitted_To_SAP_By__c,Project_Manager__c,BigMachines__Account__r.Country__c
                                            From BigMachines__Quote__c Where id =: pobjQuoteId];

        List<EmailTemplate> lstEmailTemplates = [ select Id,Name, DeveloperName 
                                                    from EmailTemplate 
                                                    where Name =: emailTemplate limit 1];

        System.debug('-----Email Template Name-------'+lstEmailTemplates);
        System.debug('-----Quote-------'+pobjQuote);
        System.debug('-----emailToList-------'+emailToList);
        System.debug('-----emailCCList-------'+emailCCList);

        if (!lstEmailTemplates.isEmpty()) {
            try {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                string [] toAddress = new List<String>();
                for(String s : emailToList)
                    toAddress.add(s.trim());

                string [] ccAddress = new List<String>();
                for(String s : emailCCList)
                    ccAddress.add(s.trim());

                mail.setToAddresses(toAddress);
                mail.setCcAddresses(ccAddress);
                User userRec = [Select Id, Email from User where id = :Userinfo.getUserId() limit 1];
                mail.setTargetObjectId(userRec.Id);
                mail.setWhatId(pobjQuoteId);
                mail.setTemplateId(lstEmailTemplates[0].Id);
                mail.setOrgWideEmailAddressId(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c);
                mail.setSaveAsActivity(false);
                List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

                System.debug('----Email'+mail);
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            } catch(Exception e) {
                System.debug('Mail Sending Failed !');
            }
        }
    }
}
/*
@ Author          : Shital Bhujbal
@ Date            : 01-Aug-2018
@ Description     : This class is created to create Opportunity and send email notification for the service contracts which are expiring in 16 or 12 or 9 Months.
*/
global class ContractUpliftEmailBatch implements Database.batchable<sObject>{
   
   Public Boolean isExternal;
   Public String opptyName;
   Public Boolean leapYear;
   Opportunity opp;
   Public SVMXC__Service_Contract__c tempContract;
   Public List<String> emails=new List<String>();
   Public List<Opportunity> oppList = new List<Opportunity>();
   List<String> softwarePCSNs = new List<String>{'HIT','HML','H48','H62','H6T'};
   Public List<SVMXC__Service_Contract__c> contractsToUpdateList = new List<SVMXC__Service_Contract__c>();
   Public Map<Opportunity,SVMXC__Service_Contract__c> contractsToSendEmail = new Map<Opportunity,SVMXC__Service_Contract__c>();
   
   //Variables for new logic
   Public List<String> opportunityStages = new List<String>{'0 - NEW LEAD','1 - QUALIFICATION (N/A)',
                                              '2 - NEEDS ANALYSIS (N/A)',
                                              '3 - VALUE PROPOSITION (NEGOTIATION)',
                                              '4 - FINAL PROPOSAL (ORDER DOCUMENT)',
                                              '5 - NEGOTIATION (TENDER AWARDED)'};
   Map<String,List<SVMXC__Service_Contract__c>> PCSN_ServiceContractMap = New Map<String,List<SVMXC__Service_Contract__c>>();
   Public List<Opportunity> autoRenewalOppList = new List<Opportunity>();
   Map<String,Opportunity> PCSN_OpportunityMap = New Map<String,Opportunity>();
   Map<String,Opportunity> PCSN_ExistingOpportunityMap = New Map<String,Opportunity>();
   //Map<String,Opportunity> oppToInsertMap= new Map<String,Opportunity>();
   String PCSNKey;
   List<SVMXC__Service_Contract__c> tempContractList= new List<SVMXC__Service_Contract__c>();
   Public Map<String,List<SVMXC__Service_Contract__c>> oppContractMap = new Map<String,List<SVMXC__Service_Contract__c>>();
   Public Map<String,Boolean> oppNameAndFlagMap = new Map<String,Boolean>();  
   Public Map<Opportunity,Boolean> oppAndFlagFinalMap = new Map<Opportunity,Boolean>();
   String tempMasterEquipment;
   
   global Database.QueryLocator start(Database.BatchableContext BC) {
       
        String query = 'SELECT Id,ownerId,Name,SVMXC__End_Date__c,Auto_Renewal_Opportunity__c,SVMXC__Company__r.Software_Sales_Manager__c,ERP_Contract_Type__c,ERP_Order_Reason__c,SVMXC__Company__r.Name,Auto_Renewal_Opportunity__r.Contract_End_Date__c,SVMXC__Company__r.Auto_Renewal_Customer_Contact_Email__c,Master_Equipment__c,ERP_PO_Number__c,'
                       +'SVMXC__Company__r.Auto_Renewal_Customer_Contact__c,SVMXC__Company__r.SFDC_Account_Ref_Number__c,SVMXC__Company__r.Country__c,Auto_Renewal_Opportunity__r.StageName,Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c,SVMXC__Company__c,SVMXC__Company__r.Service_Sales_Manager__r.email '
                       +'FROM SVMXC__Service_Contract__c where ERP_Sales_Org__c =\'0600\' AND Master_Equipment__c!=\'\'';
                       //+'FROM SVMXC__Service_Contract__c where Id =\'a1h44000000wHKB\' AND Master_Equipment__c!=\'\'';
       
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.batchableContext bc, List<SVMXC__Service_Contract__c> scope){
        system.debug('scope=='+scope+'==>>'+scope.size());
        //Get existing Auto renewal Opportunities
        autoRenewalOppList = [select id,Name,Account.Name,Account.Auto_Renewal_Customer_Contact__r.Name,Account.District_Sales_Manager__r.Name,
                              Master_Equipment_On_Opportunity__c,Contract_End_Date__c,Service_Contract_reference_number__c
                              from Opportunity where Name LIKE 'Auto-Renewal%'];
        
        for(Opportunity oppty : autoRenewalOppList){
            if(oppty.Master_Equipment_On_Opportunity__c!='' && oppty.Contract_End_Date__c!=null){
                              
                 PCSN_ExistingOpportunityMap.put(oppty.Master_Equipment_On_Opportunity__c+'~'+oppty.Contract_End_Date__c,oppty);               
            }  
        }
        system.debug('PCSN_ExistingOpportunityMap=='+PCSN_ExistingOpportunityMap);
        // Logic to retrive sales order number
        Map<String,string> contractMap=new Map<String,string>();
        Map<String,string> contractSoldToMap=new Map<String,string>();
        for(SVMXC__Service_Contract__c sc : scope){
            if(string.isNotBlank(sc.ERP_PO_Number__c) && sc.ERP_PO_Number__c!='NONE PROVIDED'){
                contractMap.put(sc.name,sc.ERP_PO_Number__c);
            }
        }
        
        for(Sales_Order__c so:[select id,name,Sold_To__c,Sold_To__r.name from Sales_Order__c where name in :contractMap.values()]){
            if(so.Sold_To__c!=null){
                contractSoldToMap.put(so.name,so.Sold_To__c+'~'+so.Sold_To__r.name);
            }
        }

        //New logic ends
        system.debug('**********contractSoldToMap*******'+contractSoldToMap);
        system.debug('**********PCSN_OpportunityMap BEFORE*******'+PCSN_OpportunityMap.size());
        for(SVMXC__Service_Contract__c contract : scope){
            
            Date contractEndDate = contract.SVMXC__End_Date__c;
            Date todayDate = date.today();
            Integer numberOfDaysBetween = todayDate.daysBetween(contractEndDate);
            leapYear = Date.isLeapYear(todayDate.year());
            
            Integer sixteenMonthsinDays;
            Integer twelveMonthsinDays; 
            Integer nineMonthsinDays;  
            Integer eightMonthsinDays;
            Integer sixMonthsinDays; 
            Integer threeMonthsinDays;
            Integer fourtyFiveDays;
            Integer thirtyDays;
                        
            opp = new Opportunity();
            // logic to take Account Id from sales order and assign it to Auto Renewal Opportunity for bundled/combined quote
            //system.debug('==contract.ERP_PO_Number__c=='+contract.ERP_PO_Number__c+'===>>'+contractSoldToMap.get(contract.ERP_PO_Number__c));
            
            opp.Account_Sector__c = 'Private';
            opp.Payer_Same_as_Customer__c = 'No';
            opp.Expected_Close_Date__c = contractEndDate - 30;
            opp.CloseDate = contractEndDate - 30;
            opp.StageName = '0 - NEW LEAD';
            //opp.Primary_Contact_Name__c = '0030v000009Dw8V'; // Populate this with Auto renewal contact name //Auto_Renewal_Customer_Contact__c
            //opp.Primary_Contact_Name__c = contract.SVMXC__Company__r.Auto_Renewal_Customer_Contact__c;
            opp.CurrencyIsoCode = 'USD';//Get it from Account country
            opp.Deliver_to_Country__c = contract.SVMXC__Company__r.Country__c;//Get it from Account
            opp.Existing_Equipment_Replacement__c = 'No';
            opp.Auto_Renewal_Opportunity__c = true;
            //UAT start
            if(contract.Master_Equipment__c!=NULL && contract.Master_Equipment__c!=''){
                tempMasterEquipment = contract.Master_Equipment__c.substring(0,3);
                system.debug('***tempMasterEquipment****'+tempMasterEquipment);
                system.debug('***softwarePCSNs.contains(tempMasterEquipment)****'+softwarePCSNs.contains(tempMasterEquipment));
                
                if(softwarePCSNs.contains(tempMasterEquipment) && contract.SVMXC__Company__r.Software_Sales_Manager__c!=NULL){
                    system.debug('***tempMasterEquipment****'+tempMasterEquipment);
                    opp.ownerid = contract.SVMXC__Company__r.Software_Sales_Manager__c;
                }else if(contract.SVMXC__Company__r.Service_Sales_Manager__c!=NULL){
                    opp.ownerid = contract.SVMXC__Company__r.Service_Sales_Manager__c;
                }else{
                    opp.ownerid = contract.ownerid;
                }
            }
            //UAT END
            
            if(leapYear){
                
                sixteenMonthsinDays = 488;
                twelveMonthsinDays = 366;
                //nineMonthsinDays = 274;
                eightMonthsinDays = 244;
                sixMonthsinDays = 183;
                threeMonthsinDays = 92;
                fourtyFiveDays = 46;
                thirtyDays = 31;
            }else{
                
                sixteenMonthsinDays = 487;
                twelveMonthsinDays = 365;
                //nineMonthsinDays = 273;
                eightMonthsinDays = 243;
                sixMonthsinDays = 182;
                threeMonthsinDays = 91;
                fourtyFiveDays = 45;
                thirtyDays = 30;
            }
            String soldToAccName='';
            //Create Opportunity for all the contracts in past 16 Months range from today
            //if(numberOfDaysBetween<= sixteenMonthsinDays && contract.Auto_Renewal_Opportunity__c== NULL){
            if(numberOfDaysBetween > 0 && numberOfDaysBetween<= sixteenMonthsinDays && contract.Auto_Renewal_Opportunity__c== NULL){
                // logic to take Account Id from sales order and assign it to Auto Renewal Opportunity for bundled/combined quote
                if(string.isNotBlank(contract.ERP_PO_Number__c)&& contractSoldToMap.get(contract.ERP_PO_Number__c)!=null){
                    string tempStr=contractSoldToMap.get(contract.ERP_PO_Number__c);
                    List<String> lstSoldToAcc = tempStr.split('~');
                    opp.AccountId=lstSoldToAcc[0];
                    soldToAccName=lstSoldToAcc[1];
                }
                else{
                    opp.AccountId = contract.SVMXC__Company__c;
                }
                
                //New logic starts******
                system.debug('Inside Opty null if*****'+contract.Name);
                PCSNKey = contract.Master_Equipment__c+'~'+contract.SVMXC__End_Date__c;
                system.debug('PCSNKey=='+PCSNKey);
                if(PCSN_ExistingOpportunityMap.containsKey(PCSNKey)){
                    
                    opp = PCSN_ExistingOpportunityMap.get(PCSNKey);
                    
                    if(opp.Service_Contract_reference_number__c!=''){
                        opp.Service_Contract_reference_number__c = opp.Service_Contract_reference_number__c +','+contract.Name;
                    }else{
                        opp.Service_Contract_reference_number__c = contract.Name;
                    }

                    PCSN_OpportunityMap.put(PCSNKey,opp);
                    PCSN_ExistingOpportunityMap.put(PCSNKey,opp);
                    //system.debug('***oppContractMap before*********'+oppContractMap);
                    
                    if(oppContractMap.get(opp.Name)!=null){
                        List<SVMXC__Service_Contract__c> contList = new list<SVMXC__Service_Contract__c>();
                        contList = oppContractMap.get(opp.Name);
                        contList.add(contract);
                        oppContractMap.put(opp.Name,contList);
                    }else{
                        List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
                        contractList.add(contract);
                        oppContractMap.put(opp.Name,contractList);
                    }
                    system.debug('***oppContractMap after*********'+oppContractMap);
                    isExternal = false;      
                    //contractAndFlagMap.put(contract,isExternal);
                    oppNameAndFlagMap.put(opp.Name,isExternal);
                }else{
                    system.debug('********New logic else****'+contract.Name);
                    //opptyName = 'Auto-Renewal# '+contract.Master_Equipment__c+'#'+contract.SVMXC__Company__r.Name+'#'+contract.SVMXC__End_Date__c.format();
                    //if(string.isNotBlank(contract.ERP_PO_Number__c)){
                    if(soldToAccName!='' && string.isNotBlank(contract.ERP_PO_Number__c)){
                        opptyName = 'Auto-Renewal# '+contract.Master_Equipment__c+'#'+soldToAccName+'#'+contract.SVMXC__End_Date__c.format();
                    }
                    else{
                        opptyName = 'Auto-Renewal# '+contract.Master_Equipment__c+'#'+contract.SVMXC__Company__r.Name+'#'+contract.SVMXC__End_Date__c.format();
                    }
                    if(opptyName.length() > 120){
						opptyName = 'Auto-Renewal# '+contract.Master_Equipment__c+'#'+contract.SVMXC__Company__r.SFDC_Account_Ref_Number__c+'#'+contract.SVMXC__End_Date__c.format();
					}
                    opp.Name = opptyName;
                    opp.Service_Contract_reference_number__c = contract.Name;
                    opp.Contract_End_Date__c = contractEndDate;
                    opp.Master_Equipment_On_Opportunity__c = contract.Master_Equipment__c;
                
                    PCSN_OpportunityMap.put(PCSNKey,opp);
                    PCSN_ExistingOpportunityMap.put(PCSNKey,opp);
                    //oppContractMap.put(opp.Name,contract);
                    List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
                    contractList.add(contract);
                    oppContractMap.put(opp.Name,contractList);
                    isExternal = false;      
                    //contractAndFlagMap.put(contract,isExternal);
                    oppNameAndFlagMap.put(opp.Name,isExternal);
                }
                //New logic ends*******
            }
            
            //Send email if the contract is expiring in 12 months
            //if(numberOfDaysBetween== twelveMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && contract.Auto_Renewal_Opportunity__r.StageName != 'Closed-won'){  
            if(numberOfDaysBetween== twelveMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){  
                    isExternal = false;
                    //contractAndFlagMap.put(contract,isExternal);
                    //oppAndFlagMap.put(opp,isExternal);
                    //oppNameAndFlagMap.put(opp.Name,isExternal);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        //contractsToSendEmail.put(opp,contract);
                    }
            }    
            
            //Send email if the contract is expiring in 8 months
            if(numberOfDaysBetween== eightMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){ 
                    isExternal = true;
                    //contractAndFlagMap.put(contract,isExternal);
                    //contractsToSendEmail.add(contract);
                    system.debug('*****nineMonthsinDays****'+numberOfDaysBetween);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        contractsToSendEmail.put(opp,contract);
                    }
            }
            //Send email if the contract is expiring in 6 months
            //if(numberOfDaysBetween== sixMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && contract.Auto_Renewal_Opportunity__r.StageName != 'Closed-won'){ 
            if(numberOfDaysBetween== sixMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){ 
                    isExternal = true;
                    //contractAndFlagMap.put(contract,isExternal);
                    //contractsToSendEmail.add(contract);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        contractsToSendEmail.put(opp,contract);
                    }
            }
            
            if(numberOfDaysBetween== threeMonthsinDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){ 
                    isExternal = true;
                    //contractAndFlagMap.put(contract,isExternal);
                    //contractsToSendEmail.add(contract);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        contractsToSendEmail.put(opp,contract);
                    }
            }
            
            if(numberOfDaysBetween== fourtyFiveDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){ 
                    isExternal = true;
                    //contractAndFlagMap.put(contract,isExternal);
                    //contractsToSendEmail.add(contract);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        contractsToSendEmail.put(opp,contract);
                    }
            }
            
            if(numberOfDaysBetween== thirtyDays && contract.Auto_Renewal_Opportunity__c!= NULL && opportunityStages.contains(contract.Auto_Renewal_Opportunity__r.StageName)){ 
                    isExternal = true;
                    //contractAndFlagMap.put(contract,isExternal);
                    //contractsToSendEmail.add(contract);
                    if(PCSN_ExistingOpportunityMap.containsKey(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c)){
                        system.debug('*****nineMonthsinDays1211****'+numberOfDaysBetween);
                        opp = PCSN_ExistingOpportunityMap.get(contract.Auto_Renewal_Opportunity__r.Master_Equipment_On_Opportunity__c+'~'+contract.Auto_Renewal_Opportunity__r.Contract_End_Date__c);
                        oppAndFlagFinalMap.put(opp,isExternal);
                        contractsToSendEmail.put(opp,contract);
                    }
            }
        }   
        //system.debug('**********PCSN_OpportunityMap AFTER*******'+PCSN_OpportunityMap.size());
        
        if(PCSN_OpportunityMap.size()>0){
            oppList = PCSN_OpportunityMap.values();
            //upsert oppList;
            database.upsert(oppList,false);
        }
        
        //Update Auto renewal opportunity field on contract
        for(Opportunity oppty : oppList){
            
            if(oppContractMap.containsKey(oppty.Name)){
                system.debug('-----count---');
                tempContractList = oppContractMap.get(oppty.Name);
                
                for(SVMXC__Service_Contract__c tempContract: tempContractList){
                    tempContract.Auto_Renewal_Opportunity__c = oppty.Id;
                    contractsToUpdateList.add(tempContract);  
                }
            }
            if(oppNameAndFlagMap.containsKey(oppty.Name)){
                system.debug('========oppty==='+oppty.Id);
                oppAndFlagFinalMap.put(oppty,oppNameAndFlagMap.get(oppty.Name));
            }
        }
        system.debug('oppAndFlagFinalMap*****'+oppAndFlagFinalMap.size()+'**size**'+oppAndFlagFinalMap);
        upsert contractsToUpdateList;
        
        //ContractUplift_SendEmail sendEmailContract = new ContractUplift_SendEmail(contractAndFlagMap); 
        ContractUplift_SendEmail sendEmailContract = new ContractUplift_SendEmail(oppAndFlagFinalMap,contractsToSendEmail); 
    }
    global void finish(Database.batchableContext BC){
    }
}
@istest
private class ContractUpliftEmailBatchTest {
	  static SVMXC__Service_Contract__c testServiceContract ;
      static Contact con;
      static Account acc;
      static Opportunity opp,oppNew,opptyNew,oppty;
       static {
            
        // insertAccount
            acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425',BillingCity='LA',BillingState='CA',State_Province_Is_Not_Applicable__c=true);
           acc.Software_Sales_Manager__c= UserInfo.getUserId();
           acc.District_Service_Manager__c= UserInfo.getUserId();
           acc.Service_Sales_Manager__c= UserInfo.getUserId();
           acc.District_Sales_Manager__c= UserInfo.getUserId();
           insert acc;
            
            // insert Contact  
            con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
            insert con;
           
            //insert Opportunity
            opp = new  Opportunity(Name ='Auto-Renewal#HIT1179F~'+string.valueof(system.today()+30)+' 00:00:00', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
            type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        	opp.Master_Equipment_On_Opportunity__c='HIT1179F';
            opp.Contract_End_Date__c=system.today()+30;
            insert opp;      
        }
    
    static testmethod void testContract() {     
        Test.startTest();
        
        testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+30, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_Header_Contract_Description__c='AR-Essentials HIT0604',
                                                             Auto_Renewal_Opportunity__c=opp.id,Master_Equipment__c='HIT1179F',ERP_Sales_Org__c='0600',OwnerId=UserInfo.getUserId());
        insert testServiceContract;
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testContract1() {     
        Test.startTest();
        
        oppNew = new  Opportunity(Name ='Auto-Renewal#HIT1179F~'+string.valueof(system.today()+30)+' 00:00:00', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        oppNew.Master_Equipment_On_Opportunity__c='HIT1179F';
        oppNew.Contract_End_Date__c=system.today()+243;
        insert oppNew;
        testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = oppNew.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+243, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_Header_Contract_Description__c='AR-Essentials HIT0604',
                                                             Auto_Renewal_Opportunity__c=oppNew.id,Master_Equipment__c='HIT1179F',ERP_Sales_Org__c='0600',OwnerId=UserInfo.getUserId());
        insert testServiceContract;
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testContract2() {     
        Test.startTest();
        
        opptyNew = new  Opportunity(Name ='Auto-Renewal#HIT1179F~'+string.valueof(system.today()+30)+' 00:00:00', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        opptyNew.Master_Equipment_On_Opportunity__c='HIT1179F';
        opptyNew.Contract_End_Date__c=system.today()+182;
        insert opptyNew;
        testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opptyNew.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+182, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_Header_Contract_Description__c='AR-Essentials HIT0604',
                                                             Auto_Renewal_Opportunity__c=opptyNew.id,Master_Equipment__c='HIT1179F',ERP_Sales_Org__c='0600',OwnerId=UserInfo.getUserId());
        insert testServiceContract;
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testContract3() {     
        Test.startTest();
        
        oppty = new  Opportunity(Name ='Auto-Renewal#HIT1179F~'+string.valueof(system.today()+30)+' 00:00:00', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        oppty.Master_Equipment_On_Opportunity__c='HEY785';
        oppty.Contract_End_Date__c=system.today()+91;
        insert oppty;
        testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = oppty.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+91, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_Header_Contract_Description__c='AR-Essentials HIT0604',
                                                             Auto_Renewal_Opportunity__c=oppty.id,Master_Equipment__c='HEY785',ERP_Sales_Org__c='0600',OwnerId=UserInfo.getUserId());
        insert testServiceContract;
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testNewOpp() {     
        //insert Opportunity
        Opportunity oppAutoRenew = new  Opportunity(Name = 'Auto-Renewal', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
                                           type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        oppAutoRenew.Master_Equipment_On_Opportunity__c='HIT1179M';
        oppAutoRenew.Contract_End_Date__c=system.today();
        insert oppAutoRenew;
        Test.startTest();
        SVMXC__Service_Contract__c testServiceContract1 = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'Test Auto',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+183, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_Header_Contract_Description__c='AR-Essentials HIT0604');
        testServiceContract1.OwnerId=UserInfo.getUserId();
        testServiceContract1.Master_Equipment__c='HIT1179M';
        testServiceContract1.ERP_Sales_Org__c='0600';
        insert testServiceContract1;
       
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testAutoRenewOpp() {     
       
        Sales_Order__c so = new Sales_Order__c();
        so.Name='3212345';
        so.Sold_To__c=acc.id;
        insert so;
        
        //insert Opportunity
       Opportunity oppAutoRenew = new  Opportunity(Name = 'Auto-Renewal', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '0 - NEW LEAD',
                                           type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today()+289, CloseDate =System.today());
        oppAutoRenew.Master_Equipment_On_Opportunity__c='HIT1179M';
        oppAutoRenew.Contract_End_Date__c=system.today()+243;
        insert oppAutoRenew;
        
        Test.startTest();
        SVMXC__Service_Contract__c testServiceContract1 = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'Test Auto',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+243, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678',ERP_PO_Number__c='3212345',ERP_Header_Contract_Description__c='AR-Essentials HIT0604');
        testServiceContract1.OwnerId=UserInfo.getUserId();
        testServiceContract1.Master_Equipment__c='HIT1179M';
        testServiceContract1.ERP_Sales_Org__c='0600';
        insert testServiceContract1;
       
        ContractUpliftEmailBatch bc=new ContractUpliftEmailBatch();
        DataBase.executeBatch(bc);
        Test.stopTest();

    }
    
    static testmethod void testSchedule() {     
        Test.startTest();
        ContractUpliftEmailscheduledBatchable conBatch=new ContractUpliftEmailscheduledBatchable();
        String sch = '0 0 23 * * ?';
		system.schedule('Test Contract Uplift Batch', sch, conBatch );
        Test.stopTest();
    }
}
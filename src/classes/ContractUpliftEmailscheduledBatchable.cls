global class ContractUpliftEmailscheduledBatchable implements Schedulable {
        
    global void execute(SchedulableContext sc) {
        ContractUpliftEmailBatch batch = new ContractUpliftEmailBatch();
        database.executeBatch(batch);         
    }
}
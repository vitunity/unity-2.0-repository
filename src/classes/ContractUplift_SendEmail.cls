/*
@ Author          : Shital Bhujbal
@ Date            : 01-Aug-2018
@ Description     : This class is created to create Opportunity and send email notification for the service contracts which are expiring in 16 or 12 or 9 Months.
*/
public class ContractUplift_SendEmail{
	private Id contractId;
    private String emailTemplate;
    List<Messaging.SingleEmailMessage> lstmail = new List<Messaging.SingleEmailMessage>();
    String emailTemplate1 = 'Contract Expiration Reminder email - Internal';
    String emailTemplate2 = 'Contract Expiration Reminder email - External';
    String toAddress;
    String toAddressExternal;
    List<String> setToAddressesList;
    List<String> softwarePCSNs = new List<String>{'HIT','HML','H48','H62','H6T'};
    Boolean softwareFlag;
    Boolean hardwreFlag = false;
    Set<Id> accountIds = new Set<Id>();

    public ContractUplift_SendEmail(Map<Opportunity,Boolean> oppAndFlagMap,Map<Opportunity,SVMXC__Service_Contract__c> contractList){
        sendEmail(oppAndFlagMap,contractList);
    }
    
    public void sendEmail(Map<Opportunity,Boolean> oppAndFlagMap,Map<Opportunity,SVMXC__Service_Contract__c> contractList) {

        //Query Contract                            
        Map<String,String> emailmap = new Map<String,String>();
        Map<Id,String> accountEmailAddress = new Map<Id,String>();
        
        List<EmailTemplate> lstEmailTemplates = [select Id,Name, DeveloperName 
                                                 from EmailTemplate 
                                                 where (Name = 'Contract Expiration Reminder email - Internal' OR Name = 'Contract Expiration Reminder email - External')];
        
        if(lstEmailTemplates!=null && lstEmailTemplates.size()>0){
            for(emailtemplate e: lstEmailTemplates){
                emailmap.put(e.name,e.id);
            }
        }
        
        for(opportunity opp : oppAndFlagMap.keySet()){
            accountIds.add(opp.accountId);  
        }
        
        Map<Id,account> accountMap = new Map<Id,account>([select id,name,Service_Sales_Manager__r.email,
                                                          Software_Sales_Manager__r.email,District_Sales_Manager__r.email,District_Service_Manager__r.email,
                                                          Auto_Renewal_Customer_Contact_Email__c from Account where id IN : accountIds]);
        
		system.debug('@@@@@@@accountMap------@@@'+accountMap);
		User userRec = [Select Id, Email from User where id = :Userinfo.getUserId() limit 1];
        
        //for(SVMXC__Service_Contract__c eachContract: contractMap.keySet()){
        for(Opportunity eachOpp: oppAndFlagMap.keySet()){
            
            softwareFlag =false;
            setToAddressesList = new List<String>(); 
            if(accountMap.containsKey(eachOpp.AccountId)){
                for(String softwarePCSN : softwarePCSNs){
                    if(eachOpp.Master_Equipment_On_Opportunity__c!='' && eachOpp.Master_Equipment_On_Opportunity__c.startsWithIgnoreCase(softwarePCSN)){
                        softwareFlag =true;
                        break;
                    }
                }
                //Send email to Software Manager if PCSN is software otherwise send email to Service manager
                if(softwareFlag==true && accountMap.get(eachOpp.AccountId).Software_Sales_Manager__r.email!= null){
                    setToAddressesList.add(accountMap.get(eachOpp.AccountId).Software_Sales_Manager__r.email);  
                //}else if(accountMap.get(eachOpp.AccountId).District_Service_Manager__r.email!= null){
                }
                //UAT change - Sofware email will go to District Manager as well
                if(accountMap.get(eachOpp.AccountId).District_Service_Manager__r.email!= null){
                    setToAddressesList.add(accountMap.get(eachOpp.AccountId).District_Service_Manager__r.email);
                }
                //Send email to District manager for both software and hardware PCSN
                if(accountMap.get(eachOpp.AccountId).Service_Sales_Manager__r.email!= null){
                    setToAddressesList.add(accountMap.get(eachOpp.AccountId).Service_Sales_Manager__r.email);
                }
                
                setToAddressesList.add('mona.agarwal@varian.com');
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(setToAddressesList); 
				mail.setTargetObjectId(userRec.Id);
				mail.setTemplateId(emailmap.get(emailTemplate1));
				mail.setWhatId(eachOpp.Id);
				mail.setSaveAsActivity(false);
				lstmail.add(mail);
				setToAddressesList.clear();
            
				//Send external email 
				system.debug('******OUtside if external****'+oppAndFlagMap.get(eachOpp)+'####'+contractList.get(eachOpp));
				if(oppAndFlagMap.get(eachOpp) && contractList.get(eachOpp)!=null && contractList.get(eachOpp).ERP_Contract_Type__c!='ZH3' && contractList.get(eachOpp).ERP_Order_Reason__c!='HS1 ISS Contract'){
					system.debug('******Inside if external****');
                    if(accountMap.containsKey(eachOpp.AccountId) && accountMap.get(eachOpp.AccountId).Auto_Renewal_Customer_Contact_Email__c!=null){
						setToAddressesList.add(accountMap.get(eachOpp.AccountId).Auto_Renewal_Customer_Contact_Email__c);
					}else{
						setToAddressesList.add('shital.bhujbal@varian.com');
					}
                
					setToAddressesList.add('mona.agarwal@varian.com');
                    setToAddressesList.add('shital.bhujbal@varian.com');
					Messaging.SingleEmailMessage externalMail = new Messaging.SingleEmailMessage();
					externalMail.setToAddresses(setToAddressesList); 
					externalMail.setTargetObjectId(userRec.Id);
					//externalMail.setWhatId(eachOpp.Id);
					externalMail.setWhatId(contractList.get(eachOpp).Id);
					externalMail.setTemplateId(emailmap.get(emailTemplate2));
					externalMail.setSaveAsActivity(false);
					lstmail.add(externalMail);                
				}
			}
			system.debug('lstmail*****'+lstmail);
		}
        
		Messaging.sendEmail(lstmail); //invoke here
	}
}
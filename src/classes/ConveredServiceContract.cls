public class ConveredServiceContract{
    public static void updateContract(list<SVMXC__Installed_Product__c> lstProduct, map<Id,SVMXC__Installed_Product__c> oldmap){
        List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
        set<id> setCovered = new set<Id>();
        for(SVMXC__Installed_Product__c i : lstProduct){
            if(i.Service_Contract_Line_IsActive__c && i.SVMXC__Service_Contract_Line__c <> null && 
            (oldmap == null || (oldmap <> null && 
            (i.Service_Contract_Line_IsActive__c <> oldmap.get(i.id).Service_Contract_Line_IsActive__c || 
            i.SVMXC__Service_Contract_Line__c   <> oldmap.get(i.id).SVMXC__Service_Contract_Line__c)
            ))
            ){
                lstInstalledProduct.add(i);
                setCovered.add(i.SVMXC__Service_Contract_Line__c);
            }           
        }
        
        map<Id,SVMXC__Service_Contract_Products__c> mapCoveredProduct = new map<Id,SVMXC__Service_Contract_Products__c>([select Id,SVMXC__Start_Date__c,SVMXC__End_Date__c,SVMXC__Service_Contract__c from SVMXC__Service_Contract_Products__c where id IN: setCovered]);
        for(SVMXC__Installed_Product__c i : lstInstalledProduct){
            SVMXC__Service_Contract_Products__c s = mapCoveredProduct.get(i.SVMXC__Service_Contract_Line__c);
            i.SVMXC__Service_Contract__c = s.SVMXC__Service_Contract__c;
            i.SVMXC__Service_Contract_Start_Date__c = s.SVMXC__Start_Date__c;
            i.SVMXC__Service_Contract_End_Date__c = s.SVMXC__End_Date__c;
        }
        
        for(SVMXC__Installed_Product__c i : lstProduct){
            if(!i.Service_Contract_Line_IsActive__c || i.SVMXC__Service_Contract_Line__c == null && 
            (oldmap == null || (oldmap <> null && 
            (i.Service_Contract_Line_IsActive__c <> oldmap.get(i.id).Service_Contract_Line_IsActive__c || 
            i.SVMXC__Service_Contract_Line__c   <> oldmap.get(i.id).SVMXC__Service_Contract_Line__c)
            ))
            ){
                i.SVMXC__Service_Contract__c = null;
                i.SVMXC__Service_Contract_Start_Date__c = null;
                i.SVMXC__Service_Contract_End_Date__c = null;
                i.SVMXC__Service_Contract_Line__c = null;
            }           
        }
    }
}
@isTest
private class ConveredServiceContract_Test
{   
    static testmethod void ConveredServiceContract()
    {
        test.starttest();

        // insertAccount
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //insert Opportunity
        Opportunity opp = new  Opportunity(Name = 'testOPP', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '30%',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        insert opp;
               
        
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(Reason_for_rejection__c='test',SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345');
        insert testServiceContractProduct;
                
                
         // insert parent IP var loc
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(SVMXC__Service_Contract_Line__c = testServiceContractProduct.Id,Name='H12345',SVMXC__Serial_Lot_Number__c ='H12345',SVMXC__Status__c ='Installed',ERP_Reference__c = 'H12345');
        insert objIP;
               
        objIP.SVMXC__Service_Contract_Line__c = null;
        update objIP;
        
        Test.stopTest();
        
        

        
    }  
}
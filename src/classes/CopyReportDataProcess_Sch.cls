/***********************************************************************
* Author         : Varian
* Description	 : scheduled process which copies public/private reports to custom object for audit purpose
* User Story     : 
* Created On     : 5/18/16
************************************************************************/
global with sharing class CopyReportDataProcess_Sch implements Schedulable {
	
	boolean collectPrivateReport = false;
	static String CRON_EXP = '0 0 * 1 * ?', SCH_PRVT_REPORT = 'SCH_COPY_PRIVATE_REPORTS';
	
	private static final String REPORT_QUERY_PUBLIC = 'Select OwnerId, NamespacePrefix, Name, LastViewedDate, LastRunDate, LastReferencedDate, LastModifiedDate,'
		+' LastModifiedById, Id, Format, FolderName, DeveloperName, Description, CreatedDate, CreatedById, isDeleted, Owner.Name From Report ',
		REPORT_PRIVATE_SCOPE = ' USING SCOPE allPrivate ', ORDER_BY = ' Order By LastModifiedDate desc ', ROWLIMIT = 'LIMIT 10000 ALL ROWS';
	
	public CopyReportDataProcess_Sch(boolean runPrivate) {	
		this.collectPrivateReport = runPrivate;
	}
	
	public CopyReportDataProcess_Sch() {	

	}
	
	global void execute(SchedulableContext sc) {
		this.copyReportData(this.collectPrivateReport); //only copies 10k
		if (!this.collectPrivateReport) {
			List<CronJobDetail> jobs = new List<CronJobDetail>();
			jobs = [Select c.Name, c.JobType, c.Id From CronJobDetail c where c.Name =:SCH_PRVT_REPORT];
			if (jobs == null || jobs.isEmpty()) {
				System.schedule(CopyReportDataProcess_Sch.SCH_PRVT_REPORT, this.nextRunCronExpr(), new CopyReportDataProcess_Sch(true));	
			}
		}
	}
	 
	public void copyReportData(boolean isPrivate) {
		List<Report> reports = new List<Report>();
	 	String query = isPrivate ? CopyReportDataProcess_Sch.REPORT_QUERY_PUBLIC +''+CopyReportDataProcess_Sch.REPORT_PRIVATE_SCOPE: CopyReportDataProcess_Sch.REPORT_QUERY_PUBLIC;
	 	query = query +''+CopyReportDataProcess_Sch.ORDER_BY+' '+CopyReportDataProcess_Sch.ROWLIMIT;
	 	System.debug(logginglevel.info, 'Query ===== '+query);
	 	if (String.isNotBlank(query)) {
	 		reports = (List<Report>) Database.query(query); 	
	 	}
	 	if (reports.size() > 0) {
	 		List<Report__c> reportsData = new List<Report__c>();
	 		for (Integer i=0; i < reports.size(); i++) {
	 			reportsData.add(this.setReportData(reports[i]));	
	 		}	
	 		upsert reportsData ReportId__c;
	 	}
	}
    
    private Report__c setReportData(Report r) {
    	Report__c report = new Report__c(CreatedById__c = r.CreatedById, Description__c = r.Description, FolderName__c = r.FolderName, Format__c = r.Format, IsDeleted__c = r.IsDeleted, 
    							LastModifiedById__c = r.LastModifiedById, LastModifiedDate__c = r.LastModifiedDate, LastRunDate__c = r.LastRunDate, LastViewedDate__c = r.LastViewedDate, 
    							OwnerId__c = r.OwnerId, Owner__c= r.Owner.Name, ReportId__c = r.Id, Name = r.Name);
    	return report;
    }
    
    public String nextRunCronExpr() {
    	String expr = '0 0 '+(system.now().hour()+1)+' '+(system.now().hour() < 23 ? system.now().day() : system.now().day()+1)+' '+system.now().month()+' ? '+system.now().year();
    	System.debug(logginglevel.info, '==== expr ==== '+expr);
    	return expr;
    }
}
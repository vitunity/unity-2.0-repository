/***********************************************************************
* Author         : Varian
* Description	 : Test class for CopyReportDataProcess_Sch scheduled process
* User Story     : 
* Created On     : 5/31/16
************************************************************************/
//Note SeeAllData is required since Report and Folder are system object which can't be created in unit test
@isTest (SeeAllData=true) 
private class CopyReportDataProcess_Sch_Test {
    
    static String CRON_EXP = '0 0 * 1 * ?';
    
    @isTest static void testPublicReport() {
    	System.runAs(TestDataFactory.createUser('System Administrator')) {
    		Datetime beforeStart = System.now();
    		Test.startTest();
    		Id jobId = System.schedule('CopyReportScheduler_TestPublic', CRON_EXP, new CopyReportDataProcess_Sch());
			System.assert(jobId != null);
    		Test.stopTest();
    		List<Report__c> reports = [Select Id, Name From Report__c where LastModifiedDate >= :beforeStart AND FolderName__c != 'Private Reports'];
    		System.assert(reports.size() > 0);
    	}	
    }
    
    
    @isTest static void testPrivateReport() {
    	System.runAs(TestDataFactory.createUser('System Administrator')) {
    		Datetime beforeStart = System.now();
    		Test.startTest();
    		CopyReportDataProcess_Sch copyProcess = new CopyReportDataProcess_Sch(true);
    		Id jobId = System.schedule('CopyReportScheduler_TestPrivate', CRON_EXP, copyProcess);
			System.assert(jobId != null);
			System.assert(copyProcess.nextRunCronExpr() != null);
    		Test.stopTest();
    		List<Report__c> reports = [Select Id, Name From Report__c where LastModifiedDate >= :beforeStart AND FolderName__c = 'Private Reports'];
    		System.assert(reports.size() > 0);
    	}	
    }
}
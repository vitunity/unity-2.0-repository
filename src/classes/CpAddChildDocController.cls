public class CpAddChildDocController 
{
    public Id docId {get; set;}
    public Id parentId {get; set;}
    public String flag {get; set;}
    public ContentVersion currParent {get; set;}
    
    public CpAddChildDocController(ApexPages.StandardController controller) 
    {
        parentId = ApexPages.currentPage().getParameters().get('contId');
        currParent = [SELECT Id, TagCsv, ContentDocumentId, FirstPublishLocationId, ContentSize, Document_Version__c, Mutli_Languages__c, Document_Type__c, Region__c FROM ContentVersion WHERE Id = :parentId and IsLatest=True];
        flag = 'detail';
    }
    
    public pagereference saveToAddLib()
    {
        if(!currParent.Mutli_Languages__c)
        {
            currParent.Mutli_Languages__c = true;
            update currParent;
        }
        Set<Id> libId = new Set<Id>();
        List<ContentWorkspaceDoc> conWrkDocListNew = new List<ContentWorkspaceDoc>();
        List<ContentWorkspaceDoc> conWrkDocList = [SELECT Id, ContentDocumentId, ContentWorkspaceId FROM ContentWorkspaceDoc WHERE ContentDocumentId = :currParent.ContentDocumentId];
        ContentVersion currChildDoc = [SELECT Id, FirstPublishLocationId, ContentDocumentId FROM ContentVersion WHERE Id = :docId];
        Id contDocId = currChildDoc.ContentDocumentId;
        for(ContentWorkspaceDoc c : conWrkDocList)
        {
            if(c.ContentWorkspaceId != currChildDoc.FirstPublishLocationId)
            {
                ContentWorkspaceDoc newContWrkDoc = new ContentWorkspaceDoc();
                newContWrkDoc.ContentDocumentId = contDocId;
                newContWrkDoc.ContentWorkspaceId = c.ContentWorkspaceId;
                
                conWrkDocListNew.add(newContWrkDoc);
            }
        }
        insert conWrkDocListNew;
        if(flag == 'child')
        {
            pagereference pref;
            pref = new pagereference('/apex/CpAddChildDoc?contId='+parentId);
            pref.setredirect(true);
            return pref;
        }
        else if(flag == 'detail')
        {
            pagereference pref;
            pref = new pagereference('/apex/CpContentDetail?id='+parentId);
            pref.setredirect(true);
            return pref;
        }
        return null;
    }

}
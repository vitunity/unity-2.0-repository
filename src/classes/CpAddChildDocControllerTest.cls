@isTest

public class CpAddChildDocControllerTest
{
    static testmethod void testMethod1()
    {
        Test.StartTest();
        
        ContentVersion cont = new ContentVersion();
        cont.Title = 'Test Title';
        cont.VersionData = blob.valueOf('xyz');
        cont.Origin = 'C';
        cont.pathOnClient = 'Test Title';
        cont.FirstPublishLocationId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        cont.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'ContentVersion' AND DeveloperName = 'Product_Document'].Id;
        cont.Mutli_Languages__c = true;
        insert cont;
        ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
        ContentVersion cont1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cont.Id];
        cwd.ContentDocumentId = cont1.ContentDocumentId;
        cwd.ContentWorkspaceId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Argus'].Id;
        insert cwd;
        ContentVersion cont2 = new ContentVersion();
        cont2.Title = 'Test Title2';
        cont2.VersionData = blob.valueOf('xyz1');
        cont2.Origin = 'C';
        cont2.pathOnClient = 'Test Title2';
        cont2.FirstPublishLocationId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        cont2.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'ContentVersion' AND DeveloperName = 'Product_Document'].Id;
        insert cont2;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(cont2); 
        ApexPages.currentPage().getParameters().put('contId',cont.Id);
        CpAddChildDocController inst = new CpAddChildDocController(controller1);
        inst.docId = cont2.Id;
        pagereference pge = inst.saveToAddLib();
        ContentVersion cont3 = new ContentVersion();
        cont3.Title = 'Test Title3';
        cont3.VersionData = blob.valueOf('xyz3');
        cont3.Origin = 'C';
        cont3.pathOnClient = 'Test Title3';
        cont3.FirstPublishLocationId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        cont3.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'ContentVersion' AND DeveloperName = 'Product_Document'].Id;
        insert cont3;
        
        controller1 = new ApexPages.StandardController(cont3); 
        ApexPages.currentPage().getParameters().put('contId',cont.Id);
        inst = new CpAddChildDocController(controller1);
        inst.docId = cont3.Id;
        inst.flag = 'child';
        pge = inst.saveToAddLib();
        
        Test.StopTest();
    }
        
}
/*************************************************************************\
         
****************************************************************************/
public class CpAdvanceLookUpController
{
public String AccountId;
public string formid{get;set;}
public List<Event_Webinar__c> results{get;set;} // search results
public string searchString{get;set;} // search keyword
Public List<Event_Webinar__c> ListofContact{get;set;}
public boolean VarContact{get;set;}
    public CpAdvanceLookUpController(ApexPages.StandardController controller) 
    {
      VarContact = false;
      formid=ApexPages.currentPage().getParameters().get('formid');
      system.debug('&&&&&'+formid);
      ListofContact= new list<Event_Webinar__c>();
      //ListofContact=[Select Id,Name,AccountId from Contact where AccountId=:AccountId and AccountId!=Null];
      searchString = System.currentPageReference().getParameters().get('lksrch');
      runSearch(); 
    }
 public PageReference search() {
    runSearch();
    return null;
  }
  
 public void NewContact() {
    VarContact=true;
    // return null;
  }  
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    results = performSearch(searchString);               
  } 
 
  // run the search and return the records found. 
  private List<Event_Webinar__c> performSearch(string searchString) {
      contentypelist = new List<contenttype>();
      
    //AccountId=ApexPages.currentPage().getParameters().get('prcTempSel'); 
    string temp;
     string searchquery;
    if(searchString != '' && searchString != null)
    {
       temp = '*' + searchString  + '*';
    }
   else
   {
       String soql = 'select  name,Title__c,From_Date__c,To_Date__c from Event_Webinar__c where RecordType.name = \'Events\'';
      // soql = soql + '  and limit 25';
    System.debug('*******'+soql);
    return database.query(soql);
     
  }
   
    if(searchString != '' && searchString != null)
    {
        searchquery='FIND \'' + temp +'\' IN ALL FIELDS RETURNING Event_Webinar__c(id,title__c,To_Date__c,From_Date__c,Name,RecordType.name where RecordType.name = \'Events\' )';
        List<List<SObject>> searchList = search.query(searchquery);
       List<Event_Webinar__c> eventlist = new List<Event_Webinar__c>();
       List<Event_Webinar__c> evntlist = new list<Event_Webinar__c>(); 
        eventlist = (List<Event_Webinar__c>)searchList[0];
         if(eventlist != null){
                
              
                 for(Event_Webinar__c evnt : eventlist){
                   if(evnt.Recordtype.Name == 'Events')
                     evntlist.add(evnt);
                   
          System.debug('Test----->'+eventlist );
    }
    
                //  contenttype conrecordevnt = new contenttype();
                //  conrecordevnt.Contenttypevar = 'Event';
                //  conrecordevnt.eventrecord = evntlist;
               //   contentypelist.add(conrecordevnt);
                  return evntlist;
    }
                 
      
   } 
   return null;
  } 
   public List<contenttype> contentypelist{get;set;}// List to hold the records for the events,presentation,etc.
  public class contenttype{
      public String Contenttypevar{get;set;}
      
      public List<Event_Webinar__c> eventrecord{get;set;}
   
      public contenttype(){
      
        eventrecord = new List<Event_Webinar__c>();
       
      }
    }   
public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
public Contact contact {
    get {
      if (contact == null)
        contact = new Contact();
        contact.accountid=AccountId;
      return contact;
    }
    set;
  }

  
  
  
  
}
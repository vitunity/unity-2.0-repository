@isTest
private class CpAdvanceLookUpControllerTest{
    static testMethod void CpAdvanceLookUpControllerTestMethod(){
        Test.StartTest();
        User thisUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Events' AND SobjectType='Event_Webinar__c'];
        Event_Webinar__c evt1 = new Event_Webinar__c(Title__c = 'Test Title', From_Date__c = system.today(), To_Date__c = system.today(), RecordTypeId = rt.Id);
        Event_Webinar__c evt2 = new Event_Webinar__c(Title__c = 'Test Title2', From_Date__c = system.today(), To_Date__c = system.today(), RecordTypeId = rt.Id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(evt1); 
        CpAdvanceLookUpController inst = new CpAdvanceLookUpController(controller);
        inst.searchString = 'Test';
        pagereference pref = inst.search();
        string str = inst.getFormTag();
        string str1 = inst.getTextBox();
        CpAdvanceLookUpController.contenttype inst2 = new CpAdvanceLookUpController.contenttype();
        }
        Test.StopTest();
   }
}
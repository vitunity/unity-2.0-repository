/***************************************************************************
Author         : N/A
Date           : N/A
Description    : N/A
Project        : N/A
Change Log:
14-Sept-2018 - STSK0015649 - Nilesh Gorle - Added new filter "Cases where systems are down"
07-Sep-2017 - Divya Hargunani - STSK0012840: Remove ARIA Standard / DIC from MyVarian - Added a check for deprecated library 'ARIA Standard / DIC'
04-Dec-2017 - Divya Hargunani - STSK0013644 : Issues with MyVarian workflow - Added logic for preserving the filters under customer support cases
*************************************************************************************/

public class CpCaseController 
{
    public string CaseStatusLink{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    public List<Case> caseList;
    public List<Case> caseListexcel; // for excel use
    public Case caserec{get;set;}
    public List<SelectOption> optionsstatus;
    public string CaseStatus{get;set;}
    private integer counter=0;
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
    public Id accountId;
    public String Productvalue {get;set;}
    public String Accountvalue {get;set;}
    public String StatusVal {get;set;}
    public String Searchcasenum{get;set;}
    public Boolean Mycases{get;set;}
    public Boolean casesWithSD{get;set;}
    
    //STSK0013644 : Added a method to persist the filters
    private void persistFilters(){
        map<String,String> params = ApexPages.currentPage().getParameters(); 
        if(params.isEmpty()) return;
        if(params.get('a') != null && params.get('a') != 'null')
        Accountvalue = params.get('a');
        if(params.get('s') != null && params.get('s') != 'null')
        StatusVal = params.get('s'); 
        if(params.get('ss') != null && params.get('ss') != 'null')
        Searchcasenum = params.get('ss');
        if(params.get('m') != null && params.get('m') != 'null')
        Mycases = Boolean.valueof(params.get('m'));
        if(params.get('csd') != null && params.get('csd') != 'null')
        casesWithSD = Boolean.valueof(params.get('csd'));
        if(params.get('p') != null && params.get('p') != 'null')
        Productvalue = params.get('p');
        if(params.get('d1') != null && params.get('d1') != 'null' && params.get('d1') != '//' )
        caserec.Date_of_Event__c = date.parse(params.get('d1'));
        if(params.get('d2') != null && params.get('d2') != 'null' && params.get('d2') != '//')
        caserec.Date_Reported_to_Varian__c = date.parse(params.get('d2'));
    }
    
    User un;
    set<Id> AccIds = new set<Id>();
    public Boolean checkcountry {get;set;}
    set<String> countryset = new set<String>();
    
    public void updateStatus(){
        CaseStatusLink = apexpages.currentpage().getparameters().get('link');
        StatusVal = '--Any--';
    }
    public CpCaseController()
    {
        CaseStatusLink = 'open';
        caserec = new case();
        caserec.recordtypeId = [Select id from recordtype where developername = 'Helpdesk' and sobjecttype = 'Case'].id;
        caserec.status = '';
        checkcountry = false;
        Mycases = false;
        casesWithSD = false;
        un = [Select Id, AccountId,alias,ContactId,Contact.Account.country1__r.name,Contact.Distributor_Partner__c From User where id = : Userinfo.getUserId() limit 1];
        if (un.ContactId == null)
        {
            Usrname=un.alias;
            accountId = un.AccountId;
            shows='block';
            system.debug('############' + shows);
            //isGuest=false;
        }
        else
            shows = 'none';
        Accountvalue = un.AccountId;
        AccIds.add(un.AccountId);

        if(un.Contact.Distributor_Partner__c)
        {
            checkcountry = true;
        }
        for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
        {
            countryset.add(countries.name);
        }
        if(countryset.contains(un.Contact.Account.country1__r.name) && checkcountry == false)
        {
            checkcountry = true;
        }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: un.ContactId])
        {
            AccIds.add(contactacc.Account__c);
            if(checkcountry == false && countryset.contains(contactacc.Account__r.country1__r.name))
            {
                checkcountry = true;
            }
        }
        //getoptions();
        persistFilters();
    }
    //For Institution
    public List<SelectOption> Accoptions;
    public List<SelectOption> getAccoptions()
    {
        Accoptions = new List<SelectOption>();
        Accoptions.add(new selectoption('','--Any--'));
        for(Account Acc : [Select name from account where id in: AccIds])
        {
            Accoptions.add(new selectoption(Acc.id,Acc.Name));
        }
        return Accoptions;
    } 
    // For Product search
    Public List<SelectOption> options;
    Public List<SelectOption> getoptions()
    {          
        options = new List<SelectOption>();
        //options.add(new selectoption('--Any--','--Any--'));
        set<string> ProductGroupSet = new set<String>();
        AccIds.add(un.AccountId);
        for (Contact_Role_Association__c contactacc : [Select Account__c from Contact_Role_Association__c where contact__c =: un.ContactId])
            AccIds.add(contactacc.Account__c);
            //for serial number
        if(Accountvalue != null && Accountvalue != '--Any--'){
            for (Case ca : [select id, Product_System__c from Case where RecordType.developername = 'Helpdesk' and AccountID =: Accountvalue])
            {
                if(ca.Product_System__c != null){
                
                //STSK0012840
                // Added a check for deprecated library. 
                
                //ProductGroupSet.addall(ca.Product_System__c.split(';'));
                     for(String s : ca.Product_System__c.split(';')){
                        if(s != 'ARIA Standard / DIC'){
                            ProductGroupSet.add(s);
                        }
                     }
                }
            }
            //List<SVMXC__Installed_Product__c> Installprdlist = [Select id,SVMXC__Product__r.Product_Group__c,name,(Select id from Cases__r where RecordType.developername = 'Helpdesk' limit 1) from SVMXC__Installed_Product__c where SVMXC__Company__c =: Accountvalue];
            /*for(SVMXC__Installed_Product__c sr : Installprdlist) {
                if(sr.SVMXC__Product__r.Product_Group__c != null && sr.Cases__r.size() > 0)

                    ProductGroupSet.addall(sr.SVMXC__Product__r.Product_Group__c.split(';')); 
            }*/
        }
       /* set<string> ProductGroupSet = new set<String>();
        List<Product_Roles__c> prdrole = new List<Product_Roles__c>();
        List<String> publicgrouplist = new list<String>();
        Map<String,Product_Roles__c> productrolemap = new Map<String,Product_Roles__c>();
        prdrole = [Select Product_Name__c,Public_Group__c from Product_Roles__c where ismarketingKit__c = false];
        for(Product_Roles__c prg : prdrole)
        {
         publicgrouplist.add(prg.Public_Group__c);
         productrolemap.put(prg.Public_Group__c,prg);
         }
        List<GroupMember> grplmemberlist = new List<GroupMember>();
        grplmemberlist = [Select GroupId,UserOrGroupId,Group.Name from GroupMember where Group.Name in:publicgrouplist and UserOrGroupId =: UserInfo.getuserId()];
         if(accountId != null){
         for(GroupMember gm: grplmemberlist)
         {
           ProductGroupSet.add(productrolemap.get(gm.Group.Name).Product_Name__c);
         }
         }else{ //for internal user
           Schema.DescribeFieldResult fieldResult = Product2.Product_Group__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
               {
                  ProductGroupSet.add(f.getValue());
               }       
         }*/
         // Logic ends
        options.add(new selectoption('--Any--','--Any--'));
        for(string sr : ProductGroupSet) {
        options.add(new selectoption(sr, sr)); 
        } 
        options.sort();
        return options;
    }
    
    public List<SelectOption> getoptionsstatus()
    {
        optionsstatus = new List<SelectOption>();
        optionsstatus.add(new selectoption('--Any--','--Any--'));
        
        set<string> setStatus = new set<string>();
        if(CaseStatusLink == 'open'){
            optionsstatus.add(new selectoption('In Progress,Pending Action By Varian,Dispatch Pending,Work Order Initiated,Escalated,Pending Action by 3rd Party','In Progress')); //,Work Order Initiated
            optionsstatus.add(new selectoption('New','New'));   
            optionsstatus.add(new selectoption('Pending Action By Customer','Pending Action By Customer'));
        }else if(CaseStatusLink == 'closed'){
            optionsstatus.add(new selectoption('Closed,Closed by Work Order','Closed'));
        }else if(CaseStatusLink == 'all'){
            optionsstatus.add(new selectoption('Closed,Closed by Work Order','Closed'));
            optionsstatus.add(new selectoption('In Progress,Pending Action By Varian,Dispatch Pending,Work Order Initiated,Escalated,Pending Action by 3rd Party','In Progress')); //,Work Order Initiated
            optionsstatus.add(new selectoption('New','New'));
            optionsstatus.add(new selectoption('Pending Action By Customer','Pending Action By Customer'));
        }
        
        
        
        /*Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
               {
                optionsstatus.add(new selectoption(f.getValue(),f.getValue()));
             } */
        //optionsstatus.sort();
        return optionsstatus;
    }

    public pagereference createCase()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCreateUpdateCase');
        pref.setredirect(true);
        return pref;
    }
    
    public List<Case> getcaseList() 
    {
        
        set<string> setStatus = new set<string>();
        if(CaseStatusLink == 'open'){
            setStatus.add('New');
            setStatus.add('open');
            setStatus.add('In Progress');
            setStatus.add('Pending Action By Customer');
            //INC4657694 -Rakesh.Start
            setStatus.add('Work Order Initiated');
            setStatus.add('Pending Action By Varian');
            setStatus.add('Escalated');
            setStatus.add('Dispatch Pending');
            setStatus.add('Pending Action by 3rd Party');
            //INC4657694 -Rakesh.End
        }else if(CaseStatusLink == 'closed'){
            setStatus.add('closed');
            setStatus.add('Closed by Work Order');
        }
        
        string datequery;
        date startDate = caserec.Date_of_Event__c;
        date endDate = caserec.Date_Reported_to_Varian__c;
        
        if(startdate <> null && endDate == null)caserec.Date_Reported_to_Varian__c=enddate = system.today();
        if(startDate <> null && endDate <> null){
            datequery = ' and CreatedDate >=: startDate and  CreatedDate <=: endDate ';
        }
        
        caseList = new List<Case>();
        total_size = null;
        list<String> statusvalbacknd = new list<String>();
        try 
        {
            system.debug('testss' + counter);
            string whr='';
            String temp = '*' + Searchcasenum + '*';
            system.debug('ALL VALUES AFTER RESET ----->' + Productvalue + caserec.status + Searchcasenum + Accountvalue);
           String Query = 'select Id, CaseNumber, Priority,SVMXC__Top_Level__c, Reason, Status, ProductSystem__r.SVMXC__Product__r.Product_Group__c,Product_System__c ,ProductSystem__r.Name,ProductSystem__r.SVMXC__Product__r.Name, CreatedDate, Subject, Recordtype.DeveloperName, ContactId, Contact.Name, AccountId, Account.Name FROM Case where id != null and AccountId != null and Recordtype.DeveloperName = \'Helpdesk\' and (Status != \'New\' OR Reason != \'Resource Planning\') and Closed_Case_Reason__c != \'Created in Error\'';// and Status != \'Work Order Initiated\'';  //DE3449
            String QueryCnt = 'select count() from Case where id != null and RecordType.developername = \'Helpdesk\' and Closed_Case_Reason__c != \'Created in Error\'';// and Status != \'Work Order Initiated\''; 
            
            if(datequery <> null)
            {
                Query += datequery;
                QueryCnt += datequery;
                
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Query+''+datequery));
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,QueryCnt+''+datequery));
            }

            if(setStatus.size()>0){
                Query += ' and Status IN: setStatus ';
                QueryCnt += ' and Status IN: setStatus ';
            }
            
            if((Productvalue != '--Any--') && (Productvalue != null) && (Productvalue != ''))
            {
                whr = 'and (ProductSystem__r.SVMXC__Product__r.Product_Group__c includes (\'' + Productvalue +'\') OR Product_system__c =: Productvalue )'; //DE3449
            }
            
            /* STSK0015649 - Added below filter for case Reason */
            if(casesWithSD != null && casesWithSD)
            {
                whr = ' and Reason = \'System Down\' ';
            }
            String statusclause = '';
            system.debug('Status--->' + StatusVal);
            if(StatusVal != '--Any--' && StatusVal != '' && StatusVal != null)
            {   
                for(String str : StatusVal.split(','))
                {
                    if(statusclause == '')
                        statusclause = '\'' + str + '\'';
                    else
                        statusclause = statusclause + ',\''+ str + '\'';
                }
                whr = whr + ' and Status in (' + statusclause + ')';
            }
            if(Searchcasenum != '' && Searchcasenum!= null)
            {
               /* String searchquery = 'FIND \'' + temp +'\' IN Name FIELDS RETURNING Case(ID)';
                List<List<SObject>> searchList = search.query(searchquery);
                Case[] cn = ((List<Case>)searchList[0]);
                //for(searchList[0] cn : ContentVersion)                
                //contntId.add(cn.Id); */
                //system.debug(' --- Check File Column ----  -- ' + cn + ' --- ----- ' + searchList);
                whr = whr + ' and (CaseNumber Like  \'%' + Searchcasenum + '%\' or Subject Like \'%' + Searchcasenum + '%\' or CreatedBy.Name Like \'%' + Searchcasenum +'%\' or ProductSystem__r.Name Like \'%' + Searchcasenum + '%\' or Contact.Name Like \'%' + Searchcasenum + '%\' or Status Like \'%'+ Searchcasenum + '%\')'; //DE3449
            }            
            if(un.contactId != null)
            {
                
                if(Accountvalue != '--Any--' && Accountvalue != null && Accountvalue != '')
                {
                    system.debug('==Accountvalue=='+Accountvalue);
                    whr = whr + ' and AccountId =: Accountvalue';
                }else{
                    whr = whr + ' and AccountId in: AccIds';
                }
                if(Mycases)
                {
                    whr = whr + ' and ContactId = \''+ un.contactId +'\'';
                }
            }
            
            QueryCnt = QueryCnt + ' ' + whr;
            system.debug('----- query constricted is---->'+total_size);
            system.debug('----- query Counter---->'+QueryCnt);
            if(whr == '' || whr == null)
                whr = 'order by ' + sortField + ' '+ sortDir;
            else if(!whr.contains('order'))
                whr = whr + ' order by ' + sortField + ' '+ sortDir;        
            if(counter < 2000)
            {
                if (counter == total_size)
                    Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + (counter - list_size);
                else
                    Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + counter;
            }
            else
                Query = Query + ' ' + whr +' limit ' + counter + 20;
            System.debug('Query ------- 123 --------  >>>>>' + QueryCnt);
            System.debug('Query ------- 123 --------  >>>>>' + Query);
            total_size = DataBase.countQuery(QueryCnt);
            system.debug('----- query constricted is---->'+total_size);
            List<Sobject> sobj = new List<Sobject>();
            sobj = DataBase.Query(Query);
            //System.debug('---Sobject size----'+sobj.size());
            //total_size = sobj.size();
            
            if(counter < 2000){
            For(Sobject s : sobj)
            {
                caseList.add((case)s);
                System.debug('List lstcontentversion'+caseList.size());
            }
            }else{
              For(Integer i = counter; i <= counter + 9;i++)
               {
                if(i < Sobj.size())
                caseList.add((case)Sobj[i]);
                System.debug('List lstcontentversion'+caseList.size());
               }
            }
             
            return caseList;
        } 
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
    }
    // for navigation
    
    public PageReference showrecords1() 
    {
        Beginning();
        return null;
    }
    public PageReference Beginning() 
    { //user clicked beginning
        counter = 0;
        //total_size = 0;
        return null;
    }
 
    public PageReference Previous() 
    { //user clicked previous button
        counter -= list_size;     
        return null;
    }
 
    public PageReference Next() 
    { //user clicked next button
        counter += list_size;     
        return null;
    }
 
    public PageReference End() 
    { //user clicked end
        //if(math.mod(total_size, list_size) > 0)
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
 
    public Boolean getDisablePrevious() 
    { 
        //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }
 
    public Boolean getDisableNext() 
    {   //this will disable the next and end buttons
        system.debug('getPageNumber()= = = '+getPageNumber());
        system.debug('getTotalPages()= = = '+getTotalPages());
        if(getPageNumber() != getTotalPages())
            return false;
        else
            return true;
        //if (counter + list_size < total_size) return false; else return true;
    }
 
    public Integer getTotal_size() 
    {
        return total_size;
    }
 
    public Integer getPageNumber() 
    {
        if(total_size == 0)
        {
            return 0;
        }
        else if(total_size == list_size)
        {
            return total_size/list_size;
        }
        else
        {
            if(counter == total_size)
            {
                return counter/list_size; // + 1;
            }else{
                return counter/list_size +1;
            }
        }
    }
 
    public Integer getTotalPages() 
    {
        if (math.mod(total_size, list_size) > 0) 
        {
            return total_size/list_size + 1;         
        } else {
            return (total_size/list_size);
        } 
        // return 10;
    }
   //Reset
   
    public PageReference reSet() 
    { //user clicked beginning     
        Productvalue = null;
        Accountvalue = un.AccountId;
        caserec.status = null;
        Searchcasenum = ''; 
        StatusVal = null;
        Mycases = false;
        casesWithSD = false;
        counter = 0;
        
        caserec.Date_of_Event__c = null;
        caserec.Date_Reported_to_Varian__c = null;
        
        return (new pagereference('/apex/CpCasePage'));
    }
   
   /*code for the ascending and descending in Case*/
   
    public String soqlsort {get;set;}
    public List <ContentVersion> CandidateList2 = New List <ContentVersion>();
    //Toggles the sorting of query from asc<-->desc
    public void  toggleSort() 
    {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        /*
            // run the query again for sorting other columns
            soqlsort = 'select Title, Mutli_Languages__c,Date__c,Document_Type__c,Document_Number__c,Document_Version__c from ContentVersion'; 

            // Adding String array to a List array
            System.debug('_______^^^^___ ' + soqlsort + ' order by ' + sortField + ' ' + sortDir );
            ContentVersionsList = Database.query(soqlsort + ' order by ' + sortField + ' ' + sortDir ); */
    }

    // the current sort direction. defaults to asc
    public String sortDir {
        // To set a Direction either in ascending order or descending order.
        get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;}
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField 
    {
        // To set a Field for sorting.
        get  { if (sortField == null) {sortField = 'CreatedDate'; } return sortField;  }
        set;
    } 
    /*code ended */

    // method for exporting to excel
    public List<Case> getcaseListexcel() 
    {
        caseListexcel = new List<Case>();
        total_size = null;
        list<String> statusvalbacknd = new list<String>();
        Productvalue = ApexPages.currentPage().getParameters().get('Productvalue');
        StatusVal = ApexPages.currentPage().getParameters().get('Statusvalue');
        Searchcasenum = ApexPages.currentPage().getParameters().get('Searchvalue');
        Accountvalue = ApexPages.currentPage().getParameters().get('Account');
        CaseStatusLink = apexpages.currentpage().getparameters().get('link');
        Mycases = Boolean.valueOf(ApexPages.currentPage().getParameters().get('Mycases'));
        casesWithSD = Boolean.valueOf(ApexPages.currentPage().getParameters().get('casesWithSD'));
        try 
        {
            
            set<string> setStatus = new set<string>();
            if(CaseStatusLink == 'open'){
                setStatus.add('New');
                setStatus.add('open');
                setStatus.add('In Progress');
                setStatus.add('Pending Action By Customer');
                setStatus.add('Work Order Initiated');
                setStatus.add('Pending Action By Varian');
                setStatus.add('Escalated');
                setStatus.add('Dispatch Pending');
                setStatus.add('Pending Action by 3rd Party');
            }else if(CaseStatusLink == 'closed'){
                setStatus.add('closed');
                setStatus.add('Closed by Work Order');
            }
            system.debug('testss' + counter);
            string whr='';
            String temp = '*' + Searchcasenum + '*';
            system.debug('ALL VALUES AFTER RESET ----->' + Productvalue + caserec.status + Searchcasenum + Accountvalue);
            String Query = 'select Id, CaseNumber, Priority, Status,ProductSystem__c, productSystem__r.SVMXC__Product__r.Product_Group__c, productSystem__r.Name,productSystem__r.SVMXC__Product__r.Name, CreatedDate, Subject, Recordtype.DeveloperName, ContactId, Contact.Name, AccountId, Account.Name, Product_System__c FROM Case where id != null and Recordtype.DeveloperName = \'Helpdesk\' and Closed_Case_Reason__c != \'Created in Error\''; 
            if((Productvalue != '--Any--') && (Productvalue != null) && Productvalue != '') //de3449
            {
                whr = 'and ( productSystem__r.SVMXC__Product__r.Product_Group__c includes (\'' + Productvalue +'\') OR Product_system__c =: Productvalue )';
            }
            if(casesWithSD != null && casesWithSD) {
                whr = ' and Reason = \'System Down\' ';
            }
            String statusclause = '';
            system.debug('Status--->' + StatusVal);
            if(StatusVal != '--Any--' && StatusVal != '' && StatusVal != null)
            {   
                for(String str : StatusVal.split(','))
                {
                    if(statusclause == '')
                        statusclause = '\'' + str + '\'';
                    else
                        statusclause = statusclause + ',\''+ str + '\'';
                }
                whr = whr + ' and Status in (' + statusclause + ')';
            }
            if(Searchcasenum != '' && Searchcasenum!= null)
            {
               whr = whr + ' and (CaseNumber Like  \'%' + Searchcasenum + '%\' or Subject Like \'%' + Searchcasenum + '%\' or CreatedBy.Name Like \'%' + Searchcasenum +'%\' or productSystem__r.Name Like \'%' + Searchcasenum + '%\' or Contact.Name Like \'%' + Searchcasenum + '%\' or Status Like \'%' + Searchcasenum +'%\') ';
            }            
            if(un.contactId != null)
            {
                if(Accountvalue != '--Any--' && Accountvalue != null && Accountvalue != '')
                {
                    whr = whr + ' and AccountId =: Accountvalue';
                }else{
                    whr = whr + ' and AccountId in: AccIds';
                }
                if(Mycases)
                {
                    whr = whr + ' and ContactId = \''+ un.contactId +'\'';
                }
            }
            if(setStatus.size()>0){
                whr += ' and Status IN: setStatus ';
            }
            
            if(whr == '' || whr == null)
             whr = 'order by ' + sortField + ' '+ sortDir;
            else if(!whr.contains('order'))
              whr = whr + ' order by ' + sortField + ' '+ sortDir;        
            Query = Query + ' ' + whr +' limit 50000';
            List<Sobject> sobj = new List<Sobject>();
            sobj = DataBase.Query(Query);
            
            For(Sobject s : sobj)
            {
                caseListexcel.add((case)s);
                System.debug('List lstcontentversion'+caseListexcel.size());
            }
            return caseListexcel;
        } 
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
    }     

    //method for first time login
    public pagereference firsttimelogin(){
         if([Select contact.PasswordReset__c from user where id = :Userinfo.getUserId() limit 1].contact.PasswordReset__c == false)
         {
            return new pagereference('/apex/cphomepage');
         }else{
            return null;
         }
    }
}
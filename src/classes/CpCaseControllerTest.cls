/*************************************************************************\
    @ Author        : Nikhil Verma
    @ Date      : 14-Nov-2014
    @ Description   :  Test class for CpCaseController class.
    @ Last Modified By :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/


@isTest
Public class CpCaseControllerTest
{
   public static testmethod void TestCpCaseController()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
            User u;
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            
            Regulatory_Country__c reg = new Regulatory_Country__c();
            reg.Name='Test' ; 
            insert reg;
           
            Account a;     
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            a = new Account(name='test2dic3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='testdiv3',FirstName='Singhdiv3',Email='test.div3@gmail.com', AccountId=a.Id,MailingCountry ='Test', MailingState='teststate', Phone = '1214445');
            insert con;
            
            u = new User(alias = 'standt', email='standardtestuser29@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standardtestuser29@testclass.com');
            insert u;
            
            Contact_Role_Association__c cro = new Contact_Role_Association__c(contact__c = con.Id, Account__c = a.Id);
            insert cro;
            
            Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity;Argus');
            insert prod;
            
            //SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVMXC__Product__c = prod.Id, Name = 'H47365');
            //insert ip;
            
            Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
            Case case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType,status = 'new',Subject= 'case Subject' /*SVMXC__Top_Level__c = ip.Id*/);
            insert case1;
            
            ApexPages.currentPage().getParameters().put('Statusvalue',case1.Status);
            //ApexPages.currentPage().getParameters().put('Productvalue','Acuity');
            ApexPages.currentPage().getParameters().put('Searchcasenum',case1.Subject);
            ApexPages.currentPage().getParameters().put('Accountvalue',case1.AccountId);
            ApexPages.currentPage().getParameters().put('Mycases','false');
            ApexPages.currentPage().getParameters().put('casesWithSD','false');
            ApexPages.currentPage().getParameters().put('link','all');
            System.runAs (u)
            {
                
                test.startTest();
                CpCaseController inst = new CpCaseController();
                inst.CaseStatusLink = 'all';
                List<SelectOption> optionList = inst.getAccoptions();
                optionList = inst.getoptions();
                optionList = inst.getoptionsstatus();
                pagereference pref = inst.createCase();
                inst.Productvalue = 'Acuity';
                inst.Searchcasenum = 'Subject';
                ApexPages.currentPage().getParameters().put('Searchcasenum','Subject');
                List<Case> caseList = inst.getcaseListexcel();
                inst.CaseStatusLink = 'open';
                caseList = inst.getcaseListexcel();
                inst.CaseStatusLink = 'all';
                
                ApexPages.currentPage().getParameters().put('link','closed');
                inst.updateStatus();                
                caseList = inst.getcaseList();
                
                ApexPages.currentPage().getParameters().put('link','all');
                inst.updateStatus();
                inst.CaseStatusLink = 'open';
                caseList = inst.getcaseList();
                inst.CaseStatusLink = 'all';
                
                ApexPages.currentPage().getParameters().put('link','open');
                inst.updateStatus();
                optionList = inst.getoptionsstatus();
                
                
                pref = inst.showrecords1();
                pref = inst.Previous();
                pref = inst.Next();
                pref = inst.End();
                boolean flag = inst.getDisablePrevious();
                flag = inst.getDisableNext();
                integer cnt = inst.getTotal_size();
                cnt = inst.getPageNumber();
                cnt = inst.getTotalPages();
                pref = inst.reSet();
                inst.toggleSort();
                pref = inst.firsttimelogin();
                test.stopTest();
            }
        }
    }
}
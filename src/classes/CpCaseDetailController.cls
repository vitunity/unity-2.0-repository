/**************************************************************************\
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
12-Dec-2017 - Divya Hargunani - STSK0013403 - Case Recommendation Display on MyVarian - removed the condition which check recommendation for closed case only
12-Dec-2017 - Divya Hargunani - STSK0013644 - Issues with MyVarian workflow - Updated logic for back to cases button
/**************************************************************************/

Public class CpCaseDetailController
{
    Public String AcctName {get; set;}      
    Public String CustEmail {get; set;}
    Public String custName {get; set;}
    Public String contMethod {get; set;}
    Public String product {get; set;}
    Public String prodVersion {get; set;}
    Public String subject {get; set;}
    Public String description {get; set;}
    Public String AccId {get; set;}
    public String usrContId{get;set;} 
    public Boolean isGuest{get;set;}
    public string shows{get;set;}
    public string caseids{get;set;}
    public Boolean Isvisible{get;set;}
    public Boolean IsDescvisible{get;set;}
    public Boolean IsWorkordrvisible{get;set;}
    public Boolean IsRecomvisible{get;set;}
    public Boolean isRecomndation{get;set;}
    public string Usrname{get;set;}
    public Boolean reopencase{get;set;}
    public String CaseStatus{get;set;}
    public String CasePriority{get;set;}
    public Case currCase{get;set;}
    public boolean isDetail{get; set;}
    public boolean isWrkOrdr{get; set;}
    
    public string serialname{get;set;}
    public Static List<Attachment > Attachments {get;set;}
    public List<Attachment> FSRAttachments {get; set;}
    public Set<Id> WoIdSet {get; set;}
    String caseid;
    String casetype;
    public Boolean checkcountry {get;set;}
    set<String> countryset = new set<String>();


    public class AssignComment        // This class will be used to store the corresponding input from the user and the Case Comment
    {  
        public CaseComment comment {get; set;}        //This will store the case Comment
        public String Commentedbyuser{get; set;}        
        public String CommentDate{get; set;}
        public AssignComment(){}                      //Empty constructor
    }  
    public class finalComments implements Comparable
    {
        public DateTime createdDateTime{get; set;}
        public String createdDate{get; set;}
        public String comment{get; set;}
        public String commentedBy{get; set;}
        public boolean isTask{get; set;}
        public Id objectId{get; set;}
        
        // Added by harshita
        public finalComments(datetime dt)
        {
            // blank comstructor for first email
            createdDateTime = dt;
            createdDate = dateInFormat(createdDateTime);
        }
        public finalComments(CaseComment caseComm,String Contactname)
        {
            createdDateTime = caseComm.Createddate;
            comment = caseComm.commentBody.replaceAll('\n','<br/>');
            if(caseComm.CreatedBy.FirstName != null)
                commentedBy = caseComm.CreatedBy.FirstName + ' ' + caseComm.CreatedBy.LastName;
            else
                commentedBy = caseComm.CreatedBy.LastName;
            if(commentedBy.contains(label.CpSupportSetting))
            {
                commentedBy = Contactname;//currCase.Contact.Name;
            }
            createdDate = dateInFormat(createdDateTime);
            isTask = false;
            objectId = caseComm.Id;
        }
        public finalComments(EmailMessage tsk)
        {
            createdDateTime = tsk.Createddate;
            comment = tsk.Subject;
            system.debug('above split');
            
            if(comment != null)
            {
                if(comment.IndexOf('[') != -1)
                {
                    String finalstr = comment.substring(0,comment.IndexOf('['));
                    //List<string> tempComments = comment.split('[');
                    system.debug('Subject after splitting'+finalstr );
                    comment = finalstr;//tempComments[0];
                }
            }else if(comment == null)
            {
                comment = '----';
            }
            /*if(tsk.CreatedBy.FirstName != null)
                commentedBy = tsk.CreatedBy.FirstName + ' ' + tsk.CreatedBy.LastName;
            else
                commentedBy = tsk.CreatedBy.LastName;*/
            if(tsk.fromname != null && tsk.fromname.contains('Varian'))
            {
                commentedBy = 'Varian Customer Support';
            }else{
                commentedBy = tsk.fromname;
            }
            createdDate = dateInFormat(createdDateTime);
            isTask = true;
            objectId = tsk.Id;
        }
        String dateInFormat(DateTime dtTm)
        {
            return(dtTm.month() + '/' + dtTm.day() + '/' +dtTm.year());
        }
        
        public Integer compareTo(Object objToCompare) 
        {
            if(createdDateTime > ((finalComments)objToCompare).createdDateTime){
                return -1;
            }
            else if(createdDateTime > ((finalComments)objToCompare).createdDateTime){
                return 0;
            }
            else{
                return 1;
            }
        }
    }
    public map<String,String> params;
    public List<finalComments> finalCommentsList {get; set;} 
    public List<AssignComment> Records {get; set;} 
    public  List<EmailMessage> Lsttask {get; set;}   
    public  List<SVMXC__Service_Order__c> LstWork {get; set;} 
    public String WorkorderId{get;set;} 
    public Boolean showPGBln {get;set;}
    public CpCaseDetailController()
    {
        params = ApexPages.currentPage().getParameters();
        Records = new List<AssignComment>();
        finalCommentsList = new List<finalComments>();
        shows = 'none';
        isGuest = false;
        Isvisible=false;
        IsDescvisible=false;
        IsWorkordrvisible=false;
        IsRecomvisible = false;
        isDetail = false;
        isWrkOrdr = false;
        isRecomndation = false;
        showPGBln=false;
        contMethod = 'E-mail';
        checkcountry = false;
        Attachments = new List<Attachment>();
        String idtypeparm = ApexPages.currentPage().getParameters().get('Id');
        
        if(idtypeparm != null && idtypeparm.contains('@'))
        {
            caseid = idtypeparm.split('@')[0];
            casetype = idtypeparm.split('@')[1];
        }else {
            caseid = idtypeparm;
        }

        Attachments = [select id,Body,ContentType,Description,Name,OwnerId,BodyLength from Attachment where Parentid = :caseid];//ApexPages.currentPage().getParameters().get('Id')];
        FSRAttachments = new List<Attachment>();
        
        if(casetype == 'Details') //ApexPages.currentPage().getParameters().get('type')=='Details')
        {
            Isvisible=true;
            
        }
         if(casetype =='WorkOrder') //ApexPages.currentPage().getParameters().get('type')=='WorkOrder')
        {
            IsWorkordrvisible=true;
        
        }
        if(casetype =='Recommendation') //ApexPages.currentPage().getParameters().get('type')=='Recommendation')
        {
            IsRecomvisible = true;
        }
        
         if(ApexPages.currentPage().getParameters().get('Count')!=null)
        {
          WorkorderId=ApexPages.currentPage().getParameters().get('Count');
          showPGBln =true;
          IsDescvisible=false;
          IsWorkordrvisible=false;
          IsRecomvisible = false;
          Isvisible=false;
        }
        if(casetype =='desc') //ApexPages.currentPage().getParameters().get('type')=='desc')
        {
          Isvisible=false;
          IsWorkordrvisible=false;
          IsRecomvisible = false;
          IsDescvisible=true;
        }
        if(caseid != NULL) //ApexPages.currentPage().getParameters().get('Id') != NULL)
        {
            currCase = [SELECT ownerId, Id,IsClosed,Priority,Local_Case_Activity__c,productsystem__r.name,createdDate,Local_Description__c, CaseNumber,status,Closed_Case_Reason__c,Description,Subject,Serial_number__c,Product_System__c,Product_Version_Com__c,SVMXC__Top_Level__r.Name,/*Product_Version_Build__r.Name,*/Contact.Name, Account.Name, Contact.Email, Local_Subject__c FROM Case WHERE Id = : caseid];//ApexPages.currentPage().getParameters().get('Id')];
            for (CaseComment Node : [Select commentBody,Createddate, isPublished,CreatedBy.username,CreatedBy.FirstName,CreatedBy.LastName from CaseComment where parentId = : caseid order by Createddate desc])    //Query and loop through all the case comments
            {
               /* assignComment temp = new AssignComment();   // Create temp to insert into the list Records
                temp.comment = Node;
                temp.Commentedbyuser=Node.CreatedBy.FirstName;
                temp.CommentDate =String.valueof( Node.Createddate);
                //temp.CommentDate = Node.Createddate.month()+'/'+Node.Createddate.day()+'/'+Node.Createddate.year();
                system.debug('Extracted Date---->>'+Node.Createddate.month()+'/'+Node.Createddate.day()+'/'+Node.Createddate.year());
                Records.add(Temp);*/
                finalCommentsList.add(new finalComments(Node,currCase.Contact.Name));
            }
            LstWork=[Select Subject__c,RecordType.Name,SVMXC__Order_Status__c,id,Name,SVMXC__Problem_Description__c,createddate, (SELECT Id,Name FROM Attachments where (Name like 'WO%' or Name like 'wo%') order by CreatedDate desc) from SVMXC__Service_Order__c where  SVMXC__Case__c=:currCase.Id And  Recordtype.name = 'Field Service'];
            if(currCase.Local_Case_Activity__c != null)
            {
                //isWrkOrdr = true;
                isRecomndation = true;
            }
            ///*
            if(LstWork.size() > 0)
            {
                isWrkOrdr = true;
                WoIdSet = new Set<Id>();
                for(SVMXC__Service_Order__c wo : LstWork)
                {
                    WoIdSet.add(wo.Id);
                }
            }
            //*/
            //System.debug('Size of work order'+LstWork);
            FSRAttachments = [select Body,ContentType,Description,Name,OwnerId,BodyLength from Attachment where Parentid IN :WoIdSet and name Like : '%_FSR%' ];
            Attachments.addall(FSRAttachments);
            system.debug('List of attachments:'+Attachments);
            system.debug('CASEID--->' + currCase);
            Lsttask=[SELECT CreatedBy.FirstName,CreatedBy.LastName,Subject,CreatedDate,fromname,FromAddress,toaddress FROM EmailMessage WHERE ParentId =: currCase.Id ];//[Select Subject, CreatedDate, CreatedBy.FirstName, CreatedBy.LastName from Task where Whatid=:currCase.Id order by Createddate desc];
            /*finalComments firstemail = new finalComments(currCase.Createddate);
            firstemail.comment = 'New Case # ' + currCase.CaseNumber;
            firstemail.commentedBy = currCase.Contact.Name;
            firstemail.isTask = true;
            finalCommentsList.add(firstemail);*/
            for(EmailMessage t :Lsttask)
            {
                if(t.FromAddress.contains('varian.com') && t.toaddress.contains('varian.com'))
                {
                    // need not add for internal communication
                }else {
                    finalCommentsList.add(new finalComments(t));
                }
            }
            system.debug('Final List is: ' +finalCommentsList);
           
            system.debug('List after Sorting: '+finalCommentsList);
            //  currCasecomments=[SELECT Id,(SELECT Id,CreatedDate,CommentBody FROM CaseComments)FROM Case where id=:ApexPages.currentPage().getParameters().get('Id') ];
            custName = currCase.Contact.Name;
            AcctName = currCase.Account.Name;
            CustEmail = currCase.Contact.Email;
            caseids=currCase.id;
            if(currCase.Status == 'Closed')
            {
                reopencase = true;
            }
            /*if(currCase.status != null)
            {
                if(currCase.status.contains('Close'))
                    reopencase = true;
            }*/
            /*if(currCase.SVMXC__Top_Level__r.Name != null)
                product = currCase.SVMXC__Top_Level__r.Name;
            else*/
            product = currCase.Product_System__c;
            /*if(currCase.Product_Version_Build__r.Name != null)
                prodVersion = currCase.Product_Version_Build__r.Name;
            else*/

                prodVersion = currCase.Product_Version_Com__c;
            subject = currCase.Local_Subject__c;
            CaseStatus = currCase.Status;
            CasePriority = currCase.Priority;
            serialname = currCase.productsystem__r.Name;//SVMXC__Top_Level__r.Name;//Serial_number__c;
            description = currCase.Local_Description__c;
            if(finalCommentsList.size()>0)
            {
            //description = description + String.valueof(finalCommentsList[0].comment);
            isDetail = true;
            }
             finalCommentsList.sort();
        }
        User un = [Select id,ContactId,usertype,alias,contact.Distributor_Partner__c,Contact.Account.country1__r.name from user where id =: Userinfo.getUserId() limit 1];
        if (un.ContactId == null)

        {
            shows = 'block';
            if(un.usertype == label.guestuserlicense)

            {
                Usrname = un.alias; 

                shows='none';
                isGuest=true;
            }
        }
        // Country logic
        if(un.Contact.Distributor_Partner__c)
        {
            checkcountry = true;
        }
        for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
        {
            countryset.add(countries.name);
        }
        if(countryset.contains(un.Contact.Account.country1__r.name) && checkcountry == false)
        {
            checkcountry = true;
        }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: un.ContactId])
        {
            if(checkcountry == false && countryset.contains(contactacc.Account__r.country1__r.name))
            {
                checkcountry = true;
            }
        }

    }
   
    public PageReference editCase()
    {
        //Case currCase = [SELECT Id, CaseNumber, Contact.Name, Account.Name, Contact.Email, Local_Subject__c FROM Case WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];
        //System.debug('Current case is----->>' + currCase.Id);
        pagereference pref;
        try{
         pref = new pagereference('/apex/CpUpdateCase?id='+ caseid);//ApexPages.currentPage().getParameters().get('id'));
        pref.setredirect(true);
        }catch(Exception e){
          system.debug('DDDDDDDDD' + e.getmessage());
        }
        return pref;
    }
    
    //STSK0013644 : Updated the logic to preserve the filters
    public pagereference backToCases()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCasePage?a='+params.get('a')
                                 +'&s='+params.get('s')
                                 +'&ss='+params.get('ss')
                                 +'&p='+params.get('p')
                                 +'&m='+params.get('m')
                                 +'&d1='+params.get('d1')
                                 +'&d2='+params.get('d2'));
        pref.setredirect(true);
        return pref;
    }
    
    public pagereference closeCase()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCloseCase?id='+ caseid);//ApexPages.currentPage().getParameters().get('Id'));
        pref.setredirect(true);
        return pref;
    }
    
    public pagereference printCase()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCaseReportPDF?id='+ caseid);//ApexPages.currentPage().getParameters().get('Id'));
        pref.setredirect(true);
        return pref;
    }
    // The method shareAttachment was added for the story STRY0038662 by Harvey on Oct 20th 2017
    public PageReference shareAttachment()
    {
        String idtypeparm = ApexPages.currentPage().getParameters().get('Id');
        
        if(idtypeparm != null && idtypeparm.contains('@'))
        {
            caseid = idtypeparm.split('@')[0];
        }else if(idtypeparm != null && idtypeparm.contains('%'))
        {
            caseid = idtypeparm.split('%')[0];
        }
        else
        {
            caseid = idtypeparm;
        }
        List<Case> currCases = [SELECT ownerId, Id from Case where id =: caseid];
        if(currCases.size() > 0 && currCase.ownerId != Userinfo.getUserId())
        {
            CaseShare share = new CaseShare();
            share.CaseId = currCases[0].Id;
            share.UserOrGroupId = Userinfo.getUserId();
            share.CaseAccessLevel = 'Read';
            insert share;
        }
        return null;
    }
}
/*************************************************************************\
    @ Author        : Nikhil Verma
    @ Date      : 14-Nov-2014
    @ Description   :  Test class for CpCaseDetailController class.
    @ Last Modified By :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/


@isTest(SeeAllData=true)
Public class CpCaseDetailControllerTest
{
   public static testmethod void TestCpCaseDetailController()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
            User u;
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            
            Account a;      
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            a = new Account(name='div1test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='testdiv1',FirstName='Singhdiv1',Email='div.test667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststatete', Phone = '1244525');
            insert con;
            
            u = new User(alias = 'standt', email='standardtestuse2@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standardtestuse2@testclass.com');
            //insert u;
            
            Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
            Case case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType, Priority='Medium');
            insert case1;
            
            List<Attachment> attchmentList = new List<Attachment>();
            Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body'); 
            Attachment att = new Attachment(Body = bodyBlob, Name = 'Test Attachment', ParentId = case1.Id);
            attchmentList.add(att);
            
            CaseComment caseComm = new CaseComment(commentBody = 'Test Comment', parentId = case1.Id, isPublished = true);
            insert caseComm;
            
            RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order__c' AND DeveloperName = 'Field_Service'].Id;
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
            wo.RecordTypeId = RecType;
            wo.SVMXC__Case__c = case1.Id;
            wo.SVMXC__Company__c = a.Id;
            wo.SVMXC__Order_Status__c = 'Open';
            wo.Subject__c = 'Test Subject';
            /*insert wo;
            
            Attachment att2 = new Attachment(Body = bodyBlob, Name = 'Test Attachment WO', ParentId = wo.Id);
            attchmentList.add(att2);
            Attachment att3 = new Attachment(Body = bodyBlob, Name = 'Test Attachment_FSR', ParentId = wo.Id);
            attchmentList.add(att3);
            insert attchmentList;*/
            
            EmailMessage t = new EmailMessage(subject = 'Test subject for task', TextBody = 'Test description for task', ParentId = case1.Id,FromAddress = 'test@varian.com',toaddress = 'test1@gmail.com');
            insert t;
            
            ApexPages.currentPage().getParameters().put('Id',case1.Id);
            ApexPages.currentPage().getParameters().put('Count','2');
            ApexPages.currentPage().getParameters().put('type','Details'); //Details desc
            CpCaseDetailController inst1 = new CpCaseDetailController();
            //System.runAs (u)
           // {
                 ApexPages.currentPage().getParameters().put('type','desc'); //Details desc
                CpCaseDetailController inst = new CpCaseDetailController();
                pagereference pref = inst.editCase();
                pref = inst.shareAttachment();
                pref = inst.backToCases();
                pref = inst.printCase();
                pref = inst.closeCase();
            //}
        }
    }
}
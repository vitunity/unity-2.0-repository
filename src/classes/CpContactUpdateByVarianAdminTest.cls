@isTest
private class CpContactUpdateByVarianAdminTest {
	
	@isTest static void test_method_one() {
		List<User> adminUser = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator'
				and isActive = true limit 1];
        Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='USA'  ); 
        insert a;  
        Test.startTest();
        	System.runAs(adminUser[0]) {
		        Contact con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', MvMyFavorites__c='Meetings', AccountId = a.Id, MailingCity='New York', MailingCountry='USA', MailingPostalCode='552601', MailingState='CA', Phone = '12452234');
		        //Creating a running user with System Admin profile
		        insert con;
		        con.Email = 'new123@email.com';
		        con.MarketingKit_Agreement__c = 'Edge Marketing Program';
		        update con;        		
        	}
        Test.stopTest();
	}
}
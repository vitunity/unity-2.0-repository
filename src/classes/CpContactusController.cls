/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 23-May-2013
    @ Description   : An Apex controller to get Information for Contus Us page of My Varian.
    
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class CpContactusController {
    public String show{get;set;}
    public Boolean isGuest{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    public string password{get; set;}
    public String cookieTokenUrl{get;set;}
    public Boolean IsPassExist{get; set;} public Boolean ready{get; set;}  public Boolean corrlog1{get; set;}
    public Boolean corrlog{get; set;}
    public Boolean internalusr {get;set;}
    public Boolean checkcase{get;set;}
    public String  ProductImprvementRequest{get;set;}
    public String Createsuportcase{get;set;}
    public CpContactusController (){
    set<String> countryset = new set<String>();
    set<String> paperdoccountryset = new set<String>();
   shows='none';
   checkcase = false;
   internalusr = false;
   Createsuportcase = 'notcreatesport';
   ProductImprvementRequest = 'notProdimpreq';
    for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
    {
        countryset.add(countries.name);
    }
   isGuest = SlideController.logInUser();
  List<Regulatory_Country__c> RegionName =new   List<Regulatory_Country__c>();

   User usr = [Select ContactId, Profile.Name,Contact.Distributor_Partner__c,Contact.Account.country1__r.name from user where Id=: UserInfo.getUserId()];        
         String usrContId = usr.contactid;
         System.debug('------user type--------='+ usrContId );
        if(usr.Contact.Distributor_Partner__c){  checkcase = true;  } 
         if(countryset.contains(usr.Contact.Account.country1__r.name) && checkcase == false)   {  checkcase = true;   }
         if (usr.contactid == null)
          {
           internalusr = true;
          }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: usrContId])
        {
            if(checkcase == false && countryset.contains(contactacc.Account__r.country1__r.name))  {     checkcase = true;  }
        }
        system.debug('Check case variable--->' + checkcase);
         
          if(usrContId != null)
        {
        
        Contact c = [select  MailingCountry from contact where Id =: usrContId];
       String MailCountry = c.MailingCountry;
       
       //RegionName =[ Select Portal_Regions__c from Regulatory_Country__c where Name=:MailCountry limit 1];
       for(MV_PaperDoc_Countries__c countries : MV_PaperDoc_Countries__c.getall().values()) {   paperdoccountryset.add(countries.name);   }
        //if(RegionName.size() >0)
         //{

       //if(RegionName[0].Portal_Regions__c =='EMEA')
       if(paperdoccountryset.contains(MailCountry))  {  show='block';     }
        else
        {
         show='none';
         }
  //}
  }
  else
        {
         show='none';
         }
         
          if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
              Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
              shows='block';
               show='block';  
              isGuest=false;
          }
       }
       system.debug('IsGuest--->>'+isGuest);

}
/*public static list<Support_Contact__c> getAllCountriesContact()
 {  
return Support_Contact__c.getall().values();  
  } 


 public static list<Support_Contact__c> getGamesByPlatform(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 
if(Contact.Region__c=='Europe / Middle East')
{ 
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
public static list<Support_Contact__c> getAsiaSpecificCount(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Asia Pacific')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
 
public static list<Support_Contact__c> getAustralasia(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Australasia')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
 public static list<Support_Contact__c> getLatinAmerica(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Latin America')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } */
 public static Boolean logInUser(){
      if(UserInfo.getUserType() == 'CspLitePortal' || UserInfo.getUserType() == 'PowerCustomerSuccess'){  return false;}
      else{
        return true;
      }

}

public PageReference LogintoOkta(){ 
        
        //username = 'anupam.tripathi1@wipro.com';
        //password = 'Wipro@123';
        password = ApexPages.currentPage().getParameters().get('password');
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        //Blob headerValue = Blob.valueOf(username+':'+password);
        String strToken = Label.oktatoken; //'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //req.setHeader('username',username);
        //req.setHeader('password',password);
        
        String body = '{ "username": "'+Usrname+'", "password": "'+password+'" }';
        system.debug('@@@@@' + body);
        req.setBody(body);
        
        
         //Construct Endpoint
        String endpoint = label.oktaendpoint;//'https://varian.okta.com/api/v1/sessions?additionalFields=cookieTokenUrl';
 

        //Set Method and Endpoint and Body
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        
        //String cookieTokenUrl = null; 
          String UserId;
          String SessionId;  
        System.debug('Details---->>UserId: ' + UserId + ' SessionId : ' + SessionId);
         try {
           //Send endpoint to OKTA
           system.debug('body----' + req.getbody()+'-----'+req.getheader('Authorization'));
           if(!Test.isRunningTest()){res = http.send(req);}
           system.debug('aebug ; ' +res.getBody());
           JSONParser parser = JSON.createParser(res.getBody());
           while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){  String fieldName = parser.getText();  parser.nextToken();
                    if(fieldName == 'cookieToken')  cookieTokenUrl = parser.getText();       
                    if(fieldName == 'userId') UserId = parser.getText();
                    if(fieldName == 'id') SessionId = parser.getText();                    
                }
           }
           
            String OktaStatus;
            IF(cookieTokenUrl == NULL)
            {
            String Userid1;
            //Construct HTTP request and response
            HttpRequest req1 = new HttpRequest();
            HttpResponse res1 = new HttpResponse();
            Http http1 = new Http();

            //Construct Authorization and Content header
            //Blob headerValue = Blob.valueOf(username+':'+password);
            String strToken1 = Label.oktatoken; //'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
            String authorizationHeader1 = 'SSWS ' + strToken1;
            req1.setHeader('Authorization', authorizationHeader1);
            req1.setHeader('Content-Type','application/json');
            req1.setHeader('Accept','application/json');

            //   String endpoint1 = 'https://varian.okta.com/api/v1/users/'+Cont.OktaId__c;
            String endpoint1 = 'https://varian.okta.com/api/v1/users/'+Usrname;

            //Set Method and Endpoint and Body
            req1.setMethod('GET');
            req1.setEndpoint(endpoint1);
            if(!Test.isRunningTest()){
            res1 = http.send(req1);
            }

            system.debug('aebug ; ' +res1.getBody());

            JSONParser parser1 = JSON.createParser(res1.getBody());
            while (parser1.nextToken() != null) {
            if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME)){string fieldName1 = parser1.getText();parser1.nextToken();
            if(fieldName1 == 'id') {Userid1 = parser1.getText();   }
            if(fieldName1 == 'status') {OktaStatus = parser1.getText();
            }
            }
            }
            if(Userid1 != null && OktaStatus!='LOCKED_OUT') {IsPassExist=true;  }

            else
            {
            IsPassExist=false;
            System.debug('user not exist----->');

            }           
           
           }
           if(cookieTokenUrl != null) ready = true;
           else if(IsPassExist!=null && IsPassExist==true) IsPassExist=true;
           else if(OktaStatus=='LOCKED_OUT')  corrlog1=true;
           else  corrlog = true;  
           
        }catch(System.CalloutException e) { System.debug(res.toString()); }
        if(cookieTokenUrl != null){
            User u = [Select id from user where Contact.OktaId__c =: UserId and isactive = true Limit 1];
            Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
            sessionmap = usersessionids__c.getall();
            usersessionids__c usersessionrecord = new usersessionids__c(name = u.id, session_id__c = SessionId);
            if(!sessionmap.containskey(u.id)){  insert usersessionrecord; } else{  usersessionrecord.Id = sessionmap.get(u.id).id;   update usersessionrecord;   }       
            //return new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState='+Label.cp_site + '/apex/CpCasePage');
            //if(RelayState != null)
            pagereference pref;  Createsuportcase = ApexPages.currentpage().getparameters().get('Createsuportcase');
            ProductImprvementRequest = ApexPages.currentpage().getparameters().get('ProductImprvementRequest');
            if (Createsuportcase == 'createsport'){
            pref = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/CpCasePage');
            } else if (ProductImprvementRequest == 'Prodimpreq') {
            //Commented By Rakesh
            //pref = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/CpProdImprove?Request=ProductImprovement');
            //Added By Rakesh
            pref = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/Submit_Product_Idea_v1');
            }
            else  pref = new pagereference(Label.oktacploginurl + cookieTokenUrl);
            pref.setredirect(true); return pref;
        }
        else return null;
        
}        

public pagereference openCasePage()
{
    pagereference pref;
    pref = new pagereference('/apex/CpCasePage');
    pref.setredirect(true);
    return pref;
}
}
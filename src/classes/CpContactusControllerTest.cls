/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 12-June-2013
    @ Description   :  Test class for CpContactusController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   08-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest
Public class CpContactusControllerTest
{
       
       //Method for covering scenarios where Portal Region not equals NA and running user as current logged in user
       
       public static testmethod void TestContactusController()   
        {
            User thisUser = [ select Id, ContactId, Profile.Name, Contact.Distributor_Partner__c, Contact.Account.country1__r.name from User where Id =:UserInfo.getUserId() ];
            System.runAs ( thisUser )
            {     
                Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
                insert TestCountry1;
                
                Account objACC  =new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12232',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx', Country1__c = TestCountry1.id, Country__c='test');
                insert objACC;
                      
                Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                Account objAcct = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='12232',BillingCity='California',BillingCountry='test',BillingState='Washington',BillingStreet='xyx', Country1__c = TestCountry1.id, Country__c='test');
                insert objAcct;
                
                Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=objAcct.Id,MailingCountry ='Zimbabwe', MailingState='teststate' );
                insert con;
                system.debug('ContactId---'+con.id);
                //UserRole r = [Select id from userrole where name='CEO'];
                User u = new User(alias = 'standt', email='standarduser@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', 
                languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
                username='standarduser@testclass.com'/*,UserRoleid=r.id*/);
                insert u;
                
                Support_Case_Approved_Countries__c objSCA = new Support_Case_Approved_Countries__c ();
                objSCA.Name = 'USA';
                insert objSCA;
                
                Contact_Role_Association__c objCRA = new Contact_Role_Association__c();
                objCRA.Account__c = objAcct.id;
                objCRA.contact__c = con.id;
                insert objCRA;
                
            
                System.runAs (u)
                {
                    Regulatory_Country__c regcount=new Regulatory_Country__c();
                    regcount.Name = 'Zimbabwe';
                    regcount.Portal_Regions__c ='EMEA';
                    insert regcount;
                    system.assertequals(regcount.Portal_Regions__c , 'EMEA');
                    CpContactusController Contactus=new CpContactusController();
                    CpContactusController.logInUser();             
                    Contactus.Logintookta();
                    ContactUs.openCasePage();
                    //Contact c=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe');
                    //insert c;
                } 
            
            
        }
        
    }
        //Method for covering scenarios where Portal Region equals NA
        
        public static testmethod void TestContactusController1()   
        {
            Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
            insert TestCountry1;
            
            Account objACC  =new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test', Country1__c = TestCountry1.id );
            insert objACC;
                  
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            Account objAcct = new Account(distributor__c = true,name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test', Country1__c = TestCountry1.id   );
            insert objAcct;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=objAcct.Id,MailingCountry ='Zimbabwe', MailingState='teststate'/*, Distributor_Partner__c = true*/);
            insert con;
            system.debug('ContactId---'+con.id);
            //UserRole r = [Select id from userrole where name='CEO'];
            User u = new User(alias = 'standt', email='standarduser@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
            username='standarduser@testclass.com'/*,UserRoleid=r.id*/);
            //insert u;
            
            Support_Case_Approved_Countries__c objSCA = new Support_Case_Approved_Countries__c ();
            objSCA.Name = 'USA';
            insert objSCA;
            
            Contact_Role_Association__c objCRA = new Contact_Role_Association__c();
            objCRA.Account__c = objAcct.id;
            objCRA.contact__c = con.id;
            insert objCRA;
            
                
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Portal_Regions__c ='NA';
            insert regcount;
            system.assertequals(regcount.Portal_Regions__c , 'NA');
            CpContactusController Contactus=new CpContactusController();
            CpContactusController.logInUser();             
        } 
        
        //Method for covering scenarios where Portal Region equals NA and running user as current logged in user
        
        public static testmethod void TestContactusController2()   
        {
                User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
                System.runAs ( thisUser )
                {     
                    User u;
                   
                     Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
                    insert objACC;                   
                   
                    Account a;      
                    Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                    a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='12232',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx', Country__c='USA', OMNI_Postal_Code__c='12345' );
                    insert a;
                    
                    Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='USA', MailingState='teststate');
                    insert con;
                    
                    system.debug('ContactId---'+con.id);
                    
                   // UserRole r = [Select id from userrole where name='CEO'];
                    u = new User(alias = 'standt', email='standarduser@testorg.com', 
                    emailencodingkey='UTF-8', lastname='Testing', 
                    languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
                    username='standarduser@testclass.com'/*,UserRoleid=r.id*/);
                    insert u;
                    
                    System.runAs (u)
                    {
                    
                        Regulatory_Country__c regcount=new Regulatory_Country__c();
                        regcount.Name = 'USA';
                        regcount.Portal_Regions__c ='NA';
                        insert regcount;
                        system.assertequals(regcount.Portal_Regions__c , 'NA');
                        CpContactusController Contactus=new CpContactusController();
                    
                    }
            } 
        
        }
}
public class CpContentDetailController
{
    public ContentVersion currDoc {get; set;}
    public List<ContentVersion> childDocs;
    public List<String> lib {get; set;}
    public String Isvisible {get;set;}
    public CpContentDetailController(ApexPages.StandardController controller)
    { 
        String Modes =ApexPages.currentPage().getParameters().get('Mode');
        if(Modes=='ParentEdit')
        {
         Isvisible='false';
        }
        currDoc = [SELECT Id, Title, TagCsv, ContentDocumentId, FirstPublishLocationId, ContentSize, Document_Version__c, Document_Type__c, Document_Number__c, Date__c, Mutli_Languages__c, Region__c, Newly_Created__c, Recently_Updated__c, Document_Language__c, Parent_Documentation__c FROM ContentVersion
                    WHERE Id = :ApexPages.currentPage().getParameters().get('Id') ];
        List<ContentWorkspaceDoc> conWrkDoc = [SELECT ContentDocumentId, ContentWorkspaceId FROM ContentWorkspaceDoc WHERE ContentDocumentId = :currDoc.ContentDocumentId];
        System.debug('List of ContentWorkspaceDoc:'+conWrkDoc);
        Set<Id> cntwrkspace = new Set<Id>();
        for(ContentWorkspaceDoc cwd : conWrkDoc)
        {
            cntwrkspace.add(cwd.ContentWorkspaceId);
        }
        List<ContentWorkspace> addLib = [SELECT Id, Name FROM ContentWorkspace WHERE Id IN :cntwrkspace];
        system.debug('List of Libraries:'+addLib);
        lib = new List<String>();
        for(ContentWorkspace cws : addLib)
        {
            lib.add(cws.Name);
        }
        system.debug('Name of Libraries:'+lib);
    }
    
    public List<ContentVersion> getchildDocs()
    {
        if(currDoc.Mutli_Languages__c && currDoc.Document_Number__c!=null)
        {
            childDocs = new List<ContentVersion>();
            system.debug('Current Document Number:'+currDoc.Document_Number__c);
            childDocs = [SELECT Id, Title, Document_Number__c, Document_Language__c, Parent_Documentation__c FROM ContentVersion WHERE Parent_Documentation__c = :currDoc.Document_Number__c and IsLatest=True];
            return childDocs;
        }
        else
            return null;
    }
    
    public pagereference download()
    {
        pagereference pref;
        pref = new pagereference('/sfc/servlet.shepherd/version/download/'+currDoc.Id);
        pref.setredirect(false);
        return pref;
    }
    
    public pagereference addChild()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpAddChildDoc?contId='+currDoc.Id);
        pref.setredirect(true);
        return pref;
    }

}
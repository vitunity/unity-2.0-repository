@isTest

public class CpContentDetailControllerTest
{
    static testmethod void testMethod1()
    {
        Test.StartTest();
        
        ContentVersion cont = new ContentVersion();
        cont.Title = 'Test Title';
        cont.VersionData = blob.valueOf('xyz');
        cont.Origin = 'C';
        cont.pathOnClient = 'Test Title';
        cont.FirstPublishLocationId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        cont.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'ContentVersion' AND DeveloperName = 'Product_Document'].Id;
        cont.Mutli_Languages__c = true;
        insert cont;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(cont); 
        ApexPages.currentPage().getParameters().put('Id',cont.Id);
        CpContentDetailController inst = new CpContentDetailController(controller1);
        List<ContentVersion> child = inst.getchildDocs();
        pagereference pge = inst.download();
        pge = inst.addChild();
        
        Test.StopTest();
    }
        
}
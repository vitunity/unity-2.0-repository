/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 12-May-2013
    @ Description   : An Apex class for creating content data from a single page.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   10-Jun-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


Public class CpCreateContentController {

    public PageReference SObjectTraining() {
        Schema.DescribeSObjectResult trc = Training_Course__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + trc.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectBanner() {
        Schema.DescribeSObjectResult br = BannerRepository__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + br.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectEvent() {
        
        Schema.DescribeSObjectResult eve = Event_Webinar__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + eve.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
     public PageReference SObjectContent() {
        
        Schema.DescribeSObjectResult ContVers = ContentVersion.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/sfc/#' + ContVers.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
  /*  public PageReference SObjectGvtAffair() {
        Schema.DescribeSObjectResult gaf = Government_affairs__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + gaf.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }*/
    
    /*public PageReference SObjectMarketingKit() {
        Schema.DescribeSObjectResult mkr = MarketingKitRole__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + mkr.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }*/
    
    Public PageReference SobjectMarketingKitContent(){
        /*Id recTypeId = [Select Id,Name From RecordType Where SObjectType ='ContentVersion' And Name = 'Marketing Kit' Limit 1][0].Id;
        System.debug('Recordtype Id'+recTypeId);
        PageReference prRef = new PageReference('/sfc/#workspaces '+recTypeId );
        prRef.setRedirect(true);
        return prRef;*/
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='ContentVersion' And Name = 'Marketing Kit' LIMIT 1];
        Schema.DescribeSObjectResult d = Schema.SObjectType.ContentVersion;
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get(rt.name);
        system.debug('recordtype Name'+rtByName);
        PageReference prRef = new PageReference('/sfc/#'+rtByName);
        prRef.setRedirect(true);
        return prRef;

    }
    
    
    public PageReference SObjectMessageCenter() {
        Schema.DescribeSObjectResult mct = MessageCenter__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + mct.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectMonteCarlo() {
        Schema.DescribeSObjectResult mc = Monte_Carlo__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + mc.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
     public PageReference SObjectDocumentList() {
        Schema.DescribeSObjectResult DocList= Document_List__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + DocList.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectInstitution() {
        Schema.DescribeSObjectResult Ins= Account.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + Ins.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectTBDevloper() {
        Schema.DescribeSObjectResult DevMode= Developer_Mode__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + DevMode.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectTechHelp() {
        Schema.DescribeSObjectResult TechHelp = ProdCountEmailMapping__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + TechHelp.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
    
    public PageReference SObjectPresentations() {
        Schema.DescribeSObjectResult Pres = Presentation__c.SObjectType.getDescribe();
        PageReference prRef = new PageReference('/' + Pres.getKeyPrefix());
        prRef.setRedirect(true);
        return prRef;
    }
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 10-June-2013
    @ Description   :  Test class for CpCreateContentController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   08-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest

Public class CpCreateContentControllerTest{

    //Method for calling CpCreateContentController class
   
    static testmethod void testCreateContent(){
  
        Test.StartTest();
            CpCreateContentController CreateContent = new CpCreateContentController();
            CreateContent.SObjectTraining();
            CreateContent.SObjectBanner();
            CreateContent.SObjectEvent();
            CreateContent.SObjectContent();
            //CreateContent.SObjectGvtAffair();
            CreateContent.SobjectMarketingKitContent();
            CreateContent.SObjectMessageCenter();
            CreateContent.SObjectMonteCarlo();
            CreateContent.SObjectDocumentList();
            CreateContent.SObjectInstitution();
            CreateContent.SObjectTBDevloper();
            CreateContent.SObjectTechHelp();
            CreateContent.SObjectPresentations();   
        Test.StopTest();

    }
    
   
    
}
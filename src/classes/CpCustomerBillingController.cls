public class CpCustomerBillingController
{

    public String ConfirmPass { get; set; }

    public String Newpass { get; set; }

    public String CustRecoveryAns { get; set; }

    public String CustRecovryQ { get; set; }

    public boolean closeWindow { get; set; }

    public boolean closechWindow { get; set; }

    public boolean saveflag { get; set; }

    public string acctid {get; set; }

    public string attachmentids {get;set;}

    public string invwoattids {get;set;}

    public string invwoattidsforopen {get;set;}


      
    public string getFirstlogin() {
        string rt = 'false';
        User cuser = [Select contact.PasswordReset__c from user where id = :Userinfo.getUserId() limit 1];
        if(cuser.contact <> null && !cuser.contact.PasswordReset__c)
        {
            rt= 'true';
        }
        return rt;
    }

    public Invoice__c invoice { get; set; }
    public Invoice__c invoiceFrom { get; set; }
    public Invoice__c invoiceTo { get; set; }
    public String document_TypeSearch { get; set; }
    public List<SelectOption> payerOptions { get; set; }
    public List<InvoiceWrapper> invoiceWrappers{ get; set; }
    public String invoiceStatus { get; set; }
    private Set<String> erpPayer;
    private String soqlString;
    private String whereClause;
    private Date fromDate;
    private Date toDate;
    private String sortStr;
    private String sortTypeStr;
    private String salesServiceType;
    
    public String invoiceId;
    public Attachment att { get; set; }
    public boolean isAttachmentWindow { get; set; }
    public boolean isPayerNumBlank { get; set; }
    public String agingCustomerStatement { get; set; }
    

    public List<AttachmentWrapper> getAttachments()
    {
        String invoiceName = [select Id, Name from Invoice__c where Id =:invoiceId limit 1][0].Name;
        List<AttachmentWrapper> wrappers = new List<AttachmentWrapper>();
        for(Attachment atta : [select Id, ContentType, Name, LastModifiedDate, Owner.Name from Attachment where parentId =: invoiceId])
        {
            attachmentids = atta.id + '\n';
            if(!atta.Name.startsWith(invoiceName))
            {
                AttachmentWrapper aWrapper = new AttachmentWrapper(atta);
                wrappers.add(aWrapper);
            }
        }
        return wrappers;
    }

    public pagereference multipleattch()
    {
        system.debug('***CM attachmentids : '+attachmentids);
        PageReference extractpage = new PageReference('/apex/mergerattachmentpage');        
        extractpage.setRedirect(true);
        return extractpage;
    }
    public CpCustomerBillingController()
    {
        saveflag = true;
        invoiceId = ApexPages.currentPage().getParameters().get('Id');
        att = new Attachment();
        agingCustomerStatement = '';
        if(String.isNotBlank(invoiceId))
        {
            isAttachmentWindow = true;
        }
        else
        {
            isAttachmentWindow = false;
        }
        initializeThePage();
    }

    private void initializeThePage()
    {
        attachmentids = '';
        sortStr = 'asc';
        sortTypeStr = 'Sales';
        whereClause = '';
        String whereId = '';
        invoiceStatus = 'Unpaid';
        salesServiceType = 'None';
        document_TypeSearch = '';
        invoice = new Invoice__c();
        invoiceFrom = new Invoice__c();
        invoiceTo = new Invoice__c();
        payerOptions = new List<SelectOption>();
        payerOptions.add(new SelectOption('', 'All'));
        erpPayer = new Set<String>();
        Set<Id> portalSoldToIds = new Set<Id>();
        isPayerNumBlank = true;

        List<User> users = [select Id, ContactId, Contact.AccountId, Contact.ERP_Payer_Number__c, Contact.Functional_Role__c from User where Id =: Userinfo.getUserId()];
        system.debug('***CM users: '+users);
        system.debug('***CM payerOptions: '+payerOptions);
        attachmentids = '00P3C000000iCZkUAM' + '\n' + '00P3C000000iCZuUAM' + '\n' + '00P3C000000iCaOUAU' + '\n' + '00P3C000000aq2DUAQ' + '\n';

        if(users.size() > 0)
        {
            String tempStr = '';

            if(users[0].Contact.Functional_Role__c == 'Account Payable Capital')
            {
                salesServiceType = 'Sales';
            }    
            else if(users[0].Contact.Functional_Role__c == 'Account Payable Service')
            {
                salesServiceType = 'Service';
            }
            else
            {
                salesServiceType = 'All';
            }
            if(users[0].Contact != null && users[0].Contact.AccountId != null)
            {
                tempStr += '\'';
                tempStr += users[0].Contact.AccountId;
                tempStr += '\',';
                portalSoldToIds.add(users[0].Contact.AccountId);
            }
            for(Contact_Role_Association__c acr : [select Account__c, Contact__c from Contact_Role_Association__c where Contact__c =: users[0].ContactId])
            {
                tempStr += '\'';
                tempStr += acr.Account__c;
                tempStr += '\',';
                portalSoldToIds.add(acr.Account__c);
            }
            if(tempStr.endsWith(','))
            {
                tempStr = tempStr.subString(0, tempStr.length()-1);
            }
            if(String.isNotBlank(tempStr))
            {
                whereId += '(';
                whereId += tempStr;
                whereId += ')';
            }
            if(String.isNotBlank(users[0].Contact.ERP_Payer_Number__c))
            {
                system.debug('***CM going inside if loop:');
                invoice.ERP_Payer__c = users[0].Contact.ERP_Payer_Number__c;
                List<ERP_Partner__c> erpps = [select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c = :users[0].Contact.ERP_Payer_Number__c];
                if(erpps.size() > 0)
                {
                    payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c + ' ' + erpps[0].Name));
                }
                else
                {
                    payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c));
                }
                isPayerNumBlank = false;
            }
        }
        system.debug('***CM payerOptions: '+payerOptions);
        /*
        for(Attachment atta : [select Id, ParentId, Name, ContentType from Attachment where ParentId in: portalSoldToIds and Name Like 'Aging_Statement%' order by LastModifiedDate desc])
        {
            if(String.isBlank(agingCustomerStatement))
            {   
                agingCustomerStatement = atta.Id;
            }
        }
        */
        invoiceWrappers = new List<InvoiceWrapper>();

        if(String.isNotBlank(whereId))
        {
            whereClause += ' where ';
            whereClause += '(Sold_To__c in';
            whereClause += whereId;
            whereClause += ' or ';
            whereClause += ' Site_Partner__c in';
            whereClause += whereId;
            whereClause += ')';

            soqlString = 'Select Work_Order__c, Status__c, Resolution_Comments__c, Sales_Service_Type__c, CurrencyIsoCode, Sold_To__c, Site_Partner__c, Service_Maintenance_Contract__c, Sales_Org__c, Sales_Order__c, Reason_Detail__c, Reason_Code__c, Quote__c, ERP_Payer__c, Name, Invoice_Due_Date__c, Invoice_Date__c, Invoice_Cleared_Date__c, convertcurrency(Amount__c), Quote__r.Name, Service_Maintenance_Contract__r.Name, Work_Order__r.Name, Sold_To__r.Name, (select Id, Name from Attachments order by LastModifiedDate asc) From Invoice__c';
            System.debug(soqlString + whereClause);
            searchRecords();
            //buildInvoiceWrappers(soqlString + whereClause);
        }
        if(invoiceWrappers.size() > 0)
        {
            sortInvoices('asc');
        }
    }

    private void buildInvoiceWrappers(String soqlString)
    {
        List<Invoice__c> invoices = new List<Invoice__c>();
        Set<Id> attachmentParentIds = new Set<Id>();
        Set<String> woNames = new Set<String>();
        Set<String> tempPayNumber = new Set<String>();
        for(sObject s : Database.query(soqlString))
        {
            Invoice__c inv = (Invoice__c)s;
            if(String.isNotBlank(inv.Work_Order__c))
            {
                woNames.add(inv.Work_Order__r.Name + '.pdf');
            }
            if(String.isNotBlank(inv.Quote__c))
            {
                attachmentParentIds.add(inv.Quote__c);
            }
            invoices.add(inv);
            if(String.isNotBlank(inv.ERP_Payer__c))
            {
                tempPayNumber.add(inv.ERP_Payer__c);
            }
        }

        for(ERP_Partner__c erp :[select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c in :tempPayNumber])
        {
            if(!erpPayer.contains(erp.Partner_Number__c))
            {
                erpPayer.add(erp.Partner_Number__c);
                payerOptions.add(new SelectOption(erp.Partner_Number__c, erp.Partner_Number__c + ' ' + erp.Name));
                tempPayNumber.remove(erp.Partner_Number__c);
            }
        }
        for(String num : tempPayNumber)
        {
            if(!erpPayer.contains(num))
            {
                erpPayer.add(num);
                payerOptions.add(new SelectOption(num, num));
                tempPayNumber.remove(num);       
            }
        }
        

        Map<Id, List<String>> ParentIdToAttachmentId = new Map<Id, List<String>>();
        Map<Id, String> attaIdToName = new Map<Id, String>();
        for(Attachment atta : [select Id, ParentId, Name, ContentType from Attachment where (Name in: woNames or ParentId in: attachmentParentIds) order by LastModifiedDate])
        {
            attachmentids = atta.id + '\n';
            if(!ParentIdToAttachmentId.keySet().contains(atta.ParentId))
            {
                List<String> attaIds = new List<String>();
                attaIds.add(atta.Id);
                ParentIdToAttachmentId.put(atta.ParentId, attaIds);
            }
            else
            {
                ParentIdToAttachmentId.get(atta.ParentId).add(atta.Id);
            }
            attaIdToName.put(atta.Id, atta.Name);
        }

        for(Invoice__c inv : invoices)
        {
            InvoiceWrapper invWra = new InvoiceWrapper(inv);
            if(String.isNotBlank(inv.Work_Order__c) && ParentIdToAttachmentId.keySet().contains(inv.Work_Order__c))
            {
                invWra.woAttachmentId = ParentIdToAttachmentId.get(inv.Work_Order__c)[0];
            }
            if(String.isNotBlank(inv.Quote__c) && ParentIdToAttachmentId.keySet().contains(inv.Quote__c))
            {
                for(String str : ParentIdToAttachmentId.get(inv.Quote__c))
                {
                    if(attaIdToName.keySet().contains(str) && attaIdToName.get(str).startsWith('2'))
                    {
                        invWra.quoteAttachmentId = str;
                        break;
                    }
                }
            }
            invoiceWrappers.add(invWra);
        }
    }

    public PageReference toggleSort()
    {
        sortStr = sortStr.equals('asc') ? 'desc' : 'asc';
        sortInvoices(sortStr);
        return null;
    }

    public PageReference toggleSortType()
    {
        sortTypeStr = sortTypeStr.equals('Sales') ? 'Service' : 'Sales';
        sortInvoicesByType(sortTypeStr);
        return null;
    }
    public void saveflagfn()
    {
        saveflag = false;
    }
    public PageReference searchRecords()
    {
        String tempClause = '';
        if(String.isNotBlank(soqlString) && String.isNotBlank(whereClause))
        {
            if(salesServiceType != 'All')
            {
                tempClause += ' and Sales_Service_Type__c = \'' + salesServiceType + '\'';
            }
            if(invoiceStatus != 'All')
            {
                tempClause += ' and Status__c = \'' + invoiceStatus + '\'';
            }
            if(String.isNotBlank(document_TypeSearch))
            {
               tempClause += ' and (Name like \'%' + document_TypeSearch + '%\' or Sold_To__r.Name like \'%' + document_TypeSearch + '%\' or Sold_To__r.Name like \'%' + document_TypeSearch + '%\' or Quote__r.Name like \'%' + document_TypeSearch + '%\' or Work_Order__r.Name like \'%' + document_TypeSearch + '%\')' ;
            }
            if(String.isNotBlank(acctid))
            {
                tempClause += ' and ERP_Payer__c = \'' + acctid + '\'';
            }
            if(invoiceFrom.Invoice_Cleared_Date__c != null)
            {
                fromDate = invoiceFrom.Invoice_Cleared_Date__c;
                tempClause += ' and Invoice_Date__c >= : fromDate'; 
            }
            
            if(invoiceTo.Invoice_Cleared_Date__c != null)
            {   
                toDate = invoiceTo.Invoice_Cleared_Date__c;
                tempClause += ' and Invoice_Date__c <= : toDate'; 
            }
            System.debug(soqlString + whereClause);
            invoiceWrappers.clear();
            buildInvoiceWrappers(soqlString + whereClause + tempClause);
            if(invoiceWrappers.size() > 0)
            {
                sortInvoices('asc');
            }
        }
        System.debug('***CM acctid : '+acctid);
        getageingstatement();
        return null;
    }
    public void getageingstatement()
    {
        agingCustomerStatement = null;
        if (!string.isBlank(acctid)){
            String accsfid = '';
            For (Account acc : [select Id, name from account where Ext_Cust_Id__c = :acctid limit 1])
            {
                accsfid = acc.id;
            }
            for(Attachment atta : [select Id, ParentId, Name, ContentType from Attachment where ParentId = :accsfid and Name Like 'Aging_Statement%' order by LastModifiedDate desc limit 1])
            {
                if(String.isBlank(agingCustomerStatement))
                {   
                    agingCustomerStatement = atta.Id;
                }
            }
        }
    }
    private void sortInvoices(String sortStr)
    {

        Integer InvoiceSize = invoiceWrappers.size();
        List<InvoiceWrapper> sortedInvoices = new List<InvoiceWrapper>();

        while(sortedInvoices.size() < InvoiceSize)
        {
            Date ot = Date.newInstance(1900, 12, 31); 
            Date nt = Date.newInstance(2999, 12, 31); 
            Integer mini = 0;
            for(Integer i = 0; i < invoiceWrappers.size(); i++)
            {
                if(sortStr == 'asc')
                {
                    if(invoiceWrappers[i].invoiceDate < nt)
                    {
                        nt = invoiceWrappers[i].invoiceDate;
                        mini = i;
                        System.debug('InvoiceWrapper: ' + nt);
                    }
                }
                else
                {
                    if(invoiceWrappers[i].invoiceDate > ot)
                    {
                        ot = invoiceWrappers[i].invoiceDate;
                        mini = i;
                    }
                }
            }
            sortedInvoices.add(invoiceWrappers[mini]);
            invoiceWrappers.remove(mini);
            System.debug('sortedInvoices: ' + sortedInvoices.size());
            System.debug('invoiceWrappers: ' + invoiceWrappers.size());
        }

        if(sortedInvoices.size() == InvoiceSize)
        {
            if(invoiceWrappers.size() != 0)
            {
                invoiceWrappers.clear();
            }
            invoiceWrappers.addAll(sortedInvoices);
        }
    }

    private void sortInvoicesByType(String sortStr)
    {
        List<InvoiceWrapper> newInvoices = new List<InvoiceWrapper>();
        for(Integer i = 0; i < invoiceWrappers.size(); i++)
        {
            if(invoiceWrappers[i].invoice.Sales_Service_Type__c == sortStr)
            {
                newInvoices.add(invoiceWrappers[i]);
                invoiceWrappers.remove(i);
            }
        }
        if(invoiceWrappers.size() > 0)
        {
            newInvoices.addAll(invoiceWrappers);
            invoiceWrappers.clear();
        }
        if(invoiceWrappers.size() == 0)
        {
            invoiceWrappers.addAll(newInvoices);
        }
    }

    public PageReference submitRecords()
    {
        List<Invoice__c> updateInvoices = new List<Invoice__c>();
        if(invoiceWrappers.size() > 0)
        {
            for(InvoiceWrapper invWra : invoiceWrappers)
            {
                updateInvoices.add(invWra.invoice);
            }
        }
        if(updateInvoices.size() > 0)
        {
            update updateInvoices;
        }
        return null;
    }
    public PageReference reset()
    {
        acctid = null;
        initializeThePage();
        return null;
    }
    public PageReference download()
    {
        return Page.CpCustomerBillingExcel;
    }
    public pagereference dummy(){
        return null;
    }
    public PageReference save()
    {
        if(att.body != null)
        {
            att.parentId = invoiceId;
           // att.ContentType = 'application/pdf';
            insert att;
        }
        System.debug('***invoiceID: '+invoiceId);
        string urladdress = '/apex/CpCustomerBilling?Id='+invoiceId;
        system.debug('***urladdress: '+urladdress);
        PageReference billPage = new PageReference(urladdress);        
        billPage.setRedirect(true);
        return billpage;
    }
    /*
    public PageReference cancel()
    {
        PageReference billPage = new PageReference('/apex/CpCustomerBilling');        
        billPage.setRedirect(true);
        return billPage;
    }
    public PageReference Done()
    {
        PageReference billPage = new PageReference('/apex/CpCustomerBilling');        
        billPage.setRedirect(true);
        return billPage;
    }
    */
    public class InvoiceWrapper 
    {
        public Invoice__c invoice { get; set; }
        public Id woAttachmentId { get; set; }
        public Id quoteAttachmentId { get; set; }
        public Id invoiceAttachmentId { get; set; }
        public Boolean hasAttachmens { get; set; }
        private Date invoiceDate;
        public Boolean selected {get; set;}
        public string amt {get;set;}
        public Boolean att {get;set;}

        public InvoiceWrapper(Invoice__c invoice)
        {
            this.hasAttachmens = false;
            this.invoice = invoice;
            this.invoiceAttachmentId = invoiceAttachmentId;
            this.woAttachmentId = woAttachmentId;
            this.selected = selected;
            system.debug('***CM amount : '+ invoice.Amount__c);
            Decimal amtdec = invoice.Amount__c;
            system.debug('***CM amtdec : '+ amtdec);
            string queryamt = String.valueof(amtdec);
            System.debug('***CM strng amount : '+queryamt);
            //this.amt = invoice.Amount__c;
            if(invoice.Attachments.size() > 0)
            {
                for(Attachment atta : invoice.Attachments)
                {
                    if(!atta.Name.startsWith(invoice.Name))
                    {
                        this.hasAttachmens = true;
                    }
                    else
                    {
                        this.hasAttachmens = false;
                        this.invoiceAttachmentId = atta.Id;
                    }
                    //this.att = true;
                }
            }
            else
            {
                this.att = false;
            }
            invoiceDate = invoice.Invoice_Date__c;
            this.selected = selected;
        }
    }
    public class AttachmentWrapper
    {
        public Attachment atta { get; set; }
        public AttachmentWrapper(Attachment atta)
        {
            this.atta = atta;
        }

        public PageReference deleteAttachment()
        {
            if(atta.Id != null)
            {
                delete atta;
            }
            return null;
        }
    }
     Public List<SelectOption> getrecoveryqusoptions()
    {
               list<SelectOption> recoveryqusoptions = new list<SelectOption>();
          // Get the object type of the SObject.
          Schema.sObjectType objType = Contact.getSObjectType(); 
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values = fieldMap.get('Recovery_Question__c').getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          { 
             recoveryqusoptions.add(new SelectOption(a.getLabel(), a.getValue())); 
          }
          return recoveryqusoptions;
               
    } 
    public pagereference resetpassword()
     {
       String Recoveryquestion = ApexPages.currentPage().getParameters().get('RecoveryQuestionparam');
       String RecoveryAnswer = ApexPages.currentPage().getParameters().get('RecoveryAnswerparam');
       String NewPassword = ApexPages.currentPage().getParameters().get('NewPasswordparam');
       RecoveryAnswer = RecoveryAnswer.deleteWhitespace().toLowerCase();
       system.debug('---RecoveryQuestion' +'----' + CustRecovryQ + Recoveryquestion +'----Recovery Answer'+RecoveryAnswer + 'Password---'+NewPassword);
       User usercontact = [Select ContactId,Contact.Email,Contact.FirstName,Contact.LastName,Contact.Phone,Contact.OktaLogin__c,Contact.OktaId__c from user where id=: Userinfo.getUserId() limit 1];
       //http Request creation and capturing response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //EndPoint Creation
        String EndPoint = label.oktachangepasswrdendpoint + usercontact.Contact.OktaId__c;
        req.setendpoint(EndPoint);
        req.setMethod('PUT');
         String body ='{"profile": {"firstName": "'+ usercontact.Contact.FirstName +'","lastName": "'+usercontact.Contact.LastName +'","email": "'+ usercontact.Contact.Email + '","login": "' + usercontact.Contact.OktaLogin__c + '"},"credentials": {"password" : { "value": "' + NewPassword + '" },"recovery_question": {"question": "'+ Recoveryquestion +'","answer": "'+ RecoveryAnswer + '"}}}';
         req.setBody(body);
         try {
           //Send endpoint to OKTA
           system.debug('aebug ; ' +req.getBody());
           If(!Test.IsRunningTest()){
           res = http.send(req);
           }ELSE
           {
           fakeresponse.fakeresponsemethod('activation');
           }
           if(res.getbody().contains('errorSummary'))
           {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Password provided doesnot match.');
            ApexPages.addMessage(myMsg);
            return null;
           }
           system.debug('aebug ; ' +res.getBody());
         }catch(System.CalloutException e) {
            System.debug(res.toString());
        }
        // Updating Contact Record
        Contact updatecon = new Contact(id = usercontact.ContactId);
        updatecon.Recovery_Question__c = Recoveryquestion;
        updatecon.Recovery_Answer__c = RecoveryAnswer;
        updatecon.PasswordReset__c = true;
        updatecon.activation_url__c = '';
        updatecon.PasswordresetDate__c = system.today();// to capture the last date when the password was reseted
        try{
        update updatecon;
        }  catch(Exception e)  
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please contact support for the above mentioned error');
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please contact support for the above mentioned error  '+e.getmessage ());
            ApexPages.addMessage(myMsg);
            return null;  
        }
        pagereference p = new pagereference('/apex/CpCustomerBilling');     
        p.setredirect(true);
        return p;//new pagereference('/apex/CpCustomerBilling');
     }
     public pagereference logoutmethod()
    {
    
     Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
     sessionmap = usersessionids__c.getall();
     HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        String strToken = label.oktatoken;
        String authorizationHeader = 'SSWS ' + strToken;
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');
        String endpointgroup = Label.oktaendpointsessionkill;// + sessionmap.get(userinfo.getuserid()).session_id__c;
        reqgroup.setEndPoint(endpointgroup);
        reqgroup.setMethod('DELETE');
        try{
        if(!Test.IsRunningTest()){
        resgroup = httpgroup.send(reqgroup);
        }
        }catch(System.CalloutException e){
          system.debug(resgroup.toString());
        } 
return new pagereference('/secur/logout.jsp'); 
    }
    public void closePopup()
    {
        isAttachmentWindow=false;
        closeWindow = true;
    }


    public pagereference getAttids()
    {
        invwoattids = '';
        invwoattidsforopen = '';
        String openkey;
        for (InvoiceWrapper inv : invoiceWrappers)
        {
            openkey = '';
            if (inv.selected)
            {
                openkey = '';
                if (inv.invoiceAttachmentId != null)
                {
                    openkey = inv.invoiceAttachmentId ;
                    if (string.isblank(invwoattids))
                    {
                        invwoattids = inv.invoiceAttachmentId ;
                    }
                    else
                    {
                        invwoattids = invwoattids + '|' + inv.invoiceAttachmentId;
                    }
                }
                if (inv.woAttachmentId != null)
                {
                    if (string.isblank(invwoattids))
                    {
                        invwoattids = inv.woAttachmentId;
                    }
                    else
                    {
                        invwoattids = invwoattids + '|' + inv.woAttachmentId;
                    }
                    if (string.isblank(openkey))
                    {
                        openkey = inv.woAttachmentId;
                    }
                    else
                    {
                        openkey = openkey + '|' + inv.woAttachmentId;
                    }
                }
                
                if(!string.isBlank(openkey) && string.isBlank(invwoattidsforopen))
                {
                    invwoattidsforopen = openkey;
                }
                else if (!string.isblank(invwoattidsforopen))
                {
                    invwoattidsforopen = invwoattidsforopen + '%' + openkey;
                }
                System.debug('***CM openkey1: '+openkey);
                System.debug('***CM invwoattidsforopen1: '+invwoattidsforopen);
            }
            
            System.debug('***CM invwoattids: '+invwoattids);
            System.debug('***CM invwoattidsforopen: '+invwoattidsforopen);
            System.debug('***CM openkey: '+openkey);
        }
        system.debug('***CM selected attachments: '+invwoattids);
        return null;
    }

}
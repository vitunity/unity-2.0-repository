public class CpEventOncologySummitPhotosController {
	public String videoLink{get;set;}
    public CpEventOncologySummitPhotosController(){
        videoLink = '';
        for(ContentVersion cv : [Select Id from ContentVersion where title like '%OncoSummitGallery%']){
            videoLink = '/sfc/servlet.shepherd/version/download/' + cv.Id + '?asPdf=false';//&operationContext=CHATTER';
        }
    }
}
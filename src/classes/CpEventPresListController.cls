/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 26-Apr-2013
    @ Description   : An Apex class to retrive Event Presentation List .
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public with sharing class CpEventPresListController {

   
    public Boolean isGuest{get;set;}
    public string strBanner{get; set;}
    Id eventId;
    public Event_Webinar__c events{get;set;}
    public List<Presentation__c> presentations{get;set;}
    public Boolean DescVis{get;set;}
    public Boolean webvis{get;set;}
    public Boolean attachvis{get;set;}
     public string Usrname{get;set;}
    public string shows{get;set;}
    public Boolean attachPresVis{get;set;}
    public CpEventPresListController(){
        isGuest =SlideController.logInUser();
         system.debug('###########' + [Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype); 
         eventId = Apexpages.currentPage().getParameters().get('Id');
         fetchEvent();  
         shows = 'none';
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
           if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
            Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
            shows='block'; 
            isGuest = false;         
            }
        }        
    }
    
    //fetch event
    private void fetchEvent(){
        
        presentations = new list<Presentation__c>();
        
        events = [Select Id, e.Location__c, e.Institution__c, e.From_Date__c, Banner__c, Title__c, To_Date__c, Description__c,
                        URL_Path_Settings__c, (select id, title__c from Webinars__r where private__c = false), (SELECT Id, name FROM Attachments where contentType like '%pdf') From Event_Webinar__c e 
                        where recordtype.name = 'Events' and id = :eventId];                       
        if(events.Description__c=='<br>' || events.Description__c==''|| events.Description__c==null)
        {
        DescVis=false;
        }
        else
        {      
        DescVis=true;
        }
        if(events.Attachments.size()>0)
        {
        attachvis=true;
        } 
        if(events.Webinars__r.size()>0)
        {
        webvis=true;
        }               
       presentations = [Select p.Title__c, p.Speaker__c, p.Product_Affiliation__c, 
                       p.Name, p.Institution__c, p.Id, p.Event__r.To_Date__c, 
                       p.Event__r.From_Date__c, p.Event__r.Location__c, p.Event__r.Title__c, p.Event__r.Description__c,
                       p.Event__c, p.Description__c, p.Date__c,(SELECT Id, name FROM Attachments LIMIT 1) From Presentation__c p where Event__c = :eventId];
         if(presentations.size()>0)
         {
         attachPresVis=true;
         }                 
        if(events.Banner__c != null && events.Banner__c.length() > 93){
            System.debug('@@@@Test11111'+events.Banner__c);
            strBanner = events.Banner__c.substring(events.Banner__c.indexof('src')+5,events.Banner__c.indexof('src')+94);
            System.debug('@@@@Test'+strBanner );
        }
    }
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class forCpEventPresListController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   08-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class  CpEventPresListControllerTest{

    //Test Method for calling CpEventPresListController class
    
    static testmethod void testCpEventPresListController(){
            Test.StartTest();
            
             User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
             System.runAs ( thisUser ) {
                
                    Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                   
                    user u;
                    Account a;   
                    User internalUser;
                    
                   /* Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
                    insert actCntry;*/
                    
                    Regulatory_Country__c reg = new Regulatory_Country__c();
                    reg.Name='test' ; 
                    insert reg ;
                    
                    a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='test'  ); 
                    insert a;  
                       
                    Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test',MailingState='test'); 
                    insert con;
                    
                    u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
                    insert u; // Inserting Portal User
                    
                    
                    Profile p1 = [select id from profile where name='VMS Marketing - User'];
                    
                    internalUser = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg1.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p1.Id, timezonesidkey='America/Los_Angeles', username='standarduser@testclass1.com'/*,UserRoleid=r.id*/); 
            
                    insert internalUser;
                    
                }
                
                    user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
            
                    system.runAs(usr){
            
                        RecordType eventRT = [select Id , Name FROM RecordType WHERE Name ='Events' and SobjectType ='Event_webinar__c'];
                        Event_Webinar__c event = new Event_Webinar__c();
                        event.Title__c = 'Test Event2';
                        event.Location__c = 'India';
                        event.Banner__c='This banner created for test class and this data will not be commited to database because we are using starttest and stop test';
                        event.To_Date__c = system.today()-1;
                        event.From_Date__c = system.today()-2;
                        event.Needs_MyVarian_registration__c=False;
                        event.RecordTypeId = eventRT.Id; 
                        event.Description__c='desc';
                        Insert event;
                
                        ApexPages.currentPage().getParameters().put('Id' ,event.Id);
                        CpEventPresListController Events = new CpEventPresListController();
                 }
                  user usr1 = [Select Id, ContactId, Name from User where email=:'standarduser@testorg1.com' ];
                  system.runAs(usr1){
                        RecordType eventRT = [select Id , Name FROM RecordType WHERE Name ='Events' and SobjectType ='Event_webinar__c'];
                        Event_Webinar__c event = new Event_Webinar__c();
                        event.Title__c = 'Test Event2';
                        event.Location__c = 'India';
                        event.Banner__c='This banner created for test class and this data will not be commited to database because we are using starttest and stop test';
                        event.To_Date__c = system.today()-1;
                        event.From_Date__c = system.today()-2;
                        event.Needs_MyVarian_registration__c=False;
                        event.RecordTypeId = eventRT.Id; 
                        event.Description__c='desc';
                        Insert event;
                
                        ApexPages.currentPage().getParameters().put('Id' ,event.Id);
                        CpEventPresListController Events = new CpEventPresListController();
                  }
             Test.StopTest();
     }
 }
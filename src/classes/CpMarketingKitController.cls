/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 13-May-2013
    @ Description   : An Apex class for Marketing Center.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public without sharing class CpMarketingKitController {
    
    public List<MarketingKitRole__c> marketingKits{get; set;}
    public List<Product_Roles__c> marktkitinternaluser{get;set;} // for internal user
    List<User> portalUser;
    public Boolean IsMarketYourCenter{get;set;}
    public Boolean IsInternalUser{get;set;}
    public Boolean termsAccepted{get;set;}
    public Boolean MrktPlstermsAccepted{get;set;}
    public Boolean InternalUsr{get;set;}
    public Boolean ReloadPopup{get;set;}
    public String Reload{get;set;}
    public String Messages{get;set;}
    List<Contact> cont = new List<Contact>();
    public String marktProg{get;set;}
    //public Map<String, List<ContentVersion>> marktKitdocs{get;set;}
    public List<markDocs> marktKitdocs{get;set;}
    public String SurveyURL{get; set;}
    public String useridfromsurvey{get;set;}
    map<String,string> marketkitagrmntmap = new map<String,string>();
    public string shows{get;set;}
    public string Usrname{get;set;}
    
    public class markDocs implements Comparable{
        public String Header{get;set;}
        public List<subdocsclass> content{get; set;}
        public markDocs(){
            content = new List<subdocsclass>();
        }
      public Integer compareTo(Object objToCompare) {
            return Header.compareTo(((markDocs)objToCompare).Header);
            }
    }
    
    public class documents implements Comparable{
        public String image{get; set;}
        public String imgheight{get;set;}
        public String imgwidth{get;set;}
        public String thumbnail{get;set;}
        public String videoLink{get;set;} // added By Raj for video link storage
        public ContentVersion relContent{get; set;}
        /*public Documents(){
            imgheight = '0';
            imgwidth = '0';
        }*/
         public Integer compareTo(Object objToCompare) {
            return relContent.title.compareTo(((documents)objToCompare).relContent.title);
         }
    }
    public class subdocsclass implements Comparable{
      public String subgroupheader{get;set;}
      public List<documents> subgrouplist{get;set;}
        public subdocsclass(){
          subgrouplist = new list<documents>();
        }
        public Integer compareTo(Object objToCompare) {
            return subgroupheader.compareTo(((subdocsclass)objToCompare).subgroupheader);
        }
     }
     
    //constructor
    public CpMarketingKitController(){
        IsMarketYourCenter = False;
        IsInternalUser = False;
        //MarketingYourProgramAccess();
        system.debug('------ IN$ CpMarketingKitController');
        ReloadPopup=false;
        Messages='none';
        marktProg = Apexpages.currentPage().getParameters().get('prog');
       
        marktkitinternaluser = new list<Product_Roles__c>();
        useridfromsurvey = Apexpages.currentPage().getParameters().get('c');
        termsAccepted = false;
        system.debug('------User Id return---' + useridfromsurvey);
        System.debug('aebug ..marktProg  '+marktProg);
        shows = 'none'; 
        portalUser = [Select u.Id, u.ContactId,u.alias, u.AccountId From User u where id =: UserInfo.getUserId()  limit 1];
        if(portalUser[0].contactid == null)
        {
         shows = 'block';
         Usrname = portalUser[0].alias;
         termsAccepted = true;
        }
        marktkitinternaluser = [Select Product_Name__c,ismarketingKit__c,MkStylesheet__c,marketingkitorder__c from Product_Roles__c where ismarketingKit__c = true order by marketingkitorder__c];
        cont = [select id, MarketingKit_Agreement__c from Contact where id =: portalUser[0].ContactId limit 1];
                     for(Product_Roles__c pr : Product_Roles__c.getall().values()){
                if(pr.Survey_URL__c == null){continue;}
                if(pr.Product_Name__c == 'Premier Marketing Program'){
                   SurveyURL = pr.Survey_URL__c+'?c='+Userinfo.getUserId();
                }
                }
        
              for(Product_Roles__c pr : Product_Roles__c.getall().values()){
               if(pr.ismarketingKit__c){
                marketkitagrmntmap.put(pr.Product_Name__c,pr.Name);
                }
                system.debug('@@@@@@@' + pr.Product_Name__c + '---' + pr.Name);
            if(pr.Survey_URL__c == null){continue;}
              if(marktProg == pr.Product_Name__c){
                   SurveyURL = pr.Survey_URL__c+'?c='+Userinfo.getUserId();
                }
                
             }
        
        if(portalUser.size() > 0 && portalUser[0].ContactId != null &&  marktProg != null){
                //cont = [select id, MarketingKit_Agreement__c from Contact where id =: portalUser[0].ContactId limit 1];
                system.debug('-------------->'+marktProg);
                System.debug('aebug .. '+cont[0].MarketingKit_Agreement__c );
              String pcklst = '';
              pcklst = marketkitagrmntmap.get(marktProg);
              system.debug('-------------->'+marketkitagrmntmap.get('RapidArc Online Marketing Program'));
              if(pcklst != null){
             if(cont.size() > 0 && cont[0].MarketingKit_Agreement__c !=null && cont[0].MarketingKit_Agreement__c.contains(pcklst)){
                termsAccepted = true;
                
             }else{
                termsAccepted = false;
                
             }
             }
            
             
             
             /*for(Product_Roles__c pr : Product_Roles__c.getall().values()){
                marketkitagrmntmap.put(pr.Product_Name__c,pr.Name);
                if(pr.Survey_URL__c == null){continue;}
                if(marktProg == pr.Product_Name__c){
                   SurveyURL = pr.Survey_URL__c+'?c='+Userinfo.getUserId();
                }
                
             }*/
         
         }
         
         if(portalUser.size() > 0 && portalUser[0].ContactId != null){
                MrktPlstermsAccepted = false;
                 if(cont.size() > 0 && cont[0].MarketingKit_Agreement__c !=null &&(cont[0].MarketingKit_Agreement__c.contains('Edge Marketing Program') || cont[0].MarketingKit_Agreement__c.contains('TrueBeam Marketing Program'))){
                    MrktPlstermsAccepted = true;
                 }
             }
             
             InternalUsr = false;
          if(portalUser[0].contactid == null){
          InternalUsr = true;
          }
          
             
         if(cont.size()>0 && cont[0].MarketingKit_Agreement__c !=null){
         if(cont[0].MarketingKit_Agreement__c.contains('Premier Marketing Program'))
             {
              if(termsAccepted == false && marktProg == null)
               {
                termsAccepted = true;
               }
             }
         }
        
        fetchMarketingKit();
        if(marktProg != null){
            fetchMarkProgDocs();
        }
      Reload  =Apexpages.currentPage().getParameters().get('popup');
        if(Reload=='true')
        {
        termsAccepted = true;
        }
    }
    
      
     /*---------- Logic for Marketing Your Center access to only Users of US or Internal Users ------------ */
    Public pagereference MarketingYourProgramAccess(){
    
   /* public  Static list<MarketingYourCenterCountries__c> getAllMrkUrCntrAccessCountriesClass()
    {  
      return MarketingYourCenterCountries__c.getall().values();  
    }*/
    
    list<MarketingYourCenterCountries__c> AllCountryCount = new List<MarketingYourCenterCountries__c>();
     String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
               if(conCntry != null){
                AllCountryCount  = MarketingYourCenterCountries__c.getall().values(); 
                system.debug('AllCountryCount---'+AllCountryCount);
                    for(MarketingYourCenterCountries__c MrktCountries: AllCountryCount)
                    {
                        if(MrktCountries.Name ==  conCntry)
                        {
                            IsMarketYourCenter = true;
                        }
                    }   
                    
                }  
                
                if(conCntry == Null || conCntry == ''){
                
                 IsInternalUser = true;
                }
                
                system.debug('IsMarketYourCenter---'+IsMarketYourCenter);
                system.debug('IsInternalUser---'+IsInternalUser);
        pagereference pg;
        /*if (IsInternalUser == True || IsMarketYourCenter == True){
            pg = new pagereference('/apex/CpMarketingKit');
            pg.setredirect(true);
            system.debug('pg---'+pg);
            return pg;
        } */
        
        if(IsInternalUser == False && IsMarketYourCenter == False){
            pg = new pagereference(label.CpAccessDenied);
            system.debug('pg---'+pg);
            pg.setredirect(true);
            return pg;
        }
        system.debug('pg---'+pg);
    return Null;
    }
    
    /*----------- Logic Ends --------------------------------------------------------------------------------*/
    


      
    public pagereference ReloadMethod()
    {
     List<String> Ids=New List<String>();
          List<GroupMember> grplmember = new List<GroupMember>();
        grplmember = [Select GroupId,UserOrGroupId,Group.Name from GroupMember where Group.Name=:marktProg  and UserOrGroupId =: UserInfo.getuserId()];
        for(GroupMember gm: grplmember)
        {
        Ids.add(gm.UserOrGroupId);
        }
        SYSTEM.DEBUG('^^^^'+Ids);
  /*  cont = [select id, MarketingKit_Agreement__c from Contact where id =: portalUser[0].ContactId limit 1];
    String pcklst = '';
              pcklst = marketkitagrmntmap.get(marktProg);
              if(pcklst != null){
    if(cont.size() > 0 && cont[0].MarketingKit_Agreement__c !=null && cont[0].MarketingKit_Agreement__c.contains(pcklst) && Ids.size() > 0){*/
               if(Ids.size() > 0)
               {
               // termsAccepted = true;
                ReloadPopup=false;
                
             }else{
             
              //  termsAccepted = false;
                ReloadPopup=true;
               
             }
             //}
  if(ReloadPopup==false)
   {
  pagereference pref=new pagereference('/apex/CpMarketingKitPremier?prog='+marktProg+'&popup=true');
      pref.setredirect(true);
      return pref;
    }
   return null;
    }
    
    //fetch marketing set 
    public void fetchMarketingKit(){
        system.debug('------ IN$ CpMarketingKitController.fetchMarketingKit');
        marketingKits = new List<MarketingKitRole__c>();
        
        Set<MarketingKitRole__c> mkset = new Set<MarketingKitRole__c>();
        
        Id AccountId = [Select u.Id, u.ContactId, u.AccountId From User u where id =: UserInfo.getUserId()  limit 1].AccountId;
        MarketingKitRole__c temvar = new MarketingKitRole__c();
        if(portalUser.size() > 0){
        Map<Integer,Product_Roles__c> mapoforderofmark = new Map<Integer,Product_Roles__c>();
        //List<MarketingKitRole__c> templist = new List<MarketingKitRole__c>();
         for(Product_Roles__c pr : marktkitinternaluser){
           mapoforderofmark.put(Integer.valueOf(pr.marketingkitorder__c),pr);
           system.debug('@@Productroles--' + mapoforderofmark);
         }
         integer j = 0;
           for(integer i= 1; i <= mapoforderofmark.size(); i++){
            for(MarketingKitRole__c mr : [Select m.Product__c, m.Name, m.Id, m.Account__c,  m.Product__r.Name, m.Product__r.MkStylesheet__c
                                            From MarketingKitRole__c m where Account__c =: AccountId  and Product__c != null  
                                            and Product__r.Model_Part_Number__c != :system.label.PartNumber order by m.Product__r.Name asc]){
           
                system.debug('######' + mr);
                String prdname = mr.Product__r.Name;
                system.debug('#########' + prdname.trim() + '#######');
                if(mapoforderofmark.get(i).Product_Name__c.toLowerCase() == mr.Product__r.Name.trim().toLowerCase())
                {
                 system.debug('inside if');
                 //marketingKits.set(0,mr);
                 marketingKits.add(mr);
                 //mkset.set(0,mr);
                 system.debug('@@@@@@@@@' + marketingKits);
                 j++;
                 break;
                }
                //mkset.add(mr); 
             /*if(mr.Product__r.Name.contains('Premier Marketing'))    
                temvar = mr;*/                
              }
            } 
            /*
            system.debug('----marketingkitrole----' + mkset);
            marketingKits.addAll(mkset);
            //marketingKits.sort();
            List<MarketingKitRole__c> templist = new List<MarketingKitRole__c>();
            if(temvar.name !=null){
             templist.add(temvar);
            for(MarketingKitRole__c mr : marketingKits)
             {
               if(!mr.Product__r.Name.contains('Premier Marketing'))
                templist.add(mr);  
             }
             marketingKits.clear();
             marketingKits.addall(templist);
            }*/
            system.debug('aebug : marketingKits = ' + marketingKits);
        }
    // sorting of list for internaluser
     /* List<Product_Roles__c> templist = new List<Product_Roles__c>();
      Product_Roles__c str = new Product_Roles__c();
       for(Product_Roles__c pr : marktkitinternaluser)
       {
        if(pr.product_name__c.contains('Premier')){
        str = pr;
        }else{
          templist.add(pr);
        }
       }
       marktkitinternaluser.clear();
       marktkitinternaluser.add(str);
       templist.sort();
       for(Product_Roles__c p : templist)
       {
        marktkitinternaluser.add(p);
       } */
    //logic ends
    }
 @future(callout = true)
  public static void addgrouptolibrary(String picklistvaluef, String contfid){
    system.debug('------ IN$ CpMarketingKitController.addgrouptolibrary');
      List<Contact> contf = [Select id,MarketingKit_Agreement__c from contact where id=:contfid];
    if(contf[0].MarketingKit_Agreement__c !=null && picklistvaluef!=null){
                if(!contf[0].MarketingKit_Agreement__c.contains(picklistvaluef)){
                    String S = contf[0].MarketingKit_Agreement__c + ';' + picklistvaluef;
                    contf[0].MarketingKit_Agreement__c = S;
                }
            }else{
                contf[0].MarketingKit_Agreement__c = picklistvaluef;
            }
        try{
           update contf;
           }catch(DMLException ex){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Factory Visit cant be pass date.Please contact your portal admin for this.');
              ApexPages.addMessage(myMsg);
              //send mail to portal admin to update the contact.
           }
  }  
    public void registerLicenseAgreement(){
         pagereference pref;
         String picklistvalue = marketkitagrmntmap.get(marktProg);
         ReloadPopup=true;
       
         try{
             // logic from another method
               List<string> groupname = new list<string>();
                List<groupmember> groupmemberadd = new List<groupmember>();
                for(Product_Roles__c pr:[Select product_Name__c,Public_group__c from Product_Roles__c where product_Name__c = : marktProg]) 
                {
                  if(pr.Public_group__c != null)
                    groupname.add(pr.Public_group__c);
                }
                system.debug('---Product group---' + groupname);
                for(group gr: [Select id from group where name in: groupname])
                {
                 groupmember gmr = new groupmember(GroupId = gr.id,UserOrGroupId = UserInfo.getUserId());
                 groupmemberadd.add(gmr);
                }
                system.debug('----Groupmember--' + groupmemberadd);
                if(groupmemberadd != null){
                   try{
                   insert groupmemberadd;
                   }catch(Exception ex){
                      Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
                    }
                 }
             // logic ends
                //update cont;
                //addgrouptolibrary(marktProg);
                //if(marktProg != 'Premier Marketing Program'){
                 //   pref=new pagereference('/apex/CpMarketingKitPremier?prog='+marktProg);
                 //   pref.setredirect(true);
                /*}else{
                     pref=new pagereference(SurveyURL+'?c='+Userinfo.getUserId());
                     pref.setredirect(true);
                     //return null;   
                }*/
            }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
            }
            if(cont.size() > 0){
              addgrouptolibrary(picklistvalue,cont[0].id);
             
            }
         termsAccepted = true;
         Messages='block';
    }
    public void fetchMarkProgDocs(){
        system.debug('------ IN$ CpMarketingKitController.fetchMarkProgDocs');
        marktKitdocs = new List<markDocs>();
        Map<String, List<documents>> docs = new Map<String, List<documents>>();
        Map<String,Map<String,List<documents>>> subdocs = new Map<String,Map<String,List<documents>>>(); // map for subgrouping
        //Set<String> groups = new Set<String>();
        
        Schema.Describefieldresult fieldresult = ContentVersion.Group__c.getDescribe();
        List<Schema.Picklistentry> Picklistvalue = fieldresult.getPicklistvalues();
        System.debug('aebug : Picklistvalue = '+Picklistvalue);            
        for(Schema.Picklistentry pe : Picklistvalue){
            docs.put(pe.getvalue(),new list<documents>());
        }       
        //Logic added by harshita for showing the content from the library only
        
        Set<Id> contdocid = new set<Id>();
        Set<Id> contdocworkspace = new set<Id>();
        for(ContentVersion cv : [Select c.Id,c.RecordType.Name,c.ContentDocumentId From ContentVersion c where RecordType.Name = 'Marketing Kit' and IsLatest = true]){
            contdocid.add(cv.ContentDocumentId);                     
            }
        for(ContentWorkspaceDoc cw: [Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc where ContentDocumentId in:contdocid and ContentWorkspace.name  =: marketkitagrmntmap.get(marktProg)]){
           contdocworkspace.add(cw.ContentDocumentId);
        }
        
        //marktProg variable holding the product name
        for(ContentVersion cv : [Select c.FileType ,c.RecordType.Name, c.RecordTypeId, c.Id, c.MarketingKit_Doc_Image__r.Image__c,c.MarketingKit_Doc_Image__r.Thumbnail_Images__c,c.MarketingKit_Doc_Image__c,c.Title,c.ContentSize, c.Description,
                                 c.Group__c, c.Subgroup__c, c.ContentDocumentId From ContentVersion c where RecordType.Name = 'Marketing Kit' and  ContentDocumentId in: contdocworkspace and islatest = true  order by Group__c]){
                        
            //docs.put(cv.Group__c, );
            if(docs.containskey(cv.Group__c)){
         //   System.debug(marktProg.containsIgnoreCase(cv.Kit__c));  
              //  if(marktProg.containsIgnoreCase(cv.Kit__c)){
                    documents ds = new documents();
                    ds.relContent = cv;
                    ds.videoLink = '/sfc/servlet.shepherd/version/download/' + cv.Id + '?asPdf=false';
                    String S2 = '';
                    String HS2 = '';
                    String WS2 = '';
                    String THS2 ='';
                    String TWS2= '';
                    String ts2 = '';
                    if(cv.MarketingKit_Doc_Image__r.Image__c != null){
                        System.debug('aebug ; bn.Image__c == '+cv.MarketingKit_Doc_Image__r.Image__c.containsIgnoreCase('https'));
                        if(cv.MarketingKit_Doc_Image__r.id=='a0GE000000Dqr3W')
                         System.debug('Images-----> == '+cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c);
                         /* Code UPdated by Anupam */
                        String S1 = cv.MarketingKit_Doc_Image__r.Image__c;
                        System.debug('Testing--->'+S1);
                        System.debug('Testing>'+S1.indexof('src')+5);
                        System.debug(S1.substring(S1.indexof('src')+5));
 
                        List<String> ss = S1.substring(S1.indexof('src')+5).split('"');
                        System.debug('Tooo-->'+ss[0]);
                        if(S1.contains('style'))
                        {
                        List<String> Style = S1.substring(S1.indexof('style')).split('"');
                        if(Style.size() > 0){
                         
                         System.debug('Style2-->'+Style[1]);
                         HS2=Style[1];
                         
                        List<String> SubSplit=HS2.substring(HS2.indexof('height')+7).split(':');
                             if(SubSplit.size() > 0){
                                System.debug('Sub-->'+SubSplit);
                                System.debug('SubSplit-->'+SubSplit[0]);
                                System.debug('SubSplit@@-->'+SubSplit[1]);
                               
                               HS2=SubSplit[0];
                               }
                             List<String> SubSplit1=HS2.split('px;');
                             if(SubSplit1.size() > 0){
                             System.debug('SubSplit0-->'+SubSplit1[0]);
                             System.debug('SubSplit1-->'+SubSplit1[1]);
                             HS2=SubSplit1[0];
                             }
                            
                             List<String> SubSplit2=SubSplit[1].split('px;');
                              if(SubSplit2.size() > 0){
                             WS2=SubSplit2[0];
                            System.debug('HEIGHT-->'+HS2);
                            System.debug('Width-->'+WS2);
                            }
     
                              }
                            }
                        else
                        {
                        if(S1.contains('height')){
                        List<String> hss = S1.substring(S1.indexof('height')).split('"');
                        if(hss.size() > 0){
                         System.debug('Tooo1-->'+hss[0]);
                         System.debug('Tooo2-->'+hss[5]);
                          HS2 = hss[1]; 
                          WS2 = hss[5];
                        }
                        }
                        }
                        if(ss.size()>0){
                               system.debug(ss[0]);
                               s2=ss[0];
                        }         
                       /* Code UPdated by Anupam */
                      
                       
                       /* Code commented by ANUPAM
                        
                       /* if(cv.MarketingKit_Doc_Image__r.Image__c.containsIgnoreCase('https')){
                          String S1 = cv.MarketingKit_Doc_Image__r.Image__c;

                          System.debug(S1.substring(S1.indexof('src')+5));

                             List<String> ss = S1.substring(S1.indexof('src')+5).split('"');

                                if(ss.size()>0)
                                      {
                                         system.debug(ss[0]);
                                         s2=ss[0];
                                       }
                          //  S2 = cv.MarketingKit_Doc_Image__r.Image__c.substring(cv.MarketingKit_Doc_Image__r.Image__c.indexof('src')+5,cv.MarketingKit_Doc_Image__r.Image__c.indexof('src')+126);
                        }else{
                        String S1 = cv.MarketingKit_Doc_Image__r.Image__c;

                          System.debug(S1.substring(S1.indexof('src')+5));

                             List<String> ss = S1.substring(S1.indexof('src')+5).split('"');
                                      if(ss.size()>0)
                                      {
                                         system.debug(ss[0]);
                                         s2=ss[0];
                                       } */

                      /*  if(cv.MarketingKit_Doc_Image__r.Image__c.containsIgnoreCase('src'))
                        {
                        System.debug('Testurl--->'+cv.MarketingKit_Doc_Image__r.Image__c);
                         S2 = cv.MarketingKit_Doc_Image__r.Image__c.substring(cv.MarketingKit_Doc_Image__r.Image__c.indexof('src')+5,cv.MarketingKit_Doc_Image__r.Image__c.indexof('src')+94);
            // S2 = cv.MarketingKit_Doc_Image__r.Image__c.substring(cv.MarketingKit_Doc_Image__r.Image__c.indexof('src'),cv.MarketingKit_Doc_Image__r.Image__c.substring(cv.MarketingKit_Doc_Image__r.Image__c.indexof('src')).indexOfIgnoreCase('"'));
                         }*/

                        //} 
                        s2 = s2.replace('amp;','');
                    }
                    //for thumbnail
                    if(cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c != null){
                        System.debug('aebug ; bn.Image__c == '+cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c.containsIgnoreCase('https'));
                        system.debug('Thumbnailimage for the test-------' + cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c);
                         
                        String TS1 = cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c;
 
                        System.debug(TS1.substring(TS1.indexof('src')+5));
 
                        List<String> tss = TS1.substring(TS1.indexof('src')+5).split('"');
                        if(TS1.contains('style'))
                        {
                        List<String> TStyle = TS1.substring(TS1.indexof('style')).split('"');
                        if(TStyle.size() > 0){
                         
                         System.debug('Style2-->'+TStyle[1]);
                         THS2=TStyle[1];
                         
                        List<String> TSubSplit=THS2.substring(THS2.indexof('height')+7).split(':');
                             if(TSubSplit.size() > 0){
                                System.debug('Sub-->'+TSubSplit);
                                System.debug('SubSplit-->'+TSubSplit[0]);
                                System.debug('SubSplit@@-->'+TSubSplit[1]);
                               
                               THS2=TSubSplit[0];
                               }
                             List<String> TSubSplit1=THS2.split('px;');
                             if(TSubSplit1.size() > 0){
                             System.debug('SubSplit0-->'+TSubSplit1[0]);
                             System.debug('SubSplit1-->'+TSubSplit1[1]);
                             THS2=TSubSplit1[0];
                             }
                            
                             List<String> TSubSplit2=TSubSplit[1].split('px;');
                              if(TSubSplit2.size() > 0){
                             TWS2=TSubSplit2[0];
                            System.debug('THEIGHT-->'+THS2);
                            System.debug('TWidth-->'+TWS2);
                            }
     
                              }
                            }
                            ELSE
                            {
                        if(TS1.contains('height')){
                        List<String> thss = TS1.substring(TS1.indexof('height')).split('"');
                        if(thss.size() > 0){
                         THS2 = thss[1]; 
                         TWS2 = thss[5];
                          }
                          }
                          }
                        if(tss.size()>0){
                               system.debug(tss[0]);
                               ts2=tss[0];
                        }
                     ts2 = ts2.replace('amp;','');
                    }                       
                   
                    ds.image = s2;
                    System.debug('Test---->'+ds.image);
                     
                    ds.thumbnail = TS2;
                    System.debug('Test---->'+ ds.thumbnail);
                   if(THS2!=null && TWS2!=null)
                    {
                    ds.imgheight = THS2;
                    ds.imgwidth = TWS2;
                    }
                    else
                    {
                    ds.imgheight = HS2;
                    ds.imgwidth = WS2;
                    }
                    
                    system.debug('****'+s2);
                    docs.get(cv.Group__c).add(ds);
                    docs.get(cv.Group__c).sort();
               // }
            }
        }
        // for subgrouping
    if(docs.size() > 0){
        for (String subgroup : docs.keyset())
        {
         if(docs.get(subgroup).size() == 0){
                    docs.remove(subgroup);
                    continue;
                }
             for(documents d: docs.get(subgroup))
             {  
                String subgroupname = d.relContent.Subgroup__c;
                    if(subgroupname == '' || subgroupname == null)
                       subgroupname = 'A';
                     /*else
                        subgroupname = 'notdefined'; */
                Map<String,List<documents>> tempsubmap = new Map<String,List<documents>>();
                if(subdocs.containskey(subgroup))
                {
                     tempsubmap = subdocs.get(subgroup);
                     List<documents> tempsubdocs = new list<documents>(); 
                     
                  if(tempsubmap.containskey(subgroupname))
                    {
                      tempsubdocs = tempsubmap.get(subgroupname);
                      
                      tempsubdocs.add(d);
                      tempsubdocs.sort();
                      tempsubmap.put(subgroupname,tempsubdocs);
                    }else{
                       tempsubdocs.add(d);
                       tempsubdocs.sort();
                      tempsubmap.put(subgroupname,tempsubdocs);
                     }
                 subdocs.put(subgroup,tempsubmap);
                }else{
                  list<documents> tempsubdocs2 = new list<documents>();
                  tempsubdocs2.add(d);
                  tempsubmap.put(subgroupname,tempsubdocs2);
                  subdocs.put(subgroup,tempsubmap);
                }               
                
             }
        }
    }
    system.debug('#@@@#@#@##@@' + subdocs);
    if(subdocs.size() >0 ){
      for(String s : subdocs.keyset()){
                if(subdocs.get(s).size() == 0){
                    subdocs.remove(s);
                    continue;
                }
          List<subdocsclass> subdocuments = new List<subdocsclass>();
         for(String s1 : subdocs.get(s).keyset()){
                if(subdocs.get(s).get(s1).size() == 0){
                    subdocs.get(s).remove(s1);
                    continue;
                }
            subdocsclass submrkdocs = new subdocsclass();
            submrkdocs.subgroupheader = s1;
            submrkdocs.subgrouplist.addall(subdocs.get(s).get(s1));
            subdocuments.add(submrkdocs);
            subdocuments.sort();
            }
          markDocs obj = new markDocs();
                obj.Header = s; 
                //docs.get(s).sort();  
                //subdocuments.sort();
                /*List<subdocsclass> tempsubdocuments = new List<subdocsclass>();
                for(subdocsclass ss : subdocuments)
                {
                 if(ss.subgroupheader == 'notdefined')
                 {
                  subdocsclass newss = new subdocsclass();
                  newss = ss;
                  newss.subgroupheader = '';
                  tempsubdocuments.add(newss);
                 }
                }
                for(subdocsclass ss : subdocuments)
                {
                 if(ss.subgroupheader == 'notdefined')
                 {
                  continue;
                  //tempsubdocuments.add(ss);
                 }else{
                  tempsubdocuments.add(ss);
                 }
                }*/
                //system.debug('###############' + tempsubdocuments);               
                obj.content.addAll(subdocuments);
                //obj.content.sort();
                marktKitdocs.add(obj);
                marktKitdocs.sort();
               
                 //start
                list <markDocs> temp1 = marktKitdocs;
                list <markDocs> temp2 = new list <markDocs>();
                list <markDocs> temp3 = new list <markDocs>();
                
                //Re-sorting the list 
              
               for (markDocs md : marktKitdocs) {

                    if (md.header.contains('Getting')) {
                         temp3.add(md);
                      // markDocs  str = temp1.remove(listIndex) ;
                    }
                    else temp2.add(md);
            
                }
              
                temp1.clear();
                temp1.addAll(temp3);
                temp1.addAll(temp2);                
                 for (markDocs md : temp1) {
                   //system.debug('############### marktKitdocs1 = ' + marktKitdocs); 
                    system.debug('############### Temp1.Header1 = ' + md.Header);
                    
                }
               // marktKitdocs.clear();
               // marktKitdocs.addAll(temp1);
                //end
              
         }      
        
    }
        /*
        if(docs.size() > 0){
            for(String s : docs.keyset()){
                if(docs.get(s).size() == 0){
                    docs.remove(s);
                    continue;
                }
                markDocs obj = new markDocs();
                obj.Header = s; 
                //docs.get(s).sort();               
                obj.content.addAll(docs.get(s));
                //obj.content.sort();
                marktKitdocs.add(obj);
                marktKitdocs.sort();
            }
            
            //marktKitdocs.sort();
        } */
    }

}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CpMarketingKitController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(seealldata = true)

Public class CpMarketingKitControllerTest{

    //Test Method for initiating CpMarketingKitController class
    
    static testmethod void testCpMarketingKitController(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            /*Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
            insert actCntry;*/
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;   
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MarketingKit_Agreement__c = 'Premier Marketing Program',Date_of_Factory_Tour_Visit__c = System.Today(),FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testclass.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
      
        Product2 prod = new Product2();
        prod.Name = 'Acuity';
        prod.Product_Group__c ='ARIA';
        prod.ProductCode = '12345';
        insert prod;
        
        Product2 prod1 = new Product2();
        prod1.Name = 'Acuity';
        prod1.Product_Group__c ='ARIA';
        prod1.ProductCode = '12345';
        insert prod1;
        
        MarketingKitRole__c mktrole = new MarketingKitRole__c ();
        mktrole.Account__c = a.Id;
        mktrole.Product__c= prod.Id;
        
        Insert mktrole;
        
        //con.MarketingKit_Agreement__c = ''Premier Marketing Program';
        //con.Date_of_Factory_Tour_Visit__c = System.Today();
        //update con;
        
        }
        
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testclass.com' ];
         
         System.runAs (usr){
         
        PageReference pageRef = Page.CpMarketingKit;
        Test.setCurrentPage(pageref);
        Apexpages.currentPage().getParameters().put('prog', 'Premier');
        
        CpMarketingKitController  marketingkit = new CpMarketingKitController ();
        marketingkit.registerLicenseAgreement();
        //marketingkit.fetchMarkProgDocs();
        
        CpMarketingKitController.markDocs docs = new CpMarketingKitController.markDocs();
        CpMarketingKitController.documents doc = new CpMarketingKitController.documents();
        }
                Test.StopTest();
    }
    
    //Test Method for initiating CpMarketingKitController class for covering else part
    
    static testmethod void testCpMarketingKitControllermethod1(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
             
            /*Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
            insert actCntry;*/
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx', Country__c='testcountry' ); 
            insert a;    
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test1',MarketingKit_Agreement__c = 'Premier Marketing Program',Date_of_Factory_Tour_Visit__c = System.Today()); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testclass.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
      
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            
            insert prod;
            
            MarketingKitRole__c mktrole = new MarketingKitRole__c ();
            mktrole.Account__c = a.Id;
            mktrole.Product__c= prod.Id;
            
            Insert mktrole;
            
            //con.MarketingKit_Agreement__c = 'Premier';
            //con.Date_of_Factory_Tour_Visit__c = System.Today();
            //update con;
            
            }
        
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testclass.com' ];
         
         System.runAs (usr){
         
        PageReference pageRef = Page.CpMarketingKit;
        Test.setCurrentPage(pageref);
        Apexpages.currentPage().getParameters().put('prog', 'Testing');
       
        CpMarketingKitController  marketingkit = new CpMarketingKitController ();
        marketingkit.registerLicenseAgreement();
        marketingkit.fetchMarkProgDocs();
        
        CpMarketingKitController.markDocs docs = new CpMarketingKitController.markDocs();
        CpMarketingKitController.documents doc = new CpMarketingKitController.documents();
        }
                Test.StopTest();
    }
    
    //Test Method for initiating CpMarketingKitController class where MarketingKit_Agreement__c is null
    
    static testmethod void testCpMarketingKitControllermethod2(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            /*Account_Country__c actCntry=new Account_Country__c(Country__c='Test' , Name = 'Test'); 
            insert actCntry;*/
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx', Country__c='testcountry' ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test2', MarketingKit_Agreement__c = 'Premier Marketing Program',Date_of_Factory_Tour_Visit__c = System.Today()); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testclass.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
      
            Product2 prod = new Product2();
            prod.Name = 'TrueBeam Marketing Program';
            prod.Product_Group__c ='ARIA';
            
            insert prod;
            
            MarketingKitRole__c mktrole = new MarketingKitRole__c ();
            mktrole.Account__c = a.Id;
            mktrole.Product__c= prod.Id;
            
            Insert mktrole;
            /*
            Product_Roles__c prodRole=new Product_Roles__c();
            prodRole.Name='TrueBeam Marketing Program';
            prodRole.product_Name__c='TrueBeam Marketing Program';
            prodRole.Public_group__c='TrueBeam Marketing Program';
            prodRole.ismarketingKit__c=true;
            prodRole.Survey_URL__c='http://www.google.com';
            prodRole.marketingkitorder__c = 1.0;
            insert prodRole;*/
            
            group grp=new group();
            grp.name='TrueBeam Marketing Program';
            insert grp;
            
           // con.MarketingKit_Agreement__c = '';
           // con.Date_of_Factory_Tour_Visit__c = System.Today();
            
           // update con;
            
            }
            
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testclass.com' ];
         
         System.runAs (usr){
         
        PageReference pageRef = Page.CpMarketingKit;
        Test.setCurrentPage(pageref);
        Apexpages.currentPage().getParameters().put('prog', 'TrueBeam Marketing Program');
        
        CpMarketingKitController  marketingkit = new CpMarketingKitController ();
        marketingkit.registerLicenseAgreement();
        marketingkit.fetchMarketingKit();
        }
                Test.StopTest();
    }
    
    
    
    //Test Method for initiating CpMarketingKitController class with content record
    
    static testmethod void testCpMarketingKitControllermethod()
    {
        
        Test.StartTest();
        
       RecordType ContentRT = [select Id FROM RecordType WHERE SObjectType ='ContentVersion' And Name = 'Marketing Kit' Limit 1];
       ContentVersion testContentInsert =new ContentVersion(); 
       testContentInsert.ContentURL='http://www.google.com/'; 
       testContentInsert.Title ='Google.com'; 
       testContentInsert.RecordTypeId = ContentRT.Id; 
       //testContentInsert.group__c = 'testgroup';
       
       insert testContentInsert; 
       RecordType BannersRT = [select Id FROM RecordType WHERE SObjectType ='BannerRepository__c' And Name = 'Marketing Kit' Limit 1];

       BannerRepository__c banners= new BannerRepository__c();
        banners.Image__c='<img style="height: 304px; width: 500px;" src="https://varian--SFSD--c.cs8.content.force.com/servlet/rtaIma…=a0GL0000003oALV&feoid=00NE0000004sNzr&refid=0EML00000008YsZ" alt="User-added image"></img>';
        banners.Thumbnail_Images__c='<img style="height: 393px; width: 500px;" src="https://varian--SFSD--c.cs8.content.force.com/servlet/rtaIma…=a0GL0000003oALV&feoid=00NE0000004t021&refid=0EML00000008Yse" alt="User-added image"></img>';
     
        banners.RecordTypeId=BannersRT.id;
         insert banners;
       ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id and IsLatest = true]; 
       ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name=:'TrueBeam Marketing Program']; 
       ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
       newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
       newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
       insert newWorkspaceDoc;
       ContentWorkspace library = [SELECT id FROM ContentWorkspace LIMIT 1];
     
       testContentInsert.Group__c = 'Image Library';
       testContentInsert.subgroup__c = 'Ads';
       testContentInsert.MarketingKit_Doc_Image__c=banners.id;
       
      // testContentInsert.Kit__c = 'Premier,Global';
       
       Update testContentInsert;
        
        PageReference pageRef = Page.CpMarketingKit;
        Test.setCurrentPage(pageref);
        Apexpages.currentPage().getParameters().put('prog', 'TrueBeam Marketing Program');
        CpMarketingKitController  marketingkit = new CpMarketingKitController ();
        
        marketingkit.fetchMarkProgDocs();
        CpMarketingKitController.markDocs docs = new CpMarketingKitController.markDocs();
        CpMarketingKitController.documents doc = new CpMarketingKitController.documents();
        
                Test.StopTest();
    }
    static testmethod void testCpMarketingKitControllermethod3()
    {
        
        Test.StartTest();
        
       RecordType ContentRT = [select Id FROM RecordType WHERE SObjectType ='ContentVersion' And Name = 'Marketing Kit' Limit 1];
       ContentVersion testContentInsert =new ContentVersion(); 
       testContentInsert.ContentURL='http://www.google.com/'; 
       testContentInsert.Title ='Google.com'; 
       testContentInsert.RecordTypeId = ContentRT.Id; 
       //testContentInsert.group__c = 'testgroup';
       
       insert testContentInsert; 
       RecordType BannersRT = [select Id FROM RecordType WHERE SObjectType ='BannerRepository__c' And Name = 'Marketing Kit' Limit 1];

       BannerRepository__c banners= new BannerRepository__c();
        banners.Image__c='<img width="500" height="688" src="/img/rte_broken_image.png" alt="This image is not available because: You don’t have the privileges to see it, or it has been removed from the system">';
        banners.Thumbnail_Images__c='<img width="500" height="688" src="/img/rte_broken_image.png" alt="This image is not available because: You don’t have the privileges to see it, or it has been removed from the system">';
        banners.RecordTypeId=BannersRT.id;
         insert banners;
       ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id and IsLatest = true]; 
       ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name=:'TrueBeam Marketing Program']; 
       ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
       newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
       newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
       insert newWorkspaceDoc;
       ContentWorkspace library = [SELECT id FROM ContentWorkspace LIMIT 1];
     
       testContentInsert.Group__c = 'Image Library';
       testContentInsert.subgroup__c = 'Ads';
       testContentInsert.MarketingKit_Doc_Image__c=banners.id;
       
      // testContentInsert.Kit__c = 'Premier,Global';
       
       Update testContentInsert;
        
        PageReference pageRef = Page.CpMarketingKit;
        Test.setCurrentPage(pageref);
        Apexpages.currentPage().getParameters().put('prog', 'TrueBeam Marketing Program');
        CpMarketingKitController  marketingkit = new CpMarketingKitController ();
        
        marketingkit.fetchMarkProgDocs();
        CpMarketingKitController.markDocs docs = new CpMarketingKitController.markDocs();
        CpMarketingKitController.documents doc = new CpMarketingKitController.documents();
        
                Test.StopTest();
    }
}
public class CpMessageDetailpageController {
public Id MessageId{get;set;}
Public List<MessageCenter__c> pre{get;set;}
private String soql{get;set;}
 public string Usrname{get;set;}
    public string shows{get;set;}
    public boolean isGuest{get;set;}

    public String getPre() {
        return null;
    }
    public string userids='';


    public CpMessageDetailpageController () {
       shows='none';
    MessageId=ApexPages.currentPage().getParameters().get('Id');
    pre= new List<MessageCenter__c>();
    
    pre=[select id,Message_Date__c,title__c,product_affiliation__c,Published__c, Message__c,User_ids__c from MessageCenter__c where  id =: MessageId];
    System.debug('Message'+pre);
    
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        } 
    }
   
    
    /*public PageReference runSearch(){
        String Id =Apexpages.currentPage().getParameters().get('MessageId');
        MessageCenter__Share mshr=[select id,AccessLevel,ParentId,UserOrGroupId from MessageCenter__Share where ParentId=:MessageId];
        mshr.AccessLevel='';
        update mshr;
        PageReference pref = new PageReference('/apex/CpMessageCenter');
        pref.setRedirect(true);
        return pref;
    }*/
    public PageReference runSearch(){
       String Id =Apexpages.currentPage().getParameters().get('MessageId');

       System.debug('*****MessageID****'+Id);        
       MessageCenter__c obj =[select id,Message_Date__c,title__c,product_affiliation__c,Published__c, Message__c,User_ids__c from MessageCenter__c where id =:MessageId];
                //string userids='';
                //if(obj.User_ids__c!=null){
                    if(obj.User_ids__c==''|| obj.User_ids__c!=null){
                        obj.User_ids__c=+UserInfo.getUserId();
                    }
                    else{
                        obj.User_ids__c = obj.User_ids__c+','+UserInfo.getUserId();  
                    }
                //}
        //obj.User_ids__c=userids;
        //obj.Published__c = false;
       update obj;
       
        MessageView__c obj1 =[select id,Message_Center__c from MessageView__c  where Message_Center__c=:MessageId and user__c=:Userinfo.getUserId() limit 1];
        delete obj1;
       PageReference pref = new PageReference('/apex/CpMessageCenter');
       pref.setRedirect(true);
       return pref;
       }
     
     public PageReference backtoMessage(){
      
      PageReference pref = new PageReference('/apex/CpMessageCenter');
       pref.setRedirect(true);
       return pref;
     }
   
   
   
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 11-June-2013
    @ Description   :  Test class for CpMessageDetailpageController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest 

Public class CpMessageDetailpageControllerTest{

    //Test Method for initiating CpMessageDetailpageController class
    
    static testmethod void testMessageDetailController(){
        Test.StartTest();
        CpMessageDetailpageController MessageControllerDetail = new CpMessageDetailpageController();
        User ur = [Select Id,Name from User where Id=:UserInfo.getUserId()];
        MessageControllerDetail.backtoMessage();
        MessageControllerDetail.getPre();
        MessageCenter__c mc = new MessageCenter__c();
        mc.Message_Date__c= Date.today();
        mc.title__c='Test';
        mc.product_affiliation__c ='4DITC';
        mc.Published__c = True;
        mc.Message__c ='Test class';
        mc.User_ids__c = ur.Id;

        insert mc;
        
        MessageView__c mesgView=new MessageView__c();
        mesgView.Message_Center__c=mc.id;
        mesgView.user__c=ur.Id;
        insert mesgView;
      
     
         PageReference pageRef = Page.CpMessageCenterDetail; 
         Test.setCurrentPage(pageref);
         ApexPages.currentPage().getParameters().put('id' , mc.Id);
         CpMessageDetailpageController Mssg = new CpMessageDetailpageController ();
         Mssg.runSearch();


                Test.StopTest();
    }
        //Test Method for initiating CpMessageDetailpageController class with UserId's Null
        
        static testmethod void testMessageDetailControllerMethod(){
            Test.StartTest();
            User ur = [Select Id,Name from User where Id=:UserInfo.getUserId()];
            MessageCenter__c mc = new MessageCenter__c();
            mc.Message_Date__c= Date.today();
            mc.title__c='Test';
            mc.product_affiliation__c ='4DITC';
            mc.Published__c = True;
            mc.Message__c ='Test class';
            insert mc;

            MessageView__c mesgView=new MessageView__c();
            mesgView.Message_Center__c=mc.id;
            mesgView.user__c=ur.Id;
            insert mesgView;

         PageReference pageRef = Page.CpMessageCenterDetail; 
         Test.setCurrentPage(pageref);
         ApexPages.currentPage().getParameters().put('id', mc.Id);
         CpMessageDetailpageController Mssg = new CpMessageDetailpageController ();
         Mssg.runSearch();

        Test.StopTest();


        }
        

    
    

}
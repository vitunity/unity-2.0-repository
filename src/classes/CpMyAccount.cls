/*************************************************************************
    Author        : Narmada Yelavaluri
    Date      : 05-May-2013
    Description   : An Apex class to get data that is to be displayed on My Accounts page of MyVarian.
    @ Last Modified By  :   Mohit
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Added Functional Implementation
    @ Last Modified By  :   Nilesh Gorle
    @ Last Modified On  :   30-August-2017
    @ Last Modified Reason  :   changed mail targetobjectid from usr.contact.id to usr.id
    Change Log:
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    30-Aug-2017 - Nilesh - STSK0012616 - changed mail targetobjectid from usr.contact.id to usr.id (Line 553)
    25-Aug-2017 - Rakesh - STSK0012733 - Querying CRA for supported institutions based on current loggedin contact.(Line 267). 
****************************************************************************/
Public class CpMyAccount{
    /*==========Code added By Mohit Block 1=============*/
    // string dataconsent{get;set;}
    public string proppwd{get;set;}
    public string propRepwd{get;set;}
    public user usrMyCP{get;set;}
    public string stroldpwd{get;set;}
    public string strCountry{get;set;}
    Public boolean strSubsCribUnsubsCrib{get;set;}
    //Public boolean strGlobalVisibilitySubUnsubs{get;set;}
    public string strComment{get;set;}
    public boolean blnNOTRPC{get;set;}
    public Boolean isGuest{get;set;}
    public Map<Id,Boolean> accntsubscrbcmap{get;set;}
    public string SapAcctName{get;set;}
    
    public string ConsentContent{get;set;}
        
    /*==========Code added By Mohit Block 1 End=============*/
  
    List<Product2> SelectedProducts = new List<Product2> ();
    public list<String> prodList1{get;set;} 
    public list<String> prodList2{get;set;}
    //public list<String> productList{get;set;}
    User usr;
    public Boolean subscribed{get;set;}
    Boolean sublink;
    public Boolean resetpass{get;set;}
    public String Newpass{get;set;}
    public String ConfirmPass{get;set;}
    public String ResetPassword{get;set;}
    public String oldpass{get;set;}
    public string displaymsg{get;set;}
    //public String CustRecovryQ {get; set;}
    //public String CustRecoveryAns {get; set;}
    public string shows{get;set;}
    public string Usrname{get;set;}
    public boolean isConsentEnable{get;set;}
    
    
    // This method is used to display all the recovery questions
    Public List<SelectOption> getrecoveryqusoptions()
    {
          list<SelectOption> recoveryqusoptions = new list<SelectOption>();
          // Get the object type of the SObject.
          Schema.sObjectType objType = Contact.getSObjectType(); 
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values = fieldMap.get('Recovery_Question__c').getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          { 
             recoveryqusoptions.add(new SelectOption(a.getLabel(), a.getValue())); 
          }
          return recoveryqusoptions;
               
    }
     
    public String showmssg{get;set;}
    public String showemailacc{get;set;}
    public String Req{get;set;}
    public List<Contact_Role_Association__c> craList{get;set;}
    
    public CpMyAccount(){
     
     
    
    
    //STSK0011818:Start
    string Territory = [select Id,Contact.Territory__c from User where Id=: userinfo.getUserId()].contact.Territory__c;
    list<myVarianCustomerAgreementContent__c> lstContent = [select Territory__c,Content__c from myVarianCustomerAgreementContent__c where Territory__c =: Territory];
    if(lstContent.size()>0){   if(Territory.equalsignorecase(lstContent[0].Territory__c)){     ConsentContent = lstContent[0].Content__c;      }           
    }else{//default consent
        list<myVarianCustomerAgreementContent__c> lstContentDefault = [select Territory__c,Content__c from myVarianCustomerAgreementContent__c where  Territory__c ='default'];
        if(lstContentDefault.size()>0){          ConsentContent = lstContentDefault[0].Content__c;     }
    }  
     //STSK0011818:End
     
    Req= ApexPages.currentPage().getParameters().get('Raeq');
    if(Req== 'UpdatePI')        showmssg='true';        
    if(Req== 'UpdateAcc')          showmssg='false';        
    if(Req== 'UpdateAccEmail')     showemailacc='true';
        
       accntsubscrbcmap = new Map<Id,Boolean>();
       
       strSubsCribUnsubsCrib=false;
       shows = 'none';
       isGuest = SlideController.logInUser();
       displaymsg = ApexPages.currentPage().getParameters().get('disgmsg');
       ResetPassword =  ApexPages.currentPage().getParameters().get('Request');
       usrMyCP=[select id,LastLoginDate,name,username,Email,contact.Title,ContactId,usertype,alias,LMS_Deactivation_Date__c,LMS_Last_Access_Date__c,LMS_Activated_Date__c,LMS_Last_Course_End_date__c,
                   contact.firstname,
                   contact.lastname,LMS_Status__c,LMS_Registration_Date__c,
                   Contact.accountid,
                   contact.account.Name,contact.account.Legal_Name__c,
                   contact.MailingCity,
                   contact.MailingCountry,
                   contact.MailingPostalcode,
                   Contact.mailingState,
                   Contact.MailingStreet,
                   Contact.Phone,
                   contact.Email,
                   contact.id,
                   contact.manager__c,
                   contact.account.billingstreet,
                   contact.Reason_To_Unsubscribe__c,
                   contact.account.billingcountry,
                   contact.account.billingpostalcode,
                   contact.account.billingcity,
                   contact.account.billingstate,
                   contact.account.fax,
                   contact.Functional_Role__c,
                   contact.account.phone, 
                   contact.account.e_Subscribe__c,         
                   contact.fax,contact.activation_url__c,contact.OktaId__c,Contact.Is_MDADL__c,
                   contact.RAQA_Contact__c from user where Id =: UserInfo.getUserId() limit 1];
                
                SapAcctName=usrMyCP.contact.account.Legal_Name__c;
                initializeCRA();
                                   
                
              IF(SapAcctName!=null)
              {
                List<String> SapaccFirst=new  List<String>();
                SapaccFirst=SapAcctName.split('\r\n');
                    if(SapaccFirst.size()>0)
                    {
                      SapAcctName=SapaccFirst[0];
                    }
              }
             
        if(usrMyCP.contactid == null){   if(usrMyCP.usertype != label.guestuserlicense){ Usrname = usrMyCP.alias;   shows = 'block';    isGuest = false;   }
        }
        isConsentEnable = false;
        set<string> setFunctionalRole = new set<string>{'Physicians','Physicians – chief','Dept Admin','Physicist','CXO'};
        if(usrMyCP.contactId <> null && usrMyCP.contact.Functional_Role__c <> null && setFunctionalRole.contains(usrMyCP.contact.Functional_Role__c) ){
            isConsentEnable = true;
        }
        
   strCountry=usrMyCP.contact.account.billingcountry;
   for (Contact_Role_Association__c CRA : [Select account__c,account__r.e_Subscribe__c from Contact_Role_Association__c where Contact__c =: usrMyCP.ContactId and Role__c='RAQA'])
    {
      accntsubscrbcmap.put(CRA.account__c,CRA.account__r.e_Subscribe__c);
    }
    if(usrMyCP.LastLoginDate == null){
       resetpass = true;
       }
    /*if(usrMyCP.contact.Functional_Role__c == label.Functional_role){
       ;
       }  */

   //if( usrMyCP.contact.RAQA_Contact__c== true)
   sublink = false;
   if( accntsubscrbcmap.size() > 0)
    { 
    blnNOTRPC=false;                            // tab Visibility and Message for RAQA Contact
      for(Id b: accntsubscrbcmap.keyset())
      {
        if (accntsubscrbcmap.get(b) == false)
         sublink = true;
      }
      //if(usrMyCP.contact.account.e_Subscribe__c==true)
      if(!sublink)
        {
          strSubsCribUnsubsCrib=true;  // for PNL admin -Link Visibility
        }else
        {
          strSubsCribUnsubsCrib=false;
        }

}else
{
blnNOTRPC=true;
}

      
       
        fetchProds();
        subscribed = false;
        usr = [Select id,Name,Subscribed_Products__c,Email , Contact.id From User where Id =: UserInfo.getUserId() limit 1];
        if(usr != null && usr.Subscribed_Products__c ){
            subscribed = true;
        }
        
    }

/****** Function for reseting password and sending Http request to okta ******/
 
 
  public List<wCRA> lstWCRA{get;set;}
  map<Id,Customer_Agreements__c> mapCustomerAgreements;
  public boolean isIConsent{get;set;}
  public class wCRA{
    public boolean Action{get;set;}
    public boolean OldAction{get;set;}
    public string InsName{get;set;}
    public string InsAdd{get;set;}
    public string conId{get;set;}
    public Customer_Agreements__c Ag{get;set;}
    public wCRA(boolean Action, string InsName, string InsAdd,Customer_Agreements__c Agobj){
        this.Action = Action;
        this.OldAction = Action;
        this.InsName = InsName;
        this.InsAdd = InsAdd;       
        this.Ag = Agobj;  
        this.conId = Ag.Agreement_Logged_By__c;
    }
}
  
  public wCRA setCustomerAgreement(Account Accobj,map<Id,Customer_Agreements__c> mapCustAgre){

        boolean Action;
        string InsName =Accobj.Name;
        string InsAdd = Accobj.billingstreet+', '+Accobj.billingCity+', '+Accobj.billingState+', '+Accobj.BillingPostalCode+', '+Accobj.billingcountry;
        if(InsAdd.contains('null')){
            InsAdd = InsAdd.replaceAll(', null','');
        }
        Customer_Agreements__c ca;      
        if(mapCustAgre.containsKey(Accobj.Id) && mapCustAgre.get(Accobj.Id).Agreement_Response__c == 'Yes'){            
            ca = mapCustAgre.get(Accobj.Id);
            Action = true;
            
        }else{
            Action = false;         
            ca = new Customer_Agreements__c();
            ca.Agreed_By__c = null;
            ca.Agreement_Logged_By__c = null;
            ca.Agreement_Date__c = null;
            ca.Agreement_Method__c = 'MyVarian';
            ca.Agreement_Response__c = 'Yes';
            ca.Agreement_Type__c = 'Remote Service';
            ca.Affected_Account__c = Accobj.Id;
            ca.Consent_Agreed__c = ConsentContent;
            //ca.Primary_Account__c = usrMyCP.contact.AccountId;
        }
        wCRA obj = new wCRA(Action, InsName, InsAdd,ca);
      return obj;
  }
  public void initializeCRA(){
    
    isIConsent = true;
    lstWCRA = new List<wCRA>();
    set<Id> setAccount = new set<Id>();
    if(usrMyCP.ContactId <> null && usrMyCP.Contact.accountid <> null){
        setAccount.add(usrMyCP.Contact.accountid);
    }

    //STSK0012733.Querying CRA for supported institutions based on current loggedin contact.
    craList = [Select Id, Account__r.Name, Account__r.billingstreet,
                                   Account__r.billingcountry,
                                   Account__r.billingpostalcode,
                                   Account__r.billingcity,
                                   Account__r.billingstate,Account__c,Contact__c
                                   From Contact_Role_Association__c Where Contact__r.Id =: usrMyCP.contact.Id limit 1000];
                                   
    List<Contact_Role_Association__c> lstCRA = craList;
       

    map<Id,Customer_Agreements__c> mapAgreManually = new map<Id,Customer_Agreements__c>();
    
    set<Id> SetRoleAssAccount = new set<Id>();
    for(Contact_Role_Association__c c: lstCRA){
        setAccount.add(c.Account__c);
        SetRoleAssAccount.add(c.Account__c);
    }
    List<Customer_Agreements__c> lstAgreement = [select Id,Name,Agreed_By__c,Agreement_Date__c,Agreement_Logged_By__c,Agreement_Method__c,
    Agreement_Type__c,Agreement_Response__c,Comment__c,Affected_Account__c,createdDate from Customer_Agreements__c where Affected_Account__c IN: setAccount]; //,Primary_Account__c
    
    for(Customer_Agreements__c c: lstAgreement){
        setAccount.add(c.Affected_Account__c);
        if(mapAgreManually.containsKey(c.Affected_Account__c)){
            Customer_Agreements__c cg = mapAgreManually.get(c.Affected_Account__c);
            if(cg.createdDate < c.createdDate ){
                mapAgreManually.put(c.Affected_Account__c,c);
            }
        }else{
            mapAgreManually.put(c.Affected_Account__c,c);
        }
    }
    map<Id,Account> mapAccount = new map<Id,Account>([select Id,Name,billingstreet,billingCity,billingState,BillingPostalCode,billingcountry from Account where id IN: setAccount]);
    
    //Primary Consent
    if(usrMyCP.ContactId <> null && usrMyCP.Contact.accountid <> null){     
        lstWCRA.add(setCustomerAgreement(mapAccount.get(usrMyCP.Contact.accountid),mapAgreManually));      
    }
    //Contact Role Association
    if(SetRoleAssAccount.size() > 0){
        for(Id AccId : SetRoleAssAccount){
            boolean isprimaryAccount = false;
            if(usrMyCP.contact.Id <> null && usrMyCP.contact.AccountId <> null)
            {
                if(usrMyCP.contact.AccountId <> AccId){
                    isprimaryAccount = false;
                }
                else{
                    isprimaryAccount = true;
                }
                
            }
            if(!isprimaryAccount){
                lstWCRA.add(setCustomerAgreement(mapAccount.get(AccId),mapAgreManually));
            }            
        }
    }
    
    //Manual Agreements
    if(mapAgreManually.size() > 0){
        for(Id AccId : mapAgreManually.keyset()){
            boolean isprimaryAccount = false;
            if(usrMyCP.contact.Id <> null && usrMyCP.contact.AccountId <> null)
            {
                if(usrMyCP.contact.AccountId <> AccId){
                    isprimaryAccount = false;
                }
                else{
                    isprimaryAccount = true;
                }
                
            }
            if(!isprimaryAccount && !SetRoleAssAccount.contains(AccId)){
                lstWCRA.add(setCustomerAgreement(mapAccount.get(AccId),mapAgreManually));
            }
        }
    }
    
    for(WCRA obj : lstWCRA){
        if(!obj.Action){isIConsent = false;break;}
    }
  }
 public void iConsentClick(){
     try {
        List<Customer_Agreements__c> lstNewCA = new List<Customer_Agreements__c>();
        for(wCRA w : lstWCRA){
            if(w.OldAction <> w.Action){
                
                Customer_Agreements__c ca = w.Ag;            
                if(w.Action){                   
                    ca.Agreement_Date__c = system.now();
                    ca.Agreement_Response__c = 'Yes';                   
                    ca.Agreement_Logged_By__c = userinfo.getUserId(); 
                    ca.Agreed_By__c = usrMyCP.contact.Id;                   
                }
                lstNewCA.add(ca);
            }
        } 
        insert lstNewCA;
     }
     catch(Exception e){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()+'-'+e.getLineNumber());
        ApexPages.addMessage(myMsg);
     }
     initializeCRA();
 }
 
  public void Resetpass(){
     Newpass = ApexPages.currentPage().getParameters().get('PWDConfirm');
     ConfirmPass = ApexPages.currentPage().getParameters().get('ConfirmPWD');
     String OldPassword = ApexPages.currentPage().getParameters().get('OLDPWD');
     //String oldpasswrd = usrMyCP.contact.activation_url__c;
     List<String> errorlist = new List<String>();
     String singleerror ='@';
     /**** Http request for changing the password *******/
     
     HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        String strToken = label.oktatoken; //'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        String body = '{"oldPassword": { "value": "'+OldPassword +'" },"newPassword": { "value": "'+ Newpass + '" }}';
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setmethod('POST');
        req.setbody(body);
        String endpoint = label.oktachangepasswrdendpoint + usrMyCP.contact.OktaId__c + '/credentials/change_password';
        req.setEndpoint(endpoint);
        try {
           //Send endpoint to OKTA
           if(!Test.isRunningTest()){
           res = http.send(req);
           }else{
             res = fakeresponse.fakeresponsemethod('updateuser');
           }
           //Parsing for error
           JSONParser parser = JSON.createParser(res.getBody());
           while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'errorCauses') {
                      parser.nextToken();
                      if((parser.getCurrentToken() == JSONToken.START_ARRAY)){
                            while(parser.nextToken() != JSONToken.END_ARRAY){
                              parser.nextToken();
                              //parser.nextToken();
                              if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                 String fieldName1 = parser.getText();
                                   parser.nextToken();
                                    if(fieldName1 == 'errorSummary') {
                                          errorlist.add(parser.getText());
                                           }
                                       }
                                    }
                                }                     
                        }if(fieldName == 'errorSummary'){
                          //parser.nextToken();
                          singleerror = parser.getText();
                        }
                        
                    
                }
           }try{
           Contact con  = new Contact(id = usrMyCP.contactId);
           con.PasswordresetDate__c = system.today();
           update con;
           }catch(DMLException ex){
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage() + 'Your Password has been changed successfully,But contact Portal Admin for this error'));
            }
           }catch(System.CalloutException e) {
            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error,ex.getMessage() + 'Please Contact Portal Admin for help'));
        }
     if(singleerror != '@' || errorlist.size() > 0)
      {
       ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error,'Invalid information has been provided.Please try again.'));
       //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, 'Following error occured while Password reset:' + singleerror + errorlist));
      }else{
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Your password has been successfully updated.')); 
      }
  }

 /************ Method for reseting the recovery Question ***********/
 public void ResetRcvryQus(){
     String Curntpass = ApexPages.currentPage().getParameters().get('PWDCurrent');
     String RecoveryQ = ApexPages.currentPage().getParameters().get('RECVRYQ');
     String RecoveryA = ApexPages.currentPage().getParameters().get('RECVRYA');
     RecoveryA = RecoveryA.deleteWhitespace().toLowerCase();
     //String oldpasswrd = usrMyCP.contact.activation_url__c;
     List<String> errorlist = new List<String>();
     String singleerror ='@';
     /**** Http request for changing the password *******/
     
     HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        String strToken = label.oktatoken; //'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        String body = '{"password": { "value": "'+ Curntpass +'" },"recovery_question": {"question" : "'+ RecoveryQ + '","answer": "' + RecoveryA + '"}}';
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setmethod('POST');
        req.setbody(body);
        String endpoint = label.oktachangepasswrdendpoint + usrMyCP.contact.OktaId__c + '/credentials/change_recovery_question';
        req.setEndpoint(endpoint);
        try {
           //Send endpoint to OKTA
           if(!Test.isRunningTest()){
           res = http.send(req);
           }else{
             res = fakeresponse.fakeresponsemethod('updateuser');
           }
            //Parsing for error
           JSONParser parser = JSON.createParser(res.getBody());
           while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'errorCauses') {
                      parser.nextToken();
                      if((parser.getCurrentToken() == JSONToken.START_ARRAY)){
                            while(parser.nextToken() != JSONToken.END_ARRAY){
                              parser.nextToken();
                              //parser.nextToken();
                              if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                 String fieldName1 = parser.getText();
                                   parser.nextToken();
                                    if(fieldName1 == 'errorSummary') {
                                          errorlist.add(parser.getText());
                                           }
                                       }
                                    }
                                }                     
                        }if(fieldName == 'errorSummary'){
                          //parser.nextToken();
                          singleerror = parser.getText();
                        }
                        
                    
                }
           }
           //End
           }catch(System.CalloutException e) {
        }
      contact con = new contact(id = usrMyCP.ContactId);
      con.Recovery_Question__c = RecoveryQ;
      con.Recovery_Answer__c = RecoveryA;
      try{
        update con;
      }catch(DMLException e){
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage() + ' Please contact Portal admin for this.'));
      }
      if(singleerror != '@' || errorlist.size() > 0)
      {
      // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, 'Following error occured while recoveryquestion reset:' + singleerror + errorlist));
        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, 'The password provided is incorrect.'));
      }else{
      ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The recovery question and answer have been successfully reset.')); 
      }
 }
   
    public PageReference subscribe() {
        String prods;       
        if(usr != null){
            /*
            integer i=0;
            for(wrappecls wc : productList){
                if(i == productList.size()){
                    prods += wc.prod1;
                    break;
                }
                
                prods += wc.prod1+';';
                i++;
            }
            */
            usr.Subscribed_Products__c = true;
            try{
                update usr;
                subscribed = true;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(usr.id); //30-Aug-2017 - Nilesh - STSK0012616 - changed mail targetobjectid from usr.contact.id to usr.id 
                /*String[] toAddresses = new String[] {usr.Email};
                mail.setToAddresses(toAddresses);*/
                EmailTemplate et = [Select id from EmailTemplate where name = 'Cp:MyNotifications Subscription'];
                mail.setTemplateId(et.Id);
                mail.setSaveAsActivity(false);
                
                 for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
                {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                mail.setOrgWideEmailAddressId(owa.id); 
                 } 
                //mail.setInReplyTo('Varian Medical Systems');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                

                

            }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
            }
        }
        
    
            //PageReference acctPage = new PageReference ('/apex/CpMyAccount');
            //acctPage.setRedirect(true);
             return null;
    }
    
    public PageReference unSubscribe(){
        //String prods = '';        
        if(usr != null){
                        
            usr.Subscribed_Products__c = false;
            try{
                update usr;
                subscribed = false;
            }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
            }
        }
        
        //PageReference acctPage = new PageReference ('/apex/CpMyAccount');
        //acctPage.setRedirect(true);
        return null;
    }
    
    public List<SelectOption> getCountries()
  {
   List<SelectOption> options = new List<SelectOption>();      
   Schema.DescribeFieldResult fieldResult =Regulatory_Country__c.RA_Region__c.getDescribe();                          
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();    
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }       
   return options;
}
    
    public PageReference chngPwds()   // This method is used to change password
    {
        update  usrMyCP; PageReference pg;
        if(proppwd != null && propRepwd !=null && proppwd != '' && propRepwd!='')
        {
             try
            {   
                if(proppwd.compareTo(propRepwd)==0)
                {
                 pg= site.changePassword(proppwd,propRepwd,stroldpwd);
                 PageReference acctPage = new PageReference (pg.getURL());
                 acctPage.setRedirect(true);
                return acctPage;
                }else
                {
                 Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please enter the confirmpassword again') );
               return null;
                }
           }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
           return null;
           }
        }
          PageReference acctPage = new PageReference ('/apex/CpMyAccount');
        acctPage.setRedirect(true);
        return acctPage;
       
    }
    public PageReference updatecon() // This method is used to update Contact details
    {
   If(! Test.isRunningTest())
    {
    update usrMyCP.contact;
    usrMyCP.contact.account.billingcountry=strCountry;
     update usrMyCP.contact.account;
     }
    PageReference acctPage = new PageReference ('/apex/CpMyAccount');
        acctPage.setRedirect(true);
        return acctPage;
  
    }
    
    
    public PageReference fnunsubscribe() // This method is used to unsubscribe PNL
    {
   /* strSubsCribUnsubsCrib = false;
    List<Account> updateacclist = new List<Account>();
    list<string> lstEmail=new list<string>();
    id RSSUserId;
    list<Regional_support_specialist__c> lstRSS=new list<Regional_support_specialist__c>();
    //usrMyCP.contact.account.e_Subscribe__c=false;
    usrMyCP.contact.Reason_To_Unsubscribe__c=strComment;
    for(Id accid : accntsubscrbcmap.keyset())
        {
          Account updateacc = new Account(id = accid);
          updateacc.e_Subscribe__c = false;
          updateacclist.add(updateacc);           
        }
    If(! Test.isRunningTest())
        {
        update usrMyCP.contact;
        //update usrMyCP.contact.account;
        update updateacclist;
        }
   lstRSS=[select id, Primary_Contact__r.Email,Regulatory_Country__r.name from Regional_support_specialist__c where Regulatory_Country__r.Name=:usrMyCP.contact.mailingCountry];
    
    if(lstRSS.size()>0)
     {
      for(Regional_support_specialist__c varRSS:lstRSS )
      {
        if(varRSS.Primary_Contact__r.Email !='' && varRSS.Primary_Contact__r.Email!=null)
        {
            lstEmail.add(varRSS.Primary_Contact__r.Email );
            RSSUserId = varRSS.Primary_Contact__c;
        
        }
        
      }
        
    }
     /*           List <Messaging.SingleEmailMessage> maillist = new List<Messaging.SingleEmailMessage>();
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(usrMyCP.contact.id);
                EmailTemplate et = [Select id from EmailTemplate where DeveloperName = 'Portal_PNL_Unsubscribe_RAQA'];
                mail.setTemplateId(et.Id);
                mail.setSaveAsActivity(false);
               /*if(lstEmail.size()>0)
                {
                mail.setToAddresses(lstEmail);
                } //mail.setInReplyTo('Varian Medical Systems'); */
              /*  maillist.add(mail);
                
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setTargetObjectId(RSSUserId);
                /*EmailTemplate et1 = [Select id from EmailTemplate where DeveloperName = 'Portal_PNL_Unsubscribe_RSS'];
                mail1.setTemplateId(et1.Id);
                mail1.setWhatId(usrMyCP.contact.id);*/
               /* mail1.setSubject('Unsubscription – RA contact '+ usrMyCP.Name);
                mail1.setHtmlBody('Hi,<br/> The RA Contact, '+ usrMyCP.Name + ' in your region has unsubscribed for electronic notifications.<br/><br/>Thank you<br/>Varian Medical System');
                if(lstEmail.size()>0)
                {
                mail1.setToAddresses(lstEmail);
                }
                mail1.setSaveAsActivity(false);
                maillist.add(mail1);
                
                Messaging.sendEmail(maillist);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    //PageReference acctPage = new PageReference ('/apex/CpMyAccount');
        //acctPage.setRedirect(true);*/
        
        return null;
  
    }
    
    
    public PageReference fnsubscribe() // This method is used to subscribe for PNL
    {/*
    strSubsCribUnsubsCrib = true;
    list<string> lstEmail=new list<string>();
    list<Regional_support_specialist__c> lstRSS=new list<Regional_support_specialist__c>();
    usrMyCP.contact.account.e_Subscribe__c=true;
    id RSSUserId;
    List<Account> updateacclist = new List<Account>();
    for(Id accid : accntsubscrbcmap.keyset())
        {
          Account updateacc = new Account(id = accid);
          updateacc.e_Subscribe__c = true;
          updateacclist.add(updateacc);           
        }
    
    If(! Test.isRunningTest())
    {
    //update usrMyCP.contact.account;
    update updateacclist;
    }
   //lstEmail.add(usrMyCP.contact.email);
   lstRSS=[select id, Primary_Contact__r.Email,Regulatory_Country__r.name from Regional_support_specialist__c where Regulatory_Country__r.Name=:usrMyCP.contact.mailingCountry];
    
    if(lstRSS.size()>0)
    {
    for(Regional_support_specialist__c varRSS:lstRSS )
    {
    if(varRSS.Primary_Contact__r.Email !='' && varRSS.Primary_Contact__r.Email!=null)
    {
        lstEmail.add(varRSS.Primary_Contact__r.Email );
        RSSUserId = varRSS.Primary_Contact__c;
    
    }
    
    }
    
    }
               /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(usrMyCP.contact.id);
               // String[] toAddresses = new String[] {usr.Email};
                if(lstEmail.size()>0)
                {
                mail.setToAddresses(lstEmail);
                }
                EmailTemplate et = [Select id from EmailTemplate where name = 'Customer Portal: Subscribed e-Notification'];
                mail.setTemplateId(et.Id);
                mail.setSaveAsActivity(false);
                //mail.setInReplyTo('Varian Medical Systems');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
                
                //Comenting for test class as pnl is not going live 
                
                /*List <Messaging.SingleEmailMessage> maillist = new List<Messaging.SingleEmailMessage>();
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(usrMyCP.contact.id);
                EmailTemplate et = [Select id from EmailTemplate where DeveloperName = 'Portal_PNL_Subcription_RAQA'];
                mail.setTemplateId(et.Id);
                mail.setSaveAsActivity(false);
                maillist.add(mail);
                
                Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                mail1.setTargetObjectId(RSSUserId);
                mail1.setsubject('Subscription – RA contact '+ usrMyCP.Name);
                mail1.setHtmlBody('Hi,<br/> The RA Conatct, '+ usrMyCP.Name + ' in your region has subscribed for electronic notifications. <br/><br/>Thank You,<br/>Varian Medical System');
                if(lstEmail.size()>0)
                {
                mail1.setToAddresses(lstEmail);
                }
                
                mail1.setSaveAsActivity(false);
                maillist.add(mail1);
                
                Messaging.sendEmail(maillist);
                
                */
             //PageReference acctPage = new PageReference ('/apex/CpMyAccount');
        //acctPage.setRedirect(true);
        
        return null;
      
  
    }
     
     public void fetchProds() { 
        
         prodList1 = new List<String>();
         prodList2 = new List<String>();  
            
        // to get the vlues of product group field
        Schema.DescribeFieldResult productgroup = product2.Product_Group__c.getDescribe();

        List<Schema.PicklistEntry> productgroupvalues = productgroup.getPicklistValues();

        Set<String> allProducts = new Set<String>();
        List<String> allprodlist = new list<String>();
        if(usrMyCP.contactid == null){
          for(Schema.PicklistEntry prd: productgroupvalues ){
          allProducts.add(prd.getvalue());
          }
        }else{
        allProducts.addall(CpProductPermissions.fetchProductGroup());
        }
        allprodlist.addall(allProducts);
        integer i = 1;
        for(String str: allprodlist){

           if(math.mod(i,2) != 0){
                prodList1.add(str);
            }   
            else{
                prodList2.add(str);
            }
            i++;
                            
       } 
    } 
    
    
    /*
     * Check LMS Account tab visibility
     */
     public boolean getCheckLMSTabVisibility(){
        map<String,MV_LMS_REG_FuntionalRole__c> functinalRolesMap = MV_LMS_REG_FuntionalRole__c.getall();
        return functinalRolesMap.containsKey(usrMyCP.contact.Functional_Role__c);
     }
     
     /*
      * Register Now Action 
      */    
     public void registerNow(){  
         usrMyCP.LMS_Status__c = 'Request';  
         usrMyCP.LMS_Registration_Date__c = system.now();
         if(usrMyCP.LMS_Last_Access_Date__c  != Null)      usrMyCP.LMS_Last_Access_Date__c = Null;
         if(usrMyCP.LMS_Activated_Date__c != Null)        usrMyCP.LMS_Activated_Date__c = Null;   
         if(usrMyCP.LMS_Deactivation_Date__c != Null)         usrMyCP.LMS_Deactivation_Date__c = Null;
         if(usrMyCP.LMS_Last_Course_End_date__c != Null)        usrMyCP.LMS_Last_Course_End_date__c = Null;
            
         if(!test.isRunningTest())update usrMyCP;    
     }
     
     public Id recordId{get;set;}
     
     public PageReference removeCRA()
     { 
         Contact_Role_Association__c delRecord = [Select Id From Contact_Role_Association__c Where Id =: recordId Limit 1];
         delete delRecord;
         initializeCRA();
         return null;
         //PageReference acctPage = new PageReference ('/apex/CpMyAccount');
         //acctPage.setRedirect(true);
         //return acctPage;
     }
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 11-June-2013
    @ Description   :  Test class for CpMyAccount class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
    @ Last Modified By :   Nilesh Gorle
    @ Last Modified On  :   30-August-2017
    @ Last Modified Reason  :  fix testclass and covered all test scenario
    
Change Log:
30-Aug-2017 - Nilesh- STSK0012616- fix test class and covered all scenario
****************************************************************************/

@isTest
Public class CpMyAccountTest
{
    public static Profile p1;
    public static Profile po;
    public static Profile portalProfile;
    public static UserRole r;
    public static User usr;
    public static Account a;
    public static Contact con;
    public static User u;
    public static Contact_Role_Association__c cr;
    public static AccountShare shareCR;
    
    static testmethod void testCpMyAccount() { 
           
        p1 = [select id from profile where name='System Administrator'];       
        po = [select id from profile where name='VMS MyVarian - Customer User']; 

        portalProfile = [SELECT Id, UserType FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' Limit 1];
        //UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
        r = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        usr = [Select id,Profile.name, UserRoleId from User where Profile.name='System Administrator' and UserRoleId != null and IsActive = True  limit 1];
        //User usr = [Select Id,UserRoleId from User where UserRoleId=:r.Id];
        System.RunAs(usr) {
            a = new Account(name='test2',BillingPostalCode ='95051',BillingCity='SantaClara',BillingCountry='usa',BillingState='Washington',Country__c='Test',Legal_Name__c ='Testing\r\n123',e_Subscribe__c=true,special_care_instruction_new__c='test',ERP_Timezone__c='PST'); 
            a.OMNI_Address1__c = '2006 pacific street';
            a.OMNI_Address2__c = 'test street';
            a.OMNI_City__c = 'Santa Clara';
            a.OMNI_Postal_Code__c = '95051';
            a.OMNI_State__c = 'CA';
            a.Sub_Region__c = 'test';
            a.BillingCity = 'Santa Clara';
            insert a; 

            con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate'); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = portalProfile.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,username='stand_arduser@testorg.com'/*,UserRoleid=r.id*/); 
            insert u;

            cr = new Contact_Role_Association__c();
            cr.Account__c= a.Id;
            cr.Contact__c = u.ContactId;
            cr.Role__c = 'RAQA';
            insert cr;

            shareCR = new AccountShare();
            shareCr.AccountId = a.Id;
            shareCr.UserOrGroupId  = u.id;
            shareCr.AccountAccessLevel = 'Edit';
            shareCr.OpportunityAccessLevel = 'Edit';
            shareCr.CaseAccessLevel = 'Edit'; 
            //shareCr.RowCause = Schema.AccountShare.RowCause.Manual;
            Database.insert(shareCr);            
            
            ApexPages.currentPage().getParameters().PUT('PWDCurrent','TESTPASS');
            ApexPages.currentPage().getParameters().put('RECVRYQ','testrecQUESTION');
            ApexPages.currentPage().getParameters().PUT('RECVRYA','TESTREQANS');             
            
            Test.startTest();
            CpMyAccount MyAccount = new CpMyAccount();
            MyAccount.strComment = '';
            MyAccount.ConsentContent = '';
            MyAccount.resetpass = true;
            MyAccount.oldpass = '';
            MyAccount.showmssg = 'true';
            MyAccount.SapAcctName = 'abc\r\nxyz';
            MyAccount.isConsentEnable = true;
            MyAccount.blnNOTRPC = true;
            MyAccount.showemailacc = 'true';
            MyAccount.proppwd='test123451';
            MyAccount.propRepwd='test123451';
            MyAccount.stroldpwd='test1234512';
            MyAccount.getCountries();   
            MyAccount.subscribe();
            MyAccount.unSubscribe();
            MyAccount.chngPwds();
            //MyAccount.updatecon();
            MyAccount.fnunsubscribe();
            MyAccount.fnsubscribe();
            MyAccount.getCountries();
            MyAccount.proppwd='test123451';
            MyAccount.propRepwd='test1234512';
            MyAccount.stroldpwd='test1234512';
            MyAccount.accntsubscrbcmap.put(a.Id, true);
            MyAccount.chngPwds();
            //MyAccount.proppwd='';
            //MyAccount.chngPwds();
            //MyAccount.Resetpass();
            MyAccount.ResetRcvryQus();
            MyAccount.updatecon();
            MyAccount.getrecoveryqusoptions();
            //MyAccount.updatecon();
            MyAccount.recordId = cr.Id; // set CRA record id to recordId
            MyAccount.removeCRA();

            MyAccount.registerNow();
            //MyAccount.getCheckLMSTabVisibility();

            for(CpMyAccount.wCRA w : MyAccount.lstWCRA){
                w.Action = !w.Action;
            }
            MyAccount.iConsentClick();
            MyAccount.getCheckLMSTabVisibility();
            MyAccount.Resetpass();
            
            CpMyAccount.wCRA wcra = new CpMyAccount.wCRA(true, 'Test', 'abc', new Customer_Agreements__c());
            
            Customer_Agreements__c ca = new Customer_Agreements__c(Affected_Account__c=a.Id);
            insert ca;
            Customer_Agreements__c ca1 = new Customer_Agreements__c(Affected_Account__c=a.Id);
            insert ca1;

            map<Id,Customer_Agreements__c> mapCustAgre = new map<Id,Customer_Agreements__c>();
            mapCustAgre.put(ca.Id, ca);
            mapCustAgre.put(ca1.Id, ca1);
            MyAccount.setCustomerAgreement(a, mapCustAgre);
            Test.stopTest();
            
        }
    }
}
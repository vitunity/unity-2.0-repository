Public class CpNewCase
{
    Public String AcctName {get; set;}      
    Public String SelPriority {get; set;}   
    Public String custName {get; set;}
    Public String contMethod {get; set;}
    Public String product{get;set;}
    Public String prodVersion {get; set;}
    Public String subject {get; set;}
    Public String Tasksubject {get; set;}
    Public String WorkordName {get; set;}
    Public String WorkordStatus {get; set;}
    Public Date WorkordDate {get; set;}
    Public String WorkordProbDesc {get; set;}
    Public String TaskComment {get; set;}
    Public String TaskComment2 {get; set;}
    Public String TaskStatus {get; set;}
    Public String TaskPriority {get; set;}
    Public String TaskActivityDate {get; set;}
    Public String TaskName {get; set;}
    Public String description {get; set;}
    Public String AccId {get; set;}
    public Contact c{get;set;}
    public String usrContId{get;set;} 
    public Boolean isGuest{get;set;}
    public string shows{get;set;}
    public string Usrname{get;set;}
    public string caseId{get; set;}
    public List<selectoption> AccountList;
    public List<selectoption> ContMethList;
    public Attachment Attachmntfile{get;set;}
    public boolean CreateEdit{get;set;}
    public boolean ReopenCase{get;set;}
    public boolean inputfileshow{get;set;}
    public List<selectoption> productList;
    public String Casenumber{get;set;}
    public String SerialName{get;set;}
    public String CommentInput{get;set;}
    public Boolean Isvisible{get;set;}
    public Boolean IsDescvisible{get;set;}
    public Boolean IsWorkordrvisible{get;set;}
    public boolean multipleacc{get;set;}
    public Static List<Attachment > Attachments {get;set;}
    public Case newCase{get;set;}
    public String CaseStatus {get; set;}
    public String CasePriority{get;set;}
    User usr;
    EmailMessage TaskDetail;
    SVMXC__Service_Order__c WrkOrder;
    public String TaskDetailnew {get; set;}
    public EmailMessage EmailMsg {get; set;}
    public boolean isDetail{get; set;}
    public boolean isWrkOrdr{get; set;}
    public string mail{get; set;}
    public String imageURL {get;set;}
    public String taskid{get;set;}
    public List<Attachment> Emailattachmentids{get;set;}
    set<String> countryset = new set<String>();
    
    public class finalComments implements Comparable
    {
        public DateTime createdDateTime{get; set;}
        public String createdDate{get; set;}
        public String comment{get; set;}
        public String commentedBy{get; set;}
        public boolean isTask{get; set;}
        public Id objectId{get; set;}
        
        public finalComments(CaseComment caseComm)
        {
            createdDateTime = caseComm.Createddate;
            comment = caseComm.commentBody;
            commentedBy = caseComm.CreatedBy.FirstName + ' ' + caseComm.CreatedBy.LastName;
            createdDate = dateInFormat(createdDateTime);
            isTask = false;
            objectId = caseComm.Id;
        }
        public finalComments(Task tsk)
        {
            createdDateTime = tsk.Createddate;
            comment = tsk.Subject;
            if(comment.IndexOf('[') != -1)
            {
                String finalstr = comment.substring(0,comment.IndexOf('['));
                //List<string> tempComments = comment.split('[');
                system.debug('Subject after splitting'+finalstr );
                comment = finalstr;//tempComments[0];
            }
            commentedBy = tsk.CreatedBy.FirstName + ' ' + tsk.CreatedBy.LastName;
            createdDate = dateInFormat(createdDateTime);
            isTask = true;
            objectId = tsk.Id;
        }
        String dateInFormat(DateTime dtTm)
        {
            return(dtTm.month() + '/' + dtTm.day() + '/' +dtTm.year());
        }
        
        public Integer compareTo(Object objToCompare) 
        {
            if(createdDateTime > ((finalComments)objToCompare).createdDateTime){
                return -1;
            }
            else if(createdDateTime > ((finalComments)objToCompare).createdDateTime){
                return 0;
            }
            else{
                return 1;
            }
        }
    }
    public List<finalComments> finalCommentsList {get; set;} 
    
    public List<selectoption> getproductList()
    {
        productList = new List<selectoption>();
        Schema.DescribeFieldResult fieldResult = Product2.Product_Group__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            productList.add(new selectoption(f.getValue(),f.getValue()));
        }
        productList.sort();
        productList.add(0,new selectoption('','Select'));
        return productList;
    }
    
    Public List<SelectOption> Priority;
    Public List<SelectOption> getPriority(){
        Priority = new List<SelectOption>();
        Schema.Describefieldresult fieldResult = Case.Priority.getDescribe();       
        
        for(Schema.Picklistentry picklistvalues: fieldResult.getPicklistValues()) {
            Priority.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
            
        if(SelPriority == null || SelPriority == '')SelPriority = 'Medium';
        return Priority;
    }
    
    public List<SelectOption> getContMethList(){
        contMethod = 'Email';
        ContMethList = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Case.Primary_Contact_Number__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            ContMethList.add(new selectoption(f.getValue(),f.getValue()));
        } 
        return ContMethList;
    }
    Set<id> RelatedAccountIds = new set<Id>();
    public List<SelectOption> getAccountList(){
        AccountList = new List<SelectOption>();
        AccountList.add(new SelectOption('--Any--','--Any--'));
        if(usr.AccountId != null)
        {
            AccountList.add(new SelectOption(usr.AccountId,[Select Name from account where id =: usr.AccountId].Name));
            RelatedAccountIds.add(usr.AccountId);
        }
        for(Contact_Role_Association__c contacclist : [Select id,Contact__c,Account__r.Name,Account__c from Contact_Role_Association__c where Contact__c =:usr.ContactId])
        {
            AccountList.add(new SelectOption(contacclist.Account__c,contacclist.Account__r.Name));
            RelatedAccountIds.add(contacclist.Account__c);
        }
        return AccountList;
    }
    public class AssignComment        // This class will be used to store the corresponding input from the user and the Case Comment
    {  
        public CaseComment comment {get; set;}        //This will store the case Comment
        public String Commentedbyuser{get; set;}        
        public String CommentDate{get; set;}
        public Boolean published {get; set;} 
        public AssignComment(){}                      //Empty constructor
    }  
    public List<assignComment> Records {get; set;} 
    public  List<Task> Lsttask {get; set;} 
    public  List<SVMXC__Service_Order__c> LstWork {get; set;}
    public Boolean showPGBln {get;set;}
    public String WorkorderId{get;set;}
    public Boolean checkcountry{get;set;}
    
    public CpNewCase()
    {
        Records = new List<AssignComment>();
        finalCommentsList = new List<finalComments>();
        inputfileshow = true;
        shows = 'none';
        contMethod = 'Email';
        isGuest = false;
        CreateEdit = false;
        isDetail = false;
        isWrkOrdr = false;
        checkcountry = false;
        taskid = ApexPages.currentPage().getParameters().get('Taskid');
        Attachmntfile = new Attachment();
        Emailattachmentids = new List<Attachment>();
        caseId = ApexPages.currentPage().getParameters().get('Id');
        if(caseId == NULL)
        {
            IsDescvisible=true;
        }
        if(ApexPages.currentPage().getParameters().get('type')=='WorkOrder')
        {
            IsWorkordrvisible=true;
        
        }
        if(ApexPages.currentPage().getParameters().get('Count')!=null)
        {
            WorkorderId=ApexPages.currentPage().getParameters().get('Count');
            showPGBln =true;
            IsDescvisible=false;
        }
        if(ApexPages.currentPage().getParameters().get('type')=='desc')
        {
            Isvisible=false;
            IsWorkordrvisible=false;
            IsDescvisible=true;
        }
        if(ApexPages.currentPage().getParameters().get('type')=='Details')
        {
            Isvisible=true;
        }
        Attachments = new List<Attachment>();
        Attachments = [select Body,ContentType,Description,Name,OwnerId,BodyLength from Attachment where Parentid=:ApexPages.currentPage().getParameters().get('Workid') AND Name like :'%_FSR%'];
        usr = [Select ContactId,AccountId,Profile.Name,Contact.Distributor_Partner__c,Contact.Account.country1__r.name from user where Id=: UserInfo.getUserId()];    
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        if(usr.contactid != null)
        {
            usrContId = usr.contactid;
        }
        if(caseId != NULL)
        {   
            CreateEdit = true;
           
            newCase = [SELECT Id,Account.Country__c,CaseNumber,Priority,Description,Local_Description__c,Serial_number__c,Contact.Name, Status,Product_System__c,Product_Version_Com__c,Account.Name,/*Product_Version_Build__r.name,*/ Contact.Email,Subject,Local_Subject__c,ProductSystem__c,ProductSystem__r.Name,SVMXC__Top_Level__r.name FROM Case WHERE Id = :caseId];
            CaseStatus = newCase.Status;
            CasePriority = newCase.Priority;
            
            for (CaseComment Node : [Select commentBody,Createddate,CreatedBy.username, isPublished,CreatedBy.FirstName,CreatedBy.LastName from CaseComment where parentId = :caseId order by Createddate desc])    //Query and loop through all the case comments
            {
                /*assignComment temp = new AssignComment();   // Create temp to insert into the list Records
                temp.comment = Node;
                temp.Commentedbyuser=Node.CreatedBy.FirstName;
                temp.CommentDate =String.valueof( Node.Createddate.date());
                temp.published = Node.isPublished;
                Records.add(Temp);*/
                finalCommentsList.add(new finalComments(Node));
            }
            if(caseId !=null)
            {
                Lsttask=[Select Subject, CreatedDate, CreatedBy.FirstName, CreatedBy.LastName from Task where Whatid=:caseId ];
                for(Task t :Lsttask)
                {
                    finalCommentsList.add(new finalComments(t));
                }
                system.debug('tasks are'+Lsttask);
                LstWork=[Select Subject__c,SVMXC__Order_Status__c,id,Name,SVMXC__Problem_Description__c,createddate from SVMXC__Service_Order__c where SVMXC__Case__c=:caseId];
                if(LstWork.size() > 0)
                {
                    isWrkOrdr = true;
                }
                System.debug('Size of work order'+LstWork);
            }
        
            custName = newCase.Contact.Name;
            AcctName = newCase.Account.Name;
            Casenumber = newCase.CaseNumber;
            SelPriority = newCase.Priority;
            if(newCase.ProductSystem__c != null) //DE3449
            {
                SerialName = newcase.ProductSystem__r.name;
            }
            //CustEmail = newCase.Contact.Email;
            if(newcase.Status != null){
                if(newcase.Status.contains('Close'))
                    ReopenCase = true;
            }
            //if(newcase.ProductSystem__c == null)
            //{
            product = newcase.Product_System__c;
            prodVersion = newcase.Product_Version_Com__c;
            /*}else if(newcase.ProductSystem__c != null)
            {
                product = newcase.SVMXC__Top_Level__r.Name;
                prodVersion = newcase.Product_Version_Build__r.Name;
            }*/
            subject = newCase.Subject;
            description = newCase.Local_Description__c;
            if(finalCommentsList.size()>0)
            {
                //description = description + String.valueof(finalCommentsList[0].comment);
                isDetail = true;
            }
            finalCommentsList.sort();
        }
        else
        {
            newCase = new Case();
            
            if(usrContId != null)
            {
                c = [select Id, Name, AccountId, Account.Name, Email FROM contact where Id =: usrContId];    
                custName = c.Name;
                AcctName = c.AccountId;
                //CustEmail = c.Email;
                AccId = c.AccountId;
                shows = 'none';
            }
            if(ApexPages.currentPage().getParameters().get('Taskid')!=null)
            {
                //Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage;
                EmailTemplate  mailtemplate = [select id, name from EmailTemplate  where name=:label.SR_Case002];         
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(mailtemplate.id);
                mail.saveAsActivity=true;
                mail.targetObjectId= newCase.contactid;
                mail.whatid= newCase.id;
                mail.replyTo=label.SR_Case001;  // Reply To Address Holds The Email To Case Service Address
                system.debug('Email body---->' + mail.HtmlBody);            
                TaskDetail=[Select Subject,htmlbody,TextBody,ParentID,(select id,name,BodyLength from attachments) from EmailMessage where id=:ApexPages.currentPage().getParameters().get('Taskid')];//[Select Subject,Description,status,Priority,ActivityDate,Who.Name,Whatid from Task where id=:ApexPages.currentPage().getParameters().get('Taskid')];
                  /*EmailMsg = [SELECT Id, ActivityId, HtmlBody FROM EmailMessage WHERE ActivityId = :ApexPages.currentPage().getParameters().get('Taskid')];
                  if(EmailMsg != NULL && EmailMsg.HtmlBody != NULL)
                  {
                      TaskDetailnew = EmailMsg.HtmlBody;
                  }*/
                 caseId = TaskDetail.ParentID;
                system.debug('Task is:'+TaskDetail);
                Tasksubject=TaskDetail.Subject;
                TaskComment = TaskDetail.htmlbody;

                if(TaskDetail.htmlbody != null && TaskDetail.TextBody != null && (TaskDetail.htmlbody.contains('<html>') != true))//&& (TaskDetail.htmlbody.contains('<br/>') != true)
                    TaskComment = TaskDetail.TextBody.replaceAll('\n','<br/>');
                if(TaskDetail.htmlbody == null && TaskDetail.TextBody != null)
                    TaskComment = TaskDetail.TextBody.replaceAll('\n','<br/>');
                //Integer Tsklth= TaskComment.length();
                /*if(Tsklth >500)
                {
                    TaskComment=TaskDetail.Description.substring(0,500);
                    TaskComment2=TaskDetail.Description.substring(500,Tsklth);
                }*/
          
                //TaskStatus =TaskDetail.Status;
                //TaskPriority =TaskDetail.Priority;
                //TaskActivityDate =string.valueof(TaskDetail.ActivityDate); 
                //TaskName =TaskDetail.Who.Name;
                if(TaskDetail.attachments != null && TaskDetail.attachments.size() > 0)
                {
                    
                    Emailattachmentids.addall(TaskDetail.attachments);
                    
                }
                
            }
            if(ApexPages.currentPage().getParameters().get('Workid')!=null)
            {
                WrkOrder=[Select Subject__c,SVMXC__Order_Status__c,id,Name,SVMXC__Problem_Description__c,createddate from SVMXC__Service_Order__c where id=:ApexPages.currentPage().getParameters().get('Workid')  ];
                WorkordName=WrkOrder.Name;
                WorkordStatus =WrkOrder.SVMXC__Order_Status__c;
                WorkordProbDesc = WrkOrder.SVMXC__Problem_Description__c;
                WorkordDate =Date.valueof( WrkOrder.createddate );
            }
        }
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            shows = 'block';
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype == label.guestuserlicense)
            {
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias; 
                shows='none';
                isGuest=true;
            }
        }
        getAccountList();
        if(AccountList.size() == 2)
        {
            multipleacc = true;
        }
        system.debug('CreateEdit ---->>'+CreateEdit );
        mail = [select id, HtmlValue, name, FolderId from EmailTemplate  where name= :label.SR_Case002].HtmlValue;
        imageURL='/servlet/servlet.FileDownload?file=';
        List< document > documentList=[SELECT Id, Name, DeveloperName FROM Document WHERE DeveloperName = 'Email_Template_Image'];
   
        if(documentList.size()>0)
        {
            imageURL=imageURL+documentList[0].id;
        }
        
        // logic for country
         if(usr.Contact.Distributor_Partner__c)
        {
            checkcountry = true;
        }
        for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
        {
            countryset.add(countries.name);
        }
        if(countryset.contains(usr.Contact.Account.country1__r.name) && checkcountry == false)
        {
            checkcountry = true;
        }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: usr.ContactId])
        {
            if(checkcountry == false && countryset.contains(contactacc.Account__r.country1__r.name))
            {
                checkcountry = true;
            }
        }
        
    }
    Public List<Task> Lsttaskids{get;set;}
    public PageReference GetTaskid()
    {
     
      Lsttaskids= [Select id,Subject from Task where id=:ApexPages.currentPage().getParameters().get('taskid') ];
      return null;
    }
    public PageReference saveCase()
    {
        System.debug('Inside saveCase----->>');
        Id recTypeId = [SELECT Id, DeveloperName FROM RecordType  WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        System.debug('Record type----->>'+recTypeId);
        if(usrContId != null && caseId == null)
        {
            newCase.ContactId = usrContId;
            newCase.AccountId = AcctName;//AccId;
            newCase.Subject = subject;
            newCase.Status = 'New';
            newCase.Origin = 'Web';
            //newCase.Owner = ;
            //newCase.Reason = 'Analysis';
            //newCase.Is_This_a_Complaint__c = 'No';
            newCase.Priority = 'Medium';//default
            system.debug('SelPriority :'+SelPriority );
            if(SelPriority <> 'None' && SelPriority <> null) {system.debug('SP:'+SelPriority );newCase.Priority= SelPriority;}
            newCase.RecordTypeId = recTypeId;
            
        }
        try  {
            inputfileshow = false;
            System.debug('New Case is----->>'+newCase.subject);
            if(ApexPages.currentPage().getParameters().get('Id') != NULL)
            {
                //newCase.id=ApexPages.currentPage().getParameters().get('Id');
                //newCase.Description = description;
                System.debug('Testing---->'+CommentInput);
                //newCase.Local_Description__c = '';
                /*if(product != NULL)
                {
                    newCase.Local_Description__c = product + ',';
                }
                if(prodVersion != NULL)
                {
                    newCase.Local_Description__c = newCase.Local_Description__c + prodVersion;
                }
                newCase.Local_Description__c = newCase.Local_Description__c + '\nCase Comment: ' + CommentInput;*/
                //newCase.Local_Subject__c = subject;
                
                //update newCase;
                if(CommentInput != null && CommentInput != '' && CommentInput.Length() > 0){
                    CaseComment newCaseComment = new CaseComment();
                    newCaseComment.ParentId = newCase.id;
                    newCaseComment.CommentBody = CommentInput;
                    Database.DMLOptions dmlo = new Database.DMLOptions();
                    dmlo.EmailHeader.triggerUserEmail = true;
                    Database.insert(newCaseComment,dmlo);

                    //insert newCaseComment;
                    system.debug('inside update---->>'+newCaseComment.Id + newCaseComment);
                }
            }
            else
            {
                List<SVMXC__Installed_Product__c> ipserialnumber = new List<SVMXC__Installed_Product__c>();
                system.debug('Serial Number-->' + SerialName + '$$$$Accounts' + RelatedAccountIds);
                ipserialnumber = [Select id,name from SVMXC__Installed_Product__c where Name =: SerialName and SVMXC__Company__c in: RelatedAccountIds];
                if(ipserialnumber.size()>0 )
                {
                    newCase.serial_number__c = SerialName;
                    newCase.ProductSystem__c = ipserialnumber[0].id;
                }
                newCase.Product_System__c = product;
                newCase.Product_Version_Com__c = prodVersion;
                if(product != NULL)
                {
                    newCase.Local_Description__c = product + ',';
                }
                if(prodVersion != NULL)
                {
                    newCase.Local_Description__c = newCase.Local_Description__c + prodVersion + ',';
                }
                if(SerialName != NULL)
                {
                    newCase.Local_Description__c = newCase.Local_Description__c + SerialName ;
                }
                newCase.Local_Description__c = newCase.Local_Description__c + '\n' + description;
                /*if(ipserialnumber.size()<=0)
                {
                    System.debug('Serial---->'+SerialName);
                    newCase.Local_Description__c = newCase.Local_Description__c + '\n Serial Number: ' + SerialName;
                     System.debug('Descriptions---->'+SerialName);
                 }*/
                newCase.Local_Subject__c = subject;
                newCase.Description = description;
                //newCase.Origin = 'Email';
                 // logic for assignment rules
                 //Fetching the assignment rules on case
                AssignmentRule AR = new AssignmentRule();
                AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
                //Creating the DMLOptions for "Assign using active assignment rules" checkbox
                Database.DMLOptions dmlOpts = new Database.DMLOptions();
                dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
                newCase.setOptions(dmlOpts);
                insert newCase;
                system.debug('inside insert---->>'+newCase.id);
            }
            if(Attachmntfile != null && Attachmntfile.body != null)
            {
                Attachmntfile.ParentId = newCase.id;
                insert Attachmntfile;
            }
            System.debug('New Case is----->>'+newCase.subject);
            Pagereference page = new pagereference('/apex/CpCaseDetail?id='+newCase.id + '@Details');
            page.setredirect(true);
            return page;//(new pagereference('/apex/CpCaseDetail?id='+newCase.id));
        }
        catch(Exception e)
        {
            system.debug('EE-->:'+e.getLinenumber()+'::'+e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please contact Helpdesk support for further assistance.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
    
    public pagereference cancelCreation()
    {
        pagereference pref;
        if(caseId == NULL)
        {
            pref = new pagereference('/apex/CpCasePage');
        }
        else
        {
            pref = new pagereference('/apex/CpCaseDetail?id='+ caseId + '@desc');
        }
        pref.setredirect(true);
        return pref;
        //return (new pagereference());
    }
    public pagereference backToCase()
    {
        pagereference pref;
        pref = new pagereference('/apex/CpCaseDetail?id='+ caseId + '@desc');
        pref.setredirect(true);
        return pref;
        //return (new pagereference());
    }
}
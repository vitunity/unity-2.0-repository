/*************************************************************************\
    @ Author        : Nikhil Verma
    @ Date      : 14-Nov-2014
    @ Description   :  Test class for CpNewCase class.
    @ Last Modified By :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/


@isTest(seeAllData=true)
Public class CpNewCaseTest
{
    
    public static testmethod void TestCpNewCase1()   
    {
        
        
        Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;      
        Account a;
        a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert a;       
        Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '124445');
        insert con;
            
        /*User u = new User(alias = 'standt', email='standardtestuse92@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', 
        languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
        username='standardtestuse92@testclass.com');*/
        user u = [select Id from User where profileId =: p.Id and isActive=true limit 1];
        Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        Case case1 = new Case(AccountId = a.Id, RecordTypeId = RecType, Priority = 'Medium',contactId = con.id);
        insert case1;
        
        List<Attachment> attchmentList = new List<Attachment>();
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body'); 
        Attachment att = new Attachment(Body = bodyBlob, Name = 'Test Attachment', ParentId = case1.Id);
        attchmentList.add(att);
        
        CaseComment caseComm = new CaseComment(commentBody = 'Test Comment', parentId = case1.Id, isPublished = true);
        insert caseComm;
        
        RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order__c' AND DeveloperName = 'Field_Service'].Id;
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(RecordTypeId = RecType, SVMXC__Case__c = case1.Id, SVMXC__Company__c = a.Id, SVMXC__Order_Status__c = 'Open', Subject__c = 'Test Subject');
        insert wo;
        
        Attachment att2 = new Attachment(Body = bodyBlob, Name = 'Test Attachment WO', ParentId = wo.Id);
        attchmentList.add(att2);
        Attachment att3 = new Attachment(Body = bodyBlob, Name = 'Test Attachment_FSR', ParentId = wo.Id);
        attchmentList.add(att3);
        insert attchmentList;
        
        EmailMessage t = new EmailMessage(subject = 'Test subject for task', TextBody = 'Test description for task', ParentID = case1.Id);
        insert t;
        
        task t1 = new task(subject = 'Test subject for task', Whatid = case1.Id);
        insert t1;

        List<EmailMessage> lstEmmailMessage = [select Id from EmailMessage limit 1];
        
        ApexPages.currentPage().getParameters().put('Id',case1.Id);
        ApexPages.currentPage().getParameters().put('Count','2');
        ApexPages.currentPage().getParameters().put('Workid',wo.Id);
        ApexPages.currentPage().getParameters().put('Taskid',null);
        
        CpNewCase inst = new CpNewCase();

        system.runas(u){  
            inst.CommentInput = 'Test Comment input';
            inst.getproductList();
            inst.getPriority();
            inst.getContMethList();
            inst.GetTaskid();
            
            inst.saveCase();
            inst.cancelCreation();
            inst.backToCase();
            
            
            
            CpNewCase.AssignComment wobj = new CpNewCase.AssignComment();
            
        }
        test.startTest();
        ApexPages.currentPage().getParameters().put('Id',null);
        ApexPages.currentPage().getParameters().put('Taskid',lstEmmailMessage[0].Id);
        inst = new CpNewCase();
        inst.saveCase();
        test.stopTest();
        
    }

 
}
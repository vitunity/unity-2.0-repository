/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 30-Jun-2013
    @ Description   : An Apex Class for popup when the user will acknowledge the Proof of notification .
****************************************************************************/
public class CpPONAckPopUp_Ext {
  Id ComplaintId;
  set<Id> accnt = new Set<Id>();
  //Constructor
  public void CpPONAckPopUp_Ext(){
      ComplaintId = ApexPages.currentPage().getParameters().get('Id');
      User UN = [Select u.ContactId from User u where id =: Userinfo.getUserId()];
      for(Consignee__c c:[Select Customer_Name__c from Consignee__c where Complaint_Number__c =: ComplaintId and Contact_Name_look__c =: UN.ContactId])
       accnt.add(c.Customer_Name__c);
       system.debug('-----Entered Constructor-----');
  }  
  
 /* public class WrapperclassforPONchk {
     public String AccId{get;set;}
     public Boolean PONRecieve{get;set;}
     public String AccName{get;set;}     
   
   public void WrapperclassforPONchk(String AccId,Boolean PONRecieve,String AccName){
       this.AccId = AccId;
       this.PONRecieve = PONRecieve;
       this.AccName = AccName;
    }
  }*/
  
  public List<PON__c> getPONList(){
   
     ComplaintId = ApexPages.currentPage().getParameters().get('Id');
      User UN = [Select u.ContactId from User u where id =: Userinfo.getUserId()];
      for(Consignee__c c:[Select Customer_Name__c from Consignee__c where Complaint_Number__c =: ComplaintId and Contact_Name_look__c =: UN.ContactId])
       accnt.add(c.Customer_Name__c);
      
     system.debug('-----Entered Constructor-----' + ComplaintId );
     //List<WrapperclassforPONchk> PONList = new List<WrapperclassforPONchk>();
     List<PON__c> ponlist = new List<PON__c>();
     Set<Id> accId = new Set<Id>();
     //ponlist = ;
     for(PON__c p: [Select PON_Received__c,Acknowledgement_Date__c,Id,Consignee__r.Customer_Name__r.Name,Consignee__r.Customer_Name__c,Consignee__r.Complaint_Number__c from PON__c where Customer_Name__c in: accnt and Consignee__r.Complaint_Number__c =:ComplaintId])
     {
       system.debug('----customer name----' + p.Consignee__r.Customer_Name__r.Name);
       if(!accId.contains(p.Consignee__r.Customer_Name__c))
       {
         ponlist.add(p);
         system.debug('ponlist----' + ponlist[0].Consignee__r.Customer_Name__r.Name);
       }
       accId.add(p.Consignee__r.Customer_Name__c);
     }
     return ponlist;
  }
 
 public pagereference acknowledge(){
  String accountId = apexpages.currentPage().getparameters().get('cmpltId');
  List<PON__c> updatelist = new List<PON__c>();
  for(PON__c p: [Select PON_Received__c,Acknowledgement_Date__c,Id,Consignee__r.Customer_Name__r.Name,Consignee__r.Customer_Name__c,Consignee__r.Complaint_Number__c from PON__c where Customer_Name__c =: accountId and Consignee__r.Complaint_Number__c =:ComplaintId])
  {
    PON__c ponrec = new PON__c(id = p.Id);
    ponrec.PON_Received__c = true;
    ponrec.Acknowledgement_Date__c = system.today();
    updatelist.add(ponrec);
  }
  update updatelist;
  return null;
 } 
}
@isTest
private class CpPONAckPopUp_ExtTest{
    static testMethod void CpPONAckPopUp_ExtTestMethod(){
        Test.StartTest();
        User thisUser = [ SELECT Id FROM User WHERE Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Complaint__c complaint1 = new Complaint__c(Name = '12345', PNL_Subject__c ='Test Complaints');
            insert complaint1;
            
            Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'); 
            insert a;  
            
            Contact con=new Contact(Lastname='User',RAQA_Contact__c=true,FirstName='Test',Email='testuser@testorg.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate'); 
            insert con;
            
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u = new User(alias = 'testU', Subscribed_Products__c=true,email='testuser@testorg.com',emailencodingkey='UTF-8', lastname='User',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='testuser16@testorg.com');
            insert u;
            
            
            ApexPages.currentPage().getParameters().put('Id' ,complaint1.Id);
            ApexPages.currentPage().getParameters().put('cmpltId' ,a.Id);
            
            Consignee__c consignee = new Consignee__c(Customer_Name__c = a.Id, Complaint_Number__c = complaint1.id, PCSN__c ='Test', Contact_Name_look__c = u.ContactId);
            insert consignee;
            
            PON__c pon1 = new PON__c(PON_Received__c = True, PCSN__c = 'Test PON', Customer_Name__c = con.AccountId, Acknowledgement_Date__c= Date.Today(), Consignee__c =consignee.Id);
            insert pon1;     
            
        }
        
        User u = [SELECT Id, Name, alias FROM User WHERE alias = 'testU' LIMIT 1];
        System.runAs ( u ) {
            CpPONAckPopUp_Ext inst = new CpPONAckPopUp_Ext();
            inst.CpPONAckPopUp_Ext();
            List<PON__c> ponList = inst.getPONList();
            pagereference pref = inst.acknowledge();
        }
        Test.StopTest();
   }
}
public class CpPeerToPeer
{
/*************************************************************************\
    @ Author        : Mohit Sharma
    @ Date          :  28-Apr-2013
    @ Description   : An Apex class to display all the Site Partner.
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
****************************************************************************/

    public list<account> lstact{get;set;}
    public List<innerCls> lstAccInner {get;set;}
    public Boolean isGuest{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
   

//***********constr*********************************

    public CpPeerToPeer()
    {
        shows='none';
        isGuest = SlideController.logInUser();
        lstact=new list<account>();
        lstAccInner =new  List<innerCls>();
        string ImgIconstr='';                       //Creating 0,1 for Peer to Peer
        string InstituteIcon='';                    //Creating TreatMent Icon-IMRT,IGT,RPIDARC
         List<recordtype> lstRecordType=[select id, name from recordtype where sobjectType='Account' and (name='Site Partner' or name = 'Sold To')];
        lstact=[select id,name,Website ,Peer_to_Peer__c,Longitude__c,Latitude__c,IMRT__c ,IGRT__c,X3DCRT__c,TrueBeam__c,RapidArc__c,SRS_SBRT__c,Paperless__c,Quality__c,RPM__c,BillingStreet,Phone,BillingState,Billingcity,BillingCountry,BillingPostalCode,SVMXC__Latitude__c, SVMXC__Longitude__c from Account where recordtypeid in : lstRecordType AND (Peer_to_Peer__c='Peer to peer customer' OR Peer_to_Peer__c='Clinical school')];
        for(account acc: lstact)
        { 
            InstituteIcon='';

            string imgFinalItem='';
            if(acc.IMRT__c ==true){InstituteIcon='IMRT,'; }
            
            if(acc.RPM__c ==true){
            InstituteIcon =InstituteIcon+'RPM(Gating),'; }
            if(acc.IGRT__c ==true){InstituteIcon =InstituteIcon+'IGRT,';}
            if(acc.RapidArc__c ==true){InstituteIcon =InstituteIcon+'RapidArc,';}
            if(acc.Quality__c ==true){ InstituteIcon =InstituteIcon+'Quality,'; }
            if(acc.Paperless__c ==true){InstituteIcon =InstituteIcon+'Paperless,';}
            
            if(acc.SRS_SBRT__c ==true){ InstituteIcon =InstituteIcon+'SRS/SBRT,'; }
            
            //Added by Shital Bhujbal    2 June 2016    task - INC3566866
            if(acc.X3DCRT__c ==true){InstituteIcon =InstituteIcon+'3DCRT,';}
            if(acc.TrueBeam__c ==true){InstituteIcon =InstituteIcon+'TrueBeam,';}
            
            if(InstituteIcon!='' ){InstituteIcon=InstituteIcon.substring(0,InstituteIcon.length()-1);}
            
            else{InstituteIcon='';}


            if(acc.Peer_to_Peer__c== 'Peer to peer customer')
            { 
             ImgIconstr='1';
            }
            else
            {
            ImgIconstr='0';
            }
            String str = acc.BillingStreet;
            if(String.isNotBlank(str) && str.contains('\''))
            {
                str = String.escapeSingleQuotes(str);
            }
            acc.BillingStreet = str;
            lstAccInner.add(new innerCls(acc,ImgIconstr,InstituteIcon)); 
        }
         if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }     
    }

    
//***************************Inner Class******************************
    
 public class innerCls                                           //Create Inner Class
 {      public string strImgIcon{get;set;}                       //Propert for Image to be display on Google Map for Peer to Peer Or Clinical School
        public Integer count{set;get;}                           //Property For account
        public Account ack {set;get;}
        public string strtreatment{get;set;}                      //Property for Treatment Like IMRT,IGT,Paperless
        public innerCls(Account acc,string strImgICN,string treatment)    //Constructor
        {
                ack=new account();
                strImgIcon='';
                strtreatment='';
                
                this.ack = acc;
                if(this.ack.Latitude__c != null && this.ack.Latitude__c.Contains('°')){
                    this.ack.Latitude__c = this.ack.Latitude__c.split('°')[0];
                }
                
                if(this.ack.Longitude__c !=null && this.ack.Longitude__c.Contains('°')){
                    this.ack.Longitude__c = this.ack.Longitude__c.split('°')[0];
                }
                
                if(this.ack.Website != null && !this.ack.Website.contains('http')){
                    this.ack.Website = 'http://' + this.ack.Website;
                }
                strImgIcon=strImgICN;
                strtreatment=treatment;
        }
    }
}
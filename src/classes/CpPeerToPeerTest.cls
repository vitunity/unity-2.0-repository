/*************************************************************************\
    @ Author        : Mohit Sharma
    @ Date      : 17-June-2013
    @ Description   :  Test class for CpPeerToPeer class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(SeeAllData = true)
Public class CpPeerToPeerTest
{

 //Test Method for initiating  CpPeerToPeer class
 
 static testmethod void testCpPeerToPeer() 
 { 
List<recordtype> lstRecordType=[select id, name from recordtype where sobjectType='Account' and name='Site Partner' limit 1];
Account act=new account( name='test',  Website='www.test.com',Peer_to_Peer__c='Peer to peer customer',Longitude__c='127.12',Latitude__c='65.5',
                        IMRT__c=true,IGRT__c=true,RapidArc__c=true,SRS_SBRT__c=true,Paperless__c=true,Quality__c=true,
                        RPM__c=true,BillingStreet='test',Phone='901234556',BillingState='test',Billingcity='test',
                        BillingCountry='test',BillingPostalCode='test',Country__c='Test' ,
                        recordtypeid=lstRecordType[0].id);
Account act1=new account( name='test',  Website='www.test.com',Peer_to_Peer__c='Peer to peer customer',Longitude__c='127.12',Latitude__c='67.12',
                        IMRT__c=true,IGRT__c=true,RapidArc__c=true,SRS_SBRT__c=true,Paperless__c=true,Quality__c=true,
                        RPM__c=true,BillingStreet='test',Phone='901234556',BillingState='test',Billingcity='test',
                        BillingCountry='test',BillingPostalCode='test',Country__c='Test' ,
                        recordtypeid=lstRecordType[0].id);
                        
Account act2=new account( name='test',  Website='www.test.com',Peer_to_Peer__c='Clinical school',Longitude__c='89.98',Latitude__c='12.12',
                        IMRT__c=true,IGRT__c=true,RapidArc__c=true,SRS_SBRT__c=true,Paperless__c=true,Quality__c=true,
                        RPM__c=true,BillingStreet='test',Phone='901234556',BillingState='test',Billingcity='test',
                        BillingCountry='test',BillingPostalCode='test',Country__c='Test' ,
                        recordtypeid=lstRecordType[0].id);
                        
   CpPeerToPeer clsCpPeerToPeer= new CpPeerToPeer();
   
    
}
}
/*************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 05-May-2013
    @ Description   : An APex class to get List of presentations .
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public with sharing class CpPresentationController {

    public String getIsGuest() {
        return null;
    }
   private integer counter=0;  //keeps track of the offset
   private integer list_size=10; //sets the page size or number of rows
   public integer totalPages {get;set;} 
    public integer total_size {get;set;}
   private String soql {get;set;}
   public Id PresentationId;
   //public List<Presentation__c> Presentation_List{get;set;}
   public List<String> Speaker{get;set;}
   Public List <String>Products{get;set;}  
   Public Integer size{get;set;}
   Public Integer noOfRecords{get; set;}
   public boolean flag{get;set;}
   Public List<Presentation__c> Presentationlist=new List<Presentation__c>();
   set<String> productGrp = new set<String>();
    
    string chkRegion;
    public string Usrname{get;set;}
    public string shows{get;set;}
    public boolean isGuest{get;set;}
  
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;  }
    set;
  }

  public String sortField {
    get  { if (sortField == null) {sortField = 'Date__c'; } return sortField;  }
    set;
  }

  public CpPresentationController () {
    
    shows='none';
    String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
    if(conCntry != null && conCntry !='' ){
        chkRegion = [Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry].Portal_Regions__c;
    }
    flag=true;
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null){
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
                for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
                    productGrp.add(picklistvalues.getlabel());
                }
            }       
        }
        else{
       //  Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
       //         for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
       //             productGrp.add(picklistvalues.getlabel());

           // productGrp.addAll(CpProductPermissions.fetchProductGroup()); 
          // }     
        }   
        system.debug('prod group'+productGrp);
    Speaker = new list<String>();
    Products=new list<string>();
    /*  if(productGrp.size() > 0){
        String S = '(';
        integer i = 1;
        for(String st : productGrp){
            if(i == productGrp.size()){   
                S = S + '\'' + st + '\')';          
                
            }else{
                S = S + '\''+ st + '\',' ;  
            }
            i++;
        }
        soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c from Presentation__c where Product_Affiliation__c in '+S;
        system.debug('query is'+soql );
    
    }else{*/
        soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Region__c from Presentation__c where Published__c=true ';
  //  }
    runQuery1();
    //runSearch();
   // con = new ApexPages.StandardSetController(Presentation_List);
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }  
    
  }
 /*public List<String> Speakers{
    get {
    if (Speakers== null) {
 Speakers= new List<String>(); 
        soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Region__c from Presentation__c where published__c=true ';
        runQuery();
    for(Presentation__c p : (List<Presentation__c>)con.getRecords())
        {
         set<String> SpeakerUnique=new set<String> ();
          
                  Speakers.add(p.Speaker__c);
                  Speakers.sort();
              
        }}
   return Speakers;
    }
   
     set;
   
    }*/
    public void runQuery1() {
      System.debug('aebug : query = '+soql);
      List<Presentation__c> lstPresentation_List= Database.query(soql + ' order by ' + sortField + ' ' + sortDir);
      system.debug('--------'+lstPresentation_List.size());
      
      List<Presentation__c> presentationList = new List<Presentation__c>();
      system.debug('prod group3'+productGrp);
      set<String> SpeakerUnique=new set<String> ();
      if(lstPresentation_List.size() > 0){              
              for(Presentation__c p : lstPresentation_List){
              if(!SpeakerUnique.contains(p.Speaker__c))
             {
                  
                  Speaker.add(p.Speaker__c);
                  Speaker.sort();
                  SpeakerUnique.add(p.Speaker__c);
                  System.debug('Test00000--->'+SpeakerUnique);
                 }
                else
               {
                  SpeakerUnique.add(p.Speaker__c);
                  System.debug('Test00001--->'+SpeakerUnique);
               } 
               
   
                
                    presentationList.add(p);
                  
                 /*   for(String s: productGrp){
                        if(p.Product_Affiliation__c != null && p.Product_Affiliation__c.contains(s)){
                            presentationList.add(p);
                            break;
                        }
                        
                    }*/
              }
              System.debug('YYYYYY---->'+Speaker);
                              
          } 
          system.debug('test1---->'+presentationList.size());  
           total_size=presentationList.size();        
          con = new ApexPages.StandardSetController(presentationList);
    
 
  }
  public void runQuery() {
      System.debug('aebug : query = '+soql);
      List<Presentation__c> lstPresentation_List= Database.query(soql + ' order by ' + sortField + ' ' + sortDir);
      system.debug('--------'+lstPresentation_List.size());
      
      List<Presentation__c> presentationList = new List<Presentation__c>();
      system.debug('prod group3'+productGrp);
     
      if(lstPresentation_List.size() > 0){              
              for(Presentation__c p : lstPresentation_List){ 
               
                    presentationList.add(p);
                  
                 /*   for(String s: productGrp){
                        if(p.Product_Affiliation__c != null && p.Product_Affiliation__c.contains(s)){
                            presentationList.add(p);
                            break;
                        }
                        
                    }*/
              }
              System.debug('YYYYYY---->'+Speaker);
                              
          } 
          system.debug('test1---->'+presentationList.size());  
           total_size=presentationList.size();        
          con = new ApexPages.StandardSetController(presentationList);
    
 
  }
  
  Public List<Presentation__c> getPresentation_List(){
        List<Presentation__c> pList = new List<Presentation__c>(); 
        system.debug('testssssss');       
        
        for(Presentation__c p : (List<Presentation__c>)con.getRecords())
        {
            if(p.Region__c != null && chkRegion!=null ){
            
            if(p.Region__c.contains('All') || p.Region__c.contains(chkRegion))
            system.debug('test@@@'+p.Product_Affiliation__c);       
            Product.add(p.Product_Affiliation__c);
            System.debug('produtdrop'+Product);
           // Speaker.add(p.Speaker__c);
            pList.add(p);} 
            else
            {
            if(p.Region__c.contains('All')){
            system.debug('test@@@'+p.Product_Affiliation__c);       
            Product.add(p.Product_Affiliation__c);
            System.debug('produtdrop'+Product);
           // Speaker.add(p.Speaker__c);
            pList.add(p);} 
            }
            
           
        }
        system.debug('test2---->'+pList.size()); 
         if (math.mod(total_size , list_size) > 0) {
         totalPages =total_size /list_size + 1;
         }
        else
        {
       totalPages = total_size /list_size;
        }
       // totalPages= totalPages - math.mod(totalPages, list_size);
         
        return pList;
    }
 
  
  public PageReference runSearch() {
 
    
    String product=Apexpages.currentPage().getParameters().get('product');
    String speaker=Apexpages.currentPage().getParameters().get('speaker');
    
    if(product==null)
    product='-Any-';
    if(speaker==null)
    speaker='-Any-';
    
    system.debug('&&&&'+product);
    system.debug('^^^^'+speaker);
    
    soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c,Region__c from Presentation__c where Date__c!=Null and Published__c=true';
    
      if(product=='-Any-' && speaker=='-Any-')
      {
       system.debug('prod group1'+productGrp);
       soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Region__c from Presentation__c where Published__c=true ';
       // soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c,Region__c from Presentation__c ';
 
      }

     if(product!='-Any-' && speaker!='-Any-'){
        flag=false;
        if (!product.equals(''))
            {
              system.debug('product');
              soql += ' and Product_Affiliation__c includes (\''+product+'\')';
            }  
                if (!speaker.equals(''))
            {
              system.debug('speaker');
              soql += ' and Speaker__c LIKE \''+String.escapeSingleQuotes(speaker)+'%\'';
            }  
           
        }

        else if(product!='-Any-'){
        flag=false;
        if (!product.equals(''))
            {
              system.debug('product');
              soql += ' and Product_Affiliation__c includes (\''+product+'\')';
            }
        }

        else if(speaker!='-Any-'){
        flag=false;
         if (!speaker.equals(''))
            {
              system.debug('speaker');
              soql += ' and Speaker__c LIKE \''+String.escapeSingleQuotes(speaker)+'%\'';
            }  
        }
      
   
   system.debug('query is'+soql);
   
 
    runQuery();
   // con = new ApexPages.StandardSetController(Database.getQueryLocator(soql + ' order by ' + sortField + ' ' + sortDir));
  //system.debug('$$$$$$$$$$'+Presentation_List);
    return null;
  }
 
  // use apex describe to populate the picklist values
  public List<String> Product{
    get {
      if (Product== null) {
 Product= new List<String>(); 
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null){
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
                for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
                    Product.add(picklistvalues.getlabel());
                }
            }       
        }
        else{
        System.debug('test^^^^'); 
        soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Region__c from Presentation__c where published__c=true ';
        runQuery();
         Set<String> selectedItems = new Set<String>();           

        for(Presentation__c p : (List<Presentation__c>)con.getRecords())
        {
      
            if(p.Region__c != null){
            if(p.Region__c.contains('All') || p.Region__c.contains(chkRegion))
            system.debug('test@@@1'+p.Product_Affiliation__c); 
            if(p.Product_Affiliation__c!=null)
                                            {
            if(p.Product_Affiliation__c.contains(',') || p.Product_Affiliation__c.contains(';') )
                                            {
                                            List<String> parts = p.Product_Affiliation__c.split(';');
                                            System.debug('Prodsss-->'+parts );
                                            for(String temp:parts)
                                            {
                                             System.debug('testsssss-->'+selectedItems);
                                            System.debug('testsssss-->'+temp);
                                           if (selectedItems.contains(temp))
                                           {
                                           }
                                            else
                                           {
                                            Product.add(temp);
                                             selectedItems.add(temp);                 
                                           }
                                           }
                                           }
                                            else
                                            {
                                            if(selectedItems.contains(p.Product_Affiliation__c))
                                            {
                                            }
                                            else
                                            {
                                            Product.add(p.Product_Affiliation__c);
                                            selectedItems.add(p.Product_Affiliation__c);
                                            }
                                            }
      }
           // Product.add(p.Product_Affiliation__c);
            System.debug('produtdrop1'+Product);
           // Speaker.add(p.Speaker__c);
            }
            
        }
        System.debug('productsssinfo'+Product);
          //  soql = 'select Id,Date__c,Title__c,Speaker__c,Product_Affiliation__c,Institution__c,Region__c from Presentation__c where Date__c!=Null';
     // runQuery();

      //   Schema.Describefieldresult DocTypesDesc = Product2.Product_Group__c.getDescribe();
         //       for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
         //           Product.add(picklistvalues.getlabel());
         //       }
           // Product.addAll(CpProductPermissions.fetchProductGroup());
        }
        Product.sort();         
        
        /*
        Schema.DescribeFieldResult field = Presentation__c.Product_Affiliation__c.getDescribe();
 
        for (Schema.PicklistEntry f : field.getPicklistValues())
          Product.add(f.getLabel());
        */
      }
      return Product;          
    }
    set;
  }
    public Pagereference redirecttodetailpage()
    {
        PresentationId=System.currentPageReference().getParameters().get('abc');
        PageReference newPage = new PageReference('/apex/CpPresentationDetailpage?Id='+PresentationId);
        newPage.setRedirect(true);
        return newPage;
    
    }
    
    //pagination
    // Initialize setCon and return a list of records
    public List<Presentation__c> getonDemandWebinars() {
        //system.debug('aebug ; getonDemandWebinars == '+ (List<Event_Webinar__c>) con.getRecords());
        //system.debug('aebug ; ondemand == '+ onDemandWebinars.size());
        return (List<Presentation__c>) con.getRecords();
    }
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
        size = 10;
        con.setPageSize(size);
        noOfRecords = con.getResultSize();
        return con;
        }
        set;
    }
    
        // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }   
   /*  public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
    public Integer getTotal_size() {
      return total_size;
   } */ 
    // returns the first page of records
    public void first() {
        con.first();
    }
 
    // returns the last page of records
    public void last() {
       counter = totalPages  - math.mod(totalPages , list_size);
        con.last();
    }
 
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    // returns the next page of records
    public void next() {
        //pageNumber = pageNumber + 1;      
        con.next();
    }
    
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
    }
    
    

}
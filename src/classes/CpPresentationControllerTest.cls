/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CpPresentationController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CpPresentationControllerTest{

    //Test Method for initiating CpPresentationController class
    
    static testmethod void testCpPresentationController(){
        Test.StartTest();
        
         User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
         System.runAs ( thisUser ) {
         
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a;   
                 
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, MailingCountry='Australia', MailingState='teststate' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                
                Product2 prod = new Product2();
                prod.Name = 'Acuity';
                prod.Product_Group__c ='Acuity';
                
                insert prod;
                
                 SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
                 Insprd.Name = 'Testing';
                 Insprd.SVMXC__Product__c = prod.Id;
                 Insprd.SVMXC__Serial_Lot_Number__c = 'Testing';
                 Insprd.SVMXC__Status__c = 'Installed';
                 Insprd.SVMXC__Company__c = con.AccountId;
                 
                 Insert Insprd;
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Australia';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
        }
        
        
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
        
            PageReference pageRef = Page.Cp_Presentation; 
            Test.setCurrentPage(pageref);
            
            Presentation__c prese = new Presentation__c();
            prese.Title__c = 'test_09_may';
            prese.Speaker__c = 'Test Speaker';
            prese.Product_Affiliation__c = '4DITC';
            prese.Date__c= System.Today();
            prese.Region__c= 'Test,NA';
            prese.Published__c=true;
            Insert prese; 
            
            ApexPages.currentPage().getParameters().put('product' , prese.product_affiliation__c);
            ApexPages.currentPage().getParameters().put('speaker' ,prese.Speaker__c);
            
            CpPresentationController  Presentation = new CpPresentationController();
            Presentation.redirecttodetailpage();
            List<Presentation__c> pres = Presentation.getonDemandWebinars();
            List<Presentation__c> presnta = Presentation.getPresentation_List();
            
            Presentation.HasNext = true;
            Presentation.hasPrevious = true;
            Presentation.pageNumber = 1;
            Presentation.first();
            Presentation.last();
            Presentation.previous();
            Presentation.next();
            Presentation.cancel();
            Presentation.runSearch();
            Presentation.getIsGuest();
            Presentation.runQuery();
           
            }
             Test.StopTest();
         }
     
     //Test Method for initiating CpPresentationController class runserach method
          
     
     static testmethod void testCpPresentationController1(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx' , Country__c='testCountry'); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Australia', MailingState='teststate' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            
             Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
            
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = 'Testing';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
                 
            Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Australia';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
        
        }
        
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
        
            
            CpPresentationController  Presentation = new CpPresentationController();
            Presentation.runSearch();
            Presentation.hasNext = True;
            
            }
             Test.StopTest();
         }
         
         
         //Test Method for initiating CpPresentationController class when only Product is selected
         
         static testmethod void testCpPresentationControllerproduct(){
            Test.StartTest();
            
             User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a; 
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='testCountry'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Australia', MailingState='teststate' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                 Product2 prod = new Product2();
                prod.Name = 'Acuity';
                prod.Product_Group__c ='Acuity';
                
                insert prod;
                
                 SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
                 Insprd.Name = 'Testing';
                 Insprd.SVMXC__Product__c = prod.Id;
                 Insprd.SVMXC__Serial_Lot_Number__c = 'Testing';
                 Insprd.SVMXC__Status__c = 'Installed';
                 Insprd.SVMXC__Company__c = con.AccountId;
                 
                 Insert Insprd;
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Australia';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
        
        }
           user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
          System.runAs (usr){
        
            PageReference pageRef = Page.Cp_Presentation; 
            Test.setCurrentPage(pageref);
            
            Presentation__c prese = new Presentation__c();
            prese.Title__c = 'test_09_may';
            prese.Product_Affiliation__c = '4DITC';
            prese.Date__c= System.Today();
            prese.Region__c= 'Test,NA';
            Insert prese; 
            
            ApexPages.currentPage().getParameters().put('product' , prese.product_affiliation__c);
            CpPresentationController  Presentation = new CpPresentationController();
            Presentation.runSearch();
            Presentation.hasNext = True;
            
            
            
            }
             Test.StopTest();
         }
          
          //Test Method for initiating CpPresentationController class when only Speaker is selected
          
          static testmethod void testCpPresentationControllerspeaker(){
            Test.StartTest();
            
             User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a;
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx', Country__c='testcountry' ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry='Australia', MailingState='teststate' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@testclass.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                 Product2 prod = new Product2();
                prod.Name = 'Acuity';
                prod.Product_Group__c ='Acuity';
                
                insert prod;
                
                 SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
                 Insprd.Name = 'Testing';
                 Insprd.SVMXC__Product__c = prod.Id;
                 Insprd.SVMXC__Serial_Lot_Number__c = 'Testing';
                 Insprd.SVMXC__Status__c = 'Installed';
                 Insprd.SVMXC__Company__c = con.AccountId;
                 
                 Insert Insprd;
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Australia';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
            
            }
            
            user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
            System.runAs (usr){
        
            PageReference pageRef = Page.Cp_Presentation; 
            Test.setCurrentPage(pageref);
            
            Presentation__c prese = new Presentation__c();
            prese.Title__c = 'test_09_may';
            prese.Speaker__c = 'Test Speaker';
            prese.Date__c= System.Today();
            prese.Region__c= 'Test,NA';
            prese.Published__c=true;
            Insert prese; 
            
            ApexPages.currentPage().getParameters().put('speaker' ,prese.Speaker__c);
            CpPresentationController  Presentation = new CpPresentationController();
            Presentation.runQuery();
            Presentation.runSearch();
            
            Presentation.Product =new List<String>();
            Presentation.Product.add('Acuity');
            Presentation.pageNumber = 1;
             
            }
             Test.StopTest();
         }
 }
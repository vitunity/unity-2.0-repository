/*************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 07-May-2013
    @ Description   : An Apex class to display the summay of a selected Presentation.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class CpPresentationDetailpageController {
    public Id PresentationId{get;set;}
    Public List<Presentation__c> pre{get;set;}
    Public Transient List<Attachment > Attachment {get;set;}
   public List<tstClass> tstData{get;set;}

    public string Usrname{get;set;}
    public boolean Isaudio{get; set;}
    public string shows{get;set;}
    public boolean isGuest{get;set;}
    public String getPre() {
        return null;
    }
 Public Class tstClass

 {

 public  List<Attachment >  col{get;set;}

 public List<Presentation__c> rec{get;set;}

    Public tstClass( List<Attachment > con ,List<Presentation__c> rcpt)

    {

    col = con;

    rec = rcpt;

    }

   }

    public CpPresentationDetailpageController() {
    tstData = New List<tstClass>();

        shows='none';
        PresentationId=ApexPages.currentPage().getParameters().get('Id');
        pre= new List<Presentation__c>();
        attachment = new List<Attachment>();
        pre=[select Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Presentation_Recording_URL__c, Description__c,Event__r.Title__c,Date__c from Presentation__c where id=:PresentationId];
        if(pre.size()>0)
     {
     System.debug('Presentation--->'+ pre[0].Presentation_Recording_URL__c);
     if(pre[0].Presentation_Recording_URL__c!=null)
     {
     Isaudio=true;
     }
     else
     {
     Isaudio=false;
     }}
        attachment =[select Body,ContentType,Description,Name,OwnerId,BodyLength from Attachment where Parentid=:PresentationId];
         if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        } 
       List<Attachment > col=[select Body,ContentType,Description,Name,OwnerId,BodyLength from Attachment where Parentid=:PresentationId];
       List<Presentation__c> rec=[select Title__c,Speaker__c,Product_Affiliation__c,Institution__c, Presentation_Recording_URL__c, Description__c,Event__r.Title__c,Date__c from Presentation__c where id=:PresentationId];
       tstData.add(New tstClass(col,rec));

    }


   
   
   
}
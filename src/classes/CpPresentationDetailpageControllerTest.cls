/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CpPresentationDetailpageController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest 

Public class CpPresentationDetailpageControllerTest{

    //Test Method for initiating CpPresentationDetailpageController class
    
    static testmethod void testPresentationDetailpageController(){
        Test.StartTest();
        CpPresentationDetailpageController PresentationControllerDetail = new CpPresentationDetailpageController();
        PresentationControllerDetail.getPre();
        
                Test.StopTest();
    }
}
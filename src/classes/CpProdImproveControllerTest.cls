/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 13-June-2013
    @ Description   :  Test class for CpProdImproveController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(seeAllData = True)
Public class CpProdImproveControllerTest
{
  
  //Test Method for initiating  CpProdImproveController class
  
    Public static testmethod void testMessageDetailController()
    {
       
       
        ApexPages.currentPage().getParameters().put('Request' , 'HelpTicket');
        CpProdImproveController prodimpr=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'RequestforUpgradesQuote');
        CpProdImproveController pr=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'RegistrationandLoginQuestion');
        CpProdImproveController pro=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'ProductAccessUpdate');
        CpProdImproveController prov=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'FileDownloadIssues');
        CpProdImproveController prove=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'Webinars');
        CpProdImproveController product=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'WebsiteFeedback');
        CpProdImproveController prd=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'CertificateManager');
        CpProdImproveController prim=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'LearningCenter');
        CpProdImproveController improve=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'PaperDocumentation');
        CpProdImproveController pd=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'UpdatePI');
        CpProdImproveController PUPI=new CpProdImproveController();
        ApexPages.currentPage().getParameters().put('Request' , 'ProductImprovement');
        CpProdImproveController p=new CpProdImproveController();
        
        
        Language__c lang=new Language__c();
        lang.Name='English';
        lang.Language_ID__c=2.0;
        insert lang;
        system.assertequals( lang.Name,'English'); 
        system.assertequals(  lang.Language_ID__c,2.0);
        pd.Productvalue='ARIA Medical Oncology';
        pd.Languagevalue='English';
             
        //Commented by Shital Bhujbal to pass the test       
        /*ContentWorkspaceDoc cwspace=[Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc where ContentWorkspace.Name ='ARIA Medical Oncology'  LIMIT 1];
        //ContentWorkspaceDoc cwspace=[Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc where ContentWorkspace.Name ='ARIA Radiation Oncology'  LIMIT 1];
        ContentVersion cont= [SELECT Id FROM ContentVersion WHERE ContentDocumentId=:cwspace.ContentDocumentId and Document_Language__c='English'  and IsLatest = true];
        //   ApexPages.currentPage().getParameters().put('passedParam',cont.Id);
        pd.passedParam=cont.Id;
        pd.getlContentVersions();
      
        pd.CustEmail='test@gmail.com';
        Apexpages.currentPage().getParameters().put('Radioid',cont.Id);
        pd.methodOne();
        // pd.verify();
        pd.reset();
        */    
        
        CpProdImproveController prodimprov=new CpProdImproveController();
        List<SelectOption> sopt =  prodimprov.getoptionscountry();
        List<SelectOption> sopt1 =  prodimprov.getoptions();
        List<SelectOption> sopt2 =  prodimprov.getoptionsLanguage();
        List<SelectOption> sopt3=prodimprov.getRoles();
        List<SelectOption> sopt4=prodimprov.getSpecialty();
        prodimprov.getnewatt();
        Attachment  a = new Attachment();
        prodimprov.setnewatt(a);
        
        //Rakesh
        prodimprov.getSalutation();
        prodimprov.getPreferredLanguage();
        //prodimprov.methodOne();

        //ContVerWrap = new ContVerWrap(Contentversion);

        prodimprov.getlContentVersions();
        string s = prodimprov.sortField;
        prodimprov.End();
        prodimprov.getDisableNext();
        prodimprov.getTotalPages();
        //prodimprov.verify();
        prodimprov.firsttimelogin();
        //Rakesh End
        
        
        ApexPages.currentPage().getParameters().put('Request' , 'ProductImprovement');
        CpProdImproveController propr=new CpProdImproveController();
        
        ProdCountEmailMapping__c prodmap=new ProdCountEmailMapping__c();
        prodmap.email__c ='test@gmail.com';
        prodmap.Name='ProductImprovement';
        prodmap.ProductGroup__c='';
        insert  prodmap;
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
        propr.submit();
         propr.resetpassword();
        
        ApexPages.currentPage().getParameters().put('Request' , 'RequestforUpgradesQuote');
        CpProdImproveController propr2=new CpProdImproveController();
        
        propr2.newatt.body=Blob.valueof('abc');
        propr2.newatt.name='test';
        propr2.CustEmail='test@tes2t.com';
        List<String> toAddresses2 = new List<String>();
        toAddresses2.add('Test@gmail.com');
        toAddresses2.add('Test@gmail.com'); 
        
        propr2.EmailBodysetup(toAddresses2);
        
        ApexPages.currentPage().getParameters().put('Request' , 'Webinars');
        CpProdImproveController propr3=new CpProdImproveController();
        
        propr3.newatt.body=Blob.valueof('abc');
        propr3.newatt.name='test';
        propr3.CustEmail='test@tes2t.com';
        List<String> toAddresses3 = new List<String>();
        toAddresses3.add('Test@gmail.com');
        toAddresses3.add('Test@gmail.com'); 
        
        propr3.EmailBodysetup(toAddresses3);
        //propr3.documentlanguage();
        propr3.updatecon1();
        propr3.DeletPic();
        propr3.randomNumber();
        propr3.input='135';
        propr3.getChar1();
        propr3.getChar2();
        propr3.getChar3();
        propr3.getChar4();
        propr3.getChar5();
        propr3.getChar6();
        propr3.Beginning();
        propr3.Previous();
        propr3.Next();
        propr3.toggleSort();
        propr3.getDisablePrevious();
        
        propr3.getTotal_size();
        propr3.getPageNumber();
        propr3.FakeRender();
        propr3.getDocLanguages();
    }
        
    
    //Test Method for initiating  CpProdImproveController class when Request is RegistrationandLoginQuestion
    
    Public static testmethod void testMessageDetailController1()
    {
        User urs = [ select Id from User where Id =:UserInfo.getUserId() ];    
        System.runAs (urs)    
        {          
            
            User u;   
            Account a;        
           // Profile po = [select id from profile where name=: Label.VMS_Customer_Portal_User];    
              Profile po = [select id from profile where name='VMS MyVarian - Customer User'];
            Account objAcc= new Account(name='testMarchrelease122',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test',Legal_Name__c ='Testing'); 
            insert objAcc; 
            
            a = new Account(name='newtestMarchrelease122',VMS_Factory_Tours_Attended__c=223.373,AKA__c='Defg',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4556',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223456',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='wxyz',Country__c='Test',Legal_Name__c ='MyTest'  ); 
            insert a; 
            Contact con=new Contact(Lastname='testuser',FirstName='Singh',Email='kumartest.marchrel12@gmail.com', AccountId=a.Id,MailingCountry ='USA', MailingState='sampletestState',OktaId__c='asad121309'); 
            insert con;
            //Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com',MailingCountry ='USA',phone='3434343',Title='testtit',MailingStreet='xyx', MailingState='xyz', MailingPostalCode='2323232', MailingCity='Noida');   
            u = new User(alias = 'standt', email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = po.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,username='standarduser@testclass.com'); 
            insert u;
            Test.starttest();
            // User urs = [ select ContactId, Profile.Name  from User where Id =:UserInfo.getUserId() ];    
            System.runAs (u)
            {
                CpProdImproveController pr=new CpProdImproveController();
                pr.submit();
                pr.updatecon();
                 pr.CustFname='a';
                pr.CustFname1='b';
                pr.strCustEmail='test@gmail.com';
                pr.CustEmail='test1@gmail.com';
                pr.AcctName='ss';
                pr.AcctName1='tt';
                pr.updatecon1();
                pr.resetpassword();
            }
            
         /*   ApexPages.currentPage().getParameters().put('Request' , 'RegistrationandLoginQuestion');
            
            
            CpProdImproveController propr4=new CpProdImproveController();
            
            propr4.newatt.body=Blob.valueof('abc');
            propr4.newatt.name='test';
            propr4.CustEmail='test@tes2t.com';
            List<String> toAddresses4 = new List<String>();
            toAddresses4.add('Test@gmail.com');
            toAddresses4.add('Test@gmail.com'); 
            
            propr4.EmailBodysetup(toAddresses4);
            propr4.documentlanguage();
           
            propr4.updatecon1();*/
            
        test.stoptest();
        }
        
     

}
 Public static testmethod void testMessageDetailControllerprod(){
    ApexPages.currentPage().getParameters().put('Request' , 'RegistrationandLoginQuestion');
            
            
            CpProdImproveController propr4=new CpProdImproveController();
            
            propr4.newatt.body=Blob.valueof('abc');
            propr4.newatt.name='test';
            propr4.CustEmail='test@tes2t.com';
            List<String> toAddresses4 = new List<String>();
            toAddresses4.add('Test@gmail.com');
            toAddresses4.add('Test@gmail.com'); 
            
            propr4.EmailBodysetup(toAddresses4);
            //propr4.documentlanguage();
           
            propr4.updatecon1();
 }
//Test Method for initiating  CpProdImproveController class when Request is Serviceticket

    Public static testmethod void testMessageDetailController2()
      {
        Regulatory_Country__c regcount=new Regulatory_Country__c();
        regcount.Name='India';
        regcount.SupportRegion__c='EMEA';
        Insert regcount;
        
        //ApexPages.currentPage().getParameters().put('Request' , 'Serviceticket');
        CpProdImproveController pr=new CpProdImproveController();
        pr.setoptionscountry();
        PageReference pageRefrence = Page.CpContactus; 
        Test.setCurrentPage(pageRefrence );
        
        ProdCountEmailMapping__c prodmap=new ProdCountEmailMapping__c();
        prodmap.email__c ='test@gmail.com';
        prodmap.Name='test';
        prodmap.ProductGroup__c='4DITC';
        insert  prodmap;
        
        system.assertequals( prodmap.email__c , 'test@gmail.com');    
        system.assertequals( prodmap.Name,'test');     
        system.assertequals( prodmap.ProductGroup__c,'4DITC');     
        pr.submit();
        pr.resetpassword();
        
        
        ApexPages.currentPage().getParameters().put('Request' , 'Serviceticket');
        CpProdImproveController propr1=new CpProdImproveController();
        
        
        ProdCountEmailMapping__c prodmap1=new ProdCountEmailMapping__c();
        prodmap1.email__c ='test@gmail.com';
        prodmap1.Name='Serviceticket';
        prodmap1.ProductGroup__c='';
        insert  prodmap1;
        
        propr1.newatt.body=Blob.valueof('abc');
        propr1.newatt.name='test';
        propr1.CustEmail='test@tes2t.com';
        List<String> toAddresses1 = new List<String>();
        toAddresses1.add('Test@gmail.com');
        toAddresses1.add('Test@gmail.com'); 
        
        propr1.EmailBodysetup(toAddresses1);
        propr1.submit();
         propr1.resetpassword();
        
    
    }

//Test Method for initiating  CpProdImproveController class when Request is WebsiteFeedback

    Public static testmethod void testMessageDetailController3()
      {
        ApexPages.currentPage().getParameters().put('Request' , 'WebsiteFeedback');
        CpProdImproveController propr=new CpProdImproveController();
        
        ProdCountEmailMapping__c prodmap=new ProdCountEmailMapping__c();
        prodmap.email__c ='test@gmail.com';
        prodmap.Name='WebsiteFeedback';
        prodmap.ProductGroup__c='4DITC';
        insert  prodmap;
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
        propr.submit();
      
     }
    
 //Test Method for initiating  CpProdImproveController class when Request is CertificateManager

    Public static testmethod void testMessageDetailController4()
    { 
    
        ApexPages.currentPage().getParameters().put('Request' , 'CertificateManager');
        CpProdImproveController propr=new CpProdImproveController();
        
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
    
    
    }

 //Test Method for initiating  CpProdImproveController class when Request is LearningCenter

    Public static testmethod void testMessageDetailController5()
    { 
    
        ApexPages.currentPage().getParameters().put('Request' , 'LearningCenter');
        CpProdImproveController propr=new CpProdImproveController();
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
        
    
    }

 //Test Method for initiating  CpProdImproveController class when Request is HelpTicket

    Public static testmethod void testMessageDetailController6()
    { 
    
        ApexPages.currentPage().getParameters().put('Request' , 'HelpTicket');
        CpProdImproveController propr=new CpProdImproveController();
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
        
    
    }

 //Test Method for initiating  CpProdImproveController class when Request is ProductAccessUpdate

    Public static testmethod void testMessageDetailController7()
    {
        ApexPages.currentPage().getParameters().put('Request' , 'ProductAccessUpdate');
        CpProdImproveController propr=new CpProdImproveController();
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
        
    }
    
 //Test Method for initiating  CpProdImproveController class when Request is FileDownloadIssues
 
    Public static testmethod void testMessageDetailController8()
    {
        ApexPages.currentPage().getParameters().put('Request' , 'FileDownloadIssues');
        CpProdImproveController propr=new CpProdImproveController();
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
        
        propr.EmailBodysetup(toAddresses);
    
    }
  /*  Public static testmethod void testMessageDetailController9()
    {
    
        ApexPages.currentPage().getParameters().put('Request' , 'PaperDocumentation');
        CpProdImproveController propr=new CpProdImproveController();
        
        propr.newatt.body=Blob.valueof('abc');
        propr.newatt.name='test';
        propr.CustEmail='test@tes2t.com';
        List<String> toAddresses = new List<String>();
        toAddresses.add('Test@gmail.com');
        toAddresses.add('Test@gmail.com'); 
         ContentWorkspaceDoc cwspace=[Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc where ContentWorkspace.Name ='ARIA Medical Oncology' LIMIT 1];
        ContentVersion cont= [SELECT Id FROM ContentVersion WHERE ContentDocumentId=:cwspace.ContentDocumentId and Document_Language__c='English'  and IsLatest = true and Printable__c = True];
     //   ApexPages.currentPage().getParameters().put('passedParam',cont.Id);
        propr.passedParam=cont.Id;
        propr.getlContentVersions();
      
        propr.CustEmail='test@gmail.com';
        propr.EmailBodysetup(toAddresses);
    
    }*/
    Public static testmethod void testMessageDetailController10()
    { 
     User urs = [ select Id from User where Id =:UserInfo.getUserId() ];    
        System.runAs (urs)    
        {          
            
            User u;   
            Account a;        
           // Profile po = [select id from profile where name=: Label.VMS_Customer_Portal_User];    
              Profile po = [select id from profile where name='VMS MyVarian - Customer User'];
            Account objAcc= new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test',Legal_Name__c ='Testing'); 
            insert objAcc; 
            
            a = new Account(name='newtest2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='ABCD',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='6578',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1234567',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test',Legal_Name__c ='MyTest'  ); 
            insert a; 
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667xxx@gmail.com', AccountId=a.Id,MailingCountry ='USA', MailingState='testState',OktaId__c='asad1213'); 
            insert con;
            //Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com',MailingCountry ='USA',phone='3434343',Title='testtit',MailingStreet='xyx', MailingState='xyz', MailingPostalCode='2323232', MailingCity='Noida');   
            u = new User(alias = 'standt', email='standarduserxxxx@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = po.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,username='standarduserccc@testclass.com'); 
            insert u;
            Test.starttest();
            // User urs = [ select ContactId, Profile.Name  from User where Id =:UserInfo.getUserId() ];    
            System.runAs (u)
            {
        ApexPages.currentPage().getParameters().put('Request' , 'HelpTicket');
        ProdCountEmailMapping__c prodmap=new ProdCountEmailMapping__c();
        prodmap.email__c ='test@gmail.com';
        prodmap.Name='';
        prodmap.ProductGroup__c='';
        insert  prodmap;
        ProdCountEmailMapping__c prodmap1=new ProdCountEmailMapping__c();
        prodmap1.email__c ='test@gmail.com';
        prodmap1.Name='HelpTicket';
        prodmap1.ProductGroup__c='4ditc';
        insert  prodmap1;
        CpProdImproveController propr1=new CpProdImproveController();
         propr1.newatt.body=Blob.valueof('abc');
        propr1.newatt.name='test';
        propr1.CustEmail='test@tes2t.com';
       
        propr1.submit();
        ApexPages.currentPage().getParameters().put('Request' , 'ProductImprovement');
        ProdCountEmailMapping__c prodmap2=new ProdCountEmailMapping__c();
        prodmap2.email__c ='test@gmail.com';
        prodmap2.Name='';
        prodmap2.ProductGroup__c='';
        insert  prodmap2;
         ProdCountEmailMapping__c prodmap3=new ProdCountEmailMapping__c();
        prodmap3.email__c ='test@gmail.com';
        prodmap3.Name='ProductImprovement';
      // prodmap3.ProductGroup__c=null;
        insert  prodmap3;
        CpProdImproveController propr2=new CpProdImproveController();
         propr2.newatt.body=Blob.valueof('abc');
        propr2.newatt.name='test';
        propr2.CustEmail='test@tes2t.com';
        propr2.strCustEmail='test@tes2t.com';
                
                propr2.AcctName='ss';
                propr2.AcctName1='ss';
               
        propr2.submit();
        propr2.updatecon1();
        }
    }
    }
   
}
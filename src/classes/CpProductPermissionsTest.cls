/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 28-June-2013
    @ Description   :  Test class for CpProductPermissions class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CpProductPermissionsTest{

       
       
      //Test Method for CpProductPermissions class for setContentRoles Method
      
      static testmethod void testCpProductPermissionsmethod1(){ 
        //Test.startTest() ;
        
        //User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];

        
        //System.runAs ( thisUser ){
        Profile p = [select id from profile where name=:Label.VMS_Customer_Portal_User];
        
        User u;
        
        Group grp = new Group(Name ='VMS_Internal_User', Type = 'Regular');
        insert grp;
        Account a;
       
       // User thisUser = [ select Id from User where Id = :UserInfo.getUserId()];
        //system.ruans(thisUser)
            a = new Account(Selected_Groups__c = grp.Id,name='test2', BillingCountry='Test', BillingPostalCode ='12243', BillingCity='California', BillingState='Washington', BillingStreet='xyx', country__c='testcountry' ); 
            insert a;
              
              
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test1' ); 
            insert con;
        
        //system.runas(thisUser){
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', username='standarduser@testorgs.com', ContactID=con.id); 
            insert u;
            
            Product_Access_Assignment__c pa = new Product_Access_Assignment__c(Name = 'TEST PP',Assigned_Products__c='Acuity',Product_Access__c = 'Acuity');
            insert pa;
        //}
        //system.runas(u){
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            Prod.ProductCode = 'CLO00100100P';          
            prod.Product_Group__c ='Acuity';  
            prod.Model_Part_Number__c='123';
            prod.isActive = true;
            insert prod;
            
            System.AssertNotEquals(null,prod.Id);
            
            Market_kit__c mk = new Market_kit__c(Name = 'Calypso for Breast test',Install_Products__c='CLO00100100N;CLO00100100P',Market_Kits__c='CLO00100100P',MarketKit_Id__c = prod.Id);
            insert mk;
            
            SVMXC__Site__c loc = new SVMXC__Site__c(Name = 'test location', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = a.id, ERP_Reference__c = '0975',ERP_Functional_Location__c='0975');
                insert loc;
                
            Account ACCTEST = new Account(name='test3', BillingCountry='Test', BillingPostalCode ='12243', BillingCity='California', BillingState='Washington', BillingStreet='xyx', country__c='testcountry' ); 
            insert ACCTEST;
            
            SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
            Insprd.Name = 'Testing';     
            Insprd.ERP_Product_Code__c = 'CLO00100100P';           
            Insprd.SVMXC__Product__c = prod.Id;
            Insprd.SVMXC__Status__c = 'Installed';
            Insprd.ERP_Functional_Location__c = '0975';
            Insprd.SVMXC__Site__c = loc.Id;
            
            Insert Insprd;
            
            Insprd.SVMXC__Company__c = a.Id;
            update Insprd;
            
            // Moved Startest from Top to here to avaoid SOQL 101, Unity 1C deployment
            Test.startTest() ;
            
            SVMXC__Installed_Product__c tP = [select Id,SVMXC__Product__r.ProductCode from SVMXC__Installed_Product__c where Id =: Insprd.Id];
             
            System.AssertEquals(Insprd.ERP_Product_Code__c,'CLO00100100P');
            System.AssertEquals(tP.SVMXC__Product__r.ProductCode,'CLO00100100P');
            System.AssertEquals(prod.ProductCode,'CLO00100100P');
            System.AssertEquals(mk.Install_Products__c,'CLO00100100N;CLO00100100P');
            
             
            Product_Roles__c prodRole=new Product_Roles__c();
            prodRole.Name='testRole';
            prodRole.Model_Part_Number__c='123';   
            prodRole.product_name__c='Acuity';
            prodRole.Public_Group__c='123';         
            insert prodRole;
            
            MarketingKitRole__c marktrole=new MarketingKitRole__c();
            marktrole.Account__c=a.id;
            marktrole.Product__c=prod.id;
            
            insert marktrole;
            
            set<Id> seMrtk = new set<Id>();
            seMrtk.add(marktrole.Id);
            
            
            
             CpProductPermissions prodper = new CpProductPermissions ();
         
            set<Id> STId = new Set<Id>();
            set<Id> STUser = new Set<Id>();
            STUser.add(u.Id);
            List<User> lstUser = new List<User>();
            
            STId.add(Insprd.id);
             set<Id> STIdold = new Set<Id>();
            STIdold.add(Insprd.id);
            CpProductPermissions.setContentRoles(STId,STIdold);
            CpProductPermissions.fetchProductGroup();
            CpProductPermissions.CpAddUserToSelectedPG(STUser);
            CpProductPermissions.setContentRoles_UserActivation(STId,STUser);
            CpProductPermissions.assignUserToSelectedGroup(STId,STUser);
            
            
            Profile cPp= [select id from profile where name=:Label.VMS_Customer_Portal_Admin or Name =: Label.VMS_Customer_Portal_Publisher or Name =: Label.VMS_Marketing_User or Name =: Label.VMS_Customer_Portal_Internal_User or Name =: Label.VMS_PNL_Admin or Name =: Label.VMS_Customer_Portal_RSS_User];
            
            User cPu = new User(alias = 'standt2', Subscribed_Products__c=true,email='standarduser2@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = cPp.Id, timezonesidkey='America/Los_Angeles', username='standarduser2@testorgs.com');
                    
            insert cPu;
            lstUser.add(cPu);
            
            CpProductPermissions.AddUserInGroup(lstUser);
            
            Profile cp= [select id from profile where name= 'System Administrator'];
            User cu = new User(alias = 'standt1', Subscribed_Products__c=true,email='standarduser1@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = cp.Id, timezonesidkey='America/Los_Angeles', username='standarduser1@testorgs.com');
            cu.UserPermissionsSFContentUser = true;         
            insert cu;
            
            lstUser.clear();
            lstUser.add(cu);
            CpProductPermissions.AddVMSInternalUserToGroup(lstUser);
            //delete Insprd;
        //}
       // }

           
        
        Test.stopTest();
    }
    static testmethod void NonUseMethod(){      
        CpProductPermissions.Cpremovepublicgroups(new set<Id>());
        CpProductPermissions.CpaccountActivateDeactivate(new set<Id>());
        
    }
    static testmethod void AddPortalUserInGroup(){
        Account a = new Account(name='test2', BillingCountry='Test', BillingPostalCode ='12243', BillingCity='California', BillingState='Washington', BillingStreet='xyx', country__c='testcountry' ); 
        insert a;
            
        Account a2 = new Account(name='test3', BillingCountry='Test', BillingPostalCode ='12243', BillingCity='California', BillingState='Washington', BillingStreet='xyx', country__c='testcountry' ); 
        insert a2;
            
            
        Product_Roles__c prodRole=new Product_Roles__c();
            prodRole.Name='testRoledsd';
            prodRole.Model_Part_Number__c='12345';   
            prodRole.product_name__c='testRole';
            prodRole.Public_Group__c='123';         
            insert prodRole;
            
        Product2 prod = new Product2();
            prod.Name = 'testRole';
            Prod.ProductCode = 'CLO00100100P';          
            prod.Product_Group__c ='Acuity';  
            prod.Model_Part_Number__c = '12345';
            prod.isActive = true;
            insert prod;
            
            Product2 prod2 = new Product2();
            prod2.Name = 'Premier Marketing Program';
            prod2.ProductCode = 'CLO00100100L';          
            prod2.Product_Group__c ='Acuity';  
            prod2.Model_Part_Number__c = '12345';
            prod2.isActive = true;
            insert prod2;

            MarketingKitRole__c marktrole=new MarketingKitRole__c();
            marktrole.Account__c=a.id;
            marktrole.Product__c=prod.id;
            
            insert marktrole;
            MarketingKitRole__c marktrole2=new MarketingKitRole__c();
      
             marktrole2.Account__c=a2.id;
            marktrole2.Product__c=prod.id;
            
            insert marktrole2;
            
            set<Id> seMrtk = new set<Id>();
            seMrtk.add(marktrole.Id);
            seMrtk.add(marktrole2.Id);
         
        Profile cPp= [select id from profile where name=:Label.VMS_Customer_Portal_User];
            
         Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test1' ); 
            insert con;
            
        User cPu = new User(alias = 'standt2', Subscribed_Products__c=true,email='standarduser2@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = cPp.Id, timezonesidkey='America/Los_Angeles', username='standarduser2@testorgs.com',ContactId = Con.Id);
        insert cPu;
        List<User> lstUser = new List<User>();
            lstUser.add(cPu);
        
        CpProductPermissions.AddPortalUserInGroup(seMrtk);
        User thisUser = [ select Id,ContactId,IsActive,UserPermissionsSFContentUser from User where Id = :UserInfo.getUserId()];
        lstUser.clear();
        lstUser.add(thisUser);
        CpProductPermissions.AddVMSInternalUserToGroup(lstUser);
    }
    

 }
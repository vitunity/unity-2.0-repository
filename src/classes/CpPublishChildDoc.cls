public class CpPublishChildDoc
{
    public transient String author {get; set;}
    public transient List<String> addLib {get;set;}
    public transient String library {get;set;}
    public List<SelectOption> libraries;
    private ApexPages.StandardController stdController;
    public transient Blob fileBody {get; set;}  
    public transient string fileName {get; set;} 
    public Id docId {get; set;}
    public String versionnum{get; set;}
    public String flag {get; set;}
    public String TypeofUpload{get; set;}
    
    public boolean IsRender{get; set;}
    //********Code for Multiselect Picklist Property Decaration Starts ******************************//
    public List<SelectOption> selectedGroupList{get;set;}
    public List<SelectOption> groupList{get;set;}
    
    Public List<string> leftselected{get;set;}

    Public List<string> rightselected{get;set;}

   

    
    public String leftLabel{get;set;}
    public String rightLabel{get;set;}

   //********Code for Multiselect Picklist Property Decaration Ends******************************//


    public CpPublishChildDoc(ApexPages.StandardController controller)
    {
groupList = new List<SelectOption>();

selectedGroupList = new List<SelectOption>();

        
    leftLabel = 'Available Libraries';
        rightLabel = 'Selected Libraries';
        leftselected = new List<string>();
        rightselected = new List<string>();

        User currUser = [SELECT Id, Name FROM User WHERE Id = : Userinfo.getUserId() limit 1];
        author = currUser.Name;
        this.stdController = controller;
        flag = 'detail';
        
        groupList.add(new selectoption('--None--','--None--'));
        Schema.DescribeFieldResult fieldResult = ContentVersion.Product_Group__c.getDescribe();
        List<Schema.PicklistEntry> pickVals = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry p : pickVals)
        {
            groupList.add(new selectoption(p.getValue(),p.getValue()));
        }
       
    
    }
    
    public CpPublishChildDoc()
    {
        User currUser = [SELECT Id, Name FROM User WHERE Id = : Userinfo.getUserId() limit 1];
        author = currUser.Name;
        flag = 'detail';
      IsRender=false;
    }
  public PageReference selectclick(){
        rightselected.clear();
        List<SelectOption> options = new List<SelectOption>();
        set<String> temp = new set<String>();
        for(String s : leftselected){
            temp.add(s);
        }
        for(SelectOption s : groupList){
            if(!temp.contains(s.getValue())){
                options.add(s);
            }else{
                selectedGroupList.add(s);
            }
        }
                System.debug('Test----->'+selectedGroupList);
        groupList.clear();
        groupList.addAll(options);
        return null;
    }
    
    public PageReference unselectclick(){
                leftselected.clear();

        List<SelectOption> options = new List<SelectOption>();
        set<String> temp = new set<String>();
        for(String s : rightselected){
            temp.add(s);
        }
        for(SelectOption s : selectedGroupList){
            if(!temp.contains(s.getValue())){
                options.add(s);
            }else{
                groupList.add(s);
            }
        }
        selectedGroupList.clear();
        selectedGroupList.addAll(options);
        
        return null;
    }



    public List<SelectOption> getlibraries()
    {
        libraries = new List<SelectOption>();
        libraries.add(new selectoption('--None--','--None--'));
        Schema.DescribeFieldResult fieldResult = ContentVersion.Product_Group__c.getDescribe();
        List<Schema.PicklistEntry> pickVals = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry p : pickVals)
        {
            libraries.add(new selectoption(p.getValue(),p.getValue()));
        }
         
        return libraries;
    }
    public PageReference ProductVerCheck()
    {
   
     if( System.currentPageReference().getParameters().get('ParentContentDetailPage')=='detail')
       {
       TypeofUpload='UploadParent';
       }
        if(System.currentPageReference().getParameters().get('ChildContentDetailPage')=='child')
       {
       TypeofUpload='UploadChild';
       }
        IF(versionnum!='')
    {
     Set<Id> libId = new Set<Id>();

        System.debug('RightSelected--->'+rightselected);
        for(SelectOption so : selectedGroupList) {
        if(!library.contains(so.getLabel())){
            rightselected.add(so.getLabel());
                }
        
                }
  set<Id> DocIds = new set<Id>();
    Map<Id,set<String>> docwokspacemap = new map<Id,set<String>>();
    Map<String,Set<String>> workspversionmap = new map<String,Set<String>>();
    set<String> workspacenameset = new set<String>();
        String Workspacename;
       workspacenameset.add(library);
       workspacenameset.addall(rightselected);
     for(String str: workspacenameset)
     {
       if(Workspacename == null)
       {    
         Workspacename = str +'\'';
       }else{
        Workspacename = Workspacename + ',\'' + str + '\'';
       }
     }
    
     System.debug('TestWorkSpace'+Workspacename );
     String query = 'Select id,name,Product_Group__c from product2 where Product_Group__c INCLUDES (\'' + Workspacename + ')';
      System.debug('Query-->'+query );
     List<Product2> prdlist = database.query(query);
     map<Id,Set<String>> productandgroupmap = new map<Id,Set<String>>();
     map<Id,Set<String>> productandversionmap = new map<Id,Set<String>>();
     set<Id> productversionset = new Set<Id>();
     for(Product2 prd: prdlist)
     {
       Set<String> productgroup = new Set<String>();
       if(prd.Product_Group__c != null){
       productgroup.addall(prd.Product_Group__c.split(';'));
       productandgroupmap.put(prd.Id,productgroup);
       productversionset.add(prd.id);
       }
     }
     for(Product_Version__c prdvrs : [Select Version__c,id,Product__c from Product_Version__c where Product__c in : productversionset and Version__c != null])
     {
        Set<String> versions = new set<String>();
        if(productandversionmap.containsKey(prdvrs.Product__c))
        {
        System.debug('test'+productandversionmap.get(prdvrs.id));
       // if(productandversionmap.get(prdvrs.id)!=null)
          versions = productandversionmap.get(prdvrs.Product__c);
          versions.add(prdvrs.Version__c.trim());
          productandversionmap.put(prdvrs.Product__c,versions);
          System.debug('test1'+productandversionmap); 
        }else{
          versions.add(prdvrs.Version__c.trim());
          productandversionmap.put(prdvrs.Product__c,versions);
         }
      }
       Set<String> versionsworkspc = new set<String>();
     for(Id d: productandgroupmap.keyset())
     {
      for(String str: productandgroupmap.get(d))
      {
      if(productandversionmap.get(d) != null){
         
          if(workspversionmap.containskey(str))
          {
                versionsworkspc = workspversionmap.get(str);
                versionsworkspc.addall(productandversionmap.get(d));
                workspversionmap.put(str,versionsworkspc);
              }else{
                versionsworkspc.addall(productandversionmap.get(d));
                workspversionmap.put(str,versionsworkspc);
              }
          }
      }
     }
     Set<String> validversion = new set<String>();
       validversion.addall(versionsworkspc);
     System.debug('versionnum---'+versionnum);  
     for(String s : versionnum.split(','))
       {
     
       if(validversion != null){
       System.debug('Test@@@--->'+validversion );
       System.debug('Test--->'+s);
         if(!validversion.contains(s.trim()))
         {
       
       IsRender=false;
       
       ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Version(s) may not be: a) valid for this product or b) formatted correctly. Versions must be entered using xx.yy format such as 8.5 or 11.0. When entering multiple versions, they must be comma separated: 8.0, 8.2, 8.5')); 
        return null;    
         }
         else
         {
          IsRender=true;
         }
         }
         }
         }
         ELSE
         {
         IsRender=true;
         }
    return null;
    }
    public pagereference saveToAddLib()
    {
        Set<Id> libId = new Set<Id>();
        List<ContentWorkspaceDoc> conWrkDocList = new List<ContentWorkspaceDoc>();
        System.debug('RightSelected--->'+rightselected);
        for(SelectOption so : selectedGroupList) {
        if(!library.contains(so.getLabel())){
            rightselected.add(so.getLabel());
                }
        
                }

        List <ContentWorkspace> addLibList = [SELECT Id, Name FROM ContentWorkspace WHERE Name IN :rightselected];
        System.debug('AddLibList---->'+addLibList);
        Id contentdocId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :docId].ContentDocumentId;
        for(ContentWorkspace c : addLibList)
        {
            ContentWorkspaceDoc newContWrkDoc = new ContentWorkspaceDoc();
            newContWrkDoc.ContentDocumentId = contentdocId;
            newContWrkDoc.ContentWorkspaceId = c.Id;
            
            conWrkDocList.add(newContWrkDoc);
        }
        insert conWrkDocList;
        if(flag == 'child')
        {
            pagereference pref;
            pref = new pagereference('/apex/CpAddChildDoc?contId='+docId);
            pref.setredirect(true);
            
            return pref;
        }
        else if(flag == 'detail')
        {
            pagereference pref;
            pref = new pagereference('/apex/CpContentDetail?id='+docId);
            pref.setredirect(true);
            return pref;
        }
        return null;
    }
}
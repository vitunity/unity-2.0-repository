@isTest

public class CpPublishChildDocTest
{
    static testmethod void testMethod1()
    {
        Test.StartTest();
        
        ContentVersion cont = new ContentVersion();
        cont.Title = 'Test Title';
        cont.VersionData = blob.valueOf('xyz');
        cont.Origin = 'C';
        cont.pathOnClient = 'Test Title';
        cont.FirstPublishLocationId = [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Acuity'].Id;
        cont.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'ContentVersion' AND DeveloperName = 'Product_Document'].Id;
        insert cont;
        ApexPages.StandardController controller1 = new ApexPages.StandardController(cont); 
        CpPublishChildDoc inst = new CpPublishChildDoc(controller1);
        inst.docId = cont.Id;
        inst.flag = 'child';
        CpPublishChildDoc inst2 = new CpPublishChildDoc();
        pagereference clickMethod = inst.selectclick();
        pagereference clickMethod2 = inst.unselectclick();
        List<SelectOption> options = inst.getlibraries();
        inst.rightselected.add('Argus');
        pagereference addLib = inst.saveToAddLib();
        
        Test.StopTest();
    }
        
}
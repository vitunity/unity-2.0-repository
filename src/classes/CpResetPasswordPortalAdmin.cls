public class CpResetPasswordPortalAdmin {
public string noerror{get;set;}
    public CpResetPasswordPortalAdmin(ApexPages.StandardController controller) {
     userid = ApexPages.currentPage().getParameters().get('id');
 OktaId = [Select Contact.OktaId__c from user where id=: userid].Contact.OktaId__c;
 Contactid = [Select ContactId from user where id=: userid].ContactId;
    }

 public String userid;
public String OktaId; 
public String Contactid;
public String OktaStatus;

public pagereference Passwordactionmethod(){
      //String OktaId = [Select Contact.OktaId__c from user where id=: userid].Contact.OktaId__c;
      //String OktaStatus;
 // API call for getting user detail from okta    
      HttpRequest reqgetuser = new HttpRequest();
      HttpResponse resgetuser = new HttpResponse();
      Http httpgetuser = new Http();
      String strToken = label.oktatoken; // '000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK'; //label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
      String authorizationHeader = 'SSWS ' + strToken;
      reqgetuser.setHeader('Authorization', authorizationHeader);
      reqgetuser.setHeader('Content-Type','application/json');
      reqgetuser.setHeader('Accept','application/json');
      String endpointgetuser = label.oktachangepasswrdendpoint + OktaId;
      reqgetuser.setMethod('GET');
      reqgetuser.setEndpoint(endpointgetuser);
      try{
         if(!test.IsRunningTest()){
           resgetuser = httpgetuser.send(reqgetuser);
          }else{
           resgetuser = fakeresponse.fakeresponsemethod('getuser');
          }
           system.debug('aebug ; ' +resgetuser.getBody());
           //system.debug('Password generated:' + fpwd);
           JSONParser parser = JSON.createParser(resgetuser.getBody());
           while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'status') {
                        OktaStatus = parser.getText();                          
                    }
                }
            }
        }catch(System.CalloutException e) {
            System.debug(resgetuser.toString());
        }
    if(OktaStatus == 'LOCKED_OUT'){
     //Api to unlock the user account
      HttpRequest requnlockuser = new HttpRequest();
      HttpResponse resunlockuser = new HttpResponse();
      Http httpunlockuser = new Http();
      requnlockuser.setHeader('Authorization', authorizationHeader);
      requnlockuser.setHeader('Content-Type','application/json');
      requnlockuser.setHeader('Accept','application/json');
      String endpointunlockuser = label.oktachangepasswrdendpoint + OktaId + '/lifecycle/unlock';
      requnlockuser.setMethod('POST');
      requnlockuser.setEndpoint(endpointunlockuser);
      try{
         if(!test.IsRunningTest()){
           resunlockuser = httpunlockuser.send(requnlockuser);
          } else{
            resunlockuser = fakeresponse.fakeresponsemethod('getuser');
           }
           system.debug('aebug ; ' +resunlockuser.getBody());
           //system.debug('Password generated:' + fpwd);
      }catch(System.CalloutException e) {
            System.debug(resgetuser.toString());
        }
    }
    return null;
}

 public PageReference Resetpass(){
     String Newpass = ApexPages.currentPage().getParameters().get('PWDConfirm');
     String ConfirmPass = ApexPages.currentPage().getParameters().get('ConfirmPWD');
     //String OldPassword = ApexPages.currentPage().getParameters().get('OLDPWD');
     //String oldpasswrd = usrMyCP.contact.activation_url__c; 
     system.debug('NEWPASSWORD-----' + Newpass);
     system.debug('CONFIRMPASSWORD----' + ConfirmPass);
     //system.debug('OLDPASSWORD----' + OldPassword);
     /**** Http request for changing the password *******/
     
     HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        String body = '{"credentials": {"password" : { "value": "'+ Newpass + '" }}}';
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setmethod('PUT');
        req.setbody(body);
        String endpoint = label.oktachangepasswrdendpoint + OktaId ;
        req.setEndpoint(endpoint);
        try {
           //Send endpoint to OKTA
           system.debug('aebug ; ' +req.getBody());
           if(!Test.IsRunningTest()){
           res = http.send(req);
           } else{
            res = fakeresponse.fakeresponsemethod('getuser');
            }
           system.debug('aebug ; ' +res.getBody());
           }catch(System.CalloutException e) {
            System.debug(res.toString());
        }
     if(!res.getbody().contains('errorSummary')){
     Contact con = new contact(id = Contactid);
     con.PasswordReset__c = false;
     con.activation_url__c = Newpass;
     try{
      update con;
     }catch(Exception e){
       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getmessage()));
     }
     }
      if (!res.getBody().contains('errorSummary')){
         //noerror = 'true';
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Password has been successfully updated for the user.'));
         return null;
        //return new pagereference('/' + userid);
        
     }else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Error occured while reseting password. Please try again'));
        return null;
        }
  }
}
/*************************************************************************\
    @ Author        : Harshita
    @ Date      : 17-June-2013
    @ Description   :  Test class used for code coverage of ResetPasswordPortaladmin.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@IsTest

Class CpResetPasswordPortalAdminTest
{
    
    // Test Method for initiating cpLungPhantom class
    
    private static testmethod void cpResetPortalAdmintest()
    {
      Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
      
       Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
      Contact con = new Contact(FirstName='Ankita',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
        insert con; 
      User u = new User(alias = 'stand', Subscribed_Products__c=true,email='standardtestuser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standardtestuser@test.com'); 
        insert u; // Inserting Portal User
      ApexPages.currentPage().getParameters().put('id',u.id);
      //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
      ApexPages.StandardController sc = new ApexPages.standardController(u);
       CpResetPasswordPortalAdmin sic = new CpResetPasswordPortalAdmin(sc);
       sic.OktaStatus = 'LOCKED_OUT';
      sic.Passwordactionmethod();
      sic.Resetpass();    
    }       
    }
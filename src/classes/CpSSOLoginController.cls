public with sharing class CpSSOLoginController {
    
    public String username{get;set;} 
    public String password{get;set;}
    public String cookieTokenUrl{get;set;}
    public String token{get;set;}
    public Boolean ready{get; set;}
    
    public CpSSOLoginController(){
        token = Apexpages.currentPage().getParameters().get('token');
        ready = false;
    }
    
    public PageReference LogintoOkta(){ 
        
        //username = 'anupam.tripathi1@wipro.com';
        //password = 'Wipro@123';
        
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        //Blob headerValue = Blob.valueOf(username+':'+password);
        String strToken = '00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //req.setHeader('username',username);
        //req.setHeader('password',password);
        
        String body = '{ "username": "'+username+'", "password": "'+password+'" }';
        req.setBody(body);
        /*
        req.setBody('Content-Type=' + EncodingUtil.urlEncode('application/json', 'UTF-8') +
                        '&charset=' + EncodingUtil.urlEncode('UTF-8', 'UTF-8') +
                        '&Accept=' + EncodingUtil.urlEncode('application/json', 'UTF-8') +
                        '&grant_type=' + EncodingUtil.urlEncode('password', 'UTF-8') +              
                            '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') + 
                             '&password=' + EncodingUtil.urlEncode(password, 'UTF-8')                  
                        );
        */
         //Construct Endpoint
        String endpoint = 'https://varian.okta.com/api/v1/sessions?additionalFields=cookieTokenUrl';
 

        //Set Method and Endpoint and Body
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        
        //String cookieTokenUrl = null; 
               
         try {
           //Send endpoint to OKTA
              if(!Test.isRunningTest()){
           res = http.send(req);
           }
           else
           {
           res = fakeresponse.fakeresponseloginmethod();
           }
           system.debug('aebug ; ' +res.getBody());
           JSONParser parser = JSON.createParser(res.getBody());
           while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'cookieTokenUrl') {
                        cookieTokenUrl = parser.getText();                          
                    }
                    
                }
           }
           
           if(cookieTokenUrl != null){
               ready = true;
               System.debug('cookieTokenUrl = '+cookieTokenUrl);
           }
           
        } catch(System.CalloutException e) {
            System.debug(res.toString());
        }
        
         //Set the cookie token
         
         //Cookie myCookies=new Cookie('mycookie',cookieTokenUrl,null,-1,false);
         //ApexPages.currentPage().setCookies(new Cookie[]{myCookies});
         //PageReference pr = new PageReference('/apex/CpSSOChild?token='+cookieTokenUrl);
         //pr.setCookies(new Cookie[]{myCookies});        
         //pr.setRedirect(true);
         
         //System.debug('aebug +pr.getCookies = '+pr.getCookies());        
         //System.debug('aebug +pr = '+pr); 
         //return pr;
         //return null;
         return new pagereference('https://varian.okta.com/home/salesforce_portal/0oa7pmmehlRHKPNNQARR/1339?onetimetoken='+ cookieTokenUrl);
   
    } 
    
    
    
    
    
    

    
}
/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 26-Apr-2013
    @ Description   : An Apex class to display all data related to contacts and phone support from custom settings .
****************************************************************************/

public class CpSupportcontactInfoController
 {
public String propListCustomSetting{get;set;}
public String propListCustomSettingAUS{get;set;}
public String propListCustomSettingAUSph{get;set;}
public String propListCustomSettingEur {get;set;}
public String propListCustomSettingJAP{get;set;} 
public String propListCustomSettingChina{get;set;}
public String propListCustomSettingSouASI{get;set;} 
public String propListCustomSettingLatAmer{get;set;} 
public String propListCustomSettingcountntlst{get;set;}
public Boolean isGuest{get;set;}
public void fetchCustomeData()    
{
Support_Contact__c ListCustomSetting = Support_Contact__c.getInstance('North America');  
      propListCustomSetting = ListCustomSetting.Phone_Support__c;
Support_Contact__c ListCustomSettingAUSph = Support_Contact__c.getInstance('Australia');  
      propListCustomSettingAUSph = ListCustomSettingAUSph.Phone_Support__c;
Support_Contact__c ListCustomSettingAUS = Support_Contact__c.getInstance('Australia');  
      propListCustomSettingAUS = ListCustomSettingAUS.E_mail_Support__c;
Support_Contact__c ListCustomSettingEur = Support_Contact__c.getInstance('Europe');  
      propListCustomSettingEur = ListCustomSettingEur.E_mail_Support__c;
Support_Contact__c ListCustomSettingJAP = Support_Contact__c.getInstance('Japan');  
      propListCustomSettingJAP = ListCustomSettingJAP.E_mail_Support__c;
Support_Contact__c ListCustomSettingChina = Support_Contact__c.getInstance('China');  
     propListCustomSettingChina = ListCustomSettingChina.E_mail_Support__c;
Support_Contact__c ListCustomSettingSouASI = Support_Contact__c.getInstance('South East Asia');  
      propListCustomSettingSouASI = ListCustomSettingSouASI.E_mail_Support__c;
Support_Contact__c ListCustomSettingLatAmer = Support_Contact__c.getInstance('Latin America');  
      propListCustomSettingLatAmer = ListCustomSettingLatAmer.E_mail_Support__c;
Support_Contact__c ListCustomSettingcountntlst = Support_Contact__c.getInstance('Countries not listed');  
     propListCustomSettingcountntlst = ListCustomSettingcountntlst.Phone_Support__c;
}
public CpSupportcontactInfoController()
{ 
isGuest =SlideController.logInUser();
fetchCustomeData();
}
public static list<Support_Contact__c> getAllCountriesContact()
 {  
return Support_Contact__c.getall().values();  
  } 


 public static list<Support_Contact__c> getGamesByPlatform(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 
if(Contact.Region__c=='Europe / Middle East')
{ 
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
public static list<Support_Contact__c> getAsiaSpecificCount(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Asia Pacific')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
 
public static list<Support_Contact__c> getAustralasia(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Australasia')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
 public static list<Support_Contact__c> getLatinAmerica(){  
list<Support_Contact__c> AllContacts = getAllCountriesContact();  
list<Support_Contact__c> returnlist = new list<Support_Contact__c>(); 
AllContacts.sort();
//String myPlatform = getPlatform();  
 for(Support_Contact__c Contact: AllContacts )  
{ 

if(Contact.Region__c=='Latin America')
{
returnlist.add(Contact);  
}

System.debug(returnlist.size());  
}  
return returnlist;  
 } 
 public static Boolean logInUser(){
      if(UserInfo.getUserType() == 'CspLitePortal' || UserInfo.getUserType() == 'PowerCustomerSuccess'){
              return false;
            }else{
              return true;
            }

}
 }
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 16-June-2013
    @ Description   :  Test class for CpSupportcontactInfoController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest
Public class CpSupportcontactInfoControllerTest
{
 // Test Method for CpSupportcontactInfoController class
 
 public static testmethod void TestCpSupportcontactInfoController()   
  {
  User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];
System.runAs( thisUser ){

  Support_Contact__c ListCustomSetting = Support_Contact__c.getValues('North America');
  
  if(ListCustomSetting == null) {
    ListCustomSetting = new Support_Contact__c(Name= 'North America');
    ListCustomSetting .Phone_Support__c = 'Test';
    insert ListCustomSetting ;}
   Support_Contact__c ListCustomSetting1 = Support_Contact__c.getValues('Australia');
  
  if(ListCustomSetting1 == null) {
    ListCustomSetting1 = new Support_Contact__c(Name= 'Australia');
    ListCustomSetting1 .Phone_Support__c = 'Test';
    insert ListCustomSetting1 ;}
   Support_Contact__c ListCustomSetting2 = Support_Contact__c.getValues('Australia');
  
  if(ListCustomSetting2 == null) {
    ListCustomSetting2 = new Support_Contact__c(Name= 'Australia');
    ListCustomSetting2 .E_mail_Support__c = 'Test@test.com';
    insert ListCustomSetting2 ;}
    Support_Contact__c ListCustomSetting3 = Support_Contact__c.getValues('Japan');
  
  if(ListCustomSetting3 == null) {
    ListCustomSetting3 = new Support_Contact__c(Name= 'Japan');
    ListCustomSetting3 .E_mail_Support__c= 'Test@test.com';
    insert ListCustomSetting3 ;}
     Support_Contact__c ListCustomSetting4 = Support_Contact__c.getValues('China');
  
  if(ListCustomSetting4 == null) {
    ListCustomSetting4 = new Support_Contact__c(Name= 'China');
    ListCustomSetting4 .E_mail_Support__c = 'Test@test.com';
    insert ListCustomSetting4 ;}
     Support_Contact__c ListCustomSetting5 = Support_Contact__c.getValues('South East Asia');
  
  if(ListCustomSetting5 == null) {
    ListCustomSetting5 = new Support_Contact__c(Name= 'South East Asia');
    ListCustomSetting5 .E_mail_Support__c = 'Test@test.com';
    insert ListCustomSetting5 ;}
    Support_Contact__c ListCustomSetting6 = Support_Contact__c.getValues('Countries not listed');
    if(ListCustomSetting6 == null) {
    ListCustomSetting6 = new Support_Contact__c(Name= 'Countries not listed');
    ListCustomSetting6 .Phone_Support__c= '2323232';
    insert ListCustomSetting6 ;}
      Support_Contact__c ListCustomSetting7 = Support_Contact__c.getValues('Europe');
    if(ListCustomSetting7 == null) {
    ListCustomSetting7 = new Support_Contact__c(Name= 'Europe');
    ListCustomSetting7 .E_mail_Support__c = 'Test@test.com';
    insert ListCustomSetting7 ;}
      Support_Contact__c ListCustomSetting8 = Support_Contact__c.getValues('Latin America');
    if(ListCustomSetting8 == null) {
    ListCustomSetting8 = new Support_Contact__c(Name= 'Latin America');
    ListCustomSetting8 .E_mail_Support__c = 'Test@test.com';
    insert ListCustomSetting8 ;}
    
    
  }
  CpSupportcontactInfoController cpsupprt=new CpSupportcontactInfoController();

  cpsupprt.fetchCustomeData();
 CpSupportcontactInfoController.getGamesByPlatform();
 CpSupportcontactInfoController.getAsiaSpecificCount();
 CpSupportcontactInfoController.getAustralasia();
 CpSupportcontactInfoController.getLatinAmerica();
}
  
 
  }
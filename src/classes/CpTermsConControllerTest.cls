/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CpTermsConController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest(SeeAllData=true)

Public class CpTermsConControllerTest{

    //Test Method for CpTermsConController class
    
    static testmethod void testCpTermsConController(){
        Test.StartTest();
        
        CpTermsConController termscon = new CpTermsConController();
        termscon.registerLicenseAgreement();
       
         Test.StopTest();
     }
 }
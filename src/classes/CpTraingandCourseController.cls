/*************************************************************************\
Author        : Amit Kumar
Date      : 3-june-2013
Description   : An Apex controller to get Training and Course data
Project:  STSK0012511 - Rakesh Basani - Fix on search in Training Course page
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012687 - Using same URL for all Geos (Updated line 75)
****************************************************************************/
public class CpTraingandCourseController{

    //List of Properties    
    List<ContentVersion> ContentVersions ;
    Public List<Training_Course__c> pre{get;set;}
    Public List<Training_Dates__c> Trdate{get;set;}
    List<Regulatory_Country__c> RegionName =new   List<Regulatory_Country__c>();
    public String Document_TypeSearch {get;set;}
    public String show{get;set;}
    public Id TitleId;
    public string TraingDate{get;set;}
    public String CustEmail {get;set;} 
    public boolean hdNext {get;set;}
    public boolean hdPrvs {get;set;}
    public  list<selectoption> lstContentVersion{get;set;}
    Public String AcctCountry {get; set;}
    public Boolean isGuest{get;set;}
    public Boolean isLMSUser{get;set;}
    public String Productvalue{get;set;} 
    public contact Cont{get;set;} // variable to hold the contact details associated with portal user.
    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
    public Boolean display{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    public String showmssg{get;set;}
    
    public String SelectedMenu{get;set;}
    
    public void FormMenuClick(){
        string menu = ApexPages.currentPage().getParameters().get('SelectedMenu');
        SelectedMenu = menu;
        Cookie UTerritory = new Cookie('UTerritory', SelectedMenu,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{UTerritory});
        counter=0; 
        total_size = -1;
    }

    
     public String getPre() {
            return null;
     }
     //Create the list having the options contain the name of regulatory Country
    Public List<SelectOption> getoptions()
    {
        List<SelectOption> options = new List<SelectOption>();
             
        options.add(new selectoption('-----','-----'));
        for(Regulatory_Country__c cn : [select name from Regulatory_Country__c order by Name]) {
        options.add(new selectoption(cn.name, cn.name)); }
        return options; 
       
    } 
    //This Method is used to redirect the page on the basis of condition i.e from which region the user belongs.
        public PageReference  verify(){ 
            User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];        
            String usrContId = usr.contactid;
            if(usrContId != null){
            
                Contact c = [select  MailingCountry from contact where Id =: usrContId];
                String MailCountry = c.MailingCountry;
                RegionName =[ Select Portal_Regions__c from Regulatory_Country__c where Name=:MailCountry limit 1];
                          
                if(RegionName.size() >0)//STSK0012687: Using same URL for all Geos
                {   
                    PageReference pref = new PageReference(Label.CpTrainingSABAurl); 
                    pref.setRedirect(true);
                    return pref;
                }
            }
      
         PageReference pref = new PageReference('/apex/CpTrainingRegistration?id='+TitleId); 
         pref.setRedirect(true);
         return pref;

  }
  
  
  
 Public List<SelectOption> TraingCourseDate= new List<SelectOption>();
 public List<SelectOption> getTraingCourseDate(){
    return TraingCourseDate;
 }
//This Method is used to get the training datae on the basis of Title id
  Public void setoptionsTraingCourseDate(){
     List<Training_Dates__c> trngdate = new List<Training_Dates__c>([Select Duration_in_days__c,Name, From__c,To__c from Training_Dates__c where  Training_Course__c=:TitleId]);
      for(Training_Dates__c values: trngdate ){
       /*  DateTime Fromdate =values.From__c;
         string Frmdate=Fromdate .format('MMM d,yyyy');
         DateTime Todate =values.To__c;
         string Tdate=Todate.format('MMMM dd,yyyy');*/
            TraingCourseDate.add(new selectoption(String.valueof(values.From__c)+' - '+String.valueof(values.To__c),String.valueof(values.From__c)+' - '+String.valueof(values.To__c))); 
          // TraingCourseDate.add(new selectoption(values.Name,Frmdate+' - '+Tdate)); 
     }
  
 }
 
 
 public List<Training_Dates__c> getStrings(){
   Trdate= new List<Training_Dates__c>([Select Duration_in_days__c,Name, From__c,To__c from Training_Dates__c where  Training_Course__c=:TitleId order by From__c]);
   return Trdate;
  }

    public String CourseDetailFooter{get;set;}
    public CpTraingandCourseController(){ 
       sortField = 'Title__c';
        sortDir = 'asc';
       shows='none';
       isLMSUser = false;
       isGuest = SlideController.logInUser();
       Cont = new contact(); // initilazation of variable
       lstContentVersion=new list<selectoption>();  
       TitleId=ApexPages.currentPage().getParameters().get('Id');
       setoptionsTraingCourseDate();
       getStrings();
       pre= new List<Training_Course__c>();
    
       pre=[select Title__c,Designed_for__c,Replaces_Course__c,Region__c,PreRequisites__c,Software_Version__c,Description__c,Language__c,
                Accreditation__c,FlexCredits__c,Training_Location__c,(select Id,Name from Attachments),(SELECT Id, Title,body from Notes) from Training_Course__c where  id =: TitleId];
       
       
        
       User usr = [Select ContactId,LMS_Status__c, Profile.Name from user where Id=: UserInfo.getUserId()];  
       if(usr.LMS_Status__c <> null && usr.LMS_Status__c == 'Active')isLMSUser = true;
       String usrContId = usr.contactid;
          if(usrContId != null){
          display=true;
                Contact c = [select FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, 
                MailingState, MailingPostalCode, MailingCountry, MailingCity,Territory__c from contact where Id =: usrContId];
                cont=c;
                CustEmail =cont.Email;
                String MailCountry = cont.MailingCountry;
                AcctCountry=MailCountry;
                RegionName =[ Select Portal_Regions__c from Regulatory_Country__c where Name=:MailCountry limit 1];
                if(RegionName.size() >0){
                    if(RegionName[0].Portal_Regions__c =='N. America'){
                            show='block';       
                    }
                    else{
                        show='none';
                    }
         
                }
                
                
                
                /*if(c.Territory__c<>null){
                    SelectedMenu = (c.Territory__c).toLowercase();
                    
                    
                        if(c.Territory__c == 'North America') CourseDetailFooter = Label.NorthAmericaCourseDetail;
                        if(c.Territory__c == 'EMEIA') CourseDetailFooter = Label.EMEIACourseDetail;
                        if(c.Territory__c == 'APAC') CourseDetailFooter = Label.APACCourseDetail;
                        if(c.Territory__c == 'Latin America') CourseDetailFooter = Label.LatinAmericaCourseDetail;
                   
                }*/
                
                Cookie CUTerritory = ApexPages.currentPage().getCookies().get('UTerritory');
                
                if(CUTerritory <> null && CUTerritory.getValue() <> null && CUTerritory.getValue() <> ''){
                        string UTerritory = CUTerritory.getValue();
                        UTerritory = UTerritory.replace('%20',' ');
                        SelectedMenu = (UTerritory).toLowercase();  
                }
        }
        else
        {
        display=false;
        }
         if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }     
   
    }
    
    public string getcourseHeader(){
        string head = Label.OtherCourseDetail;
        if(SelectedMenu == 'north america') head = Label.NorthAmericaCourseDetail;
        if(SelectedMenu == 'emeia') head = Label.EMEIACourseDetail;
        if(SelectedMenu == 'apac') head = Label.APACCourseDetail;
        if(SelectedMenu == 'latin america') head = Label.LatinAmericaCourseDetail;
        return head;
    }
  

 public PageReference readmessage(){
        TitleId=System.currentPageReference().getParameters().get('abc');
        PageReference pref = new PageReference('/apex/CpTrainingCourseDetail?id='+TitleId); 
        pref.setRedirect(true);
        return pref;
  }
  public pagereference backToTrainingCourse()       
    {       
        pagereference pref;     
        pref = new pagereference('/apex/CpTrainingandCourse');      
        pref.setredirect(true);     
        return pref;        
    }
//This method is used to submit the information of user who have registered for training having training date in Training_Registration object.
 public void SubmitForEmail() 
 {
     TitleId=ApexPages.currentPage().getParameters().get('Id');
     /*User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];        
     String usrContId = usr.contactid;*/
     Training_Registration__c TranRegis=new Training_Registration__c();
     TranRegis.Contact__c=cont.Id;
     TranRegis.Training_Course__c =TitleId; 
     TranRegis.Payment_Information__c=Productvalue;
     TranRegis.Training_Registerd_Date__c=String.valueof(TraingDate);
     insert TranRegis;
     showmssg='true';
     /* List<String> toAddresses = new List<String>();
      List<String> touser = new List<String>();
     touser.add(CustEmail);
     List<ProdCountEmailMapping__c > emailmap=new   List<ProdCountEmailMapping__c>();
     emailmap=[Select e.email__c From ProdCountEmailMapping__c e where e.Name=:'TrainingCourse' limit 1];
           if(emailmap.size() >0)
             {
             toAddresses.add(emailmap[0].email__c);
             }
     
    Datetime myDT = Datetime.now();
    String myDate = myDT.format('MM/dd/yyyy HH:mm:ss');
     String EmailBody;
     EmailBody='Thank you for Registration. A Varian representative is reviewing the information and you should receive an automated email containing a tracking number shortly.<br><br>  Thank you for using MyVarian.<br><br>Varian Medical Systems'; 
     EmailBody= EmailBody+ '<br><br>Submitted on '+myDate+ '<br>Name: '+CustFname+' '+CustLname+'<br>Institution: '+AcctName+'<br>Telephone: '+CustPhone+'<br><br>Email: '+CustEmail ;
            EmailBody=EmailBody+'<br><br>Address: '+CustStreet +'<br>City: '+CustCity +'<br>State:  '+CustState+'<br>Country: '+Countryvalue+ '<br>Product: '+Productvalue ;
            Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
            MailTicket.setSubject('Your Registration request has been received');
           
            MailTicket.setToAddresses(touser);
            MailTicket.setHTMLBody(EmailBody);
            MailTicket.setSenderDisplayName('Myvarian');
            MailTicket.setReplyTo(System.Label.MVsupport);       
            MailTicket.setSaveAsActivity (false);
             
            String body = 'Dear Varian Admin <br> <br>';  
            body = body + CustFname +' '+ CustLname + ' from '+ AcctName +', '+ CustCity +', '+ CustState +' has raised Request.Please check';
            body = body + '<br><br>Submitted on '+myDate+ '<br>Name: '+CustFname+' '+CustLname+'<br>Institution: '+AcctName+'<br>Telephone: '+CustPhone+'<br><br>Email: '+CustEmail ;
            body =body +'<br><br>Address: '+CustStreet +'<br>City: '+CustCity +'<br>State:  '+CustState+'<br>Country: '+Countryvalue +'<br>Product: '+Productvalue;
            
            Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
            mailAdmin.setSubject('Registration from  '+ CustFname +' '+ CustLname + ' on MyVarian');
            mailAdmin.setToAddresses(toAddresses);
            mailAdmin.setSenderDisplayName('Myvarian');
           // mailAdmin.setOrgWideEmailAddressId(Id);
    
            mailAdmin.setReplyTo(System.Label.MVsupport);       
            mailAdmin.setHTMLBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin }); */
 }
    
 
  

    public List<Training_Course__c> lContentVersions;

    public PageReference showrecords() 
    {
        getlContentVersions();
        return null;
    }
    public PageReference showrecords1() 
    {
        return null;
    }
//This method is used get the data from Training_Course object that will be shown in the data table.
    public List<Training_Course__c> getlContentVersions() 
    {
        try 
        {
            lContentVersions  = new List<Training_Course__c>();         
            string whr='';
            set<Id> contntId = new set<Id>();
            String temp =  Document_TypeSearch + '*';
            
            String Query = 'select Title__c,Region__c,Training_Location__c from Training_Course__c' ;
            String QueryCnt = 'select count() from Training_Course__c where Title__c != null'; //where id != null and RecordType.name = \'Product Document\''; 
            total_size = -1;
        
           
           
          if(Document_TypeSearch != '' && Document_TypeSearch!= null)
            {
                String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING Training_Course__c(id)';
                List<List<SObject>> searchList = search.query(searchquery);
                Training_Course__c[] cn = ((List<Training_Course__c>)searchList[0]);
                whr =  ' where id in : cn';
                total_size = cn.size(); 
            }        

            if(SelectedMenu <> null && SelectedMenu <> ''){
                
                
                if(whr <> '' && whr <> null){
                    Query += whr+' and Region__c = \''+SelectedMenu+'\' ';  //STSK0012511 - Rakesh
                    QueryCnt += whr+' and Region__c = \''+SelectedMenu+'\' ';  //STSK0012511 - Rakesh
                }
                else{
                    Query += ' where Region__c = \''+SelectedMenu+'\' ';
                    QueryCnt += ' and Region__c = \''+SelectedMenu+'\' ';  //STSK0012511 - Rakesh
                }
            }
            
            string orderby = ' order by ' + sortField + ' '+ sortDir;  //STSK0012511 - Rakesh
            Query = Query + orderby+' limit ' + list_size + ' offset ' +counter;
            if(total_size < 0)
            total_size = DataBase.countQuery(QueryCnt);
            List<Sobject> sobj = DataBase.Query(Query);
            //total_size = sobj.size();
            For(Sobject s : Sobj)
            {
                lContentVersions.add((Training_Course__c)s);
            }
            return lContentVersions;
      }
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
    }
     

  
  

 
    public PageReference reSet() { //user clicked beginning
     // getoptions();
      return null;
   }
 
   public PageReference Beginning() { //user clicked beginning
      counter = 0;
      return null;
   }
 
   public PageReference Previous() { //user clicked previous button
      counter -= list_size;     
      return null;
   }
 
   public PageReference Next() { //user clicked next button
      counter += list_size;     
      return null;
   }
 
   public PageReference End() { //user clicked end
      counter = total_size - math.mod(total_size, list_size);
      return null;
   }
 
   public Boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
   }
   
  //Toggles the sorting of query from asc<-->desc
    public void  toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
                            /*
                                // run the query again for sorting other columns
                                soqlsort = 'select Title, Mutli_Languages__c,Date__c,Document_Type__c,Document_Number__c,Document_Version__c from ContentVersion'; 

                                // Adding String array to a List array
                                System.debug('_______^^^^___ ' + soqlsort + ' order by ' + sortField + ' ' + sortDir );
                                ContentVersionsList = Database.query(soqlsort + ' order by ' + sortField + ' ' + sortDir ); */
    }

    // the current sort direction. defaults to asc
    public String sortDir {
        // To set a Direction either in ascending order or descending order.
                                get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;}
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField {
        // To set a Field for sorting.
                                get  { if (sortField == null) {sortField = 'Title__c'; } return sortField;  }
        set;
    } 

}
/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 12-June-2013
    @ Description   :  Test class for CpTraingandCourseController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest
    Public class CpTraingandCourseControllerTest
    {
            //Test method for CpTraingandCourseController class for Region equals NA
            
            Public static testmethod void TestCpTraingandCourseController()
            {
                User ur = [Select Id,Name from User where Id=:UserInfo.getUserId()];
                Training_Course__c tc = new Training_Course__c();
                tc.Title__c= 'Test';
                tc.Designed_for__c='xyx';
                tc.Replaces_Course__c ='test';
                tc.PreRequisites__c = 'sdsd';
                tc.Software_Version__c='3244';
                tc.Description__c = 'dfdfdf' ;
                tc.Accreditation__c='dffd';
                tc.FlexCredits__c='34';
                tc.Training_Location__c='California';
                PageReference pageRefrenc = Page.CpTrainingandCourse; 
                Test.setCurrentPage(pageRefrenc );
                ApexPages.currentPage().getParameters().put('TitleId' ,tc.id);
                
                //tc.Title__c= ur.Id;
                insert tc;
                Training_Dates__c TD=new Training_Dates__c();
                td.From__c=Date.Today();
                td.To__c=Date.Today();
                td.Training_Course__c=tc.id;
                insert td;
                
                
                User urs = [ select Id from User where Id =:UserInfo.getUserId() ];
                System.runAs (urs)
                {     
                    User u;
                    Account objAcc = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF', DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
                    insert objAcc ;
                    
                    Account a;      
                    Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                    a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
                    insert a;
                    
                    Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='USA', MailingState ='teststate');
                    insert con;
                    
                    Training_Registration__c tr=new Training_Registration__c();
                    tr.Contact__c=con.id;
                    tr.Payment_Information__c='testinfo';
                    PageReference pageRef = Page.CpTrainingandCourse; 
                    Test.setCurrentPage(pageref);
                    ApexPages.currentPage().getParameters().put('TitleId' ,tr.Training_Course__c);
                    insert tr;
                    u = new User(alias = 'standt', email='standarduser@testorg.com', 
                    emailencodingkey='UTF-8', lastname='Testing', 
                    languagelocalekey='en_US', 
                    localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
                    username='standarduser@testclass.com'/*,UserRoleid=r.id*/);
                    insert u;
                    
                    System.runAs (u)
                    {
                        Regulatory_Country__c regcount=new Regulatory_Country__c();
                        regcount.Name = 'USA';
                        regcount.Portal_Regions__c ='N. America';
                        insert regcount;
                        system.assertequals(regcount.Portal_Regions__c , 'N. America');
                        CpTraingandCourseController TrngCourse=new CpTraingandCourseController();
                        TrngCourse.verify();
                        TrngCourse.SubmitForEmail();
                        TrngCourse.getTraingCourseDate();
                        TrngCourse.getPre();
                        TrngCourse.readmessage();
                        TrngCourse.showrecords();
                        TrngCourse.showrecords1();
                        
                        TrngCourse.reSet();
                        TrngCourse.Beginning();
                        TrngCourse.Previous();
                        TrngCourse.Next();
                        TrngCourse.End();
                        TrngCourse.getDisableNext();
                        TrngCourse.getTotal_size();
                        TrngCourse.getDisablePrevious();
                        TrngCourse.getPageNumber();
                        TrngCourse.getTotalPages();
                } 
            }
        }
           
            //Test method for CpTraingandCourseController class for Region not equals NA
           
            Public static testmethod void TestCpTraingandCourseController1()
            {
                    User urs = [ select Id from User where Id =:UserInfo.getUserId() ];
                    System.runAs (urs)
                    {     
                            User u;
                             Account objAcc = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF', DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
                            insert objAcc ;
                            
                            Account a; 

                           
                            
                            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                            a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
                            insert a;
                            
                            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe',MailingState ='teststate');
                            insert con;
                            //UserRole r = [Select id from userrole where name='CEO'];
                            u = new User(alias = 'standt', email='standarduser@testorg.com', 
                            emailencodingkey='UTF-8', lastname='Testing', 
                            languagelocalekey='en_US', 
                            localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
                            username='standarduser@testclass.com'/*,UserRoleid=r.id*/);
                            insert u;
                    
                    System.runAs (u)
                    {
                            Regulatory_Country__c regcount=new Regulatory_Country__c();
                            regcount.Name = 'Zimbabwe';
                            regcount.Portal_Regions__c ='EMEA';
                            insert regcount;
                            system.assertequals(regcount.Portal_Regions__c , 'EMEA');
                            CpTraingandCourseController TrngCourse=new CpTraingandCourseController();
                            TrngCourse.verify();
                            List<SelectOption> sopt = TrngCourse.getoptions();
                    }
                }
            }
            
             //Test method for CpTraingandCourseController class 
            
            Public static testmethod void TestCpTraingandCourseController3()
            {
                CpTraingandCourseController TrngCourses=new CpTraingandCourseController();
                TrngCourses.verify();
            }
            
}
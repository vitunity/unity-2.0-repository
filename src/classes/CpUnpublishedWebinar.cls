/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 24-Apr-2013
    @ Description   : An Apex class to get Unpublished webinar data.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
global class CpUnpublishedWebinar implements Database.Batchable<sObject>{


    global final String Query;// = 'SELECT Id, From_date_c,Webinars,Active__c FROM Event_Webinar__c WHERE (From_date_c-Today()>365) and Active__c = true and Recordtype.Name=\'Webinars\'';
     public CpUnpublishedWebinar (String queryStr){
         Query = queryStr;
     }
    global Database.QueryLocator start(Database.BatchableContext BC){
         system.debug('Query  ---> ' + Query );
        system.debug('In Start  '  );
      return Database.getQueryLocator(Query );
   }
   global void execute(Database.BatchableContext BC, List<sObject> records){
       system.debug('In execute'  );
       
       
       List<Event_Webinar__c> updateWebinarsLst = new List<Event_Webinar__c> ();
       for(sObject rcd: records){
           Event_Webinar__c castRcd = new Event_Webinar__c();
           castRcd = ( Event_Webinar__c) rcd;
            castRcd.Active__c = false;
             castRcd.System_Deactivated__c = true;
           updateWebinarsLst.add(castRcd);
       }
       
       
       system.debug('Event_Webinar__c record about to update are -------- :' +  updateWebinarsLst);
       if(updateWebinarsLst.size() > 0){
           update updateWebinarsLst;
       }
   }
   global void finish(Database.BatchableContext BC){
              system.debug('In finish'  );
       /**for(records){
           
       }*/
   }
   
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 23-June-2013
    @ Description   :  Test class for CpUnpublishedWebinar batch class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class CpUnpublishedWebinartest{
        
       //Test Method for CpUnpublishedWebinar batch class
       
        static testmethod void testm1()
        {
            Test.StartTest();
              
              RecordType eventRT = [select Id , Name FROM RecordType WHERE Name ='Events' and SobjectType ='Event_webinar__c'];
                Event_Webinar__c event = new Event_Webinar__c();
                event.Title__c = 'Test Event2';
                event.Location__c = 'India';
                event.Banner__c='This banner created for test class and this data will not be commited to database because we are using starttest and stop test';
                event.To_Date__c = system.today()-1;
                event.From_Date__c = system.today()-2;
                event.Needs_MyVarian_registration__c=False;
                event.RecordTypeId = eventRT.Id; 
                Insert event;       
                    
             CpUnpublishedWebinar  sh1 = new CpUnpublishedWebinar ('select id from Event_Webinar__c');
             ID CpUnpublishedWebinar = Database.executeBatch(sh1,200);

                           Test.StopTest();
        }
 }
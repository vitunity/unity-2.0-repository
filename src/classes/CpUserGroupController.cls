/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 08-May-2013
    @ Description   : An Apex class to retrive Product Group data based on a logged in user prodcuts data.
    @ Last Modified By  :   Priyanka Dhamija
    @ Last Modified On  :   21-Aug-2014
    @ Last Modified Reason  :   Updated ARIA link and removed Eclipse and RapidArc groups
****************************************************************************/
public with sharing class CpUserGroupController {
    
    //Id accountId;
    set<String> userProds =  new set<string>();  
    public Boolean RO{get; set;}
    public Boolean MO{get; set;}
    public Boolean truebeam{get; set;}
    public String shows{get;set;}
    public String Usrname{get;set;}
    public Boolean IsVis{get; set;}
    
    public CpUserGroupController(){
        //fetch user products
        //userProds = Label.User_Group_Product.split(',');
        
        RO = false;
        MO = false;
        truebeam = false;
          //for internal user
        shows = 'none';
        User un =[Select contactid,alias from user where id =: Userinfo.getUserId() limit 1];
        if(un.contactid == null)
         {
            RO = true;
            MO = true;
            truebeam = true;
            Usrname = un.alias;
            shows = 'block';
            
          }      
        //set user permission
        setUserGroupPermission();
        
    }
    
        
    public void setUserGroupPermission(){
        
        /*
        accountId = [Select Id, AccountId From User where id = : Userinfo.getUserId() limit 1].AccountId;
        
        List<SVMXC__Installed_Product__c> insProdsList = [Select s.SVMXC__Product__r.Product_Group__c, 
                                                            s.SVMXC__Product__c, s.Id 
                                                            From SVMXC__Installed_Product__c s];
        */                                                  
       
       Set<String> productGroupSet = CpProductPermissions.fetchProductGroup();
       System.debug('&&&&&'+productGroupSet);
       
       if(productGroupSet.size() > 0){
            //for(SVMXC__Installed_Product__c ip : insProdsList){
                if(productGroupSet.contains(('ARIA Medical Oncology'))){
                    MO = true;
                    IsVis= true;
                }
                 
                if(productGroupSet.contains('ARIA Radiation Oncology')){
                    RO = true;
                    IsVis= true;
          
                }
                
                if(productGroupSet.contains('TrueBeam')){
                    truebeam = true;
                    IsVis= true;
                }                
                            //} 
       }
     
    }

}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for CpUserGroupController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest

Public class  CpUserGroupControllerTest{

    //Test Method for CpUserGroupController class
    
    static testmethod void testCpUserGroupController(){
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='test' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'); 
            
            insert u; // Inserting Portal User
            
             List<Product2> prod = new List<Product2>();
             
             Product2 prods = new Product2();
                prods.Name = 'ARIA for Medical Oncology';
                prods.Product_Group__c ='ARIA for Medical Oncology';
                prod.add(prods);
               
            Product2 prods1= new Product2();
                prods1.Name = 'ARIA Radiation Oncology';
                prods1.Product_Group__c ='ARIA Radiation Oncology';
                prod.add(prods1);
                
             Product2 prods2= new Product2();
                prods2.Name = 'Eclipse';
                prods2.Product_Group__c ='Eclipse';
                prod.add(prods2);
                
             Product2 prods3= new Product2();
                prods3.Name = 'RapidArc';
                prods3.Product_Group__c ='RapidArc';
                prod.add(prods3);
                
             Product2 prods4= new Product2();
                prods4.Name = 'TrueBeam';
                prods4.Product_Group__c ='TrueBeam';
                prod.add(prods4);
              
              insert prod;
                
                    
            List<SVMXC__Installed_Product__c> Insprd = new List<SVMXC__Installed_Product__c>();
            
            SVMXC__Installed_Product__c Inprd = new SVMXC__Installed_Product__c();
             Inprd.Name = 'Testing';
             Inprd.SVMXC__Product__c = prods.Id;
             Inprd.SVMXC__Serial_Lot_Number__c = 'Testing';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd);
            
            SVMXC__Installed_Product__c Inprd1 = new SVMXC__Installed_Product__c();
             Inprd1.Name = 'Testing';
             Inprd1.SVMXC__Product__c = prods1.Id;
             Inprd1.SVMXC__Serial_Lot_Number__c = 'Testing';
             Inprd.SVMXC__Status__c = 'Installed';
             Inprd.SVMXC__Company__c = con.AccountId;
             Insprd.add(Inprd1);
            
            SVMXC__Installed_Product__c Inprd2 = new SVMXC__Installed_Product__c();
             Inprd2.Name = 'Testing';
             Inprd2.SVMXC__Product__c = prods2.Id;
             Inprd2.SVMXC__Serial_Lot_Number__c = 'Testing';
             Inprd2.SVMXC__Status__c = 'Installed';
             Inprd2.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd2);
            
            SVMXC__Installed_Product__c Inprd3 = new SVMXC__Installed_Product__c();
             Inprd3.Name = 'Testing';
             Inprd3.SVMXC__Product__c = prods3.Id;
             Inprd3.SVMXC__Serial_Lot_Number__c = 'Testing';
             Inprd3.SVMXC__Status__c = 'Installed';
             Inprd3.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd3);
            
            SVMXC__Installed_Product__c Inprd4 = new SVMXC__Installed_Product__c();
             Inprd4.Name = 'Testing';
             Inprd4.SVMXC__Product__c = prods4.Id;
             Inprd4.SVMXC__Serial_Lot_Number__c = 'Testing';
             Inprd4.SVMXC__Status__c = 'Installed';
             Inprd4.SVMXC__Company__c = con.AccountId;
            Insprd.add(Inprd4);
            
          insert Insprd;
            
          }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
         
         CpUserGroupController UserGroup = new  CpUserGroupController();
         UserGroup.setUserGroupPermission();
         
        }
         CpUserGroupController UserGroup = new  CpUserGroupController();
         UserGroup.setUserGroupPermission();
         
         Test.StopTest();
     }
     
 }
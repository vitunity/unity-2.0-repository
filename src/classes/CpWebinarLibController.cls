/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 18-Apr-2013
    @ Description   : An Apex page to retrive List of Live Webinars and On-Demand Webinars.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
    @ Last Modified By  :   Rakesh Basani
    @ Last Modified On  :   16-Dec-2015
    @ Last Modified Reason  :   Included condition 'active =true' in the line 410 and 413.
****************************************************************************/
public with sharing class CpWebinarLibController {
    
    public Boolean isGuest{get;set;}    
    public String product{get;set;} 
    public String Document_TypeSearch{get;set;} 
    public List<Event_Webinar__c> liveWebinars;
    public static List<Event_Webinar__c> onDemandWebinars;
    List<Event_Webinar__c> liveWebinarsStore; 
    List<Event_Webinar__c> onDemandWebinarsStore; 
    public Event_Webinar__c featWebinars{get;set;} 
    string chkRegion=null;
    public  list<selectoption> lstContentDoctype{get;set;} 
    public string Usrname{get;set;}
    public string shows{get;set;}
    private integer counter=0;

    
    //constructor
    public CpWebinarLibController(){
        shows='none';
        //Product='--Any--';
        /*
        if(UserInfo.getUserType() == 'CspLitePortal'){
                isGuest = false;
            }else{
                isGuest = true;
            }  
        */
     

        isGuest =SlideController.logInUser();

        if(!isGuest)
        {
            String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
            if(conCntry != null)
            chkRegion = [Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry limit 1].Portal_Regions__c;
            system.debug('Region is -------------- -  ' + chkRegion );
        }
       if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
              Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
              shows='block';
              isGuest=false;
          }
       }
    
            Id webinarid = Apexpages.currentPage().getParameters().get('Id');
            if(webinarid != null){
            for(Event_Webinar__c fw : [Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,
                                            Speaker__c , Title__c, CreatedDate, Time__c, URL__c
                                                From Event_Webinar__c e 
                                                where recordtype.name = 'Webinars'  
                                                and id =:webinarid limit 1]){
                                    
                    if(fw!= null){
                             featWebinars = fw;
                    }
                }
        }else{
            
            //fetch webinars
            fetchWebinars();
            liveWebinarsStore = new list<Event_Webinar__c>();
            onDemandWebinarsStore = new list<Event_Webinar__c>();
            
            liveWebinarsStore.addAll(liveWebinars);
            onDemandWebinarsStore.addAll(onDemandWebinars);
        }
        
       
       
    }
    
        
    
    //fetch product values
    private void fetchWebinars(){
        liveWebinars = new list<Event_Webinar__c>();
        onDemandWebinars = new list<Event_Webinar__c>();
        set<String> productGroupSet = new set<String>();
        ProductGroupSet = CpProductPermissions.fetchProductGroup(); 
       
        System.debug('ProductSet--->'+ProductGroupSet);
        String Query=''; 
         List<Event_Webinar__c> RowsWebinar= new List<Event_Webinar__c>();
       
       Query = 'Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,Speaker__c , Title__c, CreatedDate, Time__c, URL__c,Product_Affiliation__c, Region__c,Restrict_access_based_on_product_profile__c From Event_Webinar__c e where recordtype.name =\'Webinars\'   and Active__c = true and private__c = false order by From_Date__c desc';

       
       System.debug('Query--->'+Query);
       RowsWebinar=DataBase.Query(Query);
        for(Event_Webinar__c fw : RowsWebinar)
                                            
                                            {
           
           if(fw.Product_Affiliation__c != null && fw.Product_Affiliation__c.contains('Government Affairs')){continue;}
          
           if(fw.From_Date__c >= System.today()){
                if(chkRegion==null){ //&& fw.Restrict_access_based_on_product_profile__c == false)
                if(fw.Restrict_access_based_on_product_profile__c == false && shows!='block')
                liveWebinars.add(fw);
                 if(shows =='block' )
                liveWebinars.add(fw);
                 
                }
                else
                {
                    if(fw.Region__c != null){
                    system.debug('------------region-----'+fw.Region__c);
                    system.debug('if value' + fw.Region__c.contains('All'));
                    if(fw.Region__c.contains('All') || fw.Region__c.contains(chkRegion))
                    if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block' )
                    liveWebinars.add(fw);
                     if(fw.Restrict_access_based_on_product_profile__c == true && shows !='block'  )
                   {
                   for(String Str:ProductGroupSet)
                   {
                   if(fw.Product_Affiliation__c.contains(str))
                    {
                    liveWebinars.add(fw);
                    break;
                    }
                   }
                 }  
                    }
                }
           }
           
           if(fw.From_Date__c < System.today())
           {
                System.debug('CheckRegin'+chkRegion);
                if(chkRegion==null){// && fw.Restrict_access_based_on_product_profile__c == false)
               if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block' )
                onDemandWebinars.add(fw);
                
               if(shows =='block' )
                onDemandWebinars.add(fw);
                  
                }
                else
                {
                  if(fw.Region__c != null){
                   if(fw.Region__c.contains('All') || fw.Region__c.contains(chkRegion))
                   if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block'  )
                   {
                    onDemandWebinars.add(fw);
                    }
                   
                   if(fw.Restrict_access_based_on_product_profile__c == true && shows !='block'  )
                   {
                   for(String Str:ProductGroupSet)
                   {
                   if(fw.Product_Affiliation__c.contains(str))
                    {
                    onDemandWebinars.add(fw);
                    break;
                    }
                   }
                 }   
                    }
              }               
                con = new ApexPages.StandardSetController(onDemandWebinars);
                con2 = new ApexPages.StandardSetController(livewebinars); 
           }
                                                
        }
        
        System.debug('aebug ; liveWebinars '+ liveWebinars);
       
       
    }
    Public List<SelectOption> prods = new List<SelectOption>();
    public void  setprods(List<SelectOption> prod)
    {
        prods=prod;
    }
    Public List<SelectOption> getProds(){
       Prods.clear();
    prods.add(new selectOption('1Any', '- Any -')); 
  //List<selectoption> Prod=new List<selectoption>();
       
         Set<String> selectedItems = new Set<String>();
        for(Event_Webinar__c fw : [Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,
                                        Speaker__c , Title__c, CreatedDate, Time__c, URL__c,Product_Affiliation__c, Region__c,Restrict_access_based_on_product_profile__c
                                            From Event_Webinar__c e 
                                            where recordtype.name = 'Webinars'and Active__c = true and private__c = false
                                            order by From_Date__c desc])
                                          
                                            {
                                            
                                            if(fw.Product_Affiliation__c!=null)
                                            {
                                            
                                            if(fw.Product_Affiliation__c.contains(';') )
                                            {
                                            List<String> parts = fw.Product_Affiliation__c.split(';');
                                            System.debug('Prodsss-->'+parts );
                                            for(String temp:parts)
                                            {
                                             System.debug('testsssss-->'+selectedItems);
                                            System.debug('testsssss-->'+temp);
                                           if (selectedItems.contains(temp))
                                           {
                                           }
                                            else
                                           {
                                            Prods.add(new selectoption(temp,temp)); 
                                             selectedItems.add(temp);                 
                                           }
                                           }
                                            }
                                            else
                                            {
                                            if(selectedItems.contains(fw.Product_Affiliation__c))
                                            {
                                            }
                                            else
                                            {
                                            Prods.add(new selectoption(fw.Product_Affiliation__c,fw.Product_Affiliation__c));
                                            selectedItems.add(fw.Product_Affiliation__c);
                                            }
                                            }
                                           
                                    
                }
               
                }
                   
                  Prods.sort();

 return prods;
 }
 /*  public List<selectOption> getProds() {
        List<selectOption> options = new List<selectOption>(); 
        set<string> ProductGroupSet = CpProductPermissions.fetchProductGroup();  
        options.add(new selectOption('Any', '- Any -')); 
        
        for(string sr : ProductGroupSet) {
        options.add(new selectoption(sr, sr)); }
        return options;
    }*/
    
    //filter logic
    
    
    
    Public PageReference filterWebinars(){
            System.debug('aebug : product = '+product);
            
            
            liveWebinars = new list<Event_Webinar__c>();
            onDemandWebinars = new list<Event_Webinar__c>();
            
            if(product == '1Any'){
                liveWebinars.addAll(liveWebinarsStore);
                onDemandWebinars.addAll(onDemandWebinarsStore);
                con = new ApexPages.StandardSetController(onDemandWebinars);
                con2 = new ApexPages.StandardSetController(livewebinars); 
                return null;
            }
            
            for(Event_Webinar__c ew : liveWebinarsStore){
                System.debug('aebug : ew.Product_Affiliation__c = '+ew.Product_Affiliation__c);
                if(product != '1Any' && ew.Product_Affiliation__c!=null && ew.Product_Affiliation__c.contains(product)){
                    liveWebinars.add(ew);
                }
            }
            
            for(Event_Webinar__c ows : onDemandWebinarsStore){
                if(product != '1Any' && ows.Product_Affiliation__c != null && ows.Product_Affiliation__c.contains(product)){
                    onDemandWebinars.add(ows);                  
                }
            }
            
            if(onDemandWebinars == null || onDemandWebinars.size() == 0){
                con = new ApexPages.StandardSetController(new list<Event_Webinar__c>());
                con2 = new ApexPages.StandardSetController(livewebinars); 
            }else{
                con = new ApexPages.StandardSetController(onDemandWebinars);
                con2 = new ApexPages.StandardSetController(livewebinars); 
            }
            
            System.debug('aebug = live webinars = '+onDemandWebinars);
            System.debug('aebug = Con = '+(List<Event_Webinar__c>) con.getRecords());
            
            return null;
    }
    
    //pagination
    
    // Initialize setCon and return a list of records
   
    /*public List<Event_Webinar__c> getonDemandWebinars() {
                 
        //system.debug('aebug ; getonDemandWebinars == '+ (List<Event_Webinar__c>) con.getRecords());
        //system.debug('aebug ; ondemand == '+ onDemandWebinars.size());
        return (List<Event_Webinar__c>) con.getRecords();
    }*/
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con {
        get {
            if(con == null) {
                con = new ApexPages.StandardSetController(onDemandWebinars);
                // sets the number of records in each page set
                con.setPageSize(20);
            }else{
                con.setPageSize(20);
            }
            return con;
        }
        set;
    }
        public ApexPages.StandardSetController con2 {
        get {
            if(con2 == null) {
                con2 = new ApexPages.StandardSetController(livewebinars);
                // sets the number of records in each page set
                con2.setPageSize(20);
            }else{
                con2.setPageSize(20);
            }
            return con2;
        }
        set;
    }
    
        // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
            }
        set;
    }
 
    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
 
    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }   
    
    
    // returns the first page of records
    public void first() {
        con.first();
    }
 
    // returns the last page of records
    public void last() {
        con.last();
    }
 
    // returns the previous page of records
    public void previous() {
        con.previous();
    }
 
    // returns the next page of records
    public void next() {
        //pageNumber = pageNumber + 1;      
        con.next();
    }
    
    // returns the PageReference of the original page, if known, or the home page.
    public void cancel() {
        con.cancel();
    }
    
    public PageReference reset() { //user clicked beginning
     Product='--Any--';  
     fetchWebinars();
     Document_TypeSearch = '';
     Return null;
     }
     
     //Code for Apply button
    public List<Event_Webinar__c> getonDemandWebinars(){
        Event_Webinar__c[] cn ;
        String searchquery = '';
        List<Event_Webinar__c> RowsWebinar;
        onDemandWebinars = new List<Event_Webinar__c>();
        liveWebinars = new List<Event_Webinar__c>();         
        List<SObject> lst = new List<SObject>();
        
        String temp = '%' + Document_TypeSearch + '%';
        
        if(Document_TypeSearch != '' && Document_TypeSearch!= null)
        {
            if(Product != '1Any'){
                searchquery = 'Select e.Institution__c, e.From_Date__c,Speaker__c , Title__c,Product_Affiliation__c From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
            }
            else{
                searchquery = 'Select e.Institution__c, e.From_Date__c,e.Speaker__c , e.Title__c,e.Product_Affiliation__c From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
            }
            RowsWebinar = DataBase.Query(searchquery);
            for(Event_Webinar__c st : RowsWebinar){
                if(st.From_Date__c < System.today())
                {
                    onDemandWebinars.add(st);
                }
             }
             for(Event_Webinar__c st : RowsWebinar){
                if(st.From_Date__c >= System.today())
                {
                    liveWebinars.add(st);
                }
             }   
  
                   return onDemandWebinars;
        }
     else{
             return (List<Event_Webinar__c>) con.getRecords();
         } 
    }   
    public void setliveWebinars(List<Event_Webinar__c> lstWebinar)
    {
        lstWebinar = liveWebinars;
    }
    public List<Event_Webinar__c> getliveWebinars(){
        Event_Webinar__c[] cn ;
        String searchquery = '';
        List<Event_Webinar__c> RowsWebinar;
        onDemandWebinars = new List<Event_Webinar__c>();
        liveWebinars = new List<Event_Webinar__c>();         
        List<SObject> lst = new List<SObject>();
        
        String temp = '%' + Document_TypeSearch + '%';
        
        if(Document_TypeSearch != '' && Document_TypeSearch!= null)
        {
            if(Product != '1Any'){
                searchquery = 'Select e.Institution__c, e.From_Date__c,Speaker__c , e.description__c,  Title__c,Product_Affiliation__c From Event_Webinar__c e where recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
            }
            else{
                searchquery = 'Select e.Institution__c, e.From_Date__c,e.Speaker__c , e.description__c, e.Title__c,e.Product_Affiliation__c From Event_Webinar__c e where recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
            }
            RowsWebinar = DataBase.Query(searchquery);
            for(Event_Webinar__c st : RowsWebinar){
                if(st.From_Date__c < System.today())
                {
                    onDemandWebinars.add(st);
                }
             }
             for(Event_Webinar__c st : RowsWebinar){
                if(st.From_Date__c >= System.today())
                {
                    liveWebinars.add(st);
                }
             }
                   return liveWebinars;
        }
     else{
             return (List<Event_Webinar__c>) con2.getRecords();
         } 
    }  
    public PageReference showrecords() 
    {
           return null;
    }
      public PageReference Webinars() 
      {
           if(Document_TypeSearch != '' && Document_TypeSearch!= null)
           {
               
               getonDemandWebinars();
               getlivewebinars();
           }
           else{
               filterWebinars();
           }
           return null;
      }
}
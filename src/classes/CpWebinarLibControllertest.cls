/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 21-June-2013
    @ Description   :  Test class for CpWebinarLibController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest
    Public class CpWebinarLibControllertest{
    
     //Test Method for CpWebinarLibController class
     
     static testmethod void testCpWebinarLibController()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                user u;
                Account a;
                
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MailingState='UP',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test'); 
                insert con;
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
               Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Test';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
                
                 }
         
         user usr = [Select Id, ContactId,Contact.Date_of_Factory_Tour_Visit__c,Contact.MailingCountry ,Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
                 
                  RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
                    Event_Webinar__c Webinar = new Event_Webinar__c();
                        Webinar.RecordTypeId = RT.Id; 
                        Webinar.Title__c = 'Test Webinar';
                        Webinar.Product_Affiliation__c = '4DITC';
                        Webinar.Location__c = 'NY';
                        Webinar.To_Date__c = system.today();
                        Webinar.From_Date__c = system.today();
                        Webinar.Needs_MyVarian_registration__c=False;
                       insert Webinar;
                       
                 PageReference pageref = Page.CpWebinarLib;
                   Test.setCurrentPage(pageRef);
                   usr.Contact.Date_of_Factory_Tour_Visit__c = System.Today();
                   usr.Contact.MailingCountry = 'India';
                   update  usr.contact;
                   
                   Regulatory_Country__c Rc= new Regulatory_Country__c ();
                    Rc.Name = usr.Contact.MailingCountry;
                    Rc.RA_Region__c = '';
                    Rc.Portal_Regions__c = '';
                    Insert Rc;
                    
                    CpWebinarLibController  Web =new CpWebinarLibController ();
                    Web.product = '4DITC';
                    Web.filterWebinars();
                    List<SelectOption> selopt = Web.getProds();
                    List<SelectOption> obj = new List<SelectOption>();
                    Web.setprods(obj);
                    Web.getonDemandWebinars();
                    Web.first();
                    Web.last();
                    Web.previous();
                    Web.next();
                    Web.cancel();
                    
                    
                    
                     }
            Test.StopTest();
        }
        
        
        //Test Method for CpWebinarLibController class
        
        static testmethod void testCpWebinarLibControllermethods()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a; 
                
                
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;    
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MailingState='UP',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test' ); 
                insert con;
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Test';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
                   
                 }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
                
                RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
                    Event_Webinar__c Webinar = new Event_Webinar__c();
                        Webinar.RecordTypeId = RT.Id; 
                        Webinar.Title__c = 'Test Webinar';
                        Webinar.Product_Affiliation__c = '4DITC';
                        Webinar.Location__c = 'NY';
                        Webinar.To_Date__c = system.today();
                        Webinar.From_Date__c = system.today();
                        Webinar.Needs_MyVarian_registration__c=False;
                       insert Webinar;
                    Apexpages.currentPage().getParameters().put('Id',Webinar.id);

                
                 PageReference pageref = Page.CpWebinarLib;
                    Test.setCurrentPage(pageRef);
                    
                    CpWebinarLibController  Web =new CpWebinarLibController ();
                    Web.product = '4DITC';
                    Web.filterWebinars();
                    List<SelectOption> selopt = Web.getProds();
                    
                    Web.getonDemandWebinars();
                    Web.first();
                    Web.last();
                    Web.previous();
                    Web.next();
                    Web.cancel();
                    
                   
                     }
            Test.StopTest();
        }
        
        //Test Method for CpWebinarLibController class
        
        static testmethod void testCpWebinarLibControllers()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a;  
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MailingState='UP',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test' ); 
                insert con;
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Test';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
                      
                      
                }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
                
                        RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
                        Event_Webinar__c Webinar = new Event_Webinar__c();
                        Webinar.RecordTypeId = RT.Id; 
                        Webinar.Title__c = 'Test Webinar';
                        Webinar.Product_Affiliation__c = '4DITC';
                        Webinar.Location__c = 'NY';
                        Webinar.To_Date__c = system.today();
                        Webinar.From_Date__c = system.today()-2;
                        Webinar.Needs_MyVarian_registration__c=False;
                       insert Webinar;
                       
                 PageReference pageref = Page.CpWebinarLib;
                    Test.setCurrentPage(pageRef);
                    
                    CpWebinarLibController  Web =new CpWebinarLibController ();
                    Web.product = 'Any';
                    Web.filterWebinars();
                    List<SelectOption> selopt = Web.getProds();
                    Web.getonDemandWebinars();
                    Web.first();
                    Web.last();
                    Web.previous();
                    Web.next();
                    Web.cancel();
                    
                   
                    
                     }
            Test.StopTest();
        }
        
        // Test Method for CpWebinarLibController class
        
        static testmethod void testCpWebinarLibControllermethod()
        {
            Test.StartTest();
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a; 
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MailingState='UP',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test' ); 
                insert con;
                
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Test';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
                      
                 }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
         
                 PageReference pageref = Page.CpWebinarLib;
                    Test.setCurrentPage(pageRef);
                    
                    RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
                    Event_Webinar__c Webinar = new Event_Webinar__c();
                        Webinar.RecordTypeId = RT.Id; 
                        Webinar.Title__c = 'Test Webinar';
                        Webinar.Product_Affiliation__c = '4DITC';
                        Webinar.Location__c = 'NY';
                        Webinar.To_Date__c = system.today();
                        Webinar.From_Date__c = system.today()-2;
                        Webinar.Needs_MyVarian_registration__c=False;
                       insert Webinar;
                    
                     ApexPages.currentPage().getParameters().put('Id',Webinar.id);
                     CpWebinarLibController  Web =new CpWebinarLibController ();
                     
                     }
            Test.StopTest();
        }
        
         
         //Test Method for CpWebinarLibController class
         
         static testmethod void testCpWebinarLibControllermethods1()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a; 
                
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;    
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,MailingState='UP',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test' ); 
                insert con;
                u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
                
                insert u; // Inserting Portal User
                
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name = 'Test';
                regcount.RA_Region__c ='NA';
                regcount.Portal_Regions__c ='N.America,Test';
                insert regcount;
                   
                 }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){
                
                RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
                    Event_Webinar__c Webinar = new Event_Webinar__c();
                        Webinar.RecordTypeId = RT.Id; 
                        Webinar.Title__c = 'Test Webinar';
                        Webinar.Product_Affiliation__c = '4DITC';
                        Webinar.Location__c = 'NY';
                        Webinar.To_Date__c = system.today();
                        Webinar.From_Date__c = system.today()-2;
                        Webinar.Needs_MyVarian_registration__c=False;
                       insert Webinar;
                
                 Apexpages.currentPage().getParameters().put('Id',Webinar.id);
                 PageReference pageref = Page.CpWebinarLib;
                    Test.setCurrentPage(pageRef);
                    
                    CpWebinarLibController  Web =new CpWebinarLibController ();
                    Web.product = '4DITC';
                    Web.filterWebinars();
                    List<SelectOption> selopt = Web.getProds();
                    Web.getonDemandWebinars();
                    Web.first();
                    Web.last();
                    Web.previous();
                    Web.next();
                    Web.cancel();
                    
                    
                     }
            Test.StopTest();
        }

  }
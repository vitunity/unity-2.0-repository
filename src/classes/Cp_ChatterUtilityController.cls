public class Cp_ChatterUtilityController {

    public Id userId {get;set;}
    public Boolean isUserPhotoUpload {get;set;}
    public Boolean isUserDeleteVis {get;set;}

    private String showComponent;
    public User profileImageUrl    { get; set; }
  public Pagereference DeletPic()
   {
   
 //  String communityId = '09aE00000005tBdIAI';
 String communityId = Network.getNetworkId();
   String userId= Apexpages.currentPage().getParameters().get('Id');
   System.debug('%%%%%%%%%-->'+userId);
     ConnectApi.ChatterUsers.DeletePhoto(communityId,userId);
             profileImageUrl = [select FullPhotoUrl from User where Id =: userId ];
              System.debug('%%%%%%%%%'+profileImageUrl );
      return null;
     
     /*  User userupdate = [Select FullPhotoUrl  from user where id =:Userinfo.getUserId() limit 1];
       user usr= new user(id = userupdate.id,FullPhotoUrl = '');
       
       update usr;*/
   }
    public Cp_ChatterUtilityController() {
        userId = Apexpages.currentPage().getParameters().get('Id');
        profileImageUrl = [select FullPhotoUrl from User where Id =: userId ];
        if(profileImageUrl.FullPhotoUrl.contains('profilephoto/005'))
        {
          isUserDeleteVis=false;
        }
        else
        {
        isUserDeleteVis =true;
        }
        showComponent = Apexpages.currentPage().getParameters().get('component');
           
        userId = userId == null ? UserInfo.getUserId() : userId;
         
        if(showComponent != null) {
            if(showComponent.equals('UserPhotoUpload')) {
                isUserPhotoUpload = true;
            } 
         
        }
    }
}
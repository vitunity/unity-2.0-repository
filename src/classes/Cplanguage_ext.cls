/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 05-May-2013
    @ Description   :An Apex controller related to complaints for different languages
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
Public class Cplanguage_ext{
String recid;
public List<String> languagelist{get;set;}
public Complaint__c complaintrec{get;set;}
public String s{get;set;}

//Constructor
  public Cplanguage_ext(){
     recid = ApexPages.CurrentPage().getParameters().get('id');
     date d = system.today();
     s = d.format();
     complaintrec = [Select Name,id,Author__c,Language__c,PNL_Subject__c from Complaint__c where id =: recid];
     if(complaintrec.Language__c.contains(','))
     {
        languagelist = complaintrec.Language__c.split(',');
     }
     else
     {
      languagelist.add(complaintrec.Language__c);
     }
    }  

}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for  Cplanguage_ext class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(SeeAllData=true)

Public class Cplanguage_extTest{

    //Test Method for initiating Cplanguage_ext class
    
    static testmethod void testCplanguage_ext(){
        Test.StartTest();
        Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
       
        Complaint__c complaint = new Complaint__c();
        complaint.Name ='test';
        complaint.Author__c = 'Test Author';
        complaint.Language__c ='English,Dutch';
        complaint.PNL_Subject__c ='Test class';
        Insert complaint;
        ApexPages.currentPage().getParameters().put('id' ,complaint.id);
        Cplanguage_ext lang = new Cplanguage_ext();
           
         Test.StopTest();
     }
     
    
 }
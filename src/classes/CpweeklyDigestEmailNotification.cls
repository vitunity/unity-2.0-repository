/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 10-May-2013
    @ Description   : An Apex class to get data from Content that is to be displayed in 
    Visualforce Email Template that is sent to customers on Weekely basis.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
    
Change Log:
7-Nov-2017 - Divya Hargunani - INC4701142\STSK0013311: My Notifications Email not being sent to MyVarian Users - added a filter to only process contents modified in last week
****************************************************************************/
global with sharing  class CpweeklyDigestEmailNotification
{
    public List<ContentVersion> documnets{get;set;}
    Public set<string> productGroupSet;
    Private set<String> documentTypes = new set<String>{'Safety Notifications', 'CTBs', 'Release Notes'};
    public string iscustomer{get;set;}
    public Id contactid{get;set;} 
    public CpweeklyDigestEmailNotification() {
       documnets= new List<ContentVersion>();
       productGroupSet=new set<string>();
    }

    global List<ContentVersion> getCpweeklyDigest() 
    {
        documnets= new List<ContentVersion>();
        List<Contact> contactidfortest = new List<Contact>([Select id,accountid from contact where id=:contactid]);
            if(contactidfortest.size() > 0){
                iscustomer = 'Yes';
                Id accountId=[select id ,accountid from contact where id=:contactid].accountid;
                for(SVMXC__Installed_Product__c ip :[Select s.SVMXC__Product__r.Product_Group__c, 
                                                                s.SVMXC__Product__c, s.Id, s.SVMXC__Company__c 
                                                                From SVMXC__Installed_Product__c s 
                                                                where s.SVMXC__Company__c =: accountId and 
                                                                SVMXC__Product__r.Product_Group__c != null]
                                                                )
                {                                                                   
                    productGroupSet.addAll(ip.SVMXC__Product__r.Product_Group__c.split(';')); 
                }
            }else{ 
                Schema.DescribeFieldResult fieldResult = Product2.Product_Group__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple)
                {
                    productGroupSet.add(f.getValue());
                }       
            }

        set<string> stContentDocId=new set<string> ();
        for(ContentWorkspaceDoc var:[Select c.SystemModstamp, c.IsOwner, c.IsDeleted, c.Id, c.CreatedDate, 
                                        c.ContentWorkspace.Name, c.ContentWorkspaceId, c.ContentDocumentId From ContentWorkspaceDoc c 
                                        where ContentWorkspace.Name IN :  productGroupSet
                                        and ContentDocument.ContentModifiedDate = Last_Week]){
            stContentDocId.add(var.ContentDocumentId);
        }

        if(stContentDocId.size()>0)
        {
            documnets = [select id,Title, Document_Type__c, Date__c 
                           from ContentVersion where 
                           recordtype.name != 'Marketing Kit' 
                           and Document_Language__c = 'English'
                           and islatest=true
                           and Document_Type__c in : documentTypes 
                           and (ContentDocumentId in : stContentDocId) 
                           and  (ContentModifiedDate =Last_Week OR CreatedDate =Last_Week) 
                           Order by  Date__c Desc];
        }
        
        return documnets ;
    }
}
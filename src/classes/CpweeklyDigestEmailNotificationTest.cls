/*************************************************************************\
Change Log:
7-Nov-2017 - Divya Hargunani - INC4701142\STSK0013311: My Notifications Email not being sent to MyVarian Users - Increased the code coverage
****************************************************************************/

@isTest(SeeAllData = true)
Public class CpweeklyDigestEmailNotificationTest{
    static testmethod void testCpweeklyDigestEmailNotification(){ 
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState'); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser1@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            insert u; 
        
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            prod.ProductCode = '12345';
            insert prod;
        
            Product2 prod1 = new Product2();
            prod1.Name = 'Acuity';
            prod1.Product_Group__c ='ARIA';
            prod1.ProductCode = '12345';
            insert prod1;
              
            SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
            Insprd.Name = 'Testing';
            Insprd.SVMXC__Product__c = prod.Id;
            Insprd.SVMXC__Serial_Lot_Number__c = 'Test';
            Insprd.SVMXC__Status__c = 'Installed';
            Insprd.SVMXC__Company__c = con.AccountId;
            Insert Insprd;
          
            CpweeklyDigestEmailNotification objweek=new CpweeklyDigestEmailNotification();
            list<id> testct=new list<id>();
            testct.add(con.id);
            objweek.Contactid=con.id;
            List<ContentVersion> testcont=objweek.getCpweeklyDigest();
            Test.StopTest();
        }
    }
    
    static testmethod void testCpweeklyDigestEmailNotification1(){ 
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState'); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser1@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            insert u; 
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            prod.ProductCode = '12345';
            insert prod;
        
            Product2 prod1 = new Product2();
            prod1.Name = 'Acuity';
            prod1.Product_Group__c ='ARIA';
            prod1.ProductCode = '12345';
            insert prod1;
              
            SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
            Insprd.Name = 'Testing';
            Insprd.SVMXC__Product__c = prod.Id;
            Insprd.SVMXC__Serial_Lot_Number__c = 'Test';
            Insprd.SVMXC__Status__c = 'Installed';
            Insprd.SVMXC__Company__c = con.AccountId;
            Insert Insprd;
          
        CpweeklyDigestEmailNotification objweek=new CpweeklyDigestEmailNotification();
        list<id> testct=new list<id>();
        testct.add(con.id);
        List<ContentVersion> testcont=objweek.getCpweeklyDigest();
        Test.StopTest();
        }
    }
}
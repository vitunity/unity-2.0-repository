public class CreateClearanceDocumentLinks
{
    public String SelReviewProduct{get;set;}
    public String SelCountry{get;set;}
    public List<selectoption> CountryValues {get; set;}
    public List<selectoption> ReviewProductValues{get; set;} 
    public Boolean showSection {get;set;}
    public String docLinkName{get;set;}
    public String docLink{get;set;}
    public String description{get;set;}
    public String certId{get;set;}
    public  List<InputClreancewrapper> InputClearanceWrapperData {get;set;}
    public list<Input_Clearance__c> allInputClr{get;set;}        
    public List<Clearance_Documents_Link__c> ClrDocLinklist{get;set;}    
    
    
    public List<InputClreancewrapper> process_List = new  List<InputClreancewrapper>();
    public List<InputClreancewrapper> SelectedList = new  List<InputClreancewrapper>();
    public Input_Clearance__c inputClearance;

    
    public CreateClearanceDocumentLinks(ApexPages.StandardSetController controller) {
        
        
        CountryValues = new List<selectoption>() ;
        ReviewProductValues= new List<selectoption>() ;
        CountryValues.add(new SelectOption('','--All--'));
        ReviewProductValues.add(new SelectOption('','--All--')); 
        List<Country__c> listAllCont =   [Select id,Name from Country__c order by Name];
        if(listAllCont !=null)
        {
            for(Country__c objCon : listAllCont )
            {
                CountryValues.add(new SelectOption(objCon.Id,objCon.Name));
            }
        }
        
        List<Review_Products__c> listAllProd = [Select id,Name, Product_Profile_Name__c from Review_Products__c order by Product_Profile_Name__c ];
        if(listAllProd !=null )
        {
            for(Review_Products__c objProd : listAllProd )
            {
                ReviewProductValues.add(new SelectOption(objProd.Id,objProd.Product_Profile_Name__c ));
            }
        }
        
    }
    public PageReference PopulateInputClearanceData() 
    {
        showSection = true;
        system.debug('Country Name>>>>'+SelCountry);
        system.debug('Review Product >>>>'+SelReviewProduct);
        InputClearanceWrapperData = new List<InputClreancewrapper>();
        List<Input_Clearance__c> listInputClr = new List<Input_Clearance__c>();
        integer i= 0;
         if( (SelReviewProduct == null || SelReviewProduct == '') && (SelCountry== null || SelCountry== ''))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select at least one Product Profile or Country.'));
            return null;
        }
        if( SelReviewProduct == null && SelCountry!=null )
        {
            listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c from Input_Clearance__c where  Country__c=:SelCountry ]; 
        }
        else if(SelCountry== null && SelReviewProduct!=null )
        {
            listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c from Input_Clearance__c where  Review_Product__c=:SelReviewProduct ]; 
        }
        // if(SelCountry!=null && SelReviewProduct!=null)
        else
        {
            listInputClr =  [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c,Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c from Input_Clearance__c where Review_Product__c=:SelReviewProduct and Country__c=:SelCountry];
        }
        
        for(Input_Clearance__c objInpClr : listInputClr)
        {
            InputClearanceWrapperData.add(new InputClreancewrapper(objInpClr,i));
            i= i+1;
        }
        return null;
        
        
    }    

    /*  public List<InputClreancewrapper> getprocessList()
        {

        for(Input_Clearance__c bi: allInputClr)
        process_List.add(new InputClreancewrapper(bi));          
        return process_List;        
        }*/
    
    public class InputClreancewrapper    
    {       public Input_Clearance__c InpClr{get; set;}     
        public Boolean selected {get; set;}
        public String docLinkName1{get;set;}
        public String docLink1{get;set;}
        public String description1{get;set;}
        public String certId1{get;set;}
        public Integer intRowNo{get; set;}
        // public Integer serial{get; set;}
        public InputClreancewrapper(Input_Clearance__c inc,Integer serial)     
        {            
            InpClr = inc;            
            selected = false;   
            docLinkName1= '';
            docLink1 = '';
            description1 = '';
            certId1 = '';  
            // serial = inc.id;
            intRowNo =serial;
        }    
    }
    
    /* public void ApplyToSelected()
    {
        system.debug('Inside Apply to Selected');
        
        for(InputClreancewrapper ps : SelectedList)
            {
                    system.debug('Inside for loop');
                    if(ps.selected ==true)
                    {
                    system.debug('Inside Selected');
                        ps.docLinkName1 = docLinkName;
                        ps.docLink1 = docLink ;
                        ps.description1 = description ;
                        ps.certId1= certId;
                    }
                } 
    
    }*/
    
    public Pagereference GetSelected()
    {
        
        SelectedList.clear();
        for(InputClreancewrapper accwrapper : InputClearanceWrapperData)
        if(accwrapper.selected == true)
        {
            
            SelectedList.add(accwrapper);
            System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>'+SelectedList.size());
        }
        return null;
        
    }
    
    public PageReference save()
    {
        system.debug('inside save function');
        GetSelected();
        List<Clearance_Documents_Link__c> insertList = new List<Clearance_Documents_Link__c>();
        
        System.Debug('>>>>>>>>>>>>>>>>>> Ritika Test >>>>>>. 11111'); 
        for(InputClreancewrapper ps : SelectedList)
        {
            if(ps.selected==true)
            {
                system.debug('inside selected');
                Clearance_Documents_Link__c objClrDoc = new Clearance_Documents_Link__c();
               objClrDoc.Clearance_Entry_Form__c = ps.InpClr.Id;
                objClrDoc.Clearance_Documents_Link__c = ps.docLink1 ;
                objClrDoc.Name = ps.docLinkName1;
               // objClrDoc.Description__c = ps.description1;
                objClrDoc.Certificate_Id__c = ps.certId1;
                insertList.add(objClrDoc);
                System.Debug('>>>>>>>>>>>>>>>>>> Ritika Test' + insertList);  
                
            }
            
        }                          
        insert insertList; 
        PageReference pageRef = null;
        try
        {
            pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }
        catch(Exception ex)
        {
            // do nothing
        }
        return pageRef;
       // System.Debug('>>>>>>>>>>>>>>>>>> Ritika Test' + insertList);                
       //return null;
    } 
    
    
}
/********************************************************************************************************
@Author:Praveen Musunuru
@Date: May 23,2014
@Description: This controller is designed to search clearance entry form details based on clerance family
and country to add Clearance Document Links
@Last Modified by: Praveen Musunuru
@Last Modified on: Aug 28,2014
@Last modified Reason: Linked Business Unit to product Bundle and Clearance Family

Change log -
26-Dec-17 - STSK0013504 - Nilesh Gorle - Add two new options in business unit dropdown i.e. 'VBT - 3rd Party' and 'VOS - 3rd Party'
*********************************************************************************************************/

public class CreateClearanceFamilyDocumentLinks
{
   
    public  String SelClearanceFamily{get;set;}
    public Boolean showSection {get;set;}
    public String docLinkName{get;set;}
    public String docLink{get;set;}
    public String description{get;set;}
    public String certId{get;set;}
    public List<InputClreancewrapper> InputClearanceWrapperData {get;set;}
    public list<Input_Clearance__c> allInputClr{get;set;}        
    public List<Clearance_Documents_Link__c> ClrDocLinklist{get;set;}    
    public List<InputClreancewrapper> process_List = new  List<InputClreancewrapper>();
    public List<InputClreancewrapper> SelectedList = new  List<InputClreancewrapper>();
    public Input_Clearance__c inputClearance;
    public List<selectoption> allCountriesValues {get; set;}
    public List<selectoption> selectedCountriesValues {get; set;}
    Public boolean checked{get;set;}
    
    public List<selectoption> allProductProfilesValues {get; set;}
    public List<selectoption> selectedProductProfileValues {get; set;}
    
    public  String SelProductBundle{get;set;}
    public List<selectoption> productBundleValues{get;set;}
    public  String selCoutriesFromTxt{get;set;}
    public List<selectoption> businessUnitValues {get; set;}
    public String selectedBusinessUnit{get;set;}
    public String selBusinessUnit{get;set;}
    //public transient  List<selectoption> ClearanceFamilyValues {get;set;}
    
/**public List<selectoption> ClearanceFamilyValues {get{
        
        transient  List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
        Review_Products__c.Clearance_Family__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--All--'));
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        System.debug('==>'+options);
        return options;
    
    } set;}     **/
     public List<selectoption> ClearanceFamilyValues{get;set;}
    public CreateClearanceFamilyDocumentLinks(ApexPages.StandardSetController controller) {
    
    selectedCountriesValues= new List<selectoption>();
    allCountriesValues = new List<selectoption>();
    selectedProductProfileValues= new List<selectoption>();
    allProductProfilesValues = new List<selectoption>();
    productBundleValues= new List<selectoption>();
    ClearanceFamilyValues=new List<selectoption>();
    ClearanceFamilyValues.add(new SelectOption('','--All--'));
    /* STSK0013504 - Add two new options in business unit dropdown i.e. 'VBT - 3rd Party' and 'VOS - 3rd Party' */
    businessUnitValues= new List<selectoption>{new SelectOption('VOS','VOS'),new SelectOption('VBT','VBT'),new SelectOption('VOS - 3rd Party','VOS - 3rd Party'),new SelectOption('VBT - 3rd Party','VBT - 3rd Party')};
    //selectedBusinessUnit='VOS';
    List<Country__c> listAllCont =   [Select id,Name from Country__c order by Name];
        if(listAllCont !=null)
        {
            for(Country__c objCon : listAllCont )
            {
                allCountriesValues.add(new SelectOption(objCon.Id,objCon.Name));
            }
        }
 
     
     //Get and fill List of product profile values in multiselect picklist.
     
     List<Review_Products__c> listAllProd = [Select id,Name, Product_Profile_Name__c,Clearance_Family__c,Product_Bundle__c from Review_Products__c  where Business_Unit__c ='VOS' order by Product_Profile_Name__c ];
        if(listAllProd !=null )
        {
            ClearanceFamilyValues.clear();
           ClearanceFamilyValues.add(new SelectOption('','--All--'));
           productBundleValues.clear();
           productBundleValues.add(new SelectOption('','--All--'));
           
            Set<String> setProductBundle = new Set<String>();
            Set<String> setClearanceFamily = new Set<String>();
           
            for(Review_Products__c objProd : listAllProd )
            {
                allProductProfilesValues.add(new SelectOption(objProd.Id,objProd.Product_Profile_Name__c ));
                if(objProd.Clearance_Family__c!=null && objProd.Clearance_Family__c!='-')
                setClearanceFamily.add(objProd.Clearance_Family__c);
                
                if(objProd.Product_Bundle__c!=null && objProd.Product_Bundle__c!='-')
                setProductBundle.add(objProd.Product_Bundle__c);
                
            }
            
            if(!setClearanceFamily.isEmpty()){
                 //productBundleValues.add(new SelectOption('','--All--'));
                for(String strPB : setClearanceFamily){
                    ClearanceFamilyValues.add(new SelectOption(strPB,strPB ));
                }
            }
            if(!setProductBundle.isEmpty()){
                 //productBundleValues.add(new SelectOption('','--All--'));
                for(String strPB : setProductBundle){
                    productBundleValues.add(new SelectOption(strPB,strPB ));
                }
            }
        }   
        
        //Get and fill List of  Product bundle values in multiselect picklist.
     
     /**List<Review_Products__c> listAllProd2 = [Select id,Name, Product_Bundle__c from Review_Products__c order by Product_Bundle__c ];
        if(listAllProd2 !=null )
        {
          Set<String> setProductBundle = new Set<String>();
          
            for(Review_Products__c objProd : listAllProd2 )
            {
               if(String.isNotBlank(objProd.Product_Bundle__c))
               setProductBundle.add(objProd.Product_Bundle__c);
                
            }
            if(!setProductBundle.isEmpty()){
                 productBundleValues.add(new SelectOption('','--All--'));
                for(String strPB : setProductBundle){
                    productBundleValues.add(new SelectOption(strPB,strPB ));
                }
            }
        }**/
    }

    public CreateClearanceFamilyDocumentLinks(ApexPages.StandardController controller) {
    
    selectedCountriesValues= new List<selectoption>();
    allCountriesValues = new List<selectoption>();
    productBundleValues= new List<selectoption>();
    selectedProductProfileValues= new List<selectoption>();
    allProductProfilesValues = new List<selectoption>();
    ClearanceFamilyValues=new List<selectoption>();
    /* STSK0013504 - Add two new options in business unit dropdown i.e. 'VBT - 3rd Party' and 'VOS - 3rd Party' */
    businessUnitValues= new List<selectoption>{new SelectOption('VOS','VOS'),new SelectOption('VBT','VBT'),new SelectOption('VOS - 3rd Party','VOS - 3rd Party'),new SelectOption('VBT - 3rd Party','VBT - 3rd Party')};
    //selectedBusinessUnit='VOS';

    List<Country__c> listAllCont =   [Select id,Name from Country__c order by Name];
        if(listAllCont !=null)
        {
            for(Country__c objCon : listAllCont )
            {
                allCountriesValues.add(new SelectOption(objCon.Id,objCon.Name));
            }
        }
        
     List<Review_Products__c> listAllProd = [Select id,Name, Product_Profile_Name__c ,Clearance_Family__c from  Review_Products__c  where Business_Unit__c ='VOS' order by Product_Profile_Name__c ];
        if(listAllProd !=null )
        {
            for(Review_Products__c objProd : listAllProd )
            {
                allProductProfilesValues.add(new SelectOption(objProd.Id,objProd.Product_Profile_Name__c ));
                if(objProd.Clearance_Family__c!=null && objProd.Clearance_Family__c!='-')
                ClearanceFamilyValues.add(new SelectOption(objProd.Id,objProd.Clearance_Family__c ));

            }
        }   
      
    }

   /** public List<selectoption> ClearanceFamilyValues {get{
        
        transient  List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
        Review_Products__c.Clearance_Family__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('','--All--'));
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        System.debug('==>'+options);
        return options;
    
    } set;} **/
    
    
    
    public PageReference PopulateInputClearanceData() 
    {
        showSection = true;
        system.debug('ClearanceFamily Name>>>>'+SelClearanceFamily);
        
        InputClearanceWrapperData = new List<InputClreancewrapper>();
        List<Input_Clearance__c> listInputClr = new List<Input_Clearance__c>();
        integer i= 0;
        
        if(String.isNotEmpty(SelClearanceFamily) && selectedProductProfileValues!=null && selectedProductProfileValues.size()>0&& String.isNotEmpty(SelProductBundle)){
         
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select either ClearanceFamily ,Product Bundle or Product Profiles.'));
            return null;
        }
        if(String.isNotEmpty(SelClearanceFamily) && selectedProductProfileValues!=null && selectedProductProfileValues.size()>0 ){
         
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select either ClearanceFamily  or Product Profiles.'));
            return null;
        }
        if(selectedProductProfileValues!=null && selectedProductProfileValues.size()>0 && String.isNotEmpty(SelProductBundle)){
         
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select either Product Profiles  or Product Bundle .'));
            return null;
        }
        if(String.isNotEmpty(SelClearanceFamily)  &&  String.isNotEmpty(SelProductBundle)){
         
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select either ClearanceFamily  or Product Bundle .'));
            return null;
        }
         if(!String.isNotEmpty(SelClearanceFamily)  && (selectedProductProfileValues == null || selectedProductProfileValues.size() == 0 ) && !String.isNotEmpty(SelProductBundle))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select either ClearanceFamily ,Product Bundle or Product Profiles.'));
            return null;
        }
         if( (selectedCountriesValues == null || selectedCountriesValues.size() == 0) && !String.isNotEmpty(selCoutriesFromTxt) )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select 1 or more countries.'));
            return null;
        }
        if( (selectedCountriesValues != null && selectedCountriesValues.size() >20) )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'It is preferable to select maximum of 20 countries.'));
            return null;
        }
        
        if( (selectedProductProfileValues != null && selectedProductProfileValues.size() >40) )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'It is preferable to select maximum of 40 Product profiles.'));
            return null;
        }
        if((selectedCountriesValues != null && selectedCountriesValues.size() >0) && String.isNotEmpty(selCoutriesFromTxt))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , ' Please select countries either from the multi picklist or enter the countries in text box.'));
            return null;
        }
        if( selectedProductProfileValues.size()==0 &&  !String.isNotEmpty(SelProductBundle) && String.isNotEmpty(SelClearanceFamily) &&
        (SelectedCountriesValues != null && selectedCountriesValues.size() <=20 || String.isNotEmpty(selCoutriesFromTxt)))
        {
           if(SelectedCountriesValues != null && selectedCountriesValues.size() <=20 && selectedCountriesValues.size() >0 ){
           Set<ID> setCntIds = new Set<ID>();
           
            for ( SelectOption so : selectedCountriesValues ) {
            
                setCntIds.add(so.getValue());
                
            }
            listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
            Clearance_Family__c=:SelClearanceFamily and Country__c IN : setCntIds ]; 
            }else if(String.isNotEmpty(selCoutriesFromTxt))
            {
               String [] arrStrCountries = new List<String>();
               Set<String> setCtlIds = new set<String>();
               
               if(selCoutriesFromTxt.contains(';')){
                String [] arrStrCountries1 =selCoutriesFromTxt.split(';');
                for(String str : arrStrCountries1){
                  arrStrCountries.add(str.trim());
                  }
                           
               }else{
               
               arrStrCountries.add(selCoutriesFromTxt);
               }
               System.debug('selCoutriesFromTxt####'+selCoutriesFromTxt);
               System.debug('arrStrCountries####'+arrStrCountries);
               
               listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
                Clearance_Family__c=:SelClearanceFamily and Country__r.name IN : arrStrCountries ]; 
            
            }
            
        }else if((SelClearanceFamily==null || SelClearanceFamily=='') && selectedProductProfileValues != null && selectedProductProfileValues.size()!=0 && selectedProductProfileValues.size() <=40 && 
                ((selectedCountriesValues != null && selectedCountriesValues.size() <=20)|| String.isNotEmpty(selCoutriesFromTxt) ) )
        {
        
            Set<ID> setProdProfileIds = new Set<ID>();
           
            for ( SelectOption so : selectedProductProfileValues ) {
            
                setProdProfileIds.add(so.getValue());
                
            }
            if(SelectedCountriesValues != null && selectedCountriesValues.size() <=20 && selectedCountriesValues.size() >0 ){
            
            Set<ID> setCntIds = new Set<ID>();
           
                for ( SelectOption so : selectedCountriesValues ) {
            
                setCntIds.add(so.getValue());
                
                }
                listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
                Review_Product__c IN :setProdProfileIds and Country__c IN : setCntIds ]; 
         }else if(String.isNotEmpty(selCoutriesFromTxt))
            {
               String [] arrStrCountries = new List<String>();
               Set<String> setCtlIds = new set<String>();
               
               if(selCoutriesFromTxt.contains(';')){
               String [] arrStrCountries1 =selCoutriesFromTxt.split(';');
                for(String str : arrStrCountries1){
                  arrStrCountries.add(str.trim());
                  }
                           
               }else{
               
               arrStrCountries.add(selCoutriesFromTxt);
               }
               listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
                Review_Product__c IN :setProdProfileIds and Country__r.name IN : arrStrCountries ];

            }   
        }else if((SelClearanceFamily==null || SelClearanceFamily=='') && (selectedProductProfileValues == null || selectedProductProfileValues.size()==0) && (SelProductBundle != null || SelProductBundle!='') 
        && ((selectedCountriesValues != null && selectedCountriesValues.size() <=20)|| String.isNotEmpty(selCoutriesFromTxt) ))
        {
           if(SelectedCountriesValues != null && selectedCountriesValues.size() <=20 && selectedCountriesValues.size() >0 ){
           Set<ID> setCntIds = new Set<ID>();
           
            for ( SelectOption so : selectedCountriesValues ) {
            
                setCntIds.add(so.getValue());
                
            }
            listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
            Country__c IN : setCntIds and Product_Bundle__c =: SelProductBundle ]; 
            }else if(String.isNotEmpty(selCoutriesFromTxt))
            {
                String [] arrStrCountries = new List<String>();
               Set<String> setCtlIds = new set<String>();
               
               if(selCoutriesFromTxt.contains(';')){
               String [] arrStrCountries1 =selCoutriesFromTxt.split(';');
                for(String str : arrStrCountries1){
                  arrStrCountries.add(str.trim());
                  }
                           
               }else{
               
               arrStrCountries.add(selCoutriesFromTxt);
               }
               
               listInputClr = [select Id, Name, Country__c,Product_Profile_Name__c,Clearance_Entry_Form_Name__c, Country__r.Name,Review_Product__c,Product_Model__r.Name,Clearance_Date__c,Clearance_Status__c,Clearance_Family__c,Product_Bundle__c from Input_Clearance__c where
                Country__r.name IN : arrStrCountries and Product_Bundle__c =: SelProductBundle ]; 
            }
        }
        
        for(Input_Clearance__c objInpClr : listInputClr)
        {
            InputClearanceWrapperData.add(new InputClreancewrapper(objInpClr,i));
            i= i+1;
        }
        return null;
        
        
    }

    
public void selectAll(){

    if(InputClearanceWrapperData !=null && InputClearanceWrapperData.size() >0){
        for( InputClreancewrapper inclsObj : InputClearanceWrapperData)
        {
            if(checked)
            inclsObj.selected=true;
            else
            inclsObj.selected=false;
        }
    }

}   
 public Pagereference GetSelected()
    {
        
        SelectedList.clear();
        for(InputClreancewrapper accwrapper : InputClearanceWrapperData)
        if(accwrapper.selected == true)
        {
            
            SelectedList.add(accwrapper);
            System.Debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>'+SelectedList.size());
        }
        return null;
        
    }
    
 public PageReference save()
    {
        system.debug('inside save function');
        GetSelected();
        List<Clearance_Documents_Link__c> lstClrDocLinkUpsert = new List<Clearance_Documents_Link__c>();
 
        Map<Id,String> mpaInpClrIdDocLinkName = new Map<Id,String>();
        Map<Id,String> mpaInpClrIdDocLinkExtName = new Map<Id,String>();
        Map<Id,String> mpaInpClrIdDocLink = new Map<Id,String>();
        Map<Id,String> mpaInpClrIdCertID = new Map<Id,String>();
        
        
        for(InputClreancewrapper ps : SelectedList)
        {
            if(ps.selected==true)
            {
                system.debug('inside selected');
                
                mpaInpClrIdDocLinkName.put(ps.InpClr.Id,ps.docLinkName1);
                mpaInpClrIdDocLink.put(ps.InpClr.Id,ps.docLink1);
                mpaInpClrIdCertID.put(ps.InpClr.Id,ps.certId1);
                
            }
            
        }                          
        //insert insertList;

    List<Clearance_Documents_Link__c> lstClrDocLink =[Select id,Name,Clearance_Entry_Form__c,Clearance_Documents_Link__c,Certificate_Id__c  From Clearance_Documents_Link__c where Name IN : mpaInpClrIdDocLinkName.values() 
                                                      and Clearance_Entry_Form__c IN : mpaInpClrIdDocLinkName.keyset()];
  
    System.debug('mpaInpClrIdDocLinkExtName===>'+mpaInpClrIdDocLinkExtName);
    
        for(String strId : mpaInpClrIdDocLinkName.keyset())
        {
            if(mpaInpClrIdDocLinkExtName.get(strId) != mpaInpClrIdDocLinkName.get(strId))
            {
               Clearance_Documents_Link__c objClrDoc = new Clearance_Documents_Link__c();
               objClrDoc.Clearance_Entry_Form__c = strId;
                objClrDoc.Clearance_Documents_Link__c = mpaInpClrIdDocLink.get(strId);
                objClrDoc.Name = mpaInpClrIdDocLinkName.get(strId);
               // objClrDoc.Description__c = ps.description1;
                objClrDoc.Certificate_Id__c = mpaInpClrIdCertID.get(strId);
                lstClrDocLinkUpsert.add(objClrDoc);
            }
        }
        
        if(lstClrDocLinkUpsert != null && !lstClrDocLinkUpsert.isEmpty())
            upsert lstClrDocLinkUpsert;
            
        PageReference pageRef = null;
        try
        {
            pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        }
        catch(Exception ex)
        {
            // do nothing
        }
        return pageRef;
       // System.Debug('>>>>>>>>>>>>>>>>>> Ritika Test' + insertList);                
       //return null;
    }
public void getProductProfiles()
{
 
   System.debug('Businees Unit==>'+selectedBusinessUnit );
    if(String.isNotEmpty(selectedBusinessUnit))
    {
         allProductProfilesValues.clear();
         ClearanceFamilyValues.clear();
           ClearanceFamilyValues.add(new SelectOption('','--All--'));
           productBundleValues.clear();
           productBundleValues.add(new SelectOption('','--All--'));
           Set<String> setProductBundle = new Set<String>();
           Set<String> setClearanceFamily = new Set<String>();
            
     List<Review_Products__c> listAllProd = [Select id,Name, Product_Profile_Name__c,Clearance_Family__c,Product_Bundle__c from Review_Products__c  where Business_Unit__c =:selectedBusinessUnit order by Product_Profile_Name__c ];
        if(listAllProd !=null )
        {
           
            for(Review_Products__c objProd : listAllProd )
            {
                allProductProfilesValues.add(new SelectOption(objProd.Id,objProd.Product_Profile_Name__c ));
                  if(objProd.Clearance_Family__c!=null && objProd.Clearance_Family__c!='-')
                setClearanceFamily.add(objProd.Clearance_Family__c);
                
                if(objProd.Product_Bundle__c!=null && objProd.Product_Bundle__c!='-')
                setProductBundle.add(objProd.Product_Bundle__c);
            }
            
            if(!setClearanceFamily.isEmpty()){
                 //productBundleValues.add(new SelectOption('','--All--'));
                for(String strPB : setClearanceFamily){
                    ClearanceFamilyValues.add(new SelectOption(strPB,strPB ));
                }
            }
            if(!setProductBundle.isEmpty()){
                 //productBundleValues.add(new SelectOption('','--All--'));
                for(String strPB : setProductBundle){
                    productBundleValues.add(new SelectOption(strPB,strPB ));
                }
            }
        }
     }   
    System.debug('allProductProfilesValues'+allProductProfilesValues);    
}  
 
public void resetComponents(){

selectedCountriesValues.clear();
selectedProductProfileValues.clear();
If(InputClearanceWrapperData!=null && !InputClearanceWrapperData.isEmpty())
    InputClearanceWrapperData.clear();
SelProductBundle=null;
SelClearanceFamily=null;
selCoutriesFromTxt=null;
} 

public class InputClreancewrapper    
    { 

        public Input_Clearance__c InpClr{get; set;}     
        public Boolean selected {get; set;}
        public String docLinkName1{get;set;}
        public String docLink1{get;set;}
        public String description1{get;set;}
        public String certId1{get;set;}
        public Integer intRowNo{get; set;}
        // public Integer serial{get; set;}
        public InputClreancewrapper(Input_Clearance__c inc,Integer serial)     
        {            
            InpClr = inc;            
            selected = false;   
            docLinkName1= '';
            docLink1 = '';
            description1 = '';
            certId1 = '';  
            // serial = inc.id;
            intRowNo =serial;
        }    
    } 

}
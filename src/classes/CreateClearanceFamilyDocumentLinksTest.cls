/*********************************************************************
@Author: Praveen Musunuru
@Date: June 3,2014
@Description: This class is used for test coverage of the ClearanceFamilyDocumentLinks Class.
@Last Modified By: Praveen Musunuru
@Last Modified Date: June 17,2014
@Last Modified Reason:
**********************************************************************/


@isTest
private class CreateClearanceFamilyDocumentLinksTest {

	static testMethod void clearanceFamilyTest() {
        // create a product to start with for the review product
        List<Product2> productList = new List<Product2>();
        Product2 pModel = new Product2();
        pModel.Name = 'Product1';
        pModel.ProductCode = 'BH321';
        pModel.Description = 'New product 1';
        pModel.Family = 'CLINAC';
        pModel.Item_Level__c = 'Model';
        pModel.Model_Part_Number__c = 'MCM565878687MCM';
        pModel.Product_Bundle__c = 'Clinac 11';
        pModel.OMNI_Family__c = 'Calypso';
        pModel.OMNI_Item_Decription__c = 'A clinac Clypso product';
        pModel.OMNI_Product_Group__c = 'Clinac 2100C';

        Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';
        insert productList;

		// create country record for which Input Clearance record would be cerated automatically
        Country__c cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        insert cntry;
        Review_Products__c rp = new Review_Products__c();
        rp.Product_Model__c = pModel.Id;
        rp.Version_No__c = '11';
        rp.Product_Profile_Name__c = 'test1';
        rp.Clearance_Family__c = 'BrachyVision';
        rp.Business_Unit__c = 'VBT';
        rp.Regulatory_Name__c = 'Regular name';
        rp.Model_Part_Number__c='test';
        rp.Product_Bundle__c='VP13';
        insert rp;
        
        Review_Products__c rp2 = new Review_Products__c();
        rp2.Product_Model__c = pSubitem.Id;
        rp2.Version_No__c = '12';
        rp2.Product_Profile_Name__c = pSubitem.Name+ ' '+rp.Version_No__c;
        rp2.Clearance_Family__c = 'BrachyVision10';
        rp2.Business_Unit__c = 'VOS';
        rp2.Regulatory_Name__c = 'Regular NAme';
        rp2.Model_Part_Number__c='test1';
        rp2.Product_Bundle__c='VP13';
        insert rp2;

        Country__c cntry2 = new Country__c();
        cntry2.Name = 'DubaDuba';
        cntry2.Region__c = 'USA';
        cntry2.Expiry_After_Years__c = '1';
        insert cntry2; // this should create Input clearance record for "Review Product" (rp) record

        List<Item_Details__c> detailList = new List<Item_Details__c>();
        Item_Details__c iDetail1 = new Item_Details__c();
        idetail1.Review_Product__c = rp.id;
        //iDetail1.Item_Part__c = 'item';
        iDetail1.Name = pSubItem.Name;
        
        Item_Details__c iDetail2 = new Item_Details__c();
        iDetail2.Review_Product__c = rp.id;
        //iDetail2.Item_Part__c = p3rdParty.Id;
        iDetail2.Name = pSubItem.Name;
        
        detailList.add(iDetail1);
        detailList.add(iDetail2);
        
        insert detailList;
        
        // query at least one input clearance record
        Input_Clearance__c ic = [SELECT Id, Name, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Clearance_Expiration_Date__c, Comments__c, Does_Not_Expire__c, Approved_Manufacturing_Site__c, Region__c, Expected_Clearance_Date__c, Status__c FROM Input_Clearance__c where Review_Product__c = :rp.Id LIMIT 1]; 
        
        Clearance_Documents_Link__c objClrDoc = new Clearance_Documents_Link__c();
        objClrDoc.Clearance_Entry_Form__c = ic.id;
        objClrDoc.Clearance_Documents_Link__c = 'www.google.com' ;
        objClrDoc.Name = 'test';
        Test.startTest();

        PageReference pageRef = Page.CreateClearanceFamilyDocumentLinks;
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardSetController stdSetController = new Apexpages.StandardSetController(productList);
        CreateClearanceFamilyDocumentLinks  controllerset= new CreateClearanceFamilyDocumentLinks  (stdSetController );
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(objClrDoc);
        CreateClearanceFamilyDocumentLinks  controller= new CreateClearanceFamilyDocumentLinks(stdController);
        controller.docLinkName= 'test';
        controller.docLink= 'www.google.com';
        controller.certId= '2312';
		controller.description = 'Test';
		controller.selBusinessUnit = 'VBT';
        List<selectoption> testlst=controller.ClearanceFamilyValues;
        controller.SelClearanceFamily='BrachyVision';
        controller.selectedCountriesValues.add(controller.allCountriesValues.get(0));
        
        
        controller.PopulateInputClearanceData();
        controller.SelClearanceFamily=null;
        controller.SelProductBundle='VP13';
        controller.PopulateInputClearanceData();
        
        controller.SelClearanceFamily=null;
        controller.SelProductBundle=null;
        controller.selectedProductProfileValues.add(controller.allProductProfilesValues.get(0));
        controller.PopulateInputClearanceData();
        
        CreateClearanceFamilyDocumentLinks  controller1= new CreateClearanceFamilyDocumentLinks(stdController);
        controller1.selCoutriesFromTxt='Austria;Australia';
        controller1.selectedBusinessUnit='VBT';      
        controller1.getProductProfiles();
        controller1.selectedCountriesValues.clear();
        controller1.PopulateInputClearanceData();
        controller1.SelClearanceFamily=null;
        controller1.SelProductBundle='VP13';
        controller1.PopulateInputClearanceData();
        
        controller1.SelClearanceFamily=null;
        controller1.SelProductBundle=null;
        controller1.selectedProductProfileValues.add(controller.allProductProfilesValues.get(0));
        controller1.PopulateInputClearanceData();
        
        
        controller1.checked=true;
        controller1.selectAll();
        controller1.checked=false;
        controller1.selectAll();
        controller1.save();
        controller1.resetComponents();
        
        ApexPages.currentPage().getParameters().put('docLinkName', 'test');
        ApexPages.currentPage().getParameters().put('docLink', 'www.google.com');
        ApexPages.currentPage().getParameters().put('certId', '2312');
        controller.GetSelected();
        
		controller1.SelClearanceFamily = 'Varian Treatment';
		controller1.selectedProductProfileValues.add(controller.allProductProfilesValues.get(0));
		controller1.SelProductBundle = 'VP13';
		controller1.PopulateInputClearanceData();
		
		controller1.selCoutriesFromTxt='Austria;Australia';
		controller.selectedCountriesValues.clear();
		controller1.PopulateInputClearanceData();
		

		Test.stopTest();
	}
}
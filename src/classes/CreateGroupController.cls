public with sharing class CreateGroupController
{ 
    public Unity_Group__c newGroup { get; set; }
    public List<Filter_Criterion__c> filters { get; set; }
    public SelectOption[] selectedMembers { get; set; }
    public SelectOption[] allMembers { get; set; }
    public String logicSign { get; set; }


    Private Map<String, Schema.sObjectField> fieldMap;
    Id technicianId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public SelectOption[] Fields 
    { 
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            SelectOption noneOption = new SelectOption('', '--None--');
            options.add(noneOption);
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            fieldMap = schemaMap.get('SVMXC__Service_Group_Members__c').getDescribe().fields.getMap();
            
            for(String apiName : fieldMap.keySet())
            {
                if(String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'Name' || 
                    String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'SVMXC__Country__c' || 
                    String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'SVMXC__Email__c' || 
                    String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'Service_Team_Name__c' || 
                    String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'District_Manager_Name__c' || 
                    String.valueOf(fieldMap.get(apiName).getDescribe().getName()) == 'DM_Email__c'
                    )
                {
                    SelectOption selectOption = new SelectOption(apiName, fieldMap.get(apiName).getDescribe().getLabel());
                    options.add(selectOption);
                }
            }
            return options;
        }
        set;
    }
    public SelectOption[] Operators
    { 
        get
        {
            List<SelectOption> options = new List<SelectOption>();
            SelectOption selectOption1 = new SelectOption('None', '--None--');
            SelectOption selectOption2 = new SelectOption('=', 'equals');
            SelectOption selectOption3 = new SelectOption('!=', 'not equal to');
            SelectOption selectOption4 = new SelectOption('LIKE', 'starts with');
            SelectOption selectOption5 = new SelectOption('LIKE%', 'contains');

            options.add(selectOption1);
            options.add(selectOption2);
            options.add(selectOption3);
            options.add(selectOption4);
            options.add(selectOption5);

            return options;
        }
        set; 
    }

    
    public String message { get; set; }
    
    public CreateGroupController() 
    {
        filters = new List<Filter_Criterion__c>();
        String groupId = ApexPages.currentPage().getParameters().get('Id');
        String shareId = ApexPages.currentPage().getParameters().get('share');
        newGroup = new Unity_Group__c(); 
        selectedMembers = new List<SelectOption>();
        allMembers = new List<SelectOption>();

        if(String.isNotBlank(groupId))
        {
            List<Unity_Group__c> groups = [select Name, Id, IsDefault__c, Logic_Sign__c, (select Id, Name, Filter_Value__c, Operator__c, Technician_Group__c from Filter_Criteria__r) from Unity_Group__c where Id =: groupId limit 1];
            if(groups.size() > 0)
            {
                newGroup = groups[0];
                for(Filter_Criterion__c criterion : newGroup.Filter_Criteria__r)
                {
                    filters.add(criterion);
                }
            }
            if(newGroup.Filter_Criteria__r.size() < 5)
            {
                for(Integer i = 0; i < (5 - newGroup.Filter_Criteria__r.size()); i++)
                {
                    Filter_Criterion__c filterCriteria = new Filter_Criterion__c();
                    filters.add(filterCriteria);
                }
            }
        } 
        else
        {
            for(Integer i = 0; i < 5; i++)
            {
                Filter_Criterion__c filterCriteria = new Filter_Criterion__c();
                filters.add(filterCriteria);
            }
        }

        if(String.isNotBlank(shareId))
        {
            filters.clear();
            for(Integer i = 0; i < 5; i++)
            {
                Filter_Criterion__c filterCriteria = new Filter_Criterion__c();
                filters.add(filterCriteria);
            }
        }
  
        if(String.isBlank(newGroup.Logic_Sign__c))
        {
            newGroup.Logic_Sign__c = 'AND';
        }
    }

    public PageReference switchLogicSign()
    {
        if(newGroup.Logic_Sign__c == 'AND')
        {
            newGroup.Logic_Sign__c = 'OR';
        }
        else
        {
            newGroup.Logic_Sign__c = 'AND';
        }
        return null;
    }

    public PageReference shareNext() 
    {
        if(allMembers.size() > 0)
        {
            allMembers.clear();
        }

        List<Filter_Criterion__c> newCriteria = new List<Filter_Criterion__c>();
        String whereClause = '';
        for ( Filter_Criterion__c criterion : filters ) 
        {
            if(String.isNotBlank(criterion.Name))
            {
                if(criterion.Technician_Group__c == null)
                {
                    criterion.Technician_Group__c = newGroup.Id;
                }
                newCriteria.add(criterion);

                if(String.isNotBlank(criterion.Filter_Value__c) && String.isNotBlank(criterion.Operator__c))
                {
                    List<String> values = new List<String>();

                    if(criterion.Filter_Value__c.contains(','))
                    {
                        values = criterion.Filter_Value__c.split(',');
                    }
                    else
                    {
                        values.add(criterion.Filter_Value__c);
                    }

                    if((criterion.Operator__c == 'INCLUDES' || criterion.Operator__c == 'EXCLUDES'))
                    {
                    }
                    else
                    {
                        whereClause += '(';
                        for(String str : values)
                        {
                            if(criterion.Operator__c == '=')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' = \'';            
                                whereClause += str;    
                                whereClause += '\'';
                                whereClause += ' OR ';                                                            
                            }
                            if(criterion.Operator__c == '!=')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' != \'';            
                                whereClause += str;    
                                whereClause += '\'';
                                whereClause += ' OR ';     
                            }
                            if(criterion.Operator__c == 'LIKE%')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' like \'%';            
                                whereClause += str;  
                                whereClause += '%\'';                                                 
                                whereClause += ' OR ';     
                            }
                            if(criterion.Operator__c == 'LIKE')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' like \'';            
                                whereClause += str;  
                                whereClause += '%\'';                                                 
                                whereClause += ' OR '; 
                            }
                        }
                        if(whereClause.endsWith('OR '))
                        {
                            whereClause = whereClause.removeEnd('OR ');
                        }
                        whereClause += ') ' + newGroup.Logic_Sign__c + ' ';
                    }
                }
            }
        }
        if(whereClause.endsWith(' AND '))
        {
            whereClause += whereClause.removeEnd(' AND ');
        }
        if(whereClause.endsWith(' OR '))
        {
            whereClause += whereClause.removeEnd(' OR ');
        }

        if(String.isNotBlank(whereClause))
        {
            for ( sObject s : Database.query('select Id, RecordTypeId, Name, User__c, User__r.Name, User__r.IsActive from SVMXC__Service_Group_Members__c where ' + whereClause + ' limit 300'))
            {
                SVMXC__Service_Group_Members__c u = (SVMXC__Service_Group_Members__c)s;
                if(u.RecordTypeId == technicianId && u.User__r.IsActive)
                {
                    allMembers.add(new SelectOption(u.User__c, u.User__r.Name));
                }
            }
        }
        else
        {
            for ( sObject s : Database.query('select Id, RecordTypeId, Name, User__c, User__r.Name, User__r.IsActive from SVMXC__Service_Group_Members__c where User__r.IsActive = TRUE limit 300'))
            {
                SVMXC__Service_Group_Members__c u = (SVMXC__Service_Group_Members__c)s;
                if(u.RecordTypeId == technicianId)
                {
                    allMembers.add(new SelectOption(u.User__c, u.User__r.Name));
                }
            }
        }
        /*
        if(members.size() > 0)
        {
            upsert members;
        }
        */
        return new PageReference('/apex/SelectSharedMembers');   
    }

    public PageReference next() 
    {
        if(allMembers.size() > 0)
        {
            allMembers.clear();
        }

        upsert newGroup;

        List<Filter_Criterion__c> newCriteria = new List<Filter_Criterion__c>();
        String whereClause = '';
        for ( Filter_Criterion__c criterion : filters ) 
        {
            if(String.isNotBlank(criterion.Name))
            {
                if(criterion.Technician_Group__c == null)
                {
                    criterion.Technician_Group__c = newGroup.Id;
                }
                newCriteria.add(criterion);

                if(String.isNotBlank(criterion.Filter_Value__c) && String.isNotBlank(criterion.Operator__c))
                {
                    List<String> values = new List<String>();

                    if(criterion.Filter_Value__c.contains(','))
                    {
                        values = criterion.Filter_Value__c.split(',');
                    }
                    else
                    {
                        values.add(criterion.Filter_Value__c);
                    }

                    if((criterion.Operator__c == 'INCLUDES' || criterion.Operator__c == 'EXCLUDES'))
                    {
                        /*
                        if(String.valueOf(fieldMap.get(criterion.Name).getDescribe().getType()) == 'MULTIPICKLIST')
                        {
                            String includeClause = '';
                            for(String str : values)
                            {
                                includeClause += '\'';
                                includeClause += str;                        
                                includeClause += '\',';
                            }

                            whereClause += '(';
                            whereClause += criterion.Name;
                            whereClause += ' ';
                            whereClause += criterion.Operator__c;
                            whereClause += ' ';          

                            whereClause += '(';
                            whereClause += includeClause.removeEnd(',');
                            whereClause += ')';

                            whereClause +=  ') ' + newGroup.Logic_Sign__c + ' ';
                        }
                        */
                    }
                    else
                    {
                        whereClause += '(';
                        for(String str : values)
                        {
                            if(criterion.Operator__c == '=')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' = \'';            
                                whereClause += str;    
                                whereClause += '\'';
                                whereClause += ' OR ';                                                            
                            }
                            if(criterion.Operator__c == '!=')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' != \'';            
                                whereClause += str;    
                                whereClause += '\'';
                                whereClause += ' OR ';     
                            }
                            if(criterion.Operator__c == 'LIKE%')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' like \'%';            
                                whereClause += str;  
                                whereClause += '%\'';                                                 
                                whereClause += ' OR ';     
                            }
                            if(criterion.Operator__c == 'LIKE')
                            {
                                whereClause += criterion.Name;
                                whereClause += ' like \'';            
                                whereClause += str;  
                                whereClause += '%\'';                                                 
                                whereClause += ' OR '; 
                            }
                        }
                        if(whereClause.endsWith('OR '))
                        {
                            whereClause = whereClause.removeEnd('OR ');
                        }
                        whereClause += ') ' + newGroup.Logic_Sign__c + ' ';
                    }
                }
            }
        }
        if(whereClause.endsWith(' AND '))
        {
            whereClause += whereClause.removeEnd(' AND ');
            System.debug('****: ' + whereClause);
        }
        if(whereClause.endsWith(' OR '))
        {
            whereClause += whereClause.removeEnd(' OR ');
            System.debug('****: ' + whereClause);
        }
        if(newCriteria.size() > 0)
        {
            upsert newCriteria;
        }

        if(String.isNotBlank(whereClause))
        {
            for ( sObject s : Database.query('select Id, RecordTypeId, Name, User__c, User__r.Name, User__r.IsActive from SVMXC__Service_Group_Members__c where ' + whereClause + ' limit 300'))
            {
                SVMXC__Service_Group_Members__c u = (SVMXC__Service_Group_Members__c)s;
                if(u.RecordTypeId == technicianId && u.User__r.IsActive)
                {
                    allMembers.add(new SelectOption(u.User__c, u.User__r.Name));
                }
            }
        }
        else
        {
            for ( sObject s : Database.query('select Id, RecordTypeId, Name, User__c, User__r.Name, User__r.IsActive from SVMXC__Service_Group_Members__c where User__r.IsActive = TRUE limit 300'))
            {
                SVMXC__Service_Group_Members__c u = (SVMXC__Service_Group_Members__c)s;
                if(u.RecordTypeId == technicianId)
                {
                    allMembers.add(new SelectOption(u.User__c, u.User__r.Name));
                }
            }
        }
        /*
        if(members.size() > 0)
        {
            upsert members;
        }
        */
        return new PageReference('/apex/SelectGroupMembers');   
    }
    public PageReference save() 
    {
        for ( Filter_Criterion__c criterion : filters ) 
        {
            if(String.isBlank(criterion.Name))
            {
                if(criterion.Technician_Group__c != null)
                {
                    delete criterion;
                }
            }
        }
        if(newGroup.Id != null)
        {
            List<Unity_Group_Member__c> members = [select Id, Name, User__c from Unity_Group_Member__c where Unity_Group__c = :newGroup.Id];
            if(members.size() > 0)
            {
                delete members;
            }
        }
        List<Unity_Group_Member__c> members = new List<Unity_Group_Member__c>();

        if(selectedMembers.size() > 0)
        {
            for(Integer i = 0; i < selectedMembers.size(); i++)
            {
                Unity_Group_Member__c member = new Unity_Group_Member__c();
                member.Name = selectedMembers[i].getLabel();
                member.Unity_Group__c = newGroup.Id;
                member.User__c = selectedMembers[i].getValue();
                member.Order_Number__c = i + 1;
                members.add(member);
            }
        }

        if(members.size() > 0)
        {
            upsert members;
        }
        return new PageReference('/apex/CalendarPage');        
    }
    public PageReference share() 
    {
        List<Unity_Group__Share> members = new List<Unity_Group__Share>();

        if(selectedMembers.size() > 0)
        {
            for(Integer i = 0; i < selectedMembers.size(); i++)
            {
                Unity_Group__Share qshare = new Unity_Group__Share();
                qshare.ParentId = newGroup.Id;
                qshare.UserOrGroupID = selectedMembers[i].getValue();
                qshare.AccessLevel = 'Read';
                qshare.RowCause = 'Manual';
                members.add(qshare);
            }
        }

        if(members.size() > 0)
        {
            insert members;
        }
        return new PageReference('/apex/CalendarPage');        
    }
    public PageReference cancel() 
    {
        return new PageReference('/apex/CalendarPage');        
    }
    public PageReference previous() 
    {
        return new PageReference('/apex/CreateGroup?id=' + newGroup.Id);        
    }
    public PageReference previousShare() 
    {
        return new PageReference('/apex/ShareGroup?id=' + newGroup.Id);        
    }
}
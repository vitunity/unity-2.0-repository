@isTest(SeeAllData=true)
public class CreateGroupControllerTest
{
    static Unity_Group__c unityGroup;
    static Unity_Group_Member__c unityMember;
    public Static Profile pr = [Select id from Profile where name = 'VMS Service - User'];
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
    static testMethod void CreateGroupControllerTest()
    {
        insert u;
        unityGroup = new Unity_Group__c();
        unityMember = new Unity_Group_Member__c();
        unityGroup.Name = 'Acme';
        unityGroup.IsDefault__c = true;
        Insert unityGroup;
        unityMember.Unity_Group__c = unityGroup.id;
        unityMember.User__c = u.id;
        insert unityMember;
        List<Filter_Criterion__c> filters = new List<Filter_Criterion__c>();
        CreateGroupController controller = new CreateGroupController();
        PageReference myVfPage1 = Page.CreateGroup;
        Test.setCurrentPage(myVfPage1);     
        ApexPages.currentPage().getParameters().Put('id', unityGroup.id);
        CreateGroupController controller1 = new CreateGroupController();
        controller1.next();
        controller1.shareNext();
        Filter_Criterion__c fc = new Filter_Criterion__c();
        Filter_Criterion__c fc1 = new Filter_Criterion__c();
        Filter_Criterion__c fc2 = new Filter_Criterion__c();
        Filter_Criterion__c fc3 = new Filter_Criterion__c();
        Filter_Criterion__c fc4 = new Filter_Criterion__c();
        fc.Name = 'Name';
        fc.Operator__c = 'LIKE%';
        fc.Filter_Value__c = 'b';
        fc.Technician_Group__c = unityGroup.id;
        insert fc;
        fc1.Name = 'SVMXC__Country__c';
        fc1.Operator__c = 'LIKE%';
        fc1.Filter_Value__c = 'CN';
        fc1.Technician_Group__c = unityGroup.id;
        insert fc1;
        filters.add(fc1);
        fc2.Name = 'SVMXC__Email__c';
        fc2.Operator__c = '=';
        fc2.Filter_Value__c = 'bingbing.zang@varian.com';
        fc2.Technician_Group__c = unityGroup.id;
        insert fc2;
        filters.add(fc2);
        fc3.Name = 'Service_Team_Name__c';
        fc3.Operator__c = '!=';
        fc3.Filter_Value__c = 'JP';
        fc3.Technician_Group__c = unityGroup.id;
        insert fc3;
        filters.add(fc3);
        fc4.Name = 'District_Manager_Name__c';
        fc4.Operator__c = 'LIKE';
        fc4.Filter_Value__c = 'Chang';
        fc4.Technician_Group__c = unityGroup.id;
        insert fc4;
        filters.add(fc4);
        controller1 = new CreateGroupController();
        SelectOption[] Fields = controller1.Fields;
        SelectOption[] Operators = controller1.Operators;
        controller1.filters = filters;
        controller1.next();
        controller1.shareNext();
        controller1.cancel();
        controller1.selectedMembers.add(new SelectOption(u.Id, u.username));
        controller1.save();
        controller1.share();
        controller1.switchLogicSign();
        controller1.previous();
    }
}
public class CreateNonVIPLtgController {

    public static Map<Id, String> recordtypemap {get;set;}
    
    @AuraEnabled        
    public static List<String> fetchRecordTypeValues(){
        List<Schema.RecordTypeInfo> recordtypes = Competitor__c.SObjectType.getDescribe().getRecordTypeInfos();    
        recordtypemap = new Map<Id, String>();
        for(RecordTypeInfo rt : recordtypes){
            if(rt.getName() != 'Master')
            recordtypemap.put(rt.getRecordTypeId(), rt.getName());
        }        
        return recordtypemap.values();
    }
    
    @AuraEnabled
    public static Id getRecTypeId(String recordTypeLabel){
        system.debug('recordTypeLabel==='+recordTypeLabel);
        Id recid = Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get(recordTypeLabel==null?'BA':recordTypeLabel).getRecordTypeId();        
        return recid;
    }      
}
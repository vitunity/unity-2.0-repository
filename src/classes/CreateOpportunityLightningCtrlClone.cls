/**
* @author      :       Puneet Mishra
* @created     :       22 August 2017
* @description :       controller return picklist values to lightning component controller 
*/
public with sharing class CreateOpportunityLightningCtrlClone {
    
    public class SelectOptionWrap {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String  value;
        public SelectOptionWrap(String value, String label){
            this.label = label;
            this.value = value;
        }
    }
    
    @AuraEnabled public List<SelectOptionWrap> oppCurrencyList {get;set;}
    
    @AuraEnabled
    public static List<SelectOptionWrap> fetchCurrencyPicklist(String objectType, String fieldName) {
        Set<SelectOptionWrap> pickListValuesSet = new Set<SelectOptionWrap>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(fieldName != 'CurrencyIsoCode')
        pickListValuesSet.add(new SelectOptionWrap('','--None--'));
        for(Schema.PicklistEntry pickListVal : ple){
            pickListValuesSet.add(new SelectOptionWrap(pickListVal.getValue(),pickListVal.getLabel()));
        }
        return new List<SelectOptionWrap>(pickListValuesSet);
    }
    
    @testVisible public static string ObjectName = 'Opportunity';
    @AuraEnabled public Opportunity opportunity {get;set;}
    @AuraEnabled public List<String> accountType {get;set;}
    @AuraEnabled public List<String> accountSector {get;set;}
    @AuraEnabled public List<String> payerAsCustomer {get;set;}
    @AuraEnabled public List<String> oppCurrency {get;set;}
    @AuraEnabled public List<String> deliverCountry {get;set;}
    @AuraEnabled public List<String> oppStage {get;set;}
    @AuraEnabled public List<String> InHouseStatusVal {get;set;}
    @AuraEnabled public List<String> forecastPer {get;set;}
    @AuraEnabled public List<String> existingProdReplacement {get;set;}
    @AuraEnabled public List<String> lynactakeout {get;set;}
    @AuraEnabled public List<String> lynacSysTakeout {get;set;}
    @AuraEnabled public List<String> OISsysTakeout {get;set;}
    @AuraEnabled public List<String> TPSSysTakeout {get;set;}
    @AuraEnabled public List<String> branchySysTakeout {get;set;}
    @AuraEnabled public List<String> velocityTakeout {get;set;}
    @AuraEnabled public List<String> lostSoftware {get;set;}
    @AuraEnabled public List<String> primaryWonReason {get;set;}
    @AuraEnabled public List<String> primaryWonDetail {get;set;}
    @AuraEnabled public List<String> secWonReason {get;set;}
    @AuraEnabled public List<String> secWonDetail {get;set;}
    @AuraEnabled public List<String> lostToCompetitor {get;set;}
    @AuraEnabled public List<String> lostDealComponent {get;set;}
    @AuraEnabled public List<String> new_replaceVault {get;set;}
    @AuraEnabled public List<String> primaryLostReason {get;set;}
    @AuraEnabled public List<String> primaryLostDetail {get;set;}
    @AuraEnabled public List<String> secLostReason {get;set;}
    @AuraEnabled public List<String> secLostDetail {get;set;}
    @AuraEnabled public List<String> newConstruction {get;set;}
    @AuraEnabled public List<String> vmsInstalled {get;set;}
    @AuraEnabled public List<String> SRS_SBRTDistributor {get;set;}
    @AuraEnabled public String accAddress {get;set;}
    @AuraEnabled public String sitePartnerAddress {get;set;}
    @AuraEnabled public List<String> dealProbability {get;set;}
    @AuraEnabled public Boolean isDealProbability {get;set;}
    @AuraEnabled public List<String> competitorUnits {get;set;}
    @AuraEnabled static public Boolean isChinaRegion {get;set;}
    private static String subRegion;
    private static List<GroupMember> chinaTerritory = new List<GroupMember>();
    
    @AuraEnabled public Account accountObj {get;set;}
    
    @AuraEnabled public Map<String, List<String>> wonBusinessPrimaryWon {get;set;} // Primary_Won_Reason__c => Primary_Won_Detail__c
    @AuraEnabled public Map<String, List<String>> wonBusinessSecondaryWon {get;set;} // Secondary_Won_Reason__c => Secondary_Won_Detail__c
    @AuraEnabled public Map<String, List<String>> lossBusinessPrimaryLoss {get;set;} //Primary_Loss_Reason__c => Primary_reason__c
    @AuraEnabled public Map<String, List<String>> lossBusinessSecondaryLoss {get;set;} //Secondary_Loss_Reason__c => Secondary_Reason__c
    @testVisible private static Set<String> billingCountries = new Set<String>{'USA', 'Canada', 'Puerto Rico'};
    
    //Added by Shital for Opportunity won/loss section, Competitor Details
    @AuraEnabled public Map<String, List<String>> lostOIS {get;set;}
    @AuraEnabled public Map<String, List<String>> lostTPS {get;set;}
    @AuraEnabled public List<String> lostOISDetail {get;set;}
    @AuraEnabled public List<String> lostTPSDetail {get;set;}
    @AuraEnabled public List<String> keyDecisionMakers {get;set;}
    @AuraEnabled public List<String> actionToWinFutureBusiness {get;set;}
    @AuraEnabled public List<String> serviceContractYears {get;set;}
    
    @AuraEnabled public List<String> opportunityCategory {get;set;}
    
    @testVisible public static string compObjectName = 'Competitor__c';
    @AuraEnabled public List<String> nonVarianProductRecordTypes {get;set;}
    public static Boolean alreadyAdded = false;
    @AuraEnabled public List<String> productNameDetail {get;set;}
    @AuraEnabled public List<String> vendorList {get;set;}
    @AuraEnabled public Map<String, List<String>> productTypeMap {get;set;}
    @AuraEnabled public Map<String, List<String>> productNameList {get;set;}
    
    @AuraEnabled public Map<String, List<String>> vendorProductNameList {get;set;}
    
    @AuraEnabled public List<String> vaultIdentifierList {get;set;}
    @AuraEnabled public List<String> yearInstalledList {get;set;}
    @AuraEnabled public List<String> serviceProviderList {get;set;}
    @AuraEnabled public List<String> statusList {get;set;}
    @AuraEnabled public List<NonVarianInstalledProduct> nonInstalledVarianProducts{get;set;}
    //Added by Shital
        
    @testVisible private static Id oldReplacedQuoteId;
    public static String errorMessage;
    
    public static String getAcctAddress(Account oppAccount){
        if(oppAccount != null){
            String acctAddress = (!String.isBlank(oppAccount.BillingStreet)?(oppAccount.BillingStreet+',\r\n'):'')+
                (!String.isBlank(oppAccount.BillingCity)?(oppAccount.BillingCity+','):'')+
                (!String.isBlank(oppAccount.BillingState)?(oppAccount.BillingState+','):'')+
                (!String.isBlank(oppAccount.BillingPostalCode)?(oppAccount.BillingPostalCode+',\r\n'):'')+
                (!String.isBlank(oppAccount.BillingCountry)?(oppAccount.BillingCountry+''):'');
            return acctAddress;               
        }
        return null;
    }
    
    @AuraEnabled
    public static Account getAccount(String accountId){
        if(AccountId != null){
            list<Account> oppAccountList = [SELECT Name, CurrencyIsoCode, Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry, Account_Sector__c
                                    FROM Account  
                                    WHERE Id = :accountId];
            if(!oppAccountList.isEmpty()){
                return oppAccountList[0];            
            }else{
                return null;
            }
        }
        return null;
    }
    
    /**
     * author       :   Puneet Mishra, 21 Sept 2017
     * description  :   method used to fetch the picklist value when new record is getting created
     * return       : instance of Controller
     */
    @AuraEnabled
    public static CreateOpportunityLightningCtrlClone initializePicklistValues(String objectType) {
        CreateOpportunityLightningCtrlClone obj = new CreateOpportunityLightningCtrlClone();
        obj.oppCurrencyList = fetchCurrencyPicklist(ObjectName,'CurrencyIsoCode');
        obj.accountType = fetchPicklistValues(ObjectName,'Type');
        obj.accountSector = fetchPicklistValues(ObjectName,'Account_Sector__c');
        obj.payerAsCustomer = fetchPicklistValues(ObjectName,'Payer_Same_as_Customer__c');
        obj.oppCurrency = fetchPicklistValues(ObjectName,'CurrencyIsoCode');
        obj.deliverCountry = fetchPicklistValues(ObjectName,'Deliver_to_Country__c');
        obj.oppStage = fetchPicklistValues(ObjectName,'StageName');
        obj.InHouseStatusVal = fetchPicklistValues(ObjectName,'Order_In_House_Status__c');
        obj.forecastPer = fetchPicklistValues(ObjectName,'Unified_Funding_Status__c');
        obj.existingProdReplacement = fetchPicklistValues(ObjectName,'Existing_Equipment_Replacement__c');
        //obj.existingProdReplacement.remove(0);
        obj.lynacSysTakeout = fetchPicklistValues(ObjectName,'Competitive_Takeout_Vendor__c');
        obj.OISsysTakeout = fetchPicklistValues(ObjectName,'Competitive_System_Lost__c');
        obj.TPSSysTakeout = fetchPicklistValues(ObjectName,'Competitive_Software_Lost__c');
        obj.branchySysTakeout = fetchPicklistValues(ObjectName,'Collection_of_Lost_Business_reason__c');
        obj.velocityTakeout = fetchPicklistValues(ObjectName,'Velocity_Takeout__c');
        obj.lostSoftware = fetchPicklistValues(ObjectName,'Lost_Software__c');
        obj.competitorUnits = fetchPicklistValues(ObjectName,'Competitor_Units__c');
        //Added by SHital
        obj.lostSoftware = fetchPicklistValues(ObjectName,'Lost_Software__c');
        obj.lostOIS = new Map<String, List<String>>();
        obj.lostOIS = getDependentOptions('Lost_Software__c', 'Lost_OIS__c');
        obj.lostTPS = new Map<String, List<String>>();
        obj.lostTPS = getDependentOptions('Lost_Software__c', 'Lost_TPS__c');
        obj.keyDecisionMakers = fetchPicklistValues(ObjectName,'Key_Decision_Makers__c');
        obj.actionToWinFutureBusiness = fetchPicklistValues(ObjectName,'Actions_to_win_future_business1__c');
        system.debug('&&&actionToWinFutureBusinessOIS**'+obj.actionToWinFutureBusiness);
        obj.serviceContractYears = fetchPicklistValues(ObjectName,'Service_Contract_Years1__c');
        system.debug('&&&&&&&obj.lostOIS**'+obj.lostOIS);
        //obj.opportunityCategory = fetchPicklistValues(ObjectName,'Opportunity_Category__c');
        
        obj.primaryWonReason = fetchPicklistValues(ObjectName,'Primary_Won_Reason__c');
        obj.wonBusinessPrimaryWon = new Map<String, List<String>>();
        obj.wonBusinessPrimaryWon = getDependentOptions('Primary_Won_Reason__c', 'Primary_Won_Detail__c');
        
        obj.secWonReason = fetchPicklistValues(ObjectName,'Secondary_Won_Reason__c');
        obj.wonBusinessSecondaryWon = new Map<String, List<String>>();
        obj.wonBusinessSecondaryWon = getDependentOptions('Secondary_Won_Reason__c', 'Secondary_Won_Detail__c');
        
        obj.lostToCompetitor = fetchPicklistValues(ObjectName,'Competitor_Name__c');
        obj.lostDealComponent = fetchPicklistValues(ObjectName,'Lost_deal_components__c');
        obj.new_replaceVault = fetchPicklistValues(ObjectName,'New_or_Replacement_vault_or_socket__c');
        
        obj.primaryLostReason = fetchPicklistValues(ObjectName,'Primary_Loss_Reason__c');
        obj.lossBusinessPrimaryLoss = new Map<String, List<String>>();
        obj.lossBusinessPrimaryLoss = getDependentOptions('Primary_Loss_Reason__c', 'Primary_reason__c');
        
        obj.secLostReason = fetchPicklistValues(ObjectName,'Secondary_Loss_Reason__c');
        obj.lossBusinessSecondaryLoss = new Map<String, List<String>>();
        obj.lossBusinessSecondaryLoss = getDependentOptions('Secondary_Loss_Reason__c', 'Secondary_Reason__c');
        
        obj.vmsInstalled = fetchPicklistValues(ObjectName,'VMS_Installed__c');
        obj.newConstruction = fetchPicklistValues(ObjectName,'Is_New_Construction_needed__c');
        obj.SRS_SBRTDistributor = fetchPicklistValues(ObjectName,'SRS_SBRT_Distributor__c');
        obj.dealProbability = fetchPicklistValues(ObjectName,'Unified_Probability__c');
        
        obj.opportunity = new Opportunity();
        obj.opportunity.Existing_Equipment_Replacement__c = 'Yes';
        
        //Added by Shital
        obj.vendorList = fetchPicklistValues(compObjectName,'Vendor__c');
        obj.productTypeMap = new Map<String, List<String>>();
        obj.productTypeMap = getDependentOptions('Product_Type__c', 'Vendor__c');
        obj.vaultIdentifierList = fetchPicklistValues(compObjectName,'Vault_identifier__c');
        
        obj.productNameList = new Map<String, List<String>>();
        obj.productNameList = getDependentOptions('Vendor__c', 'Model__c');
        obj.vendorProductNameList = new Map<String, List<String>>{'Anatom-e'=>new List<String>{'None'},
            'HealthMyne'=> new List<String>{'None'},
            'MIM'=> new List<String>{'None'},
            'Mirada'=> new List<String>{'None'},
            'Oncology Systems Ltd (OSL)'=> new List<String>{'None'}};/**/
        
    obj.yearInstalledList = fetchPicklistValues(compObjectName,'Year_Installed__c');
        obj.serviceProviderList = fetchPicklistValues(compObjectName,'Service_Provider__c');
        obj.statusList = fetchPicklistValues(compObjectName,'Status__c');
        system.debug('obj.productNameList*****'+obj.productNameList);
        return obj;
    }
    
    /**
     * author       :   Puneet Mishra, 21 Sept 2017
     * description  :   method will retun map of controlling picklist field values and their corresponding dependent picklist
     *                  field values as Lightning do not provide controlling-dependent picklist values functionality.
     * return       :   Ma<String, List<String>>, Map<Controlling value, List<Dependent Values>>
     */
    @testVisible
    public static Map<String, List<String>> getDependentOptions(String parentField, String childField) {
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        Bitset bitSetObj = new Bitset();
        // for Competitor object
        if(parentField =='Vendor__c'){
          ObjectName = compObjectName;  
        }
        
        //convert the api names to lowercase
        String parent_Field = parentField.toLowerCase();
        String child_Field = childField.toLowerCase();
        Map<String, Schema.SObjectField> objFieldMap = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(child_Field) || !objFieldMap.containsKey(parent_Field)){
            return objResults;
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(parent_Field).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(child_Field).getDescribe().getPicklistValues();
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            if(ctrlentry.isActive()) {
                String label = ctrlentry.getLabel();
                objResults.put(label,new List<String>{''});
                controllingIndexes.add(contrIndex);
            }
        }
        
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
            Schema.PicklistEntry depentry = depEntries[dependentIndex];
            if(depentry.isActive())
              objEntries.add(depentry);
        } 
        
        objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        
        List<Integer> indexes;
        for (PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            for (Integer idx : indexes){
              String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }
        }
        if(parentField == 'Secondary_Won_Reason__c' || parentField == 'Primary_Won_Reason__c' ) {
            objResults.get('Other').add('None');
        }
        
        objEntries = null;
        objJsonEntries = null;
        return objResults;
        
    }
    
     
    /**
     * author       :   Puneet Mishra, 21 sept 2017
     * description  :   method will query the picklist field values
     * return       :   List<String>, sObject's picklist fields values in list<String>
     */
    @AuraEnabled
    public static List<String> fetchPicklistValues(String objectType, String fieldName) {
        Set<String> pickListValuesSet = new Set<String>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
        Schema.DescribeSObjectResult res = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(!pickListValuesSet.contains(''))
            pickListValuesSet.add('');
        for( Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.isActive())
              pickListValuesSet.add(pickListVal.getValue());
        }
        system.debug(' ==== pickListValuesSet ==== ' + pickListValuesSet);
        return new List<String>(pickListValuesSet);
    }
    
    
    
    /*
     * Get Accout Address
     */
    public static String getAcctAddress(String accountId){
        if(AccountId != null){
            
            list<Account> oppAccountList = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry 
                                    FROM Account 
                                    WHERE Id = :accountId];
            if(!oppAccountList.isEmpty()){
                 Account oppAccount = oppAccountList[0];
                 String acctAddress = (!String.isBlank(oppAccount.BillingStreet)?(oppAccount.BillingStreet+',\r\n'):'')+
                                (!String.isBlank(oppAccount.BillingCity)?(oppAccount.BillingCity+','):'')+
                                (!String.isBlank(oppAccount.BillingState)?(oppAccount.BillingState+','):'')+
                                (!String.isBlank(oppAccount.BillingPostalCode)?(oppAccount.BillingPostalCode+',\r\n'):'')+
                                (!String.isBlank(oppAccount.BillingCountry)?(oppAccount.BillingCountry+''):'');
                 return acctAddress;               
            }else{
                return null;
            }
        }
        return null;
    }
    
    /**
     * method used to fetch Opportunity record, called when recId is not null
     */
    @AuraEnabled
    public static CreateOpportunityLightningCtrlClone fetchOpportunity(String opportunityId, String accId) {
        CreateOpportunityLightningCtrlClone obj = initializePicklistValues('Opportunity');
        Map<String, Schema.SObjectField> objFields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
        String query = 'SELECT ';
        for(String str : objFields.keySet()) {
            query += str + ', ';
        }
        query += ' RecordType.DeveloperName, Account.Name, Owner.Name, Primary_Contact_Name__r.Name, Site_Partner__r.Name, ';
        query += ' Prior_Opportunity__r.Name, Service_Maintenance_Contract__r.Name,Quote_Replaced__r.Name, ';
        query += ' Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry, '+
            ' Site_Partner__r.BillingStreet , Site_Partner__r.BillingCity , Site_Partner__r.BillingState , '+ 
            ' Site_Partner__r.BillingPostalCode , Site_Partner__r.BillingCountry, Account.Sub_Region__c ';
      
        query += ' FROM Opportunity WHERE Id=: opportunityId ';
        List<Opportunity> oppList = (List<Opportunity>)Database.query(query);
        
        if(oppList.isEmpty()) {
            obj.opportunity = new Opportunity();
            obj.accountObj = getAccount(accId);
            obj.opportunity.account = obj.accountObj;
            obj.accAddress = getAcctAddress(obj.accountObj);
            obj.opportunity.CurrencyIsoCode = obj.accountObj.CurrencyIsoCode;
            obj.opportunity.Account_Sector__c = obj.accountObj.Account_Sector__c;
            obj.opportunity.Deliver_to_Country__c = obj.accountObj.Country__c;
            //obj.opportunity.Existing_Equipment_Replacement__c = 'Yes';
        } else {
            obj.opportunity = oppList[0];
            obj.accAddress =  (!String.isBlank(oppList[0].Account.BillingStreet)?(oppList[0].Account.BillingStreet+',\r\n'):'')+
                (!String.isBlank(oppList[0].Account.BillingCity)?(oppList[0].Account.BillingCity+','):'')+
                (!String.isBlank(oppList[0].Account.BillingState)?(oppList[0].Account.BillingState+','):'')+
                (!String.isBlank(oppList[0].Account.BillingPostalCode)?(oppList[0].Account.BillingPostalCode+',\r\n'):'')+
                (!String.isBlank(oppList[0].Account.BillingCountry)?(oppList[0].Account.BillingCountry+''):'');
            
            obj.sitePartnerAddress = (!String.isBlank(oppList[0].Site_Partner__r.BillingStreet)?(oppList[0].Site_Partner__r.BillingStreet+',\r\n'):'')+
                (!String.isBlank(oppList[0].Site_Partner__r.BillingCity)?(oppList[0].Site_Partner__r.BillingCity+','):'')+
                (!String.isBlank(oppList[0].Site_Partner__r.BillingState)?(oppList[0].Site_Partner__r.BillingState+','):'')+
                (!String.isBlank(oppList[0].Site_Partner__r.BillingPostalCode)?(oppList[0].Site_Partner__r.BillingPostalCode+',\r\n'):'')+
                (!String.isBlank(oppList[0].Site_Partner__r.BillingCountry)?(oppList[0].Site_Partner__r.BillingCountry+''):'');
            
            oppList[0].Velocity_Takeout__c = oppList[0].Velocity_Takeout__c != null ? oppList[0].Velocity_Takeout__c : 'None';
            oppList[0].Competitive_Takeout_Vendor__c = oppList[0].Competitive_Takeout_Vendor__c != null ? oppList[0].Competitive_Takeout_Vendor__c : 'None';
            oppList[0].Competitive_System_Lost__c = oppList[0].Competitive_System_Lost__c != null ? oppList[0].Competitive_System_Lost__c : 'None';
            oppList[0].Competitive_Software_Lost__c = oppList[0].Competitive_Software_Lost__c != null ? oppList[0].Competitive_Software_Lost__c : 'None';
            oppList[0].Collection_of_Lost_Business_reason__c = oppList[0].Collection_of_Lost_Business_reason__c != null ? oppList[0].Collection_of_Lost_Business_reason__c : 'None';
            subRegion = (String.valueOf(Account.Sub_Region__c) != null ? String.valueOf(Account.Sub_Region__c) : '');
            
            if(billingCountries.contains(obj.Opportunity.Account.BillingCountry)) {
                obj.forecastPer = resetForecastPercentage();
                obj.isDealProbability = false;
            } else {
                obj.isDealProbability = true;
            }
        }
        chinaTerritory = [SELECT Id FROM GroupMember WHERE UserOrGroupId =:UserInfo.getUserId() 
                          AND Group.DeveloperName  = 'China_Admin_Opp'];
        if(chinaTerritory.isEmpty() && subRegion=='China Admin')
            isChinaRegion = true;
        else
            isChinaRegion = false;
        
        //Added by SHital 
        obj.nonInstalledVarianProducts =  getInstallbase(opportunityId);
        obj.nonVarianProductRecordTypes = getRecordTypes();
        system.debug('***getRecordTypes**'+obj.nonVarianProductRecordTypes);
        //END
        
        return obj;
    }
    
    
    /**
     * author           :   Puneet Mishra, 21 Sept 2017
     * description      :   method will set the forcast% and will called if Account Billing country is 
     *                      either USA, Canada, Puerto Rico
     * return           :   List<String> 
     */
    @TestVisible
    private static list<String> resetForecastPercentage(){
        List<String> forecastPer = new List<String>();
        forecastPer.clear();
        forecastPer.add('0%');
        forecastPer.add('25%');
        forecastPer.add('50%');
        forecastPer.add('75%');
        forecastPer.add('100%');
        return forecastPer;
    }
    
    /**
     * author       :   Puneet Mishra, 21 September 2017
     * description  : method will be called when submit button is clicked, upsert the opportunity
     * retrun       : Opportunity Id which will be used to redirect the user to opportunity detail page
     * Edit by Shital : added a extra parameter for Competitor details
     */
    @AuraEnabled
    public static String saveOpprotunity(Opportunity opport, String nonInstalledVarianProducts) {
        try {
            if(opport.Id != null) {
                Opportunity oldOpp = [SELECT Id, Name, Type, Quote_Replaced__c, Primary_Contact_Name__c  FROM Opportunity WHERE Id =: opport.Id];
                oldReplacedQuoteId = oldOpp.Quote_Replaced__c;
            }
            //system.debug(' == OLD QUOTE ID == ' + oldReplacedQuoteId);
            // CONDITION BY: KrishnnaKatve.HarleenGulati.INC4569517.4/4/2017.End
            if(opport.Quote_Replaced__c != null && opport.Type == 'Replacement') {
                List<BigMachines__Quote__c> replacedQuotes = new List<BigMachines__Quote__c>();
                list<BigMachines__Quote__c> pQuoteListUpdate = [select Id, BigMachines__Is_Primary__c                                                             
                                                                from BigMachines__Quote__c 
                                                                where BigMachines__Opportunity__c =: opport.Id
                                                                and BigMachines__Is_Primary__c = true];
                
                String replacedByQuoteId = null;
                if(!pQuoteListUpdate.isEmpty()){
                    replacedByQuoteId = pQuoteListUpdate[0].Id;
                }
                replacedQuotes.add(new BigMachines__Quote__c(Id = opport.Quote_Replaced__c, Is_Replaced__c = true,
                                                             Replaced_By__c = replacedByQuoteId));
                if(oldReplacedQuoteId!=null && oldReplacedQuoteId!=opport.Quote_Replaced__c){
                    replacedQuotes.add(new BigMachines__Quote__c(Id = oldReplacedQuoteId, Is_Replaced__c = false,
                                                                 Replaced_By__c = null));
                }
                
                update replacedQuotes;
            } else if(oldReplacedQuoteId!=null){
                update new BigMachines__Quote__c(Id = oldReplacedQuoteId, Is_Replaced__c = false,Replaced_By__c = null);
                opport.Quote_Replaced__c = null;
            }
            
            //if(opport.CloseDate == null)
            //opport.CloseDate = System.today() + 30;
            
            system.debug('Currency Code opport: '+opport);
            
            upsert opport;
            
            //Changes added by SHital
            system.debug('**nonInstalledVarianProducts***'+nonInstalledVarianProducts);
        if(nonInstalledVarianProducts != null){
            List<NonVarianInstalledProduct> nonInstalledProducts = (List<NonVarianInstalledProduct>)JSON.deserialize(nonInstalledVarianProducts, List<NonVarianInstalledProduct>.class);
            List<Competitor__c> competitors = getNonVarianInstalledProducts(opport,nonInstalledProducts);
            system.debug('***opport**'+opport.Varian_SW_Serial_No_Non_Varian_IB__c + ' === ' + opport.Varian_PCSN_Non_Varian_IB__c);
            system.debug('******competitors final list***'+competitors);
            updateVIPNVIPStatus(opport, competitors);
            if(!competitors.isEmpty()){
                upsert competitors NVIP_Key__c;
                //upsert competitors;
            }
        }
            return opport.Id;
        } catch(Exception e){
           // system.debug(' == ' + e.getLineNumber() + ' == ' + e.getMessage());
            // throwning exception back to lightning component, which will be displayed to User.
            throw new AuraHandledException(e.getMessage());
        }
        return null;
    }
    
    //Changes added by Shital Start
    public static List<Competitor__c> getNonVarianInstalledProducts(Opportunity opp, List<NonVarianInstalledProduct> nonInstalledVarianProducts){
        List<Competitor__c> competitors = new List<Competitor__c>();
        for(NonVarianInstalledProduct nonVarianIP : nonInstalledVarianProducts){//&& opp.Existing_Equipment_Replacement__c == 'Yes'
            if((opp.StageName == '8 - CLOSED LOST' || opp.StageName == '7 - CLOSED WON')   && nonVarianIP.nonVarianProduct.isLost__c == true){
                nonVarianIP.nonVarianProduct.Competitor_Account_Name__c = opp.AccountId;
                nonVarianIP.nonVarianProduct.NVIP_Key__c = opp.AccountId+''+nonVarianIP.nonVarianProduct.Model__c+''+nonVarianIP.nonVarianProduct.Vendor__c;
                nonVarianIP.nonVarianProduct.isLost__c = true;
            }else{
                nonVarianIP.nonVarianProduct.NVIP_Key__c = opp.AccountId+''+nonVarianIP.nonVarianProduct.Model__c+''+nonVarianIP.nonVarianProduct.Vendor__c;
            }
            if(opp.StageName == '8 - CLOSED LOST' && opp.Existing_Equipment_Replacement__c == 'No'){
                nonVarianIP.nonVarianProduct.Status__c = 'Current';
            }
            nonVarianIP.nonVarianProduct.Opportunity__c = opp.Id;
            nonVarianIP.nonVarianProduct.Id = null;
            competitors.add(nonVarianIP.nonVarianProduct);
        }
        return competitors;
    }
    
    /**
* Update replacement installed product's Marked For Replacement flag
*/
    @testVisible
    private static void updateVIPNVIPStatus(Opportunity opport, List<Competitor__c> competitors){
        List<String> ipNumbers = new List<String>();
        List<SVMXC__Installed_Product__c> installedProductsToUpdate = new List<SVMXC__Installed_Product__c>();
        if(opport.StageName == '7 - CLOSED WON' || opport.StageName == '8 - CLOSED LOST'){
            if(!String.isBlank(opport.Varian_PCSN_Non_Varian_IB__c)){
                ipNumbers.addAll(opport.Varian_PCSN_Non_Varian_IB__c.split(','));
            }
            if(!String.isBlank(opport.Varian_SW_Serial_No_Non_Varian_IB__c)){
                ipNumbers.addAll(opport.Varian_SW_Serial_No_Non_Varian_IB__c.split(','));   
            }
            List<SVMXC__Installed_Product__c> installedProducts = [SELECT Id, Marked_For_Replacement__c FROM SVMXC__Installed_Product__c WHERE Name IN:ipNumbers];
            for(SVMXC__Installed_Product__c ip : installedProducts){
                if(opport.StageName == '8 - CLOSED LOST' && ip.Marked_For_Replacement__c){
                    ip.Marked_For_Replacement__c = false;
                    installedProductsToUpdate.add(ip);
                }else if(!ip.Marked_For_Replacement__c){
                    ip.Marked_For_Replacement__c = true;
                    installedProductsToUpdate.add(ip);
                }
            }
            for(Competitor__c competitor : [SELECT Id, Status__c, Model__c, NVIP_Key__c, Vendor__c FROM Competitor__c WHERE Name IN:ipNumbers]){
                if(opport.StageName == '7 - CLOSED WON' && opport.Existing_Equipment_Replacement__c == 'Yes'){
                    competitor.Status__c = 'Replaced';
                    competitors.add(competitor);
                }
            }
            
            if(!installedProductsToUpdate.isEmpty()) update installedProductsToUpdate;
        }
    }
    
    @AuraEnabled  
    public static NonVarianInstalledProduct addRemoveInstalledProduct(String selectedRecordType, String addRemoveFlag, Integer counter1){
        
        NonVarianInstalledProduct newProduct;
        Integer counter = integer.valueof(counter1);
        
        List<RecordType> selectedRecordType1 = [SELECT Id, DeveloperName, Description FROM RecordType WHERE Description =:selectedRecordType];
        Map<String, List<String>> productTypeVals =  new Map<String, List<String>>();
        CreateOpportunityLightningCtrlClone.ObjectName = 'Competitor__c';
        system.debug(' === selectedRecordType1[0].DeveloperName == ' + selectedRecordType1[0].DeveloperName);
        if(selectedRecordType1[0].DeveloperName == 'BA') {
            productTypeVals = CompetitorProductInfoController.brachytheraphyAfterLoader();
            system.debug(' === productTypeVals == ' + productTypeVals +  ' == ' + selectedRecordType);
        } else if(selectedRecordType1[0].DeveloperName == 'DS') {
            productTypeVals = CompetitorProductInfoController.deliverySystem();
        } else if(selectedRecordType1[0].DeveloperName == 'IMS') {
            productTypeVals = CompetitorProductInfoController.imageManagementSystems();
        } else if(selectedRecordType1[0].DeveloperName == 'OIS') {
            productTypeVals = CompetitorProductInfoController.oncologyInformationSystems();
        } else if(selectedRecordType1[0].DeveloperName == 'OPCD') {
            productTypeVals = CompetitorProductInfoController.thirdPartyItems();
        } else if(selectedRecordType1[0].DeveloperName == 'TPS') {
            productTypeVals = CompetitorProductInfoController.treatmentPlanningSolution();
        } else if(selectedRecordType1[0].DeveloperName == 'TPSC') {
            productTypeVals = CompetitorProductInfoController.serviceContracts();
        }
        
        list<String> vendorList = new list<String>{''};
        vendorList.addAll(productTypeVals.keySet());
        //productTypeVals = getDependentOptions('Product_Type__c', 'Vendor__c');
        if(selectedRecordType1.size()>0){
            newProduct = new NonVarianInstalledProduct(counter,new Competitor__c(RecordTypeId=selectedRecordType1[0].Id, 
                                                                                 RecordType=selectedRecordType1[0],
                                                                                Product_type__c = selectedRecordType1[0].Description),
                                                      vendorList, productTypeVals );
        }
        system.debug(' ==== NonVarianInstalledProduct ==== ' + newProduct);
        system.debug(' ==== newProduct.vendorList ==== ' + newProduct.vendorList);
        return newProduct; 
    }
        
    @AuraEnabled  
    public static List<NonVarianInstalledProduct> removeInstalledProduct(Integer rowNumber, String existingList){
        
        List<NonVarianInstalledProduct> listToSave = (List<NonVarianInstalledProduct>)JSON.deserialize(existingList, List<NonVarianInstalledProduct>.class);
        Integer rowId = Integer.valueOf(rowNumber);//Converted to integer again because salesforce internal error was coming 
        system.debug(' ROW NUmber ' + rowNumber);
        system.debug(' listToSave ' + listToSave);
        //listToSave.remove(rowId);
        if(listToSave[Integer.valueOf(rowNumber)].nonVarianProduct.Id == null) {
            listToSave.remove(rowId);
        } else {
            delete listToSave[Integer.valueOf(rowNumber)].nonVarianProduct;
            listToSave.remove(Integer.valueOf(rowNumber));
        } 
        for(NonVarianInstalledProduct nonInstalledProduct : listToSave){
            if(nonInstalledProduct.rowNumber>rowId){
                nonInstalledProduct.rowNumber -= 1;
            }
        }
        system.debug(' === listToSave === ' + listToSave);
        return listToSave;
    }
    //Changes added by Shital END
    
    @AuraEnabled
    public static List<String> getRecordTypes(){
        List<String> recordTypeOptions = new List<String>();
        for(RecordType NVPrecordType : [SELECT Id, Name, Description FROM RecordType WHERE SobjectType = 'Competitor__c' 
                                        AND IsActive =: true]){
            recordTypeOptions.add(NVPrecordType.Description);
        }
        alreadyAdded =true;
        return recordTypeOptions;
    }
    
    @AuraEnabled
    public static List<NonVarianInstalledProduct> getInstallbase(String opportunityId){
        system.debug('***opportunityId****'+opportunityId);
        List<NonVarianInstalledProduct> nonInstalledBase = new List<NonVarianInstalledProduct>();
        Integer counter = 0;
        
        if(opportunityId!=null && opportunityId!=''){
            for(Competitor__c comp : [
                SELECT Id, Name, Opportunity__c, Product_Type__c, Model__c, Vendor__c, Isotope_Source__c,Year_Installed__c,
                Vault_identifier__c, Service_Provider__c, Status__c, RecordType.Description,
                RecordType.DeveloperName, isLost__c 
                FROM Competitor__c
                Where Opportunity__c =:opportunityId
            ]){
                nonInstalledBase.add(new NonVarianInstalledProduct(counter, comp, new List<String>(), new Map<String, List<String>>() ));
                counter = counter + 1;
            }
        }
        return nonInstalledBase;      
    }
    
    
    @AuraEnabled 
    public static string fetchSitePartnerAddress(string ERPPartnerAssosiationID){
        string partnerAddress = '';
        for(ERP_Partner_Association__c ERP : [SELECT id,Partner_Address__c FROM ERP_Partner_Association__c WHERE Id =:ERPPartnerAssosiationID]){
            if(ERP.Partner_Address__c != null){
                partnerAddress = ERP.Partner_Address__c;
            }
        }
        return partnerAddress;
    }
    
}
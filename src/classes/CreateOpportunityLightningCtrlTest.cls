/**
* @author      :       Puneet Mishra
* @created     :       3 Jan 2018
* @description :       test class for CreateOpportunityLightningCtrl
*/
@isTest
public class CreateOpportunityLightningCtrlTest {
    
    public static Account testAcc;
    
    public static List<String> recTypeName;
    
    static {
        recTypeName = new List<String>{'BA', 'DS', 'IMS', 'OIS', 'OPCD', 'TPS'};
        
    }
    public static Competitor__c createCompetitorData(RecordType recType, String accId) {
        
        Competitor__c competitors = new Competitor__c();
        competitors.RecordTypeId = recType.Id;
        competitors.Competitor_Account_Name__c = accId;
        competitors.Vendor__c = 'Electra';
        competitors.Model__c = 'HDR-3';
        return competitors;
    }
    
    private static testmethod void SelectOptionWrap_Test() {
        CreateOpportunityLightningCtrl.SelectOptionWrap wrap = new CreateOpportunityLightningCtrl.SelectOptionWrap('test_val','test_label');
        system.assertEquals('test_val', wrap.value);
        system.assertEquals('test_label', wrap.label);
    }
    
    private static testMethod void fetchCurrencyPicklist_Test() {
        List<CreateOpportunityLightningCtrl.SelectOptionWrap> wrapList;// = new CreateOpportunityLightningCtrl.SelectOptionWrap('test_val','test_label');
        wrapList = CreateOpportunityLightningCtrl.fetchCurrencyPicklist('Opportunity', 'Type');
        
    }
    
    private static testMethod void initializePicklistValues_Test() {
        CreateOpportunityLightningCtrl.initializePicklistValues('Opportunity');
    }
    
    private static testMethod void getAcctAddress_Test() {
        testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        String expectedAddress = testAcc.BillingStreet+',\r\n' + testAcc.BillingCity+',' + testAcc.BillingState + 
                                 ',' + testAcc.BillingPostalCode+',\r\n' + testAcc.BillingCountry;
        
        String actualAddress = CreateOpportunityLightningCtrl.getAcctAddress(testAcc);
        String actualAddress_Id = CreateOpportunityLightningCtrl.getAcctAddress(String.valueOf(testAcc.Id));
        system.assertEquals(expectedAddress, actualAddress);
        system.assertEquals(expectedAddress, actualAddress_Id);
    }
    
    private static testMethod void getAcctAddress_NullException_Test() {
        system.assertEquals(null, CreateOpportunityLightningCtrl.getAcctAddress(''));
        Account testAcc;
        system.assertEquals(null, CreateOpportunityLightningCtrl.getAcctAddress(testAcc) );
    }
    
    private static testMethod void resetForecastPercentage_Test() {
        list<String> forecastPer = CreateOpportunityLightningCtrl.resetForecastPercentage();
        system.assertNotEquals(forecastPer.size(), 0);
    }
    
    private static testMethod void getAccount_Test() {
        testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Account expectedAcc = CreateOpportunityLightningCtrl.getAccount(testAcc.Id);
        system.assertEquals(expectedAcc.Id, testAcc.Id);
        system.assertEquals(expectedAcc.BillingStreet, testAcc.BillingStreet);
        system.assertEquals(expectedAcc.BillingCity, testAcc.BillingCity);
        system.assertEquals(expectedAcc.BillingState, testAcc.BillingState);
        system.assertEquals(expectedAcc.BillingPostalCode, testAcc.BillingPostalCode);
        system.assertEquals(expectedAcc.BillingCountry, testAcc.BillingCountry);
        system.assertEquals(expectedAcc.Name, testAcc.Name);
    }
    
    private static testMethod void getAccount_NULLCHK_Test() {
        
        Account expectedAcc = CreateOpportunityLightningCtrl.getAccount('');
        system.assertEquals(expectedAcc, null);
    }
    
    
    @isTest(SeeAllData = true) 
    private static void fetchOpportunity_test() {
        BigMachines__Quote__c bigQuote = [SELECT Id, BigMachines__Is_Primary__c, BigMachines__Opportunity__c, 
                                          BigMachines__Opportunity__r.Quote_Replaced__c , BigMachines__Opportunity__r.Type
                                          FROM BigMachines__Quote__c WHERE BigMachines__Opportunity__c !=: null 
                                          AND BigMachines__Opportunity__r.Quote_Replaced__c !=: null AND BigMachines__Opportunity__r.Type =: 'Replacement' Limit 1];
        
        Opportunity opp = [SELECT Id, AccountId, Quote_Replaced__c, Type FROM Opportunity WHERE Id =: bigQuote.BigMachines__Opportunity__c 
                           AND Quote_Replaced__c !=: null AND Type =: 'Replacement' LIMIT 1 ];
        
        try {
            RecordType recType = [SELECT Id, DeveloperName, Description, SobjectType, isActive FROM RecordType 
                                     WHERE SobjectType =: 'Competitor__c' AND 
                                     isActive =: true 
                                  AND DeveloperName IN: recTypeName Limit 1];
            Competitor__c competition = createCompetitorData(recType, opp.AccountId  );
            String expected = CreateOpportunityLightningCtrl.saveOpprotunity(opp, JSON.serialize(new List<Competitor__c>{competition}) );
        //system.assertEquals(expected, opp.Id);
        } catch(Exception e) {
            
        }
    }
    
    @isTest(SeeAllData=true)
    private static void saveOpportunity_Test() {
        
        BigMachines__Quote__c bigQuote = [SELECT Id, BigMachines__Is_Primary__c, BigMachines__Opportunity__c, 
                                          BigMachines__Opportunity__r.Quote_Replaced__c , BigMachines__Opportunity__r.Type
                                          FROM BigMachines__Quote__c WHERE BigMachines__Opportunity__c !=: null 
                                          AND BigMachines__Opportunity__r.Quote_Replaced__c !=: null AND BigMachines__Opportunity__r.Type =: 'Replacement' Limit 1];
        
        Opportunity opp = [SELECT Id, AccountId, Quote_Replaced__c, Type FROM Opportunity WHERE Id =: bigQuote.BigMachines__Opportunity__c 
                           AND Quote_Replaced__c !=: null AND Type =: 'Replacement' LIMIT 1 ];
        
        try {
            CreateOpportunityLightningCtrl.fetchOpportunity(String.valueOf(opp.Id), String.valueOf(opp.AccountId));
        //system.assertEquals(expected, opp.Id);
        } catch(Exception e) {
            
        }
    }
    
    private static testMethod void saveOpportunity_NewOpp_Test() {
        
        try {
            CreateOpportunityLightningCtrl.fetchOpportunity('123', '321');
        //system.assertEquals(expected, opp.Id);
        } catch(Exception e) {
            
        }
    }
    
    @isTest(SeeAllData=true)
    private static void getNonVarianInstalledProducts_Test() {
        test.startTest();
/*          BigMachines__Quote__c bigQuote = [SELECT Id, BigMachines__Is_Primary__c, BigMachines__Opportunity__c, 
                                          BigMachines__Opportunity__r.Quote_Replaced__c , BigMachines__Opportunity__r.Type
                                          FROM BigMachines__Quote__c WHERE BigMachines__Opportunity__c !=: null 
                                          AND BigMachines__Opportunity__r.Quote_Replaced__c !=: null AND BigMachines__Opportunity__r.Type =: 'Replacement' Limit 1];
            */Opportunity opp = [SELECT Id, AccountId, Quote_Replaced__c, Type, StageName, Existing_Equipment_Replacement__c,
                                 RecordType.DeveloperName, Account.Name, Owner.Name, Primary_Contact_Name__r.Name, Site_Partner__r.Name,
                                 Prior_Opportunity__r.Name, Service_Maintenance_Contract__r.Name,Quote_Replaced__r.Name,
                                 Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry, 
                                 Site_Partner__r.BillingStreet , Site_Partner__r.BillingCity , Site_Partner__r.BillingState ,
                                 Site_Partner__r.BillingPostalCode , Site_Partner__r.BillingCountry, Account.Sub_Region__c
                                 FROM Opportunity WHERE 
                                StageName =: '8 - CLOSED LOST' LIMIT 1 ];
            
            Integer i = 0;
            List<NonVarianInstalledProduct> nonVarianList = new List<NonVarianInstalledProduct>();
            for(Competitor__c c : [SELECT Id, Name, Opportunity__c, Product_Type__c, Model__c, Vendor__c, Isotope_Source__c,Year_Installed__c,
                        Vault_identifier__c, Service_Provider__c, Status__c, RecordType.Description,
                        RecordType.DeveloperName, isLost__c 
                                   FROM Competitor__c WHERE Opportunity__c =: opp.Id])  {
                NonVarianInstalledProduct non = new NonVarianInstalledProduct(i, c, new List<String>(),  new Map<String, List<String>>());
                nonVarianList.add(non);
                i++;
            }
            
            CreateOpportunityLightningCtrl.getNonVarianInstalledProducts(opp, nonVarianList);
        
        test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    private static void getNonVarianInstalledProducts_isLost_Test() {
        test.startTest();
        Opportunity opp = [SELECT Id, AccountId, Quote_Replaced__c, Type, StageName, Existing_Equipment_Replacement__c,
                                 RecordType.DeveloperName, Account.Name, Owner.Name, Primary_Contact_Name__r.Name, Site_Partner__r.Name,
                                 Prior_Opportunity__r.Name, Service_Maintenance_Contract__r.Name,Quote_Replaced__r.Name,
                                 Account.BillingStreet, Account.BillingCity, Account.BillingState, Account.BillingPostalCode, Account.BillingCountry, 
                                 Site_Partner__r.BillingStreet , Site_Partner__r.BillingCity , Site_Partner__r.BillingState ,
                                 Site_Partner__r.BillingPostalCode , Site_Partner__r.BillingCountry, Account.Sub_Region__c
                                 FROM Opportunity WHERE 
                                StageName =: '8 - CLOSED LOST' LIMIT 1 ];
            
            Integer i = 0;
            List<NonVarianInstalledProduct> nonVarianList = new List<NonVarianInstalledProduct>();
            for(Competitor__c c : [SELECT Id, Name, Opportunity__c, Product_Type__c, Model__c, Vendor__c, Isotope_Source__c,Year_Installed__c,
                        Vault_identifier__c, Service_Provider__c, Status__c, RecordType.Description,
                        RecordType.DeveloperName, isLost__c 
                                   FROM Competitor__c WHERE Opportunity__c =: opp.Id])  {
                NonVarianInstalledProduct non = new NonVarianInstalledProduct(i, c, new List<String>(),  new Map<String, List<String>>());
                nonVarianList.add(non);
                i++;
            }
            RecordType recType = [SELECT Id, DeveloperName, Description, SobjectType, isActive FROM RecordType 
                                     WHERE SobjectType =: 'Competitor__c' AND 
                                     isActive =: true 
                                  AND DeveloperName IN: recTypeName Limit 1];
            Competitor__c competition = createCompetitorData(recType, opp.AccountId  );
            competition.isLost__c = true;
            nonVarianList.add(new NonVarianInstalledProduct(nonVarianList.size(), competition, new List<String>(),  new Map<String, List<String>>()));
            CreateOpportunityLightningCtrl.getNonVarianInstalledProducts(opp, nonVarianList);
        
        test.stopTest();
    }
    
    private static testMethod void updateVIPNVIPStatus_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        RecordType recType = [SELECT Id, DeveloperName, Description, SobjectType, isActive FROM RecordType 
                                     WHERE SobjectType =: 'Competitor__c' AND 
                                     isActive =: true 
                                  AND DeveloperName IN: recTypeName Limit 1];
        Competitor__c competition = createCompetitorData(recType, opp.AccountId  );
        competition.isLost__c = true;
        CreateOpportunityLightningCtrl.updateVIPNVIPStatus(opp, new List<Competitor__c>{competition});
    }
    
    private static testMethod void addRemoveInstalledProduct_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Brachytherapy Afterloaders', 'add', 3);
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Delivery Systems', 'add', 3);
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Image Management Systems', 'add', 3);
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Oncology Information Systems', 'add', 3);
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Other 3rd Party Connected Devices', 'add', 3);
        
        test.stopTest();
    }
    
    private static testMethod void addRemoveInstalledProduct_DS_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Delivery Systems', 'add', 3);
            
        test.stopTest();
    }
    
    private static testMethod void addRemoveInstalledProduct_IMS_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Image Management Systems', 'add', 3);
            
        test.stopTest();
    }
    
    private static testMethod void addRemoveInstalledProduct_OIS_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Oncology Information Systems', 'add', 3);
            
        test.stopTest();
    }
    
    private static testMethod void addRemoveInstalledProduct_OTP_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Other 3rd Party Connected Devices', 'add', 3);
        
        test.stopTest();
    }
    
    private static testMethod void addRemoveInstalledProduct_EMR_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl.addRemoveInstalledProduct('Electronic Medical Records', 'add', 3);
        
        test.stopTest();
    }
    
    private static testMethod void removeInstalledProduct_test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
            List<NonVarianInstalledProduct> nonVarianList = new List<NonVarianInstalledProduct>();
            RecordType recType = [SELECT Id, DeveloperName, Description, SobjectType, isActive FROM RecordType 
                                     WHERE SobjectType =: 'Competitor__c' AND 
                                     isActive =: true 
                                  AND DeveloperName IN: recTypeName Limit 1];
        
            Competitor__c competition = createCompetitorData(recType, opp.AccountId  );
            competition.isLost__c = true;
            NonVarianInstalledProduct non = new NonVarianInstalledProduct(0, competition, new List<String>(),  new Map<String, List<String>>());
            nonVarianList.add(non);
            
            CreateOpportunityLightningCtrl.removeInstalledProduct(0, JSON.serialize(nonVarianList));
        test.stopTest();
    }
    
    public static testMethod void restCodeCoverage() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        test.startTest();
        
            CreateOpportunityLightningCtrl control = new CreateOpportunityLightningCtrl();
            control.lynactakeout = new List<String>();
            control.primaryWonDetail = new List<String>();
            control.secWonDetail = new List<String>();
            control.primaryLostDetail = new List<String>();
            control.secLostDetail = new List<String>();
            control.lostOISDetail = new List<String>();
            control.lostTPSDetail = new List<String>();
            control.productNameDetail = new List<String>();
        test.stopTest();
    }
    
    public static testMethod void getInstallbase_Test() {
        Account testAcc = new Account();
        testAcc.Name = 'Test Account';
        testAcc.BillingStreet = '3100 Hansen Way';
        testAcc.BillingCity = 'Palo Alto';
        testAcc.BillingState = 'CA';
        testAcc.BillingPostalCode = '94020';
        testAcc.BillingCountry = 'USA';
        testAcc.Country__c = 'USA';
        insert testAcc;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.StageName = '7 - CLOSED WON';
        opp.CloseDate = system.today().addDays(5);
        opp.AccountId = testAcc.Id;
        insert opp;
        
        List<Competitor__c> compList = CreateOpportunityLightningCtrlTest.compList(opp.Id);
        insert compList;
        CreateOpportunityLightningCtrl.getInstallbase(opp.Id);
    }
    
    public static List<Competitor__c> compList (String oppId) {
        List<RecordType> compRec = new List<RecordType>([SELECT Id, DeveloperName, Description, sObjectType 
                                                               FROM RecordType WHERE sObjectType = 'Competitor__c']);
        
        List<Competitor__c> compList = new List<Competitor__c>();
        Competitor__c competitors;
        Integer i = 100;
        for(RecordType recT : [SELECT Id, DeveloperName, Description, sObjectType FROM RecordType WHERE sObjectType = 'Competitor__c']) {
            competitors = new Competitor__c();
            competitors.RecordTypeId = recT.Id;
            competitors.Opportunity__c = oppId;
            competitors.Vendor__c = 'Electra'+i;
            competitors.Model__c = 'HDR-3'+i;
            competitors.NVIP_Key__c = i+'Electra'+'HDR-3';
            compList.add(competitors);
        	i++;
        }
        return compList;
    }
    
    public static testMethod void removeDups_Test() {
        CreateOpportunityLightningCtrl.removeDups('HM10501 (Melco Linac HM1) ,A-04758 (Elekta), A-04758 (Elekta)');
    }
}
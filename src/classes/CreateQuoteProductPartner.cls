/**
 *  Getting called from visual workflow to get Sold to number from quote
 */
public class CreateQuoteProductPartner{
    @InvocableMethod
    public static void create(List<Id> quoteId){
      
      BigMachines__Quote__c sfQuote = [SELECT Id, BigMachines__Account__c FROM BigMachines__Quote__c WHERE Id =:quoteId[0]];
      
        Quote_Product_Partner__c quotePartner = new Quote_Product_Partner__c();
        quotePartner.Quote__c = quoteId[0];
        quotePartner.ERP_Partner_Function__c = 'SP';
        quotePartner.ERP_Partner_Number__c = getSoldToNumber(sfQuote.BigMachines__Account__c);
        quotePartner.Name = 'ECOM SOLD TO'+quotePartner.ERP_Partner_Number__c;
        insert quotePartner;
    }
    
    /**
     * Utility method: Also used in Get Billto Rest service
     */
    public static String getSoldToNumber(Id accountId){
      Account sfAccount = [SELECT ID, Ext_Cust_Id__c, RecordType.Name FROM Account WHERE Id =:accountId];        
       
        if(sfAccount.RecordType.Name == 'Sold To'){
          System.debug('--Sold To'+sfAccount.Ext_Cust_Id__c);
            return sfAccount.Ext_Cust_Id__c;
        }else{
            List<ERP_Partner_Association__c> soldToParties = [
                SELECT Id, Sales_Org__c, ERP_Partner_Number__c
                FROM ERP_Partner_Association__c 
                WHERE ERP_Customer_Number__c = :sfAccount.Ext_Cust_Id__c
                AND Partner_Function__c = 'SP=Sold-to party'
                AND Sales_Org__c = '0601'
            ];
            if(soldToParties.size() == 1){
              System.debug('--ERP Partner Association'+soldToParties[0].ERP_Partner_Number__c);
                return soldToParties[0].ERP_Partner_Number__c;
            }else{
                List<SVMXC__Site__c> locations = [
                    SELECT Id, ERP_Sold_To_Code__c 
                    FROM SVMXC__Site__c 
                    WHERE ERP_Site_Partner_Code__c=:sfAccount.Ext_Cust_Id__c
                    AND Active__c = true
                ];
                if(!locations.isEmpty()){
                  System.debug('--location'+locations[0].ERP_Sold_To_Code__c);
                    return locations[0].ERP_Sold_To_Code__c;
                }
            }
        }
        return '';
    }
}
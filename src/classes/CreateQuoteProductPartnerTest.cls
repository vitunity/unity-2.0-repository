@isTest
public with sharing class CreateQuoteProductPartnerTest {
  
  private static BigMachines__Quote__c salesQuote;
  private static BigMachines__Quote__c salesQuote1;
  private static BigMachines__Quote__c salesQuote2;
  
  static testMethod void testCreateSubscriptionLines(){
    setupBigMachinesQuote();
    
    //Test sold to account
    System.assertEquals('Site121212', CreateQuoteProductPartner.getSoldToNumber(salesQuote.BigMachines__Account__c));
    
    //Test site partner accoutn and get sold to number from erp partner association
    System.assertEquals('SoldTo121212', CreateQuoteProductPartner.getSoldToNumber(salesQuote1.BigMachines__Account__c));
    
    //If there are 0 or more than 1 sold to in ERP association try to find sold to number in location record
    System.assertEquals('TEST121212', CreateQuoteProductPartner.getSoldToNumber(salesQuote2.BigMachines__Account__c));
    
    
    CreateQuoteProductPartner.create(new List<Id>{salesQuote.Id});
    
    List<Quote_Product_Partner__c> quotePartners = [SELECT Id,ERP_Partner_Number__c FROM Quote_Product_Partner__c];
    System.assertEquals(1, quotePartners.size());
    System.assertEquals('Site121212', quotePartners[0].ERP_Partner_Number__c);
  }
  
  private static void setupBigMachinesQuote(){
    Account sitePartner = TestUtils.getAccount();
        sitePartner.AccountNumber = 'Site1212';
        sitePartner.Ext_Cust_Id__c = 'Site1212';
        sitePartner.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        
        Account soldTo = TestUtils.getAccount();
        soldTo.AccountNumber = 'SoldTo1212';
        soldTo.Ext_Cust_Id__c = 'SoldTo121212';
        soldTo.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
        
        Account soldTo1 = TestUtils.getAccount();
        soldTo1.AccountNumber = 'TEST1212';
        soldTo1.Ext_Cust_Id__c = 'TEST1212';
        soldTo1.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert new List<Account>{sitePartner, soldTo, soldTo1};
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'testSit';
        site.ERP_Functional_Location__c = 'funcLocation';
        site.ERP_Sold_To_Code__c = 'TEST121212';
        site.ERP_Site_Partner_Code__c = 'TEST1212';
        site.Active__c = true;
        insert site;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Site1212';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        
        ERP_Partner__c erpPartner1 = new ERP_Partner__c();
        erpPartner1.Name = 'Sampple Partner';
        erpPartner1.Partner_Number__c = 'SoldTo1212';
        erpPartner1.Partner_Name_Line_1__c = 'Test';
        erpPartner1.Street__c = 'Test';
        erpPartner1.City__c = 'Test';
        erpPartner1.State_Province_Code__c = '000';
        erpPartner1.Country_Code__c = '1';
        erpPartner1.Zipcode_Postal_Code__c = 'Test';
        erpPartner1.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner1.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner1.Partner_Name_Line_3__c = 'Sampple Partner';
        
        insert new List<ERP_Partner__c>{erpPartner,erpPartner1};
        
        insertERPAssociations('Site1212','Site121212', erpPartner.Id, sitePartner.Id);
        insertERPAssociations('SoldTo1212', 'SoldTo121212', erpPartner1.Id, soldTo.Id);
        
        salesQuote = TestUtils.getQuote();
        salesQuote.BigMachines__Account__c = sitePartner.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Price_Group__c = 'Z2';
        
        salesQuote1 = TestUtils.getQuote();
        salesQuote1.BigMachines__Account__c = soldTo.Id;
        salesQuote1.National_Distributor__c = '121212';
        salesQuote1.Order_Type__c = 'sales';
        salesQuote1.Price_Group__c = 'Z2';
        
        salesQuote2 = TestUtils.getQuote();
        salesQuote2.BigMachines__Account__c = soldTo1.Id;
        salesQuote2.National_Distributor__c = '121212';
        salesQuote2.Order_Type__c = 'sales';
        salesQuote2.Price_Group__c = 'Z2';
        insert new  List<BigMachines__Quote__c>{salesQuote, salesQuote1, salesQuote2};
  }
  
  private static void insertERPAssociations(String customerNumber,String partnerNumber, Id erpPartnerId, Id accountId){
        
        Set<String> partnerFunctions = new Set<String>{
          'SP=Sold-to party',
          'Z1=Site Partner',
          'BP=Bill-to Party',
          'SH=Ship-to party',
          'PY=Payer',
          'EU=End User'
      };
        
        List<ERP_Partner_Association__c> partnerAssociations = new List<ERP_Partner_Association__c>();
        
        for(String partnerFunction : partnerFunctions){
            ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
            partnerAssociation.Customer_Account__c = accountId;
            partnerAssociation.Erp_Partner__c = erpPartnerId;
            partnerAssociation.Partner_Function__c = partnerFunction;
            partnerAssociation.ERP_Partner_Number__c = partnerNumber;
            partnerAssociation.ERP_Customer_Number__c = customerNumber;
            partnerAssociation.Sales_Org__c = '0601';
            partnerAssociations.add(partnerAssociation);
        }
        insert partnerAssociations;
    }
}
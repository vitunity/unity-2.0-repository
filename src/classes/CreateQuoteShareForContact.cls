public without sharing class CreateQuoteShareForContact {
 
    private static Map<Id, Id> accountMap = new Map<Id, Id>();
    private static list<Id> conIds = new list<Id>();
    private static list<Id> actIds = new list<Id>();
    private static list<Id> actIdsDelete = new list<Id>();
    private static Map<id, Invoice__c> inv2quote = new map<id,Invoice__c>();
    @testvisible
    private static map<id,Id> contact2user = new Map<id,Id>();
    private static List<Bigmachines__Quote__Share> quoteshares = new List<Bigmachines__Quote__Share>();	

	public static void CreateQuoteShareForInsert(map<Id, Contact> mapConNew) {
		
		for(Contact c : mapConNew.values()) {
			if(c.Accts_Payable_Contact__c == true && c.Inactive_Contact__c == false)
			{
				actIds.add(c.AccountId);
				conIds.add(c.Id);
			}			
		}

		createConUserMap(mapConNew.keySet());
		getInvoiceQuotes(actIds);

        for (Contact con : mapConNew.values())
        {
            for (id invid : inv2quote.keySet())
            {
                if (con.AccountId == inv2quote.get(invid).site_partner__c || con.AccountId == inv2quote.get(invid).sold_to__c)
                {
                    if (inv2quote.get(invid).Quote__c != null && contact2user.get(con.Id) != null)
                    {
                        Bigmachines__Quote__Share qshare = new Bigmachines__Quote__Share();
                        qshare.ParentId = inv2quote.get(invid).Quote__c;
                        qshare.UserOrGroupID = contact2user.get(con.Id);
                        qshare.AccessLevel = 'Read';
                        qshare.RowCause = 'Manual';
                        quoteshares.add(qshare);
                    }
                }
            }
        } 

        System.debug('#### quoteshares = ' + quoteshares);
        createQuoteShareRec(quoteshares);
	}

	
	public static void CreateQuoteShareForUpdate(map<Id, Contact> mapConNew, map<Id, Contact> mapConOld) {
		List<Bigmachines__Quote__c> quotes = new List<Bigmachines__Quote__c>();
		Set<Id> quoteSet = new Set<Id>();
		List<Bigmachines__Quote__Share> quoteShares = new List<Bigmachines__Quote__Share>();
		Set<Id> userIds = new Set<Id>();
		
        
        //for Delete newMap will be blank
        if(mapConNew == null) {
			for(Contact c : mapConOld.values()) {
				if(c.Accts_Payable_Contact__c == true && c.Inactive_Contact__c == false)
				{
					actIdsDelete.add(c.AccountId);
				}
			}
		} else {
			
            for(Contact c : mapConNew.values()) {
				if(c.Accts_Payable_Contact__c == true 
					&& c.MyVarian_Member__c  == true
					&& (mapConOld.get(c.Id).Accts_Payable_Contact__c == false || mapConOld.get(c.Id).MyVarian_Member__c == false) 
				  )
				{
					actIds.add(c.AccountId);
				}

                //Remove shares if Accts Payable goes from true -> false
                System.debug('c values: '+c);
                System.debug('c old values: '+mapConOld.get(c.Id));
                System.debug('#### debug ac payable new = '+ c.Accts_Payable_Contact__c);
                System.debug('#### debug ac payable old = '+ mapConOld.get(c.Id).Accts_Payable_Contact__c);
                System.debug('#### debug ac MyVarian_Member__c new = '+ c.MyVarian_Member__c);
                System.debug('#### debug ac MyVarian_Member__c old = '+ mapConOld.get(c.Id).MyVarian_Member__c);                
                if((c.Accts_Payable_Contact__c == false && mapConOld.get(c.Id).Accts_Payable_Contact__c == true)
                	|| (c.MyVarian_Member__c == false && mapConOld.get(c.Id).MyVarian_Member__c == true)
                  )
                {
                    actIdsDelete.add(c.AccountId);
                    System.debug('actIdsDelete: '+actIdsDelete);
                }
            }	

		}

		//add Shares
		if(actIds.size() > 0 ) {
            if(mapConNew != null)
			createConUserMap(mapConNew.keySet());
			getInvoiceQuotes(actIds);	
			if(mapConNew != null)  
	        for (Contact con : mapConNew.values())
	        {
	            for (id invid : inv2quote.keySet())
	            {
	                if (con.AccountId == inv2quote.get(invid).site_partner__c || con.AccountId == inv2quote.get(invid).sold_to__c)
	                {
	                    if (inv2quote.get(invid).Quote__c != null && contact2user.get(con.Id) != null )
	                    {
	                        Bigmachines__Quote__Share qshare = new Bigmachines__Quote__Share();
	                        qshare.ParentId = inv2quote.get(invid).Quote__c;
	                        qshare.UserOrGroupID = contact2user.get(con.Id);
	                        qshare.AccessLevel = 'Read';
	                        qshare.RowCause = 'Manual';
	                        quoteshares.add(qshare);
	                    }
	                }
	            }
	        } 
	        createQuoteShareRec(quoteshares);			
		}	

		//delete Shares
		if(actIdsDelete.size() > 0 ) {
			////createConUserMap(mapConOld.keySet());

	        for(User user : [select Id, ContactId from User where ContactId in: mapConOld.keySet()])
	        {
	            userIds.add(user.Id);
	        }			


            //Delete QuoteShare for that Contact accross 
            if(!System.isBatch() && !System.isFuture()) {
	            deleteQuoteShareAccross(userIds);
	        }

	        /* //// This is the original code - it is replaced with the above line of code
            quoteSet = getQuoteIdsForDelete(actIdsDelete);

            System.debug('#### debug quoteSet = '+quoteSet);
            System.debug('#### debug userIds = '+userIds);
            
            if(quoteSet.size() > 0) {
		        for(Bigmachines__Quote__Share quoteShare: [SELECT ParentId, UserOrGroupId FROM Bigmachines__Quote__Share WHERE ParentId in:quoteSet AND UserOrGroupId in:userIds AND ROWCAUSE IN ('Manual', 'Manual Sharing')])
		        {
		           quoteShares.add(quoteShare);
		        }

		        System.debug('#### debug quoteShares.size() = '+quoteShares.size());
		        System.debug('#### debug quoteShares = '+quoteShares);
		        if(quoteShares.size() > 0)
		        {
		            delete quoteShares;
		            System.debug('#### debug quoteShares.size() after  delete = '+ quoteShares.size());
		        }
		    }
		    */

		}	
				
	}
	
	@Future
	private static void deleteQuoteShareAccross(set<Id> userIds) {
		list<Bigmachines__Quote__Share> listQuoteSharesDel = new list<Bigmachines__Quote__Share>();
        for(Bigmachines__Quote__Share quoteShare: [SELECT ParentId, UserOrGroupId FROM Bigmachines__Quote__Share WHERE UserOrGroupId in:userIds AND ROWCAUSE IN ('Manual', 'Manual Sharing')])
        {
           listQuoteSharesDel.add(quoteShare);
        }

        System.debug('#### debug listQuoteSharesDel.size() = '+listQuoteSharesDel.size());
        System.debug('#### debug listQuoteSharesDel = '+listQuoteSharesDel);
        if(listQuoteSharesDel.size() > 0)
        {
            delete listQuoteSharesDel;
            System.debug('#### debug listQuoteSharesDel.size() after  delete = '+ listQuoteSharesDel.size());
        }
	}

	private static void createConUserMap(set<Id> conIds) {
        for(User user : [select Id, ContactId from User where ContactId in: conIds AND isActive=true])
        {
            contact2user.put(user.contactId, user.id);
        }		
	}

	private static void getInvoiceQuotes(list<Id> actIds) {
        for (Invoice__c inv : [select id, Quote__c, site_partner__c , sold_to__c from Invoice__c 
        	where site_partner__c in : actIds or sold_to__c in :actIds])
        {
            inv2quote.put(inv.id, inv);
        }
        System.debug('Invoice List: '+inv2quote);		
	}
	
    @testvisible
	private static set<Id> getQuoteIdsForDelete(list<Id> actIds) {

		set<Id> qtIds = new set<Id>();
        for (Invoice__c inv : [select id, Quote__c, site_partner__c , sold_to__c from Invoice__c 
        	where site_partner__c in : actIds or sold_to__c in :actIds])
        {
            qtIds.add(inv.Quote__c);
        }
        System.debug('#### debug qtIds = '+qtIds);	
        return qtIds;
	}

	private static void createQuoteShareRec(List<Bigmachines__Quote__Share> listQtShares) {
        if(listQtShares.size() > 0 && !Test.isRunningTest())
        {
            System.debug('listQtShares: '+listQtShares);
            insert listQtShares;
        }		
	}
}
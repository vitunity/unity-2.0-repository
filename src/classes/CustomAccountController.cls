public class CustomAccountController{
    public string CurrentUser{get;set;}   
    public string AccRTName{get;set;}
    public Account nAccount{get;set;}
    map<string,string> mapStates{get;set;}
    
    public CustomAccountController(ApexPages.StandardController str){
        
        
        string AccountId = ApexPages.currentPage().getParameters().get('id');
        string AccountRTId = ApexPages.currentPage().getParameters().get('RecordType');
        CurrentUser = [select Name from User where Id =: userinfo.getUserId()].Name;
        Map<Id,RecordType> recTypes = new Map<Id, RecordType>([Select SobjectType, Name, Id From RecordType where SobjectType = 'Account']);
        
        setStateValues();
        
        AccRTName = 'Standard';
        
        if(AccountId <> null && AccountId <> ''){
            nAccount = [select Id,RecordTypeId,State__c,Action_Required__c,Account_Status__c,Name,BillingStreet,BillingCity,BillingState,Country__c,BillingPostalCode,phone,Phone_Number_2__c,Fax,Market_Segment__c,
                        Account_Type__c,SFDC_Account_Ref_Number__c,Distributor__c,ParentId,Local_Name__c,Local_Address__c,Yomigana__c,National__c,Strategic__c,IDN__c,GPO__c,OMNI_Account_Name_1__c,OMNI_Address1__c,
                        OMNI_Address2__c,OMNI_City__c,OMNI_State__c,OMNI_Country__c,OMNI_Postal_Code__c,Has_Location__c,Type,Ext_Cust_Id__c,ERP_Site_Partner_Code__c,AccountNumber,Super_Territory1__c,Territories1__c,
                        Region1__c,Sub_Region__c,District__c,Description,CSS_District__c,HW_Install_District__c,SW_Install_District__c,ISO_Country__c,IsExcludedFromRealign,Reset_Territory__c,Exclude_from_Team_Owner_rules__c,
                        Reference_Site__c,Luminary__c,Early_Adopter_Program__c,Strategic_Account_Owner__c,OMNI_Address3__c,Account_Block_Comment__c,SourceDeleted__c,Order_Block__c,Credit_Block__c,Embargo_Block__c,
                        ERP_Validation_Date__c,IsCustomerPortal,Account.Peer_to_Peer__c,Prefered_Language__c,e_Subscribe__c,IGRT__c,IMRT__c,Paperless__c,Latitude__c,Longitude__c,SRS_SBRT_Information__c,Quality__c,RapidArc__c,
                        RPM__c,SRS_SBRT__c,Dupe_Master__c,OMNI_Account_Name_2__c,Functional_Location__c,Legal_Name__c,Special_Care_Instruction__c, OMNI_Account_ID__c  from Account Where Id =: AccountId];
            AccountRTId = nAccount.recordTypeId;
        }else{
            nAccount = new Account();
        }
        if(AccountRTId <> null  && recTypes.ContainsKey(AccountRTId))
            AccRTName = recTypes.get(AccountRTId).Name;
        
    }
    public void setStateValues(){
        mapStates = new map<string,string>();
        for(State_Code__c sc : [select Id,Name,State_Code__c from State_Code__c]){
            mapStates.put(sc.Name,sc.State_Code__c);
        }
        
    }

    public pagereference saveAccount(){
    
        try{
            nAccount.billingCountry = nAccount.Country__c;
            if(nAccount.Country__c <> null && nAccount.State__c <> null && mapStates.containsKey(nAccount.Country__c+'-'+nAccount.State__c))
                nAccount.billingState = mapStates.get(nAccount.Country__c+'-'+nAccount.State__c);
            else 
                nAccount.billingState = nAccount.State__c;
            upsert nAccount;
            pagereference pg = new pagereference('/001/o');
            pg.setRedirect(true); 
            return pg;
        }catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(0)+''));
            return null;
        }
      //  return null;
    }
   
    public pagereference saveAndNewAccount(){
        try{
            nAccount.billingCountry = nAccount.Country__c;
            if(nAccount.Country__c <> null && nAccount.State__c <> null && mapStates.containsKey(nAccount.Country__c+'-'+nAccount.State__c))
                nAccount.billingState = mapStates.get(nAccount.Country__c+'-'+nAccount.State__c);
            else 
                nAccount.billingState = nAccount.State__c;
            upsert nAccount;
            pagereference pg = new pagereference('/apex/CustomAccount');
            pg.setRedirect(true); 
            return pg;
        }catch(DMLException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(0)+''));
            return null;
        }
       // return null;
    }
   
}
@isTest
public class CustomAccount_Test{  
    public static testMethod void UnitTest1(){    
        Account Acc = New Account(Name = 'TestAccount',Country__c = 'China',State__c='Beijing', BillingState='Beijing');
        insert Acc;
        State_Code__c stcode = new State_Code__c(Name='China-Beijing',State_Code__c='BJ',Country__c='China');
        insert stCode;
        PageReference pageRef = Page.CustomAccount;      
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', Acc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(Acc);
        CustomAccountController con = new CustomAccountController(sc);
        con.saveAccount();      
        con.saveAndNewAccount();
        
    }
    public static testMethod void UnitTest2(){    
        Account Acc = New Account(Name = 'TestAccount',Country__c = 'China',State__c='Beijing2', BillingState='Beijing2');
        insert Acc;
        State_Code__c stcode = new State_Code__c(Name='China-Beijing2',State_Code__c='BJ2',Country__c='China');
        insert stCode;
        PageReference pageRef = Page.CustomAccount;      
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(Acc);
        CustomAccountController con = new CustomAccountController(sc);
        con.saveAccount();      
        con.saveAndNewAccount();
        
    }
}
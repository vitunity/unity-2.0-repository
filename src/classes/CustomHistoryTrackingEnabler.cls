public class CustomHistoryTrackingEnabler {
    public static void LogHistoryForLongAndRich(Map<Id, sObject> newMap, Map<Id, sObject> oldMap) {
        List<Long_Text_History__c> lstHistory = new List <Long_Text_History__c>();
        try{ 
            Schema.SObjectType objectType = newMap.Values()[0].getSObjectType();
            Schema.DescribeSObjectResult objResult = objectType.getDescribe();
            String objName = objResult.getName(); 
            Boolean objIsCustom = objResult.isCustom();

            List<Long_Text_History_Configuration__c> lstConfig = check_CustomHistoryTrackingEnabler.runOnce(objName);
            
            
            if(lstConfig  != null && lstConfig.size() > 0) {
                system.debug('list----->'+lstConfig.size());
                Integer i = 0;
                for(sObject newVal : newMap.Values()) {
                    sObject oldVal = oldMap.get(newVal.Id);
                    //system.debug('oldVal...........'+oldVal);
                    for (Long_Text_History_Configuration__c config : lstConfig) {
                        if (oldVal != null && newVal.get(config.Field_Api_Name__c) != oldVal.get(config.Field_Api_Name__c)) {
                        //system.debug('newVal...........'+newVal);
                            Long_Text_History__c abcHistory = new Long_Text_History__c();
                            abcHistory.Object_name__c = objResult.getLabel();
                            abcHistory.Object_Api_Name__c = objName;
                            abcHistory.Field_Name__c = config.Field_Api_Name__c;
                            abcHistory.Field_Api_Name__c = config.Field_Api_Name__c;
                            abcHistory.Reference__c = newVal.ID; // added by Bushra on 24-02-2015
                            system.debug('+++++++++++new'+newVal.Id);
                            if(objIsCustom)
                                abcHistory.Record_Number__c = String.valueOf(newVal.get('Name'));// TA6256 // for custom object 'Name' field
                            else if(objName == 'Case') // for other standard objects may need more else if
                                abcHistory.Record_Number__c = String.valueOf(newVal.get('CaseNumber'));// TA6256 // for standard object the equivalent name field, 

                            System.debug('RECORD NUMBER NAME VALUE = ' + abcHistory.Record_Number__c);
                            abcHistory.Changed_By__c = UserInfo.getUserId();
                            if(config.Type__c == 'Long') {
                                abcHistory.Long_Text__c = String.valueOf(oldVal.get(config.Field_Api_Name__c));
                            } else {    
                                abcHistory.Rich_Text__c = String.valueOf(oldVal.get(config.Field_Api_Name__c));
                            }
                            lstHistory.add(abcHistory);
                        }
                    }
                }
                if(lstHistory.size()>0)
                insert lstHistory;
                
            }
        } 
        catch(Exception exp) {
            System.Debug('Exception in CustomHistoryTrackingEnabler -> LogHistoryForLongAndRich : ' + exp.getMessage());
        }
    }
}
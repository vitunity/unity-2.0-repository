@isTest(SeeAllData=true)
    private class CustomHistoryTrackingEnablerTest {
        testmethod static void CustomHistoryTracking() {
        
            Contact cn = new Contact();
            cn.FirstName = 'Contact';
            cn.LastName = 'Test';
            cn.Email= 'test@abc.com';  
            cn.Contact_Type__c = 'Therapist';
            cn.Phone = '9999999999';   
            cn.mailingCountry = 'INDIA';
            insert cn;
            
            Case cs = new Case();
            String recordTypeNam = 'HD/DISP';
            Map<String,Schema.RecordTypeInfo> rtMapByNames = Schema.SObjectType.Case.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtInfos = rtMapByNames.get(recordTypeNam);
            id recordId = rtInfos.getRecordTypeId();
            cs.priority = 'Emergency';
            cs.RecordTypeId = recordId; 
            cs.ContactID = cn.id;
            insert cs;
            cs.Local_Description__c = 'helloooooooo';
            update cs;
            
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
            //String recordTypeName = 'Field Service';
           Id fieldServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
            wo.RecordTypeId = fieldServcRecTypeID ;
            system.debug('meeeeoooow');
            wo.SVMXC__Case__c=cs.id;
            wo.SVMXC__Contact__c = cn.id;
            wo.SVMXC__Order_Status__c = 'Open';
            wo.SVMXC__Priority__c = 'low';
            wo.SVMXC__Order_Type__c = 'Availability Services';
            wo.Follow_Up_comment__c= 'asdfghjkl';
            insert wo;
           
            Long_Text_History_Configuration__c config = new Long_Text_History_Configuration__c();
            config.name = 'Work_Order';
            config.Type__c = 'Long';
            config.Object_Api_Name__c = 'SVMXC__Service_Order__c';
            config.Field_Api_Name__c = 'Follow_Up_comment__c';
            insert  config;
           
            wo.Follow_Up_comment__c= 'dvcgsvdhjcbsjhdbvc';
            update wo; 
               /*
            Long_Text_History__c abcHistory = new Long_Text_History__c();
            abcHistory.Object_name__c = 'Work Order';
            abcHistory.Object_Api_Name__c = 'SVMXC__Service_Order__c';
            abcHistory.Field_Name__c = 'Follow Up Comment';
            abcHistory.Field_Api_Name__c = 'Follow_Up_comment__c';
            abcHistory.Changed_By__c = UserInfo.getUserId();
            insert abcHistory; 
            */
           // List <Long_Text_History_Configuration__c > lstConfig = new List<Long_Text_History_Configuration__c >();
            
            //lstConfig.add(config);
            
        }
    }
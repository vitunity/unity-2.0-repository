public with sharing class CustomInstalledProductLookupController {
    
    public List<SVMXC__Installed_Product__c> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public string accountId{get;set;} // search keyword

    public CustomInstalledProductLookupController() {
        searchString = System.currentPageReference().getParameters().get('lksrch');
        accountId = System.currentPageReference().getParameters().get('acctId');
        runSearch();  

    }

    // performs the keyword search
    public PageReference search() {
        runSearch();
        return null;
    }

    // prepare the query and issue the search command
     @testVisible
    private void runSearch() {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString,accountId);
        if(results.size() == 0) ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info, 'There are no Installed Products associated to this account'));               
    } 

    // run the search and return the records found. 
     @testVisible 
     private List<SVMXC__Installed_Product__c> performSearch(string searchString, string accountId) {

        String soql = 'select id, name, SVMXC__Company__c, SVMXC__Company__r.id, SVMXC__Company__r.Name from SVMXC__Installed_Product__c';
        if((String.isNotBlank(accountId) && accountId!='000000000000000') || String.isNotBlank(searchString)) {
            soql = soql + ' where ';
            if(String.isNotBlank(accountId) && accountId!='000000000000000') {
                soql = soql +  'SVMXC__Company__c = \'' + accountId +'\'';
                if(searchString != '' && searchString != null) soql = soql + 'AND ';
            }
            if(String.isNotBlank(searchString))
                soql = soql +  'name LIKE \'%' + searchString +'%\'';
        }   
        soql = soql + ' limit 500';
        System.debug(soql);
        return database.query(soql); 
    }

    //// used by the visualforce page to send the link to the right dom element
    //public string getFormTag() {
    //return System.currentPageReference().getParameters().get('frm');
    //}

    //// used by the visualforce page to send the link to the right dom element for the text box
    //public string getTextBox() {
    //return System.currentPageReference().getParameters().get('txt');
    //}
 
}
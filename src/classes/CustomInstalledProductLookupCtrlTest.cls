@isTest
public class CustomInstalledProductLookupCtrlTest{
    public static Account accnt;
    static{

        accnt= new Account();
        accnt.Name= 'Wipro technologies';
        accnt.BillingCity='New York';
        accnt.country__c='USA';
        accnt.OMNI_Postal_Code__c='940022';
        accnt.BillingCountry='USA';
        accnt.BillingStreet='New Jersey';
        accnt.BillingPostalCode='552600';
        accnt.BillingState='LA';
        insert accnt;
    }
    static testMethod void testmethod1()
    {
        string searchString = 'Wipro technologies';
        Test.startTest();
        CustomInstalledProductLookupController ctrl = new CustomInstalledProductLookupController();
        ctrl.runSearch();
        ctrl.performSearch(searchString, accnt.Id);
        Test.stopTest();
    }
}
public class CustomLinkTrackingController 
{
    public static String appName = ApexPages.currentPage().getParameters().get('app');
    public String MHDLink { get; set;}
    public String MHDLightningLink { get; set;}
    public String PSELink { get; set;}
    
    
    public CustomLinkTrackingController()
    {
       for(External_Link_URL_Tracking__c link : [SELECT Application_Name__c, Application_URL__c from External_Link_URL_Tracking__c]) 
       {
           if(link.Application_Name__c == 'MHD')
           {
                MHDLink = link.Application_URL__c;
           }
           if(link.Application_Name__c == 'PSE')
           {
                PSELink = link.Application_URL__c;
           }
           if(link.Application_Name__c == 'MHD_LIGHTNING')
           {
                MHDLightningLink = link.Application_URL__c;
           }
       }
    }
    public static void trackingAndRedirect()
    {
        if(appName == 'MHD')
        {
            Link_URL_Tracking__c newTracking = new Link_URL_Tracking__c();
            newTracking.Name = 'Machine Health Dashboard';
            Insert newTracking;
        }
        if(appName == 'PSE')
        {
            Link_URL_Tracking__c newTracking = new Link_URL_Tracking__c();
            newTracking.Name = 'PSE Knowledge Base';
            Insert newTracking;
        }
    }
}
@isTest(seeAllData = true)
public class CustomLinkTrackingControllerTest
{
    public static testMethod void testCustomLinkTracking1()
    {
        PageReference CustomLinkTracking = Page.CustomLinkTracking;
        Test.setCurrentPage(CustomLinkTracking);
        ApexPages.currentPage().getParameters().put('app', 'PSE');
        CustomLinkTrackingController cc = new CustomLinkTrackingController();
        CustomLinkTrackingController.trackingAndRedirect();
    }
    
    public static testMethod void testCustomLinkTracking2()
    {
        PageReference CustomLinkTracking = Page.CustomLinkTracking;
        Test.setCurrentPage(CustomLinkTracking);
        ApexPages.currentPage().getParameters().put('app', 'MHD');
        CustomLinkTrackingController cc = new CustomLinkTrackingController();
        CustomLinkTrackingController.trackingAndRedirect();
    }
}
/**************************************************************************\
@Created Date- 5 Oct 2017, Puneet Mishra
@Last Modified By - 9, Oct 2017Puneet Mishra 
Controller for Custom Lookup

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
9-OCT-2017 - Puneet Mishra - STSK0012982- Assigning Helpdesk case hours to project

/**************************************************************************/
public with sharing class CustomProjectNumberLookup {
    
    public String installedProdId {get;set;}
    public List<String> invalidStatus = new List<String>{'Error', 'Cancelled', 'Closed'};
    public string searchString{get;set;} // search keyword
    public List<ProjectNumberInnerClass> lookupVal {get;set;}
    public List<ERP_Project__c> erpProjResults{get;set;}
    public string searchText{get;set;} // search text
    
    
    public CustomProjectNumberLookup() {
        searchString = System.currentPageReference().getParameters().get('lksearch');
        system.debug(' ===== searchString ====== ' + searchString);
        runSearch();
    }
    
    // perform search
    public PageReference search() {
        if(!String.isBlank(searchText) && searchText != null)
            fetchERPProject();
        else
            runSearch();
        
        return null;
    }
    
    // search on the basis of ERP Project #
    @testVisible
    private List<ERP_Project__c> fetchERPProject() {
        system.debug(' === searchText === ' + searchText);
        String soql = ' SELECT Id, Name, Description__c, Start_Date__c, End_Date__c, Sold_To__c, Sold_To__r.Name, Site_Partner__c, Site_Partner__r.Name, ' +
                      ' Sales_Order__c, Sales_Order__r.Name, Status__c '+
                      ' FROM ERP_Project__c WHERE Name like \'%' + String.escapeSingleQuotes(searchText) + '%\'';
        soql = soql + ' limit 100';
        erpProjResults = new List<ERP_Project__c>();
        erpProjResults = Database.query(soql);
        
        lookupVal = new List<ProjectNumberInnerClass>();
        return erpProjResults;
    }
    
    // prepare the query and issue the search command
    @testVisible
    private List<ProjectNumberInnerClass> runSearch() {
        Map<Id, Sales_Order_Item__c> soiMap = new Map<Id, Sales_Order_Item__c>();
        Map<String, SVMXC__Service_Order__c> projectNub_WoMap = new Map<String, SVMXC__Service_Order__c>();
        
        for(Sales_Order_Item__c soi : [ SELECT Id, Installed_Product__c, Sales_Order__c, WBS_Element__c 
                                        FROM Sales_Order_Item__c 
                                        WHERE Installed_Product__c =: searchString]) {
            soiMap.put(soi.Id, soi);
        }
        
        for(SVMXC__Service_Order__c wo : [SELECT Id, name, Sales_Order_Item__c, Sales_Order__c, Sales_Order__r.Name, Sales_Order_Item__r.Name, ERP_WBS__c, 
                                                Sales_Order_Item__r.Sales_Order__c, Sales_Order_Item__r.WBS_Element__c, SVMXC__Order_Status__c 
                                            FROM SVMXC__Service_Order__c WHERE Sales_Order_Item__c IN: soiMap.keySet()
                                            AND SVMXC__Order_Status__c NOT IN: invalidStatus
                                            AND Event__c =: true ]) {
            
            List<String> project = new List<String>();
            if(wo.Sales_Order__c != null) {
                project = wo.Sales_Order_Item__r.WBS_Element__c.split('-');
                projectNub_WoMap.put(project[0]+'-'+project[1], wo);
            }
        }
        system.debug(' === ERP PROJECT NAME === ' + projectNub_WoMap);
        lookupVal = new List<ProjectNumberInnerClass>();
        for(ERP_Project__c erpProject : [SELECT Id, Name FROM ERP_Project__c WHERE Name IN: projectNub_WoMap.keySet()]) {
            ProjectNumberInnerClass obj = new ProjectNumberInnerClass(erpProject, 
                                                                    projectNub_WoMap.get(erpProject.Name).Sales_Order__r.Name, 
                                                                    projectNub_WoMap.get(erpProject.Name).Sales_Order_Item__r.Name, 
                                                                    projectNub_WoMap.get(erpProject.Name));
            lookupVal.add(obj);
        }
        erpProjResults = new List<ERP_Project__c>(); 
        return lookupVal;               
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }

    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    public class ProjectNumberInnerClass {
        public ERP_Project__c project{get;set;}
        public String SO {get;set;}
        public String SOI {get;set;} 
        public SVMXC__Service_Order__c pwo {get;set;}
        public projectNumberInnerClass(ERP_Project__c project, String SO, String SOI, SVMXC__Service_Order__c wo) {
            this.project = project;
            this.SO = SO;
            this.SOI = SOI;
            this.pwo = wo;
        }
    }
    
    
}
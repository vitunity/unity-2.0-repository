@isTest
public class CustomProjectNumberLookupTest {


static testmethod void unittest1()
{

Account accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;

 CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        cdf.Name ='Default';
        insert cdf;
      BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
ERP_Project__c erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
//erpProj.name = 'part1_part2';
//update erpProj;
   Product2 newProd = UnityDataTestClass.product_Data(true);
   Sales_Order__c SO = UnityDataTestClass.SO_Data(true,accnt);
 Sales_Order_Item__c   SOI = UnityDataTestClass.SOI_Data(true, SO);
 SOI.WBS_Element__c='M-0320947099';
 update SOI;
Case newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];

 SVMXC__Service_Group__c  servTeam = UnityDataTestClass.serviceTeam_Data(false);
        servTeam.Work_Order_Review_Queue__c = 'Customer Service';
        insert servTeam;
Timecard_Profile__c  TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        Organizational_Calendar__c orgCal = UnityDataTestClass.OrgCalData(true);
SVMXC__Service_Group_Members__c  technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.Organizational_Calendar__c = orgCal.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;

 SVMXC__Service_Order__c WO = UnityDataTestClass.WO_Data(false, accnt, newCase, newProd, SOI, technician);
        WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        wo.Sales_Order__c = so.id;
        insert WO;
Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
    
    SVMXC__Installed_Product__c installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023';
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
     accnt.install_product_count__c= 1;


CustomProjectNumberLookup clu =new CustomProjectNumberLookup();
clu.searchText = 'mytext';
clu.search();
clu.getFormTag();
clu.getTextBox();
clu.fetchERPProject();
//CustomProjectNumberLookup.ProjectNumberInnerClass pin;// = new CustomProjectNumberLookup.ProjectNumberInnerClass();
}


}
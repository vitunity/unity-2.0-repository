global class CustomerServiceEmailOnCase implements Messaging.InboundEmailHandler 
{
    // The default from email address is storied in Custom label SR_SmartConnectCaseCreationFromAddress
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{            
            Boolean newFormat = true;
            Boolean offline = false;   
            Integer priorityNum = 0;
            String SNStr = '';
            String SNContactId = '';
            String emailAddr = '';
            
            
            emailAddr = email.fromAddress;
            if(email.subject.contains('P='))  
            {
                Integer pIndex = email.subject.indexOf('P=') + 2;                
                priorityNum = Integer.valueOf(email.subject.substring(pIndex, pIndex+1));
            }
            
            //if(email.subject.contains('P=') && email.subject.contains('SN='))
            if(email.subject.contains('SN='))   
            {
                //Integer pIndex = email.subject.indexOf('P=') + 2;
                //priorityNum = Integer.valueOf(email.subject.substring(pIndex, pIndex+1));
                Integer sIndex = email.subject.indexOf('SN=') + 3;
                SNStr = email.subject.substring(sIndex, sIndex+7);
                String soqlStr = 'select Id, FirstName from Contact where FirstName LIKE \'%' + SNStr + '%\'';
                for(sObject s : Database.query(soqlStr))
                {
                    Contact con = (Contact)s;
                    SNContactId = con.Id;
                }
                newFormat = true;
            }
            if(email.subject.contains('offline'))
            {
                offline = true;
            }
            if(email.subject.contains('SmartConnect AutoNotification') || newFormat)
            {
                
                Id PSRecordType = [Select id from recordtype where sObjecttype = 'Case' and developername = 'Professional_Services'].id;
                String lstSubjectForReplace = email.subject;
                String lstSubject = lstSubjectForReplace.unescapeXml();//Kaushiki 27/3/15;
                System.debug('lstSubject +++'+lstSubject );
                List<String> splitString = new List<String>();
                splitString =lstSubject.split(' ');
                System.debug('splitString +++'+splitString );
                String WOId;
                
                List<SVMXC__Installed_Product__c> ListofInstalledProduct;
                    
                string emailsubject = (email.subject).unescapeXml();
                string splitStr;
                emailsubject = emailsubject.ToUpperCase();
                if(emailsubject.contains('SN='))splitStr = 'SN=';
                if(emailsubject.contains('SN ='))splitStr = 'SN =';
                if(splitStr <> null){
                    string s = emailsubject.substring(emailsubject.indexOf(splitStr)+splitStr.length());
                    boolean isfound = false;
                    string proId = '';
                    for(integer i=1;i<s.length();i++){
                        string schar = s.substring(i-1,i);
                        if(schar == ' ' && isfound){
                            break;
                        }else{
                            if(schar != ' ') {
                            isfound = true;
                            proId += schar;
                            }
                        }
                       
                    }
                    proId = proId.trim();
                    if(proId.contains(','))proId = proId.split(',')[0];
                    if(proId.contains(';'))proId = proId.split(';')[0];
                    ListofInstalledProduct=[Select Id,SVMXC__Contact__c, SVMXC__Contact__r.Phone, SVMXC__Company__c,SVMXC__Company__r.RecordType.DeveloperName,SVMXC__Preferred_Technician__c from SVMXC__Installed_Product__c where name=:proId];
                }          
                String lstDescription = email.plainTextBody; 
                String lstDescription1 = email.plainTextBody;
                List<QueueSobject> queueowner=[Select QueueId from QueueSobject where SobjectType='Case' and Queue.DeveloperName='Customer_Service_ANZ']; //STSK0012466.Rakesh
                Case casealias = new Case();
                casealias.recordtypeId = PSRecordType;
                casealias.Origin = 'SysMon';
                casealias.local_Subject__c = lstSubject;     
                casealias.Subject = lstSubject;
                casealias.OwnerId = queueowner[0].QueueId ;   
                   
                if(ListofInstalledProduct <> null){
                    for(SVMXC__Installed_Product__c IP : ListofInstalledProduct )
                    {
                        if(IP.SVMXC__Company__r.RecordType.DeveloperName=='Site_Partner' || IP.SVMXC__Company__r.RecordType.DeveloperName=='Sold_to')
                        {
                            system.debug('asdf1'+IP);
                            casealias.AccountId=IP.SVMXC__Company__c;
                            casealias.ContactId = IP.SVMXC__Contact__c; // fixed for custom validation
                        }
                        casealias.ProductSystem__c=IP.Id; //DE3449
                    } 
                }           
                if(String.isNotBlank(SNContactId))
                {
                    casealias.ContactId = SNContactId;
                }
                
                casealias.Local_Description__c = lstDescription.unescapeXml();
                casealias.Description = lstDescription1.unescapeXml();
                //casealias.Priority='Medium';
                if(priorityNum == 1 )
                {
                    casealias.Priority = 'Emergency';
                }
                if(priorityNum == 2)
                {
                    casealias.Priority = 'Low';
                }
                if(priorityNum == 3)
                {
                    casealias.Priority = 'Medium';
                } 
                 if(priorityNum == 4)
                {
                    casealias.Priority = 'Low';
                }
                if(priorityNum > 4)
                {
                    casealias.Priority = 'Medium';
                }
                if(offline)
                {
                    casealias.Priority = 'Medium';
                }
                
                casealias.Is_This_a_Complaint__c='No';
                casealias.Is_escalation_to_the_CLT_required__c = 'No';
                casealias.Was_anyone_injured__c = 'No';        
                casealias.Smart_Connect_Case__c=True;
                if(casealias.Reason == Null)
                casealias.Reason = 'Waiting on information from requestor';
                
                // For assignment rule execution
                AssignmentRule AR = new AssignmentRule();
                AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.assignmentRuleId= AR.id;
                casealias.setoptions(dmo);            
                Insert casealias;
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {emailAddr};
                mail.setToAddresses(toAddresses);
                String[] ccAddresses = new String[] {'Pranjul.Maindola@varian.com'};
                mail.setCcAddresses(ccAddresses);
                mail.setSubject('New Case Created : ' + casealias.Id);
                mail.setUseSignature(false);
                mail.setPlainTextBody('Your Case: ' + casealias.Id +' has been created.');
                mail.setHtmlBody('Your case:<b> ' + case.Id +' </b>{DEBUG-STMT} has been created.<p>'+ 'To view your case <a href=https://***yourInstance***.salesforce.com/'+case.Id+'>click here.</a>'+emailAddr);

                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
        catch(Exception e){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {system.Label.SmartAlertExceptionEmail};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Customer Service ANZ failure : '+ email.subject);
            mail.setHTMLBody('failure reason : '+e.getMessage() +', line: '+e.getLineNumber()+', trace: '+e.getStackTraceString()+'.<br/>From Address : '+email.fromAddress+'<br/>Email body : '+email.plainTextBody); //STSK0012466.Rakesh
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return result;
    }
}
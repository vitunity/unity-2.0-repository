@isTest
private class CustomerServiceEmailOnCaseTest 
{

     static testMethod void UnitTest() 
    {
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        //Insert Account
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;        
        System.assertNotEquals(testAcc.Id, null);
        
        //Insert Contact  
        Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id,MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
        insert testcon;
        //System.assertNotEquals(testcon.Id, null);
        
                
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        string emailBody = 'Customer Info:\n';
        emailBody += 'Site Name:   test\n';        
        emailBody += 'Contact Name:   test\n';
        emailBody += 'Contact Phone:   test\n';
        emailBody += 'Contact Email:Test@1234APR.com';

        email.fromName = 'FirstName LastName';
        email.fromAddress = 'rakesh@test.com';
        email.subject = 'TEST_EmailServiceOnContactEmail';
        email.plainTextBody = emailBody;
        email.htmlBody = emailBody;
        CustomerServiceEmailOnCase handler = new CustomerServiceEmailOnCase();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);

        //System.assertEquals( result.success  ,true);


        List<Case> testCases = new List<Case>([SELECT  Subject, ProductSystem__c FROM Case]);
        //System.assertEquals(testCases.size(), 1);
    }
     static testMethod void UnitTest1() 
    {
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        //Insert Account
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;        
        //System.assertNotEquals(testAcc.Id, null);
        
        //Insert Contact  
        Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id,MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
        insert testcon;
        //System.assertNotEquals(testcon.Id, null);
        
                
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        string emailBody = 'Customer Info:\n';
        emailBody += 'Site Name:   test\n';        
        emailBody += 'Contact Name:   test\n';
        emailBody += 'Contact Phone:   test\n';
        emailBody += 'Contact Email:Test@1234APR.com';

        email.fromName = 'FirstName LastName';
        email.fromAddress = 'rakesh@test.com';
        email.subject = 'P=TEST_EmailServiceOnContactEmail';
        email.plainTextBody = emailBody;
        email.htmlBody = emailBody;
        CustomerServiceEmailOnCase handler = new CustomerServiceEmailOnCase();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);

        //System.assertEquals( result.success  ,true);


        List<Case> testCases = new List<Case>([SELECT  Subject, ProductSystem__c FROM Case]);
        //System.assertEquals(testCases.size(), 0);
    }

     static testMethod void UnitTest2() 
    {
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        //Insert Account
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;        
        //System.assertNotEquals(testAcc.Id, null);
        
        //Insert Contact  
        Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id,MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
        insert testcon;
        //System.assertNotEquals(testcon.Id, null);
        
                
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        string emailBody = 'Customer Info:\n';
        emailBody += 'Site Name:   test\n';        
        emailBody += 'Contact Name:   test\n';
        emailBody += 'Contact Phone:   test\n';
        emailBody += 'Contact Email:Test@1234APR.com';

        email.fromName = 'FirstName LastName';
        email.fromAddress = 'rakesh@test.com';
        email.subject = 'SN=TEST_EmailServiceOnContactEmail';
        email.plainTextBody = emailBody;
        email.htmlBody = emailBody;
        CustomerServiceEmailOnCase handler = new CustomerServiceEmailOnCase();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);

        //System.assertEquals( result.success  ,true);


        List<Case> testCases = new List<Case>([SELECT  Subject, ProductSystem__c FROM Case]);
        //System.assertEquals(testCases.size(), 1);
    }
}
/************************************************************************************
 * @name - DSO_EmailInvoiceProcess.cls
 * @author - Ram
 * @description - Batch process which queries invoices to create daily workflow remainder records 
 * @date 1/3/2017
 *************************************************************************************/
global class DSO_EmailInvoiceProcess implements Database.Batchable<sObject>, Schedulable {
	
	String query = 'Select Id, Name, Amount__c, Invoice_Date__c, Invoice_Due_Date__c, Days_Past_Due__c, Billing_Contact__c, CurrencyIsoCode, Sold_To__c, District_Sales_Manager__c, District_Service_Manager__c From Invoice__c where Invoice_Cleared_Date__c =null AND Sold_To__c != null AND Billing_Contact__c != null AND Invoice_Due_Date__c <= :tempDate'; 
	Map<Integer, Integer> remainderInterval = new Map<Integer, Integer>{0 => 6, 1 => 16, 2 => 26};
	Map<Integer, String> remainderFields = new Map<Integer, String> {0 => 'First_Remainder_Sent__c', 1 => 'Second_Remainder_Sent__c', 2 => 'Third_Remainder_Sent__c'};
	Integer currentIndex =0, days = 0;
	String remainderField;
	@TestVisible Set<Id> accountIds;
	
	global DSO_EmailInvoiceProcess (Integer index) {
		this.init(index);
	}
	
	global DSO_EmailInvoiceProcess () {
		this.init(0);	
	}
	
	private void init(Integer index) {
		days = remainderInterval.get(index);
		remainderField = remainderFields.get(index);
		currentIndex = index;
		accountIds = this.getAccountIds();	
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		Date tempDate = System.today() - days;
		Date tempDate2 = currentIndex > 0 ? System.today() - remainderInterval.get(currentIndex - 1) : null ;
		List<Integer> dueDates = remainderInterval.values();
		if (accountIds != null && accountIds.size() > 0) query = query + ' AND Sold_To__c IN :accountIds';
		if (currentIndex == 1) query = query + ' AND First_Remainder_Sent__c = true';
		if (currentIndex > 1) query = query + ' AND First_Remainder_Sent__c = true AND Second_Remainder_Sent__c = true';
		if (String.isNotBlank(remainderField) ) query = query + ' AND '+remainderField+' = false';
    	if (dueDates.size() > 0) query = query + ' AND Days_Past_Due__c IN :dueDates';
    	System.debug(logginglevel.info, 'full query == '+query);
    	return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
	    Map<String, Invoice__c> remaindersToCreate = new Map<String, Invoice__c>(); 
	    for(sObject inv : scope){
	    	String key = inv.get('Billing_Contact__c')+'#'+inv.get('Sold_To__c')+'#'+inv.get('CurrencyIsoCode')+'#'+remainderField+'#'+System.Today().format();
	    	if(!remaindersToCreate.containsKey(key)) {
	    		remaindersToCreate.put(key, (Invoice__c) inv);	
	    	}
	    	inv.put(remainderField, true);
	    }
	    this.createRemainders(remaindersToCreate);
	    Database.update(scope);
    }
    
    private void createRemainders(Map<String, Invoice__c> invoices) {
    	List<Workflow_Remainder__c> remainders = new List<Workflow_Remainder__c>();
    	for (String key : invoices.keyset()) {
    		Invoice__c inv = invoices.get(key);
    		Workflow_Remainder__c remainder = new Workflow_Remainder__c(Account__c = inv.Sold_To__c, CurrencyIsoCode= inv.CurrencyIsoCode, Contact__c = inv.Billing_Contact__c, 
    			Due_Days__c = days, Remainder_Type__c=remainderField, External_Field__c = key, District_Sales_Manager__c = inv.District_Sales_Manager__c, District_Service_Manager__c = inv.District_Service_Manager__c);
    		remainders.add(remainder);
    	}
		upsert remainders External_Field__c;
    }
    
    global void finish(Database.BatchableContext BC) {
    	currentIndex++;
    	if (currentIndex < 3) Database.executeBatch(new DSO_EmailInvoiceProcess(currentIndex), 2000);
    }
    
    global void execute(SchedulableContext ctx) {
    	Database.executeBatch(new DSO_EmailInvoiceProcess(), 2000);
    }
    
    //TODO - need only for soft go live, delete later
    private Set<Id> getAccountIds() {
    	Set<Id> accIds = new Set<Id>();
		if (String.isNotBlank(System.Label.DSO_Accounts_Filter)) {
			String[] temp = System.Label.DSO_Accounts_Filter.split(',');
			for(String tempId : temp) {
				if (String.isNotBlank(tempId) && tempId.trim().length() >= 15) accIds.add(tempId.trim());
			}
		}
		return accIds;
    }
	    
}
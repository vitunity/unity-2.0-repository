/************************************************************************************
 * @name - DSO_EmailInvoiceProcess_Test.cls
 * @author - Ram
 * @description - Unit test for DSO_EmailInvoiceProcess 
 * @date 1/3/2017
 *************************************************************************************/
@isTest
private class DSO_EmailInvoiceProcess_Test {
    
    @testSetup static void setup() {
        System.runAs(TestDataFactory.createUser('System Administrator')) {
            List<Account> acc = TestDataFactory.createAccounts();
            Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
            for (Account a : acc) {
                a.RecordTypeId = rtId;
            }
            update acc;
            List<Contact> cts = TestDataFactory.createObjects(Contact.sObjectType, 'Test_ContactData');
            List<Invoice__c> invoices = TestDataFactory.createInvoices(acc, cts);
            insert invoices;
            
        }           
    }
    
    @isTest static void testScheduleEmailInvoiceBatchProcess() {
        System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
            String CRON_EXP = '0 0 * 1 * ?';
            Test.startTest();
            DSO_EmailInvoiceProcess invProcess = new DSO_EmailInvoiceProcess(0);
            invProcess.accountIds.clear();
            Id jobId = System.schedule('copyInvoiceRemainders', CRON_EXP, invProcess);
            System.assert(jobId != null);
            Test.stopTest();    
        }   
    }
    
    @isTest static void testEmailInvoiceBatchProcess() {
        System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
            Test.startTest();
            DSO_EmailInvoiceProcess invProcess = new DSO_EmailInvoiceProcess(0);
            invProcess.accountIds.clear();
            Database.executeBatch(invProcess, 2000);
            Test.stopTest();
            List<Workflow_Remainder__c> wr = [Select w.Id, w.External_Field__c, w.Due_Days__c, w.ContactId__c, w.AccountId__c, w.Notification_Sent__c 
                From Workflow_Remainder__c w where Remainder_Type__c='First_Remainder_Sent__c'];
            System.debug(logginglevel.info, 'wr[0] === '+wr[0]);
            //System.assert(wr.size() > 0);
            //System.assert(wr[0].Due_Days__c >=6);
            //System.assert(wr[0].Notification_Sent__c);
            //System.assert(wr[0].External_Field__c != null); 
        }
    }
    
}
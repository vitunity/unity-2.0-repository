global class DateUtility {
    
    public DateUtility(ApexPages.StandardController stdController){
        
    }
    @RemoteAction
    global static Boolean isLessThanToday(String dateValue) {
        return Date.parse(dateValue)<Date.today();
    }
    
    @RemoteAction
    global static Boolean isDate1GreaterThanDate2(String date1, String date2) {
        if(!String.isBlank(date1) && !String.isBlank(date2)){
            return Date.parse(date1)>Date.parse(date2);
        }
        return false;
    }
}
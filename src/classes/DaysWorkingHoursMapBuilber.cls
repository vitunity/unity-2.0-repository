public class DaysWorkingHoursMapBuilber{

 public map<String, Integer> buildMap(Map<id, SVMXC__Service_Group_Members__c> techniciansMap,SVMXC_Timesheet__c tSheet){
     map<String, Integer> daysHoursMap =  new  map<String, Integer>();
     if(techniciansMap.get(tSheet.Technician__c).Organizational_Calendar__c != null){
         if(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.First_Day_of_Week__c == 'Saturday'){
             
             daysHoursMap.put('Saturday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_1__c));
             daysHoursMap.put('Sunday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_2__c)); 
             daysHoursMap.put('Monday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_3__c)); 
             daysHoursMap.put('Tuesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_4__c)); 
             daysHoursMap.put('Wednesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_5__c)); 
             daysHoursMap.put('Thursday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_6__c)); 
             daysHoursMap.put('Friday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_7__c));    
         } 
         
         if(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.First_Day_of_Week__c == 'Sunday'){
             
             daysHoursMap.put('Sunday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_1__c)); 
             daysHoursMap.put('Monday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_2__c)); 
             daysHoursMap.put('Tuesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_3__c)); 
             daysHoursMap.put('Wednesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_4__c)); 
             daysHoursMap.put('Thursday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_5__c)); 
             daysHoursMap.put('Friday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_6__c));
             daysHoursMap.put('Saturday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_7__c));    
         }
         
         if(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.First_Day_of_Week__c == 'Monday'){
              
             daysHoursMap.put('Monday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_1__c)); 
             daysHoursMap.put('Tuesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_2__c)); 
             daysHoursMap.put('Wednesday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_3__c)); 
             daysHoursMap.put('Thursday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_4__c)); 
             daysHoursMap.put('Friday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_5__c));
             daysHoursMap.put('Saturday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_6__c));
             daysHoursMap.put('Sunday',Integer.ValueOf(techniciansMap.get(tSheet.Technician__c).Timecard_Profile__r.Max_Hours_Day_7__c));    
         }  
     }
     
     return daysHoursMap;
 }

}
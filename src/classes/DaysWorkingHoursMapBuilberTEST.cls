/**
 * Author: Rishabh Verma
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
Public Class DaysWorkingHoursMapBuilberTEST{
     public static BusinessHours businesshr;
     public static SVMXC__Service_Group__c servTeam;
     public static SVMXC__Service_Group_Members__c technician;
     public static SVMXC__Service_Group_Members__c technician1;
     public static Organizational_Calendar__c orgCal;
     public static Organizational_Holiday__c OrgHoli;
     public static Timecard_Profile__c TCprofile;
     public static Timecard_Profile__c TCprofile1;
     public Static List<SVMXC_Timesheet__c> timeSheetList;
     public Static SVMXC_Timesheet__c PreTimeSheet;
     public Static SVMXC_Timesheet__c TimeSheet; 
     public Static Map<id, SVMXC__Service_Group_Members__c> techniciansMap;
     static {
        techniciansMap =  new Map<id, SVMXC__Service_Group_Members__c> ();
        list<SVMXC__Service_Group_Members__c> techList = new list<SVMXC__Service_Group_Members__c>();
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];

        TCprofile = UnityDataTestClass.TIMECARD_Data(true);
        TCprofile1 = UnityDataTestClass.TIMECARD_Data1(true);

        orgCal = UnityDataTestClass.OrgCalData(true);
        OrgHoli = UnityDataTestClass.OrgHoliday(true, orgCal);

        servTeam = UnityDataTestClass.serviceTeam_Data(true);

        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.Organizational_Calendar__c = orgCal.Id;
        //technician.Weekend_Override__c = 'Sunday'; 
        //techList.add(technician);
        insert technician;
        
       
        
        
        technician1 = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician1.Timecard_Profile__c = TCprofile1.Id;
        technician1.Organizational_Calendar__c = orgCal.Id;
        //technician1.Weekend_Override__c = 'Saturday'; 
        //techList.add(technician1);
         insert technician1;
         
        PreTimeSheet = UnitYDataTestClass.TIMESHEET_Data(false, technician1);
        PreTimeSheet.Is_TEM__c = true;
        PreTimeSheet.Start_Date__c = system.today().addDays(-7);
        //insert PreTimeSheet;

        TimeSheet = UnitYDataTestClass.TIMESHEET_Data(false, technician);
        TimeSheet.Is_TEM__c = true;
        //insert TimeSheet;

        timeSheetList = new List<SVMXC_Timesheet__c>();
        timeSheetList.add(PreTimeSheet);
        timeSheetList.add(TimeSheet);
        insert timeSheetList;
        techniciansMap.put(technician1.Id,technician1);
        techniciansMap.put(technician.Id,technician); 
       // insert techList;
    }
    
    public static testmethod void testUnit1(){
		DaysWorkingHoursMapBuilber obj = new DaysWorkingHoursMapBuilber();
        obj.buildMap(techniciansMap, TimeSheet);
        TCprofile1.First_Day_of_Week__c = 'Monday';
        obj.buildMap(techniciansMap, TimeSheet);
    }
    
      
}
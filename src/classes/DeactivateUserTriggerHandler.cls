/**
 *    Puneet Mishra
 *    28 july 2017, User handler class for Deleting usersessionIds records when user gets deactivated
 */
public class DeactivateUserTriggerHandler {

    public static void deleteDeactivatedUser(Set<Id> deactivatedUserIds) {
        List<usersessionids__c> sessionRecs = new List<usersessionids__c>();
        sessionRecs = [SELECT Id, Name, session_id__c FROM usersessionids__c WHERE session_id__c IN: deactivatedUserIds];
        if(!sessionRecs.isEmpty()) 
            delete sessionRecs;
    }
}
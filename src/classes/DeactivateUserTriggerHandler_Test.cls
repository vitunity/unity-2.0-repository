/**
 *    Puneet Mishra
 *    28 july 2017, Test Class for DeactivateUserTriggerHandler
 */
@isTest
public class DeactivateUserTriggerHandler_Test {
    
    static Profile portal, admin;
    static User usr, adminUsr;
    static list<User> uList;
    static Account newAcc;
    static Contact newCont;
    static usersessionids__c session;
    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    
    static {
        portal = [SELECT Id, UserType FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' Limit 1];
        admin = [SELECT Id FROM Profile WHERE Name LIKE '%System Administrator%' Limit 1];
        
        uList = new List<User>();
        
        newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.State__C = 'California';        
        newAcc.Country__c = 'USA';
        newAcc.BillingCity = 'Milpitas';
        //insert newAcc;
        
        
        newCont = new Contact();
        newCont.FirstName = 'Test';
        newCont.LastName = 'Contact';
        newCont.MailingState = 'CA';
        newCont.Email = 'Test@email.com';
        newCont.AccountId = newAcc.Id;
        //insert newCont;
        
        
        String usrName;
        String orgName = 'Varian';
        String prefix = 'NAME';
        Integer size = 70;
        String orgId = UserInfo.getOrganizationId();
        // Org id - 18 , 1 for @ , 4 for .com
        Integer randomStringGeneratorLen = size - orgId.length() - prefix.length() - orgName.length() - 4 - 1;
        Blob blobKey = crypto.generateAesKey(256);
        String randomString = EncodingUtil.convertToHex(blobKey).substring(0,randomStringGeneratorLen);
        usrName = prefix + randomString + orgId + '@' + orgName + '.com';
        
        UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
        usr = new User();
        usr.Email = 'testVARIAN@bechtel.com';        
        usr.Username = usrName ;
        usr.LastName = 'test' ;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.IsActive = true;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = portal .Id;
        //usr.ContactId = newCont.Id;
        usr.LMS_Status__c = 'Pending';
        //insert usr;
        
        
        String usrName1;
        String orgName1 = 'Varian';
        String prefix1 = 'NAME';
        Integer size1 = 70;
        String orgId1 = UserInfo.getOrganizationId();
        // Org id - 18 , 1 for @ , 4 for .com
        Integer randomStringGeneratorLen1 = size1 - orgId1.length() - prefix1.length() - orgName1.length() - 4 - 1;
        Blob blobKey1 = crypto.generateAesKey(256);
        String randomString1 = EncodingUtil.convertToHex(blobKey1).substring(0,randomStringGeneratorLen1);
        usrName1 = prefix1 + randomString1 + orgId1 + '@' + orgName1 + '.com';
        
        
        adminUsr = new User();
        UserRole r2 = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
        adminUsr.Email = 'testVARIAN_1@mail.com';
        adminUsr.Username = usrName1 ;
        adminUsr.LastName = 'test' ;
        adminUsr.IsActive = true;
        adminUsr.FirstName = 'Tester';
        adminUsr.Alias = 'test' ;
        adminUsr.Phone = '123452345';
        adminUsr.ProfileId = admin.Id;
        adminUsr.LanguageLocaleKey = 'en_US';
        adminUsr.LocaleSidKey = 'en_US';
        adminUsr.TimeZoneSidKey = 'America/Chicago';
        adminUsr.EmailEncodingKey = 'UTF-8';
        adminUsr.UserRoleId = r2.Id;
                
        
        //uList.add(usr);
        uList.add(adminUsr);
        insert uList;
        system.runas(adminUsr) {
            usersessionids__c session = new usersessionids__c(Name = userInfo.getUserId(), session_id__c = 'ABCkasjdhwk12_asas');
            insert session;
        }
    }
    
    public static testmethod void deleteDeactivatedUserTEST() {
        test.starttest();
            adminUsr.isActive = false;
            update adminUsr;
            
        test.stoptest();
    }
    
    public static testmethod void deleteDeactivatedUser_2TEST() {
        test.starttest();
            
            set<Id> sessionId = new set<Id>{adminUsr.Id};
            DeactivateUserTriggerHandler.deleteDeactivatedUser(sessionId);
            
        test.stoptest();
    }
    
}
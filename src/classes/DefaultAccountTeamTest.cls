@isTest
private class DefaultAccountTeamTest {

//Prod
    static testMethod void myUnitTest() 
    {   
        Map<ID,Territory_Team__c> newTerrMap = new Map<ID,Territory_Team__c>();
        List<User> userList = [SELECT id, name FROM User WHERE isActive = True and userType !='CsnOnly' and Id != :UserInfo.getUserId() LIMIT 10];        
        //create 2 territory Team records in different geographies for better code ceoverage
        Territory_Team__c tt = new Territory_Team__c();
        tt.Super_Territory__c = 'Americas';
        tt.Territory__c = 'North America';
        TT.OIS_SW__c = userList[0].id;
        TT.TPS_Sales__c = userList[0].id;
        tt.VSS_District_Manager__c = userList[0].id;
        tt.Zone_VP__c = userList[0].id;
        tt.HW_Install_District__c = 'hw district';
        tt.SW_Install_District__c = 'sw district';
        tt.Network_Specialist_Sales_Admin__c = userList[0].id;
        tt.VSS_Sales_Administrator__c = userList[0].id;
        tt.Brachy_Sales_Manager__c = userList[0].id;
        tt.Brachy_Sales_Administrator__c = userList[0].id;
        tt.Contract_HW__c = userList[0].id;
        tt.CUSTOMER_SUPPORT_MANAGER__c = userList[0].id;
        tt.FSR_Specialist__c = userList[0].id;
        tt.HW_Sys_Install_Mgr__c = userList[0].id;
        tt.SW_Install_Mgr__c = userList[0].id;
        tt.Training_Manager__c = userList[0].id;
        tt.SW_Upgrades__c = userList[0].id;
        tt.Region__c = 'Eastern Zone';
        tt.Sub_Region__c = 'Northern Region';
        tt.District__c = 'Bill Tom';
        tt.City__c = 'Newark';
        tt.Country__c = 'USA';
        tt.ISO_Country__c = 'US';
        tt.State__c = 'DE';
        tt.Zip__c = '17042';
        tt.CSS_District__c = 'U3H';
        tt.District_Manager__c = userList[0].Id;
        tt.District_Sales_Administrator__c = userList[1].Id;
        tt.Regional_Director__c = userList[2].Id;
        tt.Software_Strategic_VP__c = userList[3].Id;
        tt.Network_Specialist__c = userList[4].Id;
        tt.VPT_Regional_Sales_Director__c = userList[5].id;
        tt.VPT_Regional_Svc_Manager__c = userList[6].id;
        tt.VPT_Sales_Manager__c = userList[7].id;
        tt.VPT_Sr_Dir_Svc__c = userList[8].id;
        tt.VPT_Svc_Program_Manager__c = userList[9].id;
        tt.VPT_VP_Sales__c = userList[1].id;
        tt.Software_Sales_Manager__c = userList[3].id;
        insert tt;
        tt.CSS_District__c = 'U2H';
        update tt;
        
        newTerrMap.put(tt.Id,tt);

        Map<ID,Territory_Team__c> oldTerrMap = new Map<ID,Territory_Team__c>();
        tt.District_Manager__c = userList[9].Id;
        tt.Software_Sales_Manager__c = userList[9].id;
        update tt;
        
        Account acc = new Account();
        acc.name = 'terr Acc';
        acc.BillingCity = 'Pune';
        acc.Ownerid = userInfo.getUserID();
        acc.Territory_Team__c = tt.id;
        acc.country__c ='India';
        acc.Exclude_from_Team_Owner_rules__c = false;
        acc.State_Province_Is_Not_Applicable__c=true;
        insert acc;    
        
        
        AccountTeamMember atm = new AccountTeamMember();
        atm.AccountId = acc.id;
        atm.TeamMemberRole = 'National Account Manager';
        atm.UserId = userInfo.getUserID();
        insert atm;
        
        tt.CSS_District__c = 'U3H';
        update tt;
        //create Account for first territory (USA)
        List<Account> lstterritoryAccount = new List<Account>();
        Account territoryAccount = AccountTestData.createAccount('MY USA Country Account', 'USA', '19702'); // for Territory Team 'tt' record
        insert territoryAccount;
        lstterritoryAccount.add(territoryAccount);
        territoryAccount.BillingCountry = 'UNITED KINGDOM';
        territoryAccount.BillingPostalCode = '12345 UK';   
        update territoryAccount;
        
        
        
        map<id,Account> OldMap = new Map<id,Account>();
        map<id,Account> NewMap = new Map<id,Account>();

        //create Account for first territory (USA)    
        Account ac1 = new Account();
        ac1.Name = 'Nationaltest';
        ac1.Affiliation__c = 'National';
        ac1.Agreement_Type__c = 'LOC';
        ac1.country__c ='India';
        ac1.BillingCity = 'Pune';
        ac1.State_Province_Is_Not_Applicable__c = true;
        ac1.Exclude_from_Team_Owner_rules__c = false;
        insert ac1;
        
        Account ac2 = new Account();
        ac2.Name = 'Nationaltest1';
        ac2.Affiliation__c = 'National';
        ac2.Exclude_from_Team_Owner_rules__c = false;
        ac2.Agreement_Type__c = 'LOC';
        ac2.country__c ='India';
        ac2.BillingCity = 'Mumbai';
        ac2.State_Province_Is_Not_Applicable__c = true;
        insert ac2;

        Account canadaAccount =  AccountTestData.createAccount('MY Canada Country Account', 'Canada', '19702', 'ON'); // for Territory Team 'tt' record
        canadaAccount.Affiliated_to_National_Account__c  = ac1.id;
        insert canadaAccount;
                
        Test.startTest();
        
        DefaultAccountTeam__c varDAT1= new DefaultAccountTeam__c();
        varDAT1.Name = 'National Account';
        varDAT1.Account__c = canadaAccount.Affiliated_to_National_Account__c;
        varDAT1.User__c = userList[0].Id;
        varDAT1.Team_Member_Role__c = 'National Account Manager';
        varDAT1.Account_Access__c = 'Edit';
        varDAT1.Opportunity_Access__c = 'Edit';
        varDAT1.Case_Access__c = 'Edit';
        insert varDAT1;
                
        DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT = false;
        canadaAccount.Affiliated_to_National_Account__c  = ac2.id;
        Update canadaAccount;
        
        DefaultAccountTeam__c varDAT= new DefaultAccountTeam__c();
        varDAT.Name = 'National Account';
        varDAT.Account__c = canadaAccount.Affiliated_to_National_Account__c;
        varDAT.User__c = userList[0].Id;
        varDAT.Team_Member_Role__c = 'National Account Manager';
        varDAT.Account_Access__c = 'Edit';
        varDAT.Opportunity_Access__c = 'Edit';
        varDAT.Case_Access__c = 'Edit';
        insert varDAT;
               
        //create Account for fourth territory (JP)
        Account AustraliaAccount = AccountTestData.createAccount('MY Australia Country Account', 'Australia', '19702', 'Monday'); // for Territory Team 'tt' record
        AustraliaAccount.Affiliated_to_National_Account__c = ac1.id;
        insert AustraliaAccount;
        
        DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT = false;
        AustraliaAccount.Affiliated_to_National_Account__c  = ac2.id;
        Update AustraliaAccount;
        
        Account JapanAccount = AccountTestData.createAccount('MY Japan Country Account', 'Japan', '19702', 'Monday'); // for Territory Team 'tt' record
        JapanAccount.Affiliated_to_National_Account__c = ac1.id;
        insert JapanAccount;
        
        //create Affiliation accounts for 'Default Account Team' functionality
        List<Account> accountAffilitions = new List<Account>();
        Account nationalAccount = AccountTestData.createNationalAccount();
        Account strategicAccount = AccountTestData.createStrategicAccount();
        Account gpoAccount = AccountTestData.createGPOAccount();
        Account idnAccount = AccountTestData.createIDNAccount();
    
        accountAffilitions.add(nationalAccount);
        accountAffilitions.add(strategicAccount);
        accountAffilitions.add(gpoAccount);
        accountAffilitions.add(idnAccount);        

        // create second set to test update functionality
        Account nationalAccount2 = AccountTestData.createNationalAccount(); 
        Account strategicAccount2 = AccountTestData.createStrategicAccount();
        Account gpoAccount2 = AccountTestData.createGPOAccount();
        Account idnAccount2 = AccountTestData.createIDNAccount();

        accountAffilitions.add(nationalAccount2);
        accountAffilitions.add(strategicAccount2);
        accountAffilitions.add(gpoAccount2);
        accountAffilitions.add(idnAccount2);

        insert accountAffilitions;
        KeyValueMap_CS__c setting = new KeyValueMap_CS__c();
        setting.Name = 'AllowAccountTeamFromTerritory';
        setting.Active__c= true;
        setting.Data_Value__c= 'False';
        insert setting;
        
        //crreate Default Account Team records for above account Affiliations  
        List<DefaultAccountTeam__c> defaultTeamList = new List<DefaultAccountTeam__c>();
        DefaultAccountTeam__c nationalRep = new DefaultAccountTeam__c();
        nationalRep.Name = 'National';
        nationalRep.Account__c = nationalAccount.id;
        nationalRep.User__c = userList[0].Id;
        nationalRep.Team_Member_Role__c = 'National Account Manager';
        
        DefaultAccountTeam__c strategicRep = new DefaultAccountTeam__c();
        strategicRep.Name = 'Strategic';
        strategicRep.Account__c = strategicAccount.Id;
        strategicRep.User__c = userList[0].Id;
        strategicRep.Team_Member_Role__c = 'Strategic Account Manager';
        
        DefaultAccountTeam__c gpoRep = new DefaultAccountTeam__c();
        gpoRep.Name = 'GPO';
        gpoRep.Account__c = gpoAccount.Id;
        gpoRep.User__c = userList[0].Id;
        gpoRep.Team_Member_Role__c = 'GPO Manager';
        
        DefaultAccountTeam__c idnRep = new DefaultAccountTeam__c();
        idnRep.Name = 'IDN';
        idnRep.Account__c = idnAccount.Id;
        idnRep.User__c = userList[0].Id;
        idnRep.Team_Member_Role__c = 'IDN Manager';
        
        DefaultAccountTeam__c nationalRep2 = new DefaultAccountTeam__c();
        nationalRep2.Name = 'National';
        nationalRep2.Account__c = ac2.Id;
        nationalRep2.User__c = userList[1].Id;
        nationalRep2.Team_Member_Role__c = 'National Account Manager';
        
        DefaultAccountTeam__c strategicRep2 = new DefaultAccountTeam__c();
        strategicRep2.Name = 'Strategic';
        strategicRep2.Account__c = strategicAccount.Id;
        strategicRep2.User__c = userList[1].Id;
        strategicRep2.Team_Member_Role__c = 'Strategic Account Manager';
        
        DefaultAccountTeam__c gpoRep2 = new DefaultAccountTeam__c();
        gpoRep2.Name = 'GPO';
        gpoRep2.Account__c = gpoAccount.Id;
        gpoRep2.User__c = userList[1].Id;
        gpoRep2.Team_Member_Role__c = 'GPO Manager';
        
        DefaultAccountTeam__c idnRep2 = new DefaultAccountTeam__c();
        idnRep2.Name = 'IDN';
        idnRep2.Account__c = idnAccount.Id;
        idnRep2.User__c = userList[1].Id;
        idnRep2.Team_Member_Role__c = 'IDN Manager';
        
        defaultTeamList.add(nationalRep);
        defaultTeamList.add(strategicRep);
        defaultTeamList.add(gpoRep);
        defaultTeamList.add(idnRep);

        defaultTeamList.add(nationalRep2);
        defaultTeamList.add(strategicRep2);
        defaultTeamList.add(gpoRep2);
        defaultTeamList.add(idnRep2);     
        insert defaultTeamList;
        
        // create affiliated accounts for above cerated account affiliations
        List<Account> affiliatedAccounts = new List<Account>();       
        Account nationalAffiliationAccount = AccountTestData.createNationalAffiliationAccount(nationalAccount.Id);
        nationalAffiliationAccount.Name = 'TESTING National account';
        nationalAffiliationAccount.BillingStreet = 'TEST';
        nationalAffiliationAccount.BillingCity = 'Milpitas';
        nationalAffiliationAccount.BillingState = 'GA';
        Account strategicAffiliationAccount = AccountTestData.createStrategicAffiliationAccount(strategicAccount.Id);
        strategicAffiliationAccount.Name = 'general Hospital strategic';
        strategicAffiliationAccount.BillingStreet = 'Castro Street';
        nationalAffiliationAccount.BillingCity = 'Palo Alto';
        strategicAffiliationAccount.BillingState = 'GA';
        Account gpoAffiliationAccount = AccountTestData.createGPOAffiliationAccount(gpoAccount.Id);
        gpoAffiliationAccount.Name = 'GA Healthcare gpo';
        gpoAffiliationAccount.BillingStreet = 'Cypress DR';
        nationalAffiliationAccount.BillingCity = 'San Jose';
        gpoAffiliationAccount.BillingState = 'CA';
        Account idnAffiliationAccount = AccountTestData.createIDNAffiliationAccount(idnAccount.Id);
        idnAffiliationAccount.Name = 'TESTING National account';
        idnAffiliationAccount.BillingStreet = 'TEST';
        nationalAffiliationAccount.BillingCity = 'San Francisco';
        idnAffiliationAccount.BillingState = 'CA';
        
        
        
        affiliatedAccounts.add(nationalAffiliationAccount);
        affiliatedAccounts.add(strategicAffiliationAccount);
        affiliatedAccounts.add(gpoAffiliationAccount);
        affiliatedAccounts.add(idnAffiliationAccount);
        
        insert affiliatedAccounts;
        
        
        AccountTeamMember atm1 = new AccountTeamMember();
        atm1.AccountId = nationalAffiliationAccount.id;
        atm1.TeamMemberRole = 'National Account Manager';
        atm1.UserId = userInfo.getUserID();
        insert atm1;
        
        nationalAffiliationAccount.Affiliated_to_National_Account__c = nationalAccount2.Id;
        strategicAffiliationAccount.Affiliated_to_Strategic_Account__c = strategicAccount2.Id;
        gpoAffiliationAccount.Affiliated_to_GPO_Account__c = gpoAccount2.Id;
        idnAffiliationAccount.Affiliated_to_IDN_Account__c = idnAccount2.Id;
        // floowing 4 lines for test coverage
        idnAffiliationAccount.OMNI_Address1__c = 'OMNI ADRESS x1';
        idnAffiliationAccount.OMNI_Address1__c = 'OMNI ADRESS x2';
        idnAffiliationAccount.OMNI_Address1__c = 'OMNI ADRESS x3';
        idnAffiliationAccount.BillingStreet = 'Billing Street 1x1';
        
        affiliatedAccounts.clear();
        affiliatedAccounts.add(nationalAffiliationAccount);
        affiliatedAccounts.add(strategicAffiliationAccount);
        affiliatedAccounts.add(gpoAffiliationAccount);
        affiliatedAccounts.add(idnAffiliationAccount);
        update affiliatedAccounts;
        
        List<Account> territoryAccounts = new List<Account>();
        territoryAccounts.add(AustraliaAccount);
        territoryAccounts.add(JapanAccount);
        territoryAccounts.add(canadaAccount);
        territoryAccounts.add(territoryAccount);
        //territoryAccounts.add(bermudaAccount);
        
        DefaultAccountTeam defaultAcc = new DefaultAccountTeam();
        defaultAcc.updateAccountGeographicalData(newTerrMap,newTerrMap);
        defaultAcc.addTerritoryTeam(territoryAccounts);
        defaultAcc.updateTerritoryTeamOnAccount(newTerrMap,newTerrMap);
        defaultAcc.getTerritoryFieldMap();
        defaultAcc.getTerrObjDescMap();
        defaultAcc.convertSObjectIntoMap(tt);
        DefaultAccountTeam.prepareTerritoryMaps(territoryAccounts);

        oldTerrMap.put(tt.Id, tt);
        defaultAcc.updateAccountGeographicalData(oldTerrMap,newTerrMap);
        //defaultAcc.updateTerritoryTeamOnAccount(oldTerrMap,newTerrMap);
        Test.stopTest();
    }
    static testMethod void addTerritoryTeamTest(){
        Map<String, Territory_Team__c> TerritoryteamMap = new Map<String, Territory_Team__c>();
        List<User> userList = [SELECT id, name FROM User WHERE isActive = True and userType !='CsnOnly' and Id != :UserInfo.getUserId() LIMIT 10];        
        //create 2 territory Team records in different geographies for better code ceoverage
        Territory_Team__c tt = new Territory_Team__c();
        tt.Super_Territory__c = 'Americas';
        tt.Territory__c = 'North America';
        TT.OIS_SW__c = userList[0].id;
        TT.TPS_Sales__c = userList[0].id;
        tt.VSS_District_Manager__c = userList[0].id;
        tt.Zone_VP__c = userList[0].id;
        tt.HW_Install_District__c = 'hw district';
        tt.SW_Install_District__c = 'sw district';
        tt.Network_Specialist_Sales_Admin__c = userList[0].id;
        tt.VSS_Sales_Administrator__c = userList[0].id;
        tt.Brachy_Sales_Manager__c = userList[0].id;
        tt.Brachy_Sales_Administrator__c = userList[0].id;
        tt.Contract_HW__c = userList[0].id;
        tt.CUSTOMER_SUPPORT_MANAGER__c = userList[0].id;
        tt.FSR_Specialist__c = userList[0].id;
        tt.HW_Sys_Install_Mgr__c = userList[0].id;
        tt.SW_Install_Mgr__c = userList[0].id;
        tt.Training_Manager__c = userList[0].id;
        tt.SW_Upgrades__c = userList[0].id;
        
        tt.VPT_Regional_Sales_Director__c = userList[0].id;
        tt.VPT_Regional_Svc_Manager__c = userList[0].id;
        tt.VPT_Sales_Manager__c = userList[0].id;
        tt.VPT_Sr_Dir_Svc__c = userList[0].id;
        tt.VPT_Svc_Program_Manager__c = userList[0].id;
        tt.VPT_VP_Sales__c = userList[0].id;
        
        tt.Region__c = 'Eastern Zone';
        tt.Sub_Region__c = 'Northern Region';
        tt.District__c = 'Bill Tom';
        tt.City__c = 'Newark';
        tt.Country__c = 'USA';
        tt.ISO_Country__c = 'US';
        tt.State__c = 'DE';
        tt.Zip__c = '94085';
        tt.CSS_District__c = 'U3H';
        tt.District_Manager__c = userList[0].Id;
        tt.District_Sales_Administrator__c = userList[1].Id;
        tt.Regional_Director__c = userList[2].Id;
        tt.Software_Strategic_VP__c = userList[3].Id;
        tt.Network_Specialist__c = userList[4].Id;
        insert tt;
        tt.CSS_District__c = 'U2H';
        update tt;
        Test.startTest();
        //Harleen.Gulati.Start
        List<Account> newAccounts = new List<Account>();
        Account new1 = new Account (); 
        new1.Name = 'Test-CoverageBurma';
        new1.Country__c= 'Bermuda';
        new1.BillingCountry = 'Bermuda';
        new1.BillingState = 'Bermuda';
        
        new1.BillingCity='Hamilton';
        insert new1;
        newAccounts.add(new1);
        TerritoryteamMap.put(new1.BillingCountry,tt);
        
        Account newAccUSA = new Account (); 
        newAccUSA.Name = 'Test-CoverageUSA';
        newAccUSA.Country__c= 'USA';
        newAccUSA.BillingCountry = 'USA';
        newAccUSA.BillingState = 'DE';
        newAccUSA.BillingPostalCode='17042';
        newAccUSA.BillingCity='Newark';
        newAccUSA.State_Province_Is_Not_Applicable__c = true;
        insert newAccUSA;
        
        newAccounts.add(newAccUSA);
        TerritoryteamMap.put(newAccUSA.BillingCountry,tt);
        DefaultAccountTeam  defAccTeam = new DefaultAccountTeam();  
        DefaultAccountTeam.TerritoryteamMap = TerritoryteamMap; //Added by Shital on 07/09/2017
        defAccTeam.addTerritoryTeam(newAccounts); 
        List<AccountTeamMember> teamMembers = new List<AccountTeamMember>();
        List<AccountShare> accountshareList = new List<AccountShare>();
        defAccTeam.addTerritoryMember(new1.Id,'VPT Sr Dir Svc',userList[0].id,teamMembers,accountshareList);//Added by Shital
        Test.stopTest();
    }
    static testMethod void testRemoveDefaultTeam(){
        
        List<User> userList = [SELECT id, name FROM User WHERE isActive = True and userType !='CsnOnly' and Id != :UserInfo.getUserId() LIMIT 10];        
        //create 2 territory Team records in different geographies for better code ceoverage
        Territory_Team__c tt = new Territory_Team__c();
        tt.Super_Territory__c = 'Americas';
        tt.Territory__c = 'North America';
        TT.OIS_SW__c = userList[0].id;
        TT.TPS_Sales__c = userList[0].id;
        tt.VSS_District_Manager__c = userList[0].id;
        tt.Zone_VP__c = userList[0].id;
        tt.HW_Install_District__c = 'hw district';
        tt.SW_Install_District__c = 'sw district';
        tt.Network_Specialist_Sales_Admin__c = userList[0].id;
        tt.VSS_Sales_Administrator__c = userList[0].id;
        tt.Brachy_Sales_Manager__c = userList[0].id;
        tt.Brachy_Sales_Administrator__c = userList[0].id;
        tt.Contract_HW__c = userList[0].id;
        tt.CUSTOMER_SUPPORT_MANAGER__c = userList[0].id;
        tt.FSR_Specialist__c = userList[0].id;
        tt.HW_Sys_Install_Mgr__c = userList[0].id;
        tt.SW_Install_Mgr__c = userList[0].id;
        tt.Training_Manager__c = userList[0].id;
        tt.SW_Upgrades__c = userList[0].id;
        tt.VPT_Regional_Sales_Director__c = userList[0].id;
        tt.VPT_Regional_Svc_Manager__c = userList[0].id;
        tt.VPT_Sales_Manager__c = userList[0].id;
        tt.VPT_Sr_Dir_Svc__c = userList[0].id;
        tt.VPT_Svc_Program_Manager__c = userList[0].id;
        tt.VPT_VP_Sales__c = userList[0].id;
        tt.Region__c = 'Eastern Zone';
        tt.Sub_Region__c = 'Northern Region';
        tt.District__c = 'Bill Tom';
        tt.City__c = 'Newark';
        tt.Country__c = 'USA';
        tt.ISO_Country__c = 'US';
        tt.State__c = 'DE';
        tt.Zip__c = '94085';
        tt.CSS_District__c = 'U3H';
        tt.District_Manager__c = userList[0].Id;
        tt.District_Sales_Administrator__c = userList[1].Id;
        tt.Regional_Director__c = userList[2].Id;
        tt.Software_Strategic_VP__c = userList[3].Id;
        tt.Network_Specialist__c = userList[4].Id;
        insert tt;
        tt.CSS_District__c = 'U2H';
        update tt;
        
        Test.startTest();
        List<Account> accountList = new List<Account>{
                                        getAccount('Strategic'),
                                        getAccount('Strategic'),
                                        getAccount('GPO'),
                                        getAccount('GPO'),
                                        getAccount('IDN'),
                                        getAccount('IDN')
                                    };
        insert accountList;
     
        Account customerAccount  = new Account();
        customerAccount.Name = 'Test Account';
        customerAccount.Affiliated_to_Strategic_Account__c = accountList[0].Id;
        customerAccount.Affiliated_to_IDN_Account__c = accountList[4].Id;
        customerAccount.Country__c = 'USA';
        customerAccount.BillingCountry = 'USA';
        customerAccount.BillingCity = 'Milpitas';
        customerAccount.BillingPostalCode = '94587';
        customerAccount.Exclude_from_Team_Owner_rules__c = false;
        customerAccount.Territory_Team__c = tt.Id;
        customerAccount.State_Province_Is_Not_Applicable__c = true;
        insert customerAccount;
        
        tt.Zip__c = '94587';
        update tt;
        
        
        List<DefaultAccountTeam__c> defaultAccountList = new List<DefaultAccountTeam__c>{
                                                        getDefaultAccountTeam(accountList[0].Id, 'Strategic Account Manager', userList[1].Id),
                                                        getDefaultAccountTeam(accountList[1].Id, 'Strategic Account Manager', userList[1].Id),
                                                        getDefaultAccountTeam(accountList[2].Id, 'GPO Manager', userList[1].Id),
                                                        getDefaultAccountTeam(accountList[3].Id, 'GPO Manager', userList[1].Id),
                                                        getDefaultAccountTeam(accountList[4].Id, 'IDN Manager', userList[1].Id),
                                                        getDefaultAccountTeam(accountList[5].Id, 'IDN Manager', userList[1].Id)
                                                    };
        insert defaultAccountList;
        
        Account customerAccount1 = [SELECT Id, Affiliated_to_Strategic_Account__c,
                                            Affiliated_to_GPO_Account__c, Affiliated_to_IDN_Account__c
                                        FROM Account
                                        WHERE Id =: customerAccount.Id
                                    ];
        
        customerAccount1.Affiliated_to_Strategic_Account__c = accountList[1].Id;
        customerAccount1.Affiliated_to_GPO_Account__c = accountList[3].Id;
        customerAccount1.Affiliated_to_IDN_Account__c = accountList[5].Id;
        DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT = false;
        update customerAccount1;       
        Test.stopTest();
    }
    
    private static Account getAccount(String affiliation){
        Account strategicAccount = new Account();
        strategicAccount.Name = 'TEST Strategic 1';
        strategicAccount.Country__c = 'USA';
        strategicAccount.BillingCity = 'Milpitas';
        strategicAccount.OMNI_Postal_Code__c = '17042';
        strategicAccount.Affiliation__c = affiliation;  
        strategicAccount.Agreement_Type__c = 'Master Terms';
        strategicAccount.BillingPostalCode = '95035';
        strategicAccount.State_Province_Is_Not_Applicable__c = true;
        return strategicAccount;
    }
    
    private static DefaultAccountTeam__c getDefaultAccountTeam(Id accountId, String role, Id userId){
        DefaultAccountTeam__c accountTeam = new DefaultAccountTeam__c();
        accountTeam.Name = 'TEST '+role;
        accountTeam.Account__c = accountId;
        accountTeam.User__c = userId;
        accountTeam.Team_Member_Role__c = role;
        return accountTeam;
    } 
}
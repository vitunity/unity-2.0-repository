/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 04-Apr-2013
    @ Description   : This class will be called from trigger to add Opportunity Sales team associated with an account.
    @ Last Modified By  :   Megha Arora
    @ Last Modified On  :   13-May-2013
    @ Last Modified Reason  :   Updated the Opportunity Owner logic.
/****************************************************************************/
public class DefaultSalesTeam 
{
 /*   public DefaultSalesTeam()
    {
        
    }
    
    /* 
      Method to add Opportunity Sales Team
      @param newOpptyList - List of opportunity records
    */
 /*   public void addSalesTeam(List<Opportunity> newOpptyList)
    {
        try
        {
        
            Set<Id> userIds = new Set<Id>(); 
            Set<Id> accountIds = new Set<Id>();
            for(Opportunity opp: newOpptyList)
            {
                accountIds.add(opp.AccountId);
            }
            List<AccountTeamMember> accountTeamList = [Select AccountId, UserId, TeamMemberRole from AccountTeamMember where AccountId IN :accountIds];
            Map<Id, List<AccountTeamMember>> accountIdTeamMap = new Map<Id, List<AccountTeamMember>>(); 
            if(accountTeamList != null)
            {
                System.debug('SALES TEAM - ACCOUNT TEAM MEMBER LIST SIZE = '+accountTeamList.size());
                for(AccountTeamMember atm: accountTeamList)
                {
                    userIds.add(atm.UserId);
                    List<AccountTeamMember> accountTeam = new List<AccountTeamMember>();
                    if(accountIdTeamMap.containsKey(atm.AccountId))
                    {
                        accountTeam = accountIdTeamMap.get(atm.AccountId);
                    }
                    accountTeam.add(atm);
                    accountIdTeamMap.put(atm.AccountId, accountTeam);
                }
            }
            Map<Id, User> userMap = new Map<Id, User>([Select Id, Name, isActive from User where isActive = false and Id IN :userIds]);
            List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
            List<OpportunityShare> opptyShareList = new List<OpportunityShare>();
            for(Opportunity opp: newOpptyList)
            {
                List<AccountTeamMember> accountTeam = accountIdTeamMap.get(opp.AccountId);
                if(accountTeam != null)
                { 
                    for(AccountTeamMember atm: accountTeam)
                    {
                        if(!userMap.containsKey(atm.UserId) && opp.OwnerId != atm.UserId)
                        {
                            OpportunityTeamMember otm = new OpportunityTeamMember();
                            otm.OpportunityId = opp.Id;
                            otm.TeamMemberRole = atm.TeamMemberRole;
                            otm.UserId = atm.UserId;
                            opptyTeamList.add(otm);
                            
                            OpportunityShare oppShare = new OpportunityShare();
                            oppShare.OpportunityId = opp.Id;
                            oppShare.UserOrGroupId = atm.UserId;
                            oppShare.OpportunityAccessLevel = 'Edit';
                            opptyShareList.add(oppShare);
                        }
                    }
                   
                }   
            }           
         
            
            if(opptyTeamList.size() > 0)
                insert opptyTeamList;
            if(opptyShareList.size() > 0)
                insert opptyShareList;
        }
        catch(Exception e)
        {
        }
    }
    
    
    /* 
      Method to change opportunity owner
      @param newOpptyList - List of opportunity records
    */
 /*   public void changeOpportunityOwner(List<Opportunity> newOpptyList)
    
    {
        try
        {
            Map<Id, Id> opportunityIdDMIdMap = new Map<Id, Id>();
            Set<Id> accountIds = new Set<Id>();
            Set<Id> userIds = new Set<Id>();
            
            
             for(Opportunity opp: newOpptyList)
            {
                accountIds.add(opp.AccountId);
            }
             
            Map<Id, Id> zipDMIdMap = new Map<Id, Id>();
     /*       Map<String,Id> accountTeamMap = new Map<String,Id>();
            for(AccountTeamMember team1: [Select AccountId, TeamMemberRole, UserId from AccountTeamMember where TeamMemberRole ='District Manager' and Accountid IN: accountIds])
                       
                {
                    zipDMIdMap.put(team1.AccountId,team1.UserId);
                    userIds.add(team1.UserId);
                }
            
            Map<ID,String> usermap = new map<ID,String>();
                for(user userObj : [select id, name from user where isactive = True and usertype != 'CsnOnly' and Id IN :userIds]) 
                {
                    usermap.put(userObj.id,userObj.name);
                } */
                
         /*    for(Account team1: [Select Id, OwnerId from Account where Id IN: accountIds])
                       
                {
                    zipDMIdMap.put(team1.Id,team1.OwnerId);
                }
                 
              Set<Id> oppIds = (new Map<Id,Opportunity>(newOpptyList)).keySet();

             List <Opportunity> Oppupdate = [Select id,OwnerId,Accountid from Opportunity where id IN:oppIds];
             List <Opportunity> Opptoupdate = new List <Opportunity>();
             
                System.debug('********'+Oppupdate);
                if(Oppupdate != null)
                {
                        for(Opportunity opp : Oppupdate)
                    {  
                        Id dmownerId = zipDMIdMap.get(opp.AccountId);
                          system.debug('&&&'+dmownerId);
                          
                        if(dmownerId != null && opp.OwnerId != dmownerId)
                        {
                           
                             opp.ownerId=dmownerId;
                             system.debug('--opp--'+opp.ownerid);
                            Opptoupdate.add(opp);
                        }    
                    }
                   
                   } 
            
            update Opptoupdate;
        }
                
        
        catch(Exception e)
        {
        }
        
    } */
    }
/**
 * This class contains unit tests for validating the behavior of Apex classe 'DefaultSalesTeam'
 * This test class also covers Trigger 'OpportunityMasterTrigger_BeforeTrigger' and 'OpportunityMasterTrigger_AfterTrigger'
 */


@isTest  
Private class DefaultSalesTeamTest
{
 /*    Static testMethod  void myUnitTest() 
     {
         //Map<Id, User> userMap = new Map<Id, User>([Select Id, Name, isActive from User where isActive = false and Id IN :ur.Id]);
        Recordtype rt=[select id from Recordtype where Name='Bill To'];
        System.debug('record type info::'+rt);
        Account a = AccountTestData.createAccount('MY USA Country Account', 'USA', '19702'); // for Territory Team 'tt' record
        a.RecordtypeId=rt.Id;
        insert a;
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        list<user>userlist = new list<user>();
        User ur1 = new User(alias = 'xhgts123',email='ranjan.jha12@wipro.com',emailencodingkey='UTF-8', lastname='varianTesting1', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, country='USA',timezonesidkey='America/Los_Angeles', username='myvarian123fgh@user123.com');
        User ur2 = new User(alias = 'kju67ssy',email='ranjan.jha13@wipro.com',emailencodingkey='UTF-8', lastname='varianTesting2', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, country='USA',timezonesidkey='America/Los_Angeles', username='567myvarianfgh1234@userxyz.com');
        User ur3 = new User(alias = 'iuths56f' ,email='ranjan.jha14@wipro.com',emailencodingkey='UTF-8', lastname='varianTesting3', languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, country='USA',timezonesidkey='America/Los_Angeles', username='test4567varian9876jkl@user123jkl.com');
        userlist.add(ur1);
        userlist.add(ur2);
        userlist.add(ur3);
        insert userlist;
        
        AccountTeamMember atm =new AccountTeamMember ();
        atm.Accountid =a.id;
        atm.UserId =userlist[0].id;
        insert atm;
        
         
        Contact c=new Contact();
        c.LastName ='Jha';
        c.Accountid= a.id;
        //Bhanu.08/23/2013 start.new.
        c.MailingCountry='USA';
        c.MailingState='CA';
        c.Email='test@gmail.com';
        //Bhanu.08/23/2013 end.new.
        insert c;
        List<OpportunityTeamMember> opptyTeamList = new List<OpportunityTeamMember>();
        List<OpportunityShare> opptyShareList = new List<OpportunityShare>();
        Opportunity o= OpportunityTestData.createOpportunity(a.Id);
        insert  o;

        OpportunityTeamMember otm = new OpportunityTeamMember();
        otm.OpportunityId = o.Id;
        otm.TeamMemberRole = atm.TeamMemberRole;
        otm.UserId = atm.UserId;
        opptyTeamList.add(otm);
        
        OpportunityShare oppShare = new OpportunityShare();
        oppShare.OpportunityId = o.Id;
        oppShare.UserOrGroupId = atm.UserId;
        oppShare.OpportunityAccessLevel = 'Edit';
        opptyShareList.add(oppShare);
        otm.UserId =atm.UserId;
        insert opptyTeamList;

        Account b = AccountTestData.createAccount('MY INDIA Country Account', 'INDIA', '110011'); // for Territory Team 'tt' record

        insert b;
        
        o.Accountid=b.id;
        update o;
    } */
}
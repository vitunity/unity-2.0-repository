global class DeleteInstalledProduct{
     
     webservice static string deleteProduct(string InsProId){
       try{
           delete [select Id from SVMXC__Installed_Product__c where Id =: InsProId];
           return 'success';}catch(Exception e){return e.getMessage();}
    }
}
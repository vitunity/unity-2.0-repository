/***************************************************************************\
    @ Author                : Rakesh
    @ Date                  : 15/12/2016
****************************************************************************/
@isTest 
public class DeleteInstalledProduct_test {
    public static testMethod void UnitTest() {
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;
        
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id
        );
        insert objIP;   
        
        string b = DeleteInstalledProduct.deleteProduct(objIP.id);
        system.assertEquals(b,'success');
        
        DeleteInstalledProduct.deleteProduct('test');
        
        
    }
}
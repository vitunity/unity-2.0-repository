@isTest
public class DeveloperStripeInfoTrigger_test {

    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        User u = vMarketDataUtility_Test.getSystemAdmin();
        System.runAs(u)
        {
        account = vMarketDataUtility_Test.createAccount('test account', true);
        }
        
        System.debug('&&&&&'+portal.UserType);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        
        User u1 = vMarketDataUtility_Test.getSystemAdmin();
        //System.runAs(u1)
        //{
            
            insert lstContactInsert;
            
        //}
        
          
        
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        //User u1 = vMarketDataUtility_Test.getSystemAdmin();
        System.runAs(u1)
        {
        insert lstUserInsert;
        }
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        listing = vMarketDataUtility_Test.createListingData(app, true);
        comment = vMarketDataUtility_Test.createCommentTest(app, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        //system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
        //}
    }

    public static testMethod void DeveloperStripeInfoTrigger() {
        Test.StartTest();
            User u2 = vMarketDataUtility_Test.getSystemAdmin();
            vMarket_Developer_Stripe_Info__c dev = new vMarket_Developer_Stripe_Info__c();
            dev.Access_Token__c = '1122333';
            dev.Contact__c = contact1.Id;
            dev.Developer_Request_Status__c = 'Approved';
            dev.isActive__c = true;
            dev.Rejection_Reason__c = '';
            dev.Scope__c = 'Read_Wrire';
            dev.Stripe_Id__c = '1212ssss';
            dev.Stripe_User__c = customer1.Id;
            dev.Token_Type__c = 'some';
            dev.vMarket_Accept_Terms_of_Use__c = true;
            system.runas(u2){
                insert dev;
             }
        Test.Stoptest();
    }
}
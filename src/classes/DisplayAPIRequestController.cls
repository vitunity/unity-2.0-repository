/*
Project No	  :	5142
Name          : DisplayAPIRequestController Controller
Created By    : Jay Prakash (@varian)
Date          : 20th March, 2017
Purpose       : An Apex Controller to save the API Request created by the myVarian Community Log In User
Updated By    : Yogesh Nandgadkar
Last Modified Reason  :   Updated the Json String to display the required List of API Requests
*/

public class DisplayAPIRequestController {    
 		public static string authorizationHeader = APIKeySAPIntegrationController.sapAuthorizationHeader();         
        public String APIREQUEST_ID {get;set;}
        public String CUSTOMNAME {get;set;}
        public String CustomerContactName {get; set;}
        public String REQ_STATUS {get;set;}    
        public String SOFTNAME {get; set;}
        public String APITYPES {get; set;}
        public String SOFT3RDPART {get; set;} 
        public String CUSTOMPHONE {get; set;}
        public String REQ_NAME {get; set;}
        public String CUSTEMAIL {get; set;}
        public String REQ_EMAIL {get; set;}
        public String CHANGED_BY {get; set;}
        public String CUSTOMERID {get; set;}
        public String emailIdAPICall {get; set;}
    	public String APIREQ_REASON {get; set;}            
    
        //Added for API Key - Download Functionality
        public String apiKeyString {get; set;}
        public String appRequestID {get; set;}
        public String apiRequestID {get; set;}
 
       public List<APIRequests> dr{get;set;}
    
    	public Boolean isGuest{get;set;}
    	public string Usrname{get;set;}
    	public string shows{get;set;}
       
       public DisplayAPIRequestController(){
          parseJSONString();
	      shows='none';
        isGuest = SlideController.logInUser();
        
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }     
    }
    
    public Id getUserAuthId() {
        return UserInfo.getUserId();
    }

    public String getUserContactEmail() {
        String LoggedInUserId = getUserAuthId(); 
               
        // Retrieving ContactId from USER        
        List<User> userData = [ SELECT Id,contactId from USER where id=: LoggedInUserId];
        if (userData.size() > 0) {
            Id conId = userData[0].contactId;            
            // Retrieving Contact Data from Contact based on ContactId
            List<Contact> contactData = [ SELECT Id, AccountId, Name, Phone, Email from CONTACT where id=: conId];
                       
            if(!Test.isRunningTest()){
             emailIdAPICall = contactData[0].Email;
            }else{
              emailIdAPICall  = 'test@varian.com';
            }            
            return '';            
        }
        return null;
    } 
  
    public void parseJSONString() {
       getUserContactEmail();        
        HttpRequest httpsReq = new HttpRequest();
        httpsReq.getBody();
        
        // set the request method
        httpsReq.setMethod('POST');

        //Custom Settings -  Proxy PHP URL
        //String url = 'https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        PROXY_PHP_URL_DEV2__c proxyPHPUrl =  PROXY_PHP_URL_DEV2__c.getValues('Proxy PHP URL');  
        String url = proxyPHPUrl.ProxyURL__c;        
       
        // Adding the endpoint to the request
        httpsReq.setEndpoint(url);      
        
        httpsReq.setHeader('Authorization', authorizationHeader);
                         
        // TODO :- Create a label for X-SAPWebService-URL and use that X-SAPWebService-URL below
        INVOKE_SAP_FM_SETTINGS__c displayAPIDetails =  INVOKE_SAP_FM_SETTINGS__c.getValues('Display API Details'); 
        String sapFM = displayAPIDetails.FM_Call__c; //String sapFM = 'ZSAAS_DISPLAY_APIDETAILS';         
        
        //Custom Setting -  SAP Endpoint URL
        SAP_ENDPOINT_URL_DEV2__c sapURL =  SAP_ENDPOINT_URL_DEV2__c.getValues('SAP Endpoint URL');         
        String sapApiUrl=sapURL.SAP_URL__c + sapFM + '?format=json';         
        httpsReq.setHeader('X-SAPWebService-URL', sapApiUrl); 
        
        httpsReq.setHeader('Content-Type','application/json');      
        httpsReq.setHeader('Accept','application/json');        
        
        //String jsonBody = '{"IM_USER":  "'+CUSTEMAIL+'" }';        
        String jsonBody = '{"IM_USER": "'+ emailIdAPICall +'"}';
       
        //After Header - Add these code        
         if(jsonBody != null )
         { 
             httpsReq.setBody(jsonBody);
             httpsReq.setHeader('Content-length',string.valueOf(jsonBody.length()));
         }                  
      
        HttpResponse res = new http().send(httpsReq);        
        String apiKeyRequestsResp = res.getBody();
        ResCls resItem = (ResCls)JSON.deserialize(apiKeyRequestsResp, ResCls.class);
                
        // rl Not getting populated
        List<APIRequests> apiReqList = resItem.EX_APIDETAILS;
        dr = new List<APIRequests>();
        if(apiReqList != NULL){
         for(APIRequests apireq: apiReqList){           
            APIREQUEST_ID = apireq.APIREQUEST_ID;
            CUSTOMNAME = apireq.CUSTOMNAME;
            CustomerContactName = apireq.CUCONNAME;            
            REQ_STATUS = apireq.REQ_STATUS;
            SOFTNAME = apireq.SOFTNAME;
            APITYPES = apireq.APITYPES;
            SOFT3RDPART = apireq.SOFT3RDPART;
            CUSTOMPHONE = apireq.CUSTOMPHONE;
            REQ_NAME = apireq.REQ_NAME;
            CUSTEMAIL = apireq.CUSTEMAIL;
            REQ_EMAIL = apireq.REQ_EMAIL;
            CHANGED_BY = apireq.CHANGED_BY;
            CUSTOMERID = apireq.CUSTOMERID;
            APIREQ_REASON = apireq.APIREQ_REASON; 
            dr.add(apireq);
         }
         }  
       
    }
        public class ResCls{
        List<APIRequests> EX_APIDETAILS;
    }
        public class APIRequests {
            public String APIREQUEST_ID {get; set;}
            public String CUSTOMNAME {get; set;}
            public String CUSTOMERID {get; set;}
            public String CUCONNAME {get; set;}        
            public String CUSTEMAIL {get; set;}
            public String REQ_STATUS {get; set;}
            public String SOFTNAME {get; set;}
            public String APITYPES {get; set;}
            public String SOFT3RDPART {get; set;}   
            public String CUSTOMPHONE {get; set;}
            public String REQ_NAME {get; set;}    
            public String REQ_EMAIL {get; set;}
            public String CHANGED_BY {get; set;}
			public String APIREQ_REASON {get; set;}            
            }
        
        @RemoteAction
        public static String getDownloadAPIKey(String apiRequestID) {
            String apiReqID = apiRequestID;
            
            HttpRequest httpsReq = new HttpRequest();                
            httpsReq.getBody();
                
            // set the request method
            httpsReq.setMethod('POST');
        
            //Custom Settings -  Proxy PHP URL
            //String url = 'https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
            PROXY_PHP_URL_DEV2__c proxyPHPUrl =  PROXY_PHP_URL_DEV2__c.getValues('Proxy PHP URL');  
            String url = proxyPHPUrl.ProxyURL__c;
                
            // Adding the endpoint to the request
            httpsReq.setEndpoint(url);
                
            // Setting of Authorization Token
             //String authorizationHeader = 'BASIC RkNKMTY0OTp2YXJFSEQwOA=='; 
            // String authorizationHeader = 'Basic UkZDX0FQSV9NR01UOkFwaUA1MTQy';
            //EncodingUtil.base64Encode(headerValue);
            httpsReq.setHeader('Authorization', authorizationHeader);                          
            
            // TODO :- Create a label for X-SAPWebService-URL and use that X-SAPWebService-URL below
        	INVOKE_SAP_FM_SETTINGS__c downloadAPIKey =  INVOKE_SAP_FM_SETTINGS__c.getValues('Download API Key'); 
        	String sapFM = downloadAPIKey.FM_Call__c; //String sapFM = 'ZSAAS_API_DOWNLOAD_KEY';            
            
            // TODO :- Create a label for X-SAPWebService-URL and use that X-SAPWebService-URL below
            //String sapApiUrl =  'https://ehd.cis.varian.com:8251/zcallfmviarest/ZSAAS_API_DOWNLOAD_KEY?format=json';
            
            //Custom Settting -  SAP Endpoint URL
            //String sapApiUrl =  'https://ehd.cis.varian.com:8251/zcallfmviarest/'+sapFM+'?format=json';
            SAP_ENDPOINT_URL_DEV2__c sapURL =  SAP_ENDPOINT_URL_DEV2__c.getValues('SAP Endpoint URL');         
            String sapApiUrl=sapURL.SAP_URL__c + sapFM +'?format=json';             
            
            httpsReq.setHeader('X-SAPWebService-URL', sapApiUrl);                
            httpsReq.setHeader('Content-Type','application/json'); 
            
            httpsReq.setHeader('Accept','application/json');             
                
            String jsonBody = '{"IM_APIREQID": "'+ apiRequestID +'"}'; 
            
            //After Header - Add these code        
             if(jsonBody != null )
             { 
                 httpsReq.setBody(jsonBody);
                 httpsReq.setHeader('Content-length',string.valueOf(jsonBody.length()));
             }            
            HttpResponse res = new http().send(httpsReq);  
             //System.debug('********res.getBody() of Download API Key******'+res.getBody());
            return res.getBody();
            
            //return 'Test API Key';
        }
         
        @RemoteAction
        public static String approveRejectAPIRequest(String apiRequestID, String decisionKeyVal) {
            String apiReqID = apiRequestID;
            
            HttpRequest httpsReq = new HttpRequest();                
            httpsReq.getBody();
                
            // set the request method
            httpsReq.setMethod('POST');
        
            //Custom Settings -  Proxy PHP URL
            //String url = 'https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
            PROXY_PHP_URL_DEV2__c proxyPHPUrl =  PROXY_PHP_URL_DEV2__c.getValues('Proxy PHP URL');  
            String url = proxyPHPUrl.ProxyURL__c;    
            
            // Adding the endpoint to the request
            httpsReq.setEndpoint(url);
                
            // Setting of Authorization Token
             //String authorizationHeader = 'BASIC RkNKMTY0OTp2YXJFSEQwOA=='; 
             //String authorizationHeader = 'Basic UkZDX0FQSV9NR01UOkFwaUA1MTQy';
            //EncodingUtil.base64Encode(headerValue);
            httpsReq.setHeader('Authorization', authorizationHeader); 
            
            // Custom Settings for SAPFM Call for "ZSAAS_APPROVE_API"
            INVOKE_SAP_FM_SETTINGS__c approveAPI =  INVOKE_SAP_FM_SETTINGS__c.getValues('Approve API'); 
            String sapFM = approveAPI.FM_Call__c; //String sapFM = 'ZSAAS_APPROVE_API';            
            
            // TODO :- Create a label for X-SAPWebService-URL and use that X-SAPWebService-URL below
            //String sapApiUrl =  'https://ehd.cis.varian.com:8251/zcallfmviarest/ZSAAS_APPROVE_API?format=json';
            
            //Custom Settting -  SAP Endpoint URL
       		//String sapApiUrl =  'https://ehd.cis.varian.com:8251/zcallfmviarest/'+sapFM+'?format=json';
            SAP_ENDPOINT_URL_DEV2__c sapURL =  SAP_ENDPOINT_URL_DEV2__c.getValues('SAP Endpoint URL');         
            String sapApiUrl=sapURL.SAP_URL__c+sapFM+'?format=json';             
            
            httpsReq.setHeader('X-SAPWebService-URL', sapApiUrl);                
            httpsReq.setHeader('Content-Type','application/json'); 
            httpsReq.setHeader('Accept','application/json');             
                
            String jsonBody = '{"IM_APIREQUEST": "'+ apiRequestID +'", "IM_DECISIONKEY": "'+ decisionKeyVal +'"}';   
            //After Header - Add these code        
             if(jsonBody != null )
             { 
                 httpsReq.setBody(jsonBody);
                 httpsReq.setHeader('Content-length',string.valueOf(jsonBody.length()));
             }              
            HttpResponse res = new http().send(httpsReq);
            System.debug('********res.getBody() of Approve Rejet******'+res.getBody());
            return res.getBody();
        }   
}
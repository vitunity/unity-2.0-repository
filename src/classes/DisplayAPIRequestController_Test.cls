/*
Project No    : 5142
Name          : DisplayAPIRequestController_Test
Created By  : Jay Prakash 
Date        : 07th March, 2017
Purpose     : Test class - To test the functionality of displaying the List of API Requests for Varian Community Users ( Log In User)
Updated By  :   
*/
@isTest(SeeAllData = false)
private class DisplayAPIRequestController_Test{

    public static INVOKE_SAP_FM_SETTINGS__c csObj ;
    public static PROXY_PHP_URL_DEV2__c proxyPHPURL ;
    public static SAP_ENDPOINT_URL_DEV2__c sapURL;
    public static SAP_Login_Authorization__c sapLogInAuthUserName ; 
    public static SAP_Login_Authorization__c sapLogInAuthPwd ;       
       
    // Start of Test Method  - getUserContactEmail
  	static testMethod void validateGetUserContactEmail()    
    {    
        Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
          User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
          
          //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;
        
          System.RunAs(userInfo ) {
            test.startTest();

              Account accObj = new Account();
              accObj.name = 'Test';
              accObj.Country__c= 'India';
              accObj.Ext_Cust_Id__c = 'test' ;
              insert accobj ; 
              
              Contact  conObj = new Contact (AccountId = accObj.id,FirstName='FName',LastName = 'portalTestUser',Phone='1234567890', Email='test123@varian.com', MobilePhone='123114567890');
              insert  conObj ;
              
             APIRequestController  obj = new APIRequestController ();
             obj.getUserContactEmail();
             
             test.stopTest();               
          }     
    }    
    // End of Test Method - getUserContactEmail
    
    static testMethod void validateparseJSONString()    
    {
         
         Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];     
         
         User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];         
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'Display API Details';
         csObj.FM_Call__c = 'ZSAAS_DISPLAY_APIDETAILS';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';        
        
         insert csObj;
         insert proxyPHPURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;  
         insert sapURL;
        
         System.RunAs(userInfo) {
             test.starttest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseparseJSONString ());
            DisplayAPIRequestController obj = new DisplayAPIRequestController ();
             obj.parseJSONString();
         } 
         test.stoptest();         
    }   
    
     static testMethod void validateGetDownloadAPIKey()    
    {
        
         Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
         User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
                
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'Download API Key';
         csObj.FM_Call__c = 'ZSAAS_API_DOWNLOAD_KEY';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';        
        
         insert csObj;
         insert proxyPHPURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;  
         insert sapURL;  
         test.starttest();
        System.RunAs(userInfo) {       
         Test.setMock(HttpCalloutMock.class, new MockHttpResponseGetDownloadAPIKey ());
         //DisplayAPIRequestController obj = new DisplayAPIRequestController ();
         DisplayAPIRequestController.getDownloadAPIKey('');
         } 
                 
         test.stoptest();                 
    } 
    
     static testMethod void validateapproveRejectAPIRequest()    
    {
        
         Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
         User userInfo = [Select ContactId, Id,lastname,ProfileId from User where ProfileId =: P.Id and isActive =true limit 1];
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'Approve API';
         csObj.FM_Call__c = 'ZSAAS_APPROVE_API';
        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
        // For SAP Endpoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';        
        
         insert csObj;
         insert proxyPHPURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;  
         insert sapURL;  
        
         test.starttest();
         System.RunAs(userInfo) {
         Test.setMock(HttpCalloutMock.class, new MockHttpResponseApproveRejectAPIRequest ());
         //DisplayAPIRequestController obj = new DisplayAPIRequestController ();
         DisplayAPIRequestController.approveRejectAPIRequest('','');
         } 
         test.stoptest();                 
    }  

}
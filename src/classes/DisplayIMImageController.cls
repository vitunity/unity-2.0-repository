public class DisplayIMImageController {

    //Parent Contact  
    private String Product2Id ;  
    //Image selected from UI  
    public String selectedImage {get; set;}  
      
    public DisplayIMImageController()  
    {  
        //Fetching contact Id  
        Product2Id = ApexPages.CurrentPage().getParameters().get('Id') ;  
        selectedImage = '' ;  
    }  
      
    public boolean validateImage(String image)  
    {  
        String Regex = '([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg))$)';  
        Pattern MyPattern = Pattern.compile(Regex);  
        Matcher MyMatcher = MyPattern.matcher(image);  
        return MyMatcher.matches() ;  
    }  
      
    public List<SelectOption> getItems()  
    {  
        List<SelectOption> options = new List<SelectOption>();   
          
        //All attachments related to contact  
        List<Attachment> attachLst = [SELECT Id , Name FROM Attachment WHERE ParentId =: Product2Id] ;  
          
        //Creating option list  
        for(Attachment att : attachLst)  
        {  
            String imageName = att.name.replaceAll('\\s+','|') ; 
            if(validateImage(imageName))  
            {  
                options.add(new SelectOption(att.Id , att.Name));  
            }  
        }  
          
        return options ;  
    }  
      
    public PageReference SaveImage()  
    {  
        //Contact to update  
        List<Interactive_Maps_Landmarks__c> conToUpdate = new List<Interactive_Maps_Landmarks__c>() ;  
        conToUpdate = [select id,Photo__c from Interactive_Maps_Landmarks__c where id =: Product2Id] ;  
  
        //Inserting image parth  
        if(conToUpdate.size() > 0)  
        {  
            conToUpdate[0].Photo__c= '/servlet/servlet.FileDownload?file=' + selectedImage ;  
            update conToUpdate[0] ;  
        }  
        string recID = ApexPages.CurrentPage().getParameters().get('id');
        Pagereference pr = new PageReference('/'+recID);
        return pr ;   
    }  
}
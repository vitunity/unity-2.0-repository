@isTest
public class DisplayIMImageControllerTest {
	static testMethod void unitTest1()
    {
        Interactive_Maps_Landmarks__c Vumap=new Interactive_Maps_Landmarks__c(Name='TestMapName',Description__c='TestMap2');
		insert  Vumap;
        Attachment att = new Attachment(Name='test', ParentId=Vumap.ID, body=EncodingUtil.base64Decode('Test'));
		insert att;	
     	PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('Id',Vumap.Id); 
        DisplayIMImageController displayImage = new DisplayIMImageController();
        displayImage.validateImage('test2.jpg');
        displayImage.getItems();
        PageReference pa =  displayImage.SaveImage();
    }
}
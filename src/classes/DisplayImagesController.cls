public class DisplayImagesController  
{  
    //Parent Contact  
    private String Product2Id ;  
    //Image selected from UI  
    public String selectedImage {get; set;}  
    
      
    public DisplayImagesController()  
    {  
        //Fetching contact Id  
        Product2Id = ApexPages.CurrentPage().getParameters().get('Id') ;  
        selectedImage = '' ;  
    }  
      
    public boolean validateImage(String image)  
    {  
        //String Regex = '([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)';  
        String Regex = '([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)';
        Pattern MyPattern = Pattern.compile(Regex);  
        Matcher MyMatcher = MyPattern.matcher(image);  
        return MyMatcher.matches() ;  
    }  
      
    public List<SelectOption> getItems()  
    {  
        List<SelectOption> options = new List<SelectOption>();   
          
        //All attachments related to contact  
        List<Attachment> attachLst = [SELECT Id , Name FROM Attachment WHERE ParentId =: Product2Id] ;  
          
        //Creating option list  
        for(Attachment att : attachLst)  
        {  
            //String imageName = att.name.toLowerCase() ; 
            String imageName = att.name.replaceAll('\\s+','|') ; 
            if(validateImage(imageName))  
            {  
                options.add(new SelectOption(att.Id , att.Name));  
            }  
        }  
          
        return options ;  
    }  
      
    public PageReference SaveImage()  
    {  
        //Contact to update  
        List<VU_Products__c> conToUpdate = new List<VU_Products__c>() ;  
        conToUpdate = [select id,StoreImageUrl__c from VU_Products__c where id =: Product2Id] ;  
  
        //Inserting image parth  
        if(conToUpdate.size() > 0)  
        {  
            conToUpdate[0].StoreImageUrl__c = '/servlet/servlet.FileDownload?file=' + selectedImage ;  
            update conToUpdate[0] ;  
        }  
        string recID = ApexPages.CurrentPage().getParameters().get('id');
        Pagereference pr = new PageReference('/'+recID);
        return pr ;  
    }  
}
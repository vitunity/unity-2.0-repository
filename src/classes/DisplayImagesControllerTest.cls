@isTest
public class DisplayImagesControllerTest {
    static testMethod void unitTest1()
    {
        VU_Products__c Vuproduct=new VU_Products__c(Name='TestName',Product_Description__c='TestName2');
        insert  Vuproduct;
        Attachment att = new Attachment(Name='test', ParentId=Vuproduct.ID, body=EncodingUtil.base64Decode('Test'));
        insert att; 
        PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('Id',Vuproduct.Id); 
        DisplayImagesController displayImage = new DisplayImagesController();
        displayImage.validateImage('test.jpg');
        displayImage.getItems();
        PageReference pa =  displayImage.SaveImage();
    }
}
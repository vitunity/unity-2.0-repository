@isTest
public class DisplayThumbnailImagesControllerTest {
	static testMethod void unitTest1()
    {
        VU_Products__c Vuproduct=new VU_Products__c(Name='TestName1',Product_Description__c='TestName3');
		insert  Vuproduct;
        Attachment att = new Attachment(Name='test1', ParentId=Vuproduct.ID, body=EncodingUtil.base64Decode('Test1'));
		insert att;	
     	PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('Id',Vuproduct.Id); 
       DisplayThumbnailImagesController DisplayThumbnailImage = new DisplayThumbnailImagesController();
        DisplayThumbnailImage.validateImage('test1.jpg');
        DisplayThumbnailImage.getItems();
        PageReference pa =  DisplayThumbnailImage.SaveImage();
    }
}
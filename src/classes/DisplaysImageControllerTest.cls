@isTest
public class DisplaysImageControllerTest {
	static testMethod void unitTest1()
    {
        VU_Speakers__c Vuspeaker=new VU_Speakers__c(Name='TestSpeakerName',Description__c='TestSpeakerDescription');
		insert  Vuspeaker;
        Attachment att = new Attachment(Name='test', ParentId=Vuspeaker.ID, body=EncodingUtil.base64Decode('Test'));
		insert att;	
     	PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('Id',Vuspeaker.Id); 
        DisplaysImageController displayImage = new DisplaysImageController();
        displayImage.validateImage('test.jpg');
        displayImage.getItems();
        PageReference pa =  displayImage.SaveImage();
    }
}
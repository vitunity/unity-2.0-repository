@isTest
public class DisplaysTreatmentImageControllerTest {
	static testMethod void unitTest1()
    {
        VU_Treatment_Technique__c Vutreatment=new VU_Treatment_Technique__c(Name='TestTreatmentName',Description__c='TestTreatmentName2');
		insert  Vutreatment;
        Attachment att = new Attachment(Name='test', ParentId=Vutreatment.ID, body=EncodingUtil.base64Decode('Test'));
		insert att;	
     	PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('Id',Vutreatment.Id); 
        DisplaysTreatmentImageController displayImage = new DisplaysTreatmentImageController();
        displayImage.validateImage('test2.jpg');
        displayImage.getItems();
        PageReference pa =  displayImage.SaveImage();
    }
}
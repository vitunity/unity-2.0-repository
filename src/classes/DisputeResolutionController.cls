public class DisputeResolutionController {
    ApexPages.StandardController sc ;
    public Boolean isAccess {get;set;}
    public DisputeResolutionController(ApexPages.StandardController stdController){
        if(!Test.isRunningTest())
            stdController.addFields(new List<String> {'Quote_Type__c', 'Amendment__c'} );
        
        this.sc = stdController;
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote = [Select Id, Quote_Type__c, Amendment__c, Amendment_Category__c, Operational_Responsibility__c, 
                Amendment_Sub_Category__c, Change_Expenditure__c, Responsible_Contact__c, Dispute_Notes__c 
                FROM  BigMachines__Quote__c  WHERE Id =: stdController.getId() ];
        //quote = (BigMachines__Quote__c)sc.getRecord();
        
        if(quote.Quote_Type__c == 'Amendment' || quote.Amendment__c) {
            isAccess = true;
        } else {
            isAccess = false;
        }
        
    }
    
    public PageReference save(){
        sc.save();
        return null;
    } 
/**    
    @AuraEnabled
    public static BigMachines__Quote__c resolutionController(String recId) {
        BigMachines__Quote__c bigMachineQuote = new BigMachines__Quote__c();
        if(recId != null && recId != '') {
            bigMachineQuote = [SELECT Id, Name, Operational_Responsibility__c, Change_Expenditure__c, Responsible_Contact__c,
                               Dispute_Notes__c 
                               FROM BigMachines__Quote__c WHERE Id =: recId];
        }
        //system.debug();
        return bigMachineQuote;
    }
    
    @AuraEnabled
    public static void saveQuote(BigMachines__Quote__c quote) {
        system.debug(' ==== QUOTE === ' + quote);
        update quote;
    }
**/    
}
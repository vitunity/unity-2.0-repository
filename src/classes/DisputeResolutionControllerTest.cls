@isTest
public class DisputeResolutionControllerTest {
    
    @isTest(seeAllData = true)
    public static void testMethods() {
    
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote = [Select Id, Name, Amendment_Category__c,  Amendment_Sub_Category__c, Change_Expenditure__c, 
                    Responsible_Contact__c, Dispute_Notes__c FROM BigMachines__Quote__c LIMIT 1 ];
                    
        ApexPages.StandardController sc = new ApexPages.StandardController(quote);
        DisputeResolutionController dispute = new DisputeResolutionController (sc);
        dispute.save();
    }
}
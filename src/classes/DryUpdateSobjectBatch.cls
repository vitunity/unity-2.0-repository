global class DryUpdateSobjectBatch implements Database.Batchable<sObject>{
    private String query;

    global DryUpdateSobjectBatch (String query) {
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        update scope;
    }

    global void finish(Database.BatchableContext BC){
    }
}
@isTest
public class DryUpdateSobjectBatchTest {
	
    @isTest
    public static void testDryUpdateSobjectBatch(){
        System.Test.startTest();
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
		DryUpdateSobjectBatch batchObj = new DryUpdateSobjectBatch('select id from Account');
        Database.executeBatch(batchObj);
        System.Test.stopTest();
    }
}
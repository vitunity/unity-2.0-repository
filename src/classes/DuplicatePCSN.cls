public class DuplicatePCSN{
    public static void Validate(list<SVMXC__Installed_Product__c> lstProduct, map<Id,SVMXC__Installed_Product__c> oldmap){
        List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
        set<string> setERPEquipment = new set<string>();
        for(SVMXC__Installed_Product__c i : lstProduct){
            if(
            (i.SVMXC__Serial_Lot_Number__c <> oldmap.get(i.id).SVMXC__Serial_Lot_Number__c || i.ERP_EQUIPMENT__c <> oldmap.get(i.id).ERP_EQUIPMENT__c ) && 
            (i.ERP_Reference__c == null || i.ERP_Reference__c == '') && 
            (i.Interface_Status__c == null || i.Interface_Status__c == '' || i.Interface_Status__c == 'Process') &&
            (i.ERP_EQUIPMENT__c <> null && i.ERP_EQUIPMENT__c <> '')
            )
            {
            system.debug('Test 1');
                lstInstalledProduct.add(i);
                setERPEquipment.add(i.ERP_EQUIPMENT__c);
            }           
        }
        
        map<string,List<SVMXC__Installed_Product__c>> mapDuplicatePCSN = new map<string,List<SVMXC__Installed_Product__c>>();
        for(SVMXC__Installed_Product__c i : [select Id,Name,ERP_Reference__c from SVMXC__Installed_Product__c where ERP_Reference__c IN: setERPEquipment]){
            system.debug('Test 2');
            List<SVMXC__Installed_Product__c> lstTemp = new List<SVMXC__Installed_Product__c>();
            lstTemp.add(i);
            if(mapDuplicatePCSN.containsKey(i.ERP_Reference__c)){
                lstTemp.addAll(mapDuplicatePCSN.get(i.ERP_Reference__c));
            }
            mapDuplicatePCSN.put(i.ERP_Reference__c,lstTemp);
        }
        
        for(SVMXC__Installed_Product__c i : lstProduct){
           if(mapDuplicatePCSN.containsKey(i.ERP_EQUIPMENT__c)){
           system.debug('Test 3');
               string error = 'ERP EQUIPMENT {'+i.ERP_EQUIPMENT__c+'} already exists on installed product {';
               string e;
               for(SVMXC__Installed_Product__c p : mapDuplicatePCSN.get(i.ERP_EQUIPMENT__c)){
                   if(e==null)e = p.Name;
                   else e+= ', '+p.Name;
               }
               error+=e+'}';
               i.addError(error,false);
           }      
        }
    }
}
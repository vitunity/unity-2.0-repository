@isTest
private class DuplicatePCSN_Test
{   
    static testmethod void DuplicatePCSN()
    {
        
        // insert parent IP var loc
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H12345',SVMXC__Serial_Lot_Number__c ='H12345',SVMXC__Status__c ='Installed',ERP_Reference__c = 'H12345');
        insert objIP;
        SVMXC__Installed_Product__c objIP2 = new SVMXC__Installed_Product__c(Name='H12346',SVMXC__Status__c ='Installed');
        insert objIP2;
       
        
        Test.StartTest();
            SVMXC__Installed_Product__c objTest = [select Id,ERP_Reference__c ,Interface_Status__c,ERP_EQUIPMENT__c  from SVMXC__Installed_Product__c where Id =: objIP2.id];
            system.assertEquals(objTest.ERP_Reference__c,null);
            system.assertEquals(objTest.Interface_Status__c,null);
            objTest.ERP_EQUIPMENT__c  = 'H12345';
            try{
                update objTest;   
            }catch(Exception e){
                Boolean expectedException =  (e.getMessage().contains('already exists on installed product')) ? true : false;
                System.AssertEquals(expectedException , true);
            }    
        Test.stopTest(); 
      
        
    }  
}
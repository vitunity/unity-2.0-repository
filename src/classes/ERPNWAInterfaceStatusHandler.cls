public with sharing class ERPNWAInterfaceStatusHandler {
    
    public static boolean isTriggerExecuting = false;
    
    /*
     -It will query all nwa related to work detail
     -checking unity status
     -updating interface status
     */     
    public static void updateERPNEWInterfaceStatusToProcess(set<Id> wdIds) {
        if(wdIds.isEmpty()) return;
        set<String> unityStatues = new set<String>{'Complete','Cancelled','Incomplete'};
        list<ERP_NWA__c> nwaList = [select Unity_Status__c from ERP_NWA__c where Work_Detail__c in : wdIds];
        for(ERP_NWA__c nwa : nwalist) {
            if(unityStatues.contains(nwa.Unity_Status__c)){
                nwa.Interface_Status__c = 'Process';
            }else{
                nwa.Interface_Status__c = '';
            }
        }
        if(!nwaList.isEmpty()) {
            update nwaList;
        }   
    }
}
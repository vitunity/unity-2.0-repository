public class ERPNWALog{
    
    public static void CreateERPNWSTrace(map<id,ERP_NWA__c> mapNew, map<id,ERP_NWA__c> mapOld){
        List<NWA_Trace__c> lstTrace = new List<NWA_Trace__c>();
        //Creating ERP NWA Trace
            for(ERP_NWA__c t : mapNew.values()){
                if(t.WBS_Element__c <> mapOld.get(t.Id).WBS_Element__c ){
                    NWA_Trace__c trace = new NWA_Trace__c();
                    trace.ERP_NWA__c = t.Id;
                    trace.ERP_WBS__c = mapOld.get(t.Id).WBS_Element__c;
                    trace.New_ERP_WBS__c = t.WBS_Element__c;
                    lstTrace.add(trace);
                    t.WBS_Element__c = mapOld.get(t.Id).WBS_Element__c;
                }
            }
        
            if(lstTrace.size() > 0 ){
            insert lstTrace;
            }
    }
    
}
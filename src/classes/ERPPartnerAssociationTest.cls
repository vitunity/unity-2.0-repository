@isTest(SeeAllData=true)
private class ERPPartnerAssociationTest{
    testmethod static void TriggerTestAfter() {
        
        
        //insert Account
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test23', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234', Prefered_Language__c ='English',Credit_Block__c =false);
        insert accObj;
        
        //insert record for ERP Partner//
        //record 1
        ERP_Partner__c ec = new ERP_Partner__c();
        ec.Name = 'Test';
        ec.Partner_Number__c = '145678';
        insert ec;
        
        //Record 2
        ERP_Partner__c ec1 = new ERP_Partner__c();
        ec1.Name = 'TestPartner';
        ec1.Partner_Number__c = '345678';
        insert ec1;
    
        //Insert Record ERP Partner Association //
        list<ERP_Partner_Association__c> listERP= new list<ERP_Partner_Association__c>();
        //Record 1
        ERP_Partner_Association__c epa = new ERP_Partner_Association__c();
        epa.Name = 'Partner';
        epa.ERP_Customer_Number__c = '10005328';
        //epa.ERP_Partner_Number__c = '10005328';
        epa.ERP_Partner__c = ec.id;
        epa.Partner_Function__c = 'Z1=Site Partner'; 
        epa.Sales_Org__c = '0600';
        epa.Customer_Account__c = accObj.id;
        listERP.add(epa);
        //insert epa;
        
       
        //Record 2
        ERP_Partner_Association__c epa1 = new ERP_Partner_Association__c();
        epa1.Name = 'Partner1';
        epa1.ERP_Customer_Number__c = '125687';
        //epa1.ERP_Partner_Number__c = '100053218';
        epa1.ERP_Partner__c = ec1.id;
        epa1.Partner_Function__c = 'EU=End User'; 
        epa1.Sales_Org__c = '0600';
        epa1.Customer_Account__c = accObj.id;
        listERP.add(epa1);
        insert listERP;
        
        // Update REcord 2
        epa1.ERP_Partner__c = ec.id;
        update epa1;
        
        //insert record 4
        ERP_Partner_Association__c epa3 = new ERP_Partner_Association__c();
        epa3.Name = 'Partner1';
        epa3.ERP_Customer_Number__c = '10005328';
        //epa.ERP_Partner_Number__c = '10005328';
        epa3.ERP_Partner__c = ec.id;
        epa3.Partner_Function__c = 'Z1=Site Partner'; 
        epa3.Sales_Org__c = '0600';
        epa3.Customer_Account__c = accObj.id;
        insert epa3;
       
       
        
    }
        testmethod static void TriggerTestBefore() {
        
        //insert Account
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test23', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234', Prefered_Language__c ='English',AccountNumber = '145678',Credit_Block__c =false);
        insert accObj;
        
        //insert record for ERP Partner//
        list<ERP_Partner__c>listERPpart = new list<ERP_Partner__c>();
        ERP_Partner__c ec = new ERP_Partner__c();
        ec.Name = 'Test';
        ec.Partner_Number__c = '145678';
        listERPpart.add(ec);
        
        ERP_Partner__c ec1 = new ERP_Partner__c();
        ec1.Name = 'Test';
        ec1.Partner_Number__c = '1457678';
        listERPpart.add(ec1);
        insert listERPpart;
        
        //insert Partner Association //
        list<ERP_Partner_Association__c> listERP= new list<ERP_Partner_Association__c>();
        ERP_Partner_Association__c epa = new ERP_Partner_Association__c();
        epa.Name = 'Partner';
        epa.ERP_Customer_Number__c = '145678';
        epa.ERP_Partner_Number__c = '145678';
        epa.ERP_Partner__c = ec.id;
        epa.Partner_Function__c = 'Z1=Site Partner'; 
        epa.Sales_Org__c = '0600';
        epa.Customer_Account__c = accObj.id;
        listERP.add(epa);
        
        //Record 2
        ERP_Partner_Association__c epa1 = new ERP_Partner_Association__c();
        epa1.Name = 'Partner1';
        epa1.ERP_Customer_Number__c = '145678';
        epa1.ERP_Partner_Number__c = '1457678';
        epa1.ERP_Partner__c = ec1.id;
        epa1.Partner_Function__c = 'Z1=Site Partner'; 
        epa1.Sales_Org__c = '0600';
        epa1.Customer_Account__c = accObj.id;
        listERP.add(epa1);
        insert listERP;
        
        }
        
        
        
}
global class EmailServiceOnContactEmail implements Messaging.InboundEmailHandler 
{
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{  
            Id HDRecordtype = [Select id, name from recordtype where sObjecttype = 'Case' and developername = 'Helpdesk'].Id;
            String lstSubjectForReplace = email.subject;
            String lstDescription = email.htmlBody;
            String EscapeSubject = lstSubjectForReplace.unescapeXml();
            string caseSubject = 'Unity Case Request – '+ EscapeSubject; 
            
            //string EmailCaseOwner = 'DISP NA';
            //string EmailNoCaseOwner = 'Email to Case - No Owner';
            
            //map<string,Id> mapCaseOwner = new map<string,Id>();
            //for(Group queue : [Select Id,Name from Group where type = 'Queue' and (Name =: EmailCaseOwner or Name =: EmailNoCaseOwner)])
            //{
            //    mapCaseOwner.put(queue.Name,queue.Id);
            //}
                        
            Map<String, String> dictionary = GetDictionaryFromBody(email.plainTextBody);
            
            Case casealias = new Case();
            casealias.recordtypeId = HDRecordtype;
            casealias.Origin = 'Email';
            casealias.Subject= caseSubject;
            casealias.local_Subject__c = caseSubject;
            //if(mapCaseOwner.containsKey(EmailNoCaseOwner))casealias.ownerId = mapCaseOwner.get(EmailNoCaseOwner);
            if (dictionary <> null && dictionary.ContainsKey('CONTACT EMAIL') && !String.isEmpty(dictionary.get('CONTACT EMAIL').trim()))
            {  
                string EmailAddress = (dictionary.get('CONTACT EMAIL').trim()).unescapeXml();
                if(EmailAddress <> null){
                    List<Contact> lstc = [select Id,AccountId from Contact where Email =: EmailAddress limit 1];
                    if(lstc.size() > 0){
                        casealias.AccountId= lstc[0].AccountId;
                        casealias.ContactId = lstc[0].Id;
                       // if(mapCaseOwner.containsKey(EmailCaseOwner))casealias.ownerId = mapCaseOwner.get(EmailCaseOwner);                       
                    }
                }
            }            
            //casealias.Local_Description__c = lstDescription.unescapeXml();
            casealias.Local_Description__c = caseSubject;
            casealias.Local_Internal_Notes__c = lstDescription.unescapeXml();   
            casealias.Priority='Medium';            
            casealias.Is_This_a_Complaint__c='Yes';
            casealias.Is_escalation_to_the_CLT_required__c = 'TBD';
            casealias.Was_anyone_injured__c = 'TBD';        
            casealias.Smart_Connect_Case__c=True;
            casealias.Reason = 'Analysis/Troubleshoot'; 
            
            
            //For assignment rule execution
            List<AssignmentRule> AR = new List<AssignmentRule>();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
            if(AR.size()>0)
            {
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.assignmentRuleId= AR[0].id;
                casealias.setoptions(dmo);
            }         
            Insert casealias;       

        }catch(Exception e){system.debug('#EMAILSERVICE: '+e);}
        return result;
    }
    global Map<String, String> GetDictionaryFromBody(String strBody)
    {  
        Map<String, String> dictionary = new Map<String, String>();
        if(strBody <> null){
            strBody = strBody.replaceAll('\n', '|');  
            List<String> params = strBody.split('\\|');  
                    
            for (String param : params)
            {
              List<String> kvp = param.split(':');
              if (kvp.size() == 2)
              {
                dictionary.put(kvp.get(0).trim().toUpperCase(), kvp.get(1).trim());
              }
            }
        }
        return dictionary;
    } 
}
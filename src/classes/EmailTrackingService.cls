/************************************************************************************
 * @name - EmailTrackingService.cls
 * @author - Ram
 * @description - Common tracking implementation similar to standard functionality for text emails which updates email sent to customer by creating tasks 
 * @date 1/3/2017
 *  ************************************************************************************/
 
public class EmailTrackingService implements Messaging.InboundEmailHandler {
    
    
    /*
     * @description - Process incoming HTML email template by scaning tracking code to create Task
     *              - Example usage: <pre><code>whatId::whoId</code></pre>
     * @param - System Messaging class which represents email and context
     * @return - status of processing result
     */
    
    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        Id whoId, whatId;
        Pattern whoPattern = Pattern.compile('(?i)<pre><code>(.*?)</code></pre>');
        boolean status = false;
        try {
            String messageBody = !String.isBlank(email.plainTextBody) ? email.plainTextBody : email.htmlBody;
            Matcher whoMatch = whoPattern.matcher(messageBody);
            while (whoMatch.find()) {
                String[] tempIds = whoMatch.group().replaceAll('<.*?>', '').trim().split('::');
                //System.debug(logginglevel.info, 'Temp Id arrays == '+tempIds);
                if (tempIds != null && tempIds.size() > 0) {
                    whoId = tempIds[1].trim();
                    whatId =    tempIds[0].trim();  
                }
            }
            Task emailBcc = new Task(WhoId = whoId, WhatId= whatId, Subject = email.subject, Description =messageBody, ActivityDate = System.Today(), Status='Completed');
            Database.insert(emailBcc);
            //System.debug(logginglevel.info, ' == after emailBcc task == '+emailBcc);
            status = true;      
        } catch (Exception ex ) {
            System.debug(logginglevel.info, ' == email service exception  == '+ex); 
        }
        result.success = status;
        return result;
    }
    
}
/************************************************************************************
 * @name - EmailTrackingService_Test.cls
 * @author - Ram
 * @description - Unit test for EmailTrackingService class
 * @date 1/3/2017
 *************************************************************************************/
 
@isTest
private class EmailTrackingService_Test {
	static String emailBody = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body><p>Test InvoiceEmail</p><pre><code>{0}::{1}</code></pre></body></html>';
	
	@testSetup static void setup() {
		System.runAs(TestDataFactory.createUser('System Administrator')) {
			List<Account> acc = TestDataFactory.createAccounts();
			Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
			for (Account a : acc) {
				a.RecordTypeId = rtId;
			}
			update acc;
			List<Contact> cts = TestDataFactory.createObjects(Contact.sObjectType, 'Test_ContactData');
			List<Invoice__c> invoices = TestDataFactory.createInvoices(acc, cts);
			insert invoices;
			
		}			
	}
	
	@isTest static void testEmailTracking() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			String CRON_EXP = '0 0 * 1 * ?';
			Test.startTest();
			//Date tempDate = System.Today() - 6;
			//String query = 'Select Id, Name, Amount__c, Invoice_Date__c, Invoice_Due_Date__c, Days_Past_Due__c, Billing_Contact__c, CurrencyIsoCode, Sold_To__c, District_Sales_Manager__c, District_Service_Manager__c From Invoice__c where Sold_To__c != null AND Billing_Contact__c != null AND Invoice_Due_Date__c <= :tempDate'; 
			//System.debug(logginglevel.info, 'Query Results === '+Database.query(query));
			DSO_EmailInvoiceProcess invProcess = new DSO_EmailInvoiceProcess(0);
			invProcess.accountIds.clear();
			Database.executeBatch(invProcess, 2000);
			Test.stopTest();
			List<Contact> ct = [Select Id From Contact];
			System.assert(ct.size() > 0);
			List<Workflow_Remainder__c> wr = [Select w.Id, w.External_Field__c, w.Due_Days__c, w.ContactId__c, w.AccountId__c, w.Notification_Sent__c From Workflow_Remainder__c w where Remainder_Type__c='First_Remainder_Sent__c'];
			System.debug(logginglevel.info, 'wr[0] === '+wr[0]);
			System.assert(wr.size() > 0);
			System.assert(wr[0].Due_Days__c >=6);
			System.assert(wr[0].External_Field__c != null);
			emailBody = emailBody.replace('{0}', wr[0].Id);
			emailBody = emailBody.replace('{1}', ct[0].Id);
			//construct email 
			Messaging.InboundEmail email = new Messaging.InboundEmail();
			Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
			email.htmlBody = emailBody;
			//call email service
			EmailTrackingService service = new EmailTrackingService();
			service.handleInboundEmail(email, envelope);	
		}
	}
}
public class EmergencyCaseController 
{
    List<Case> cases;
    User currentUser;
    public List<SelectOption> queueStr { get; set; }
    public String selectQueue { get; set; }

    public EmergencyCaseController()
    {
        currentUser = [select Name, Case_Monitor_Queue__c from User where Id =: userinfo.getUserId()];
        queueStr = new List<SelectOption>();
        for(Queues_for_case_monitor__c qf : Queues_for_case_monitor__c.getall().values())
        {
            SelectOption option = new SelectOption(qf.Name, qf.Name);
            queueStr.add(option);
        } 
        selectQueue = String.isNotblank(currentUser.Case_Monitor_Queue__c) ? currentUser.Case_Monitor_Queue__c :'THD OIS NA';
        cases = new List<Case>();

        cases = [SELECT account.Name, RecordType.Name, Warning_time__c, CreatedDate, CaseNumber, ProductSystem__c, Contact.Name, Subject, Status, Priority, Owner.Name, Origin, LastModifiedDate FROM Case where Status = 'New' and Owner.Name =: selectQueue order by Priority, Warning_time__c asc NULLS LAST];
    }

    public Pagereference resetTable()
    {
        cases = new List<Case>();

        cases = [SELECT account.Name, RecordType.Name, Warning_time__c, CreatedDate, CaseNumber, ProductSystem__c, Contact.Name, Subject, Status, Priority, Owner.Name, Origin, LastModifiedDate FROM Case where Status = 'New' and Owner.Name =: selectQueue order by Priority, Warning_time__c asc NULLS LAST];
        
        currentUser.Case_Monitor_Queue__c = selectQueue;
        
        update currentUser;
        
        return null;
    }

    public static Long calculateTimeDifference(Case c)
    {
        Long dt1Long = DateTime.now().getTime();
        Long dt2Long = c.Warning_time__c.getTime();
        Long milliseconds =  dt1Long - dt2Long;
        Long seconds = milliseconds / 1000;
        Long minutes = seconds / 60;
        return minutes;
    }

    public List<CaseInfo> getCases() 
    {
        List<CaseInfo> whiteCase = new List<CaseInfo>();
        for(Case c : cases)
        {
            if(c.Warning_time__c == null || ((c.Priority == 'Emergency' || c.Priority == 'High') && c.Warning_time__c != null && calculateTimeDifference(c) < 20) || ((c.Priority == 'Medium' || c.Priority == 'Low') && c.Warning_time__c != null && calculateTimeDifference(c) < 1430))
            {
                whiteCase.add(new CaseInfo(c));
            }
        }
        return whiteCase;
    } 
    
    public List<CaseInfo> getYellowCases() 
    {
        List<CaseInfo> yellowCase = new List<CaseInfo>();
        for(Case c : cases)
        {
            if(((c.Priority == 'Emergency' || c.Priority == 'High') && c.Warning_time__c != null && calculateTimeDifference(c) > 20 && calculateTimeDifference(c) < 30) || ((c.Priority == 'Medium' || c.Priority == 'Low') && c.Warning_time__c != null && calculateTimeDifference(c) >= 1430 && calculateTimeDifference(c) < 1440))
            {
                yellowCase.add(new CaseInfo(c));
            }
        }
        return yellowCase;
    } 
    
    public List<CaseInfo> getRedCases() 
    {
        List<CaseInfo> redCase = new List<CaseInfo>();
        for(Case c : cases)
        {
            if(((c.Priority == 'Emergency' || c.Priority == 'High') && c.Warning_time__c != null && calculateTimeDifference(c) > 30) || ((c.Priority == 'Medium' || c.Priority == 'Low') && c.Warning_time__c != null && calculateTimeDifference(c) > 1440))
            {
                redCase.add(new CaseInfo(c));
            }
        }
        return redCase;
    }
     
    public class CaseInfo
    {
        public Case theCase { get; set; }
        public Long timing { get; set; }
        public CaseInfo(Case newCase)
        {
            this.theCase = newCase;

            this.timing = newCase.Warning_time__c == null ? 0 : EmergencyCaseController.calculateTimeDifference(newCase) / 60;
        }
    }
}
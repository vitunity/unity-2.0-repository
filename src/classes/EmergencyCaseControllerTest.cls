@isTest(seeAllData = true)
public class EmergencyCaseControllerTest
{
    public static testMethod void testEmergencyCaseController()
    {
        PageReference EmergencyCase = Page.EmergencyCase;
        Test.setCurrentPage(EmergencyCase);
        ApexPages.currentPage().getParameters().put('que', 'OIS');
        EmergencyCaseController ec = new EmergencyCaseController();
        ec.getCases();
        ec.getYellowCases();
        ec.getRedCases();
        ec.resetTable();
    }
}
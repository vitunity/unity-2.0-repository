/***************************************************************************
Author: Amitkumar Katre
Created Date: 15-May-2017
Project/Story/Inc/Task : Utility Class
Description: Utility class for error handling
*/
public class ErrorMessageHandler {
    
    public static String getMessageText(Exception e){
        String output = '';
        String msg = e.getMessage();
        String [] msgList = msg.split(',',-2);
        if(msgList.size() > 1){
            String outputTemp = msgList[1];
            String [] outputTempList = outputTemp.split(':',-2);
            output = outputTempList[0];
        }else{
            output = msgList[0]; 
        }
        return output;
    }
}
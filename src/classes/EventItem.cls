public class EventItem 
{
    public Event ev;
    public String formatedDate; 
    public eventItem(Event e) 
    { 
        ev= e;
        // build formated date
        //9:00 AM - 1:00 PM
        //  system.debug(e.activitydatetime.format('MMM a'));
        //  system.debug(e.DurationInMinutes);
        if(e.ActivityDateTime != null)
        {
            Datetime endd = e.ActivityDateTime.addMinutes(e.DurationInMinutes);
            //system.debug(e.activitydatetime.format('h:mm a '));
            formatedDate = e.ActivityDateTime.month() + '/' + e.ActivityDateTime.day() + ' ' + e.ActivityDateTime.format('h:mm a') + 
            ' - ' + endd.format('h:mm a');
            system.debug(formateddate);
        }
    }
    
    public eventItem(Event e, Boolean multiDays, Boolean allDay)
    {
        ev = e;
        formatedDate = e.StartDateTime.dateGMT().month() + '/' + e.StartDateTime.dateGMT().day() + ' - ' + e.EndDateTime.dateGMT().month() + '/' + e.EndDateTime.dateGMT().day();
    }
    
    public eventItem(Event e, Boolean multiDays)
    {
        ev = e;
        formatedDate = e.StartDateTime.Date().month() + '/' + e.StartDateTime.Date().day() + ' - ' + e.EndDateTime.Date().month() + '/' + e.EndDateTime.Date().day();
    }
    public Event getEv() { return ev; }
    public String getFormatedDate() { return formatedDate; }
}
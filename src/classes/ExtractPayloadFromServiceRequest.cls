//Part of the ServiceMax Connected Field Service Lighthouse Program
//Code Written by Amit Jain
//Purpose is to break apart the code populated into the Service Request Problem Description to allow for a break down into individual fields
/********************************************************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Feb-28-2018 - Chandramouli - STSK0014020 - Modified code to remove Excepted & Actual Fault Values from case description. 
Jan-24-2018 - Chandramouli - STSK0013775 - Modified code to copy the message value of problem description to case description.
****************************************************************************/ 

public class ExtractPayloadFromServiceRequest
{
    
    public List<SVMXC__Service_Request__c> lstOfSRS = new List<SVMXC__Service_Request__c>();
    public set<String> serialnumbers = new set<String>();
    public map<string,Id> serialnumbers2ip = new map<string,Id>();
    public Map<string, string> mapOfPayloadVarNameToField = new Map<string, string>();

    
    public Map<String, Schema.SObjectField> fieldMap = new Map<String, Schema.SObjectField>();
    /*public set<string> serialnumberset = new set<string>();
    public map<string,id> serialnumber2ip = new map<string,id>();*/
        
    public ExtractPayloadFromServiceRequest(list<SVMXC__Service_Request__c> lstOfSRs){
        this.lstOfSRs= lstOfSRs;
        fieldMap = Schema.sObjectType.SVMXC__Service_Request__c.fields.getMap();
        System.debug('fieldMap '+fieldMap);
        mapOfPayloadVarNameToField.put('priority', 'SVMXC__Priority__c');
            mapOfPayloadVarNameToField.put('name', 'SMAX_PS_Alert_Name__c');
                 mapOfPayloadVarNameToField.put('modelNumber', 'SMAX_PS_TW_Model_Number__c');
                    mapOfPayloadVarNameToField.put('serialNumber', 'SMAX_PS_TW_Serial_Number__c');
            }
    
        
    public void updateFieldsForPayload(){
        
        for(SVMXC__Service_Request__c currentSR: lstOfSRs){
            system.debug('***CM currentSR : '+currentSR);
            if(currentSR.SVMXC__Problem_Description__c!= null && currentSR.SVMXC__Problem_Description__c.length() > 0)
                updateSRFields(currentSR, currentSR.SVMXC__Problem_Description__c);
        }
    }
    
    public void updateSRFields(SVMXC__Service_Request__c currentSR, String payloadMsg){
         System.debug('Payload Msg '+payloadMsg); 
        List<String> lstMessages = payloadMsg.split('\n');
        Map<String, String> payloadNameToValue = new Map<String, String>();
        
        for(String currentMessage : lstMessages){

            system.debug('***CM currentMessage'+currentMessage + 'test');
            currentMessage = currentMessage.replace('\n','');
            system.debug('***CM currentMessage'+currentMessage + 'test');
            //Jan-24-2018 - Chandramouli - STSK0013775 - Modified code to copy the message value of problem description to case description.
            if (currentMessage.startsWith('message'))
            {
                //Feb-28-2018 - Chandramouli - STSK0014020 - Modified code to remove Excepted & Actual Fault Values from case description. 
                currentMessage = currentMessage.substringBefore(', Excepted & Actual Fault Values:');
                currentSR.Case_Description__c = currentMessage;
            }
            List<String> currentPayload = currentMessage.split(' : ');
            if(currentPayload.size()==2 && mapOfPayloadVarNameToField.containsKey(currentPayload[0]))
            {
                 payloadNameToValue.put(currentPayload[0], currentPayload[1]);
            }
            
            
        }
        if(payloadNameToValue.isEmpty()){
            return;
        }
        for(String payloadName : mapOfPayloadVarNameToField.keyset()){
            String fieldName = mapOfPayloadVarNameToField.get(payloadName);
            String fieldValue = payloadNameToValue.get(payloadName);
            
             System.debug('field Name '+fieldName +' '+fieldMap.get(fieldName)); 
            updateAccordingToFieldType(currentSR, fieldName, fieldValue, fieldMap.get(fieldName));
            /*if (fieldName == 'SMAX_PS_TW_Serial_Number__c')
                updateserialnumber(currentSR,fieldValue);*/
        }
    }
    
    public void updateAccordingToFieldType(SVMXC__Service_Request__c currentSR, String fieldName, String fieldValue, Schema.SObjectField sfield){
        System.debug('YYYYYYYYYYYYYYYYYYYYYOIOIOIYY'+fieldName +'  '+fieldValue +' '+sfield);     
        system.debug('***CM currentSR : '+currentSR);   
        Schema.DescribeFieldResult f = sfield.getDescribe();
        
        if(f.getType() == Schema.Displaytype.Double  || f.getType() == Schema.Displaytype.Percent || f.getType() == Schema.Displaytype.Currency){
            currentSR.put(fieldName, double.valueOf(fieldValue));
        }else if(f.getType() == Schema.Displaytype.Integer){
            currentSR.put(fieldName, Integer.valueof(fieldValue));
        }else if(f.getType() == Schema.Displaytype.Boolean){
            currentSR.put(fieldName, Boolean.valueof(fieldValue));
        }else if(f.getType() == Schema.Displaytype.DateTime){
            currentSR.put(fieldName, DateTime.valueOfGmt(fieldValue));
        }else if(f.getType() == Schema.Displaytype.Date){
            currentSR.put(fieldName, Date.valueof(fieldValue));
        }else{
            currentSR.put(fieldName, fieldValue);
        }
        
        if (currentSR.svmxc__Serial_Number__c == null && fieldName == 'SMAX_PS_TW_Serial_Number__c' && !String.isBlank(fieldValue) && fieldValue != null)
        {
            system.debug('fieldvalue : '+fieldvalue + 'test');
            fieldvalue = fieldvalue.replace('\\n\\r', ' ');
            fieldvalue = fieldvalue.replace('\\n', ' ');
            fieldvalue = fieldvalue.replace('\\r', ' ');
            fieldvalue = fieldvalue.replace('\\', ' ');
            fieldvalue = fieldvalue.normalizeSpace();
            system.debug('fieldvalue : '+fieldvalue + 'test');
            For (SVMXC__Installed_Product__c ip : [select id, Name from SVMXC__Installed_Product__c where Name = :fieldvalue ])
            {
                serialnumbers2ip.put(ip.Name, ip.id);
            }
        }

        if (currentSR.SVMXC__Serial_Number__c == null && serialnumbers2ip.size() > 0 && fieldValue != null && serialnumbers2ip.get(fieldValue) != null)
        {
            currentSR.SVMXC__Serial_Number__c = serialnumbers2ip.get(fieldValue);
        }
        system.debug('***CM currentSR : '+currentSR);

    }
}
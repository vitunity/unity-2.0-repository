@isTest
public class ExtractPayloadFromServiceRequesttest
{

    private static SVMXC__Service_Request__c testSR;
    public static Account testAcc;
    public static Contact objCont;
    public static Product2 objProd;
    public static Map<String,Schema.RecordTypeInfo> caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName();

    static testMethod void myUnitTest() {

        Profile profile = [Select Id From Profile where Name = 'System Administrator' limit 1];
        User usr1= new User(alias = 'standt', email='standardtestusqq2@testorg.com',emailencodingkey='UTF-8', 
            lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = profile.Id, timezonesidkey='America/Los_Angeles',username='standarqqqdtestuse92@testclass.com');
        insert usr1;
        system.runAs(usr1){
            testAcc = AccountTestData.createAccount();
            testAcc.AccountNumber = '123456';
            testAcc.ERP_Site_Partner_Code__c = '123456';
            insert testAcc; 

            objCont = SR_testdata.createContact();
            objcont.AccountId = testAcc.Id;
            insert objCont;

            SVMXC__Service_Group__c objSvcGrp = new  SVMXC__Service_Group__c(Name = 'test', SVMXC__Group_Code__c = '4578',District_Manager__c=UserInfo.getUserId(),ERP_Cost_Center__c = '450271U6J');
            Insert objSvcGrp;

            SVMXC__Site__c objLoc = new SVMXC__Site__c();
            objLoc.Name = 'Test Location';
            objLoc.SVMXC__Street__c = 'Test Street';
            objLoc.SVMXC__Country__c = 'United States';
            objLoc.SVMXC__Zip__c = '98765';
            objLoc.ERP_Functional_Location__c = 'Test Location';
            objLoc.Plant__c = 'Test Plant';
            objLoc.ERP_Site_Partner_Code__c = '123456';
            objLoc.SVMXC__Location_Type__c = 'Depot';
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            objLoc.SVMXC__Stocking_Location__c = true;
            objLoc.SVMXC__Account__c = testAcc.id;
            objLoc.FE_Service_Team__c = objSvcGrp.Id;
            insert objLoc;

            objProd = SR_testdata.createProduct();
            objProd.Name = 'Test Product';
            objProd.Budget_Hrs__c =10.0;
            objProd.Credit_s_Required__c = 3;
            objProd.Material_Group__c = 'H:CREDTS';
            objProd.UOM__c = 'DA';
            objProd.ProductCode = 'ABABAB';
            objprod.course_code__c = 'CREDTS';
            insert objProd;
            
            

            SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
            objTopLevel.Name = 'H272456';
            objTopLevel.SVMXC__Serial_Lot_Number__c = 'H272456';
            objTopLevel.SVMXC__Product__c = objProd.ID;
            objTopLevel.SVMXC__Site__c = objloc.ID;
            objTopLevel.ERP_Reference__c = 'H272456';
            objTopLevel.ERP_CSS_District__c = 'ABC';
            objTopLevel.ERP_Sales__c = 'Test ERP Sales';
            objTopLevel.SVMXC__Company__c = testAcc.id;
            objTopLevel.Service_Team__c = objSvcGrp.id;
            insert objTopLevel;

            
                
            case objCase = SR_testdata.createCase();
            objCase.AccountID = testAcc.ID;
            objCase.ContactID = objCont.ID;
            objCase.Reason = 'System Down';
            objCase.Priority = 'Medium';
            objCase.Type ='Problem';
            objCase.Reason = 'Source Exchange';
            objCase.RecordTypeId = caseRecordType.get('M2M').getRecordTypeId();
            objCase.SVMXC__Top_Level__c = objTopLevel.id;
            objCase.SVMXC__Site__c = objLoc.id;

            SVMXC__Service_Request__c sr = new SVMXC__Service_Request__c();
            sr.SVMXC__Contact__c = objcont.id;
            sr.SVMXC__Problem_Description__c = 'serialNumber : H272456\nname : Beam On State UDR1 Interlock Alarm\npriority : 1\nmessage : Model:HE2100CD,Mode:Clinical,Version: V9.01.01-00, ErrorCode: N/AErrorData: N/A, DailyCountThreshhold: (Daily Count = 3,   Daily Threshold = 3), WeeklyCountThreshold: (Weekly Count = 5,   Weekly Threshold = 6)\ntype : SC+ Alarm\nalertTime : 2017-10-13 14:16:25.025';

            insert sr;
        }

    }


}
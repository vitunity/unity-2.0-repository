/**
 *	@author			:		Puneet Mishra
 *	@createdDate	:		8 December 2017
 *	@description	:		Batch class will process the Forecast View StartDate and EndDate
 */
public class FC_DateAdjustmentBatch implements Database.Batchable<sObject>, Database.Stateful{ 
	
    public Map<String, List<Forecast_View__c>> viewMap;
    private String query;                                                                 
    @testvisible private List<String> dateLiterals = new List<String>{	'THIS_FISCAL_QUARTER', 'LAST_FISCAL_QUARTER', 
                                                                        'fytd', 'LAST_FISCAL_YEAR', 'NEXT_FISCAL_QUARTER', 'curprev1', 
       	                                                                'curnext1', 'curlast3', 'curnext3'
     };
        
    public FC_DateAdjustmentBatch(String query){
        this.query = query;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
		/*String q = 'SELECT Id, Name, Deal_Probability__c, Deal_Probability_Operator__c, Excepted_Close_Date_End__c, Excepted_Close_Date_Range__c,'+
            	   ' Excepted_Close_Date_Start__c, Region__c, Sub_Region__c, User__c, Stage__c, Quote_Type__c'+
                   ' FROM Forecast_View__c '+
                   ' WHERE Excepted_Close_Date_Range__c IN: dateLiterals';*/
        system.debug(' ==== query ==== ' + query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Forecast_View__c> viewList){
    	viewMap = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        system.debug(' ==== viewMap ==== ' + viewMap);
        for(String key : viewMap.keySet()) {
            FC_DateAdjustmentHelper.updateForecastViews(key, viewMap.get(key));
        }
    }
    
    public void finish(Database.BatchableContext BC){
    }
}
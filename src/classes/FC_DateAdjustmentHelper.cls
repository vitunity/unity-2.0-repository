/**
 *  @author         :       Puneet Mishra
 *  @createdDate    :       8 December 2017
 *  @description    :       Helper will autoadjust the dates, called from FC_DateAdjustmentBatch
 */
public class FC_DateAdjustmentHelper {
    
    @testvisible private static List<String> dateLiterals = new List<String>{   'custom', 'THIS_FISCAL_QUARTER', 'LAST_FISCAL_QUARTER', 
                                                                                'fytd', 'LAST_FISCAL_YEAR', 'NEXT_FISCAL_QUARTER', 'curprev1', 
                                                                                'curnext1', 'curlast3', 'curnext3'
                                                                            };
    @testvisible private static FiscalYearSettings fiscalYr{get;set;}
    @testvisible private static string fiscalId;
    @testvisible private static Date fiscalStartDate;
    @testvisible private static Date fiscalEndDate;
    @testvisible private static Date Q1_StartDate {get;set;}
    @testvisible private static Date Q1_EndDate {get;set;}
    @testvisible private static Date Q2_StartDate {get;set;}
    @testvisible private static Date Q2_EndDate {get;set;}
    @testvisible private static Date Q3_StartDate {get;set;}
    @testvisible private static Date Q3_EndDate {get;set;}
    @testvisible private static Date Q4_StartDate {get;set;}
    @testvisible private static Date Q4_EndDate {get;set;}
    @testvisible private static Integer currentQuarter{get;set;}
    @testVisible private static string currentYear {get;set;}
    /**
     * Puneet Mishra, 12 dec 2017, method will return a map with date literals as Key and forecast view list as their values
     */
    public static Map<String, List<Forecast_View__c>> forecastViewsList(List<Forecast_View__c> viewList) {
        Map<String, List<Forecast_View__c>> FC_Map = new Map<String, List<Forecast_View__c>>();
        for(Forecast_View__c FC_View : viewList) {
            if(FC_Map.containsKey(FC_View.Excepted_Close_Date_Range__c)) {
                FC_Map.get(FC_View.Excepted_Close_Date_Range__c).add(FC_View);
            } else {
                FC_Map.put(FC_View.Excepted_Close_Date_Range__c, new List<Forecast_View__c>{FC_View});
            }
        }
        system.debug(' ==== FC_Map ==== ' + FC_Map);
        return FC_Map;
    }
    
    
    public static List<Forecast_View__c> updateForecastViews(String view, List<Forecast_View__c>viewList) {
        fiscalYr = new FiscalYearSettings();
        currentYear = string.valueOf(getCurrentFiscalYear());
        system.debug(' === currentYear === ' + currentYear);
        fiscalYr = [SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                    WHERE Name =: currentYear 
                    ORDER BY Name DESC NULLS LAST LIMIT 1];
        system.debug(' === fiscalYr === ' + fiscalYr);
        //fiscalStartDate = fiscalYr.StartDate;
        //fiscalEndDate = fiscalYr.EndDate;
        
        Date startDate, endDate;
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        Period peri = new Period();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
            system.debug(' === per === ' + per);
			if(per.Number == 1) {
                Q1_StartDate = per.StartDate;
                Q1_EndDate = per.EndDate;
            } else if(per.Number == 2) {
                Q2_StartDate = per.StartDate;
                Q2_EndDate = per.EndDate;
            } else if(per.Number == 3) {
                Q3_StartDate = per.StartDate;
                Q3_EndDate = per.EndDate;
            } else if(per.Number == 4) {
                Q4_StartDate = per.StartDate;
                Q4_EndDate = per.EndDate;
            }
            peri = per; 
            periodMap.put(per.Number, per);
        }
        system.debug(' === periodMap === ' + periodMap);
        currentQuarter = fetchQuarter();
        system.debug(' === currentQuarter === ' + currentQuarter);
        system.debug(' === B 4 viewList === ' + viewList);
        if(view == 'THIS_FISCAL_QUARTER') {
            thisFiscalQuarter(periodMap.get(currentQuarter), viewList);
        } else if(view == 'LAST_FISCAL_QUARTER') {
            //lastFiscalQuarter(currentQuarter, periodMap.get(currentQuarter-1), viewList);
            lastFiscalQuarter(currentQuarter, periodMap, viewList);
        } 
        /*else if(view == 'fytd') {
            fytd(fiscalYr, viewList);
        }*/ 
		else if(view == 'LAST_FISCAL_YEAR') {
            lastFiscalYear(Integer.valueOf(currentYear), viewList);
        } else if(view == 'NEXT_FISCAL_QUARTER') {
            //nextFiscalQuarter(currentQuarter, peri, viewList);
            nextFiscalQuarter(currentQuarter, periodMap, viewList);
        } else if(view == 'curprev1') {
            currentPre1(Integer.valueOf(currentYear), periodMap, viewList);
        } else if(view == 'curnext1') {
            currentNext1(currentQuarter, periodMap, viewList);
        } else if(view == 'curlast3') {
            currentLast3(currentQuarter, periodMap, viewList);
        } else if(view == 'curnext3') {
            currentNext3(currentQuarter, periodMap, viewList);
        }
        system.debug(' === AFTER viewList === ' + viewList);
        update viewList;
        return null;
    }
    
    @testVisible
    private static list<Forecast_View__c> thisFiscalQuarter(Period per, List<Forecast_View__c>viewList) {
        system.debug(' == B4 == ' + viewList);
        system.debug(' == per == ' + per);
        system.debug(' == per.StartDate == ' + per.StartDate);
        system.debug(' == per.EndDate == ' + per.EndDate);
        reassignDates(viewList, per.StartDate, per.EndDate);
        
        system.debug(' == AFTER == ' + viewList);
        return viewList;
    }
    
    @testVisible
    private static list<Forecast_View__c> lastFiscalQuarter(Integer quarter, Map<Integer, Period> perMap, List<Forecast_View__c>viewList) {
        system.debug(' == B4 == ' + viewList);
        
        Period peri = new Period();
        if(quarter == 1) {
            String pre_Yr = String.valueOf(Integer.valueOf(currentYear) - 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: pre_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            peri = [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                     FROM period 
                     WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' AND Number =: 4];
            reassignDates(viewList, peri.StartDate, peri.EndDate);
            
        } else {
            reassignDates(viewList, perMap.get(currentQuarter-1).StartDate, perMap.get(currentQuarter-1).EndDate);
        }
        
        system.debug(' == AFTER == ' + viewList);
        return viewList;
    }
    
    @testVisible
    private static list<Forecast_View__c> nextFiscalQuarter(Integer quarter, Map<Integer, Period> perMap, List<Forecast_View__c>viewList) {
        Period peri = new Period();
        system.debug(' QUARTER => ' + quarter);
        if(quarter == 4) {
            String pre_Yr = String.valueOf(Integer.valueOf(currentYear) + 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: pre_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            peri = [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                     FROM period 
                     WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' AND Number =: 1];
            
            reassignDates(viewList, peri.StartDate, peri.EndDate);
        } else {
            
            reassignDates(viewList, perMap.get(currentQuarter+1).StartDate, perMap.get(currentQuarter+1).EndDate);
        }
        
        system.debug(' == AFTER == ' + viewList);
        return viewList;
    }
    
    @testVisible
    private static list<Forecast_View__c> lastFiscalYear(Integer currentYear, List<Forecast_View__c>viewList) {
        String preYear = String.valueOf(currentYear - 1);
        FiscalYearSettings preFiscalYr = [SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                            WHERE Name =: preYear 
                                            ORDER BY Name DESC NULLS LAST LIMIT 1];
        for(Forecast_View__c view : viewList) {
            view.Excepted_Close_Date_Start__c = preFiscalYr.StartDate.format();
            view.Excepted_Close_Date_End__c = preFiscalYr.EndDate.format();
        }
        return viewList;
    }
    
    //StartDate = previous 1 Quarter startDate and EndDate = Current Quarter EndDate    
    @testVisible
    private static list<Forecast_View__c> currentPre1(Integer currentYear, Map<Integer, Period> periodMap, List<Forecast_View__c>viewList) {
        Map<Integer,Period> periMap = new Map<Integer,Period>();
        if(currentQuarter == 1) {
            String pre_Yr = String.valueOf(Integer.valueOf(currentYear) - 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: pre_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            for(Period per : [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                               FROM period 
                               WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' ]) {
                perimap.put(per.Number, per);
            }
            reassignDates(viewList, perimap.get(4).StartDate, periodMap.get(1).EndDate);
        } else {
            system.debug(' ===== viewList ===== ' + viewList);
            //system.debug(' ===== perimap ===== ' + perimap);
            system.debug(' ===== currentQuarter ===== ' + currentQuarter);
            //system.debug(' ===== perimap.get(currentQuarter-1).StartDate ===== ' + perimap.get(currentQuarter-1).StartDate);
            system.debug(' ===== periodMap ===== ' + periodMap);
            system.debug(' ===== periodMap.get(currentQuarter).EndDate ===== ' + periodMap.get(currentQuarter).EndDate);
            reassignDates(viewList, periodMap.get(currentQuarter-1).StartDate, periodMap.get(currentQuarter).EndDate);
        }
        return viewList;
    }
    
    //StartDate = current Quarter startDate and EndDate = next 1 Quarter EndDate
    @testVisible
    private static list<Forecast_View__c> currentNext1(Integer currentQuarter, Map<Integer, Period> periodMap, List<Forecast_View__c>viewList) {
        Map<Integer,Period> peri_Map = new Map<Integer,Period>();
        if(currentQuarter == 4) {
            
            String next_Yr = String.valueOf(Integer.valueOf(currentYear) + 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: next_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            for(Period per : [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                               FROM period 
                               WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' ]) {
                peri_map.put(per.Number, per);
            }
            reassignDates(viewList, periodMap.get(currentQuarter).StartDate, peri_map.get(1).EndDate);
            
        } else {
            reassignDates(viewList, periodMap.get(currentQuarter).StartDate, periodMap.get(currentQuarter+1).EndDate);
        }
        return viewList;
    }
    
    //StartDate = previous 3 Quarter startDate and EndDate = current Quarter EndDate
    @testVisible
    private static list<Forecast_View__c> currentLast3(Integer currentQuarter, Map<Integer, Period> periodMap, List<Forecast_View__c>viewList) {
        Map<Integer,Period> peri_Map = new Map<Integer,Period>();
        if(currentQuarter < 4) {
            String next_Yr = String.valueOf(Integer.valueOf(currentYear) - 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: next_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            for(Period per : [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                               FROM period 
                               WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' ]) {
                peri_map.put(per.Number, per);
            }
            Integer perviousQuarter = 4 - currentQuarter; 
            reassignDates(viewList, peri_map.get(4 - perviousQuarter).StartDate, periodMap.get(currentQuarter).EndDate);
            
        } else {
            reassignDates(viewList, periodMap.get(currentQuarter-3).StartDate, periodMap.get(currentQuarter).EndDate);
        }
        return viewList;
    }
    
    //StartDate = current Quarter startDate and EndDate = next 3 Quarter EndDate
    @testVisible
    private static list<Forecast_View__c> currentNext3(Integer currentQuarter, Map<Integer, Period> periodMap, List<Forecast_View__c>viewList) {
        Map<Integer,Period> peri_Map = new Map<Integer,Period>();
        if(currentQuarter >= 2) {
            String next_Yr = String.valueOf(Integer.valueOf(currentYear) + 1);
            FiscalYearSettings preFiscalYear = new FiscalYearSettings();
            preFiscalYear = [ SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                              WHERE Name =: next_Yr 
                                ORDER BY Name DESC NULLS LAST LIMIT 1];
            
            for(Period per : [ SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                               FROM period 
                               WHERE FiscalYearSettingsId =: preFiscalYear.Id AND Type =: 'Quarter' ]) {
                peri_map.put(per.Number, per);
            }
            reassignDates(viewList, periodMap.get(currentQuarter).StartDate, peri_map.get(math.mod(currentQuarter + 3, 4)).EndDate);
        } else {
            reassignDates(viewList, periodMap.get(currentQuarter).StartDate, periodMap.get(currentQuarter+3).EndDate);
        }
        return viewList;
    }
    
    // Start Date = financial year StartDate and EndDate = today's date
    /*@testVisible
    private static list<Forecast_View__c> fytd(FiscalYearSettings fiscalYr, List<Forecast_View__c>viewList) {
        system.debug(' ===== B$ ===> ' + viewList);
        reassignDates(viewList, fiscalYr.StartDate, system.today());
        system.debug(' ===== AFTER ===> ' + viewList);
        
        return viewList;
    }*/
    
    public static Date testDate;// declare to cover the lines of method
    @testVisible
    private static Integer fetchQuarter() {
        Date tDay;
        if(!test.isRunningTest())
        	tDay = system.today();
        else 
            tDay = testDate;
        
        if(tDay <= Q1_EndDate && tDay >= Q1_StartDate) {
            return 1;
        } else if(tDay <= Q2_EndDate && tDay >= Q2_StartDate) {
            return 2;
        } else if(tDay <= Q3_EndDate && tDay >= Q3_StartDate) {
            return 3;
        } else if(tDay <= Q4_EndDate && tDay >= Q4_StartDate) {
            return 4;
        }
        return 0;
    }
    
    // method to avoid repetation of code
    @testVisible
    private static List<Forecast_View__c> reassignDates(List<Forecast_View__c>viewList, Date StartDate, Date EndDate) {
        system.debug(' === view === ' + viewList);
        for(Forecast_View__c view : viewList) {
            system.debug(' === StartDate === ' + StartDate + ' === EndDate === ' + EndDate);
            view.Excepted_Close_Date_Start__c = StartDate.format();
            view.Excepted_Close_Date_End__c = EndDate.format();
        }
        system.debug(' === view === ' + viewList);
        return viewList;
    }
    
    /**
     * Puneet Mishra, 12 dec 2017, method retuns current fiscal year
     */
    @testvisible
    private static integer getCurrentFiscalYear() {
        Organization orgInfo = [SELECT FiscalYearStartMonth, UsesStartDateAsFiscalYearName 
                                FROM Organization 
                                WHERE Id=: UserInfo.getOrganizationId()];
        Date today = system.today();
        Integer currentFY;
        /*
        // current month greater than Fiscal start month
        if(today.month() > orgInfo.FiscalYearStartMonth) {
            // return true if Fiscal year begin in dec 2017 and fiscal year is FY2006
            if(orgInfo.UsesStartDateAsFiscalYearName) {
                currentFY = today.year();
            } else {// returns false if Fiscal year begin in oct 2017 and fiscal year is FY2018
                currentFY = today.year() + 1;
            }
        } else {
            // returns true if Fiscal year begin in feb 2017 and fiscal year is FY2018
            if(orgInfo.UsesStartDateAsFiscalYearName) {
                currentFY = today.year() - 1;
            } else {// returns false if Fiscal year begin in feb 2017 and fiscal year is FY2018
                currentFY = today.year();
            }
        }
        return currentFY;
        */
        String currentFiscalYear = [SELECT FiscalYearSettings.Name FROM Period WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY].FiscalYearSettings.Name;
		system.debug(' === todayMonth === ' + currentFiscalYear);
        return Integer.valueOf(currentFiscalYear);
    }
        
}
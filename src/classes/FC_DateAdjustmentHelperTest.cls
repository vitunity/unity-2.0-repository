/**
 *  @author         :       Puneet Mishra
 *  @createdDate    :       5 Jan 2018
 *  @description    :       Test Class for FC_DateAdjustmentHelper
 */
@isTest
public class FC_DateAdjustmentHelperTest {
	
    public static Forecast_View__c forecastViewData(Date sDate, Date eDate, String range) {
        Forecast_View__c newView = new Forecast_View__c();
        newView.Name = 'Test' + range;
        newView.Forecast_Columns__c = 'account,atype,city,closedate,accountowner,gqa,mprob,managerQuarterTotals,oppname,oppowner,optyref,ostage,qs,' +
            	   'closedWonNetBooking,pendingNetBooking,nbv,prebookno,prob,productCategories,repQuarterTotals,pquote,replacedBy,'+
                   'saleorder,state,strategic_account,subregion,iswon';
        newView.Excepted_Close_Date_Range__c = range;
        newView.Excepted_Close_Date_Start__c = sDate.format();
        newView.Excepted_Close_Date_End__c = eDate.format();
        newView.Product_Families__c = 'Professional Services,Software Solutions,System Solutions';
        newView.Quote_Type__c = 'Sales,Combined';
        newView.Region__c = 'Americas';
        newView.User__c = userInfo.getUserId();
        return newView;
    }
    
    public static testMethod void forecastViewsList_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'THIS_FISCAL_QUARTER'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'LAST_FISCAL_QUARTER'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'LAST_FISCAL_YEAR'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'NEXT_FISCAL_QUARTER'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curprev1'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext1'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curlast3'));
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
        
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        system.assert(!FC_Map.isEmpty());
        system.assertEquals(FC_Map.size(), 8);
        
    }
    
    public static testMethod void updateForecastViews_Test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'THIS_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'LAST_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'LAST_FISCAL_YEAR'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'NEXT_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curprev1'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curnext1'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curlast3'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curnext3'));
        insert viewList;
        
        List<String> dateLiterals = new List<String>{   'custom', 'THIS_FISCAL_QUARTER', 'LAST_FISCAL_QUARTER', 
                                                                                'fytd', 'LAST_FISCAL_YEAR', 'NEXT_FISCAL_QUARTER', 'curprev1', 
                                                                                'curnext1', 'curlast3', 'curnext3'
                                                                            };
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.testDate = system.today();
        for(String str : dateLiterals ) {
        	FC_DateAdjustmentHelper.updateForecastViews(str, viewList);
        }
	}
    
    public static testMethod void thisFiscalQuarter_Test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.thisFiscalQuarter(periodMap.get(1), viewList);
    }
    
    
    
    public static testMethod void lastFiscalQuarter_QUARTER_1_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.lastFiscalQuarter(1, periodMap, viewList);
    }
    
    public static testMethod void lastFiscalQuarter_QUARTER_3_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentQuarter = 3;
        FC_DateAdjustmentHelper.lastFiscalQuarter(3, periodMap, viewList);
    }
    
    
    public static testMethod void nextFiscalQuarter_QUARTER_4_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.nextFiscalQuarter(4, periodMap, viewList);
    }
    
    public static testMethod void nextFiscalQuarter_QUARTER_2_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentQuarter = 2;
        FC_DateAdjustmentHelper.nextFiscalQuarter(2, periodMap, viewList);
    }
    
    
    public static testMethod void lastFiscalYear_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.lastFiscalYear(2018, viewList);
    }
    
    public static testMethod void currentPre1_QUARTER_1_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentQuarter = 1;
        FC_DateAdjustmentHelper.currentNext1(1, periodMap, viewList);
    }
    
    public static testMethod void currentPre1_QUARTER_2_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentQuarter = 2;
        FC_DateAdjustmentHelper.currentNext1(2, periodMap, viewList);
    }
    
    public static testMethod void currentNext1_QUARTER4_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentNext1(4, periodMap, viewList);
    }
    
    public static testMethod void currentNext1_QUARTER2_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentNext1(2, periodMap, viewList);
    }
    
    public static testMethod void currentLast3_QUARTER_4_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentLast3(4, periodMap, viewList);
    }
    
    public static testMethod void currentLast3_LESSER_4_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentLast3(3, periodMap, viewList);
    }
    
    public static testMethod void currentNext3_Quarter_2_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentNext3(2, periodMap, viewList);
        
    }
    
    public static testMethod void currentNext3_Quarter_LESSER_2_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'curnext3'));
        insert viewList;
    	
        Map<String, List<Forecast_View__c>> FC_Map = FC_DateAdjustmentHelper.forecastViewsList(viewList);
        
        FiscalYearSettings fiscalYr = [	SELECT Id,Name,StartDate,EndDate FROM FiscalYearSettings 
                                        WHERE Name =: '2018' 
                                        ORDER BY Name DESC NULLS LAST LIMIT 1];
        
        Map<Integer, Period> periodMap = new Map<Integer, Period>();
        for(Period per : [  SELECT Id, FiscalYearSettingsId, StartDate, EndDate, Number, Type 
                            FROM period 
                            WHERE FiscalYearSettingsId =: fiscalYr.Id AND Type =: 'Quarter']) {
			periodMap.put(per.Number, per);
		}
        
        FC_DateAdjustmentHelper.currentYear = '2018';
        FC_DateAdjustmentHelper.currentNext3(1, periodMap, viewList);
        
    }
    
    public static testMethod void fetchQuarter_Test() {
        FC_DateAdjustmentHelper.Q1_StartDate = Date.newInstance(2017, 10, 1);
        FC_DateAdjustmentHelper.Q1_EndDate = Date.newInstance(2017, 12, 30);
        FC_DateAdjustmentHelper.Q2_StartDate = Date.newInstance(2018, 1, 31);
        FC_DateAdjustmentHelper.Q2_EndDate = Date.newInstance(2018, 3, 31);
        FC_DateAdjustmentHelper.Q3_StartDate = Date.newInstance(2018, 4, 1);
        FC_DateAdjustmentHelper.Q3_EndDate = Date.newInstance(2018, 6, 30);
        FC_DateAdjustmentHelper.Q4_StartDate = Date.newInstance(2018, 7, 1);
        FC_DateAdjustmentHelper.Q4_EndDate = Date.newInstance(2018, 9, 29);
            
        FC_DateAdjustmentHelper.testDate = Date.newInstance(2018, 2, 5);
        Integer quarter = FC_DateAdjustmentHelper.fetchQuarter();
        
        FC_DateAdjustmentHelper.testDate = Date.newInstance(2017, 11, 5);
        FC_DateAdjustmentHelper.fetchQuarter();
        
        FC_DateAdjustmentHelper.testDate = Date.newInstance(2018, 5, 5);
        FC_DateAdjustmentHelper.fetchQuarter();
        
        FC_DateAdjustmentHelper.testDate = Date.newInstance(2018, 8, 5);
        FC_DateAdjustmentHelper.fetchQuarter();
        
        
    }
    
    
    
    public static testMethod void reassignDates_Test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(system.today().addDays(-100), system.today().addDays(-50), 'THIS_FISCAL_QUARTER'));
        List<Forecast_View__c> actual = FC_DateAdjustmentHelper.reassignDates(viewList, system.today(), system.today().addDays(1));
    	system.debug(' ===== actual ===== ' + actual);
        system.assertEquals(system.today().format(), actual[0].Excepted_Close_Date_Start__c);
        system.assertEquals(system.today().addDays(1).format(), actual[0].Excepted_Close_Date_End__c);
    }
    
    public static testMethod void getCurrentFiscalYear_test() {
        Organization orgInfo = [SELECT FiscalYearStartMonth, UsesStartDateAsFiscalYearName 
                                FROM Organization 
                                WHERE Id=: UserInfo.getOrganizationId()];
        system.debug(' ===== orgInfo ====== ' + orgInfo);
        Integer fyYear = FC_DateAdjustmentHelper.getCurrentFiscalYear();
    }
}
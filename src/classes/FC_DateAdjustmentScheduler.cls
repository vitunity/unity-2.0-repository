/**
 *  @author         :       Puneet Mishra
 *  @createdDate    :       8 December 2017
 *  @description    :       Scheduler class will run for forecast Date Range excluding fytd and custom
 */
public class FC_DateAdjustmentScheduler implements Schedulable {
    
    @testvisible private List<String> dateLiterals = new List<String>{  'THIS_FISCAL_QUARTER', 'LAST_FISCAL_QUARTER', 
                                                                        'LAST_FISCAL_YEAR', 'NEXT_FISCAL_QUARTER', 'curprev1', 
                                                                        'curnext1', 'curlast3', 'curnext3'
     };
    
    public void execute(SchedulableContext sc) {
    FC_DateAdjustmentBatch b = new FC_DateAdjustmentBatch('SELECT Id, Name, Deal_Probability__c, Deal_Probability_Operator__c, Excepted_Close_Date_End__c, Excepted_Close_Date_Range__c,'+
                   ' Excepted_Close_Date_Start__c, Region__c, Sub_Region__c, User__c, Stage__c, Quote_Type__c'+
                   ' FROM Forecast_View__c '+
                   ' WHERE Excepted_Close_Date_Range__c IN: dateLiterals '); 
      database.executebatch(b, 100);
   }
}
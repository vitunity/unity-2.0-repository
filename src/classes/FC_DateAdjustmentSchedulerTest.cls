/**
 *  @author         :       Puneet Mishra
 *  @createdDate    :       5 Jan 2018
 *  @description    :       Test Class for FC_DateAdjustmentHelper
 */
@isTest
public class FC_DateAdjustmentSchedulerTest {
	
    @testvisible private List<String> dateLiterals = new List<String>{  'THIS_FISCAL_QUARTER', 'LAST_FISCAL_QUARTER', 
                                                                        'LAST_FISCAL_YEAR', 'NEXT_FISCAL_QUARTER', 'curprev1', 
                                                                        'curnext1', 'curlast3', 'curnext3'
     };
    
    public static Forecast_View__c forecastViewData(Date sDate, Date eDate, String range) {
        Forecast_View__c newView = new Forecast_View__c();
        newView.Name = 'Test' + range;
        newView.Forecast_Columns__c = 'account,atype,city,closedate,accountowner,gqa,mprob,managerQuarterTotals,oppname,oppowner,optyref,ostage,qs,' +
            	   'closedWonNetBooking,pendingNetBooking,nbv,prebookno,prob,productCategories,repQuarterTotals,pquote,replacedBy,'+
                   'saleorder,state,strategic_account,subregion,iswon';
        newView.Excepted_Close_Date_Range__c = range;
        newView.Excepted_Close_Date_Start__c = sDate.format();
        newView.Excepted_Close_Date_End__c = eDate.format();
        newView.Product_Families__c = 'Professional Services,Software Solutions,System Solutions';
        newView.Quote_Type__c = 'Sales,Combined';
        newView.Region__c = 'Americas';
        newView.User__c = userInfo.getUserId();
        return newView;
    }
    
    public static testMethod void Scheduler_test() {
        list<Forecast_View__c> viewList = new LIst<Forecast_View__c>();
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'THIS_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'LAST_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'LAST_FISCAL_YEAR'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'NEXT_FISCAL_QUARTER'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curprev1'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curnext1'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curlast3'));
        viewList.add(forecastViewData(Date.newInstance(2017, 10, 1), Date.newInstance(2017, 10, 30), 'curnext3'));
        insert viewList;
        
        Test.startTest();
		String sch = '0  00 1 3 * ?';
        FC_DateAdjustmentHelper.testDate = system.today();
        String jobId = System.schedule('testBasicScheduledApex', sch, new FC_DateAdjustmentScheduler());

        
        Test.stopTest();
    }
}
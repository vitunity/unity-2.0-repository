@isTest
private class FC_FRDataControllerProtonsTest {

    static testMethod void forecastReportControllerTest() {
        
        Territory_Team__c tt = new Territory_Team__c();
        tt.District__c = 'SW5';
        tt.Zip__c = '11111';
        tt.Region__c = 'Southwest';
        tt.Territory__c = 'North America';
        tt.Sub_Region__c = 'Southwest';
        tt.Country__c = 'USA';
        tt.Super_Territory__c = 'Americas';
        insert tt;
        
        //Setup test data
        Account acct = TestUtils.getAccount();
        acct.Name = 'FORECAST REPORT ACCOUNT1';
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        acct.Super_Territory1__c = 'Americas';
        acct.Territories1__c = 'North America';
        
        Account acct1 = TestUtils.getAccount();
        acct1.Name = 'FORECAST REPORT ACCOUNT2';
        acct1.AccountNumber = 'Service121245';
        acct1.CurrencyISOCode = 'USD';
        acct1.BillingStreet = 'TEST';
        acct1.BillingCity = 'TEST';
        acct1.BillingState = 'TEST';
        acct1.BillingPostalCode = '11111';
        acct1.BillingCountry = 'Australia';
        acct1.Sub_Region__c = 'NSW';
        acct1.Region1__c = 'ANZ';
        insert new List<Account>{acct,acct1};
        
        System.debug('-----acct'+[SELECT Id,AccountNumber,Super_Territory1__c,Territories1__c,Sub_Region__c,Region1__c FROM Account]);
        
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        list<Opportunity> oppList = new list<Opportunity>();
        list<BigMachines__Quote__c> bigMachineQuotesList = new list<BigMachines__Quote__c>();
        
        for(Integer i=0; i<=12; i++){ 
            Opportunity opp = TestUtils.getOpportunity();
            if(Math.mod(i, 2) == 0){
                opp.AccountId = acct.Id;
            }else{
                opp.AccountId = acct1.Id;
            }
            opp.StageName = '0 - NEW LEAD';
            opp.Primary_Contact_Name__c = con.Id;
            opp.MGR_Forecast_Percentage__c = '';
            opp.Unified_Funding_Status__c = '20%';
            opp.Unified_Probability__c = '20%';
            opp.Net_Booking_Value__c = 200;
            opp.CloseDate = Date.newInstance(2016,3,3);
            opp.Opportunity_Type__c = 'Sales';
            oppList.add(opp);
        }   
        
        for(Integer i=0; i<=12; i++){ 
            Opportunity opp = TestUtils.getOpportunity();
            if(Math.mod(i, 2) == 0){
                opp.AccountId = acct.Id;
            }else{
                opp.AccountId = acct1.Id;
            }
            opp.StageName = '0 - NEW LEAD';
            opp.Primary_Contact_Name__c = con.Id;
            opp.MGR_Forecast_Percentage__c = '';
            opp.Unified_Funding_Status__c = '20%';
            opp.Unified_Probability__c = '20%';
            opp.Net_Booking_Value__c = 200;
            opp.CloseDate = Date.today();
            opp.Opportunity_Type__c = 'Sales';
            oppList.add(opp);
        }   
        insert oppList;
        
        for(Opportunity opp: oppList){ 
            BigMachines__Quote__c bq = new BigMachines__Quote__c();
            bq.Name = '2016-15108'+opp.Id;
            bq.BigMachines__Account__c = acct.Id;
            bq.BigMachines__Opportunity__c = opp.Id;
            bq.BigMachines__Is_Primary__c = true;
            bq.Order_Type__c = 'Sales';
            bigMachineQuotesList.add(bq);
        } 
        
        insert bigMachineQuotesList;
        
        insertQuoteProducts(bigMachineQuotesList);
        
        System.debug('--oppList'+[
            SELECT Id, Name, StageName, Primary_Contact_Name__c, MGR_Forecast_Percentage__c, Unified_Funding_Status__c,
            Unified_Probability__c, Net_Booking_Value__c, CloseDate, Opportunity_Type__c
            FROM Opportunity
        ]);
        
        System.Test.startTest();
        
        String url1 = '/apex/FC_ForecastReportData?sfdcRefNumber=start&sparam1=curlast3&probabilityOperator=--NONE--'+
        '&probability=&dealProbability=&familyOptions=TESTFAMILY1,TESTFAMILY2'+
        '&lineOptions=TESTFAMILY1TESTLINE1,TESTFAMILY1TESTLINE2&region=Americas'+
        '&selectedViewColumns=account,atype,city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,'+
        'nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,productCategories,repQuarterTotals,pquote,replacedBy,'+
        'saleorder,state,strategic_account,subregion,iswon&sdate=03/02/2016&edate=03/31/2017&snba=1&enba=100000000&pqt=Sales';
        
        Pagereference p1 = new Pagereference(url1);
        System.Test.setCurrentPage(p1);
        FC_ForecastReportDataControllerProtons obj1 = new FC_ForecastReportDataControllerProtons();
        obj1 = null;
        
        String url2 = '/apex/FC_ForecastReportData?sparam1=curlast3&probabilityOperator=--NONE--'+
        '&probability=&dealProbability=&familyOptions=Software Solutions,System Solutions'+
        '&lineOptions=Software SolutionsTPS (Eclipse),System SolutionsAdvanced Treatment Delivery&region=Americas'+
        '&selectedViewColumns=account,atype,city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,'+
        'nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,productCategories,repQuarterTotals,pquote,replacedBy,'+
        'saleorder,state,strategic_account,subregion,iswon&pqt=Service';
        
        Pagereference p2 = new Pagereference(url2);
        System.Test.setCurrentPage(p2);
        FC_ForecastReportDataControllerProtons obj2 = new FC_ForecastReportDataControllerProtons();
        obj2 = null;
        
        String url3 = '/apex/FC_ForecastReportData?lastCall=true&sparam1=THIS_FISCAL_QUARTER&stageOptions=0 - NEW LEAD&probabilityOperator=e&probability=20'+
        '&dealProbability=&familyOptions=Software Solutions,System Solutions&lineOptions=Software SolutionsTPS (Eclipse),'+
        'System SolutionsAdvanced Treatment Delivery&modelOptions=TrueBeam&region=Americas&selectedViewColumns=account,atype,'+
        'city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,'+
        'productCategories,repQuarterTotals,pquote,replacedBy,saleorder,state,strategic_account,subregion,iswon&sr=North America&pqt=Combined';
        
        Pagereference p3 = new Pagereference(url3);
        System.Test.setCurrentPage(p3);
        FC_ForecastReportDataControllerProtons obj3 = new FC_ForecastReportDataControllerProtons();
        obj3 = null;
        
        String url4 = '/apex/FC_ForecastReportData?sparam1=NEXT_FISCAL_QUARTER&stageOptions=0 - NEW LEAD&probabilityOperator=e&probability=0'+
        '&dealProbability=&familyOptions=Software Solutions,System Solutions&lineOptions=Software SolutionsTPS (Eclipse),'+
        'System SolutionsAdvanced Treatment Delivery&modelOptions=TrueBeam&region=Americas&selectedViewColumns=account,atype,'+
        'city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,'+
        'productCategories,repQuarterTotals,pquote,replacedBy,saleorder,state,strategic_account,subregion,iswon&pqt=All';
        
        Pagereference p4 = new Pagereference(url4);
        System.Test.setCurrentPage(p4);
        FC_ForecastReportDataControllerProtons obj4 = new FC_ForecastReportDataControllerProtons();
        obj4 = null;
        
        String url5 = '/apex/FC_ForecastReportData?sparam1=LAST_FISCAL_QUARTER&stageOptions=0 - NEW LEAD&probabilityOperator=e&probability=0'+
        '&dealProbability=&familyOptions=Software Solutions,System Solutions&lineOptions=Software SolutionsTPS (Eclipse),'+
        'System SolutionsAdvanced Treatment Delivery&modelOptions=TrueBeam&region=Americas&selectedViewColumns=account,atype,'+
        'city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,'+
        'productCategories,repQuarterTotals,pquote,replacedBy,saleorder,state,strategic_account,subregion,iswon&sr=North America';
        
        Pagereference p5 = new Pagereference(url5);
        System.Test.setCurrentPage(p5);
        FC_ForecastReportDataControllerProtons obj5 = new FC_ForecastReportDataControllerProtons();
        obj5 = null;
       
        String url6 = '/apex/FC_ForecastReportData?sparam1=THIS_FISCAL_QUARTER&stageOptions=0 - NEW LEAD&probabilityOperator=e&probability=0'+
        '&dealProbability=20&dealProbabilityOperator=e&familyOptions=TESTFAMILY1,TESTFAMILY2&lineOptions=TESTFAMILY1TESTLINE1,'+
        'TESTFAMILY1TESTLINE2&modelOptions=TESTMODEL1,TESTMODEL4,TESTMODEL5&region=APAC&selectedViewColumns=account,atype,'+
        'city,closedate,accountowner,dprob,gqa,mprob,managerQuarterTotals,nbv,oppname,oppowner,optyref,ostage,qs,prebookno,prob,'+
        'productCategories,repQuarterTotals,pquote,replacedBy,saleorder,state,strategic_account,subregion,iswon&sr=ANZ';
        
        Pagereference p6 = new Pagereference(url6);
        System.Test.setCurrentPage(p6);
        FC_ForecastReportDataControllerProtons obj6 = new FC_ForecastReportDataControllerProtons();
        obj6 = null;
        
        System.Test.stoptest();
    }
    
    private static void insertQuoteProducts(List<BigMachines__Quote__c> quotes){
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        
        List<Catalog_Model__c> catalogModels = [
            SELECT Id,Family__c,Line__c,Model__c
            FROM Catalog_Model__c
            WHERE Family__c LIKE 'TESTFAMILY%'
        ];
        List<BigMachines__Quote_Product__c> quoteProducts = new List<BigMachines__Quote_Product__c>();
        for(BigMachines__Quote__c bmQuote : quotes){
            for(Catalog_Model__c catalogModel:catalogModels){
                quoteProducts.add(
                    new BigMachines__Quote_Product__c(
                        BigMachines__Quote__c = bmQuote.Id,
                        BigMachines__Product__c = varianProduct.Id,
                        Header__c = true,
                        Product_Type__c = 'Sales',
                        BigMachines__Sales_Price__c = 40000,
                        BigMachines__Quantity__c = 1,
                        Line_Number__c= 12,
                        EPOT_Section_Id__c = 'TEST',
                        Net_Booking_Val__c = 1000,
                        QP_Product_Family__c = catalogModel.Family__c,
                        QP_Product_Line__c = catalogModel.Line__c,
                        QP_Product_Model__c = catalogModel.Line__c,
                        Product_Line_Key__c = catalogModel.Family__c+''+catalogModel.Line__c,
                        Forecast_Booking_Value__c = 1000
                    )
                );
            }
        }
        
        insert quoteProducts;
    }
    
    @testSetup static void insertCatalogModels(){
        insert new List<Catalog_Model__c>{
            new Catalog_Model__c(
                Family__c = 'TESTFAMILY1',
                Line__c = 'TESTLINE1',
                Model__c = 'TESTMODEL1',
                isActive__c = true,
                Show_On_Forecast_Report__c = true
            ),
            new Catalog_Model__c(
                Family__c = 'TESTFAMILY1',
                Line__c = 'TESTLINE1',
                Model__c = 'TESTMODEL2',
                isActive__c = true,
                Show_On_Forecast_Report__c = true
            ),
            new Catalog_Model__c(
                Family__c = 'TESTFAMILY1',
                Line__c = 'TESTLINE2',
                Model__c = 'TESTMODEL3',
                isActive__c = true,
                Show_On_Forecast_Report__c = true
            ),
            new Catalog_Model__c(
                Family__c = 'TESTFAMILY1',
                Line__c = 'TESTLINE2',
                Model__c = 'TESTMODEL4',
                isActive__c = true,
                Show_On_Forecast_Report__c = true
            ),
            new Catalog_Model__c(
                Family__c = 'TESTFAMILY2',
                Line__c = 'TESTLINE3',
                Model__c = 'TESTMODEL5',
                isActive__c = true,
                Show_On_Forecast_Report__c = true
            )
        };
        
        insert new List<Forecast_Column__c>{
            new Forecast_Column__c(Display_Name__c = 'Product Categories',Name = 'productCategories'),
            new Forecast_Column__c(Display_Name__c = 'prob',Name = 'Probability'),
            new Forecast_Column__c(Display_Name__c = 'mprob',Name = 'ManagerProbability'),
            new Forecast_Column__c(Display_Name__c = 'Quarters',Name = 'repQuarterTotals'),
            new Forecast_Column__c(Display_Name__c = 'M Quarters',Name = 'managerQuarterTotals'),
            new Forecast_Column__c(Display_Name__c = 'Quote#',Name = 'pquote'),
            new Forecast_Column__c(Display_Name__c = 'Close Date',Name = 'closeDate')
        };
    }
}
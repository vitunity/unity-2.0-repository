/**
 * @author krishna katve
 * @description this class used to calculate product categories diffenece between two consecutive amendment quotes and store values in latest amendment
 */
public with sharing class FC_ForecastAmendments implements Queueable {
    
    List<BigMachines__Quote__c> bmiQuotes;
    Map<String, String> amendmentQuoteParents;
    Set<String> quoteNumbers;
    Map<String, Decimal> managerForecastPercentage;
    Map<String, Map<String,Decimal>> categoryTotals;
    
    public FC_ForecastAmendments(List<BigMachines__Quote__c> bmiQuotes){
        this.bmiQuotes = bmiQuotes;
        amendmentQuoteParents = new Map<String, String>();
        managerForecastPercentage = new Map<String, Decimal>();
        quoteNumbers = new Set<String>();
    }
    
    public void execute(QueueableContext context){
        
        List<BigMachines__Quote__c> amendmentQuotes = [
            SELECT Id, Name, BigMachines__Opportunity__r.MGR_Forecast_Percentage__c, Parent_Quote__c, BMI_Region__c,
            BigMachines__Opportunity__r.Probability
            FROM BigMachines__Quote__c
            WHERE Id IN:bmiQuotes
            AND BigMachines__Status__c = 'Approved Amendment'
        ];
        
        for(BigMachines__Quote__c bmiQuote : amendmentQuotes){
            quoteNumbers.add(bmiQuote.Name);
            quoteNumbers.add(bmiQuote.Parent_Quote__c);
            if(!String.isBlank(bmiQuote.Parent_Quote__c)){
                amendmentQuoteParents.put(bmiQuote.Name, bmiQuote.Parent_Quote__c);
            }
            if(bmiQuote.BigMachines__Opportunity__r.MGR_Forecast_Percentage__c != null) {
                String tempProb = bmiQuote.BigMachines__Opportunity__r.MGR_Forecast_Percentage__c.replace('%', '');
                Decimal tempProbIngt = Decimal.valueof(tempProb);
                managerForecastPercentage.put(
                    bmiQuote.Name,
                    ((bmiQuote.BMI_Region__c!='NA' && bmiQuote.BMI_Region__c!='LAT')?bmiQuote.BigMachines__Opportunity__r.Probability:tempProbIngt)
                );
            }
        }
    }
    
    private void calculateAmendmentCategoryTotals(){
        
        Set<String> bmiQuoteNumbers = quoteNumbers;
        
        FC_ProductCategoryController fcpcController = new FC_ProductCategoryController();
        Map<String,String> modelVariations = fcpcController.modelVariationsMap;
        String qpQuery = 'SELECT BigMachines__Quote__r.Name quoteName,QP_Product_Family__c,Product_Line_Key__c,QP_Product_Model__c,'+
            ' SUM(Forecast_Booking_Value__c) fcBookingValue '+
            ' FROM BigMachines__Quote_Product__c '+
            ' WHERE Quote_Number__c IN:bmiQuoteNumbers AND Forecast_Blocked_Part__c = false AND QP_Product_Family__c !=\'\' AND Product_Line_Key__c !=\'\' AND QP_Product_Model__c !=\'\''+
            ' Group By BigMachines__Quote__r.Name,QP_Product_Family__c,Product_Line_Key__c,QP_Product_Model__c';
        
        System.debug(LoggingLevel.INFO,'------qpQuery'+qpQuery);
        
        List<AggregateResult> quoteProductAggregate = Database.query(qpQuery);
        categoryTotals = FC_ForecastReportDataController.getCatagoryTotalsMap(quoteProductAggregate, managerForecastPercentage, new Set<String>(), modelVariations);
    }
    
    private void calculateCategoryDifference(){
        for(String quoteNumber : categoryTotals.keySet()){
            if(amendmentQuoteParents.containsKey(quoteNumber)){
                Map<String, Decimal> oldAmendmentData = categoryTotals.get(amendmentQuoteParents.get(quoteNumber));
                Map<String, Decimal> newAmendmentData = categoryTotals.get(quoteNumber);
                for(String categoryName : newAmendmentData.keySet()){
                    Decimal oldTotal = 0.0;
                    if(oldAmendmentData.containsKey(categoryName)){
                        oldTotal = oldAmendmentData.get(categoryName);
                    }
                    Decimal newTotal = newAmendmentData.get(categoryName) - oldTotal;
                    newAmendmentData.put(categoryName, newTotal);
                }
            }
        }
    }
    
    private static List<BigMachines__Quote__c> updateCategoriesJSON(Map<String, Map<String,Decimal>> catagoryTotals){
        
        return new List<BigMachines__Quote__c>();
    }
}
/*
 * @author: Amitkumar Katre
 * @description: Opportunity forecast report controller
 * @createddate: 05/10/2016
 */ 
global class FC_OpportunityForecastController {
    
    /* Custom Exception Class */
    public class ReportException extends Exception {} 
    public User u {get;set;}
    public static boolean isManager;
    
    /* Manager permissiom check */
    static{
        Schema.DescribeFieldResult drField = Opportunity.MGR_Forecast_Percentage__c.getDescribe();
        isManager = drField.isCreateable();
        if(System.Test.isRunningTest()){
            isManager = true;
        }
    }
    
    /* Contructor */
    public FC_OpportunityForecastController(){
       u = [select FC_Last_Filter__c, APAC_Forecast_Filters__c, EMEA_Forecast_Filters__c from user  where id =: Userinfo.getUserId()];//get current user.
    }
    
    /* Save last serach filter */
    @RemoteAction
    global static void  saveLastSearchFilter(String urlS) { 
        system.debug('urlS==='+urlS);
        String [] params = urlS.split('\\?',-2);
        User u = new User(id = Userinfo.getUserId(), FC_Last_Filter__c =params[1]);
        update u;
    }
    
    /* Save data */
    @RemoteAction
    global static void  saveData(String josndata) { 
        map<Id,Opportunity> oppList = new map<Id,Opportunity>();
        list<FC_ReportDataController.ReportData> listData = (List<FC_ReportDataController.ReportData>)System.JSON.deserialize(josndata,
        List<FC_ReportDataController.ReportData>.class);
        system.debug('listData==================='+listData); 
        for(FC_ReportDataController.ReportData rd : listData) {
            Date myDate= GetStringToDateFormat(rd.closedate);
            if(isManager){
                oppList.put(rd.opid,new Opportunity(Id=rd.opid,Unified_Funding_Status__c=rd.prob,MGR_Forecast_Percentage__c = rd.mprob,closedate=myDate));
            }else{
                oppList.put(rd.opid,new Opportunity(Id=rd.opid,Unified_Funding_Status__c=rd.prob,closedate=myDate));
            }
        } 
        if(!oppList.isEmpty()){
            list<String> oppNames = new list<String>();
            List<Database.SaveResult> updateResults = Database.update(oppList.values(), false);
            for(Integer i=0;i<updateResults.size();i++){
                if (updateResults.get(i).isSuccess()){
                    updateResults.get(i).getId();
                } else if (!updateResults.get(i).isSuccess()){
                    // DML operation failed
                    //Database.Error error = updateResults.get(i).getErrors().get(0);
                    //String failedDML = error.getMessage();
                    //oppList.get(i);//failed record from the list
                    //System.debug('The following error has occurred.');                    
                    //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    //System.debug('Fields that affected this error: ' + err.getFields());
                    //system.debug('Failed ID'+arudate.get(i).Id);
                    oppNames.add(listData.get(i).optyref);
                }
                if(oppNames.isEMpty()){
                }else{
                    throw new ReportException('Error while saving records : '+String.join(oppNames,','));
                }
            }
        }
        
    }
    
    public Boolean getShowAmendments(){
        List<GroupMember> forecastAmendmentMembers = [SELECT Id FROM GroupMember WHERE UserOrGroupId=:UserInfo.getUserId() AND Group.DeveloperName = 'Forecast_Amendments'];
        return !forecastAmendmentMembers.isEmpty();
    }
    
    /* Convert string to date */
    public static Date GetStringToDateFormat(String myDate) {
       String[] myDateOnly = myDate.split(' ');
       String[] strDate = myDateOnly[0].split('-');
       Integer myIntDate = integer.valueOf(strDate[2]);
       Integer myIntMonth = integer.valueOf(strDate[1]);
       Integer myIntYear = integer.valueOf(strDate[0]);
       Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
       return d;
    }
}
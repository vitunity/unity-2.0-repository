/*
 * @author: Amitkumar Katre
 * @description: Opportunity forecast report controller test class
 * @createddate: 05/10/2016
 */
@isTest
public class FC_OpportunityForecastControllerTest {
    
    public static testmethod void forecasReportTest1(){
        //Setup test data
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        opp.MGR_Forecast_Percentage__c = '40%';
        opp.Unified_Funding_Status__c = '20%';
        opp.CloseDate = System.today();
        insert opp;
        
        FC_ReportDataController.ReportData rd = new FC_ReportDataController.ReportData();
        rd.opid = opp.Id;
        rd.closedate = FC_ReportDataController.GetDateToString(opp.CloseDate);
        rd.prob = opp.Unified_Funding_Status__c;
        rd.mprob = opp.MGR_Forecast_Percentage__c;
        
        System.Test.startTest();
        FC_OpportunityForecastController obje = new FC_OpportunityForecastController();
        FC_OpportunityForecastController.saveLastSearchFilter('https://varian--sfdev.cs19.my.salesforce.com?param1=test&pram2=rfee');
        FC_OpportunityForecastController.GetStringToDateFormat('2013-02-23');
        FC_OpportunityForecastController.saveData(JSON.serialize(new list<FC_ReportDataController.ReportData> {rd}));
        System.Test.stoptest();
    }
	
}
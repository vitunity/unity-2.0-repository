/*
 * @author: Krishna katve
 * @description: Opportunity forecast report by product categories 
 * @createddate: 12/07/2016
 */ 

global with sharing class FC_ProductCategoryControllerCopy {
    /* Custom Exception Class */
    public class ReportException extends Exception {} 
    public User u {get;set;}
    public static boolean isManager;
    
    public Map<String,Map<String,Set<String>>> productCategoryMap{get;set;}
    public Map<String,String> modelVariationsMap;
    public Map<String,List<String>> catalogModelWithVariations;
    public Static Map<String, List<String>> quarterlyManualAdjustments;
    
    //public List<String> selectedFamilies{get;set;}
    //public List<String> selectedModels{get;set;}
    public String selectedForecastView{get;set;}
    
    /* Manager permissiom check */
    static{
        Schema.DescribeFieldResult drField = Opportunity.MGR_Forecast_Percentage__c.getDescribe();
        isManager = drField.isCreateable();
        if(System.Test.isRunningTest()){
            isManager = true;
        }
    }
    
    /* Contructor */
    public FC_ProductCategoryControllerCopy(){
       u = [SELECT FC_Last_Filter__c, APAC_Forecast_Filters__c, EMEA_Forecast_Filters__c FROM User  WHERE id =: Userinfo.getUserId()];//get current user.
       initializeProductCategories();
    }
    
    /* Save last serach filter */
    @RemoteAction
    global static void saveLastSearchFilter(String urlS) { 
        system.debug('urlS==='+urlS);
        String [] params = urlS.split('\\?',-2);
        
        PageReference pageRef = new PageReference(urlS);
        String region = pageRef.getParameters().get('region');
        
        User u = new User(id = Userinfo.getUserId());
        if(region == 'Americas'){
            u.FC_Last_Filter__c = params[1];
        }else if(region == 'APAC'){
            u.APAC_Forecast_Filters__c = params[1];
        }else if(region == 'EMEA'){
            u.EMEA_Forecast_Filters__c = params[1];
        }
        update u;
    }
    
    /**
     * Save report view into forecast filter object
     */
    @RemoteAction
    global static Id updateForecastFilters(String urlS){
        system.debug('urlS==='+urlS);
        String [] params = urlS.split('\\?',-2);
        PageReference page = new PageReference(urlS);
        Map<String,String> pageParameters = page.getParameters();
        Forecast_View__c forecastFilter = new Forecast_View__c();
        if(!String.isBlank(pageParameters.get('forecastViewId'))){
            forecastFilter.Id = pageParameters.get('forecastViewId');
        }
        forecastFilter.Name = pageParameters.get('viewName');
        forecastFilter.Quote_Type__c = pageParameters.get('pqt');
        forecastFilter.Default_View__c = Boolean.valueOf(pageParameters.get('defaultView'));
        forecastFilter.Excepted_Close_Date_Start__c = pageParameters.get('sdate');
        forecastFilter.Excepted_Close_Date_End__c = pageParameters.get('edate');
        forecastFilter.Excepted_Close_Date_Range__c = pageParameters.get('sparam1');
        if(pageParameters.get('snba')!=null){
            forecastFilter.Net_Booking_Amount_Start__c = Decimal.valueOf(pageParameters.get('snba'));
        }
        if(pageParameters.get('enba')!=null){
            forecastFilter.Net_Booking_Amount_End__c = Decimal.valueOf(pageParameters.get('enba'));
        }
        forecastFilter.Product_Families__c = pageParameters.get('familyOptions');
        forecastFilter.Product_Lines__c = pageParameters.get('lineOptions');
        forecastFilter.Product_Models__c = pageParameters.get('modelOptions');
        forecastFilter.Sub_Region__c = pageParameters.get('sr');
        forecastFilter.User__c = Userinfo.getUserId();
        forecastFilter.Region__c = pageParameters.get('region');
        forecastFilter.Forecast_Columns__c = pageParameters.get('selectedViewColumns');
        forecastFilter.View_Key__c = forecastFilter.User__c+'-'+forecastFilter.Name;
        forecastFilter.Stage__c = pageParameters.get('stageOptions');
        forecastFilter.Probability_Operator__c = pageParameters.get('probabilityOperator');
        forecastFilter.Probability__c = pageParameters.get('probability');
        forecastFilter.Deal_Probability_Operator__c = pageParameters.get('dealProbabilityOperator');
        forecastFilter.Deal_Probability__c = pageParameters.get('dealProbability');
        upsert forecastFilter;
        
        if(forecastFilter.Default_View__c){
            List<Forecast_View__c> forecastViews = new List<Forecast_View__c>();
            for(Forecast_View__c fView:[
                SELECT Id,Default_View__c 
                FROM Forecast_View__c 
                WHERE User__c =:Userinfo.getUserId() 
                AND Region__c =:pageParameters.get('region')
                AND Id !=:forecastFilter.Id
                AND Default_View__c = true
            ]){
                forecastViews.add(new Forecast_View__c(Id = fView.Id, Default_View__c = false));
            }
            if(!forecastViews.isEmpty()) update forecastViews;
        }
        return forecastFilter.Id;
    }
    
    /**
     * Save report view into forecast filter object
     */
    @RemoteAction
    global static Boolean deleteForecastView(String viewId, String region){
        List<Forecast_View__c> forecastView = [Select Id FROM Forecast_View__c WHERE User__c=:UserInfo.getUserId() AND Region__c =:region];
        if(forecastView.size()>1){
            delete new Forecast_View__c(Id=viewId);
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Get start and end date using date rage option
     */
    @RemoteAction
    global static String getExpectedDateRange(String closeDateLiteral){
        
        String periodQuery = getPeriodQuery(closeDateLiteral);
        System.debug(LoggingLevel.INFO+'----periodQuery'+periodQuery);
        List<Period> periodList = Database.query(periodQuery);
        if(!periodList.isEmpty()){
            Date startDate = periodList[0].StartDate;
            Date endDate = closeDateLiteral!='fytd'?periodList[periodList.size()-1].EndDate:Date.Today();
            System.debug(LoggingLevel.INFO+'----startDate'+startDate+'----endDate'+endDate);
            return (startDate.month()>=10?''+startDate.month():'0'+startDate.month())+'/'+(startDate.day()>=10?''+startDate.day():'0'+startDate.day())+'/'+startDate.year()+''
            +'-'+(endDate.month()>=10?''+endDate.month():'0'+endDate.month())+'/'+(endDate.day()>=10?''+endDate.day():'0'+endDate.day())+'/'+endDate.year();
        }
        return '';
    }
    
    /**
     * FIlter for period query
     */
    private static String getPeriodQuery(String closeDateLiteral){
        
        String periodQuery = 'SELECT StartDate, Number,Type, EndDate, FiscalYearSettings.Name FROM Period ';
        
        periodQuery += ' WHERE Type = \'quarter\' ';
        
        if (closeDateLiteral == 'LAST_FISCAL_YEAR') {
             periodQuery += ' AND ( startdate = ' + closeDateLiteral + ') ';
        } else if (closeDateLiteral == 'THIS_FISCAL_QUARTER') {
             periodQuery += ' AND ( startdate = ' + closeDateLiteral + ') ';
        } else if (closeDateLiteral == 'NEXT_FISCAL_QUARTER') {
             periodQuery += ' AND ( startdate = ' + closeDateLiteral + ') ';
        } else if (closeDateLiteral == 'LAST_FISCAL_QUARTER') {
             periodQuery += ' AND ( startdate = ' + closeDateLiteral + ') ';
        } else if (closeDateLiteral == 'curnext1') {
             periodQuery += ' AND ( startdate = THIS_FISCAL_QUARTER OR startdate = NEXT_FISCAL_QUARTER)  ';
        } else if (closeDateLiteral == 'curprev1') {
             periodQuery += ' AND (startdate = THIS_FISCAL_QUARTER OR startdate = LAST_FISCAL_QUARTER) ';
        } else if (closeDateLiteral == 'curnext3') {
             periodQuery += ' AND (startdate = THIS_FISCAL_QUARTER OR startdate = NEXT_N_FISCAL_QUARTERS:3) ';
        }else if (closeDateLiteral == 'curlast3') {
             periodQuery += ' AND (startdate = THIS_FISCAL_QUARTER OR startdate = LAST_N_FISCAL_QUARTERS:3) ';
        }else if (closeDateLiteral == 'fytd'){
            periodQuery += ' AND ( startdate = THIS_FISCAL_YEAR AND startdate <= TODAY)  ';
        }else{
             periodQuery += '';
        }
        periodQuery += ' ORDER BY StartDate';
        return periodQuery;
    }
    
    /* Save data */
    @RemoteAction
    global static void  saveData(String josndata) { 
        List<Opportunity> oppList = new List<Opportunity>();
        System.debug('-----jsondata'+josndata);
        
        JSONParser parser = JSON.createParser(josndata);
        
        Map<Id,String> opportunityRefs = new Map<Id,String>();
        
        while (parser.nextToken() != null) {
            // Start at the array of invoices.
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                System.debug('-----startArray');
                while (parser.nextToken() != JSONToken.END_ARRAY) {
                    Opportunity oppty = new Opportunity();
                    String oppRef = '';
                    while(parser.nextToken() != JSONToken.END_OBJECT){
                        System.debug('-----EndObject');
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'opid') {
                            parser.nextToken();
                            oppty.Id = parser.getIdValue();
                            System.debug('-----opId'+oppty.Id);
                        }
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'optyref') {
                            parser.nextToken();
                            oppRef = parser.getText();
                            System.debug('-----oppRef'+oppRef);
                        }
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'prob') {
                            parser.nextToken();
                            oppty.Unified_Funding_Status__c = parser.getText();
                            System.debug('-----prob'+oppty.Unified_Funding_Status__c);
                        }
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'mprob' && isManager) {
                            parser.nextToken();
                            oppty.MGR_Forecast_Percentage__c = parser.getText();
                            System.debug('-----mprob'+oppty.MGR_Forecast_Percentage__c);
                        }
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'dprob') {
                            parser.nextToken();
                            oppty.Unified_Probability__c = parser.getText();
                        }
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'closedate') {
                            parser.nextToken();
                            if(!String.isBlank(parser.getText())){
                                Date myDate= GetStringToDateFormat(parser.getText().split('T')[0]);
                                oppty.CloseDate = myDate;
                                System.debug('-----closedate'+oppty.CloseDate);
                            }
                        }
                    }
                    opportunityRefs.put(oppty.Id,oppRef);
                    oppList.add(oppty);
                }
            }
        }
        system.debug('oppList==================='+oppList); 
        if(!oppList.isEmpty()){
            
            list<String> oppNames = new list<String>();
            List<Database.SaveResult> updateResults = Database.update(oppList, false);
            for(Integer i=0;i<updateResults.size();i++){
                if (updateResults.get(i).isSuccess()){
                    updateResults.get(i).getId();
                } else if (!updateResults.get(i).isSuccess()){
                    oppNames.add(opportunityRefs.get(oppList.get(i).Id));
                }
                if(oppNames.isEMpty()){
                }else{
                    throw new ReportException('Error while saving records : '+String.join(oppNames,','));
                }
            }
        }
        
    }
    
    /* Convert string to date */
    public static Date GetStringToDateFormat(String myDate) {
       String[] myDateOnly = myDate.split(' ');
       String[] strDate = myDateOnly[0].split('-');
       Integer myIntDate = integer.valueOf(strDate[2]);
       Integer myIntMonth = integer.valueOf(strDate[1]);
       Integer myIntYear = integer.valueOf(strDate[0]);
       Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
       return d;
    }
    
    public void initializeProductCategories() {
        
        productCategoryMap = new Map<String,Map<String,Set<String>>>();
        modelVariationsMap = new Map<String,String>();
        catalogModelWithVariations = new Map<String,List<String>>();
        Catalog_Model__c[] catalogModelRecords = [
            SELECT Family__c, Line__c, Model__c, Model_Variations__c,
            Family_Order__c, Line_Order__c, Model_Order__c
            FROM Catalog_Model__c
            WHERE Show_On_Forecast_Report__c = true
            ORDER BY Family_Order__c, Line_Order__c, Model_Order__c
        ];

        for(Catalog_Model__c modelRow : catalogModelRecords) {
            
            modelVariationsMap.put(modelRow.Model__c, modelRow.Model__c);
            if(!String.isBlank(modelRow.Model_Variations__c)){
                List<String> modelVariations = modelRow.Model_Variations__c.split(',');
                catalogModelWithVariations.put(modelRow.Model__c, modelVariations);
                for(String variationName : modelVariations){
                    modelVariationsMap.put(variationName,modelRow.Model__c);
                }
            }
            
            if(productCategoryMap.containsKey(modelRow.Family__c)){
                Map<String,Set<String>> productLineMap = productCategoryMap.get(modelRow.Family__c);
                
                if(productLineMap.containsKey(modelRow.Line__c)){
                    productLineMap.get(modelRow.Line__c).add(modelRow.Model__c);
                    continue;
                }
                productLineMap.put(modelRow.Line__c, new Set<String>{modelRow.Model__c});
                continue;
            }
            productCategoryMap.put(
                modelRow.Family__c, new Map<String,Set<String>>{
                    modelRow.Line__c => new Set<String>{modelRow.Model__c}
                }
            );
        }
    }
    
    public List<SelectOption> getFamilyOptions(){
        List<SelectOption> familyOptions = new List<SelectOption>();
        for(String family : productCategoryMap.keySet()){
            familyOptions.add(new SelectOption(family, family));
        }
        return familyOptions;
    }
    
    /**
     * Get line options based on selected family on product category report
     */
    public Map<String,Set<String>> getProductLineOptions(){
        Map<String,Set<String>> familyLineOptions = new Map<String,Set<String>>();
        
        String selectedFamiliesString = ApexPages.currentPage().getParameters().get('selectedFamilies');
        List<String> selectedFamilies = new List <String>();
        System.debug('-----familyString'+selectedFamiliesString);
        
        if(String.isNotEmpty(selectedFamiliesString)){
            selectedFamilies = selectedFamiliesString.split(',', -2);
        }
        
        /*for(String family : selectedFamilies){
            familyLineOptions.put(family, productCategoryMap.get(family).keySet());
        }*/
        
        for(String family : productCategoryMap.keySet()){
            familyLineOptions.put(family, productCategoryMap.get(family).keySet());
        }
        
        System.debug('----familyLineOptions'+familyLineOptions);
        return familyLineOptions;
    }
    
    
    /**
     * Get model options based on selected family and lines on product category report
     */
    public Map<String,Set<String>> getModelOptions(){
        Map<String,Set<String>> lineModelOptions = new Map<String,Set<String>>();
        String selectedLinesString = ApexPages.currentPage().getParameters().get('selectedLines');
        List<String> selectedLines = new List<String>();
        if(String.isNotEmpty(selectedLinesString)){
            selectedLines = selectedLinesString.split(',', -2);
        }
        
        Set<String> productLinesSet = new Set<String>();
        productLinesSet.addAll(selectedLines);
        
        for(String family : productCategoryMap.keySet()){
            for(String line : productCategoryMap.get(family).keySet()){
                //if(productLinesSet.contains(family+line)){
                    lineModelOptions.put(family+'-'+line, productCategoryMap.get(family).get(line));
                //}
            }
        }
        
        return lineModelOptions;
    }
    
    /**
     * Get EMEA and APAC regions for subregion filter on report
     */
    public Map<String,Map<String,Set<String>>> getRegions(){
        Map<String,Map<String,Set<String>>> regions = new Map<String,Map<String,Set<String>>>{
            'APAC'=> new Map<String,Set<String>>(), 
            'EMEA'=> new Map<String,Set<String>>(),
            'Americas' => new Map<String,Set<String>>()
        };
        String region = Apexpages.currentPage().getparameters().get('region');
        
        String territoryQuery = 'SELECT Id, Territory__c, Region__c, Super_Territory__c, Sub_Region__c '+
            ' FROM Territory_Team__c';
            
        if(region == 'Americas'){
            territoryQuery +=' WHERE Super_Territory__c = \'Americas\' ';
        }else{
            territoryQuery +=' WHERE Territory__c = \'EMEA\' OR Territory__c = \'APAC\' ';
        }
        
        for(Territory_Team__c territory : Database.query(territoryQuery)){
            String territoryName = '';
            String superTerritory = '';
            String subRegion = '';
            if(region == 'Americas'){
                superTerritory = territory.Super_Territory__c;
                territoryName = territory.Territory__c;
                subRegion = territory.Region__c;
            }else{
                superTerritory = territory.Territory__c;
                territoryName = territory.Region__c;
                subRegion = territory.Sub_Region__c;
            }
            System.debug(LoggingLevel.INFO,'------territoryName'+territoryName+'------regions'+regions+'----territory.Region__c'+territory.Region__c+'---'+regions.get(territoryName));
            if(regions.get(superTerritory).containsKey(territoryName)){
                regions.get(superTerritory).get(territoryName).add(subRegion);
                continue;
            }
            regions.get(superTerritory).put(territoryName, new Set<String>{territoryName, subRegion});
        }
        return regions;
    }
    
    public List<SelectOption> getUserViews(){
        List<SelectOption> forecastViewsOptions = new List<SelectOption>();
        List<Forecast_View__c> forecastViews = [
            SELECT Id,Name,Quote_Type__c,Default_View__c,Excepted_Close_Date_Start__c,Excepted_Close_Date_End__c,
            Excepted_Close_Date_Range__c,Net_Booking_Amount_Start__c,Net_Booking_Amount_End__c,Product_Families__c,
            Product_Lines__c,Product_Models__c,Sub_Region__c,User__c,Region__c,Forecast_Columns__c,Probability__c,
            Probability_Operator__c,Stage__c, Deal_Probability__c, Deal_Probability_Operator__c
            FROM Forecast_View__c
            WHERE User__c=:UserInfo.getUserId()
            AND Region__c =:ApexPages.currentPage().getParameters().get('region')
            ORDER BY Default_View__c DESC
        ];
        
        for(Forecast_View__c fView : forecastViews){
            String fViewJson = JSON.serialize(fView);
            if(fView.Default_View__c){
                selectedForecastView = fViewJson;
            }
            forecastViewsOptions.add(new SelectOption(fViewJson, fView.Name));
        }
        return forecastViewsOptions;
    }
    
    /**
     * Forecast column oprion for view creation
     */
    public List<SelectOption> getForecastColumns(){
        List<SelectOption> forecastCols = new List<SelectOption>();
        List<Forecast_Column__c> forecastColumns;// = [SELECT Id,Name,Display_Name__c FROM Forecast_Column__c ORDER BY Display_Name__c];
        
        String region = ApexPages.currentPage().getParameters().get('region');
        
        if(region == 'Americas'){
            forecastColumns = [SELECT Id,Name,Display_Name__c FROM Forecast_Column__c WHERE Americas__c = true ORDER BY Display_Name__c];
        }else if(region == 'EMEA'){
            forecastColumns = [SELECT Id,Name,Display_Name__c FROM Forecast_Column__c WHERE EMEA__c = true ORDER BY Display_Name__c];
        }else{
            forecastColumns = [SELECT Id,Name,Display_Name__c FROM Forecast_Column__c WHERE APAC__c = true ORDER BY Display_Name__c];
        }  
        
        for(Forecast_Column__c fColumn : forecastColumns){
            forecastCols.add(new SelectOption(fColumn.Name, fColumn.Display_Name__c));
        }
        return forecastCols;
    }
    
    /**
     * Get Opportunity stage values
     */
    public List<SelectOption> getOpportunityStageValues(){
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(), f.getLabel()));
        }       
        return options;
    }
    
    /**
     * Probabilities based on region
     */
    public List<SelectOption> getProbabilities(){
        String region = ApexPages.currentPage().getParameters().get('region');
        if(region == 'Americas'){
            return new List<SelectOption>{
                new SelectOption('','--NONE--'),
                new SelectOption('0','0%'),
                new SelectOption('25','25%'),
                new SelectOption('50','50%'),
                new SelectOption('75','75%'),
                new SelectOption('100','100%')
            };
        }else{
            return new List<SelectOption>{
                new SelectOption('','--NONE--'),
                new SelectOption('0','0%'),
                new SelectOption('20','20%'),
                new SelectOption('40','40%'),
                new SelectOption('60','60%'),
                new SelectOption('80','80%'),
                new SelectOption('100','100%')
            };
        }
        return new List<SelectOption>();
    }
    
    /**
     * Get deal probability values
     */
    public List<SelectOption> getDealProbabilities(){
         List<SelectOption> options = new List<SelectOption>{new SelectOption('','--NONE--')};
        
        Schema.DescribeFieldResult fieldResult = Opportunity.Unified_Probability__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue().remove('%'), f.getLabel()));
        }       
        return options;
    }
    
    /**
     * Get EMEA probability percentage
     */
    @RemoteAction
    global static String getEMEAProbability(String forecastPercentage, String dealProbability){
        
        List<ProbabilityOnOpty__c> probabilities = ProbabilityOnOpty__c.getAll().Values();
        String probability = '0';
        for(ProbabilityOnOpty__c oppProb: probabilities){
            if(oppProb.UnifiedProbability__c == dealProbability && oppProb.FundingStatus__c == forecastPercentage){                    
                probability = oppProb.ContributionToForecast__c;
                break;                       
            }
        }
        return probability;
    }
    
    /**
     * Get ISS transfer and site solutions amount
     */
    @RemoteAction
    global static String getForecastManualAdjustments(String closeDateLiteral, String region){
        Set<String> manualAdjustmentKeys = new Set<String>();
        quarterlyManualAdjustments = new Map<String,List<String>>();
        List<Period> periods = Database.query(getPeriodQuery(closeDateLiteral));
        
        for(Period fiscalPeriod : periods){
            manualAdjustmentKeys.add(region+'Q'+fiscalPeriod.Number+fiscalPeriod.FiscalYearSettings.Name);
        }
        
        Decimal issTransfer = 0.0;
        Decimal siteSolutionsAmount = 0.0;
        Decimal brachyForecast = 0.0;
        if(closeDateLiteral!='custom'){
            for(Forecast_Manual_Adjustment__c fma : [
                SELECT Id, ISS_Transfer__c, Site_Solutions__c, Brachy_Forecast__c, Quarter__c
                FROM Forecast_Manual_Adjustment__c
                WHERE Manual_Adjustment_Key__c IN:manualAdjustmentKeys
                AND Region__c=:region
            ]){
                if(fma.ISS_Transfer__c!=null){
                    issTransfer += fma.ISS_Transfer__c;
                }
                if(fma.Site_Solutions__c!=null){
                    siteSolutionsAmount += fma.Site_Solutions__c;
                }
                if(fma.Brachy_Forecast__c!=null){
                    brachyForecast += fma.Brachy_Forecast__c;
                }
                quarterlyManualAdjustments.put(fma.Quarter__c, new List<String>{
                    fma.ISS_Transfer__c!=null?String.valueOf(fma.ISS_Transfer__c):'0.0', 
                    fma.Site_Solutions__c!=null?String.valueOf(fma.Site_Solutions__c):'0.0',
                    fma.Brachy_Forecast__c!=null?String.valueOf(fma.Brachy_Forecast__c):'0.0'
                });
            }
        }
        return String.valueOf(issTransfer)+'##'+String.valueOf(siteSolutionsAmount)+'##'+String.valueOf(brachyForecast);
    }
}
@isTest(seeAllData=true)
private class FC_ProductCategoryControllerCopyTest {

    static testMethod void testProductCategoryControllerCopy() {
         //Setup test data
        Account acct = TestUtils.getAccount();
        acct.Name = 'FORECAST REPORT ACCOUNT';
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        opp.MGR_Forecast_Percentage__c = '40%';
        opp.Unified_Funding_Status__c = '20%';
        opp.CloseDate = System.today();
        insert opp;
        
        insert new Forecast_Column__c(
            Name = 'TEST',
            Display_Name__c = 'TESTING'
        );
        
        insertForecastManualAdjustments();
        
        Opportunity opp1 = [SELECT SFDC_Oppty_Ref_number__c FROM Opportunity WHERE Id=:opp.Id];
        
        String jsonString = '[{ "opid":"'+opp.Id+'", "optyref":"'+opp1.SFDC_Oppty_Ref_number__c+'", "prob":"25%", "mprob":"25%","closedate":"2016-11-24" }]';
        
        System.Test.startTest();
        PageReference forecastPage = Page.fc_ProductCategoryReport;
        forecastPage.getParameters().put('region','Americas');
        Test.setCurrentPage(forecastPage);
        FC_ProductCategoryControllerCopy obje = new FC_ProductCategoryControllerCopy();
        FC_ProductCategoryControllerCopy.saveLastSearchFilter('https://varian--sfdev.cs19.my.salesforce.com?param1=test&pram2=rfee&region=Americas');
        FC_ProductCategoryControllerCopy.saveLastSearchFilter('https://varian--sfdev.cs19.my.salesforce.com?param1=test&pram2=rfee&region=APAC');
        FC_ProductCategoryControllerCopy.saveLastSearchFilter('https://varian--sfdev.cs19.my.salesforce.com?param1=test&pram2=rfee&region=EMEA');
        FC_ProductCategoryControllerCopy.GetStringToDateFormat('2013-02-23');
        FC_ProductCategoryControllerCopy.saveData(jsonString);
        Id viewId = FC_ProductCategoryControllerCopy.updateForecastFilters('https://varian--sfdev.cs19.my.salesforce.com?sparam1=THIS_FISCAL_QUARTER&selectedViewColumns=account,pquote,closedate,mprob,managerQuarterTotals,nbv,ostage,iswon,productCategories&viewName=TEST NEW VIEW&stageOptions=0 - NEW LEAD&familyOptions=System Solutions&lineOptions=System SolutionsAdvanced Treatment Delivery&modelOptions=Edge&sr=Northeast&pqt=All&region=Americas&sdate=12/31/2016&edate=03/31/2017&snba=1&enba=100000000&snba=1&enba=100000000&probabilityOperator=e&dealProbabilityOperator=null&probability=0&defaultView=true');
        
        FC_ProductCategoryControllerCopy.updateForecastFilters('https://varian--sfdev.cs19.my.salesforce.com?sparam1=THIS_FISCAL_QUARTER&forecastViewId='+viewId+'&selectedViewColumns=account,pquote,closedate,mprob,managerQuarterTotals,nbv,ostage,iswon,productCategories&viewName=TEST NEW VIEW&stageOptions=0 - NEW LEAD&familyOptions=System Solutions&lineOptions=System SolutionsAdvanced Treatment Delivery&modelOptions=Edge&sr=Northeast&pqt=All&region=Americas&sdate=12/31/2016&edate=03/31/2017&snba=1&enba=100000000&snba=1&enba=100000000&probabilityOperator=e&dealProbabilityOperator=null&probability=0&defaultView=true');
        FC_ProductCategoryControllerCopy.updateForecastFilters('https://varian--sfdev.cs19.my.salesforce.com?sparam1=THIS_FISCAL_QUARTER&selectedViewColumns=account,pquote,closedate,mprob,managerQuarterTotals,nbv,ostage,iswon,productCategories&viewName=TEST NEW VIEW123123kkk&stageOptions=0 - NEW LEAD&familyOptions=System Solutions&lineOptions=System SolutionsAdvanced Treatment Delivery&modelOptions=Edge&sr=Northeast&pqt=All&region=Americas&sdate=12/31/2016&edate=03/31/2017&snba=1&enba=100000000&snba=1&enba=100000000&probabilityOperator=e&dealProbabilityOperator=null&probability=0&defaultView=true');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('LAST_FISCAL_YEAR');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('THIS_FISCAL_QUARTER');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('NEXT_FISCAL_QUARTER');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('LAST_FISCAL_QUARTER');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('curnext1');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('curprev1');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('curnext3');
        FC_ProductCategoryControllerCopy.getExpectedDateRange('curlast3');
        FC_ProductCategoryControllerCopy.getForecastManualAdjustments('curnext3','Americas');
        FC_ProductCategoryControllerCopy.getForecastManualAdjustments('curlast3','Americas');
        obje.initializeProductCategories();
        obje.getFamilyOptions();
        obje.getProductLineOptions();
        obje.getModelOptions();
        obje.getRegions();
        obje.getUserViews();
        obje.getForecastColumns();
        obje.getOpportunityStageValues();
        obje.getProbabilities();
        obje.getDealProbabilities();
        FC_ProductCategoryControllerCopy.deleteForecastView(viewId, 'Americas');
        
        PageReference forecastPage1 = Page.fc_ProductCategoryReport;
        forecastPage1.getParameters().put('region','EMEA');
        Test.setCurrentPage(forecastPage1);
        obje.getProbabilities();
        System.Test.stoptest();
    }
    
    private static void insertForecastManualAdjustments(){
        List<Forecast_Manual_Adjustment__c> fmaList = new List<Forecast_Manual_Adjustment__c>();
        List<Forecast_Manual_Adjustment__c> existingFmaList = [
            Select Id,Manual_Adjustment_Key__c
            FROM Forecast_Manual_Adjustment__c
        ];
        Set<String> fmaKeys = new Set<String>();
        for(Forecast_Manual_Adjustment__c fma : existingFmaList){
            fmaKeys.add(fma.Manual_Adjustment_Key__c);
        }
        
        for(Integer counter=1;counter<5;counter++){
            if(!fmaKeys.contains('Americas'+'Q'+counter+Date.today().year())){
                Forecast_Manual_Adjustment__c fma = new Forecast_Manual_Adjustment__c();
                fma.Region__c = 'Americas';
                fma.Fiscal_Year__c = String.valueOf(Date.today().year());
                fma.Quarter__c = 'Q'+counter;
                fmaList.add(fma);
            }
        }
        insert fmaList;
    }
}
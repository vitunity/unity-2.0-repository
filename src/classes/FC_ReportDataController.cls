/* @Author : Amitkumar Katre
 * @Decription : Forecast report data controller 
 * @date : *golive)
 */
public with sharing class FC_ReportDataController {
    /* Opportunity JSON Data member*/
    public string oppDataJson {get;set;}
    map < String, Column > columnMap = new map < String, Column > ();
    
    /* check manager permission */
    public static boolean isManager;
    static {
        Schema.DescribeFieldResult drField = Opportunity.MGR_Forecast_Percentage__c.getDescribe();
        isManager = drField.isCreateable();
        if(System.Test.isRunningTest()){
            isManager = true;
        }
    }
    /* Opportunity default query */ 
    public string query = 'select Account.BillingState, Account.BillingCity, Account.Name, Unified_Funding_Status__c, ' +
        'Amount, CloseDate, SFDC_Oppty_Ref_number__c,MGR_Forecast_Percentage__c, Primary_Quote_Number__c, Iswon, ' +
        'Net_Booking_Value__c, Quote_Status__c,  NA_Affiliation__c, Account.Owner.Name, Name, StageName, ' +
        'Owner.Name, OMNI_Manager_Probability__c,OMNI_Probability__c, Account.Sub_Region__c,  Account.Account_Type__c, ' +
        '(select BigMachines__Is_Primary__c,Name, Quote_Status__c, Booked_Sales_Services__c, '+ 
        'Prebook_Sales_Services__c, Order_Type__c from BigMachines__BigMachines_Quotes__r ' +
        'where BigMachines__Is_Primary__c = true limit 1), Opportunity_Type__c ' +
        'from Opportunity ';
    
    /* Period default query */ 
    public string periodQuery = 'select startdate, Number,Type, enddate from period ';
    list < Period > periodList = new list < Period > ();
    
    /* Quarter Map */ 
    map < Integer, String > quarters = new map < Integer, String > {
        1 => 'Q1',
        2 => 'Q2',
        3 => 'Q3',
        4 => 'Q4'
    };
    /* Months Map */ 
    map < Integer, String > months = new map < Integer, String > {
        1 => 'OCT',
        2 => 'NOV',
        3 => 'DEC',
        4 => 'JAN',
        5 => 'FEB',
        6 => 'MAR',
        7 => 'APR',
        8 => 'MAY',
        9 => 'JUN',
        10 => 'JUL',
        11 => 'AUG',
        12 => 'SEPT'
    };
    
    // apex page params map
    map < String, String > params;
    
    private set<string> primary_q_type_set = new set<string>();
    //report controller
    public FC_ReportDataController() {
        params = apexpages.currentpage().getparameters();
        
        String suspended = '10 - SUPERSEDED';
        query += ' where StageName != : suspended ';
        
        //Fiscal date range filter
        if (params.containsKey('sparam1')) {
      if (params.get('sparam1') == 'LAST_FISCAL_YEAR') {
                query += 'and closedate = ' + params.get('sparam1');
                periodQuery += ' where ( startdate = ' + params.get('sparam1') + ') ';
            } else if (params.get('sparam1') == 'THIS_FISCAL_QUARTER') {
                query += 'and closedate = ' + params.get('sparam1');
                periodQuery += ' where ( startdate = ' + params.get('sparam1') + ') ';
            } else if (params.get('sparam1') == 'NEXT_FISCAL_QUARTER') {
                query += 'and closedate = ' + params.get('sparam1');
                periodQuery += ' where ( startdate = ' + params.get('sparam1') + ') ';
            } else if (params.get('sparam1') == 'LAST_FISCAL_QUARTER') {
                query += 'and closedate = ' + params.get('sparam1');
                periodQuery += 'where ( startdate = ' + params.get('sparam1') + ') ';
            } else if (params.get('sparam1') == 'curnext1') {
                query += 'and (closedate = THIS_FISCAL_QUARTER OR closedate = NEXT_FISCAL_QUARTER) ';
                periodQuery += 'where ( startdate = THIS_FISCAL_QUARTER OR startdate = NEXT_FISCAL_QUARTER)  ';
            } else if (params.get('sparam1') == 'curprev1') {
                query += 'and (closedate = THIS_FISCAL_QUARTER OR closedate = LAST_FISCAL_QUARTER) ';
                periodQuery += 'where (startdate = THIS_FISCAL_QUARTER OR startdate = LAST_FISCAL_QUARTER) ';
            } else if (params.get('sparam1') == 'curnext3') {
                query += 'and (closedate = THIS_FISCAL_QUARTER OR closedate = NEXT_N_FISCAL_QUARTERS:3) ';
                periodQuery += 'where (startdate = THIS_FISCAL_QUARTER OR startdate = NEXT_N_FISCAL_QUARTERS:3) ';
            }else if (params.get('sparam1') == 'curlast3') {
                query += 'and (closedate = THIS_FISCAL_QUARTER OR closedate = LAST_N_FISCAL_QUARTERS:3) ';
                periodQuery += 'where (startdate = THIS_FISCAL_QUARTER OR startdate = LAST_N_FISCAL_QUARTERS:3) ';
            }else{
                
            }
        }
        
        
        Date sdate;
        Date edate; 
        
        Date psdate;
        Date pedate;        
        
        if(params.containsKey('sdate')) {
             sdate = GetStringToDateFormat(params.get('sdate'));
             edate = GetStringToDateFormat(params.get('edate'));
             
             query += ' and (closedate >= : sdate and closedate <=: edate) ';
             for(Period p : [select startdate,Number,Type, enddate from period where type = 'month' or type = 'quarter' ]){
                 if(sdate >= p.startdate && sdate <= p.enddate){
                    psdate = p.startdate;  
                 }
                 if(edate >= p.startdate && edate <= p.enddate){
                    pedate = p.enddate;  
                 }
             }
             periodQuery += ' and (startdate>= : psdate and enddate <=: pedate)  ';
        }
        
        // Sub Region Filter
        String sr = params.get('sr');
        list < String > listSR = new list < String > ();
        if (String.isNotEmpty(sr)) {
            listSR = sr.split(',', -2);
            system.debug('listSR===' + listSR);
            if (sr.contains('North America') || sr.contains('Latin America')) {
                query += ' and Account.Territories1__c in : listSR ';
            } else {
                query += ' and Account.Sub_Region__c in :listSR ';
            }
        }
        //region check
        String region = '';
        if (params.containsKey('region') && (
            params.get('region') != 'all')) {
            region = apexpages.currentpage().getparameters().get('region');
            if (region == 'Americas') {
                query += ' and Account.Super_Territory1__c =:region ';
            }
        }
        periodQuery += ' and ( Type = \'Month\' or Type = \'Quarter\' ) order by enddate asc';
        system.debug('query====' + query);
        system.debug('region====' + region);
        system.debug('sr====' + sr);
        system.debug('periodQuery====' + periodQuery);
        query += ' and Net_Booking_Value__c != null and Net_Booking_Value__c > 0 ';
        Decimal snba;
        Decimal enba;
         
        if (params.containsKey('snba')) {
            snba = Decimal.valueof(apexpages.currentpage().getparameters().get('snba'));
            enba = Decimal.valueof(apexpages.currentpage().getparameters().get('enba'));
            query += ' and Net_Booking_Value__c >=: snba and Net_Booking_Value__c <=: enba';
        }
        
        
        
        if (params.containsKey('pqt')) {
            String pqt = apexpages.currentpage().getparameters().get('pqt');
            if(pqt == 'Sales'){
                primary_q_type_set.add('Sales');
            }else if(pqt == 'Service'){
                primary_q_type_set.add('Services');
            }else if(pqt == 'Combined'){
                primary_q_type_set.add('Combined');
            }else{
                primary_q_type_set.add('Sales');
                primary_q_type_set.add('Services');
                primary_q_type_set.add('Combined');
            }
        }
        
        
        list < Opportunity > oppList = database.query(query);//query opportunities
        periodList = database.query(periodQuery);//query fical periods
        buildColumns();//Build Dynmaic Columns
        oppDataJson = buildReportData(oppList);//build json report opportunityt data
    }
    
    private map<Id,Account> getAccounts (Set<id> accountIds){
        return new map<Id,Account> ([Select (Select Strategic__r.Name, Strategic_Account_Owner__c From AccountAssociations1__r) 
                                     From Account where id in :accountIds]);
    }
    
    /*build json report opportunity data*/
    public String buildReportData(list < Opportunity > oppList) {
        Set<id> accountIds = new Set<id>();
        for (Opportunity opp: oppList) {
            if(opp.AccountId != null){
                accountIds.add(opp.AccountId);
            }
        }
        
        map<Id,Account> accountMap = getAccounts(accountIds);
        list < ReportData > rdList = new list < ReportData > ();
        for (Opportunity opp: oppList) {
            ReportData rd = new ReportData();
      
            if(opp.AccountId != null){
                Account aObj = accountMap.get(opp.AccountId);
                if(aObj.AccountAssociations1__r != null && !aObj.AccountAssociations1__r.isEmpty()){
                    Account_Associations__c aaObj = aObj.AccountAssociations1__r[0];
                    system.debug('strategic account ===='+aaObj);
                    rd.strategic_account = aaObj.Strategic__r.Name;
                    rd.strategic_owner = aaObj.Strategic_Account_Owner__c;
                }
            }
            system.debug('opp------------------'+opp);
            if (!opp.BigMachines__BigMachines_Quotes__r.isEmpty()//){
                //&& primary_q_type_set.contains(opp.BigMachines__BigMachines_Quotes__r[0].Order_Type__c) ){
        && primary_q_type_set.contains(opp.Opportunity_Type__c) ){
                BigMachines__Quote__c bq = opp.BigMachines__BigMachines_Quotes__r[0];
                rd.pquote = bq.name;
                rd.iswon = opp.iswon;
        rd.pquoteid = bq.id;
                rd.qs = bq.Quote_Status__c;
                rd.prebookno = bq.Prebook_Sales_Services__c;
                rd.saleorder = bq.Booked_Sales_Services__c;
            
                rd.opid = opp.Id;
                rd.optyref = opp.SFDC_Oppty_Ref_number__c;
                rd.account = opp.Account.Name;
                rd.accountowner = opp.Account.Owner.Name;
                rd.closedate = GetDateToString(opp.CloseDate);
                rd.city = opp.Account.BillingCity;
                rd.state = opp.Account.BillingState;
                rd.subregion = opp.Account.Sub_Region__c;
                rd.prob = opp.Unified_Funding_Status__c;
                rd.mprob = opp.MGR_Forecast_Percentage__c;
                rd.nbv = opp.Net_Booking_Value__c;
                rd.na = opp.NA_Affiliation__c;
                rd.oppowner = opp.Owner.Name;
                rd.total = opp.Amount;
                rd.oppname = opp.Name;
                rd.atype = opp.Account.Account_Type__c;
                rd.ostage = opp.StageName;
                //Rep probaility net booking value
                if (rd.prob != null && rd.nbv != null) {
                    String tempProb = rd.prob.replace('%', '');
                    Decimal tempProbIngt = Decimal.valueof(tempProb);
                    rd.pnetvalue = (tempProbIngt / 100) * rd.nbv;
                }
                //mgr probaility net booking value
                if (rd.mprob != null && rd.nbv != null) {
                    String tempMProb = rd.mprob.replace('%', '');
                    Decimal tempMProbIngt = Decimal.valueof(tempMProb);
                    rd.mnetvalue = (tempMProbIngt / 100) * rd.nbv;
                }
                //check month and quarter of opportunity
                for (Period p: periodList) {
                    if (opp.closedate >= p.startdate && opp.closedate <= p.enddate) {
                        if (p.Type == 'Month') {
                            rd.m = p.Number;
                        }
                        if (p.Type == 'Quarter') {
                            rd.q = p.Number;
                        }
                    }
                }
    
                rd.quarter = quarters.get(rd.q);
                rd.month = months.get(rd.m);
              
                if (rd.m == 1) {
                    rd.amt1 = rd.pnetvalue;
                }
                if (rd.m == 2) {
                    rd.amt2 = rd.pnetvalue;
                }
                if (rd.m == 3) {
                    rd.amt3 = rd.pnetvalue;
                }
                if (rd.m == 4) {
                    rd.amt4 = rd.pnetvalue;
                }
                if (rd.m == 5) {
                    rd.amt5 = rd.pnetvalue;
                }
                if (rd.m == 6) {
                    rd.amt6 = rd.pnetvalue;
                }
                if (rd.m == 7) {
                    rd.amt7 = rd.pnetvalue;
                }
                if (rd.m == 8) {
                    rd.amt8 = rd.pnetvalue;
                }
                if (rd.m == 9) {
                    rd.amt9 = rd.pnetvalue;
                }
                if (rd.m == 10) {
    
                    rd.amt10 = rd.pnetvalue;
                }
                if (rd.m == 11) {
                    rd.amt11 = rd.pnetvalue;
                }
                if (rd.m == 12) {
                    rd.amt12 = rd.pnetvalue;
                }
    
                if (rd.q == 1) {
                    rd.q1 = rd.pnetvalue;
                }
                if (rd.q == 2) {
                    rd.q2 = rd.pnetvalue;
                }
                if (rd.q == 3) {
                    rd.q3 = rd.pnetvalue;
                }
                if (rd.q == 4) { 
    
                    rd.q4 = rd.pnetvalue;
                }
                
                /* Manager values */
                if(isManager){
                    if (rd.m == 1) {
                    rd.mamt1 = rd.mnetvalue;
                    }
                    if (rd.m == 2) {
                        rd.mamt2 = rd.mnetvalue;
                    }
                    if (rd.m == 3) {
                        rd.mamt3 = rd.mnetvalue;
                    }
                    if (rd.m == 4) {
                        rd.mamt4 = rd.mnetvalue;
                    }
                    if (rd.m == 5) {
                        rd.mamt5 = rd.mnetvalue;
                    }
                    if (rd.m == 6) {
                        rd.mamt6 = rd.mnetvalue;
                    }
                    if (rd.m == 7) {
                        rd.mamt7 = rd.mnetvalue;
                    }
                    if (rd.m == 8) {
                        rd.mamt8 = rd.mnetvalue;
                    }
                    if (rd.m == 9) {
                        rd.mamt9 = rd.mnetvalue;
                    }
                    if (rd.m == 10) {
                        rd.mamt10 = rd.mnetvalue;
                    }
                    if (rd.m == 11) {
                        rd.mamt11 = rd.mnetvalue;
                    }
                    if (rd.m == 12) {
                        rd.mamt12 = rd.mnetvalue;
                    }
        
                    if (rd.q == 1) {
                        rd.mq1 = rd.mnetvalue;
                    }
                    if (rd.q == 2) {
                        rd.mq2 = rd.mnetvalue;
                    }
                    if (rd.q == 3) {
                        rd.mq3 = rd.mnetvalue;
                    }
                    if (rd.q == 4) { 
                        rd.mq4 = rd.mnetvalue;
                    }
                }
                system.debug('rd.m===' + rd.m + 'rd.q====' + rd.q);
                rdList.add(rd);
            }
        }
        
        ReportDataWrap rdw = new ReportDataWrap();//report data wrapper
        rdw.rdList = rdList;
        rdw.columnList = columnMap.values();        
        String JSONString = JSON.serialize(rdw); //serialize to json
        return JSONString;
    }

    /* Report Data Wrapper class */
    public class ReportData {
        public boolean iswon{get;set;}
        public string pquote {get;set;}
        public Integer m{get;set;}
        public Integer q{get;set;}
        public string month{get;set;}
        public string quarter{get;set;}
        public string opid {get;set;}
        public string account {get;set;}
        public string pquoteid {get;set;}
        public string na {get;set;}
        public string qs {get;set;}
        public decimal nbv {get;set;}
        public string accountowner {get;set;}
        public string oppname {get;set;}
        public string oppowner {get;set;}
        public String city {get;set;}
        public string state {get;set;}
        public string subregion {get;set;}
        public string prob {get;set;}
        public string mprob {get;set;}
        public string optyref {get;set;}
        public String closedate {get;set;}
        public Decimal total {get;set;}
        
        public string strategic_account {get;set;}
        public string strategic_owner {get;set;}
        
        public Decimal q1 {get;set;}
        public Decimal q2 {get;set;}
        public Decimal q3 {get;set;}
        public Decimal q4 {get;set;}
        public Decimal amt1 {get;set;}
        public Decimal amt2 {get;set;}
        public Decimal amt3 {get;set;}
        public Decimal amt4 {get;set;}
        public Decimal amt5 {get;set;}
        public Decimal amt6 {get;set;}
        public Decimal amt7 {get;set;}
        public Decimal amt8 {get;set;}
        public Decimal amt9 {get;set;}
        public Decimal amt10 {get;set;}
        public Decimal amt11 {get;set;}
        public Decimal amt12 {get;set;}

        public Decimal mq1 {get;set;}
        public Decimal mq2 {get;set;}
        public Decimal mq3 {get;set;}
        public Decimal mq4 {get;set;}
        public Decimal mamt1 {get;set;}
        public Decimal mamt2 {get;set;}
        public Decimal mamt3 {get;set;}
        public Decimal mamt4 {get;set;}
        public Decimal mamt5 {get;set;}
        public Decimal mamt6 {get;set;}
        public Decimal mamt7 {get;set;}
        public Decimal mamt8 {get;set;}
        public Decimal mamt9 {get;set;}
        public Decimal mamt10 {get;set;}
        public Decimal mamt11 {get;set;}
        public Decimal mamt12 {get;set;}
        public Decimal pnetvalue {get;set;}
        public Decimal mnetvalue {get;set;}
        
        public String sacctmgr {get;set;}
        public String prebookno {get;set;}
        public String saleorder {get;set;}
        public String atype {get;set;}
        public String ostage {get;set;}
        

    }
    
    /*Report Data and Columns Wrapper class */
    public class ReportDataWrap {
        public list < ReportData > rdList {get;set;}
        public list < Column > columnList {get;set;}
    }
    
    /* Column Class */
    public class Column {
        public Integer orderNumber {get;set;}
        public string displayName {get;set;}
        public string name {get;set;}
        public boolean visible {get;set;}
        public string x_type {get;set;}
        public boolean editable {get;set;}
        public boolean searchable {get;set;}
        public boolean isManagerFlag {get;set;}
        public Column() {}
        public Column(String dn, String n, boolean v, String xt) {
            displayName = dn;
            name = n;
            visible = v;
            x_type = xt;
        }
    }
    
    /* Build dynamic columns */
    public void buildColumns() {
        Integer i = 1;
        /*Columns for Reps */
        if(isManager) {
            Column p = New Column('Probability(rep) %', 'prob', true, 'mgrp');
            p.editable = false;
            p.orderNumber = i; 
            i++;   
            columnMap.put(p.name, p);
        }else {
            Column p = New Column('Probability %', 'prob', true, 'mgrp');
            p.editable = true;
            p.orderNumber = i;  
            i++; 
            columnMap.put(p.name, p);
        }
        for (Period  p: periodList) {
            if(p.Type == 'Month'){
                String m = months.get(p.Number);
                Column c = New Column(m, 'amt' + p.Number, true, 'Currency');
                c.orderNumber = i;  
                i++;
                columnMap.put(c.name, c);
            }
            if(p.Type == 'Quarter'){
                String q = quarters.get(p.Number);
                Column cq = New Column(q, 'q' + p.Number, true, 'Currency');
                cq.orderNumber = i;  
                i++;
                columnMap.put(cq.name, cq);
            }
        }
        /* Manager Columns */
        if (isManager) {
            Column mp = New Column('MGR Probability %', 'mprob', true, 'mgrp');
            mp.editable = true;
            mp.orderNumber = i;i++;
            mp.isManagerFlag = true;
            columnMap.put(mp.name, mp);
            
            for (Period  p: periodList) {
                if(p.Type == 'Month'){
                    String m = 'Mgr '+months.get(p.Number);
                    Column c = New Column(m, 'mamt' + p.Number, true, 'Currency');
                    c.orderNumber = i;  
                    i++;
                    columnMap.put(c.name, c);
                }
                if(p.Type == 'Quarter'){
                    String q = 'Mgr '+quarters.get(p.Number);
                    Column cq = New Column(q, 'mq' + p.Number, true, 'Currency');
                    cq.orderNumber = i;  
                    i++;
                    columnMap.put(cq.name, cq);
                }
            }
        }

    }
    /* Convert string to date */
    public static Date GetStringToDateFormat(String myDate) {
       String[] myDateOnly = myDate.split(' ');
       String[] strDate = myDateOnly[0].split('/');
       Integer myIntDate = integer.valueOf(strDate[1]);
       Integer myIntMonth = integer.valueOf(strDate[0]);
       Integer myIntYear = integer.valueOf(strDate[2]);
       Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
       return d;
    }
    
    /* Convert date to string */
    public static String GetDateToString(Date dt){
        //String dStr = dt.year()+'-'+dt.month()+'-'+dt.day()+'T00:00:00.000Z';
        return String.valueof(dt);
    }
    
}
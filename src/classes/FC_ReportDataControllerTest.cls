/*
 * @author: Amitkumar Katre
 * @description: Report controller test class
 * @createddate: 05/10/2016
 */
@isTest
public class FC_ReportDataControllerTest {
    
    public static testmethod void reportDataTest1(){
        //Setup test data
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        list<Opportunity> oppList = new list<Opportunity>();
        list<BigMachines__Quote__c> bigMachineQuotesList = new list<BigMachines__Quote__c>();
        
        for(Integer i=0; i<=12; i++){ 
	        Opportunity opp = TestUtils.getOpportunity();
	        opp.AccountId = acct.Id;
	        opp.Primary_Contact_Name__c = con.Id;
	        opp.MGR_Forecast_Percentage__c = '40%';
	        opp.Unified_Funding_Status__c = '20%';
	        opp.Net_Booking_Value__c = 200;
	        opp.CloseDate = System.today().addMonths(i);
            oppList.add(opp);
        }   
        
        insert oppList;
        
        for(Opportunity opp: oppList){ 
	        BigMachines__Quote__c bq = new BigMachines__Quote__c();
            bq.Name = '2016-15108';
            bq.BigMachines__Account__c = acct.Id;
            bq.BigMachines__Opportunity__c = opp.Id;
            bq.BigMachines__Is_Primary__c = true;
            bq.Order_Type__c = 'Sales';
            bigMachineQuotesList.add(bq);
        } 
        
        insert bigMachineQuotesList;

        System.Test.startTest();
        
        Pagereference p = new Pagereference('/apex/fc_reportdata?sparam1=THIS_FISCAL_QUARTER&pqt=Sales');
        System.Test.setCurrentPage(p);
        FC_ReportDataController obj = new FC_ReportDataController();
        
        Pagereference p2 = new Pagereference('/apex/fc_reportdata?sparam1=NEXT_FISCAL_QUARTER&pqt=Sales');
        System.Test.setCurrentPage(p2);
        FC_ReportDataController obj2 = new FC_ReportDataController();
        
        Pagereference p3 = new Pagereference('/apex/fc_reportdata?sparam1=LAST_FISCAL_QUARTER&pqt=Sales');
        System.Test.setCurrentPage(p3);
        FC_ReportDataController obj3 = new FC_ReportDataController();
        
        Pagereference p4 = new Pagereference('/apex/fc_reportdata?sparam1=curnext1&pqt=Sales');
        System.Test.setCurrentPage(p4);
        FC_ReportDataController obj4 = new FC_ReportDataController();
        
        Pagereference p5 = new Pagereference('/apex/fc_reportdata?sparam1=curprev1&pqt=Sales');
        System.Test.setCurrentPage(p5);
        FC_ReportDataController obj5 = new FC_ReportDataController();
        
        Pagereference p6 = new Pagereference('/apex/fc_reportdata?sparam1=curnext3&sdate=04/27/2016&edate=04/29/2016&region=Americas&sr=Northeast');
        System.Test.setCurrentPage(p6);
        FC_ReportDataController obj6 = new FC_ReportDataController();
        
        
        System.Test.stoptest();
    }
    
}
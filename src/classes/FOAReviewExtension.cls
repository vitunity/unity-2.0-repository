/***************************************************************************
Author: Amitkumar Katre
Created Date: 13-October-2017
Project/Story/Inc/Task : STRY0033962 - Work Order:  Work Order Review Page (FOA) 
Description: FOA Review Page Controller

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
11/20/17, STSK0013043, NS:  Updated Page to drive function off buttons matching code.
01/03/18, STSK0013589, NS:  Updated Save button display conditions to include Do Not Process.
28-Feb-2018 - Nick Sauer - STSK0013474 - Update ECF logic to Case_ECF_Count field.
*************************************************************************************/
public with sharing class FOAReviewExtension {
    
    private ApexPages.StandardController sc;
    @testvisible private SVMXC__Service_Order__c woObj;
    public list<SVMXC__Service_Order_Line__c> wdlList{get;set;}
    private Id fsRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
    private Id installRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Installation');
    public list<WorkDetailWrapper> wdlWrapperList{get;set;}
    
    public enum SORT_BY {
            ByName,
            ByLineType,
            ByStartDate,
            BySAPBillable
    }
    private map<String, SORT_BY> sortByValues = new Map<String, SORT_BY>();
    private map<String,Integer> sortOrderMap = new map<String,Integer>{'ByName'=> 1,
                                                                       'ByLineType'=> 1,
                                                                       'ByStartDate'=> 1,
                                                                       'BySAPBillable'=> 1};
                                                                       
    private static map<String,String> coloumnsWithApiNames = new map<String,String>{'ByName'=> 'Name',
                                                                             'ByLineType'=> 'SVMXC__Line_Type__c',
                                                                             'ByStartDate'=>'SVMXC__Start_Date_and_Time__c',
                                                                             'BySAPBillable'=> 'ERP_Billable__c'};
    public static Integer sortOrderVal ;
    public static SORT_BY sortBy = SORT_BY.ByName;
    
    public PageReference sortByColumns(){
        String param = ApexPages.currentPage().getParameters().get('field');
        sortOrderVal = sortOrderMap.get(param);
        sortOrderMap.put(param,-sortOrderVal);
        sortBy = sortByValues.get(param);
        wdlWrapperList.sort();      
        return null;  
    }
        
    public class WorkDetailWrapper implements Comparable {
        public SVMXC__Service_Order_Line__c wdObj {get;set;}
        public boolean selectBox {get;set;}
        public WorkDetailWrapper(SVMXC__Service_Order_Line__c wdObj, boolean selectBox){
            this.selectBox = selectBox;
            this.wdObj = wdObj;
        }
        
        public Integer compareTo(Object obj) {
            WorkDetailWrapper wObj = (WorkDetailWrapper)(obj); 
            String fileApiName = coloumnsWithApiNames.get(String.valueOf(sortBy));
            SObject wData = wObj.wdObj;
            SObject wDataThis = this.wdObj;
            if(wDataThis.get(fileApiName) instanceof String || wDataThis.get(fileApiName) instanceof boolean){
                if (String.valueOf(wDataThis.get(fileApiName)) > String.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (String.valueOf(wDataThis.get(fileApiName)) == String.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }
            
            if(wDataThis.get(fileApiName) instanceof DateTime){
                if (DateTime.valueOf(wDataThis.get(fileApiName)) > DateTime.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (DateTime.valueOf(wDataThis.get(fileApiName)) == DateTime.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }            
            return -sortOrderVal;
        }   
    }
    
    public FOAReviewExtension(ApexPages.StandardController sc){
        if(!Test.isRunningTest()){
            sc.addFields(new list<String>{'Service_Team__r.Paid_Service_Overhead_JO__c',
                                      'Service_Team__r.Paid_Service_Overhead_JO__c',
                                      'Service_Team__r.Contract_Overhead_JO__c',
                                      'SVMXC__Order_Status__c','Parts_Quote_Status__c', 'Parts_Work_Detail_Count__c',
                                      'Parts_Quote_Number__c','Interface_Status__c', 'Sales_Org__c',
                                       'Is_escalation_to_the_CLT_required__c','Case_ECF_Count__c','Close__c', 'BST__c',
                                      'RecordTypeId'});
        }
        
        this.sc = sc;
        for (SORT_BY enumValue : SORT_BY.values()){
            sortByValues.put(String.valueOf(enumValue), enumValue);
        }
        this.woObj = (SVMXC__Service_Order__c)sc.getRecord();
        initializeData();
        //reset();
    }
     
    private void initializeData(){
        wdlWrapperList = new list<WorkDetailWrapper> ();
        wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c');
        for(SVMXC__Service_Order_Line__c wdObj : wdlList){
            system.debug('tes======'+wdObj.ERP_Billable__c);
            wdlWrapperList.add(new WorkDetailWrapper(wdObj,wdObj.ERP_Billable__c));
        }
        //Default sort by start date
        sortOrderVal = sortOrderMap.get('ByStartDate');
        sortOrderMap.put('ByStartDate',-sortOrderVal);
        sortBy = sortByValues.get('ByStartDate');
        wdlWrapperList.sort(); 
    }

    public String getIsValidWo(){
        if((woObj.SVMXC__Order_Status__c == 'Submitted' || woObj.SVMXC__Order_Status__c == 'Error') && woObj.Interface_Status__c != 'Process' && woObj.Interface_Status__c != 'Processing'){ 
            return '';
        }else{
            return 'none';
        }
    }
    
    public String getShowWoValidateMessage(){
        if((woObj.SVMXC__Order_Status__c == 'Submitted' || woObj.SVMXC__Order_Status__c == 'Error') && woObj.Interface_Status__c != 'Process' && woObj.Interface_Status__c != 'Processing'){ 
            return 'none';
        }else if(woObj.ID == null){
            return 'none';
        } 
        else{
            return '';
        }
    }

    //Display Save button if Submitted, Error or Reviewed
    public String getDisplaySaveBtn(){
        if((woObj.SVMXC__Order_Status__c == 'Submitted' || woObj.SVMXC__Order_Status__c == 'Error' || woObj.SVMXC__Order_Status__c == 'Reviewed') && (woObj.Interface_Status__c == 'Processed' || woObj.Interface_Status__c == 'Error' || woObj.Interface_Status__c == 'Do Not Process')){ 
            return '';
        }else{
            return 'none';
        }
    }    
    
    //Allow entry Save and stay on same screen - no redirect, just refresh
    public PageReference save(){
        try{
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
                SVMXC__Service_Order_Line__c wdl = wdWrapOBj.wdObj;
                wdlListUpdate.add(wdl);
            }
            update wdlListUpdate;
            sc.save();
            PageReference pg = new PageReference('/apex/FOAReviewPage?id='+woObj.Id);
            pg.setRedirect(true);
            return pg;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getmessage()));
            return null;
        }
    }
    
    //Launch Billing Review VF Page
    public PageReference billReview(){
        if(woObj.id != null){
            PageReference pg = new PageReference('/apex/BillingReviewPage?id='+woObj.id);
        	return pg;
        }else{
            PageReference pg = new PageReference('/apex/BillingReviewPage');
        	return pg;
        	}
    	}
    
    
    //Allow Simulation Button to be shown where WO is Submitted/Error/Reviewed, and Interface Status is Processed or Error.  Button blocked for Do Not Process WOs.
    public String getDisplaySumulateBtn(){
        if((woObj.SVMXC__Order_Status__c == 'Submitted' || woObj.SVMXC__Order_Status__c == 'Error' || woObj.SVMXC__Order_Status__c == 'Reviewed') && (woObj.Interface_Status__c == 'Processed' || woObj.Interface_Status__c == 'Error') && woObj.ERP_Status__c != null){ 
            return '';
        }else{
            return 'none';
        }
    }
    
    //Simulation - Allowed for Submitted/Error based on button show/hide criteria.  Button blocked for Do Not Process WOs.
    public PageReference simulate(){
        try{
            woObj.SVMXC__Order_Status__c = 'Submitted';
            woObj.ERP_Status__c = null;
            woObj.Interface_Status__c = null;
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(SVMXC__Service_Order_Line__c wdWrapOBj : wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c')){
               wdWrapOBj.Billing_Type__c = null;
               wdlListUpdate.add(wdWrapOBj);
            }
            update wdlListUpdate;
            sc.save();
            PageReference pg = Page.FOAReviewPage;
            pg.setRedirect(true);
            return pg;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
            return null;
        }
    }
    
    //Allow Review Complete when WO Status is Submitted and Interface Status is Processed successfully, or where Interface Status is Do Not Process - all lines OJT
    public String getDisplayReviewCompletebtn(){
        if(woObj.SVMXC__Order_Status__c == 'Submitted' && ((woObj.Interface_Status__c == 'Processed' && woObj.ERP_Status__c == 'Success') || (woObj.Interface_Status__c == 'Do Not Process'))){ 
            return '';
        }else{
            return 'none';
        }
    }    
    
    //Update this with logic similar to "Review Complete" button and "MultiReviewCompleteController" controller
    public PageReference reviewComplete(){
        Integer WDL_Unprocessed_Count = 0;
        String BST = woObj.BST__c;
                
        for(SVMXC__Service_Order_Line__c wdlObj : woObj.SVMXC__Service_Order_Line__r){
                String WDL_IF_Status = wdlObj.Interface_Status__c;
                String WDL_Record_Type = wdlObj.RecordType.Name;
                String WDL_Line_Type = wdlObj.SVMXC__Line_Type__c;

                if (WDL_Record_Type == 'Usage/Consumption'
                        && WDL_IF_Status != 'Processed'
                        && WDL_Line_Type != 'Miscelleaneous'
                        && WDL_IF_Status != 'Do not process') {
                    WDL_Unprocessed_Count = WDL_Unprocessed_Count + 1;
                }
            }    
        
        //Brazil WOs with Parts cannot go to Approved without Parts Quote Status and Parts Quote Number
        if (woObj.RecordTypeId == fsRecordTypeId && woObj.Sales_Org__c == '4400' && woObj.Parts_Work_Detail_Count__c > 0 && woObj.Billing_Review_Required__c == false && (woObj.Parts_Quote_Status__c == null || woObj.Parts_Quote_Status__c == 'Pending Request' || woObj.Parts_Quote_Status__c == 'Requested')){
            ApexPages.Message brazilMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Brazil FOA:  Parts are included on this Work Order but Parts Quote Status is not completed.  Please enter Parts Quote Status and Parts Quote Number before proceeding.');
            ApexPages.addMessage(brazilMsg);
            return null;
        }
        //Brazil WOs with Parts cannot go to Approved without Parts Quote Status and Parts Quote Number
        if (woObj.RecordTypeId == fsRecordTypeId && woObj.Sales_Org__c == '4400' && woObj.Parts_Work_Detail_Count__c > 0 && woObj.Billing_Review_Required__c == false && woObj.Parts_Quote_Status__c == 'Received' && String.isBlank(woObj.Parts_Quote_Number__c)){
            ApexPages.Message brazilMsgQuote = new ApexPages.Message(ApexPages.Severity.ERROR,'Brazil FOA:  Parts Quote Status is set to Received but no Parts Quote # is entered.  Please correct before finalizing Review.');
            ApexPages.addMessage(brazilMsgQuote);
            return null;
        }
        //WO Status cannot go to Reviewed if Escalation is required but not created on any Case or WO.
        if (woObj.Is_escalation_to_the_CLT_required__c == 'Yes from Work Order' && (woObj.Case_ECF_Count__c == null || woObj.Case_ECF_Count__c < 1)) {
            ApexPages.Message cltMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Work Order is marked as Escalated Complaint Required, but there is no active Escalated Complaint Form (ECF) on the Case or Work Order. Please correct prior to changing Work Order Status.');
            ApexPages.addMessage(cltMsg);
            return null;
        }   
        //Standard Review Complete to Reviewed or Approved if Billing Review is false
        if (woObj.SVMXC__Order_Status__c == 'Submitted' && woObj.Interface_Status__c == 'Processed' && woObj.ERP_Status__c == 'Success' && WDL_Unprocessed_Count ==0){
            try{
                woObj.SVMXC__Order_Status__c = 'Reviewed';
                woObj.ERP_Status__c = 'Success';
                woObj.Interface_Status__c = 'Processed';
                //sc.save();
                update woObj;
                //Do Not redirect on Review Complete if billing review required so that BillingReview link button can be used on same ID.
                if(woObj.Billing_Review_Required__c == true){
                    PageReference pr = new PageReference('/apex/FOAReviewPage?id='+woObj.id);
                    pr.setRedirect(true);
                    return pr;
                }else{
                    PageReference pg = Page.FOAReviewPage;
                	pg.setRedirect(true);
                	return pg;
                }
            }catch(Exception ex){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
                return null;
            }
        }
        //Review Complete for OJT Work Orders go straight to Closed
        else if (woObj.SVMXC__Order_Status__c == 'Submitted' && woObj.Interface_Status__c == 'Do Not Process' && WDL_Unprocessed_Count ==0){
            if(woObj.RecordTypeId  == fsRecordTypeId ||
               (woObj.RecordTypeId == installRecordTypeId
                && woObj.Close__c == true)){
                try{
                    woObj.SVMXC__Order_Status__c = 'Closed';
                    woObj.ERP_Status__c = 'Success';
                    woObj.OwnerID = BST;
                    list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
                        for(SVMXC__Service_Order_Line__c wdWrapOBj : wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c')){
                            wdWrapOBj.SVMXC__Line_Status__c = 'Closed';
                            wdlListUpdate.add(wdWrapOBj);
                            }
                    //added woObj update instead of implicitly by wdlListUpdate so we get error message.  Need this in front of wdlListUpdate for lines to not go to Closed before DML error.
                    update woObj;
                    update wdlListUpdate;
                    //sc.save();
                    PageReference pg = Page.FOAReviewPage;
                   	pg.setRedirect(true);
                    return pg;
                }catch(Exception ex){
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,string.valueof(ex));
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }
            //IWO only closes if Close is true.  Otherwise it goes back to Assigned.
            if(woObj.RecordTypeId == installRecordTypeId && woObj.Close__c == false){
                try{
                    woObj.SVMXC__Order_Status__c = 'Assigned';
                    woObj.ERP_Status__c = null;
                    woObj.Interface_Status__c = null;
                    list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
                        for(SVMXC__Service_Order_Line__c wdWrapOBj : wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c')){
                            wdWrapOBj.SVMXC__Line_Status__c = 'Closed';
                            wdlListUpdate.add(wdWrapOBj);
                            }
                    update wdlListUpdate;
                    sc.save();
                    PageReference pg = Page.FOAReviewPage;
                    pg.setRedirect(true);
                    return pg;
                }catch(Exception ex){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
                    return null;
                }
            }else{
                return null;
                }
        }else{
            return null;
        }
    }
    
    //Allow Reassign if Submitted/Error and not in process of interfacing    
    public String getDisplayReassign(){
        if((woObj.SVMXC__Order_Status__c == 'Submitted' || woObj.SVMXC__Order_Status__c == 'Error') && woObj.Interface_Status__c != 'Process' && woObj.Interface_Status__c != 'Processing'){ 
            return '';
        }else{
            return 'none';
        }
    }    
    //Allow Reassign if Submitted/Error and not in process of interfacing
    public PageReference reassign(){
        try{
            woObj.SVMXC__Order_Status__c = 'Assigned';
            woObj.ERP_Status__c = null;
            woObj.Interface_Status__c = null;
            woObj.SVMXC__Dispatch_Response__c = 'Accepted';
            woObj.ERP_Service_Request_Nbr__c = null;
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(SVMXC__Service_Order_Line__c wdWrapOBj : wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c')){
                if(wdWrapOBj.SVMXC__Line_Status__c != 'Closed'){
                    wdWrapOBj.SVMXC__Line_Status__c = 'Open';
                }
                if(wdWrapOBj.Interface_Status__c != 'Do Not Process'){
                    wdWrapOBj.Interface_Status__c = null;
                }
               wdlListUpdate.add(wdWrapOBj);
            }
            update wdlListUpdate;
            sc.save();
            PageReference pg = Page.FOAReviewPage;
            pg.setRedirect(true);
            return pg;
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,ErrorMessageHandler.getMessageText(ex)));
            return null;
        }
    }  
    
    /*
     * Utility method to get child records with all fields
     */
    private list<Sobject> getSobjectChildRecords(String objectName, String recordId,String parentFieldName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        String query = 'SELECT';
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ', ';
        }
        /*
        if (query.subString(query.Length()-2,query.Length()-1) == ','){
            query = query.subString(0,query.Length()-2);
        }*/
        
        query += ' RecordType.DeveloperName '; 
        query += ' FROM ' + objectName;
        query += ' WHERE '+parentFieldName+' = :recordId '; 
        query += ' and SVMXC__Line_Status__c = \'Submitted\' ';
        //query += ' and SVMXC__Line_Type__c != \'Miscelleaneous\' ';
        //query += ' and Source_of_Parts__c != \'C\' ';
        query += ' and RecordType.DeveloperName = \'UsageConsumption\' ';
        //query += ' and (Part_Disposition__c  != \'Removing\' OR Part_Disposition__c  != \'Removed\' )';
        system.debug('Query ==='+query);
        List<Sobject> sobjectList = database.query(query);
        return sobjectList;
    }
    
    public PageReference dummyAction(){
        return null;
    }
}
@isTest
public class FOAReviewExtensionTest {

    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static Account newAcc;
    public static Case newCase;
    public static Case newCase1;
    public static Case newCase2;
    public static Case newCase3;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order__c WO1;
    public static SVMXC__Service_Order__c WO2;
    public static SVMXC__Service_Order__c WO3;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static SVMXC__Service_Order_Line__c WD2;
    public static SVMXC__Service_Order_Line__c WD4;
    public static SVMXC__Service_Order_Line__c WD5;
    public static List < SVMXC__Service_Order_Line__c > WDList;
    public static CountryDateFormat__c dateFormat;
    public static string BST;

    Static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.User__c = userInfo.getUserId();
        insert technician;

        //get Account from UnityDataTestClass
        newAcc = UnityDataTestClass.AccountData(true);

        //CountryDateFormat__c custom setting data creation
        dateFormat = new CountryDateFormat__c(Name = 'Default', Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat;

        //insert case
        Case newCase = new Case();
        newCase.AccountId = newAcc.Id;
        newCase.Status = 'Pending Action by Varian';
        newCase.Priority = 'Low';
        newCase.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase;

        //create and insert Work Order

        WO = new SVMXC__Service_Order__c();
        WO.SVMXC__Case__c = newCase.id;
        WO.SVMXC__Company__c = newAcc.id;
        WO.SVMXC__Order_Status__c = 'Submitted';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.SVMXC__Group_Member__c = technician.id;
        WO.Subject__c = 'test subject';
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now() + 3;
        WO.SVMXC__Preferred_Start_Time__c = system.now();
        WO.Interface_Status__c = 'Processed';
        WO.BST__c = BST;
        WO.ERP_Status__c = 'Success';
        WO.Service_Team__c = technician.SVMXC__Service_Group__c;
        insert WO;

        //create and insert Work Detail
        /*
        WDList = new List<SVMXC__Service_Order_Line__c>();
        WD = new SVMXC__Service_Order_Line__c();
        WD.SVMXC__Line_Type__c = 'Labor';
        WD.SVMXC__Activity_Type__c = 'Configure';
        WD.SVMXC__Group_Member__c = technician.id;
        WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        WD.SVMXC__Work_Description__c = 'test';
        WD.SVMXC__Start_Date_and_Time__c =System.now();
        WD.SVMXC__End_Date_and_Time__c = System.now().addMinutes(60);
        WD.Actual_Hours__c = 1;
        WD.SVMXC__Service_Order__c = WO.id;
        WD.SVMXC__Line_Status__c = 'Submitted';
        WD.Interface_Status__c = 'Processed';
        insert WD;
        */
    }
    //run general test
    @isTest public static void testFOAReview() {
        Test.StartTest();

        WD1 = new SVMXC__Service_Order_Line__c();
        WD1.SVMXC__Line_Type__c = 'Labor';
        WD1.SVMXC__Activity_Type__c = 'Configure';
        WD1.SVMXC__Group_Member__c = technician.id;
        WD1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        WD1.SVMXC__Work_Description__c = 'test';
        WD1.SVMXC__Start_Date_and_Time__c = System.now().addMinutes(90);
        WD1.SVMXC__End_Date_and_Time__c = System.now().addMinutes(120);
        WD1.Actual_Hours__c = 1;
        WD1.SVMXC__Service_Order__c = WO.id;
        WD1.SVMXC__Line_Status__c = 'Submitted';
        WD1.Interface_Status__c = 'Processed';
        insert WD1;

        ApexPages.StandardController sc = new ApexPages.StandardController(WO);
        FOAReviewExtension extension = new FOAReviewExtension(sc);
        for (FOAReviewExtension.WorkDetailWrapper workDetailWrap2: extension.wdlWrapperList) {
            workDetailWrap2.selectBox = true;
        }
        extension.getIsValidWo();
        extension.dummyAction();
        //needs @testvisible - method private in controller extension.initializeData();
        extension.getShowWoValidateMessage();
        ApexPages.currentPage().getParameters().put('field', 'ByLineType');
        extension.sortByColumns();
        FOAReviewExtension.WorkDetailWrapper workDetailWrap2 = new FOAReviewExtension.WorkDetailWrapper(WD1, true);
        workDetailWrap2.compareTo(workDetailWrap2);
        ApexPages.currentPage().getParameters().put('field', 'ByStartDate');
        extension.sortByColumns();
        FOAReviewExtension.WorkDetailWrapper workDetailWrap = new FOAReviewExtension.WorkDetailWrapper(WD1, true);
        workDetailWrap.compareTo(workDetailWrap);
        extension.Simulate();
        extension.Reassign();
        extension.Save();
        extension.reviewComplete();
        extension.getDisplaySaveBtn();
        extension.getDisplaySumulateBtn();
        extension.billReview();
        extension.getDisplayReviewCompletebtn();
        extension.getDisplayReassign();

        Test.stopTest();
    }

    //ReviewComplete failure checks
    @isTest public static void testReviewCompleteBrazil() {
        Test.StartTest();

        Case newCase1 = new Case();
        newCase1.AccountId = newAcc.Id;
        newCase1.Status = 'Pending Action by Varian';
        newCase1.Description = 'test';
        newCase1.Reason = 'Information';
        newCase1.Priority = 'Low';
        newCase1.Is_This_a_Complaint__c = 'No';
        newCase1.Was_anyone_injured__c = 'No';
        newCase1.Is_escalation_to_the_CLT_required__c = 'No';
        newCase1.Ownerid = userinfo.getuserid();
        newCase1.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase1;

        WO1 = new SVMXC__Service_Order__c();
        WO1.SVMXC__Case__c = newCase1.id;
        WO1.Event__c = True;
        WO1.SVMXC__Company__c = newAcc.id;
        WO1.SVMXC__Order_Status__c = 'Submitted';
        WO1.SVMXC__Preferred_Technician__c = technician.id;
        WO1.SVMXC__Group_Member__c = technician.id;
        WO1.Subject__c = 'test subject';
        WO1.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        WO1.SVMXC__Problem_Description__c = 'Test Description';
        WO1.SVMXC__Preferred_End_Time__c = system.now() + 3;
        WO1.SVMXC__Preferred_Start_Time__c = system.now();
        WO1.Interface_Status__c = 'Processed';
        WO1.ERP_Status__c = 'Success';
        WO1.Sales_Org__c = '4400';
        WO1.Billing_Review_Required__c = False;
        WO1.Parts_Quote_Status__c = null;
        WO1.Parts_Quote_Number__c = '';
        WO1.BST__c = '00GE0000003xGorMAE';
        insert WO1;

        WD2 = new SVMXC__Service_Order_Line__c();
        WD2.SVMXC__Line_Type__c = 'Part';
        WD2.SVMXC__Activity_Type__c = 'Configure';
        WD2.SVMXC__Group_Member__c = technician.id;
        WD2.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD2.SVMXC__Work_Description__c = 'test';
        WD2.SVMXC__Start_Date_and_Time__c = System.now();
        WD2.SVMXC__Service_Order__c = WO1.id;
        WD2.SVMXC__Line_Status__c = 'Submitted';
        WD2.Interface_Status__c = 'Processed';
        insert WD2;

        String WDL_IF_Status = WD2.Interface_Status__c;
        String WDL_Record_Type = WD2.RecordTypeId;
        String WDL_Line_Type = WD2.SVMXC__Line_Type__c;

        ApexPages.StandardController sc = new ApexPages.StandardController(WO1);
        FOAReviewExtension extension = new FOAReviewExtension(sc);

        try {
            WO1.Parts_Quote_Status__c = null;
            extension.reviewComplete();
            //ApexPages.Message[] pageMessages = ApexPages.getMessages();
            //System.assertNotEquals(0, pageMessages.size());
        } catch (Exception e) {
            System.assertEquals(true, e.getMessage().Contains('Parts Quote Status'));
        }

        try {
            WO1.Parts_Quote_Status__c = 'Received';
            extension.reviewComplete();
            //ApexPages.Message[] pageMessages = ApexPages.getMessages();
            //System.assertNotEquals(0, pageMessages.size());
        } catch (Exception e) {
            System.assertEquals(true, e.getMessage().Contains('no Parts Quote # is entered'));
        }

        try {
            WO1.Is_escalation_to_the_CLT_required__c = 'Yes from Work Order';
            extension.reviewComplete();
        } catch (Exception e) {
            System.assertEquals(true, e.getMessage().Contains('Escalation'));
        }

        System.assertNotEquals(Null, WDL_IF_Status);
        System.assertNotEquals(Null, WDL_Record_Type);
        System.assertNotEquals(Null, WDL_Line_Type);

        Test.stopTest();
    }

    //IWO & OJT True
    @isTest public static void testOJTCloseTrue() {
        Test.StartTest();

        Case newCase2 = new Case();
        //newCase2.AccountId = newAcc.Id;
        newCase2.Status = 'Pending Action by Varian';
        newCase2.Description = 'test';
        newCase2.Reason = 'Information';
        newCase2.Priority = 'Low';
        newCase2.Is_This_a_Complaint__c = 'No';
        newCase2.Was_anyone_injured__c = 'No';
        newCase2.Is_escalation_to_the_CLT_required__c = 'No';
        //newCase2.OwnerId = userinfo.getuserid();
        newCase2.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase2;

        SVMXC__Service_Order__c WO2 = new SVMXC__Service_Order__c();
        WO2.SVMXC__Case__c = newCase2.id;
        //WO2.SVMXC__Company__c = newAcc.id;
        WO2.SVMXC__Order_Status__c = 'Submitted';
        //WO2.OwnerId = userinfo.getuserid();
        WO2.Interface_Status__c = 'Do Not Process';
        WO2.SVMXC__Group_Member__c = technician.id;
        WO2.Subject__c = 'test subject';
        WO2.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        WO2.Sales_Org__c = '0600';
        WO2.Close__c = True;
        WO2.BST__c = '00GE0000003xGorMAE';
        insert WO2;
        /*
        WD4 = new SVMXC__Service_Order_Line__c();
        WD4.SVMXC__Line_Type__c = 'Labor';
        WD4.SVMXC__Activity_Type__c = 'Configure';
        WD4.Interface_Status__c = 'Do Not Process';
        WD4.SVMXC__Group_Member__c = technician.id;
        WD4.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD4.SVMXC__Work_Description__c = 'test';
        WD4.SVMXC__Start_Date_and_Time__c = System.now().addMinutes(150);
        WD4.SVMXC__End_Date_and_Time__c = System.now().addMinutes(180);
        WD4.Actual_Hours__c = 1;
        WD4.SVMXC__Service_Order__c = WO2.id;
        WD4.SVMXC__Line_Status__c = 'Submitted';
        insert WD4;
        */

        ApexPages.StandardController sc = new ApexPages.StandardController(WO2);
        FOAReviewExtension extension = new FOAReviewExtension(sc);

        try {
            extension.reviewComplete();
            //System.assertEquals('Closed',WO2.SVMXC__Order_Status__c);
            //System.assertEquals('Success',WO2.ERP_Status__c);
            System.assertEquals('Do Not Process', WO2.Interface_Status__c);
            //System.assertEquals('Closed',WD4.SVMXC__Line_Status__c);
        } catch (Exception e) {
            System.assertEquals('Submitted', WO2.SVMXC__Order_Status__c);
        }

        Test.stopTest();

    }

    //IWO & OJT False
    @isTest public static void testOJTCloseFalse() {
        Test.StartTest();

        Case newCase3 = new Case();
        //newCase2.AccountId = newAcc.Id;
        newCase3.Status = 'Pending Action by Varian';
        newCase3.Description = 'test';
        newCase3.Reason = 'Information';
        newCase3.Priority = 'Low';
        newCase3.Is_This_a_Complaint__c = 'No';
        newCase3.Was_anyone_injured__c = 'No';
        newCase3.Is_escalation_to_the_CLT_required__c = 'No';
        //newCase2.OwnerId = userinfo.getuserid();
        newCase3.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase3;

        WO3 = new SVMXC__Service_Order__c();
        WO3.SVMXC__Case__c = newCase3.id;
        WO3.SVMXC__Company__c = newAcc.id;
        WO3.SVMXC__Order_Status__c = 'Submitted';
        WO3.Interface_Status__c = 'Do Not Process';
        WO3.SVMXC__Group_Member__c = technician.id;
        WO3.Subject__c = 'test subject';
        WO3.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO3.Sales_Org__c = '0600';
        WO3.Close__c = False;
        WO3.BST__c = '00GE0000003xGorMAE';
        insert WO3;
        /*
        WD5 = new SVMXC__Service_Order_Line__c();
        WD5.SVMXC__Line_Type__c = 'Labor';
        WD5.SVMXC__Activity_Type__c = 'Configure';
        WD5.Interface_Status__c = 'Do Not Process';
        WD5.SVMXC__Group_Member__c = technician.id;
        WD5.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD5.SVMXC__Work_Description__c = 'test';
        WD5.SVMXC__Start_Date_and_Time__c = System.now().addMinutes(190);
        WD5.SVMXC__End_Date_and_Time__c = System.now().addMinutes(220);
        WD5.Actual_Hours__c = 1;
        WD5.SVMXC__Service_Order__c = WO3.id;
        WD5.SVMXC__Line_Status__c = 'Submitted';
        insert WD5;
        */

        ApexPages.StandardController sc = new ApexPages.StandardController(WO3);
        FOAReviewExtension extension = new FOAReviewExtension(sc);

        try {
            extension.reviewComplete();
            //System.assertEquals('Assigned',WO3.SVMXC__Order_Status__c);
            //System.assertEquals(null,WO3.Interface_Status__c);
            System.assertEquals(null, WO3.ERP_Status__c);
            //System.assertEquals('Closed',WD5.SVMXC__Line_Status__c);
        } catch (Exception e) {
            System.assertEquals('Submitted', WO3.SVMXC__Order_Status__c);
        }

        Test.stopTest();
    }
    
    //IWO & OJT False
    @isTest public static void testWOCloseTrue() {
        Test.StartTest();

        Case newCase3 = new Case();
        //newCase2.AccountId = newAcc.Id;
        newCase3.Status = 'Pending Action by Varian';
        newCase3.Description = 'test';
        newCase3.Reason = 'Information';
        newCase3.Priority = 'Low';
        newCase3.Is_This_a_Complaint__c = 'No';
        newCase3.Was_anyone_injured__c = 'No';
        newCase3.Is_escalation_to_the_CLT_required__c = 'No';
        //newCase2.OwnerId = userinfo.getuserid();
        newCase3.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase3;

        WO3 = new SVMXC__Service_Order__c();
        WO3.SVMXC__Case__c = newCase3.id;
        WO3.SVMXC__Company__c = newAcc.id;
        WO3.SVMXC__Order_Status__c = 'Submitted';
        WO3.Interface_Status__c = 'Do Not Process';
        WO3.SVMXC__Group_Member__c = technician.id;
        WO3.Subject__c = 'test subject';
        WO3.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO3.Sales_Org__c = '0600';
        WO3.Close__c = true;
        WO3.BST__c = '00GE0000003xGorMAE';
        insert WO3;
        
        
        WD5 = new SVMXC__Service_Order_Line__c();
        WD5.SVMXC__Line_Type__c = 'Labor';
        WD5.SVMXC__Activity_Type__c = 'Configure';
        WD5.Interface_Status__c = 'Do Not Process';
        WD5.SVMXC__Group_Member__c = technician.id;
        WD5.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD5.SVMXC__Work_Description__c = 'test';
        WD5.SVMXC__Start_Date_and_Time__c = System.now().addMinutes(190);
        WD5.SVMXC__End_Date_and_Time__c = System.now().addMinutes(220);
        WD5.Actual_Hours__c = 1;
        WD5.SVMXC__Service_Order__c = WO3.id;
        WD5.SVMXC__Line_Status__c = 'Submitted';
        insert WD5;
        
        FOAReviewExtension.WorkDetailWrapper wdlWrap = new FOAReviewExtension.WorkDetailWrapper(WD5,true);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(WO3);
        FOAReviewExtension extension = new FOAReviewExtension(sc);
        extension.woObj = WO3;
        extension.wdlWrapperList.add(wdlWrap);
        try {
            extension.reviewComplete();
            extension.save();
            //System.assertEquals('Assigned',WO3.SVMXC__Order_Status__c);
            //System.assertEquals(null,WO3.Interface_Status__c);
            //System.assertEquals('Closed',WD5.SVMXC__Line_Status__c);
        } catch (Exception e) {
            System.assertEquals('Submitted', WO3.SVMXC__Order_Status__c);
        }

        Test.stopTest();
    }
    
    //IWO & OJT False
    @isTest public static void testWOCloseFalse() {
        Test.StartTest();

        Case newCase3 = new Case();
        //newCase2.AccountId = newAcc.Id;
        newCase3.Status = 'Pending Action by Varian';
        newCase3.Description = 'test';
        newCase3.Reason = 'Information';
        newCase3.Priority = 'Low';
        newCase3.Is_This_a_Complaint__c = 'No';
        newCase3.Was_anyone_injured__c = 'No';
        newCase3.Is_escalation_to_the_CLT_required__c = 'No';
        //newCase2.OwnerId = userinfo.getuserid();
        newCase3.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        insert newCase3;

        WO3 = new SVMXC__Service_Order__c();
        WO3.SVMXC__Case__c = newCase3.id;
        WO3.SVMXC__Company__c = newAcc.id;
        WO3.SVMXC__Order_Status__c = 'Submitted';
        WO3.Interface_Status__c = 'Do Not Process';
        WO3.SVMXC__Group_Member__c = technician.id;
        WO3.Subject__c = 'test subject';
        WO3.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO3.Sales_Org__c = '0600';
        WO3.Close__c = false;
        WO3.BST__c = '00GE0000003xGorMAE';
        insert WO3;
        
        
        WD5 = new SVMXC__Service_Order_Line__c();
        WD5.SVMXC__Line_Type__c = 'Labor';
        WD5.SVMXC__Activity_Type__c = 'Configure';
        WD5.Interface_Status__c = 'Do Not Process';
        WD5.SVMXC__Group_Member__c = technician.id;
        WD5.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD5.SVMXC__Work_Description__c = 'test';
        WD5.SVMXC__Start_Date_and_Time__c = System.now().addMinutes(190);
        WD5.SVMXC__End_Date_and_Time__c = System.now().addMinutes(220);
        WD5.Actual_Hours__c = 1;
        WD5.SVMXC__Service_Order__c = WO3.id;
        WD5.SVMXC__Line_Status__c = 'Submitted';
        insert WD5;
        
        

        ApexPages.StandardController sc = new ApexPages.StandardController(WO3);
        FOAReviewExtension extension = new FOAReviewExtension(sc);
        extension.woObj = WO3;
        try {
            extension.reviewComplete();
            extension.reassign();
            extension.woObj.SVMXC__Order_Status__c = 'Assigned';
            extension.getIsValidWo();
            extension.getShowWoValidateMessage();
            extension.woObj.Id = null;
            extension.getShowWoValidateMessage();
            extension.woObj.SVMXC__Order_Status__c = 'Submitted';
            extension.woObj.Interface_Status__c = 'Processed';
            
            //System.assertEquals('Assigned',WO3.SVMXC__Order_Status__c);
            //System.assertEquals(null,WO3.Interface_Status__c);
            //System.assertEquals('Closed',WD5.SVMXC__Line_Status__c);
        } catch (Exception e) {
            System.assertEquals('Submitted', WO3.SVMXC__Order_Status__c);
        }

        Test.stopTest();
    }
}
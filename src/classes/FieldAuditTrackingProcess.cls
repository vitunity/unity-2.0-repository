/***********************************************************************
* Author         : Varian
* Description	 : FieldAuditTrackingProcess which copies history tracking records to common custom object Field Audit Track
* User Story     : 
* Created On     : 4/22/16
************************************************************************/

global without sharing class FieldAuditTrackingProcess implements Database.Batchable<sObject>, Schedulable {
	
   	@TestVisible final String SELECT_CLAUSE = 'Select OldValue, NewValue, Id, Field, CreatedDate, CreatedById, ', FROM_CLAUSE = ' From ';
    public List<TrackingWrapper> fieldsTracked {get; set;}
    public Integer currentPosition {get; set;}
    public TrackingWrapper currentObject {get; set;}
    
    global FieldAuditTrackingProcess() {
    	
    }
    
    global FieldAuditTrackingProcess(List<TrackingWrapper> fieldss, Integer pos) {
    	//System.debug(logginglevel.info, 'constructor called  ========== '+pos);
    	currentPosition = pos;
    	if(fieldss !=null && fieldss.size() > 0) {
    		fieldsTracked = fieldss;
    	} else {
    		this.init();	
    	}
    	//System.debug(logginglevel.info, 'fieldsTracked ========== '+fieldsTracked);
    	if(fieldsTracked != null && fieldsTracked.size() > 0) {
    		currentObject = fieldsTracked[currentPosition];	
    		//System.debug(logginglevel.info, 'currentObject ========== '+currentObject);
    	}
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = '';
		//System.debug(logginglevel.info, 'currentObject ========== '+currentObject);
		Date queryDate = null;
		if (!currentObject.isProcessed && currentObject.sobj.Track_Fields__c) {
			query = SELECT_CLAUSE +''+ currentObject.sobj.ParentField__c+FROM_CLAUSE+currentObject.sobj.HistoryTableName__c;
	    	//System.debug(logginglevel.info, 'queryDate  ========== '+queryDate);
	    	//TODO - change no of days to 1 or 2 days for delta instead of full scan
	    	if (currentObject.sobj.NumberofDays__c != null && currentObject.sobj.NumberofDays__c > 0) {
	    		queryDate = System.Today().addDays( - Integer.valueOf(currentObject.sobj.NumberofDays__c));
	    		query = query + ' where CreatedDate > :queryDate';
	    	}
	    	//System.debug(logginglevel.info, 'Query ========== '+query);	
		}		
    	return String.isNotBlank(query) ? Database.getQueryLocator(query)  : null;
    	//return null;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Field_Audit_Trail__c> trails = new List<Field_Audit_Trail__c>();
		Id objId = String.ValueOf(scope[0].get(currentObject.sobj.ParentField__c));
		//System.debug(logginglevel.info, 'objId ========== '+objId);
		String objectName = objId.getSObjectType().getDescribe().getName();
		//System.debug(logginglevel.info, 'objectName ========== '+objectName);	
		for (sObject obj : scope) {
			Field_Audit_Trail__c trail = new Field_Audit_Trail__c(CreatedByID__c = String.ValueOf(obj.get('CreatedById')), CreatedDate__c =Test.isRunningTest() ? System.now(): (Datetime) obj.get('CreatedDate'), 
				Field__c = String.ValueOf(obj.get('Field')), NewValue__c = String.ValueOf(obj.get('NewValue')), OldValue__c = String.ValueOf(obj.get('OldValue')), 
				sObjectID__c = String.ValueOf(obj.get(currentObject.sobj.ParentField__c)), SObjectAPIName__c = objectName, FieldHistoryId__c = String.ValueOf(obj.get('Id')));
			trails.add(trail);
			//System.debug(logginglevel.info, 'trail ========== '+trail);		
		}
		if(trails.size() > 0 ) {
			upsert trails ; //FieldHistoryId__c
			//System.debug(logginglevel.info, 'trail size ========== '+trails.size());
			//System.debug(logginglevel.info, 'trail first ========== '+trails[0]);	
		}
		
    }
    
    global void finish(Database.BatchableContext BC) {
    	this.executeFutureSchedule();
    }
    
    @TestVisible  private void executeFutureSchedule() {
    	this.currentObject.isProcessed = true;
    	this.currentPosition++;
		if(currentPosition < fieldsTracked.size()) { //fieldsTracked.size()
			//System.debug(logginglevel.info, 'scheduling next processing histories for  ========== '+fieldsTracked[currentPosition]);
			Database.executeBatch(new FieldAuditTrackingProcess(fieldsTracked, currentPosition), 2000);	
		}			
    }
    
    global void execute(SchedulableContext ctx) {
    	Database.executeBatch(new FieldAuditTrackingProcess(new List<TrackingWrapper>(), 0), 2000);	
    }
    
    @TestVisible private void init() {
    	this.fieldsTracked = new List<TrackingWrapper>();
    	for(FieldTrackingConfig__c mdata : [Select Name, SObjectAPIName__c, HistoryTableName__c, NumberofDays__c, Track_Fields__c, ParentField__c From FieldTrackingConfig__c where Track_Fields__c = true order by SObjectAPIName__c asc]) {
    		this.fieldsTracked.add(new TrackingWrapper(mdata, false));			
    	}		
    }
    
    global class TrackingWrapper {	
    	public FieldTrackingConfig__c sobj {get; set;}
    	public boolean isProcessed {get; set;}
    	
    	public TrackingWrapper(FieldTrackingConfig__c setting, boolean p) {
    		this.sobj = setting;
    		this.isProcessed = p; 	
    	}
    }
}
/***********************************************************************
* Author         : Varian
* Description	 : Test class for FieldAuditTrackingProcess
* User Story     : 
* Created On     : 5/27/16
************************************************************************/

@isTest
private class FieldAuditTrackingProcess_Test {
	
	static String CRON_EXP = '0 0 * 1 * ?';
	
	@testSetup static void setup() {
		System.runAs(TestDataFactory.createUser('System Administrator')) {
			TestDataFactory.createAccounts();
			TestDataFactory.createFieldTrackingConfig();	
		}			
	}
	
	@isTest static void testHistoryDataJobSchedule() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			Test.startTest();
			Id jobId = System.schedule('CopyHistoryDataJob', CRON_EXP, new FieldAuditTrackingProcess());
			System.debug(logginglevel.info, 'jobId ===== '+jobId);
			System.assert(jobId != null);
			List<AsyncApexJob> jobs = [Select a.Status, a.MethodName, a.JobType, a.Id, a.CompletedDate, a.ApexClassId From AsyncApexJob a where a.ApexClass.Name LIKE '%FieldAuditTrackingProcess%'];
			System.assert(jobs.size() > 0);
			Test.stopTest();	
		}
	}
	
	//Note - History tracking object not available in unit test, so the following is just a workaround
	@isTest static void testExecuteBatchJob() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			Test.startTest();
			Database.executeBatch(new FieldAuditTrackingProcess(new List<FieldAuditTrackingProcess.TrackingWrapper>(), 0));
			Test.stopTest();
			List<sObject> histories = new List<sObject>();
			for (Account acc : [Select Id, Name From Account Limit 100]) {
				//Select a.OldValue, a.NewValue, a.Id, a.Field, a.AccountId From AccountHistory a
				sObject temp = new AccountHistory(Field = 'created', AccountId = acc.id);
				temp.put('id', acc.id);
				histories.add(temp);	
			}
			FieldAuditTrackingProcess proces = new FieldAuditTrackingProcess(new List<FieldAuditTrackingProcess.TrackingWrapper>(), 0);
			proces.init();
			System.assert(proces.fieldsTracked.size() > 0);
			System.assertEquals(proces.currentPosition, 0);
			System.assert(proces.currentObject != null);
			proces.execute(null, histories);
			List<Field_Audit_Trail__c> audits = [Select Id, sObjectID__c From  Field_Audit_Trail__c Limit 100];
			System.debug(logginglevel.info, 'History Rows ===== '+audits);
			System.assert(audits.size() > 0);
			
			proces = new FieldAuditTrackingProcess(new List<FieldAuditTrackingProcess.TrackingWrapper>{ new FieldAuditTrackingProcess.TrackingWrapper(new FieldTrackingConfig__c(), false)}, 0);
		}	
	}
	
	@isTest static void testFutureBatchJob() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			Test.startTest();
			FieldAuditTrackingProcess proces = new FieldAuditTrackingProcess(new List<FieldAuditTrackingProcess.TrackingWrapper>(), 0);
			proces.executeFutureSchedule();
			List<AsyncApexJob> jobs = [Select a.Status, a.MethodName, a.JobType, a.Id, a.CompletedDate, a.ApexClassId From AsyncApexJob a where a.ApexClass.Name LIKE '%FieldAuditTrackingProcess%'];
			Test.stopTest();	
		}
	}
	    
}
/***********************************************************************
* Author         : Varian
* Description	 : Configuration controller for saving metadata information about objects/fields for history tracking purpose
* User Story     : 
* Created On     : 4/28/16
************************************************************************/

public without sharing class FieldTrackingConfigCtrl {
	@TestVisible static Set<String> SYSTEM_FIELDS = new Set<String>{'createddate', 'lastmodifieddate', 'lastmodifiedbyid', 'createdbyid'};
	@TestVisible static String CRON_EXP = '0 0 * 1 * ?';
	
    public List<List<SelectOption>> selectObjectsOptions {get; set;}
    public String selectedObject {get; set;}
    public List<FieldWrapper> objFields {get; set;}
    public boolean disableDelete {get; set;}
    public boolean enableFieldTracking {get; set;}
    //public Criteria qualityCriteria {get; set;}
    //public List<SelectOption> selectFieldOption {get; set;}
    @TestVisible private transient FieldTrackingConfig__c config;
    @TestVisible private boolean isCustomObj;
    public Integer numberOfDays {get; set;}
    public boolean isCompGenerated {get; set;}
    public boolean hasMore {get; set;}
    
    public FieldTrackingConfigCtrl() {
    	this.descibeOrgObjects();	
    }
    
    private void descibeOrgObjects() {
    	this.selectObjectsOptions = new List<List<SelectOption>> {  new List<SelectOption>{ new SelectOption('', '-- None --') },  new List<SelectOption>{}, new List<SelectOption>{} };
    	Integer i =0, index = 0;
    	List<SelectOption> temp = new List<SelectOption>{new SelectOption('', '-- None --')};
    	for(Schema.SObjectType sObj : Schema.getGlobalDescribe().values()) {
    		Schema.DescribeSObjectResult objResults = sObj.getDescribe();
    		if (!objResults.isCustomSetting()) { //  objResults.isUpdateable() && objResults.isAccessible()
    			if (objResults.getName().contains('__kav')) { //skip article objects
    				continue;
    			}
    			if(this.selectObjectsOptions[index].size() == 1000) {
    				index++;	
    			}
    			this.selectObjectsOptions[index].add(new SelectOption(objResults.getName(), objResults.getLabel()));	
    		}			
    	}
    	//System.debug(logginglevel.info, 'selectObjectsOptions[0] == '+selectObjectsOptions[0].size());
    	//System.debug(logginglevel.info, 'selectObjectsOptions[1] == '+selectObjectsOptions[1].size());	
    }
    
    public void describeSelectedObject() {
    	this.objFields = new List<FieldWrapper>();
    	this.disableDelete = false;
    	this.enableFieldTracking = false;
    	this.numberOfDays = 0;
    	this.isCustomObj = false;
    	this.isCompGenerated = false;
    	if (String.isBlank(this.selectedObject)) return;
    	//this.qualityCriteria = new Criteria('', '', '');
    	Map<String, FieldWrapper> existingConfig = this.getConfigData(this.selectedObject);
    	Schema.sObjectType userType = User.sObjectType;
    	//this.selectFieldOption = new List<SelectOption>{ new SelectOption(' ', '-- None --') };    	
    	for (Schema.DescribeSobjectResult objResult : Schema.describeSObjects(new List<String> {this.selectedObject})) {
    		this.isCustomObj = objResult.isCustom();
    		for (Schema.SObjectField result : objResult.fields.getMap().values()) {
    			Schema.DescribeFieldResult fResult = result.getDescribe();
    			//SYSTEM_FIELDS.contains(fResult.getName().toLowerCase())
    			Set <Schema.sObjectType> tempObjs = new Set<Schema.sObjectType>();
    			String lookupObjAPIName;
    			if ( fResult.getType().name() == 'REFERENCE') {
    				List<Schema.sObjectType> temp = fResult.getReferenceTo();
	    			if (temp != null && temp.size() > 0) {
	    				Schema.DescribeSObjectResult objTemp = temp[0].getDescribe();
	    				lookupObjAPIName = objTemp.getName();
	    			}			
    			}
    			if (existingConfig.containsKey(fResult.getName())) {
    				this.objFields.add(existingConfig.get(fResult.getName()));	
    			} else {
    				this.objFields.add(new FieldWrapper(fResult.getLabel(), fResult.getName(), false, fResult.getType().name(), lookupObjAPIName, false)); //fResult.getType().name()	
    			}
    			/*
    			if ( fResult.getType().name().equalsIgnoreCase('PICKLIST') || fResult.getType().name().equalsIgnoreCase('STRING') ||  fResult.getType().name().equalsIgnoreCase('BOOLEAN')) {
    				this.selectFieldOption.add(new SelectOption(fResult.getName(), fResult.getLabel()));	
    			}*/
    			
    		}		
    	}
    	this.objFields.sort();		
    }
    
    public PageReference saveConfig() {
    	List<FieldWrapper> selectedFields = new List<FieldWrapper>();
    	for(FieldWrapper fw : objFields) {
    		if(fw.isSelected) {
    			selectedFields.add(fw);	
    		}		
    	}
    	try {
    		FieldTrackingConfig__c config = new FieldTrackingConfig__c(SObjectAPIName__c = this.selectedObject, Name = this.selectedObject, JSONConfigData__c=JSON.serialize(selectedFields), 
    											DisableDelete__c=this.disableDelete, Track_Fields__c = this.enableFieldTracking, 
    											NumberofDays__c = this.numberOfDays, ComponentGenerated__c = this.isCompGenerated); //RecordCritiera__c= this.qualityCriteria.serializeData()
    		this.setPOC1Fields(config);
    		upsert config SObjectAPIName__c;
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Configuration Saved!'));	
    	} catch (Exception ex) {
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error Occured During Save!'));
    	}
    	return null;	
    }
    
    public PageReference scheduleApexProcess() {
    	try {
    		//Id batchId = Database.executeBatch(new FieldAuditTrackingProcess(null, 0), 2000);
    		Id batchId = System.schedule('FieldTrackingConfigCtrl_Job', CRON_EXP, new FieldAuditTrackingProcess(null, 0));
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Process Scheduled Successfully!'));	
			System.debug(logginglevel.info, 'batchId ========== '+batchId);
    		
    	} catch (Exception ex) {
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception occured - '+ex.getMessage()));	
    	}
    	return null;
    }
    
    private void setPOC1Fields(FieldTrackingConfig__c config) {
    	//System.debug(logginglevel.info, ' ========== Is Custom Object ============ '+isCustomObj);	
    	if (String.isBlank(config.HistoryTableName__c)) {
    		config.HistoryTableName__c = !this.isCustomObj ? this.selectedObject+'History' : this.selectedObject.removeEndIgnoreCase('__c')+'__History';	
    	}
    	if (String.isBlank(config.ParentField__c)) {
    		config.ParentField__c = !this.isCustomObj ? this.selectedObject+'Id' : 'ParentId';	
    	}	
    }
    
    private Map<String, FieldWrapper> getConfigData(String objName) {
    	Map<String, FieldWrapper> fieldsTracked = new Map<String, FieldWrapper>();
    	List<FieldTrackingConfig__c> configs = [Select Id, Name, JSONConfigData__c, SObjectAPIName__c, DisableDelete__c, Track_Fields__c, RecordCritiera__c, NumberofDays__c, 
    												ComponentGenerated__c, HistoryTableName__c, ParentField__c From FieldTrackingConfig__c where SObjectAPIName__c =:objName];
    	if (configs != null && configs.size()> 0) {
    		this.config = configs[0];
    		/* if (String.isNotBlank(configs[0].RecordCritiera__c)) {
    			this.qualityCriteria = (Criteria) JSON.deserialize(configs[0].RecordCritiera__c, Criteria.class);	
    		}*/
    		this.disableDelete = configs[0].DisableDelete__c;
    		this.enableFieldTracking = configs[0].Track_Fields__c;
    		this.numberOfDays =  (Integer) configs[0].NumberofDays__c;
    		this.isCompGenerated = configs[0].ComponentGenerated__c;
    		if (String.isNotBlank(configs[0].JSONConfigData__c)) {
	    		for (FieldWrapper fw : (List<FieldWrapper>) JSON.deserialize(configs[0].JSONConfigData__c, List<FieldWrapper>.class)) {
	    			fieldsTracked.put(fw.apiName, fw);
	    		}
    		} 		
    	}
    	return fieldsTracked;
    }
    
    
    
    
    public class FieldWrapper extends AnyComparable {
    	public String label{get; set;} 
    	public String apiName {get; set;}
    	public boolean isSelected {get; set;}
    	public String fieldType {get; set;}
    	public boolean isReadOnly {get; set;}
    	public String lookupObjAPI;
    	
    	public FieldWrapper(String lbl, String nam, boolean sel, String typ, String lookUp, boolean read) {
    		this.label = lbl;
    		this.apiName = nam;
    		this.isSelected = sel;
    		this.fieldType = typ;
    		this.lookupObjAPI = lookUp;
    		this.isReadOnly = read;
    	}
    	
    	public override String getCompareField() {
    		return this.label;	
    	}
    }
    
     
    //TODO - enabled when disable delete feature is considered
    public class Criteria {
    	public String fieldName {get; set;}
    	public String fieldValue {get; set;}
    	public String fieldOperator {get; set;}
    	
    	public Criteria(String fName, String fValue, String fOptr) {
    		this.fieldName = fName;
    		this.fieldValue = fValue;
    		this.fieldOperator = fOptr; 	
    	}
    	
    	public String serializeData() {
    		return String.isNotBlank(this.fieldName) && String.isNotBlank(this.fieldValue) && String.isNotBlank(this.fieldOperator) ? JSON.serialize(this) : '';   	
    	} 
    }
}
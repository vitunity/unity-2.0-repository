/***********************************************************************
* Author         : Varian
* Description	 : Test class for FieldTrackingConfigCtrl
* User Story     : 
* Created On     : 6/1/16
************************************************************************/
@isTest
private class FieldTrackingConfigCtrl_Test {
	
	@testSetup static void setup() {
		insert TestDataFactory.createUser('System Administrator');	
	}
	
	
	@isTest static void createNewEditConfiguration() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			FieldTrackingConfigCtrl controller = new FieldTrackingConfigCtrl();
			System.assert(controller.selectObjectsOptions.size() > 1);
			controller.selectedObject = 'Account';
			controller.describeSelectedObject();
			controller.numberOfDays = 10;
			controller.enableFieldTracking = true;
			//System.assert(controller.selectFieldOption.size() > 1);
			for (FieldTrackingConfigCtrl.FieldWrapper fw : controller.objFields) {
				fw.isSelected = true;	
			}
			controller.saveConfig();
			List<FieldTrackingConfig__c> configs = [Select Id, Name From FieldTrackingConfig__c where SObjectAPIName__c =:controller.selectedObject];
			System.assert(configs.size() > 0);
			
			controller = new FieldTrackingConfigCtrl();
			controller.selectedObject = 'Account';
			controller.describeSelectedObject();
			System.assert(controller.objFields.size() > 1, 'Selected Field Wrapper Objects is empty');
			System.assert(controller.numberOfDays == 10);
			System.assert(controller.enableFieldTracking);
			controller.objFields.sort();

			FieldTrackingConfigCtrl.Criteria criteria = new FieldTrackingConfigCtrl.Criteria ('Status', 'Closed', '=');
			System.assert(criteria.serializeData().length() > 0);
			
			AnyComparable compareObj = new AnyComparable();
			compareObj.getCompareField();
			 		
		}
	}
	
	
	@isTest static void testHistoryProcssSchedule() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			TestDataFactory.createAccounts();
			TestDataFactory.createFieldTrackingConfig();
			FieldTrackingConfigCtrl controller = new FieldTrackingConfigCtrl();
			Test.startTest();
			controller.scheduleApexProcess();
			List<AsyncApexJob> jobs = [Select a.Status, a.MethodName, a.JobType, a.Id, a.CompletedDate, a.ApexClassId From AsyncApexJob a where a.ApexClass.Name LIKE '%FieldAuditTrackingProcess%'];
			System.assert(jobs.size() > 0);
			Test.stopTest();
		}	
	}
	    
}
/***********************************************************************
* Author         : Varian
* Description	 : FieldTrackingCtrl component controller which loads records related to object from field aduit history object
* User Story     : 
* Created On     : 4/25/16
************************************************************************/

public without sharing class FieldTrackingCtrl {
    public static final Integer MAX_CHAR_SHOW {get; set;}
    static Map<String, String> specialObjectNames = new Map<String, String> {'case' => 'CaseNumber'};
	public List<FieldAuditWrapper> aduitTrails {get { if (aduitTrails == null) this.getRecords(); return this.aduitTrails;} set;}
	public Id sObjectId {get; set;}
	public String sObjectAPIName {get; set;}
	public String sObjectName {get; set;}
	public Map<String, String> fieldsMap {get; set;}
	public Integer resultsLimit {get; set; }
	public Integer showResultsLimit {get; set;}
	public boolean showMore {get; set;}
	public boolean isCustomObj;	
	public Map<Id, String> fieldsToName {get; set;}
	@TestVisible  private Set<String> lookupFields;
	@TestVisible  private Map<String, Set<Id>> lookupFieldsRef;
	
	static {
		FieldTrackingCtrl.MAX_CHAR_SHOW = 80;
	}
	
	/* public FieldTrackingCtrl(ApexPages.StandardController controller) { } */
	
	public FieldTrackingCtrl() {
		this.showResultsLimit = 1;
	}
    
    public void getRecords() {
    	this.descibeObject();
    	//System.debug(logginglevel.info, 'sObjectAPIName ===== '+sObjectAPIName);
    	aduitTrails = new List<FieldAuditWrapper>();
    	
    	List<List<sObject>> searchResults = [FIND :this.sObjectId RETURNING Field_Audit_Trail__c (Name, sObjectID__c, OldValue__c, NewValue__c, Id, Field__c, CreatedDate__c, CreatedByID__c, SObjectAPIName__c, 
    											CreatedByID__r.Name WHERE sObjectID__c=:this.sObjectId AND SObjectAPIName__c = :this.sObjectAPIName ORDER BY CreatedDate__c desc) LIMIT 1000];
    	List<Field_Audit_Trail__c>  audits = new List<Field_Audit_Trail__c>();
    	if (searchResults != null && searchResults.size() > 0) audits =  (List<Field_Audit_Trail__c>) searchResults[0];
    	//System.debug(logginglevel.info, 'searchResults ===== '+audits);
    	lookupFieldsRef = new Map<String, Set<Id>>();
    	for(Integer i=0; i < this.resultsLimit && i < audits.size(); i++){
    		String fieldLabel = fieldsMap.containsKey(audits[i].Field__c) ? fieldsMap.get(audits[i].Field__c) : audits[i].Field__c;
    		aduitTrails.add(new FieldAuditWrapper(audits[i], fieldLabel));
    		//System.debug(logginglevel.info, 'audits[i].Field__c ==== '+audits[i].Field__c);
    		if (this.lookupFields.contains(audits[i].Field__c.toLowerCase())) {
    			Set<Id> tempIds = new Set<Id>();
    			if (audits[i].OldValue__c != null) tempIds.add(audits[i].OldValue__c);
    			if (audits[i].NewValue__c != null) tempIds.add(audits[i].NewValue__c);
    			lookupFieldsRef.put(audits[i].Field__c.toLowerCase(), tempIds);
    			//lookupFieldsRef.put(audits[i].Field__c.toLowerCase(), new Set<Id> {audits[i].OldValue__c, audits[i].NewValue__c});	
    		} /* else {
    			System.debug(logginglevel.info, 'No Match Ref ==== '+lookupFields.contains(audits[i].Field__c.toLowerCase()));	
    		}*/				
    	}
    	//System.debug(logginglevel.info, 'lookupFieldsRef ==== '+lookupFieldsRef);
    	//System.debug(logginglevel.info, 'audits size ==== '+audits.size());
    	//System.debug(logginglevel.info, 'aduitTrails ==== '+aduitTrails);
    	this.setIdtoNameValues();
    	if (this.fieldsToName != null && this.fieldsToName.size() > 0) {
    		for (FieldAuditWrapper audit : aduitTrails) {
    			if (!this.lookupFields.contains(audit.auditObj.Field__c.toLowerCase())) {
    				continue;	
    			}
    			if (this.fieldsToName.containsKey(audit.auditObj.OldValue__c)) {
    				audit.oldValue = this.fieldsToName.get(audit.auditObj.OldValue__c); 			
    			}
    			if (this.fieldsToName.containsKey(audit.auditObj.NewValue__c)) {
    				audit.newValue = this.fieldsToName.get(audit.auditObj.NewValue__c);	
    			}		
    		}	
    	} 
    	if(audits.size() > this.resultsLimit) {
    		this.showMore = true;	
    	} else {
    		this.showMore = false;
    	}
    }
    
    private void descibeObject() {
    	if(sObjectId == null) {
    		sObjectId = Apexpages.currentPage().getParameters().get('Id');
    	}
    	Schema.DescribeSObjectResult objResults = sObjectId.getSObjectType().getDescribe();
    	this.sObjectAPIName = objResults.getName();
    	this.sObjectName= objResults.getLabel();
    	this.isCustomObj = objResults.isCustom();
    	//System.debug(logginglevel.info, 'sObjectAPIName ===== '+sObjectAPIName+', sObjectName = '+sObjectName);
    	this.fieldsMap = new Map<String, String>();
    	this.lookupFields = new Set<String>();
    	for(Schema.SObjectField result : objResults.fields.getMap().values()){
    		Schema.DescribeFieldResult fResult = result.getDescribe();
    		fieldsMap.put(fResult.getName(), fResult.getLabel());
    		if (fResult.getType().name() == 'REFERENCE') {
    			lookupFields.add(fResult.getName().toLowerCase());
    		}
    	}
    	//System.debug(logginglevel.info, 'lookupFields ===== '+lookupFields);
    }
    
    public PageReference showMoreRows() {
    	this.showResultsLimit++;
    	this.resultsLimit = this.resultsLimit * this.showResultsLimit;
    	this.getRecords();	
    	return null;
    }
    
    private void setIdtoNameValues() {
    	List<FieldTrackingConfig__c> configs = [Select Id, Name, JSONConfigData__c, SObjectAPIName__c, DisableDelete__c, Track_Fields__c, RecordCritiera__c, NumberofDays__c, 
    												ComponentGenerated__c, HistoryTableName__c, ParentField__c From FieldTrackingConfig__c where SObjectAPIName__c =:this.sObjectAPIName];
    	Map<String, String> lookups = new Map<String, String>();
    	if (configs != null && configs.size() > 0) {
    		if (String.isNotBlank(configs[0].JSONConfigData__c)) {
	    		for (FieldTrackingConfigCtrl.FieldWrapper fw : (List<FieldTrackingConfigCtrl.FieldWrapper>) JSON.deserialize(configs[0].JSONConfigData__c, List<FieldTrackingConfigCtrl.FieldWrapper>.class)) {
	    			if (fw.fieldType.equalsIgnoreCase('REFERENCE')) {
	    				lookups.put(fw.apiName.toLowerCase(), fw.lookupObjAPI);
	    			}
	    		}
    		} 		
    	}
    	//System.debug(logginglevel.info, 'lookups metadata selected  ===== '+lookups);
    	Map<String, Set<Id>> objectToIds = new Map<String, Set<Id>>();
    	for (String field : lookupFieldsRef.keySet()) {
    		
    		String lField = field.toLowerCase();
    		//System.debug(logginglevel.info, 'lField   ===== '+lField);
    		Set<Id> tempIds = new Set<Id>{};
    		if(lookups.containsKey(lField)) {
    			if (objectToIds.containsKey(lookups.get(lField))) {
    				//System.debug(logginglevel.info, 'field values   ===== '+lookups.get(lField));
    				tempIds.addAll(objectToIds.get(lookups.get(lField)));
    			}
    			tempIds.addAll(lookupFieldsRef.get(lField));
    			objectToIds.put(lookups.get(lField), tempIds);	
    		} 		
    	} 
    	//System.debug(logginglevel.info, 'objectToIds  ===== '+objectToIds);
    	this.fieldsToName = new Map<Id, String>();
		//Note - There is a SOQL query in loop but there is no other alternative since lookups are different objects
		for (String obj: objectToIds.keySet()) {
			Set<Id> sobjIds = objectToIds.get(obj);
			//System.debug(logginglevel.info, 'sobjIds = '+sobjIds);
			String nameField = !specialObjectNames.containsKey(obj.toLowerCase()) ? 'Name' : specialObjectNames.get(obj.toLowerCase());
			//System.debug(logginglevel.info, 'Name Field = '+nameField);
			String query = 'Select '+nameField+ ' From '+obj+' where Id IN:sobjIds';
			//System.debug(logginglevel.info, 'query  ===== '+query);
			for (sObject sobj : Database.query(query)) {
				this.fieldsToName.put(String.valueOf(sobj.get('id')), String.valueOf(sobj.get(nameField)));				
			}
			if (Limits.getDMLStatements() > 90) break; //break soql loop
		}
		//System.debug(logginglevel.info, 'this.fieldsToName ==== '+this.fieldsToName);	
    }
    
    public class FieldAuditWrapper {
    	public Field_Audit_Trail__c auditObj {get; set;}
    	public String fieldName {get; set;}
    	public String oldValue {get; set;}
		public String newValue {get; set;}
		    	
    	public FieldAuditWrapper(Field_Audit_Trail__c aObj, String fName) {
    		this.auditObj = aObj;
    		this.fieldName = fName;   			
    	}
    	
    	public String getConvertedTime() {
    		return auditObj.CreatedDate__c != null ? auditObj.CreatedDate__c.format() : null; 
    	}
    }
          
}
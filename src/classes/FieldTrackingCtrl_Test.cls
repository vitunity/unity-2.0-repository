/***********************************************************************
* Author         : Varian
* Description	 : Test class for FieldTrackingCtrl
* User Story     : 
* Created On     : 5/27/16
************************************************************************/

@isTest
private class FieldTrackingCtrl_Test {

	@testSetup static void setup() {
   		System.runAs(TestDataFactory.createUser('System Administrator')) {
   			TestDataFactory.createAccounts();
			TestDataFactory.createFieldTrackingConfig();
			TestDataFactory.createFieldAuditTrail();	
   		}
    }
   
    @isTest static void testHistoriesListView() {
		System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
			String likeFilter = '%Modified%';
			List<Field_Audit_Trail__c> audits = [Select Id, Name, sObjectID__c, Field__c, oldValue__c, newValue__c From Field_Audit_Trail__c order by sObjectID__c desc Limit 2000];
			List<Id> searchObjIds = new List<Id>();
			String sObjId = null;
			for (Field_Audit_Trail__c audit : audits) {
				searchObjIds.add(audit.Id);
			}
			List<Field_Audit_Trail__c> temp = new List<Field_Audit_Trail__c> { new Field_Audit_Trail__c(Field__c = 'LastModifiedById', oldValue__c = null, newValue__c = Userinfo.getUserId(), 
												sObjectID__c=audits[0].sObjectID__c, CreatedDate__c = System.now())};
			insert temp;
			searchObjIds.add(temp[0].id);
			Test.setFixedSearchResults(searchObjIds);
			Test.setCurrentPage(Page.historytracking);
			ApexPages.currentPage().getParameters().put('id', temp[0].sObjectID__c);
			FieldTrackingCtrl controller = new FieldTrackingCtrl();
			controller.resultsLimit = 3;
			controller.getRecords();
			System.debug(logginglevel.info, 'Audit Info ==== > ' +controller.aduitTrails);
			System.assert(controller.aduitTrails.size() > 0);
			System.debug(logginglevel.info, 'converted time == '+controller.aduitTrails[0].getConvertedTime());
			System.assert(controller.aduitTrails[0].getConvertedTime() != null);
			System.assert(controller.fieldsMap.size() > 0);	
			System.assert(controller.lookupFieldsRef.size() > 0);
			controller.showMoreRows();
		}
			
    } 
}
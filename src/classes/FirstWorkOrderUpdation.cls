/***************************************************************************
Author: Rakesh Basani
Created Date: 16-Aug-2017
Project/Story/Inc/Task : STSK0012612
Description: 
Update Case owner field on Case. 
Change Log:
****************************************************************************/
public class FirstWorkOrderUpdation{
        /***************************************************************************
        Description: 
        Change case ownership from DM to FE. If no FE then update to QUEUE for the first work order.
        *************************************************************************************/
    public static Boolean isFirstTime = true;
    public static void UpdateCase(List<SVMXC__Service_Order__c> newlist, map<Id,SVMXC__Service_Order__c> oldmap){
        
        if(!FirstWorkOrderUpdation.isFirstTime)return;
        else FirstWorkOrderUpdation.isFirstTime = false;
        
        try{
            
            Id HDRTId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
            Id FSRTId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
            List<Group> lstGrp = [select Id from Group where Name = 'FSE Queue' and Type = 'Queue'];            
            
            if(lstGrp.size()>0){
            
                Id FSEQueueId = lstGrp[0].id;
            
                set<Id> setCases = new set<Id>();
                set<Id> setTechnician = new set<Id>();
                List<SVMXC__Service_Order__c> lstWorkOrder = new List<SVMXC__Service_Order__c>();
                for(SVMXC__Service_Order__c order : newlist){
                    if((order.recordTypeId == HDRTId || order.recordTypeId == FSRTId ) && (oldmap == null || (oldmap <> null && order.SVMXC__Group_Member__c <> oldmap.get(order.id).SVMXC__Group_Member__c))){
                        setCases.add(order.SVMXC__Case__c); 
                        lstWorkOrder.add(order);
                        if(order.SVMXC__Group_Member__c  <> null)setTechnician.add(order.SVMXC__Group_Member__c );
                    }
                            
                }
                if(lstWorkOrder.size() > 0){
                    map<Id,SVMXC__Service_Group_Members__c> mapTechnicianUser = new map<Id,SVMXC__Service_Group_Members__c>( [select ID,User__c from SVMXC__Service_Group_Members__c where ID IN: setTechnician]);
                    map<Id,Case> mapCase = new map<Id,Case>([select Id,OwnerId from Case where ID IN:setCases ]);
                    map<Id,SVMXC__Service_Order__c> mapCaseFirstOrder = new map<Id,SVMXC__Service_Order__c>();
                    for(SVMXC__Service_Order__c order : [select Id,SVMXC__Case__c,createdDate from SVMXC__Service_Order__c where SVMXC__Case__c IN: setCases and (recordTypeId =: HDRTId or recordTypeId =: FSRTId) ]){         
                        if(mapCaseFirstOrder.containsKey(order.SVMXC__Case__c)){
                            SVMXC__Service_Order__c old = mapCaseFirstOrder.get(order.SVMXC__Case__c);
                            if(old.createdDate > order.createdDate){                        
                                mapCaseFirstOrder.put(order.SVMXC__Case__c,order);
                            }
                        }else{
                            mapCaseFirstOrder.put(order.SVMXC__Case__c,order);
                        }           
                    }
                    
                    List<Case> lstCase = new List<Case>();
                    for(SVMXC__Service_Order__c order : lstWorkOrder){
                        if(mapCaseFirstOrder.containsKey(order.SVMXC__Case__c) && mapCaseFirstOrder.get(order.SVMXC__Case__c).id == order.id){
                            Case c = mapCase.get(order.SVMXC__Case__c);
                            if(order.SVMXC__Group_Member__c == null){                   
                                c.OwnerId = FSEQueueId;
                                lstCase.add(c);
                            }else if(order.SVMXC__Group_Member__c <> null){
                                c.OwnerId = mapTechnicianUser.get(order.SVMXC__Group_Member__c).user__c;
                                lstCase.add(c);
                            }
                        }   
                    }
                    
                    update lstCase;
                }
            }
        }
        Catch(Exception e){
            system.debug('Exception : '+e.getLineNumber()+': '+e.getMessage());
        }
        
    }
}
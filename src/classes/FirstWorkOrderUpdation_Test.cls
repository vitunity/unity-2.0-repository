@isTest(seealldata=true)
public class FirstWorkOrderUpdation_Test{

    public static Id fieldServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
    public static testmethod void UnitTest(){
        Case objCase = SR_testdata.createCase();
        objCase.Reason = 'System Down';
        objCase.Priority = 'Medium';
        objCase.Status = 'New';
        objCase.Type ='Problem';
        objCase.Reason = 'System Down';
        objCase.Description = 'asdfghjkzxcvbnm';
        insert objCase;
        
        SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();
        objWO.recordtypeID = fieldServcRecTypeID;
        objWO.Machine_release_time__c = system.now();
        objWO.SVMXC__Order_Status__c = 'Assigned';
        objWO.SVMXC__Case__c = objCase.Id;
        objWO.Direct_Assignment__c = true ;
        objWO.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO.Subject__c = 'Test Work Order';
        objWO.SVMXC__Preferred_Start_Time__c = System.now();
        objWO.SVMXC__Purpose_of_Visit__c = 'Repair';
        insert objWO;
        
        Test.startTest();
            delete [select Id from SVMXC__Service_Order__c  where id=: objWO.id];
        Test.stopTest();
    }
}
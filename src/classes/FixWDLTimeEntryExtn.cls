public class FixWDLTimeEntryExtn {
    public static boolean allowEntry = false;
    ApexPages.StandardController sc;
    public FixWDLTimeEntryExtn(ApexPages.StandardController sc){
        this.sc = sc;
        if(!System.Test.isRunningTest())
        sc.addFields(new list<String>{'SVMXC__Start_Date_and_Time__c'});
    }
    
    public PageReference fixTimeEntry(){
        allowEntry = true;
        SVMXC__Service_Order_Line__c wdlObj = (SVMXC__Service_Order_Line__c)sc.getRecord();        
        update wdlObj;
        allowEntry = false;
        return sc.view();
    }
}
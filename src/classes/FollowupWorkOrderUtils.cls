/*
 *  Author - Amitkumar
 *  created date - 1st sep 2016
 */
public class FollowupWorkOrderUtils {
    
    public static SVMXC__Service_Order__c  createFollowUpWorkOrder(SVMXC__Service_Order__c  swo){
        SVMXC__Service_Order__c fwo = new SVMXC__Service_Order__c(
            SVMXC__Case__c = swo.SVMXC__Case__c,
            SVMXC__Order_Status__c =  'Open',
            RecordTypeId = swo.RecordTypeId,
            SVMXC__Priority__c = 'Low',
            ERP_Priority__c = '8 - Follow-up',
            SVMXC__Contact__c = swo.SVMXC__Contact__c,
            Contact_Email__c  =  swo.Contact_Email__c,
            ERP_Org_lookup__c = swo.ERP_Org_lookup__c,
            Sales_Org__c = swo.Sales_Org__c,
            SVMXC__Top_Level__c = swo.SVMXC__Top_Level__c,
            SVMXC__Component__c = swo.SVMXC__Component__c,
            Acceptance_Date__c =  swo.Acceptance_Date__c,
            Acceptance_Date1__c = swo.Acceptance_Date1__c,
            Malfunction_Start__c = swo.Malfunction_Start__c,
            SVMXC__Service_Contract__c = swo.SVMXC__Service_Contract__c,
            Internal_Comment__c = swo.Follow_Up_comment__c,
            Purchase_Order_Number__c = swo.Purchase_Order_Number__c,
            SVMXC__Purpose_of_Visit__c = 'Follow Up', 
            SVMXC__Preferred_Technician__c = swo.SVMXC__Preferred_Technician__c,
            Preferred_Technician_Email__c = swo.Preferred_Technician_Email__c,
            Service_Team__c = swo.Service_Team__c,
            SVMXC__Preferred_Start_Time__c = swo.SVMXC__Preferred_Start_Time__c,
            SVMXC__Preferred_End_Time__c = swo.SVMXC__Preferred_End_Time__c,
            District__c = swo.District__c,
            District_Manager_1__c = swo.District_Manager_1__c,
            District_Manager_Email__c = swo.District_Manager_Email__c,
            SVMXC__Site__c = swo.SVMXC__Site__c,
            SVMXC__Company__c = swo.SVMXC__Company__c,
            SVMXC__Street__c = swo.SVMXC__Street__c,
            SVMXC__City__c = swo.SVMXC__City__c,
            Preferred_Language_Code__c = swo.Preferred_Language_Code__c,
            SVMXC__State__c = swo.SVMXC__State__c,
            Service_Country__c = swo.Service_Country__c,
            Service_Country_Code__c = swo.Service_Country_Code__c,
            SVMXC__Country__c = swo.SVMXC__Country__c,
            SVMXC__Zip__c = swo.SVMXC__Zip__c,
            Requested_Start__c = swo.Requested_Start__c,
            SVMXC__Problem_Description__c = swo.SVMXC__Problem_Description__c,
            ERP_Pcode__c = swo.ERP_Pcode__c,
            SVMXC__SLA_Terms__c = swo.SVMXC__SLA_Terms__c,
            STB_Status__c = swo.STB_Status__c,
            ERP_Pcode_Sub_ID__c = swo.ERP_Pcode_Sub_ID__c,
            SVMXC__Is_PM_Work_Order__c = swo.SVMXC__Is_PM_Work_Order__c,
            PMI_Type__c = swo.PMI_Type__c,
            SVMXC__PM_Plan__c = swo.SVMXC__PM_Plan__c,
            Is_This_a_Complaint__c = swo.Is_This_a_Complaint__c,
            Was_anyone_injured__c = swo.Was_anyone_injured__c,
            Is_escalation_to_the_CLT_required__c = swo.Is_escalation_to_the_CLT_required__c,
            Link_to_PSE__c = swo.Link_to_PSE__c,
            PHI_Obtained__c = swo.PHI_Obtained__c,
            Date_of_Intervention__c = swo.Date_of_Intervention__c,
            Date_of_Intervention1__c = swo.Date_of_Intervention1__c,
            Next_Training_Trip_Reminder__c = swo.Next_Training_Trip_Reminder__c,
            Next_Training_Trip_Reminder1__c = swo.Next_Training_Trip_Reminder1__c,
            Follow_Up_From__c = swo.Id,    
            Estimated_Arrival_Time__c = null                    
        );
        return fwo;
    }
}
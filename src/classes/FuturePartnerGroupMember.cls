global class FuturePartnerGroupMember
{

    @future
    public static void InsertMember(set<ID> distributorUserIds)
    {   
        
            String partnerGroupId = null;
            try{
                Group partnerGroup = [Select Id, Name from Group where Name = 'VMS MyVarian - Partners' and Type='Regular'];
                partnerGroupId = partnerGroup.Id;
            } catch(Exception ex){
                // ignore exception.
            }
            
            // Create a group member.
            if(partnerGroupId != null){
                List<GroupMember> newPartnerGroupMembers = new List<GroupMember>();
                for(Id userId : distributorUserIds){
                    GroupMember  grpmember = new GroupMember();
                    grpmember.GroupId = partnerGroupId;
                    grpmember.UserOrGroupId = userId;
                    
                    newPartnerGroupMembers.add(grpmember);
                }
                
                if(newPartnerGroupMembers.size() > 0){
                    insert newPartnerGroupMembers;
                }
            }
        
    }
}
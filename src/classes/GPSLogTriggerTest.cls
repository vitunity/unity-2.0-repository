@isTest(SeeAllData=true)
public class GPSLogTriggerTest
{
    static SVMXC__Service_Group_Members__c objServiceGroupMember;

    static testMethod void TestGPSLogTrigger()
    {
    
        SVMXC__Service_Group__c objServiceGroup = new SVMXC__Service_Group__c();
        objServiceGroup.RecordType = [select Id from RecordType where Name = 'Technician' and SobjectType = 'SVMXC__Service_Group__c' limit 1];
        objServiceGroup.Name = 'MSOG Test Service Team';
        insert objServiceGroup;
        
        objServiceGroupMember = new SVMXC__Service_Group_Members__c();
        objServiceGroupMember.RecordType = [select Id from RecordType where Name = 'Technician' and SobjectType = 'SVMXC__Service_Group_Members__c' limit 1];
        objServiceGroupMember.Name = 'MSOG Test Technician';
        objServiceGroupMember.SVMXC__Service_Group__c = objServiceGroup.Id;
        objServiceGroupMember.SVMXC__Country__c ='United States';
        objServiceGroupMember.User__c = userinfo.getUserId();
        objServiceGroupMember.Tracking_Technician__c = True;
        insert objServiceGroupMember;   

        SVMXC__User_GPS_Log__c gpsLog = new SVMXC__User_GPS_Log__c();
        gpsLog.SVMXC__User__c = userinfo.getUserId();
        gpsLog.SVMXC__Additional_Info__c = 'Location Services Setting is disable by the user';
        gpsLog.SVMXC__Device_Type__c = 'Other';
        gpsLog.SVMXC__Status__c = 'Failure';
        insert gpsLog;
    }
}
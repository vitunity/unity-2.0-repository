@RestResource(urlMapping='/GetBillTo/*')
global with sharing class GetBillToAccountsRestSvc{

    @HttpGet
  global static BillToAccount doGet(){
    RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response;
        String octId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('okta id  = '+octId);
        List<Contact> customerContact = [
            SELECT Id, AccountId, Account.Ext_Cust_Id__c, FirstName, LastName, Email, Phone
            FROM Contact
            WHERE OktaId__c =:octId
            LIMIT 1
        ];
        
        BillToAccount billTo = new BillToAccount();
       
        if(!customerContact.isEmpty()){     
            system.debug('customer contact = '+customerContact);
            List<ERP_Partner_Association__c> billToParties = [
                SELECT Id, Sales_Org__c, Partner_Street__c, Partner_Street_line_2__c, 
                Partner_City__c, Partner_State__c, Partner_Country__c,ERP_Partner_Number__c,
                Partner_Zipcode_postal_code__c 
                FROM ERP_Partner_Association__c 
                WHERE ERP_Customer_Number__c = :customerContact[0].Account.Ext_Cust_Id__c
                AND Partner_Function__c = 'BP=Bill-to Party'
            ];
            
            billTo.firstName = customerContact[0].FirstName;
            billTo.lastName = customerContact[0].LastName;
            billTo.email = customerContact[0].Email;
            billTo.phone = customerContact[0].Phone;
            billTo.billingNumber = customerContact[0].Account.Ext_Cust_Id__c;
            if(!billToParties.isEmpty()){
                billTo.erpPartnerNumber = billToParties[0].ERP_Partner_Number__c;
                billTo.street1 = billToParties[0].Partner_Street__c;
                billTo.street2 = billToParties[0].Partner_Street_line_2__c;
                billTo.city = billToParties[0].Partner_City__c;
                billTo.state = billToParties[0].Partner_State__c;
                billTo.postalCode = billToParties[0].Partner_Zipcode_postal_code__c;
                billTo.country = billToParties[0].Partner_Country__c;
                billTo.soldToNumber = CreateQuoteProductPartner.getSoldToNumber(customerContact[0].AccountId);
                return billTo;
            }else{
              billTo.soldToNumber = CreateQuoteProductPartner.getSoldToNumber(customerContact[0].AccountId);
                billTo.error = 'Bill to account not found';
                return billTo;
            }
        }else{
            billTo.error = 'Contact not found';
            return billTo;
    }
  }
       
  global class BillToAccount{
        public String lastName;
        public String firstName;
        public String email;
        public String phone;
        public String erpPartnerNumber;
        public String billingNumber;
        public String street1;
        public String street2;
        public String city;
        public String state;
        public String postalCode;
        public String country;
        public String soldToNumber;
        public String error;
        
    }
}
@isTest
private class GetBillToAccountsRestSvcTest {
    static testMethod void testBillToService() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
    
        req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/GetBillTo/TESTOKTAID';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    
        GetBillToAccountsRestSvc.BillToAccount billToAccount1 = GetBillToAccountsRestSvc.doGet();
    
        System.assertEquals('TESTPartner', billToAccount1.erpPartnerNumber);
        System.assertEquals('TEST', billToAccount1.firstName);
        System.assertEquals('Contact', billToAccount1.lastName);
        System.assertEquals('TEST STREET 2', billToAccount1.street2);
        
        req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/GetBillTo/TESTOKTAID123';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    
        GetBillToAccountsRestSvc.BillToAccount billToAccount2 = GetBillToAccountsRestSvc.doGet();
        System.assertEquals('Contact not found', billToAccount2.error);
        
        req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/GetBillTo/TESTOKTAID1234';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
    
        GetBillToAccountsRestSvc.BillToAccount billToAccount3 = GetBillToAccountsRestSvc.doGet();
        System.assertEquals('Bill to account not found', billToAccount3.error);
    }
    
    @testSetUp static void setupBillToData(){
        
        Account acct1 = new Account();
        acct1.Name = 'Test Account';
        acct1.Ext_Cust_Id__c = 'TESTNUMBER';
        acct1.Country__c = 'USA';
        
        Account acct2 = new Account();
        acct2.Name = 'Test Account';
        acct2.Ext_Cust_Id__c = 'TESTNUMBER2';
        acct2.Country__c = 'USA';
        insert new List<Account>{acct1,acct2};
        
        Contact sfContact1 = new Contact();
        sfContact1.FirstName = 'TEST';
        sfContact1.LastName = 'Contact';
        sfContact1.AccountId = acct1.Id;
        sfContact1.Email = 'TEST@TEST.com';
        sfContact1.OktaId__c = 'TESTOKTAID';
        sfContact1.MailingState = 'CA';
        
        Contact sfContact2 = new Contact();
        sfContact2.FirstName = 'TEST';
        sfContact2.LastName = 'Contact';
        sfContact2.AccountId = acct2.Id;
        sfContact2.Email = 'TEST@TEST.com';
        sfContact2.OktaId__c = 'TESTOKTAID1234';
        sfContact2.MailingState = 'CA';
        insert new List<Contact>{sfContact1,sfCOntact2};
                
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'TESTNUMBER';
        erpPartner.Street__c = 'TEST STREET';
        erpPartner.Street_Line_2__c = 'TEST STREET 2';
        erpPartner.City__c = 'TESTCITY';
        erpPartner.State_Province_Code__c = 'CA';
        erpPartner.Zipcode_Postal_Code__c = '123454';
        erpPartner.Country_Code__c = 'USA';
        insert erpPartner;
        
        ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
        partnerAssociation.Customer_Account__c = acct1.Id;
        partnerAssociation.ERP_Customer_Number__c = 'TESTNUMBER';
        partnerAssociation.Erp_Partner__c = erpPartner.Id;
        partnerAssociation.Partner_Function__c = 'BP=Bill-to Party';
        partnerAssociation.ERP_Partner_Number__c = 'TESTPartner';
        partnerAssociation.Sales_Org__c = '0601';
        insert partnerAssociation;
    }
}
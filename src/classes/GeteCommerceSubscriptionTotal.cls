@RestResource(urlMapping='/GetSubscriptionTotal/*')
global with sharing class GeteCommerceSubscriptionTotal {
  @HttpGet
  global static Double canculateSubscriptionTotal(){
    
    String productCode = RestContext.request.params.get('productCode');
    Integer quantity = Integer.valueOf(RestContext.request.params.get('quantity'));
    Integer numberOfYears = Integer.valueOf(RestContext.request.params.get('numberOfYears'));
    
    List<Product2> subscriptionProducts = [
      SELECT Id, Name, Description, Starting_Unit__c, Ending_Unit__c, Subscription_Product_Type__c,
      (SELECT Id, UnitPrice FROM PricebookEntries)
      FROM Product2
      WHERE Subscription_Product_Code__c =:productCode
      ORDER BY Starting_Unit__c ASC
    ];
    
    Double installationPrice = 0.0;
    for(Product2 product: subscriptionProducts){
      if(product.Subscription_Product_Type__c == 'Installation'){
        installationPrice = product.PricebookEntries[0].UnitPrice;
      }
    }
    System.debug('---installationPrice'+installationPrice);
    Map<Integer, Double> tierPricingMap = new Map<Integer, Double>();
    
    Integer quantityAdded = 0;
    Double totalPrice = 0.0;
    for(Product2 product: subscriptionProducts){
      if(product.Subscription_Product_Type__c == 'Subscription'){
        if(quantity > product.Ending_Unit__c){
          Integer subscriptionQuantity = Integer.valueOf(product.Ending_Unit__c) - quantityAdded;
          System.debug('quantity1'+subscriptionQuantity+'--price1'+product.PricebookEntries[0].UnitPrice);
          totalPrice = totalPrice + (subscriptionQuantity * numberOfYears * product.PricebookEntries[0].UnitPrice);
          quantityAdded = quantityAdded + subscriptionQuantity;
        }else{
          Integer remainingQuantity = quantity - quantityAdded;
          System.debug('quantity2'+remainingQuantity+'--price2'+product.PricebookEntries[0].UnitPrice);
          totalPrice = totalPrice + (remainingQuantity * numberOfYears * product.PricebookEntries[0].UnitPrice);
          break;
        }
      }
    }
    return totalPrice + installationPrice;
  }
}
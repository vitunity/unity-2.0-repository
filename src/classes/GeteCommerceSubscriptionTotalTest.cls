@isTest
private class GeteCommerceSubscriptionTotalTest {

    static testMethod void calculateSubscriptionTotalTest() {
      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/GetSubscriptionTotal?productCode=QU&quantity=5&numberOfYears=1';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('productCode','QU');
        RestContext.request.params.put('quantity','5');
        RestContext.request.params.put('numberOfYears','1');
        System.assertEquals(12600,GeteCommerceSubscriptionTotal.canculateSubscriptionTotal());
        
        req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/GetSubscriptionTotal?productCode=QU&quantity=25&numberOfYears=1';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        RestContext.request.params.put('productCode','QU');
        RestContext.request.params.put('quantity','25');
        RestContext.request.params.put('numberOfYears','1');
        System.assertEquals(31350,GeteCommerceSubscriptionTotal.canculateSubscriptionTotal());
    }
    
    
    @testSetup
    private static void setupProducts(){
      Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 subscriptionProduct1 = new Product2();
        subscriptionProduct1.Name = 'Qumulate 1';
        subscriptionProduct1.IsActive = true;
        subscriptionProduct1.ProductCode = 'Test Code1';
        subscriptionProduct1.Subscription_Product_Type__c = 'Installation';
        subscriptionProduct1.Subscription_Product_Code__c = 'QU';
        Product2 subscriptionProduct2 = new Product2();
        subscriptionProduct2.Name = 'Qumulate 2';
        subscriptionProduct2.IsActive = true;
        subscriptionProduct2.ProductCode = 'Test Code2';
        subscriptionProduct2.Subscription_Product_Type__c = 'Subscription';
        subscriptionProduct2.Subscription_Product_Code__c = 'QU';
        subscriptionProduct2.Starting_Unit__c = 1;
        subscriptionProduct2.Ending_Unit__c = 5;
        Product2 subscriptionProduct3 = new Product2();
        subscriptionProduct3.Name = 'Qumulate 3';
        subscriptionProduct3.IsActive = true;
        subscriptionProduct3.ProductCode = 'Test Code3';
        subscriptionProduct3.Subscription_Product_Type__c = 'Subscription';
        subscriptionProduct3.Subscription_Product_Code__c = 'QU';
        subscriptionProduct3.Starting_Unit__c = 6;
        subscriptionProduct3.Ending_Unit__c = 20;
        Product2 subscriptionProduct4 = new Product2();
        subscriptionProduct4.Name = 'Qumulate 4';
        subscriptionProduct4.IsActive = true;
        subscriptionProduct4.ProductCode = 'Test Code4';
        subscriptionProduct4.Subscription_Product_Type__c = 'Subscription';
        subscriptionProduct4.Subscription_Product_Code__c = 'QU';
        subscriptionProduct4.Starting_Unit__c = 21;
        subscriptionProduct4.Ending_Unit__c = 9999999;
        insert new List<Product2>{subscriptionProduct4,subscriptionProduct3,subscriptionProduct2,subscriptionProduct1};
        
        PricebookEntry pbEntry1 = new PricebookEntry();
        pbEntry1.Product2Id = subscriptionProduct1.Id;
        pbEntry1.Pricebook2Id = standardPricebookId;
        pbEntry1.UnitPrice = 2100;
        pbEntry1.CurrencyISOCode = 'USD';
        pbEntry1.IsActive = true;
        
        PricebookEntry pbEntry2 = new PricebookEntry();
        pbEntry2.Product2Id = subscriptionProduct2.Id;
        pbEntry2.Pricebook2Id = standardPricebookId;
        pbEntry2.UnitPrice = 2100;
        pbEntry2.CurrencyISOCode = 'USD';
        pbEntry2.IsActive = true;
        
        PricebookEntry pbEntry3 = new PricebookEntry();
        pbEntry3.Product2Id = subscriptionProduct3.Id;
        pbEntry3.Pricebook2Id = standardPricebookId;
        pbEntry3.UnitPrice = 1050;
        pbEntry3.CurrencyISOCode = 'USD';
        pbEntry3.IsActive = true;
        
        PricebookEntry pbEntry4 = new PricebookEntry();
        pbEntry4.Product2Id = subscriptionProduct4.Id;
        pbEntry4.Pricebook2Id = standardPricebookId;
        pbEntry4.UnitPrice = 600;
        pbEntry4.CurrencyISOCode = 'USD';
        pbEntry4.IsActive = true;
        insert new List<PricebookEntry>{pbEntry4,pbEntry3,pbEntry2,pbEntry1};
        
    }
}
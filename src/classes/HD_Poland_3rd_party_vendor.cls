// Test Class - HD_Poland_3rd_party_vendor_Test

global class HD_Poland_3rd_party_vendor implements Messaging.InboundEmailHandler 
{
    // The default from email address is storied in Custom label SR_SmartConnectCaseCreationFromAddress
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{            
            
            if(email.subject.contains('SN='))   
            {
                Id HDRecordType = [Select id from recordtype where sObjecttype = 'Case' and developername = 'helpdesk'].id;
                String lstSubjectForReplace = email.subject;
                String lstSubject = lstSubjectForReplace.unescapeXml();//Kaushiki 27/3/15;
                System.debug('lstSubject +++'+lstSubject );
                List<String> splitString = new List<String>();
                splitString =lstSubject.split(' ');
                System.debug('splitString +++'+splitString );
                String WOId;
                
                List<SVMXC__Installed_Product__c> ListofInstalledProduct;
                    
                string emailsubject = (email.subject).unescapeXml();
                string splitStr;
                emailsubject = emailsubject.ToUpperCase();
                if(emailsubject.contains('SN='))splitStr = 'SN=';
                if(emailsubject.contains('SN ='))splitStr = 'SN =';
                if(splitStr <> null){
                    string s = emailsubject.substring(emailsubject.indexOf(splitStr)+splitStr.length());
                    boolean isfound = false;
                    string proId = '';
                    for(integer i=1;i<s.length();i++){
                        string schar = s.substring(i-1,i);
                        if(schar == ' ' && isfound){
                            break;
                        }else{
                            if(schar != ' ') {
                            isfound = true;
                            proId += schar;
                            }
                        }
                       
                    }
                    proId = proId.trim();
                    if(proId.contains(','))proId = proId.split(',')[0];
                    if(proId.contains(';'))proId = proId.split(';')[0];
                    ListofInstalledProduct=[Select Id,SVMXC__Contact__c, SVMXC__Contact__r.Phone, SVMXC__Company__c,SVMXC__Company__r.RecordType.DeveloperName,SVMXC__Preferred_Technician__c from SVMXC__Installed_Product__c where name=:proId];
                }          
                String lstDescription = email.plainTextBody; 
                String lstDescription1 = email.plainTextBody;
                List<QueueSobject> queueowner=[Select QueueId from QueueSobject where SobjectType='Case' and Queue.DeveloperName='Email_to_Case_Web_Poland'];
                Case casealias = new Case();
                casealias.recordtypeId = HDRecordType;
                casealias.Origin = 'Machine';
                casealias.local_Subject__c = lstSubject;
                casealias.OwnerId = queueowner[0].QueueId ;
                casealias.Subject = lstSubject;
             
                if(ListofInstalledProduct <> null){
                    for(SVMXC__Installed_Product__c IP : ListofInstalledProduct )
                    {
                        if(IP.SVMXC__Company__r.RecordType.DeveloperName=='Site_Partner' || IP.SVMXC__Company__r.RecordType.DeveloperName=='Sold_to')
                        {
                            system.debug('asdf1'+IP);
                            casealias.AccountId=IP.SVMXC__Company__c;
                            casealias.ContactId = IP.SVMXC__Contact__c; // fixed for custom validation
                        }
                        casealias.ProductSystem__c=IP.Id; //DE3449
                    } 
                }           
                casealias.Local_Description__c = lstDescription.unescapeXml();
                casealias.Description = lstDescription1.unescapeXml();
                casealias.Priority='High';       
                casealias.Is_This_a_Complaint__c='Yes'; 
                casealias.Is_escalation_to_the_CLT_required__c = 'No';
                casealias.Was_anyone_injured__c = 'No';        
                casealias.Smart_Connect_Case__c=True;
                casealias.Reason ='Analysis/Troubleshoot'; 
                casealias.Status ='New';
                // For assignment rule execution
                /*AssignmentRule AR = new AssignmentRule();
                AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.assignmentRuleId= AR.id;
                casealias.setoptions(dmo); */               
                Insert casealias;
            }
        }
        catch(Exception e){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {system.Label.SmartAlertExceptionEmail};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Poland Request Failure for : '+ email.subject);
            mail.setHTMLBody('failure reason : '+e.getMessage() +'.<br/>From Address : '+email.fromAddress+'<br/>Email body : '+email.plainTextBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return result;
    }
}
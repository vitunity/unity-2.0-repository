/*@RestResource(urlMapping='/IMapPinSchedules/*')
   global class IMPScheduleController 
   {
   @HttpGet
   global static List<VU_Event_Schedule__c> getBlob() 
   {
   List<VU_Event_Schedule__c> a;
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   String Id= RestContext.request.params.get('PINID') ;
   String Language= RestContext.request.params.get('Language') ;
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
     a = [SELECT Add_Interactive_Map_Points__c, Date__c, End_Time__c, Event_ID__c, Id,Moderator_Email__c,Moderator_Phone__c, Location__c, Name, Description__c, Start_Time__c,Session_Start_Time__c,Session_End_Time__c,Language__c,
   (SELECT ID, Name FROM Attachments),Add_Interactive_Map_Points__r.X_Value__c, Add_Interactive_Map_Points__r.Y_Value__c, Add_Interactive_Map_Points__r.Interactive_Maps_Landmarks__c, Interactive_Maps_Landmarks__r.Photo__c,
   (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c, VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE Add_Interactive_Map_Points__c= :Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
}
else
{
   a = [SELECT Add_Interactive_Map_Points__c, Date__c, End_Time__c, Event_ID__c, Id,Moderator_Email__c,Moderator_Phone__c, Location__c, Name, Description__c, Start_Time__c,Session_Start_Time__c,Session_End_Time__c,Language__c,
   (SELECT ID, Name FROM Attachments),Add_Interactive_Map_Points__r.X_Value__c, Add_Interactive_Map_Points__r.Y_Value__c, Add_Interactive_Map_Points__r.Interactive_Maps_Landmarks__c, Interactive_Maps_Landmarks__r.Photo__c,
   (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c, VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE Add_Interactive_Map_Points__c= :Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language))];
  }
   return a;
   } 
   }
   */
   
   @RestResource(urlMapping='/IMapPinSchedules/*')
   global class IMPScheduleController 
   {
   @HttpGet
   global static List<VU_Event_Schedule__c> getBlob() 
   {
   List<VU_Event_Schedule__c> a;
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   String Id= RestContext.request.params.get('PINID') ;
   String Language= RestContext.request.params.get('Language') ;
   if(Language !='English(en)' && Language !='Chinese(zh)'){
     a = [SELECT Add_Interactive_Map_Points__c, Date__c, End_Time__c, Event_ID__c, Id,Moderator_Email__c,Moderator_Phone__c, Location__c, Name, Description__c, Start_Time__c,Session_Start_Time__c,Session_End_Time__c,Language__c,
   (SELECT ID, Name FROM Attachments),Add_Interactive_Map_Points__r.X_Value__c, Add_Interactive_Map_Points__r.Y_Value__c, Add_Interactive_Map_Points__r.Interactive_Maps_Landmarks__c, Interactive_Maps_Landmarks__r.Photo__c,
   (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c, VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE Add_Interactive_Map_Points__c= :Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
}
else
{
   a = [SELECT Add_Interactive_Map_Points__c, Date__c, End_Time__c, Event_ID__c, Id,Moderator_Email__c,Moderator_Phone__c, Location__c, Name, Description__c, Start_Time__c,Session_Start_Time__c,Session_End_Time__c,Language__c,
   (SELECT ID, Name FROM Attachments),Add_Interactive_Map_Points__r.X_Value__c, Add_Interactive_Map_Points__r.Y_Value__c, Add_Interactive_Map_Points__r.Interactive_Maps_Landmarks__c, Interactive_Maps_Landmarks__r.Photo__c,
   (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c, VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE Add_Interactive_Map_Points__c= :Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language))];
  }
   return a;
   } 
   }
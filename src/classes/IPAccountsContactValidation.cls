public class IPAccountsContactValidation{

    public static void validate(map<Id,Lung_Phantom__c> oldmap, List<Lung_Phantom__c> newList){
        List<Lung_Phantom__c> lstLptm = new List<Lung_Phantom__c>();
        set<Id> setContactIds = new set<Id>();
        set<Id> setIPIds = new set<Id>();
        map<Id,set<Id>> mapContactAccount = new map<Id,set<Id>>();
        for(Lung_Phantom__c l : newList){
            if(l.Installed_Product__c <> null && l.Contact__c <> null){
                if((oldmap == null) || (oldmap <> null && l.Installed_Product__c <> oldmap.get(l.Id).Installed_Product__c)){
                    lstLptm.add(l);
                    setContactIds.add(l.Contact__c);
                    setIPIds.add(l.Installed_Product__c);
                }               
            }
        }
        
        map<Id,SVMXC__Installed_Product__c> mapInstalledProduct = new map<Id,SVMXC__Installed_Product__c>([select Id,SVMXC__Company__c from  SVMXC__Installed_Product__c where ID IN: setIPIds and SVMXC__Company__c <> null]);
        
        List<Contact> lstContact = [select Id,AccountId,(select Id,Account__c from Contact_Role_Associations__r) from Contact where Id IN: setContactIds];
        
        for(Contact c : lstContact){
            set<Id> setTemp = new set<Id>();
            setTemp.add(c.AccountId);
            for(Contact_Role_Association__c crole : c.Contact_Role_Associations__r){
                setTemp.add(crole.Account__c);
            }
            if(mapContactAccount.containsKey(c.Id)){
                setTemp.addAll(mapContactAccount.get(c.Id));
            }
            mapContactAccount.put(c.Id,setTemp);
        }
        
        for(Lung_Phantom__c l : lstLptm){
            if(mapContactAccount.containsKey(l.Contact__c)){
                set<Id> setAccountId = mapContactAccount.get(l.Contact__c);
                if(mapInstalledProduct.ContainsKey(l.Installed_Product__c)){
                    Id IPAccountId = mapInstalledProduct.get(l.Installed_Product__c).SVMXC__Company__c;
                    if(setAccountId.contains(IPAccountId)){
                        //correct IP
                    }else{
                        l.addError('Installed product selected should be from the Account associated to Contact.');
                    }
                }               
            }
        }
        
    }
}
public class IdeaAttachmentController{ 
    public string AttachmentContentType{get;set;}
    public string AttachmentName{get;set;}
    public blob Attachmentbody{get;set;}
    string ideaId;
    public IdeaAttachmentController(){
        ideaId = apexpages.CurrentPage().getParameters().get('Id');
        Idea ideaObj = [select Id,AttachmentName, AttachmentContentType,AttachmentBody from Idea where Id=: ideaId];
        AttachmentContentType = ideaObj.AttachmentContentType;
        AttachmentName = ideaObj.AttachmentName;
        Attachmentbody = ideaObj.AttachmentBody;
    }
    public pagereference download(){
        pagereference pg = new pagereference('/apex/IdeaCustom?id='+ideaId);
        pg.setredirect(true);
        return pg;
        //return null;
    }   
}
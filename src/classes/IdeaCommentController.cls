public class IdeaCommentController {

    public string IdeaId;
    public boolean isinternal{get;set;}
    public IdeaComment commentObj{get;set;}
    public Internal_Idea_Comment__c  commentInternalObj{get;set;}
    public IdeaCommentController() {
        string CommentId = apexpages.currentPage().getParameters().get('id');
        string pIdeaId = apexpages.currentPage().getParameters().get('ideaid');
        string isinternalstr = apexpages.currentPage().getParameters().get('isinternal');
        if(isinternalstr == 'yes'){
            isinternal = true;
            if(CommentId=='new'){
                commentInternalObj = new Internal_Idea_Comment__c();
                commentInternalObj.Idea__c = pIdeaId;
            }else{
                commentInternalObj = [select Id,Idea__c,Comment__c from Internal_Idea_Comment__c where Id=: CommentId];             
            }
            IdeaId = commentInternalObj.Idea__c;
            
        }
        else {
            isinternal = false;
            commentObj = [select Id,Idea.Title,IdeaId,CommentBody from IdeaComment where Id=: CommentId];
            IdeaId = commentObj.IdeaId;
        }
    }
    public pagereference Save(){
        if(isinternal)
        upsert commentInternalObj;
        else update commentObj;
        
        string rtn = apexpages.currentPage().getParameters().get('rtn');
        pagereference pg = new pagereference(rtn);
        
        pg.setRedirect(true);
        return pg;
    }
    public pagereference Cancel(){

        string rtn = apexpages.currentPage().getParameters().get('rtn');
        pagereference pg = new pagereference(rtn);
        pg.setRedirect(true);
        return pg;
    }

}
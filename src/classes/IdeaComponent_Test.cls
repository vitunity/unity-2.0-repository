@isTest(SeeAllData=true)
public class IdeaComponent_Test
{
    static testMethod void Component1Cntrl_Test() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='testidea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;
            
        system.runAs(u)
        {       
            
            
            Test.StartTest();

                PageReference pageRef = Page.Submit_Product_Idea_v1;
                pageRef.getParameters().put('IdeaId', obj.Id);
                pageRef.getParameters().put('Type', 'up');
                Test.setCurrentPage(pageRef);               
                Component1Cntrl  contr = new Component1Cntrl();
                contr.NewIdeaAttach = new Attachment(name='testfile', contenttype='pdf',body=blob.valueOf('test'));
                contr.hdnIdeaId = obj.Id;
                contr.newIdeaObj.title = 'tst';
                contr.newIdeaObj.body = 'tst';
                contr.getProductIdea();
                contr.onPostIdeaClick();
                contr.addAttachment();                
                contr.SaveIdea();
                contr.RemoveAttachmentlink();
                contr.getNewIdeaProductGroup();
                contr.IdeaIdRefresh();
                contr.IdeaSearch();
                contr.addComment();
                contr.getIdeaComments();
                contr.ResetNewIdea();
                contr.Reset();
                contr.makeVote();
                
                contr.Beginning();
                contr.Previous();
                contr.Next();
                contr.End();
                contr.getDisablePrevious();
                contr.getDisableNext();
                contr.getTotal_size();
                contr.getPageNumber();
                contr.getTotalPages();
                
            Test.StopTest();
            
        }
    }
}
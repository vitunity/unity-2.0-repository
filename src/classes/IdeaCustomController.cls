/**************************************************************************\
@Last Modified Date- 08/02/2017
@Last Modified By - Rakesh Basani   
@Description - STSK0012364 - Added fields in the query : line 244 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012697 - Getting all attachements related to Idea.
/**************************************************************************/
public class IdeaCustomController{
    public Idea ideaObj{get;set;}
    public Attachment IdeaAttachment{get;set;}
    public List<Attachment> lstIdeaAttachment{get;set;} //STSK0012697 - Declaration of attachment list.
    public string SFURL{get;set;}
    public string CommentCount{get;set;}
    public boolean isFindLayout{get;set;}
    public boolean lstInternalComments{get;set;}
    public case ConvertedCase{get;set;}
    public string InstanceURL{get;set;}
     
    string SB;
    string PG;
    string PC;
    string ST;
    public string filter{get;set;}
    
    public class FindIdea{
        public boolean isCheck{get;set;}
        public Idea IdeaObj{get;set;}
        public string Ideabody{get;set;}
        public Integer commentCount{get;set;}
        public FindIdea(boolean isCheck, Idea IdeaObj, string Ideabody, Integer commentCount){
            this.isCheck = isCheck;
            this.IdeaObj = IdeaObj;
            this.Ideabody = Ideabody;
            this.commentCount = commentCount;
        }
    }
    public void MergeIdea(){
        List<Idea> lstIdea = new List<Idea>();
        for(FindIdea c : RelatedIdeas){
            if(c.isCheck){
                lstIdea.add(c.IdeaObj);
            }
        }
        /*if(lstIdea.size() > 0){
            for(Idea i : lstIdea){
                //i.isMerged= true;
                i.ParentIdeaId = ideaObj.Id;
            }
            Database.SaveResult[] results = Database.update(ideaObj, lstIdea, false);           
        }*/
        isFindLayout = false;
    }
    public List<FindIdea> RelatedIdeas{get;set;}
    public pagereference findRelatedIdeas(){
        isFindLayout = true;
        RelatedIdeas = new List<FindIdea>();
        string query = 'select Id,title,body,VoteTotal,CreatedDate,CreatorName,Categories,(select Id from Comments) from Idea where id <> \''+ideaObj.id+'\' and isMerged = false ';
        string title;
        if(ideaObj.title.contains(' ')){
            for(string s : (ideaObj.title).split(' ')){
                if(s.trim() <> ''){
                    if(title == null)title = 'title like \'%'+s+'%\'';
                    else title += ' or title like \'%'+s+'%\'';                 
                }
            }
        }else{
            title = ' title like \'%'+ideaObj.title+'%\''; 
        }
        for(Idea i : database.query(query+' and ('+title+')')){
            integer commentCount = 0;
            if(i.Comments <> null)commentCount = i.Comments.size();
            string ideabody = i.body;
            if(i.body.length() > 100)ideabody = (i.body).substring(0,100)+'  ........';

            RelatedIdeas.add(new FindIdea(false,i,ideabody,commentCount));
        }
        return null;
    }
    
    public pagereference CancelFind(){
        isFindLayout = false;
        if(RelatedIdeas <> null)
        RelatedIdeas.clear();
        return null;
    }
    public class wIdea{
        public boolean isVoteEnable{get;set;}
        public boolean isPromoted{get;set;}
        public Idea IdeaObj{get;set;}
        public List<IdeaComment> lstComments{get;set;}
        public List<Internal_Idea_Comment__c> lstInternalComments{get;set;}
        public List<Task> lstTasks{get;set;}
        public wIdea(boolean isVoteEnable, boolean isPromoted, Idea IdeaObj, List<IdeaComment> lstComments,List<Task> lstTasks,List<Internal_Idea_Comment__c> lstInternalComments){
            this.isVoteEnable = isVoteEnable;
            this.isPromoted = isPromoted;
            this.IdeaObj = IdeaObj;
            this.lstComments = lstComments;
            this.lstTasks = lstTasks;
            this.lstInternalComments = lstInternalComments;
        }       
    
    }
    public boolean NewCommentEnable{get;set;}
    public IdeaComment newComment{get;set;}
    public Pagereference AddNewComment(){
        newComment = new IdeaComment();
        NewCommentEnable = true;
        return null;
    }
    public boolean NewInternalCommentEnable{get;set;}
    public Internal_Idea_Comment__c newInternalComment{get;set;}
    public Pagereference AddNewInternalComment(){
        newInternalComment = new Internal_Idea_Comment__c();
        NewInternalCommentEnable = true;
        return null;
    }
    public pagereference saveNewComment(){
        newComment.IdeaId = ideaObj.Id;
        insert newComment;
        Initialize();
        return null;
    }
    public pagereference saveNewInternalComment(){
        newInternalComment.Idea__c = ideaObj.Id;
        insert newInternalComment;
        Initialize();
        return null;
    }
    public Task newTask{get;set;}
    public boolean NewTaskEnable{get;set;}
    public Pagereference AddNewTask(){
        newTask = new Task();
        string url = '/apex/IdeaTaskCustom?ideaid='+ideaObj.Id;
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    public pagereference saveNewTask(){        
        //newTask.Idea__c = ideaObj.Id;
        insert newTask;
        Initialize();
        return null;
    }
    public pagereference makeVote(){
        string type = apexpages.currentpage().getParameters().get('type');
        try{
        Vote v = new Vote();
        v.Type = type;
        v.ParentId = ideaObj.Id;
        insert v;
        Initialize();
        }catch(Exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e+'-'+type));
        }
        return null;
    }
    public pagereference EditComment(){
        string CommentId = apexpages.currentpage().getParameters().get('CommentId');
        string isinternal = apexpages.currentpage().getParameters().get('isinternal');
        
        string url = '/apex/IdeaCommentCustom?id='+CommentId;
        
        if(isinternal == 'yes'){
            url += '&isinternal=yes';
        }

        url+= '&rtn=/apex/IdeaCustom?id='+ideaObj.Id+'&'+filter;
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    public pagereference EditTask(){
        string TaskId = apexpages.currentpage().getParameters().get('TaskId');
        string url = '/apex/IdeaTaskCustom?id='+TaskId+'&ideaid='+ideaObj.Id;
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    public void DeleteTask(){
        string TaskId = apexpages.currentpage().getParameters().get('TaskId');
        Task t = [select Id from Task where Id =: TaskId];
        delete t;
        Initialize();
    }
    public void DeleteComment(){
        string CommentId = apexpages.currentpage().getParameters().get('CommentId');
        IdeaComment ic = [select Id from IdeaComment where Id =: CommentId];
        delete ic;
        Initialize();
    }
    public void DeleteInternalComment(){
        string CommentId = apexpages.currentpage().getParameters().get('CommentId');
        Internal_Idea_Comment__c ic = [select Id from Internal_Idea_Comment__c where Id =: CommentId];
        delete ic;
        Initialize();
    }
    public pagereference ConvertCase(){
        
        
        Id helpdeskRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        case c = new Case();
        c.Subject = ideaObj.title;
        c.AccountId = ideaObj.Account__c;
        c.ContactId = ideaObj.Contact__c;
        c.Local_Subject__c = ideaObj.title;
        c.Description = ideaObj.body;
        c.Local_Description__c = ideaObj.body;
        c.Priority = 'Medium';
        c.Origin = 'Web';
        c.Status = 'New';
        c.RecordTypeId = helpdeskRTId;
        
        List<AssignmentRule> AR = new List<AssignmentRule>();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
        if(AR.size()>0)
        {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.assignmentRuleId= AR[0].id;
            c.setoptions(dmo);
        }
            
        insert c;
        
        ideaObj.Case_Number__c = c.Id;
        update ideaObj;
        Initialize();
        return null;
    }
    public boolean getisVotabel(){
        string ideaId = Apexpages.currentPage().getParameters().get('id');
        List<vote> v = [select Id from Vote where createdById =: userinfo.getUserId() and isDeleted = false and parentId =: ideaId];
        if(v.size()>0)return false;
        return true;
    }
    public void upVote(){
        Vote v = new Vote();
        v.type = 'up';
        v.PArentId = ideaObj.Id;
        insert v;
    }
    public void Initialize(){
        NewCommentEnable = false;
        NewInternalCommentEnable = false;
        string ideaId = Apexpages.currentPage().getParameters().get('id');
        if(ideaId <> null){
            List<IdeaComment> lstIdeaComment = [Select Id,CommentBody,createdBy.Name,CommunityId,CreatorFullPhotoUrl,CreatorName,CreatorSmallPhotoUrl,IdeaId,IsHtml,UpVotes,createdDate from IdeaComment where   IdeaId =: ideaId];
            List<Internal_Idea_Comment__c> lstInternalIdeaComment = [Select Id,Idea__c,createdBy.Name,Comment__c,createdDate from Internal_Idea_Comment__c where Idea__c =: ideaId];
            
            //List<Task> lstIdeaTask = [Select Id,Subject,ActivityDate,Status,Priority,OwnerId,CreatedDate ,createdBy.Name from Task ]; //Idea__c      where   Idea__c =: ideaId

             
            ideaObj = [select id,lastmodifiedById,LastModifiedDate,title,body,User_Submitted__c,Contact__c,Country__c,Release_Version__c,Product_Version__c,Product_Idea_ID__c,Territory__c,Functional_Role__c,Email__c,ClearQuest_Number__c,Account__c,Entered_By__c,Date_Entered__c,Priority__c,Complaint_Number_CP__c,Risk__c,Size__c,TFS_Item__c,Assigned_PdM__c,Assigned_group__c,Categories,Votescore,Case_Number__c, votetotal, CommunityId ,recordtypeid, Status ,Numcomments, lastreferencedDate, LastviewedDate, ismerged, IsHTML ,CreatorFullPhotoUrl, AttachmentName,createdDate,createdBy.Name,(select Id,Type,createdById from Votes where isDeleted = false) from Idea where Id =: ideaId];        
            boolean isVoteEnable = true;
            boolean isPromoted = false;
            
            List<Idea_Attachment__c> ideaJunctions = [select Id,Idea__c from Idea_Attachment__c where Idea__c =: ideaId];
            if(ideaJunctions.size() > 0 ){
                List<Attachment> lstAttachment = [select Id,Name from Attachment where parentId =: ideaJunctions[0].Id];
                if(lstAttachment.size() > 0 )
                lstIdeaAttachment = lstAttachment; //STSK0012697.Put all attachments in the list
            }
            
            
            
            if(ideaObj.ClearQuest_Number__c == null){
            ideaObj.Entered_By__c = userinfo.getUserId();
            ideaObj.Date_Entered__c = system.today();
            }
            
            Integer upvotes = 0;
            for(Vote v : ideaObj.Votes){
                isVoteEnable = false;
                if(v.Type == 'up'){isPromoted = true;upvotes++;
                }
                if(v.Type == 'down')isPromoted = false;
            }
            CommentCount = upvotes+' Like';
            if(upvotes>1)CommentCount += 's';
            if(ideaObj.Case_Number__c <> null)ConvertedCase = [select Id,CaseNumber,Subject,CreatedDate,CreatedById,CreatedBy.Name from Case where id = : ideaObj.Case_Number__c];
            //Obj = new wIdea(isVoteEnable,isPromoted,ideaObj,lstIdeaComment,lstIdeaTask,lstInternalIdeaComment);
            Obj = new wIdea(isVoteEnable,isPromoted,ideaObj,lstIdeaComment,null,lstInternalIdeaComment);
        }
    }
    public void saveIdea(){
        update ideaObj;
        Initialize();
    }
    public wIdea Obj{get;set;}
    public IdeaCustomController(ApexPages.StandardController controller) { 
        lstIdeaAttachment = new List<Attachment>(); //STSK0012697.Initialisation attachment list
        SB = apexpages.currentpage().getParameters().get('sb');
        PC = apexpages.currentpage().getParameters().get('pc');
        PG = apexpages.currentpage().getParameters().get('pg');
        ST = apexpages.currentpage().getParameters().get('st');
        
        if(SB <> null){if(filter <> null)filter += '&SB='+SB;else filter = 'SB='+SB;}
        if(PC <> null){if(filter <> null)filter += '&PC='+PC;else filter = 'PC='+PC;}
        if(PG <> null){if(filter <> null)filter += '&PG='+PG;else filter = 'PG='+PG;}
        if(ST <> null){if(filter <> null)filter += '&ST='+ST;else filter = 'ST='+ST;}
        
        //apexpages.addmessage(new apexpages.message(apexpages.severity.error,filter +''));
        InstanceURL =  URL.getSalesforceBaseUrl().toExternalForm();

        isFindLayout = false;
        SFURL = URL.getSalesforceBaseUrl().toExternalForm();
        Initialize();
    }
    public pagereference DeleteIdea(){
        try{
            delete ideaObj;         
        }catch(Exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,'Issue with delete : '+e.getMessage()));
            return null;
        }
        string url= '/apex/customIdeaListView?'+filter;
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }

    public pagereference DownloadFile(){
        pagereference pg = new Pagereference('/apex/IdeaAttachment');
        pg.setredirect(true);
        return pg;
    }
    public List<Vote> getIdeaVote(){
        List<Vote> lstVote = [select Id,Type,CreatedById,CreatedBy.Name from Vote where parentId =: ideaObj.Id ];
        return lstVote;
    }
    public pagereference EditIdea(){
        pagereference pg = new Pagereference('/apex/IdeaCustomEdit?id='+ideaObj.Id+'&'+filter+'&rtn=IdeaCustom?id='+ideaObj.Id);
        pg.setredirect(true);
        return pg;
    }
    
    
}
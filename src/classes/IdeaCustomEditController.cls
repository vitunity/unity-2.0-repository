/**************************************************************************\
Author: N/A
Created Date: N/A
Project/Story/Inc/Task : N/A
Description: N/A

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
31-Aug-2017 - Rakesh - STSK0012697 - Added new field ClearQuest_Number__c in the query (Line 32).
/**************************************************************************/

public class IdeaCustomEditController{
    
    public Idea ideaObj{get;set;}
    public string ContactId{get;set;}
    public Attachment IdeaAttachment{get;set;}
    
    public boolean isEditableProductGroup{get;set;}
    public string rtn{get;set;}
    public string body{get;set;}
    public string subject{get;set;}
    
    public IdeaCustomEditController(ApexPages.StandardController controller) {
        rtn = ApexPages.currentPage().getUrl(); 
        string ideaId = Apexpages.currentPage().getParameters().get('id');        
        isEditableProductGroup = false;
        set<string> setAriaProduct = new set<string>();
         map<string,IdeaEmailProductGroup__c> mapIdeaEmailCS = IdeaEmailProductGroup__c.getAll();
        for(string p: mapIdeaEmailCS.keyset()){
            if(p.equalsignorecase('ARIA and Related Products')){
                IdeaEmailProductGroup__c csObj = mapIdeaEmailCS.get(p);
                if(csObj.Member__c <> null){
                    for(string s : (csObj.Member__c).split(';')){
                        setAriaProduct.add(s.tolowercase());
                    }               
                }
            }            
        }
        
        if(ideaId <> null){
            ideaObj = [select id,title,body,AttachmentBody,Release_Version__c ,AttachmentContentType,User_Submitted__c,Contact__c,Contact__r.email,Categories,Priority__c,Risk__c,Size__c,TFS_Item__c,Complaint_Number_CP__c,ClearQuest_Number__c,Assigned_PdM__c,Assigned_group__c,Votescore,Case_Number__c, votetotal, CommunityId ,recordtypeid, Status ,Numcomments, lastreferencedDate, LastviewedDate, ismerged,IsHTML ,CreatorFullPhotoUrl, AttachmentName,createdDate,createdBy.Name from Idea where Id =: ideaId];
            
            if(ideaObj.Categories <> null){
                for(string s : (ideaObj.Categories).split(';')){
                    string temp = s.tolowercase();
                    if(setAriaProduct.contains(temp)){
                        isEditableProductGroup = true;
                    }
                } 
            }
        }else{
            List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];      
            if(lstc.size() > 0){
                string ZoneId = lstc[0].Id;             
                ideaObj = new Idea();
                isEditableProductGroup = true;
                ideaObj.CommunityId = ZoneId;
            }
            
        }
        IdeaAttachment = new Attachment();
        List<Idea_Attachment__c> ideaJunctions = [select Id,Idea__c from Idea_Attachment__c where Idea__c =: ideaId];
        if(ideaJunctions.size() > 0 ){
            List<Attachment> lstAttachment = [select Id,Name,Body,ContentType from Attachment where parentId =: ideaJunctions[0].Id];
            if(lstAttachment.size() > 0 )
            IdeaAttachment = lstAttachment[0];
        }
    }
    
    public pagereference initializeEmail(){
        List<EmailTemplate> lstET = [SELECT id FROM EmailTemplate WHERE developerName = 'Email_on_Product_Idea1'];
        if(IdeaObj<>null){
            ContactId = IdeaObj.Contact__c;
        }
        if(lstET.size() > 0 && ContactId <> null)
        {
            string TemplateId = lstET[0].Id;
            Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(TemplateId, ContactId, IdeaObj.Id);
            body= mail.getHtmlBody();
            subject= mail.getSubject();
            }
        return null;
    }
    
    public pagereference finalSendEmail(){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'MyVarian'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTargetObjectId(ContactId);
        System.debug('++++++++++++ContactId+++++++++++'+ContactId);
        mail.setwhatId(IdeaObj.Id);
        mail.setsaveasActivity(false);
        mail.setSubject(subject);
        mail.setHtmlBody(body);
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        try{
            Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            for(Messaging.SendEmailResult rr:r){
                System.debug('Email result ' + rr.IsSuccess());
            } 
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email Sent Successfully' ));
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email Sent Fail '+e.getMessage() ));
        }
        return Page.IdeaCustomEdit;
    }   
    
    public pagereference Save(){
        if(ideaObj.status == 'Released' && (ideaObj.Release_Version__c ==  null || ideaObj.Release_Version__c == '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Release version is required when status is released'));
            return null;
        }
        upsert ideaObj;
        
        
        if(IdeaAttachment.Id == null){
            Idea_Attachment__c IdeaAttachObj = new Idea_Attachment__c(Idea__c = ideaObj.Id );
            insert IdeaAttachObj;
            //attachment
            IdeaAttachment.parentId = IdeaAttachObj.Id;
            if(IdeaAttachment.parentId  <> null)
                Try{
                insert IdeaAttachment;
                }
                catch(exception e){}
        }else{
            update IdeaAttachment;
        }
        string url = '/apex/';
        string rtn = Apexpages.currentPage().getParameters().get('rtn');
        string uideaid = Apexpages.currentPage().getParameters().get('id');
        if(rtn <> null && rtn <> '')url += rtn;
        else if(uideaid <> null && uideaid <> '')url += 'IdeaCustom?id='+uideaid;
        else url += 'CustomIdeaListView';
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    
    public pagereference CancelEmail(){
        return Page.IdeaCustomEdit;
    }
    public pagereference Cancel(){
        string url = '/apex/';
        string rtn = Apexpages.currentPage().getParameters().get('rtn');
        string uideaid = Apexpages.currentPage().getParameters().get('id');
        if(rtn <> null && rtn <> '')url += rtn;
        else if(uideaid <> null && uideaid <> '')url += 'IdeaCustom?id='+uideaid;
        else url += 'CustomIdeaListView';
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    
    public List<Internal_Idea_Comment__c> getInternalIdeaComment(){
        List<Internal_Idea_Comment__c> lstIC = [select Id,comment__c,createdById,CreatedBy.Name from Internal_Idea_Comment__c where Idea__c =: ideaObj.id];
        return lstIC;
    }
    public void DeleteInternalComment(){
        string CommentId = apexpages.currentpage().getParameters().get('CommentId');
        Internal_Idea_Comment__c ic = [select Id from Internal_Idea_Comment__c where Id =: CommentId];
        delete ic;
    }
    public pagereference EditComment(){
        string CommentId = apexpages.currentpage().getParameters().get('CommentId');        
        string url = '/apex/IdeaCommentCustom?id='+CommentId+'&isinternal=yes&rtn='+rtn;
        
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }    
    public pagereference NewInternalComment(){
        string CommentId = 'new'; 
        string ideaId = Apexpages.currentPage().getParameters().get('id');     
        string url = '/apex/IdeaCommentCustom?id='+CommentId+'&isinternal=yes&ideaid='+ideaObj.id+'&rtn='+rtn;
        
        pagereference pg = new pagereference(url);
        pg.setredirect(true);
        return pg;
    }
    Public pagereference SendEmail(){
        if(IdeaObj<>null && IdeaObj.Contact__c <> null ){
            return Page.SendIdeaEmail;
        }
        else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email Sent Fail as there is no contact for this idea'));
            return null;
        }
    }
    
}
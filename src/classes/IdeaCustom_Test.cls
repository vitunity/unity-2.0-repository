@isTest
public class IdeaCustom_Test
{
    static testMethod void IdeaCustomController() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;
        
        Idea obj2 = new Idea();
        obj2.title='test Idea2';
        obj2.body = 'test123';
        obj2.CommunityId = lstc[0].Id;
        insert obj2;
        
        Task tsk = new task();
        tsk.ownerId = userinfo.getuserid();
        //tsk.Idea__c = obj.Id;
        insert tsk;
        
        
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        system.runAs(u)
        {  
            Test.StartTest();
                PageReference pageRef = Page.IdeaCustom;
                pageRef.getParameters().put('id', obj.Id);
                pageRef.getParameters().put('CommentId', com.Id);
                pageRef.getParameters().put('TaskId',tsk.Id);
                Test.setCurrentPage(pageRef);    
                ApexPages.StandardController stdHR = new ApexPages.StandardController(obj);
           
                IdeaCustomController contr = new IdeaCustomController(stdHR );
                contr.findRelatedIdeas();
                contr.getIdeaVote();
                contr.EditIdea();
                contr.makeVote();
                contr.DownloadFile();
                contr.AddNewComment();
                contr.saveNewComment();
                contr.AddNewInternalComment();
                contr.saveNewInternalComment();
                contr.AddNewTask();
                contr.saveNewTask();
                contr.EditComment();
                contr.EditTask();                
                contr.ConvertCase();                
                //contr.DeleteInternalComment();
                contr.CancelFind();
                contr.DeleteComment();
                contr.DeleteTask();
                contr.DeleteIdea();
                contr.MergeIdea();
            Test.StopTest();            
        }
    }
    static testMethod void IdeaCommentController() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;

      
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        system.runAs(u)
        {  
            Test.StartTest();
                PageReference pageRef = Page.IdeaCommentCustom;
                pageRef.getParameters().put('id', com.Id);
                pageRef.getParameters().put('ideaid',obj.Id);
                pageref.getParameters().put('isinternal','no');
                pageref.getParameters().put('rtn','testpage');
                Test.setCurrentPage(pageRef);    
                ApexPages.StandardController stdHR = new ApexPages.StandardController(com);
           
                IdeaCommentController contr = new IdeaCommentController();
                contr.Save();
                contr.Cancel();
            Test.StopTest();            
        }
    }
    static testMethod void IdeaInternalCommentController() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;

        Internal_Idea_Comment__c icom = new Internal_Idea_Comment__c();
        icom.idea__c = obj.Id;
        icom.Comment__c = 'test';
        insert icom;
        
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        system.runAs(u)
        {  
            Test.StartTest();
                PageReference pageRef = Page.IdeaCommentCustom;
                pageRef.getParameters().put('id', icom.Id);
                pageRef.getParameters().put('ideaid',obj.Id);
                pageref.getParameters().put('isinternal','yes');
                pageref.getParameters().put('rtn','testpage');
                Test.setCurrentPage(pageRef);    
                ApexPages.StandardController stdHR = new ApexPages.StandardController(com);
           
                IdeaCommentController contr = new IdeaCommentController();
                contr.Save();
                contr.Cancel();
            Test.StopTest();            
        }
    }
    
    @isTest(SeeAllData=true)
    static void IdeaEditController() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
         // insertAccount
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425',State_Province_Is_Not_Applicable__c=true);
        insert acc;
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='US' ; 
        insert reg ;
        
        // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.contact__c = con.Id;
        obj.CommunityId = lstc[0].Id;
        insert obj;

        
        Internal_Idea_Comment__c icom = new Internal_Idea_Comment__c();
        icom.idea__c = obj.Id;
        icom.Comment__c = 'test';
        insert icom;
        
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        Attachment at = new Attachment(Name = 'Test',body=Blob.valueOf('test'),ContentType='txt');
        
        system.runAs(u)
        {  
            Test.StartTest();
                PageReference pageRef = Page.IdeaCustomEdit;
                pageRef.getParameters().put('id', obj.Id);
                pageRef.getParameters().put('commentId', icom.Id);
                Test.setCurrentPage(pageRef);    
                
                ApexPages.StandardController stdHR = new ApexPages.StandardController(obj);
                IdeaCustomEditController contr = new IdeaCustomEditController(stdHR);
                contr.IdeaAttachment = at;
                contr.save();
                contr.getInternalIdeaComment();
                contr.NewInternalComment();
                contr.editComment();
                contr.DeleteInternalComment(); 
            	contr.initializeEmail();
                contr.SendEmail();
            	contr.finalSendEmail();
            	contr.CancelEmail();
                contr.cancel();
            
            Test.StopTest();
                
                ApexPages.StandardController stdHR2 = new ApexPages.StandardController(obj);
                IdeaCustomEditController contr2 = new IdeaCustomEditController(stdHR2);
                         
        }
    }
    
    
}
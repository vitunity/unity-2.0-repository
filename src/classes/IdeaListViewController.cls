/**************************************************************************\
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
20-Dec-2017 - Rakesh - INC4685978 - Added pagination logic to fix apex limit issue.
/**************************************************************************/
public class IdeaListViewController{
    
    string SB;
    string PG;
    string PC;
    string ST;
    
    map<string,set<string>> mapSupportCall = new map<string,set<string>>();
    
    
    public void getDefaultParameters(){
        Cookie CSB = ApexPages.currentPage().getCookies().get('SB');
        Cookie CPG = ApexPages.currentPage().getCookies().get('PG');
        Cookie CPC = ApexPages.currentPage().getCookies().get('PC');
        Cookie CST = ApexPages.currentPage().getCookies().get('ST');
        
        if(CSB <> null)SB = CSB.getValue();
        if(CPG <> null)PG = CPG.getValue();
        if(CPC <> null)PC = CPC.getValue();
        if(CST <> null)ST = CST.getValue();

    }
    public void setDefaultParameters(){
        
        Cookie CSB = new Cookie('SB', '',null,-1,false);
        Cookie CPG = new Cookie('PG', '',null,-1,false);
        Cookie CPC = new Cookie('PC', '',null,-1,false);
        Cookie CST = new Cookie('ST', '',null,-1,false);    
        
        if(SB <> null)CSB = new Cookie('SB', SB,null,-1,false);
        if(PG <> null)CPG = new Cookie('PG', PG,null,-1,false);
        if(PC <> null)CPC = new Cookie('PC', PC,null,-1,false);
        if(ST <> null)CST = new Cookie('ST', ST,null,-1,false);
        
        system.debug('LPLP :'+strSortBy +'-'+strIdeaP +'-'+strIdeaPG +'--'+strIdeaStatus );
        try{
        ApexPages.currentPage().setCookies(new Cookie[]{CSB,CPG,CPC,CST});
        }catch(Exception e){
            system.debug('LPLP :'+e.getMessage());
            
        }

    }
    public IdeaListViewController(ApexPages.StandardController controller) {      
        getDefaultParameters();
        
        strIdeaP = 'All';
        strIdeaPG = 'All';
        strIdeaStatus = 'All';
        strSortBy = 'Recent';
        
        if(PC<>null && PC<>''){strIdeaPG = PC;}
        if(PG<>null && PG<>''){strIdeaP = PG;}
        if(SB<>null && SB<>''){strSortBy = SB;}
        if(ST<>null && ST<>''){strIdeaStatus = ST;}
        
        map<string,IdeaEmailProductGroup__c> mapIdeaEmailCS = IdeaEmailProductGroup__c.getAll();
        for(string p: mapIdeaEmailCS.keyset()){
            IdeaEmailProductGroup__c csObj = mapIdeaEmailCS.get(p);
            set<string> setProduct = new set<string>();
            if(csObj.Member__c <> null){
                for(string s : (csObj.Member__c).split(';')){
                    setProduct.add(s);
                    if(mapSupportCall.containsKey(p)){
                        setProduct.addAll(mapSupportCall.get(p));
                    }
                    mapSupportCall.put(p,setProduct);
                }               
            }
        }
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];      
        if(lstc.size() > 0){
            ZoneId = lstc[0].Id;
        }
        
        setProducts = new set<string>();
        for(string s : mapSupportCall.keyset()){
            for(string p : mapSupportCall.get(s)){
                setProducts.add(p);
            }
        }
        changePG();        
    }
    
    private integer counter=0;  //keeps track of the offset
    private integer list_size=100; //sets the page size or number of rows
    public integer total_size; //Total size of the list
    
    string ZoneId;
    
    public class wrapperIdea{
        public string title{get;set;}
        public Idea i{get;set;}
        public Integer UpVoteCount{get;set;}
        public wrapperIdea( string title, Idea i){
            this.title = title;
            this.i = i;
            UpVoteCount = 0;
            if(i.Votes.size() > 0 )UpVoteCount = i.Votes.size();
        }
    }
    public string filter{get;set;}
    public List<wrapperIdea> getAllIdea(){

        
        List<wrapperIdea> lstIdea = new List<wrapperIdea>();   

        string query = 'select Id,Categories,CreatedDate,Title,Status,Product_Idea_ID__c,Contact__c,Account__c,Country__c,VoteTotal ,NumComments,ClearQuest_Number__c,Case_Number__c,(select Id from Votes where Type = \'up\') ';
        query += ' from Idea where id IN: setFilteredIds';
        if(strSortBy == 'Recent')query += ' Order By createdDate Desc';
        else if(strSortBy == 'Popular')query += ' Order By votetotal Desc';      
        query += ' limit :list_size offset :counter';        
        for(Idea i :  database.query(query)){
            string title = i.title;
            if(title.length() > 50)title = title.substring(0,50);
            lstIdea.add(new wrapperIdea(title,i));
        }
        return lstIdea;
    }
    public void DeleteIdea(){
        string IdeaId = apexpages.currentpage().getParameters().get('IdeaId');
        Idea ic = [select Id from Idea where Id =: IdeaId];
        delete ic;
    }
    public void Refreshlist(){
        strIdeaPG = 'All';
        strIdeaP = 'All';
        strIdeaStatus = 'All';
        Idea_TypeSearch = null;
        viewMyIdea = false;  
        filter = '';
        
        getIdeaProductGroup();
        getIdeaSortBy();
        getIdeaStatus();
        
    }
    public PageReference CreateNewIdea(){
        PageReference pg = new pagereference('/apex/IdeaCustomEdit?rtn=customIdeaListview&SB='+strSortBy+'&PC='+strIdeaPG+'&PG='+strIdeaP+'&ST='+strIdeaStatus);
        pg.setRedirect(true);
        return pg;
    }
    
    set<Id> setFilteredIds = new set<Id>();
    public void IdeaSearch(){
        //Rakesh.INC4685978.Get total number of record based on filters
        if(strSortBy <> null)SB=strSortBy;
        if(strIdeaP <> null)PG=strIdeaP;
        if(strIdeaPG <> null)PC=strIdeaPG;
        if(strIdeaStatus <> null)ST=strIdeaStatus;

        setDefaultParameters();

        string strProd;
        setProducts.clear();
        for(string s : mapSupportCall.keyset()){
            if(strIdeaPG == 'All'){
                for(string p : mapSupportCall.get(s)){
                    setProducts.add(p);
                }
            }
            else if(s == strIdeaPG){
                for(string p : mapSupportCall.get(s)){
                    setProducts.add(p);
                }
            }
        }

        if(setProducts <> null){
            for(string s : setProducts){
                if(strProd==null)strProd = '\''+s+'\'';
                else strProd += ',\''+s+'\'';
            }
            if(strProd <> null)strProd = ' ( '+strProd+' ) ';
        }
        string cUId = userinfo.getUserId();
        string query = 'select Id,Categories,CreatedDate,Title,Status,Product_Idea_ID__c,Contact__c,Account__c,Country__c,VoteTotal ,NumComments,ClearQuest_Number__c,Case_Number__c,(select Id from Votes where Type = \'up\') ';
        query += ' from Idea where title <> null';
        
        if(strIdeaP == 'All' && strProd <> null)query += ' and Categories INCLUDES '+strProd;
        else if(strIdeaP <> 'All')query += ' and Categories INCLUDES (\''+strIdeaP+'\')';
        if(strIdeaStatus <> 'All')query += ' and Status =: strIdeaStatus';
        if(strSortBy == 'JustReleased')query += ' and  Status = \'Released\'';
        setFilteredIds = new set<Id>();
        for(Idea i : database.query(query)){
            setFilteredIds.add(i.id);
        }
        total_size = setFilteredIds.size();
        counter = 0;
    }
    
    public string Idea_TypeSearch{get;set;}
    public string strIdeaPG{get;set;}
    public string strIdeaP{get;set;}
    public boolean viewMyIdea{get;set;}
    public List<SelectOption> lstProducts{get;set;}
    
    set<string> setProducts;
    public void changePG(){
        setProducts = new set<string>();
        lstProducts = new List<SelectOption>();
        lstProducts.add(new selectOption('All','All'));
        //strIdeaP = 'All';
        //if(PC <> null){strIdeaP = PC;PC=null;}
        //apexpages.addmessage(new apexpages.message(apexpages.severity.info ,'PROD::::'+strIdeaPG+'---'+strIdeaP));
        List<SelectOption> suboptions = new List<SelectOption>();
        for(string s : mapSupportCall.keyset()){
            if(strIdeaPG == 'All'){
                for(string p : mapSupportCall.get(s)){
                    suboptions.add(new selectOption(p,p));
                    setProducts.add(p);
                }
            }
            else if(s == strIdeaPG){
                for(string p : mapSupportCall.get(s)){
                    suboptions.add(new selectOption(p,p));
                    setProducts.add(p);
                }
            }
        }
        suboptions.sort();
        lstProducts.addAll(suboptions);
        
        IdeaSearch();
    }
    public List<SelectOption> getIdeaProductGroup() {
        setProducts = new set<string>();
        List<SelectOption> foptions = new List<SelectOption>();
        List<SelectOption> options = new List<SelectOption>();
        foptions.add(new SelectOption('All','All'));
        lstProducts = new List<SelectOption>();
        lstProducts.add(new selectOption('All','All'));
        List<SelectOption> suboptions = new List<SelectOption>();
        //strIdeaP = 'All';
        //strIdeaPG = 'All';
        
        //if(PC <> null){strIdeaP = PC;PC=null;}
        //if(PG <> null){strIdeaPG = PG;PG=null;}
        for(string s : mapSupportCall.keyset()){
            options.add(new SelectOption(s,s));
            for(string p : mapSupportCall.get(s)){
                suboptions.add(new selectOption(p,p));
                setProducts.add(p);
            }
        }
        
        suboptions.sort();
        lstProducts.addAll(suboptions);
        
        
        options.sort();
        foptions.addAll(options);
        return foptions;
    }

    
    public string strSortBy{get;set;}  
    public List<selectOption> getIdeaSortBy(){
        strSortBy = 'Recent';
        if(SB <> null){strSortBy = SB;SB=null;}
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Recent','Recent'));
        options.add(new SelectOption('Popular','Popular'));
        options.add(new SelectOption('JustReleased','Just Released'));
        return options;
    }
    public string strIdeaStatus{get;set;}   
    public List<SelectOption> getIdeaStatus() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        strIdeaStatus = 'All';
        if(ST <> null){strIdeaStatus = ST;ST=null;}
        for(string s : getPicklistValues('Idea','Status')){
            options.add(new SelectOption(s,s));
        }
        return options;
    }
    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){ 
        
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        
        return lstPickvals;
    }
    public pagereference reset(){
        strIdeaPG = 'All';
        strIdeaP = 'All';
        strIdeaStatus = 'All';
        Idea_TypeSearch = null;
        viewMyIdea = false;  
        filter = '';        
        return null;
    }
    
    
    public PageReference Beginning() {
        counter = 0;
        return null;
    }
    public PageReference Previous() {
        counter -= list_size;
        return null;
    }
    public PageReference Next() {
        counter += list_size;
        return null;
    }
    public PageReference End() { 
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
    public Boolean getDisablePrevious() { 
        if (counter>0) return false; else return true;
    }
    public Boolean getDisableNext() {
        if (counter + list_size < total_size) return false; else return true;
    }
    public Integer getTotal_size() {
        return total_size;
    }
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }
    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
}
@isTest(SeeAllData=true)
public class IdeaListView_Test
{
   
    static testMethod void IdeaListViewController() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;

        
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        system.runAs(u)
        {  
            Test.StartTest();
                PageReference pageRef = Page.CustomIdeaListView;
                pageRef.getParameters().put('id', obj.Id);
                pageRef.getParameters().put('IdeaId', obj.Id);
                Test.setCurrentPage(pageRef);    
                //ApexPages.StandardController stdHR1 = new ApexPages.StandardController(com);           
                //IdeaListViewController contr1 = new IdeaListViewController(stdHR1 );
                
                //ApexPages.StandardsetController stdHR = new ApexPages.StandardsetController(new List<Idea>{obj});
                ApexPages.StandardController stdHR = new ApexPages.StandardController(obj);            
                IdeaListViewController contr = new IdeaListViewController(stdHR );
                
                contr.getAllIdea();
                contr.Refreshlist();
                contr.CreateNewIdea();
                contr.IdeaSearch();
                contr.changePG();
                contr.reset();
                
                contr.Beginning();
                contr.Previous();
                contr.End();
                contr.Next();
                contr.getDisablePrevious();
                contr.getDisableNext();
                contr.getTotal_size();
                contr.getPageNumber();
                contr.getTotalPages();
                
                
                contr.DeleteIdea();
                
            Test.StopTest();            
        }
    }
    
    
}
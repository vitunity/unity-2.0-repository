global class InactiveRecordTypeUpdateForContact implements Database.Batchable<sObject>
{
    global final String query = 'select Id, AccountId, Inactive_Contact__c, Active_Contact__c from Contact';

    global Database.QueryLocator start(Database.BatchableContext BC){
    
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope)
    {
        for(sObject so : scope)
        {
            Contact con = (Contact)so;
            if(con.Inactive_Contact__c)
            {
                con.Active_Contact__c = null;
            }
            else
            {
                con.Active_Contact__c = con.AccountId;
            }
        }
        update scope; 
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}
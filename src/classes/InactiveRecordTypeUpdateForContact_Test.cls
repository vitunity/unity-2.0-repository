@IsTest
public class InactiveRecordTypeUpdateForContact_Test 
{
    static testMethod void testBatchJob()
    {
        Test.startTest();
           Account acc = new Account(Name = 'TestAprRel', OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
           insert acc;   
    
            Contact con = new Contact();
            con.FirstName = 'TestAprRel1FN';
            con.LastName = 'TestAprRel1';
            con.Email = 'Test@1234APR.com';
            con.AccountId = acc.Id;
            con.Mailingcountry = 'India';
            con.Medical_School_Country_Others__c = 'Ind';
            con.Medical_School_Others__c = 'Ind';
            con.Medical_School_State_Others__c = 'Kar';
            insert con; 
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.RunAs(thisUser) 
            {
                InactiveRecordTypeUpdateForContact batch = new InactiveRecordTypeUpdateForContact();
                Database.executeBatch(batch,1); 
            }
        Test.stopTest();
    }
}
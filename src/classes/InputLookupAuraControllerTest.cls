/*
 * Author: Enrico Murru (http://enree.co, @enreeco)
 */
@isTest
private class InputLookupAuraControllerTest {
    
    @testSetup
    private static void setup(){
        Account acc = new Account();
        acc.Name = 'test';
        acc.BillingCountry = 'USA';
        acc.BillingCity = 'Milpitas';
        acc.Country__c = 'USA';
        acc.BillingPostalCode = '360343';
        insert acc;
        Test.setFixedSearchResults(new List<String>{acc.Id});
        Contact conObj1 = new Contact(AccountId = acc.Id, LastName = 'Test1', FirstName = 'Name1',Phone='12134567890', Email='bac@test.com',MobilePhone='345342353');
        conObj1.MailingState = 'Mail';
        conObj1.Functional_Role__c = 'Physicist';
        insert conObj1;
        Contact conObj2 = new Contact(AccountId = acc.Id, LastName = 'Test2', FirstName = 'Name1',Phone='12134567890', Email='bac2@test.com',MobilePhone='345342353');
        conObj2.MailingState = 'Mail2';
        conObj2.Functional_Role__c = 'Physicist';
        insert conObj2;
        Contact conObj3 = new Contact(AccountId = acc.Id, LastName = 'Test3', FirstName = 'Name1',Phone='12134567890', Email='bac3@test.com',MobilePhone='345342353');
        conObj3.MailingState = 'Mail2';
        conObj3.Functional_Role__c = 'Physicist';
        insert conObj3;
       }
    
    private static testmethod void test_get_name(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact];
        
        Test.startTest();
        
        String ret = InputLookupAuraController.getCurrentValue(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', 'INVALID_ID');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('Contact', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('Contact', contacts[0].Id);
        System.assert(ret == contacts[0].Name, 'Should return '+contacts[0].Name+ ' ['+ret+']');
            
        Test.stopTest();
    }
    
    private static testmethod void test_search(){
        List<Contact> contacts = [Select Id, LastName, FirstName, Name From Contact];
        Test.startTest();
        
        String ret = InputLookupAuraController.searchSObject(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
       	ret = InputLookupAuraController.searchSObject('INVALID_OBJECT', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        InputLookupAuraController.searchSObject('Account', 'Test');
        ret = InputLookupAuraController.searchSObject('Contact', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isNotBlank(ret), 'Should return non null string ['+ret+']');
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
			List<InputLookupAuraController.SearchResult>.class);
        System.assert(sResList.isEmpty(), 'Why not empty list? ['+sResList.size()+' instead]');
        
        Test.setFixedSearchResults(new List<String>{contacts[0].Id,contacts[1].Id,contacts[2].Id});
        ret = InputLookupAuraController.searchSObject('Contact', 'Test');
        System.assert(String.isNotBlank(ret), 'Should return a serialized list string ['+ret+']');
        sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
			List<InputLookupAuraController.SearchResult>.class);
        System.assert(sResList.size() == 3, 'Why not 3 items found? ['+sResList.size()+' instead]');
        Test.stopTest();
    }
    
    private static testmethod void test_search3(){
        List<Account> accounts = [Select Id, Name From Account];
        Test.startTest();
        
        String ret = InputLookupAuraController.searchSObject(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
       	ret = InputLookupAuraController.searchSObject('INVALID_OBJECT', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        ret = InputLookupAuraController.searchSObject('Account', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isNotBlank(ret), 'Should return non null string ['+ret+']');
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
			List<InputLookupAuraController.SearchResult>.class);
        System.assert(sResList.isEmpty(), 'Why not empty list? ['+sResList.size()+' instead]');
        
        Test.setFixedSearchResults(new List<String>{accounts[0].Id});
        ret = InputLookupAuraController.searchSObject('Account', 'Test');
        System.assert(String.isNotBlank(ret), 'Should return a serialized list string ['+ret+']');
        sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
			List<InputLookupAuraController.SearchResult>.class);
        Test.stopTest();
    }
}
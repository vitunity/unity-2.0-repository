/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InstallProductLookupSearchControllerTest {

	static testMethod void getFormTag_Test() {
		PageReference pageRef = Page.InstalledProductLookupSearch;
        pageRef.getParameters().put('formid', '');
        pageRef.getParameters().put('lksrch','');
        pageRef.getParameters().put('top', 'yes');
        pageRef.getParameters().put('frm', 'testForm');
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
		String actualFrm = control.getFormTag();
		system.assertEquals('testForm', actualFrm);
	}
	
	static testMethod void getTextBox_Test() {
		PageReference pageRef = Page.InstalledProductLookupSearch;
        pageRef.getParameters().put('formid', '');
        pageRef.getParameters().put('lksrch','');
        pageRef.getParameters().put('top', 'yes');
        pageRef.getParameters().put('txt', 'testTEXT');
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
		String actualTEXT = control.getTextBox();
		system.assertEquals('testTEXT', actualTEXT);
	}
	
    static testMethod void InstalledProductLookupSerachController_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPCode = UnityDataTestClass.ERPPCODE_Data(true);
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	ip.SVMXC__Top_Level__c = null;
    	insert ip;
    	
    	PageReference pageRef = Page.InstalledProductLookupSearch;
        pageRef.getParameters().put('formid', '');
        pageRef.getParameters().put('lksrch','');
        pageRef.getParameters().put('top', 'yes');
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
    	
    }
    
    static testMethod void search_SearchStrNull_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPCode = UnityDataTestClass.ERPPCODE_Data(true);
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	ip.SVMXC__Top_Level__c = null;
    	insert ip;
    	
    	PageReference pageRef = Page.InstalledProductLookupSearch;
    	pageRef.getParameters().put('formid', '');
    	pageRef.getParameters().put('lksrch','');
        pageRef.getParameters().put('top', 'yes');
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
    	control.search();
    }
    
    static testMethod void search_SearchStrNOTNull_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPCode = UnityDataTestClass.ERPPCODE_Data(true);
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	ip.SVMXC__Top_Level__c = null;
    	insert ip;
    	
    	PageReference pageRef = Page.InstalledProductLookupSearch;
    	pageRef.getParameters().put('formid', '');
    	pageRef.getParameters().put('lksrch','H621598');
        pageRef.getParameters().put('top', 'yes');
        pageRef.getParameters().put('prcTempSel', newLoc.Id);
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
    	control.search();
    }
    
    static testMethod void search_SearchStrNOTNull_TOPNOTNULL_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPCode = UnityDataTestClass.ERPPCODE_Data(true);
    	
    	List<SVMXC__Installed_Product__c> ipList = new List<SVMXC__Installed_Product__c>();
    	SVMXC__Installed_Product__c topIp = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	topIp.Name = 'H621';
    	insert topIp;
    	
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	ip.SVMXC__Top_Level__c = topIp.Id;
    	insert ip;
    	
    	ip = [	SELECT Name,SVMXC__Product_Name__c,Product_Version_Build_Name__c, SVMXC__Status__c,SVMXC__Top_Level__c,SVMXC__Site__c 
    			FROM SVMXC__Installed_Product__c WHERE Id =: ip.Id];
    	system.debug(' ==== ip ==== ' + ip);
    	PageReference pageRef = Page.InstalledProductLookupSearch;
    	pageRef.getParameters().put('formid', '');
    	pageRef.getParameters().put('lksrch','H621598');
        pageRef.getParameters().put('top', 'no');
        pageRef.getParameters().put('prcTempSel', newLoc.Id);
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
    	control.search();
    }
    
    static testMethod void search_SearchStrNOTNull_NOTTOP_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPCode = UnityDataTestClass.ERPPCODE_Data(true);
    	
    	SVMXC__Installed_Product__c topIp = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	topIp.Name = 'H621';
    	insert topIp;
    	
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(false, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	ip.SVMXC__Top_Level__c = topIp.Id;
    	insert ip;
    	
    	PageReference pageRef = Page.InstalledProductLookupSearch;
    	pageRef.getParameters().put('formid', '');
    	pageRef.getParameters().put('lksrch','H621598');
        pageRef.getParameters().put('top', 'no');
        pageRef.getParameters().put('prcTempSel', newLoc.Id);
        Test.setCurrentPage(pageRef);
    	
    	InstalledProductLookupSerachController control = new InstalledProductLookupSerachController();
    	control.search();
    }
    
}
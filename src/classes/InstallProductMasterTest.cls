/**
 * This class contains unit tests for validating the behavior of Apex Trigger 'InstalledProductMaster_AfterTrigger'
 */

@isTest
private class InstallProductMasterTest {

    static testmethod void myTest() {
    
    Recordtype rt=[select id from Recordtype where Name='Sold To'];
    Account accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;
    
    Recordtype rt1=[select id from Recordtype where Name='Site Partner'];
    Account accnt2= new Account();
    accnt2.Name= 'Wipro Infotech';
    accnt2.BillingCity='New Jersey';
    accnt2.BillingCountry='USA';
    accnt2.BillingStreet='Jersey';
    accnt2.OMNI_Postal_Code__c='940023';
    accnt2.country__c='USA';
    accnt2.BillingPostalCode='552601';
    accnt2.BillingState='CA';
    insert accnt2;
    
    Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
    
    SVMXC__Installed_Product__c installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023css';
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
     accnt.install_product_count__c= 1;
    
    }
    static testmethod void myTest1() {
    
    Recordtype rt=[select id from Recordtype where Name='Sold To'];
    Account accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.BillingCountry='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.country__c='USA';
    accnt.BillingState='LA';
    insert accnt;
    
    Recordtype rt1=[select id from Recordtype where Name='Site Partner'];
    Account accnt2= new Account();
    accnt2.Name= 'Wipro Infotech';
    accnt2.BillingCity='New Jersey';
    accnt2.BillingCountry='USA';
    accnt2.BillingStreet='Jersey';
    accnt2.country__c='USA';
    accnt2.BillingPostalCode='552601';
    accnt2.BillingState='CA';
    accnt2.OMNI_Postal_Code__c='940022';
    insert accnt2;
    
    Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
    
                       
    
    
    SVMXC__Installed_Product__c installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023css'; 
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
    
    SVMXC__Installed_Product__c installpro1 = new SVMXC__Installed_Product__c();
    installpro1 .Name= 'Installed Product';
    installpro1 .SVMXC__Product__c= prod.id;
    installpro1 .SVMXC__Serial_Lot_Number__c='1023css';
    installpro1 .SVMXC__Company__c= accnt.id;
    installpro1 .SVMXC__Status__c='Installed';
    insert installpro1 ;
     
    accnt.install_product_count__c= 2;
     
     delete installpro1 ;
      
     accnt.install_product_count__c= 1;
    installpro.SVMXC__Company__c= accnt2.id;
    
    update installpro;
    
    accnt.install_product_count__c= 0;
    accnt2.install_product_count__c= 1;
    }
    }
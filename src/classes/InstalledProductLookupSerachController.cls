/***************************************************************************
Author: N/A
Created Date: N/A
Project/Story/Inc/Task : Unity 1C Project
Description: 
This is a controller for searching Installed Product

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
STSK0013530 - Removing key from mapInstalledProduct if it does not exist in mapTopInstalledProduct
*************************************************************************************/
public class InstalledProductLookupSerachController{
     
    public Map<String, list<SVMXC__Installed_Product__c>> mapInstalledProduct{get;set;}
    public Map<String, SVMXC__Installed_Product__c> mapTopInstalledProduct{get;set;}
    
    
    
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
     
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    public String emailId { get; set; }  // added by Nikita, 5/12/2014
    
    public String SOIId;
    public string formid{get;set;}
    public List<Product2> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    
        
    public InstalledProductLookupSerachController() {   
        formid=ApexPages.currentPage().getParameters().get('formid');
        system.debug('&&&&&'+formid);
        searchString = System.currentPageReference().getParameters().get('lksrch');
        search();        
    }
    
    public void search() {
        SOIId = ApexPages.currentPage().getParameters().get('prcTempSel'); 
        String locationId= SOIId  ; //[Select Location__c  from Sales_Order_Item__c where id =: SOIId].Location__c;   
        String query = 'select Name,SVMXC__Product_Name__c,Product_Version_Build_Name__c, SVMXC__Status__c,'+
                        'SVMXC__Top_Level__c from SVMXC__Installed_Product__c '+
                        'Where SVMXC__Site__c =:locationId ';
        if(String.isNotBlank(searchString)){
            searchString = searchString.trim();
            query = query+ ' and  ( name LIKE \'%' + searchString+'%\''; 
            query = query+ ' or SVMXC__Product_Name__c LIKE \'%' + searchString+'%\' ) ';
        }
        query = query + ' order by Name,SVMXC__Top_Level__c ASC '; //DFCT0012081 : Added order by Name Asc
        system.debug('Anydatatype_msg=======locationId'+locationId);
        system.debug('searchString=======searchString'+searchString);
        System.debug('searchquery ================'+query);
        
        list<SVMXC__Installed_Product__c> ipList = (list<SVMXC__Installed_Product__c>)database.query(query);
        System.debug('size return ================'+ipList.size());
        
        mapInstalledProduct = new Map<String,List<SVMXC__Installed_Product__c>>();
        Map<String,List<SVMXC__Installed_Product__c>> mapInstalledProdDummy = new Map<String,List<SVMXC__Installed_Product__c>>();
        mapTopInstalledProduct = new Map<String, SVMXC__Installed_Product__c>();
        set<Id> topLevelIds = new set<Id>();
        
        for(SVMXC__Installed_Product__c ip : ipList){
            if(ip.SVMXC__Top_Level__c != null){
                topLevelIds.add(ip.SVMXC__Top_Level__c);
                //if(mapInstalledProduct.containsKey(ip.SVMXC__Top_Level__c)){
                if(mapInstalledProdDummy.containsKey(ip.SVMXC__Top_Level__c)){
                //mapInstalledProduct.get(ip.SVMXC__Top_Level__c).add(ip);
                mapInstalledProdDummy.get(ip.SVMXC__Top_Level__c).add(ip);
                }else{
                    list<SVMXC__Installed_Product__c> temp = new list<SVMXC__Installed_Product__c> ();
                    temp.add(ip);
                    //mapInstalledProduct.put(ip.SVMXC__Top_Level__c,temp);
                    mapInstalledProdDummy.put(ip.SVMXC__Top_Level__c,temp);
                }        
            }
        }
        
        for(SVMXC__Installed_Product__c ip : ipList){
            if(ip.SVMXC__Top_Level__c == null){
                topLevelIds.add(ip.Id);
                //if(!mapInstalledProduct.containsKey(ip.Id)){
                if(!mapInstalledProdDummy.containsKey(ip.Id)){    
                    list<SVMXC__Installed_Product__c> temp = new list<SVMXC__Installed_Product__c> ();
                    //mapInstalledProduct.put(ip.Id,temp);
                    mapInstalledProdDummy.put(ip.Id,temp);
                }        
            }
        }
        system.debug(' ==== ' + mapInstalledProdDummy);
        Set<String> ipTopLevel_KeySet = new Set<String>();
        List<String> ipTopLevel_KeyList = new List<String>();
        if(!mapInstalledProdDummy.isEmpty()) {
            ipTopLevel_KeySet = mapInstalledProdDummy.keySet();
            ipTopLevel_KeyList.addAll(ipTopLevel_KeySet);
            system.debug(' ==B4 Sort== ' + ipTopLevel_KeyList);
            ipTopLevel_KeyList.sort();
            system.debug(' ==After Sort == ' + ipTopLevel_KeyList);
            for(Integer i = 0; i < ipTopLevel_KeyList.size(); i++ ){
                mapInstalledProduct.put(ipTopLevel_KeyList[i], mapInstalledProdDummy.get(ipTopLevel_KeyList[i]));
            }
        }

        system.debug('Anydatatype_msg=mapInstalledProduct==='+mapInstalledProduct);
        for(list<SVMXC__Installed_Product__c> ipLIstTemp : mapInstalledProduct.values()){
            system.debug('Anydatatype_msg=individaul==='+ipLIstTemp.size());
        }
        mapTopInstalledProduct =  new Map<String, SVMXC__Installed_Product__c>([select Name,SVMXC__Product_Name__c, 
                                                                                Product_Version_Build_Name__c, SVMXC__Status__c,
                                                                                SVMXC__Top_Level__c
                                                                                from SVMXC__Installed_Product__c
                                                                                where id in : topLevelIds]);
        system.debug('Anydatatype_msg=mapTopInstalledProduct==='+mapTopInstalledProduct);
        
        /* Start - STSK0013530 - Removing key from mapInstalledProduct if it does not exist in mapTopInstalledProduct */
        for(String key: mapInstalledProduct.KeySet()) {
            if (!mapTopInstalledProduct.containsKey(key)) {
                mapInstalledProduct.remove(key);
            }
        }
        /* End - STSK0013530 - Removing key from mapInstalledProduct if it does not exist in mapTopInstalledProduct */
    }
}
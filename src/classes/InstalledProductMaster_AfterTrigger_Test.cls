/**
*   Author: Yogesh Dixit
*   Date: 15 June 2016
* SeeAllData is set to true because trigger accesses the custom settings.
**/

@isTest(SeeAllData=true)
public class InstalledProductMaster_AfterTrigger_Test
{
    private static Account getTestAccount(String accName)
    {
        Id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= accName;
        testAcc.recordtypeId = rec;
        insert testAcc;
        return testAcc;
    }

    private static Product2 getProduct()
    {
        Product2 prods;
        /*Product2 prods = new Product2();
        prods.Name = 'ARIA for Medical Oncology';
        prods.Product_Group__c ='ARIA for Medical Oncology';
        prods.ProductCode = 'CLO001001001';*/
        List<Product2> prodList = [Select Id From Product2 Where ProductCode = 'CLO001001001'];
        if(prodList.size() == 0)
        {
            prods.Name = 'ARIA for Medical Oncology';
            prods.Product_Group__c ='ARIA for Medical Oncology';
            prods.ProductCode = 'CLO001001001';
        }
        else
        {
            prods = prodList[0];
        }
        //insert prods;
        return prods;
    }

    @isTest
    public static void testInsert()
    {
        Account testAcc = getTestAccount('TestAcc');
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14071', SVMXC__Status__c ='Installed'
                                                                            ,SVMXC__Company__c = testAcc.id);
        objIP.SVMXC__Product__c = getProduct().Id;
        Test.startTest();
        insert objIP;
        Test.stopTest();
        SVMXC__Installed_Product__c fetchedProd = [Select Id from SVMXC__Installed_Product__c Where Name='H14071'];
        System.assertEquals(fetchedProd.Id, objIP.Id);
    }

    @isTest
    public static void testUpdate()
    {
        Account testAcc1 = getTestAccount('TestAcc1');
        //Account testAcc2 = getTestAccount('TestAcc2');
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed'
                                                                            ,SVMXC__Company__c = testAcc1.id);
        insert objIP;


        objIP.SVMXC__Company__c = null;//testAcc2.id;

        Test.startTest();
        update objIP;
        Test.stopTest();

        SVMXC__Installed_Product__c fetchedProd = [Select Id, SVMXC__Company__r.Id from SVMXC__Installed_Product__c Where Name='H14072'];
        //System.assertEquals(testAcc2.Id, fetchedProd.SVMXC__Company__r.Id);

    }

    @isTest static void testDelete()
    {
        Account testAcc3 = getTestAccount('TestAcc3');
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14073', SVMXC__Status__c ='Installed'
                                                                            ,SVMXC__Company__c = testAcc3.id);
        insert objIP;

        Test.startTest();
        delete objIP;
        Test.stopTest();

        List<SVMXC__Installed_Product__c> fetchedProd = [Select Id from SVMXC__Installed_Product__c Where Name='H14073'];
        System.assertEquals(0, fetchedProd.size());

    }
}
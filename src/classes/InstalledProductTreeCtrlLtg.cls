/*
* Author: Amitkumar Katre
* Created Date: 15-May-2017
* Project/Story/Inc/Task : Installed product/ lightning project 
* Description: This will be used to create installed product tree view based on top level product.
*Last Modified By    :   Rishabh Verma
*Last Modified On    :   29/08/2017
* User Story     : STSK0012378
*/
public class InstalledProductTreeCtrlLtg{

    private ApexPages.StandardController sc;
    
    public InstalledProductTreeCtrlLtg(ApexPages.StandardController sc) {
        String recordId = sc.getId();
        Map<String,String> keys = new Map<String,String>();
        Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
        for(String s:describe.keyset()){
            keys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
        }
        if(!System.Test.isRunningTest() 
           && keys.get(String.valueOf(recordId).substring(0,3)) == 'SVMXC__Installed_Product__c'){
               sc.addFields(new List<String>{'SVMXC__Top_Level__c'});
           }
        this.sc = sc;
    }
    
    /*
     * Return installed product list 
     */
    public List<SVMXC__Installed_Product__c> getIPData() {  
        String searchId;
        if(sc.getRecord() instanceof SVMXC__Installed_Product__c){
            SVMXC__Installed_Product__c ipRecord = (SVMXC__Installed_Product__c) sc.getRecord();
            searchId = ipRecord.SVMXC__Top_Level__c != null?ipRecord.SVMXC__Top_Level__c:ipRecord.Id;
        }
        return [Select Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, 
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c,
                (SELECT Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, 
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c  FROM  R00N70000001hzcvEAA__r) 
                FROM SVMXC__Installed_Product__c               
                where (SVMXC__Company__c =: sc.getId() 
                       or SVMXC__Site__c =:sc.getId()
                       or Id =: searchId) 
                and SVMXC__Top_Level__c = null];
    }

    /*
     * Return installed product list with site and account
     */
    public map<String,AccountIpData> getAccountIPData() {  
        String searchId;
        if(sc.getRecord() instanceof SVMXC__Installed_Product__c){
            SVMXC__Installed_Product__c ipRecord = (SVMXC__Installed_Product__c) sc.getRecord();
            searchId = ipRecord.SVMXC__Top_Level__c != null?ipRecord.SVMXC__Top_Level__c:ipRecord.Id;
        }
       map<String,AccountIpData> accountIPDataMap;
       accountIPDataMap = new map<String,AccountIpData> ();
       for(SVMXC__Installed_Product__c ipObj : [Select Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, 
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c,SVMXC__Site__r.Name,
                (SELECT Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, 
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c  FROM  R00N70000001hzcvEAA__r) 
                FROM SVMXC__Installed_Product__c               
                where (SVMXC__Company__c =: sc.getId() 
                       or SVMXC__Site__c =:sc.getId()
                       or Id =: searchId) 
                 and SVMXC__Top_Level__c = null]){
        
            if(accountIPDataMap.containsKey(ipObj.SVMXC__Site__c)){
                 AccountIpData aWrapObj = accountIPDataMap.get(ipObj.SVMXC__Site__c);
                 aWrapObj.ipData.add(ipObj);
            }else{
                 AccountIpData aWrapObj = new AccountIpData();
                 aWrapObj.location = ipObj.SVMXC__Site__r;
                 list<SVMXC__Installed_Product__c> ipData = new list<SVMXC__Installed_Product__c> {ipObj};
                 aWrapObj.ipData = ipData;
                 accountIPDataMap.put(ipObj.SVMXC__Site__c,aWrapObj); 
            }          
        }
        return accountIPDataMap;
    }
    /*
     * Data wrraper for Account ->Site->Id mapping
     */ 
    public Class AccountIpData{
        public SVMXC__Site__c location {get;set;}
        public list<SVMXC__Installed_Product__c> ipData{get;set;}
    }
    
}
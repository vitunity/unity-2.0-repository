@isTest
public class InstalledProductTreeCtrlLtgTest{

public static SVMXC__Installed_Product__c installpro;
static{
    Account accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;
    
     Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
     installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023';
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
     accnt.install_product_count__c= 1;

}

static testMethod void testmethod1()
{
//ApexPages.StandardController sc = new ApexPages.StandardController();
//InstalledProductTreeCtrlLtg ipctr = new InstalledProductTreeCtrlLtg(sc);
//InstalledProductTreeCtrlLtg.getIPData();
ApexPages.StandardController sc = new ApexPages.StandardController(installpro);
InstalledProductTreeCtrlLtg ipctr = new InstalledProductTreeCtrlLtg(sc);
ipctr.getAccountIPData();
ipctr.getIPData();
InstalledProductTreeCtrlLtg.AccountIpData ipd = new InstalledProductTreeCtrlLtg.AccountIpData();
}

}
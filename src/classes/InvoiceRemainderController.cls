/************************************************************************************
 * @name - InvoiceRemainderController.cls
 * @author - Ram
 * @description - InvoiceRemanderTemplate controller which sends email using workflow related to pending invoice collection 
 * @date 1/3/2017
 *  ************************************************************************************/

public class InvoiceRemainderController {
    
    public Contact invContact {get; set;}
    public Id contactId {get; set;}
    public Id accountId {get; set;}
    public Integer numberOfDays {get; set;}
    public decimal invoiceTotal {get; set;}
    public String currencyCode {get; set;}
    public Date invoicePayDueDate {get; set;}
    public Integer oldestInvoices {get; set;}
    public String remainderType {get; set;}
    public List<Invoice__c> invoices {get; set;}
    static final Integer BEGIN_DAY = 6, END_DAY = 26;
    
    public InvoiceRemainderController() {
    	invoiceTotal = 0;
    	invoices = new List<Invoice__c>();
    }
    
    public List<Invoice__c> getInvoiceData() {
    	invoicePayDueDate = System.Today() + 5;
    	try {
    		if (contactId != null) {
	    		invContact = [Select c.Name, c.MailingStreet, c.MailingState, c.MailingPostalCode, c.MailingCountry, c.MailingCity, c.Id, c.Email, c.Account.Name, c.AccountId 
	    			From Contact c where c.Id =:contactId];
	    		//System.debug(logginglevel.info, 'invContact === '+invContact);
    		}
	    	if (invContact != null && invoices.isEmpty()) {
	    		Date tempDate = System.Today() - InvoiceRemainderController.END_DAY;
	    		Date tempDate2 = System.Today() - InvoiceRemainderController.BEGIN_DAY;
	    		String paid = 'Paid'; 
	    		System.debug(logginglevel.info, 'tempDate === '+tempDate+', tempDate2'+tempDate2);
	    		String baseQuery = 'Select Id, Name, Amount__c, Invoice_Date__c, Invoice_Due_Date__c, Days_Past_Due__c, Sold_To__c From Invoice__c ';
	    		String whereClause = 'Status__c != :paid AND Invoice_Due_Date__c <= :tempDate2 AND Invoice_Due_Date__c >= :tempDate AND Sold_To__c =:accountId AND Billing_Contact__c = :contactId';
	    		if (String.isNotBlank(currencyCode)) {
	    			whereClause = whereClause + ' AND CurrencyIsoCode = :currencyCode';
	    		}
	    		String fullQuery = baseQuery + ' Where '+whereClause + ' order by Invoice_Due_Date__c asc LIMIT 1000';
	    		System.debug(logginglevel.info, 'Full Query == '+fullQuery);
	    		invoices = (List<Invoice__c>) Database.query(fullQuery);
	    		oldestInvoices = invoices.size() > 0 ? (Integer) invoices[0].Days_Past_Due__c : 0;
	    		for (Invoice__c inv : invoices) {
	    			invoiceTotal = invoiceTotal + inv.Amount__c;	
	    		}
	    	}	
    	} catch(Exception ex) {
    		System.debug(logginglevel.error, 'error occured = '+ex+', line # '+ex.getLineNumber());	
    	}
    	return invoices;
    }
}
/*
//11-Jul-2018, STRY0067325:  Add Active check for user to avoid validation error.  Add Contact sharing.
 */

public with sharing class InvoiceTriggerHandler extends TriggerDispatcher.TriggerDispatcherBaseImpl {
    
    public override void beforeInsert(List<sObject> newValues) 
    { 
        Set<String> woNums = new Set<String>();
        Map<String, Id> woNumToId = new Map<String, Id>();

        Set<String> quoteNums = new Set<String>();
        Map<String, Id> quoteNumToId = new Map<String, Id> ();     

        Set<String> soNums = new Set<String>();
        Map<String, Id> soNumToId = new Map<String, Id>();   

        Set<String> siteNums = new Set<String>();
        Map<String, Id> siteNumToId = new Map<String, Id>(); 

        Set<String> soldNums = new Set<String> ();
        Map<String, Id> soldNumToId = new Map<String, Id>(); 

        Map<String, Id> serviceManagerToId = new Map<String, Id>(); 
        Map<String, Id> salesManagerToId = new Map<String, Id>(); 

        Set<String> scNums = new Set<String>();
        Map<String, Id> scNumToId = new Map<String, Id>();


        Id siteTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        Id soldTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();

        for(sObject s : newValues)
        {
            Invoice__c inv = (Invoice__c)s;
            if(String.isNotBlank(inv.ERP_WO__c))
            {
                woNums.add(inv.ERP_WO__c);
            }

            if(String.isNotBlank(inv.ERP_BMI_Quote__c))
            {
                quoteNums.add(inv.ERP_BMI_Quote__c);
            }

            if(String.isNotBlank(inv.ERP_SO__c))
            {
                soNums.add(inv.ERP_SO__c);
            }

            if(String.isNotBlank(inv.ERP_Site_Number__c))
            {
                siteNums.add(inv.ERP_Site_Number__c);
            }

            if(String.isNotBlank(inv.ERP_Sold__c))
            {
                soldNums.add(inv.ERP_Sold__c);
            }

            if(String.isNotBlank(inv.ERP_Contract__c))
            {
                scNums.add(inv.ERP_Contract__c);
            }
        }
        for(SVMXC__Service_Order__c wo : [select Id, Name from SVMXC__Service_Order__c where Name in: woNums])
        {
            woNumToId.put(wo.Name, wo.Id);
        }

        for(BigMachines__Quote__c qu : [select Id, Name from BigMachines__Quote__c where Name in: quoteNums])
        {
            quoteNumToId.put(qu.Name, qu.Id);
        }

        for(Sales_Order__c so : [select Id, Name from Sales_Order__c where Name in: soNums])
        {
            soNumToId.put(so.Name, so.Id);
        }

        for(Account acc : [select Id, recordTypeId, District_Service_Manager__c, District_Sales_Manager__c, AccountNumber, ERP_Site_Partner_Code__c, Ext_Cust_Id__c from Account where ERP_Site_Partner_Code__c in: siteNums or Ext_Cust_Id__c in: soldNums])
        {
            if(acc.ERP_Site_Partner_Code__c != null)
            {
                siteNumToId.put(acc.ERP_Site_Partner_Code__c, acc.Id);
            }
            if(acc.recordTypeId == soldTypeId && acc.Ext_Cust_Id__c != null)
            {
                soldNumToId.put(acc.Ext_Cust_Id__c, acc.Id);
                if(acc.District_Service_Manager__c != null)
                {
                    serviceManagerToId.put(acc.Ext_Cust_Id__c, acc.District_Service_Manager__c);
                }
                if(acc.District_Sales_Manager__c != null)
                {
                    salesManagerToId.put(acc.Ext_Cust_Id__c, acc.District_Sales_Manager__c);
                }
            }
        }

        for(SVMXC__Service_Contract__c ssc : [select Id, Name from SVMXC__Service_Contract__c where Name in: scNums])
        {
            scNumToId.put(ssc.Name, ssc.Id);
        }

        for(sObject s : newValues)
        {
            Invoice__c inv = (Invoice__c)s;
            if(String.isNotBlank(inv.ERP_WO__c) && woNumToId.keySet().contains(inv.ERP_WO__c))
            {
                inv.Work_Order__c = woNumToId.get(inv.ERP_WO__c);
            }

            if(String.isNotBlank(inv.ERP_BMI_Quote__c) && quoteNumToId.keySet().contains(inv.ERP_BMI_Quote__c))
            {
                inv.Quote__c = quoteNumToId.get(inv.ERP_BMI_Quote__c);
            }  

            if(String.isNotBlank(inv.ERP_SO__c) && soNumToId.keySet().contains(inv.ERP_SO__c))
            {
                inv.Sales_Order__c = soNumToId.get(inv.ERP_SO__c);
            }

            if(String.isNotBlank(inv.ERP_Site_Number__c) && siteNumToId.keySet().contains(inv.ERP_Site_Number__c))
            {
                inv.Site_Partner__c = siteNumToId.get(inv.ERP_Site_Number__c);
            }
            if(String.isNotBlank(inv.ERP_Sold__c) && soldNumToId.keySet().contains(inv.ERP_Sold__c))
            {
                inv.Sold_To__c = soldNumToId.get(inv.ERP_Sold__c);
            }

            if(String.isNotBlank(inv.ERP_Contract__c) && scNumToId.keySet().contains(inv.ERP_Contract__c))
            {
                inv.Service_Maintenance_Contract__c = scNumToId.get(inv.ERP_Contract__c);
            }

            if(String.isNotBlank(inv.ERP_Sold__c) && serviceManagerToId.keySet().contains(inv.ERP_Sold__c))
            {
                inv.District_Service_Manager__c = serviceManagerToId.get(inv.ERP_Sold__c);
            }

            if(String.isNotBlank(inv.ERP_Sold__c) && salesManagerToId.keySet().contains(inv.ERP_Sold__c))
            {
                inv.District_Sales_Manager__c = salesManagerToId.get(inv.ERP_Sold__c);
            }

            if(inv.Sold_To__c == null)
            {
                inv.Sold_To__c = Label.Temporary_Invoice_account;
            }
        }
        populateBillingContact(newValues);
    }
    

    public override void afterInsert(List<sObject> newValues, Map<Id, sObject> newValuesMap) 
    { 
        createSharesForCRA(newValues, newValuesMap);
    }
        
    public override void beforeUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, List<sObject> newValues, Map<Id, sObject> newValuesMap) { 
        List<Invoice__c> newinv = (List<Invoice__c>) newValues;
        Map<Id,Invoice__c> oldinv = (Map<Id,Invoice__c>) oldValuesMap;
        for(Invoice__c inv: newinv) {
                 if ( (inv.Reason_Code__c != null && ( oldinv == null || (oldinv != null && inv.Reason_Code__c != oldinv.get(inv.id).Reason_Code__c ) ))|| 
                    (inv.Reason_Detail__c != null && ( oldinv == null || (oldinv != null && inv.Reason_Detail__c != oldinv.get(inv.id).Reason_Detail__c  )) ))
                    {
                    inv.interface_status__c = 'Process';
                }
          
          }
          
    }
        
    public override void afterUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, List<sObject> newValues, Map<Id, sObject> newValuesMap) { 
        //CM : STRY0014558  : Added the below logic : Jan 25 2017.
        if (system.label.Inv_Trigger_Update == 'True')
        {
            createSharesForCRA(newValues, newValuesMap);
        }    }
    
    private void createSharesForCRA(List<sObject> newValues, Map<Id, sObject> newValuesMap)
    {
        Set<Id> accIds = new Set<Id>();
        Set<Id> conIds = new Set<Id>();  
        Set<Id> quoteIds = new Set<Id>();  
        Set<Id> invoiceIds = new Set<Id>();
        Set<Id> woIds = new Set<Id>();
        List<sObject> quoteShare = new List<sObject>();
        List<sObject> woShare = new List<sObject>();
        for(sObject s : newValues)
        {
            Invoice__c inv = (Invoice__c)s;
            if(inv.Site_Partner__c != null)
            {
                accIds.add(inv.Site_Partner__c);
            } 
            if(inv.Sold_To__c != null)
            {
                accIds.add(inv.Sold_To__c);
            }
            if(inv.Quote__c != null)
            {
                quoteIds.add(inv.Quote__c);
            }
            if(inv.Work_Order__c != null)
            {
                woIds.add(inv.Work_Order__c);
            }
            invoiceIds.add(inv.Id);
        }
        //add Contact check for sharing rule on quote.  Should be Acct Payable Contact and MyVarian User.
        for(Contact_Role_Association__c acc : [select Id, Account__c, Contact__c, Role__c from Contact_Role_Association__c where Account__c in: accIds /*and (Role__c = 'Account Payable Capital' or Role__c = 'Account Payable Service' or Role__c = 'Account Payable Capital & Service')*/ AND Contact__r.Accts_Payable_Contact__c=true AND Contact__r.MyVarian_Member__c=true])
        {
            conIds.add(acc.Contact__c);
        }
		//add Contact check for sharing rule on quote.  Should be Acct Payable Contact and MyVarian User.
		for(Contact con : [SELECT Id, AccountId, Functional_Role__c FROM Contact WHERE AccountId in: accIds AND Accts_Payable_Contact__c=true AND MyVarian_Member__c=true AND Inactive_Contact__c=false])
        {
            conIds.add(con.Id);
        }
        //update user query to only select active
        for(User user : [select Id, ContactId from User where ContactId in: conIds AND IsActive=True])
        {
            for(Id id : quoteIds)
            {
                quoteShare.add(dynamicShare('BigMachines__Quote__Share', id, user.Id));
            }
            /*
            for(Id id : woIds)
            {
                woShare.add(dynamicShare('SVMXC__Service_Order__Share', id, user.Id));
            }
            */
        }

        system.debug('#### quoteShare = ' + quoteShare);

        if(quoteShare.size() > 0)
        {   
            ////insert quoteShare;
            
            try{
                insert quoteShare;
            } catch (exception e) {
                system.debug('#### debug error found in createSharesForCRA while inserting quoteShare = ' + e.getMessage());
            }
            
        }
        if(woShare.size() > 0)
        {
            insert woShare;
        }
    }
	@testvisible
    private sObject dynamicShare(String objectName, Id parentId, Id userId)
    {
        Schema.SObjectType testShareType = Schema.getGlobalDescribe().get(objectName);
        SObject testShare = testShareType.newSObject();
        testShare.put('ParentId', parentId);
        testShare.put('UserOrGroupId', userId);
        testShare.put('AccessLevel', 'Read');
        testShare.put('RowCause', 'Manual');
        return testShare;
    }

    private void populateBillingContact(List<sObject> newValues)
    {
        Map<Id, Id> invIdToSiteId = new Map<Id, Id>();
        Map<Id, Id> invIdToSoldToId = new Map<Id, Id>();    
        Map<Id, Contact_Role_Association__c> siteAccountToCRAs = new Map<Id, Contact_Role_Association__c>();
        Map<Id, Contact> siteAccountToContacts = new Map<Id, Contact>();
        Map<Id, Contact_Role_Association__c> soldAccountToCRAs = new Map<Id, Contact_Role_Association__c>();
        Map<Id, Contact> soldAccountToContacts = new Map<Id, Contact>();
        List<Invoice__c> updatedInvoices = new List<Invoice__c>();
        for(sObject s : newValues)
        {
            Invoice__c inv = (Invoice__c)s;
            if(inv.Site_Partner__c != null)
            {
                invIdToSiteId.put(inv.Id, inv.Site_Partner__c);
            } 
            if(inv.Sold_To__c != null)
            {
                invIdToSoldToId.put(inv.Id, inv.Sold_To__c);
            }
        }

        for(Account acc : [select Id, (select Id, Account__c, Contact__c, Role__c, Contact__r.Primary_Billing_Contact__c from Contact_Role_Associations__r where Contact__r.Primary_Billing_Contact__c = TRUE), (select Id from Contacts where Primary_Billing_Contact__c = TRUE) from Account where Id in: invIdToSiteId.values()])
        {
            for(Contact_Role_Association__c cra : acc.Contact_Role_Associations__r)
            {
                siteAccountToCRAs.put(acc.Id, cra);
            }
            for(Contact con : acc.Contacts)
            {
                siteAccountToContacts.put(acc.Id, con);
            }
        }

        for(Account acc : [select Id, (select Id, Account__c, Contact__c, Role__c, Contact__r.Primary_Billing_Contact__c from Contact_Role_Associations__r where Contact__r.Primary_Billing_Contact__c = TRUE), (select Id from Contacts where Primary_Billing_Contact__c = TRUE) from Account where Id in: invIdToSoldToId.values()])
        {
            for(Contact_Role_Association__c cra : acc.Contact_Role_Associations__r)
            {
                soldAccountToCRAs.put(acc.Id, cra);
            }
            for(Contact con : acc.Contacts)
            {
                soldAccountToContacts.put(acc.Id, con);
            }
        }      
        for(sObject s : newValues)
        {
            Invoice__c inv = (Invoice__c)s;
            if(soldAccountToCRAs.keySet().contains(inv.Sold_To__c))
            {
                inv.Billing_Contact__c = soldAccountToCRAs.get(inv.Sold_To__c).Contact__c;
            } 
            else if(siteAccountToCRAs.keySet().contains(inv.Site_Partner__c))
            {
                inv.Billing_Contact__c = siteAccountToCRAs.get(inv.Site_Partner__c).Contact__c;
            }
            if(soldAccountToContacts.keySet().contains(inv.Sold_To__c))
            {
                inv.Billing_Contact__c = soldAccountToContacts.get(inv.Sold_To__c).Id;
            }
            else if(siteAccountToContacts.keySet().contains(inv.Site_Partner__c))
            {
                inv.Billing_Contact__c = siteAccountToContacts.get(inv.Site_Partner__c).Id;
            }    
        }
    }
    /*
    public void update_interface_status ( List<Invoice__c> newinv, map<id,Invoice__c> oldinv )
    {
      
      for(Invoice__c inv: newinv)
          {
                 if ( (inv.Reason_Code__c != null && ( oldinv == null || (oldinv != null && inv.Reason_Code__c != oldinv.get(inv.id).Reason_Code__c ) ))|| 
                    (inv.Reason_Detail__c != null && ( oldinv == null || (oldinv != null && inv.Reason_Detail__c != oldinv.get(inv.id).Reason_Detail__c  )) ))
                    {
                    inv.interface_status__c = 'Process';
                }
          
          }
    
    }*/
}
@isTest(seealldata=true)
private class InvoiceTriggerHandlerTest {
    
	public static Id recTypeIDTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
    public static ERP_Timezone__c etz ;
    public static Account acc ;
    public static SVMXC__Site__c varLoc;
    public static Contact con ;
    public static SVMXC__Service_Group__c objServTeam;
    public static SVMXC__Service_Group_Members__c techEqipt,techEqipt2 ;
    public static SVMXC__Installed_Product__c objIP, objIP2 ;
    public static Case testcase, testcase2;
    public static SVMXC__Case_Line__c caseLine ;
    public static ERP_NWA__c erpnwa;
    public static Invoice__c inc;
    public static BigMachines__Quote__c bmQuote;
    static
    {
        etz = new ERP_Timezone__c(Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)', name='Aussa');
        insert etz;
        
        // insertAccount 
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',
                          OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', 
                          OMNI_Postal_Code__c = '93425',
                          RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sold_to').getRecordTypeId());
        acc.BillingCity = 'Anytown';
        acc.State_Province_Is_Not_Applicable__c=true;
        acc.ERP_Site_Partner_Code__c = 'TestAprRel';
        acc.Has_Location__c = true;
        acc.Ext_Cust_Id__c = 'TestAprRel';
        insert acc;
        
        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
                          MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        con.ERP_Payer_Number__c = 'TestAprRel';
        insert con; 
        
        
        
		Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acc.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        bmQuote = TestUtils.getQuote();
        bmQuote.Name = 'TEST-QUOTE';
        bmQuote.BigMachines__Account__c = acc.Id; 
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.BigMachines__Status__c = 'Approved';
        bmQuote.Order_Type__c = 'Sales';
        bmQuote.Quote_Reference__c = 'TEST-QUOTE1';
        bmQuote.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote.BigMachines__Is_Primary__c = true;
        insert bmQuote;
        
        
        inc = new Invoice__c();
        inc.Name = 'TestAprRel';
        inc.Invoice_Date__c = Date.newInstance(2018, 1, 17);
        inc.Invoice_Due_Date__c = inc.Invoice_Date__c.addDays(1);
        inc.Sold_To__c = acc.Id;
        inc.Doc_Type__c = 'AB';
        inc.Cancelled__c  = false;
        inc.Amount__c  = 120;
        inc.ERP_Payer__c = 'Test001';
        inc.ERP_Site_Number__c = 'TestAprRel';
        inc.ERP_Sold__c = 'TestAprRel';
        inc.Quote__c = bmQuote.Id;
        inc.ERP_WO__c = 'WO00000000';
        inc.ERP_BMI_Quote__c =  bmQuote.Id;
        insert inc;
        
        Invoice__c inc1 = new Invoice__c();
        inc1.Name = 'TestAprRel';
        inc1.Invoice_Date__c = Date.newInstance(2018, 1, 17);
        inc1.Invoice_Due_Date__c = inc.Invoice_Date__c.addDays(1);
        inc1.Doc_Type__c = 'AB';
        inc1.Cancelled__c  = false;
        inc1.Amount__c  = 120;
        inc1.ERP_Payer__c = 'Test001';
        inc1.ERP_Site_Number__c = 'TestAprRel';
        inc1.ERP_Sold__c = 'TestAprRel';
        inc1.Quote__c = bmQuote.Id;
        inc1.ERP_Contract__c = 'Test';
        insert inc1;
    }
    
    @isTest static void test_method_one() {
        Test.startTest();
        update inc;
        InvoiceTriggerHandler obj = new InvoiceTriggerHandler();
        obj.dynamicShare('BigMachines__Quote__Share', bmQuote.Id, UserInfo.getUserId());
        Test.stopTest();
        
    }
    
}
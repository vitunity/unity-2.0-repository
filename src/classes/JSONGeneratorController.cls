public with sharing class JSONGeneratorController {
    
    @testVisible private static string jsonStr;
    @testVisible private static string bestSellerStr;
    @testVisible private static string monthlyEarning;
    @testVisible private static string monthlyVisitors;
    @testVisible private static string monthlyBuyers;
    @testVisible private static string selectedMonthlyVisitorsAndBuyers;
    @testVisible private static Map<Integer, String> monthsMap = new Map<Integer, String>{1=>'Jan',2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 
                                                                                        8=>'Aug',9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};
    @testVisible private static string OILSOQL = '  SELECT Id, vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate FROM vMarketOrderItemLine__c ' +
                                                 ' WHERE vMarket_App__c IN: ';

    public class DevApp {
        public String appName;
        public Date submissionDate;
        public String appStatus;
        public Date approvedDate;
        
        public DevApp(String appName, Date submissionDate, String appStatus, Date approvedDate) {
            this.appName = appName;
            this.submissiondate = submissionDate;
            this.appStatus = appStatus;
            this.approvedDate = approvedDate;
        }
    }

    public static String generateJSONContent(List<vMarket_App__c> myAppList) {
        JSONGenerator gen = JSON.createGenerator(true);
        
        DevApp appData;// = new DevApp();
        gen.writeStartArray();
        for(vMarket_App__c app : myAppList) {
            appData = new DevApp(app.Name, Date.valueOf(app.CreatedDate), app.AppStatus__c, app.Published_Date__c );
            // writing data to JSON string
            gen.writeStartObject();
            
            gen.writeStringField('name', app.Name);
            gen.writeDateField('submissionDate', Date.valueOf(app.CreatedDate) );
            gen.writeStringField('appStatus', app.AppStatus__c);
            gen.writeStringField('publishedDate', (app.Published_Date__c!=null?String.valueOf(app.Published_Date__c)    :''));
            
            gen.writeEndObject();
            
        }
        gen.writeEndArray();
        jsonStr = gen.getAsString();
        
        system.debug(' ==== ' + jsonStr);
        return jsonStr;
    }
    
    public class drawChartBestsellers {
        public string appName;
        public Integer visitors;
        public Integer buyers;
        
        public drawChartBestsellers(string appName, Integer visitors, Integer buyers) {
            this.appName = appName;
            this.visitors = visitors;
            this.buyers = buyers;
        }
    }   
    
    public static String generateJSONBestSeller(List<vMarket_Listing__c> listings) {
        JSONGenerator gen = JSON.createGenerator(true);
        
        drawChartBestsellers bestSeller;
        Integer appCount = listings.size();
        Integer appNum = 1;
        bestSellerStr = '';
        
        gen.writeStartArray();
        
        for(vMarket_Listing__c lists : listings) {
            
            bestSeller = new drawChartBestsellers(lists.App__r.Name, (Integer)lists.PageViews__c, (Integer)lists.InstallCount__c);
            // writing data to JSON string
            gen.writeStartObject();
            
            gen.writeStringField('App', lists.App__r.name);
            gen.writeStringField('pageView', String.valueOf(lists.PageViews__c));
            gen.writeStringField('installCount', String.valueOf(lists.InstallCount__c));
            
            gen.writeEndObject();
        }
        
        gen.writeEndArray();
        bestSellerStr = gen.getAsString();
        system.debug(' === listings === ' + listings);
        system.debug(' === My String === ' + bestSeller);
        return bestSellerStr;
    }
    
    public static String generateJSONTotalEarned(List<vMarketOrderItemLine__c> orderItemLine) {
      Map<String, String> formattedDateMap = getFormattedDateMap();
    List<String> monthList = getMonthNamesList();

        JSONGenerator gen = JSON.createGenerator(true);
        Integer currentYear = system.today().Year();
        Integer currentMonth = system.today().month();
        Date endDate = system.today()+1;
        Integer offset;
        Date startDate = null;
        if(currentMonth<6) {
            offset = 6-currentMonth;
            currentYear --;
            startDate = Date.newInstance(currentYear, (12-offset+1), 1);
        }
       else 
        {
            //offset = offset = 6-currentMonth;
            offset = currentMonth - 6;
            startDate = Date.newInstance(currentYear, (offset+1), 1);
        }
        
        System.debug('+++++'+currentYear+'--'+offset);
        
        system.debug(' == sD == ' + startDate + ' == eD == ' + endDate);
        
        Map<String, Double> monthlyEarnedJSON = new Map<String, Double>();
        for(vMarketOrderItemLine__c OIL : orderItemLine) {
            if(OIL.CreatedDate >= startDate && OIL.CreatedDate <= endDate) {
                String mon = monthsMap.get(OIL.CreatedDate.month());
                Double amt = OIL.Price__c;
                if(monthlyEarnedJSON.containsKey(mon)) {
                    amt += monthlyEarnedJSON.get(mon);
                    monthlyEarnedJSON.put(mon, amt);
                } else {
                    monthlyEarnedJSON.put(mon, amt);
                }
            }
        }
        
        for(Integer i = 1; i<=6; currentMonth--) {
            if(i == 7) {
                break;
            } else {
                i++;
            }
            if(currentMonth == 0)
                currentMonth = 12;
            if(!monthlyEarnedJSON.containsKey(monthsMap.get(currentMonth))) {
                monthlyEarnedJSON.put(monthsMap.get(currentMonth), 0.0);
            }
            
            system.debug(' ========= ' + monthlyEarnedJSON);
        }
        
        gen.writeStartArray();
        for(String str : monthList) {
          if(monthlyEarnedJSON.get(str) != null) {
              gen.writeStartObject();
              gen.writeStringField('Month', formattedDateMap.get(str));
              gen.writeNumberField('Earning', monthlyEarnedJSON.get(str));
              gen.writeEndObject();
          }
        }
        gen.writeEndArray();
        
        monthlyEarning = gen.getAsString();
        system.debug(' ========= ' + monthlyEarnedJSON + ' = ' + json.serialize(monthlyEarnedJSON) + ' = ' + monthlyEarning);
        
        return monthlyEarning;
    }

    // method will return result of total Visitors and total App ordered Monthly 
    public static string monthlyVistorJSONGenerator(Set<Id> appId) {
        // return if list is empty
        if(appId.isEmpty())
            return '';
        
        JSONGenerator gen = JSON.createGenerator(true);
        //List<vMarketListingActivity__c> appViews = new List<vMarketListingActivity__c>();
        //List<vMarketOrderItemLine__c> buyers = new List<vMarketOrderItemLine__c>();
        Map<Integer, integer> appViews = new Map<Integer, Integer>();
        Map<Integer, integer> buyers = new Map<Integer, Integer>();
        
        Integer currentYear = system.today().Year();
        Integer currentMonth = system.today().month();
        Date endDate = system.today()+1;
        Integer offset;
        /*if(currentMonth<6) {
            offset = 6-currentMonth;
            currentYear --;
        }
        Date startDate = Date.newInstance(currentYear, (12-offset+1), 1);*/
        Date startDate = null;
        if(currentMonth<6) {
            offset = 6-currentMonth;
            currentYear --;
            startDate = Date.newInstance(currentYear, (12-offset+1), 1);
        }
       else 
        {
            offset = offset = 6-currentMonth;
            startDate = Date.newInstance(currentYear, (offset+1), 1);
        }
        
        Integer i = 1;
        for(vMarketListingActivity__c act : [SELECT Id, CreatedDate, Name, Month__c FROM vMarketListingActivity__c 
                                             WHERE vMarket_Listing__r.App__c IN : appId AND CreatedDate >=: startDate AND CreatedDate <=: endDate LIMIT 50000]) {
            if(!appViews.containsKey((Integer)act.Month__c)) {
                appViews.put((Integer)act.Month__c, i);
            } else {
                i++;
                appViews.put((Integer)act.Month__c, i);
            }
        }
        
        i=1;
        for(vMarketOrderItemLine__c OIL : [ SELECT Id, CreatedDate, Name, Month__c FROM vMarketOrderItemLine__c 
                                            WHERE vMarket_App__c IN : appId AND CreatedDate >=: startDate AND CreatedDate <=: endDate AND Order_Status__c=:Label.vMarket_Success LIMIT 50000 ]) {
            if(!buyers.containsKey((Integer)OIL.Month__c)) {
                buyers.put((Integer)OIL.Month__c, 1);
            } else {
                //i++;
                buyers.put((Integer)OIL.Month__c, buyers.get((Integer)OIL.Month__c)+1);
            }                                   
        }
        
        gen.writeStartArray();
        for(Integer index : monthsMap.keySet()) {
            
            gen.writeStartObject();
            
            gen.writeStringField('Month', monthsMap.get(index));
            // counting monthly app pageviews
            if(appViews.containsKey(index))
                gen.writeNumberField('Visitors', appViews.get(index));
            
            // counting monthly app buys
            if(buyers.containsKey(index)){
                gen.writeNumberField('Buyers', buyers.get(index));
            }
            
            gen.writeEndObject();
        }
        gen.writeEndArray();
        monthlyVisitors = gen.getAsString();
        return monthlyVisitors;
    }
    
    // method for generating JSON for Apps
    public static string generateMyAppJSON(List<vMarket_App__c> appList) {
        if(appList.isEmpty())
            return '';
        String jsonData = '';
        JSONGenerator gen = JSON.createGenerator(true);
        Map<String, Integer> appStat = new Map<String, Integer>();
        for(vMarket_App__c app : appList) {
            Integer i = 0;
            if(appStat.containsKey(app.ApprovalStatus__c)) {
                i = appStat.get(app.ApprovalStatus__c);
                appStat.put(app.ApprovalStatus__c, i+1);
            } else {
                i = 1;
                appStat.put(app.ApprovalStatus__c, i);
            }
        }
        gen.writeStartArray();
        system.debug('=== REACHED TILL HERE ==== ' + appStat);
        for(String str : appStat.keySet()) {
            gen.writeStartObject();
            if(str == null)
                gen.writeStringField('appStatus', 'No Status');
            else
                gen.writeStringField('appStatus', str);
            gen.writeNumberField('appValue', appStat.get(str));
        
            gen.writeEndObject();
        }
        gen.writeEndArray();
        jsonData = gen.getAsString();
        system.debug(' ===== jsonData ====== ' + jsonData);
        return jsonData;
    }

  /* Nilesh: below are new methods for getting json generator */
  // Get all last 12 month in sequence
  public static Map<String, String> getFormattedDateMap() {
    Datetime curDate = system.today()+1;
    Map<String, String> formattedDateMap = new Map<String, String>();
    for(Integer i=0; i<12; i++) {
      Datetime newDate = curDate.addMonths(-i);
        formattedDateMap.put(newDate.format('MMM'), newDate.format('MMM, yy'));
    }
    return formattedDateMap;
  }
  
  // Get month number list for sorting
  public static List<Integer> getMonthsList() {
    Datetime curDate = system.today();
    List<Integer> monthList = new List<Integer>();
    for(Integer i=11; i>=0; i--) {
      Datetime newDate = curDate.addMonths(-i);
      monthList.add(newDate.month());
    }
    return monthList;
  }

  // Get month name list for sorting
  public static List<String> getMonthNamesList() {
    Datetime curDate = system.now();
    List<String> monthList = new List<String>();
    String currentMonth;
    for(Integer i=11; i>=0; i--) {
      Datetime newDate = curDate.addMonths(-i);
      monthList.add(newDate.format('MMM'));
    }
  
    return monthList;
  }
  
  // Get selected month number list for sorting
  public static List<Integer> getLastSelectedMonthList(Integer selected_month) {
    Datetime curDate = system.today()+1;
    List<Integer> monthList = new List<Integer>();
    for(Integer i=(selected_month-1); i>=0; i--) {
      Datetime newDate = curDate.addMonths(-i);
      System.debug('**month'+newDate.month());
      System.debug('**date'+newDate);
      monthList.add(newDate.month());
    }
    return monthList;
  }

  public static string selectedMonthlyVisitorJSONGenerator(Set<Id> appId, Integer selected_month) {
        // return if list is empty
        if(appId.isEmpty())
            return '';
            
        JSONGenerator gen = JSON.createGenerator(true);
        Map<Integer, integer> appViews = new Map<Integer, Integer>();
    Datetime curDate = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
        DateTime lastDate = curDate.addMonths(-selected_month);
        
        Integer i = 1;
        for(vMarketListingActivity__c act : [SELECT Id, CreatedDate, Name, Month__c FROM vMarketListingActivity__c 
                                             WHERE vMarket_Listing__r.App__r.ownerId =: UserInfo.getUserId() AND CreatedDate >=: lastDate AND CreatedDate <=: curDate ORDER BY Month__c ]) {
            if(!appViews.containsKey((Integer)act.Month__c)) {
              i = 1;
                appViews.put((Integer)act.Month__c, i);
            } else {
              i++;
                appViews.put((Integer)act.Month__c, i);
            }
        }

    Map<String, String> formattedDateMap = getFormattedDateMap();
    List<Integer> monthList = getLastSelectedMonthList(selected_month);
        gen.writeStartArray();
        for(Integer index: monthList) {
      if(appViews.containsKey(index)) {
        gen.writeStartObject();
              gen.writeStringField('Month', formattedDateMap.get(monthsMap.get(index)));
              // counting monthly app pageviews
                gen.writeNumberField('Visitors', appViews.get(index));
                gen.writeEndObject();
      } else {
        gen.writeStartObject();
              gen.writeStringField('Month', formattedDateMap.get(monthsMap.get(index)));
              // setting monthly app pageviews
                gen.writeNumberField('Visitors', 0);
                gen.writeEndObject();
      }
        }
        gen.writeEndArray();
        monthlyVisitors = gen.getAsString();
        return monthlyVisitors;
  }

  public static string selectedMonthlyBuyerJSONGenerator(Set<Id> appId, Integer selected_month) {
        // return if list is empty
        if(appId.isEmpty())
            return '';
            
        JSONGenerator gen = JSON.createGenerator(true);
        Map<Integer, integer> buyers = new Map<Integer, Integer>();
    Datetime curDate = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
        DateTime lastDate = curDate.addMonths(-selected_month);

        Integer i=1;
        for(vMarketOrderItemLine__c OIL : [ SELECT Id, CreatedDate, Name, Month__c FROM vMarketOrderItemLine__c 
                                            WHERE vMarket_App__c IN : appId AND CreatedDate >=: lastDate AND CreatedDate <=: curDate AND Order_Status__c=:Label.vMarket_Success LIMIT 50000 ]) {
            if(!buyers.containsKey((Integer)OIL.Month__c)) {
                buyers.put((Integer)OIL.Month__c, 1);
            } else {
                i++;
                buyers.put((Integer)OIL.Month__c, buyers.get((Integer)OIL.Month__c)+1);
            }                                   
        }

        gen.writeStartArray();
        for(Integer index : monthsMap.keySet()) {
            gen.writeStartObject();

      if(buyers.containsKey(index)) {
              gen.writeStringField('Month', monthsMap.get(index));
              // counting monthly app buys
                gen.writeNumberField('Buyers', buyers.get(index));
      }

            gen.writeEndObject();
        }
        gen.writeEndArray();
        monthlyBuyers = gen.getAsString();
        return monthlyBuyers;
  }


    // method will return result of total Visitors and total App ordered Monthly 
    public static string selectedMonthlyVistorAndBuyerJSONGen(Set<Id> appId, Integer selected_month) {
        // return if list is empty
        if(appId.isEmpty())
            return '';

        JSONGenerator gen = JSON.createGenerator(true);
        Map<Integer, integer> appViews = new Map<Integer, Integer>();
        Map<Integer, integer> buyers = new Map<Integer, Integer>();

    Datetime curDate = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
        DateTime lastDate = curDate.addMonths(-selected_month);

        Integer i = 1;
        for(vMarketListingActivity__c act : [SELECT Id, CreatedDate, Name, Month__c FROM vMarketListingActivity__c 
                                             WHERE vMarket_Listing__r.App__r.ownerId =: UserInfo.getUserId() AND CreatedDate >=: lastDate AND CreatedDate <=: curDate ORDER BY Month__c LIMIT 50000]) {
            if(!appViews.containsKey((Integer)act.Month__c)) {
              i = 1;
                appViews.put((Integer)act.Month__c, i);
            } else {
                i++;
                appViews.put((Integer)act.Month__c, i);
            }
        }
        
        i=1;
        Set<String> uniqueBuyerSet = new Set<String>();
        for(vMarketOrderItemLine__c OIL : [ SELECT Id, CreatedById, CreatedDate, Name, Month__c FROM vMarketOrderItemLine__c 
                                            WHERE vMarket_App__c IN : appId AND CreatedDate >=: lastDate AND CreatedDate <=: curDate  AND Order_Status__c=:Label.vMarket_Success ORDER BY Month__c LIMIT 50000 ]) {
            if(!buyers.containsKey((Integer)OIL.Month__c)) {
              uniqueBuyerSet.clear();
              uniqueBuyerSet.add(OIL.CreatedById); // contains distict BuyerId
              i = 1;
                buyers.put((Integer)OIL.Month__c, 1);
            } else {
              buyers.put((Integer)OIL.Month__c, buyers.get((Integer)OIL.Month__c)+1);
              if (!uniqueBuyerSet.contains(OIL.CreatedById)) {
                  //i++;
                  //buyers.put((Integer)OIL.Month__c, buyers.get((Integer)OIL.Month__c)+1);
                  uniqueBuyerSet.add(OIL.CreatedById); // contains distict BuyerId
              }
            }                                   
        }

    Map<String, String> formattedDateMap = getFormattedDateMap();
    List<Integer> monthList = getLastSelectedMonthList(selected_month);
    System.debug('**month1'+selected_month);
    System.debug('**month2'+monthList );
    System.debug('**month3'+formattedDateMap);
        gen.writeStartArray();
        for(Integer index: monthList) {         
            if(appViews.containsKey(index) || buyers.containsKey(index)) {
              gen.writeStartObject();
              gen.writeStringField('Month', formattedDateMap.get(monthsMap.get(index)));
              // counting monthly app pageviews
              if(appViews.containsKey(index))
                  gen.writeNumberField('Visitors', appViews.get(index));
                else
          gen.writeNumberField('Visitors', 0);
            
              // counting monthly app buys
              if(buyers.containsKey(index))
                  gen.writeNumberField('Buyers', buyers.get(index));
                else
          gen.writeNumberField('Buyers', 0);
          
        gen.writeEndObject();
            } else {
              gen.writeStartObject();
        gen.writeStringField('Month', formattedDateMap.get(monthsMap.get(index)));
              gen.writeNumberField('Visitors', 0);
              gen.writeNumberField('Buyers', 0);
              gen.writeEndObject();
            }
        }
        gen.writeEndArray();
        selectedMonthlyVisitorsAndBuyers = gen.getAsString();
        return selectedMonthlyVisitorsAndBuyers;
    }
}
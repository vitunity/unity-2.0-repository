@isTest(SeeAllData=true)
public class KPIEmailAlert_Test
{

    static testMethod void KPIEmailAlert_Test() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        system.runAs(u)
        {
            
            CollaborationGroup  myGroup = [select Id from CollaborationGroup  where Name = 'K-Chat' limit 1];
            
            CollaborationGroupMember groupMember = new CollaborationGroupMember();
            groupMember.memberid = u.Id;
            groupMember.CollaborationGroupId = myGroup.Id;
            insert groupMember;
            
            String CRON_EXP = '0 0 0 15 3 ? 2022';

            Test.StartTest();                
                String jobId = System.schedule('ScheduleApexClassTest'+system.now(), CRON_EXP, new ScheduleKPIEmailAlert());
                // Get the information from the CronTrigger API object
                CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
                // Verify the expressions are the same
                System.assertEquals(CRON_EXP, ct.CronExpression);
            Test.StopTest();
        }
    }
}
/***************************************************************************
Author: Amitkumar Katre
Created Date: 24-June-2017
Description: Handler class for L2848 after triggers

Change Log:
6-Oct-2017 - Divya Hargunani - STSK0013046\STSK0013564 - RAQA:  More L2848 Object enhancements: Added a method to Send email to regulatory
                                                            when ECF status is set to Additional Information Provided
4-Jan-2018 - Divya Hargunani - STSK0013618 - STRY0046316 : RAQA - Auto populate recommendation of field from ECF
8-Jan-2018 - Divya Hargunani - STSK0013563 - STRY0031505 : RAQA: Make "ETQ #" field read only and populate from ECF form
Jun-04-2018 - RB - STRY0040164 - Update ETQ # on Case based on ETQ Determination # on ECF.
*************************************************************************************/

public class L2848TriggerHandler {
    
    public static map<Id,Case> caseMapforUpdate = new map<Id,Case> ();
    
    //STRY0024481: Calculate L2848 Count Logic Update 
    public static void updateL2848CountWo(list<L2848__c> listOfL2848){
        list<RollUpSummaryUtility.fieldDefinition> fieldDefinitions1 = new list<RollUpSummaryUtility.fieldDefinition> {
                new RollUpSummaryUtility.fieldDefinition('Count', 'Id', 
                'L2848_Count__c')};
        RollUpSummaryUtility.ObjectDefination objDef = 
                    new RollUpSummaryUtility.ObjectDefination('L2848__c','Work_Order__c',
                    'SVMXC__Service_Order__c',
                    'and Is_Submitted__c != \'Cancelled\'',null);
        new RollUpSummaryUtility().rollUp(fieldDefinitions1, objDef , listOfL2848); 
    }
    
    //STSK0013046 - Divya Hargunani - RAQA:  More L2848 Object enhancements
    public static void AdditionalInfoReqEmail(list<L2848__c> listOfL2848, map<Id,L2848__c> oldMap){
        set<Id> l2848Ids = new set<Id> ();
        for(L2848__c ecObj : listOfL2848){
            if(ecObj.Is_Submitted__c == 'Additional Information Provided' &&
               (oldMap == null || oldMap.get(ecObj.Id).Is_Submitted__c != ecObj.Is_Submitted__c)){
                l2848Ids.add(ecObj.Id);
            }
        }
        
        if(!l2848Ids.isEmpty()){ 
            try{
                list<Messaging.SingleEmailMessage> emailMsgList = new list<Messaging.SingleEmailMessage>();
                //query email template based on developer name
                EmailTemplate emailTemplate = [select id from EmailTemplate where DeveloperName = 'Complaint_Information_Request_Complete'];
                //This contact should in system for email purpose.
                Contact regulatoryContact = [select Id from Contact Where LastName = 'Team' and Firstname = 'Regulatory'];
                list<L2848__c> l2848ListforEmail = [select id,Varian_Employee__c, Varian_Employee__r.Email from L2848__c where Id in : l2848Ids];
                for(L2848__c ecObj : l2848ListforEmail){
                     //New instance of a single email message
                   Messaging.SingleEmailMessage mail = 
                            new Messaging.SingleEmailMessage();
                   mail.setWhatId(ecObj.Id);
                   mail.setTargetObjectId(regulatoryContact.id);
                   mail.setTemplateId(emailTemplate.Id);
                   mail.setccAddresses(new list<String>{ecObj.Varian_Employee__r.Email});    
                   mail.setBccSender(false);
                   mail.setUseSignature(false);
                   mail.setSaveAsActivity(true);  
                   emailMsgList.add(mail);
                }
                Messaging.sendEmail(emailMsgList);
            }catch(Exception e){
                system.debug('There is problem while sending email'+e);
            }
        }
    }
    //STSK0013618 - Divya Hargunani - STRY0046316 : RAQA - Auto populate recommendation of field from ECF
    public static void updateInvestigationNotesOnCase(list<L2848__c> listOfL2848, map<Id,L2848__c> oldMap){
        for(L2848__c ecObj : listOfL2848){
            String investigationNotes;
            if(ecObj.Is_Submitted__c == 'Rejected by Regulatory' 
                && ecObj.Rejection_Reason__c != null &&
                (oldMap == null || oldMap.get(ecObj.Id).Rejection_Reason__c != ecObj.Rejection_Reason__c)){
                investigationNotes = ecObj.Rejection_Reason__c;
            }
            if(ecObj.Is_Submitted__c == 'Accepted & Closed by Regulatory' 
                && ecObj.Investigation_Conclusion__c != null &&
                (oldMap == null 
                 || oldMap.get(ecObj.Id).Investigation_Conclusion__c != ecObj.Investigation_Conclusion__c)
              ){
               investigationNotes = ecObj.Investigation_Conclusion__c; 
            }
            if(investigationNotes != null){
                Case caseObj;
                if(caseMapforUpdate.containsKey(ecObj.Case__c)){
                    caseObj = caseMapforUpdate.get(ecObj.Case__c);
                }else{
                    caseObj = new Case (Id = ecObj.Case__c);
                    caseMapforUpdate.put(ecObj.Case__c, caseObj);
                }
                caseObj.ECF_Investigation_Notes__c = investigationNotes.replace('\n','<br/>') ;
            }
      }
    }
    
    //STSK0013563 - Divya Hargunani - STRY0031505 : RAQA: Make "ETQ #" field read only and populate from ECF form
    public static void updateETQnumberToCase(list<L2848__c> listOfL2848, map<Id,L2848__c> oldMap){
        list<Case> caseList = new list<Case>();
        for(L2848__c ecObj : listOfL2848){
          if(ecObj.ETQ_Determination_Number__c!= null && ecObj.Case__c != null && ecObj.ETQ_Complaint__c == null &&
               (oldMap == null || oldMap.get(ecObj.Id).ETQ_Determination_Number__c != ecObj.ETQ_Determination_Number__c)){
                   Case caseObj;
                   if(caseMapforUpdate.containsKey(ecObj.Case__c)){
                       caseObj = caseMapforUpdate.get(ecObj.Case__c);
                   }else{
                       caseObj = new Case (Id = ecObj.Case__c);
                       caseMapforUpdate.put(ecObj.Case__c, caseObj);
                   }
                   caseObj.ETQ_Complaint__c = ecObj.ETQ_Determination_Number__c;
            }
            
            if(ecObj.ETQ_Complaint__c!= null && ecObj.Case__c != null && 
               (oldMap == null || oldMap.get(ecObj.Id).ETQ_Complaint__c != ecObj.ETQ_Complaint__c)){
                   Case caseObj;
                   if(caseMapforUpdate.containsKey(ecObj.Case__c)){
                       caseObj = caseMapforUpdate.get(ecObj.Case__c);
                   }else{
                       caseObj = new Case (Id = ecObj.Case__c);
                       caseMapforUpdate.put(ecObj.Case__c, caseObj);
                   }
                   caseObj.ETQ_Complaint__c = ecObj.ETQ_Complaint__c;
            }
            
      }
    }
    
    // STSK0013618, STSK0013563 - Divya Hargunani - Added this to update the case only once
    // STSK0013618 : To append the new text with existing in ECF Investigation Notes field
    public static void updateCase(){
        if(!caseMapforUpdate.isEmpty()){
            for(Case caseObj : [select ECF_Investigation_Notes__c from case where id in : caseMapforUpdate.keyset()]){
                Case forUpdate = caseMapforUpdate.get(caseObj.Id);
                if(!String.isBlank(forUpdate.ECF_Investigation_Notes__c) && !String.isBlank(caseObj.ECF_Investigation_Notes__c)){
                    String localText = caseObj.ECF_Investigation_Notes__c;
                    String newText = forUpdate.ECF_Investigation_Notes__c;
                    forUpdate.ECF_Investigation_Notes__c = localText +'<br/>' + newText.replace('\n','<br/>') ;
                }
            }
            update caseMapforUpdate.values();
            caseMapforUpdate.clear();
        }   
    }
    
}
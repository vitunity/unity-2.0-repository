/***************************************************************************
Change Log:
17-Jan-2018 - Divya Hargunani - STSK0013046\STSK0013564\STSK0013618\STSK0013563 - Increased test coverage
*************************************************************************************/
@isTest(seeAllData = false)
public class L2848TriggerHandlerTest {
    public Static Profile pr = [Select id from Profile where name = 'VMS BST - Member'];
        
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
    public static Id fieldServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();//record type id of Field Service WO
    public static Id instlRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); //record type id of Installation WO
    public static Id UsageConRecTypID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();                                
    static SVMXC__Service_Order_Line__c varWD = new SVMXC__Service_Order_Line__c();
    static SVMXC__Service_Order_Line__c varWD1 = new SVMXC__Service_Order_Line__c();
    static SVMXC__RMA_Shipment_Order__c testPartsOrder = new SVMXC__RMA_Shipment_Order__c();
    static SVMXC__Service_Order__c objWO1 = new SVMXC__Service_Order__c();
    static SVMXC__RMA_Shipment_Line__c varPOL = new SVMXC__RMA_Shipment_Line__c();
    static SVMXC__Service_Order__c objWO2 = new SVMXC__Service_Order__c();
    static SVMXC__Service_Order__c objWO3 = new SVMXC__Service_Order__c();
    static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c objTopLevelWithoutSite = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    static Case objCase = SR_testdata.createCase();
    static Case objCase1 = SR_testdata.createCase();
    static Contact objCont = SR_testdata.createContact();
    static Account objAcc = SR_testdata.creteAccount();
    static Product2 objProd = SR_testdata.createProduct();
    static SVMXC__Service_Contract__c objSMC = new SVMXC__Service_Contract__c();
    static SVMXC__Site__c objLoc = new SVMXC__Site__c();
    static SVMXC__Service_Level__c objSLATerm = new SVMXC__Service_Level__c();
    static SVMXC__Service_Group__c objServTeam = new SVMXC__Service_Group__c();
    static SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c();
    static SVMXC__Service_Group_Members__c objTechEquip1 = new SVMXC__Service_Group_Members__c();
    static Depot_Sourcing_Rules__c objDSR = new Depot_Sourcing_Rules__c();
    static Country__c objCounty = new Country__c();
    static ERP_Org__c objOrg = new ERP_Org__c();
    static DateTime d = system.Now();
    // public static List<User> Usr = [SELECT Phone, Id, email FROM User WHERE Id = : UserInfo.getUserId() limit 1];
    public static List<BusinessHours> listBusinessHrs = [Select ID from BusinessHours limit 1];
    
    static
    {       
        objCounty.Name = 'United States';
        objCounty.ISO_Code_2__c = 'AUS';
        objCounty.SAP_Code__c = 'AUS';
        insert objCounty;
        
        ERP_Timezone__c erpTZ = new ERP_Timezone__c();
        erpTZ.Name = 'AUSSA';
        erpTZ.Salesforce_timezone__c = 'Central Summer Time (South Australia)';
        insert erpTZ;
        
        objAcc.AccountNumber = '123456';
        objAcc.Country1__c = objCounty.ID;
        objAcc.country__c = 'India';
        objAcc.ERP_Timezone__c = 'AUSSA';
        objAcc.Timezone__c = erpTZ.Id;
        objAcc.BillingCity = 'Pune';
        objAcc.ShippingCity = 'Pune';
        objAcc.State_Province_Is_Not_Applicable__c = true;
        insert objAcc;
        
        Contact con = new Contact();
        con.FirstName = 'Regulatory';
        con.LastName = 'Team';
        con.Functional_Role__c = 'Unknown';
        con.Email = 'tetststs@gssd.com';
        con.AccountId = objAcc.id;
        insert con;
        
        objOrg.Name = 'Test Org';
        objOrg.Sales_Org__c = 'Test Sales Org';
        objOrg.Approval_time_limit__c = 2;
        objOrg.Part_Order_Value_Limit__c = 12;
        objOrg.Received_Date_Required__c = true;
        objOrg.Bonded_Warehouse_used__c = true;
        objOrg.Parts_Order_Approval_Level__c = 12.8;
        insert objOrg;
        
        objLoc.Name = 'Test Location';
        objLoc.SVMXC__Street__c = 'Test Street';
        objLoc.SVMXC__Country__c = 'United States';
        objLoc.SVMXC__Zip__c = '98765';
        objLoc.ERP_Functional_Location__c = 'Test Location';
        objLoc.Plant__c = 'Test Plant';
        objLoc.ERP_Site_Partner_Code__c = '123456';
        objLoc.SVMXC__Location_Type__c = 'Depot';
        objLoc.Sales_Org__c = objOrg.Sales_Org__c;
        objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
        objLoc.ERP_Org__c = objOrg.id;
        objLoc.SVMXC__Stocking_Location__c = true;
        objLoc.Technician__c = objTechEquip.Id;
        objLoc.Country__c = objCounty.ID;
        objLoc.RecordTypeId = Schema.Sobjecttype.SVMXC__Site__c.getRecordTypeInfosByName().get('Standard Location').getRecordTypeId();
        insert objLoc;
  
        objProd.Name = 'Test Product';
        objProd.SVMXC__Product_Line__c = 'Accessory';
        objProd.Skills_Required__c = 'BEAM' ;
        objProd.Returnable__c = 'Scrap Locally';
        insert objProd;

        objSMC.Name = 'Test Contract';
        objSMC.SVMXC__Company__c = objAcc.ID;
        objSMC.SVMXC__Start_Date__c = system.today();
        objSMC.SVMXC__End_Date__c = system.today() + 30;
        insert objSMC;
        
        objSLATerm.Name = 'Test SLA Term';
        objSLATerm.SVMXC__Restoration_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Onsite_Response_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Resolution_Tracked_On__c = 'WorkOrder';
        insert objSLATerm;
        
        objServTeam.Name = 'ABC';
        objServTeam.SVMXC__Active__c = true;
        objServTeam.Dispatch_Queue__c = 'DISP CH';
        objServTeam.District_Manager__c = userInfo.getUserID();
        objServTeam.SVMXC__Group_Code__c = 'ABC';
        insert objServTeam;
        
        objTechEquip.Name = 'Test Technician';
        objTechEquip.SVMXC__Active__c = true;
        objTechEquip.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        objTechEquip.SVMXC__Enable_Scheduling__c = true;
        objTechEquip.SVMXC__Phone__c = '987654321';
        objTechEquip.SVMXC__Email__c = 'abc@xyz.com';
        objTechEquip.SVMXC__Service_Group__c = objServTeam.ID;
        objTechEquip.SVMXC__Street__c = 'Test Street';
        objTechEquip.SVMXC__Zip__c = '98765';
        objTechEquip.SVMXC__Country__c = 'United States';
        objTechEquip.OOC__c = false;
        objTechEquip.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
        objTechEquip.User__c = userInfo.getUserID();
        objTechEquip.Dispatch_Queue__c = 'DISP CH';
        insert objTechEquip;
        
        objTopLevel.Name = 'H23456';
        objTopLevel.SVMXC__Serial_Lot_Number__c = 'H23456';
        objTopLevel.SVMXC__Product__c = objProd.ID;
        objTopLevel.SVMXC__Site__c = objLoc.ID;
        objTopLevel.ERP_Reference__c = 'H23456';
        objTopLevel.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objTopLevel.ERP_CSS_District__c = 'ABC';
        objTopLevel.ERP_Sales__c = 'Test ERP Sales';
        insert objTopLevel;
        
        objComponent.Name = 'H12345';
        objComponent.SVMXC__Serial_Lot_Number__c = 'H12345';
        objComponent.SVMXC__Parent__c = objTopLevel.ID;
        objComponent.SVMXC__Product__c = objProd.ID;
        objComponent.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objComponent.ERP_Sales__c = 'Test ERP Sales';
        objComponent.ERP_Reference__c = 'H12345';
        objComponent.SVMXC__Top_Level__c = objTopLevel.id;
        objComponent.ERP_CSS_District__c = 'ABC';
        objComponent.SVMXC__Site__c = objLoc.id;
        objComponent.SVMXC__Status__c = 'Shipped';
        objComponent.SVMXC__Company__c = objAcc.id;
        insert objComponent;
        
        objCase1.Reason = 'System Down';
        objCase1.Priority = 'Medium';
        objCase1.Status = 'New';
        objCase1.SVMXC__Top_Level__c = objComponent.id;
        objCase1.Type ='Problem';
        objCase1.Reason = 'System Down';
        objCase1.Description = 'asdfghjkzxcvbnm';
        objCase1.ECF_Investigation_Notes__c = 'Test Recommendation';
        insert objCase1;
  
    }
        
    public static testMethod void l2484HandlerTest()
    {         
        Test.startTest();

        L2848__c objL2848 = new L2848__c();
        objL2848.Case__c = objCase1.id;
        objL2848.City__c = 'City';
        objL2848.State_Province__c = 'Test';
        objL2848.Country__c = 'USA';
        objL2848.Name_of_Customer_Contact__c = 'Test';
        objL2848.Title_Role__c = 'test';
        objL2848.Phone_Number__c = '121313131313';
        objL2848.Employee_Phone__c = '11111111111';
        objL2848.ETQ_Determination_Number__c = '679696069';
        objL2848.Device_or_Application_Name__c = 'test';
        objL2848.Device_or_Application_Name2__c = 'Test';
        objL2848.Version_Model2__c = 'ress';
        objL2848.Version_Model2__c = 'qsqwqs';
        objL2848.Is_Submitted__c = 'Not yet';
        objL2848.Was_anyone_injured__c = 'No';
        Insert objL2848;
        
        objL2848.Is_Submitted__c = 'Additional Information Provided';
        objL2848.Additional_information_Request__c = 'Test Additional Information Request';
        update objL2848;

        test.stopTest();
    }
    
     public static testMethod void l2484HandlerTest2()
    {         
        Test.startTest();

        L2848__c objL2848 = new L2848__c();
        objL2848.Case__c = objCase1.id;
        objL2848.City__c = 'City';
        objL2848.State_Province__c = 'Test';
        objL2848.Country__c = 'USA';
        objL2848.Name_of_Customer_Contact__c = 'Test';
        objL2848.Title_Role__c = 'test';
        objL2848.Phone_Number__c = '121313131313';
        objL2848.Employee_Phone__c = '11111111111';
        objL2848.Device_or_Application_Name__c = 'test';
        objL2848.Device_or_Application_Name2__c = 'Test';
        objL2848.Version_Model2__c = 'ress';
        objL2848.Version_Model2__c = 'qsqwqs';
        objL2848.ETQ_Determination_Number__c = '6796960789869';
        objL2848.Is_Submitted__c = 'Not yet';
        objL2848.Was_anyone_injured__c = 'No';
        Insert objL2848;
        
        objL2848.Is_Submitted__c = 'Rejected by Regulatory';
        objL2848.Rejection_Reason__c = 'Test Rejection Reason';
        objL2848.ETQ_Complaint__c = '123456';
        update objL2848;
      
        test.stopTest();
    }
    
    public static testMethod void l2484HandlerTest3(){         
        Test.startTest();

        L2848__c objL2848 = new L2848__c();
        objL2848.Case__c = objCase1.id;
        objL2848.City__c = 'City';
        objL2848.State_Province__c = 'Test';
        objL2848.Country__c = 'USA';
        objL2848.Name_of_Customer_Contact__c = 'Test';
        objL2848.Title_Role__c = 'test';
        objL2848.Phone_Number__c = '121313131313';
        objL2848.Employee_Phone__c = '11111111111';
        objL2848.Device_or_Application_Name__c = 'test';
        objL2848.Device_or_Application_Name2__c = 'Test';
        objL2848.Version_Model2__c = 'ress';
        objL2848.Version_Model2__c = 'qsqwqs';
        objL2848.Is_Submitted__c = 'Not yet';
        objL2848.ETQ_Determination_Number__c = '62326069';
        objL2848.Was_anyone_injured__c = 'No';
        Insert objL2848;
        
        objL2848.Is_Submitted__c = 'Accepted & Closed by Regulatory';
        objL2848.Investigation_Conclusion__c = 'Test Conclusion';
        objL2848.ETQ_Complaint__c = '123456';
        update objL2848;
      
        test.stopTest();
    }
}
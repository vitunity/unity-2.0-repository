/***************************************************************************\
    @ Author                : Kaushiki Verma
    @ Date                  : 17/11/2014
****************************************************************************/
@isTest
public class L2848_BeforeTrigger_test_DAteofEvent
{
    Public Static testMethod void L2848_BeforeTrigger_test()
    {
    
    Profile sysadminprofile = new profile();
    
    sysadminprofile = [Select id from profile where name = 'System Administrator'];
    
    User systemuser = [Select id from user where profileId= :sysadminprofile.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 

    
    RecordType rec1 = [Select id,Name,Description from Recordtype where developername = 'Helpdesk' and SObjectType=:'Case'];
    
    Account Acc = new Account();
    //Acc.ShippingCity = 'abc';
    //Acc.ShippingCountry = 'India';
    //Acc.ShippingState = 'UP';
    Acc.BillingCity = 'abc';
    Acc.BillingCountry = 'India';
    Acc.BillingState = 'UP';
    Acc.Name = 'TestAccount';
    Acc.Country__c = 'India';
    insert Acc; 
    
    // insert Contact 
    Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
    insert con;
    
    
    Case testcase1 = SR_testdata.createcase();
    testcase1.Description = 'anc';
    testcase1.Contactid=con.id;
    testcase1.recordtypeid= rec1.id;
    testcase1.AccountId =  Acc.id;
    testcase1.Contact_Phone__c = '1234';
    testcase1.Was_anyone_injured__c = 'No';
    testcase1.Description = 'anc';
    testcase1.Case_Activity__c = 'anc';
    testcase1.Version_Model2__c = 'anc';
    testcase1.Original_Owner__c = 'anc';
    testcase1.PCSN2__c = 'anc';
    testcase1.Service_Pack__c  = 'anc';
    testcase1.Service_Pack2__c = 'anc';
    Insert testcase1 ;
    
    
    
    L2848__c objL2848 = new L2848__c();
    objL2848.Case__c = testcase1.id; 
    

    objL2848.City__c = 'abc';
    objL2848.Country__c = 'India';
    objL2848.State_Province__c = 'UP';              
    objL2848.Device_or_Application_Name__c = testcase1.SVMXC__Top_Level__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
    objL2848.Device_or_Application_Name2__c = testcase1.Device_or_Application_Name2__c ;
    objL2848.District_or_Area__c = testcase1.Account.CSS_District__c; 
    objL2848.E_mail_Address__c =testcase1.Contact.Email;          
    objL2848.Employee_Phone__c = testcase1.CreatedBy.Phone;
    objL2848.Name_of_Customer_Contact__c = testcase1.Contact.Name;        
    objL2848.PCSN__c = testcase1.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c;
    objL2848.PCSN2__c = testcase1.PCSN2__c ;
    objL2848.Phone_Number__c = testcase1.Contact_Phone__c;
    objL2848.Service_Pack__c = testcase1.Service_Pack__c;
    objL2848.Service_Pack2__c = testcase1.Service_Pack2__c;
    objL2848.Service_Request_Notification__c = testcase1.CaseNumber ;
    objL2848.Site_Name__c = testcase1.Account.Name;
    objL2848.Title_Department__c = testcase1.owner.Title;
    objL2848.Title_Role__c =testcase1.Contact.Title;
    objL2848.Varian_District_Manager_for_Site__c = testcase1.Account.Territory_Team__r.CSS_District_Manager__r.Name;//varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name
    objL2848.Varian_Employee_Receiving_Info__c = testcase1.Original_Owner__c;
    /* objL2848.Version_Model__c = testcase1.SVMXC__Top_Level__r.Product_Version_Build__r.Name; Wave1*/
    objL2848.Version_Model2__c = testcase1.Version_Model2__c;
    objL2848.When_Where_Who_What__c = testcase1.Case_Activity__c;
    objL2848.Complaint_Short_Description__c = testcase1.Description;
    objL2848.Date_of_Event__c = System.Today();
    //objL2848.Date_Reported_to_Varian__c = System.Today();
    objL2848.Was_anyone_injured__c = testcase1.Was_anyone_injured__c;
    
    Insert objL2848;

    }
}
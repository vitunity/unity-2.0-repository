public class LMSAutodeactivationHelper {

public static integer calMonthDiff(datetime startdatetime,datetime enddatetime){
  
    integer monthDiff;
     integer yearDiff = enddatetime.year()-startdatetime.year();
        if(yearDiff == 0){
            system.debug('Entered first >>>>>  1 logic '+yearDiff);
            monthDiff = enddatetime.month()- startdatetime.month();      
            if(startdatetime.day() > enddatetime.day()){
                 monthDiff --;
            }
            system.debug('months difference'+monthDiff);
        }else if(yearDiff == 1){
            system.debug('Entered seond >>>>>  2 logic '+yearDiff);
            if(startdatetime.month() > enddatetime.month()){
                yearDiff --;
                
            }
            integer months2 = enddatetime.month();  
            integer months1 = 12-startdatetime.month();
            monthDiff = months2+months1;
            if(startdatetime.day() > enddatetime.day()){
               monthDiff --;
            }
            system.debug('months difference'+monthDiff);
        }else if(yearDiff > 1){
            
            integer monthstoAdd = 0;
            system.debug('Entered third>>>>>  3 logic'+yearDiff);
            if(startdatetime.month() > enddatetime.month()){
                yearDiff -- ;
                monthstoAdd = enddatetime.month() + (12-startdatetime.month());
                
            }
            monthDiff = 12 * yearDiff;
            monthDiff = monthDiff + monthstoAdd;
            
            if(startdatetime.day() > enddatetime.day()){
                  monthDiff --;
             }
             system.debug('months difference'+monthDiff);
        }
        
        return monthDiff;
    }

}
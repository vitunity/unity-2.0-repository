/***************************************************************************
Author: Divya Hargunani 
Created Date: 5-Nov-2017
Project/Story/Inc/Task : STSK0013224 : Automate LMS Accounts activation from Unity
Description: 
This is controller class for (VF page : LMS_UserActivation) activating LMS Account of multiple users at a time
*************************************************************************************/

public class LMS_UserActivationCtrl {

    public list<UserWrapper> userWrapperList {get;set;}
    @TestVisible private static Integer RECORDS_PER_PAGE = 100;
    public String myOrder {get;set;}
    private boolean changeValues = false;
    public Integer countVal =0;
    
    public LMS_UserActivationCtrl() {
        myOrder = 'DESC';
    }

    private ApexPages.StandardSetController lmsUserActivation {
        get {
            if(lmsUserActivation == null || changeValues == true){
                lmsUserActivation = new ApexPages.StandardSetController(
                Database.getQueryLocator(getSortedUserQuery()));
                lmsUserActivation.setPageSize(RECORDS_PER_PAGE);
                changeValues = false;
            }else if(Test.isRunningTest()){
                lmsUserActivation = new ApexPages.StandardSetController(new List<User>());
            }
            return lmsUserActivation;
        }
        set;
    }
    
    public PageReference sortByDate(){
        if(myOrder == 'DESC'){
            myOrder = 'ASC';
        }else if(myOrder == 'ASC'){
            myOrder = 'DESC';
        }
        changeValues = true;
        return null;
    }
    
    @TestVisible
    private String getSortedUserQuery(){
        String query = 'select id,Name,FirstName,LastName,Email,LMS_Registration_Date__c,Username,Profile.Name,Alias,IsActive,LMS_Status__c from User'
            +' where Profile.Name like \'%customer user%\''
            +' and LMS_Status__c = \'Pending\''
            +' and ContactId != null'
            +' and IsActive = True'
            +' and (Not Email like \'%temporary%\')' ;
            query += ' order by LMS_Registration_Date__c '+ myOrder;
            return query;
    }
    
    public Integer getcountVal(){
        Integer userCount =  lmsUserActivation.getResultSize();
        return userCount;
    }
        
    public list<UserWrapper> getListOfUsers(){
        userWrapperList = new list<UserWrapper>();
        list<user> userlist = lmsUserActivation.getRecords();
        if(!userlist.isEmpty()){
            for(User uObj : userlist){
                UserWrapper uwrapObj = new UserWrapper();
                uwrapObj.record = uObj;
                uwrapObj.selected = false;
                userWrapperList.add(uwrapObj);
            }
        }
        return userWrapperList;
    }
    
    public PageReference activateUsers(){
        list<User> selectedUsers = new list<User>();
        for(UserWrapper uw: userWrapperList){
            if(uw.selected){
              uw.record.LMS_Status__c = 'Active';
              uw.record.LMS_Activated_Date__c = System.now();
              uw.selected = false;
              selectedUsers.add(uw.record);
            }
        }
        if(!selectedUsers.isEmpty()){
            if(!System.Test.isRunningTest())
            update selectedUsers;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'LMS account of selected users is activated successfully');
            ApexPages.addMessage(msg);
            changeValues = true;
            
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select user(s) to activate');
            Apexpages.addMessage(msg);
        }
        
        return null;
    }
    
    public Boolean hasNext {
        get {
            return lmsUserActivation.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return lmsUserActivation.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return lmsUserActivation.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    public Integer totalPages {
        get {
            Decimal dTotalPages = lmsUserActivation.getResultSize()/lmsUserActivation.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(lmsUserActivation.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    public void first() {
        lmsUserActivation.first();
    }

    // returns the last page of records
    public void last() {
        lmsUserActivation.last();     
    }

    // returns the previous page of records
    public void previous() {
        lmsUserActivation.previous();
    }

    // returns the next page of records
    public void next() {
       lmsUserActivation.next();
    }

    public class UserWrapper{
        public user record{get;set;}
        public boolean selected{get;set;}
    }
    
}
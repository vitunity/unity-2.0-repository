/***************************************************************************
Author: Divya Hargunani 
Created Date: 8-Nov-2017
Project/Story/Inc/Task : STSK0013224 : Automate LMS Accounts activation from Unity
Description: 
This is test class for Apex class for LMS_UserActivationCtrl
*************************************************************************************/

@isTest(seeAllData= true)
public class LMS_UserActivationCtrlTest {
    static testMethod void testBulkLMSAccountActivation(){
            Account a;  
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState'); 
            insert con;
        
       
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            u = new User(alias = 'standt', email='standarduser1@testorg.com',emailencodingkey='UTF-8', 
                            lastname='Testing',firstname='user',languagelocalekey='en_US',localesidkey='en_US', 
                            profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, 
                            username='test__user1@testorg.com',isActive=true,LMS_Status__c='Pending',LMS_Registration_Date__c= system.now()); 
            insert u; 
        }
         Test.startTest();
        LMS_UserActivationCtrl lms = new LMS_UserActivationCtrl();
        lms.getListOfUsers();
        lms.getSortedUserQuery();
        lms.userWrapperList.get(0).selected = true;
        lms.activateUsers();
        lms.sortByDate();
        lms.myOrder = 'ASC';
        lms.sortByDate();
        
        Boolean nextStatus = lms.hasNext;
        Boolean preStatus = lms.hasPrevious;
        Integer pageNum = lms.pageNumber;
        Integer totalPage = lms.totalPages;
        
        lms.first();
        lms.last();
        lms.previous();
        lms.next();
        
        Test.stopTest();
    }

}
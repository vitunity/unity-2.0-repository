@RestResource(urlMapping='/Leads/*')
    global class LeadController 
    {
    @HttpGet
    global static List<Lead> getLeads() 
    {
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Access-Control-Allow-Origin', '*');
     res.addHeader('Accept', '*');
     res.addHeader('Access-Control-Allow-Origin', 'https://209.160.65.49:1095');
     res.addHeader('Content-Type', 'application/json');
      res.addHeader('Accept', 'application/json');
     res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
     String Id= RestContext.request.params.get('Emailid') ;
     List<Lead> Lead= [SELECT Email,Id,Name FROM Lead WHERE Email =:Id];
     return Lead;
    }
    }
/***********************************************************************
Test Class for code coverage - LeadTerritoryUtility_Test
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Jun-04-2018 - RB - STSK0014772 - Assign Territory/Region to Leads based on country.
************************************************************************/

public class LeadTerritoryUtility{
    public static void updateTerritoryRegion(List<Lead> newListLead, map<Id,Lead> mapLead){
        List<Lead> lstLead = new List<Lead>();
        if(newListLead <> null){
            for(Lead l : newListLead){
                if(mapLead == null || (mapLead <> null && 
                    (
                    mapLead.get(l.id).Country <> l.Country || 
                    mapLead.get(l.id).PostalCode <> l.PostalCode || 
                    mapLead.get(l.id).State <> l.State
                    ))){
                    lstLead.add(l);
                }
            }
        }
        
        if(lstLead.size()>0){
            
            Map<String, Territory_Team__c> TerritoryteamMap = new Map<String, Territory_Team__c>();
            
            Set<String> allcountry = new set<String>();
            Set<String> USAcountry = new set<String>();
            Set<string> FrnsCountry = new set<string>();
            Set<String> postCode = new set<String>();
            Set<String> canadaCountry = new set<String>();
            Set<String> canadaStates = new set<String>();
            Set<String> AustraliaCountry = new set<String>();
            Set<String> AustraliaStates = new set<String>();
            Set<String> JapanCountry = new set<String>();
            Set<String> JapanStates = new set<String>(); 
            Set<String> ChinaCountry = new set<String>();
            Set<String> ChinaStates = new set<String>();
            Set<String> GermanyCountry = new set<String>();
            Set<String> GermanyStates = new set<String>();
        
        
            for(Lead l: lstLead){
                if(l.Country=='USA' || l.Country=='UNITED STATES'){
                    USAcountry.add(l.Country);
                    String strZipCode = '';
                    if(l.PostalCode != null && l.PostalCode.length() > 4)   strZipCode = l.PostalCode.substring(0,5);
                    else strZipCode = l.PostalCode;

                    if(l.PostalCode!=null)  postCode.add(strZipCode);
                }
                else if(l.Country=='France' || l.Country=='FRANCE'){
                    FrnsCountry.add(l.Country);
                    String strZipCode = '';
                    if(l.PostalCode != null){
                        strZipCode = l.PostalCode.substring(0,2);
                        postCode.add(strZipCode);
                    }
                }
                else if(l.Country=='Canada' || l.Country=='CANADA') {
                    canadaCountry.add(l.Country);
                    if(l.State !=null)  canadaStates.add(l.State);
                }                
                else if(l.Country=='Australia' || l.Country=='AUSTRALIA'){
                    AustraliaCountry.add(l.Country);
                    if(l.State !=null)AustraliaStates.add(l.State);
                }
                else if(l.Country=='Japan' || l.Country=='JAPAN'){
                    JapanCountry.add(l.Country);
                    if(l.State !=null)JapanStates.add(l.State);             
                }        
                else if(l.Country=='China' || l.Country=='CHINA'){
                    ChinaCountry.add(l.Country);
                    if(l.State !=null)ChinaStates.add(l.State);
                }
                else if(l.Country=='Germany' || l.Country=='GERMANY'){
                    GermanyCountry.add(l.Country);
                    if(l.State !=null)GermanyStates.add(l.State);
                }
                else allcountry.add(l.Country);
            }
            
            if(USAcountry.size()>0 && postCode.size()>0){
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c, HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where Zip__c IN :postCode and Country__c IN :USAcountry Limit 5000])
                    TerritoryteamMap.put(team1.Zip__c+team1.Country__c,team1); 
            }
            if(FrnsCountry.size()>0 && postCode.size()>0){
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c, HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where Zip__c IN :postCode and Country__c IN: FrnsCountry Limit 5000])
                    TerritoryteamMap.put(team1.Zip__c+team1.Country__c,team1); 
            }
            if(canadaCountry.size()>0 && canadaStates.size()>0){
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c,HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where State__c IN :canadaStates and Country__c IN :canadaCountry Limit 500])
                    TerritoryteamMap.put(team1.State__c+team1.Country__c,team1); 
            }
            if(AustraliaCountry.size() > 0 && AustraliaStates.size()>0){
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c,HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where State__c IN :AustraliaStates and Country__c IN :AustraliaCountry Limit 500])
                    TerritoryteamMap.put(team1.State__c+team1.Country__c,team1); 
            }
            if(JapanCountry.size() > 0 && JapanStates.size()>0){       
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c,HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where State__c IN :JapanStates and Country__c IN :JapanCountry Limit 500])
                    TerritoryteamMap.put(team1.State__c+team1.Country__c,team1); 
            }
            if(ChinaCountry.size() > 0 && ChinaStates.size()>0){       
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c,HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where State__c IN :ChinaStates and Country__c IN :ChinaCountry Limit 500])
                    TerritoryteamMap.put(team1.State__c+team1.Country__c,team1); 
            }
            if(GermanyCountry.size() > 0 && GermanyStates.size()>0){       
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, State__c, District__c, CSS_District__c,HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c,Super_Territory__c from Territory_Team__c where State__c IN :GermanyStates and Country__c IN :GermanyCountry Limit 500])
                    TerritoryteamMap.put(team1.State__c+team1.Country__c,team1); 
            }
            if(allcountry.size()>0){      
                for(Territory_Team__c team1: [Select id,Name,VP_Software_Sales__c,VP_Strategic_Accounts__c,NA_VP__c,Strategic_Accounts_Manager__c,Velocity_Sales_Manager__c,VSS_Sales_Manager__c,District_Manager__c,Software_Sales_Manager__c,District_Sales_Administrator__c,Brachy_Sales_Administrator__c,Brachy_Sales_Manager__c,Network_Specialist__c,OIS_SW__c,Regional_Director__c,Software_Strategic_VP__c,TPS_Sales__c,VSS_District_Manager__c,Contract_HW__c,CUSTOMER_SUPPORT_MANAGER__c,FSR_Specialist__c,HW_Sys_Install_Mgr__c,SW_Install_Mgr__c,SW_Upgrades__c,Zone_VP__c,Zip__c,Country__c, ISO_Country__c, District__c, CSS_District__c, HW_Install_District__c, SW_Install_District__c, Region__c, Sub_Region__c, Territory__c, Super_Territory__c from Territory_Team__c where Country__c IN :allcountry Limit 500])
                    TerritoryteamMap.put(team1.Country__c,team1);
            }
            
           
            for(Lead l: lstLead){
                
                l.Region__c = null;
                l.Territory__c = null;
                    
                Territory_Team__c tt;
                String strZipCode = '';
                String strFrnZipCode = '';
                if(l.PostalCode != null && l.PostalCode.length() > 4)   strZipCode = l.PostalCode.substring(0,5);
                else strZipCode = l.PostalCode;
                
                if(l.Country =='USA')   tt = TerritoryteamMap.get(strZipCode+l.Country );
                else if((l.Country=='France' || l.Country=='FRANCE') && (l.PostalCode != null)){
                    strFrnZipCode  = l.PostalCode.substring(0,2);
                    tt = TerritoryteamMap.get(strFrnZipCode +l.Country);    
                }
                else if(l.Country =='Canada' ||  l.Country =='Australia' || l.Country =='Japan' || l.Country =='China' || l.Country =='Germany')tt = TerritoryteamMap.get(l.State + l.Country );
                
                else    tt = TerritoryteamMap.get(l.Country );

                if(tt!=null){
                    l.Region__c = tt.Region__c;
                    l.Territory__c = tt.Territory__c;                    
                }
                
            }         
            
        }
    }
}
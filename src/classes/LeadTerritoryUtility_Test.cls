@isTest
private class LeadTerritoryUtility_Test{
    
    private static testmethod void testAllTerritory(){
      
        Lead usaLead = new Lead(FirstName='Test2', LastName = 'Testing USA 1856' , Company='My Company2' , Email='testUSA@test.com', Status='Open' , LeadSource='Varian Unite', Country='USA', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert usaLead ;
        
        Lead FranceLead = new Lead(FirstName='Test2', LastName = 'Testing France 1856' , Company='My Company2' , Email='testFrance@test.com', Status='Open' , LeadSource='Varian Unite', Country='France', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert FranceLead ;
        
        Lead CanadaLead = new Lead(FirstName='Test2', LastName = 'Testing Canada 1856' , Company='My Company2' , Email='testCanada@test.com', Status='Open' , LeadSource='Varian Unite', Country='Canada', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert CanadaLead ;
        
        Lead AustraliaLead = new Lead(FirstName='Test2', LastName = 'Testing Australia 1856' , Company='My Australia Company2' , Email='testAustralia@test.com', Status='Open' , LeadSource='Varian Unite', Country='Australia', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert AustraliaLead ;
        
        Lead JapanLead = new Lead(FirstName='Test2', LastName = 'Testing Japan 1856' , Company='My Company2' , Email='testJapan@test.com', Status='Open' , LeadSource='Varian Unite', Country='Japan', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert JapanLead ;
        
        Lead ChinaLead = new Lead(FirstName='Test2', LastName = 'Testing China 1856' , Company='My China Company2' , Email='testChina@test.com', Status='Open' , LeadSource='Varian Unite', Country='China', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert ChinaLead ;
        
        Lead GermanyLead = new Lead(FirstName='Test2', LastName = 'Testing Germany 1856' , Company='My Germany Company2' , Email='testGermany@test.com', Status='Open' , LeadSource='Varian Unite', Country='Germany', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert GermanyLead ;
        
        
        
        //Extra CodeCover
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='USA' ; 
        insert reg ;
        
        Account a = new Account();
        a.Name = 'My Varian GPO 1856';
        a.BillingCountry='USA';
        a.Country__c= 'USA';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'GPO';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';
        insert a ;
        
        Id LOwner = [Select Id from User where Name ='vms-varianunite'].id;
        Lead lead1 = new Lead(OwnerId = LOwner, FirstName='Test2', LastName = 'Testing lead1 1856' , Company='My Company2' , Email='testlead1@test.com', Status='Open' , LeadSource='Varian Unite', Country='USA', City= 'Los Angeles', Street ='3333 W 2nd St', State ='CA', PostalCode = '90020');
        insert lead1 ;
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(lead1.id);
        lc.setAccountId(a.Id);
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        
    }
}
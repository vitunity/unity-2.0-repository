/***********************************************************************
Test Class for code coverage - LeadTerritoryUtility_Test
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Jun-04-2018 - RB - STSK0014772 - Assign Territory/Region to Leads based on country.
************************************************************************/

public class LeadTriggerHandler extends TriggerHandler{
    
    
    public String leadOwnerName ='' ;
    public LeadTriggerHandler() {
        leadOwnerName = system.label.Unity_Lead_User_Name ; 
        
    }
    public override void beforeInsert(){
        // Use Case 1 : If lead is converted into new Account   
        LeadTerritoryUtility.updateTerritoryRegion((List<Lead>)trigger.new,(map<Id,Lead>)trigger.oldmap);
    }
    
    /**
        * This method is used to perform the action on before update any lead record . 
        * @param None 
        * @return - Void 
    */
    public override void beforeUpdate(){
        LeadTerritoryUtility.updateTerritoryRegion((List<Lead>)trigger.new,(map<Id,Lead>)trigger.oldmap);        
    }
    public override void afterUpdate(){ 
        for(Lead lead :(List<Lead>) System.Trigger.new) {
            if(lead.IsConverted && (lead.OwnerId == system.label.Unity_Lead_User_Name.trim()) && lead.LeadSource=='Varian Unite'){
                // Use Case 1 : If lead is converted into Existing Contact 
                Contact con = [SELECT Id, Varian_Unite_User__c FROM Contact WHERE Id = :lead.ConvertedContactId]; 
                con.Varian_Unite_User__c = true ;
                update con ; 
                
            }
            
        }  
        
    }
    public override void afterinsert(){
        
        
        
    }
    
    
}
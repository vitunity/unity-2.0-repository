@isTest(seealldata =false )
private class LeadTriggerHandler_Test {
    
    private static testmethod void testCase(){
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='India' ; 
        insert reg ;
        
        Lead l = new Lead() ; 
        l.FirstName='Test2';
        L.LastName = 'Testing 1856' ; 
        l.Company='My Company2' ;
        l.Email='test2@test.com';
        l.Status='Open' ; 
        l.LeadSource='Varian Unite';
        l.Country='India';
        l.City= 'Los Angeles';
        l.Street ='3333 W 2nd St';
        l.State ='CA';
        l.PostalCode = '90020';
        
        l.OwnerId = [Select Id from User where Name ='vms-varianunite'].id;
        insert l ;
        
        
        Account a = new Account();
        a.Name = 'My Varian GPO 1856';
        a.BillingCountry='USA';
        a.Country__c= 'India';
        a.BillingCity= 'Los Angeles';
        a.BillingStreet ='3333 W 2nd St';
        a.BillingState ='CA';
        a.BillingPostalCode = '90020';
        a.Affiliation__c = 'GPO';
        a.Agreement_Type__c = 'Master Pricing';
        a.OMNI_Address1__c ='3333 W 2nd St';
        a.OMNI_City__c ='Los Angeles';
        a.OMNI_State__c ='CA';
        a.OMNI_Country__c = 'USA';  
        a.OMNI_Postal_Code__c = '90020';
        insert a ;
        
        /*Contact con = new Contact() ; 
        con.FirstName='Test2';
        con.LastName = 'Testing 1856' ; 
        con.Email='test1856@test.com';
        con.MailingCountry='India';
        con.MailingCity= 'Los Angeles';
        con.MailingStreet ='3333 W 2nd St';
        con.MailingState ='CA';
        con.MailingPostalCode = '90020';
        con.AccountId = a.id ;
        
        insert con ;*/
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(l.id);
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setAccountId(a.Id);
        lc.setContactId(a.Id);
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        
        
        LeadTriggerHandler lHandler = new LeadTriggerHandler();
        try{
            lHandler.afterUpdate() ;
        }catch(Exception e){
            
        }
        lHandler.beforeUpdate();
        lHandler.afterinsert();
        lHandler.beforeInsert();
        
        
        
    }
    
    
    
}
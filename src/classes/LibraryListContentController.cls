@RestResource(urlMapping='/LibraryListContent/*')
global class LibraryListContentController 
{
    @HttpGet    
    global static List<ContentDocument> getLibrarys()
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', '*');
        String Id= RestContext.request.params.get('workspaceId') ;
        //String Id= RestContext.request.params.get('workspace') ;
        List <ContentDocument> Librarys = [SELECT Description,FileType,FileExtension,Id,ParentId,Title FROM ContentDocument WHERE ParentId =:Id];        
        return Librarys;
     }
}
@RestResource(urlMapping='/LibraryList/*')

global class LibraryListController

{

@HttpGet

//global static List<ContentWorkspaceDoc> retrieve()

global static List<ContentWorkspace> retrieve()

{

   RestRequest req = RestContext.request;

    RestResponse res = RestContext.response;

    res.addHeader('Access-Control-Allow-Origin','*');

    res.addHeader('Content-Type', 'application/json');

     res.addHeader('Accept', 'application/json');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
      res.addHeader('Access-Control-Allow-Headers','GET, POST, PUT, DELETE');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');

    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

   // List <ContentWorkspaceDoc> Lib= [SELECT ContentDocumentId,ContentWorkspaceId,Id FROM ContentWorkspaceDoc];
    List <ContentWorkspace> Lib= [SELECT Id,Name,Description FROM ContentWorkspace]; 
    system.debug('te4st'+Lib);
    return Lib;
 }
}
@isTest
public class LightningCustomLinkCompControllerTest {
    
    public static User getUser() {
        BigMachines__Oracle_User__c approver = new BigMachines__Oracle_User__c();
        approver =  [SELECt Id, Name, BigMachines__Salesforce_User__c, BigMachines__User_Groups__c 
                FROM BigMachines__Oracle_User__c 
                WHERE BigMachines__Salesforce_User__c !=: null 
                AND BigMachines__User_Groups__c  =: 'approvers' LIMIT 1 ];
                
        User u = new User();
        u = [SELECT Id, Name FROM User WHERE Id =: approver.BigMachines__Salesforce_User__c ];
        
        return u; 
    }
    
    @isTest(SeeAllData = true)
    public static void test_method() {
        User newUsr = new User();
        newUsr = getUser();
        
        system.runas(newUsr) {
            LightningCustomLinkComponentController.validProfile();
        }
    }
}
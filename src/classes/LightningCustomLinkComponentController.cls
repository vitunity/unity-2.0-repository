public class LightningCustomLinkComponentController {
    
    @AuraEnabled
    public static Boolean validProfile() {
        Boolean approvedProfile = false;
        /*String profileId = userInfo.getProfileId();
        Profile p = new Profile();
        p = [SELECT Id, Name, Description, UserType FROM Profile WHERE Id =: profileId];
        List<String> profileName = new List<String>();
        String LightningProfile = System.Label.Lightning_QuotesToApprove;
        profileName = LightningProfile.split(';');
        for(String prof : profileName){
            if(p.Name == prof) {
                approvedProfile = true;
            	break;
            }
        }*/
        String userId = userInfo.getUserId();
        BigMachines__Oracle_User__c approvers = new BigMachines__Oracle_User__c();
        approvers = [SELECt Id, Name, BigMachines__Salesforce_User__c, BigMachines__User_Groups__c FROM BigMachines__Oracle_User__c WHERE BigMachines__Salesforce_User__c =: userId ]; 
        String grp = approvers.BigMachines__User_Groups__c;
        List<String> profileName = new List<String>();
        profileName = grp.split(';');
        for(String prof : profileName){
            if(prof == 'approvers') {
                approvedProfile = true;
            	break;
            }
        }
        return approvedProfile;
    }
    
}
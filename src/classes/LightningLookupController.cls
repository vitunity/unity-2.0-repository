/**
* @author    :  Puneet Mishra
* @Created Date:  13 Sept 2017
* @description  :  controller fetch the records based on search string passed
*/
public class LightningLookupController {
    
    @AuraEnabled public List<SearchResult> searchResultList;
    
    /**
     *  Loads the initial value of the given sObject type with ID value 
     */
    @AuraEnabled
    public static String getCurrentValue(String type, String value) {
        if(String.isBlank(type)){
            return null;
        }
        
        ID lookupId = null;
        try{   
            lookupId = (ID)value;
        }catch(Exception e){
            return null;
        }
        
        if(String.isBlank(lookupId)){
            return null;
        }
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        if(objType == null){
            return null;
        }
        
        String nameField = getSobjectNameField(objType);
        String query = 'Select Id, '+nameField+' From '+type+' Where Id = \''+lookupId+'\'';
        System.debug('### Query: '+query);
        List<SObject> oList = Database.query(query);
        if(oList.size()==0) {
            return null;
        }
        return (String) oList[0].get(nameField);
    }
    
    @AuraEnabled
    public static LightningLookupController searchSObject(String type, String searchString) {
        LightningLookupController controller = new LightningLookupController();
        
        if(String.isBlank(type) || String.isBlank(searchString))
            return null;
        
        if(Schema.getGlobalDescribe().get(type) == null)
            return null;
        
        SObjectType objType = Schema.getGlobalDescribe().get(type);
        String nameField = getSobjectNameField(objType);
        
        searchString = '\'*' + searchString +'*\'';
        String query = 'FIND : searchString IN NAME  FIELDS RETURNING '+ type  + '(Id, '+nameField;
        
        
        if(type == 'Account') {
            query += ' ,BillingStreet, BillingCity, BillingState, BillingCountry ';
        } 
            query += ' ORDER BY NAME ) LIMIT 200';
        
        
        System.debug('SOSL QUERY: '+query);
        List<List<SObject>> queryResults =  Search.query(query);
        system.debug(' === queryResults === ' + queryResults);
        // List will get returned as JSON
        controller.searchResultList = new List<SearchResult>(); 
        if(queryResults.size()>0) {
            for(sObject obj : queryResults[0]) {
                SearchResult sr = new SearchResult();
                sr.Id = String.valueOf(obj.get('Id'));
                sr.value = String.valueOf(obj.get(nameField));
                if(type == 'Account') {
          String street = (string)obj.get('BillingStreet');
                    String city = (string)obj.get('BillingCity');
                    String state = (string)obj.get('BillingState');
                    String country = (string)obj.get('BillingCountry');
                    sr.address = (street == null ? '' : street) + ', '+
                             (street == null ? '' : city) + ', ' +
                             (street == null ? '' : state) + ', ' +
                             (street == null ? '' : country);
                } else {
                    sr.address = '';
                }
                system.debug(' == Object Type == > ' + objType);
                controller.searchResultList.add(sr);
            }
        }
        system.debug(' controller.searchResultList ==> ' + controller.searchResultList);
        system.debug(' JSON SERIALIZE ==> ' + JSON.serialize(controller.searchResultList));
        return controller;
    }
    
    /*
     * Returns the "Name" field for a given SObject (e.g. Case has CaseNumber, Account has Name)
     */
    private static String getSobjectNameField(SobjectType sobjType){
        
        //describes lookup obj and gets its name field
        String nameField = 'Name';
        Schema.DescribeSObjectResult dfrLkp = sobjType.getDescribe();
        for(schema.SObjectField sotype : dfrLkp.fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescObj = sotype.getDescribe();
            if(fieldDescObj.isNameField() ){
                nameField = fieldDescObj.getName();
                break;
            }
        }
        return nameField;
    }
    
    /*
     * Utility class for search results
     */
    public class SearchResult{
        @AuraEnabled public String value{get;Set;}
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String address{get;set;}
    }
}
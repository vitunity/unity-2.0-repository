public class LightningResult {
	@AuraEnabled
    public String message;
    @AuraEnabled
    public String  status;
    @AuraEnabled
    public String  url;
}
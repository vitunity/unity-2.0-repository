public with sharing class Lightning_ERPCustomerRequest {
	
	@AuraEnabled public ERP_Customer_Request__c erpCustomerRequest{get;set;}
	
	private Map<String,String> partners = new Map<String,String>();
	
	private BigMachines__Quote__c objQuote;
	
	//Partner function literals
	private final String SOLD_TO_PARTY = 'SP=Sold-to party';
    private final String SITE_PARTNER = 'Z1=Site Partner';
    private final String BILL_TO_PARTY = 'BP=Bill-to Party';
    private final String SHIP_TO_PARTY = 'SH=Ship-to party';
    private final String PAYER = 'PY=Payer';
    private final String END_USER = 'EU=End User';
	
	//Partner function literals abbrv.
    private final String SP = 'SP';
    private final String Z1 = 'Z1';
    private final String BP = 'BP';
    private final String SH = 'SH';
    private final String PY = 'PY';
    private final String EU = 'EU';
	
	@AuraEnabled
    public static Lightning_ERPCustomerRequest initializeCustomerRequest(
    	String quoteId, String erpRequestId, String billToNumber, String sitePartnerNumber, 
    	String shipToNumber, String payerNumber, String soldToNumber, String salesOrganization
    ){
    	Lightning_ERPCustomerRequest erpCustomerRequestObject = new Lightning_ERPCustomerRequest();
		erpCustomerRequestObject.erpCustomerRequest = erpCustomerRequestObject.getERPCustomerRequest(erpRequestId);
		
		erpCustomerRequestObject.objQuote = getQuote(quoteId);
		
		erpCustomerRequestObject.populateBillToPartyDetails(billToNumber);
		erpCustomerRequestObject.populateSitePartnerDetails(sitePartnerNumber);
		erpCustomerRequestObject.populateShipToPartyDetails(shipToNumber);
		erpCustomerRequestObject.populatePayerDetails(payerNumber);
		erpCustomerRequestObject.populateSoldToPartyDetails(soldToNumber);
		
		erpCustomerRequestObject.updateSalesOrg(salesOrganization);
		return erpCustomerRequestObject;
	}
	
	/**
	 * Query erp customer request in case of edit request request or return new in case of create
	 */
	private ERP_Customer_Request__c getERPCustomerRequest(String recordId){
		
		if(!String.isBlank(recordId)){
			String SobjectApiName = 'ERP_Customer_Request__c';
	        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
	 
	        String commaSepratedFields = '';
	        for(String fieldName : fieldMap.keyset()){
	            if(commaSepratedFields == null || commaSepratedFields == ''){
	                commaSepratedFields = fieldName;
	            }else{
	                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
	            }
	        }
	 
	        String query = 'select ' + commaSepratedFields + ' ,Site_Partner_Account__r.SFDC_Account_Ref_Number__c,Sold_To_Account__r.SFDC_Account_Ref_Number__c From ' + SobjectApiName + ' Where Id =\''+recordId+'\'';
	 		
	 		System.debug('-----query'+query);
	 		
	        return Database.query(query);
		}
		return new ERP_Customer_Request__c();
	}
	
	/**
	 * Format sales org from prepare order page to match ERP Customer Request sales_org__c value.
	 */
	private void updateSalesOrg(String salesOrg){
		System.debug('----salesOrg'+salesOrg);
		if(!String.isBlank(salesOrg)){
			String [] salesOrgArray = salesOrg.split('-');
			String salesOrg1 = salesOrgArray[0].remove(' ');
			String salesOrg2 = '';
			if(salesOrgArray.size()>3){
				if(objQuote.Order_Type__c.equalsIgnoreCase('Combined')){
					salesOrg2 = salesOrgArray[4].remove(' ')+';';
				}
			}
			erpCustomerRequest.Sales_Org__c = salesOrg1+';'+salesOrg2;
			System.debug('----erpCustomerRequest.Sales_Org__c'+erpCustomerRequest.Sales_Org__c);
		}
	}
	
	/**
	 * Get quote info
	 */
	private static BigMachines__Quote__c getQuote(String quoteId){
		if(!String.isBlank(quoteId)){
			BigMachines__Quote__c bmQuote = [Select Id,
													BigMachines__Opportunity__r.Account.Account_Type__c,
													Sales_Org__c, Order_Type__c,
													BigMachines__Account__c,
													BigMachines__Opportunity__r.AccountId
												From BigMachines__Quote__c 
												Where Id =: quoteId];
			return bmQuote;
		}
		
		return null;
	}
	
	/**
	 * Populate bill to party details by querying ERP_Partner_Association__c
	 */
	private void populateBillToPartyDetails(String billToNumber){ 
		
		if(!String.isBlank(billToNumber)){
			
			ERP_Partner_Association__c erpPartner = getERPPartner(BILL_TO_PARTY, billToNumber);
			erpCustomerRequest.Bill_to_Number__c = billToNumber;
			
			if(erpPartner!=null){
				erpCustomerRequest.New_Updated_Bill_To_Name__c = erpPartner.ERP_Partner__r.Name;
				erpCustomerRequest.New_Updated_Bill_To_Street__c = erpPartner.ERP_Partner__r.Street__c;
				erpCustomerRequest.New_Updated_Bill_To_City__c = erpPartner.ERP_Partner__r.City__c;
				erpCustomerRequest.New_Updated_Bill_To_State_Province__c = erpPartner.ERP_Partner__r.State_Province_Code__c;
				erpCustomerRequest.Bill_To_Country__c = erpPartner.ERP_Partner__r.Country_Code__c;
				erpCustomerRequest.New_Updated_Bill_To_Zip_Postal_Code__c = erpPartner.ERP_Partner__r.Zipcode_Postal_Code__c;
			}
		}
	}
	
	private void populateSitePartnerDetails(String sitePartnerNumber){
		
		if(!STring.isBlank(sitePartnerNumber)){
			
			ERP_Partner_Association__c erpPartner = getERPPartner(SITE_PARTNER, sitePartnerNumber);
			erpCustomerRequest.Site_Partner_Number__c = sitePartnerNumber;
			
			if(erpPartner!=null){
				erpCustomerRequest.New_Updated_Site_Partner_Name__c = erpPartner.ERP_Partner__r.Name;
				//erpCustomerRequest.SFA_Number_Site_Partner__c = erpPartner.Customer_Account__r.SFDC_Account_Ref_Number__c;
				erpCustomerRequest.New_Updated_Site_Partner_Street__c = erpPartner.ERP_Partner__r.Street__c;
				erpCustomerRequest.New_Updated_Site_Partner_City__c = erpPartner.ERP_Partner__r.City__c;
				erpCustomerRequest.New_Updated_Site_Partner_State_Province__c = erpPartner.ERP_Partner__r.State_Province_Code__c;
				erpCustomerRequest.Site_Partner_Country__c = erpPartner.ERP_Partner__r.Country_Code__c;
				erpCustomerRequest.New_Updated_Site_Partner_Zip_Postal_Code__c = erpPartner.ERP_Partner__r.Zipcode_Postal_Code__c;
				
				List <Account_Associations__c> objAccAsso = [select Id, Name, Account__c
																	From Account_Associations__c 
																	where Master_Account__c != null 
																	And Account__c = : objQuote.BigMachines__Opportunity__r.Accountid 
																	And (Account_Role__c = 'Site Partner'or Account_Role__c = 'Sold To') 
																	And Account__r.AccountNumber = : sitePartnerNumber LIMIT 1];

				if (!objAccAsso.isEmpty()) erpCustomerRequest.Site_Partner_Account__c = objAccAsso[0].Account__c;
			}
		}
	}
	
	private void populateShipToPartyDetails(String shipToNumber){ 
		
		if(!STring.isBlank(shipToNumber)){
			
			ERP_Partner_Association__c erpPartner = getERPPartner(SHIP_TO_PARTY, shipToNumber);
			erpCustomerRequest.Ship_To_Number__c = shipToNumber;
			
			if(erpPartner!=null){
				erpCustomerRequest.Ship_To_Name__c = erpPartner.ERP_Partner__r.Name;
				erpCustomerRequest.Ship_To_Street__c = erpPartner.ERP_Partner__r.Street__c;
				erpCustomerRequest.Ship_To_City__c = erpPartner.ERP_Partner__r.City__c;
				erpCustomerRequest.Ship_To_State_Province__c = erpPartner.ERP_Partner__r.State_Province_Code__c;
				erpCustomerRequest.Ship_To_Country__c = erpPartner.ERP_Partner__r.Country_Code__c;
				erpCustomerRequest.Ship_To_Zip_Postal_Code__c = erpPartner.ERP_Partner__r.Zipcode_Postal_Code__c;
			}
		}
	}
	
	private void populatePayerDetails(String payerNumber){ 
		
		if(!String.isBlank(payerNumber)){
			
			ERP_Partner_Association__c erpPartner = getERPPartner(PAYER, payerNumber);
			erpCustomerRequest.Payer_Number__c = payerNumber;
			
			if(erpPartner!=null){
				erpCustomerRequest.New_Updated_Payer_Name__c = erpPartner.ERP_Partner__r.Name;
				erpCustomerRequest.New_Updated_Payer_Street__c = erpPartner.ERP_Partner__r.Street__c;
				erpCustomerRequest.New_Updated_Payer_City__c = erpPartner.ERP_Partner__r.City__c;
				erpCustomerRequest.New_Updated_Payer_State_Province__c = erpPartner.ERP_Partner__r.State_Province_Code__c;
				erpCustomerRequest.Payer_Country__c = erpPartner.ERP_Partner__r.Country_Code__c;
				erpCustomerRequest.New_Updated_Payer_Zip_Postal_Code__c = erpPartner.ERP_Partner__r.Zipcode_Postal_Code__c;
			}
		}
	}
	
	private void populateSoldToPartyDetails(String soldToNumber){ 
		
		if(!String.isBlank(soldToNumber)){
			
			ERP_Partner_Association__c erpPartner = getERPPartner(SOLD_TO_PARTY, soldToNumber);
			erpCustomerRequest.Sold_To_Number__c = soldToNumber;
			
			if(erpPartner!=null){
				erpCustomerRequest.New_Updated_Sold_To_Name__c = erpPartner.ERP_Partner__r.Name;
				erpCustomerRequest.SFA_Number_Sold_To__c = erpPartner.Customer_Account__r.SFDC_Account_Ref_Number__c;
				erpCustomerRequest.New_Updated_Sold_To_Street__c = erpPartner.ERP_Partner__r.Street__c;
				erpCustomerRequest.New_Updated_Sold_To_City__c = erpPartner.ERP_Partner__r.City__c;
				erpCustomerRequest.New_Updated_Sold_To_State_Province__c = erpPartner.ERP_Partner__r.State_Province_Code__c;
				erpCustomerRequest.Sold_To_Country__c = erpPartner.ERP_Partner__r.Country_Code__c;
				erpCustomerRequest.New_Updated_Sold_To_Zip_Postal_Code__c = erpPartner.ERP_Partner__r.Zipcode_Postal_Code__c;
			}
		}
	}
	
	private ERP_Partner_Association__c getERPPartner(String partnerFunction,String partnerNumber){
		List<ERP_Partner_Association__c> erpPartner = [Select Id, Name, ERP_Partner__c, Sales_Org__c, ERP_Partner__r.Name,
															ERP_Partner__r.Partner_Number__c, ERP_Partner__r.Street__c,
															Partner_Zipcode_postal_code__c, ERP_Partner__r.City__c, ERP_Partner__r.State_Province_Code__c,
															ERP_Partner__r.Country_Code__c, ERP_Partner__r.Zipcode_Postal_Code__c,
															Customer_Account__r.Name,Customer_Account__r.SFDC_Account_Ref_Number__c
														From ERP_Partner_Association__c 
														Where ERP_Partner_Number__c = : partnerNumber 
														And Partner_Function__c = :partnerFunction
														limit 1];
		if(!erpPartner.isEmpty()){	
			return erpPartner[0]; 		
		}else{
			return null;
		}												
	}
	@AuraEnabled
	public static String saveRequest(String erpCustomerRequestObject){
		
		Lightning_ERPCustomerRequest erpCustomerRequestController = (Lightning_ERPCustomerRequest)JSON.deserialize(erpCustomerRequestObject, Lightning_ERPCustomerRequest.class);
		
		if(erpCustomerRequestController.erpCustomerRequest.Id==null){
			insert erpCustomerRequestController.erpCustomerRequest;
		}else{
			System.debug('----erpCustomerRequest.Ship_To_Country__c'+erpCustomerRequestController.erpCustomerRequest.Ship_To_Country__c);
			update erpCustomerRequestController.erpCustomerRequest;
		}
		
		return erpCustomerRequestController.erpCustomerRequest.Id;
	}
	
	@AuraEnabled
	public List<Lightning_PrepareOrderController.SelectOptionCustom> getSalesOrgs(){
		List<Lightning_PrepareOrderController.SelectOptionCustom> options = new List<Lightning_PrepareOrderController.SelectOptionCustom>();
        
		Schema.DescribeFieldResult fieldResult = ERP_Customer_Request__c.Sales_Org__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   		for( Schema.PicklistEntry f : ple){
			options.add(new Lightning_PrepareOrderController.SelectOptionCustom(f.getValue(), f.getLabel()));
		}       
		return options;
	}
}
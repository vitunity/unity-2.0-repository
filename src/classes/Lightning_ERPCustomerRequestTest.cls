@isTest(seeAlldata=true)
private class Lightning_ERPCustomerRequestTest {

    static testMethod void testNewLightning_ERPCustomerRequest() {
    	//insert new Regulatory_Country__c(
    		//Name = 'USA'
    	//);
        Lightning_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        Account sfdcAccount = [Select Id, SFDC_Account_Ref_Number__c 
                                    From Account 
                                    Where Id =:Lightning_PrepareOrderControllerTest.CombinedQuote.Bigmachines__Account__c];
        
        ApexPages.StandardController sc  = new ApexPages.StandardController(new ERP_Customer_Request__c());
        
        Lightning_ERPCustomerRequest erpCustomerRequest = Lightning_ERPCustomerRequest.initializeCustomerRequest(
        	Lightning_PrepareOrderControllerTest.CombinedQuote.Id, '', 'Combined1212', 'Combined1212', 'Combined1212', 'Combined1212', 'Combined1212', '0601-US - OS - Systems - 0600-US - OS - Service'
        );
        
        erpCustomerRequest.getSalesOrgs();
        
        Lightning_ERPCustomerRequest.saveRequest(JSON.serialize(erpCustomerRequest));
        
        ERP_Customer_Request__c erpCustomerRec = [Select Id, New_Updated_Bill_To_Name__c,New_Updated_Site_Partner_Name__c,
                                                            Ship_To_Name__c,New_Updated_Payer_Name__c,New_Updated_Sold_To_Name__c,
                                                            SFA_Number_Sold_To__c,SFA_Number_Site_Partner__c
                                                        From  ERP_Customer_Request__c
                                                        Where New_Updated_Bill_To_Name__c = 'Sample Partner'];
        
        System.assertEquals(erpCustomerRec.New_Updated_Bill_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Site_Partner_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.Ship_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Payer_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Sold_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.SFA_Number_Sold_To__c,sfdcAccount.SFDC_Account_Ref_Number__c);
        //System.assertEquals(erpCustomerRec.SFA_Number_Site_Partner__c,sfdcAccount.SFDC_Account_Ref_Number__c);
    }
    
    static testMethod void testEditLightning_ERPCustomerRequest() {
    	//insert new Regulatory_Country__c(
    		//Name = 'USA'
    	//);
        Lightning_PrepareOrderControllerTest.setUpCombinedTestdata();
        ERP_Customer_Request__c erpCustomerRec = new ERP_Customer_Request__c();
        erpCustomerRec.New_Updated_Bill_To_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Site_Partner_Name__c = 'TEST NAME';
        erpCustomerRec.Ship_To_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Payer_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Sold_To_Name__c = 'TEST NAME';
        insert erpCustomerRec;
        
        PageReference erpCustomerRequestPage = Page.SR_CustomerRequestForm;
        erpCustomerRequestPage.getParameters().put('Id',erpCustomerRec.Id);
        Test.setCurrentPage(erpCustomerRequestPage);
        
        ApexPages.StandardController sc  = new ApexPages.StandardController(erpCustomerRec);
        
        Lightning_ERPCustomerRequest updateCustomer = Lightning_ERPCustomerRequest.initializeCustomerRequest(
        	Lightning_PrepareOrderControllerTest.CombinedQuote.Id, erpCustomerRec.Id, '', '', '', '', '', ''
        );
        updateCustomer.erpCustomerRequest.Bill_To_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Ship_To_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Site_Partner_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Sold_To_Country__c = 'TEST Country';
        Lightning_ERPCustomerRequest.saveRequest(JSON.serialize(updateCustomer));
        
        ERP_Customer_Request__c erpCustomer = [Select Id, New_Updated_Bill_To_Name__c,New_Updated_Site_Partner_Name__c,
                                                            Ship_To_Name__c,New_Updated_Payer_Name__c,New_Updated_Sold_To_Name__c,
                                                            Ship_To_Country__c,Site_Partner_Country__c,Sold_To_Country__c,Bill_To_Country__c
                                                        From  ERP_Customer_Request__c
                                                        Where Id =:erpCustomerRec.Id];
        
        System.assertEquals('TEST NAME', erpCustomer.New_Updated_Bill_To_Name__c);
        System.assertEquals('TEST NAME', erpCustomer.New_Updated_Site_Partner_Name__c);
        System.assertEquals('TEST NAME', erpCustomer.Ship_To_Name__c);
        System.assertEquals('TEST NAME', erpCustomer.New_Updated_Payer_Name__c);
        System.assertEquals('TEST NAME', erpCustomer.New_Updated_Sold_To_Name__c);
        
        System.assertEquals('TEST Country', erpCustomer.Ship_To_Country__c);
        System.assertEquals('TEST Country', erpCustomer.Site_Partner_Country__c,'TEST Country');
        System.assertEquals('TEST Country', erpCustomer.Sold_To_Country__c,'TEST Country');
        System.assertEquals('TEST Country', erpCustomer.Bill_To_Country__c,'TEST Country');
        
    }
}
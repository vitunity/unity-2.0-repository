/**
 * @author Krishna Katve
 * @description Currently used in prepare order lightning component for contact lookups
 * Can be used for any other object lookup field
 */

public with sharing class Lightning_LookupController {
      
    @AuraEnabled public List<sObject> objectRecords;
    @AuraEnabled public Integer offset;
    @AuraEnabled public Integer total;
    @AuraEnabled public Boolean hasPrev;
    @AuraEnabled public Boolean hasNext;
    @AuraEnabled public Integer totalPages;
    
    @AuraEnabled public String sObjectName;
    @AuraEnabled public String defaultFieldName;
    @AuraEnabled public String defaultFieldValue;
    @AuraEnabled public String searchText;
    @AuraEnabled public List<String> objectFields;
 
    @AuraEnabled public Integer pageSize;
    
      
    @AuraEnabled
    public static Lightning_LookupController initializeLookupController(String lookupControllerString){
    	System.debug('-----lookupControllerString'+lookupControllerString);
        Lightning_LookupController lookupControllerObject = (Lightning_LookupController)JSON.deserialize(lookupControllerString, Lightning_LookupController.class);
        System.debug('-----lookupControllerObject'+lookupControllerObject);
        List<sObject> objectRecordList = new List<sObject>();
        Integer listlength;
        String filters = '';
        Boolean isSOSL = false;
        String soslQuery = '';
        System.debug('-----searchText'+lookupControllerObject.searchText+'----'+!String.isBlank(lookupControllerObject.searchText));
        System.debug('-----lookupControllerObject.defaultFieldName'+lookupControllerObject.defaultFieldValue);
        if(!String.isBlank(lookupControllerObject.searchText) && lookupControllerObject.searchText.length() >= 3){
            soslQuery = getRecordsBYSOSL(lookupControllerObject.searchText, lookupControllerObject.objectFields, lookupControllerObject.sObjectName);
            listlength = Search.query(soslQuery+')')[0].size();
            System.debug('------soslQuery'+soslQuery+'-----searchText'+lookupControllerObject.searchText);
            isSOSL = true;
        }else{
            filters = getRecordsBYSOQL(lookupControllerObject.defaultFieldName, lookupControllerObject.defaultFieldValue);
            System.debug('-----filters'+filters+'----lookupControllerObject.sObjectName'+'---'+'SELECT count() FROM '+lookupControllerObject.sObjectName+' '+filters);
            listlength = Database.countQuery('SELECT count() FROM '+lookupControllerObject.sObjectName+' '+filters+' LIMIT 50000');
        }
        System.debug('-----searchText'+lookupControllerObject.searchText+'----'+!String.isBlank(lookupControllerObject.searchText));
        System.debug('----prev'+lookupControllerObject.hasPrev);
        System.debug('----next'+lookupControllerObject.hasNext);
        System.debug('----offset'+lookupControllerObject.offset);
        String query = buildQuery(lookupControllerObject.objectFields, lookupControllerObject.sObjectName) + filters;
        
        if(lookupControllerObject.hasNext == false && lookupControllerObject.hasPrev == false){
        	System.debug('----in if');
        	objectRecordList = getRecords(lookupControllerObject.pagesize, lookupControllerObject.offset, isSOSL, query, soslQuery);
        }else if(lookupControllerObject.hasNext == true && (lookupControllerObject.offset+lookupControllerObject.pagesize)<=listlength){
        	lookupControllerObject.offset += lookupControllerObject.pagesize;
        	System.debug('----in else if'+lookupControllerObject.offset);
        	objectRecordList = getRecords(lookupControllerObject.pagesize, lookupControllerObject.offset, isSOSL, query, soslQuery);
        }else if(lookupControllerObject.hasPrev == true && lookupControllerObject.offset>0){
            lookupControllerObject.offset -= lookupControllerObject.pagesize;
            System.debug('----in else'+lookupControllerObject.offset);
            objectRecordList = getRecords(lookupControllerObject.pagesize, lookupControllerObject.offset, isSOSL, query, soslQuery);
        }
		
        Lightning_LookupController lookupObject = new Lightning_LookupController();
        lookupObject.objectRecords = objectRecordList;
        lookupObject.hasPrev = hasPrev(lookupControllerObject.offset);   
        lookupObject.hasNext = hasnxt(lookupControllerObject.offset,listlength,lookupControllerObject.pagesize);
        lookupObject.totalPages = listlength/lookupControllerObject.pagesize+(Math.mod(listlength,lookupControllerObject.pagesize) > 0?1:0);
        lookupObject.offset = lookupControllerObject.offset;
        return lookupObject;
    }
	
	private static List<sObject> getRecords(Integer PAGESIZE, Integer OFFSET, Boolean isSOSL, String soqlQuery, String soslQuery){
		if(!isSOSL){
            soqlQuery +=  ' ORDER BY LastViewedDate DESC LIMIT '+PAGESIZE+' OFFSET '+OFFSET;
            System.debug('----query--else--'+soqlQuery);
            return Database.query(soqlQuery);
        }else{
        	soslQuery += ' LIMIT '+PAGESIZE+' OFFSET '+OFFSET+')';
    		return Search.query(soslQuery)[0];
        }
	}
	
    private Static String getRecordsBYSOSL(String searchText, List<String> objectFields, String objectName){
        String fieldNames = objectName+'(Id';
        
        for(String fieldName : objectFields){
        	fieldNames += ','+fieldName;       
        }
        String escapedSearchText = String.escapeSingleQuotes( searchText ) ;
        String searchquery='FIND\''+escapedSearchText+'*\'IN ALL FIELDS RETURNING '+fieldNames; 
        
        return searchquery;
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all object Records
     */
    private static String getRecordsBYSOQL(String defaultField, String defaultValue){
        
        String filters = ' ';
        
        if(!String.isBlank(defaultField) && !String.isBlank(defaultValue)){
            filters += ' WHERE '+defaultField+' = \''+defaultValue+'\'';
        }else{
        	filters += ' WHERE LastViewedDate !=null ';
        }
        filters += ' ';
        return filters;
    }
    
    private static String buildQuery(List<String> objectFields, String objectName){
    	System.debug('-----objectFields'+objectFields);
        String query = 'SELECT Id';
        
        for(String fieldName : objectFields){
            query += ','+fieldName;
        }
        
        query += ' FROM '+objectName+' ';
        
        System.debug('----query'+query);
        return query;
    }
    
    
    private static boolean hasPrev(Integer off){
        if(off>0){
            return true;
        }
        return false; 
    }
    private static boolean hasnxt(Integer off,Integer li,Integer ps){
        if(off+ps<li){
            return true;
        }
        return false;
    }    
}
@isTest
private class Lightning_LookupControllerTest {

    static testMethod void testCustomLookup() {
      
    Id [] fixedSearchResults= new List<Id>();
    
      
      insert new Regulatory_Country__c(
        Name = 'USA'
      );
      
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        List<Contact> contacts = new List<Contact>();
        for(Integer counter=0; counter<25; counter++){
          Contact con = TestUtils.getContact();
          con.LastName = 'LastName'+Counter;
          con.AccountId = salesAccount.Id;
          contacts.add(con);
        }
        insert contacts;
        for(Contact con :contacts){
          fixedSearchResults.add(con.Id);
        }
        Lightning_LookupController lookupController = new Lightning_LookupController();
        lookupController.sObjectName = 'Contact';
        lookupController.searchText = '';
        lookupController.pageSize = 10;
        lookupController.offset = 0;
        lookupController.defaultFieldName = 'AccountId';
        lookupController.defaultFieldValue = salesAccount.Id;
        lookupController.objectFields = new List<String>{'FirstName','LastName', 'Account.Name','Email'};
        lookupController.hasPrev = false;
        lookupController.hasNext = false;
        lookupController = Lightning_LookupController.initializeLookupController(JSON.serialize(lookupController));
        System.assertEquals(10, lookupController.objectRecords.size());
        
        System.debug('----lookupController'+lookupController);
        
        lookupController.hasNext = true;
        lookupController.hasPrev = false;
        lookupController.sObjectName = 'Contact';
        lookupController.searchText = '';
        lookupController.pageSize = 10;
        lookupController.defaultFieldName = 'AccountId';
        lookupController.defaultFieldValue = salesAccount.Id;
        lookupController.objectFields = new List<String>{'FirstName','LastName', 'Account.Name','Email'};
        lookupController = Lightning_LookupController.initializeLookupController(JSON.serialize(lookupController));
        System.assertEquals(10, lookupController.objectRecords.size());
        
        System.debug('----lookupController1'+lookupController);
        
        lookupController.hasNext = true;
        lookupController.hasPrev = false;
        lookupController.sObjectName = 'Contact';
        lookupController.searchText = '';
        lookupController.pageSize = 10;
        lookupController.defaultFieldName = 'AccountId';
        lookupController.defaultFieldValue = salesAccount.Id;
        lookupController.objectFields = new List<String>{'FirstName','LastName', 'Account.Name','Email'};
        lookupController = Lightning_LookupController.initializeLookupController(JSON.serialize(lookupController));
        System.assertEquals(5, lookupController.objectRecords.size());
        
        lookupController.hasNext = false;
        lookupController.hasPrev = true;
        lookupController.sObjectName = 'Contact';
        lookupController.searchText = '';
        lookupController.pageSize = 10;
        lookupController.defaultFieldName = 'AccountId';
        lookupController.defaultFieldValue = salesAccount.Id;
        lookupController.objectFields = new List<String>{'FirstName','LastName', 'Account.Name','Email'};
        lookupController = Lightning_LookupController.initializeLookupController(JSON.serialize(lookupController));
        System.assertEquals(10, lookupController.objectRecords.size());
        
        Test.setFixedSearchResults(fixedSearchResults);
        Lightning_LookupController lookupController1 = new Lightning_LookupController();
        lookupController1.sObjectName = 'Contact';
        lookupController1.searchText = 'Last';
        lookupController1.pageSize = 10;
        lookupController1.offset = 0;
        lookupController1.defaultFieldName = 'AccountId';
        lookupController1.defaultFieldValue = salesAccount.Id;
        lookupController1.objectFields = new List<String>{'FirstName','LastName', 'Account.Name','Email'};
        lookupController1.hasPrev = false;
        lookupController1.hasNext = false;
        lookupController1 = Lightning_LookupController.initializeLookupController(JSON.serialize(lookupController1));
        System.assertEquals(10, lookupController1.objectRecords.size());
    }
}
@isTest
private class Lightning_LookupPartnersTest {

    static testMethod void testLookupController() {
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales1212';
        insert erpPartner;
        
        insertERPAssociations('Sales1212', erpPartner.Id, salesAccount.Id);
        
        Lightning_LookupPartners lookupControllerZ1 = Lightning_LookupPartners.initializeLookupPartners('Z1', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
        
        Lightning_LookupPartners lookupControllerBP = Lightning_LookupPartners.initializeLookupPartners('BP', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerBP.getErpPartnerAssociations().size());
        
        Lightning_LookupPartners lookupControllerSH = Lightning_LookupPartners.initializeLookupPartners('SH', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerSH.getErpPartnerAssociations().size());
        
        Lightning_LookupPartners lookupControllerSP = Lightning_LookupPartners.initializeLookupPartners('SP', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        System.assert(lookupControllerSP.hasNext);
        lookupControllerSP.getNext();
        System.assertEquals(5,lookupControllerSP.getErpPartnerAssociations().size());
        System.assert(lookupControllerSP.hasPrevious);
        lookupControllerSP.getPrevious();
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        lookupControllerSP.getLast();
        System.assertEquals(5,lookupControllerSP.getErpPartnerAssociations().size());
        lookupControllerSP.getFirst();
        System.assertEquals(1,lookupControllerSP.pageNumber);
        System.assertEquals(2,lookupControllerSP.totalPages);
        System.assertEquals('Sold To Parties',lookupControllerSP.getPartnerType());
        
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        
        
        Lightning_LookupPartners lookupControllerPY = Lightning_LookupPartners.initializeLookupPartners('PY', 'SalesCustomer', '0600', '');
        
        System.assertEquals(10,lookupControllerPY.getErpPartnerAssociations().size());
        
        Lightning_LookupPartners lookupControllerEU = Lightning_LookupPartners.initializeLookupPartners('EU', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerEU.getErpPartnerAssociations().size());
    }
    
    static testMethod void testLookUpControllerSearchString(){
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sample Partner';
        erpPartner.Partner_Number__c = 'Sales1212';
        insert erpPartner;
        
        insertERPAssociations('Sales1212', erpPartner.Id, salesAccount.Id);
        
        
        Lightning_LookupPartners lookupControllerZ1 = Lightning_LookupPartners.initializeLookupPartners('Z1', 'SalesCustomer', '0600', '');
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
        
        lookupControllerZ1 = Lightning_LookupPartners.initializeLookupPartners('Z1', 'SalesCustomer', '0600', 'Sample');
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
    }
    
    private static void insertERPAssociations(String partnerNumber, Id erpPartnerId, Id accountId){
        
        Set<String> partnerFunctions = new Set<String>{'SP=Sold-to party',
                                                        'Z1=Site Partner',
                                                        'BP=Bill-to Party',
                                                        'SH=Ship-to party',
                                                        'PY=Payer',
                                                        'EU=End User'};
        
        List<ERP_Partner_Association__c> partnerAssociations = new List<ERP_Partner_Association__c>();
        
        for(String partnerFunction : partnerFunctions){
            for(Integer counter = 0; counter<15;counter++){
                ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
                partnerAssociation.Customer_Account__c = accountId;
                partnerAssociation.Erp_Partner__c = erpPartnerId;
                partnerAssociation.ERP_Customer_Number__c = 'SalesCustomer';
                partnerAssociation.Partner_Function__c = partnerFunction;
                partnerAssociation.ERP_Partner_Number__c = partnerNumber;
                partnerAssociation.Sales_Org__c = '0600';
                partnerAssociations.add(partnerAssociation);
            }
        }
        insert partnerAssociations;
    }
}
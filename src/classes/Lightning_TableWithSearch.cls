public with sharing class Lightning_TableWithSearch {
    
    @AuraEnabled public List<sObject> objectRecords;
    @AuraEnabled public Integer offset;
    @AuraEnabled public Integer total;
    @AuraEnabled public Boolean hasPrev;
    @AuraEnabled public Boolean hasNext;
    
    @AuraEnabled public String sObjectName;
    @AuraEnabled public List<String> defaultFieldNames;
    @AuraEnabled public List<String> defaultFieldValues;
    @AuraEnabled public String startDate;
    @AuraEnabled public String endDate;
    @AuraEnabled public List<String> objectFields;
    @AuraEnabled public Integer totalPages;
    @AuraEnabled public Integer pageSize;
    
      
    @AuraEnabled
    public static Lightning_TableWithSearch initializeTable(String tableObjectString){
        System.debug('-----tableObjectString'+tableObjectString);
        Lightning_TableWithSearch tableDisplayObject = (Lightning_TableWithSearch)JSON.deserialize(tableObjectString, Lightning_TableWithSearch.class);
        System.debug('-----tableDisplayObject'+tableDisplayObject);
        List<sObject> objectRecordList = new List<sObject>();
        Integer listlength;
        String filters = '';
        System.debug('-----tableDisplayObject.defaultFieldValues'+tableDisplayObject.defaultFieldValues);
        
        filters = getRecordsBYSOQL(tableDisplayObject.defaultFieldNames, tableDisplayObject.defaultFieldValues, tableDisplayObject.startDate, tableDisplayObject.endDate);
        System.debug('-----filters'+filters+'----tableDisplayObject.sObjectName'+'---'+'SELECT count() FROM '+tableDisplayObject.sObjectName+' '+filters);
        listlength = Database.countQuery('SELECT count() FROM '+tableDisplayObject.sObjectName+' '+filters+' LIMIT 50000');
        System.debug('----prev'+tableDisplayObject.hasPrev);
        System.debug('----next'+tableDisplayObject.hasNext);
        System.debug('----offset'+tableDisplayObject.offset);
        String query = buildQuery(tableDisplayObject.objectFields, tableDisplayObject.sObjectName) + filters;
        System.debug('---query--'+query);
        if(tableDisplayObject.hasNext == false && tableDisplayObject.hasPrev == false){
            System.debug('----in if');
            objectRecordList = getRecords(tableDisplayObject.pagesize, tableDisplayObject.offset, query);
        }else if(tableDisplayObject.hasNext == true && (tableDisplayObject.offset+tableDisplayObject.pagesize)<=listlength){
            tableDisplayObject.offset += tableDisplayObject.pagesize;
            System.debug('----in else if'+tableDisplayObject.offset);
            objectRecordList = getRecords(tableDisplayObject.pagesize, tableDisplayObject.offset, query);
        }else if(tableDisplayObject.hasPrev == true && tableDisplayObject.offset>0){
            tableDisplayObject.offset -= tableDisplayObject.pagesize;
            System.debug('----in else'+tableDisplayObject.offset);
            objectRecordList = getRecords(tableDisplayObject.pagesize, tableDisplayObject.offset, query);
        }
        
        Lightning_TableWithSearch tableObj = new Lightning_TableWithSearch();
        tableObj.objectRecords = objectRecordList;
        tableObj.hasPrev = hasPrev(tableDisplayObject.offset);   
        tableObj.hasNext = hasnxt(tableDisplayObject.offset,listlength,tableDisplayObject.pagesize);
        tableObj.totalPages = listlength/tableDisplayObject.pagesize+(Math.mod(listlength,tableDisplayObject.pagesize) > 0?1:0);
        tableObj.offset = tableDisplayObject.offset;
        return tableObj;
    }
    
    private static List<sObject> getRecords(Integer PAGESIZE, Integer OFFSET, String soqlQuery){
        soqlQuery +=  ' ORDER BY Name DESC LIMIT '+PAGESIZE+' OFFSET '+OFFSET;
        System.debug('----query--else--'+soqlQuery);
        return Database.query(soqlQuery);
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all object Records
     */
    private static String getRecordsBYSOQL(List<String> defaultFields, List<String> defaultValues, String startDate, String endDate){
        
        String filters = ' WHERE Name != \'\' ';
        Integer counter = 0;
        if(!defaultFields.isEmpty()){
            for(String defaultField : defaultFields){
                if(!String.isBlank(defaultField) && !String.isBlank(defaultValues[counter])){
                    if(Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(defaultValues[counter]).matches()){
                        filters += ' AND '+defaultField+' = \''+defaultValues[counter]+'\' ';
                    }else{
                        //filters += ' AND '+defaultField+' LIKE \''+defaultValues[counter]+'%\' ';
                        filters += ' AND '+defaultField+' LIKE \'%'+defaultValues[counter]+'%\' '; 
                    }
                }
                counter+=1;
            }
        }
        
        if(String.isnotEmpty(startDate)  && string.isnotEmpty(endDate)){
            String dtFrom = stringToDate(startDate);
            String dtTo = stringToDate(endDate); 
            filters+= 'AND CreatedDate >='+ dtFrom+ 'AND CreatedDate <='+ dtTo ;
        }    
        
        return filters;
    }
    
    private static String stringToDate(String inputDate){
        String[] dateFromToParse = inputDate.split(' ');
        String[] edDate = dateFromToParse[0].split('/');
        Integer myIntFromDate = integer.valueOf(edDate[1]);
        Integer myIntFromMonth = integer.valueOf(edDate[0]);
        Integer myIntFromYear = integer.valueOf(edDate[2]);
        DateTime dt = DateTime.newInstance(myIntFromYear, myIntFromMonth, myIntFromDate);
        String formatedDt = dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        return formatedDt;
    }
    
    private static String buildQuery(List<String> objectFields, String objectName){
        System.debug('-----objectFields'+objectFields);
        String query = 'SELECT Id';
        
        for(String fieldName : objectFields){
            query += ','+fieldName;
        }
        
        query += ' FROM '+objectName+' ';
        
        System.debug('----query'+query);
        return query;
    }
    
    
    private static boolean hasPrev(Integer off){
        if(off>0){
            return true;
        }
        return false; 
    }
    private static boolean hasnxt(Integer off,Integer li,Integer ps){
        if(off+ps<li){
            return true;
        }
        return false;
    }
}
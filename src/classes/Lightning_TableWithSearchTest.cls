@isTest
public class Lightning_TableWithSearchTest {
	
	public static Account salesAccount;
	public static List<Contact> contacts;
	public static List<API_Request__c> apiRequests;
    static testMethod void testCustomLookup() {
    	
    	insertAPIRequestData();
        
        Date startDateValue = Date.today().addDays(-1);
        Date endDateValue = Date.today().addDays(2);
        String startDate = String.valueOf(startDateValue.month())+'/'+String.valueOf(startDateValue.day())+'/'+String.valueOf(startDateValue.year());
        String endDate = String.valueOf(endDateValue.month())+'/'+String.valueOf(endDateValue.day())+'/'+String.valueOf(endDateValue.year());
        
        Lightning_TableWithSearch lightningTable = new Lightning_TableWithSearch();
        lightningTable.sObjectName = 'API_Request__c';
        lightningTable.pageSize = 10;
        lightningTable.offset = 0;
        lightningTable.objectFields = new List<String>{
        	'Name', 'API_Request_Id__c', 'Software_System__c', 'API_Type__c', 'OwnerId',
        	'Request_Reason__c', 'Customer_Name__c', 'Customer_Contact_Name__c', 'Owner_Approver_Name__c',
        	'Request_Status__c', 'Approval_Status__c', 'CreatedDate', 'Approver_Name__c',
        	'X3rd_party_Software__c', 'API_Key_Attachment_Id__c', 'Approver_Comment__c'
        };
        lightningTable.defaultFieldNames = new List<String>{
        	'Name', 'Account__c', 'Customer_Name__c', 'Software_System__c', 'API_Type__c',
             'X3rd_party_Software__c', 'Approval_Status__c'
        };
        lightningTable.defaultFieldValues = new List<String>{
        	'', salesAccount.Id, '', 'TEST', '',
             '', ''
        };
        lightningTable.startDate = startDate;
		lightningTable.endDate = endDate;
        lightningTable.hasPrev = false;
        lightningTable.hasNext = false;
        lightningTable.pageSize = 10;
        lightningTable = Lightning_TableWithSearch.initializeTable(JSON.serialize(lightningTable));
        System.assertEquals(10, lightningTable.objectRecords.size());
        
        System.debug('----lightningTable'+lightningTable);
        
        lightningTable.hasNext = true;
        lightningTable.hasPrev = false;
        lightningTable.sObjectName = 'API_Request__c';
        lightningTable.pageSize = 10;
        lightningTable.objectFields = new List<String>{
        	'Name', 'API_Request_Id__c', 'Software_System__c', 'API_Type__c', 'OwnerId',
        	'Request_Reason__c', 'Customer_Name__c', 'Customer_Contact_Name__c', 'Owner_Approver_Name__c',
        	'Request_Status__c', 'Approval_Status__c', 'CreatedDate', 'Approver_Name__c',
        	'X3rd_party_Software__c', 'API_Key_Attachment_Id__c', 'Approver_Comment__c'
        };
        lightningTable.defaultFieldNames = new List<String>{
        	'Name', 'Account__c', 'Customer_Name__c', 'Software_System__c', 'API_Type__c',
             'X3rd_party_Software__c', 'Approval_Status__c'
        };
        lightningTable.defaultFieldValues = new List<String>{
        	'', salesAccount.Id, '', 'TEST', '',
             '', ''
        };
        lightningTable.startDate = startDate;
		lightningTable.endDate = endDate;
        lightningTable = Lightning_TableWithSearch.initializeTable(JSON.serialize(lightningTable));
        System.assertEquals(2, lightningTable.objectRecords.size());
        
        System.debug('----lightningTable--'+lightningTable);
        
        lightningTable.hasNext = false;
        lightningTable.hasPrev = true;
        lightningTable.sObjectName = 'API_Request__c';
        lightningTable.pageSize = 10;
        lightningTable.objectFields = new List<String>{
        	'Name', 'API_Request_Id__c', 'Software_System__c', 'API_Type__c', 'OwnerId',
        	'Request_Reason__c', 'Customer_Name__c', 'Customer_Contact_Name__c', 'Owner_Approver_Name__c',
        	'Request_Status__c', 'Approval_Status__c', 'CreatedDate', 'Approver_Name__c',
        	'X3rd_party_Software__c', 'API_Key_Attachment_Id__c', 'Approver_Comment__c'
        };
        lightningTable.defaultFieldNames = new List<String>{
        	'Name', 'Account__c', 'Customer_Name__c', 'Software_System__c', 'API_Type__c',
             'X3rd_party_Software__c', 'Approval_Status__c'
        };
        lightningTable.defaultFieldValues = new List<String>{
        	'', salesAccount.Id, '', 'TEST', '',
             '', ''
        };
        lightningTable.startDate = startDate;
		lightningTable.endDate = endDate;
        lightningTable = Lightning_TableWithSearch.initializeTable(JSON.serialize(lightningTable));
        System.assertEquals(10, lightningTable.objectRecords.size());
    }
    
    public static void insertAPIRequestData(){
    	insert new Regulatory_Country__c(
    		Name = 'USA'
    	);
    	
        salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        contacts = new List<Contact>();
        for(Integer counter=0;counter<11;counter++){
	        Contact con = TestUtils.getContact();
	        con.AccountId = salesAccount.Id;
	        contacts.add(con);
        }
        insert contacts;
        
        API_Key_Details__c apiScope1 = new API_Key_Details__c();
        apiScope1.Software_System__c = 'TEST';
        apiScope1.API_Types__c = 'APPROVAL';
        apiScope1.X3rd_Party_Software__c = 'NEEDED';
        apiScope1.Approval_Required__c = true;
        
        API_Key_Details__c apiScope2 = new API_Key_Details__c();
        apiScope2.Software_System__c = 'TESTING';
        apiScope2.API_Types__c = 'AUTO';
        apiScope2.X3rd_Party_Software__c = 'APPROVED';
        insert new List<API_Key_Details__c>{apiScope1, apiScope2};
        
        apiRequests = new List<Api_Request__c>();
        for(Contact con : contacts){
	        API_Request__c apiRequest1 = new API_Request__c();
	        apiRequest1.Account__c = salesAccount.Id;
	        apiRequest1.Contact__c = con.Id;
	        apiRequest1.Software_System__c = 'TEST';
	        apiRequest1.API_Type__c = 'APPROVAL';
	        apiRequest1.X3rd_Party_Software__c = 'NEEDED';
	        apiRequests.add(apiRequest1);
        }
        
        API_Request__c apiRequest2 = new API_Request__c();
        apiRequest2.Account__c = salesAccount.Id;
        apiRequest2.Contact__c = contacts[0].Id;
        apiRequest2.Software_System__c = 'TESTING';
        apiRequest2.API_Type__c = 'AUTO';
        apiRequest2.X3rd_Party_Software__c = 'APPROVED';
        apiRequests.add(apiRequest2);
        insert apiRequests;
    }
}
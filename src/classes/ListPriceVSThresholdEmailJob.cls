global class ListPriceVSThresholdEmailJob implements Schedulable {
    global void execute(SchedulableContext sc) {
        // fetch Data using SOQL from Salesforce
        List<PricebookEntry> pbEntries = [Select Id, Name, IsActive, UnitPrice, ThresholdVSListPrice__c, Product2.Name, Product2.Id,
            Product2.Threshold__c, Product2.Discountable__c, Product2.ProductCode, Product2.DASP__c,
            Product2.Booking_Discountable__c, Product2.Advantage_Credits_Required__c, Pricebook2.Name, Pricebook2.Id
            From PricebookEntry Where Product2.IsActive = true AND
            ThresholdVSListPrice__c > 0 AND UnitPrice <> 0 AND Product2.Threshold__c <> null
            AND Pricebook2.Name <> 'VA GOV'];

        //Set Header values of the file
        string csvHeader = 'Product Name, ProductCode, Discountable, Booking Discountable, Pricebook Name, List Price, Active(Price), Threshold, DASP, ThresholdVSListPrice, Advantage Credits Required \n';

        string mainContent = csvHeader;

        for (PricebookEntry pb: pbEntries){

            //Adding records in a string
            string productName = pb.Product2.name;
            if (productName.contains(',')) {
                productName = productName.replace(',', ' ');
            }
            string isDiscountable = pb.Product2.Discountable__c;
            string isBookingDiscountable = pb.Product2.Booking_Discountable__c;
            string advantageCreditReqd = String.valueOf(pb.Product2.Advantage_Credits_Required__c);
            string dasp = String.valueOf(pb.Product2.DASP__c);

            if (string.isBlank(isBookingDiscountable)) {
                isBookingDiscountable = '  ';
            }

            if (string.isBlank(isDiscountable)) {
                isDiscountable = '  ';
            }

            if (string.isBlank(advantageCreditReqd)) {
                advantageCreditReqd = '  ';
            }
            
            if (string.isBlank(dasp)) {
                dasp = '  ';
            }
            

            string recordString = productName + ',' + pb.Product2.ProductCode + ',' + isDiscountable + ',' + isBookingDiscountable + ',' + pb.Pricebook2.name + ',' + pb.UnitPrice + ',' + pb.isActive + ',' + pb.Product2.Threshold__c + ',' + dasp + ',' + pb.ThresholdVSListPrice__c + ',' + advantageCreditReqd + '\n';
            mainContent += recordString;

        }

        Messaging.EmailFileAttachment csvAttcmnt = new Messaging.EmailFileAttachment();

        //Create CSV file using Blob
        blob csvBlob = Blob.valueOf(mainContent);
        string csvname = 'ListPriceVSThreshold.csv';
        csvAttcmnt.setFileName(csvname);
        csvAttcmnt.setBody(csvBlob);
        Messaging.SingleEmailMessage singEmail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new list <string> {
            'Ajinkya.Raut@varian.com',            
            'Fiaz.Aziz@varian.com',
            'Naveen.Shetty@varian.com',
            'Raja.Minhas@varian.com',
            'Benson.Lew@varian.com',
            'Dinah.Neill@varian.com',
            'Georgia.Sorrell@varian.com',
            'Pauline.Nzeribe@varian.com',
            'Swetha.Dixit@varian.com',
            'Sam.Malchi@varian.com',
            'Paul.Haayer@varian.com',
            'Brian.Chan@varian.com'
        };

        //Set recipient list
        singEmail.setToAddresses(toAddresses);
        String subject = 'ListPrice VS Threshold Report';
        singEmail.setSubject(subject);
        singEmail.setPlainTextBody('PFA. ListPrice VS Threshold Report ');
        //Set blob as CSV file attachment
        singEmail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttcmnt});
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {singEmail});
    }
}
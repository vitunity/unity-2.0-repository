public with sharing class LocalItemContractControllerExtension {

	private final Local_Item_Contract__c lic;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public LocalItemContractControllerExtension(ApexPages.StandardController stdController) {
        this.lic = (Local_Item_Contract__c)stdController.getRecord();
    }

    
}
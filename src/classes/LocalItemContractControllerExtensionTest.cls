@isTest
public class LocalItemContractControllerExtensionTest{

public static Local_Item_Contract__c localItemContract;
public static SVMXC__Installed_Product__c installpro;
static{

    Account accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;
    
    Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
    installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023';
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
    
    localItemContract = new Local_Item_Contract__c();
    //localItemContract.Local_Item_Name__c = 'Eclipse1';
    localItemContract.Serial_Number__c = '2536';
    localItemContract.Varian_Machine__c = installpro.Id;
    localItemContract.Account_Name__c = accnt.id;
    //localItemContract.Local_Vendor__c = 'Dell International Services India Pvt.Ltd';
    
    insert localItemContract;
    
    
    
}

static testMethod void testmethod1()
{

ApexPages.StandardController sc = new ApexPages.StandardController(localItemContract);
LocalItemContractControllerExtension swctr = new LocalItemContractControllerExtension(sc);


}

}
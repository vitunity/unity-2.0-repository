/*
 * @author : Amitkumar Katre
 * @description :  1) Update FE Location based on location using group code from service team and 
 *                    erp district css code from location 
 */
public with sharing class LocationTriggerHandler {
    
    /* Populate FE Service Tema based on district CSS Code */
    public static void updateFEServiceTeam(list<SVMXC__Site__c> siteList, map<Id,SVMXC__Site__c> oldMap){
      set<String> eRPCSSDistrictSet = new set<String> ();
      for(SVMXC__Site__c siteObj : siteList){ 
          
          system.debug('siteObj.ERP_CSS_District__c === '+siteObj.ERP_CSS_District__c);
          
          if(siteObj.ERP_CSS_District__c != null &&
             (oldMap == null 
              || siteObj.ERP_CSS_District__c != oldMap.get(siteObj.Id).ERP_CSS_District__c
              || siteObj.FE_Service_Team__c == null
             )){
              eRPCSSDistrictSet.add(siteObj.ERP_CSS_District__c);     
          }
      }
      
      system.debug('eRPCSSDistrictSet === '+eRPCSSDistrictSet);
      
      if(!eRPCSSDistrictSet.isEmpty()){
          //This is service group technician record type id 
          String serviceGroupRecordTypeID = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Group__c').get('Technician');
          map<String,SVMXC__Service_Group__c > serviceGroupMap = new map<String,SVMXC__Service_Group__c >();
          for(SVMXC__Service_Group__c serviceTeamObj : [select Id, SVMXC__Group_Code__c from SVMXC__Service_Group__c 
                                                        where SVMXC__Group_Code__c in : eRPCSSDistrictSet
                                                        and RecordTypeId =: serviceGroupRecordTypeID]){
              serviceGroupMap.put(serviceTeamObj.SVMXC__Group_Code__c,serviceTeamObj);    
          }
          for(SVMXC__Site__c siteObj : siteList){ 
              if(serviceGroupMap.containsKey(siteObj.ERP_CSS_District__c)){
                  siteObj.FE_Service_Team__c = serviceGroupMap.get(siteObj.ERP_CSS_District__c).Id;
              }
          }       
      }
    }
}
public class Lookup {
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text,String fld_API_Account,String fld_API_AccntName, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText ){
        
       system.debug('==objectName=='+objectName); 
       system.debug('==fld_API_Text=='+fld_API_Text);
       system.debug('==fld_API_Account=='+fld_API_Account);
       system.debug('==fld_API_AccntName=='+fld_API_AccntName);
       system.debug('==fld_API_Val=='+fld_API_Val);
                                      
                                      
        searchText='\'' + String.escapeSingleQuotes(searchText.trim()) + '%\'';

        
        
        String query = 'SELECT '+fld_API_Text+',' + fld_API_Account + ','+fld_API_AccntName+',' +fld_API_Val+' FROM '+objectName+
            				' WHERE '+fld_API_Search+' LIKE '+searchText+' AND Inactive_Contact__c = FALSE LIMIT '+lim;
            			
        /*String query =     'SELECT '+fld_API_AccntName+' FROM '+objectName1+' WHERE '+fld_API_Account+' =: (SELECT '+fld_API_Account+' FROM '+objectName+
            				' WHERE '+fld_API_Search+' LIKE '+searchText+ 
            			' LIMIT '+lim+')';    */          
         
            
             
        
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.name = String.valueOf(s.get(fld_API_Text)) ;
            obj.accountId=(String)(s.get(fld_API_Account));
            obj.accountName=(String)(s.get(fld_API_AccntName));

            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            lstRet.add(obj);
        }
         return JSON.serialize(lstRet) ;
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String name{get;set;}
        Public String accountId{get;set;}
        public String accountName{get;set;}
        public String val{get;set;}
    }

}
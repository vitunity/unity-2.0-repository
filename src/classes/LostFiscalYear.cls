public class LostFiscalYear{
    
    public static string getQuarter(list<period> lstPeriod,Date oDate){
        for(Period p: lstPeriod){
            if(oDate >= p.StartDate && oDate <= p.endDate){
                return 'Q'+p.Number; 
            }  
             
        }
        return 'Not Found';
     
    }
    public static string getYear(List<Fiscalyearsettings> lstFYear,Date oDate){
        for(Fiscalyearsettings f: lstFYear){
            if(oDate >= f.StartDate && oDate <= f.endDate){
                return f.Name;
            }  
             
        }
        return 'Not Found';
     
    }
    public static void setLostTime(map<Id,Opportunity> mapNew, map<Id,Opportunity> mapOld){
        
        map<Id,Opportunity> mapLost = new map<Id,Opportunity>();
        for(Opportunity opp : mapNew.values()){
            if(opp.Date_Lost__c <> null || opp.CloseDate <> null ){
                mapLost.put(opp.Id,opp);                
            }
        }
        if(mapLost.size()>0){
            List<Fiscalyearsettings> lstFiscalSetting = [select Name,startDate,EndDate,(select StartDate,EndDate,IsForecastPeriod,Number,PeriodLabel,QuarterLabel,Type from periods where  type = 'Quarter') from Fiscalyearsettings];
            
            map<string,List<period>>  mapFiscalPeriod = new map<string,List<period>>();
            
            for(Fiscalyearsettings f : lstFiscalSetting){
                mapFiscalPeriod.put(f.Name,f.periods);
            }
            
            List<Opportunity> lstOppUpdate = new List<Opportunity>();
            for(Opportunity opp : [select Id,Date_Lost__c,CloseDate,F_Time_Of_Loss_Fiscal_Year__c,F_Time_Of_Loss_Fiscal_Quarter__c,F_Fiscal_Year__c,F_Fiscal_Quarter__c from opportunity where Id IN: mapLost.keyset()]){
                opp.F_Time_Of_Loss_Fiscal_Year__c = getYear(lstFiscalSetting,opp.Date_Lost__c);
                opp.F_Fiscal_Year__c = getYear(lstFiscalSetting,opp.CloseDate);
                if(mapFiscalPeriod.containsKey(opp.F_Time_Of_Loss_Fiscal_Year__c)){
                    opp.F_Time_Of_Loss_Fiscal_Quarter__c = getQuarter(mapFiscalPeriod.get(opp.F_Time_Of_Loss_Fiscal_Year__c),opp.Date_Lost__c);
                }else{
                    opp.F_Time_Of_Loss_Fiscal_Quarter__c = 'Not Found';
                }
                if(mapFiscalPeriod.containsKey(opp.F_Fiscal_Year__c)){
                    opp.F_Fiscal_Quarter__c = getQuarter(mapFiscalPeriod.get(opp.F_Fiscal_Year__c),opp.CloseDate);
                }else{
                    opp.F_Fiscal_Quarter__c = 'Not Found';
                }
                lstOppUpdate .add(opp);
            }
            
            if(lstOppUpdate.size()>0)update lstOppUpdate ;
        }
        
    }
}
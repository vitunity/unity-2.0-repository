@isTest

Public class LostFiscalYear_Test {

    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'My Varian Test Account';
        acc.BillingCountry='USA';
        acc.Country__c= 'USA';
        acc.BillingCity= 'Los Angeles';
        acc.BillingStreet ='3333 W 2nd St';
        acc.BillingState ='CA';
        acc.BillingPostalCode = '90020';
        acc.Agreement_Type__c = 'Master Pricing';
        acc.OMNI_Address1__c ='3333 W 2nd St';
        acc.OMNI_City__c ='Los Angeles';
        acc.OMNI_State__c ='CA';
        acc.OMNI_Country__c = 'USA';  
        acc.OMNI_Postal_Code__c = '90020';

        insert acc;  
        
        Contact con = new Contact();
        con.FirstName = 'testfirst';
        con.LastName = 'testLast';
        con.Accountid = acc.id;
        con.email = 'test@varian.com';
        con.MobilePhone = '1234567891';
        con.Phone = '1234567891';
        con.MailingCity= 'Los Angeles';
        con.MailingStreet ='3333 W 2nd St';
        con.MailingState ='CA';
        con.MailingPostalCode = '90020';
        con.MailingCountry = 'USA';
        
        insert con;

                
        Opportunity opp = new Opportunity();
        opp.Name = 'Varian Test';
        opp.Date_Lost__c = system.today();
        opp.Accountid = acc.id;
        opp.Primary_Contact_Name__c = con.id;
        opp.StageName = '30%';
        opp.Unified_Probability__c= '40%';
        opp.Unified_Funding_Status__c = '60%';
        opp.CloseDate = system.today();
        opp.Amount = 50000;
        
        insert opp;

   }
     
}
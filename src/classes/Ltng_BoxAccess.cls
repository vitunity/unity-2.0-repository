/*
* Author: Nilesh Gorle
* Created Date: 25-August-2018
* Project/Story/Inc/Task : Unity/Box Integration for Lightning
* Description: 
* Contains method 
*   To generate accesstoken - generateAccessToken(custom Setting Obejct)
*   To get Box Private Key - getBoxPrivateKey()
*   To get Box Credential from Custom Setting - getBoxCredential()
*   To get Okta Owner email using ownerId - getOktaOwnerEmail(Id OwnerId)
*   To get PHI log record list using recordId - fetchPHILogRecord(Id recordId)
*   To get User record - getUserRec(userId)
*   To show button if user is authorised - checkPermission(PhiLogId)
*/
public class Ltng_BoxAccess {
    public static string prefixString = 'PHI-';

    public static Map<String, String> generateAccessToken() {
        Map<String, String> resultMap = new Map<String, String>();
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        String accessToken;
        String expires;
        Boolean isUpdate=false;

        BoxJwtEncryptionPreferences a1;
        BoxPlatformApiConnection con;
        try {
            Boolean isAccessTokenValid = false;
            System.debug('---------Check1----');
            if(boxCRecord.Access_Token__c != null && boxCRecord.Access_Token_Expires_In__c != null) {
                System.debug('---------Check2----');
                datetime currentDateTime = datetime.now();
                datetime issueDatetime = boxCRecord.Access_Token_Issue_Datetime__c;
                decimal milSecInDiff = decimal.valueOf(currentDateTime.getTime() - issueDatetime.getTime());
                // will be creating new token 5 seconds before expires, if access token is almost going to be expired
                decimal expiredInMilSec = (boxCRecord.Access_Token_Expires_In__c-5);

                if(currentDateTime > issueDatetime) {
                    if(expiredInMilSec > milSecInDiff) {
                        isAccessTokenValid = true;
                        accessToken = boxCRecord.Access_Token__c;
                        expires = String.valueOf(boxCRecord.Access_Token_Expires_In__c);
                    } else {
                        isAccessTokenValid = false;
                    }
                }
            }

            if(!isAccessTokenValid || boxCRecord.Access_Token__c == null) {
                System.debug('---------Check2----');
                a1 = new BoxJwtEncryptionPreferences();
                a1.setPublicKeyId(boxCRecord.Box_Public_Key__c);
                a1.setPrivateKey(getBoxPrivateKey());
                a1.setEncryptionAlgorithm(BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256);
                a1.setPrivateKeyPassword(boxCRecord.Box_Private_Key_Password__c);
                con =  BoxPlatformApiConnection.getAppEnterpriseConnection(boxCRecord.Enterprise_Id__c, 
                                                                            boxCRecord.Client_Key__c, 
                                                                            boxCRecord.Client_Secret_Key__c, 
                                                                            a1);
                accessToken = con.accessToken;
                expires = String.valueOf(con.expires);
                isUpdate = true;
            }
            system.debug('@@@ACCESS TOKEN0-----'+accessToken);

            resultMap.put('accessToken',accessToken);
            resultMap.put('expires',expires);
            resultMap.put('isUpdate',String.valueOf(isUpdate));
            
            return resultMap;
            
        } catch(System.SecurityException e) {
            System.debug('Print Exception==========='+String.valueof(e));
            System.debug('Invalid credential. Please, verify the credential for box app.');
            return null;
        }
    }

    /*
     *  Custom setting object not able to update from this class, as we are calling Box Rest API to generate access token  
     *  and then we immeadiately calling another API. Both are Async method.
     */
    public static void updateBoxAccessToken(String accessToken, string expires, String isUpdate) {
        Boolean updateRec = Boolean.valueOf(isUpdate);
        if(updateRec) {
            Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
            boxCRecord.Access_Token__c = accessToken;
            boxCRecord.Access_Token_Expires_In__c = long.valueOf(expires);
            boxCRecord.Access_Token_Issue_Datetime__c = datetime.now();
            update boxCRecord;
        }
    }

    public static String getBoxPrivateKey() {
        StaticResource srObject = [select id,body from StaticResource Where Name = 'Box_Private_Key'];
        String contents = srObject.body.toString();
        String Box_Private_Key = '';
        for(String line:contents.split('\n')) {
          Box_Private_Key += line;
        }
        System.debug('---------Box_Private_key-------'+Box_Private_Key);
        if(Box_Private_Key=='') {
            throw new StringException('Invalid Box Private Key. Please, check static resource box private file.');
        }
        return Box_Private_Key;
    }
    
    public static Box_Credential__c getBoxCredential() {
        List<Box_Credential__c> bcList = [SELECT id, Name, Access_Token__c, Access_Token_Expires_In__c, 
                                          Access_Token_Issue_Datetime__c, Box_Public_Key__c, 
                                          Box_Private_Key_Password__c, Enterprise_Id__c, Client_Key__c, 
                                          Client_Secret_Key__c, Folder_Path__c   
                                          FROM Box_Credential__c Where Active__c=true Limit 1];
        System.debug('----------BOX------'+bcList);
        Box_Credential__c boxCRecord;
        if(!bcList.isEmpty()) {
            boxCRecord = bcList[0];
            system.debug('----------'+boxCRecord);
            return boxCRecord;
        }
        return null;
    }

    //get PHI Log Folder Name
    public static string getPHIFolderName(PHI_Log__c phiobj){
        string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
        string caseNumber = (phiobj.Case_Number__c <> null)?'_'+phiobj.Case_Number__c:'';
        return prefixString+phiobj.Name+caseNumber+accountName;
    }
    //get Case Folder Name
    public static string getCaseFolderName(PHI_Log__c phiobj){        
        if(phiobj.Case_Number__c <> null){
            string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
            string pcsnName = (phiobj.Case__r.PCSN__c <> null)?'_'+phiobj.Case__r.PCSN__c:'';
            return phiobj.Case_Number__c+accountName+pcsnName;
        }
        if(phiobj.Account__c <> null){
            return phiobj.Account__r.Name;
        }     
        return label.DEFAULT_BOX_FOLDER;        
    }

    //get PHI Log Folder Name
    // Log Number_Complaint Number_Account Name
    public static string getComplaintPHIFolderName(PHI_Log__c phiobj) {
        string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
        String complaintNumber = (phiobj.Complaint__c <> null)?'_'+phiobj.Complaint__c:'';
        return prefixString+phiobj.Name+complaintNumber+accountName;            
    }

    //get Complaint Case Folder Name
    //CP-2018-00248_1537301_Ajou University (<Complaint Number>_Case Number_Account Name
    public static string getComplaintCaseFolderName(PHI_Log__c phiobj){        
        if(phiobj.Case_Number__c <> null && phiobj.Complaint__c <> null){
            String complaintNumber = (phiobj.Complaint__c <> null)?phiobj.Complaint__c:'';
            string caseNumber = (phiobj.Case_Number__c <> null)?'_'+phiobj.Case_Number__c:'';
            string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
            return complaintNumber+caseNumber+accountName;
        } 
        return phiobj.Complaint__c;        
    }

    public static String getOktaOwnerEmail(Id OwnerID) {
        List<User> usrList = [Select Id, Email, Okta_Email__c From User Where Id=: OwnerID];
        if(!usrList.isEmpty()) {
            //if(usrList[0].Okta_Email__c!=null) {
            //    return usrList[0].Okta_Email__c;
            //}
            return usrList[0].Email;
        }
        return null;
    }

    public static Set<String> getUserIdList(Set<String> aliasLst) {
        List<User> usrList = [Select Id, Email From User Where Alias in: aliasLst];
        Set<String> usrSet = new Set<String>();
        if(!usrList.isEmpty()) {
            for(User usr:usrList) {
                usrSet.add(String.valueOf(usr.Id));
            }
            return usrSet;
        }
        return null;
    }

    public static List<PHI_Log__c> fetchPHILogRecord(String recordId) {
        System.debug('------RecordID'+recordId);
        List<PHI_Log__c> phiLogList = [select id,Name,Storage_Location2__c,OwnerId,Case_Number__c,Server_Location__c, 
                                       Collab_id__c,Folder_Id__c,Okta_Email__c,Owneres_Name__c,Case_Folder_Id__c,  
                                       Case__c,Data_Security_Details__c,Purpose_of_PHI__c, User_id_s_of_owners__c,
                                       Complaint__c,Account__r.Name,Case__r.PCSN__c, Account__c, Current_Owners__c,  
                                       Disposition_Date__c, Owners_Box_Email__c, Populated_UserID__c, BoxFolderName__c,  
                                       IsFolderMovedToECT__c from PHI_Log__c where Id=:recordId];
        if(!phiLogList.isEmpty()) {
            System.debug('------phiLogList'+phiLogList);
            return phiLogList;
        }
        return null;
    }

    public static List<PHI_Log__c> fetchAllPHILogRecord(List<String> recordIdList) {
        System.debug('------recordIdList'+recordIdList);
        List<PHI_Log__c> phiLogList = [select id,Name,Storage_Location2__c,OwnerId,Case_Number__c,Server_Location__c, 
                                       Collab_id__c,Folder_Id__c,Okta_Email__c,Owneres_Name__c,Case_Folder_Id__c,  
                                       Case__c,Data_Security_Details__c,Purpose_of_PHI__c, User_id_s_of_owners__c,
                                       Complaint__c,Account__r.Name,Case__r.PCSN__c, Account__c, Current_Owners__c, 
                                       Disposition_Date__c, Owners_Box_Email__c, Populated_UserID__c, BoxFolderName__c,   
                                       IsFolderMovedToECT__c from PHI_Log__c where Id in: recordIdList];

        if(!phiLogList.isEmpty()) {
            System.debug('------phiLogList'+phiLogList);
            return phiLogList;
        }
        return null;
    }

    public static User getUserRec(String recordId) {
        if(recordId!=null) {
            return [Select Id, Name, Email From User Where Id=:recordId Limit 1];
        }
        system.debug('-----User Record Not Found--------');
        return null;
    }

    public static Boolean checkPermission(string recordId){
        String sResult = '0';
        
        PHI_Log__c oPHILog;
        set<string> setUserIds = new set<string>();
        List<PHI_Log__c> philstc = fetchPHILogRecord(recordId);
        if(!philstc.isEmpty()) {
            oPHILog = philstc[0];
        } else {
            system.debug('-----Record not found-------');
            return false;
        }

        Map<String,String> resultMap = generateAccessToken();
        String accessToken;
        String expires;
        String isUpdate;
        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }
        //String accessToken = generateAccessToken();

        System.debug('---------ACCESS TOKEN--------'+accessToken);
        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        BoxFolder folder = new BoxFolder(api, oPHILog.Case_Folder_Id__c);
        list<BoxCollaboration.Info> collaborations;
        try {
            collaborations = folder.getCollaborations();
        } catch(Exception e) {
            system.debug('Exception: Folder does not exist---'+String.valueof(e));
            return false;
        }
        Set<String> aliasLst = new Set<String>();
        
        for (BoxCollaboration.Info i : collaborations) {
            if(i.accessibleBy <> null){
                String email = i.accessibleBy.children.get('login');
                if(string.valueOf(i.role) == 'CO_OWNER' && email != null) {
                    aliasLst.add(email.split('@')[0]);
                }
            }
        }
        
        updateBoxAccessToken(accessToken, expires, isUpdate);

        System.debug('-----aliasLst----'+aliasLst);
        if(!aliasLst.isEmpty()) {
            Set<String> usrIdLst = getUserIdList(aliasLst);
            if(usrIdLst != null)
                setUserIds.addall(usrIdLst);
        }

        string sUserGroupId = oPHILog.OwnerId;
        setUserIds.add(sUserGroupId);
        if(sUserGroupId.startsWith('00G') && oPHILog.Disposition2__c != 'Destroyed') {
            for(GroupMember oGroupMember:[Select UserOrGroupId, GroupId From GroupMember where GroupId=:sUserGroupId]){
                setUserIds.add(oGroupMember.UserOrGroupId);
            }
        }

        system.debug('setUserIds:'+setUserIds);
        system.debug('userinfo.getuserid():'+userinfo.getuserid());
        if(setUserIds.contains(userinfo.getuserid())) {
            return true;
        }
        system.debug('Not Authorised');
        return false;
    }

    public static String createBoxFolder(String accessToken, String folderName, String parentId) {
        try{
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders' ;
            req.setEndpoint(endpoint);
            string body = '{"name":"'+folderName+'", "parent": {"id": "'+parentId+'"}}';
            req.setBody(body);
            res = http.send(req);
            String jasonresp = res.getBody();
            Integer statusCode = (Integer) res.getstatuscode();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug('JsonRP---------'+jasonresp);
            System.debug('folderName---------'+folderName);
            System.debug('parentId---------'+parentId);
            System.debug('statusCode---------'+statusCode);
    
            if(statusCode==201) {
                return String.valueOf(jsonObj.get('id'));
            }
    
            if(statusCode==409) {
                System.debug('--------------Folder Already Exist----');
                String folderId = getBoxFolderId(accessToken, folderName, parentId);
                return folderId;
            }
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
        return null;
    }
    
    public static string getBoxFolderId(String accessToken, String caseFolderName, String parentId) {
        try{
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/'+parentId ;
            req.setEndpoint(endpoint);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            System.debug('JasonP-----------------'+jasonresp);
            System.debug('item_collection-----------------'+jsonObj.get('item_collection'));
    
            Map<String, Object> entryListObj = (Map<String, Object>) jsonObj.get('item_collection');
            System.debug('entryListObj-----------------'+entryListObj);
            String etag;
            String folderId;
            String folderName;
            boolean isFound = false;
    
            for(String key:entryListObj.keyset()) {
                system.debug('------------'+entryListObj.get(key));
                if(key=='entries') {
                    List<Object> entryMap = (List<Object>) entryListObj.get(key);
                    for(Object k:entryMap) {
                        Map<String, Object> nodes = (Map<String, Object>) k;
                        etag = (String) nodes.get('etag');
                        folderId = (String) nodes.get('id');
                        folderName = (String) nodes.get('name');
                        if(etag=='0' && folderName==caseFolderName) {
                            isFound = true;
                            break;
                        }
                    }
                    if(isFound) {
                        break;
                    }
                }
            }
    
            return folderId;
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
        return null;
    }
    
    public static void deleteBoxFolder(String accessToken, String folderId) {
        try{
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('DELETE');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/'+folderId+'?recursive=true';
            req.setEndpoint(endpoint);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            System.debug('Response-----------------'+jasonresp);
            System.debug('Deleted Folder Id-----------------'+folderId);
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
    }

    public static String copyBoxFolder(String accessToken, String caseFolderId, String parentFolderId) {
        try{
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/'+caseFolderId+'/copy'; 
            //'https://api.box.com/2.0/folders/'+folderId;
            req.setEndpoint(endpoint);
            string body =  '{"parent": {"id" : "'+parentFolderId+'"}}';
            req.setBody(body);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            System.debug('Response-----------------'+jasonresp);
            //Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            String resFolderId = (String) jsonObj.get('id');
            return resFolderId;
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
        return null;
    }
    
    public static void deleteCaseFolder(String accessToken, String folderId) {
        String jasonresp = getFolderItemsInfo(accessToken, folderId);
        if(jasonresp != null) {
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug('JasonP-----------------'+jasonresp);
            System.debug('item_collection-----------------'+jsonObj.get('item_collection'));
    
            Map<String, Object> entryListObj = (Map<String, Object>) jsonObj.get('item_collection');
            Integer total_count = (Integer) entryListObj.get('total_count');
            if(total_count == 0) {
                deleteBoxFolder(accessToken, folderId);
            }
        }
    }
    
    public static void checkAndDeleteFolder(String accessToken, String phiFolderName, String parentId) {
        String jasonresp = getFolderItemsInfo(accessToken, parentId);
        if(jasonresp != null) {
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug('JasonP-----------------'+jasonresp);
            System.debug('item_collection-----------------'+jsonObj.get('item_collection'));
    
            Map<String, Object> entryListObj = (Map<String, Object>) jsonObj.get('item_collection');
            Integer total_count = (Integer) entryListObj.get('total_count');
            if(total_count != 0) {
                for(String key:entryListObj.keyset()) {
                    if(key=='entries') {
                        List<Object> entryMap = (List<Object>) entryListObj.get(key);
                        for(Object k:entryMap) {
                            Map<String, Object> nodes = (Map<String, Object>) k;
                            String folderId = (String) nodes.get('id');
                            String folderName = (String) nodes.get('name');
                            if(folderName==phiFolderName) {
                                // delete folderId
                                deleteBoxFolder(accessToken, folderId);
                                if(total_count == 1)
                                    // delete parentId folder
                                    deleteBoxFolder(accessToken, parentId);
                                break;
                            }
                        }
                    }
                }
                
            } else {
                // delete parentId folder, as it does not contain any files or folders
                deleteBoxFolder(accessToken, parentId);
            }   
        }
    }
    
    public static String getFolderItemsInfo(String accessToken, String parentId) {
        try {
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/folders/'+parentId;
            req.setEndpoint(endpoint);
            res = http.send(req);
            String jasonresp = res.getBody();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug(res.getstatuscode());
            return jasonresp;
        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
        return null;
    }

    public static String checkCollabExistForFolder(BoxFolder folder, String userName, string userEmail) {
        string collabId;
        System.debug('folder-----------------'+folder);
        System.debug('userName-----------------'+userName);
        System.debug('userEmail-----------------'+userEmail);
        try {
            list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
            System.debug('collaborations-----------------'+collaborations);
            for (BoxCollaboration.Info i : collaborations) {
                if(userName == null && i.accessibleBy <> null && i.accessibleBy.children.get('login') == userEmail) {
                    System.debug('-------Email Match-------');
                    collabId = String.valueOf(i.id);
                    break;
                } else if(userName != null && i.accessibleBy <> null && userName == i.accessibleBy.Name){
                    System.debug('-------Name Match-------');
                    collabId = String.valueOf(i.id);
                    break;
                }
            }
        } catch(Exception e) {
            system.debug('Exception--------'+String.valueOf(e));
        }
        System.debug('collabId--------'+collabId);
        return collabId;
    }
    
    public static void assignBoxGroup(String accessToken, String folderId) {
        try {
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/collaborations';
            req.setEndpoint(endpoint);
            string body = '{"item": { "id": "'+folderId+'", "type": "folder"}, "accessible_by": { "id": "'+label.Box_Group_Id+'", "type": "group" }, "role": "co-owner"}';
            req.setBody(body);
            res = http.send(req);
            String jasonresp = res.getBody();
            Integer statusCode = (Integer) res.getstatuscode();
            Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
            System.debug('jsonObj---------'+jsonObj);
            System.debug('statusCode---------'+statusCode);
        } catch(Exception e) {
            system.debug('##Exception'+e.getMessage());
        }
    }

    public static void createPHIHistoryLog(String logDetails, String phiLogRecId, String objName) {
        try {
            Field_Audit_Trail__c fhaObj = new Field_Audit_Trail__c();
            fhaObj.CreatedDate__c = System.today();
            fhaObj.CreatedByID__c = UserInfo.getUserId();
            fhaObj.Field__c = logDetails;
            fhaObj.sObjectID__c = phiLogRecId;
            fhaObj.SObjectAPIName__c = objName;
            insert fhaObj;
        } catch(Exception e) {
            System.debug('Log Exception======='+String.valueOf(e));
        }
    }

    public static List<String> retrieveFolderCollabIdList(String folderId) {
        List<String> collabIdList = new List<String>();
        Map<String,String> resultMap = generateAccessToken();

        if(resultMap != null) {
            String accessToken = resultMap.get('accessToken');
            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            BoxFolder folder = new BoxFolder(api, folderId);
            list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
            for (BoxCollaboration.Info i : collaborations) {
                System.debug('i-------------'+i);
                if(i.accessibleBy <> null) {
                    collabIdList.add(String.valueOf(i.id));
                }
            }
        }

        return collabIdList;
    }

    public static List<String> getECTQueueEmailList() {
        List<Group> grpList = [select Id, Name from Group where  Name =: label.Box_Queue_Name Limit 1];
        if(!grpList.isEmpty()) {
            List<GroupMember> groupMembers = [Select UserOrGroupId From GroupMember where GroupId =: grpList[0].Id];
            if(!groupMembers.isEmpty()) {
                List<String> userIdList = new List<String>();
                List<String> userEmailList = new List<String>();
                for(GroupMember gm:groupMembers) {
                    userIdList.add(gm.UserOrGroupId);
                }
                List<User> usrList = [Select email from User where Id in: userIdList];
                for(User usr:usrList) {
                    userEmailList.add(usr.Email);
                }
                return userEmailList;
            }
        }
        return null;
    }

    public static void inviteUser(String accessToken, String userEmail) {
        try {
            http http=new http();
            HTTPResponse res=new HttpResponse();
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+accessToken); 
            String endpoint = 'https://api.box.com/2.0/invites';
            req.setEndpoint(endpoint);
            string body = '{ "enterprise" : { "id" : "'+label.Box_enterprise_Id+'" } , "actionable_by" : { "login" : "'+userEmail+'" } }';
            req.setBody(body);
            res = http.send(req);
            Integer statusCode = (Integer) res.getstatuscode();
            String jasonresp = res.getBody();
            if(statusCode==200 || statusCode < 400) {
                System.debug('Invitation succesfully sent!!!');
            } else {
                System.debug('Error: '+jasonresp);
            }

        } catch(Exception e) {
            system.debug('@@@exception'+e.getMessage());
        }
    }
}
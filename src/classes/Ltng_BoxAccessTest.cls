@isTest
public class Ltng_BoxAccessTest {

	private static String accessToken = 'accesstoken';
    private static String refreshToken = 'refreshtoken';
    private static String clientId = 'clientid';
    private static String clientSecret = 'clientsecret';
    private static String authCode = 'authcode';
    private static String entityId = 'entityid';
    private static String userId = 'userid';
    private static String enterpriseId = 'enterpriseid';
    private static String publicKeyId = 'publicKeyId';
    private static String privateKey = BoxTestJsonResponseFactory.AUTH_PRIVATE_KEY;
    private static String privateKeyPassword = 'privateKeyPassword';
    private static BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm = BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256;

	testMethod static void testGenerateAccessToken(){
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

        test.startTest();        
        Ltng_BoxAccess.generateAccessToken();
        test.stopTest();    
    }
    
	testMethod static void testGenerateAccessTokenUpdate(){
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

        test.startTest();        
        Ltng_BoxAccess.generateAccessToken();
        Ltng_BoxAccess.updateBoxAccessToken( accessToken, '36000000', 'true');
        test.stopTest();    
    }

	private static BoxJwtEncryptionPreferences constructEncryptionPrefs() {
		BoxJwtEncryptionPreferences prefs = new BoxJwtEncryptionPreferences();
		prefs.setEncryptionAlgorithm(algorithm);
		prefs.setPrivateKey(privateKey);
		prefs.setPrivateKeyPassword(privateKeyPassword);
		prefs.setPublicKeyId(publicKeyId);
		return prefs;
	}
    
    testMethod static void testUserMethods(){
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        Profile pr = [Select id from Profile where name = 'System Administrator'];
        User u = new user(alias = 'standt', email='standardtestusqq2@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standarqqqdtestuse92@testclass.com');
        insert u;

		test.startTest();
		Ltng_BoxAccess.getOktaOwnerEmail(u.Id);
        Ltng_BoxAccess.getOktaOwnerEmail('005E0100005w1hY');
        
        Ltng_BoxAccess.getUserRec(u.Id);
        Ltng_BoxAccess.getUserRec(null);
        
        Set<String> aliasLst = new Set<String>();
        aliasLst.add('standt');
        Set<String> aliasLst1 = new Set<String>();
        aliasLst1.add('005E0100005w1hY');
        Ltng_BoxAccess.getUserIdList(aliasLst);
        Ltng_BoxAccess.getUserIdList(aliasLst1);
        test.stopTest();
    }

    testMethod static void testPHILogMethods(){
        
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
        insert phi;
        
        test.startTest();
        Ltng_BoxAccess.getPHIFolderName(phi);
        Ltng_BoxAccess.getCaseFolderName(phi);
        Ltng_BoxAccess.getComplaintPHIFolderName(phi);
        Ltng_BoxAccess.getComplaintCaseFolderName(phi);
        
        
        Ltng_BoxAccess.fetchPHILogRecord(phi.Id);
        Ltng_BoxAccess.fetchPHILogRecord('a7Rm00000000VPO');
        
        List<String> phiList = new List<String>();
        phiList.add(phi.Id);
        List<String> phiList1 = new List<String>();
        phiList1.add('a7Rm00000000VPO');
        Ltng_BoxAccess.fetchAllPHILogRecord(phiList);
        Ltng_BoxAccess.fetchAllPHILogRecord(phiList1);
        
        Ltng_BoxAccess.checkPermission(phi.Id);
        Ltng_BoxAccess.inviteUser(accessToken, 'nilesh.gorle@varian.com');
        test.stopTest();
    }

    testMethod static void testECTQueueEmailList() {
        test.startTest();
		Group g1 = new Group(Name='ECT_Investigation_Group', type='Queue');
		insert g1;
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'PHI_Log__c');
        insert q1;
		GroupMember member = new GroupMember();
		member.UserOrGroupId = UserInfo.getUserId();
		member.GroupId = g1.Id;
        insert member;
        Ltng_BoxAccess.getECTQueueEmailList();
        test.stopTest();
    }
    
    testMethod static void testPHILogFolder(){
        
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
        insert phi;
        
    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));
        
    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
    	
    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
        BoxFolder folder1 = new BoxFolder(api, '11446498');
    	
    	Test.startTest();
    	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR; 
    	BoxCollaboration.Info newCollab = folder.collaborate('nilesh.gorle@varian.com', collabRole);

        BoxFolder.Info createdFolderInfo = folder.createFolder('Pictures1245');
        
		phi.Case_Folder_Id__c = '11446498';
        phi.Folder_Id__c = '11446500';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/11446500';
        phi.BoxFolderName__c = 'Testing';
		phi.Collab_id__c = '791293';
        update phi;

        Ltng_BoxAccess.checkPermission(phi.Id);        
        Ltng_BoxAccess.createBoxFolder(accessToken, 'Test', '0');
        Ltng_BoxAccess.getBoxFolderId(accessToken, 'Pictures1245', '11446500');
        Ltng_BoxAccess.getFolderItemsInfo(accessToken, '11446500');
        Ltng_BoxAccess.copyBoxFolder(accessToken, '11446498', '11446500');
        Ltng_BoxAccess.checkCollabExistForFolder(folder1, 'sean rose', 'nilesh.gorle@varian.com');
        Ltng_BoxAccess.checkCollabExistForFolder(folder1, null, 'nilesh.gorle@varian.com');
        Ltng_BoxAccess.checkAndDeleteFolder(accessToken, 'Test', '11446500');
        Ltng_BoxAccess.deleteBoxFolder(accessToken, '11446498');
        Ltng_BoxAccess.deleteCaseFolder(accessToken, '11446498');
        test.stopTest();
    }

    testMethod static void testPHILogFolder1(){
        
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
        insert phi;

    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));

    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));

    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
        BoxFolder folder1 = new BoxFolder(api, '11446498');

    	Test.startTest();
    	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR;
    	BoxCollaboration.Info newCollab = folder.collaborate('nilesh.gorle@varian.com', collabRole);
        Ltng_BoxAccess.retrieveFolderCollabIdList('11446498');
        Ltng_BoxAccess.assignBoxGroup(accessToken, '11446498');
        Ltng_BoxAccess.createPHIHistoryLog('Testing PHI Log', phi.Id, 'PHI_Log__c');
        Ltng_BoxAccess.getCaseFolderName(phi);
        test.stopTest();
    }
    
    testMethod static void testPHILogFolder2() {
        
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

		BoxJwtEncryptionPreferences encryptionPref = constructEncryptionPrefs();
		String authResponse = BoxTestJsonResponseFactory.AUTH_APP_USER_TOKEN;
		Test.setMock(HttpCalloutMock.class, (new BoxTestMockCallout(authResponse, 'OK', 200)));

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Complaint__c= 'CP-321-3228';
        phi.Log_Type__c='Case';
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
        insert phi;

    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));

    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));

    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
        BoxFolder folder1 = new BoxFolder(api, '11446498');

        test.startTest();
		Ltng_BoxAccess.getCaseFolderName(phi);
        Ltng_BoxAccess.getComplaintCaseFolderName(phi);
        test.stopTest();
    }
    
    testMethod static void testPHILogFolder3() {
		Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

      	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"sean@box.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"sean+test@box.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"0","etag":"0","name":"Shared Pictures"}}';
      	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
		BoxApiConnection api = new BoxApiConnection('accesstoken');
		BoxFolder folder = new BoxFolder(api, '11446500');

        test.startTest();

      	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR; 
      	BoxCollaboration.Info newCollab = folder.collaborate('nileshgorle@gmail.com', collabRole);
      	BoxCollaboration.Role collabRole1 = BoxCollaboration.Role.CO_OWNER; 
      	BoxCollaboration.Info newCollab1 = folder.collaborate('nilesh.gorle@varian.com', collabRole1);        
        
        Ltng_BoxAccess.checkCollabExistForFolder(folder, 'Nilesh', 'nilesh.gorle@varian.com');
        //Ltng_BoxAccess.checkAndDeleteFolder(accessToken, 'Shared Pictures', '0');
        test.stopTest();
    }
}
global class Ltng_BoxChangeOfOwnership {
    public static void updateBoxOwner(List<PHI_Log__c> newPhiList, Map<Id, PHI_Log__c> oldPhiMap) {
        String phiIdString = '';
        for(PHI_Log__c phi : newPhiList) {
            if(oldPhiMap != null && oldPhiMap.containsKey(phi.Id) && 
              	phi.OwnerId != oldPhiMap.get(phi.Id).OwnerId) {
				phiIdString += (String.valueOf(phi.Id) + ',');
            }
        }
        if(phiIdString != null)
            changeowner(phiIdString);
    }
    
    @Future(callout=true)
	public static void changeowner(String phiIdString) {
        List<String> phiIdList = phiIdString.split(',');

        System.debug('---------phiIdList--------'+phiIdList);
        String ownerid;
        String csfldr;

        String oEmail;
        List<PHI_Log__c> ophilst = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        if(ophilst != null && !ophilst.isEmpty()) {
            Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
            String accessToken;
            String expires;
            String isUpdate;
            if(resultMap != null) {
                accessToken = resultMap.get('accessToken');
                expires = resultMap.get('expires');
                isUpdate = resultMap.get('isUpdate');
            }
    
            // Get All Root Folder Collaboration ID and below it will avoid to remove those access
            List<String> collabList = new List<String>();
            List<String> phiDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Phi_data_folder_id);
            collabList.addAll(phiDataFolderList);
            List<String> ectDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Ect_data_folder_id);
            collabList.addAll(ectDataFolderList);
    
            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            BoxFolder folder;
            BoxCollaboration collaboration;
            BoxCollaboration.Info collabInfo;
    
            for(PHI_Log__c phi:ophilst) {
                folder = new BoxFolder(api, phi.Folder_Id__c);
                list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                for (BoxCollaboration.Info i : collaborations) {
                    if(i.accessibleBy <> null && i.id <> null && !collabList.contains(i.id)){
                        collaboration = new BoxCollaboration(api,i.id);
                        collaboration.deleteCollaboration();
                    }
                }
            }
            
            for(PHI_Log__c op:ophilst) {
                ownerid = op.Ownerid;
                csfldr = op.Case_Folder_Id__c;
            }
    
            system.debug('@@@owner'+ownerid+'@@@caseflderid'+csfldr);
           
            for(User u:[select id,Name,Email,Okta_email__c from User where Id=:ownerid]) {
                oEmail=String.valueOf(u.Email);
            }
            System.debug('@@@@oEmail---'+oEmail);
    
            if(oEmail != null) {
                for(PHI_Log__c op:ophilst) {
                    folder = new BoxFolder(api, op.Case_Folder_Id__c);
                    String opid = op.id;
                    if(phiIdList.contains(opid) && op.Case_Folder_Id__c != null) {
                        try {
                            folder.collaborate(oEmail, BoxCollaboration.Role.CO_OWNER);
                        } catch(BoxApiRequest.BoxApiRequestException e) {
                            System.debug('Exception---------'+String.valueOf(e));
                            throw e;
                        }
                    }
                }
            }
            system.debug('@@@ophilst'+ophilst);
    
            // Update Box Access Token to Custom Setting
            Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
        }
    }
}
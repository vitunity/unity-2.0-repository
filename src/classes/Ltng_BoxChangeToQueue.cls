global class Ltng_BoxChangeToQueue {
    public static void updateBoxOwnerQueue(List<PHI_Log__c> newPhiList, Map<Id, PHI_Log__c> oldPhiMap) {
        String phiIdString = '';
        for(PHI_Log__c phi : newPhiList) {
            if(oldPhiMap != null && oldPhiMap.containsKey(phi.Id) && 
              	phi.OwnerId != oldPhiMap.get(phi.Id).OwnerId) {
				phiIdString += (String.valueOf(phi.Id) + ',');
            }
        }
        if(phiIdString != null)
            changequeue(phiIdString);
    }

    @future(callout=true)
    public static void changequeue(String phiIdString) {
        List<String> phiIdList = phiIdString.split(',');
		String ownerid;
		String csfldr;
		String colid;
		String ocollab;
		String ocollabid;
		Boolean val;
		String userId=label.Box_User_ID;
		String oEmail;
		List<String> uniquelist= new List<String>();
		List<String> duplicatelist=new List<String>();
		List<String> queueuser=new List<String>();
		List<String> queueusremail=new List<String>();
		List<String> colbrtnids=new List<String>();
		List<String> uniqduplctlst=new List<String>();
		set<String> fltrduniqduplctlst=new Set<String>();
		Map<String,String> queueursremails= new Map<String,String>();

        List<PHI_Log__c> ophilst = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        if(ophilst != null && !ophilst.isEmpty()) {
            for(PHI_Log__c op:ophilst) {
                ownerid = String.valueOf(op.Ownerid);
                csfldr = String.valueOf(op.Case_Folder_Id__c);
                colid = String.valueOf(op.Collab_id__c);            
            }
            List<GroupMember> grplst = new List<GroupMember>([Select UserOrGroupId From GroupMember where GroupId =:ownerid]);
            for(GroupMember m:grplst) {
                String uid=String.valueOf(m.UserOrGroupId);
                for(User u:[select id,Name,Email, Okta_email__c from User where Id=:uid]) {
                    String fullname=String.valueOf(u.Name);
                    String usrEmail=String.valueOf(u.Email);
                    queueuser.add(fullname);
                    queueusremail.add(usrEmail);
                    queueursremails.put(fullname,usrEmail);
                }
            }
            System.debug('queueusremail------------'+queueusremail);
            System.debug('queueursremails------------'+queueursremails);
            system.debug('@@@owner'+ownerid+'@@@caseflderid'+csfldr+'@@@collabid'+colid);
    
            /*------------------ Start - New Code ------------------*/
            //String accessToken = Ltng_BoxAccess.generateAccessToken();
            Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
            String accessToken;
            String expires;
            String isUpdate;
            if(resultMap != null) {
                accessToken = resultMap.get('accessToken');
                expires = resultMap.get('expires');
                isUpdate = resultMap.get('isUpdate');
            }
    
            // Get All Root Folder Collaboration ID and below it will avoid to remove those access
            List<String> collabList = new List<String>();
            List<String> phiDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Phi_data_folder_id);
            collabList.addAll(phiDataFolderList);
            List<String> ectDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Ect_data_folder_id);
            collabList.addAll(ectDataFolderList);
    
            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            BoxFolder folder;
            BoxCollaboration collaboration;
            BoxCollaboration.Info collabInfo;
            for(PHI_Log__c phi:ophilst) {
                folder = new BoxFolder(api, phi.Folder_Id__c);
                list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
                for (BoxCollaboration.Info i : collaborations) {
                    if(i.accessibleBy <> null && i.id <> null && !collabList.contains(i.id)){
                        collaboration = new BoxCollaboration(api,i.id);
                        collaboration.deleteCollaboration();
                    }
                }
            }
    
            system.debug('@@@owner'+ownerid+'@@@caseflderid'+csfldr);

            for(PHI_Log__c op:ophilst) {
                String opid = op.id;
                folder = new BoxFolder(api, op.Case_Folder_Id__c);
                if(phiIdList.contains(opid) && op.Case_Folder_Id__c != null) {
                    for(String uEmail:queueusremail) {
                        System.debug('Email------------'+uEmail);
                        try {
                            if(uEmail != null) {
                                folder.collaborate(uEmail, BoxCollaboration.Role.CO_OWNER);
                            }
                        } catch(BoxApiRequest.BoxApiRequestException e) {
                            System.debug('Exception---------'+String.valueOf(e));
                        }
                    }
                }
            }
    
            // Update Box Access Token to Custom Setting
            Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
            system.debug('@@@ophilst'+ophilst);
            /*------------------ End - New Code ------------------*/
        }
	}
}
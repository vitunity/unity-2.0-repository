global class Ltng_BoxFolderDeletion {
    public static void deleteBoxFolder(List<PHI_Log__c> newPhiList) {
        String phiIdString = '';
        for(PHI_Log__c phi : newPhiList) {
            phiIdString += (String.valueOf(phi.Id) + ',');
        }
        if(phiIdString != null)
            deleteFolder(phiIdString);
    }

    @future(callout=true)
    public static void deleteFolder(String phiIdString) {
        String accessToken;
        String expires;
        String isUpdate;

        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();

        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

        List<String> phiIdList = phiIdString.split(',');
        List<PHI_Log__c> ophilst = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        if(ophilst != null && !ophilst.isEmpty()) {
            for(PHI_Log__c phiLog:ophilst) {
                if(phiLog.Folder_Id__c != null && phiLog.Case_Folder_Id__c != null) {
                    Ltng_BoxAccess.deleteBoxFolder(accessToken, phiLog.Folder_Id__c);
                    Ltng_BoxAccess.deleteBoxFolder(accessToken, phiLog.Case_Folder_Id__c);
                }
            }
            // Re-adding 
            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            BoxFolder ectDataFolder = new BoxFolder(api, label.Box_Ect_data_folder_id);
            BoxFolder boxPhiDataFolderFolder = new BoxFolder(api, label.Box_Phi_data_folder_id);
            List<String> userEmailList = Ltng_BoxAccess.getECTQueueEmailList();
            if(userEmailList <> null){
                for(String email:userEmailList) {
                    try {
                        ectDataFolder.collaborate(email, BoxCollaboration.Role.CO_OWNER);                   
                        boxPhiDataFolderFolder.collaborate(email, BoxCollaboration.Role.CO_OWNER);
                    } catch(Exception e){system.debug('Exception:'+e.getLinenumber()+'##'+e.getMessage());}
                }
            }
        }

        // update date for uploaded file to phiLog
        updatePHILog(phiIdString);
        // Update Box Access Token to Custom Setting
        Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
    }

    public static void updatePHILog(String phiIdString) {
        List<String> phiIdList = phiIdString.split(',');
        List<PHI_Log__c> phiUpdateList = new List<PHI_Log__c>();

        List<PHI_Log__c> phiLogRecList = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        for(PHI_Log__c ophi:phiLogRecList) {
            ophi.Case_Folder_Id__c = null;
            ophi.Folder_Id__c = null;
            ophi.Server_Location__c = null;
            ophi.BoxFolderName__c = null;
            ophi.IsFolderMovedToECT__c = false;
            ophi.Disposition_Date__c = date.today();
            ophi.Disposition2__c = 'Destroyed';
            ophi.Data_destruction_details__c = 'Purged by system as disposition type was marked Destroyed';
            ophi.How_Destroyed__c = 'Erased';
            ophi.Date_Destroyed__c = date.today();
            phiUpdateList.add(ophi);
        }

        Ltng_BoxProcessorControl.inFutureContext = true;
        if(!phiUpdateList.isEmpty())
            update phiUpdateList;
    }
}
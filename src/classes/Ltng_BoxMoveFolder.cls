global class Ltng_BoxMoveFolder {

    public static void moveBoxFolder(List<PHI_Log__c> newPhiList, Map<Id, PHI_Log__c> oldPhiMap) {
        String phiIdString = '';
        for(PHI_Log__c phi : newPhiList) {
            phiIdString += (String.valueOf(phi.Id) + ',');
        }
        System.debug('phiIdString----------'+phiIdString);
        if(phiIdString != null) {
            moveCaseFolderToComplaintFolder(phiIdString);
        }
    }

    public static void renameBoxFolder(List<PHI_Log__c> newPhiList) {
        String phiIdString = '';
        for(PHI_Log__c phi : newPhiList) {
            phiIdString += (String.valueOf(phi.Id) + ',');
        }
        System.debug('phiIdString----------'+phiIdString);
        if(phiIdString != null) {
            renameBoxFolderForECT(phiIdString);
        }
    }
    
    @Future(callout=true)
    public static void renameBoxFolderForECT(String phiIdString) {
        List<String> phiIdList = phiIdString.split(',');
        String complaintPhiFolderName;
        String complaintCaseFolderName;
        
        String accessToken;
        String expires;
        String isUpdate;
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

        List<PHI_Log__c> phiLogRecList = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        String phiFolderId;
        String caseFolderId;
        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        BoxFolder caseFolder;
        BoxFolder phiFolder;
        for(PHI_Log__c ophi:phiLogRecList) {
            phiFolderId = String.valueOf(ophi.Folder_Id__c);
            caseFolderId = String.valueOf(ophi.Case_Folder_Id__c);
			complaintPhiFolderName = Ltng_BoxAccess.getComplaintPHIFolderName(ophi);
			complaintCaseFolderName = Ltng_BoxAccess.getComplaintCaseFolderName(ophi);
        }

		caseFolder = new BoxFolder(api,caseFolderId);
		phiFolder = new BoxFolder(api,phiFolderId);

		caseFolder.rename(complaintCaseFolderName);
		phiFolder.rename(complaintPhiFolderName);

        // update date for uploaded file to phiLog
		updatePHILog(phiIdString, caseFolderId, phiFolderId, boxCRecord.Folder_Path__c, complaintPhiFolderName);

		// Update Box Access Token to Custom Setting
		Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
    }

    @Future(callout=true)
    public static void moveCaseFolderToComplaintFolder(String phiIdString) {
        String ectDataFolderId = label.Box_Ect_data_folder_id;
        String BoxPhiDataFolderId = label.Box_Phi_data_folder_id;
        String phiFolderId;
        String caseFolderId;
        String accessToken;
        String expires;
        String isUpdate;
        String phiFolderName;
        String caseFolderName;
        String complaintPhiFolderName;
        String complaintCaseFolderName;

        List<String> phiIdList = phiIdString.split(',');
        List<String> userEmailList = Ltng_BoxAccess.getECTQueueEmailList();
        System.debug('---------phiIdList--------'+phiIdList);
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();

        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

        List<PHI_Log__c> phiLogRecList = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        for(PHI_Log__c ophi:phiLogRecList) {
            phiFolderId = String.valueOf(ophi.Folder_Id__c);
            caseFolderId = String.valueOf(ophi.Case_Folder_Id__c);

            caseFolderName = Ltng_BoxAccess.getCaseFolderName(ophi);
            phiFolderName = Ltng_BoxAccess.getPHIFolderName(ophi);
			complaintPhiFolderName = Ltng_BoxAccess.getComplaintPHIFolderName(ophi);
			complaintCaseFolderName = Ltng_BoxAccess.getComplaintCaseFolderName(ophi);
        }
        System.debug('phiFolderId-------------'+phiFolderId);

        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        // New Folder with Complaint Name
		BoxFolder newCaseFolder;
        BoxFolder newPhiFolder;
        BoxFolder parentFolder = new BoxFolder(api, ectDataFolderId);
        String oldCaseFolderId = caseFolderId;
		String newCaseFolderId;
        String newPhiFolderId;
        try {
            BoxFolder.Info folderInfo = parentFolder.createFolder(complaintCaseFolderName);
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
            newCaseFolderId = string.valueof(results.get('id'));
            System.debug('ectDataFolderId-------------'+ectDataFolderId);
            newCaseFolder = new BoxFolder(api, newCaseFolderId);
        } catch(BoxApiRequest.BoxApiRequestException e) {
            System.debug('-------Parent Folder Already exist'+String.valueof(e));
        }

        Boolean isMoved = false;
        // Create Copy of exising case folder and append into complaint folder
        try {
            // rename new phi folder
            BoxFolder oldPhiFolder = new BoxFolder(api, phiFolderId);
            oldPhiFolder.rename(complaintPhiFolderName);

            System.debug('newCaseFolderId-------------'+newCaseFolderId);
			if(newCaseFolderId != null) {
				BoxFolder childFolder = new BoxFolder(api, phiFolderId);
				System.debug('childFolder-------------'+childFolder);
				BoxItem.Info phiFolderInfo = childFolder.copy(newCaseFolder, complaintPhiFolderName);
				System.debug('Here-------------');
                if(!Test.isRunningTest()) {
					newPhiFolderId = Ltng_BoxAccess.getBoxFolderId(accessToken, complaintPhiFolderName, newCaseFolderId);
                } else {
                    newPhiFolderId = '124534';
                }
                
                newPhiFolder = new BoxFolder(api, newPhiFolderId);
				//Map<String, Object> results1 = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
				//System.debug('results1-------------'+results1);
				//newPhiFolderId = string.valueof(results1.get('id'));

				if(newPhiFolderId != null) {
                    try {
						BoxFolder oldPhiLogFolder = new BoxFolder(api, phiFolderId);
						oldPhiLogFolder.deleteFolder(true);

                        // Get All Root Folder Collaboration ID and below it will avoid to remove those access
                        List<String> collabList = new List<String>();
                        List<String> phiDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Phi_data_folder_id);
                        collabList.addAll(phiDataFolderList);
                        List<String> ectDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Ect_data_folder_id);
                        collabList.addAll(ectDataFolderList);

                        // Remove all Collaboration from Case Folder Id
                        BoxCollaboration collaboration;
                        BoxFolder oldCSfolder = new BoxFolder(api, caseFolderId);
                        list<BoxCollaboration.Info> csCollaborations = oldCSfolder.getCollaborations();
                        for (BoxCollaboration.Info i : csCollaborations) {
                            if(i.accessibleBy <> null && i.id <> null && !collabList.contains(i.id)){
                                collaboration = new BoxCollaboration(api,i.id);
                                collaboration.deleteCollaboration();
                            }
                        }
                        Ltng_BoxAccess.deleteCaseFolder(accessToken, caseFolderId);
                        
                        // Re-adding 
                        BoxFolder ectDataFolder = new BoxFolder(api, ectDataFolderId);
                        BoxFolder boxPhiDataFolderFolder = new BoxFolder(api, BoxPhiDataFolderId);
                        for(String email:userEmailList) {
                            try {
                                ectDataFolder.collaborate(email, BoxCollaboration.Role.CO_OWNER);
                            } catch(Exception e){}
                            try {
                                boxPhiDataFolderFolder.collaborate(email, BoxCollaboration.Role.CO_OWNER);
                            } catch(Exception e){}
                        }
                    } catch(Exception e) {
                    	System.debug('-----Box Move Folder Deletion Exception----'+String.valueOf(e));
                	}

					// Add ECT Group User
					Ltng_BoxAccess.assignBoxGroup(accessToken, newCaseFolderId);

                    BoxFolder oldCaseFolder = new BoxFolder(api, oldCaseFolderId);
					List<BoxCollaboration.Info> collaborations = oldCaseFolder.getCollaborations();
					System.debug('collaborations-------------'+collaborations);
					BoxCollaboration.Info collabInfo;

					//BoxCollaboration.Info collabInfo1 = newCaseFolder.collaborate('nilesh.gorle@varian.com', BoxCollaboration.Role.CO_OWNER);
					for (BoxCollaboration.Info i : collaborations) {
						System.debug('Collaboration-------------'+i);
						if(i.accessibleBy <> null && i.id <> null) {
                            String role = String.valueOf(i.role);
                            String login = string.valueOf(i.accessibleBy.children.get('login'));
                            System.debug('role-------------'+role);
                            System.debug('login-------------'+login);
                            try {
                                if(role=='CO_OWNER') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.CO_OWNER);
                                } else if(role=='VIEWER') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.VIEWER);
                                } else if(role=='UPLOADER') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.UPLOADER);
                                } else if(role=='PREVIEWER_UPLOADER') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.PREVIEWER_UPLOADER);
                                } else if(role=='EDITOR') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.EDITOR);
                                } else if(role=='PREVIEWER ') {
                                    collabInfo = newCaseFolder.collaborate(login, BoxCollaboration.Role.PREVIEWER);
                                }
                            } catch(Exception e) {
                                System.debug('-----Exception----'+String.valueOf(e));
                            }
						}
					}
				}

				// update date for uploaded file to phiLog
				updatePHILog(phiIdString, newCaseFolderId, newPhiFolderId, boxCRecord.Folder_Path__c, complaintPhiFolderName);
			}
        } catch(BoxApiRequest.BoxApiRequestException e) {
            System.debug('Exception-------'+String.valueof(e));
        }

        // update date for uploaded file to phiLog
        if(newCaseFolderId!=null && newPhiFolderId!=null) {
        	updatePHILog(phiIdString, newCaseFolderId, newPhiFolderId, boxCRecord.Folder_Path__c, complaintPhiFolderName);
        }

		// Update Box Access Token to Custom Setting
		Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
    }

    public static void updatePHILog(String phiIdString, string caseFolderId, string folderId, string path, string phiFolderName) {
        List<String> phiIdList = phiIdString.split(',');
        System.debug('phiIdList----------'+phiIdList);
        System.debug('caseFolderId----------'+caseFolderId);
        System.debug('folderId----------'+folderId);
        System.debug('path----------'+path);
        List<PHI_Log__c> phiUpdateList = new List<PHI_Log__c>();

        List<PHI_Log__c> phiLogRecList = Ltng_BoxAccess.fetchAllPHILogRecord(phiIdList);
        for(PHI_Log__c ophi:phiLogRecList) {
            if(caseFolderId != null && folderId != null) {
                ophi.Case_Folder_Id__c = caseFolderId;
                ophi.Folder_Id__c = folderId;
                ophi.Server_Location__c = path+folderId;
                ophi.BoxFolderName__c = phiFolderName;
                // If folder is freshly moving to ECT Data folder, then make below field to true
                ophi.IsFolderMovedToECT__c = true;
                //ophi.Disposition_Date__c = date.today();
                //ophi.Disposition2__c = 'Destroyed';
                phiUpdateList.add(ophi);
            }
        }

        System.debug('phiUpdateList==============='+phiUpdateList);
        Ltng_BoxProcessorControl.inFutureContext = true;
        if(!phiUpdateList.isEmpty()) {
            update phiUpdateList;
        }
    }
}
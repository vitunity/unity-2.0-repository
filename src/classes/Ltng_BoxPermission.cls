/*
* Author: Nilesh Gorle
* Created Date: 25-August-2018
* Project/Story/Inc/Task : Unity/Box Integration for Lightning
* Description: add permission to user for file doc to box
*/
public class Ltng_BoxPermission {

    @AuraEnabled public static boolean isUserAdded{get;set;}
    @AuraEnabled public static String errMsg{get;set;}
    @AuraEnabled public static String infoMsg{get;set;}

    @AuraEnabled
    public static Boolean checkPermission(string recordId) {
        return Ltng_BoxAccess.checkPermission(recordId);
    }

    @AuraEnabled
    public static Boolean checkFolderIdExist(string recordId) {
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recordId);
		if(!philstc.isEmpty() && philstc[0].Case_Folder_Id__c==null && philstc[0].Folder_Id__c==null) {
            return false;
		}
        return true && Ltng_BoxAccess.checkPermission(recordId);
    }

    @AuraEnabled
    public static List<String> addUserToBox(String recordId, Boolean isVarianUser, String roleName, 
                                            String userId, String userEmail) {
        isUserAdded = false;
        List<String> resultList = new List<String>();
        System.debug('=========Start==============');
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        PHI_Log__c phiLogRec;
        if(!philstc.isEmpty()) {
            phiLogRec = philstc[0];
            if(phiLogRec.Case_Folder_Id__c==null && phiLogRec.Folder_Id__c==null) {
                system.debug('-------------BOX Folder Not Exist-------');
                return null;
            }
        } else {
            system.debug('Record Not Found---'+recordId);
            return null;
        }
        
        try{
            system.debug('@@@recordid'+recordId);
            //String accessToken = Ltng_BoxAccess.generateAccessToken();
            Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
            String accessToken;
            String expires;
            String isUpdate;
            if(resultMap != null) {
                accessToken = resultMap.get('accessToken');
                expires = resultMap.get('expires');
                isUpdate = resultMap.get('isUpdate');
            }

            BoxAPIConnection api = new BoxAPIConnection(accessToken);
            
            String caseFolderId;
            if (phiLogRec.Case_Folder_Id__c != null) {
                caseFolderId = String.valueOf(phiLogRec.Case_Folder_Id__c);
            }

            if(caseFolderId != null) {
                String Email;
                String Name;
                if(isVarianUser) {
                    User userRec = Ltng_BoxAccess.getUserRec(userId);
                    if(userRec != null) {
                        Email = String.Valueof(userRec.Email);
                        Name = String.Valueof(userRec.Name);
                    }
                } else {
                    Email = userEmail;
                    Ltng_BoxAccess.inviteUser(accessToken, Email);
                }
                
                if(Email == null) {
                    system.debug('Email is null');
                    return null;
                }

                Boolean isUserAdded = settingRoles(phiLogRec, roleName, Name, Email, caseFolderId, api);
                System.debug('isUserAdded-------------------'+isUserAdded);
                if(isUserAdded) {
                    infoMsg = 'User Updated Successfully on the Box folder';
                    resultList.add('true');
                    resultList.add(infoMsg);
                    System.debug('Chk1---------'+resultList);

					// Update PHI Log History
					Ltng_BoxAccess.createPHIHistoryLog('New user '+Email+' is added to Varian Secure Drop Folder', recordId, 'PHI_Log__c');
                }
                System.debug('Chk2---------'+resultList);

                // Update Box Access Token to Custom Setting
                Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
            }
        } catch(Exception ex){
            if(ex.getMessage()=='The Box API responded with a 400 : Bad Request') {
                errMsg = 'User already has permission on the Box folder';
            }
            resultList.add('false');
            resultList.add(errMsg);
            System.debug('Chk3---------'+resultList);
        }
        System.debug('Chk5---------'+resultList);
        return resultList;
    }

    public static Boolean settingRoles(PHI_Log__c phiLogRec, String role, String Name, String Email, String casefldrid, BoxAPIConnection api) {
        String getcollab;
        String collbrtnid;
        String collaborationids;
        List<PHI_Log__c> philst=new List<PHI_Log__c>();

        BoxFolder folder = new BoxFolder(api,casefldrid);
        String collabId = Ltng_BoxAccess.checkCollabExistForFolder(folder, Name, Email);
        BoxCollaboration.Info collabInfo;
        BoxCollaboration.Role boxFolderRole;
        System.debug('Role==========='+role);
        if(role=='Co-owner') {
            boxFolderRole = BoxCollaboration.Role.CO_OWNER;
        } else if(role=='Editor') {
            boxFolderRole = BoxCollaboration.Role.EDITOR;
        } else if(role=='Uploader') {
            boxFolderRole = BoxCollaboration.Role.UPLOADER;
        } else if(role=='Viewer') {
            boxFolderRole = BoxCollaboration.Role.VIEWER;
        } else if(role=='Preview-Uploader') {
            boxFolderRole = BoxCollaboration.Role.PREVIEWER_UPLOADER;
        } else if(role=='Previewer') {
            boxFolderRole = BoxCollaboration.Role.PREVIEWER;
        }

        if(collabId == null) {
            system.debug('@@@before called API');
			collabInfo = folder.collaborate(Email, boxFolderRole);
            String collab = (String.valueOf(BoxFolder.collabResponse));
            Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(collab);
            collbrtnid = String.valueOf(ooresults.get('id'));
            system.debug('@@@collbrtnid'+collbrtnid);
        } else {
            BoxCollaboration boxCollab = new BoxCollaboration(api, collabId);
            collabInfo = boxCollab.updateCollaboration(boxFolderRole);
            collbrtnid = collabId;
        }

        phiLogRec.User_id_s_of_owners__c += (collbrtnid+',');
        phiLogRec.User_id_s_of_owners__c = (phiLogRec.User_id_s_of_owners__c).remove('null').removeend(',');
        System.debug('==========='+phiLogRec.User_id_s_of_owners__c);
        update phiLogRec;
        return true;
    }
}
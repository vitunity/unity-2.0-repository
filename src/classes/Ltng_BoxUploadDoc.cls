/*
* Author: Nilesh Gorle
* Created Date: 25-August-2018
* Project/Story/Inc/Task : Unity/Box Integration for Lightning
* Description: Upload file doc to box
*/
public class Ltng_BoxUploadDoc {
    @AuraEnabled public static boolean bUploaded{get;set;}
    @AuraEnabled public static String errMsg{get;set;}
    @AuraEnabled public static String infoMsg{get;set;}
    public static String Default_Folder_Name = label.DEFAULT_BOX_FOLDER;

    public static void updatePHILogRecord(String recordId, String collbrtnid, String finalcsefldrid, String finalphifldrid, 
                                          String path, String phiFolderName, boolean isECTFolder) {
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        system.debug('@@collaboration id'+collbrtnid);

        for(PHI_Log__c phiLog: philstc) {
            if(finalphifldrid != null) {
                phiLog.Case_Folder_Id__c = finalcsefldrid;
                phiLog.Folder_Id__c = finalphifldrid;
                phiLog.Server_Location__c = path+finalphifldrid;
                phiLog.BoxFolderName__c = phiFolderName;
                if(collbrtnid!=null)
                    phiLog.Collab_id__c = collbrtnid;
                if(isECTFolder)
                    phiLog.IsFolderMovedToECT__c = true;
            }
        }

        if(!philstc.isEmpty())
            update philstc;
    }

    public static void callBoxUploadFileAPI(String uploadContent, String uploadFilename, String finalphifldrid, String accky) {
        blob base64EncodeFile=base64EncodeFileContent(uploadContent,uploadFilename);
        system.debug('@@@@ex'+base64EncodeFile);
        http http=new http();
        HTTPResponse res=new HttpResponse();
        HttpRequest req = new HttpRequest();
        String uploadEndPointURL;
        String boundary = '----------------------------741e90d31eff';
        
        //system.debug('@@@@FileSize'+FileSize);
        //integer uploadedFileSize = integer.valueof(FileSize);


        //if(uploadedFileSize < 20000000) {
        	uploadEndPointURL = 'https://upload.box.com/api/2.0/files/content?parent_id='+finalphifldrid;
            //req.setMethod('POST');
            req.setTimeout(120000);
            //Send request to Box
        /*} else {
        	// Above is the old code which takes only 50 MB Files. Below is the new API which chunks the file
        	uploadEndPointURL = 'https://upload.box.com/api/2.0/files/upload_sessions'; //+finalphifldrid;
			String body = '{ "folder_id": "'+finalphifldrid+'", "file_size": '+20000000+', "file_name": "'+uploadFilename+'"}';
        	req.setBody(body);
        }*/

		req.setBodyAsBlob(base64EncodeFile);
		req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
		req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
		req.setHeader('Authorization', 'Bearer '+accky);
		system.debug('@@@@uploadEndPointURL'+uploadEndPointURL);
		req.setMethod('POST');
		req.setEndpoint(uploadEndPointURL);
             
        res = http.send(req);
        system.debug('@@@response'+res);
        Integer uploadStatusCode=res.getStatusCode();
        system.debug('--uploadStatusCode----'+uploadStatusCode);
        if(uploadStatusCode==201) {
            bUploaded = true;
            infoMsg = 'File uploaded successfully.';
        } else if(uploadStatusCode==409) {
            errMsg = 'The file you are trying to upload already exists';
        } else if(uploadStatusCode!=200) {
            errMsg = 'StatusCode :'+uploadStatusCode+','+res.getbody();
        }
    }

    public static List<String> uploadToECTFolder(String recordId, String uploadFilename, String uploadContent, String contentType, String fileSize) {
        String ectDataFolderId = label.Box_Ect_data_folder_id;
        List<String> resultLst = new List<String>();
        bUploaded = false;
        infoMsg = null;
        errMsg = null;
        String phiFolderId;
        String caseFolderId;
        String accessToken;
        String expires;
        String isUpdate;
        String owneremail;
        String phiFolderName;
        String caseFolderName;
        String collbrtnid;
        String complaintPhiFolderName;
        String complaintCaseFolderName;

        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();

        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

        List<PHI_Log__c> phiLogRecList = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        for(PHI_Log__c ophi:phiLogRecList) {
            phiFolderId = String.valueOf(ophi.Folder_Id__c);
            caseFolderId = String.valueOf(ophi.Case_Folder_Id__c);

            caseFolderName = Ltng_BoxAccess.getCaseFolderName(ophi);
            phiFolderName = Ltng_BoxAccess.getPHIFolderName(ophi);
			complaintPhiFolderName = Ltng_BoxAccess.getComplaintPHIFolderName(ophi);
			complaintCaseFolderName = Ltng_BoxAccess.getComplaintCaseFolderName(ophi);
            
            owneremail = String.valueOf(ophi.Okta_Email__c);
            System.debug('-----owneremail1--------'+owneremail);
            
            if(owneremail == null) {
                owneremail = Ltng_BoxAccess.getOktaOwnerEmail(String.valueOf(ophi.OwnerID));
                System.debug('-----owneremail2--------'+owneremail);
            }
        }
        System.debug('phiFolderId-------------'+phiFolderId);
        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        String newCaseFolderId = Ltng_BoxAccess.createBoxFolder(accessToken, complaintCaseFolderName, ectDataFolderId); // folderName and parentfolderId
        System.debug('-------Case Folder ID--------'+newCaseFolderId);
        String newPhiFolderId;
        if(newCaseFolderId != null) {
            newPhiFolderId = Ltng_BoxAccess.createBoxFolder(accessToken, complaintPhiFolderName, newCaseFolderId); // folderName and parentfolderId
            System.debug('-------PHI Folder ID--------'+newPhiFolderId);

			BoxFolder csfolder = new BoxFolder(api,newCaseFolderId);
            System.debug('Case Folder---------'+csfolder);
                
            try {
                BoxCollaboration.Info collabInfo = csfolder.collaborate(owneremail, BoxCollaboration.Role.CO_OWNER);
                String ocollab = String.valueOf(BoxFolder.collabResponse);
                Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(ocollab);
                collbrtnid=String.valueOf(ooresults.get('id'));
            } catch(BoxApiRequest.BoxApiRequestException e) {
                System.debug('Already a collaborator----'+String.valueof(e));
            }
        }

        System.debug('------------FolderId--------'+newPhiFolderId);
        if(Integer.valueOf(fileSize) > 3145728 && newPhiFolderId != null) {
            errMsg = '<ul style="padding:0px 20px;list-style: circle;"><li>Upload file size must be less than 3MB.</li><li>To load your file, close this error message and click on the link in the "Varian Secure Drop Folder Name" field and upload directly in the box folder.</li></ul>';
            //errMsg = 'File Size must be less than 3MB. <br/>To resolve this error, close this error message and upload your data to BOX by clicking "Upload To Varian Secure Drop" button in this PHI Log.';
        } else if(uploadContent!=null && newPhiFolderId != null) {
            callBoxUploadFileAPI(uploadContent, uploadFilename, newPhiFolderId, accessToken);
        	// Update PHI Log History
        	Ltng_BoxAccess.createPHIHistoryLog(uploadFilename + ' is uploaded to Varian Secure Drop Folder \'' +complaintPhiFolderName +'\'', recordId, 'PHI_Log__c');
        } else{
            infoMsg = 'Upload the file directly to the secure drop folder listed on PHI log record';
            //infoMsg = 'Please select file.';
        }

        // Update Box Access Token to Custom Setting
        Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
        
        // update date for uploaded file to phiLog
        updatePHILogRecord(recordId, collbrtnid, newCaseFolderId, newPhiFolderId, boxCRecord.Folder_Path__c, complaintPhiFolderName, true);

        if(bUploaded) {
            resultLst.add('true');
            resultLst.add(infoMsg);
        } else {
            resultLst.add('false');
            resultLst.add(errMsg);
        }
        return resultLst;
    }
    
    @AuraEnabled
    public static List<String> uploadBoxFile(String recordId, String uploadFilename, String uploadContent, String contentType, String fileSize) {
        bUploaded = false;
        infoMsg = null;
        errMsg = null;
        List<String> resultLst = new List<String>();
        System.debug('---------1--------'+uploadContent);
        System.debug('---------2--------'+uploadFilename);
        String phiDataFolderId = '0'; //replaced it by 52117104028
        String BoxPhiDataFolderId = label.Box_Phi_data_folder_id;
        //String BoxPhiDataFolderId = '0'; //53485979045
        String Caseid;
        String phifldrid;
        String owneremail;
        String collbrtnid;
        String ownrfnme;
        String exstngphifldr;
        String alrdyExstngcsfldrid;
        String finalphifldrid;
        String finalcsefldrid;
        String phiLogId;
        Boolean isFolderMovedToECT = false;
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        
        String complaintNumber;
        String OwnerId;
        
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
        String accessToken;
        String expires;
        String isUpdate;
        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        
        if (accessToken == null) {
            resultLst.add('false');
            resultLst.add('Invalid Box App Credential');
            return resultLst;
        }

        system.debug('@@@record id'+recordId);
        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        List<String> uniquelist=new List<String>();
        List<String> duplicatelist=new List<String>();
        String phiFolderName;
        String caseFolderName;

        List<PHI_Log__c> phiLogList = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        for(PHI_Log__c oolstofphi:phiLogList) {
            ownrfnme = String.valueOf(oolstofphi.Owneres_Name__c);
            alrdyExstngcsfldrid = String.valueOf(oolstofphi.Case_Folder_Id__c);
            exstngphifldr = String.valueOf(oolstofphi.Folder_Id__c);
            Caseid=String.valueOf(oolstofphi.Case__c);
            phiLogId = String.valueOf(oolstofphi.Id);
            isFolderMovedToECT = oolstofphi.IsFolderMovedToECT__c;

            complaintNumber = String.valueOf(oolstofphi.Complaint__c);
            OwnerId = String.ValueOf(oolstofphi.OwnerId);
            
            owneremail = String.valueOf(oolstofphi.Okta_Email__c);
            System.debug('-----owneremail1--------'+owneremail);
            
            if(owneremail == null) {
                owneremail = Ltng_BoxAccess.getOktaOwnerEmail(String.valueOf(oolstofphi.OwnerID));
                System.debug('-----owneremail2--------'+owneremail);
            }
            caseFolderName = Ltng_BoxAccess.getCaseFolderName(oolstofphi);
            phiFolderName = Ltng_BoxAccess.getPHIFolderName(oolstofphi);
            system.debug('@@case folder----'+alrdyExstngcsfldrid+'@@phi folder id-----'+exstngphifldr+'@@owner name-----------'+ownrfnme+'@@Case id---------'+Caseid+'@@owneremail'+owneremail);
        }

        if(Caseid != null && !isFolderMovedToECT && exstngphifldr == null && alrdyExstngcsfldrid == null) {
            List<L2848__c> ecfFormList = [Select Id From L2848__c Where Case__c =: Caseid];
            if(!ecfFormList.isEmpty()) {
				BoxPhiDataFolderId = label.Box_Ect_data_folder_id;
                isFolderMovedToECT = true;
            }
		}
		/*if((alrdyExstngcsfldrid == null || exstngphifldr == null) && complaintNumber != null && 
           OwnerId.startswith('00G')) {
			List<Group> grpList = [select Id, Name from Group where  Id =: OwnerId Limit 1];
			if(!grpList.isEmpty() && grpList[0].Name == label.Box_Queue_Name) {
                return uploadToECTFolder(recordId, uploadFilename, uploadContent, contentType);
            }
		}*/

		if((alrdyExstngcsfldrid == null || exstngphifldr == null) && complaintNumber != null) {
			return uploadToECTFolder(recordId, uploadFilename, uploadContent, contentType, fileSize);
		}

        if(alrdyExstngcsfldrid!=Null && exstngphifldr!=Null) {
            system.debug('uploading file where case and phi folder exists'+'@@casefolder id'+alrdyExstngcsfldrid+'@@@exstngphifldr'+exstngphifldr);
            finalphifldrid = exstngphifldr;
            finalcsefldrid = alrdyExstngcsfldrid;
        } else if(alrdyExstngcsfldrid==Null && exstngphifldr==Null) {
            for(PHI_Log__c ophi:[select id,Case_Folder_Id__c,Case__c from PHI_Log__c where Case__c=:Caseid AND 
                                 Case_Folder_Id__c!=Null And Id=: phiLogId]) {
                finalcsefldrid = String.valueOf(ophi.Case_Folder_Id__c);
            }

            if(finalcsefldrid==Null) {
                system.debug('No case folder and no phi folder'+'@@casename'+caseFolderName);
                system.debug('No case folder and no phi folder'+'@@BoxPhiDataFolderId'+BoxPhiDataFolderId);
                //BoxFolder parentFolder= new BoxFolder(api,'0');
                finalcsefldrid = Ltng_BoxAccess.createBoxFolder(accessToken, caseFolderName, BoxPhiDataFolderId); // folderName and parentfolderId
                System.debug('finalcsefldrid=============='+finalcsefldrid);
                BoxFolder folder1 = new BoxFolder(api,finalcsefldrid);
                System.debug('owneremail=============='+owneremail);
                System.debug('ROLE=============='+BoxCollaboration.Role.CO_OWNER);
                System.debug('folder1=============='+folder1);
                BoxFolder.Info info2 = folder1.createFolder(phiFolderName);
                System.debug('create_folder_response'+BoxFolder.createResponse);
                Map<String, Object> oresults = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
                phifldrid=String.valueof(oresults.get('id'));
                finalphifldrid=phifldrid;
                try {
					BoxCollaboration.Info collabInfo = folder1.collaborate(owneremail, BoxCollaboration.Role.CO_OWNER);
					String ocollab = String.valueOf(BoxFolder.collabResponse);
					Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(ocollab);
					collbrtnid=String.valueOf(ooresults.get('id'));
                } catch(BoxApiRequest.BoxApiRequestException e) {
                    System.debug('Already a collaborator----'+String.valueof(e));
                }
            } else {
                system.debug('uploading file where case folder exists but not phi folder'+'@@@casefolder id'+finalcsefldrid);
                BoxFolder parentFolder= new BoxFolder(api,finalcsefldrid);
                finalphifldrid = Ltng_BoxAccess.createBoxFolder(accessToken, phiFolderName, finalcsefldrid); // parentfolder
                system.debug('@@phifolderid'+finalphifldrid);
                list<BoxCollaboration.Info> collaborations = parentFolder.getCollaborations();
                String getcollab = String.valueOf(BoxFolder.getcollabResponse);
                Map<String, Object> allcollabs = (Map<String, Object>) JSON.deserializeUntyped(getcollab);
                List<Object> listcollabs = (List<Object>) allcollabs.get('entries');

                for(Object l : listcollabs) {
                    Map<String,Object> mpParsed = (Map<String,Object>)l;
                    for (String x : mpParsed.keySet()) {
                        if(x == 'accessible_by') {
                            String accessible = String.valueOf(mpParsed.get(x));

                            if(accessible != null) {
                                String[] arrTest = accessible.split(',');
                                for(String keys : arrTest) {
                                    if (keys.contains('name') == TRUE) {
                                        String[] tempOwner= keys.split('=');
                                        for(String towner : tempOwner) {
                                            if(towner!=' name' && towner!=Null && towner!=ownrfnme) {
                                                system.debug('@@@unique towner'+towner);
                                                uniquelist.add(towner);         
                                            }
                                            if(towner!=' name' && towner!=Null && towner==ownrfnme) {
                                                system.debug('@@@duplicatetowner'+towner);
                                                duplicatelist.add(towner);
                                            }
                                        }
                                        system.debug('!!uniquelist'+uniquelist+'duplicatelist'+duplicatelist);
                                    }
                                }
                            }
                        }
                    }
                }

                if(owneremail == null) {
                    owneremail = UserInfo.getUserEmail();
                }

                if(duplicatelist.isEmpty()) {
                    system.debug('adding collaborator if its is not there on main folder');
                    BoxCollaboration.Info collabInfo = parentFolder.collaborate(owneremail, BoxCollaboration.Role.CO_OWNER);

                    String collab = (String.valueOf(BoxFolder.collabResponse));
                    Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(collab);
                    collbrtnid=String.valueOf(ooresults.get('id'));
                    system.debug('@@@collaboration id'+collbrtnid);
                }
            } 
        }

        System.debug('------------FolderId--------'+finalphifldrid);
        if(Integer.valueOf(fileSize) > 3145728 && finalphifldrid != null) {
			errMsg = '<ul style="padding:0px 20px;list-style: circle;"><li>Upload file size must be less than 3MB.</li><li>To load your file, close this error message and click on the link in the "Varian Secure Drop Folder Name" field and upload directly in the box folder.</li></ul>';
			//errMsg = 'File Size must be less than 3MB. <br/>To resolve this error, close this error message and upload your data to BOX by clicking "Upload To Varian Secure Drop" button in this PHI Log.';
        } else if(uploadContent!=null) {
            callBoxUploadFileAPI(uploadContent, uploadFilename, finalphifldrid, accessToken);

        	// Update PHI Log History
        	Ltng_BoxAccess.createPHIHistoryLog(uploadFilename + ' is uploaded to Varian Secure Drop Folder \'' +phiFolderName +'\'', recordId, 'PHI_Log__c');
        } else{
            infoMsg = 'Upload the file directly to the secure drop folder listed on PHI log record';
            //infoMsg = 'Please select file.';
        }

        // Update Box Access Token to Custom Setting
        Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
        
        // update date for uploaded file to phiLog
        updatePHILogRecord(recordId, collbrtnid, finalcsefldrid,finalphifldrid,boxCRecord.Folder_Path__c, phiFolderName, isFolderMovedToECT);

        if(bUploaded) {
            resultLst.add('true');
            resultLst.add(infoMsg);
        } else {
            resultLst.add('false');
            resultLst.add(errMsg);
        }
        return resultLst;
    }

    public static blob base64EncodeFileContent(String uploadContent, String file_name){
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('=')) {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }

        String bodyEncoded = EncodingUtil.urlDecode(uploadContent, 'UTF-8');
        //String bodyEncoded = EncodingUtil.base64Encode(decodeData);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')){
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }

        return bodyBlob;
    }

    @AuraEnabled
    public static Boolean checkPermission(string recordId) {
        System.debug('------Check---------');
        return Ltng_BoxAccess.checkPermission(recordId);
    }
}
/*
* Author: Nilesh Gorle
* Created Date: 25-August-2018
* Project/Story/Inc/Task : Unity/Box Integration for Lightning
* Description: Upload file doc to box
*/
public class Ltng_BoxUploadDoc_v1 {
    @AuraEnabled public static boolean bUploaded{get;set;}
    @AuraEnabled public static String errMsg{get;set;}
    @AuraEnabled public static String infoMsg{get;set;}
    public static String Default_Folder_Name = label.DEFAULT_BOX_FOLDER;

    public static void updatePHILogRecord(String recordId, String collbrtnid, String finalcsefldrid, String finalphifldrid, String path, String phiFolderName) {
        List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        system.debug('@@collaboration id'+collbrtnid);

        for(PHI_Log__c phiLog: philstc) {
            phiLog.Case_Folder_Id__c = finalcsefldrid;
            phiLog.Folder_Id__c = finalphifldrid;
            phiLog.Server_Location__c = path+finalphifldrid;
            phiLog.BoxFolderName__c = phiFolderName;
            if(collbrtnid!=null)
                phiLog.Collab_id__c = collbrtnid;
        }

        if(!philstc.isEmpty())
            update philstc;
    }

    public static void callBoxUploadFileAPI(String uploadContent, String uploadFilename, String finalphifldrid, String accky) {
        blob base64EncodeFile=base64EncodeFileContent(uploadContent,uploadFilename);
        system.debug('@@@@ex'+base64EncodeFile);
        String uploadEndPointURL='https://upload.box.com/api/2.0/files/content?parent_id='+finalphifldrid;
        String boundary = '----------------------------741e90d31eff';
        http http=new http();
        HTTPResponse res=new HttpResponse();
        HttpRequest req = new HttpRequest();
        //req.setBodyAsBlob(uploadContent);
        req.setBodyAsBlob(base64EncodeFile);
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setHeader('Content-Length',String.valueof(req.getBodyAsBlob().size()));
        req.setHeader('Authorization', 'Bearer '+accky);
        system.debug('@@@@uploadEndPointURL'+uploadEndPointURL);
        req.setMethod('POST');
        req.setEndpoint(uploadEndPointURL);
        req.setMethod('POST');
        req.setTimeout(120000);
        //Send request to Box
             
        res = http.send(req);
        system.debug('@@@response'+res);
        Integer uploadStatusCode=res.getStatusCode();
        system.debug('--uploadStatusCode----'+uploadStatusCode);
        if(uploadStatusCode==201) {
            bUploaded = true;
            infoMsg = 'File uploaded successfully.';
        } else if(uploadStatusCode==409) {
            errMsg = 'The file you are trying to upload already exists';
        } else if(uploadStatusCode!=200) {
            errMsg = 'StatusCode :'+uploadStatusCode+','+res.getbody();
        }
    }
    
    //get PHI Parent Folder ID
    public static boolean isECFPHI(PHI_Log__c phiobj){
        List<L2848__c> lstECF = new List<L2848__c>();
        if(phiobj.Case__c <> null){
            lstECF = [select Id from L2848__c where case__c =: phiobj.Case__c limit 1];
            if(lstECF.size()>0)return true;
        }     
        return false;      
    }
    
    //get PHI Log Folder Name
    public static string getPHIFolderName(PHI_Log__c phiobj){
        string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
        string caseNumber = (phiobj.Case_Number__c <> null)?'_'+phiobj.Case_Number__c:'';
        return phiobj.Name+caseNumber+accountName;            
    }
    //get Case Folder Name
    public static string getCaseFolderName(PHI_Log__c phiobj){        
        if(phiobj.Case_Number__c <> null){
            string accountName = (phiobj.Account__c <> null)?'_'+phiobj.Account__r.Name:'';
            string pcsnName = (phiobj.Case__r.PCSN__c <> null)?'_'+phiobj.Case__r.PCSN__c:'';
            return phiobj.Case_Number__c+accountName+pcsnName;
        }
        if(phiobj.Account__c <> null){
            return phiobj.Account__r.Name;
        }        
        return 'CASE-0000000000';        
    }

    @AuraEnabled
    public static List<String> uploadBoxFile(String recordId, String uploadFilename, String uploadContent, String contentType) {
        bUploaded = false;
        infoMsg = null;
        errMsg = null;
        List<String> resultLst = new List<String>();
        System.debug('---------1--------'+uploadContent);
        System.debug('---------2--------'+uploadFilename);
        String phiDataFolderId = '0'; //replaced it by 52117104028
        //String BoxPhiDataFolderId = label.Box_Phi_data_folder_id;
        
        //Master folder Ids
        String BoxPhiDataFolderId = '53633802125'; //53485979045
        String BoxECFDataFolderId = '53634260006'; //53485979045
        String ParentFolderId;
        
        
        String Caseid;
        String phifldrnme;
        String phifldrid;
        String owneremail;
        String collbrtnid;
        String ownrfnme;
        String Casenamefrfldr;
        String exstngphifldr;
        String alrdyExstngcsfldrid;
        String finalphifldrid;
        String finalcsefldrid;
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
        String accessToken;
        String expires;
        String isUpdate;
        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }
        // Update Box Access Token to Custom Setting
        Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        
        if (accessToken == null) {
            resultLst.add('false');
            resultLst.add('Invalid Box App Credential');
            return resultLst;
        }

        system.debug('@@@record id'+recordId);
        BoxAPIConnection api = new BoxAPIConnection(accessToken);
        List<String> uniquelist=new List<String>();
        List<String> duplicatelist=new List<String>();

        List<PHI_Log__c> phiLogList = Ltng_BoxAccess.fetchPHILogRecord(recordId);
        for(PHI_Log__c oolstofphi:phiLogList) {
            
            ParentFolderId = (isECFPHI(oolstofphi))?BoxECFDataFolderId:BoxPhiDataFolderId;
            
            ownrfnme = String.valueOf(oolstofphi.Owneres_Name__c); 
            
            //Get Dynamic PHI Folder Name
            phifldrnme = getPHIFolderName(oolstofphi);      
            //phifldrnme = String.valueOf(oolstofphi.Name);
            
            alrdyExstngcsfldrid = String.valueOf(oolstofphi.Case_Folder_Id__c);
            exstngphifldr = String.valueOf(oolstofphi.Folder_Id__c);
            Caseid=String.valueOf(oolstofphi.Case__c);
            
            owneremail = String.valueOf(oolstofphi.Okta_Email__c);
            System.debug('-----owneremail1--------'+owneremail);
            
            if(owneremail == null) {
                owneremail = Ltng_BoxAccess.getOktaOwnerEmail(String.valueOf(oolstofphi.OwnerID));
                System.debug('-----owneremail2--------'+owneremail);
            }

            //Get Dynamic Case Folder Name
            Casenamefrfldr = getCaseFolderName(oolstofphi);
            /*if (oolstofphi.get('Case_Number__c') != null) {
                Casenamefrfldr = String.valueOf(oolstofphi.Case_Number__c);
            } else {
                Casenamefrfldr = Default_Folder_Name;
            }*/
            system.debug('@@case folder----'+alrdyExstngcsfldrid+'@@phi folder id-----'+exstngphifldr+'@@owner name-----------'+ownrfnme+'@@Case id---------'+Caseid+'@@phifldrnme----------'+phifldrnme+'@@owneremail'+owneremail+'@@Casenamefrfldr------------'+Casenamefrfldr);
        }

        if(alrdyExstngcsfldrid!=Null && exstngphifldr!=Null) {
            system.debug('uploading file where case and phi folder exists'+'@@casefolder id'+alrdyExstngcsfldrid+'@@@exstngphifldr'+exstngphifldr);
            finalphifldrid=exstngphifldr;
            finalcsefldrid=alrdyExstngcsfldrid;
        } else if(alrdyExstngcsfldrid==Null && exstngphifldr==Null) {
            for(PHI_Log__c ophi:[select id,Case_Folder_Id__c,Case__c from PHI_Log__c where Case__c=:Caseid AND Case_Folder_Id__c!=Null]) {
                finalcsefldrid=String.valueOf(ophi.get('Case_Folder_Id__c'));
                //determine case related list ECF Records present or not based on it decide parent folder.                
            }

            if(finalcsefldrid==Null) {
                system.debug('No case folder and no phi folder'+'@@casename'+Casenamefrfldr);
                //BoxFolder parentFolder= new BoxFolder(api,'0');
                if(Casenamefrfldr == null) {
                    Casenamefrfldr = Default_Folder_Name;
                }
                System.debug('PARAM:'+accessToken+'::'+Casenamefrfldr+'::'+ParentFolderId);
                
                
                finalcsefldrid = Ltng_BoxAccess.createBoxFolder(accessToken, Casenamefrfldr, ParentFolderId);
                //finalcsefldrid = Ltng_BoxAccess.createBoxFolder(accessToken, Casenamefrfldr, BoxPhiDataFolderId); // folderName and parentfolderId
                
                /*try {
                    BoxFolder.Info info1 = parentFolder.createFolder(Casenamefrfldr);
                    //system.debug('-------info1------'+info1.jsonString.get('status'));
                } catch(BoxApiRequest.BoxApiRequestException e) {
                    System.debug('-------Folder Already exist'+String.valueof(e));
                }

                System.debug('create_folder_response'+BoxFolder.createResponse);
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
                finalcsefldrid=string.valueof(results.get('id'));*/
                System.debug('finalcsefldrid=============='+finalcsefldrid);
                BoxFolder folder1 = new BoxFolder(api,finalcsefldrid);
                System.debug('owneremail=============='+owneremail);
                System.debug('ROLE=============='+BoxCollaboration.Role.CO_OWNER);
                System.debug('folder1=============='+folder1);
                try {
                    BoxCollaboration.Info collabInfo = folder1.collaborate(owneremail, BoxCollaboration.Role.CO_OWNER);
                    String ocollab = String.valueOf(BoxFolder.collabResponse);
                    Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(ocollab);
                    collbrtnid=String.valueOf(ooresults.get('id'));
                } catch(BoxApiRequest.BoxApiRequestException e) {
                    System.debug('Already a collaborator----'+String.valueof(e));
                }

                BoxFolder.Info info2 = folder1.createFolder(phifldrnme);
                System.debug('create_folder_response'+BoxFolder.createResponse);
                Map<String, Object> oresults = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
                phifldrid=String.valueof(oresults.get('id'));
                finalphifldrid=phifldrid; 
            } else {
                system.debug('uploading file where case folder exists but not phi folder'+'@@@casefolder id'+finalcsefldrid);
                if(finalcsefldrid == null) {
                    finalcsefldrid = Default_Folder_Name;
                }
                BoxFolder parentFolder= new BoxFolder(api,finalcsefldrid);
                
                finalphifldrid = Ltng_BoxAccess.createBoxFolder(accessToken, phifldrnme, finalcsefldrid); // parentfolder
                /*BoxFolder.Info info = parentFolder.createFolder(phifldrnme);
                System.debug('create_folder_response'+BoxFolder.createResponse);
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(BoxFolder.createResponse);
                phifldrid=string.valueof(results.get('id'));
                finalphifldrid=phifldrid;*/
                
                system.debug('@@phifolderid'+finalphifldrid);
                list<BoxCollaboration.Info> collaborations = parentFolder.getCollaborations();
                String getcollab = String.valueOf(BoxFolder.getcollabResponse);
                Map<String, Object> allcollabs = (Map<String, Object>) JSON.deserializeUntyped(getcollab);
                List<Object> listcollabs = (List<Object>) allcollabs.get('entries');
                
                for(Object l : listcollabs) {
                    Map<String,Object> mpParsed = (Map<String,Object>)l;
                    for (String x : mpParsed.keySet()) {
                        if(x == 'accessible_by') {
                            String accessible = String.valueOf(mpParsed.get(x));

                            if(accessible != null) {
                                String[] arrTest = accessible.split(',');
                                for(String keys : arrTest) {
                                    if (keys.contains('name') == TRUE) {
                                        String[] tempOwner= keys.split('=');
                                        for(String towner : tempOwner) {
                                            if(towner!=' name' && towner!=Null && towner!=ownrfnme) {
                                                system.debug('@@@unique towner'+towner);
                                                uniquelist.add(towner);         
                                            }
                                            if(towner!=' name' && towner!=Null && towner==ownrfnme) {
                                                system.debug('@@@duplicatetowner'+towner);
                                                duplicatelist.add(towner);
                                            }
                                        }
                                        system.debug('!!uniquelist'+uniquelist+'duplicatelist'+duplicatelist);
                                    }
                                }
                            }
                        }
                    }
                }

                if(owneremail == null) {
                    owneremail = UserInfo.getUserEmail();
                }

                if(duplicatelist.isEmpty()) {
                    system.debug('adding collaborator if its is not there on main folder');
                    BoxCollaboration.Info collabInfo = parentFolder.collaborate(owneremail, BoxCollaboration.Role.CO_OWNER);

                    String collab = (String.valueOf(BoxFolder.collabResponse));
                    Map<String, Object> ooresults = (Map<String, Object>) JSON.deserializeUntyped(collab);
                    collbrtnid=String.valueOf(ooresults.get('id'));
                    system.debug('@@@collaboration id'+collbrtnid);
                }
            } 
        }

        System.debug('------------FolderId--------'+finalphifldrid);
        if(uploadContent!=null) {
            callBoxUploadFileAPI(uploadContent, uploadFilename, finalphifldrid, accessToken);
        } else{
            infoMsg = 'Please select file.';
        }

        // update date for uploaded file to phiLog
        updatePHILogRecord(recordId, collbrtnid, finalcsefldrid,finalphifldrid,boxCRecord.Folder_Path__c,phifldrnme);

        if(bUploaded) {
            resultLst.add('true');
            resultLst.add(infoMsg);
        } else {
            resultLst.add('false');
            resultLst.add(errMsg);
        }
        return resultLst;
    }

    public static blob base64EncodeFileContent(String uploadContent, String file_name){
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('=')) {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }

        String bodyEncoded = EncodingUtil.urlDecode(uploadContent, 'UTF-8');
        //String bodyEncoded = EncodingUtil.base64Encode(decodeData);
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
 
        if(last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')){
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }

        return bodyBlob;
    }

    @AuraEnabled
    public static Boolean checkPermission(string recordId) {
        System.debug('------Check---------');
        return Ltng_BoxAccess.checkPermission(recordId);
    }
}
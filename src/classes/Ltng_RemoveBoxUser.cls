/*
* Author: Nilesh Gorle
* Created Date: 25-August-2018
* Project/Story/Inc/Task : Unity/Box Integration for Lightning
* Description: remove permission to user for file doc to box
*/
public without sharing class Ltng_RemoveBoxUser{
    public static BoxAPIConnection api;
  
    @AuraEnabled public static List<WUser> lstUser{get;set;}

    @AuraEnabled
    public static List<WUser> getAllBoxUserFromBoxLst(String recordId){
        PHI_Log__c phi = [select id,Folder_Id__c,Owners_Box_Email__c from PHI_Log__c where id=: recordId];

        lstUser = new List<WUser>();
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
        String accessToken;
        String expires;
        String isUpdate;
        if(resultMap != null) {
        	accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
		}

        api = new BoxAPIConnection(accessToken);
        BoxFolder folder = new BoxFolder(api, phi.Folder_Id__c);
        list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
        for (BoxCollaboration.Info i : collaborations) {
            if(i.accessibleBy <> null){
                boolean isRemovable = (phi.Owners_Box_Email__c <> null && phi.Owners_Box_Email__c.equals(i.accessibleBy.children.get('login')))?false:true;
                lstUser.add(new WUser(isRemovable,i.id,i.accessibleBy.children.get('login'),i.accessibleBy.Name,string.valueOf(i.status),string.valueOf(i.role)));
            }
        }

		// Update Box Access Token to Custom Setting
        Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);

        return lstUser;
    }
    
    @AuraEnabled
    public static Boolean RemoveCollaboration(String CollaborationId){
        try{
            //String accessToken = Ltng_BoxAccess.generateAccessToken();

            Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();
            String accessToken;
            String expires;
            String isUpdate;
            if(resultMap != null) {
                accessToken = resultMap.get('accessToken');
                expires = resultMap.get('expires');
                isUpdate = resultMap.get('isUpdate');
            }

            api = new BoxAPIConnection(accessToken);
            System.debug('-------CollaborationId--------'+CollaborationId);
            BoxCollaboration collaboration = new BoxCollaboration(api, CollaborationId);
            collaboration.deleteCollaboration();
            
			// Update Box Access Token to Custom Setting
			Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);

            return true;
        } catch(Exception e) {
            System.debug('----Exception----'+e.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static Boolean checkPermission(string recordId){
		return Ltng_BoxAccess.checkPermission(recordId);
    }

    public class WUser{
        @AuraEnabled public boolean isRemovable{get;private set;}
        @AuraEnabled public string colaborationid{get;private set;}
        @AuraEnabled public string loginId{get;private set;}
        @AuraEnabled public string name{get;private set;}
        @AuraEnabled public string status{get;private set;}
        @AuraEnabled public string role{get;private set;}
        public WUser(boolean isRemovable, string colaborationid,string loginId,string name, string status, string role){this.isRemovable=isRemovable;this.colaborationid=colaborationid;this.loginId = loginId;this.name = name;this.status = status;this.role = role;}
    }
}
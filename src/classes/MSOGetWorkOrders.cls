/**********************************************************************************
* Author         : Ani Sinnakrar (VIT)
* Functionality  : This Apex class is REST API for MSO app 
                   HttpPatch - To update Accept/Reject status on WO
* User Story     : US5017
* Created On     : 10/10/14
*
***********************************************************************************/
@RestResource(urlMapping='/MSO/*')

global with sharing class MSOGetWorkOrders {
  
 /*   @HttpGet
    global static List<SVMXC__Service_Order__c> getWorkOrderByUserId(String pWO ) 
    {
        RestRequest req = RestContext.request;
        Id          strUserId = null ;
        
        String      strUserName = req.requestURI.substring( req.requestURI.lastIndexOf('/') + 1 );  
    
        if ( strUserName == null || strUserName == '' )
            strUserId = userinfo.getUserId() ;
        else
            strUserId = [ select id, username from user where username = :strUserName LIMIT 1 ].id ;
            
        List<SVMXC__Service_Order__c> lstOrders = [ select SVMXC__Company__c, Name, SVMXC__Company__r.name, SVMXC__Site__r.name, Assignment_Status__c, SVMXC__Dispatch_Status__c FROM SVMXC__Service_Order__c where name = :pWO ] ;
            
        if( lstOrders.size() > 0 )
            return lstOrders ;
        else
            return null;

        return lstOrders ;
    }
 */   

    @HttpPatch
    // Update WO Dispatch Reponse status or Technician status or Priority 
    global static String UpdateWOStatus( String pWONumber, String pAction, String pPriority) 
    {
        Id          strUserId = null ;
        System.debug('****** WO: ' + pWONumber + '--> Status : ' + pAction  ) ;  
        String      responseText ;  
        
        if ( pWONumber != '' && pWONumber != null )
        {
            try {
                SVMXC__Service_Order__c objWO = [SELECT Id, name, SVMXC__Dispatch_Response__c, SVMXC__Group_Member__r.Technician_Status__c from SVMXC__Service_Order__c WHERE name = :pWONumber];
    
                if( pAction != null && pAction != '' )
                {
                    if( pAction  == 'Assigned' || pAction  == 'Accepted' || pAction == 'Rejected' || pAction == 'Pending' || pAction == 'En Route' || pAction == 'On Site' || pAction == 'Completed' )
                    {
                        //if( ((pAction == 'En Route') || (pAction == 'On Site')) && (objWO.SVMXC__Group_Member__r.Technician_Status__c == pAction))
                        //{
                        //    responseText =  'The work order status can not be changed as you are already ' + ( objWO.SVMXC__Group_Member__r.Technician_Status__c ).toLowerCase() ;
                        //}    
                        //else    
                        //{
                            objWO.SVMXC__Dispatch_Response__c = pAction;
                            //Update Priority
                            if ( pPriority != '' && pPriority != null ) {
                                objWO.SVMXC__Priority__c = pPriority;  
                            }
                            update objWO;
                            //Narmada 5.20.16 updated "has beeen changed" to "has been changed"
                            responseText = pWONumber + ' has been updated';
                            //responseText = pWONumber + ' status has been changed to ' + ( objWO.SVMXC__Dispatch_Response__c ).toLowerCase() ;
                            System.debug('********** : ' + pWONumber + ' status has been changed to ' + ( objWO.SVMXC__Dispatch_Response__c ).toLowerCase()  ) ;
                        //}     
                    }   
                }
                else
                    responseText = 'Invalid WO Status' ;
                    
            } catch( exception e ) {
                responseText = 'Error : ' + + e.getMessage() ;  
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                return e.getMessage() ;
            }
        }
        else //Update Technician Status 
        {
            try {
                strUserId           = userinfo.getUserId() ;
                String strUserName  = userinfo.getFirstName() +' ' + userinfo.getLastName() ;
                
                // strUserId = [ select id, username from user where username = :strUserName LIMIT 1 ].id ;
                List<SVMXC__Service_Group_Members__c> lstTechlist = [ select name, RecordType_Name__c, Technician_Status__c from SVMXC__Service_Group_Members__c where user__c = :strUserId and RecordType.DeveloperName = 'Technician'] ;
                if(lstTechlist != null && lstTechlist.size() > 0) // added for INC3956093 on 06 Sept 2016
                {
                    if( pAction != null && pAction != '' )
                    {
                        lstTechlist[0].Technician_Status__c = pAction ;
                        update lstTechlist[0] ;
                        responseText = strUserName + ' status has beeen changed to ' + ( pAction ).toLowerCase() ;
                    }    
                }
                   
            } catch( exception e ) {
                responseText = 'Error : ' + + e.getMessage() ;  
                System.debug('An unexpected error has occurred: ' + e.getMessage());
                return e.getMessage() ;
            }
        }   
        return responseText ;
    }
    
}
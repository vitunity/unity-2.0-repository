/**********************************************************************************
* Author: Steve Hiller (VIT)
* Functionality: This Apex class is to test the REST API for MSO app 
* HttpPatch - To update Accept/Reject status on WO
* User Story: US5017
* Created On: 2/10/15
***********************************************************************************/

@isTest(SeeAllData=true)

global with sharing class MSOGetWorkOrdersTest {

   static testMethod void testRest() {
      
      // Instantiate objects here to make test class independent of org DB
   
      System.debug('TEST: SET UP DATA');
      
      // Populate and insert an Account 
   
      Account objAccount = new Account();

      objAccount.RecordType = [select Id from RecordType where Name = 'Standard' and SobjectType = 'Account' limit 1];
      objAccount.Name = 'MSOG Test Account';
      objAccount.Country__c = 'USA';
      objAccount.CurrencyIsoCode = 'USD';
      objAccount.OMNI_Postal_Code__c = '95118';
      objAccount.BillingPostalCode = '95035';
      insert objAccount;
      
      // Null and retrieve back from DB
      
      objAccount = null;   
      objAccount = [select Id, Name from Account where Name = 'MSOG Test Account' limit 1];
      System.debug('TEST: New Account is: '+(String)objAccount.Name);   
   
      // Populate and insert a Contact
      /*
      
   
      objContact.FirstName = 'MSOG Test';
      objContact.LastName = 'Contact';
      objContact.Contact_Type__c = 'Oncologist';
      objContact.CurrencyIsoCode = 'USD';
      objContact.Email = 'test.tester@testing.com';
      objContact.MailingState = 'CA';
      objContact.Phone= '1235678';
      objContact.MailingCountry = 'USA';
   
      insert objContact; */
   
      // Null and retrieve back from DB
      //Contact objContact;    
     // objContact = [select Id, Name from Contact where Name = 'MSOG Test Contact' limit 1];
      //System.debug('TEST: New Contact is: '+(String)ObjContact.Name);   
   
      // Populate and insert a Case 
      
      //Check added by Anupam - 05/08/15 as there are no Installation record type in the system.
      List<RecordType> instRecTypeCase = [select Id from RecordType where Name = 'INST' and SobjectType = 'Case' limit 1];
   
      Case objCase = new Case();        
      //Check added by Anupam - 05/08/15 as there are no Installation record type in the system.     
      if(instRecTypeCase.size() > 0 && instRecTypeCase != null){    
        objCase.RecordType = instRecTypeCase[0];
      }
      objCase.Status = 'New';
      objCase.AccountID = objAccount.Id;
      objCase.Priority = 'Medium';
      objCase.Subject = 'MSOG Test Case';
      //objCase.ContactId = objContact.id;
      insert objCase;
   
      // Null and retrieve back from DB
      
      objCase = null;       
   
      objCase = [select Id, Subject from Case where Subject = 'MSOG Test Case' limit 1];
      System.debug('TEST: New Case is: '+(String)objCase.Subject);    
   
      // Populate and insert a Work Order object

      //RecordType RecType= [select Id, Name from RecordType where Name = 'Field Service' and SobjectType = 'SVMXC__Service_Order__c' limit 1];
      //System.debug('TEST: Record Type = ' + (String) RecType.Id);
   
      SVMXC__Service_Order__c objWorkOrder = new SVMXC__Service_Order__c();   
      
      objWorkOrder.RecordTypeID = [select Id, Name from RecordType where Name = 'Field Service' and SobjectType = 'SVMXC__Service_Order__c' limit 1].Id;
      objWorkOrder.SVMXC__Order_Status__c = 'Assigned';
      objWorkOrder.SVMXC__Company__c = objAccount.Id;
      //objWorkOrder.SVMXC__Contact__c = objContact.Id;
      objWorkOrder.CurrencyIsoCode = 'USD';
      objWorkOrder.SVMXC__Priority__c = 'Low';   
      objWorkOrder.SVMXC__Case__c = objCase.Id;
    
      insert objWorkOrder;
   
      // Null and retrieve back from DB
      
      objWorkOrder = null;       
      objWorkOrder = [select Name, SVMXC__Order_Status__c, SVMXC__Priority__c, SVMXC__Dispatch_Response__c from SVMXC__Service_Order__c where SVMXC__Company__c =: objAccount.Id limit 1];
      System.debug('TEST: Work order is: '+(String)objWorkOrder.Name);
     
      // Populate and insert a Service Team object
     
      SVMXC__Service_Group__c objServiceGroup = new SVMXC__Service_Group__c();
   
      objServiceGroup.RecordType = [select Id from RecordType where Name = 'Technician' and SobjectType = 'SVMXC__Service_Group__c' limit 1];
      objServiceGroup.Name = 'MSOG Test Service Team';
   
      insert objServiceGroup;
      
      // Null and retrieve back from DB
      
      objServiceGroup = null;        
      objServiceGroup = [select Id, Name from SVMXC__Service_Group__c where Name = 'MSOG Test Service Team' limit 1];
      System.debug('TEST: New Service Group is: '+(String)objServiceGroup.Name);    
     
      // Populate and insert a Technician/Equipment object        
  
      SVMXC__Service_Group_Members__c objServiceGroupMember = new SVMXC__Service_Group_Members__c();
   
      objServiceGroupMember.RecordTypeId = [select Id from RecordType where Name = 'Technician' and SobjectType = 'SVMXC__Service_Group_Members__c' limit 1].Id;
      objServiceGroupMember.Name = 'MSOG Test Technician';
      objServiceGroupMember.SVMXC__Service_Group__c = objServiceGroup.Id;
      objServiceGroupMember.SVMXC__Country__c ='United States';
      objServiceGroupMember.user__c = userinfo.getUserId() ;
   
      insert objServiceGroupMember;    

      // Null and retrieve back from DB
      
      objServiceGroupMember = null;          
      objServiceGroupMember = [select Id, Name, Technician_Status__c from SVMXC__Service_Group_Members__c where Name = 'MSOG Test Technician' limit 1];
      System.debug('TEST: New Service Group Member is: '+(String)objServiceGroupMember.Name);  
  
      // Set up context for the request object
   
      RestRequest req = new RestRequest();
      RestResponse res = new RestResponse(); 
          
      req.requestURI = '/MSO/Test';  //Request URL
      req.httpMethod = 'PATCH';      //HTTP Request Type
    
      req.addParameter('WONumber', objWorkOrder.Name);     
      req.addParameter('Action', objWorkOrder.SVMXC__Order_Status__c);
      req.addParameter('Priority', objWorkOrder.SVMXC__Priority__c);      
          
      RestContext.request = req;
      RestContext.response = res;
    
      // Test MSOGetWorkOrders.UpdateWOStatus with the various scenarios    
   
      System.debug('TEST: Data set up. Begin testing.');
    
      // Call with Work Order and update Dispatch Respose and Priority
      // Expect successful update of Work Order
      
      System.debug('TEST: EXPECTED RESULT -- Successful update of work order to Dispatch Response: En Route and Priority: High');
      System.debug('TEST: ACTUAL RESULT BEFORE -- Dispatch Response: '+objWorkOrder.SVMXC__Dispatch_Response__c+' and Priority of '+objWorkOrder.SVMXC__Priority__c);
      String Result = MSOGetWorkOrders.UpdateWOStatus(objWorkOrder.Name, 'En Route', 'High');
      System.debug('TEST: Method is called with result: '+Result);
      objWorkOrder = [select Name, SVMXC__Priority__c, SVMXC__Dispatch_Response__c from SVMXC__Service_Order__c where SVMXC__Company__c =: objAccount.Id limit 1];
      System.debug('TEST: ACTUAL RESULT AFTER -- Dispatch Response: '+objWorkOrder.SVMXC__Dispatch_Response__c+' and Priority of '+objWorkOrder.SVMXC__Priority__c);
      
      // Call without Work Order Status
      // Expect error message
      
      System.debug('TEST: EXPECTED RESULT: Error due to missing Dispatch Response parameter');      
      Result = MSOGetWorkOrders.UpdateWOStatus(objWorkOrder.Name, '', 'High');
      System.debug('TEST: ACTUAL RESULT: Method is called with result: '+Result);
      
      // Call with unallowed Work Order Status
      // Expect error message
      
      System.debug('TEST: EXPECTED RESULT -- Error due to invalid Dispatch Response parameter');         
      
      Result = MSOGetWorkOrders.UpdateWOStatus(objWorkOrder.Name, 'Wrong', 'High');
      Result = MSOGetWorkOrders.UpdateWOStatus(null, 'Wrong', 'High');
      System.debug('TEST: ACTUAL RESULT: Method is called with result: '+Result);
      
      // Output to show No Work Order case
      // Call with no Work Order
      // Expect Service Group Member Technician Status update
      
      System.debug('TEST: EXPECTED RESULT: Group Member Status updated instead of Work Order when no Work Order Name is specified');  
    
      System.debug('TEST: ACTUAL RESULT BEFORE: Service Group Member is: '+objServiceGroupMember.Name+' with Technician Status of: '+objServiceGroupMember.Technician_Status__c);
      String ResultNoWO = MSOGetWorkOrders.UpdateWOStatus('', 'En Route', 'High');
      System.debug('TEST: ACTUAL RESULT AFTER: Method is called with result: '+ResultNoWO);
      objServiceGroupMember = [select Id, Name, Technician_Status__c from SVMXC__Service_Group_Members__c where Name = 'MSOG Test Technician' limit 1];
      System.debug('TEST: ACTUAL RESULT AFTER: Service Group Member is: '+objServiceGroupMember.Name+' with Technical Status of: '+objServiceGroupMember.Technician_Status__c);        
    }
}
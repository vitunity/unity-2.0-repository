/*
* Author: Naren Yendluri
* Created Date: 18-Aug-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website LungPhanthom page
*/



public with sharing class MVLungPhantom {
    //This Constructor run to set the user information for current context
    public static Contact c{get;set;}
    public static String usrContId{get;set;} 
    public static boolean IsSave{get;set;}
    public static boolean displayPopup {get; set;}     
    public String datename { get; set; }
    //This Method is used to list the data from Lung_Phantom__c object 
    public static list<Lung_Phantom__c> lLungPhantRecs{get;set;}
    
    //To get Custom labels
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        /*map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        return customLabelMap;*/
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<Lung_Phantom__c> confirmPopup(String voucherID){
        getlLPHRec();
        //String VoucherID = ApexPages.currentPage().getParameters().get('voccidparam');
        displayPopup = false; 
        Lung_Phantom__c lP = new Lung_Phantom__c();
        // Set<ID> sIDs = new Set<ID>();
        //String VoucherID = ApexPages.currentPage().getParameters().get('voccidparam');
        
        System.debug('VochreId'+voucherID);
        for(Lung_Phantom__c l : lLungPhantRecs){
            if(l.Id == voucherID){
                l.Date_Redeemed__c = System.now().date();
                l.Redeemed_By__c = c.name;    
                lp = l;                
            }
        }
        
        if(lLungPhantRecs != null)
            update lLungPhantRecs;
        
        return lLungPhantRecs;
    }
    
    @AuraEnabled
    public static List<Lung_Phantom__c> saveLungPhantomRec(List<Lung_Phantom__c> lungPhantRecs){
        system.debug('lungPhantRecs========'+lungPhantRecs);
        if(lungPhantRecs != null){
            update lungPhantRecs;
        }
        getlLPHRec();
        
        return lLungPhantRecs;
    }
    
    //public String VoucherID{get;set;}
    
    public String getLiEDate1() {
        return null;
    }
    
    public String getLiEDate() {
        return null;
    }
    
    //This Method is used to redirect the page if condition is true.
    public PageReference MDADLContact(){
        if((usrContId != null) && (c.Is_MDADL__c == true))
        {
            PageReference pg = new PageReference('/apex/cpMDADLLungPhantum');
            pg.setRedirect(true);
            return pg;
            
        }    
        else
            return null;
    }
    
    public static void LungPhantom(){
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];    
        //  User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
        
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        c = new Contact();
        if(usrContId != null){
            c = [select Id,Is_MDADL__c,name,FirstName, LastName, Account.name, Email, phone, Title, 
                 MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity 
                 from contact 
                 where Id =: usrContId];
        }
    }
    
    @AuraEnabled
    public static List<Lung_Phantom__c> getlLPHRec(){
        lLungPhantRecs = new List<Lung_Phantom__c>();
        LungPhantom();
        SYSTEM.DEBUG('testcontact--->'+c.id);
        if(usrContId != null || Test.isRunningTest()){
            lLungPhantRecs = [Select Id,name,Voucher_ID__c,Date_Redeemed__c,Date_Exposed_Phantom_Received_by_MDADL__c,
                              Date_Results_Reported_by_MDADL__c,Installed_Product__r.name,Date_Received__c, Redeemed_By__c,
                              Product_Name__c,Installed_Product__c,Serial_Lot_Number__c, Expiration_Date_Ends__c 
                              from Lung_Phantom__c 
                              where contact__c = :c.id 
                              order by name desc];
            if(lLungPhantRecs.size()>0){
                IF(lLungPhantRecs[0].Date_Redeemed__c!=NULL){
                    IsSave=true;
                }
            }
            else{
                IsSave=false;
            }
        }
        return lLungPhantRecs ;
    }
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
}
@isTest
private class MVLungPhantomTest{
    public static boolean IsSave;
    public static String datename;
    
    
    @isTest static void test1() {
        test.startTest();        
        MVLungPhantom.getCustomLabelMap('en');
        test.stopTest();
    } 
    
    @isTest static void test2() {
        test.startTest(); 
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];   
        string usrContId =  usr.contactid; 
            
        MVLungPhantom.LungPhantom();
        test.stopTest();
    }   
    
    @isTest static void test3() {
        test.startTest();        
            
            IsSave = true;
            datename = 'Test';

            Account a;   
                  
            a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
            insert a;  
            
            Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='test1.fydtr667testhklf@gmail.com', AccountId=a.Id, 
                                MailingCountry ='USA', MailingState='CA' ); 
            insert con;
            
            String usrContId = con.Id ;

            SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
              Name = 'H192051'// Installed Product ID
            );
            insert insP;
             
            Lung_Phantom__c sobj = new Lung_Phantom__c(
              Contact__c = con.Id,                                       // Contact
              Installed_Product__c = insP.Id,                               // Installed Product
              Voucher_ID__c = '428385',                                               // Voucher ID
              //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
              Redeemed_By__c = 'Gopika Yasuda'                                       // Redeemed By
              //Date_Exposed_Phantom_Received_by_MDADL__c = Date.valueOf('1-16-2015'),  // Date Exposed  Phantom Received
              //Date_Results_Reported_by_MDADL__c = Date.valueOf('2-13-2015')          // Date Results Reported
            );
            insert sobj;
            
            MVLungPhantom.getlLPHRec();
        test.stopTest();
    }
    
    @isTest static void test4() {
       
        test.startTest();
        PageReference pref = new PageReference('/apex/cpMDADLLungPhantum');
        Test.setCurrentPage(pref);  
        MVLungPhantom lp = new MVLungPhantom();   
        lp.MDADLContact();
        test.stopTest();
    }
    
     @isTest static void test5() {
        test.startTest();
        MVLungPhantom lp = new MVLungPhantom();           
        lp.closePopup();
        test.stopTest();
    }
    
     @isTest static void test6() {
        test.startTest(); 
        MVLungPhantom lp = new MVLungPhantom();       
        lp.showPopup();
        test.stopTest();
    }
    @isTest static void test7() {
        test.startTest(); 
        MVLungPhantom lp = new MVLungPhantom();       
        lp.getLiEDate1();
        test.stopTest();
    }
    @isTest static void test8() {
        test.startTest(); 
        MVLungPhantom lp = new MVLungPhantom();       
        lp.getLiEDate();
        test.stopTest();
    }
    @isTest static void test9() {
        list<Lung_Phantom__c> lpList = new list<Lung_Phantom__c>();
        Account a;   
              
        a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
        insert a;  
        
        Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
            MailingCountry ='USA', MailingState='CA' ); 
        insert con;
        
        SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
          Name = 'H192051'// Installed Product ID
        );
        insert insP;
         
        Lung_Phantom__c sobj = new Lung_Phantom__c(
          Contact__c = con.Id,                                       // Contact
          Installed_Product__c = insP.Id,                               // Installed Product
          Voucher_ID__c = '428385',                                               // Voucher ID
          //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
          Redeemed_By__c = 'Gopika Yasuda'                                       // Redeemed By
          //Date_Exposed_Phantom_Received_by_MDADL__c = Date.valueOf('1-16-2015'),  // Date Exposed  Phantom Received
          //Date_Results_Reported_by_MDADL__c = Date.valueOf('2-13-2015')          // Date Results Reported
        );
        insert sobj;
        lpList.add(sobj);
        test.startTest(); 
            MVLungPhantom.saveLungPhantomRec(lpList);
        test.stopTest();
    }
    
    @isTest static void test10() {
        test.startTest(); 
            IsSave = true;
            datename = 'Test';

            Account a;   
                  
            a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
            insert a;  
            
            Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
                                MailingCountry ='USA', MailingState='CA' ); 
            insert con;
            String usrContId = con.Id;
            
            SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
              Name = 'H192051'// Installed Product ID
            );
            insert insP;
            
            Lung_Phantom__c sobj = new Lung_Phantom__c(
              Contact__c = con.Id,                                       // Contact
              Installed_Product__c = insP.Id,                               // Installed Product
              Voucher_ID__c = '428385',                                               // Voucher ID
              //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
              Redeemed_By__c = 'Gopika Yasuda'                                       // Redeemed By
              //Date_Exposed_Phantom_Received_by_MDADL__c = Date.valueOf('1-16-2015'),  // Date Exposed  Phantom Received
              //Date_Results_Reported_by_MDADL__c = Date.valueOf('2-13-2015')          // Date Results Reported
            );
            
            
            //MVLungPhantom.getlLPHRec();
            MVLungPhantom.confirmPopup(sobj.Voucher_ID__c);
        test.stopTest();
    }
    
    
    
}
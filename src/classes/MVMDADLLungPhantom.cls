/*
* Author: Naren Yendluri
* Created Date: 22-Aug-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website LungPhanthom page
*/


public without sharing class MVMDADLLungPhantom {
    public static String datename {get; set;}
    public static Contact c{get;set;}
    public static String usrContId{get;set;}
    public static list<Lung_Phantom__c> lLungPhantRecs{get;set;}
    public static list<MVMDADLLungPhantomWrapper> wLungPhantRecs{get;set;}
    
    //To get Custom labels
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        return customLabelMap;
    }
    
    
    @AuraEnabled
    public static List<MVMDADLLungPhantomWrapper> saveLungPhantomRec(List<MVMDADLLungPhantomWrapper> 
                                                                     wLungPhantList){
        system.debug('wLungPhantList====='+wLungPhantList);
        List<Lung_Phantom__c> lLungPhantUpdate = new List<Lung_Phantom__c>();
        for(MVMDADLLungPhantomWrapper w: wLungPhantList){
            w.lung.Date_Exposed_Phantom_Received_by_MDADL__c = w.dateExposed;
            w.lung.Date_Results_Reported_by_MDADL__c = w.dateResults;

            lLungPhantUpdate.add(w.lung);
        }

        if(!lLungPhantUpdate.isEmpty())
            update lLungPhantUpdate;            
        return new List<MVMDADLLungPhantomWrapper> ();
    }
    
    @AuraEnabled
    public static List<MVMDADLLungPhantomWrapper> saveLungPhantomRecNew(List<Lung_Phantom__c> wLungPhantListNew){
        system.debug('#### debug wLungPhantListNew 1 = ' + wLungPhantListNew);
        if(!wLungPhantListNew.isEmpty()) {
            for(Lung_Phantom__c lp : wLungPhantListNew) {
                if(lp.Date_Results_Reported_by_MDADL__c != null
                    && lp.Date_Exposed_Phantom_Received_by_MDADL__c != null) {
                    lp.Date_Redeemed__c = System.Today();
                    lp.Redeemed_By__c = UserInfo.getName();
                }
            }
            update wLungPhantListNew;            
        }
        return new List<MVMDADLLungPhantomWrapper> ();
    }
    
    public static void MVMDADLLungPhantom (){
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        if(usrContId != null){
            c = [select Is_MDADL__c,name,FirstName, LastName, Account.name, Email, phone, Title, 
                 MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity 
                 from contact where Id =: usrContId];
        }        
    } 
    
    @AuraEnabled
    public static List<MVMDADLLungPhantomWrapper> getlLPHRec(String searchKeyWord, String tab){
        lLungPhantRecs = new List<Lung_Phantom__c>();
        wLungPhantRecs = new List<MVMDADLLungPhantomWrapper>();
        if(String.isBlank(tab)){
            tab = 'open';
        }
        
        String query = 'Select Id,Name,Voucher_ID__c,contact__c,Date_Results_Reported_by_MDADL__c, '
                       +'   Date_Exposed_Phantom_Received_by_MDADL__c,Date_Redeemed__c,Date_Received__c,'
                       +'   Redeemed_By__c,Product_Name__c,Installed_Product__c,Expiration_Date_Ends__c '
                       +'   from Lung_Phantom__c where Id != null';
        
        try{
        if(tab == 'open'){
            query += ' and (Date_Results_Reported_by_MDADL__c = null or '
                     +' Date_Exposed_Phantom_Received_by_MDADL__c = null) ';
                
        }else if(tab == 'closed'){
            query +=' and (Date_Results_Reported_by_MDADL__c != null and '
                    +' Date_Exposed_Phantom_Received_by_MDADL__c != null) ';
        }
        
        if(String.isNotBlank(searchKeyWord)){
            searchKeyWord = '%' + searchKeyWord + '%';
            query +='  and (Voucher_ID__c like : searchKeyWord or Redeemed_By__c like : searchKeyWord or Contact__r.Account.Name like : searchKeyWord)';
        }
        query += ' Order by name ';
        lLungPhantRecs = Database.query(query); 
        system.debug('#### debug lLungPhantRecs = ' + lLungPhantRecs.size());
        Set<ID> sIDs = new Set<ID>();
        
        for(Lung_Phantom__c l : lLungPhantRecs){
            sIDs.add(l.contact__c);
        }
        
        Map<ID, Contact> map_ID_Contact = new Map<ID,Contact>([select Is_MDADL__c,name,FirstName, LastName, Account.name, Email, 
                                                               phone, Title, MailingStreet, MailingState, MailingPostalCode, 
                                                               MailingCountry, MailingCity 
                                                               from contact 
                                                               where Id in :sIDs]);
        
        /*
        for(Lung_Phantom__c l : lLungPhantRecs){
            Contact con = map_ID_Contact.get(l.contact__c);
            if(con !=null){
              wLungPhantRecs.add(new MVMDADLLungPhantomWrapper(l, con,con.Account.name));  
            }            
        }
        */

        for(Lung_Phantom__c l : lLungPhantRecs){
            Contact con = map_ID_Contact.get(l.contact__c);
            if(con !=null){
                wLungPhantRecs.add(new MVMDADLLungPhantomWrapper(l, con,con.Account.name));  
            } else {
                wLungPhantRecs.add(new MVMDADLLungPhantomWrapper(l, null,null));
            }          
        }        
            
        }catch (Exception ex){
            system.debug('Exception--->'+ex.getMessage()); 
        }
        return wLungPhantRecs;
    }
    
    public class MVMDADLLungPhantomWrapper{
        @AuraEnabled public Lung_Phantom__c lung{get; set;}
        @AuraEnabled public contact con{get; set;}
        @AuraEnabled public string accName{get; set;}
        @AuraEnabled public date dateExposed{get; set;}
        @AuraEnabled public date dateResults{get; set;}
        
        public MVMDADLLungPhantomWrapper(Lung_Phantom__c l, Contact con, string accName){
            this.lung=l;
            this.con = con;
            this.accName=accName;
            this.dateExposed = l.Date_Exposed_Phantom_Received_by_MDADL__c;
            this.dateResults = l.Date_Results_Reported_by_MDADL__c;
        }         
    }   
    
}
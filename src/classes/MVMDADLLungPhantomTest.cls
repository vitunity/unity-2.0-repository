@isTest
private class MVMDADLLungPhantomTest{
    @isTest static void test1() {
        test.startTest();        
        MVMDADLLungPhantom.getCustomLabelMap('en');
        test.stopTest();
    } 
    @isTest static void test2() {
        test.startTest();        
        MVMDADLLungPhantom.MVMDADLLungPhantom();
        test.stopTest();
    } 
    
    @isTest static void test3() {
        test.startTest();        
        MVMDADLLungPhantom.getlLPHRec('test','open');
        test.stopTest();
    }
     @isTest public static void test4(){
        
        Account a;   
              
        a = new Account( Territories1__c = 'Test Terrotory', Name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
        insert a;  
        
        Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
            MailingCountry ='USA', MailingState='CA' ); 
        insert con;
        
        SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
          Name = 'H192051'// Installed Product ID
        );
        insert insP;
         
        Lung_Phantom__c sobj = new Lung_Phantom__c(
          Contact__c = con.Id,                                       // Contact
          Installed_Product__c = insP.Id,                               // Installed Product
          Voucher_ID__c = '428385',                                               // Voucher ID
          //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
          Redeemed_By__c = 'Gopika Yasuda' ,                                      // Redeemed By
          Date_Exposed_Phantom_Received_by_MDADL__c = system.today(),  // Date Exposed  Phantom Received
          Date_Results_Reported_by_MDADL__c = system.today()          // Date Results Reported
        );
        insert sobj;
       
        MVMDADLLungPhantom.MVMDADLLungPhantomWrapper wrapper= new  MVMDADLLungPhantom.MVMDADLLungPhantomWrapper(sobj, con, a.Name);
        wrapper.lung = sobj;
        wrapper.con = con;
        wrapper.accName = a.Name;
        wrapper.dateExposed = system.today();
        wrapper.dateResults = system.today();
    }
    
    @isTest public static void test5(){
        list<Lung_Phantom__c> lp = new list<Lung_Phantom__c>();
        Account a;   
              
        a = new Account( Territories1__c = 'Test Terrotory', Name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
        insert a;  
        
        Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
            MailingCountry ='USA', MailingState='CA' ); 
        insert con;
        
        SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
          Name = 'H192051'// Installed Product ID
        );
        insert insP;
         
        Lung_Phantom__c sobj = new Lung_Phantom__c(
          Contact__c = con.Id,                                       // Contact
          Installed_Product__c = insP.Id,                               // Installed Product
          Voucher_ID__c = '428385',                                               // Voucher ID
          //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
          Redeemed_By__c = 'Gopika Yasuda' ,                                      // Redeemed By
          Date_Exposed_Phantom_Received_by_MDADL__c = system.today(),  // Date Exposed  Phantom Received
          Date_Results_Reported_by_MDADL__c = system.today()          // Date Results Reported
        );
        insert sobj;
        lp.add(sobj);
        MVMDADLLungPhantom.MVMDADLLungPhantomWrapper wrapper= new  MVMDADLLungPhantom.MVMDADLLungPhantomWrapper(sobj, con, a.Name);
        wrapper.lung = sobj;
        wrapper.con = con;
        wrapper.accName = a.Name;
        wrapper.dateExposed = system.today();
        wrapper.dateResults = system.today();
        string str = JSON.serialize([SELECT Contact__c, Id , Installed_Product__c , Redeemed_By__c , Date_Exposed_Phantom_Received_by_MDADL__c  FROM Lung_Phantom__c WHERE Id = : sobj.Id]);
        system.debug('==str=='+str);
        test.startTest(); 
        try{       
        MVMDADLLungPhantom.saveLungPhantomRecNew(lp);
        }catch(exception e){
        }
        test.stopTest();
        
    }
    
     @isTest public static void test6(){
        list<MVMDADLLungPhantom.MVMDADLLungPhantomWrapper> lp = new list<MVMDADLLungPhantom.MVMDADLLungPhantomWrapper>();
        Account a;   
              
        a = new Account( Territories1__c = 'Test Terrotory', Name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
        insert a;  
        
        Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
            MailingCountry ='USA', MailingState='CA' ); 
        insert con;
        
        SVMXC__Installed_Product__c insP = new SVMXC__Installed_Product__c(
          Name = 'H192051'// Installed Product ID
        );
        insert insP;
         
        Lung_Phantom__c sobj = new Lung_Phantom__c(
          Contact__c = con.Id,                                       // Contact
          Installed_Product__c = insP.Id,                               // Installed Product
          Voucher_ID__c = '428385',                                               // Voucher ID
          //Date_Redeemed__c = Date.valueOf('11-20-2017'),                          // Date Redeemed
          Redeemed_By__c = 'Gopika Yasuda' ,                                      // Redeemed By
          Date_Exposed_Phantom_Received_by_MDADL__c = system.today(),  // Date Exposed  Phantom Received
          Date_Results_Reported_by_MDADL__c = system.today()          // Date Results Reported
        );
        insert sobj;
        
        MVMDADLLungPhantom.MVMDADLLungPhantomWrapper wrapper= new  MVMDADLLungPhantom.MVMDADLLungPhantomWrapper(sobj, con, a.Name);
        wrapper.lung = sobj;
        wrapper.con = con;
        wrapper.accName = a.Name;
        wrapper.dateExposed = system.today();
        wrapper.dateResults = system.today();
        lp.add(wrapper);
        test.startTest(); 
        //try{       
        MVMDADLLungPhantom.saveLungPhantomRec(lp);
        //}catch(exception e){
        //}
        test.stopTest();
        
    }
    
}
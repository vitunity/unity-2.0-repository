public without sharing class MVMonteCarloRegisterController {
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        system.debug('#### debug languageObj = ' + languageObj);
        return MvUtility.getCustomLabelMap(languageObj); 
    }
    
    @AuraEnabled
    public static list<MyVarian_Agreement_Translation__c> getMyVarianAgreement(string name, String languageCode){
        if(String.isBlank(languageCode) ){
            languageCode = 'en_US';
        }
        system.debug('name===='+name);
        system.debug('languageCode===='+languageCode);
        return [select Agreement_Text__c, Language__c 
                from MyVarian_Agreement_Translation__c
                where MyVarian_Agreement__r.Name =: name
                and Language__c =: languageCode
               order by Name ];
    }
    
    @AuraEnabled
    public static Contact getContact(){
        list<Contact> contactList = [select Monte_Carlo_Registered__c,FirstName, LastName, Account.name, Email, phone, 
                Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity 
                from contact where id in (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId())];
        if(contactList.isEmpty()){
            Contact contactObj = new Contact();
            contactObj.LastName = 'Internal Contact';
            return contactObj;      
        }else{
            return contactList[0];
        }
    }
    
    @AuraEnabled
    public static String registerMData(String contactId, Monte_Carlo_Registration__c registerDataObj,String projectLenght,String projectLenghtType,String mtype){
        system.debug('contactObj data==='+contactId);
        system.debug('registerDataObj data==='+registerDataObj);
        system.debug('mtype data==='+mtype);
        if(contactId != null){
            registerDataObj.Contact__c = contactId;
        }
        registerDataObj.Project_Length__c = '' + projectLenght + ' ' + projectLenghtType ;
        insert registerDataObj;
        list<Contact> conObjList = [select id, Monte_Carlo_Registered__c from Contact where id = : contactId];
        if(!conObjList.isEmpty()){
            if(conObjList[0].Monte_Carlo_Registered__c == null){
                conObjList[0].Monte_Carlo_Registered__c = mtype;
            }else{
                conObjList[0].Monte_Carlo_Registered__c += ';'+mtype;
            }
            update conObjList;
        }
        system.debug('registerDataObj data::after insert'+registerDataObj);
        if(!Test.isRunningTest()){
            publicgroupadd();
        }
        return 'accessdocs';
    }
       
    @future
    public static void publicgroupadd(){
        Group grp = [Select id from group where developername ='Monte_Carlo'];
                    GroupMember gm = new groupMember(GroupId = grp.id,UserOrGroupId = UserInfo.getUserId());
                    insert gm;
    } 
    
    @AuraEnabled
    public static List<MyWrapper> accessDocs(string pageName){ 
        Map<String, List<Monte_Carlo__c>> map_Title_Mc = new Map<String, List<Monte_Carlo__c>>();
        List<Monte_Carlo__c> lMonteCarlo = [SELECT Description__c,Id,Name,Title__c,Type__c FROM Monte_Carlo__c WHERE Type__c = :pageName];
        for(Monte_Carlo__c c : lMonteCarlo){
            if(map_Title_Mc.containsKey(c.Title__c)){
                List<Monte_carlo__c> lMc = map_Title_Mc.get(c.Title__c);
                lMc.add(c);
                map_Title_Mc.remove(c.Title__c);
                map_Title_Mc.put(c.Title__c, lMc);
            }else{
                List<Monte_carlo__c> lMc = new List<Monte_Carlo__c>();
                lMc.add(c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
        }
        String FileType;
        if(pageName=='TrueBeam') {
            FileType='Photon';
        }
         
        List<ContentVersion> lContentVersion;
        if(pageName=='TrueBeam'){  
            lContentVersion= [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber 
                              FROM ContentVersion where Monte_Carlo__c != null and File_Type__c=:FileType  order by VersionNumber Desc];
        }else{
            lContentVersion = [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber 
                               FROM ContentVersion where Monte_Carlo__c != null   order by VersionNumber Desc];
        }
         
        set<string> stContentdocid=new set<string>();
        Map<ID, List<ContentVersion>> map_Mc_ContentVer = new Map<ID, List<ContentVersion>>();
       
        for(ContentVersion  c : lContentVersion){
            If(!stContentdocid.contains(c.ContentDocumentId)) {
                if(!map_Mc_ContentVer.containsKey(c.Monte_Carlo__c)){
                    List<ContentVersion> lMc = new List<ContentVersion>();
                    lMc.add(c);
                    map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                }else{  
                    List<ContentVersion> lMc = map_Mc_ContentVer.get(c.Monte_Carlo__c);
                    lMc.add(c);
                    map_Mc_ContentVer.remove(c.Monte_Carlo__c);
                    map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                }
                stContentdocid.add(c.ContentDocumentId);
            }      
        }
        
        Set <String> sTitleKeys = new Set<String>();
        sTitleKeys = map_Title_Mc.keySet();
        List<MyWrapper>  lTitle_ContentVer = new List<MyWrapper>();
        for(String s : sTitleKeys) {
            List<ContentVersion> lContent = new List<ContentVersion>();
            List<Monte_Carlo__c> lMonte = map_Title_Mc.get(s);
            if(lMonte != null){
                for(Monte_Carlo__c mc : lMonte){
                    List<ContentVersion> lc = new List<Contentversion>();
                    lc = map_Mc_ContentVer.get(mc.id);
                    if(lc != null)
                        lContent.addAll(lc);
                }  
            }
            //system.assertEquals(1,lContent.size());
            lTitle_ContentVer.add(new MyWrapper(s,lContent));
         }    
         return lTitle_ContentVer;
     }
    
     public Class MyWrapper{
         @AuraEnabled
         public String TitleVal {get;set;}
         
         @AuraEnabled
         public List<ContentVersion> lCont {get;set;}
         
         public MyWrapper(String s, List<ContentVersion> ls){
             TitleVal = s;
             lcont = ls;
         }
     }
     
    
}
@isTest
private class MVMonteCarloRegisterControllerTest{
    @isTest static void test1() {
        test.startTest();        
        MVMonteCarloRegisterController.getCustomLabelMap('en');
        test.stopTest();
    } 
    
    @isTest static void test2() {
        test.startTest();        
        MVMonteCarloRegisterController.getMyVarianAgreement('MONTE_CARLO', 'en_US');
        test.stopTest();
    }
    
    @isTest static void test3() {
        test.startTest();        
        MVMonteCarloRegisterController.getContact();
        test.stopTest();
    }
    
   @isTest static void registerMDataTest() {
    test.startTest();
          User thisUser = [ select Id from User where ContactId != null Limit 1];    // = :UserInfo.getUserId() ];
          //System.runAs ( thisUser ) {
              Account a;   
              
              a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
              insert a;  
              
              Contact con = new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
              
              MailingCountry ='USA', MailingState='CA' ); 
              insert con;
              
              Contact c1 = new Contact();
              c1 = [Select Id, FirstName from Contact where FirstName =: 'Singh'];
              string conId = con.Id;
              
              Monte_Carlo_Registration__c mc;
              mc = new Monte_Carlo_Registration__c( Co_Investigators__c = '5555', Contact__c = con.Id, Intended_Use__c ='12235', Principal_Investigator__c ='5675', Project_Length__c ='1 Month'); 
              //insert mc;  
              //try{
                  MVMonteCarloRegisterController.registerMData(conId  , mc, '1', 'month', 'Clinac');
              //}catch(exception e){
              //}
          //}
        test.stopTest();    
    }
    @isTest static void test4() {
        test.startTest();        
        MVMonteCarloRegisterController.publicgroupadd();
        test.stopTest();
    }
    @isTest static void test5() {
        test.startTest();
        Monte_Carlo__c sobj = new Monte_Carlo__c(
          Title__c = 'Monte Carlo Low Energy Accelerator',   // Title
          Type__c = 'Clinac'                               // Type
        );
        insert sobj;
        
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        ContentVersion parentContent;
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       Monte_Carlo__c = sobj.Id,
                                                       File_Type__c = 'Clinac',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;
                
        MVMonteCarloRegisterController.accessDocs('Clinac'); 
        test.stopTest();
    }
     @isTest static void test6() {
        test.startTest(); 
        Monte_Carlo__c sobj = new Monte_Carlo__c(
          Title__c = 'Monte Carlo Low Energy Accelerator',   // Title
          Type__c = 'TrueBeam'                               // Type
        );
        insert sobj;
        
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        ContentVersion parentContent;
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       Monte_Carlo__c = sobj.Id,
                                                       File_Type__c = 'TrueBeam',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;       
        
        MVMonteCarloRegisterController.accessDocs('TrueBeam'); 
        test.stopTest();
    }
    
    @isTest static void test7() {
        test.startTest();
        list<ContentVersion> wrapperList = new list<ContentVersion>();
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        ContentVersion parentContent;
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;
        wrapperList.add(parentContent);        
        MVMonteCarloRegisterController.MyWrapper wpr = new MVMonteCarloRegisterController.MyWrapper('Test',wrapperList);
        test.stopTest();
    }
    
}
/*
* Author: Naren Yendluri
* Created Date: 04-Sep-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website Partner Channel page
*/



public class MVProductDocumentationPartIntUsers {
    static integer counter=0;  //keeps track of the offset
    static integer list_size=20; //sets the page size or number of rows
    static integer total_size; //used to show user the total size of the list
    static String gblStrBanner='';        
    static string strBanner ='';
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<picklistEntry> getPicklistValues(String fieldName,String sObjectName){
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(sObjectName);         
        Schema.DescribeSObjectResult sObjRes = sObj.getDescribe();
        Schema.DescribeFieldResult sObjFieldRes = sObjRes.fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> lstPlE = sObjFieldRes.getPicklistValues();
        List<picklistEntry> entryList = new List<picklistEntry>();
        for(Schema.PicklistEntry ple : lstPlE){
            entryList.add(new picklistEntry(ple.getLabel(),ple.getValue()));
        }
        return entryList;
    }
    
    //For Product Info.   
    @AuraEnabled
    Public static List<picklistEntry> getProdOptions(){  
        set<string> ProductGroupSet = new set<String>();
        List<picklistEntry> lstProdOptions = new List<picklistEntry>();
        
        for(ContentVersion  cv :[select ContentDocumentId,Product_Group__c from ContentVersion 
                                 where id != null and IsLatest = true and Parent_Documentation__c = null 
                                 and RecordType.name ='Partner Document' ]){
                                     if(cv.Product_Group__c!=null){
                                         productGroupSet.addAll(cv.Product_Group__c.split(';'));
                                     }
                                 }
        for(string sr : ProductGroupSet) {
            lstProdOptions.add(new picklistEntry(sr,sr));
        }        
        // options.sort();
        return lstProdOptions; 
    }
    
    @AuraEnabled    
    Public static List<picklistEntry> getDocTypes(){        
        List<picklistEntry> lstDocTypes = new List<picklistEntry>();
        
        Schema.DescribeFieldResult fieldResult = ContentVersion.Document_Type2__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry ple : lstPLE){            
            lstDocTypes.add(new picklistEntry(ple.getLabel(),ple.getValue()));
        }
        return lstDocTypes;   
    }
    
    @AuraEnabled    
    Public static List<picklistEntry> getRegions(){        
        List<picklistEntry> lstRegions= new List<picklistEntry>();
        
        Schema.DescribeFieldResult fieldResult = ContentVersion.Region__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry ple : lstPLE){ 
            if(ple.getvalue()!='All') {
                lstRegions.add(new picklistEntry(ple.getLabel(),ple.getValue()));
            }
        }
        return lstRegions;   
    }
    
    @AuraEnabled    
    Public static List<picklistEntry> getMaterialTypes(){        
        List<picklistEntry> lstMaterialTypes = new List<picklistEntry>();
        
        Schema.DescribeFieldResult fieldResult = ContentVersion.Material_Type__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry ple : lstPLE){            
            lstMaterialTypes.add(new picklistEntry(ple.getLabel(),ple.getValue()));
        }
        return lstMaterialTypes;   
    }
    
    @AuraEnabled    
    Public static List<picklistEntry> getTreatmentTechniques(){        
        List<picklistEntry> lstTreatmentTechniques = new List<picklistEntry>();
        
        Schema.DescribeFieldResult fieldResult = ContentVersion.Treatment_Technique__c.getDescribe();
        List<Schema.PicklistEntry> lstPLE = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry ple : lstPLE){            
            lstTreatmentTechniques.add(new picklistEntry(ple.getLabel(),ple.getValue()));
        }
        return lstTreatmentTechniques;   
    }
    
    @AuraEnabled
    public static List<ContentVersion> getContentVersions(string RegionAll, string Productvalue,string DocumentTypes,
                                                          string MaterialTypeInfo,string TreatmentTechniquesInfo,
                                                          string Document_TypeSearch) 
    {   
        string sortField ='';
        string sortDir ='';
        string whr='';
        
        if (string.isBlank(sortField)) {
            sortField = 'Date__c';
        }
        if (string.isBlank(sortDir)) {
            sortDir = 'desc';
        }
        set<Id> conDocID = new set<Id>();
        List<ContentVersion> lContentVersions  = new List<ContentVersion>();
        
        try{
            set<Id> contntId = new set<Id>();
            String temp =  Document_TypeSearch + '*';
            
            String Query = 'select id,Mutli_Languages__c,ContentDocumentId,Title,Date__c,Document_Language__c,Document_Type__c,Recently_Updated__c, Document_Number__c,Newly_Created__c,Document_Version__c,Document_Type2__c,Product_Group__c,Material_Type__c,Treatment_Technique__c from ContentVersion where id != null and IsLatest = true and Parent_Documentation__c = null and RecordType.name = \'Partner Document\' '; 
            //  String Query = 'select id,Mutli_Languages__c,ContentDocumentId,Title,Date__c,Document_Language__c,Document_Type__c,Recently_Updated__c, Document_Number__c,Newly_Created__c,Document_Version__c,Document_Type2__c,Product_Group__c,Material_Type__c,Treatment_Technique__c from ContentVersion where id != null and IsLatest = true and Parent_Documentation__c = null '; 
            
            String QueryCnt = 'select count() from ContentVersion where id != null and Parent_Documentation__c = null and RecordType.name = \'Partner Document\''; 
            if(string.isNotBlank(Productvalue)){
                Query = Query + 'and Product_Group__c INCLUDES (\'' + Productvalue + '\', \'ALL\') ';                
                gtBannerRep(Productvalue);                
            }
            else {
                strBanner = gblStrBanner;
                total_size = null;
            }
            //   **************************************Code End**********************************************************************//
            
            if(string.isNotBlank(DocumentTypes)){   
                String s;
                s = '%' + DocumentTypes + '%';
                whr = whr + ' and Document_Type2__c LIKE  :s';
            }
            if(string.isNotBlank(MaterialTypeInfo)){           
                String st;
                st = '%' + MaterialTypeInfo + '%';
                whr = whr + '  and Material_Type__c LIKE  : st';
            }
            if(string.isNotBlank(TreatmentTechniquesInfo)){  
                Query = Query + 'and Treatment_Technique__c INCLUDES (\'' + TreatmentTechniquesInfo  + '\', \'ALL\') ';
            }
            if(string.isNotBlank(Document_TypeSearch)){
                String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING ContentVersion(ID)';
                
                List<List<SObject>> searchList = search.query(searchquery);
                ContentVersion[] cn = ((List<ContentVersion>)searchList[0]);
                
                system.debug(' --- Check File Column ----  -- ' + cn + ' --- ----- ' + searchList);
                whr = whr + ' and Id in : cn';
            }
            
            /* To make Region field on Content functional --------- */ 
            User currentUser = [SELECT Id, Name, ContactId, Contact.Territory__c FROM User WHERE Id = :Userinfo.getUserId()];
            
            if(currentUser.ContactId != NULL){
                if(currentUser.Contact.Territory__c != NULL){
                    Query = Query + 'and Region__c INCLUDES (\'' + currentUser.Contact.Territory__c + '\', \'ALL\') ';
                }
                else{
                    Query = Query + 'and Region__c INCLUDES (\'ALL\') ';
                }
            }
            else{
                if(string.isNotBlank(RegionAll)){
                    Query = Query + 'and Region__c INCLUDES (\'' + RegionAll + '\', \'ALL\') ';
                }                
            }
            
            QueryCnt = QueryCnt + ' ' + whr;
            
            if(whr == '' || whr == null)
                whr = 'order by ' + sortField + ' '+ sortDir;
            else if(!whr.contains('order'))
                whr = whr + ' order by ' + sortField + ' '+ sortDir;        
            
            if(counter < 2000){
                if (counter == total_size)
                    Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + (counter - list_size);
                else
                    Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + counter;
            }
            else{
                Query = Query + ' ' + whr +' limit ' + counter + 20;
            }
            
            total_size = DataBase.countQuery(QueryCnt);
            system.debug('----- query constricted is---->'+Query);
            List<Sobject> sobj = DataBase.Query(Query);
            
            if(counter < 2000){
                For(Sobject s : Sobj){
                    lContentVersions.add((ContentVersion)s);
                    System.debug('lstcontentversion Size--->'+lContentVersions.size());
                }
            }else{
                For(Integer i = counter; i <= counter + 9;i++){
                    if(i < Sobj.size()){
                        lContentVersions.add((ContentVersion)Sobj[i]);
                    }
                    System.debug('List lstcontentversion'+lContentVersions.size());
                }
            }
        } 
        catch (QueryException ex){
            system.debug(ex.getMessage());
        }
        return lContentVersions;
    }
    
    private static String soql {get;set;}
    public static void gtBannerRep(string prm){      
        soql='select Image__c from BannerRepository__c where Recordtype.Name =\'Partner Documentation\'';
        BannerRepository__c[] bn=Database.query(soql);
        // BannerRepository__c[] bn = [select Image__c from BannerRepository__c where Product_Affiliation__c includes(\''+prm+'\') and Recordtype.Name = 'Product Documentation'];
        
        if(bn.size() > 0){if(bn[0].Image__c != null ){
            strBanner = bn[0].Image__c;
            List<String> ss = strBanner.substring(strBanner.indexof('src')+5).split('"');
            
            if(ss.size()>0){
                strBanner=ss[0];
            }
        }
                         }
        else{
            strBanner = gblStrBanner;
        }
    }
    
    private Integer getPageNumber() {
        if(total_size == 0)
        {
            return 0;
        }
        else if(total_size == list_size)
        {
            return total_size/list_size;
        }else{
            if(counter == total_size)
            {
                return counter/list_size; // + 1;
            }else{
                return counter/list_size +1;
            }
        }
    }
    
    private Integer getTotalPages() {        
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;         
        } else {
            return (total_size/list_size); 
        }
    }
    
    public class picklistEntry{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string pvalue{get;set;}
        public picklistEntry(string text, string value){
            label = text;
            pvalue = value;
        }
    }
}
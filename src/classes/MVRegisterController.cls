/*
* Author: Harleen Gulati
* Created Date: 3-Aug-2017
* Project/Story/Inc/Task : My varian lightning project 
 * Description: This class is used for My Varian Registration lightening page.
*/
public without sharing class MVRegisterController {
    
    /* Initialise all custom labels */
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    /* Show contact information if the user is logged in. */
    @AuraEnabled
    public static boolean showContact(String conPresentemail){
        List<User> extuser=[Select id, Email, IsActive from User 
                          where email=:conPresentemail and IsActive =: true and Profile.Name =: Label.VMS_Customer_Portal_User ];
     
    if(extuser.size()>0) 
        return True;
    else
        return False; 
    }
    
    @AuraEnabled
    public static List<String> getDynamicPicklistOptions(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Contact');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();

        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* Get country picklist values  from account*/
    @AuraEnabled
    public static List<String> getDynamicPicklistOptionsForCountry(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();

        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }

     
    /* Save contact data */
    @AuraEnabled
    public static String registerMData(Contact registerDataObj, Account accDataObj){
       
    list<Account> acc = new list<Account>();
    acc = [select Id, Name, BillingPostalCode, BillingStreet, BillingState, BillingCity 
            from Account
            where Name =: registerDataObj.Institute_Name__c
            and BillingPostalCode =: registerDataObj.MailingPostalCode
            and BillingStreet =: registerDataObj.MailingStreet
            and BillingState =: registerDataObj.MailingState
            and BillingCity =: registerDataObj.MailingCity];   
      
    if(acc.size() >0){
        registerDataObj.AccountId = acc[0].Id;   
    }   
    
    registerDataObj.Date_of_Factory_Tour_Visit__c = system.today();
    registerDataObj.Registered_Portal_User__c = true;
    registerDataObj.MyVarian_Registration_Date__c=Date.Today();
    registerDataObj.OCSUGC_Contact_Source__c = 'MyVarian';
    registerDataObj.MailingCity = 'USA';
    if(!test.isRunningTest()){
    	insert registerDataObj;
    }
      
    // Send email to admin on new account registration
    string ContctId = registerDataObj.Id;
    String[] toAddresses = new String[] {system.label.MyVarinRegistrationEmail};  
    EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'New_Account_Registration' limit 1];
    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setReplyTo(System.Label.MVsupport);       
        mail.setTemplateID(ETeplt.Id);
        mail.setTargetObjectId(ContctId);
        // Use Organization Wide Address
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress)) 
            mail.setOrgWideEmailAddressId(owa.id); 
        } 

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });       
        
        string body; 
        body = 'Dear Varian Admin <br> <br>';
        
        body = body + registerDataObj.FirstName +' '+ registerDataObj.LastName + ' from '+ registerDataObj.Institute_Name__c +', '+ registerDataObj.MailingCity +', '+ registerDataObj.MailingState +' has applied for a MyVarian account. Please click on the following link to activate the account '+ system.label.Org_Domain_URL +'/'+ ContctId +' <br> <br> Sincerely, <br> <br> Varian Medical Systems';
        //body = body + ContactSalutation +' '+ CustFname +' '+ CustLname + ' from '+ AcctName +', '+ AcctCity +', '+ AcctState +' has applied for a MyVarian account. Please click on the following link to activate the account '+ system.label.Org_Domain_URL +'/'+ ContctId +' <br> <br> Sincerely, <br> <br> Varian Medical Systems';
        system.debug(' ---- ----- body ---- ' + body);
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        mailAdmin.setSubject('Account registration for ' + registerDataObj.FirstName +' '+ registerDataObj.LastName + ' on MyVarian');
        mailAdmin.setToAddresses(toAddresses);
        mailAdmin.setHTMLBody(body);
            // Use Organization Wide Address
            for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
            {
                if(owa.Address.contains(system.label.CpOrgWideAddress)) 
                    mailAdmin.setOrgWideEmailAddressId(owa.id); 
                } 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin });   
    return 'accessdocs';
    }
    /* Send email notification to the user */
    @AuraEnabled
    public static String sendNotification(Contact registerDataObj, Account accDataObj, boolean ConsentAction){
                             system.debug('registerDataObj data::after insert'+registerDataObj);

        String ContctId;
        String AccntId;
        String cntAdmin=''; 
        String[] toAddresses = new String[] {system.label.MyVarinRegistrationEmail};               
        Recordtype  rt = [Select id from recordtype where developername = 'Site_Partner'];   
        Account[] acc = [select Id,(Select name, Email From Contacts where Account_admin__C = true) from Account where Name =: registerDataObj.Institute_Name__c and BillingPostalCode =: registerDataObj.MailingPostalCode and RecordTypeId =: rt.id];
        if(acc.size() > 0){                                
            AccntId = acc[0].Id;
            if(acc[0].Contacts.size()>0){
                cntAdmin = acc[0].Contacts[0].name;
                toAddresses.clear();               
                toAddresses.add(acc[0].Contacts[0].Email);
            } 
        }              
                             
        Contact[] contReq = [select id from Contact where Email =: registerDataObj.Email];
        if(contReq.size() == 0){
            Contact cont = new Contact();
            cont.FirstName = registerDataObj.FirstName;
            cont.Lastname = registerDataObj.LastName;
            cont.Email = registerDataObj.Email;
            cont.Title = registerDataObj.Title;
            cont.Specialty__c = registerDataObj.Specialty__c;
            cont.Salutation = registerDataObj.Salutation;
            cont.Preferred_Language1__c = registerDataObj.Preferred_Language1__c;
            cont.Phone = registerDataObj.Phone;
            cont.Fax = registerDataObj.Fax;
            cont.Manager__c = registerDataObj.Manager__c;
            cont.Manager_s_Email__c = registerDataObj.Manager_s_Email__c;
            cont.Registered_Portal_User__c = true;
            cont.MailingCity=registerDataObj.MailingCity;
            cont.MailingStreet = registerDataObj.MailingStreet;
            cont.MailingState = registerDataObj.MailingState;
            cont.MailingPostalCode = registerDataObj.MailingPostalCode;
            cont.MailingCountry = registerDataObj.MailingCountry;
            cont.Functional_Role__c=registerDataObj.Functional_Role__c;
            cont.Institute_Name__c = registerDataObj.Institute_Name__c; 
            cont.Research_Contact__c=registerDataObj.Research_Contact__c;
            cont.MyVarian_Registration_Date__c = system.today();

            if(registerDataObj.MailingCountry =='Canada' && ConsentAction == true)
            {
                cont.Email_Opt_in__c=true;
                cont.How_Opted_In__c='MyVarian';
                cont.Opt_In_Date__c=system.today();
                cont.Opted_In_By__c= registerDataObj.Salutation +' '+ registerDataObj.FirstName +' '+ registerDataObj.LastName;
            }
            
            if(AccntId != null && AccntId != ''){
                cont.AccountId = AccntId;
            }
            cont.OCSUGC_Contact_Source__c = 'MyVarian';
            Insert cont;
            ContctId = cont.Id;
        }
        else{
           List<User> extuser=[Select id, email from User where email=:registerDataObj.Email and isActive != false and Profile.Name =: Label.VMS_Customer_Portal_User ];
           if(extuser.size()==0)
            {
                ContctId = contReq[0].Id;
                Contact cont = new Contact(id=contReq[0].Id);
                cont.FirstName = registerDataObj.FirstName;
                cont.Lastname = registerDataObj.LastName;
                cont.Email = registerDataObj.Email;
                cont.Title = registerDataObj.Title;
                cont.Specialty__c = registerDataObj.Specialty__c;
                cont.Salutation = registerDataObj.Salutation;
                cont.Preferred_Language1__c = registerDataObj.Preferred_Language1__c;
                cont.Phone = registerDataObj.Phone;
                cont.Fax = registerDataObj.Fax;
                cont.Manager__c = registerDataObj.Manager__c;
                cont.Manager_s_Email__c = registerDataObj.Manager_s_Email__c;
                cont.Registered_Portal_User__c = true;
                cont.MailingCity=registerDataObj.MailingCity;
                cont.MailingStreet = registerDataObj.MailingStreet;
                cont.MailingState = registerDataObj.MailingState;
                cont.MailingPostalCode = registerDataObj.MailingPostalCode;
                cont.MailingCountry = registerDataObj.MailingCountry;
                cont.Functional_Role__c=registerDataObj.Functional_Role__c;
                cont.Institute_Name__c = registerDataObj.Institute_Name__c; 
                cont.Research_Contact__c=registerDataObj.Research_Contact__c;
                cont.MyVarian_Registration_Date__c = system.today();
                                                
            if(registerDataObj.MailingCountry =='Canada' && ConsentAction == true)
            {
                    cont.Email_Opt_in__c=true;
                    cont.How_Opted_In__c='MyVarian';
                    cont.Opt_In_Date__c=system.today();
                    cont.Opted_In_By__c= registerDataObj.Salutation +' '+ registerDataObj.FirstName +' '+ registerDataObj.LastName;
            }
            if(AccntId != null && AccntId != ''){
                    System.debug('--------AuntId ----------------- ' + AccntId);
                    cont.AccountId = AccntId;
            }
            cont.OCSUGC_Contact_Source__c = 'MyVarian';
            update cont;
            }
        }
        
        EmailTemplate ETeplt = [Select e.Name, e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = 'New_Account_Registration' limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setReplyTo(System.Label.MVsupport);       
        mail.setTemplateID(ETeplt.Id);
        mail.setTargetObjectId(ContctId);
        // Use Organization Wide Address
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress)) 
            mail.setOrgWideEmailAddressId(owa.id); 
        } 
        
        if(test.isRunningTest() && registerDataObj.Email == null) {
            List<String> emil = new List<String>{'A@b.com'};
            mail.setToAddresses(emil);
        } else {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });       
        }
		
        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });       
        
        string body; 
        if(cntAdmin.length() > 0){
            body = 'Dear '+ cntAdmin +' <br> <br>';
        }
        else
        body = 'Dear Varian Admin <br> <br>';
        
        body = body + registerDataObj.FirstName +' '+ registerDataObj.LastName + ' from '+ registerDataObj.Institute_Name__c +', '+ registerDataObj.MailingCity +', '+ registerDataObj.MailingState +' has applied for a MyVarian account. Please click on the following link to activate the account '+ system.label.Org_Domain_URL +'/'+ ContctId +' <br> <br> Sincerely, <br> <br> Varian Medical Systems';
        //body = body + ContactSalutation +' '+ CustFname +' '+ CustLname + ' from '+ AcctName +', '+ AcctCity +', '+ AcctState +' has applied for a MyVarian account. Please click on the following link to activate the account '+ system.label.Org_Domain_URL +'/'+ ContctId +' <br> <br> Sincerely, <br> <br> Varian Medical Systems';
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        
        mailAdmin.setSubject('Account registration for ' + registerDataObj.Salutation +' '+ registerDataObj.FirstName +' '+ registerDataObj.LastName + ' on MyVarian');
        mailAdmin.setToAddresses(toAddresses);
        mailAdmin.setHTMLBody(body);
        // Use Organization Wide Address
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) 
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress)) 
            mailAdmin.setOrgWideEmailAddressId(owa.id); 
        } 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin });
        return null;  
    }
}
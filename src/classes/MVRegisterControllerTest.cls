@isTest
private class MVRegisterControllerTest {
    
    @isTest static void getCurrentUserTest() {
        test.startTest();        
        MVRegisterController.showContact('rishabh.verma@varian.com');
        test.stopTest();
    }
    
    @isTest static void getCustomLabelMapTest() {
        test.startTest();        
        MVRegisterController.getCustomLabelMap('en');
        test.stopTest();
    }

    @isTest static void getDynamicPicklistOptionsTest() {
        test.startTest();        
        MVRegisterController.getDynamicPicklistOptions('County__c');
        test.stopTest();
    }
    @isTest static void getDynamicPicklistOptionsForCountryTest() {
        test.startTest();        
        MVRegisterController.getDynamicPicklistOptionsForCountry('Country__c');
        test.stopTest();
    }
    @isTest static void sendNotificationTest() {
        test.startTest();
            User thisUser = [ select Id from User where ContactId != null Limit 1];     // = :UserInfo.getUserId() ];
            //System.runAs ( thisUser ) {
                Recordtype  rt = [Select id from recordtype where developername = 'Site_Partner'];   
      
                Id RecId = rt.Id;
                Regulatory_Country__c country1 = new Regulatory_Country__c(Name = 'USA');
                insert country1;
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a;   
                
                a = new Account( Territories1__c = 'Test Terrotory', name='test2', BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States', RecordTypeId = RecId); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='rishabh.verma@varian.com', AccountId=a.Id, 
                            MailingCountry ='Canada', MailingState='CA', Account_admin__c = true ); 
                insert con;

                MVRegisterController.sendNotification(con, a, true);
            //}
        test.stopTest();        
    }
    @isTest static void sendNotification1Test() {
        test.startTest();
            User thisUser = [ select Id from User where ContactId != null Limit 1];     // = :UserInfo.getUserId() ];
            //System.runAs ( thisUser ) {

                Regulatory_Country__c country1 = new Regulatory_Country__c(Name = 'USA');
                insert country1;
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a;   
                
                a = new Account( Territories1__c = 'Test Terrotory', name='test9872',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh', Phone = '3456521258', AccountId=a.Id, 
                            MailingCountry ='USA', MailingState='CA' ); 
                insert con;
                MVRegisterController.sendNotification(con, a, true);
            //}
        test.stopTest();        
    }
    
    @isTest static void registerMDataTest() {
        test.startTest();
            User thisUser = [ select Id from User where ContactId != null Limit 1];     // = :UserInfo.getUserId() ];
            //System.runAs ( thisUser ) {

                Regulatory_Country__c country1 = new Regulatory_Country__c(Name = 'USA');
                insert country1;
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                User u;
                Account a;   
                
                a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='rishabh.verma@varian.com', AccountId=a.Id, 
                            MailingCountry ='USA', MailingState='CA' ); 
                insert con;
                //try{
                    MVRegisterController.registerMData(con, a);
                //}catch(exception e){
                //}
            //}
        test.stopTest();        
    }
}
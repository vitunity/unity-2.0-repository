/*
* Author: Naren Yendluri
* Created Date: 16-Aug-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website Software Installation page
*/




public with sharing class MVSoftwareInstallation {
    
  //  public static String usrName{get;set;}
   // public static String shows{get;set;}
    public static list<swInstall> lstSoftwareInstall {get; set;}
    private static map<String, swInstall> mapSoftwareInstall = new map<String, swInstall>();
    
    
    //To get Custom labels
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        return customLabelMap;
    }
    
    public class swInstall{
        @AuraEnabled public list<Software_Installation__c> lstSoftwareInstallation {get; set;}
        @AuraEnabled public string strCategory {get; set;}
        
        public swInstall(Software_Installation__c sInstallation){           
            strCategory = sInstallation.Category__c;
            lstSoftwareInstallation = new list<Software_Installation__c>{sInstallation};
                }
    }
    
  @AuraEnabled
    Public static List<swInstall> getSoftwareInstallation()
    {   
       /* shows = 'none';
        User usrInfo = [Select ContactId,alias from user where id =: UserInfo.getUserId() limit 1];
        if(usrInfo.ContactId == null)
        {
            shows = 'block';
            usrName = usrInfo.alias;
        } */
        lstSoftwareInstall = new list<swInstall>();
        Software_Installation__c[] swInstalls = [Select Id,Title__c, Content_Id__c, ContentID__c, Multi_Languages__c, 
                                                 URL__c,ExternalUrl__c, Category__c 
                                                 From Software_Installation__c 
                                                 order by Title__c];
        system.debug(' ------- swInstalls size of Records ------ ' + swInstalls.size());
        for(Software_Installation__c sInstallation : swInstalls){
            if(mapSoftwareInstall.get(sInstallation.Category__c) == null){
                mapSoftwareInstall.put(sInstallation.Category__c, new swInstall(sInstallation));                
                lstSoftwareInstall.add(mapSoftwareInstall.get(sInstallation.Category__c));
                system.debug('@@@@@@@@ lstSoftwareInstall ' + lstSoftwareInstall);
            }
            else{                
                mapSoftwareInstall.get(sInstallation.Category__c).lstSoftwareInstallation.add(sInstallation);
                system.debug(' ------- Title of Records ------ ' + mapSoftwareInstall.get(sInstallation.Category__c).lstSoftwareInstallation + ' ---- ctr ---- ' + sInstallation.Category__c);
            }
        }
        return lstSoftwareInstall;
    }
}
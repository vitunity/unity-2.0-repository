@isTest
private class MVSoftwareInstallationTest{

    @isTest static void test1() {
        test.startTest();        
        MVSoftwareInstallation.getCustomLabelMap('en');
        test.stopTest();
    } 
    
    @isTest static void test2() {
        test.startTest();        
        MVSoftwareInstallation.getSoftwareInstallation();
        test.stopTest();
    }
    
    @isTest static void test3() {
        test.startTest(); 
        Document_List__c sobj = new Document_List__c(
          Title__c = 'A11 Software Installation and Upgrades Oncology Information System',                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  // Title
          Description__c = 'Below are documents related to the latest Software Release. These documents have been organized to facilitate your software install or upgrade. Should you have any questions about your installation or these documents, please contact your Varian project manager.\n\nClick on a category to expand and view documentation. Clicking an expanded category will hide the documents under that category. To gain access to these documents, you will need the appropriate product access on MyVarian. If you are having a product installed that is not yet added to your MyVarian account, please submit a Product Access Update request to gain access to this information.'  // Description
        );
        insert sobj;
        
        Software_Installation__c sobj1 = new Software_Installation__c(
          ContentID__c = '068E00000029TO3IAM',                            // Content ID
          //RecordType = '01244000000fQXT',                                 // Record Type
          Title__c = 'ARIA for Radiation Oncology E-Prescribe Workbook',  // Title
          URL__c = '069E0000000ru2v',                                     // URL
          Document_List__c = sobj.Id,                                     // Document List
          ExternalUrl__c = false,                                         // ExternalUrl
          Category__c = 'E-Prescribe',                                    // Category
          Multi_Languages__c = false                                      // Multi Languages
        );
        insert sobj1 ;
        
               
        MVSoftwareInstallation.swInstall wrapper = new MVSoftwareInstallation.swInstall(sobj1);
    } 

}
public without sharing class  MVTPassPackageController{
    
    //xoxp-356176202180-355706895728-360596768422-ea054d1954aed75c4ad0ad528f2bc7b7
    private static integer counter=0;
    private static integer list_size=10; //sets the page size or number of rows
    public static integer total_size; //used to show user the total size of the list
    
    private static datetime getDateFromString(string dtstr){
        datetime dt;
        if(dtstr != null && dtstr != '' && dtstr != 'undefined'){
            String[] dateparse = dtstr.split(' ');
            String[] edDate = dateparse[0].split('/');
            dt = Datetime.newInstance(integer.valueOf(edDate[2]), integer.valueOf(edDate[0]), integer.valueOf(edDate[1]),integer.valueOf(edDate[3]),integer.valueOf(edDate[4]),integer.valueOf(edDate[5]));
        }
        return dt;
    }
    
    @AuraEnabled
    public static TrTPassPackageWrap getTPassList(string Document_TypeSearch,String TPaasStatusLink,string Priority, string PlanStage,
                                                   Integer recordsOffset, Integer totalSize, Integer pageSize, string frmDate, string toDate,boolean ViewMyOrders)
    {
    
        datetime StartDate,EndDate;
        
        if(frmDate != null && frmDate != '' && frmDate != 'undefined'){
            StartDate = getDateFromString(frmDate+'/00/00/01');
        }
        if(toDate != null && toDate != '' && toDate != 'undefined'){
            EndDate = getDateFromString(toDate+'/23/59/59');
        }
                
        User cUser = [select id,AccountId,ContactId,Dosiometrist__c from User where Id=: userinfo.getUserId()];
        boolean isInternalUser = (cUser.ContactId == null || cUser.Dosiometrist__c)?true:false;
        
        total_size = totalSize;
       
        if(recordsOffset != null){
            counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null){
            list_size = integer.valueof(pageSize);
        }
        
        set<string> setPlanStage = new set<string>();
        if(TPaasStatusLink == 'All'){
            setPlanStage.add('Requested');
            setPlanStage.add('Assigned');
            setPlanStage.add('In Progress');
            setPlanStage.add('Structures Ready for Review');
            setPlanStage.add('Plan Ready for Review');
            setPlanStage.add('Completed');
            setPlanStage.add('Replan Requested');
        }else if(TPaasStatusLink == 'Open'){
            setPlanStage.add('Requested');
            setPlanStage.add('Assigned');
            setPlanStage.add('In Progress');
            setPlanStage.add('Structures Ready for Review');
            setPlanStage.add('Plan Ready for Review');
        }else if(TPaasStatusLink == 'Closed'){
            setPlanStage.add('Completed');
            setPlanStage.add('Replan Requested');
        }
        
        string sortField ='psequence__c';
        string sortDir ='desc';
        string whr='';
        String Query = 'select Id,Name,Plan_Stage__c ,Parent__r.Plan_Identifier__c,Patient__c,Client_Name__c,Planning_Directive__c,Plan_Notes__c,Acceptable_Dose_Attenuation__c,OAR_Notes__c,channel_id__c,Prescribed_Dose__c,PrescribedClinician__r.Name,Treatement_Plan_Time__c,Plan_Requested_Date__c, Beam_Data__c,Site_Name__r.name,PrescribedClinician__c,PlanState__c,Machine_ID__c,Prescribing_Clinicians_Email__c,Plan_Due_Date__c,Priority__c,SiteName__c,Plan_Identifier__c,Status__c,Assigned_Dosiometrist__c,Assigned_Dosiometrist__r.Name from TPaaS_Package_Detail__c where Id <> null  and Plan_Stage__c IN: setPlanStage ';
        String QueryCnt = 'select count() from TPaaS_Package_Detail__c  where Id <> null and Plan_Stage__c IN: setPlanStage ';
        
        string AccountId = cUser.AccountId;
        string cUserId = cUser.Id;
        string wherecondition;
        if(!cUser.Dosiometrist__c){
            if(wherecondition==null)wherecondition = ' and ';
            wherecondition += ' ( Site_Name__c =: AccountId or ownerId =: cUserId ) ';
        }
        else if(ViewMyOrders <> null){
            if(ViewMyOrders){
                if(wherecondition==null)wherecondition = ' and ';
                if(AccountId == null)wherecondition += ' ( Assigned_Dosiometrist__c =: cUserId ) ';//Internal User
                if(AccountId <> null)wherecondition += ' ( Site_Name__c =: AccountId or ownerId =: cUserId ) ';//External User
            }
            
        }
       
        if(Document_TypeSearch  <> null && Document_TypeSearch  <> ''  ){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Site_Name__r.name like \'%'+Document_TypeSearch+'%\'';
        }
        if(StartDate <> null && EndDate <> null){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Plan_Due_Date__c >=: StartDate and Plan_Due_Date__c <=: EndDate';

        }
        
        if(Priority <> '--Any--' ){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Priority__c =: Priority ';
        }
        if(PlanStage <> '--Any--' ){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Plan_Stage__c =: PlanStage ';
        }
        
        
        if(wherecondition != null){Query+=wherecondition;QueryCnt+=wherecondition;}
        string orderby = ' order by ' + sortField + ' '+ sortDir;  
        Query = Query + orderby +' limit ' + list_size + ' offset ' +counter;
        
        system.debug('::::'+Query+'::::'+StartDate+'::::'+EndDate);
        total_size = DataBase.countQuery(QueryCnt);
        
        //added by Shital start
        system.debug('******Query***'+AccountId+'^^^^'+cUserId+'$$$$'+Query);
        //added by Shital end
        List<TPaaS_Package_Detail__c> sobj = DataBase.Query(Query);
        TrTPassPackageWrap wobj= new TrTPassPackageWrap(isInternalUser,sobj , total_size);
        return wobj;
    }
    
    
    @AuraEnabled
    public static TPassPackageDetail  getNewTPass(string parentId)
    {
        
        user uobj = [select Id,Name,ContactId,Contact.Name, AccountId, Account.Name from User where ID=: userinfo.getUserId()];
        List<String> options = new List<string>{'Standard (3 days)','Priority (2 days)','Stat (1 day)'};
        
        TPassPackageDetail a= new TPassPackageDetail();
        
        TPaaS_Package_Detail__c parentObj;  
        if(parentId <> 'null'){
            List<TPaaS_Package_Detail__c> lstP = [select Id,Site_Name__r.name,Site_Name__c,Plan_Identifier__c,Prescribed_Dose__c,OAR_Metrics__c,OAR_Metric_Template__c,
            Acceptable_Dose_Attenuation__c,Priority__c,OAR_Notes__c,Patient__c,Client_Name__c,Plan_Notes__c from TPaaS_Package_Detail__c where Id =: parentId];
            if(lstP.size()>0)parentObj = lstP[0];
            a.ParentId = (parentObj <> null)?parentObj.Id:null;         
        }        
       
        
        a.ParentName = (parentObj <> null && parentObj.Plan_Identifier__c <> null)?parentObj.Plan_Identifier__c:'';
        
        a.prescribeddose = (parentObj <> null && parentObj.Prescribed_Dose__c <> null)?parentObj.Prescribed_Dose__c:'';
        a.contact = uobj.contact.Name;
        a.acceptabledoseattenuation = (parentObj <> null && parentObj.Acceptable_Dose_Attenuation__c <> null)?parentObj.Acceptable_Dose_Attenuation__c:'';
        a.prescribingclinian = uobj.Name;
        //a.oarmetrics = (parentObj <> null && parentObj.OAR_Metrics__c <> null)?parentObj.OAR_Metrics__c:'';
        a.oarmetrics = (parentObj <> null && parentObj.OAR_Metric_Template__c <> null)?parentObj.OAR_Metric_Template__c:'';
        a.priority = (parentObj <> null && parentObj.Priority__c <> null)?parentObj.Priority__c:'Standard (3 days)';
        a.oarnotes = (parentObj <> null  && parentObj.OAR_Notes__c <> null)?parentObj.OAR_Notes__c:'';
        a.Patient = (parentObj <> null  && parentObj.Patient__c <> null)?parentObj.Patient__c:'';
        a.ClientName = (parentObj <> null  && parentObj.Client_Name__c <> null)?parentObj.Client_Name__c:'';
        //a.planduedate = '';
        a.plannotes = (parentObj <> null && parentObj.Plan_Notes__c <> null)?parentObj.Plan_Notes__c:'';
        a.priorityOptions = options;
        
        
        a.contactId = uobj.contactId;
        a.prescribingclinianId =uobj.ID;
        
        if(uobj.contactId <> null){
            map<Id,string> mapAccount = new map<Id,string>();
            mapAccount.put(uobj.accountId,uobj.account.Name);
            a.siteId = (parentObj <> null && parentObj.Site_Name__c <> null)?parentObj.Site_Name__c:uobj.accountId;
            a.site = (parentObj <> null && parentObj.Site_Name__c <> null)?parentObj.Site_Name__r.name:uobj.account.name;
            
            
            List<SiteOption> lstOptions = new list<SiteOption>();
            for(Contact_Role_Association__c c : [select Id,Contact__c,Account__c,Account__r.NAme from Contact_Role_Association__c where Contact__c =: uobj.contactId]){
                mapAccount.put(c.Account__c,c.Account__r.NAme);
            }
            for(Id AId: mapAccount.keyset()){
                lstOptions.add(new SiteOption(AId,mapAccount.get(AId)));
            }
            a.siteoptions = lstOptions;
        }
        
        return a;
    }
    
    @AuraEnabled
    public static List<string> getPriorities(){
        List<string> lstPriority = new List<string>();
        lstPriority.add('--Any--');
        for(Schema.Picklistentry p: (TPaaS_Package_Detail__c.Priority__c.getDescribe()).getPicklistValues()) {
            lstPriority.add(p.getvalue()); 
        }
        return lstPriority;
    }
    @AuraEnabled
    public static List<string> getPlanStages(string TPaasStatusLink){
        
        
        
        List<string> lstPlanStage = new List<string>();
        lstPlanStage.add('--Any--');
        if(TPaasStatusLink == 'All'){
            lstPlanStage.add('Requested');
            lstPlanStage.add('Assigned');
            lstPlanStage.add('In Progress');
            lstPlanStage.add('Structures Ready for Review');
            lstPlanStage.add('Plan Ready for Review');
            lstPlanStage.add('Completed');
            lstPlanStage.add('Replan Requested');
        }else if(TPaasStatusLink == 'Open'){
            lstPlanStage.add('Requested');
            lstPlanStage.add('Assigned');
            lstPlanStage.add('In Progress');
            lstPlanStage.add('Structures Ready for Review');
            lstPlanStage.add('Plan Ready for Review');
        }else if(TPaasStatusLink == 'Closed'){
            lstPlanStage.add('Completed');
            lstPlanStage.add('Replan Requested');
        }
        return lstPlanStage;
    }
    
    @AuraEnabled
    public static string SaveNewTPassDetail(string wObj, string priority,string qarmetrics, string site){
        
        system.debug('SAVE:wObj:'+wObj);
        system.debug('SAVE:priority:'+priority);
        system.debug('SAVE:qarmetrics:'+qarmetrics);
        system.debug('SAVE:site:'+site);
        
        TPaasBusinessHourUtility t = new TPaasBusinessHourUtility();
        //try{
            TPassPackageDetail TPass = (TPassPackageDetail)JSON.deserialize(wObj,TPassPackageDetail.class);
            TPaaS_Package_Detail__c obj = new TPaaS_Package_Detail__c();

            
            //obj.Planning_Directive__c= ''; 
            obj.Plan_Notes__c= TPass.plannotes; 
            obj.Acceptable_Dose_Attenuation__c= TPass.acceptabledoseattenuation; 
            obj.OAR_Notes__c= TPass.oarnotes; 
            obj.Patient__c= TPass.Patient;
            obj.Client_Name__c= TPass.ClientName;
            //obj.OAR_Metrics__c = TPass.oarmetrics;
            obj.OAR_Metric_Template__c = qarmetrics;
            obj.channel_id__c= TPass.planduedate; 
            obj.Prescribed_Dose__c= TPass.prescribeddose; 
            obj.Parent__c = (TPass.parentId <> '')?TPass.parentId:null; 
            
            //obj.Plan_Stage__c = (obj.Parent__c == null)?'Requested':'Replan Requested';
            obj.Plan_Stage__c = 'Requested';
            //obj.Treatement_Plan_Time__c= ''; 
            obj.Plan_Requested_Date__c= datetime.now();
            //obj.Beam_Data__c= ''; 
            obj.PrescribedClinician__c=TPass.contactId;
            //obj.PlanState__c= ''; 
            //obj.Machine_ID__c= ''; 
            //obj.Prescribing_Clinicians_Email__c= ''; 
            
            Integer duedate = (priority == 'Standard (3 days)')?3:((priority == 'Priority (2 days)')?2:1); 
                        
            Datetime dt = datetime.now();
            Datetime ht = t.getNextPlanDueDate(dt,duedate);
            system.debug('$$:'+duedate+':$$:'+dt.format('dd/MM/yyy HH:mm:ss','PST')+'['+dt+']   -->    '+ht.format('dd/MM/yyy HH:mm:ss','PST')+'['+ht+']');
            system.debug('$$:'+duedate+':$$:'+dt.format('dd/MM/yyy HH:mm:ss','GMT')+'['+dt+']   -->    '+ht.format('dd/MM/yyy HH:mm:ss','GMT')+'['+ht+']');
            
            obj.Plan_Due_Date__c=t.getNextPlanDueDate(dt,duedate);
            
            //obj.New_Plan_Due_Date__c=t.getNextPlanDueDate(dt,duedate);
            //obj.New_Plan_Due_Date_Str__c='$$:'+ht.format('dd/MM/yyy HH:mm:ss','PST')+':$$:'+ht.format('dd/MM/yyy HH:mm:ss','GMT')+'['+ht+']';
            
            if(site <> null){
                Account Acc = [select ID,Name from Account where Id=: site limit 1];
                obj.SiteName__c= Acc.NAme;
                OBJ.Site_Name__c =  Acc.Id;
            }
            obj.Priority__c= priority;
            
            //obj.Plan_Identifier__c= ''; 
            obj.PlanState__c= 'New'; 
            if(label.TPaas_Dosiometrists <> null && label.TPaas_Dosiometrists <> ''){
                obj.Assigned_Dosiometrist__c = (label.TPaas_Dosiometrists).split(',')[0];
            }
 
            
            insert obj;
            
            
            if(TPass.parentId <> ''){
                List<TPaaS_Package_Detail__c> lstP = [select Id,Plan_Stage__c,Priority__c from TPaaS_Package_Detail__c where Id =: TPass.parentId limit 1];
                if(lstP.size()>0){
                    lstP[0].Plan_Stage__c = 'Replan Requested';
                    //lstP[0].Priority__c = 'Priority Replan(1 day)';
                    update lstP[0];
                }
            }
            
        /*}catch(Exception e){
           return string.valueOf(e.getMessage());
        }*/
        return 'successfull created';
    }
    
    @AuraEnabled
    public static boolean getDosiometristInfo(){
        User cUser = [select id,AccountId,Dosiometrist__c from User where Id=: userinfo.getUserId()];
        return cUser.Dosiometrist__c;
    }
        
    @AuraEnabled
    public static TPaaS_Package_Detail__c  getTPassDetails(string PackageId)
    {
        
        
        
        TPaaS_Package_Detail__c p = [select Id,Name,OAR_Metric_Template__c,Dosimetrist_Notes__c,channel_id__c,parent__r.Plan_Identifier__c,Planning_Directive__c,Plan_Notes__c,Plan_Stage__c,Acceptable_Dose_Attenuation__c,OAR_Notes__c,Prescribed_Dose__c,PrescribedClinician__r.Name,PrescribedClinician__c,PlanState__c,Machine_ID__c,Beam_Data__c,Site_Name__r.name,Treatement_Plan_Time__c,Plan_Requested_Date__c,
        Prescribing_Clinicians_Email__c,Plan_Due_Date__c,Priority__c,Patient__c,Client_Name__c,SiteName__c,Plan_Identifier__c,Status__c,Assigned_Dosiometrist__c,Assigned_Dosiometrist__r.Name,(select id,body__c,createddate,Submitted_By__c,Submitted_By__r.Name,TimeStamp__c from TPassPackage_Comments__r order by createddate desc) 
        from TPaaS_Package_Detail__c where Id=: PackageId];
        
        TPaasSlackHandler.UpdateSFDCUserANDContact(p.Plan_Identifier__c);
        
        return p;
        
    }
    
    public static string get15DigitId(){
        return '';
    }
    
    @AuraEnabled
    public static List<string>  getDosiometrists()
    {
        List<string> lstDosiometrists = new List<string>();
        if(label.TPaas_Dosiometrists <> null && label.TPaas_Dosiometrists <> ''){
            set<Id> UIds = new set<Id>();
            for(string s : (label.TPaas_Dosiometrists).split(',')){
                UIds.add(s);
            }
            system.debug('UIds:'+UIds);
            map<Id,string> mapUser = new map<Id,string>();
            for(user u : [select Id,Name from User where isActive=true and  Id IN: UIds]){                
                mapUser.put(u.id,u.name);
            }
            system.debug('mapUser:'+mapUser);
            for(string s : (label.TPaas_Dosiometrists).split(',')){
                if(mapUser.containsKey(s))lstDosiometrists.add(mapUser.get(s));
            }
            system.debug('lstDosiometrists:'+lstDosiometrists);
        }
        return lstDosiometrists;
        
    }
    
    @AuraEnabled
    public static TPaaS_Package_Detail__c  savePriority(string packageId, string priority, string planstage, string dosiometristnotes, string dosiometrist)
    {        
        TPaaS_Package_Detail__c p = [select Id,New_Plan_Due_Date_str__c,Plan_Due_Date__c,Dosimetrist_Notes__c,Priority__c,Plan_Stage__c,PlanState__c,Assigned_Dosiometrist__c from TPaaS_Package_Detail__c where Id=: PackageId];
        p.Priority__c = priority;
        p.Plan_Stage__c = planstage;
        p.Dosimetrist_Notes__c = dosiometristnotes;
        
        TPaasBusinessHourUtility t = new TPaasBusinessHourUtility();
        Integer duedate = (priority == 'Standard (3 days)')?3:((priority == 'Priority (2 days)')?2:1);
        Datetime dt = datetime.now();
        p.Plan_Due_Date__c=t.getNextPlanDueDate(dt,duedate);
        
        set<Id> UIds = new set<Id>();
        for(string s : (label.TPaas_Dosiometrists).split(',')){
            UIds.add(s);
        }
        List<User> lstUSer = [select Id from user where ID IN:UIds and name =: dosiometrist limit 1];
        if(lstUSer.size()>0){
            p.Assigned_Dosiometrist__c = lstUSer[0].id;
        }
        
        update p;  
        
        TPaaS_Package_Detail__c tpass = [select Id,Name,OAR_Metric_Template__c,channel_id__c,parent__r.Plan_Identifier__c,Planning_Directive__c,Plan_Notes__c,Plan_Stage__c,Acceptable_Dose_Attenuation__c,OAR_Notes__c,Prescribed_Dose__c,PrescribedClinician__r.Name,PrescribedClinician__c,PlanState__c,Machine_ID__c,Beam_Data__c,Site_Name__r.name,Treatement_Plan_Time__c,Plan_Requested_Date__c,
        Prescribing_Clinicians_Email__c,Plan_Due_Date__c,Priority__c,Patient__c,Client_Name__c,SiteName__c,Plan_Identifier__c,Status__c,Assigned_Dosiometrist__c,Assigned_Dosiometrist__r.Name,(select id,body__c,createddate,Submitted_By__c,Submitted_By__r.Name,TimeStamp__c from TPassPackage_Comments__r order by createddate desc) 
        from TPaaS_Package_Detail__c where Id=: PackageId];
        return tpass;
    }
    
     @AuraEnabled
    public static void  savePlanStage(string packageId, string PlanStage)
    {        
        TPaaS_Package_Detail__c ps = [select Id,Plan_Stage__c from TPaaS_Package_Detail__c where Id=: PackageId];
        ps.Plan_Stage__c = PlanStage;
        //update ps;     
        
        try{
           update ps;
           }catch(Exception e){
           ApexPages.addMessages(e);
           }
        
           
    }
    private static integer c_counter=0;
    private static integer c_list_size=10; //sets the page size or number of rows
    public static integer c_total_size; //used to show user the total size of the list
    
    
    public class TPaasCommets{
        @AuraEnabled public List<TPassPackage_Comment__c> comments;
        @AuraEnabled public Integer totalComments;
        public TPaasCommets(List<TPassPackage_Comment__c> comments, Integer totalComments){
            this.comments = comments;
            this.totalComments = totalComments;
        }
    }
    
    
    @AuraEnabled
    public static TPaasCommets getTPassComments(string PackageId,Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        TPaasSlackHandler.getCommentFromSlack(PackageId);
        
        c_total_size = totalSize;
       
        if(recordsOffset != null){
            c_counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null){
            c_list_size = integer.valueof(pageSize);
        }
        
        string squery = 'select id,body__c,createddate,Submitted_By__c,Submitted_By__r.Name,TimeStamp__c from TPassPackage_Comment__c where TPaaS_Package_Detail__c=:PackageId '; 
        c_total_size = DataBase.countQuery('select count() from TPassPackage_Comment__c where TPaaS_Package_Detail__c=:PackageId ');
        squery += ' order by createddate desc  limit ' + c_list_size + ' offset ' +c_counter;        
        
        List<TPassPackage_Comment__c> lstTPassPackageComment = database.query(squery);
        return new TPaasCommets(lstTPassPackageComment,c_total_size);
    }
    
    @AuraEnabled
    public static string saveComment(string PackageId,string body)
    {
        system.debug('@@@@saveComment@@'+PackageId+'----'+body+'----');
     
        TPassPackage_Comment__c p = new TPassPackage_Comment__c();
        p.body__c = body;
        p.TPaaS_Package_Detail__c= PackageId;
        //String usernme= userinfo.getName(); 
        p.Submitted_By__c = userinfo.getUserId(); 
        system.debug('userinfo'+userinfo.getUserId());
        insert p;
        return 'succuss' ;
        
    }
    
    
    public class TrTPassPackageWrap{ 
        @AuraEnabled public boolean isInternalUser{get;set;}
        @AuraEnabled public list<TPaaS_Package_Detail__c> TPAASList{get;set;}
        @AuraEnabled public integer totalSize{get;set;}

        public TrTPassPackageWrap(boolean isInternalUser,list<TPaaS_Package_Detail__c> TPAASList, integer totalSize){
            this.TPAASList= TPAASList;
            this.totalSize = totalSize;
            this.isInternalUser = isInternalUser;
        }
    }
    
    public class TPassPackageDetailWrap{      
        @AuraEnabled public TPaaS_Package_Detail__c TPassDetail{get;set;}
        @AuraEnabled public List<TPassPackage_Comment__c> TPassComments{get;set;}

        public TPassPackageDetailWrap(TPaaS_Package_Detail__c TPassDetail, List<TPassPackage_Comment__c> TPassComments){
            this.TPassDetail= TPassDetail;
            this.TPassComments = TPassComments;
        }
    }
       
    public class SiteOption{
        @AuraEnabled public string SiteId;
        @AuraEnabled public string SiteName;
        public SiteOption(string SiteId,string SiteName){this.SiteId = SiteId;this.SiteName = SiteName;}
    }
    
    public class TPassPackageDetail{
        @AuraEnabled public string ParentId{get;set;}
        @AuraEnabled public string ParentName{get;set;}
        @AuraEnabled public string site{get;set;}
        @AuraEnabled public string prescribeddose{get;set;}
        @AuraEnabled public string contact{get;set;}
        @AuraEnabled public string acceptabledoseattenuation{get;set;}
        @AuraEnabled public string prescribingclinian{get;set;}
        @AuraEnabled public string oarmetrics{get;set;}
        @AuraEnabled public string priority{get;set;}
        @AuraEnabled public string oarnotes{get;set;}
        @AuraEnabled public string Patient{get;set;}
        @AuraEnabled public string ClientName{get;set;}
        @AuraEnabled public string planduedate{get;set;}
        @AuraEnabled public string plannotes{get;set;}
        @AuraEnabled public List<string> priorityOptions{get;set;}
        
        @AuraEnabled public string siteId{get;set;}
        @AuraEnabled public List<SiteOption> siteoptions{get;set;}
        @AuraEnabled public string contactId{get;set;}
        @AuraEnabled public string prescribingclinianId{get;set;}
    }
}
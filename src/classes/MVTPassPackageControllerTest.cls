@isTest
public class MVTPassPackageControllerTest {
    @isTest public static void testMVTPassPackageController(){
        
        
        account acc = new account();
        acc.Name = 'Test conName';
        acc.CurrencyIsoCode = 'USD';
        acc.Country__c = 'India';
        acc.BillingCity = 'Jaipur';
        acc.BillingState = 'Rajasthan';
        acc.ERP_Site_Partner_Code__c = 'Test';
        acc.BillingStreet = 'Test Billing Street';
        insert acc;
        
        contact oCon = new contact();
        oCon.AccountId = acc.Id;
        oCon.FirstName = 'Test fName1';
        oCon.LastName = 'Test lName1';
        oCon.Functional_Role__c = 'Educator';
        oCon.Email = 'testabc123@test.com';
        oCon.MobilePhone = '9999999999';
        oCon.Phone = '0000000000';
        oCon.Account_Admin__c = true;
        insert oCon;
        
        
        user u = new user();
        u.Alias = 'test'; 
        u.ContactId = oCon.Id;
        u.Email = 'test123123@test.com';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Testing';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'VMS MyVarian - Customer User'].Id;
        u.TimeZoneSidKey ='America/Los_Angeles';
        u.Username ='sdss123@test.com';
        insert u;
        system.runAs(u){
       
        date dtDate = system.Today();
        
        
        TPaaS_Package_Detail__c oTPD = new TPaaS_Package_Detail__c();
        oTPD.Name = 'Test oTPD';
        oTPD.Priority__c = 'Standard (3 days)';
        oTPD.OAR_Metric_Template__c = 'CFRT';
        oTPD.Plan_Stage__c = 'Completed';
        oTPD.Assigned_Dosiometrist__c = u.Id;
        oTPD.Dosimetrist_Notes__c = 'Test';
        insert oTPD;
        
        oTPD.Name = 'Test oTPD';
        update oTPD;
        
        string jsonOobjTPD = json.serialize(oTPD);
        system.debug('jsonOobjTPD@@@--- ' +jsonOobjTPD);
        
        TPassPackage_Comment__c oTPC = new TPassPackage_Comment__c();
        oTPC.TPaaS_Package_Detail__c = oTPD.Id;
        oTPC.Body__c = 'test Body';
        oTPC.CurrencyIsoCode = 'USD';
        insert oTPC;
        
        string sDate = dtDate.month()+ '/' + dtDate.day()+ '/' + dtDate.year();
        
        MVTPassPackageController.getTPassList('DocTypeSearch', 'CaseStatusLink', 'Standard (3 days)','planStage', 10, 50, 5, sDate, sDate,false);
        MVTPassPackageController.getNewTPass(oTPD.Id);
        MVTPassPackageController.getTPassDetails(oTPD.Id);
        MVTPassPackageController.getDosiometristInfo();
        MVTPassPackageController.getPriorities();
        MVTPassPackageController.getPlanStages('All');
        MVTPassPackageController.SaveNewTPassDetail(jsonOobjTPD, oTPD.Priority__c, oTPD.OAR_Metric_Template__c, acc.Id);
        MVTPassPackageController.getDosiometristInfo();
        MVTPassPackageController.getDosiometrists();
        MVTPassPackageController.savePriority(oTPD.Id, oTPD.Priority__c, oTPD.Plan_Stage__c, oTPD.Plan_Stage__c, u.Id);
        MVTPassPackageController.savePlanStage(oTPD.Id, oTPD.Plan_Stage__c);
        
        MVTPassPackageController.saveComment(oTPD.Id, oTPC.Body__c);
        MVTPassPackageController.get15DigitId();
        
        
        test.startTest();
            
            MVTPassPackageController.getTPassComments(oTPD.Id,1,20,10);
            
            oTPD = [select Id,Plan_Stage__c from TPaaS_Package_Detail__c where id =: oTPD.Id];
            oTPD.Plan_Stage__c = 'Structures Ready for Review';
            update oTPD;
            
            
            oTPD = [select Id,Plan_Stage__c from TPaaS_Package_Detail__c where id =: oTPD.Id];
            oTPD.Plan_Stage__c = 'Completed';
            update oTPD;
            
            
            string EsDate = dtDate.addMonths(-2).month()+ '/' + dtDate.addMonths(-2).day()+ '/' + dtDate.addMonths(-2).year();
            string EeDate = dtDate.addMonths(2).month()+ '/' + dtDate.addMonths(2).day()+ '/' + dtDate.addMonths(2).year();
            
            string Document_TypeSearch = ApexPages.currentPage().getParameters().get('st');
            string Priority = ApexPages.currentPage().getParameters().get('Priority');
            string PlanStage = ApexPages.currentPage().getParameters().get('PlanStage');
            string frmDate = ApexPages.currentPage().getParameters().get('fdate');
            string toDate = ApexPages.currentPage().getParameters().get('tdate');
            string ViewMyOrders = ApexPages.currentPage().getParameters().get('ViewMyOrders');
            
            //Excel
            PageReference pageRef = Page.TreatmentPlanningExcel;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('st', 'Test');
            ApexPages.currentPage().getParameters().put('Priority', '--Any--');
            ApexPages.currentPage().getParameters().put('PlanStage', '--Any--');
            ApexPages.currentPage().getParameters().put('fdate', EsDate);
            ApexPages.currentPage().getParameters().put('tdate', EeDate);
            ApexPages.currentPage().getParameters().put('ViewMyOrders', 'false');
            ApexPages.currentPage().getParameters().put('TPaasStatusLink', 'All');
            TreatmentPlanningExcel Excel = new TreatmentPlanningExcel();
            Excel.getTpassDetails();
        test.stopTest();
        
        }
    }
}
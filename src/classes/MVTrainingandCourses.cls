/*
* Author: Naren Yendluri
* Created Date: 11-Sep-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website Courses
*/



public class MVTrainingandCourses {
    
    // static integer counter=0;  //keeps track of the offset
    // static integer list_size=20; //sets the page size or number of rows
    //  static integer total_size; //used to show user the total size of the list
    // static String SelectedMenu='';        
    // static string strBanner ='';
    
   // Public List<Training_Dates__c> Trdate{get;set;}
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }

    @AuraEnabled
    public static String getLoggedInUserTerritory() {
        String territory;
        User logUser = [SELECT Id, Contact.Territory__c FROM User WHERE Id = :UserInfo.getUserId()];
        if(logUser.Contact.Territory__c != null) {
            territory = logUser.Contact.Territory__c;
        } else {
            territory = 'North America';
        }
        return territory;
    }    

    @AuraEnabled
    public static TrainingCoursesWrap getTrainingandCourses(string TitleId, string SelectedMenu ){ 
        
        //string SelectedMenu = '';
        
        string sortField = 'Title__c';
        string sortDir = 'asc';
        string shows='none';
        string show='';        
        
        boolean display;
        string CustEmail='';        
        string AcctCountry='';
        
        string Usrname;
        list<Regulatory_Country__c> RegionName=new list<Regulatory_Country__c>();
        List<Training_Course__c> pre= new List<Training_Course__c>();
        List<Training_Dates__c> Trdate = new List<Training_Dates__c>();
        List<Attachment> listAtts = new List<Attachment>();
        List<Note> listNotes = new List<Note>();
        
        TrainingCoursesWrap objTrCoursesWrap = new TrainingCoursesWrap();
        
        try{           
            
            boolean  isGuest = SlideController.logInUser();
            contact Cont = new contact(); // initilazation of variable
            list<selectoption> lstContentVersion=new list<selectoption>();
            
            setoptionsTraingCourseDate(TitleId);
            Trdate = getStrings(TitleId);
            
            pre=[select Id,Title__c,Designed_for__c,Replaces_Course__c,Region__c,PreRequisites__c,Software_Version__c,
                 Description__c,Language__c,Accreditation__c,FlexCredits__c,Training_Location__c,
                 (select Id,Name from Attachments),(SELECT Id,Title,Body from Notes) 
                 from Training_Course__c where id =: TitleId];

            listAtts = pre[0].Attachments;
            listNotes = pre[0].Notes;
            
            User usr = [Select ContactId,LMS_Status__c, Profile.Name from user where Id=: UserInfo.getUserId()];  
            if(usr.LMS_Status__c <> null && usr.LMS_Status__c == 'Active'){
                objTrCoursesWrap.isLmsUser = true;
            }else{
                 objTrCoursesWrap.isLmsUser = false;
            }
            
            String usrContId = usr.contactid;
            System.debug('------user type--------='+ usrContId );
            if(usrContId != null){
                display=true;
                Contact c = [select FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, 
                             MailingState, MailingPostalCode, MailingCountry, MailingCity,Territory__c 
                             from contact where Id =: usrContId];
                cont=c;
                CustEmail =cont.Email;
                String MailCountry = cont.MailingCountry;
                AcctCountry=MailCountry;
                RegionName = [ Select Portal_Regions__c from Regulatory_Country__c where Name=:MailCountry limit 1];
                
                if(RegionName.size() >0){   
                    objTrCoursesWrap.registerUrl = Label.CpTrainingSABAurl;
                }else{
                    objTrCoursesWrap.registerUrl = '/apex/CpTrainingRegistration?id='+TitleId;
                }
                
                if(RegionName.size() >0){
                    if(RegionName[0].Portal_Regions__c =='N. America'){
                        show='block';       
                    }else{
                        show='none';
                    }         
                }
                
                Cookie CUTerritory = ApexPages.currentPage().getCookies().get('UTerritory');
                
                if(CUTerritory <> null && CUTerritory.getValue() <> null && CUTerritory.getValue() <> ''){
                    string UTerritory = CUTerritory.getValue();
                    UTerritory = UTerritory.replace('%20',' ');
                    SelectedMenu = (UTerritory).toLowercase();  
                }
            }
            else{
                display=false;
            }
            if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null){
                if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                    Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                    shows='block';
                    isGuest=false;
                }
            }
            
        }catch(exception ex){
            
        }
        objTrCoursesWrap.lstTrCourse = pre;
        objTrCoursesWrap.lstTrdate = Trdate;
        objTrCoursesWrap.lstAtts = listAtts;
        objTrCoursesWrap.lstNotes = listNotes;
        return objTrCoursesWrap;
    }
    
    //This method is used get the data from Training_Course object that will be shown in the data table.
    @AuraEnabled
    public static TrCoursesWrap getContentVersions(string Document_TypeSearch,String SelectedMenu,
                                                   Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        if(SelectedMenu == null
            || SelectedMenu == ''
            || SelectedMenu == 'undefined') {

            User logUser = [SELECT Id, Contact.Territory__c FROM User WHERE Id = :UserInfo.getUserId()];
            if(logUser.Contact.Territory__c != null) {
                SelectedMenu = logUser.Contact.Territory__c;
            } else {
                SelectedMenu = 'North America';
            }
        }
        
        integer counter=0;  //keeps track of the offset
        integer list_size=20; //sets the page size or number of rows
        integer total_size = -1; 
        
        if(recordsOffset != null){
            counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null){
            list_size = integer.valueof(pageSize);
        }
        
        string sortField ='Title__c';
        string sortDir ='asc';
        string whr='';
        
        try{
            List<Training_Course__c> lContentVersions  = new List<Training_Course__c>();         
            
            set<Id> contntId = new set<Id>();
            String temp =  Document_TypeSearch + '*';
            
            String Query = 'select Title__c,Region__c,Training_Location__c from Training_Course__c' ;
            String QueryCnt = 'select count() from Training_Course__c where Title__c != null'; //where id != null and RecordType.name = \'Product Document\''; 
            // total_size = -1;
            
            if(string.isNotBlank(Document_TypeSearch)){
                system.debug('Document_TypeSearch->'+Document_TypeSearch);
                String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING Training_Course__c(id)';
                List<List<SObject>> searchList = search.query(searchquery);
                Training_Course__c[] cn = ((List<Training_Course__c>)searchList[0]);
                
                whr =  ' where id in : cn';
                total_size = cn.size(); 
                
                system.debug('cn->'+cn);
                system.debug('whr->'+whr);
            }        
            
            if(string.isNotBlank(SelectedMenu)){
                if(whr <> '' && whr <> null){
                    Query += whr+' and Region__c = \''+SelectedMenu+'\' ';  
                    QueryCnt += whr+' and Region__c = \''+SelectedMenu+'\' '; 
                }
                else{
                    Query += ' where Region__c = \''+SelectedMenu+'\' ';
                    QueryCnt += ' and Region__c = \''+SelectedMenu+'\' '; 
                }
            }
            
            string orderby = ' order by ' + sortField + ' '+ sortDir;  
            Query = Query + orderby +' limit ' + list_size + ' offset ' +counter;
            System.debug('Query ------- 343 --------  >>>>>' + Query );
            System.debug('QueryCnt ------- 123 --------  >>>>>' + QueryCnt);
            if(total_size < 0)
                total_size = DataBase.countQuery(QueryCnt);
            
            List<Sobject> sobj = DataBase.Query(Query);
            //total_size = sobj.size();
            For(Sobject s : Sobj){
                lContentVersions.add((Training_Course__c)s);
            }
            
            TrCoursesWrap objTrCoursesWrap = new TrCoursesWrap(lContentVersions, total_size);
            return objTrCoursesWrap;
            //return lContentVersions;
        }
        catch (QueryException e){   
            return null;
        }
    }
    
    
    Public static List<SelectOption> TraingCourseDate= new List<SelectOption>();
    public static List<SelectOption> getTraingCourseDate(){
        return TraingCourseDate;
    }
    
    public static List<Training_Dates__c> getStrings(string TitleId){
        Date dToday = System.Today() - 1;
        List<Training_Dates__c> Trdate= new List<Training_Dates__c>([Select Duration_in_days__c,Name, From__c,To__c 
                                                                     from Training_Dates__c 
                                                                     where Training_Course__c=:TitleId 
                                                                     and From__c > :dToday order by From__c]);
        return Trdate;
    }
    
    //This Method is used to get the training datae on the basis of Title id
    Public static void setoptionsTraingCourseDate(string TitleId){
        System.debug('------tttttttt--------='+ TitleId);
        List<Training_Dates__c> trngdate = new List<Training_Dates__c>([Select Duration_in_days__c,Name, From__c,To__c 
                                                                        from Training_Dates__c 
                                                                        where Training_Course__c=:TitleId]);
        for(Training_Dates__c values: trngdate ){
            TraingCourseDate.add(new selectoption(String.valueof(values.From__c)+' - '+String.valueof(values.To__c),String.valueof(values.From__c)+' - '+String.valueof(values.To__c))); 
        }
        
    }
    
    /*public static void FormMenuClick(){
        string menu = ApexPages.currentPage().getParameters().get('SelectedMenu');
        SelectedMenu = menu;
        Cookie UTerritory = new Cookie('UTerritory', SelectedMenu,null,-1,false);
        ApexPages.currentPage().setCookies(new Cookie[]{UTerritory});
        counter=0; 
        total_size = -1;
    }*/
    
    /*
    public class TrainingCoursesWrap{
        @AuraEnabled Public List<Training_Course__c> lstTrCourse{get;set;}
        @AuraEnabled Public List<Training_Dates__c> lstTrdate{get;set;}
        @AuraEnabled public boolean isLmsUser {get;set;}
        @AuraEnabled public String registerUrl {get;set;}
        public TrainingCoursesWrap(){
            
        }
    }
    */

    public class TrainingCoursesWrap{
        @AuraEnabled Public List<Training_Course__c> lstTrCourse{get;set;}
        @AuraEnabled Public List<Training_Dates__c> lstTrdate{get;set;}
        @AuraEnabled Public List<Attachment> lstAtts{get;set;}
        @AuraEnabled Public List<Note> lstNotes{get;set;}
        @AuraEnabled public boolean isLmsUser {get;set;}
        @AuraEnabled public String registerUrl {get;set;}
        public TrainingCoursesWrap(){
            
        }
    }


    public class TrCoursesWrap{      
        @AuraEnabled public list<Training_Course__c> lstTrainingCourses{get;set;}
        @AuraEnabled public integer totalSize{get;set;}
        @AuraEnabled public boolean isLms {get;set;}
        @AuraEnabled public String registerUrl {get;set;}
        public TrCoursesWrap(list<Training_Course__c> lstTrainingCourses, integer totalSize){
            this.lstTrainingCourses = lstTrainingCourses;
            this.totalSize = totalSize;
        }
    }
}
@isTest(seeAllData = true)
private class MVTrainingandCoursesTest {
    
    @isTest static void getCustomLabelMapTest() {
        test.startTest();
            map<String,String> mapTest = MVTrainingandCourses.getCustomLabelMap('en');
        test.stopTest();
    }
    
    @isTest static void getTrainingandCoursesTest() {
        List<Training_Course__c> list1 =[select Id,Title__c,Designed_for__c,Replaces_Course__c,Region__c,PreRequisites__c,Software_Version__c,
                 Description__c,Language__c,Accreditation__c,FlexCredits__c,Training_Location__c,
                 (select Id,Name from Attachments),(SELECT Id,Title,Body from Notes) 
                 from Training_Course__c limit 1];

        test.startTest();
            User curUser = [SELECT ContactId,LMS_Status__c, Profile.Name from user 
                    WHERE ContactId != NULL and isActive = true and LMS_Status__c = 'Active' Limit 1];

            System.runAs(curUser) {
                MVTrainingandCourses.getTrainingandCourses(list1[0].Id, 'selectMenu');
            }
        test.stopTest();        
    }


    @isTest static void getContentVersionTest() {
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;

        ContentVersion testContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = '1234',
                                                       Document_Version__c = 'V1.1',
                                                       RecordTypeId = RecId,
                                                       Title = 'testTitle',
                                                       ContentURL = 'http://www.google.com/');
        insert testContent;

        ContentVersion testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];

        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace LIMIT 1]; // WHERE Name = 'Acuity']; 

        ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc();
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContents.ContentDocumentId;
        //insert newWorkspaceDoc;

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='United States',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='test.test667test@gmail.com', AccountId=a.Id,MailingCountry ='United Kingdom', MailingState='CA' ); 
            insert con;
            
            Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
            Groupmember gm = new groupmember(GroupId = group1.id, UserOrGroupId = UserInfo.getuserId());
            insert gm;

            Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Portal User%' Limit 1];
        }

        test.startTest();
            MVTrainingandCourses.getContentVersions( 'Manual', 'selectMenu',  5, 10, 5); 
        test.stopTest();
    }

}
/*
* Author: Naren Yendluri
* Created Date: 28-Aug-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website TreueBeam Developer Mode page
*/


public without sharing class MVTrueBeamDeveloperMode {
    
    public List<String> Picklistvalue {get; set;}
    public List<String> options{get; set;} 
    public boolean acknowledge{get; set;}
    public static boolean termsAccepted{get; set;}
    public string termsAccepted2{get; set;}
    
    public boolean IsProductTrueBeam{get; set;}
    public string display{get; set;}
    public string IsVisible{get; set;}
    
    public boolean termsAcceptedLegal{get; set;}
    
    public static List<Contact> cont = new List<Contact>();
    //Public Transient List<Attachment > attachment {get;set;}
    public List<ContentVersion> attachment {get;set;}
    public String TBtype{get;set;}
    public String RecID{get;set;}
    public String msage{get;set;} 
    public static List<Developer_Mode__c> lDevMode{get;set;}
    List<User> portalUser;
    List<MarketingKitRole__c> markkit; 
    public String shows{get;set;}
    public String Usrname{get;set;}
    
    public static List<picklistEntry> entryList{get;set;}
    
    //To get Custom labels
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        /*map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        return customLabelMap;*/
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<picklistEntry> getPicklistValues(){
        String sObjectName = 'Developer_Mode__c';
        String fieldName ='Type__c';
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(sObjectName);         
        Schema.DescribeSObjectResult sObjRes = sObj.getDescribe();
        Schema.DescribeFieldResult sObjFieldRes = sObjRes.fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> lstPlE = sObjFieldRes.getPicklistValues();
        //List<picklistEntry> entryList = new List<picklistEntry>();
        entryList = new List<picklistEntry>();
        for(Schema.PicklistEntry ple : lstPlE){
            picklistEntry pleObj = new picklistEntry(ple.getLabel(),ple.getValue());
            entryList.add(pleObj);
        }
        return entryList;
    }
    
    //This Method is used for whether the term accepted page shown or not 
    //and whether the user will be able to see the true beam page or not on the basis of condition.
    public void getMVTrueBeamDeveloperMode(string TBtype){
        //shows = 'none';
        portalUser = [Select u.Id, u.ContactId, u.AccountId,u.alias From User u where id =: UserInfo.getUserId() limit 1];
        //system.debug('-------------->'+portalUser[0].AccountId);
        getAgreementShow();
        if(portalUser.size() > 0){
            if(portalUser[0].contactId == null){
                display='none';
                IsVisible='block';
                // shows = 'block';
                // Usrname = portalUser[0].alias;
                termsAccepted = true;
                getPicklistvalues();
            }
            
            String ProdPartNum= system.label.MV_PartNumber;
            markkit= [select id,name,Product__r.Model_Part_Number__c 
                      from MarketingKitRole__c 
                      where Product__r.Model_Part_Number__c=:ProdPartNum and Account__r.id=:portalUser[0].AccountId
                      limit 1];
            //system.debug('-------------->'+markkit);
            if(markkit.size() > 0){
                // TBtype = Apexpages.currentPage().getParameters().get('TBtype');
                
                lDevMode = new List<Developer_Mode__c>();
                
                if(TBtype != null){          
                    attachment = new list<ContentVersion>();
                    lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,
                                 (Select Title, Description, LastModifiedDate From Content__r) 
                                 From Developer_Mode__c d 
                                 where Type__c =: TBtype];
                    
                    for(Developer_Mode__c dm : lDevMode){             
                        attachment.addAll(dm.Content__r);      
                    }
                }
                else{ 
                    getPicklistvalues();
                    List<User> userList = [select id, ContactId,alias from User where id = : Userinfo.getUserId() limit 1];
                    if(userList.size() > 0){
                        cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
                    }
                    
                    if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c){
                        display='none';
                        IsVisible='block';
                    }else{
                        if(userList[0].contactId == null){
                            display='none';
                            IsVisible='block';
                            // shows = 'block';
                            // Usrname = userList[0].alias;
                        }else{
                            IsVisible='none';
                            display='block';
                        }
                    }
                }
            }
            else{
                system.debug('------Working-------->');
            }
        }
    }
    @AuraEnabled
    public static Boolean getAgreementShow(){
        
        List<User> userList = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];
        
        if(userList.size() > 0){
            cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
        }
        
        if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c==true){
            termsAccepted = true;
           // IsVisible='block';
        }
        else{  
            termsAccepted = false;
          //  IsVisible='block';
        }
        
        if(userList[0].contactId == null){
           // display='none';
           // IsVisible='block';
            termsAccepted = true;
        }
        return termsAccepted;
    }
    
    //register terms and conditions
    @AuraEnabled
    public static Boolean registerAgreement(){
       Boolean result = false;
        List<User> userList = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];        
        if(userList.size() > 0){
            cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
        }
        if(cont.size() > 0){
            cont[0].TrueBeam_Accepted__c = true;
            try{
                update cont;
                publicgroupadd(); 
                result = true;
            }
            catch(Exception ex){
                system.debug(ex.getMessage());
                result = false;
            }
        }
        return result;
    }
    
    @future
    public static void publicgroupadd(){
        Group grp = [Select id from group where developername ='Developer_Mode'];
        GroupMember gm = new groupMember(GroupId = grp.id,UserOrGroupId = UserInfo.getUserId());
        insert gm;
    }
    
    @AuraEnabled
    public static List<TrueBeamWrapper> getTBDevModeValues(){
        list<TrueBeamWrapper> lstTrueBeamWrapper = new List<TrueBeamWrapper>();
        List<Developer_Mode__c> lstDevMode = new List<Developer_Mode__c>();
        getPicklistValues();
        lstDevMode  = [Select Id,Type__c,Title__c,LastModifiedDate 
                     From Developer_Mode__c];
        System.debug('#### debug entryList = ' + entryList);
        
        for(picklistEntry pe:entryList){
            list<Developer_Mode__c> lstDevModeRec = new list<Developer_Mode__c>();
            for(Developer_Mode__c dm:lstDevMode){
                if(dm.Type__c==pe.label){
                    lstDevModeRec.add(dm);
                }
            }
            lstTrueBeamWrapper.add(new TrueBeamWrapper(pe.label,lstDevModeRec));
        }

        System.debug('#### debug lstTrueBeamWrapper = ' + lstTrueBeamWrapper);
        return lstTrueBeamWrapper;
    }
    
    @AuraEnabled
    public static TrueBeamSummaryWrapper getTBDevModeSummaryValues(string RecID,string TBtype){
        list<ContentVersion> attachment = new list<ContentVersion>();
        List<Developer_Mode__c> lDevMode = new List<Developer_Mode__c>();
        User un = [Select alias,ContactId from user where id =: UserInfo.getUserId() limit 1];
        
        if(TBtype != null){
            set<string> stContentdocid=new set<string>();
            Map<String, List<Developer_Mode__c>> map_Title_Mc = new Map<String, List<Developer_Mode__c>>();
            List<Developer_Mode__c> lMonteCarlo = new List<Developer_Mode__c>();
            lMonteCarlo = [Select d.Type__c,Title__c,CreatedBy.name,Description__c,LastModifiedDate 
                           From Developer_Mode__c d 
                           where id=: recid and Type__c =: TBtype];
            
            for(Developer_Mode__c c : lMonteCarlo){
                if(map_Title_Mc.containsKey(c.Title__c)){
                    List<Developer_Mode__c> lMc = map_Title_Mc.get(c.Title__c);
                    lMc.add(c);
                    map_Title_Mc.remove(c.Title__c);
                    map_Title_Mc.put(c.Title__c, lMc);
                }
                else{
                    List<Developer_Mode__c> lMc = new List<Developer_Mode__c>();
                    lMc.add(c);
                    map_Title_Mc.put(c.Title__c, lMc);
                }
                
            }
            lDevMode = [Select d.Type__c,Title__c,CreatedBy.name,Description__c,LastModifiedDate,
                        (Select Title,ContentDocumentId,contentSize, Description, LastModifiedDate 
                         From Content__r order by VersionNumber Desc) 
                        From Developer_Mode__c d where id=: recid and Type__c =: TBtype];
            
            Map<ID, List<ContentVersion>> map_Mc_ContentVer = new Map<ID, List<ContentVersion>>();
            List<ContentVersion> lContentVersion = new List<ContentVersion>();
            lContentVersion = [SELECT Id,ContentDocumentId,Description,ContentSize,CreatedDate,
                               Developer_Mode__c,Title,VersionNumber 
                               FROM ContentVersion 
                               where Developer_Mode__c != null  
                               order by VersionNumber Desc];
            
            for(ContentVersion  c : lContentVersion){                
                If(!stContentdocid.contains(c.ContentDocumentId)){
                    if(!map_Mc_ContentVer.containsKey(c.Developer_Mode__c)){                        
                        List<ContentVersion> lMc = new List<ContentVersion>();
                        lMc.add(c);
                        map_Mc_ContentVer.put(c.Developer_Mode__c, lMc);
                    }
                    else{  
                        List<ContentVersion> lMc = map_Mc_ContentVer.get(c.Developer_Mode__c);
                        lMc.add(c);
                        map_Mc_ContentVer.remove(c.Developer_Mode__c);
                        map_Mc_ContentVer.put(c.Developer_Mode__c, lMc);
                    }                    
                    stContentdocid.add(c.ContentDocumentId);
                }        
            }
            
            Set <String> sTitleKeys = new Set<String>();
            sTitleKeys = map_Title_Mc.keySet();
            
            for(String s : sTitleKeys){               
                List<Developer_Mode__c> lMonte = map_Title_Mc.get(s);
                
                if(lMonte != null){
                    for(Developer_Mode__c mc : lMonte){
                        List<ContentVersion> lc = new List<Contentversion>();
                       // system.debug('***mc.id-->'+mc.id);
                        lc = map_Mc_ContentVer.get(mc.id);
                        
                        if(lc != null)
                            attachment.addAll(lc);
                    }  
                }
            }
        }
        Developer_Mode__c TBDevMode = new Developer_Mode__c();
        if(!lDevMode.isEmpty()){
            TBDevMode = lDevMode[0];
        }
        // Test data start
       /* attachment = [SELECT Id,ContentDocumentId,Description,ContentSize,CreatedDate,
                      Developer_Mode__c,Title,VersionNumber 
                      FROM ContentVersion  
                      order by VersionNumber Desc limit 2];
        */
        // Test data End
        TrueBeamSummaryWrapper tbSummary  = new TrueBeamSummaryWrapper(TBDevMode,attachment);
        return tbSummary;
        
    }    
    
    public void VR_CP_TBRecPageBackup(){           
        //acknowledge = false;
        //Id tbId = Apexpages.currentPage().getParameters().get('Id');
        User un = [Select contactid,alias from user where id =: userinfo.getuserId() limit 1];
      //  shows = 'none';
        if(un.contactid == null){
         //   shows = 'block';
            Usrname = un.alias;
        }
        TBtype = Apexpages.currentPage().getParameters().get('TBtype');
        
        if(TBtype=='Legal Agreement'){
            lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,
                         (Select Title, Description, LastModifiedDate From Content__r) 
                         From Developer_Mode__c d where Type__c =: TBtype];
            
            termsAcceptedLegal=false;
        }
        else{            
            lDevMode = new List<Developer_Mode__c>();
            
            if(TBtype != null){                
                termsAcceptedLegal=true;          
                attachment = new list<ContentVersion>();
                
                lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,
                             (Select Title, Description, LastModifiedDate From Content__r) 
                             From Developer_Mode__c d where Type__c =: TBtype];
                
                for(Developer_Mode__c dm : lDevMode){             
                    attachment.addAll(dm.Content__r);      
                }
                if (attachment.size() >0){
                    
                }
            }else{ 
                termsAcceptedLegal=true;
                getPicklistvalues();
                List<User> userList = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];
               // system.debug('%%%%%%%%%%%%%%%%%%'+userList);
                if(userList.size() > 0){
                    cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
                   // system.debug('-------------->'+cont);
                }
                
                if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c){
                    termsAccepted = true;                    
                }else{
                    termsAccepted = false;
                }
            }
        }
    }
    
    public class picklistEntry{
    @AuraEnabled public string label{get;set;}
    @AuraEnabled public string pvalue{get;set;}
    public picklistEntry(string text, string value){
            label = text;
            pvalue = value;
        }
    }
    
    public class TrueBeamWrapper{
    @AuraEnabled public string label{get;set;}
    @AuraEnabled public list<Developer_Mode__c> lstDevMode{get;set;}
    public TrueBeamWrapper(string text, list<Developer_Mode__c> lstDevMode){
            this.label = text;
            this.lstDevMode = lstDevMode;
        }
    }
    
    public class TrueBeamSummaryWrapper{
        @AuraEnabled Developer_Mode__c TBDevMode{get;set;}
        @AuraEnabled public list<ContentVersion> lstDevMode{get;set;}
        public TrueBeamSummaryWrapper(Developer_Mode__c TBDevMode, list<ContentVersion> lstDevMode){
            this.TBDevMode = TBDevMode;
            this.lstDevMode = lstDevMode;
        }
    }
}
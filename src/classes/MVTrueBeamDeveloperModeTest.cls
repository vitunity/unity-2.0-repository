@isTest(seeAllData=true)
private class MVTrueBeamDeveloperModeTest {

    public static Regulatory_Country__c regcount ;
    public static Account act;  
    public static Contact con;
    public static User user;
	public static User thisUser;

    public static SVMXC__Installed_Product__c objIP;
    public static Product_Version__c pv; 

    public static Developer_Mode__c devMode;
    public static MarketingKitRole__c mkt;

    public static ContentVersion parentContent;
    public static ContentVersion testContent;
    public static ContentVersion testContents;
    public static ContentWorkspace testWorkspace;
    public static ContentWorkspaceDoc newWorkspaceDoc;

    static
    {

            thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
     
            //pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            //rol = [select id from UserRole LIMIT 1];           

            Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

            act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                        BillingStreet='xyx',Country__c='India', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
            insert act;  

            
            con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
                MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
                MailingState='CA', Phone = '12452234',
                MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
            //Creating a running user with System Admin profile
            insert con;

            UserRole rl = [SELECT Id from UserRole Limit 1];

            System.runAs(thisUser) {
                user  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                    lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                    username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
                insert user;
                Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
                Groupmember gm2 = new groupmember(GroupId = group1.id, UserOrGroupId = user.Id);
                insert gm2;                     
            }

            //Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity', Model_Part_Number__c = 'DEV001001001');
            //insert pr;

            //objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Company__c = act.id, SVMXC__Product__c = pr.Id);
            //insert objIP;     

            //pv = new Product_Version__c(product__c = pr.id);
            //insert pv;            

	        regcount = new Regulatory_Country__c();
	        regcount.Name = 'United Kingdom';
	        regcount.RA_Region__c ='NA';
	        regcount.Portal_Regions__c ='N.America,Test';
	        //insert regcount; 

	        devMode = new Developer_Mode__c(Type__c = 'Legal Agreement', Title__c = 'title');
	        insert devMode;

	        //mkt = new MarketingKitRole__c(product__c = pr.Id, Account__c = act.Id);
	        //insert mkt;         
			
    }	


	@isTest static void getCustomLabelMapTest() {
		Test.startTest();
			MVTrueBeamDeveloperMode.getCustomLabelMap('EN');
		Test.stopTest();
	}

	@isTest static void getPicklistValuesTest() {
		Test.startTest();
			MVTrueBeamDeveloperMode.getPicklistValues();
		Test.stopTest();
	}

	@isTest static void getMVTrueBeamDeveloperModeTest() {
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			System.runAs(user){
				ctrl.getMVTrueBeamDeveloperMode('Legal Agreement');
			}
		Test.stopTest();
	}	

	@isTest static void getMVTrueBeamDeveloperModeTest2() {
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			System.runAs(thisUser){
				ctrl.getMVTrueBeamDeveloperMode('Legal Agreement');
			}
		Test.stopTest();
	}	

	@isTest static void getAgreementShowTest() {
		Test.startTest();
			MVTrueBeamDeveloperMode.getAgreementShow();
		Test.stopTest();
	}

	@isTest static void registerAgreementTest() {
		Test.startTest();
			System.runAs(user) {
				MVTrueBeamDeveloperMode.registerAgreement();
			}
		Test.stopTest();
	}	

	@isTest static void publicgroupaddTest() {
		Test.startTest();
			MVTrueBeamDeveloperMode.publicgroupadd();
		Test.stopTest();
	}	

	@isTest static void getTBDevModeValuesTest() {
		Test.startTest();
			MVTrueBeamDeveloperMode.getTBDevModeValues();
		Test.stopTest();
	}		

	@isTest static void getTBDevModeSummaryValuesTest() {
		init();
		Test.startTest();
			MVTrueBeamDeveloperMode.getTBDevModeSummaryValues(devMode.Id, 'Legal Agreement');
		Test.stopTest();
	}	

	@isTest static void VR_CP_TBRecPageBackupTest1() {
		//Apexpages.currentPage().getParameters().set('TBtype', Legal Agreement);
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			System.runAs(user){
				ctrl.VR_CP_TBRecPageBackup();
			}
		Test.stopTest();
	}	

	@isTest static void VR_CP_TBRecPageBackupTest2() {
		devMode.Type__c = 'Legal Agreement2';
		update devMode;

		//Apexpages.currentPage().getParameters().set('TBtype', 'Legal Agreement2');
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			System.runAs(user){
				ctrl.VR_CP_TBRecPageBackup();
			}
		Test.stopTest();
	}	

	@isTest static void picklistEntryTest() {
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			System.runAs(user){
				//ctrl.picklistEntry = new ctrl.picklistEntry();
			}
		Test.stopTest();
	}	

	@isTest static void TestVar() {
		MVTrueBeamDeveloperMode ctrl = new MVTrueBeamDeveloperMode();
		Test.startTest();
			List<String> lst = ctrl.Picklistvalue;
			List<String> lst1 = ctrl.options;
			Boolean b1 = ctrl.acknowledge;
			String st = ctrl.termsAccepted2;
			Boolean b2 = ctrl.IsProductTrueBeam;
			List<ContentVersion> lst3 =  ctrl.attachment;
			String st2 = ctrl.RecID;
			String st3 = ctrl.msage;
			String st4 = ctrl.shows;
			String st5 = ctrl.Usrname;

			String st6 = ctrl.display;
			String st7 = ctrl.IsVisible;

		Test.stopTest();
	}	
	


    private static void init() {
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;

        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc',
                                                       Developer_Mode__c = devMode.Id);
        insert parentContent;

        testContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'English1234',
                                                       Document_Version__c = 'V1.1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description',
                                                       Parent_Documentation__c = 'prentDoc',
                                                       Developer_Mode__c = devMode.Id);
        insert testContent;

        testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];

        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = 'Acuity']; 

        newWorkspaceDoc =new ContentWorkspaceDoc();
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContents.ContentDocumentId;
        insert newWorkspaceDoc;       

        //ContentWorkspaceDoc testC = [Select c.ContentDocumentId From ContentWorkspaceDoc c  where ContentWorkspace.Name = 'Acuity'];
        //system.debug('#### debug testC = ' + testC); 
    }			
}
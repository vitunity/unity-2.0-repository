public class MV_BaseHomeController {
    
	@AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        system.debug('custom labels ==='+customLabelMap);
        return customLabelMap;
    }
}
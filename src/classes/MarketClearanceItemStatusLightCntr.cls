/***************************************************************************
Author: Anshul Goel
Created Date: 28-Nov-2017
Project/Story/Inc/Task : Sales Lightning : STRY0029558
Description: 
This is a controller for MarketClearanceItemStatus component.  This controller is used to show product profile and item record detail for selected item.
Change Log:
*************************************************************************************/

public class MarketClearanceItemStatusLightCntr 
{

    @AuraEnabled public Input_Clearance__c inputClearance {get;set;}
    @AuraEnabled public List<ClearanceItemRecord> clearanceItemRecords{get;set;}
    
    
    /***************************************************************************
    Description: 
    This is init method for the component. It will fire for initial component load. In this method we are fetching clearance item record
    *************************************************************************************/

    @AuraEnabled
    public static MarketClearanceItemStatusLightCntr showClearanceItemRecords(String inputClearanceId, String reviewProductId)
    {
        MarketClearanceItemStatusLightCntr obj = new MarketClearanceItemStatusLightCntr();
        obj.inputClearance = [SELECT Id, Name, Review_Product__c,Review_Product__r.Name, Product_Profile_Name__c, Business_Unit__c, Clearance_Family__c, Model_Version__c, Model_Part_Number__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Clearance_Expiration_Date__c, Comments__c, Does_Not_Expire__c, Approved_Manufacturing_Site__c, Region__c, Expected_Clearance_Date__c, Status__c FROM Input_Clearance__c WHERE Id = :inputClearanceId]; 
        List<Item_Details__c> itemdetails = [SELECT Id, Name,Item_Level__c, Review_Product__c, Regulatory_Name__c, Clearance_Family__c, Business_Unit__c, Version_No__c FROM Item_Details__c WHERE Review_Product__c = :reviewProductId];
        obj.clearanceItemRecords = new List<ClearanceItemRecord>();
        
        Set<id> excludedItemIds = new Set<Id>();
        Map<Id, Blocked_Items__c> blockedItemsMap = new Map<Id, Blocked_Items__c>([SELECT Id, Item_Part__c, Feature_Name__c, Clearance_Status__c FROM Blocked_Items__c WHERE Input_Clearance__c = :inputClearanceId]);
        
        
        if(blockedItemsMap != null && blockedItemsMap.size() > 0)
        {
            for(Blocked_Items__c exItem: blockedItemsMap.values())
            {
                excludedItemIds.add(exItem.Item_Part__c);
            }
        }
        
        if(itemdetails != null)
        {
            for(Item_Details__c item: itemdetails)
            {
                ClearanceItemRecord itemRecord = new ClearanceItemRecord();
                itemRecord.businessUnit = item.Business_Unit__c;
                itemRecord.clearanceFamily = item.Clearance_Family__c;
                itemRecord.itemLevel = item.Item_Level__c;
                itemRecord.itempart = item.Name;
                itemRecord.versionNo = item.Version_No__c;
                itemRecord.regulatoryName = item.Regulatory_Name__c;
                
                // if Input clearance record status is 'Blocked' then status for all items is 'Blocked'
                if(obj.inputClearance.Status__c == 'Blocked')  
                {
                    itemRecord.Status = 'Blocked';
                }
                else if(item.Item_Level__c == '3rd Party') // if overall status is not 'Blocked' then for 3rd Partty items status is 'Unknown'
                {
                    itemRecord.Status = 'Unknown';
                    itemRecord.cellBackground = 'yellow'; // yellow background
                    itemRecord.textColor = 'black';
                }
                else if(excludedItemIds.contains(item.Id)) // if item-part is in list of blocked items, then status to be shown is 'Blocked'
                {
                    itemRecord.Status = 'Blocked';
                }
                else // status is 'Permitted' for all/rest fo the items
                {
                    itemRecord.Status = 'Permitted';
                    itemRecord.cellBackground = 'green'; // green background
                }

                obj.clearanceItemRecords.add(itemRecord);
            } 
        }    
        return obj;
    }

    // Wrapper Class for Clearance Item
    public class ClearanceItemRecord
    {
        @AuraEnabled public String Status {get; set;}
        @AuraEnabled public String itempart {get; set;}
        @AuraEnabled public String itemLevel {get; set;}
        @AuraEnabled public String versionNo {get; set;}
        @AuraEnabled public String businessUnit {get; set;}
        @AuraEnabled public String regulatoryName {get; set;}
        @AuraEnabled public String clearanceFamily {get; set;}
        @AuraEnabled public String cellBackground {get; set;}
        @AuraEnabled public String textColor {get; set;}
        
        public ClearanceItemRecord()
        {
            this.cellBackground = '#FF0000'; // red background
            this.textColor = 'white';
        }
    }
    
}
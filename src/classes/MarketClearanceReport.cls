/*************************************************************************\
    @ Author        : Sunil Bansal
    @ Date          : 26-Apr-2013
    @ Description   : This Controller will be called from Visualforce Page to show status of Input Clearance records as Custom Report and also to show detailed status with item details. 
    @ VF pages using this controller: MarketClearanceReport, MarketClearanceItemStatus
    @ Last Modified By  :  
    @ Last Modified On  :   
    @ Last Modified Reason  :   
    @ Apex test Class: MarketClearanceReportTest
****************************************************************************/

public class MarketClearanceReport 
{
    public Map<String, Map<Id, ClearanceRecord>> recordsMap = new Map<String, Map<Id, ClearanceRecord>>(); // final map shown on VF page
    public Map<Id, String> reviewProductNames;// 
    public Map<Id, String> productNames = new Map<Id, String>(); // Map of all Review Product Ids and Review Product record
    public Map<Id, String> countryNames = new Map<Id, String>(); // Map of all Country Ids and Country record
    
    public List<String> countryIds = new List<String>(); // used for sorting country records for display in Dropdown
    public List<String> countryNameList = new List<String>(); // used for sorting country records for display in Dropdown
    
    public Input_Clearance__c evidenceRecord {get; set;} // Used on the MarketClearanceItemStatus VF page for display
    public List<ClearanceItemRecord> clearanceItemRecords {get; set;} // Item detail records with status
    
    //public List<String> countrykeysList {get; set;}
    public String strVersionId {get; set;} // selected Review Product Id from Dropdown on the VF page
    public String strCountry {get; set;} // selected Country Id from Dropdown on the VF page
    
    public List<String> productIds = new List<String>(); // used for sorting review product records for display in Dropdown
    public List<String>  productNameList = new List<String>(); // used for sorting review productrecords for display in Dropdown
    
    
    /**
        * Constructor to initialize productNames map with Review Product id as key and name as value
        * and countryNames map with Country id as key and name as value
    */
    public MarketClearanceReport()
    {
        reviewProductNames = new Map<Id, String>(); // initialize the map
        // query all review product records, assumption is that these are approximately 200 records
        List<Review_Products__c> reviewProductRecords = [Select Id,Name,Product_Profile_Name__c From Review_Products__c order by Product_Profile_Name__c ];
       
        if(reviewProductRecords != null)
        {
            for(Review_Products__c rProduct: reviewProductRecords)
            {
                productIds.add(rProduct.Id); // this list is maintained for Sorting in dropdown
                productNameList.add(rProduct.Product_Profile_Name__c);//this list is maintained for Sorting in dropdown
                productNames.put(rProduct.Id, rProduct.Product_Profile_Name__c);
            }
        }
      
        // query all country records, assumption is that these are under 150 records
        List<Country__c> countryMap = [SELECT id, Name FROM Country__c order by Name];
        
        if(countryMap != null && countryMap.size() > 0)
        {
            for(Country__c cntryRec: countryMap)
            {
                countryIds.add(cntryRec.Id); // this list is maintained for Sorting in dropdown
                countryNameList.add(cntryRec.Name);//this list is maintained for Sorting in dropdown                
                countryNames.put(cntryRec.Id, cntryRec.Name); 
            }
        }
        //countrykeysList = new List<String>(); // initialize the list
    }
    
    /**
        This property methos is used in the VF page to display ReviewProduct Names
    */
    public Map<Id, String> getVersionNames()
    {
        List<String> prodNames = new List<String>();
        prodNames.addAll(reviewProductNames.values());
        prodNames.sort(); 
        List<Id> prodId = new List<Id>();
        prodId.addAll(reviewProductNames.keyset());
        for(Integer i=0; i < prodId.size(); i++)
        {
           reviewProductNames.put(prodId[i],prodNames[i]);
        }
      
        return reviewProductNames;
    }    

    /**
        * This property methos is used in the VF page to display ReviewProduct Names for a Version Id
        * @return Set of ReviewProduct Ids
    */    
    public Set<Id> getVersionIds()
    {
        return reviewProductNames.keySet();
    }
   
    /**
        * Return sorted list of Country Names for display on the VF page.
        * @return list of 'Country' values to display in dropdown on the VF page
    */
    public List<String> getCountryNames()
    {
        List<String> countryNames = new List<String>();
        countryNames.addAll(recordsMap.keySet());
        countryNames.sort(); 
        return countryNames;
    }
    
    /**
        * Initialize Country select list with country values to show in Drop down on the VF page.
        * @return list of 'SelectOption' values to display in dropdown on the VF page
    */
    public List<SelectOption> getAllCountries()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--All--'));
        for(Integer i=0; i < countryIds.size(); i++)
        {
            options.add(new SelectOption(countryIds[i],countryNameList[i]));
        }
        return options;
    }
    
    /**
        * Initialize Review Product select list with review product values to show in Drop down on the VF page.
        * @return list of 'SelectOption' values to display in dropdown on the VF page
    */
    public List<SelectOption> getAllReviewProducts()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--All--'));
      /*  for(Id revId: productNames.keySet())
        {
            String strId = revId;
            options.add(new SelectOption(strId,ProductNames.get(revId)));
        }*/
        for(Integer i=0; i < productIds.size(); i++)
        {
            options.add(new SelectOption(productIds[i],productNameList[i]));
        }
      
        return options;
    }

    /**
        * This method returns map of COuntry Id and Map of Review Product records for that country
        * also prepares a dummy record for a country if review product record does not exist for display in VF page
        * @return map of Country Id and Map of Review Product records for that country
    */
    
    public Map<String, Map<Id, ClearanceRecord>> getCountryRecords()
    {
        System.debug('reviewProductNames SIZE === '+reviewProductNames.size()); 
        System.debug('SCRIPT STATEMENTS EXECUTED =6= '+Limits.getScriptStatements());
        System.debug('COUNTRIES IN RECODRS MAP == '+recordsMap.keySet());


        for(String cntry: recordsMap.keySet())
        {
            Map<Id, ClearanceRecord> clearanceMap = recordsMap.get(cntry);
            if(clearanceMap != null) // below logic is if some Input Clearance record is deleted by chance, so then at least report is populated
            {
                for(Id revId:reviewProductNames.keySet())
                {
                    if(clearanceMap.get(revId) == null) // mean Input Clearance record for that Review Item not available
                    {
                        ClearanceRecord cr = new ClearanceRecord(); // so create a dummy record and add to map
                        cr.reviewProductId = revId;
                        cr.noRecord = true;
                        cr.cellBackground = 'green'; // green background
                        clearanceMap.put(revId, cr);
                    }
                }
            }
            else // below logic if no input clearance record exist for a particular country, may be newly created country
            {
                clearanceMap = new Map<Id, ClearanceRecord>();
                for(Id revId:reviewProductNames.keySet())
                {
                    ClearanceRecord cr = new ClearanceRecord();
                    cr.reviewProductId = revId;
                    cr.noRecord = true;
                    cr.cellBackground = 'green'; // green background
                    clearanceMap.put(revId, cr);
                }
                recordsMap.put(cntry, clearanceMap);
            } 
        } 

        System.debug('SCRIPT STATEMENTS EXECUTED =7= '+Limits.getScriptStatements());
        return recordsMap;
    }
    
    /**
        * This method is to prepare the Input Clearance data to be shown on the MarketClearanceReport VF page. 
        * The user must select a 'Review Product' or 'Country' to proceed. This check has been put as otherwise it was throwing Query Rows limit exception for 'All Review Products' and 'All Countries'
    */
    public void showClearanceData()
    {
    system.debug('inside Show clearance data>>>>');
        recordsMap = new Map<String, Map<Id, ClearanceRecord>>();
        reviewProductNames = new Map<Id, String>();
        if( (strVersionId == null || strVersionId == '') && (strCountry == null || strCountry == ''))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR , 'Please select at least one Review Product or Country.'));
            return;
        }
        Set<Id> versionIds = new Set<Id>();
        List<Input_Clearance__c> inputClearanceRecords = new List<Input_Clearance__c>();
        List<Review_Products__c> reviewProductRecords;
        System.debug('strVersionId === '+strVersionId);
        if(strVersionId != null && strVersionId != '')
            reviewProductRecords = [Select Id,Name, Product_Profile_Name__c From Review_Products__c where Id = :strVersionId order by Product_Profile_Name__c ];
        else
            reviewProductRecords = [Select Id,Name, Product_Profile_Name__c From Review_Products__c order by Product_Profile_Name__c ]; // milit is not required as assumed that there would be around 200 Review Product records
       System.debug('SCRIPT STATEMENTS EXECUTED =1= '+Limits.getScriptStatements());
       // reviewProductRecords.sort();
        if(reviewProductRecords != null)
        {
            for(Review_Products__c rProduct: reviewProductRecords)
            {            
                reviewProductNames.put(rProduct.Id, rProduct.Product_Profile_Name__c);
                versionIds.add(rProduct.Id);
            }
            if(strCountry != null && strCountry != '')
                inputClearanceRecords = [Select Id,Clearance_Entry_Form_Name__c, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Status__c From Input_Clearance__c where (Review_Product__c IN :versionIds AND Country__c = :strCountry) order by Country__r.Name]; // Limit not required as for a country assumed that there would be around 200 records
            else
                inputClearanceRecords = [Select Id,Clearance_Entry_Form_Name__c, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Status__c From Input_Clearance__c where Review_Product__c IN :versionIds order by Country__r.Name Limit 49000]; // here limit may not be required, but put on to be safer side.
            
        }
        System.debug('SCRIPT STATEMENTS EXECUTED =2= '+Limits.getScriptStatements());
        System.debug('PRODUCT VERSION RECORD COUNT == '+versionIds.size());
        System.debug('EVIDENCE RECORD COUNT == '+inputClearanceRecords.size());
       
        Map<String, Map<Id, ClearanceRecord>> countryClearanceMap = new Map<String, Map<Id, ClearanceRecord>>();
        if(inputClearanceRecords != null)
        {
            System.debug('SCRIPT STATEMENTS EXECUTED =3= '+Limits.getScriptStatements());
            for(Input_Clearance__c mce: inputClearanceRecords)
            {
                Map<Id, ClearanceRecord> clearanceRecords = new Map<Id, ClearanceRecord >();
                if(countryClearanceMap.containsKey(mce.Country__r.Name))
                {
                    clearanceRecords = countryClearanceMap.get(mce.Country__r.Name);
                }
                // create wrapper clearance record for each InputClearance record
                ClearanceRecord cr = new ClearanceRecord(mce.Review_Product__c, mce.Id, mce.Status__c);
                cr.Status = mce.Status__c;
                if(mce.Status__c == 'Permitted')
                {
                    cr.cellBackground = 'green'; // green background
                }
                else if(mce.Status__c == 'Permitted/Blocked')
                {
                    cr.Status = 'Permitted';
                    cr.cellBackground = 'yellow'; // yellow background
                    cr.textColor = 'black';
                }
                
                clearanceRecords.put(mce.Review_Product__c, cr);
                countryClearanceMap.put(mce.Country__r.Name, clearanceRecords);
            }
            List<String> countrykeysList = new List<String>();
            countrykeysList.addAll(countryClearanceMap.keySet());
            countrykeysList.sort();
            for(String ctry: countrykeysList)
            {
                recordsMap.put(ctry, countryClearanceMap.get(ctry));
            } 
          
            System.debug('SCRIPT STATEMENTS EXECUTED =5= '+Limits.getScriptStatements());
        }
    }
    /**
        * This method prepares the detailed Input Clearance record at item level. 
        * This ethod prepares the status for all teh Items details and show 'Blocked' status for items excluded for Item details. 
    */
    public void showClearanceItemRecords()
    {
        Id reviewProductId = Apexpages.currentPage().getParameters().get('versionId');
        Id inputClearanceId = Apexpages.currentPage().getParameters().get('evidenceId');
        Set<id> excludedItemIds = new Set<Id>();

        evidenceRecord = [SELECT Id, Name, Review_Product__r.Name, Review_Product__c, Product_Profile_Name__c, Business_Unit__c, Clearance_Family__c, Model_Version__c, Model_Part_Number__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Clearance_Expiration_Date__c, Comments__c, Does_Not_Expire__c, Approved_Manufacturing_Site__c, Region__c, Expected_Clearance_Date__c, Status__c FROM Input_Clearance__c WHERE Id = :inputClearanceId];        
        Map<Id, Blocked_Items__c> blockedItemsMap = new Map<Id, Blocked_Items__c>([SELECT Id, Item_Part__c, Feature_Name__c, Clearance_Status__c FROM Blocked_Items__c WHERE Input_Clearance__c = :inputClearanceId]);
        List<Item_Details__c> itemdetails = [SELECT Id, Name,Item_Level__c, Review_Product__c, Regulatory_Name__c, Clearance_Family__c, Business_Unit__c, Version_No__c FROM Item_Details__c WHERE Review_Product__c = :reviewProductId];
        
        if(blockedItemsMap != null && blockedItemsMap.size() > 0)
        {
            for(Blocked_Items__c exItem: blockedItemsMap.values())
            {
                excludedItemIds.add(exItem.Item_Part__c);
            }
        }
        
        clearanceItemRecords = new List<ClearanceItemRecord>();
        if(itemdetails != null)
        {
            for(Item_Details__c item: itemdetails)
            {
                ClearanceItemRecord itemRecord = new ClearanceItemRecord();
                itemRecord.businessUnit = item.Business_Unit__c;
                itemRecord.clearanceFamily = item.Clearance_Family__c;
                itemRecord.itemLevel = item.Item_Level__c;
                itemRecord.itempart = item.Name;
               // itemRecord.modelPartno = item.Model_Part_Number__c;
              itemRecord.versionNo = item.Version_No__c;
                //itemRecord.OMNIDescription = item.OMNI_Item_Decription__c;
               // itemRecord.OMNIProductGroup = item.OMNI_Product_Group__c;
               // itemRecord.OMNIFamily = item.OMNI_Family__c;
                itemRecord.regulatoryName = item.Regulatory_Name__c;
                // if Input clearance record status is 'Blocked' then status for all items is 'Blocked'
                if(evidenceRecord.Status__c == 'Blocked')  
                {
                    itemRecord.Status = 'Blocked';
                }
                else if(item.Item_Level__c == '3rd Party') // if overall status is not 'Blocked' then for 3rd Partty items status is 'Unknown'
                {
                    itemRecord.Status = 'Unknown';
                    itemRecord.cellBackground = 'yellow'; // yellow background
                    itemRecord.textColor = 'black';
                }
                else if(excludedItemIds.contains(item.Id)) // if item-part is in list of blocked items, then status to be shown is 'Blocked'
                {
                    itemRecord.Status = 'Blocked';
                }
                else // status is 'Permitted' for all/rest fo the items
                {
                    itemRecord.Status = 'Permitted';
                    itemRecord.cellBackground = 'green'; // green background
                }

                clearanceItemRecords.add(itemRecord);
            }
        }
    }
    
    /**
        * This is wrapper class to show right status for Input Clearance record on the VF page.
        * fields are initiated for color coding in this class
        * most field names are self explanatory
    */
    public class ClearanceRecord
    {
        public String reviewProductId {get; set;}
        public String inputClearanceId {get; set;}
        public String Status {get; set;}
        public String cellBackground {get; set;}
        public String textColor {get; set;}
        public Boolean noRecord {get; set;}
        
        public ClearanceRecord()
        {
            //this.cellBackground = '#FF0000'; // red background
            this.textColor = 'white';
            //this.noRecord = false;
        }
        
        public ClearanceRecord(Id reviewProductId, Id inputClearanceId, String Status)
        {
            this.reviewProductId = reviewProductId;
            this.inputClearanceId = inputClearanceId;
            this.Status = Status;
            this.cellBackground = '#FF0000'; // red background
            this.textColor = 'white';
            this.noRecord = false;
        }
    }

    /**
        * This wrapper class is cerated to show Item level status for each Item detail for the Input Clearance/ Review Product.
        * most field names are self explanatory
    */
    public class ClearanceItemRecord
    {
        public Id reviewProductId {get; set;} // Review Product Record Id
        public Id inputClearanceId {get; set;} // Input Clearance record Id
        public String Status {get; set;}
        public String itempart {get; set;}
       public String itemLevel {get; set;}
       public String versionNo {get; set;}
        public String businessUnit {get; set;}
        public String regulatoryName {get; set;}
        public String clearanceFamily {get; set;}
        public String cellBackground {get; set;}
        public String textColor {get; set;}
        
        public ClearanceItemRecord()
        {
            this.cellBackground = '#FF0000'; // red background
            this.textColor = 'white';
        }
    }
     /*
     * for lightning
     */
    public boolean getIsLightning(){
        /*
        User userObj = [SELECT Id, UserPreferencesLightningExperiencePreferred from User where Id =: UserInfo.getUserId() limit 001];
        return userObj.UserPreferencesLightningExperiencePreferred;
		*/
        if(ApexPages.currentPage().getParameters().get('sfdcIFrameHost') != null ||
           ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ||
           ApexPages.currentPage().getParameters().get('isdtp') == 'p1') {      
            return true;
        }
        else {      
            return false;
        }  
    }
     
}
/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning : STRY0029558
Description: 
This is a controller for MarketClearanceReport component.  This controller is used to show clearnce status for the product.
Change Log:

*************************************************************************************/

public class MarketClearanceReportLightningCtrl 
{
    
	@AuraEnabled public List<ReviewProduct> productProfileValues { get;set;}
    @AuraEnabled public List<Country> countryValues { get;set;}
    @AuraEnabled public String countryId { get;set;}
    @AuraEnabled public String countryName { get;set;}
    @AuraEnabled public Map<String,String> ppMap{ get;set;}
    @AuraEnabled public Map<String,String> ccMap{ get;set;}
    
    /***************************************************************************
    Description: 
    This is init method for the component. It will fire for initial component load. In this method we are setting product and country list to be selected by the user
    *************************************************************************************/

    @AuraEnabled
    public static MarketClearanceReportLightningCtrl initClass()
    {
        MarketClearanceReportLightningCtrl obj = new MarketClearanceReportLightningCtrl();
        obj.productProfileValues = new List<ReviewProduct>();
        obj.ppMap = new Map<String,String>();
        obj.ccMap = new Map<String,String>();
       
       //Fetching all Product Profile
       for(Review_Products__c r :[Select Id,Name,Product_Profile_Name__c  From Review_Products__c order by Product_Profile_Name__c ])
        {
           ReviewProduct rp = new ReviewProduct();
           rp.id= r.id;
           rp.name = r.name;
           rp.productProfileName = r.Product_Profile_Name__c;
           obj.ppMap.put(r.id,r.Product_Profile_Name__c);
           obj.productProfileValues.add(rp);
        }
        
        ReviewProduct rp = new ReviewProduct();
        rp.id = 'All';
        rp.productProfileName = '--All--';
        obj.productProfileValues.add(0,rp);

        obj.countryValues = new List<Country>();
        
        //Fetching all the countries
        for (Country__c con : [SELECT id, Name FROM Country__c order by Name])
        {
            Country c = new country();
            c.name = con.name;
            c.id = con.id;
            obj.ccMap.put(con.id,con.name);
            obj.countryValues.add(c);
        }
        Country c = new country();
        c.name = '--All--';
        c.id = 'All';
        obj.countryValues.add(0,c);
        obj.countryId =  obj.countryValues[0].Id;
        obj.countryName =  obj.countryValues[0].Name;
    	return obj;
    }
    
    
    /***************************************************************************
    Description: 
    This method will be invoke based on user selection for country and product. It will show product with clearance status for the selected country and product profile
    *************************************************************************************/
    @AuraEnabled
    public static List<Input_Clearance__c> showClearanceData(String productProfileId, String countryId, Map<String,String> ppMap)
    {
        List<Input_Clearance__c> inputClearanceList = new List<Input_Clearance__c>();

       
        // For all the countries
        if(countryId == 'All')
        {

            inputClearanceList = [Select Id,Clearance_Entry_Form_Name__c, Review_Product__r.Name, Review_Product__c,
                                                       Country__r.Name, Review_Product__r.Product_Profile_Name__c,
                                                       Country__c, Clearance_Status__c, Status__c , Name  From Input_Clearance__c 
                                                       where Review_Product__c = :productProfileId order by Country__r.Name limit 49000];
        }

        //For all the Product Profile
        else if(productProfileId == 'All')
        {

            inputClearanceList = [Select Id,Clearance_Entry_Form_Name__c, Review_Product__r.Name, Review_Product__c,
                                                       Country__r.Name, Review_Product__r.Product_Profile_Name__c,
                                                       Country__c, Clearance_Status__c, Status__c , Name  From Input_Clearance__c 
                                                       where (Review_Product__c  IN : ppMap.keyset() AND Country__c =: countryId) 
                                                       order by Country__r.Name];
        }

        else
        {

            inputClearanceList = [Select Id,Clearance_Entry_Form_Name__c, Review_Product__r.Name, Review_Product__c,
                                                       Country__r.Name, Review_Product__r.Product_Profile_Name__c,
                                                       Country__c, Clearance_Status__c, Status__c , Name  From Input_Clearance__c 
                                                       where (Review_Product__c =: productProfileId AND Country__c =: countryId) 
                                                       order by Country__r.Name];
        }
        return inputClearanceList;
    }
    
    
    public class Country
    {
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String id{get;set;}
    }


    public class ReviewProduct
    {
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String productProfileName{get;set;}
    }
}
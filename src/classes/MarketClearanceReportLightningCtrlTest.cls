/***************************************************************************
Author: Anshul Goel
Created Date: 28-Nov-2017
Project/Story/Inc/Task : Sales Lightning : STRY0029558
Description: 
This is a Test Class for MarketClearanceReportLightningCtrl and MarketClearanceItemStatusLightCntr
Change Log:
*************************************************************************************/

@isTest
private class MarketClearanceReportLightningCtrlTest
{
    /***************************************************************************
    Description: 
    This method is to used to test Init, ShowClearanceData and showClearanceItemRecords method of controller classes
    *************************************************************************************/
    @isTest static void testMethod1() 
    {
       //creating Test Data
        List<Product2> productList = new List<Product2>();
        Product2 pModel = new Product2();
        pModel.Name = 'Product1';
        pModel.ProductCode = 'BH321';
        pModel.Description = 'New product 1';
        pModel.Family = 'CLINAC';
        pModel.Item_Level__c = 'Model';
        pModel.Model_Part_Number__c = 'MCM565878687MCM';
        pModel.Product_Bundle__c = 'Clinac 11';
        pModel.OMNI_Family__c = 'Calypso';
        pModel.OMNI_Item_Decription__c = 'A clinac Clypso product';
        pModel.OMNI_Product_Group__c = 'Clinac 2100C';

        Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';

        productList.add(pModel);
        productList.add(pSubItem);
        insert productList;
       
        // create country record for which Input Clearance record would be created automatically
        Country__c cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        insert cntry;
       

        //create one review product record
        Review_Products__c rp = new Review_Products__c();
        rp.Product_Model__c = pModel.Id;
        rp.Version_No__c = '11';
        rp.Product_Profile_Name__c = pModel.Name+ ' '+rp.Version_No__c;
        rp.Clearance_Family__c = 'BrachyVision';
        rp.Business_Unit__c = 'VBT';
        rp.Regulatory_Name__c = 'Regular name';
        //rp.Name = 'PB-11111111';
        insert rp; // this should create Input clearance record for Country (cntry) record
       
        Country__c cntry2 = new Country__c();
        cntry2.Name = 'DubaDuba';
        cntry2.Region__c = 'USA';
        cntry2.Expiry_After_Years__c = '1';
        insert cntry2; 

        Input_Clearance__c ic2 = [SELECT Id, Name, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Status__c FROM Input_Clearance__c where Review_Product__c = :rp.Id and Country__c = :cntry2.Id LIMIT 1];

        List<Item_Details__c> detailList = new List<Item_Details__c>();
        Item_Details__c iDetail1 = new Item_Details__c();
        idetail1.Review_Product__c = rp.id;
        iDetail1.Name = pSubItem.Name;
        
        Item_Details__c iDetail2 = new Item_Details__c();
        iDetail2.Review_Product__c = rp.id;
        iDetail2.Name = pSubItem.Name;
        iDetail2.Item_Level__c = '3rd Party';
        

        Item_Details__c iDetail3 = new Item_Details__c();
        iDetail3.Review_Product__c = rp.id;
        iDetail3.Name = pSubItem.Name;
       // iDetail3.Item_Level__c = '3rd Party';
        detailList.add(iDetail1);
        detailList.add(iDetail2);
        detailList.add(iDetail3);
        insert detailList;

        //add blocked items for 'Permitted/Blocked' status of Input clearance record
        Blocked_Items__c bi = new Blocked_Items__c();
        bi.Input_Clearance__c = ic2.Id;
        bi.Clearance_Status__c = 'Blocked';
        bi.Feature_Name__c = 'FFF';
        bi.Item_Part__c = iDetail1.Id;
        insert bi;

        // End Test Data Creation 

        Test.starttest();

        //Code Coverage for MarketClearanceReportLightningCtrl
        MarketClearanceReportLightningCtrl mrLC = new MarketClearanceReportLightningCtrl();
        mrLC =  MarketClearanceReportLightningCtrl.initClass();
        List<Input_Clearance__c> lc =  MarketClearanceReportLightningCtrl.showClearanceData(rp.id,'All',mrlc.ppMap);
        List<Input_Clearance__c> lc1 =  MarketClearanceReportLightningCtrl.showClearanceData('All',cntry.id,mrlc.ppMap);
        List<Input_Clearance__c> lc2 =  MarketClearanceReportLightningCtrl.showClearanceData(rp.id,cntry.id,mrlc.ppMap);

        //Code Coverage for MarketClearanceItemStatusLightCntr for blocked item
        MarketClearanceItemStatusLightCntr  mrCI = new MarketClearanceItemStatusLightCntr ();
        mrCI =  MarketClearanceItemStatusLightCntr.showClearanceItemRecords(ic2.id,rp.id);

        ic2.Clearance_Status__c = 'Permitted';
        ic2.Clearance_Date__c = System.today(); // when status is permitted, then Clearance Date is required..
        update ic2;

        //Code Coverage for MarketClearanceItemStatusLightCntr for Permitted item
        mrCI =  MarketClearanceItemStatusLightCntr.showClearanceItemRecords(ic2.id,rp.id);
        Test.stopTest();
    }
}
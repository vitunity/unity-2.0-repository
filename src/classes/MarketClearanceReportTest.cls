/**
 * This class contains unit tests for validating the behavior of Apex classe 'MarketClearanceReport'
 * This test class also covers Trigger 'InputClearanceMaster_BeforeTrigger', 'Review_Products_AfterTrigger' and 'CountryMaster_AfterTrigger'
 */
@isTest
private class MarketClearanceReportTest {

  static testMethod void myUnitTest() 
    {

        // create a product to start with for the review product
        List<Product2> productList = new List<Product2>();
        Product2 pModel = new Product2();
        pModel.Name = 'Product1';
        pModel.ProductCode = 'BH321';
        pModel.Description = 'New product 1';
        pModel.Family = 'CLINAC';
        pModel.Item_Level__c = 'Model';
        pModel.Model_Part_Number__c = 'MCM565878687MCM';
        pModel.Product_Bundle__c = 'Clinac 11';
        pModel.OMNI_Family__c = 'Calypso';
        pModel.OMNI_Item_Decription__c = 'A clinac Clypso product';
        pModel.OMNI_Product_Group__c = 'Clinac 2100C';
        
        Product2 pSubItem = new Product2();
        pSubItem.Name = 'Product2';
        pSubItem.ProductCode = 'BH3212';
        pSubItem.Description = 'New Sub Item 2';
        pSubItem.Family = 'CLINAC';
        pSubItem.Item_Level__c = 'Sub-Item';
        pSubItem.Model_Part_Number__c = 'MCM5658781234MCM';
        pSubItem.Product_Bundle__c = 'Clinac 11';
        pSubItem.OMNI_Family__c = 'Calypso';
        pSubItem.OMNI_Item_Decription__c = 'A clinac Clypso product2';
        pSubItem.OMNI_Product_Group__c = 'Clinac 2100C';
        
        Product2 p3rdParty = new Product2();
        p3rdParty.Name = 'Product3';
        p3rdParty.ProductCode = 'BH333';
        p3rdParty.Description = 'New 3rd Party Product 2';
        p3rdParty.Family = 'CLINAC';
        p3rdParty.Item_Level__c = '3rd Party';
        p3rdParty.Model_Part_Number__c = 'MCM5123781234MCM';
        p3rdParty.Product_Bundle__c = 'Clinac 11';
        p3rdParty.OMNI_Family__c = 'Calypso';
        p3rdParty.OMNI_Item_Decription__c = 'A clinac Clypso product3';
        p3rdParty.OMNI_Product_Group__c = 'Clinac 2100C';
        
        productList.add(pModel);
        productList.add(pSubItem);
        productList.add(p3rdParty);
        insert productList;
        
        // create country record for which Input Clearance record would be cerated automatically
        Country__c cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        
        insert cntry;

        //create one review product record
        Review_Products__c rp = new Review_Products__c();
        rp.Product_Model__c = pModel.Id;
        rp.Version_No__c = '11';
        rp.Product_Profile_Name__c = pModel.Name+ ' '+rp.Version_No__c;
        rp.Clearance_Family__c = 'BrachyVision';
        rp.Business_Unit__c = 'VBT';
        rp.Regulatory_Name__c = 'Regular name';
        
        insert rp; // this should create Input clearance record for Country (cntry) record
        
        Country__c cntry2 = new Country__c();
        cntry2.Name = 'DubaDuba';
        cntry2.Region__c = 'USA';
        cntry2.Expiry_After_Years__c = '1';
        
        insert cntry2; // this should create Input clearance record for "Review Product" (rp) record
        
        cntry2.Name = 'DubaDuba1';
        update cntry2;
        
        List<Item_Details__c> detailList = new List<Item_Details__c>();
        Item_Details__c iDetail1 = new Item_Details__c();
        idetail1.Review_Product__c = rp.id;
        //iDetail1.Item_Part__c = 'item';
        iDetail1.Name = pSubItem.Name;
        
        Item_Details__c iDetail2 = new Item_Details__c();
        iDetail2.Review_Product__c = rp.id;
        //iDetail2.Item_Part__c = p3rdParty.Id;
        iDetail2.Name = pSubItem.Name;
        
        detailList.add(iDetail1);
        detailList.add(iDetail2);
        
        insert detailList;
        
        // query at least one input clearance record
        Input_Clearance__c ic = [SELECT Id, Name, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Clearance_Expiration_Date__c, Comments__c, Does_Not_Expire__c, Approved_Manufacturing_Site__c, Region__c, Expected_Clearance_Date__c, Status__c FROM Input_Clearance__c where Review_Product__c = :rp.Id LIMIT 1]; 
        
        //add blocked items for 'Permitted/Blocked' status of Input clearance record
        Blocked_Items__c bi = new Blocked_Items__c();
        bi.Input_Clearance__c = ic.Id;
        bi.Clearance_Status__c = 'Blocked';
        bi.Feature_Name__c = 'FFF';
        bi.Item_Part__c = iDetail1.Id;
        insert bi;
        
        ic.Clearance_Status__c = 'Permitted';
        ic.Clearance_Date__c = System.today(); // when status is permitted, then Clearance Date is required..
        update ic;
        
        Test.startTest();
        // now starts controller coverage
        PageReference pageRef = Page.MarketClearanceReport;
        Test.setCurrentPage(pageRef);
        MarketClearanceReport mcr = new MarketClearanceReport();
        mcr.strVersionId = rp.Id;
        //mcr.strCountry = cntry.Id;
        mcr.showClearanceData();
        mcr.getVersionNames();
        mcr.getVersionIds();
        mcr.getCountryNames();
        mcr.getAllCountries();
        mcr.getAllReviewProducts();
        mcr.getCountryRecords();
        
        ApexPages.currentPage().getParameters().put('versionId', rp.Id);
        ApexPages.currentPage().getParameters().put('evidenceId', ic.Id);
        mcr.showClearanceItemRecords();
        
        Input_Clearance__c ic2 = [SELECT Id, Name, Review_Product__r.Name, Review_Product__c, Country__r.Name, Country__c, Clearance_Status__c, Clearance_Date__c, Status__c FROM Input_Clearance__c where Review_Product__c = :rp.Id and Country__c = :cntry2.Id LIMIT 1];
        ic2.Clearance_Status__c = 'Permitted';
        ic2.Clearance_Date__c = System.today(); // when status is permitted, then Clearance Date is required..
        update ic2;
        
        ApexPages.currentPage().getParameters().put('versionId', rp.Id);
        ApexPages.currentPage().getParameters().put('evidenceId', ic2.Id);
        mcr.showClearanceItemRecords();
        //System.assertEquals(1, ic.Blocked_Items_Count__c);
        //System.assertEquals('Permitted/Blocked', ic.Status__c);
        Test.stopTest();
        
       
    }
}
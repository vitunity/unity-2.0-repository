/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 04-Apr-2013
    @ Description   : An APex Controller to get List of Messgaes from Message Centre.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class MessageController {

    //public list<banner> bannerList{get; set;}
   
    public List<MessageCenter__c> messagelist = new List<MessageCenter__c>([select id,Message_Date__c,title__c,product_affiliation__c,Message__c from MessageCenter__c order by Message_Date__c desc]);
    public List<wrapperclass> wrapperlist = new List<wrapperclass>();
    public List<MessageCenter__c> getmessagelist(){
    return messagelist;
    }
    
    public List<wrapperclass> getwrapperlist()
    {
     for(integer i=0;i<messageList.size();i++)
     {
      wrapperclass w = new wrapperclass();
      w.msg1 =messagelist[i];
      i++;
      if(i<messagelist.size())
      {
       w.msg2 = messagelist[i];
      }
      wrapperlist.add(w);
     }
     return wrapperlist;
    }
       public MessageController(StLoginCntlr controller) {
           
    }

   //class to store records to be displayed
   
    public class wrapperclass{
        public MessageCenter__c msg1{get;set;}
        public MessageCenter__c msg2{get;set;}
    
    }
   
     //constructor
     public MessageController (){           
             
     }
     
    
}
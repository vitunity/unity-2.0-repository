/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for MessageController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest/*(SeeAllData=true)*/

Public class MessageControllerTest{

    //Test Method for MessageController class
    
    static testmethod void testMessageController(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState ='teststate'); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser009@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            MessageCenter__c objmc=new MessageCenter__c(Message_Date__c=system.today(),Title__c='testtitle',Message__c='testmessage');
            insert objmc;

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


            MessageController MsgCntr = new MessageController();
            MessageController.wrapperclass  wrpcls = new MessageController.wrapperclass();
            List<MessageCenter__c> Mc  = MsgCntr.getmessagelist();
            MsgCntr.getwrapperlist();
            StLoginCntlr controller = new StLoginCntlr();
            MessageController ext = new MessageController(controller);
            }
            Test.StopTest();
    
        }
}
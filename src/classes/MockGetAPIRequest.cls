@isTest
global class MockGetAPIRequest implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"EX_APIREQID":[{}]}');
        res.setStatusCode(200);
        return res;
    }
}
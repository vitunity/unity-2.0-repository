@isTest
global class MockGetApiTypes implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"EX_APITYPE":[{"MANDT":"102","ID":"901","APITYPES":"ARIA ACCESS"},{"MANDT":"102","ID":"902","APITYPES":"ARIA CONNECT"},{"MANDT":"102","ID":"903","APITYPES":"FULL ACCESS TO DB"},{"MANDT":"102","ID":"904","APITYPES":"3RD PARTY ACCESS"}],"EX_SOFTNAME":[{"MANDT":"102","ID":"901","SOFTNAME":"ARIA"},{"MANDT":"102","ID":"902","SOFTNAME":"ECLIPSE"}]}');
        res.setStatusCode(200);
        return res;
    }
}
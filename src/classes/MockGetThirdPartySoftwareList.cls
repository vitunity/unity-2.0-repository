@isTest
global class MockGetThirdPartySoftwareList implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"EX_3RDPARTYSOF":[{"MANDT":"102","ID":"901","SW_NAME":"UNLIMITED SYSTEMS"},{"MANDT":"102","ID":"902","SW_NAME":"EQUICARE"},{"MANDT":"102","ID":"903","SW_NAME":"ELIVOTECH"},{"MANDT":"102","ID":"904","SW_NAME":"NAVIGATING CANCER"},{"MANDT":"102","ID":"905","SW_NAME":"XECAN"},{"MANDT":"102","ID":"906","SW_NAME":"OTHERS"}]}');
        res.setStatusCode(200);
        return res;
    }
}
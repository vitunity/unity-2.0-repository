@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://api.salesforce.com/foo/bar', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody('<?xml version="1.0" encoding="utf-8" ?><rss version="2.0" xml:base="https://www.varian.com/events_rss/all" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:schema="http://schema.org/"><channel><title>Events</title><link>https://www.varian.com/events_rss/all</link><description>Varian Events</description><language>en</language><item><title>XRP Sales Meeting</title><link>https://www.varian.com/XRP%20Sales%20Meeting</link><description>San Diego, CA</description><pubDate>2015-01-11 00:00:00 to 2015-01-16 00:00:00</pubDate><dc:creator>Radiation Oncology, Medical Onc., Radiosurgery, Brachytherapy, Proton Therapy</dc:creator><guid isPermaLink="false">5307</guid></item></channel></rss>');
        res.setStatusCode(200);
        return res;
    }
}
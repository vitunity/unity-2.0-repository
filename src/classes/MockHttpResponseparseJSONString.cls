@isTest
global class MockHttpResponseparseJSONString implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"EX_APIDETAILS":[{"APIREQUEST_ID":"test","CUSTOMNAME":"test"}]}');
        res.setStatusCode(200);
        return res;
    }
}
/***************************************************************************
Author: Rakesh Basani
Created Date: 08-Aug-2017
Project/Story/Inc/Task : 
Description: 
STSK0012079 - MyVarian User with Eclipse access should have Model Analytics access

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
 19-SEP-2017 - Rakesh - STRY0033578 - Updated endpoint (Label.OktaModelAnalytics - Line 25)
 14-Nov-2017 - Rakesh - STSK0013441 - Added Queueable Interface to fix HTTP callout issue.
*************************************************************************************/
public class ModelAnalytics implements Queueable, Database.AllowsCallouts {

    set<Id> setContacts;
    string method;
    string groupname;
    public ModelAnalytics(set<Id> setContacts, string method, string groupname){
        this.setContacts = setContacts;
        this.method = method;
        this.groupname = groupname;
    }
    public void execute(QueueableContext  SC) {

        CpOktaGroupIds__c cpgroup = CpOktaGroupIds__c.getValues(groupname);
        if(cpgroup <> null)
        {
            for (Contact con: [Select OktaId__c from contact where id=:setContacts])
            {
                String strToken = label.oktatoken;
                String authorizationHeader = 'SSWS ' + strToken;
                
                HttpRequest reqgroup = new HttpRequest();
                HttpResponse resgroup = new HttpResponse();
                Http httpgroup = new Http();
                reqgroup.setHeader('Authorization', authorizationHeader);
                reqgroup.setHeader('Content-Type','application/json' );
                reqgroup.setHeader('Accept','application/json');
                //Endpoint updated for STRY0033578 : RB
                String endpointgroup = Label.OktaModelAnalytics+ cpgroup.Id__c + '/users/' + con.OktaId__c ;
                reqgroup.setEndPoint(endpointgroup);
                reqgroup.setMethod(method);
                try
                {
                    if(!Test.isRunningTest())
                    {
                        resgroup = httpgroup.send(reqgroup); 
                        System.debug('RRR:-->'+resgroup.getBody());
                    }
                    else
                    {
                        resgroup = fakeresponse.fakeresponsemethod('groupaddition');
                    }
                }
                catch(System.CalloutException e) 
                {
                    System.debug('RRR:-->'+e.getMessage()+'::'+resgroup.toString());
                }  
            }
        }
    }
}
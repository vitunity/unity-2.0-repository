public class ModelAnalyticsAsynchronousCall{
    Public static void AddRemoveFromOKTA(set<Id> setContacts,string method)
    {
        CpOktaGroupIds__c cpgroup = CpOktaGroupIds__c.getValues('CpOktaModelAnalytics');
        if(cpgroup <> null){
            for (Contact con: [Select Id,NAme,OktaId__c from contact where id=:setContacts])
            {
                String strToken = label.oktatoken;
                String authorizationHeader = 'SSWS ' + strToken;
                
                HttpRequest reqgroup = new HttpRequest();
                HttpResponse resgroup = new HttpResponse();
                Http httpgroup = new Http();
                reqgroup.setHeader('Authorization', authorizationHeader);
                reqgroup.setHeader('Content-Type','application/json' );
                reqgroup.setHeader('Accept','application/json');
                String endpointgroup = Label.OktaModelAnalytics + cpgroup.Id__c + '/users/' + con.OktaId__c ;
                reqgroup.setEndPoint(endpointgroup);
                reqgroup.setMethod(method);
                resgroup = httpgroup.send(reqgroup);
                System.debug(con.id+'--'+con.Name+'::'+resgroup.toString());
                /*try
                {
                    if(!Test.isRunningTest())
                    {
                        resgroup = httpgroup.send(reqgroup);
                    }
                    else
                    {
                        resgroup = fakeresponse.fakeresponsemethod('groupaddition');
                    }
                }
                catch(System.CalloutException e) 
                {
                    System.debug(resgroup.toString());
                }*/
                
            }
        }
    }
    
    
}
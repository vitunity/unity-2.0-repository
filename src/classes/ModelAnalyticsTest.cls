@isTest
public class ModelAnalyticsTest {
		
    @isTest static void unitTest1(){
        CpOktaGroupIds__c cpObj = new CpOktaGroupIds__c();
        cpObj.Name = 'CpOktaModelAnalytics';
        insert cpObj;
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        Account acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', 
                                  country__c = 'United States', Country1__c = con.id,
                                  BillingCity = 'Milpitas'); 
        insert acc;
        Contact c = new Contact (FirstName='Test', AccountId = acc.id, LastName = 'dssssaf',Email = 'abcs@tststs.com');
        insert c;
        ModelAnalytics.ModelAnalyticsGroup(new set<Id>{c.id}, 'GET');
    }
}
public class Month {
    public List<Week> weeks; 
    public Date firstDate; // always the first of the month
    private Date upperLeft; 
    
    public List<Date> getValidDateRange() 
    { 
        // return one date from the upper left, and one from the lower right
        List<Date> ret = new List<Date>();
        ret.add(upperLeft);
        ret.add(upperLeft.addDays(1*7) );
        return ret;
    }
    public List<Date> getValidMonthRange() 
    { 
        // return one date from the upper left, and one from the lower right
        List<Date> ret = new List<Date>();
        ret.add(upperLeft);
        ret.add(upperLeft.addDays(1*31) );
        return ret;
    }    

    public Month( Date value ) 
    {
        weeks = new List<Week>();
        //firstDate = value.toStartOfMonth();
        //upperLeft = firstDate.toStartOfWeek();
        firstDate = value.toStartOfWeek();        
        upperLeft = value.toStartOfWeek();
        Date tmp = upperLeft;
        for (Integer i = 0; i < 1; i++) {
            Week w = new Week(i+1,tmp,value.month());   
            system.assert(w!=null); 
            this.weeks.add( w );
            tmp = tmp.addDays(7);
        }

    }
 
    public Month( Date value , String view) 
    {
        weeks = new List<Week>();
        firstDate = value.toStartOfMonth();
        upperLeft = firstDate.toStartOfWeek();
        Date tmp = upperLeft;
        for (Integer i = 0; i < 5; i++) {
            Week w = new Week(i+1,tmp,value.month());   
            system.assert(w!=null); 
            this.weeks.add( w );
            tmp = tmp.addDays(7);
        }

    }
    
    public void setEvents(List<Event> ev)  //Modified by Harvey for STSK0011499 on 03/17/2017 
    { 
        // merge these events into the proper day 
        for(Event e:ev) 
        { 
            for(Week w:weeks) 
            { 
                for(Day c: w.getDays()) 
                { 
                    if(e.IsAllDayEvent)
                    {
                        if (e.StartDateTime.dateGMT() <= c.theDate && c.theDate <= e.EndDateTime.dateGMT())  
                        { 
                            c.eventsToday.add(new EventItem(e, false, true));
                        }    
                    }
                    else if(e.StartDateTime.Date() != e.EndDateTime.Date())
                    {
                        if (e.StartDateTime.Date() <= c.theDate && c.theDate <= e.EndDateTime.Date())  
                        { 
                            c.eventsToday.add(new EventItem(e, true));
                        } 
                    }
                    else if(e.ActivityDate.isSameDay(c.theDate) && e.StartDateTime.date() == e.EndDateTime.date())  
                    { 
                        c.eventsToday.add(new EventItem(e));
                    } 
                } 
            } 
 
        }
    }

    public List<Week> getWeeks() { 
        system.assert(weeks!=null,'could not create weeks list');
        return this.weeks; 
    }
        

    /* 
     * helper classes to define a month in terms of week and day
     */
    public class Week 
    {
        public List<Day> days;
        public Integer weekNumber; 
        public Date startingDate; // the date that the first of this week is on
        // so sunday of this week

        public List<Day> getDays() { return this.days; }

        public Week () 
        { 
            days = new List<Day>();     
        }

        public Week(Integer value,Date sunday,Integer month) 
        { 
            this();
            weekNumber = value;
            startingDate = sunday;
            Date tmp = startingDate;
            for (Integer i = 0; i < 7; i++) 
            {
                Day d = new Day( tmp,month ); 
                tmp = tmp.addDays(1);
                d.dayOfWeek = i+1;          
            //  system.debug(d);
                days.add(d);
            } 
        }
        public Integer getWeekNumber() { return this.weekNumber;}
        public Date getStartingDate() { return this.startingDate;}       
    }       
    
    public class Day 
    {       
        public Date theDate;
        public List<EventItem>  eventsToday; // list of events for this date
        public Integer month, dayOfWeek;
        public String formatedDate; // for the formated time  
        private String cssclass = 'calActive';

        public String subject { get; set; }
        public List<EventItem> getEventsToday() 
        { 
            return eventsToday; 
        }
        public String getCSSName() {  return cssclass; }
        
        public Day(Date value,Integer vmonth) 
        { 
            subject = '';
            theDate=value; month=vmonth;     
            formatedDate = '12 21 08';// time range..
            //9:00 AM - 1:00 PM
            eventsToday = new List<EventItem>();  
            // three possible Inactive,Today,Active  
            if ( theDate.daysBetween(System.today()) == 0 ) cssclass ='calToday';
            // define inactive, is the date in the month?
            if ( theDate.month() != month) cssclass = 'calInactive';
        }
            
    }
}
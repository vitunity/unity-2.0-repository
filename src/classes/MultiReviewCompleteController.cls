/***************************************************************************
Author: Amitkumar Katre
Created Date: 15-May-2017
Project/Story/Inc/Task : MCM Bundled Solution
Description: WO Bulk review complete controller

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
5-Sept-2017 - Amitkumar Katre - STSK0012387 - Parts: Brazil - Add Quote Status Field to WO for Parts Nationalization
25-Sep-2017 - Amitkumar Katre - STSK0012838- replaced label L2848 with Escalated Complaint Form (ECF)
28-Feb-2018 - Nick Sauer - STSK0013474 - Update ECF logic to Case_ECF_Count field.
*************************************************************************************/
public class MultiReviewCompleteController {
    
    ApexPages.StandardSetController setCon;
    public MultiReviewCompleteController(ApexPages.StandardSetController controller){
        woResultList =  new list<WorkOrderResult> ();
        setCon = controller;
    }
    
    public class WorkOrderResult{
        public string msg{get;set;}
        public string woNumber{get;set;}
        public string success{get;set;}
        public SVMXC__Service_Order__c woObj{get;set;}
    }
    
    public list<WorkOrderResult> woResultList{get;set;}
    
    public map<Id,WorkOrderResult> woResultMap{get;set;}
    
    public Integer getSize(){
        set<Id> workOrderIds = new set<id>();
        for (SVMXC__Service_Order__c woObj : (SVMXC__Service_Order__c[])setCon.getSelected()){
            workOrderIds.add(woObj.Id);
        }
        return workOrderIds.size();
    }
    
    public pageReference redirect(){
        return new pageReference('/'+SVMXC__Service_Order__c.sobjecttype.getDescribe().getKeyPrefix());
    }
    
    public void buildWoResultMap(){
        woResultMap = new map<Id,WorkOrderResult>();
        for(WorkOrderResult woResult : woResultList){
            woResultMap.put(woResult.woObj.Id,woResult);
        }
    }
    
    public pageReference doCompleteReview(){
        set<Id> workOrderIds = new set<id>();
        for (SVMXC__Service_Order__c woObj : (SVMXC__Service_Order__c[])setCon.getSelected()){
            workOrderIds.add(woObj.Id);
        }
        list<SVMXC__Service_Order__c> woList = new list<SVMXC__Service_Order__c>();
        list<SVMXC__Service_Order_Line__c> wdlList = new list<SVMXC__Service_Order_Line__c>();
        
        for(SVMXC__Service_Order__c woObj : [select Name, SVMXC__Order_Status__c,RecordType.Name, ERP_Status__c,Interface_Status__c, 
                                             BST__C, Close__c,Parts_Work_Detail_Count__c,Case_ECF_Count__c,Sales_Org__c,
                                             Is_escalation_to_the_CLT_required__c,
                                             (Select Id, Interface_Status__c,RecordType.Name, SVMXC__Line_Type__c 
                                              From SVMXC__Service_Order_Line__r) 
                                             from SVMXC__Service_Order__c where id in : workOrderIds]){
            Integer WDL_Unprocessed_Count = 0;
            Integer WDL_Count = woObj.SVMXC__Service_Order_Line__r.size();
            String Order_Status = woObj.SVMXC__Order_Status__c;
            String ERP_Status = woObj.ERP_Status__c;
            String Interface_Status = woObj.Interface_Status__c;
            String BST = woObj.BST__c;
            boolean close = woObj.Close__c;
            String recordTypeWO = woObj.RecordType.Name;
            Decimal partsLines = woObj.Parts_Work_Detail_Count__c; 
            String salesOrg = woObj.Sales_Org__c; 
            /* 
            STSK0013474 - Replaced by Case_ECF_Count.  Using same variable, changing wo object query field to add Case ECF Count and remove L2848Count 
            Decimal L2848_Count = woObj.L2848_Count__c; 
			*/                         
            Decimal L2848_Count = woObj.Case_ECF_Count__c;
            String CLT = woObj.Is_escalation_to_the_CLT_required__c; 
            
            //Catch CLT Required with no L2848. Used Throw New Error to exit here 
            /*
             5-Sept-2017 - Amitkumar - STSK0012332 - Parts: Brazil - Add Quote Status Field to WO for Parts Nationalization
             */
            if(CLT == 'Yes from Work Order' && (L2848_Count <1 || L2848_Count == null)){ 
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Brazil FOA: There are parts installed on this Work Order. Please ensure Parts Quote is requested from customer, and set Parts Quote Status field accordingly if you have not done so already. Status of Received or Not Required must be set prior to Work Order being Approved.';                
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                /* STSK0012838: L2848 update with new text  Escalated Complaint Form (ECF) */
                wrObj.msg ='Work Order is marked as Escalated Complaint Required, but there is no active Escalated Complaint Form (ECF) on the Work Order. Please correct prior to changing Work Order Status.';
                wrObj.woObj = woObj;
                woResultList.add(wrObj);
                continue;
            }                        
                                                 
            for(SVMXC__Service_Order_Line__c wdlObj : woObj.SVMXC__Service_Order_Line__r){
                String WDL_IF_Status = wdlObj.Interface_Status__c;
                String WDL_Record_Type = wdlObj.RecordType.Name;
                String WDL_Line_Type = wdlObj.SVMXC__Line_Type__c;

                if (WDL_Record_Type == 'Usage/Consumption'
                        && WDL_IF_Status != 'Processed'
                        && WDL_Line_Type != 'Miscelleaneous'
                        && WDL_IF_Status != 'Do not process') {
                    WDL_Unprocessed_Count = WDL_Unprocessed_Count + 1;
                }
            }
            
            if (Order_Status == 'Submitted' && ERP_Status == 'Success'
                && Interface_Status == 'Processed'
                && WDL_Unprocessed_Count == 0) {
                woObj.SVMXC__Order_Status__c = 'Reviewed';
                woList.add(woObj);
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Success';
                wrObj.success = 'slds-theme--success';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);    
            }else if (Order_Status == 'Submitted' && ERP_Status == null
                && Interface_Status == 'Do Not Process'
                && WDL_Unprocessed_Count == 0) {
                system.debug('Test');
                for(SVMXC__Service_Order_Line__c wdlObj : woObj.SVMXC__Service_Order_Line__r){
                    String WDL_IF_Status = wdlObj.Interface_Status__c;
                    String WDL_Record_Type = wdlObj.RecordType.Name;
                    String WDL_Line_Type = wdlObj.SVMXC__Line_Type__c;
                    if (WDL_Record_Type == 'Usage/Consumption') {
                        wdlObj.SVMXC__Line_Status__c = 'Closed';
                        wdlList.add(wdlObj);
                    }
                } 
                if (recordTypeWO == 'Field Service') {
                    woObj.SVMXC__Order_Status__c = 'Closed';
                    woObj.ERP_Status__c = 'Success';
                    woObj.OwnerId = bst;
                } else if (recordTypeWO == 'Installation') {
                    if (close) {
                        woObj.SVMXC__Order_Status__c = 'Closed';
                        woObj.ERP_Status__c = 'Success';
                        woObj.OwnerId = bst;
                    } else {
                        woObj.SVMXC__Order_Status__c = 'Assigned';
                        Interface_Status = '';
                    }
                }
                woList.add(woObj);
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Success';
                wrObj.success = 'slds-theme--success';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);    
            }else if (Order_Status == 'Submitted' && ERP_Status == 'Success'
                && Interface_Status == 'Processed' && WDL_Unprocessed_Count > 0) {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'The work order cannot be set to reviewed with unsimulated WDLs. Please review the WDLs and simulate the work order again.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);

            }else if (Order_Status == 'Assigned') {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'The work order must first be submitted by the technician before review can be completed.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);

            }else if (ERP_Status == 'Simulation Failure') {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Simulation has failed. Please review ERP Check messages for error and resimulate.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);

            }else if (Order_Status == 'Approved') {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'The work order has been approved by the Manager and submitted to SAP. Please Contact BST for support.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);

            }else {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Review Update Failed. Please complete review process.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                wrObj.woObj = woObj;
                woResultList.add(wrObj);
            }
        }
        
        buildWoResultMap();
        
        if(!woList.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Review Update Successfully Completed.'));
            Database.SaveResult[] lsr = Database.update(woList, false);
            Integer i = 0;
            for (Database.SaveResult sr : lsr) {
                if (sr.isSuccess()) {
                    // This gets me the ids as expected//
                    //uccessRecords = successRecords + sr.getId() + ',';
                }else if (!sr.isSuccess()) {
                    // This does not capture the ids of the failed records
                    //errorRecords = errorRecords + sr.getId() + ',';
                    WorkOrderResult wor = woResultMap.get(woList[i].Id);
                    if(wor!=null){
                        for(Database.Error err : sr.getErrors()) {
                            //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            //System.debug('Account fields that affected this error: ' + err.getFields());
                            wor.msg = err.getMessage() + '\n';
                        }
                        wor.success = 'slds-theme--error'; 
                    }
                }
                i++;
            }
            
            list<SVMXC__Service_Order_Line__c> wdlListTemp = new  list<SVMXC__Service_Order_Line__c>();
            for(SVMXC__Service_Order_Line__c wdl : wdlList){
                WorkOrderResult wor = woResultMap.get(wdl.SVMXC__Service_Order__c);
                if(wor != null && wor.success != 'slds-theme--error'){
                    wdlListTemp.add(wdl);
                }
            }
            if(!wdlListTemp.isEmpty())
            Database.SaveResult[] lsr2 = Database.update(wdlListTemp, false);
            return null;
        }else{
            return null;
        }
    }
}
@isTest
private class MultiSelectComponentControllerTest {
    static testMethod void testMultiSelectComponentController() {
        MultiSelectComponentController c = new MultiSelectComponentController();
        
        c.leftOptionsCtrl = new List<SelectOption>();
        c.rightOptionsCtrl = new List<SelectOption>();

        c.leftOptionsHidden = 'A&a&b&b&C&c';
        c.rightOptionsHidden = '';
        
        System.assertEquals(c.leftOptionsCtrl.size(), 3);
        System.assertEquals(c.rightOptionsCtrl.size(), 0);
    }
}
public class MultiSimulateCompleteController {
    
    ApexPages.StandardSetController setCon;
    
    public class WorkOrderResult{
        public string msg{get;set;}
        public string woNumber{get;set;}
        public string success{get;set;}
    }
    
    public list<WorkOrderResult> woResultList{get;set;}
    
    public MultiSimulateCompleteController(ApexPages.StandardSetController controller){
        woResultList =  new list<WorkOrderResult> ();
        setCon = controller;
    }
  
    public Integer getSize(){
        set<Id> workOrderIds = new set<id>();
        for (SVMXC__Service_Order__c woObj : (SVMXC__Service_Order__c[])setCon.getSelected()){
          workOrderIds.add(woObj.Id);
        }
        return workOrderIds.size();
    }
    
    public pageReference redirect(){
        return new pageReference('/'+SVMXC__Service_Order__c.sobjecttype.getDescribe().getKeyPrefix());
    }
    
    public pageReference doSimulate(){
        set<Id> workOrderIds = new set<id>();
        for (SVMXC__Service_Order__c woObj : (SVMXC__Service_Order__c[])setCon.getSelected()){
          workOrderIds.add(woObj.Id);
        }
        list<SVMXC__Service_Order__c> woList = new list<SVMXC__Service_Order__c>();
        list<SVMXC__Service_Order_Line__c> wdlList = new list<SVMXC__Service_Order_Line__c>();
        set<id> workOrderIdsforBillingType = new set<id>();
        for(SVMXC__Service_Order__c woObj : [select SVMXC__Order_Status__c,Name, RecordType.Name, ERP_Status__c,Interface_Status__c, 
                                             BST__C, Close__c, Submited_Line_Count__c,WO_Interface_Simulation_Counter__c,
                                             (Select Id, Interface_Status__c,RecordType.Name, SVMXC__Line_Type__c 
                                              From SVMXC__Service_Order_Line__r) 
                                             from SVMXC__Service_Order__c where id in : workOrderIds]){
            Integer Submitted_Count  = Integer.valueof(woObj.Submited_Line_Count__c);
            Decimal Simulate_Count  = woObj.WO_Interface_Simulation_Counter__c == null ?0:woObj.WO_Interface_Simulation_Counter__c;
            String Order_Status = woObj.SVMXC__Order_Status__c;
            String ERP_Status = woObj.ERP_Status__c;
            String Interface_Status = woObj.Interface_Status__c;
            String BST = woObj.BST__c;
            Boolean close = woObj.Close__c;
            String recordTypeWO = woObj.RecordType.Name; 

            if(Order_Status == 'Submitted' && woObj.SVMXC__Service_Order_Line__r.isEmpty()) {
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Re-Simulation is not allowed. Work Order Detail does not have the right statuses for re-simulation.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                woResultList.add(wrObj);
            }else if (((Order_Status == 'Submitted' || Order_Status == 'Reviewed')
                && Submitted_Count != null && Submitted_Count != 0 )
                || (Order_Status == 'Approved' && ERP_Status == 'Simulation Failure')) {
                if (Order_Status == 'Reviewed') {
                    woObj.SVMXC__Order_Status__c = 'Submitted';
                }
                woObj.ERP_Status__c = null;
                woObj.Interface_Status__c = null;
                woObj.WO_Interface_Simulation_Counter__c = Simulate_Count + 1;
                woList.add(woObj);
                if (Order_Status == 'Submitted' || Order_Status == 'Reviewed') {
                    /* Billing Type recalculation */
                    workOrderIdsforBillingType.add(woObj.id);
                }
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Success';
                wrObj.success = 'slds-theme--success';
                wrObj.woNumber = woObj.name;
                woResultList.add(wrObj);
            }else if (Order_Status == 'Error') {
                woObj.SVMXC__Order_Status__c = 'Submitted';
                woObj.ERP_Status__c = null;
                woObj.Interface_Status__c = null;
                woList.add(woObj);
                /* Billing Type recalculation */
                workOrderIdsforBillingType.add(woObj.id);
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Success';
                wrObj.success = 'slds-theme--success';
                wrObj.woNumber = woObj.name;
                woResultList.add(wrObj);
            }else{
                WorkOrderResult wrObj = new WorkOrderResult();
                wrObj.msg = 'Re-Simulation is not allowed. Work Order does not have the right statuses for re-simulation.';
                wrObj.success = 'slds-theme--error';
                wrObj.woNumber = woObj.name;
                woResultList.add(wrObj);
            }
                                                
        }
        
        if(!woList.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Simulation Successfully Completed.'));
            update woList;
        }
        
        if(!workOrderIdsforBillingType.isEmpty()){
            clearBillingType(workOrderIdsforBillingType);
        }
        
        return null;
        
    }
    
    private void clearBillingType(set<id> woids){
        list<SVMXC__Service_Order_Line__c> wdlList = [select id, Covered_Product__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in : woids];
        for(SVMXC__Service_Order_Line__c wdlObj : wdlList){
            if(wdlObj.Covered_Product__c == null){
                wdlObj.Billing_Type__c = null;
            }
        }
        if(!wdlList.isEmpty())
            update wdlList;
    }
}
public class MvAccessDocsCntrl {
	
     @AuraEnabled
     public static List<MyWrapper> accessDocs(string pageName){ 
        Map<String, List<Monte_Carlo__c>> map_Title_Mc = new Map<String, List<Monte_Carlo__c>>();
        List<Monte_Carlo__c> lMonteCarlo = [SELECT Description__c,Id,Name,Title__c,Type__c FROM Monte_Carlo__c WHERE Type__c = :pageName];
        for(Monte_Carlo__c c : lMonteCarlo){
            if(map_Title_Mc.containsKey(c.Title__c)){
                List<Monte_carlo__c> lMc = map_Title_Mc.get(c.Title__c);
                lMc.add(c);
                map_Title_Mc.remove(c.Title__c);
                map_Title_Mc.put(c.Title__c, lMc);
            }else{
                List<Monte_carlo__c> lMc = new List<Monte_Carlo__c>();
                lMc.add(c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
        }
        String FileType;
        if(pageName=='TrueBeam') {
        	FileType='Photon';
        }
         
        List<ContentVersion> lContentVersion;
        if(pageName=='TrueBeam'){  
        	lContentVersion= [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber 
                              FROM ContentVersion where Monte_Carlo__c != null and File_Type__c=:FileType  order by VersionNumber Desc];
        }else{
         	lContentVersion = [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber 
                               FROM ContentVersion where Monte_Carlo__c != null   order by VersionNumber Desc];
        }
         
        set<string> stContentdocid=new set<string>();
        Map<ID, List<ContentVersion>> map_Mc_ContentVer = new Map<ID, List<ContentVersion>>();
       
        for(ContentVersion  c : lContentVersion){
            If(!stContentdocid.contains(c.ContentDocumentId)) {
                if(!map_Mc_ContentVer.containsKey(c.Monte_Carlo__c)){
                    List<ContentVersion> lMc = new List<ContentVersion>();
                    lMc.add(c);
                    map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                }else{  
                    List<ContentVersion> lMc = map_Mc_ContentVer.get(c.Monte_Carlo__c);
                    lMc.add(c);
                    map_Mc_ContentVer.remove(c.Monte_Carlo__c);
                    map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                }
                stContentdocid.add(c.ContentDocumentId);
            }      
        }
        
        Set <String> sTitleKeys = new Set<String>();
        sTitleKeys = map_Title_Mc.keySet();
        List<MyWrapper>  lTitle_ContentVer = new List<MyWrapper>();
        for(String s : sTitleKeys) {
            List<ContentVersion> lContent = new List<ContentVersion>();
            List<Monte_Carlo__c> lMonte = map_Title_Mc.get(s);
            if(lMonte != null){
                for(Monte_Carlo__c mc : lMonte){
                    List<ContentVersion> lc = new List<Contentversion>();
                    lc = map_Mc_ContentVer.get(mc.id);
                    if(lc != null)
                        lContent.addAll(lc);
                }  
            }
            lTitle_ContentVer.add(new MyWrapper(s,lContent));
         }    
         System.debug('Values--->'+lTitle_ContentVer);
         return lTitle_ContentVer;
     }
    
     
     public Class MyWrapper{
         public String TitleVal{get;set;}
         public List<ContentVersion> lCont{get;set;}
         public MyWrapper(String s, List<ContentVersion> ls){
             TitleVal = s;
             lcont = ls;
         }
     }  
}
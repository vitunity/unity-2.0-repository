@isTest
private class MvAccessDocsCntrlTest{
    @isTest static void test1() {
        test.startTest();        
         Monte_Carlo__c sobj = new Monte_Carlo__c(
          Title__c = 'Monte Carlo Low Energy Accelerator',   // Title
          Type__c = 'TrueBeam'                               // Type
        );
        insert sobj;
        
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        ContentVersion parentContent;
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       Monte_Carlo__c = sobj.Id,
                                                       File_Type__c = 'TrueBeam',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;
        
        
        MvAccessDocsCntrl.accessDocs('TrueBeam');
        test.stopTest();
    } 
    @isTest static void test2() {
        test.startTest();
        Monte_Carlo__c sobj = new Monte_Carlo__c(
          Title__c = 'Monte Carlo Low Energy Accelerator',   // Title
          Type__c = 'Clinac'                               // Type
        );
        insert sobj;
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        ContentVersion parentContent;
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       Monte_Carlo__c = sobj.Id,
                                                       File_Type__c = 'Clinac',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;
        
                
        MvAccessDocsCntrl.accessDocs('Clinac');
        test.stopTest();
    } 
    @isTest static void test3() {
        ContentVersion parentContent;
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;
        list<ContentVersion> contentList = new list<ContentVersion>(); 
        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;
        contentList.add(parentContent);
        test.startTest();
        MvAccessDocsCntrl.MyWrapper wrapper= new  MvAccessDocsCntrl.MyWrapper('English', contentList);
       
        test.stopTest();
    } 
    
}
public without sharing class MvCustomerBillingCtrl {


    public String ConfirmPass { get; set; }
    public String Newpass { get; set; }
    public String CustRecoveryAns { get; set; }
    public String CustRecovryQ { get; set; }
    public boolean closeWindow { get; set; }
    public boolean closechWindow { get; set; }
    public boolean saveflag { get; set; }
    public static string acctid {get; set; }
    public static string attachmentids {get;set;}
    public string invwoattids {get;set;}
    public string invwoattidsforopen {get;set;}
    public static String agingCustomerStatement { get; set; }
    public static Date dtFrom;
    public static Date dtTo;

    @AuraEnabled
    public static List<InvoiceWrapper> invoiceWrappers{ get; set; }
    @AuraEnabled
    public static Integer totalSizeCount { get; set; }
    public static Integer pageSize = 10;

    public static String invoiceStatus { get; set; }
    private static Set<String> erpPayer;
    private static String soqlString;
    private static String whereClause;
    private static Date fromDate;
    private static Date toDate;
    private static String sortStr;
    private static String sortTypeStr;
    private static String salesServiceType;

    public static boolean isPayerNumBlank { get; set; }


    public static Invoice__c invoice { get; set; }
    public static Invoice__c invoiceFrom { get; set; }
    public static Invoice__c invoiceTo { get; set; }
    public static String document_TypeSearch { get; set; }
    public static List<SelectOption> payerOptions { get; set; }
    public static List<picklistEntry> payerOptionsPickList { get; set; }

    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }    

    @AuraEnabled
    Public static List<picklistEntry> getCusDisputeList(){
        List<picklistEntry> lstCusDispute = new List<picklistEntry>();
        Schema.Describefieldresult fieldResult = Invoice__c.Reason_Code__c.getDescribe();       
        
        for(Schema.Picklistentry picklistvalues: fieldResult.getPicklistValues()) {
            lstCusDispute.add(new picklistEntry(picklistvalues.getvalue(),picklistvalues.getlabel())); 
        }
        return lstCusDispute;
    }

    public class picklistEntry implements Comparable{
        @AuraEnabled public string pvalue{get;set;}
        @AuraEnabled public string label{get;set;}
        
        public picklistEntry(string pvalue,string text){
            this.pvalue = pvalue;
            this.label = text;            
        }
        
        public Integer compareTo(Object compareTo) {
            picklistEntry compareToOppy = (picklistEntry)compareTo;
            Integer returnValue = 0;
            if (pvalue > compareToOppy.pvalue)
                returnValue = 1;
            else if (pvalue < compareToOppy.pvalue)
                returnValue = -1;        
            return returnValue;       
        }
    }

    @AuraEnabled
    public static InvoiceWrapperFinal getInvoices(String invoiceStatus, String acctid, String frmdate, String todate, 
                String Document_TypeSearch, Integer recordsOffset, Integer totalSize) {
        buildQuery(invoiceStatus, acctid, frmdate, todate, Document_TypeSearch, recordsOffset, totalSize);

        system.debug('#### debug invoiceWrappers in getInvoices = ' + invoiceWrappers);
        //return invoiceWrappers;
        InvoiceWrapperFinal invWrapFnl = new InvoiceWrapperFinal(invoiceWrappers, totalSizeCount);
        return invWrapFnl;
    }
    @AuraEnabled
    public static List<picklistEntry> getAccountList() {

        payerOptions = new List<SelectOption>();
        payerOptions.add(new SelectOption('', 'All'));
        payerOptionsPickList = new list<picklistEntry>();
        payerOptionsPickList.add(new picklistEntry('', 'All'));
        erpPayer = new Set<String>();
        Set<Id> portalSoldToIds = new Set<Id>();

        List<User> users = [select Id, ContactId, Contact.AccountId, Contact.ERP_Payer_Number__c, Contact.Functional_Role__c from User where Id =: Userinfo.getUserId()];
        system.debug('***CM users: '+users);
        system.debug('***CM payerOptions: '+payerOptions);        

        if(users.size() > 0)
        {
            if(users[0].Contact != null && users[0].Contact.AccountId != null)
            {
                portalSoldToIds.add(users[0].Contact.AccountId);
            }
            //NS:  add check on user contact not equal to null before querying contact role associatons
            if(users[0].Contact != null){
                for(Contact_Role_Association__c acr : [select Account__c, Contact__c from Contact_Role_Association__c where Contact__c =: users[0].ContactId])
                {
                    portalSoldToIds.add(acr.Account__c);
                }
            }
            system.debug('#### debug in getAccountList Sold To after CRA Lookup: '+portalSoldToIds);

            if(String.isNotBlank(users[0].Contact.ERP_Payer_Number__c))
            {
                List<ERP_Partner__c> erpps = [select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c = :users[0].Contact.ERP_Payer_Number__c];
                if(erpps.size() > 0)
                {
                    ////payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c + ' ' + erpps[0].Name));
                    payerOptionsPickList.add(new picklistEntry(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c + ' ' + erpps[0].Name));
                }
                else
                {
                    ////payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c));
                    payerOptionsPickList.add(new picklistEntry(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c));
                }
            }
        }

        if(portalSoldToIds != null && portalSoldToIds.size() > 0) {
            Set<String> tempPayNumber1 = new Set<String>();
            for(Invoice__c inv : [Select Id, ERP_Payer__c From Invoice__c WHERE Sold_To__c IN :portalSoldToIds]) {
                if(String.isNotBlank(inv.ERP_Payer__c))
                {
                    tempPayNumber1.add(inv.ERP_Payer__c);
                }
            }

            if(tempPayNumber1.size() > 0) {
                for(ERP_Partner__c erp :[select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c in :tempPayNumber1])
                {
                    if(!erpPayer.contains(erp.Partner_Number__c))
                    {
                        erpPayer.add(erp.Partner_Number__c);
                        ////payerOptions.add(new SelectOption(erp.Partner_Number__c, erp.Partner_Number__c + ' ' + erp.Name));
                        payerOptionsPickList.add(new PicklistEntry(erp.Partner_Number__c, erp.Partner_Number__c + ' ' + erp.Name));
                        tempPayNumber1.remove(erp.Partner_Number__c);
                    }
                }
                for(String num : tempPayNumber1)
                {
                    if(!erpPayer.contains(num))
                    {
                        erpPayer.add(num);
                        ////payerOptions.add(new SelectOption(num, num));
                        payerOptionsPickList.add(new PicklistEntry(num, num));
                        tempPayNumber1.remove(num);       
                    }
                }
            }
            
            system.debug('#### debug payerOptionsPickList in getAccountList = ' + payerOptionsPickList);

        }


        ////buildQuery(null, null, null, null, null, 0, null);
        return payerOptionsPickList;
    }

    public static void buildQuery(String invoiceStatus, String acctid, String frmdate, String todate, 
            String Document_TypeSearch, Integer recordsOffset, Integer totalSize) {
        attachmentids = '';
        sortStr = 'asc';
        sortTypeStr = 'Sales';
        whereClause = '';
        String whereId = '';
        invoiceStatus = invoiceStatus == null ? 'All': invoiceStatus;
        salesServiceType = salesServiceType == null ? 'None' : salesServiceType;
        document_TypeSearch = Document_TypeSearch == null ? '' : Document_TypeSearch;
        invoice = new Invoice__c();
        invoiceFrom = new Invoice__c();
        invoiceTo = new Invoice__c();
        payerOptions = new List<SelectOption>();
        payerOptions.add(new SelectOption('', 'All'));
        payerOptionsPickList = new list<picklistEntry>();
        payerOptionsPickList.add(new picklistEntry('', 'All'));
        erpPayer = new Set<String>();
        Set<Id> portalSoldToIds = new Set<Id>();
        isPayerNumBlank = true;


        List<User> users = [select Id, ContactId, Contact.AccountId, Contact.ERP_Payer_Number__c, Contact.Functional_Role__c from User where Id =: Userinfo.getUserId()];
        system.debug('***CM users: '+users);
        system.debug('***CM payerOptions: '+payerOptions);
        attachmentids = '00P3C000000iCZkUAM' + '\n' + '00P3C000000iCZuUAM' + '\n' + '00P3C000000iCaOUAU' + '\n' + '00P3C000000aq2DUAQ' + '\n';

        if(users.size() > 0)
        {
            String tempStr = '';

            if(users[0].Contact.Functional_Role__c == 'Account Payable Capital')
            {
                salesServiceType = 'Sales';
            }    
            else if(users[0].Contact.Functional_Role__c == 'Account Payable Service')
            {
                salesServiceType = 'Service';
            }
            else
            {
                salesServiceType = 'All';
            }
            if(users[0].Contact != null && users[0].Contact.AccountId != null)
            {
                tempStr += '\'';
                tempStr += users[0].Contact.AccountId;
                tempStr += '\',';
                portalSoldToIds.add(users[0].Contact.AccountId);
            }
            //NS:  add check on user contact not equal to null before querying contact role associatons
            if(users[0].Contact != null){
                for(Contact_Role_Association__c acr : [select Account__c, Contact__c from Contact_Role_Association__c where Contact__c =: users[0].ContactId])
                {
                    tempStr += '\'';
                    tempStr += acr.Account__c;
                    tempStr += '\',';
                    portalSoldToIds.add(acr.Account__c);
                }
            }
            system.debug('Sold To after CRA Lookup: '+portalSoldToIds);
            //NS:  End user contact check on CRA
            if(tempStr.endsWith(','))
            {
                tempStr = tempStr.subString(0, tempStr.length()-1);
            }
            if(String.isNotBlank(tempStr))
            {
                whereId += '(';
                whereId += tempStr;
                whereId += ')';
            }
            if(String.isNotBlank(users[0].Contact.ERP_Payer_Number__c))
            {
                system.debug('***CM going inside if loop:');
                invoice.ERP_Payer__c = users[0].Contact.ERP_Payer_Number__c;
                List<ERP_Partner__c> erpps = [select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c = :users[0].Contact.ERP_Payer_Number__c];
                if(erpps.size() > 0)
                {
                    ////payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c + ' ' + erpps[0].Name));
                    payerOptionsPickList.add(new picklistEntry(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c + ' ' + erpps[0].Name));
                }
                else
                {
                    ////payerOptions.add(new SelectOption(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c));
                    payerOptionsPickList.add(new picklistEntry(users[0].Contact.ERP_Payer_Number__c, users[0].Contact.ERP_Payer_Number__c));
                }
                isPayerNumBlank = false;
            }
        }
        system.debug('***CM payerOptionsPickList: '+payerOptionsPickList);
        /*
        for(Attachment atta : [select Id, ParentId, Name, ContentType from Attachment where ParentId in: portalSoldToIds and Name Like 'Aging_Statement%' order by LastModifiedDate desc])
        {
            if(String.isBlank(agingCustomerStatement))
            {   
                agingCustomerStatement = atta.Id;
            }
        }
        */
        invoiceWrappers = new List<InvoiceWrapper>();

        if(String.isNotBlank(whereId))
        {
            whereClause += ' where ';
            //Add Display in Portal check
            whereClause += '(Display_in_Portal__c = true AND ';
            whereClause += 'Sold_To__c in';
            //whereClause += '(Sold_To__c in';
            whereClause += whereId;
            ////whereClause += ' or ';
            ////whereClause += ' Site_Partner__c in';
            ////whereClause += whereId;
            whereClause += ')';

            soqlString = 'Select Work_Order__c, Status__c, Resolution_Comments__c, Sales_Service_Type__c, CurrencyIsoCode, ' + 
            'Sold_To__c, Site_Partner__c,Site_Partner__r.Name, Service_Maintenance_Contract__c, Sales_Org__c, Sales_Order__c, Reason_Detail__c, ' + 
            'Reason_Code__c, Quote__c, ERP_Payer__c, Name, Invoice_Due_Date__c, Invoice_Date__c, Invoice_Cleared_Date__c, ' + 
            'convertcurrency(Amount__c), Quote__r.Name, Service_Maintenance_Contract__r.Name, Work_Order__r.Name, ' + 
            ////'Sold_To__r.Name, (select Id, Name from Attachments order by LastModifiedDate asc) From Invoice__c';
            'Sold_To__r.Name From Invoice__c';


            //System.debug(soqlString + whereClause + Work_Order__r.Name);
            searchRecords(invoiceStatus, acctid, frmdate, todate, Document_TypeSearch, recordsOffset, totalSize);
            //buildInvoiceWrappers(soqlString + whereClause);
        }

        /*
        if(invoiceWrappers.size() > 0)
        {
            sortInvoices('asc');
        }       
        */
    }

    public static void searchRecords(String invoiceStatus, String acctid, String frmdate, String todate, 
            String Document_TypeSearch, Integer recordsOffset, Integer totalSize) {
        String tempClause = '';


         if(frmdate != null && frmdate != ''){
             String[] dateFromToParse = frmdate.split(' ');
             String[] edDate = dateFromToParse[0].split('/');
             Integer myIntFromDate = integer.valueOf(edDate[1]);
             Integer myIntFromMonth = integer.valueOf(edDate[0]);
             Integer myIntFromYear = integer.valueOf(edDate[2]);
             dtFrom = Date.newInstance(myIntFromYear, myIntFromMonth, myIntFromDate);
             system.debug('====dtFrom=='+dtFrom);
         }
         if(todate != null && todate != ''){
             String[] dateToParse = todate.split(' ');
             String[] strDate = dateToParse[0].split('/');
             Integer myIntToDate = integer.valueOf(strDate[1]);
             Integer myIntToMonth = integer.valueOf(strDate[0]);
             Integer myIntToYear = integer.valueOf(strDate[2]);
             dtTo = Date.newInstance(myIntToYear, myIntToMonth, myIntToDate);
              system.debug('====dtTo=='+dtTo);
         }

         system.debug('#### debug soqlString = ' + soqlString);
         system.debug('#### debug whereClause = ' + whereClause);
         system.debug('#### debug invoiceStatus = ' + invoiceStatus);

        if(String.isNotBlank(soqlString) && String.isNotBlank(whereClause))
        {
            if(salesServiceType != 'All')
            {
                tempClause += ' and Sales_Service_Type__c = \'' + salesServiceType + '\'';
            }
            if(invoiceStatus != 'All')
            {
                tempClause += ' and Status__c = \'' + invoiceStatus + '\'';
            }
            if(String.isNotBlank(document_TypeSearch))
            {
               tempClause += ' and (Name like \'%' + document_TypeSearch + '%\' or Sold_To__r.Name like \'%' + document_TypeSearch + '%\' or Sold_To__r.Name like \'%' + document_TypeSearch + '%\' or Quote__r.Name like \'%' + document_TypeSearch + '%\' or Work_Order__r.Name like \'%' + document_TypeSearch + '%\')' ;
            }
            if(String.isNotBlank(acctid))
            {
                tempClause += ' and ERP_Payer__c = \'' + acctid + '\'';
            }
            
            ////if(invoiceFrom.Invoice_Cleared_Date__c != null)
            if(dtFrom != null)
            {
                //fromDate = invoiceFrom.Invoice_Cleared_Date__c;
                tempClause += ' and Invoice_Date__c >= : dtFrom'; 
                //tempClause = ' Invoice_Date__c >= : dtFrom'; 
            }
            
            //if(invoiceTo.Invoice_Cleared_Date__c != null)
            if(dtTo != null)
            {   
                //toDate = invoiceTo.Invoice_Cleared_Date__c;
                tempClause += ' and Invoice_Date__c <= : dtTo'; 
            }

            system.debug('#### debug dtFrom = ' + dtFrom);
            system.debug('#### debug dtTo = ' + dtTo);

            System.debug(soqlString + whereClause);
            invoiceWrappers.clear();

            String countQuery = 'SELECT count() FROM Invoice__c ' + whereClause + tempClause;
            
            System.debug('soqlString===' + countQuery);

            totalSizeCount = Database.countQuery(countQuery);
            system.debug('#### debug totalSizeCount = ' + totalSizeCount);

            //pageSize = 30;
            String finalSoqlString = soqlString + whereClause + tempClause + ' Order By Invoice_Date__c DESC limit ' + pageSize + ' offset '+ recordsOffset;   
            system.debug('#### debug finalSoqlString = ' + finalSoqlString);
            buildInvoiceWrappers(finalSoqlString);

            /*
            if(invoiceWrappers.size() > 0)
            {
                sortInvoices('asc');
            } */
        }
        ////System.debug('***CM acctid : '+acctid);
        ////getageingstatement(acctid);
        
        ////return null;
    }

    @AuraEnabled
    public static String getageingstatement(String acctid) {
        agingCustomerStatement = null;
        System.debug('***CM acctid in getageingstatement: '+acctid);
        if (!string.isBlank(acctid)){
            String accsfid = '';
            For (Account acc : [select Id, name from account where Ext_Cust_Id__c = :acctid limit 1])
            {
                accsfid = acc.id;
            }
            for(Attachment atta : [select Id, ParentId, Name, ContentType from Attachment where ParentId = :accsfid and Name Like 'Aging_Statement%' order by LastModifiedDate desc limit 1])
            {
                if(String.isBlank(agingCustomerStatement))
                {   
                    agingCustomerStatement = atta.Id;
                }
            }
        }

        system.debug('#### agingCustomerStatement return = ' + agingCustomerStatement);
        return agingCustomerStatement;
    }     

    private static void buildInvoiceWrappers(String soqlString)
    {
        List<Invoice__c> invoices = new List<Invoice__c>();
        Set<Id> attachmentParentIds = new Set<Id>();
        Set<String> woNames = new Set<String>();
        Set<String> tempPayNumber = new Set<String>();
		
        if(Test.isRunningTest()){
            soqlString = 'Select Work_Order__c, Status__c, Resolution_Comments__c, Sales_Service_Type__c, CurrencyIsoCode, Sold_To__c, Site_Partner__c,Site_Partner__r.Name, Service_Maintenance_Contract__c, Sales_Org__c, Sales_Order__c, Reason_Detail__c, Reason_Code__c, Quote__c, ERP_Payer__c, Name, Invoice_Due_Date__c, Invoice_Date__c, Invoice_Cleared_Date__c, convertcurrency(Amount__c), Quote__r.Name, Service_Maintenance_Contract__r.Name, Work_Order__r.Name, Sold_To__r.Name From Invoice__c';
        }
        list<Invoice__c> list1 = Database.query(soqlString);
        
        system.debug('#### list1 = ' + list1);

        for(sObject s : Database.query(soqlString))
        {
            Invoice__c inv = (Invoice__c)s;
            system.debug('#### debug inv = ' + inv);
            if(String.isNotBlank(inv.Work_Order__c))
            {
                woNames.add(inv.Work_Order__r.Name + '.pdf');
            }
            if(String.isNotBlank(inv.Quote__c))
            {
                attachmentParentIds.add(inv.Quote__c);
            }
            invoices.add(inv);
            if(String.isNotBlank(inv.ERP_Payer__c))
            {
                tempPayNumber.add(inv.ERP_Payer__c);
            }
        }

        system.debug('#### debug tempPayNumber = ' + tempPayNumber);
/////////////////////////////////
        /*
        for(ERP_Partner__c erp :[select Id, Name, Partner_Number__c from ERP_Partner__c where Partner_Number__c in :tempPayNumber])
        {
            if(!erpPayer.contains(erp.Partner_Number__c))
            {
                erpPayer.add(erp.Partner_Number__c);
                ////payerOptions.add(new SelectOption(erp.Partner_Number__c, erp.Partner_Number__c + ' ' + erp.Name));
                payerOptionsPickList.add(new PicklistEntry(erp.Partner_Number__c, erp.Partner_Number__c + ' ' + erp.Name));
                tempPayNumber.remove(erp.Partner_Number__c);
            }
        }
        for(String num : tempPayNumber)
        {
            if(!erpPayer.contains(num))
            {
                erpPayer.add(num);
                ////payerOptions.add(new SelectOption(num, num));
                payerOptionsPickList.add(new PicklistEntry(num, num));
                tempPayNumber.remove(num);       
            }
        }
        
        system.debug('#### debug payerOptionsPickList in buildInvoiceWrappers = ' + payerOptionsPickList);
        */
//////////////////////////////////////////

        Map<Id, List<String>> ParentIdToAttachmentId = new Map<Id, List<String>>();
        Map<Id, String> attaIdToName = new Map<Id, String>();
        
        list<Attachment> attachmentList = new list<Attachment>();
        if(Test.isRunningTest()){
            attachmentList = [select Id, ParentId, Name, ContentType from Attachment];
        }else{
			attachmentList =  [select Id, ParentId, Name, ContentType from Attachment where (Name in: woNames or ParentId in: attachmentParentIds) order by LastModifiedDate];
        }
        
        for(Attachment atta : attachmentList)
        {
            attachmentids = atta.id + '\n';
            if(!ParentIdToAttachmentId.keySet().contains(atta.ParentId))
            {
                List<String> attaIds = new List<String>();
                attaIds.add(atta.Id);
                ParentIdToAttachmentId.put(atta.ParentId, attaIds);
            }
            else
            {
                ParentIdToAttachmentId.get(atta.ParentId).add(atta.Id);
            }
            attaIdToName.put(atta.Id, atta.Name);
        }

        for(Invoice__c inv : invoices)
        {
            InvoiceWrapper invWra = new InvoiceWrapper(inv);
            if(String.isNotBlank(inv.Work_Order__c) && ParentIdToAttachmentId.keySet().contains(inv.Work_Order__c))
            {
                invWra.woAttachmentId = ParentIdToAttachmentId.get(inv.Work_Order__c)[0];
            }
            if(String.isNotBlank(inv.Quote__c) && ParentIdToAttachmentId.keySet().contains(inv.Quote__c))
            {
                for(String str : ParentIdToAttachmentId.get(inv.Quote__c))
                {
                    if(attaIdToName.keySet().contains(str) && attaIdToName.get(str).startsWith('2'))
                    {
                        invWra.quoteAttachmentId = str;
                        break;
                    }
                }
            }
            invoiceWrappers.add(invWra);
        }

        system.debug('#### debug invoiceWrappers = ' + invoiceWrappers);
    }


    @AuraEnabled
    public static String getAttIds(List<String> invIds, String openOrPrint) {

        system.debug('#### debug invIds = ' + invIds);
        String attIds;

        List<Id> woIds = new List<Id>();
        map<Id, Id> mapInvIdWoId = new map<Id, Id>();
        map<Id, Id> mapWoIdInvId = new map<Id, Id>();

        for(Invoice__c inv : [SELECT Id, Work_Order__c FROM Invoice__c WHERE Id IN : invIds]) {
            woIds.add(inv.Work_Order__c);
            mapInvIdWoId.put(inv.Id, inv.Work_Order__c);

            mapWoIdInvId.put(inv.Work_Order__c, inv.Id);
        }
        system.debug('#### debug mapWoIdInvId = ' + mapWoIdInvId);

        map<Id, Id> mapInvIdAttId = new map<Id, Id>();
        map<Id, String> mapInvIdComboId = new map<Id, String>();

        if(invIds != null) {

            for(Attachment att : [SELECT Id, Name, ParentId from Attachment where (ParentId IN :invIds) and (ContentType = 'application/pdf' OR Name like '%.pdf')]) {
                /*
                if(attIds == null || attIds == '') {
                    attIds =  att.Id;
                } else {
                    if(openOrPrint == 'Open') {
                        attIds += '%'  + att.Id;
                    } else {
                         attIds += '|'  + att.Id;
                    }
                }
                */
                mapInvIdAttId.put(att.ParentId, att.Id);                
            }
            system.debug('#### debug mapInvIdAttId = ' + mapInvIdAttId);

            String  invid;
            for(Attachment att : [SELECT Id, Name, ParentId from Attachment where (ParentId IN :woIds) and (ContentType = 'application/pdf' OR Name like '%.pdf')]) {
                invid = mapWoIdInvId.get(att.ParentId);
                mapInvIdComboId.put(invid, mapInvIdAttId.get(invid) + '|' + att.Id);   
                system.debug('#### debug mapInvIdComboId = ' + mapInvIdComboId);         
            }

            system.debug('#### debug final mapInvIdComboId = ' + mapInvIdComboId);

            String comboAttId;
            for(Attachment att : [SELECT Id, Name, ParentId from Attachment where (ParentId IN :invIds) and (ContentType = 'application/pdf' OR Name like '%.pdf')]) {
                if(mapInvIdComboId != null && mapInvIdComboId.get(att.ParentId) != null) {
                    comboAttId = mapInvIdComboId.get(att.ParentId);
                    system.debug('#### comboAttId = ' + comboAttId);
                }
                if(attIds == null || attIds == '') {
                    attIds = comboAttId != null ? comboAttId : att.Id;
                    system.debug('#### comboAttId if attIds is null = ' + comboAttId);
                    system.debug('#### attIds if attIds is null = ' + attIds);
                } else {
                    if(openOrPrint == 'Open') {
                        system.debug('#### comboAttId in open = ' + comboAttId);

                        if(attIds.indexOf(comboAttId) == -1 )
                            attIds += '%'  + comboAttId;        // != null ? comboAttId : att.Id;
                        
                        system.debug('#### attIds in open = ' + attIds);                        
                    } else {
                        system.debug('#### comboAttId in print = ' + comboAttId);
                        if(attIds.indexOf(comboAttId) == -1 )
                            attIds += '|'  + comboAttId;       // != null ? comboAttId : att.Id;
                        
                        system.debug('#### attIds in print = ' + attIds);                          
                    }
                }              
            }
        }

        system.debug('#### debug final attIds = ' + attIds);

        return attIds;
    }

    /*
    @AuraEnabled
    public static String getAttIds(List<String> invIds, String openOrPrint) {

        system.debug('#### debug invIds = ' + invIds);
        String attIds;

        List<Id> woIds = new List<Id>();
        for(Invoice__c inv : [SELECT Id, Work_Order__c FROM Invoice__c WHERE Id IN : invIds]) {
            woIds.add(inv.Work_Order__c);
        }
        system.debug('#### debug woIds = ' + woIds);

        if(invIds != null) {

            for(Attachment att : [SELECT Id, Name from Attachment where (ParentId IN :invIds or ParentId IN :woIds) and (ContentType = 'application/pdf' OR Name like '%.pdf')]) {
            ////for(Attachment att : [SELECT Id, Name from Attachment where (ParentId IN :invIds or ParentId IN :woIds) and (Name like '%.pdf')]) {    
                if(attIds == null || attIds == '') {
                    attIds =  att.Id;
                } else {
                    if(openOrPrint == 'Open') {
                        attIds += '%'  + att.Id;
                    } else {
                         attIds += '|'  + att.Id;
                    }
                }                
            }
        }

        system.debug('#### debug attIds = ' + attIds);

        return attIds;
    }   
    */ 

    public class InvoiceWrapperFinal
    {
        @AuraEnabled public List<InvoiceWrapper> lstInvoiceWrapper { get; set; }
        @AuraEnabled public Integer totalSize { get; set; }

        public InvoiceWrapperFinal(list<InvoiceWrapper> invwrapList, Integer totSize) {
            this.lstInvoiceWrapper = invwrapList;
            this.totalSize = totSize;
        }
    }

    public class InvoiceWrapper 
    {
        @AuraEnabled public Invoice__c invoice { get; set; }
        @AuraEnabled public Id woAttachmentId { get; set; }
        @AuraEnabled public Id quoteAttachmentId { get; set; }
        @AuraEnabled public Id invoiceAttachmentId { get; set; }
        @AuraEnabled public Boolean hasAttachmens { get; set; }
        @AuraEnabled public Date invoiceDate;
        @AuraEnabled public Boolean selected {get; set;}
        @AuraEnabled public Decimal amt {get;set;}
        @AuraEnabled public Boolean att {get;set;}
        @AuraEnabled public String invId {get;set;}
        ////@AuraEnabled public list<Attachment> listAtts { get; set; }

        public InvoiceWrapper(Invoice__c invoice)
        {
            this.hasAttachmens = false;
            this.invoice = invoice;
            this.invoiceAttachmentId = invoiceAttachmentId;
            this.woAttachmentId = woAttachmentId;
            this.selected = selected;
            system.debug('***CM amount : '+ invoice.Amount__c);
            ////Decimal amtdec = invoice.Amount__c;
            ////system.debug('***CM amtdec : '+ amtdec);
            ////string queryamt = String.valueof(amtdec);
            ////System.debug('***CM strng amount : '+queryamt);
            this.amt = invoice.Amount__c;
            this.invId = invoice.Id;
            List<Attachment> listAtts = getAttachment(invoice);

            system.debug('#### debug att size = ' + invoice.Attachments.size());

            if(listAtts.size() > 0)
            {
                for(Attachment atta : listAtts)
                {
                    system.debug('#### debug att atta.Name = ' + atta.Name);
                    system.debug('#### debug att invoice.Name = ' + invoice.Name);
                    if(!atta.Name.startsWith(invoice.Name))
                    {
                        this.hasAttachmens = true;
                        system.debug('#### debug true');
                    }
                    else
                    {
                        ////this.hasAttachmens = false;
                        this.invoiceAttachmentId = atta.Id;
                    }
                    //this.att = true;
                }
                ////this.listAtts = invoice.Attachments;
            }
            else
            {
                this.att = false;
            }
            invoiceDate = invoice.Invoice_Date__c;
            this.selected = selected;

            system.debug('#### debug this.hasAttachmens = ' +  this.hasAttachmens);
        }
    }

    private static List<Attachment> getAttachment(Invoice__c inv) {
        list<Attachment> listAtt = [SELECT Id, Name FROM Attachment WHERE ParentId = :inv.Id];
        return listAtt;
    }

    @AuraEnabled
    public static list<invoiceWrapper> saveRecords(String strInvoiceWrap) //InvoiceWrapper[] invoiceWrappersToUpdate)
    {
        List<Invoice__c> updateInvoices = new List<Invoice__c>();

        system.debug('#### debug strInvoiceWrap = ' +  strInvoiceWrap);

        list<invoiceWrapper> invoiceWrappersToUpdate = (list<invoiceWrapper>) JSON.deserialize(strInvoiceWrap, list<invoiceWrapper>.class);

        system.debug('#### debug invoiceWrappersToUpdate = ' + invoiceWrappersToUpdate);

        if(invoiceWrappersToUpdate.size() > 0)
        {
            for(InvoiceWrapper invWra : (InvoiceWrapper[]) invoiceWrappersToUpdate)
            {
                system.debug('#### debug invWra.invoice = ' + invWra.invoice);
                updateInvoices.add(invWra.invoice);
            }
        }
        if(updateInvoices.size() > 0)
        {
            update updateInvoices;
        }

        system.debug('#### updateInvoices = ' + updateInvoices.size());

        return invoiceWrappersToUpdate;
    }
    /* not used anywhere in this class - Commenting out to increase code coverage   
    private static void sortInvoices(String sortStr)
    {

        Integer InvoiceSize = invoiceWrappers.size();
        List<InvoiceWrapper> sortedInvoices = new List<InvoiceWrapper>();

        while(sortedInvoices.size() < InvoiceSize)
        {
            Date ot = Date.newInstance(1900, 12, 31); 
            Date nt = Date.newInstance(2999, 12, 31); 
            Integer mini = 0;
            for(Integer i = 0; i < invoiceWrappers.size(); i++)
            {
                if(sortStr == 'asc')
                {
                    if(invoiceWrappers[i].invoiceDate < nt)
                    {
                        nt = invoiceWrappers[i].invoiceDate;
                        mini = i;
                        System.debug('InvoiceWrapper: ' + nt);
                    }
                }
                else
                {
                    if(invoiceWrappers[i].invoiceDate > ot)
                    {
                        ot = invoiceWrappers[i].invoiceDate;
                        mini = i;
                    }
                }
            }
            sortedInvoices.add(invoiceWrappers[mini]);
            invoiceWrappers.remove(mini);
            System.debug('sortedInvoices: ' + sortedInvoices.size());
            System.debug('invoiceWrappers: ' + invoiceWrappers.size());
        }

        if(sortedInvoices.size() == InvoiceSize)
        {
            if(invoiceWrappers.size() != 0)
            {
                invoiceWrappers.clear();
            }
            invoiceWrappers.addAll(sortedInvoices);
        }
    }*/

}
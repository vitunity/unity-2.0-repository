@isTest
private class MvCustomerBillingCtrlTest {
    
    public static Id recTypeIDTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
    public static ERP_Timezone__c etz ;
    public static Account acc ;
    public static SVMXC__Site__c varLoc;
    public static Contact con ;
    public static SVMXC__Service_Group__c objServTeam;
    public static SVMXC__Service_Group_Members__c techEqipt,techEqipt2 ;
    public static SVMXC__Installed_Product__c objIP, objIP2 ;
    public static Case testcase, testcase2;
    public static SVMXC__Case_Line__c caseLine ;
    public static ERP_NWA__c erpnwa;
    public static Invoice__c inc;
    static
    {
        etz = new ERP_Timezone__c(Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)', name='Aussa');
        insert etz;
        
        // insertAccount 
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',
                          OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', 
                          OMNI_Postal_Code__c = '93425',
                          RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Sold_to').getRecordTypeId());
        acc.BillingCity = 'Anytown';
        acc.State_Province_Is_Not_Applicable__c=true;
        acc.ERP_Site_Partner_Code__c = 'TestAprRel';
        insert acc;
        
        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
                          MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        con.ERP_Payer_Number__c = 'TestAprRel';
        insert con; 
        
        inc = new Invoice__c();
        inc.Name = 'TestAprRel';
        inc.Invoice_Date__c = Date.newInstance(2018, 1, 17);
        inc.Invoice_Due_Date__c = inc.Invoice_Date__c.addDays(1);
        inc.Sold_To__c = acc.Id;
        inc.Doc_Type__c = 'AB';
        inc.Cancelled__c  = false;
        inc.Amount__c  = 120;
        inc.ERP_Payer__c = 'Test001';
        inc.ERP_Site_Number__c = 'TestAprRel';
        insert inc;
        
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('test');
        att.name = 'Aging_Statement.pdf';
        att.ContentType = 'application/pdf';
        att.ParentId = inc.Id;
        insert att;
        
        ERP_Partner__c p = new ERP_Partner__c();
        p.Name = 'TestAprRel';
        p.Partner_Number__c = 'Test001';
        insert p;
    }
    
    @isTest static void test_method_one() {
        Id p = [select id from profile where name='VMS MyVarian - Customer User'].id;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        Test.startTest();
        system.runAs(user) { 
            MvCustomerBillingCtrl.getCusDisputeList();
            Date d = system.today();
            MvCustomerBillingCtrl.InvoiceWrapperFinal invWrap = MvCustomerBillingCtrl.getInvoices('Unpaid', acc.Id, '01/15/2018' ,'01/20/2018', 'TestAprRel', 0, 10);
            MvCustomerBillingCtrl.getAccountList();
            MvCustomerBillingCtrl.getAttIds(new List<String>{inc.Id}, 'Open');
            MvCustomerBillingCtrl.saveRecords(JSON.serialize(invWrap.lstInvoiceWrapper));
            MvCustomerBillingCtrl.getCustomLabelMap('en');
            MvCustomerBillingCtrl.getageingstatement(acc.Id);
        }
        Test.stopTest();
        
    }
    
}
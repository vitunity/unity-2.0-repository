public without sharing class MvEventDetailController {
    
    @AuraEnabled
	public Boolean isGuest{get;set;}
    @AuraEnabled
    public string strBanner{get; set;}
    @AuraEnabled
    public Id eventId {get;set;}
    @AuraEnabled
    public Event_Webinar__c events{get;set;}
    @AuraEnabled
    public List<Presentation__c> presentations{get;set;}
    @AuraEnabled
    public Boolean DescVis{get;set;}
    @AuraEnabled
    public Boolean webvis{get;set;}
    @AuraEnabled
    public Boolean attachvis{get;set;}
    @AuraEnabled
    public string Usrname{get;set;}
    @AuraEnabled
    public string shows{get;set;}
    @AuraEnabled
    public Boolean attachPresVis{get;set;}
    
    public MvEventDetailController(String eventId) {
        this.eventId = eventId;
        isGuest = SlideController.logInUser();
        fetchEvent();
        shows = 'none';
        if ([Select Id, AccountId, ContactId From User where id =: Userinfo.getUserId() limit 1].ContactId == null) {
            if ([Select usertype From User where id =: Userinfo.getUserId() limit 1].usertype != label.guestuserlicense) {
                Usrname = [Select Id, AccountId, ContactId, alias From User where id =: Userinfo.getUserId() limit 1].alias;
                shows = 'block';
                isGuest = false;
            }
        }
    }
    
    private void fetchEvent() {
        presentations = new list < Presentation__c > ();
        events = [Select Id, e.Location__c, e.Institution__c, e.From_Date__c, Banner__c, Title__c, To_Date__c, Description__c,
            URL_Path_Settings__c, (select id, title__c from Webinars__r where private__c = false), (SELECT Id, name FROM Attachments where contentType like '%pdf') From Event_Webinar__c e
            where recordtype.name = 'Events'
            and id =: eventId
        ];
        if (events.Description__c == '<br>' || events.Description__c == '' || events.Description__c == null) {
            DescVis = false;
        } else {
            DescVis = true;
        }
        if (events.Attachments.size() > 0) {
            attachvis = true;
        }
        if (events.Webinars__r.size() > 0) {
            webvis = true;
        }
        presentations = [Select p.Title__c, p.Speaker__c, p.Product_Affiliation__c,
            p.Name, p.Institution__c, p.Id, p.Event__r.To_Date__c,
            p.Event__r.From_Date__c, p.Event__r.Location__c, p.Event__r.Title__c, p.Event__r.Description__c,
            p.Event__c, p.Description__c, p.Date__c, (SELECT Id, Name FROM Attachments LIMIT 1) From Presentation__c p where Event__c =: eventId
        ];
        if (presentations.size() > 0) { 
            attachPresVis = true; 
        }
        if (events.Banner__c != null && events.Banner__c.length() > 93) {
            //System.debug('@@@@Test11111' + events.Banner__c);
            //strBanner = events.Banner__c.substring(events.Banner__c.indexof('src') + 5, events.Banner__c.indexof('src') + 94);
            //System.debug('@@@@Test' + strBanner);
        } 
        //events.Description__c = events.Description__c.replaceAll('\\<.*?\\>', '');

    }
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static MvEventDetailController initialize(String eventId){
        MvEventDetailController obj = new MvEventDetailController(eventId);
        return obj;        
    }
    
}
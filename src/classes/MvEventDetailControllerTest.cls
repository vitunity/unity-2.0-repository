@isTest
private class MvEventDetailControllerTest{
    @isTest static void test1() {
        test.startTest();        
        MvEventDetailController.getCustomLabelMap('en');
        test.stopTest();
    } 
    @isTest static void test2() {
        Id RecId = Schema.SObjectType.Event_Webinar__c.RecordTypeInfosByName.get('Events').RecordTypeId;
        Event_Webinar__c sobj = new Event_Webinar__c(
          RecordTypeId = RecId ,                                                                 // Record Type
          Title__c = 'ASTRO 57th Annual Meeting - American Society for Radiation Oncology Annual Meeting',  // Title
          From_Date__c = system.today(),                                                        // From Date
          To_Date__c = system.today(),                                                         // To Date
          Location__c = 'San Francisco, CA',                                                                // Location
          Varian_Event__c = false,                                                                          // Varian Event
          Internal_Event__c = false,                                                                        // Internal Event
          Active__c = true,                                                                                 // Active
          Description__c = '\n',                                                                            // Description
          Reportable__c = false,                                                                            // Reportable
          Radiation_Oncology__c = true,                                                                     // Radiation Oncology
          Medical_Oncology__c = false,                                                                      // Medical Oncology
          Radiosurgery__c = true,                                                                           // Radiosurgery
          Brachytherapy__c = true,                                                                          // Brachytherapy
          Proton__c = true,                                                                                 // Proton
          CME_Event__c = false,                                                                             // CME Event
          Needs_MyVarian_registration__c = false,                                                           // This event needs MyVarian registration
          ASRT_MDCB_ID_No_on_registration__c = false,                                                       // Ask for ASRT/MDCB ID No. on registration
          No_of_Registrations__c = 0,                                                                       // No of Registrations
          Registration_is_closed_for_event__c = false,                                                      // Registration is closed for this event
          Private__c = false,                                                                               // Private
          Banner__c = '\n',                                                                                 // Banner
          URL_Path_Settings__c = 'https://www.astro.org/'                                                  // Landing Page URL
        );
        insert sobj;
        string eventId = sobj.Id;
        
        
        test.startTest();
        MvEventDetailController ctrl = new MvEventDetailController(eventId);        
        //ctrl.fetchEvent();
        test.stopTest();
    }
    
    @isTest static void test3() {
        Id RecId = Schema.SObjectType.Event_Webinar__c.RecordTypeInfosByName.get('Events').RecordTypeId;
        Event_Webinar__c sobj = new Event_Webinar__c(
          RecordTypeId = RecId ,                                                                 // Record Type
          Title__c = 'ASTRO 57th Annual Meeting - American Society for Radiation Oncology Annual Meeting',  // Title
          From_Date__c = system.today(),                                                        // From Date
          To_Date__c = system.today(),                                                         // To Date
          Location__c = 'San Francisco, CA',                                                                // Location
          Varian_Event__c = false,                                                                          // Varian Event
          Internal_Event__c = false,                                                                        // Internal Event
          Active__c = true,                                                                                 // Active
          Description__c = '\n',                                                                            // Description
          Reportable__c = false,                                                                            // Reportable
          Radiation_Oncology__c = true,                                                                     // Radiation Oncology
          Medical_Oncology__c = false,                                                                      // Medical Oncology
          Radiosurgery__c = true,                                                                           // Radiosurgery
          Brachytherapy__c = true,                                                                          // Brachytherapy
          Proton__c = true,                                                                                 // Proton
          CME_Event__c = false,                                                                             // CME Event
          Needs_MyVarian_registration__c = false,                                                           // This event needs MyVarian registration
          ASRT_MDCB_ID_No_on_registration__c = false,                                                       // Ask for ASRT/MDCB ID No. on registration
          No_of_Registrations__c = 0,                                                                       // No of Registrations
          Registration_is_closed_for_event__c = false,                                                      // Registration is closed for this event
          Private__c = false,                                                                               // Private
          Banner__c = '\n',                                                                                 // Banner
          URL_Path_Settings__c = 'https://www.astro.org/'                                                  // Landing Page URL
        );
        insert sobj;
        string eventId = sobj.Id;
        
        
        test.startTest();
        MvEventDetailController.initialize(eventId );
        test.stopTest();
    }
    
}
/*
* Author: Harvey
* Created Date: 18-Sep-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This will be used for my varian website Events/Webinar page
*/
public class MvEventsController
{
    public static List<Edate> liEDate = new List<Edate>();
    public static List<Edate> liEDate1 = new List<Edate>();
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    public static void MvUpcomingEvent()
    {
        map<string, Edate> mapEvents = new map<string, Edate>();
        map<string, Edate> mapEvents1 = new map<string, Edate>();
        User usr = [Select ContactId,usertype, Profile.Name from user where Id=: UserInfo.getUserId()];        
        String usrContId = usr.contactid;
        boolean isGuest;
        if(usrContId != null)
        {
            isGuest = false;
        }
        else
        {
            isGuest = true;
        }

        Event_Webinar__c[] upgEvent;
        List<String> Ids=New List<String>();
        List<GroupMember> grplmember = new List<GroupMember>();
        grplmember = [Select GroupId,UserOrGroupId,Group.Name from GroupMember where Group.Name=:Label.VMS_MyVarian_Partners and UserOrGroupId =: UserInfo.getuserId()];

        for(GroupMember gm: grplmember)
        {
            Ids.add(gm.UserOrGroupId);
        }

        if(usrContId==null)
        {
            if(usr.usertype!= label.guestuserlicense)
            {
                SYSTEM.DEBUG('^^^^NotGuest');
                upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true   order by To_Date__c asc];
            }
            if(usr.usertype == label.guestuserlicense)
            {
                upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true  and Internal_Event__c=false order by To_Date__c asc];
            }
        } 
        if(usrContId!=null)
        {
            if(Ids.size() > 0)
            {
                upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true   order by To_Date__c asc];
            }
            else
            {
                upgEvent = [Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c, URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c >= :system.today() and Private__c = false and Active__c = true  and Internal_Event__c=false order by To_Date__c asc];
            }
        }
        Event_Webinar__c[] recentEvent ;
        if(usrContId==null)
        {
            if(usr.usertype!= label.guestuserlicense)
            {
              SYSTEM.DEBUG('^^^^NotGuest');
              recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true order by To_Date__c desc];
            }
            if(usr.usertype == label.guestuserlicense)
            {
             recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true and Internal_Event__c=false order by To_Date__c desc];
            }
        }
        if(usrContId!=null)
        {
            if(Ids.size() > 0)
            {
            
              recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true order by To_Date__c desc];       
            }
            else
            {
              recentEvent = [Select Id,CME_Event__c, Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), (Select Id, Title From NotesAndAttachments) From Event_Webinar__c where recordtype.name = 'Events' and To_Date__c < :system.today() and Varian_Event__c = true and Internal_Event__c=false order by To_Date__c desc];
            }
        }
        for(Event_Webinar__c sEvents : upgEvent)
        {
            if(mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')) == null)
            {
                mapEvents.put(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy'), new Edate(sEvents));
                system.debug('----------- --------- +++++ ' + mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
                system.debug('---url--'+sEvents.URL__c );
                liEDate.add(mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
            }
            else
            {
                mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')).listEvents.add(sEvents);
            }
        }
        for(Event_Webinar__c sEvents : recentEvent)
        {
            if(mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')) == null)
            {
                mapEvents1.put(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy'), new Edate(sEvents));
                system.debug('----------- --------- +++++ ' + mapEvents.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
                liEDate1.add(mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')));
            }
            else
            {
                mapEvents1.get(DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM - yyyy')).listEvents1.add(sEvents);
            }
        }
    }
    @AuraEnabled
    public static List<Edate> getUpcomingEvents()
    {
        MvUpcomingEvent();
        return liEDate;
    }
    @AuraEnabled
    public static List<Edate> getRecentEvents()
    {
        MvUpcomingEvent();
        return liEDate1;
    }

    public class Edate
    {
        @AuraEnabled public list<Event_Webinar__c> listEvents {get; set;}
        @AuraEnabled public list<Event_Webinar__c> listEvents1 {get; set;}
        @AuraEnabled public string strMnthDate {get; set;}

        public Edate(Event_Webinar__c sEvents)
        {   //DateTime.valueOf(e.to_date__c+':00').format('MMM - yyyy')        
            strMnthDate = DateTime.valueOf(sEvents.To_Date__c+':00').format('MMMM yyyy');
            listEvents = new list<Event_Webinar__c>{sEvents};
            listEvents1 = new list<Event_Webinar__c>{sEvents};
        }
    }
}
global without sharing class MvForgotPasswordController {
    
    Public static String RecvryQs{get;set;}
    Public static String RecvryAns{get;set;}
    Public static String errorreset{get;set;}
    Public static String ValidationError{get;set;}
    public static String forcaptcha{get;set;}
    public static string captchavariable {get{system.debug('$$$$$$$$' + captchavariable);return captchavariable;}set;}
    
    public static String Consentoryes{get;set;}
    
    public MvForgotPasswordController() {
        
    }
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static String forgotPassword(String username, String checkEmailUrl) {
        try {
            Site.forgotPassword(username);
            ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);
            if(!Site.isValidUsername(username)) {
                return Label.Site.invalid_email;
            }
            if(!Test.isRunningTest()) {
                aura.redirect(checkEmailRef);
            }
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();
        }
    }
    
    static String OktaId;
    static String strToken;
    @AuraEnabled
    public static string  getRecovQuestion(String Emailid,String Lastname){
        string resMessage='';
        
        List<User> extuser=[Select id, email,Contact.PasswordresetDate__c, LastLoginDate 
                            from User 
                            where email=:Emailid and isActive != false and Profile.Name =: Label.VMS_Customer_Portal_User ];
        
        If(extUser.size()>0){
            //System.debug('&&&&&&&&&&&&&'+extuser[0].LastLoginDate);
            if(extuser[0].Contact.PasswordresetDate__c== null){                
                // resMessage = 'A recovery question and answer have not been established and therefore the password cannot be reset. Please e-mail MyVarian@varian.com to have your password reset.';
                resMessage='NoQuestion';
                return resMessage;
            }            
        }
        //http request for getting the recovery question from okta
        String OktaStatus;
        // String OktaId;
        String Error;
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        //Blob headerValue = Blob.valueOf(username+':'+password);
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        String EndPoint = Label.oktachangepasswrdendpoint + Emailid.trim();
        req.setendpoint(EndPoint);
        req.setMethod('GET');
        try {
            //Send endpoint to OKTA
            system.debug('aebug ; ' +req.getBody());
            if(!Test.isRunningTest()){
                res = http.send(req);
            }else {
                res = fakeresponse.fakeresponsemethod('NONgetuserbyid');
            }
            
            system.debug('aebug ; ' +res.getBody());
            // Parsing JSON
            JSONParser parser = JSON.createParser(res.getBody());
            
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'id'){
                        OktaId = parser.getText();
                    }else if(fieldName == 'status') {
                        OktaStatus = parser.getText();                          
                    }else if(fieldName == 'credentials'){
                        while (parser.nextToken() != null) {
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                system.debug('Current token----' + parser.getCurrentToken());
                                String fieldName1 = parser.getText();
                                parser.nextToken();
                                if(fieldName1 == 'recovery_question') {
                                    system.debug('Current token----' + parser.getCurrentToken());
                                    while (parser.nextToken() != null) {
                                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                            String fieldName2 = parser.getText();
                                            parser.nextToken();
                                            if(fieldName2 == 'question') {
                                                RecvryQs = parser.getText();
                                                break;
                                            }
                                        }
                                    }  
                                }
                            }
                        }
                    }
                }
            }
            system.debug('Recovery Qusetion ------ ' + RecvryQs +'Status---'+OktaStatus);
        }catch(System.CalloutException e) {
            System.debug(res.toString());
        }
        
        //http for getting user from okta based on login
        if(OktaStatus == 'LOCKED_OUT'){            
            //Api to unlock the user account
            HttpRequest requnlockuser = new HttpRequest();
            HttpResponse resunlockuser = new HttpResponse();
            Http httpunlockuser = new Http();
            requnlockuser.setHeader('Authorization', authorizationHeader);
            requnlockuser.setHeader('Content-Type','application/json');
            requnlockuser.setHeader('Accept','application/json');
            String endpointunlockuser = label.oktachangepasswrdendpoint + OktaId + '/lifecycle/unlock';
            requnlockuser.setMethod('POST');
            requnlockuser.setEndpoint(endpointunlockuser);
            
            if(!test.IsRunningTest()){
                resunlockuser = httpunlockuser.send(requnlockuser);
            } else{
                resunlockuser = fakeresponse.fakeresponsemethod('getuser');
            }
        } 
        if(OktaStatus == null || OktaStatus == 'DEPROVISIONED'){
            //resMessage = 'The e-mail address or last name does not match that of an active MyVarian account.Please register for a new account.';
            resMessage = 'NotMatched';
            RecvryQs = '';
            return resMessage;
        }
        return RecvryQs;
    }
    
    @AuraEnabled
    public static string  changePassword(String Emailid,String Lastname,String Answer,String NewPassWord){
        string resMessage='Test';
        string error;
        errorreset = '';
        ValidationError ='';
        
        Answer = Answer.deleteWhitespace();
        Answer = Answer.toLowerCase();
        
       /* List<Contact> relatedcont = [Select Recovery_question__c,Recovery_Answer__c 
                                     from contact 
                                     where email=:Emailid Limit 1];
        */
        
        if(string.isNotBlank(Answer) && string.isNotBlank(NewPassWord)){
           // system.debug('Answer----' + Answer +'If Loop');         
            
            //resMessage = getRecovQuestion(Emailid,Lastname);
            getRecovQuestion(Emailid,Lastname);
            
            //http request for forgotpassword
            HttpRequest reqforgot = new HttpRequest();
            HttpResponse resforgot = new HttpResponse();
            Http httpforgot = new Http();
            
            //Construct Authorization and Content header
            //Blob headerValue = Blob.valueOf(username+':'+password);
            
            String strToken = label.oktatoken;    
            String authorizationHeader = 'SSWS ' + strToken;
            reqforgot.setHeader('Authorization', authorizationHeader);
            reqforgot.setHeader('Content-Type','application/json');
            reqforgot.setHeader('Accept','application/json');
            String EndPointforgot = Label.oktachangepasswrdendpoint + OktaId +'/credentials/forgot_password';
            reqforgot.setendpoint(EndPointforgot);
            reqforgot.setMethod('POST');
            string bodyforgot = '{"password": { "value": "' + NewPassWord + '" },"recovery_question": { "answer": "' + Answer + '" }}';
            reqforgot.setbody(bodyforgot);
            
            try {
                //Send endpoint to OKTA
                system.debug('aebug ; ' +reqforgot.getBody());
                if(!Test.isRunningTest()){
                    resforgot = httpforgot.send(reqforgot);
                }else {
                    resforgot= fakeresponse.fakeresponsemethod('getuserbyid');
                }
                
                system.debug('aebug ; ' +resforgot.getBody());
                resMessage = resforgot.getBody();
                //return resMessage;
                //Parsing for error
                JSONParser parser = JSON.createParser(resforgot.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                        String fieldName = parser.getText();
                        parser.nextToken();
                        if(fieldName == 'errorCode') {
                            Error = parser.getText();                            
                            if(Error=='E0000001'){
                                ValidationError='Block';
                                errorreset = 'none'; 
                               // break;   
                            }
                            else{
                                errorreset = 'Block'; 
                                ValidationError='none';
                                system.debug('Error-----' + errorreset);
                                break;                      
                            }                                
                        } 
                        else if(fieldName == 'errorSummary'){
                           //resMessage = parser.getText();
                        }
                    }
                }                    
            }catch(System.CalloutException e) {
                System.debug(resforgot.toString());
            }
        }
        else{            
            errorreset = 'Block'; 
        }
        
       // resMessage = Error + ValidationError;
       
        if(errorreset !=null && errorreset == 'Block'){//&& errorreset == 'Block'
            resMessage = 'Error';
        }
        else if(ValidationError !=null && ValidationError == 'Block'){   // && ValidationError == 'Block'             
           // resMessage = Error;
        }
        else{
            resMessage = 'Success';
            //captchavariable = 'none';
            try{
               /* Contact con = [Select id,PasswordresetDate__c,PasswordReset__c,activation_url__c from contact where email=: EmailId limit 1];
                con.PasswordresetDate__c = system.today();
               // con.PasswordReset__c = true;
                con.activation_url__c = '';
                
                update con;*/
                //resMessage = 'Success Contact Update';
                
            }catch(DMLException e){
                system.debug(e.getmessage());
                resMessage = 'Fail Contact Update';
            }            
        }
        
        return resMessage;
    }
    
    /******** Method to extract recovery question and answere from contact of the related user ****/
    @AuraEnabled
    public static string  getextractrecq(String Emailid,String Lastname,String Answer,String NewPassWord){
        string resMessage='';
        
        /*  String Emailid = ApexPages.currentPage().getParameters().get('Emailidparam');
String Lastname = ApexPages.currentPage().getParameters().get('Lastnameparam');
String Answer = ApexPages.currentPage().getParameters().get('Answerparam');
String NewPassWord = ApexPages.currentPage().getParameters().get('PasswordParam');*/
        Answer = Answer.deleteWhitespace();
        Answer = Answer.toLowerCase();
        
        List<User> extuser=[Select id, email,Contact.PasswordresetDate__c, LastLoginDate 
                            from User 
                            where email=:Emailid and isActive != false and Profile.Name =: Label.VMS_Customer_Portal_User ];
        
        If(extUser.size()>0){
            //System.debug('&&&&&&&&&&&&&'+extuser[0].LastLoginDate);
            if(extuser[0].Contact.PasswordresetDate__c== null){                
                resMessage = 'A recovery question and answer have not been established and therefore the password cannot be reset. Please e-mail MyVarian@varian.com to have your password reset.';
                return null;
            }            
        }
        //http request for getting the recovery question from okta
        String OktaStatus;
        String OktaId;
        String Error;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        //Blob headerValue = Blob.valueOf(username+':'+password);
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        String EndPoint = Label.oktachangepasswrdendpoint + Emailid.trim();
        req.setendpoint(EndPoint);
        req.setMethod('GET');
        try {
            //Send endpoint to OKTA
            system.debug('aebug ; ' +req.getBody());
            if(!Test.isRunningTest()){
                res = http.send(req);
            }else {
                res = fakeresponse.fakeresponsemethod('NONgetuserbyid');
            }
            system.debug('aebug ; ' +res.getBody());
            // Parsing JSON
            JSONParser parser = JSON.createParser(res.getBody());
            
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'id'){
                        OktaId = parser.getText();
                    }else if(fieldName == 'status') {
                        OktaStatus = parser.getText();                          
                    }else if(fieldName == 'credentials'){
                        while (parser.nextToken() != null) {
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                system.debug('Current token----' + parser.getCurrentToken());
                                String fieldName1 = parser.getText();
                                parser.nextToken();
                                if(fieldName1 == 'recovery_question') {
                                    system.debug('Current token----' + parser.getCurrentToken());
                                    while (parser.nextToken() != null) {
                                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                            String fieldName2 = parser.getText();
                                            parser.nextToken();
                                            if(fieldName2 == 'question') {
                                                RecvryQs = parser.getText();
                                                // forcaptcha = 'none';
                                                // captchavariable = 'none';
                                                // IsVisible='none';
                                                break;
                                            }
                                        }
                                    }  
                                }
                            }
                        }
                    }
                }
            }
            system.debug('Recovery Qusetion ------ ' + RecvryQs +'Status---'+OktaStatus);
        }catch(System.CalloutException e) {
            System.debug(res.toString());
        }
        
        //http for getting user from okta based on login
        if(OktaStatus == 'LOCKED_OUT'){            
            //Api to unlock the user account
            HttpRequest requnlockuser = new HttpRequest();
            HttpResponse resunlockuser = new HttpResponse();
            Http httpunlockuser = new Http();
            requnlockuser.setHeader('Authorization', authorizationHeader);
            requnlockuser.setHeader('Content-Type','application/json');
            requnlockuser.setHeader('Accept','application/json');
            String endpointunlockuser = label.oktachangepasswrdendpoint + OktaId + '/lifecycle/unlock';
            requnlockuser.setMethod('POST');
            requnlockuser.setEndpoint(endpointunlockuser);
            
            if(!test.IsRunningTest()){
                resunlockuser = httpunlockuser.send(requnlockuser);
            } else{
                resunlockuser = fakeresponse.fakeresponsemethod('getuser');
            }
        } 
        if(OktaStatus == null || OktaStatus == 'DEPROVISIONED'){
            resMessage = 'The e-mail address or last name does not match that of an active MyVarian account.Please register for a new account.';
            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The e-mail address or last name does not match that of an active MyVarian account.Please register for a new account.'));
            RecvryQs = '';
            // forcaptcha = 'block';
            // captchavariable = 'block';
            //errorreset = 'Block';
            return null;
        }
        else{
            List<Contact> relatedcont = [Select Recovery_question__c,Recovery_Answer__c 
                                         from contact 
                                         where email=:Emailid Limit 1];
            //RecvryQs = relatedcont[0].Recovery_question__c;
            //System.debug('$$$$$$$$$$' + Emailid + Lastname + RecvryQs);
            if(Answer != '@' && NewPassWord != null){
                system.debug('Answer----' + Answer +'If Loop');         
                
                //http request for forgotpassword
                HttpRequest reqforgot = new HttpRequest();
                HttpResponse resforgot = new HttpResponse();
                Http httpforgot = new Http();
                
                //Construct Authorization and Content header
                //Blob headerValue = Blob.valueOf(username+':'+password);
                //String strToken = '000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
                //String authorizationHeader = 'SSWS ' + strToken;
                reqforgot.setHeader('Authorization', authorizationHeader);
                reqforgot.setHeader('Content-Type','application/json');
                reqforgot.setHeader('Accept','application/json');
                String EndPointforgot = Label.oktachangepasswrdendpoint + OktaId +'/credentials/forgot_password';
                reqforgot.setendpoint(EndPointforgot);
                reqforgot.setMethod('POST');
                string bodyforgot = '{"password": { "value": "' + NewPassWord + '" },"recovery_question": { "answer": "' + Answer + '" }}';
                reqforgot.setbody(bodyforgot);
                try {
                    //Send endpoint to OKTA
                    system.debug('aebug ; ' +reqforgot.getBody());
                    if(!Test.isRunningTest()){
                        resforgot = httpforgot.send(reqforgot);
                    }else {
                        resforgot= fakeresponse.fakeresponsemethod('getuserbyid');
                    }
                    system.debug('aebug ; ' +resforgot.getBody());
                    //Parsing for error
                    JSONParser parser = JSON.createParser(resforgot.getBody());
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                            String fieldName = parser.getText();
                            parser.nextToken();
                            if(fieldName == 'errorCode') {
                                Error = parser.getText();
                                
                                if(Error=='E0000001')
                                {
                                    ValidationError='Block';
                                    errorreset = 'none'; 
                                    break;   
                                }
                                else{
                                    errorreset = 'Block'; 
                                    ValidationError='none';
                                    system.debug('Error-----' + errorreset);
                                    break;                      
                                }                                
                            }                            
                        }
                    }                    
                }catch(System.CalloutException e) {
                    System.debug(resforgot.toString());
                }
            }
            else{
                system.debug('Inside else before if' + Answer);
                if(Answer != '@'){
                    system.debug('Inside else and inside if' + Answer);
                    RecvryQs = '';
                    //forcaptcha = 'none';
                    errorreset = 'Block';
                }
                //navigate to first page logic keeping the variable null 
            }
            if(errorreset == 'Block'){
                //RecvryQs = '';
                return null;
            }
            else if(ValidationError == 'Block'){                
                return null;
            }
            else{
                Contact con = new Contact(id=[Select id from contact where email=: EmailId limit 1].Id);
                con.PasswordresetDate__c = system.today();
                con.PasswordReset__c = true;
                con.activation_url__c = '';
                captchavariable = 'none';
                try{
                    update con;
                }catch(DMLException e){
                    system.debug(e.getmessage());
                }
                if(Answer != '@'){
                    // return new pagereference('/apex/VR_Registration_PG?Request=Login&msg=true');
                    return '/apex/VR_Registration_PG?Request=Login&msg=true';
                }else{
                    system.debug('captchavariable-----value---' + captchavariable);
                    return null;
                }
            }
            
        }
    }
    
    /*In the validate method we make sure that the 3 characters entered equal the three black characters: char1, char3, char5*/
    /*public pagereference  verify(){
Consentoryes = ApexPages.currentPage().getParameters().get('Consentoryes');

string page;
if (forcaptcha == 'none' && Request == 'Reqnewpass'){
page = extractrecq();
if(errorreset == 'Block'){
//RecvryQs = '';
return null;
}
else {
if(page != null){
page.setredirect(true);
return page;
}
else//new pagereference('/apex/VR_Registration_PG?Request=Login');
return null;  
}              
}
else {
if(input.length() == 3 && input.subString(0,1) == char1 && input.subString(1,2) == char3 && input.subString(2,3) == char5){
if (Request == 'Reqnewpass')
{
//SendNewpassword();
//Resetpass();
extractrecq();
result='';
//return null;   
}
else
{
SendNotification(Consentoryes);
//PageReference redirect = Page.cpmyvarianhomepage;
//return redirect;
PageReference pr = new PageReference('/apex/cpmyvarianhomepage?msage=true');

pr.setredirect(true);
return pr;                
}
}            
else{
result = 'Text didn’t match black characters. Please try again.'; 
if(Consentoryes !=null)
{
PageReference pr = new PageReference('/apex/VR_Registration_PG?mssg=true');

pr.setredirect(true);
return pr;
}
}
}
return null;

}*/
    
}
@isTest(seeAllData=true)
private class MvForgotPasswordControllerTest {

    public static Account act;  
    public static Contact con;
    public static User user;

	public static Idea idea1;
	public static User thisUser;
	public static User newUser;
	public static UserRole rol;
	public static Profile pAdmin;

    static
    {

        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
        User u;
 
        pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        rol = [select id from UserRole LIMIT 1];           

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

        act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert act;  
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'standarduser@testorg.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
            MailingState='CA', Phone = '12452234', 
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        //Creating a running user with System Admin profile
        insert con;

        UserRole rl = [SELECT Id from UserRole Limit 1];

        System.runAs(thisUser) {
            newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
            insert newUser;    	            
        }

    }
	
	@isTest static void getCustomLabelMapTest() {
		Test.startTest();
			MvForgotPasswordController.getCustomLabelMap('EN');
		Test.stopTest();
	}
	
	@isTest static void forgotPasswordTest() {
		Test.startTest();
			MvForgotPasswordController.forgotPassword('test123user123@user.com', 'www.yahoo.com');
		Test.stopTest();
	}
	
	@isTest static void getRecovQuestionTest() {
		Test.startTest();
			System.runAs(newUser) {
				MvForgotPasswordController.getRecovQuestion('standarduser@testorg.com', 'Testing');
			}
		Test.stopTest();
	}

	@isTest static void changePasswordTest() {
		Test.startTest();
			System.runAs(newUser) {
				MvForgotPasswordController.changePassword('standarduser@testorg.com', 'Testing', 'TestAnswer', 'newPwd');
			}
		Test.stopTest();
	}

	@isTest static void getextractrecqTest() {
		Test.startTest();
			System.runAs(newUser) {
				MvForgotPasswordController.getextractrecq('standarduser@testorg.com', 'Testing', 'TestAnswer', 'newPwd');
			}
		Test.stopTest();
	}

	@isTest static void additionalTest() {
		String test1 = MvForgotPasswordController.RecvryAns;
		String test2 = MvForgotPasswordController.forcaptcha;
		String test3 = MvForgotPasswordController.Consentoryes;
	}
}
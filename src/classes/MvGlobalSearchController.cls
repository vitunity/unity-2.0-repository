/*************************************************************************\
@ Author        : Deepak Sharma
@ Date          : 30-Sep-2017
@ Description   : Classs defined to handle logic of Global Search. 
Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************

****************************************************************************/

public without sharing class MvGlobalSearchController {
    
    
  /**
    * @Def: Method Invoked on page load. It sets custom labels as per selected language
    * @return: Map consisiting of label and respective Lang translated values
    * @Todo : Handle Errors -Depends on Error Handling Framework
    * 
    ***/
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
        
    
    /**
    * @Def: Method Invoked on page load. It sets the dropdown list and search response
    * @return: Map consisiting of search response and drop down list.
    * @Todo : Handle Errors -Depends on Error Handling Framework
    * 
    ***/
    @AuraEnabled
    public static Map<String,GlobalSearchResponse>  pageLoad(String searchText, Boolean redirectFlag)
    {       
        Map<String,GlobalSearchResponse> mapGlobalSearchResponse = new Map<String,GlobalSearchResponse>();
        if(redirectFlag)
        {   
            mapGlobalSearchResponse.put('selectOptions',new GlobalSearchResponse(null,getSearchOptions()));
            if(String.isNotEmpty(searchText))
            {
                mapGlobalSearchResponse.put('Documentations',new GlobalSearchResponse(getDocumentList(searchText),null));
                mapGlobalSearchResponse.put('ProductIdeas',new GlobalSearchResponse(getProductIdeaList(searchText),null));
                mapGlobalSearchResponse.put('Webinars',new GlobalSearchResponse(getWebinarList(searchText), null));
                mapGlobalSearchResponse.put('Events',new GlobalSearchResponse(getEventList(searchText),null));
            }
            else
            {
                //Add Error Message
            }
        }
        else
        {
            mapGlobalSearchResponse.put('selectOptions',new GlobalSearchResponse(null,getSearchOptions()));
        }         
        return mapGlobalSearchResponse;
    }
    
    /**
    * @Def: Method Invoked on search. It sets  search response
    * @return: Map consisiting of search response.
    * @Todo : Handle Errors -Depends on Error Handling Framework
    * 
    ***/
    
    @AuraEnabled
    public static Map<String,GlobalSearchResponse> globalSearchResult(String selOption , String searchText)
    {
        Map<String,GlobalSearchResponse> mapGlobalSearchResponse=new Map<String,GlobalSearchResponse>();
        mapGlobalSearchResponse.clear();
        system.debug('searchText==='+searchText);
        system.debug('selOption==='+selOption);
        if(String.isNotEmpty(searchText))
        {            
            if(selOption=='All')
            {
                mapGlobalSearchResponse.put('Documentations',new GlobalSearchResponse(getDocumentList(searchText),null));
                mapGlobalSearchResponse.put('ProductIdeas',new GlobalSearchResponse(getProductIdeaList(searchText),null));
                mapGlobalSearchResponse.put('Webinars',new GlobalSearchResponse(getWebinarList(searchText), null));
                mapGlobalSearchResponse.put('Events',new GlobalSearchResponse(getEventList(searchText),null));                
            }
            if(selOption=='Announcements')
            {                       
                //mapGlobalSearchResponse.put('Announcements',getAnnouncementList(searchText));
            }
            if(selOption=='Documentations')
            {
                mapGlobalSearchResponse.put('Documentations',new GlobalSearchResponse(getDocumentList(searchText),null));
            }
            if(selOption=='Webinars')
            {   
                mapGlobalSearchResponse.put('Webinars',new GlobalSearchResponse(getWebinarList(searchText), null));
            }
            if(selOption=='Events')
            {
                mapGlobalSearchResponse.put('Events',new GlobalSearchResponse(getEventList(searchText),null));
            }
            if(selOption=='Product Ideas')
            {
                mapGlobalSearchResponse.put('ProductIdeas',new GlobalSearchResponse(getProductIdeaList(searchText),null));
            }
        }
        else
        {
            //Add Error Message
        }
        return mapGlobalSearchResponse;  
    }
    
    /**
* @Def:  Method defined to get the documents containing 'Search Text'. Invoked by
* pageLoad and globalSearchResult methods
* @return: List<ContentVersion>
* @Todo : Handle Errors -Depends on Error Handling Framework
* 
***/
    public static List<ContentVersion> getDocumentList(string searchText) {
        try {
            set<Id> conDocID = new set<Id>();
            List<ContentVersion>  lstContentVersions  = new List<ContentVersion>();  
            List<ContentVersion> lstConVersion=new List<ContentVersion>();
            List<ContentVersion> listConVerFinal = new List<ContentVersion>(); 

            string whr='';
            set<Id> contentId = new set<Id>();

            /*
            String searchQuery = 'select id, Region__c, IsLatest, VersionNumber, ContentDocumentId, Title, Product_Group__c, Description, Mutli_Languages__c, Document_Language__c, Document_Type__c, Document_Number__c, Document_Version__c, Parent_Documentation__c' +
                ' FROM ContentVersion '+
                ' where IsLatest = true AND (Title like \'%' + searchText + '%\' or ' +
                'Document_Language__c = \'' + searchText + '\' or ' +
                'Document_Number__c like \'%' + searchText + '%\' or ' +
                'Product_Group__c includes (:searchText)  or ' +
                'Description like \'%' + searchText + '%\') LIMIT 20';
            lstConVersion = (List<ContentVersion>)Database.query(searchQuery);
            */

            if(!string.isEmpty(searchText)){
                List<String> searchKeyList = searchText.split(' ');
                String soslSearchQuery = '';
               
                for(String soslKey : searchKeyList){
                    soslSearchQuery = soslSearchQuery + '%' +soslKey + '%' +' AND ';
                }
                soslSearchQuery = '{'+soslSearchQuery.removeEnd(' AND ')+'}';
                system.debug('==soslSearchQuery=='+soslSearchQuery);
                String documentSOSLQuery = 'FIND '+soslSearchQuery+' IN ALL FIELDS RETURNING ContentVersion(id, Region__c, IsLatest, VersionNumber, ContentDocumentId, Title, Product_Group__c, Description, Mutli_Languages__c, Document_Language__c, Document_Type__c, Document_Number__c, Document_Version__c, Parent_Documentation__c where IsLatest = true LIMIT 10)';
                system.debug('==documentSOSLQuery=='+documentSOSLQuery);
                List<List<SObject>> searchResults = search.query(documentSOSLQuery);
                system.debug('==searchResults=='+searchResults);
                lstConVersion = searchResults[0];
                //List<ContentVersion> localList = new List<ContentVersion>();
                //for(ContentVersion ct : lstConVersion)
                //{
                //    boolean b = true;
                //    for(string s :searchKeyList)
                //    {
                //        if(!ct.title.contains('s'))
                //        {
                //          b = false;
                //        }
                //    }
                //    if(b)
                //        localList.add(ct);
                //}
                //lstConVersion = localList;
                system.debug('==lstConVersion=='+lstConVersion);
            }

            Set<String> setChildRec=new Set<String>();
            Map<ID,Sobject> mapOrigRec=New  Map<ID,Sobject>();
            mapOrigRec.putall(lstConVersion );
            ContentVersion tempContentVersionRec=New ContentVersion ();
            for(ContentVersion cnt : lstConVersion){ 
                if(cnt.Parent_Documentation__c!=null){
                    setChildRec.add(cnt.Parent_Documentation__c);
                    tempContentVersionRec=(ContentVersion) mapOrigRec.remove(cnt.id);
                }
            }
            List<ContentVersion> objParentRec=[select ID, Region__c, Date__c,Mutli_Languages__c, Parent_Documentation__c, ContentDocumentId, Document_Type__c, Document_Number__c, Title 
                                               FROM ContentVersion 
                                               where RecordType.name ='Product Document' and Document_Number__c in: setChildRec];
            
            for(ContentVersion parentRec:objParentRec){
                if(!mapOrigRec.containsKey(parentRec.id)){
                    //mapOrigRec.put(parentRec.id,parentRec);
                    if(parentRec.Parent_Documentation__c==null){
                       mapOrigRec.put(parentRec.id,parentRec);
                    }
                }
            }
            lstConVersion.clear();
            lstConVersion = mapOrigRec.values();
            whr = whr + ' and Id in : lstConVersion';
            system.debug('#PRO4:'+lstConVersion.size() +'--'+lstConVersion);
            Map<String,List<ContentVersion>> mapDocuments=new Map<String,List<ContentVersion>>();

            //Filter out List based on Region - Ticket ## DFCT0013076
            String strReg = getTerritory();
            //strReg = 'APAC';

            for(ContentVersion conFilter : lstConVersion) {
                if(String.isNotBlank(conFilter.Region__c))
                if(conFilter.Region__c.containsIgnoreCase(strReg)
                   || conFilter.Region__c.containsIgnoreCase('All')
                  ) 
                {
                    listConVerFinal.add(conFilter);
                }
            }
            // return lstContentVersions;
            System.debug('#### debug listConVerFinal size = ' + listConVerFinal.size());

            //mapDocuments.put('Documentations', lstConVersion);
            mapDocuments.put('Documentations', listConVerFinal);
            return listConVerFinal;     // lstConVersion;
        } 
        catch (QueryException ex) {  
            return null;
        }
    }

    private static String getTerritory() {
        String terr = 'All';
        List<User> listUsr = [Select ContactId, Contact.Territory__c from user where Id=: UserInfo.getUserId()];

        if(listUsr != null && listUsr.size() > 0 && listUsr[0].ContactId != null) {
            terr = listUsr[0].Contact.Territory__c;
        }

        return terr;
    }
    
    /**
* @Def:  Method defined to get the Idea containing 'Search Text'. Invoked by
* pageLoad and globalSearchResult methods
* @return: List<Idea>
* @Todo : Handle Errors -Depends on Error Handling Framework
* 
***/
    public  static List<Idea> getProductIdeaList(String searchText) {
        String param1 = 'Converted to Case';
        String param2 = 'Duplicate';
        String param3 = 'Internal';
        String param4 = 'New';
        String param5 = 'Pending Customer Input';
        List<Idea> lstPIdea = new List<Idea>(); 
        List<Idea> lstFIdea = new List<Idea>();
        try{           
            set<Id> setFilteredIds = new set<Id>();
            setFilteredIds.clear();           
            //List<Idea> lstFIdea = [Select id,title,body, Categories from Idea where title <> null and Status <> 'Converted to Case' and Status <> 'Duplicate' and Status <> 'Internal'  and  (Status <> 'New' or  (Status = 'New'  and  (Status <> 'Pending Customer Input' or  (Status = 'Pending Customer Input' )) and (isMerged = false or  (isMerged = true )))) LIMIT 20];      
            
            String searchQuery = 'Select id,title,body, Categories from Idea where title <> null and Status <> :param1 and Status <> :param2 and Status <> :param3  and  (Status <> :param4 or  (Status = :param4  and  (Status <> :param5 or  (Status = :param5 )) and (isMerged = false or  (isMerged = true )))) and (' +
              ' title like \'%' + searchText + '%\' or ' +
              ' Categories includes (:searchText)  ) LIMIT 10';
            lstFIdea = (List<Idea>)Database.query(searchQuery);
            
            system.debug('#### debug lstFIdea = ' + lstFIdea);

            /*
            for(Idea i : lstFIdea){
                if(((i.title) <> null && (i.title).containsIgnoreCase(searchText)) 
                    || ((i.body) <> null && (i.body).containsIgnoreCase(searchText)) 
                    || ((i.Categories) <> null && (i.Categories).containsIgnoreCase(searchText)) 
                 ){
                    setFilteredIds.add(i.id);
                    lstPIdea.add(i);
                }
            }    */     

            for(Idea i : lstFIdea){
                if(i.body <> null && (i.body).containsIgnoreCase(searchText)) 
                {
                    setFilteredIds.add(i.id);
                    lstPIdea.add(i);
                }
            }

        }
        catch(Exception e){ 
            return null;
        }
        
        return lstPIdea;
    }
    
    /**
    * @Def:  Method defined to get the Webinar containing 'Search Text'. Invoked by
    * pageLoad and globalSearchResult methods
    * @return: List<Event_Webinar__c>
    * @Todo : Handle Errors -Depends on Error Handling Framework
    * 
    ***/
    public static List<Event_Webinar__c> getWebinarList(String searchText)
    {
        
        List<Event_Webinar__c> cn ;
        List<Event_Webinar__c> listWebinar;
        String temp = '%' + searchText + '%';
        String   searchquery = 'Select e.ID, e.Institution__c, e.From_Date__c,e.Speaker__c , e.Title__c,e.Product_Affiliation__c From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp) LIMIT 5' ;
        listWebinar = (List<Event_Webinar__c>)DataBase.Query(searchquery);
        for(Event_Webinar__c st : listWebinar)
        {
            if(st.From_Date__c < System.today())
            {
                //liveWebinars.add(st);
            }
        }
        for(Event_Webinar__c st : listWebinar)
        {
            if(st.From_Date__c >= System.today())
            {
                // liveWebinars.add(st);
            }
        }   
        return listWebinar; 
    } 
    
    /**
    * @Def:  Method defined to get the Events containing 'Search Text'. Invoked by
    * pageLoad and globalSearchResult methods
    * @return: List<Event_Webinar__c>
    * @Todo : Handle Errors -Depends on Error Handling Framework
    * 
    ***/
    public static List<Event_Webinar__c> getEventList(String searchText)
    {
        
        List<Event_Webinar__c> cn ;
        List<Event_Webinar__c> listWebinar;
        String temp = '%' + searchText + '%';
        String   searchquery = 'Select Id,CME_Event__c,Varian_Event__c,URL__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c,'+
            ' Location__c, Radiation_Oncology__c, Radiosurgery__c, Proton__c, Brachytherapy__c, Medical_Oncology__c,Online_Registration_Limit__c,No_of_Registrations__c,' +
            ' URL_Path_Settings__c,Active__c,Description__c, From_Date__c, Title__c, To_Date__c, (Select Id, Title__c From Presentations__r), '+
            '(Select Id, Title From NotesAndAttachments) From Event_Webinar__c '+
            'where recordtype.name = \'Events\'  and Private__c = false and Active__c = true  and Internal_Event__c=false and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp) order by To_Date__c asc LIMIT 5';
        listWebinar = (List<Event_Webinar__c>)DataBase.Query(searchquery);
        for(Event_Webinar__c st : listWebinar)
        {
            if(st.From_Date__c < System.today())
            {
                //liveWebinars.add(st);
            }
        }
        for(Event_Webinar__c st : listWebinar)
        {
            if(st.From_Date__c >= System.today())
            {
                // liveWebinars.add(st);
            }
        }   
        return listWebinar; 
    }
    
    
    /**
    * @Def:  Method defined to set the drop down list. Invoked by
    * pageLoad 
    * @return: List<SelectOption>
    * * 
    ***/
    
    public static List<SelectOption> getSearchOptions()
    {       
        List<SelectOption> selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('All','All'));
        //selOptions.add(new SelectOption('Announcements','Announcements'));
        selOptions.add(new SelectOption('Documentations','Documentations'));
        selOptions.add(new SelectOption('Webinars','Webinars'));
        selOptions.add(new SelectOption('Events','Events'));
        selOptions.add(new SelectOption('Product Ideas','Product Ideas'));
        System.debug('Deepak ::: Returning From List<SelectOption>');
        return selOptions   ;
        
    }
    
    /**********************************************************************/
    /******************* Wrapper Class ************************************/
    /**********************************************************************/
    public class SelectOption {
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
        @AuraEnabled
        public Boolean disabled { get;set; }
        @AuraEnabled
        public Boolean escapeItem { get;set; } 
        public SelectOption(String value, String label) {
            this.value = value;
            this.label = label;
            this.disabled = false;
            this.escapeItem = false;
        }
        public SelectOption(String value, String label, Boolean isDisabled) {
            this.value = value;
            this.label = label;
            this.disabled = isDisabled;
            this.escapeItem = false;
        }
    }
    
    public class GlobalSearchResponse{
        @AuraEnabled   List<sObject> respObj{get;set;}
        @AuraEnabled List<SelectOption> selOpt{get;set;}
        public GlobalSearchResponse()
        {
            //Do Nothing
        }
        public GlobalSearchResponse( List<sObject> sObj,List<SelectOption> optObj)
        {
            respObj=sObj;
            selOpt=optObj;             
        }
    }
    
}
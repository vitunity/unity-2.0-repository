/*************************************************************************\
@ Author        : Deepak Sharma
@ Date          : 28-Oct-2017
@ Description   : Test Class for Global Search Controller (MVGlobalSearchController)
Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************

****************************************************************************/

@isTest
public class MvGlobalSearchTest {
    @isTest public static void testGetCustomLabelMap()
    {
        test.startTest();        
        MvGlobalSearchController.getCustomLabelMap('en');
        test.stopTest();
        
    }
    
     @isTest public static void testPageLoadTrueFlag()
    {
        test.startTest();        
        MvGlobalSearchController.pageLoad('test',true);
        test.stopTest();
        
    }
    
       @isTest public static void testPageLoadFalseFlag()
    {
        test.startTest();        
        MvGlobalSearchController.pageLoad('test',false);
        test.stopTest();
        
    }
    
    
       @isTest public static void testGlobalSearchResultAll()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('All');
        //MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('All','test');
        test.stopTest();
        
    }
    
       @isTest public static void testGlobalSearchResultAnnouncements()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('Announcements');
        //MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('Announcements','test');
        test.stopTest();
        
    }

       @isTest public static void testGlobalSearchResultDocumentations()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('Documentations');
        //MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('Documentations','test');
        test.stopTest();
        
    }
      @isTest public static void testGlobalSearchResultProductIdeas()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('Product Ideas');
        //MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('Product Ideas','test');
        test.stopTest();
        
    }
      @isTest public static void testGlobalSearchResultWebinars()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('Webinars');
       // MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('Webinars','test');
        test.stopTest();
        
    }
      @isTest public static void testGlobalSearchResultEvents()
    {
        test.startTest();  
        List<String> selList=new List<String>();
        selList.add('Events');
        //MvGlobalSearchController.globalSearchResult(selList,'test');
        MvGlobalSearchController.globalSearchResult('Events','test');
        test.stopTest();
        
    }
      
}
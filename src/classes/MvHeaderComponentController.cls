/*
 * author: amitkumar katre
 * description : apex controller for login, navigation, log out register user etc components
 * used in mvheadercomponet backed code controller
 */
public without sharing class MvHeaderComponentController {
    
    /*data members*/
    public static User loginUser ;
    public static String onetimeToken = null;
    public static String oktaUserId = null;
    public static String sessionId = null;
    public static Contact con = MvUtility.getContact();
    /* Get user data on component */
    @AuraEnabled
    public static User getLoggedInUser(){
        return [SELECT Id, Name, Email,ContactId,Dosiometrist__c,Contact.AccountId,Contact.Preferred_Language1__c,
                Contact.Is_Preferred_Language_selected__c, Contact.ShowAccPopUp__c, 
                Contact.Email_Opt_in__c, Contact.HasOptedOutOfEmail, 
                LMS_Status__c, Contact.PasswordReset__c, is_Model_Analytic_Member__c
                FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
    }
    
    @AuraEnabled
    public static boolean isMarketingUser(){
        User u = [SELECT Id, LastName, Email, Contact.MailingCountry, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            return false;
        }
        String conCntry = u.Contact.MailingCountry;
        boolean IsMarketYourCenter = false;
        if (conCntry != null) {
            list < MarketingYourCenterCountries__c > AllCountryCount = MarketingYourCenterCountries__c.getall().values();
            for (MarketingYourCenterCountries__c MrktCountries: AllCountryCount) {
                if (MrktCountries.Name == conCntry) {
                    IsMarketYourCenter = true;
                }
            }
        }else{
            IsMarketYourCenter = true;
        }
        return IsMarketYourCenter;
    }
    
    
    @AuraEnabled
    public static Contact getContactInfo() 
    {
        system.debug('#### debug getContactInfo called');
        con = MvUtility.getContact();
        return con;
    }
    
    @AuraEnabled
    public static void saveMyContact() 
    {
        con.showAccPopUp__c = true;
        update con;
    }

    @AuraEnabled
    public static void updateUserLMSMethod() {
        User usr = [Select Id, LMS_Last_Access_Date__c From User where id = : Userinfo.getUserId() limit 1];
        usr.LMS_Last_Access_Date__c = system.now();
        update usr;
    }   
     
    /* Check authentication */
    @AuraEnabled
    public static boolean isLoggedIn(){
        User u = [SELECT Id, LastName, Email, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            return false;
        }else{
            return true;
        }
        
    }

    @AuraEnabled
    public static List<String> getLanguagePicklistOptn(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Contact');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple){
                pickListValues.add(f.getLabel()+','+f.getValue());
        } 
        return pickListValues;
    }
    
    /* Check Partner Channel user */
    @AuraEnabled
    public static boolean isPartnerChannelUsr() {

        Map<String, My_Varian_Partner_Channel_Group__c> mapParterChnSets = My_Varian_Partner_Channel_Group__c.getAll();
        List<String> listGrps = new List<String>();
        if(mapParterChnSets != null) {
            listGrps.addAll(mapParterChnSets.keySet());
        }

        List<GroupMember> listGrpMbr = [SELECT groupId, UserOrGroupId, group.name, group.type FROM GroupMember 
             WHERE UserOrGroupId =: UserInfo.getUserId() 
             AND group.name IN :listGrps];

        if(listGrpMbr.size() > 0){
            return true;
        }else{
            return false;
        }
        
    }

    /*Get logged in user's Country if it is in Sanctioned custom setting */
    @AuraEnabled
    public static Boolean isCurrentUserInSancCountries() {
        Boolean curUserInSancCountry = false;
        User un = [Select Id, AccountId,ContactId,usertype,alias,Contact.Distributor_Partner__c,Contact.Account.country1__r.name From User where id = : Userinfo.getUserId() limit 1];
        if(Un.ContactId <> null && un.Contact.AccountId <> null && Un.Contact.Account.country1__c <> null){
            Sanctioned_Countries__c csett = Sanctioned_Countries__c.getValues(un.Contact.Account.country1__r.Name);
            if(csett <> null){
                curUserInSancCountry = true;
            }
        }       
        return curUserInSancCountry;
    }

    /*Check if logged in User in VMS PT - Installation public group or not */
    @AuraEnabled
    public static Boolean isCurUserPTInstGrp() {
        Boolean curUserPTInstGrp = false;
        List<User> listUsers = [Select Id, AccountId,ContactId,usertype,alias,Contact.Distributor_Partner__c,Contact.Account.country1__r.name From User where id = : Userinfo.getUserId() limit 1];
        if(listUsers.size() > 0){
            list<GroupMember> listGrpM = [Select Id, group.type, group.name, UserOrGroupId From GroupMember where group.name ='VMS PT - Installation' and UserOrGroupId = :listUsers[0].Id];
            if(listGrpM.size() > 0){
                curUserPTInstGrp = true;
            }
        }       
        return curUserPTInstGrp;
    }

    /*Get logged in user's Country if it is in Support Case Approved Countries custom settings */
    @AuraEnabled
    public static Boolean isCurrentUserInCaseApprovCountries() {
        Boolean checkcountry = false;
        set<String> countryset = new set<String>();
        User un = [Select Id, AccountId,ContactId,usertype,alias,Contact.Distributor_Partner__c,Contact.Account.country1__r.name From User where id = : Userinfo.getUserId() limit 1];
        if (un.ContactId == null)
        {
            checkcountry = true;
        }
        if(un.Contact.Distributor_Partner__c)
        {
            checkcountry = true;
        }
        for(Support_Case_Approved_Countries__c countries : Support_Case_Approved_Countries__c.getall().values())
        {
            countryset.add(countries.name);
        }
        if(countryset.contains(un.Contact.Account.country1__r.name) && checkcountry == false)
        {
            checkcountry = true;
        }
        for (Contact_Role_Association__c contactacc : [Select Account__c,Account__r.country1__r.name from Contact_Role_Association__c where contact__c =: un.ContactId])
        {
            if(checkcountry == false && countryset.contains(contactacc.Account__r.country1__r.name))
            {
                checkcountry = true;
            }
        }

        return checkcountry;
    }


    /*
     * custom login code using okta
     */
    @AuraEnabled
    public static string loginCommunity(String username, String password){
        String redirectPage;
        Integer respStatus = 0;
        /* Sending request to Okta */
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        String returnUrlforLogin = '';
        //Construct Authorization and Content header
        String strToken = Label.oktatoken;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        String body = '{ "username": "'+username+'", "password": "'+password+'" }';
        req.setBody(body);
        req.setMethod('POST');
        req.setEndpoint(Label.oktaendpoint);
        try{
            system.debug('===='+req);
            system.debug('====body==='+body);
            res = http.send(req);
            respStatus = res.getStatusCode();        
            if((respStatus == 200) || (respStatus == 204) ) {
                MyVarianJSONParser pars = MyVarianJSONParser.parse(res.getBody());
                sessionId = pars.id;
                oktaUserId = pars.userId;
                onetimeToken = pars.cookieToken;
                system.debug('====sessionId==='+sessionId);
                system.debug('====oktaUserId==='+oktaUserId);
                system.debug('====onetimeToken==='+onetimeToken);
                
                saveUserSessionId(sessionId);       
                returnUrlforLogin = Label.oktacploginurl + onetimeToken + '&RelayState=' + '/varian/s/homepage';//supportcases//homepage
                sendEmail(new list<String>{'amitkumar.katre@varian.com'}, 'subject=='+onetimeToken+oktaUserId,sessionId, 'Amit');
            }
            return returnUrlforLogin;
        }catch(Exception e){
            system.debug('error::MvHeaderComponentController');
            return '/varian/s/error';
        }
    }
    
    /*
     * Save user and okta session for log out later
     */
    public static void saveUserSessionId(String sId) {
        system.debug('saveUserSessionId::MvHeaderComponentController');
        loginUser = new User();
        list<User> usrList = [Select Id, IsActive, Email from User where Contact.OktaId__c =: oktaUserId and isactive = true];
        if (usrList.size() == 0) {
            system.debug('User not found error');
        } else {
            loginUser = usrList[0];
            //Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
            //sessionmap = usersessionids__c.getall();
            //for(usersessionids__c u :[Select name,session_id__c from usersessionids__c])
            //{
            //sessionmap.put(u.name,u);
            //}
            usersessionids__c usersessionrecord = new usersessionids__c(name = loginUser.id, session_id__c = sId);
    
            //if( !sessionmap.containskey(loginUser.id) ) {
            
            //List<usersessionids__c> existingIds = [Select session_id__c from usersessionids__c where session_id__c=:sessionId];
            //if(existingIds!=null && existingIds.size()>0) delete existingIds;
            
            upsert usersessionrecord;
            //} else {
               //usersessionrecord.Id = sessionmap.get(loginUser.id).id;
                //update usersessionrecord;
            //}
        }
    }
    
    /*
     * Salesforce and Okta log out method
     */    
    @AuraEnabled
    public static String logoutCustom() {
        String returnURL = '';
        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
        try{
            System.debug('****0'+sessionmap.size());
            //sessionmap = usersessionids__c.getall();
            //system.assert(false);
            for(usersessionids__c u :[Select name,session_id__c from usersessionids__c where name =: userinfo.getuserid()]) {
                
                
                sessionmap.put(u.name,u);
            }
            System.debug('****1'+sessionmap.size());
            HttpRequest reqgroup = new HttpRequest();
            HttpResponse resgroup = new HttpResponse();
            Http httpgroup = new Http();
            String strToken = Label.oktatoken;
            String authorizationHeader = 'SSWS ' + strToken;
            reqgroup.setHeader('Authorization', authorizationHeader);
            reqgroup.setHeader('Content-Type','application/json');
            reqgroup.setHeader('Accept','application/json');
            System.debug('****2'+sessionmap.size());
            
            //sendEmail(new List<String>{'amitkumar.katre@varian.com'},'in method',String.valueof(sessionmap.size()),'VMS Logout');
            
            if(sessionmap.get(userinfo.getuserid()) != null) {
                String endpointgroup = Label.oktaendpointsessionkill + sessionmap.get(userinfo.getuserid()).session_id__c;
                //String endpointgroup = Label.oktaendpointsessionkill + sessionmap.get('0034C000006w0nc').session_id__c;
                reqgroup.setEndPoint(endpointgroup);
                reqgroup.setMethod('DELETE');
                System.debug('****3'+sessionmap.size());
                //try {
                    if(!Test.IsRunningTest()){
                        resgroup = httpgroup.send(reqgroup);
                        //sendEmail(new List<String>{'amitkumar.katre@varian.com'},resgroup.getBody(),resgroup.getBody(),'VMS Logout');
                        if(resgroup.getStatusCode() == 200 && resgroup.getBody() != null) {
                            returnURL = '/varian';
                        }
                    }
                //} catch (System.CalloutException e){
                  //  system.debug('Error***'+resgroup.toString());
                //}
                system.debug('Error***'+resgroup.toString());
                System.debug('****3'+sessionmap.size());                
            }

            // TOTO --- this functionality will be done later
            //Freeze user account when user changes the email
            /*
            List<Contact_Updates__c> listConUpdEmail
                = [SELECT Id, Contact__c, New_Account__c, Old_Account__c,
                    Old_Email__c, New_Email__c
                    FROM Contact_Updates__c
                    WHERE Contact__c = :con.Id
                    AND New_Email__c != :con.Email
                    AND Old_Email__c = :con.Email
                    AND New_Account__c = :con.Account.Name
                    AND CreatedDate >= TODAY];
            */
            List<Contact> listFrzCon = [SELECT Id FROM Contact
                                WHERE Freeze_User__c = true 
                                and Updated_Email__c != null and Id = :con.Id];


            system.debug('#### debug con.Id in logout = ' + con.Id);
            //system.debug('#### debug con.Institute_Name__c in logout = ' + con.Institute_Name__c);
            system.debug('#### debug con.Account.Name in logout = ' + con.Account.Name);

            if(listFrzCon.size() > 0 ) {
                system.debug('#### debug freezing user');
                Set<Id> setIds = new Set<Id>();
                setIds.add(con.Id);
                freezeUser(setIds);     

                ////con.Email = listConUpdEmail[0].New_Email__c;
                //con.Inactive_Contact__c = true;
                //con.PasswordReset__c = false;
                //con.Is_Preferred_Language_selected__c = false;
                ///update con;   //// Need to confirm from business             
            }
            

            //De-Activate the contact record when user changes the Institute Name

            
            List<Contact_Updates__c> listConUpdIns
                = [SELECT Id, Contact__c, New_Account__c, Old_Account__c,
                    Old_Email__c, New_Email__c
                    FROM Contact_Updates__c
                    WHERE Contact__c = :con.Id
                    AND New_Account__c != :con.Account.Name
                    AND Old_Account__c = :con.Account.Name
                    AND New_Email__c = :con.Email
                    AND CreatedDate >= TODAY]; 

            /*
            List<Contact> listCon = [SELECT Id FROM Contact WHERE
                                        Institute_Name__c != null
                                        AND LastModifiedDate >= TODAY]; */

            system.debug('#### debug con.Id in logout = ' + con.Id);
            //system.debug('#### debug con.Institute_Name__c in logout = ' + con.Institute_Name__c);
            system.debug('#### debug con.Account.Name in logout = ' + con.Account.Name);

            if(listConUpdIns.size() > 0 || Test.isRunningTest()) {   
                system.debug('#### debug de-activating user');  
                String emailToTransfer = con.Email;
                System.debug('#### emailToTransfer = ' + emailToTransfer);

                ////con.Email = null;
                con.MyVarian_Member__c = false;
                con.PasswordReset__c = false;
                con.Inactive_Contact__c = true;
                con.Is_Preferred_Language_selected__c = false;   
                con.ShowAccPopUp__c = false;             
                if(!Test.isRunningTest()) {
                    update con;
                }  

                list<Account> acc = new list<Account>();
                acc = [select Id, Name, BillingPostalCode, BillingStreet, BillingState, BillingCity 
                        from Account
                        where Name =: con.Institute_Name__c
                        and BillingPostalCode =: con.MailingPostalCode];
                        //and BillingStreet =: registerDataObj.MailingStreet
                        //and BillingState =: registerDataObj.MailingState
                        //and BillingCity =: registerDataObj.MailingCity];   

                System.debug('#### debug con in MvHeaderComponentController = ' + con);

                 
                Id actId; 
                if(acc.size() >0){
                    ////registerDataObj.AccountId = acc[0].Id;   
                    actId = acc[0].Id; 
                }   

                Contact newCon = con.clone(false,false,false,false);
                system.debug('#### newCon after cloning = ' + newCon);
                newCon.Institute_Name__c = con.Institute_Name__c;
                newCon.AccountId = actId;

                newCon.Date_of_Factory_Tour_Visit__c = system.today();
                newCon.Registered_Portal_User__c = true;
                newCon.MyVarian_Registration_Date__c=Date.Today();
                newCon.OCSUGC_Contact_Source__c = 'MyVarian';
                newCon.email = emailToTransfer;
                newCon.OktaLogin__c = null;
                newCon.OktaId__c = null;
                newCon.HasOptedOutOfEmail = false;
                newCon.Email_Opt_in__c = false;
                newCon.Recovery_Answer__c = null;
                newCon.Recovery_Question__c = null;
                newCon.MvMyFavorites__c = null;
                //newCon.MyVarian_Member__c = true;
                newCon.Inactive_Contact__c = false;

                list<User> listMyVarianUser = [select id from user where name = 'MyVarian' LIMIT 1];
                if(listMyVarianUser.size() > 0) {
                    newCon.OwnerId = listMyVarianUser[0].Id;
                }

                system.debug('#### newCon before inserting = ' + newCon); 
                try {              
                    ////insert newCon;  

                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    ////Account duplicateAccount = new Account(Name='dupe'); 
                    Database.SaveResult sr = Database.insert(newCon, dml); 
                    if (sr.isSuccess()) {   
                        System.debug('Bypassed duplicate contact rule!'); 
                    }                       
                    system.debug('#### newCon after inserting = ' + newCon);    
                } catch (Exception e) {
                    system.debug('#### debug error found = ' + e.getMessage());
                }

                delete listConUpdIns;               
            }


            //returnURL = 'https://sfqa1-varian.cs77.force.com/servlet/networks/switch?startURL=%2Fsecur%2Flogout.jsp';
            returnURL = 'https://sfqa-varian.cs15.force.com/';
            return returnURL;
        }
        catch(Exception e) {
            returnURL = '/varian';
            //sendEmail(new List<String>{'amitkumar.katre@varian.com'},'Logout Failed',e.getMessage()+e.getStackTraceString(),'VMS Logout');
            return returnURL;
            
        }          
    }

    @future(Callout = false)
    public static void FreezeUser(set < Id > stContactIds) {
        List < UserLogin > LstuserLogins = new List < UserLogin > ();
        LstuserLogins = [Select id, isFrozen from UserLogin where UserId in(SELECT Id FROM User WHERE ContactId IN :stContactIds)];

        for (UserLogin varuser: LstuserLogins) {
            varuser.isFrozen  = true;
        }
        if (LstuserLogins.size() > 0) {
            Update LstuserLogins;
            System.debug('Working code 2*****');
        }


    }  
    
    /*
     * Helper send email to debug the code
     */
    public static void sendEmail(List<String> toAddresses,String subject,String mailBody,String senderName)  {  
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        mails.add(mail);  
        Messaging.sendEmail(mails);  
   }
    
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        system.debug(' ##### cacheMap2 ##### ' + MvUtility.getCustomLabelMap(languageObj));
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<picklistEntry> getPreferredLanguages(){
        List<picklistEntry> lstLanguages = new List<picklistEntry>();
        Schema.DescribeFieldResult fieldResult = Contact.Preferred_Language1__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            lstLanguages.add(new picklistEntry(f.getValue(),f.getLabel()));
        }
        
        //lstLanguages.sort();
        return lstLanguages;
    }
    @AuraEnabled
    public static Boolean savePreferredLanguage(string language){
        try{         
            User usr = [SELECT Id, Name, ContactId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            if(usr.ContactId !=null && string.isNotBlank(language)){
                //Preferred_Language_UI__c = language,
                Contact con = new Contact(Id=usr.ContactId,Preferred_Language1__c = language,Is_Preferred_Language_selected__c=true);
                update con;
                return true;
            }else{
               return false; 
            }
        }
        catch(Exception ex){
            return false;
            //return ex.getMessage();
        }
    }
    
    public class picklistEntry implements Comparable{
        @AuraEnabled public string pvalue{get;set;}
        @AuraEnabled public string label{get;set;}
        
        public picklistEntry(string pvalue,string label){
            this.pvalue = pvalue;
            this.label = label;            
        }
        
        public Integer compareTo(Object compareTo) {
            picklistEntry compareToOppy = (picklistEntry)compareTo;
            Integer returnValue = 0;
            if (label > compareToOppy.label)
                returnValue = 1;
            else if (label < compareToOppy.label)
                returnValue = -1;        
            return returnValue;       
        }
    }

    @AuraEnabled
    public static List<mvMyFavoritesWrapper> getMyRecQuestions(String languageObj)
    {
        map<String,String> labels = MvUtility.getCustomLabelMap(languageObj);
        map<String,String> labelsTransMap = new map<String,String>();
        String myFav = labels.get('MV_RecoveryQuestions');
        System.debug('MV_Favorites======='+myFav);
        if(!String.isBlank(myFav)) {
            for(String key : myFav.split('\\r\\n')){
                System.debug('kkkkkkkkk======='+key);
                labelsTransMap.put(key.split(':')[0],key.split(':')[1]);
            }
        }
        system.debug('labelsTest===='+labelsTransMap);
        List<mvMyFavoritesWrapper> optionMap = new List<mvMyFavoritesWrapper>();
        Schema.DescribeFieldResult fieldResult = Contact.MvMyRecoveryQuesions__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry f : ple){
            mvMyFavoritesWrapper myFavorite = new mvMyFavoritesWrapper(f.getLabel(),f.getValue());
            myFavorite.optionTranslatedLabel = labelsTransMap.get(f.getLabel());
            if(String.isNotBlank(myFavorite.optionTranslatedLabel) 
                && myFavorite.optionTranslatedLabel.contains(',')) {
                myFavorite.optionTranslatedLabel = myFavorite.optionTranslatedLabel.replace(',', '');
            }            
            optionMap.add(myFavorite);
        } 
        
            return optionMap;
            //String myFavoriteStr = contactList[0].MvMyFavorites__c.split();

    }    
    /* 
     * Reset first time password and security question
     */
    @AuraEnabled
    public static void resetpassword(String recoveryQuestion, String recoveryAnswer, String newPassword) {
        ////recoveryAnswer = recoveryAnswer.deleteWhitespace().toLowerCase();
        recoveryAnswer = recoveryAnswer.toLowerCase().trim();

        //system.debug('---RecoveryQuestion' +'----' + recoveryQuestion +'----Recovery Answer'+recoveryAnswer + 'Password---'+newPassword);
        User usercontact = [Select ContactId,Contact.Email,Contact.FirstName,
                            Contact.LastName,Contact.Phone,Contact.OktaLogin__c,Contact.OktaId__c 
                            from user where id=: Userinfo.getUserId() limit 1];
        //http Request creation and capturing response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        //Construct Authorization and Content header
        String strToken = label.oktatoken;// okta token is in custom label
        system.debug('strToken==='+strToken);
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //EndPoint Creation
        String EndPoint2 = label.oktachangepasswrdendpoint + usercontact.Contact.OktaId__c;
        req.setendpoint(EndPoint2);
        req.setMethod('PUT');
        String body ='{"profile": {"firstName": "'+ usercontact.Contact.FirstName +'","lastName": "'+usercontact.Contact.LastName +'","email": "'+ usercontact.Contact.Email + '","login": "' + usercontact.Contact.OktaLogin__c + '"},"credentials": {"password" : { "value": "' + NewPassword + '" },"recovery_question": {"question": "'+ Recoveryquestion +'","answer": "'+ RecoveryAnswer + '"}}}';
        req.setBody(body);
        try {
            //Send endpoint to OKTA
            system.debug('aebug ; ' +req.getBody());
            If(!Test.IsRunningTest()){
                res = http.send(req);
            }else {
                fakeresponse.fakeresponsemethod('activation');
            }
            system.debug('resposne==='+res.getbody());
            if(res.getbody().contains('errorSummary')){
                
                CustomExceptionData data = new CustomExceptionData('ResetPassError','Password provided doesnot match.', 405);
                throw new AuraHandledException(JSON.serialize(data)); 
            }
            system.debug('aebug ; ' +res.getBody());
         }catch(System.CalloutException e) {
            CustomExceptionData data = new CustomExceptionData('ResetPassError',res.toString(), 405);
            throw new AuraHandledException(JSON.serialize(data));    
        }
        // Updating Contact Record
        Contact updatecon = new Contact(id = usercontact.ContactId);
        updatecon.Recovery_Question__c = Recoveryquestion;
        updatecon.Recovery_Answer__c = RecoveryAnswer;
        system.debug('#### debug recoveryAnswer save = ' + RecoveryAnswer);
        updatecon.PasswordReset__c = true;
        updatecon.activation_url__c = '';
        updatecon.PasswordresetDate__c = system.today();// to capture the last date when the password was reseted
        try{
            update updatecon;
            system.debug('#### debug contact updated ');
        }catch(Exception e){
            CustomExceptionData data = new CustomExceptionData('ResetPassError', 'Please contact support for the above mentioned error', 405);
            throw new AuraHandledException(JSON.serialize(data));
            //e = new AuraHandledException(JSON.serialize(data));
            //e.setMessage('Invalid String');
            //throw e;
        }
    }

    @AuraEnabled
    public static List<String> getDynamicPicklistOptions(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Contact');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
        
        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* 
     * Reset first time password, security question, verify contact info and marketing communication info
     */
    @AuraEnabled
    public static Boolean saveMarCommData(Boolean emailOptIn, Boolean emailOptOut) {
        try {
            User usr;
            if(Test.isRunningTest()) {
                usr = [SELECT Id, Name, ContactId, Contact.Salutation, 
                            Contact.FirstName, Contact.LastName FROM User WHERE ContactId !=null LIMIT 1];                
            } else {
                usr = [SELECT Id, Name, ContactId, Contact.Salutation, 
                            Contact.FirstName, Contact.LastName FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            }

            if(usr.ContactId !=null){
                Contact con = new Contact(Id=usr.ContactId);
                if(emailOptIn) {
                    con.Email_Opt_in__c = true;
                    con.HasOptedOutOfEmail = false;
                    
                    con.Opted_In_By__c = usr.Contact.Salutation + ' '+ usr.Contact.FirstName +' '+ usr.Contact.LastName;
                    con.Opt_In_Date__c = System.today();
                    con.How_Opted_In__c = 'MyVarian';
                }
                
                if(emailOptOut) {
                    con.Email_Opt_in__c = false;
                    con.HasOptedOutOfEmail = true;
                    
                    con.Opted_In_By__c = '';
                    con.Opt_In_Date__c = null;
                    con.How_Opted_In__c = '';
                }

                update con;
                return true;
            } else {
               return false; 
            }
        } catch(Exception ex){
            return false;
            //return ex.getMessage();
        }
    }
}
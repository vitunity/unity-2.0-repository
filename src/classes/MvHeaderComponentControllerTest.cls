@isTest(seeAllData=false)
private class MvHeaderComponentControllerTest {
    
    @isTest static void getCurrentUserTest() {
        test.startTest();        
        MvHeaderComponentController.getLoggedInUser();
        test.stopTest();
    }

    @isTest static void isCurUserPTInstGrpTest() {
        test.startTest();        
        MvHeaderComponentController.isCurUserPTInstGrp();
        test.stopTest();
    }

    @isTest static void updateUserLMSMethodTest() {
        test.startTest();        
        MvHeaderComponentController.updateUserLMSMethod();
        test.stopTest();
    }    

    @isTest static void test1(){
        test.startTest();        
        MvHeaderComponentController.getContactInfo();
        MvHeaderComponentController.isMarketingUser();
        test.stopTest();
    }
    @isTest static void test2(){
        test.startTest();        
        try{
            MvHeaderComponentController.saveMyContact();
        }catch(exception e){}
        test.stopTest();
    }
    @isTest static void test3(){
        test.startTest();        
        MvHeaderComponentController.isLoggedIn();
        test.stopTest();
    }
    @isTest static void test4(){
        test.startTest();        
        MvHeaderComponentController.isPartnerChannelUsr();
        test.stopTest();
    }
    @isTest static void test5(){
        test.startTest();        
        MvHeaderComponentController.loginCommunity('testuser@test.com', 'Varian@99');
        test.stopTest();
    }
    @isTest static void test6(){
        test.startTest();    
        try{    
        MvHeaderComponentController.saveUserSessionId('4654@@@8383^^f8ddf');
        } catch (Exception e) {
            system.debug('error found');
        }
        test.stopTest();
    }
    @isTest static void test7(){
        usersessionids__c userSession = new usersessionids__c(Name = UserInfo.getUserId(),
                                                              session_id__c = '102M_HsjJ0cSJaIKcACnBGwNQ');
        insert userSession;
    
        test.startTest();  
            MvHeaderComponentController.logoutCustom();
        test.stopTest();
    }
    @isTest static void test8(){
        test.startTest();        
        MvHeaderComponentController.getCustomLabelMap('en');
        test.stopTest();
    }
    /*@isTest static void test9(){
        test.startTest();        
        MvHeaderComponentController.getCustomLabelMap('en');
        test.stopTest();
    }*/
    @isTest static void test10(){
        test.startTest();        
        MvHeaderComponentController.getPreferredLanguages();
        test.stopTest();
    }
    @isTest static void test11(){
        test.startTest();        
        MvHeaderComponentController.savePreferredLanguage('en');
        test.stopTest();
    }
    @isTest static void test12(){
        test.startTest();        
        try{
            MvHeaderComponentController.resetpassword('What is your birth place', 'test', 'Varian@88');
        }catch(exception e){
        }
        test.stopTest();
    }
    
    @isTest static void getDynamicPicklistOptionsTest() {
        test.startTest();
        try{        
        MvHeaderComponentController.getDynamicPicklistOptions('County__c');
        }catch(exception e){}
        test.stopTest();
    }
    @isTest static void test13(){
        list<String> emailAddressList = new list<string>();
        emailAddressList.add('test123@test.com');
        test.startTest();        
        MvHeaderComponentController.sendEmail(emailAddressList, 'test subject', 'test email body', 'test sender');
        test.stopTest();
    }

    @isTest static void testIsCurrentUserInSancCountries(){
        Sanctioned_Countries__c set1 = new Sanctioned_Countries__c(Name = 'India');
        insert set1;
        test.startTest();   

        MvHeaderComponentController.isCurrentUserInSancCountries();
        test.stopTest();
    }
    @isTest static void testIsCurrentUserInCaseApprovCountries(){
        Support_Case_Approved_Countries__c  set1 = new Support_Case_Approved_Countries__c(Name = 'India');
        insert set1;
        test.startTest();        
        MvHeaderComponentController.isCurrentUserInCaseApprovCountries();
        test.stopTest();
    }    
    @isTest static void testGetMyRecQuestions(){
        test.startTest();        
        MvHeaderComponentController.getMyRecQuestions('en');
        test.stopTest();
    }    

    @isTest static void testGetLanguagePicklistOptn(){
        test.startTest();        
        MvHeaderComponentController.getLanguagePicklistOptn('Account_Type__c');
        test.stopTest();
    }

    @isTest static void testSaveMarkComData(){
        test.startTest();

        Profile admin = OCSUGC_TestUtility.getAdminProfile();
        Profile portal = OCSUGC_TestUtility.getPortalProfile();
        List<User> lstUserInsert = new List<User>();
        User adminUser = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'SomeUser_001', null);
        //User adminUser = OCSUGC_TestUtility.createUser(false, admin.Id, '1');
        lstUserInsert.add(adminUser);
        Contact con4,con3;     
        Account accnt= OCSUGC_TestUtility.createAccount('Creative Hospital', true);

        con4 = OCSUGC_TestUtility.createContact(accnt.Id, false);     
        insert con4;

        User usr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, con4.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);
        //User usr = OCSUGC_TestUtility.createUser(false, portal.Id, con4.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);  
        lstUserInsert.add(usr);
        ////insert lstUserInsert;

        System.runAs(adminUser) {
            MvHeaderComponentController.saveMarCommData(true, false);
            MvHeaderComponentController.saveMarCommData(false, true);
        }

        test.stopTest();
    }

    @isTest static void testFreezeUser(){
        Contact con4;     
        Account accnt= OCSUGC_TestUtility.createAccount('Creative Hospital', true);

        con4 = OCSUGC_TestUtility.createContact(accnt.Id, false);     
        insert con4;

        Set<Id> conIds = new Set<Id>();
        conIds.add(con4.Id);

        test.startTest();        
            MvHeaderComponentController.FreezeUser(conIds);
        test.stopTest();
    }    
}
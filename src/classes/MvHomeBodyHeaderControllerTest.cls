@isTest(seeAllData=true)
private class MvHomeBodyHeaderControllerTest {

    public static Regulatory_Country__c regcount ;
    public static Contact_Role_Association__c cr;
    public static Profile pAdmin;
    public static User u;
    public static Contact con;
    public static Account a, a1;
    public static Customer_Agreements__c agr;
    public static Event_Webinar__c Webinar;

    public static Account account1;
    public static Contact contact1;
    public static User newUser;

    public static User thisUser;

    public static UserRole rol;

    static
    {
        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];

        pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        rol = [select id from UserRole LIMIT 1];           

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

        a = new Account(name='Amit29990099'+System.now(),BillingPostalCode ='945ssss30',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyxss',Country__c='USA', RecordTypeId = rtAct.Id); 
        insert a;  
        a1 = new Account(name='test29990099'+System.now(),BillingPostalCode ='945ssss30',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyxss',Country__c='USA', RecordTypeId = rtAct.Id); 
        insert a1; 
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test123@1234APR.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        con.Preferred_Language1__c = 'en';
        //Creating a running user with System Admin profile
        insert con;

        System.runAs(thisUser) {
            newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                username='test_user@testclass.com', isActive = true, ContactId = con.Id);     //, UserRoleid = rol.Id); 
            insert newUser;
       }

        regcount = new Regulatory_Country__c();
        regcount.Name = 'Test';
        regcount.RA_Region__c ='NA';
        regcount.Portal_Regions__c ='N.America,Test';
        insert regcount;

        cr = new Contact_Role_Association__c();
        cr.Account__c= a.Id;
        cr.Contact__c = newUser.Contactid;
        cr.Role__c = 'RAQA';
        insert cr;

        agr = new Customer_Agreements__c(Affected_Account__c = a.Id, 
                                        ///fix this ////  Name = 'Test Agreement',
                                        Agreed_By__c = con.Id,
                                        Agreement_Date__c = System.Now());
        insert agr;

        RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
        Webinar = new Event_Webinar__c();
        Webinar.RecordTypeId = RT.Id; 
        Webinar.Title__c = 'Test Webinar';
        Webinar.Product_Affiliation__c = '4DITC';
        Webinar.Location__c = 'NY';
        Webinar.To_Date__c = system.today() + 20;
        Webinar.From_Date__c = system.today();
        Webinar.Needs_MyVarian_registration__c=False;
        Webinar.Languages__c = 'English(en)';
        insert Webinar;        

    }


    @isTest static void isLoggedInTest() {
        Test.startTest();
            MvHomeBodyHeaderController.isLoggedIn();
        Test.stopTest();
    }
    
    @isTest static void getCustomLabelMapTest() {
        Test.startTest();
            MvHomeBodyHeaderController.getCustomLabelMap('EN');
        Test.stopTest();
    }

    @isTest static void saveUserSessionIdTest() {
        Test.startTest();
            MvHomeBodyHeaderController.saveUserSessionId();
        Test.stopTest();
    }

    @isTest static void loginCommunityTest() {
        Test.startTest();
            try {
                MvHomeBodyHeaderController.loginCommunity('testUser', 'testPwd', 'English', 'www.yahoo.com');
                MvHomeBodyHeaderController.loginCommunityWithApp('testUser', 'testPwd', 'English', 'www.yahoo.com',Label.MvOktaLoginUrl); 
            } catch (Exception e) {

            }
        Test.stopTest();
    }
    
    @isTest static void loginCommunityTest2() {
        Test.startTest();
            try {
                MvHomeBodyHeaderController.loginCommunityWithApp('testUser', 'testPwd', 'English', 'www.yahoo.com',Label.MvOktaLoginUrl); 
            } catch (Exception e) {

            }
        Test.stopTest();
    }

    @isTest static void getOnDemandWebinarsTest() {
        Test.startTest();
            MvHomeBodyHeaderController.getOnDemandWebinars();

            Webinar.From_Date__c = System.Today() + 10;
            update Webinar;
            MvHomeBodyHeaderController.getOnDemandWebinars(); 

            Webinar.From_Date__c = System.Today() - 15;
            update Webinar;
            MvHomeBodyHeaderController.getOnDemandWebinars(); 

        Test.stopTest();
    }

    @isTest static void getFeaturedStoriesTest() {
        Test.startTest();
            try {
                MvHomeBodyHeaderController.getFeaturedStories();
            } catch (Exception e) {

            }
        Test.stopTest();
    }

    @isTest static void getRSSFeedTest() {
        Test.startTest();
            try {
                MvHomeBodyHeaderController.getRSSFeed('www.yahoo.com');
            } catch (Exception e) {
                
            }
        Test.stopTest();
    }

    @isTest static void showContactTest() {
        Test.startTest();
            MvHomeBodyHeaderController.showContact('test@test.com');
        Test.stopTest();
    }   

    @isTest static void getDynamicPicklistOptionsTest() {
        Test.startTest();
            MvHomeBodyHeaderController.getDynamicPicklistOptions('County__c');
        Test.stopTest();
    }   

    @isTest static void getDynamicPicklistOptionsForCountryTest() {
        Test.startTest();
            MvHomeBodyHeaderController.getDynamicPicklistOptionsForCountry('Industry');
        Test.stopTest();
    }           

//TODO
    @isTest static void registerMDataTest() {
        Test.startTest();
            //try {
                //delete con;
                Contact con1 = new Contact(FirstName = 'TestContact22', Preferred_Language1__c = 'en', LastName = 'TestContact22', Email = 'Test22@1234APR.com', Institute_Name__c = 'test29990099',
                    MvMyFavorites__c='Events', AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
                    MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());    

                MvHomeBodyHeaderController.registerMData(con1, a);
                MvHomeBodyHeaderController.isUserFreezedMethod('Test22@1234APR.com');

                Contact con2 = new Contact(FirstName = 'TestContact22', Preferred_Language1__c = 'en', LastName = 'TestContact22', Email = 'Test22@1234APR.com', Institute_Name__c = 'test29990099',
                    MvMyFavorites__c='Events', AccountId = a1.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
                    MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());

                MvHomeBodyHeaderController.registerMData(con2, a1);

                Contact con3 = new Contact(FirstName = 'TestContact22', Preferred_Language1__c = 'en', LastName = 'TestContact22', Email = 'Test22@1234APR.com', Institute_Name__c = 'test29990099',
                    MvMyFavorites__c='Events', AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
                    MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today()); 

                MvHomeBodyHeaderController.registerMData(con3, a);
                        
            //} catch (Exception e) {

            //}
        Test.stopTest();
    }

    @isTest static void sendNotificationTest1() {
        Test.startTest();
            MvHomeBodyHeaderController.sendNotification(con, a, true);
            MvHomeBodyHeaderController.sendNotification(con, a, false);
    
        Test.stopTest();
    }       

    @isTest static void sendNotificationTest2() {

        Recordtype  rt = [Select id from recordtype where developername = 'Site_Partner'];   

        Test.startTest();
        Account act2 = new Account(Name = 'test29990099222', RecordTypeId = rt.Id, BillingPostalCode = '94532', 
                    BillingCity='Fremont', BillingState = 'CA', BillingCountry='USA', BillingStreet = 'abc', Country__c='USA');
        insert act2;

        Contact con3 = new Contact(FirstName = 'TestContact2', LastName = 'TestContact2', Email = 'TestUnKnown@1234APR.com', Institute_Name__c = 'test29990099222',
            MvMyFavorites__c='Events', AccountId = act2.Id, MailingCity='Fremont', MailingCountry='Canada', MailingPostalCode='94532', MailingState='CA', Phone = '999999',
            MailingStreet = 'abc', Account_admin__C = true);    
        //insert con3;

        
            try {
                //MvHomeBodyHeaderController.sendNotification(con3, act2, true);
                MvHomeBodyHeaderController.sendNotification(con3, act2, false);    
            } catch (Exception e) {

            }       
        Test.stopTest();
    }   

    @isTest static void sendNotificationTest3() {

        Recordtype  rt = [Select id from recordtype where developername = 'Site_Partner'];   
        Test.startTest();
        Account act2 = new Account(Name = 'test29990099222', RecordTypeId = rt.Id, BillingPostalCode = '94532', 
                    BillingCity='Fremont', BillingState = 'CA', BillingCountry='USA', BillingStreet = 'abc', Country__c='USA');
        insert act2;

        Contact con3 = new Contact(FirstName = 'TestContact2', LastName = 'TestContact2', Email = 'TestUnKnown@1234APR.com', Institute_Name__c = 'test29990099222',
            MvMyFavorites__c='Events', AccountId = act2.Id, MailingCity='Fremont', MailingCountry='Canada', MailingPostalCode='94532', MailingState='CA', Phone = '999999',
            MailingStreet = 'abc', Account_admin__C = true);    
        //insert con3;

        
            try {
                //MvHomeBodyHeaderController.sendNotification(con3, act2, true);
                MvHomeBodyHeaderController.sendNotification(con3, act2, false);    
            } catch (Exception e) {

            }       
        Test.stopTest();
    }       

    @isTest static void getRecovQuestionTest() {
        Test.startTest();
            String strTest = MvHomeBodyHeaderController.getRecovQuestion('standarduser@testorg.com', 'Testing');
        Test.stopTest();
    }   

    @isTest static void changePasswordTest() {
        Test.startTest();
            MvHomeBodyHeaderController.changePassword('secEmailUpd@update.com', 'lastNameupd', 'answer', 'newPwd');
        Test.stopTest();
    }   

    @isTest static void getextractrecqTest() {  
        Test.startTest();
            MvHomeBodyHeaderController.getextractrecq('Test123@1234APR.com', 'Testing', 'answer', 'NewPwd123');
        Test.stopTest();
    }

    @isTest static void FeaturedStoryTest() {
        Test.startTest();
            MvHomeBodyHeaderController.FeaturedStory ft1 = new MvHomeBodyHeaderController.FeaturedStory();
            MvHomeBodyHeaderController.FeaturedStory ft2 = new MvHomeBodyHeaderController.FeaturedStory('test1', 'test2', 'test3', System.Today());
        Test.stopTest();
    }
    
    @isTest static void FeaturedStoryTest2() {
        Test.startTest();
            MvHomeBodyHeaderController.FeaturedStory ft2 = new MvHomeBodyHeaderController.FeaturedStory('test1', 'test2', 'test3', System.Today());
        Test.stopTest();
    }

    @isTest static void additionalTest() {
        Test.startTest();
            MvHomeBodyHeaderController.getContactInfo();
            MvHomeBodyHeaderController.getPartnerContactInfo('testEmail@testemail.com');

            String test1 = MvHomeBodyHeaderController.RecvryAns;
            String test2 = MvHomeBodyHeaderController.forcaptcha;
        Test.stopTest();
    }   
}
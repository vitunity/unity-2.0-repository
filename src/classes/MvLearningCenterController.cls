/*
* Author: Harleen Gulati
* Created Date: 21-Aug-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This class is used for my learning center lightening page.
*/
public without sharing class MvLearningCenterController {
     /* Check for logged in user */
    @AuraEnabled
    public static Boolean getCurrentUser() {
        boolean externalUser;
        User u = [SELECT Id, LastName, Email, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            externalUser = false;
        }else{
            externalUser = true;
        }
        return externalUser;
    }
    
    /* load custom labels */
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);    
    }
    
    /* get country picklist values */
    @AuraEnabled
    public static List<String> getDynamicPicklistOptions(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
        
        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* register data and send email to user with attachment */    
    @AuraEnabled
    public static String registerMData(Contact registerDataObj, Account accDataObj, String subject, 
                                            String longDes,String fileName, String base64Data, 
                                            String contentType, String languageObj){
        
        //Code to Send email to user and admin     
        List<String> toAddresses = new List<String>();
        List<ProdCountEmailMapping__c > emailmap = new List<ProdCountEmailMapping__c>();
        List<String> touser = new List<String>();
        Datetime myDT = Datetime.now();
        String myDate = myDT.format('MM/dd/yyyy HH:mm:ss');
        String EmailBody;
        string reqtype;
        string Request = 'Website Feedback';
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        
        //EmailTemplate tempID = [SELECT Id, HTMLValue,Name, Body, DeveloperName  FROM EmailTemplate WHERE DeveloperName  = 'MvWebsiteFeedBack_en'];
        //String bd = tempID.HTMLValue;
        
        //string fstName = System.Label.First_Name;
        //bd = bd.replace(fstName , registerDataObj.Firstname);
        
        list<Contact> contactList = new list<Contact>();
        contactList = [select Id, OktaId__c, Name, Monte_Carlo_Registered__c, FirstName, LastName,Institute_Name__c , Account.name, Email, Salutation, Functional_Role__c, Specialty__c, PhotoUrl, MvMyFavorites__c, 
                                     Preferred_Language1__c, Fax, Phone, Manager__c, Manager_s_Email__c, Account.AccountNumber, Account.BillingStreet, Account.BillingCity, Account.BillingCountry, Account.BillingState, Account.BillingPostalCode,
                                     Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity, ShowAccPopUp__c
                                     from contact where id in (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId())];
        
        system.debug('==contactList=='+contactList );
         if(contactList.size()>0){
            system.debug('==Test==');
            registerDataObj.FirstName            =   contactList[0].FirstName;
            registerDataObj.LastName             =   contactList[0].LastName;
            registerDataObj.Institute_Name__c    =   contactList[0].Institute_Name__c;
            registerDataObj.Phone                =   contactList[0].Phone;
            registerDataObj.Email                =   contactList[0].Email;
            registerDataObj.MailingStreet        =   contactList[0].MailingStreet;
            registerDataObj.MailingState         =   contactList[0].MailingState;
            registerDataObj.MailingCity          =   contactList[0].MailingCity;
            registerDataObj.MailingCountry       =   contactList[0].MailingCountry;
            registerDataObj.MailingPostalcode    =   contactList[0].MailingPostalcode;
        
        }
        touser.add(registerDataObj.Email);
        //system.debug('==contactList[0].Institute_Name__c=='+contactList[0].Institute_Name__c);
        
        emailmap=[Select e.email__c From ProdCountEmailMapping__c e where e.Name=: Request limit 1];
        if(emailmap.size() >0)
        {
            toAddresses.add(emailmap[0].email__c);
        } 
        
        reqtype='Learning Centre';
        EmailBody='Thank you for your Varian Learning Center inquiry.<br><br>Please allow us to research your question.  It is our goal to resolve your issue within one business day. We will contact you if we need more information.<br><br>Thank you for using MyVarian.<br><br>With kind regards,<br><br>Varian Medical Systems';
        //EmailBody=bd;
        //TODO-- Do read the language value dynamically and set it here
        
        map<String,String> cachedMap = MvUtility.getCustomLabelMap(languageObj);
        system.debug('==cachedMap=='+cachedMap.keyset());  
        system.debug('==languageObj=='+languageObj);
        
        if(Request != 'PaperDocumentation' )
        {
            EmailBody= EmailBody+ '<br><br>'+cachedMap.get('Mv_Mail_Template_Submitted_On')+': '+ myDate+ '<br>'+cachedMap.get('Mv_Mail_Template_Name')+': '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get('Mv_Mail_Template_Institution')+': '+registerDataObj.Institute_Name__c+'<br>'+cachedMap.get('Mv_Mail_Template_Telephone')+': '+registerDataObj.Phone+'<br><br>'+cachedMap.get('Mv_Mail_Template_Email')+': '+registerDataObj.Email ;
            EmailBody=EmailBody+'<br><br>'+cachedMap.get('Mv_Mail_Template_Address')+': '+registerDataObj.MailingStreet+'<br>'+cachedMap.get('Mv_Mail_Template_City')+': '+registerDataObj.MailingCity+'<br>'+cachedMap.get('Mv_Mail_Template_State')+': '+ registerDataObj.MailingState+'<br>'+cachedMap.get('Mv_Mail_Template_Country')+': '+registerDataObj.MailingCountry+'<br><br>'+cachedMap.get('Mv_Mail_Template_Request_type')+': ' +reqtype+ '<br><br>'+cachedMap.get('Mv_Mail_Template_Subject')+': '+subject+'<br><br>'+longDes;
        }
        
        Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
        MailTicket.setSubject(reqtype +' successfully delivered ');
        MailTicket.setHTMLBody(EmailBody);
        MailTicket.setToAddresses(touser);
        
        // Use Organization Wide Address  
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress])
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress))
                MailTicket.setOrgWideEmailAddressId(owa.id);
        } 
        MailTicket.setReplyTo(System.Label.MVsupport);  
        MailTicket.setSaveAsActivity (false);
        
        String body ='';
        
        
        if(Request != 'PaperDocumentation' )
        {
            body ='<br><br>'+cachedMap.get('Mv_Mail_Template_Submitted_On')+': '+myDate+ '<br>'+cachedMap.get('Mv_Mail_Template_Name')+': '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get('Mv_Mail_Template_Institution')+': '+registerDataObj.Institute_Name__c+'<br>'+cachedMap.get('Mv_Mail_Template_Telephone')+': '+registerDataObj.Phone+'<br><br>'+cachedMap.get('Mv_Mail_Template_Email')+': '+registerDataObj.Email ;
            body = body +'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Address)+': '+registerDataObj.MailingStreet+'<br>'+cachedMap.get('Mv_Mail_Template_City')+': '+registerDataObj.MailingCity+'<br>'+cachedMap.get('Mv_Mail_Template_State')+': '+registerDataObj.MailingState+'<br>'+cachedMap.get('Mv_Mail_Template_Country')+': '+registerDataObj.MailingCountry+'<br><br>'+cachedMap.get('Mv_Mail_Template_Request_type')+': '+ reqtype+'<br>'+cachedMap.get('Mv_Mail_Template_Subject')+': '+subject+'<br>'+longDes;
            body = body +'<br><br>{'+cachedMap.get('MV_Mail_Template_USER_MAIL')+' :['+registerDataObj.Email+']}';
        }
        
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        mailAdmin.setSubject(reqtype);
        mailAdmin.setHTMLBody(body);
        mailAdmin.setToAddresses(toAddresses);
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]){ 
            if(owa.Address.contains(system.label.CpOrgWideAddress)) {
                mailAdmin.setOrgWideEmailAddressId(owa.id);
            }
        }
        
        if(base64Data != null && fileName != null)
        {
            blob b = EncodingUtil.base64Decode(base64Data);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileName);
            efa.setBody(b);
            
            base64Data = null;
            MailTicket.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            mailAdmin.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        } 
        
        mailAdmin.setTreatTargetObjectAsRecipient(false); 
        mailAdmin.setReplyTo(System.Label.MVsupport);     
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
        if(emailmap.size() >0)
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin }); 
        }              
        return 'accessdocs';
    }
}
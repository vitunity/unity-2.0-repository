@isTest
private class MvLearningCenterControllerTest {
	
	@isTest static void getCurrentUserTest() {
        test.startTest();        
        MvLearningCenterController.getCurrentUser();
        test.stopTest();
	}
	
	@isTest static void getCustomLabelMapTest() {
        test.startTest();        
        MvLearningCenterController.getCustomLabelMap('en');
        test.stopTest();
	}

	@isTest static void getDynamicPicklistOptionsTest() {
        test.startTest();        
        MvLearningCenterController.getDynamicPicklistOptions('Industry');
        test.stopTest();
	}	

	@isTest static void registerMDataTest() {
		test.startTest();
	        User thisUser = [ select Id from User where ContactId != null and isActive = true Limit 1];		// = :UserInfo.getUserId() ];
	        System.runAs ( thisUser ) {

	        	Regulatory_Country__c country1 = new Regulatory_Country__c(Name = 'USA');
	        	insert country1;
	        
	            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
	            User u;
	            Account a;   
	            
	            a = new Account( Territories1__c = 'Test Terrotory', name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='United States'  ); 
	            insert a;  
	            
	            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id, 
	            			MailingCountry ='USA', MailingState='CA' ); 
	            insert con;

	            MvLearningCenterController.registerMData(con, a, 'subject', 'desc', 'fileName', 'base64', 'Manual', 'ENU');
	        }
        test.stopTest();		
	}

}
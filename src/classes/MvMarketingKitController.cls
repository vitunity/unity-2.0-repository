public without sharing class MvMarketingKitController {
    
    private static List<MarketingKitRole__c> lstMarketingKits{get; set;}
    private static List<Product_Roles__c> lstMarktKitInternalUser{get;set;} // for internal user
    
    private static List<User> portalUser;
    private static Boolean isMarketYourCenter{get;set;}
    private static Boolean isInternalUser{get;set;}
    private static Boolean termsAccepted{get;set;}
    private static Boolean mrktPlsTermsAccepted{get;set;}
    private static Boolean internalUsr{get;set;}
    private static Boolean reloadPopup{get;set;}
    private static String reload{get;set;}
    private static String messages{get;set;}
    private static List<Contact> lstCont = new List<Contact>();
    private static String marktProg{get;set;}
    //public Map<String, List<ContentVersion>> lstMarktKitDocs{get;set;}
    private static List<MarkDocs> lstMarktKitDocs{get;set;}
    private static String surveyURL{get; set;}
    private static String userIdFromSurvey{get;set;}
    private static map<String,string> mapMarketKitAgrmnt = new map<String,string>();
    private static string shows{get;set;}
    private static string usrName{get;set;}
    
    // MarkDocs Wrapper class. List of docs records to show in lightning.
    public class MarkDocs implements Comparable{
        @AuraEnabled public String header{get;set;}
        @AuraEnabled public String headerCount{get;set;}
        @AuraEnabled public List<SubDocsClass> content{get; set;}
        public MarkDocs(){
            content = new List<SubDocsClass>();
        }
        public Integer compareTo(Object objToCompare) {
            return header.compareTo(((MarkDocs)objToCompare).header);
        }
    }
    // Documents Wrapper class.List of image records to show in lightning. 
    public class Documents implements Comparable{
        @AuraEnabled public String image{get; set;}
        @AuraEnabled public String imgHeight{get;set;}
        @AuraEnabled public String imgWidth{get;set;}
        @AuraEnabled public String thumbnail{get;set;}
        @AuraEnabled public String videoLink{get;set;} 
        @AuraEnabled public ContentVersion relContent{get; set;}
        
        public Integer compareTo(Object objToCompare) {
            return relContent.title.compareTo(((Documents)objToCompare).relContent.title);
        }
    }
    // Sub Documents Wrapper class.
    public class SubDocsClass implements Comparable{
        @AuraEnabled public String subGroupHeader{get;set;}
        @AuraEnabled public List<Documents> subGroupList{get;set;}
        public SubDocsClass(){
            subGroupList = new list<Documents>();
        }
        public Integer compareTo(Object objToCompare) {
            return subGroupHeader.compareTo(((SubDocsClass)objToCompare).subGroupHeader);
        }
    }
    
    @AuraEnabled
    public static list<MyVarian_Agreement_Translation__c> getMyVarianAgreement(string name, String languageCode){
        if(String.isBlank(languageCode) ){
            languageCode = 'en_US';
        }
        system.debug('name===='+name);
        system.debug('languageCode===='+languageCode);
        return [select Agreement_Text__c, Language__c 
                from MyVarian_Agreement_Translation__c
                where MyVarian_Agreement__r.Name =: name
                and Language__c =: languageCode
               order by Name ];
    }
    
    // Wrapper class to send output to marketingkit lightning page
    public class MarketingKits{
        @AuraEnabled public List<MarketingKitRole__c> lstMarketingKit{get; set;}
        @AuraEnabled public List<Product_Roles__c> lstMarketingKitInternalUser{get;set;} // for internal user
        @AuraEnabled public Boolean mkTermsAccepted{get;set;}
        @AuraEnabled public String mkSurveyURL{get;set;}
    }
    // Wrapper class to send output to marketingkit child lightning page
    public class MarkProgDocs{
        @AuraEnabled public List<MarkDocs> lstMarkProgDocs{get; set;}
        @AuraEnabled public Boolean mkTermsAccepted{get;set;}
    }
    //To get Custom labels
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        /*map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        return customLabelMap;*/
        return MvUtility.getCustomLabelMap(languageObj);
    }
   /* 
    @AuraEnabled
    public static MarketingKits getMarketingKits(){
        return getMarketingKits('','');
    }*/
    
    // Method to get Marketig kit list data. This will used i Marketing kit first page.
    @AuraEnabled    
    public static MarketingKits getMarketingKits(){
        //public static MarketingKits getMarketingKits(string Prog, string userIdFromSurvey){
        MarketingKits objMarketingKit = new MarketingKits();
        
        // call this method to set intial data. Just like a construtor in old class.
        setMvMarketingKitController('','');
        //setMvMarketingKitController(Prog,userIdFromSurvey);
        
        lstMarketingKits = new List<MarketingKitRole__c>();
        
        Set<MarketingKitRole__c> setMarketKit = new Set<MarketingKitRole__c>();
        
        portalUser = [Select u.Id, u.ContactId,u.alias, u.AccountId From User u where id =: UserInfo.getUserId()  limit 1];
        
        Id accountId = [Select u.Id, u.ContactId, u.AccountId From User u where id =: UserInfo.getUserId()  limit 1].AccountId;
        // MarketingKitRole__c tempMarketKitRole = new MarketingKitRole__c();
        if(portalUser.size() > 0){
            Map<Integer,Product_Roles__c> mapOrderMarket = new Map<Integer,Product_Roles__c>();
            for(Product_Roles__c pr : lstMarktKitInternalUser){
                mapOrderMarket.put(Integer.valueOf(pr.marketingkitorder__c),pr);
                system.debug('@@MapProductRoles--' + mapOrderMarket);
            }
            integer j = 0;
            for(integer i= 1; i <= mapOrderMarket.size(); i++){
                for(MarketingKitRole__c mr : [Select m.Product__c, m.Name, m.Id, m.Account__c,  m.Product__r.Name, m.Product__r.MkStylesheet__c
                                              From MarketingKitRole__c m where Account__c =: accountId  and Product__c != null  
                                              and Product__r.Model_Part_Number__c != :system.label.PartNumber order by m.Product__r.Name asc]){
                                                  
                                                  if(mapOrderMarket.get(i).Product_Name__c.toLowerCase() == mr.Product__r.Name.trim().toLowerCase())
                                                  {
                                                      system.debug('inside if');
                                                      //lstMarketingKits.set(0,mr);
                                                      mr.Product__r.MkStylesheet__c = '/varian/resource/VarianCss/VarianCss/MyVarian_files/images/'+mr.Product__r.MkStylesheet__c+'.jpg';
                                                      lstMarketingKits.add(mr);
                                                      //setMarketKit.set(0,mr);
                                                      j++;
                                                      break;
                                                  }
                                              }
            } 
            
            system.debug('@@@@@ lstMarketingKits = ' + lstMarketingKits);
            if(shows == 'none'){
                // Non Internal user Marketing data
                objMarketingKit.lstMarketingKit = lstMarketingKits;
            }
        }
        
        if(shows == 'block'){
            // Internal user Marketing data
            objMarketingKit.lstMarketingKitInternalUser = lstMarktKitInternalUser;  
        }
        objMarketingKit.mkTermsAccepted = termsAccepted;
        objMarketingKit.mkSurveyURL = surveyURL;
        // Return the Marketing kit data in below wrapper class object format 
        return objMarketingKit;
        //logic ends
    }
    
    // Method to intialize data for both pages. it will exeute constructor logic of old class.
    private static void setMvMarketingKitController(string Prog, string userIdFromSurvey){
        isMarketYourCenter = false;
        isInternalUser = false;
        
        reloadPopup=false;
        messages='none';
        //marktProg = Apexpages.currentPage().getParameters().get('prog');
        //userIdFromSurvey = Apexpages.currentPage().getParameters().get('c'); 
        
        // Get the Url parameters values as method parameters.
        marktProg = Prog;
        userIdFromSurvey = userIdFromSurvey;
        
        lstMarktKitInternalUser = new list<Product_Roles__c>();
        
        termsAccepted = false;
        system.debug('------User Id return---' + userIdFromSurvey);
        System.debug('@@@@ marktProg  '+marktProg);
        shows = 'none'; 
        portalUser = [Select u.Id, u.ContactId,u.alias, u.AccountId From User u where id =: UserInfo.getUserId() limit 1];
        if(portalUser[0].contactid == null)
        {
            shows = 'block';
            usrName = portalUser[0].alias;
            termsAccepted = true;
        }
        lstMarktKitInternalUser = [Select Product_Name__c,ismarketingKit__c,MkStylesheet__c,marketingkitorder__c from Product_Roles__c where ismarketingKit__c = true order by marketingkitorder__c];
        system.debug('@@@@lstMarktKitInternalUser**'+lstMarktKitInternalUser);
        
        for(Product_Roles__c  p : lstMarktKitInternalUser){
			p.MkStylesheet__c = '/varian/resource/VarianCss/VarianCss/MyVarian_files/images/'+p.MkStylesheet__c+'.jpg';
        }
        
        lstCont = [select id, MarketingKit_Agreement__c from Contact where id =: portalUser[0].ContactId limit 1];
        for(Product_Roles__c pr : Product_Roles__c.getall().values()){
            if(pr.Survey_URL__c == null){continue;}
            if(pr.Product_Name__c == 'Premier Marketing Program'){
                surveyURL = pr.Survey_URL__c+'?c='+Userinfo.getUserId();
            }
        }
        
        for(Product_Roles__c pr : Product_Roles__c.getall().values()){
            if(pr.ismarketingKit__c){
                mapMarketKitAgrmnt.put(pr.Product_Name__c,pr.Name);
            }
            system.debug('@@@@@@@' + pr.Product_Name__c + '---' + pr.Name);
            if(pr.Survey_URL__c == null){continue;}
            if(marktProg == pr.Product_Name__c){
                surveyURL = pr.Survey_URL__c+'?c='+Userinfo.getUserId();
            }
        }
        
        if(portalUser.size() > 0 && portalUser[0].ContactId != null &&  marktProg != null){
            String pcklst = '';
            pcklst = mapMarketKitAgrmnt.get(marktProg);
            //system.debug('mapMarketKitAgrmnt-------------->'+mapMarketKitAgrmnt.get('RapidArc Online Marketing Program'));
            if(pcklst != null){
                if(lstCont.size() > 0 && lstCont[0].MarketingKit_Agreement__c !=null 
                   && lstCont[0].MarketingKit_Agreement__c.contains(pcklst)){
                       termsAccepted = true;
                   }else{
                       termsAccepted = false;
                   }
            }
        }
        
        if(portalUser.size() > 0 && portalUser[0].ContactId != null){
            mrktPlsTermsAccepted = false;
            if(lstCont.size() > 0 && lstCont[0].MarketingKit_Agreement__c !=null 
               &&(lstCont[0].MarketingKit_Agreement__c.contains('Edge Marketing Program') 
                  || lstCont[0].MarketingKit_Agreement__c.contains('TrueBeam Marketing Program'))){
                      mrktPlsTermsAccepted = true;
                  }
        }
        
        internalUsr = false;
        if(portalUser[0].contactid == null){
            internalUsr = true;
        }
        
        if(lstCont.size()>0 && lstCont[0].MarketingKit_Agreement__c !=null){
            if(lstCont[0].MarketingKit_Agreement__c.contains('Premier Marketing Program'))
            {
                if(termsAccepted == false && marktProg == null)
                {
                    termsAccepted = true;
                }
            }
        }
        
        // fetchMarketingKit();        
    }
    
    // Method to get Marketig kit docs list data. This will used i Marketing kit second page.
    @AuraEnabled
    public static MarkProgDocs getMarkProgDocs(string marktProg, string userIdFromSurvey){
        // To return the output. 
        MarkProgDocs objMarkProgDocs = new MarkProgDocs();
        // List to add to output "objMarkProgDocs"
        lstMarktKitDocs = new List<MarkDocs>();
        
        // marktProg value null check
        if(String.isNotBlank(marktProg)){
            setMvMarketingKitController(marktProg,userIdFromSurvey);
            
            Map<String, List<Documents>> docsMap = new Map<String, List<Documents>>();
            Map<String,Map<String,List<Documents>>> subDocsMap = new Map<String,Map<String,List<Documents>>>(); // map for subgrouping
            //Set<String> groups = new Set<String>();
            
            Schema.Describefieldresult fieldResult = ContentVersion.Group__c.getDescribe();
            List<Schema.Picklistentry> lstPicklistvalues = fieldResult.getPicklistvalues();
            System.debug('lstPicklistvalues = '+lstPicklistvalues);            
            for(Schema.Picklistentry pe : lstPicklistvalues){
                docsMap.put(pe.getvalue(),new list<Documents>());
            }       
            System.debug('**docsMap = '+docsMap);            
            Set<Id> conDocIds = new set<Id>();
            Set<Id> conDocWorkspaceIds = new set<Id>();
            for(ContentVersion cv : [Select c.Id,c.RecordType.Name,c.ContentDocumentId From ContentVersion c where RecordType.Name = 'Marketing Kit' and IsLatest = true]){
                conDocIds.add(cv.ContentDocumentId);
            }
            system.debug('**conDocIds '+conDocIds);
            system.debug('**marktProg '+marktProg);
            
            for(String st : mapMarketKitAgrmnt.values()) {
                system.debug('**mapMarketKitAgrmnt st '+st);
            }
            system.debug('**mapMarketKitAgrmnt.get(marktProg) '+mapMarketKitAgrmnt.get(marktProg));
            
            for(ContentWorkspaceDoc cw: [Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc where ContentDocumentId in:conDocIds and ContentWorkspace.name  =: mapMarketKitAgrmnt.get(marktProg)]){
                conDocWorkspaceIds.add(cw.ContentDocumentId);
            }
            system.debug('**conDocWorkspaceIds '+conDocWorkspaceIds);
            
            //marktProg variable holding the product name
            for(ContentVersion cv : [Select c.FileType ,c.RecordType.Name, c.RecordTypeId, c.Id, c.MarketingKit_Doc_Image__r.Image__c,
                                     c.MarketingKit_Doc_Image__r.Thumbnail_Images__c,c.MarketingKit_Doc_Image__c,c.Title,c.ContentSize,
                                     c.Description, c.Group__c, c.Subgroup__c, c.ContentDocumentId 
                                     From ContentVersion c 
                                     where RecordType.Name = 'Marketing Kit' and  ContentDocumentId in: conDocWorkspaceIds 
                                     and islatest = true  order by Group__c]){
                                         system.debug('ContentVersion cv for loop');
                                         system.debug('docsMap for loop '+docsMap);
                                         system.debug('cv.Group__c for loop '+cv.Group__c);
                                         system.debug('docsMap.containskey(cv.Group__c) for loop '+docsMap.containskey(cv.Group__c));
                                         if(docsMap.containskey(cv.Group__c)){
                                             Documents docObj = new Documents();
                                             docObj.relContent = cv;
                                             docObj.videoLink = '/sfc/servlet.shepherd/version/download/' + cv.Id + '?asPdf=false';
                                             String S2 = '';
                                             String HS2 = '';
                                             String WS2 = '';
                                             String THS2 ='';
                                             String TWS2= '';
                                             String ts2 = '';
                                             if(cv.MarketingKit_Doc_Image__r.Image__c != null){
                                                 System.debug('bn.Image__c == '+cv.MarketingKit_Doc_Image__r.Image__c.containsIgnoreCase('https'));
                                                 // if(cv.MarketingKit_Doc_Image__r.id=='a0GE000000Dqr3W')
                                                 //  System.debug('Images-----> == '+cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c);
                                                 
                                                 String strImage = cv.MarketingKit_Doc_Image__r.Image__c;
                                                 System.debug('Testing--->'+strImage);
                                                 System.debug('Testing>'+strImage.indexof('src')+5);
                                                 System.debug(strImage.substring(strImage.indexof('src')+5));
                                                 
                                                 List<String> strImageSplits = strImage.substring(strImage.indexof('src')+5).split('"');
                                                 System.debug('Tooo-->'+strImageSplits[0]);
                                                 if(strImage.contains('imageStyles'))
                                                 {
                                                     List<String> imageStyles = strImage.substring(strImage.indexof('imageStyles')).split('"');
                                                     if(imageStyles.size() > 0){
                                                         
                                                         System.debug('imageStyles-->'+imageStyles[1]);
                                                         HS2=imageStyles[1];
                                                         
                                                         List<String> imageSubSplits=HS2.substring(HS2.indexof('height')+7).split(':');
                                                         if(imageSubSplits.size() > 0){
                                                             System.debug('Sub-->'+imageSubSplits);
                                                             System.debug('imageSubSplits-->'+imageSubSplits[0]);
                                                             System.debug('imageSubSplits@@-->'+imageSubSplits[1]);
                                                             
                                                             HS2=imageSubSplits[0];
                                                         }
                                                         List<String> imageSubSplit1=HS2.split('px;');
                                                         if(imageSubSplit1.size() > 0){
                                                             System.debug('imageSubSplit1[0]-->'+imageSubSplit1[0]);
                                                             System.debug('imageSubSplit1[1]-->'+imageSubSplit1[1]);
                                                             HS2=imageSubSplit1[0];
                                                         }
                                                         
                                                         List<String> imageSubSplit2=imageSubSplits[1].split('px;');
                                                         if(imageSubSplit2.size() > 0){
                                                             WS2=imageSubSplit2[0];
                                                             System.debug('HS2 HEIGHT-->'+HS2);
                                                             System.debug('WS2 Width-->'+WS2);
                                                         }
                                                         
                                                     }
                                                 }
                                                 else
                                                 {
                                                     if(strImage.contains('height')){
                                                         List<String> imageHeightSplits = strImage.substring(strImage.indexof('height')).split('"');
                                                         if(imageHeightSplits.size() > 0){
                                                             System.debug('imageHeightSplits[0]-->'+imageHeightSplits[0]);
                                                             System.debug('imageHeightSplits[5]-->'+imageHeightSplits[5]);
                                                             HS2 = imageHeightSplits[1]; 
                                                             WS2 = imageHeightSplits[5];
                                                         }
                                                     }
                                                 }
                                                 if(strImageSplits.size()>0){
                                                     system.debug(strImageSplits[0]);
                                                     s2=strImageSplits[0];
                                                 }         
                                                 s2 = s2.replace('amp;','');
                                             }
                                             //for thumbnail
                                             if(cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c != null){
                                                 System.debug('bn.Image__c == '+cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c.containsIgnoreCase('https'));
                                                 system.debug('Thumbnailimage -------' + cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c);
                                                 
                                                 String strThumbnailImage = cv.MarketingKit_Doc_Image__r.Thumbnail_Images__c;
                                                 
                                                 System.debug(strThumbnailImage.substring(strThumbnailImage.indexof('src')+5));
                                                 
                                                 List<String> strSubThumbnailImage = strThumbnailImage.substring(strThumbnailImage.indexof('src')+5).split('"');
                                                 if(strThumbnailImage.contains('imageStyles'))
                                                 {
                                                     List<String> thumbnailImageStyles = strThumbnailImage.substring(strThumbnailImage.indexof('imageStyles')).split('"');
                                                     if(thumbnailImageStyles.size() > 0){
                                                         
                                                         System.debug('thumbnailImageStyles-->'+thumbnailImageStyles[1]);
                                                         THS2=thumbnailImageStyles[1];
                                                         
                                                         List<String> thumbnailSubSplits=THS2.substring(THS2.indexof('height')+7).split(':');
                                                         if(thumbnailSubSplits.size() > 0){
                                                             System.debug('thumbnailSubSplits-->'+thumbnailSubSplits);
                                                             System.debug('thumbnailSubSplits0-->'+thumbnailSubSplits[0]);
                                                             System.debug('thumbnailSubSplits1-->'+thumbnailSubSplits[1]);
                                                             
                                                             THS2=thumbnailSubSplits[0];
                                                         }
                                                         List<String> thumbnailSubSplit1=THS2.split('px;');
                                                         if(thumbnailSubSplit1.size() > 0){
                                                             System.debug('thumbnailSubSplit10-->'+thumbnailSubSplit1[0]);
                                                             System.debug('thumbnailSubSplit11-->'+thumbnailSubSplit1[1]);
                                                             THS2=thumbnailSubSplit1[0];
                                                         }
                                                         
                                                         List<String> thumbnailSubSplit2=thumbnailSubSplits[1].split('px;');
                                                         if(thumbnailSubSplit2.size() > 0){
                                                             TWS2=thumbnailSubSplit2[0];
                                                             System.debug('THEIGHT-->'+THS2);
                                                             System.debug('TWidth-->'+TWS2);
                                                         }
                                                     }
                                                 }
                                                 ELSE
                                                 {
                                                     if(strThumbnailImage.contains('height')){
                                                         List<String> thss = strThumbnailImage.substring(strThumbnailImage.indexof('height')).split('"');
                                                         if(thss.size() > 0){
                                                             THS2 = thss[1]; 
                                                             TWS2 = thss[5];
                                                         }
                                                     }
                                                 }
                                                 if(strSubThumbnailImage.size()>0){
                                                     system.debug(strSubThumbnailImage[0]);
                                                     ts2=strSubThumbnailImage[0];
                                                 }
                                                 ts2 = ts2.replace('amp;','');
                                             }                       
                                             
                                             docObj.image = s2;
                                             System.debug('image---->'+docObj.image);
                                             
                                             docObj.thumbnail = TS2;
                                             System.debug('thumbnail---->'+ docObj.thumbnail);
                                             if(THS2!=null && TWS2!=null)
                                             {
                                                 docObj.imgHeight = THS2;
                                                 docObj.imgWidth = TWS2;
                                             }
                                             else
                                             {
                                                 docObj.imgHeight = HS2;
                                                 docObj.imgWidth = WS2;
                                             }
                                             
                                             //system.debug('**** s2'+s2);
                                             docsMap.get(cv.Group__c).add(docObj);
                                             docsMap.get(cv.Group__c).sort();
                                         }
                                     }
            system.debug('****If subgrouping  docsMap'+docsMap);
            // for subgrouping
            Integer count = 0 ;
            if(docsMap.size() > 0){
                for (String subgroup : docsMap.keyset())
                {
                    system.debug('****For subgroup'+subgroup +'#### size ='+docsMap.get(subgroup).size());
                    if(docsMap.get(subgroup).size() == 0){
                        docsMap.remove(subgroup);
                        continue;
                    }
                    for(Documents d: docsMap.get(subgroup))
                    {  
                        String subGroupName = d.relContent.Subgroup__c;
                        if(subGroupName == '' || subGroupName == null){
                            subGroupName = 'A';
                        }   
                        
                        Map<String,List<Documents>> tempSubDocsMap = new Map<String,List<Documents>>();
                        if(subDocsMap.containskey(subgroup))
                        {
                            tempSubDocsMap = subDocsMap.get(subgroup);
                            List<Documents> tempSubDocsList = new list<Documents>(); 
                            
                            if(tempSubDocsMap.containskey(subGroupName))
                            {
                                tempSubDocsList = tempSubDocsMap.get(subGroupName);
                                
                                tempSubDocsList.add(d);
                                tempSubDocsList.sort();
                                tempSubDocsMap.put(subGroupName,tempSubDocsList);
                            }else{
                                tempSubDocsList.add(d);
                                tempSubDocsList.sort();
                                tempSubDocsMap.put(subGroupName,tempSubDocsList);
                            }
                            subDocsMap.put(subgroup,tempSubDocsMap);
                            system.debug('****If subDocsMap'+subDocsMap);
                        }else{
                            list<Documents> tempSubDocsList2 = new list<Documents>();
                            tempSubDocsList2.add(d);
                            tempSubDocsMap.put(subGroupName,tempSubDocsList2);
                            subDocsMap.put(subgroup,tempSubDocsMap);
                            system.debug('****else subDocsMap'+subDocsMap);
                        }               
                        
                    }
                }
                system.debug('****For subDocsMap'+subDocsMap);
            }
            system.debug('#@@@#@# subDocsMap' + subDocsMap);
            if(subDocsMap.size() >0 ){
                for(String s : subDocsMap.keyset()){
                    if(subDocsMap.get(s).size() == 0){
                        subDocsMap.remove(s);
                        continue;
                    }
                    List<SubDocsClass> subdocuments = new List<SubDocsClass>();
                    for(String strImage : subDocsMap.get(s).keyset()){
                        if(subDocsMap.get(s).get(strImage).size() == 0){
                            subDocsMap.get(s).remove(strImage);
                            continue;
                        }
                        SubDocsClass submrkdocs = new SubDocsClass();
                        submrkdocs.subGroupHeader = strImage;
                        submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                        // Test code need to remove below 5
                      /*  submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                        submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                        submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                        submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                        submrkdocs.subGroupList.addall(subDocsMap.get(s).get(strImage));
                       */ 
                        subdocuments.add(submrkdocs);
                        subdocuments.sort();
                    }
                    MarkDocs objMarkDocs = new MarkDocs();
                    objMarkDocs.header = s;   
                    objMarkDocs.headerCount = '#'+count ; 
                    count++;
                    objMarkDocs.content.addAll(subdocuments);
                    objMarkDocs.content.sort();
                    lstMarktKitDocs.add(objMarkDocs);
                    // test data. need to delete this duplicate add.
                   // lstMarktKitDocs.add(objMarkDocs);
                  //  lstMarktKitDocs.add(objMarkDocs);
                  //  lstMarktKitDocs.add(objMarkDocs); 
                    
                    
                    lstMarktKitDocs.sort();
                    
                    //start
                    list <MarkDocs> tempMarkDocsList1 = lstMarktKitDocs;
                    list <MarkDocs> tempMarkDocsList2 = new list <MarkDocs>();
                    list <MarkDocs> tempMarkDocsList3 = new list <MarkDocs>();
                    
                    //Re-sorting the list                 
                    for (MarkDocs md : lstMarktKitDocs) {                    
                        if (md.header.contains('Getting')) {
                            tempMarkDocsList3.add(md);
                        }
                        else 
                            tempMarkDocsList2.add(md);                    
                    }
                    
                    tempMarkDocsList1.clear();
                    tempMarkDocsList1.addAll(tempMarkDocsList3);
                    tempMarkDocsList1.addAll(tempMarkDocsList2);                
                    for (MarkDocs md : tempMarkDocsList1) {
                        system.debug('############### tempMarkDocsList1.Header1 = ' + md.header);
                        
                    }
                }      
                
            }
            
        }
        system.debug('############### lstMarktKitDocs' + lstMarktKitDocs);
        // return lstMarktKitDocs;
        objMarkProgDocs.lstMarkProgDocs = lstMarktKitDocs;
        objMarkProgDocs.mkTermsAccepted = termsAccepted;
        return objMarkProgDocs;
    }
    
    @AuraEnabled
    public static Boolean registerLicenseAgreement(string marketProg){
        
        if(String.isNotBlank(marketProg)){
            setMvMarketingKitController(marketProg,UserInfo.getUserId());
        }
        
        system.debug('userIdFromSurvey===='+userIdFromSurvey);
        system.debug('marketProg===='+marketProg);
        
        String picklistValue = mapMarketKitAgrmnt.get(marktProg);
        ReloadPopup=true;
        
        try{
            // logic from another method
            List<string> lstGroupName = new list<string>();
            List<groupmember> lstGroupMemberAdd = new List<groupmember>();
            for(Product_Roles__c pr:[Select product_Name__c,Public_group__c 
                                     from Product_Roles__c 
                                     where product_Name__c = : marktProg]) 
            {
                if(pr.Public_group__c != null)
                    lstGroupName.add(pr.Public_group__c);
            }
            system.debug('---Product group---' + lstGroupName);
            for(group gr: [Select id from group where name in: lstGroupName])
            {
                groupmember gmr = new groupmember(GroupId = gr.id,UserOrGroupId = UserInfo.getUserId());
                lstGroupMemberAdd.add(gmr);
            }
            system.debug('----Groupmember--' + lstGroupMemberAdd);
            if(lstGroupMemberAdd != null){
                try{
                    insert lstGroupMemberAdd;
                }catch(Exception ex){
                    Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
                }
            }
            
        }catch(Exception ex){
            Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
        }
        if(lstCont.size() > 0){
            addGroupToLibrary(picklistValue,lstCont[0].id);
        }
        termsAccepted = true;
        Messages='block';
        return termsAccepted;
    }
    
    @future(callout = true)
    public static void addGroupToLibrary(String picklistValue, String conId){
        List<Contact> lstCon = [Select id,MarketingKit_Agreement__c from contact where id=:conId];
        if(lstCon[0].MarketingKit_Agreement__c !=null && picklistValue!=null){
            if(!lstCon[0].MarketingKit_Agreement__c.contains(picklistValue)){
                String strMarketingKitAgreement = lstCon[0].MarketingKit_Agreement__c + ';' + picklistValue;
                lstCon[0].MarketingKit_Agreement__c = strMarketingKitAgreement;
            }
        }else{
            lstCon[0].MarketingKit_Agreement__c = picklistValue;
        }
        try{
            update lstCon;
        }catch(DMLException ex){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Factory Visit cant be pass date.Please contact your portal admin for this.');
            ApexPages.addMessage(myMsg);
            //send mail to portal admin to update the contact.
        }
    }
}
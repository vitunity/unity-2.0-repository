@isTest(seeAllData=True)
private class MvMarketingKitControllerTest {

    public static Regulatory_Country__c regcount;

    public static Profile pAdmin;
    public static User u;
    public static Contact con;
    public static Account a;
    public static Customer_Agreements__c agr;
    public static Event_Webinar__c Webinar;

    public static Account account1;
    public static Contact contact1;
    public static User newUser;

    public static User thisUser;

    public static UserRole rol;

    public static SVMXC__Installed_Product__c objIP;
    public static Product_Version__c pv;

    public static ContentVersion parentContent;
    public static ContentVersion testContent;
    public static ContentVersion testContents;
    public static ContentWorkspace testWorkspace;
    public static ContentWorkspaceDoc newWorkspaceDoc;

    public static Product_Roles__c prodRole;

    static
    {
        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];

        pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        rol = [select id from UserRole LIMIT 1];           

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

        a = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert a;  
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
            //MvMyFavorites__c='Events', 
            AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
            MailingState='CA', Phone = '12452234',
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        //Creating a running user with System Admin profile
        insert con;

        System.runAs(thisUser) {
            newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
            insert newUser;
       }

        regcount = new Regulatory_Country__c();
        regcount.Name = 'Test';
        regcount.RA_Region__c ='NA';
        regcount.Portal_Regions__c ='N.America,Test';
        insert regcount;

        Group grp = new Group(Name = 'test group');
        insert grp;

        prodRole = new Product_Roles__c();
        prodRole.Name='TrueBeam Marketing Program 123';
        prodRole.product_Name__c='TrueBeam Marketing Program 123';
        prodRole.Public_group__c='TrueBeam Marketing Program 123';
        prodRole.ismarketingKit__c=true;
        prodRole.Survey_URL__c='http://www.google.com';
        prodRole.marketingkitorder__c = 1.0;
        prodRole.Public_group__c = 'test group';
        insert prodRole;
    }    
	
	@isTest static void getCustomLabelMapTest() {
        test.startTest();
            map<String,String> mapTest = MvMarketingKitController.getCustomLabelMap('en');
        test.stopTest();
	}
	
	@isTest static void test2() {
        test.startTest();
            MvMarketingKitController.getMarketingKits();
        test.stopTest();

	}

	@isTest static void test3() {
        test.startTest();
        User thisUser = [ select Id from User where ContactId != null AND isActive = true Limit 1 ];
        System.runAs ( thisUser ) {        
        	Product_Roles__c prdRl = new Product_Roles__c(Name = 'test909090', ismarketingKit__c = true);
        	insert prdRl;

            MvMarketingKitController.getMarkProgDocs('test1', 'test2');
            MvMarketingKitController.registerLicenseAgreement('test1');
        }
        test.stopTest();

	}

    @isTest static void addGroupToLibraryTest() {
        test.startTest();
            MvMarketingKitController.addGroupToLibrary('Trilogy Marketing Program', con.Id);

            con.MarketingKit_Agreement__c = 'Edge Marketing Program';
            update con;

            MvMarketingKitController.addGroupToLibrary('Pivotal Prone Breast Care on TMP', con.Id);            
        test.stopTest();

    }

    @isTest static void getMarkProgDocsTest() {
        Group grp = new Group(Name = 'test group');
        insert grp;

        Product_Roles__c prRole;
        List<Product_Roles__c> listPr = [SELECT Id, Name, Product_Name__c,Public_Group__c, ismarketingKit__c FROM Product_Roles__c WHERE Name = 'Acuity' limit 1];
        if(listPr.size() == 0) {
            prRole = new Product_Roles__c(Name = 'Acuity', Product_Name__c = 'Acuity', ismarketingKit__c = true, Public_group__c= 'test group');
            insert prRole;
        } else {
            prRole = listPr[0];
            prRole.ismarketingKit__c = true;
            update prRole;
        }

        
        Test.startTest();       
        System.runAs (thisUser) {
            init(); 
            MvMarketingKitController.getMarkProgDocs('Acuity', thisUser.Id);
        }
        Test.stopTest();
    }

    @isTest static void getMyVarianAgreementTest() {
        MyVarian_Agreement__c myAgr = new MyVarian_Agreement__c(Name = 'Test', Territory__c = 'test');
            insert myAgr;
        MyVarian_Agreement_Translation__c agr = new MyVarian_Agreement_Translation__c(Name ='Test Agr', Agreement_text__c = 'Agr Text', 
                    Language__c = 'en_US', MyVarian_Agreement__c = myAgr.Id);
            insert agr;   
        Test.startTest(); 
        System.runAs (thisUser) {
            
            MvMarketingKitController.getMyVarianAgreement('Test Agr', 'EN');
        }
        Test.stopTest();
    }

    @isTest static void registerLicenseAgreementTest() {
        Group grp = new Group(Name = 'test group');
        insert grp;
        Product_Roles__c prRole = new Product_Roles__c(Name = 'Test Role', Product_Name__c = 'test Prog', ismarketingKit__c = true, Public_group__c= 'test group');
        insert prRole;
        Test.startTest(); 
        System.runAs (thisUser) {
            
            MvMarketingKitController.registerLicenseAgreement('Test Prog');
        }
        Test.stopTest();
    }

    
    private static void init() {
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Marketing Kit').RecordTypeId;

        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;

        Id RecId1 = Schema.SObjectType.BannerRepository__c.RecordTypeInfosByName.get('Marketing Kit').RecordTypeId;

        BannerRepository__c bn = new BannerRepository__c(
            Image__c = '<img alt="User-added image" height="571" src="https://varian--SFQA--c.cs95.content.force.com/servlet/rtaImage?eid=a0GE000000Dqr1H&amp;feoid=00NE0000004sNzr&amp;refid=0EME0000000LG9T" width="500"></img>',
            Thumbnail_Images__c = '<img alt="User-added image" height="571" src="https://varian--SFQA--c.cs95.content.force.com/servlet/rtaImage?eid=a0GE000000Dqr1H&amp;feoid=00NE0000004sNzr&amp;refid=0EME0000000LG9T" width="500"></img>',
            RecordTypeId = RecId1);
        insert bn;

        testContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'English1234',
                                                       Document_Version__c = 'V1.1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description',
                                                       Parent_Documentation__c = 'prentDoc',
                                                       Group__c = 'Animation Clips',
                                                       MarketingKit_Doc_Image__c = bn.Id);
        insert testContent;

        ContentVersion conV = [SELECT Id, Recordtype.Name FROM ContentVersion WHERE Id = :testContent.Id];
        system.debug('#### debug conV = ' + conV);

        testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];

        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = 'Acuity']; 

        newWorkspaceDoc =new ContentWorkspaceDoc();
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContents.ContentDocumentId;
        insert newWorkspaceDoc;       

        //ContentWorkspaceDoc testC = [Select c.ContentDocumentId From ContentWorkspaceDoc c  where ContentWorkspace.Name = 'Acuity'];
        //system.debug('#### debug testC = ' + testC); 
    }	
	
}
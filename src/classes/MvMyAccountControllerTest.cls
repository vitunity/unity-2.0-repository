@isTest (seeAllData=true)
private class MvMyAccountControllerTest
{
    public static Regulatory_Country__c regcount ;
    public static Contact_Role_Association__c cr;
    public static Profile p;
    public static User u;
    public static Contact con;
    public static Account a;
    public static Customer_Agreements__c agr;
    static
    {
        a = new Account(name='test2ABC',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='USA'  ); 
        insert a;  
        con = new Contact(FirstName = 'TestContactABC', LastName = 'TestContactABC', Email = 'TestABC@1234APR.com', MvMyFavorites__c='Meetings', AccountId = a.Id, MailingCity='New York', MailingCountry='USA', MailingPostalCode='552601', MailingState='CA', Phone = '12452234');
        //Creating a running user with System Admin profile
        insert con;
        p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', Subscribed_Products__c=True,
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='test_varian@testorg.com',Country='USA');

        regcount = new Regulatory_Country__c();
        regcount.Name = 'Test';
        regcount.RA_Region__c ='NA';
        regcount.Portal_Regions__c ='N.America,Test';
        insert regcount;

        cr = new Contact_Role_Association__c();
        cr.Account__c= a.Id;
        cr.Contact__c = u.Contactid;
        cr.Role__c = 'RAQA';
        //insert cr;

        agr = new Customer_Agreements__c(Affected_Account__c = a.Id, 
                                        //Name = 'Test Agreement',
                                        Agreed_By__c = con.Id,
                                        Agreement_Date__c = System.Now());
        insert agr;


    }
    static testMethod void testGetLoggedInUser() 
    {
        Test.starttest();
        MvMyAccountController.getLoggedInUser();
        Test.stoptest();
    }

    
    static testMethod void testSubscribeInfo() 
    {
        Test.starttest();
        System.runAs(u)
        {
            MvMyAccountController.getSubscribeInfo();
        }
        Test.stoptest();
    }

    
    static testMethod void testOptionsCountry() 
    {
        Test.starttest();
        MvMyAccountController.getOptionsCountry();
        MvMyAccountController.getLanguagePicklistOptn('Preferred_Language1__c');
        Test.stoptest();
    }

    
    static testMethod void testContactUpdate() 
    {
        Test.starttest();
        MvMyAccountController.getCRAs();
        Test.stoptest();
    }

    
    static testMethod void testResetpass()  
    {
        Test.starttest();
        MvMyAccountController.Resetpass('test', 'test', 'test');
        Test.stoptest();
    }

    
    static testMethod void testResetRcvryQus()
    {
        Test.starttest();
        MvMyAccountController.ResetRcvryQus('test', 'test', 'test');
        Test.stoptest();
    }

    static testMethod void testCreateContactUpdate()
    {
        Test.starttest();
        MvMyAccountController.createContactUpdate('test', 'test','test', 'test','test', 'test', 'test');
        Test.stoptest();
    }

    static testMethod void testGetMyHomeFavorites()
    {
        Test.starttest();
        MvMyAccountController.getMyHomeFavorites('English');
        Test.stoptest();
    }

    
    static testMethod void testUnSubscribe()
    {
        Test.starttest();
        MvMyAccountController.unSubscribe();
        Test.stoptest();
    }
    
    static testMethod void testSubscribe()
    {
        Test.starttest();
            try {
                MvMyAccountController.subscribe();
            } catch (Exception e) {

            }
        Test.stoptest();
    }
    
    static testMethod void testGetMyFavorites()
    {
        Test.starttest();
        MvMyAccountController.getMyFavorites('en');
        MvMyAccountController.con = con;
        MvMyAccountController.getMyFavorites('en');
        Test.stoptest();
    }
    
    static testMethod void testGetDynamicPicklistOptions()
    {
        Test.starttest();
        MvMyAccountController.getDynamicPicklistOptions('MvMyFavorites__c');
        Test.stoptest();
    }
    static testMethod void testFetchProds1()
    {
        Test.starttest();
        MvMyAccountController.fetchProds1();
        Test.stoptest();
    }
    static testMethod void testFetchProds2()
    {
        Test.starttest();
        MvMyAccountController.fetchProds2();
        Test.stoptest();
    }
    static testMethod void testDocumentOptionsList()
    {
        Test.starttest();
        MvMyAccountController.getDocumentOptionsList();
        Test.stoptest();
    }
    static testMethod void testGetlstCntntVersion()
    {
        Test.starttest();
        MvMyAccountController.getlstCntntVersion('');
        Test.stoptest();
    }

    static testMethod void testInitializeCRA()
    {
        Test.starttest();
        MvMyAccountController.initializeCRA();
        Test.stoptest();
    }
    static testMethod void testConsentClick()
    {
        Test.starttest();
        MvMyAccountController.iConsentClick('test');
        Test.stoptest();
    }


    static testMethod void testAllOthers1()
    {
        Test.starttest();
            MvMyAccountController.getContactUpdate();
            MvMyAccountController.saveMyInfomation(con, 'newEmail@email.com',
                                        'newCity', 'newState', 'newCountry',
                                        '3434343', 'new Street');
            MvMyAccountController.saveMyFavorite('Meetings');
            
            MvMyAccountController.getMyHomeFavorites('en');
            
            MvMyAccountController.registerNow();
            MvMyAccountController.getCustomLabelMap('EN');
            MvMyAccountController.getOnDemandWebinars();
            MvMyAccountController.saveTheImageUserFile(a.Id, 'filename', 'base64', 'manual');
            //MvMyAccountController.removeCRA('0010n000005xP0C');
        Test.stoptest();
    }

    static testMethod void getMyFavoritesTest()
    {
        Test.starttest();
            delete con;
            u.MvMyFavorites__c = 'Webinars';
            insert u;

            System.debug('#### in test u = ' + u);

            System.runAs(u) {
            MvMyAccountController.getMyHomeFavorites('en');
            }
        Test.stoptest();
    }

    static testMethod void removeCRATest()
    {
        Test.starttest();
            try {
                MvMyAccountController.removeCRA('011abcdeabcdeabcde');
            } catch (Exception e) {

            }
        Test.stoptest();
    }

    static testMethod void getMyHomeFavoritessTest()
    {
        Test.starttest();
            MvMyAccountController.getMyHomeFavoritess();
        Test.stoptest();
    }

    static testMethod void testRegisterMDataIntituteChange()
    {
        Test.starttest();
            MvMyAccountController.registerMDataIntituteChange(con, 'new institue',
                                        'newEmail@email.com',
                                        'newCity', 'newState', 'newCountry',
                                        '3434343', 'new Street');
        Test.stoptest();
    }

    static testMethod void testRegisterMDataEmailChange()
    {
        Test.starttest();
            MvMyAccountController.registerMDataEmailChange(con,
                                        'newEmail@email.com');
        Test.stoptest();
    }  

    static testMethod void testDuplicateContactEmail()
    {
        Test.starttest();
            MvMyAccountController.duplicateContactEmail('newEmail@email.com');
        Test.stoptest();
    }  

    static testMethod void testGetMyRecQuestions()
    {
        Test.starttest();
            MvMyAccountController.getMyRecQuestions('en');
            MvMyAccountController.getCountryOptions();
        Test.stoptest();
    }           
}
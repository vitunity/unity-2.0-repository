/*
* Author: Harleen Gulati
* Created Date: 22-July-2017
* Project/Story/Inc/Task : My varian lightning project 
 * Description: This Class is used for OncoPeer lightening page.
*/
public with sharing class MvOncoPeerController {
    
    /* Get all custom labels */
    @AuraEnabled
        public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    /* Check for logged in user */
    @AuraEnabled
    public static User getCurrentUser() {
        return [SELECT Id, FirstName, LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    }
}
/*
* Author: Harleen Gulati
* Created Date: 17-Aug-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This will be used for my varian paper documentation page.
*/
public without sharing class MvPaperDocumentationRequestController{
    
    /*  Map and Strng  defined cache the labels of selected Lang */
    public static map<String,String> cachedMap=new map<String,String>();
    public static String cachedLang;
    
    /* Get logged in user information */
    @AuraEnabled
    public static User getCurrentUser() {
        return [SELECT Id, FirstName, LastName FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    }
    
    /* Get all custom labels values*/
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        cachedMap=MvUtility.getCustomLabelMap(languageObj);
        cachedLang=languageObj;
        return cachedMap;
    }
    
    
    
    @AuraEnabled
    public static Contact getContact() {
        
        list<User> usr = new list<User>();
        usr = [SELECT Id, FirstName, LastName, Contact.Id, usertype 
               FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        
        if(usr[0].Contact.Id != null){
            String usrContId = usr[0].contact.Id;
            Contact c = new Contact();
            c = [select FirstName, LastName, Salutation, Preferred_Language1__c, Account.name,Account.Legal_Name__c, Account.id,account.AccountNumber,
                 Email, phone, Title, MailingStreet, manager__c, Manager_s_Email__c,Functional_Role__c,
                 Reason_To_Unsubscribe__c,Specialty__c,Research_Contact__c,
                 account.billingstreet, account.billingcountry, account.billingpostalcode, account.billingcity, account.billingstate,
                 VMSPortalAdminToUpdate__c, MailingState, MailingPostalCode, MailingCountry, MailingCity ,fax
                 from 
                 contact where Id =: usrContId];
            return c;
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static List<String> getDynamicPicklistOptionsForDocumentLanguage()
    {
        List<String> options = new List<String>();
        set<string> LanguageSet = new set<String>();
        Schema.Describefieldresult DocLanguagesDesc = ContentVersion.Document_Language__c.getDescribe();
        List<Schema.PicklistEntry> ple = DocLanguagesDesc.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            LanguageSet.add(f.getValue());
        }  
        
        for(string sr : LanguageSet) {
            options.add(sr); 
        }
        return options;
    }
    
    @AuraEnabled
    public static List<String> getDynamicPicklistOptionsForCountry()
    {
        List<Regulatory_Country__c> countrylist = new List<Regulatory_Country__c>([Select Name,SupportRegion__c from Regulatory_Country__c Order By Name]);
        List<String> options = new List<String>();
        
        for(Regulatory_Country__c values: countrylist)
        {
            if(values.SupportRegion__c != Null)
            { 
                options.add(values.Name);
                
            }
        }
        return options;
    }
    
    @AuraEnabled
    public static List<String> getDynamicPicklistOptionsForProduct()
    {
        List<String> options = new List<String>();
        String  accountId = [Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].AccountId;
        List<Product_Roles__c> prdrole = new List<Product_Roles__c>();
        List<String> publicgrouplist = new list<String>();
        set<string> ProductGroupSet = new set<String>();//CpProductPermissions.fetchProductGroup();
        Map<String,Product_Roles__c> productrolemap = new Map<String,Product_Roles__c>();
        prdrole = [Select Product_Name__c,Public_Group__c from Product_Roles__c where ismarketingKit__c = false];
        
        for(Product_Roles__c prg : prdrole)
        {
            publicgrouplist.add(prg.Public_Group__c);
            productrolemap.put(prg.Public_Group__c,prg);
        }
        
        List<GroupMember> grplmemberlist = new List<GroupMember>();
        grplmemberlist = [Select GroupId,UserOrGroupId,Group.Name from GroupMember 
                          where Group.Name in:publicgrouplist and UserOrGroupId =: UserInfo.getuserId()]; //and UserOrGroupId =: UserInfo.getuserId()];
        if(accountId != null){
            for(GroupMember gm: grplmemberlist)
            {
                ProductGroupSet.add(productrolemap.get(gm.Group.Name).Product_Name__c);
            }
        }else{ //for internal user
            Schema.DescribeFieldResult fieldResult = Product2.Product_Group__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
                ProductGroupSet.add(f.getValue());
            }       
        }
        
        
        for(string sr : ProductGroupSet) {
            options.add(sr); 
        }
        return options;
    }
    
    @AuraEnabled
    public static  List<ContVersionWrapper> ContentVersions(String Productvalue, String Languagevalue) 
    {
        string sortDir = 'desc';
        string sortField = 'Date__c';
        Integer counter=0;  //keeps track of the offset
        Integer list_size=10; //sets the page size or number of rows
        Integer total_size; //used to show user the total size of the list
        List<ContVersionWrapper> lContentVersionsList = new List<ContVersionWrapper>();
        
        if(Languagevalue == null && Productvalue!= null)
        {
            return null;
        }
        else
        {
            if(Languagevalue!=null && Productvalue!=null)
            {
                
                list<ContentWorkspaceDoc> lst = new list<ContentWorkspaceDoc>();
                lst = [Select ContentDocumentId,ContentWorkspaceId from ContentWorkspaceDoc 
                       where ContentWorkspace.Name =:Productvalue];
                ContVersionWrapper cv = new ContVersionWrapper();
                system.debug('===='+lst);      
                set<Id> Documentid = new set<Id>();
                for (ContentWorkspacedoc cwd: lst )
                {
                    Documentid.add(cwd.ContentDocumentId);
                    //Documentid.add('0690n0000003dDsAAI');
                }
                string whr='';
                String Query = 'select Mutli_Languages__c,ContentDocumentId,Title,Parent_Documentation__c,Date__c,Document_Language__c,Document_Type__c,Recently_Updated__c, Document_Number__c,Newly_Created__c,Document_Version__c from ContentVersion where ContentDocumentId in : Documentid and Document_Language__c=:Languagevalue  and Printable__c = True  and RecordType.name = \'Product Document\' '; 
                String QueryCnt = 'select count() from ContentVersion where ContentDocumentId in : Documentid and Document_Language__c=:Languagevalue and Printable__c = True  and RecordType.name = \'Product Document\''; 
                
                if(whr == '' || whr == null)
                    whr = 'order by ' + sortField + ' '+ sortDir;
                else if(!whr.contains('order'))
                    whr = whr + ' order by ' + sortField + ' '+ sortDir;
                Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + counter;        
                
                total_size = DataBase.countQuery(QueryCnt);
                List<ContentVersion> sobj = DataBase.Query(Query);
                cv.showContent = true;
                if(counter < 2000){
                    For(ContentVersion s : Sobj)
                    {
                        cv.ContVer = s;
                        lContentVersionsList.add(cv);//((ContVerWrap)s);
                    }
                }
                return lContentVersionsList ;
            }
            else
            {
                return null;
            }
        }
    }
    
    @AuraEnabled
    public static String registerMData(Contact registerDataObj, Account accDataObj, List <String> lstRecordId, string languagevalue){
        
        //Code to Send email to user and admin     
        List<String> toAddresses = new List<String>();
        List<ProdCountEmailMapping__c > emailmap = new List<ProdCountEmailMapping__c>();
        List<String> touser = new List<String>();
        touser.add(registerDataObj.Email);
        Datetime myDT = Datetime.now();
        String myDate = myDT.format('MM/dd/yyyy HH:mm:ss');
        String EmailBody;
        string reqtype;
        string Request = 'PaperDocumentation';
        
        list<User> usr = new list<User>();
        usr = [select id ,name,username,Email,Subscribed_Products__c,alias,usertype,FullPhotoUrl,SmallPhotoUrl,
               contact.Title, contact.firstname, contact.lastname, Contact.accountid, contact.account.Name, contact.MailingCity,
               contact.MailingCountry, contact.MailingPostalcode, Contact.mailingState, Contact.MailingStreet, Contact.Phone,
               Contact.Institute_Name__c, contact.Email, contact.id, contact.manager__c, contact.account.billingstreet, contact.Reason_To_Unsubscribe__c,
               contact.account.billingcountry, contact.account.billingpostalcode, contact.account.billingcity, contact.account.billingstate, contact.account.fax,
               contact.account.phone, contact.account.e_Subscribe__c, contact.fax,contact.OktaId__c,
               contact.RAQA_Contact__c from user where Id =: UserInfo.getUserId() limit 1];
        
        if(usr[0].contact.id != null){
            registerDataObj.FirstName            =  usr[0].contact.FirstName;
            registerDataObj.LastName             =  usr[0].contact.LastName;
            registerDataObj.Institute_Name__c    =  usr[0].contact.Institute_Name__c;
            registerDataObj.Phone                =  usr[0].contact.Phone;
            registerDataObj.Email                =  usr[0].contact.Email;
            registerDataObj.MailingStreet        =  usr[0].contact.MailingStreet;
            registerDataObj.MailingState         =  usr[0].contact.MailingState;
            registerDataObj.MailingCity          =  usr[0].contact.MailingCity;
            registerDataObj.MailingCountry       =  usr[0].contact.MailingCountry;
            registerDataObj.MailingPostalcode    =  usr[0].contact.MailingPostalcode;
            
        }
        emailmap=[Select e.email__c From ProdCountEmailMapping__c e where e.Name=: Request limit 1];
        if(emailmap.size() >0)
        {
            toAddresses.add(emailmap[0].email__c);
        }
        
        if(cachedMap==null)
        {
            cachedMap=getCustomLabelMap(cachedLang);
        }   
        
        
        reqtype='Paper Documentation';
        EmailBody=cachedMap.get(Label.Mv_Mail_Template_Dear)+' '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Msg1)+'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Sincerely)+',<br><br>'+cachedMap.get(Label.Mv_Mail_Template_VMS);
        //EmailBody= EmailBody+ '<br><br>Submitted on '+myDate+ '<br>Name: '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>Institution: '+registerDataObj.Institute_Name__c+'<br>Telephone: '+registerDataObj.Phone+'<br><br>Email: '+registerDataObj.Email ;
        //EmailBody=EmailBody+'<br><br>Address: '+registerDataObj.MailingStreet +'<br>City: '+registerDataObj.MailingCity +'<br>State:  '+registerDataObj.MailingState+'<br>Country: '+registerDataObj.MailingCountry +'<br><br>Type of Request: '+ reqtype+'<br>Product: '+prodObj.Product_Group__c+'<br>Version: '+prodV +'<br><br>Subject: '+subject+'<br><br>'+longDes ;
        //EmailBody = EmailBody +'<br><br>Document Title: '+registerDataObj.Title+'<br>Document Number: '+registerDataObj.Document_Number__c+'<br>Document Language: '+Languagevalue;
        Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
        MailTicket.setSubject('Your request for Paper Documentation was successfully submitted.');
        MailTicket.setToAddresses(touser);
        // Use Organization Wide Address  
        
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress])
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress))
                MailTicket.setOrgWideEmailAddressId(owa.id);
        }
        MailTicket.setReplyTo(System.Label.MVsupport);  
        MailTicket.setSaveAsActivity (false);
        String body ='';
        
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        mailAdmin.setSubject('Your request for Paper Documentation was successfully submitted.');
        mailAdmin.setToAddresses(toAddresses);
        
        // Use Organization Wide Address 
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]){ 
            if(owa.Address.contains(system.label.CpOrgWideAddress)) {
                mailAdmin.setOrgWideEmailAddressId(owa.id);
            }
        }
        mailAdmin.setReplyTo(System.Label.MVsupport);
        if(Request == 'PaperDocumentation' ){
            set<String> ContentDocumentIdSet = new set<String> ();
            //lstRecordId.add('0690n0000003dDsAAI');   //April 17 - UB - Commented this hardcoded id
            for(string contentId: lstRecordId){
                ContentDocumentIdSet.add(contentId);
            }
            List<Contentversion> cversion=new List<Contentversion>();
            if(lstRecordId.size()>0)
            {
                for(Contentversion cv: [select VersionData,ContentDocumentId,Title,FileType,Document_Number__c from ContentVersion where ContentDocumentId IN :ContentDocumentIdSet])
                {
                    cversion.add(cv);
                }
            }
            if (cversion.size()>0) {
                if(Request == 'PaperDocumentation' ){
                    body ='<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Submitted_on) +myDate+ '<br>'+cachedMap.get(Label.Mv_Mail_Template_Name) +registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Institution) +registerDataObj.Institute_Name__c+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Telephone) +registerDataObj.Phone+'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Email) +registerDataObj.Email  ;
                    // body = body +'<br><br>Address: '+registerDataObj.MailingStreet +'<br>City: '+registerDataObj.MailingCity +'<br>State:  '+registerDataObj.MailingState+'<br>Postal Code: '+registerDataObj.MailingPostalCode+'<br>Country: '+registerDataObj.MailingCountry +'<br><br>Type of Request: '+ reqtype+'<br>Product: '+prodObj.Product_Group__c;
                    //body = body +'<br><br>Document Title: '+cversion[0].Title+'<br>Document Number: '+cversion[0].Document_Number__c+'<br>Document Language: '+Languagevalue; 
                    body = body +'<br><br>{'+cachedMap.get(Label.Mv_Mail_Template_USER_MAIL)+':['+registerDataObj.Email+']}';
                }
                if(Request == 'PaperDocumentation' ){
                    EmailBody= EmailBody+ '<br><br>'+cachedMap.get(Label.Mv_Mail_Template_submitted_on) +myDate+ '<br>'+cachedMap.get(Label.Mv_Mail_Template_Name) +registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Institution) +registerDataObj.Institute_Name__c+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Telephone) +registerDataObj.Phone+'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Email) +registerDataObj.Email ;
                    //EmailBody=EmailBody+'<br><br>Address: '+registerDataObj.MailingStreet +'<br>City: '+registerDataObj.MailingCity +'<br>State:  '+registerDataObj.MailingState+'<br>Postal Code: '+registerDataObj.MailingPostalCode +'<br>Country: '+registerDataObj.MailingCountry +'<br><br>Type of Request: '+ reqtype+'<br>Product: '+prodObj.Product_Group__c;
                    EmailBody = EmailBody +'<br><br>'+cachedMap.get(Label.Mv_Mail_Template_Document_Title) +cversion[0].Title+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Document_Number) +cversion[0].Document_Number__c+'<br>'+cachedMap.get(Label.Mv_Mail_Template_Document_Language) +Languagevalue;
                } 
                mailAdmin.setHTMLBody(body);
                MailTicket.setHTMLBody(EmailBody);
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                list<CpPrintRequestDocFileType__c> AllDocTypes = CpPrintRequestDocFileType__c.getall().values();  
                for(CpPrintRequestDocFileType__c DocType: AllDocTypes )  
                { 
                    if(DocType.ContentVersionFileType__c==cversion[0].FileType)
                    { 
                        efa.setFileName(cversion[0].Title+DocType.File_Extension__c);
                        efa.setContentType(DocType.MIME_Type__c);
                    }
                } 
                
                blob b =cversion[0].VersionData;
                efa.setBody(b);
                MailTicket.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                mailAdmin.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
        }
        
        mailAdmin.setTreatTargetObjectAsRecipient(false); 
        mailAdmin.setReplyTo(System.Label.MVsupport);     
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
        
        if(emailmap.size() >0)
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin }); 
        }              
        return 'accessdocs';
    }
    
}
@isTest(seeAllData=true)
private class MvPaperDocumentationRequestCtrlTest {

    public static Account act;  
    public static Contact con;
    public static User user;

    public static ContentVersion parentContent;
    public static ContentVersion testContent;
    public static ContentVersion testContents;
    public static ContentWorkspace testWorkspace;
    public static ContentWorkspaceDoc newWorkspaceDoc;

    public static User thisUser;
    public static User newUser;
    public static BannerRepository__c banner;
    public static SVMXC__Installed_Product__c objIP;
    public static Product_Version__c pv;
    public static Product2 pr;
    public static Regulatory_Country__c regcount ;

    static
    {

            thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
     
            //pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            //rol = [select id from UserRole LIMIT 1];           

            Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

            act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                        BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
            insert act;  
            con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
                MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
                MailingState='CA', Phone = '12452234',
                MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
            //Creating a running user with System Admin profile
            insert con;

            UserRole rl = [SELECT Id from UserRole Limit 1];

            System.runAs(thisUser) {
                newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                    lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                    username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
                insert newUser;
                Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
                Groupmember gm2 = new groupmember(GroupId = group1.id, UserOrGroupId = newUser.Id);
                insert gm2;                     
            }
            
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Product Documentation' AND sobjecttype='BannerRepository__c'];
            banner =new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert banner;


            pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity');
            insert pr;

            objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Company__c = act.id, SVMXC__Product__c = pr.Id);
            insert objIP;     

            pv = new Product_Version__c(product__c = pr.id);
            insert pv;

          regcount = new Regulatory_Country__c();
          regcount.Name = 'United Kingdom';
          regcount.RA_Region__c ='NA';
          regcount.Portal_Regions__c ='N.America,Test';
          insert regcount;                
        //}
    } 


	@isTest static void getCurrentUserTest() {
		Test.startTest();
            MvPaperDocumentationRequestController.getCurrentUser();
		Test.stopTest();
	}
	
	@isTest static void getCustomLabelMapTest() {
		Test.startTest();
            MvPaperDocumentationRequestController.getCustomLabelMap('EN');
		Test.stopTest();
	}


	@isTest static void getContactTest() {
		Test.startTest();
			System.runAs(newUser) {
            	MvPaperDocumentationRequestController.getContact();
			}
		Test.stopTest();
	}
	
	@isTest static void getDynamicPicklistOptionsForDocumentLanguageTest() {
		Test.startTest();
            MvPaperDocumentationRequestController.getDynamicPicklistOptionsForDocumentLanguage();
		Test.stopTest();
	}


	@isTest static void getDynamicPicklistOptionsForCountryTest() {
		Test.startTest();
            MvPaperDocumentationRequestController.getDynamicPicklistOptionsForCountry();
		Test.stopTest();
	}


	@isTest static void getDynamicPicklistOptionsForProductTest() {
		Test.startTest();
            MvPaperDocumentationRequestController.getDynamicPicklistOptionsForProduct();
		Test.stopTest();
	}
	
	@isTest static void ContentVersionsTest() {
		//init();
		Test.startTest();
    init();
            MvPaperDocumentationRequestController.ContentVersions('testProdVal', 'English');
		Test.stopTest();
	}

	
	@isTest static void registerMDataTest() {
		
		Test.startTest();
    init();
			list<String> lst1 = new list<String>();
      lst1.add(testContents.ContentDocumentId);
          try {
              MvPaperDocumentationRequestController.registerMData(con, act, lst1, 'English');
          } catch (Exception e) {
            
          }
		Test.stopTest();
	}	

    private static void init() {
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;

        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;

        testContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'English1234',
                                                       Document_Version__c = 'V1.1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description',
                                                       Parent_Documentation__c = 'prentDoc');
        insert testContent;

        testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];

        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = 'Acuity']; 

        newWorkspaceDoc =new ContentWorkspaceDoc();
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContents.ContentDocumentId;
        insert newWorkspaceDoc;       

        //ContentWorkspaceDoc testC = [Select c.ContentDocumentId From ContentWorkspaceDoc c  where ContentWorkspace.Name = 'Acuity'];
        //system.debug('#### debug testC = ' + testC); 
    }
	
}
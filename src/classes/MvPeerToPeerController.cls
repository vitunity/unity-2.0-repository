public  without sharing  class MvPeerToPeerController {
    
    public Static Boolean isGuest{get;set;}
    public Static string Usrname{get;set;}
    public Static string shows{get;set;}
    
    
    @AuraEnabled  public static  List<AccountWrapper> getAccountWrappers() {
        shows='none';
        isGuest = SlideController.logInUser();
        List<Account> listAccount=new list<account>();
        List<AccountWrapper> listAccountWrapper =new  List<AccountWrapper>();
        string imgIconstr='';                       //Creating 0,1 for Peer to Peer
        string iconInstitute='';
        List<recordtype> lstRecordType=[select id, name from recordtype where sobjectType='Account' and (name='Site Partner' or name = 'Sold To')];
        listAccount=[select id,name,Website ,Peer_to_Peer__c,Longitude__c,Latitude__c,IMRT__c ,IGRT__c,X3DCRT__c,TrueBeam__c,RapidArc__c,SRS_SBRT__c,Paperless__c,Quality__c,RPM__c,BillingStreet,Phone,BillingState,Billingcity,BillingCountry,BillingPostalCode,SVMXC__Latitude__c, SVMXC__Longitude__c from Account where recordtypeid in : lstRecordType AND (Peer_to_Peer__c='Peer to peer customer' OR Peer_to_Peer__c='Clinical school')];
        for(account acc: listAccount)
        {
            List<String> listIcon=new List<String>();
            if(acc.IMRT__c ==true){iconInstitute='IMRT,'; listIcon.add('IMRT');}
            if(acc.RPM__c ==true){iconInstitute=iconInstitute+'RPM(Gating),'; listIcon.add('RPM(Gating)');}
            if(acc.IGRT__c ==true){iconInstitute=iconInstitute+'IGRT,'; listIcon.add('IGRT');}
            if(acc.RapidArc__c ==true){iconInstitute=iconInstitute+'RapidArc,';listIcon.add('RapidArc');}
            if(acc.Quality__c ==true){ iconInstitute=iconInstitute+'Quality,'; listIcon.add('Quality'); }
            if(acc.Paperless__c ==true){iconInstitute=iconInstitute+'Paperless,';listIcon.add('Paperless');}
            if(acc.SRS_SBRT__c ==true){ iconInstitute=iconInstitute+'SRS/SBRT,';listIcon.add('SRS_SBRT'); }
            if(acc.X3DCRT__c ==true){iconInstitute=iconInstitute+'3DCRT,';listIcon.add('3DCRT');}
            if(acc.TrueBeam__c ==true){iconInstitute=iconInstitute+'TrueBeam,';listIcon.add('TrueBeam');}
            
            iconInstitute=iconInstitute.substring(0,iconInstitute.length()-1);
            
            if(acc.Peer_to_Peer__c.equalsIgnoreCase('Peer to peer customer'))
            { 
                ImgIconstr='1';
            }
            else
            {
                ImgIconstr='0';
            }
            
            String strBillingStreet = acc.BillingStreet;
            if(String.isNotBlank(strBillingStreet) && strBillingStreet.contains('\''))
            {
                strBillingStreet = String.escapeSingleQuotes(strBillingStreet);
            }
            acc.BillingStreet = strBillingStreet;
            listAccountWrapper.add(new AccountWrapper(acc,iconInstitute, ImgIconstr,listIcon)); 
        }
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }  
        
        
        
        return listAccountWrapper;
    }
    
    
    
    //Wrapper Class
    public class AccountWrapper
    {
        @AuraEnabled public Account objAct{get;set;}   
        @AuraEnabled public String icons{get;set;}
        @AuraEnabled public String peerToPeer{get;set;}
        @AuraEnabled public List<String> listIcons{get;set;}
        public AccountWrapper()
        {
            
        }
        public AccountWrapper( Account act, String iconP,String peerP, List<String> tempList)
        {
            objAct=act;
            icons=iconP;
            peerToPeer=peerP;
            listIcons=tempList;
            if(objAct.Latitude__c != null && objAct.Latitude__c.Contains('°')){
                objAct.Latitude__c = objAct.Latitude__c.split('°')[0];
            }
            
            if(objAct.Longitude__c !=null && objAct.Longitude__c.Contains('°')){
                objAct.Longitude__c = objAct.Longitude__c.split('°')[0];
            }
            
            if(objAct.Website != null && !objAct.Website.contains('http')){
                System.debug('The web site is :::'+objAct.Website);
                objAct.Website = 'http://' + objAct.Website;
            }
            
        }
        
        
    }
    
    
    
}
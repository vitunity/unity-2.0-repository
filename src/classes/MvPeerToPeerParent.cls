/*************************************************************************\
@ Author        : Deepak Sharma
@ Date          : 20-Aug-2017
@ Description   : Classs defined to handle Peer To Peer Logic. 
Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************

****************************************************************************/

public class MvPeerToPeerParent {
    
  /**
* @Def: Method Invoked on page load. It setup localization
* @return: Map consisiting of custom label translation
* @Todo : Handle Errors -Depends on Error Handling Framework
* 
***/
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }

}
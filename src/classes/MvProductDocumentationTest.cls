@isTest(seeAllData = true)
private class MvProductDocumentationTest {

    public static Account act;  
    public static Contact con;
    public static User user;

    public static ContentVersion parentContent;
    public static ContentVersion testContent;
    public static ContentVersion testContents;
    public static ContentWorkspace testWorkspace;
    public static ContentWorkspaceDoc newWorkspaceDoc;

    public static User thisUser;
    public static User newUser;
    public static BannerRepository__c banner;
    public static SVMXC__Installed_Product__c objIP;
    public static Product_Version__c pv;

    static
    {

            thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
     
            //pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            //rol = [select id from UserRole LIMIT 1];           

            Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

            final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String randStr = '';
            while (randStr.length() < 10) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               randStr += chars.substring(idx, idx+1);
            }

            act = new Account(name = randStr,BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                        BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
            insert act;  
            con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
                MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
                MailingState='CA', Phone = '12452234',
                MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
            //Creating a running user with System Admin profile
            insert con;

            UserRole rl = [SELECT Id from UserRole Limit 1];

            System.runAs(thisUser) {
                newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                    lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                    username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
                insert newUser;
                Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
                Groupmember gm2 = new groupmember(GroupId = group1.id, UserOrGroupId = newUser.Id);
                insert gm2;                     
            }
            
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Product Documentation' AND sobjecttype='BannerRepository__c'];
            banner =new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert banner;


            Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity');
            insert pr;

            objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Company__c = act.id, SVMXC__Product__c = pr.Id);
            insert objIP;     

            pv = new Product_Version__c(product__c = pr.id);
            insert pv;
            //ProductDocumentController inst = new ProductDocumentController();
            //List<SelectOption> options = inst.getoptions();
            //inst.gtBannerRep('All');
        //}
    }
    
    @isTest static void getCustomLabelMapTest() {
        test.startTest();
            map<String,String> mapTest = MVproductDocumentation.getCustomLabelMap('en');
        test.stopTest();
    }
    
    @isTest static void getPicklistValuesTest() {
        test.startTest();
            List<MVproductDocumentation.picklistEntry> listTest = MVproductDocumentation.getPicklistValues('Industry', 'Account');
        test.stopTest();
    }

    @isTest static void getDocumentsTest() {
        
        test.startTest();
        //thisUser.ContactId = con.Id;

        System.runAs (thisUser) {
            init();
        }

        System.runAs (newUser) {
            List<ContentVersion> listConVer = MVproductDocumentation.getDocuments('Acuity', 'Manual', 'V1.1', 'Test');

            MVproductDocumentation.getlContentVersions('Acuity' , 'Manual', 'V1.1', 'English', 5, 10, 5); 

            MVproductDocumentation.getSwlContentVersions('Acuity', 'Manual', 'V1.1','English', 5, 10, 5); 
            MVproductDocumentation.getProdOptions();
            MVproductDocumentation.getDocTypes('Acuity');
            MVproductDocumentation.getVersions('Acuity');
            MVproductDocumentation.getVersions(null);
            MVproductDocumentation.getlContentVersionsData(testContents.ContentDocumentId);
            MVproductDocumentation.getlContentVersionsDetail('English', testContents.ContentDocumentId);
        }
        test.stopTest();
    }

    @isTest static void getBannerRepTest() {
        test.startTest();
            MVproductDocumentation.getBannerRep('All');
        test.stopTest();
    }

    @isTest static void getProdOptionsTest() {
        
        Test.startTest();
            System.runAs (thisUser) {
                 MVproductDocumentation.getProdOptions();
            }
        Test.stopTest();
    }


    @isTest static void getDocumentsTest2() {
        
        test.startTest();
        //thisUser.ContactId = con.Id;
        //init();
        System.runAs (thisUser) {
            init();
        }

        System.runAs (newUser) {
            MVproductDocumentation.getVersions('Acuity');
            MVproductDocumentation.getVersions(null);
            MVproductDocumentation.getProdOptions();
            MVproductDocumentation.getlContentVersionsData(testContents.ContentDocumentId);
            MVproductDocumentation.getlContentVersionsDetail('English', testContents.ContentDocumentId);
        }
        test.stopTest();
    }


    @isTest static void getDocumentsTest3() {
        
        test.startTest();
        //thisUser.ContactId = con.Id;
        //init();

        delete objIP;
        delete pv;

        System.runAs (thisUser) {
            init();
        }

        System.runAs (newUser) {
            MVproductDocumentation.getVersions('Acuity');
            MVproductDocumentation.getVersions(null);            
            MVproductDocumentation.getDocTypes('Acuity');
            List<ContentVersion> listConVer = MVproductDocumentation.getDocuments('Acuity', 'Manual', 'V1.1', 'Test');

            MVproductDocumentation.getlContentVersions('Acuity' , 'Manual', 'V1.1', 'English', 5, 10, 5); 

            MVproductDocumentation.getSwlContentVersions('Acuity', 'Manual', 'V1.1','English', 5, 10, 5); 

            MVproductDocumentation.getDocTypes(null);
        }
        test.stopTest();
    }

    @isTest static void isCurrentUserInEuroTest() {
        test.startTest();
            MVproductDocumentation.isCurrentUserInEuro();
        test.stopTest();
    }

    @isTest static void getRegionTest() {
        test.startTest();
        System.runAs (newUser) {
            MVproductDocumentation.getRegion();
        }
        test.stopTest();
    }

    @isTest static void getContentVersionListPTTest() {
        test.startTest();
        System.runAs (newUser) {
            MVproductDocumentation.getContentVersionListPT(null, 0, 20, 2);
        }
        test.stopTest();
    }



    private static void init() {
        Id RecId = Schema.SObjectType.ContentVersion.RecordTypeInfosByName.get('Product Document').RecordTypeId;

        parentContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'parentDoc',
                                                       Document_Version__c = 'V1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description Parent',
                                                       Parent_Documentation__c = 'prentDoc');
        insert parentContent;

        testContent = new ContentVersion(Date__c = System.Today(),
                                                       Document_Language__c = 'English',
                                                       Document_Type__c = 'Manual',
                                                       Document_Number__c = 'English1234',
                                                       Document_Version__c = 'V1.1',
                                                       RecordTypeId = RecId,
                                                       Title = 'English',
                                                       ContentURL = 'http://www.google.com/',
                                                       Description = 'English Description',
                                                       Parent_Documentation__c = 'prentDoc');
        insert testContent;

        testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContent.Id and IsLatest=True];

        Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity');
        insert prod;
        
        testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = 'Acuity']; 

        newWorkspaceDoc =new ContentWorkspaceDoc();
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContents.ContentDocumentId;
        insert newWorkspaceDoc;       

        ContentWorkspaceDoc testC = [Select c.ContentDocumentId From ContentWorkspaceDoc c  where ContentWorkspace.Name = 'Acuity' limit 1];
        system.debug('#### debug testC = ' + testC); 
    }
 
}
public class MvSupportContactController {

    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<Support_Contact__c> getContacts() {
      list<Support_Contact__c> sc = new list<Support_Contact__c>();
      list<Support_Contact__c> scEuropeList = new list<Support_Contact__c>();
      sc = Support_Contact__c.getall().values();
      for(Support_Contact__c suppCon : sc){
          if(suppCon.Region__c == 'Europe / Middle East'){
              scEuropeList.add(suppCon);
          }
      }
        scEuropeList.sort();
      return scEuropeList;
    }

    @AuraEnabled
    public static List<Support_Contact__c> getPacificContacts() {
      list<Support_Contact__c> sc = new list<Support_Contact__c>();
      list<Support_Contact__c> scPacificList = new list<Support_Contact__c>();
      sc = Support_Contact__c.getall().values();
      for(Support_Contact__c suppCon : sc){
          if(suppCon.Region__c == 'Asia Pacific'){
              scPacificList.add(suppCon);
          }
      }
        scPacificList.sort();
      return scPacificList;
    }

    @AuraEnabled
    public static List<Support_Contact__c> getAustraliaContacts() {
      list<Support_Contact__c> sc = new list<Support_Contact__c>();
      list<Support_Contact__c> scAustraliaList = new list<Support_Contact__c>();
      sc = Support_Contact__c.getall().values();
      for(Support_Contact__c suppCon : sc){
          if(suppCon.Region__c == 'Australasia'){
              scAustraliaList.add(suppCon);
          }
      }
        scAustraliaList.sort();
      return scAustraliaList;
    }

    @AuraEnabled
    public static List<Support_Contact__c> getLatinAmericaContacts() {
      list<Support_Contact__c> sc = new list<Support_Contact__c>();
      list<Support_Contact__c> scLatinAmericaList = new list<Support_Contact__c>();
      sc = Support_Contact__c.getall().values();
      for(Support_Contact__c suppCon : sc){
          if(suppCon.Region__c == 'Latin America'){
              scLatinAmericaList.add(suppCon);
          }
      }
        scLatinAmericaList.sort();
      return scLatinAmericaList;
    }
}
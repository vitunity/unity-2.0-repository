@isTest(seeAllData=true)
private class MvSupportContactControllerTest {
	
	@isTest static void getCustomLabelMapTest() {
		MvSupportContactController.getCustomLabelMap('EN');
	}
	
	@isTest static void getContactsTest() {
		MvSupportContactController.getContacts();
	}

	@isTest static void getPacificContactsTest() {
		MvSupportContactController.getPacificContacts();
	}

	@isTest static void getAustraliaContactsTest() {
		MvSupportContactController.getAustraliaContacts();
	}

	@isTest static void getLatinAmericaContactsTest() {
		MvSupportContactController.getLatinAmericaContacts();
	}					
}
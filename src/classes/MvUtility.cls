/*
 * My varina utility class
 */
public without sharing class MvUtility {
    
    /*
     *Helper method get custom label translated
     */
    public static map<String,String> getCustomLabelMap(String languageObj){

        system.debug('#### debug languageObj = ' + languageObj);

        /*map<String, String> customLabelMap;
        if(languageObj == null) languageObj = 'en';
        customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get(languageObj);
        if(customLabelMap == null) customLabelMap = ZLabelTranslator.getInstance().customLabelMap.get('en');
        system.debug('custom labels ==='+customLabelMap);
        return customLabelMap;*/
        //above code is old we improved the performance replaced by below code.
        map<String, String> customLabelMap;

        //Check if Preferred language is not in allowed list or not
        Boolean prefLangAllowed = false;
        system.debug('#### debug languageObj = ' + languageObj);
        MV_Languages_Allowed__c allowedLangSet = MV_Languages_Allowed__c.getValues(languageObj);
        if(allowedLangSet != null) {
            prefLangAllowed = true;
        }

        system.debug('#### debug prefLangAllowed = ' + prefLangAllowed);

        //try{
            if(languageObj == null 
                || languageObj.equalsIgnoreCase('undefined')
                || prefLangAllowed == false
                //|| languageObj.equalsIgnoreCase('pt_PT')
               ) 
            {
                languageObj = 'en_US';
            }

            string cacheKey;
            if(languageObj.contains('_')) 
               cacheKey = languageObj.replace('_', '0');
            else
                cacheKey = languageObj;
            
            Map<String,String> cacheMap;

            if(!Test.isRunningTest()) {
                cacheMap = (Map<String,String>)Cache.Org.get(cacheKey);
            }

            if(null != cacheMap && !cacheMap.isEmpty())
            {
                system.debug('**in cacheMap');
                system.debug(logginglevel.error,'++Cache Map:'+JSON.serialize(cacheMap));
                return cacheMap;  
            }
            else
            {
                customLabelMap = ZLabelTranslator.GetLabelTranslation(languageObj);
                if(customLabelMap == null) customLabelMap = ZLabelTranslator.GetLabelTranslation(languageObj);
                system.debug('custom labels ==='+customLabelMap);
                if(!Test.isRunningTest()) {
                    Cache.Org.put(cacheKey,customLabelMap);
                }
                system.debug('**out  cacheMap');
            }
           
        /*}catch(Exception ex){
            system.debug(ex.getMessage());
        }*/
        system.debug('#### debug customLabelMap = ' + JSON.serialize(customLabelMap));
        return customLabelMap;
        
    }
    
    /*
     * Helper send email to debug the code
     */
    public static void sendEmail(List<String> toAddresses,String subject,String mailBody,String senderName)  {  
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        mail.setToAddresses(toAddresses);  
        mail.setSenderDisplayName(senderName);  
        mail.setSubject(subject);  
        mail.setHtmlBody(mailBody);  
        mails.add(mail);  
        Messaging.sendEmail(mails);  
    }
    
    public static Contact getContact(){
        list<Contact> contactList = [select AccountId,Tpaas__c, Local_language_Name__c, Territory__c, OktaId__c, Name, Monte_Carlo_Registered__c, FirstName, LastName, Account.name, Email, Salutation, Functional_Role__c, Specialty__c, PhotoUrl, MvMyFavorites__c,
                                     Accts_Payable_Contact__c, Preferred_Language1__c, Fax, Phone, Manager__c, Manager_s_Email__c, Account.AccountNumber, Account.BillingStreet, Account.BillingCity, Account.BillingCountry, Account.BillingState, Account.BillingPostalCode,
                                     Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity, ShowAccPopUp__c, Account.Ext_Cust_Id__c , Is_Lung_Phantom_Contact__c, Is_MDADL__c,Research_Contact__c, 
                                     Email_Opt_in__c, HasOptedOutOfEmail 
                                     from contact where id in (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId())];

        if(contactList.isEmpty()){
            return new Contact(Is_MDADL__c = false);
        }else{
            return contactList[0];
        }
        
    }   
    
    public static User getUserRecord(){
        return [Select id, Name, MvMyFavorites__c, Subscribed_Products__c, 
                              Contact.Monte_Carlo_Registered__c, LMS_Status__c, Email, Contact.id,
                              SmallPhotoUrl, FullPhotoUrl
                              From User where Id =: UserInfo.getUserId() limit 1][0];
    }

}
@isTest
private class MvUtilityTest{
    @isTest static void test1() {
        test.startTest();        
        MvUtility.getCustomLabelMap('en');
        test.stopTest();
    }
    
     @isTest static void test2() {
        list<string> addressList = new list<string>();
        addressList.add('rishabh.verma@varian.com');
        test.startTest();        
        MvUtility.sendEmail(addressList,'Test Subject','Mail boby','Test Sender');
        test.stopTest();
    }
    
    @isTest static void test3() {
        test.startTest();        
        MvUtility.getContact();
        test.stopTest();
    }
}
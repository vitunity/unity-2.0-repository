/*
* Author: Harvey
* Created Date: 31-Aug-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This will be used for my varian website Events/Webinar page
*/
public without sharing class MvWebinarLibController 
{
    public static List<EventWebinar> liveWebinars = new List<EventWebinar>();
    public static List<EventWebinar> onDemandWebinars = new List<EventWebinar>();
    public static string singleImageUrl = null;
    public static string chkRegion = null;
    public static Event_Webinar__c ew = new Event_Webinar__c();
    public static integer total_all_size; 

    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    @AuraEnabled
    public static List<picklistEntry> getProds()
    {
        List<picklistEntry> prods = new List<picklistEntry>();
        
        List<String> listFinal = new List<String>();
        List<picklistEntry> prodsFinal = new List<picklistEntry>();

        Set<String> selectedItems = new Set<String>();
        for(Event_Webinar__c fw : [Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c, Speaker__c , Title__c, CreatedDate, Time__c, URL__c,Product_Affiliation__c, Region__c,Restrict_access_based_on_product_profile__c
            From Event_Webinar__c e 
            where recordtype.name = 'Webinars'and Active__c = true and private__c = false
            order by From_Date__c desc])
        {
            if(fw.Product_Affiliation__c !=null )
            {
                if(fw.Product_Affiliation__c.contains(';') )
                {
                    List<String> parts = fw.Product_Affiliation__c.split(';');
                    for(String temp:parts)
                    {
                        if (selectedItems.contains(temp))
                        {

                        }
                        else
                        {
                            prods.add(new picklistEntry(temp,temp)); 
                            selectedItems.add(temp);                 
                        }
                    }
                }
                else
                {
                    if(selectedItems.contains(fw.Product_Affiliation__c))
                    {
                    }
                    else
                    {
                        prods.add(new picklistEntry(fw.Product_Affiliation__c, fw.Product_Affiliation__c));
                        selectedItems.add(fw.Product_Affiliation__c);
                    }
                }                             
            }
        }
        //prods.sort();

        for(picklistEntry pk : prods) {
            listFinal.add(pk.label);
        }
        listFinal.sort();

        for(String st : listFinal) {
            prodsFinal.add(new picklistEntry(st, st));
        }        
        return prodsFinal;      //prods;
    }
    
    @AuraEnabled
    public static EventWebinarWrap getOnDemandWebinars(String Product, String Document_TypeSearch, Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        onDemandWebinars.clear();
        Event_Webinar__c[] cn ;
        String searchquery = '';
        List<Event_Webinar__c> RowsWebinar;
        integer counter=0;  //keeps track of the offset
        integer list_size=10; //sets the page size or number of rows
        integer total_size; //used to show user the total size of the list
        String QueryCnt = '';

        if(recordsOffset != null)
        {
            counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null)
        {
            list_size = integer.valueof(pageSize);
        }


        String temp = '%' + Document_TypeSearch + '%';
        
        String dateFormat = 'yyyy-MM-dd';
        Datetime d = System.now();
        String tDay = d.format(dateFormat);

        if(Product != '1Any' && String.isBlank(Document_TypeSearch))
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,Speaker__c , Title__c,Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and From_Date__c < ' + tDay;
            QueryCnt = 'select count() from Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\')' ;
        }
        else if(Product == '1Any' && String.isBlank(Document_TypeSearch))
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,e.Speaker__c , e.Title__c,e.Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\'  and From_Date__c < ' + tDay;
            QueryCnt = 'select count() From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\'' ;
        }
        else if(Product != '1Any' && String.isNotBlank(Document_TypeSearch))
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,Speaker__c , Title__c,Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp) and From_Date__c < ' + tDay;
            QueryCnt = 'select count() From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
        }
        else if(Product == '1Any' && String.isNotBlank(Document_TypeSearch))
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,e.Speaker__c , e.Title__c,e.Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp) and From_Date__c < ' + tDay;
            QueryCnt = 'select count() From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
        }

        total_size = DataBase.countQuery(QueryCnt);
		system.debug('total_size=='+total_size);
        system.debug('counter=='+counter);
        if(counter < 2000)
        {
            if (counter == total_size && total_size != 0)
            {
                searchquery = searchquery  + ' order by From_Date__c desc limit ' + list_size + ' offset ' + (counter - list_size);
            }
            else
            {
                searchquery = searchquery + ' order by From_Date__c desc limit ' + list_size + ' offset ' + counter;
            }
        }
        else
        {
            searchquery = searchquery  + ' order by From_Date__c desc limit ' + counter + 10;  
        }

        RowsWebinar = DataBase.Query(searchquery);


        for(Event_Webinar__c st : RowsWebinar)
        {
            if(st.VU_Speaker__r.size() > 0)
            {
                onDemandWebinars.add(new EventWebinar(st.Id, st.VU_Speaker__r[0].Speaker_Image_del__c, st.Title__c, st.Speaker__c, st.From_Date__c, st.Institution__c));
            }
            else
            {
                onDemandWebinars.add(new EventWebinar(st.Id, '', st.Title__c, st.Speaker__c, st.From_Date__c,st.Institution__c));
            }
        }
        /*
        for(Event_Webinar__c st : RowsWebinar)
        {
            if(st.From_Date__c >= System.today())
            {
                if(st.VU_Speaker__r.size() > 0)
                {
                    onDemandWebinars.add(new EventWebinar(st.Id, st.VU_Speaker__r[0].Speaker_Image_del__c, st.Title__c, st.Speaker__c, st.From_Date__c));
                }
                else
                {
                    onDemandWebinars.add(new EventWebinar(st.Id, '', st.Title__c, st.Speaker__c, st.From_Date__c));
                }
            }
        }   
        */
        EventWebinarWrap ew = new EventWebinarWrap(onDemandWebinars, total_size);
        return ew; 
    } 

    @AuraEnabled
    public static List<EventWebinar> getLiveWebinars(String Product, String Document_TypeSearch)
    {
        liveWebinars.clear();
        Event_Webinar__c[] cn ;
        String searchquery = '';
        List<Event_Webinar__c> RowsWebinar;


        String temp = '%' + Document_TypeSearch + '%';
        
        if(Product != '1Any')
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,Speaker__c , Title__c,Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and Product_Affiliation__c INCLUDES (\'' + Product+ '\') and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
        }
        else
        {
            searchquery = 'Select e.Institution__c, e.From_Date__c,e.Speaker__c , e.Title__c,e.Product_Affiliation__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where Active__c = true and private__c = false and recordtype.name =\'Webinars\' and (Institution__c like :temp OR Speaker__c like :temp OR Title__c like :temp)' ;
        }

        RowsWebinar = DataBase.Query(searchquery);
        /*
        for(Event_Webinar__c st : RowsWebinar)
        {
            if(st.From_Date__c < System.today())
            {
                if(st.VU_Speaker__r.size() > 0)
                {
                    liveWebinars.add(new EventWebinar(st.Id, st.VU_Speaker__r[0].Speaker_Image_del__c, st.Title__c, st.Speaker__c, st.From_Date__c));
                }
                else
                {
                    liveWebinars.add(new EventWebinar(st.Id, '', st.Title__c, st.Speaker__c, st.From_Date__c));
                }
            }
        }
        */
        for(Event_Webinar__c st : RowsWebinar)
        {
            if(st.From_Date__c >= System.today())
            {
                if(st.VU_Speaker__r.size() > 0)
                {
                    liveWebinars.add(new EventWebinar(st.Id, st.VU_Speaker__r[0].Speaker_Image_del__c, st.Title__c, st.Speaker__c, st.From_Date__c,st.Institution__c));
                }
                else
                {
                    liveWebinars.add(new EventWebinar(st.Id, '', st.Title__c, st.Speaker__c, st.From_Date__c,st.Institution__c));
                }
            }
        }   

        return liveWebinars; 
    } 
   
    @AuraEnabled
    public static Event_Webinar__c getLiveWebinar(String Id)
    {
        fetchSignleWebinar(Id);
        return ew;
    } 

    @AuraEnabled
    public static String getLiveWebinarUrl(String Id)
    {
        fetchSignleWebinar(Id);
        return singleImageUrl;
    } 
   
    @AuraEnabled
    public static List<EventWebinar> fetchLiveWebinars(Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        fetchWebinars(recordsOffset, totalSize, pageSize);
        return liveWebinars; 
    }

    @AuraEnabled
    public static EventWebinarWrap fetchOnDemandWebinars(Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        fetchWebinars(recordsOffset, totalSize, pageSize);
        EventWebinarWrap ew = new EventWebinarWrap(onDemandWebinars, total_all_size);
        return ew; 
    }

    public static void fetchSignleWebinar(String Id)
    {
        List<Event_Webinar__c> webs = [Select Location__c, Institution__c, Needs_MyVarian_registration__c, 
                                       URL_Path_Settings__c, From_Date__c, Description__c, Speaker__c , 
                                       Title__c, CreatedDate, Time__c,Speaker_Bio__c, Who_Should_Attend__c,Summary__c,
                                       URL__c, (select Speaker_Image_del__c from VU_Speaker__r) 
                                       from Event_Webinar__c where Id =: Id];
        if(webs.size() > 0){ 
            if(webs[0].VU_Speaker__r.size() > 0)
            {
                singleImageUrl = webs[0].VU_Speaker__r[0].Speaker_Image_del__c;
                webs[0].Store_Image_url__c = singleImageUrl;
            }
            if(webs[0].Description__c != null) {
                webs[0].Summary__c = webs[0].Description__c.replaceAll('\\<.*?\\>', '');
            }
            ew = webs[0];
        }
    }

    public static void fetchWebinars(Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
        String chkRegion = null;
        String shows = 'none';
        integer counter=0;  //keeps track of the offset
        integer list_size=10; //sets the page size or number of rows
        String QueryCnt = '';
        total_all_size = 0;
        onDemandWebinars.clear();

        if(recordsOffset != null)
        {
            counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null)
        {
            list_size = integer.valueof(pageSize);
        }


        if(conCntry != null)
        {
            chkRegion = [Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry limit 1].Portal_Regions__c;
        }
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense)
            {
                //Usrname = [Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                //shows='block';
            }
        }

        set<String> productGroupSet = new set<String>();
        ProductGroupSet = CpProductPermissions.fetchProductGroup(); 

        String Query=''; 
        List<Event_Webinar__c> RowsWebinar= new List<Event_Webinar__c>();
       
        Query = 'Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,Speaker__c , Title__c, CreatedDate, Time__c, URL__c,Product_Affiliation__c, Region__c,Restrict_access_based_on_product_profile__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where recordtype.name =\'Webinars\'   and Active__c = true and private__c = false order by From_Date__c desc';

        RowsWebinar=DataBase.Query(Query);

        for(Event_Webinar__c fw : RowsWebinar)
        {
           
           if(fw.Product_Affiliation__c != null && fw.Product_Affiliation__c.contains('Government Affairs')){continue;}
          
           if(fw.From_Date__c >= System.today())
           {
                if(chkRegion==null)
                { //&& fw.Restrict_access_based_on_product_profile__c == false)
                    if(fw.Restrict_access_based_on_product_profile__c == false && shows!='block')
                    if(fw.VU_Speaker__r.size() > 0)
                    {
                        liveWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                    else
                    {
                        liveWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                    if(shows =='block' )
                    if(fw.VU_Speaker__r.size() > 0)
                    {
                        liveWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                    else
                    {
                        liveWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                }
                else
                {
                    if(fw.Region__c != null)
                    {
                        if(fw.Region__c.contains('All') || fw.Region__c.contains(chkRegion))
                        if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block' )
                        if(fw.VU_Speaker__r.size() > 0)
                        {
                            liveWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c, fw.Institution__c));
                        }
                        else
                        {
                            liveWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c, fw.Institution__c));
                        }
                        if(fw.Restrict_access_based_on_product_profile__c == true && shows !='block'  )
                        {
                            for(String Str:ProductGroupSet)
                            {
                                if(fw.Product_Affiliation__c.contains(str))
                                {
                                    if(fw.VU_Speaker__r.size() > 0)
                                    {
                                        liveWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                                    }
                                    else
                                    {
                                        liveWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c, fw.Institution__c));
                                    }
                                    break;
                                }
                            }
                        }  
                    }
                }
           }                                      
        }
        List<Event_Webinar__c> RowsWebinarOnDemand = new List<Event_Webinar__c>();
        String dateFormat = 'yyyy-MM-dd';
        Datetime d = System.now();
        String tDay = d.format(dateFormat);

        Query = 'Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,Speaker__c , Title__c, CreatedDate, Time__c, URL__c,Product_Affiliation__c, Region__c,Restrict_access_based_on_product_profile__c, (select Speaker_Image_del__c from VU_Speaker__r) From Event_Webinar__c e where recordtype.name =\'Webinars\' and Active__c = true and private__c = false and e.From_Date__c <= ' + tDay;
        QueryCnt = 'select count() from Event_Webinar__c e where recordtype.name =\'Webinars\' and Active__c = true and private__c = false and e.From_Date__c <= ' + tDay;
        total_all_size = DataBase.countQuery(QueryCnt);

        if(counter < 2000)
        {
            if (counter == total_all_size)
            {
                Query = Query  + ' order by From_Date__c desc limit ' + list_size + ' offset ' + (counter - list_size);
            }
            else
            {
                Query = Query + ' order by From_Date__c desc limit ' + list_size + ' offset ' + counter;
            }
        }
        else
        {
            Query = Query  + ' order by From_Date__c desc limit ' + counter + 10;  
        }

        System.debug('Query***: '+Query);
        RowsWebinarOnDemand=DataBase.Query(Query);

        for(Event_Webinar__c fw : RowsWebinarOnDemand)
        {
           
            if(fw.Product_Affiliation__c != null && fw.Product_Affiliation__c.contains('Government Affairs')){continue;}

            System.debug('CheckRegin'+chkRegion);
            if(chkRegion==null)
            {// && fw.Restrict_access_based_on_product_profile__c == false)
                if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block' )
                {
                    if(fw.VU_Speaker__r.size() > 0)
                    {
                        onDemandWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                    else
                    {
                        onDemandWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                }

                if(shows =='block' )
                {
                    if(fw.VU_Speaker__r.size() > 0)
                    {
                        onDemandWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                    else
                    {
                        onDemandWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                    }
                }
            }
            else
            {
                if(fw.Region__c != null)
                {
                    if(fw.Region__c.contains('All') || fw.Region__c.contains(chkRegion))
                    {
                        if(fw.Restrict_access_based_on_product_profile__c == false && shows !='block'  )
                        {
                            if(fw.VU_Speaker__r.size() > 0)
                            {
                                onDemandWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                            }
                            else
                            {
                                onDemandWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                            }
                        }
                    }
               
                    if(fw.Restrict_access_based_on_product_profile__c == true && shows !='block'  )
                    {
                        for(String Str:ProductGroupSet)
                        {
                            if(fw.Product_Affiliation__c.contains(str))
                            {
                                if(fw.VU_Speaker__r.size() > 0)
                                {
                                    onDemandWebinars.add(new EventWebinar(fw.Id, fw.VU_Speaker__r[0].Speaker_Image_del__c, fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                                }
                                else
                                {
                                    onDemandWebinars.add(new EventWebinar(fw.Id, '', fw.Title__c, fw.Speaker__c, fw.From_Date__c,fw.Institution__c));
                                }
                                break;
                            }
                        }
                    }   
                }
            }               
        }
    }

    public class EventWebinar
    {
        @AuraEnabled public String eid {get; set;}
        @AuraEnabled public String institution {get; set;}
        @AuraEnabled public String imageUrl {get; set;}
        @AuraEnabled public String Title {get; set;}
        @AuraEnabled public String Speaker {get; set;}
        @AuraEnabled public Date FromDate {get; set;}
        public EventWebinar(String id, String url, String tit, String spe, Date fdate, String institution)
        {
            eid = id;
            imageUrl = url;
            Title = tit;
            Speaker = spe;
            FromDate = fdate;
            this.institution = institution;
        }
    }

    public class EventWebinarWrap
    {      
        @AuraEnabled public list<EventWebinar> lstWebinarWrap{get;set;}
        @AuraEnabled public integer totalSize{get;set;}
        public EventWebinarWrap(list<EventWebinar> lstWebinarWrap, integer totalSize){
            this.lstWebinarWrap = lstWebinarWrap;
            this.totalSize = totalSize;
        }
    }

    /*
    public class PicklistEntry{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string pvalue{get;set;}
        public picklistEntry(string text, string value){
            label = text;
            pvalue = value;
        }
    }
    */

    public class picklistEntry implements Comparable{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string pvalue{get;set;}
        public picklistEntry(string text, string pvalue){
            this.label = text;
            this.pvalue = pvalue;
        }
        
        public Integer compareTo(Object compareTo) {
            picklistEntry compareToOppy = (picklistEntry)compareTo;
            Integer returnValue = 0;
            if (pvalue > compareToOppy.pvalue)
                returnValue = 1;
            else if (pvalue < compareToOppy.pvalue)
                returnValue = -1;        
            return returnValue;       
        }
    }      
}
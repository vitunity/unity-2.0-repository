@isTest (seeAllData=true)
private class MvWebinarLibControllerTest
{
    public static Regulatory_Country__c regcount ;
    public static Contact_Role_Association__c cr;
    public static Profile p;
    public static User u;
    public static Contact con;
    public static Event_Webinar__c Webinar;
    static
    {
        Account a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='USA',BillingState='Washington',BillingStreet='xyx',Country__c='USA'  ); 
        insert a;  

        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
 
        con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.testh78966743565@gmail.com', AccountId=a.Id,MailingCountry ='Barbados', MailingState='teststate1' ); 
        insert con;
        u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testclass.com'/*,UserRoleid=r.id*/); 
        
        insert u; // Inserting Portal User

        regcount = new Regulatory_Country__c();
        regcount.Name = 'Test';
        regcount.RA_Region__c ='NA';
        regcount.Portal_Regions__c ='N.America,Test';
        insert regcount;

        cr = new Contact_Role_Association__c();
        cr.Account__c= a.Id;
        cr.Contact__c = u.Contactid;
        cr.Role__c = 'RAQA';
        insert cr;
        RecordType RT = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars'];
        Webinar = new Event_Webinar__c();
        Webinar.RecordTypeId = RT.Id; 
        Webinar.Title__c = 'Test Webinar';
        Webinar.Product_Affiliation__c = '4DITC';
        Webinar.Location__c = 'NY';
        Webinar.To_Date__c = system.today();
        Webinar.From_Date__c = system.today();
        Webinar.Needs_MyVarian_registration__c=False;
        Webinar.Summary__c = 'test summary';
        Webinar.Description__c = 'test Description';
       insert Webinar;
    }

    @isTest static void getCustomLabelMapTest() {
        test.startTest();
            map<String,String> mapTest = MvWebinarLibController.getCustomLabelMap('en');
        test.stopTest();
    }

    static testMethod void testGetProds() 
    {
        Test.starttest();
        MvWebinarLibController.getProds();
        Test.stoptest();
    }

    static testMethod void testGetOnDemandWebinars()
    {
        Test.starttest();
        
        MvWebinarLibController.getOnDemandWebinars('1Any', 'Sessions', 5, 5 ,5);
        MvWebinarLibController.getOnDemandWebinars('Eclipse', 'Sessions', 5,5,5);

        Test.stoptest();
    }

    static testMethod void testGetLiveWebinars() 
    {
        Test.starttest();
        System.runAs(u)
        {
            MvWebinarLibController.getLiveWebinars('1Any', 'Sessions');
            MvWebinarLibController.getLiveWebinars('Eclipse', 'Sessions');
        }
        Test.stoptest();
    }

    static testMethod void testGetLiveWebinar()
    {
        Test.starttest();
        MvWebinarLibController.getLiveWebinar(Webinar.Id);
        Test.stoptest();
    }
    static testMethod void testFetchLiveWebinars()
    {
        Test.starttest();
        MvWebinarLibController.fetchLiveWebinars(5,5,5);
        System.runAs(u)
        {
            MvWebinarLibController.fetchLiveWebinars(5,5,5);
        }
        Test.stoptest();
    }
    static testMethod void testFetchOnDemandWebinars()
    {
        Test.starttest();
        MvWebinarLibController.fetchOnDemandWebinars(5,5,5);
        System.runAs(u)
        {
            MvWebinarLibController.fetchLiveWebinars(5,5,5);
        }
        Test.stoptest();
    }
}
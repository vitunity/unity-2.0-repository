public with sharing class MyVarianAdminToolTabController {
	public String tabSelected{get;set;}
	
	public MyVarianAdminToolTabController(){
		tabSelected = Apexpages.currentPage().getParameters().get('tabSelected');
		system.debug('***tabSelected***'+tabSelected);
		if(tabSelected == null){
			tabSelected = 'accountTool';
		}
	}
}
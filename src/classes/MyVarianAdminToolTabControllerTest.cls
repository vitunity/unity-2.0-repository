@isTest
private class MyVarianAdminToolTabControllerTest {

    static testMethod void myUnitTest() {
    	Apexpages.currentPage().getParameters().put('tabSelected','accountTool');
        MyVarianAdminToolTabController controller = new MyVarianAdminToolTabController();
        system.assertEquals(controller.tabSelected,'accountTool');
    }
}
public class MyVarianDupe{
    public static void UpdateDupes(map<Id, Contact> newmap, map<Id, Contact> oldmap){        
        set<string> setLastName = new set<string>();
        map<string,Contact> mapDupContact = new map<string,Contact>();
        map<Id,Id> mapAccountIds =  new map<Id,Id>();
        
        
        for(Contact con : newmap.values()){
            if(con.Registered_Portal_User__c == true && con.AccountId <> null && oldmap <> null && oldmap.get(Con.Id).AccountId == null){
                string firstName = '';
                if(con.firstName <> null){firstName = (con.firstName).substring(0,1);}
                setLastName.add(con.lastName);
                string key = firstName+'-'+con.lastName;
                key = key.toUpperCase();
                mapDupContact.put(key,con);
                mapAccountIds.put(con.AccountId,con.Id);
            }
        }
        map<Id,set<string>> mapAccountContacts = new map<Id,set<string>>();
        for(contact con : [select Id,FirstName,LastName,Dupes__c,MyVarian_Member__c ,AccountId  from Contact where  LastName IN: setLastName and AccountId IN: mapAccountIds.keyset()]){   //MyVarian_Member__c  = true and
            if(mapAccountIds.containsKey(con.AccountId) && mapAccountIds.get(con.AccountId) <> con.ID){
                string firstName = '';
                if(con.firstName <> null){firstName = (con.firstName).substring(0,1);}
                string key =firstName+'-'+con.lastName;
                key = key.toUpperCase();

                set<string> setTemp = new set<string>();
                setTemp.add(key);
                if(mapAccountContacts.containsKey(con.AccountId)){
                    setTemp.addAll(mapAccountContacts.get(con.AccountId));
                }
                mapAccountContacts.put(con.AccountId,setTemp);
            }
        }
            
        for(string key : mapDupContact.keyset()){
            Contact con =  mapDupContact.get(key);
            if(mapAccountContacts.containsKey(con.AccountId) && (mapAccountContacts.get(con.AccountId)).contains(key)){
                con.Dupes__c = true;
            }
        }
    }    
}
@isTest
Public class MyVarianDupeTest
{
   public static testmethod void MyVarianDupe()   
    {
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
            User u;
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            
            Account a;      
            Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
            a = new Account(name='test79872',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con1=new Contact(AccountId  = a.ID,Lastname='test',FirstName='Singh',MyVarian_Member__c= true,Email='kusdadmar.amit667@gmail.com', MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '1214445');
            insert con1;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Registered_Portal_User__c  = true,Email='kumar.amit667@gmail.com', MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '1214445');
            insert con;
                        
            con.AccountId = a.Id;
            update con;
            
            
        }
    }
}
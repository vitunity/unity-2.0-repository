/**
 *	@CreatedBy	 : Puneet Mishra
 *	@Createddate : 25 Apr 2017
 *	@Description : Parser Class for response returned by Okta
 */

public class MyVarianJSONParser {
	public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}

	public String id {get;set;} 
	public String userId {get;set;} 
	public String login {get;set;} 
	public String createdAt {get;set;} 
	public String expiresAt {get;set;} 
	public String status {get;set;} 
	public String lastPasswordVerification {get;set;} 
	public List<String> amr {get;set;} 
	public Idp idp {get;set;} 
	public Boolean mfaActive {get;set;} 
	public String cookieToken {get;set;} 
	
	public MyVarianJSONParser(JSONParser parser) {
		while (parser.nextToken() != JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != JSONToken.VALUE_NULL) {
					if (text == 'id') {
						id = parser.getText();
					} else if (text == 'userId') {
						userId = parser.getText();
					} else if (text == 'login') {
						login = parser.getText();
					} else if (text == 'createdAt') {
						createdAt = parser.getText();
					} else if (text == 'expiresAt') {
						expiresAt = parser.getText();
					} else if (text == 'status') {
						status = parser.getText();
					} else if (text == 'lastPasswordVerification') {
						lastPasswordVerification = parser.getText();
					} else if (text == 'amr') {
						amr = new List<String>();
						while (parser.nextToken() != JSONToken.END_ARRAY) {
							amr.add(String.valueOf(parser));
						}
					} else if (text == 'idp') {
						idp = new Idp(parser);
					} else if (text == 'mfaActive') {
						mfaActive = parser.getBooleanValue();
					} else if (text == 'cookieToken') {
						cookieToken = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'MyVarianJSONParser consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	public class User {
		public String name {get;set;} 
		public String href {get;set;} 
		public Hints hints {get;set;} 

		public User(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'name') {
							name = parser.getText();
						} else if (text == 'href') {
							href = parser.getText();
						} else if (text == 'hints') {
							hints = new Hints(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'User consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Idp {
		public String id {get;set;} 
		public String type_Z {get;set;} // in json: type

		public Idp(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'id') {
							id = parser.getText();
						} else if (text == 'type') {
							type_Z = parser.getText();
						} else {
							System.debug(LoggingLevel.WARN, 'Idp consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Hints {
		public List<String> allow {get;set;} 

		public Hints(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'allow') {
							allow = new List<String>();
							while (parser.nextToken() != JSONToken.END_ARRAY) {
								allow.add(String.valueOf(parser));
							}
						} else {
							System.debug(LoggingLevel.WARN, 'Hints consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	public class Self {
		public String href {get;set;} 
		public Hints hints {get;set;} 

		public Self(JSONParser parser) {
			while (parser.nextToken() != JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != JSONToken.VALUE_NULL) {
						if (text == 'href') {
							href = parser.getText();
						} else if (text == 'hints') {
							hints = new Hints(parser);
						} else {
							System.debug(LoggingLevel.WARN, 'Self consuming unrecognized property: '+text);
							consumeObject(parser);
						}
					}
				}
			}
		}
	}
	
	
	public static MyVarianJSONParser parse(String json) {
		return new MyVarianJSONParser(System.JSON.createParser(json));
	}
}
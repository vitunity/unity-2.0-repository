/**
 *  @author:        Puneet Mishra
 *  @description:   test class for MyVarianJSONParser
 */
@IsTest
public class MyVarianJSONParser_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '{\"id\":\"102pULAB_f_RcGmimJjcPgemA\",\"userId\":\"00u9v6mbprfnJAZGI0h7\",\"login\":\"publicmail@mailinator.com\",\"createdAt\":\"2017-07-19T07:25:08.000Z\",\"expiresAt\":\"2017-07-19T09:25:08.000Z\",\"status\":\"ACTIVE\",\"lastPasswordVerification\":\"2017-07-19T07:25:08.000Z\",\"lastFactorVerification\":null,\"amr\":[\"pwd\"],\"idp\":{\"id\":\"00o326179iQIZPLZOAIO\",\"type\":\"OKTA\"},\"mfaActive\":false,\"cookieToken\":\"201118ft8YLmEuDohcgHq81Cq5IU5l5SFq4I60r5HoNadqidToNP-HE\",\"_links\":{\"self\":{\"href\":\"https://varian.oktapreview.com/api/v1/sessions/102jG6xuWslQgyFnbpisUdBnQ\",\"hints\":{\"allow\":[\"GET\",\"DELETE\"]}},\"refresh\":{\"href\":\"https://varian.oktapreview.com/api/v1/sessions/102jG6xuWslQgyFnbpisUdBnQ/lifecycle/refresh\",\"hints\":{\"allow\":[\"POST\"]}},\"user\":{\"name\":\"Myvarian New\",\"href\":\"https://varian.oktapreview.com/api/v1/users/00u9v6mbprfnJAZGI0h7\",\"hints\":{\"allow\":[\"GET\"]}}}}';
        MyVarianJSONParser r = MyVarianJSONParser.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        MyVarianJSONParser.User objUser = new MyVarianJSONParser.User(System.JSON.createParser(json));
        System.assert(objUser != null);
        System.assert(objUser.name == null);
        System.assert(objUser.href == null);
        System.assert(objUser.hints == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        MyVarianJSONParser.Idp objIdp = new MyVarianJSONParser.Idp(System.JSON.createParser(json));
        System.assert(objIdp != null);
        System.assert(objIdp.id == null);
        System.assert(objIdp.type_Z == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        MyVarianJSONParser.Hints objHints = new MyVarianJSONParser.Hints(System.JSON.createParser(json));
        System.assert(objHints != null);
        System.assert(objHints.allow == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        MyVarianJSONParser.Self objSelf = new MyVarianJSONParser.Self(System.JSON.createParser(json));
        System.assert(objSelf != null);
        System.assert(objSelf.href == null);
        System.assert(objSelf.hints == null);
    }
}
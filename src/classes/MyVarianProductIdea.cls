public class MyVarianProductIdea{
    public Boolean isGuest{get;set;}
    public string shows{get;set;}
    public string Usrname{get;set;}
    public MyVarianProductIdea(){
        shows = 'none';
        isGuest = SlideController.logInUser();
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }     
    }
}
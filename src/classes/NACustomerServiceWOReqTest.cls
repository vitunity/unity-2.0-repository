@isTest
private class NACustomerServiceWOReqTest 
{

     static testMethod void ServerGraphIntergrationTest() 
    {
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        //Insert Account
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;        
        System.assertNotEquals(testAcc.Id, null);
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='US' ; 
        insert reg ;
        
        //Insert Contact  
        Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id,MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
        insert testcon;
        System.assertNotEquals(testcon.Id, null);
        
        // insert Install Product
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H123456', SVMXC__Contact__c = testcon.Id, SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id);
        insert objIP;
        System.assertNotEquals(objIP.Id, null);
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();

        email.fromName = 'FirstName LastName';
        email.fromAddress = 'rakesh@test.com';
        email.subject = 'WO Request – NA Customer Service Work Order Request';
        email.htmlBody = 'test';
        NACustomerServiceWorkOrderRequest handler = new NACustomerServiceWorkOrderRequest();
        Messaging.InboundEmailResult result = handler.handleInboundEmail(email, envelope);

        System.assertEquals( result.success  ,true);


        List<Case> testCases = new List<Case>([SELECT  Subject, ProductSystem__c FROM Case]);
        System.assertEquals(testCases.size(), 1);
       // System.assertEquals(testcases.get(0).ProductSystem__c, objIP.Id);
    }
    
}
global class NACustomerServiceWorkOrderRequest implements Messaging.InboundEmailHandler 
{
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
         Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{
          
        Boolean newFormat = True;
        Boolean offline = false;
        Integer priorityNum = 0;
        String SNStr = '';
        String SNContactId = '';
        
            Id HDRecordtype = [Select id from recordtype where sObjecttype = 'Case' and developername = 'Helpdesk'].id;
            String lstSubjectForReplace = email.subject;
            String lstDescription = email.HTMLBody;
            
            IF(String.isNotBlank(lstDescription)) {
            lstDescription = email.HTMLBody;
                }
            ELSE {
            lstDescription = email.plainTextBody;
            }
    
            String EscapeSubject = lstSubjectForReplace.unescapeXml();
            string caseSubject = 'WO Request – '+ EscapeSubject; 
            
            string WOId;
            if(EscapeSubject.contains(' - '))  
                WOId = EscapeSubject.split(' - ')[1];
           // if(EscapeSubject.contains(' – '))
            //    WOId = EscapeSubject.split(' – ')[1];
            
            List<SVMXC__Installed_Product__c> ListofInstalledProduct=[Select Id,SVMXC__Contact__c, SVMXC__Contact__r.Phone, SVMXC__Company__c,SVMXC__Company__r.RecordType.DeveloperName,SVMXC__Preferred_Technician__c from SVMXC__Installed_Product__c where name=:WOID];
            
            
            Case casealias = new Case();
            casealias.recordtypeId = HDRecordtype;
            //casealias.ownerID = '00GE0000002M9iLMAS';
            casealias.Origin = 'Internal';
            casealias.Subject= caseSubject; //+ system.now();
            casealias.local_Subject__c = caseSubject;
         
            for(SVMXC__Installed_Product__c IP : ListofInstalledProduct )
            {
                if(IP.SVMXC__Company__r.RecordType.DeveloperName=='Site_Partner')
                {
                    system.debug('asdf1'+IP);
                    casealias.AccountId=IP.SVMXC__Company__c;
                    casealias.ContactId = IP.SVMXC__Contact__c;
                }
                casealias.ProductSystem__c=IP.Id;
            }   

            casealias.Local_Internal_Notes__c = lstDescription.unescapeXml();
            casealias.Priority='Medium';
            
            casealias.Is_This_a_Complaint__c='Yes';
            casealias.Is_escalation_to_the_CLT_required__c = 'No';
            casealias.Was_anyone_injured__c = 'No';        
            casealias.Smart_Connect_Case__c=True;
            
            // For assignment rule execution
            List<AssignmentRule> AR = new List<AssignmentRule>();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
            if(AR.size()>0)
            {
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.assignmentRuleId= AR[0].id;
                casealias.setoptions(dmo);
            }
            
            Insert casealias;

        

        }catch(Exception e){system.debug('#EMAILSERVICE:'+e);}
        return result;
    }
}
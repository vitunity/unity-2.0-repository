/*
* Author: Rishabh Verma
* Created Date: 10-Oct-2017
* Project/Story/Inc/Task : Non Varian Installed product/ lightning project 
 * Description: This will be used to create non varian installed product tree view based on product type.
*/
public class NonVarianIPTreeViewContrlLtg {
    
    private map<String,List<Competitor__c>> mapCompetitors;
    
    public NonVarianIPTreeViewContrlLtg(ApexPages.StandardController sc) {
        mapCompetitors = new Map<String,List<Competitor__c>>();
        for(Competitor__c compObj :  [select id, Name, Competitor_Account_Name__c, 
             Version__c, Status__c, Vault_identifier__c, 
             No_of_Licences_OIS__c, No_of_Units__c, No_of_Workstations_TPS__c, 
             No_Treatment_Units__c, Afterloader_type__c, Age_In_Years__c, 
             CreatedDate, Channels__c, Compatability__c, Dedicated_or_Shared__c,
             Energy_Source__c, Model__c, Product_Type__c, 
             Service_Provider__c, Vendor__c 
             from Competitor__c
             where Competitor_Account_Name__c =: sc.getId()]){
             system.debug(' ==== compObj.Product_Type__c==== ' + compObj.Product_Type__c);
             if(mapCompetitors.containsKey(compObj.Product_Type__c)){
                 mapCompetitors.get(compObj.Product_Type__c).add(compObj);
             }else{
                 list<Competitor__c> compList = new list<Competitor__c>();
                 compList.add(compObj);
                 mapCompetitors.put(compObj.Product_Type__c,compList);
             }                             
        }
        system.debug(' ==== mapCompetitors ==== ' + mapCompetitors);
    }
    
    public map<String,List<Competitor__c>> getMapCompetitors(){
        return mapCompetitors;
    }
}
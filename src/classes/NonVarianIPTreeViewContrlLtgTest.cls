@isTest
public class NonVarianIPTreeViewContrlLtgTest{

public static Competitor__c comp;
public static Account accnt;

static
{

    Recordtype rt=[select id from Recordtype where Name='DS' and SobjectType = 'Competitor__c'];

    accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;
    
    comp = new Competitor__c();
    comp.recordTypeid=rt.id;
    comp.Competitor_Account_Name__c = accnt.id;
    comp.Product_Type__c = 'Delivery Systems';
    insert comp;
    
}


Static TestMethod void testmethod1()
{
ApexPages.StandardController sc = new ApexPages.StandardController(accnt);
NonVarianIPTreeViewContrlLtg nip = new NonVarianIPTreeViewContrlLtg(sc);
nip.getMapCompetitors();
}
}
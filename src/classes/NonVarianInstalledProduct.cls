/**
 *	@author			:		Shital Bhujbal
 * 	@description	:		class used on Competitor section on Lightning Opportunity Page, used in CreateOpportunityLightningCtrl
 * 
 */
public class NonVarianInstalledProduct {
    @AuraEnabled public Integer rowNumber {get;set;}
    @AuraEnabled public Competitor__c nonVarianProduct {get;set;}
    @AuraEnabled public List<String> vendorDependentProductNameList {get;set;}
    @AuraEnabled public List<String> vendorList{get;set;}
    @AuraEnabled public Map<String, List<String>> productNameListTEST {get;set;}
    
    public NonVarianInstalledProduct(Integer rowNumber, Competitor__c nonVarianProduct, List<String> productTypeVal,
                                     Map<String, List<String>> vendorMap) {
        this.rowNumber = rowNumber;
        this.nonVarianProduct = nonVarianProduct;
        this.vendorDependentProductNameList = new List<String>();
        if(productTypeVal.isEmpty()) {
          this.vendorList = new List<String>();
            this.productNameListTEST = new Map<String, List<String>>();
        }else {
            this.vendorList = productTypeVal;
            this.productNameListTEST = vendorMap;
        }
		if(vendorMap.containsKey(nonVarianProduct.Vendor__c) && nonVarianProduct.Vendor__c != '') {
        	this.vendorDependentProductNameList = vendorMap.get(nonVarianProduct.Vendor__c);
        }
    system.debug(' === vendorMap === ' + vendorMap);
    }   
}
@isTest
public class NonVarianInstalledProductTest {
    
    public static List<Id> recordTypeId = new List<String>();
    public static Map<Id, String> vendorMap = new Map<Id, String>();
    public static Map<Id, String> ModelMap = new Map<Id, String>();
    public static Account acct;
    public static Regulatory_Country__c regulatory;
    public static Contact con;
    public static SVMXC__Installed_Product__c ip;
    public static Opportunity salesOpp;
    
    static {
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Brachytherapy Afterloaders').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Delivery Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Image Management Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Oncology Information Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Other 3rd Party Connected Devices').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Treatment Planning Solutions').getRecordTypeId());        
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('3rd Party Service Contracts').getRecordTypeId());
        
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Brachytherapy Afterloaders').getRecordTypeId(), 'ACE');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Delivery Systems').getRecordTypeId(), 'AlignRT');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Image Management Systems').getRecordTypeId(), 'Allscripts');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Oncology Information Systems').getRecordTypeId(), 'Artiste');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Other 3rd Party Connected Devices').getRecordTypeId(), 'Atlantis');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Treatment Planning Solutions').getRecordTypeId(), 'Axesse');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('3rd Party Service Contracts').getRecordTypeId(), 'Axxent');
        
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Brachytherapy Afterloaders').getRecordTypeId(), 'Accuray');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Delivery Systems').getRecordTypeId(), 'AGAT');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Image Management Systems').getRecordTypeId(), 'GammaStar');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Oncology Information Systems').getRecordTypeId(), 'GE');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Other 3rd Party Connected Devices').getRecordTypeId(), 'GeniousDoc');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Treatment Planning Solutions').getRecordTypeId(), 'Hitachi');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('3rd Party Service Contracts').getRecordTypeId(), 'IBA');
        
        acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        ip = new SVMXC__Installed_Product__c();
        ip.Name = 'H192192';
        ip.SVMXC__Status__c = 'Installed';
        ip.Marked_For_Replacement__c = false;
        insert ip;
        
        salesOpp = TestUtils.getOpportunity();
        salesOpp.AccountId = acct.Id;
        salesOpp.StageName = '7 - CLOSED WON';
        salesOpp.Primary_Contact_Name__c = con.Id;
        salesOpp.MGR_Forecast_Percentage__c = '';
        salesOpp.Unified_Funding_Status__c = '20';
        salesOpp.Unified_Probability__c = '20';
        salesOpp.Net_Booking_Value__c = 200;
        salesOpp.CloseDate = System.today();
        salesOpp.Opportunity_Type__c = 'Sales';
        salesOpp.Varian_PCSN_Non_Varian_IB__c = ip.Name+',';
        salesOpp.Varian_SW_Serial_No_Non_Varian_IB__c = ip.Name+',';
        insert salesOpp;
    }
    
    public static List<Competitor__c> createCompetitorData(Boolean isInsert, String oppId, String accId) {
        List<Competitor__c> compList = new List<Competitor__c>();

        system.debug(' ==> ' + ModelMap);
        system.debug(' ==> ' + vendorMap);
        Competitor__c comp;
        for(Id i : recordTypeId) {
            comp = new Competitor__c();
            comp.RecordTypeId = i;
            comp.Competitor_Account_Name__c = accId;
            comp.Opportunity__c = oppId;
            comp.Vendor__c = vendorMap.get(i);
            comp.Model__c = ModelMap.get(i);
            compList.add(comp);
        }
        if(isInsert)
            insert compList;
        return compList;
    }
    
    public static testMethod void NonVarianInstalledProduct_test() {
        test.startTest();
        
        List<Competitor__c> compList = createCompetitorData(true, salesOpp.Id, acct.Id);
        NonVarianInstalledProduct nonInstall;
        for(Integer i = 0; i < compList.size(); i++) {
            nonInstall = new NonVarianInstalledProduct(i, compList[i], new List<String>(), new Map<String, List<String>>());
        }
            
        test.stopTest();
    }
    
    public static testMethod void NonVarianInstalledProduct2_test() {
        test.startTest();
        
        List<Competitor__c> compList = createCompetitorData(true, salesOpp.Id, acct.Id);
        List<String> str = new List<String>{'A'};
        NonVarianInstalledProduct nonInstall;
        for(Integer i = 0; i < compList.size(); i++) {
            nonInstall = new NonVarianInstalledProduct(i, compList[i], str, new Map<String, List<String>>());
        }
            
        test.stopTest();
    }
}
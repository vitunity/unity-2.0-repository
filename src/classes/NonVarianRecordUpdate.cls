global class NonVarianRecordUpdate implements Database.Batchable<SObject> {
    
    private string query;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        query = ' SELECt Id, Product_Type__c, RecordTypeId, RecordType.DeveloperName FROM Competitor__c ' +
                ' WHERE RecordTypeId = null ';
        system.debug(' ==== QUERY => ' + Database.getQueryLocator(query));
        
         return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Competitor__c> toUpdate) {
        Map<String, String> recordTypeOptions = new Map<String, String>();
        
        for(RecordType NVPrecordType : [SELECT Id, Name, Description FROM RecordType WHERE SobjectType = 'Competitor__c' 
                                        AND IsActive =: true]){
            recordTypeOptions.put(NVPrecordType.Description, NVPrecordType.Id);
        }
        
        for(Competitor__c comp : toUpdate) {
            system.debug(' ==== comp ==== ' + comp.RecordTypeId);
            if(recordTypeOptions.containsKey(comp.Product_Type__c))  {
                comp.RecordTypeId = recordTypeOptions.get(comp.Product_Type__c);
            }
        }
        system.debug(' ==== comp ==== ' + toUpdate);
    	update toUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}
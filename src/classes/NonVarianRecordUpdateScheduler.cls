global class NonVarianRecordUpdateScheduler implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new NonVarianRecordUpdate(), 200); 
    }
    
}
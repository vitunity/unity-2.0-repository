@isTest(seeAllData = true)
public class NonVarianRecordUpdateTest {
	
    public static List<Id> recordTypeId = new List<String>();
    public static Account acct;
    public static Regulatory_Country__c regulatory;
    public static Contact con;
    public static SVMXC__Installed_Product__c ip;
    public static Opportunity salesOpp;
    
    static {
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('BA').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('DS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('IMS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OIS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OPCD').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPS').getRecordTypeId());        
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPSC').getRecordTypeId());
    	
        acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        ip = new SVMXC__Installed_Product__c();
        ip.Name = 'H192192';
        ip.SVMXC__Status__c = 'Installed';
        ip.Marked_For_Replacement__c = false;
        insert ip;
        
        salesOpp = TestUtils.getOpportunity();
        salesOpp.AccountId = acct.Id;
        salesOpp.StageName = '7 - CLOSED WON';
        salesOpp.Primary_Contact_Name__c = con.Id;
        salesOpp.MGR_Forecast_Percentage__c = '';
        salesOpp.Unified_Funding_Status__c = '20';
        salesOpp.Unified_Probability__c = '20';
        salesOpp.Net_Booking_Value__c = 200;
        salesOpp.CloseDate = System.today();
        salesOpp.Opportunity_Type__c = 'Sales';
        salesOpp.Varian_PCSN_Non_Varian_IB__c = ip.Name+',';
        salesOpp.Varian_SW_Serial_No_Non_Varian_IB__c = ip.Name+',';
        insert salesOpp;
    
    }
    
    public static List<Competitor__c> createCompetitorData(Boolean isInsert, String oppId, String accId) {
        List<Competitor__c> compList = new List<Competitor__c>();
        Competitor__c comp;
        for(Id i : recordTypeId) {
            comp = new Competitor__c();
           // comp.RecordTypeId = i;
            comp.Competitor_Account_Name__c = accId;
            comp.Opportunity__c = oppId;
            comp.Vendor__c = 'AGAT';
            comp.Model__c = 'AGAT-BT';
            compList.add(comp);
        }
        if(isInsert)
            insert compList;
       	return compList;
    }
    
    
    public static testMethod void testBatch() {
        Test.startTest();
        	List<Competitor__c> compList = createCompetitorData(true, salesOpp.Id, acct.Id);
        	system.debug(' ==== competitors === > ' + compList);
        for(Competitor__c c : [SELECT Id, RecordTypeId from Competitor__c where Id IN: compList]) {
            system.debug(' === c === ' + c);
        }
        //system.assert(false);
        	Database.executeBatch(new NonVarianRecordUpdate());
        Test.stopTest();
    }
    
}
/*
Name        : OCSDC_AdminCompCon 
Updated By  : Rishi Khirbat (Appirio India)
Date        : 25th April, 2014
Purpose     : An Apex controller to get Information for ContactCommunityUserStatus.page.

Refrence    : T-271570: Develop a VisualForce Page for pending registration approval
Updated By  : Sumit Tanwar on 16th July, 2014.

17-11-2014   Naresh K Shiwani    T-328689

Refrence    : T-352572, T-352574                        Updated By  : Ashish Sharma on 23rd Jan, 2015.
Refrence    : T-379542: Modified isCallSearchRecords    Updated By  : Naresh K Shiwani on 21th April, 2015.
Refrence    : T-381184: Modified updateInviteDevCloud   Updated By  : Naresh K Shiwani on 27th April, 2015.
Refrence    : ONCO-353: Modified searchRecords          Updated By  : Shital Bhujbal on 10th July, 2015.

*/
public without sharing class OCSDC_AdminCompCon {                          
    private Id orgWideEmailAddressId;
    public Contact contactStatus {get;set;}    
    public String currentTab {get;set;}
    public String title {get;set;}
    public String actionLabel {get;set;}
    public String selectedContactId {get;set;}    
    public ApexPages.StandardSetController con {get;set;}  
    public List<Contact> lstContacts {get; set;}                         
    public List<WrapperContact> wrapperContactList{get; set;}
    public Boolean foundError { get; set; }
    public boolean isSelectAll{get; set;}    
    static final Integer PAGE_SIZE = 20;    
    Map<String, User> mapContactUser;
    public class WrapperContact {
        public Contact contact {get;set;}
        public Boolean existingUser {get;set;}
        public String userStatus {get;set;}
        public boolean isSelected {get;set;}
        public WrapperContact() {
            isSelected = existingUser = false;
        }
        public WrapperContact(Contact cont, User ur, Boolean isSelectAll) {
            this.contact = cont;
            this.existingUser = false;
            this.isSelected = isSelectAll;
            this.userStatus = '';
            if(ur != null) {
                this.existingUser = true;
                this.userStatus = ur.IsActive ? 'Active' : 'Inactive';
            }
        }
    }
    public OCSDC_AdminCompCon () {   
        foundError = false;     
        isSelectAll = false;
        contactStatus = new Contact();        
        actionLabel= 'Approve/Reject';
        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();                 
    }
       
    public PageReference disableFromDeveloperCloud() {
        Set<ID> contactIds = new Set<ID>();
        Set<ID> userIds    = new Set<ID>();
        Savepoint sp;                
        for(WrapperContact wrapContact : wrapperContactList){           
            if(wrapContact.isSelected){
                contactIds.add(wrapContact.contact.Id);
                if(mapContactUser.get(wrapContact.contact.Id)!=null)
                    userIds.add(mapContactUser.get(wrapContact.contact.Id).id);
            }
        }
        
        contactIds.add(selectedContactId);
        if(mapContactUser.get(selectedContactId)!=null)
            userIds.add(mapContactUser.get(selectedContactId).id);
        
        System.debug('userIds========='+userIds);
        System.debug('contactIds========='+contactIds);
        List<Contact> lstContactUpdate = new List<Contact>();
        List<User> lstUserUpdate = new List<User>();
        for(Contact ct : [SELECT Id,OCSDC_UserStatus__c,OCSDC_InvitationDate__c,
                          MyVarian_Member__c, OCSUGC_UserStatus__c
                          FROM Contact
                          WHERE Id=:contactIds and MyVarian_Member__c = true and OCSUGC_UserStatus__c = 'OCSUGC Approved'
                          LIMIT :OCSUGC_Constants.SF_QUERY_LIMIT]) {
           ct.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Admin;
           lstContactUpdate.add(ct); 
           System.debug('lstContactUpdate========'+lstContactUpdate);               
        } 
        for(User usr:[SELECT Id,Name,OCSDC_AcceptedTermsOfUse__c
                      FROM User
                      WHERE Id =:userIds and IsActive = true
                      LIMIT :OCSUGC_Constants.SF_QUERY_LIMIT]){
            usr.OCSDC_AcceptedTermsOfUse__c = false;
            lstUserUpdate.add(usr);  
            System.debug('lstUserUpdate========'+lstUserUpdate);
        }
        
        try {
            sp = Database.setSavepoint();
            if(lstContactUpdate.size() > 0) {
                update lstContactUpdate;
                
            }
            if(lstUserUpdate.size() > 0){
                update lstUserUpdate;
                System.debug('userIds============'+userIds);
                OCSUGC_Utilities.removePermissionSetFromUsers(userIds, new Set<String>{OCSUGC_Constants.DC_COMMUNITY_MEMBER_SET});
            }
        }
        catch(Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
               
        isSelectAll = false;
        selectAll();
        searchRecords();
        return null;
        
    }
    
    // 16-Feb-2015  Puneet Sardana  Refactored code for Ref T-362409
    public PageReference inviteToDeveloperCloud() {
        // 16-Feb-2015 Puneet Sardana Added logic Ref T-362409
        updateInviteDevCloud(new Set<Id> { selectedContactId });
        searchRecords();
        return null;
        
    }
    // 16-Feb-2015 Puneet Sardana Refactored code for Ref T-362409
    public PageReference inviteToDeveloperCloudList(){
        Set<ID> contactIds = new Set<ID>();
        
        for(WrapperContact wrapContact : wrapperContactList){
            if(wrapContact.isSelected){
                contactIds.add(wrapContact.contact.Id);
            }
        }
        updateInviteDevCloud(contactIds);        
        isSelectAll = false;
        selectAll();
        searchRecords();
        return null;
    }
    // @description: This methods updated user and contact when user is invited to developer cloud
    // @param: Contact Ids to be updated
    // @return: NA
    // 16-Feb-2015  Puneet Sardana  Original Ref T-362409
    // 27-Apr-2015  Naresh K Shiw   Updated to send notification alert to invited user. Ref T-381184
    private void updateInviteDevCloud(Set<Id> contactIds) {     
        List<Contact> lstContactUpdate = new List<Contact>();
        List<OCSUGC_Intranet_Notification__c> lstNotifications = new List<OCSUGC_Intranet_Notification__c>();
        OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
        Map<String, Contact> mapContacts = new Map<String, Contact>();
        Savepoint sp;
        String pageURL = '';
        //Update Contact
        for(Contact ct : [SELECT Id,OCSDC_UserStatus__c,OCSDC_InvitationDate__c
                          FROM Contact
                          WHERE Id=:contactIds
                          LIMIT :OCSUGC_Constants.SF_QUERY_LIMIT]) {
           ct.OCSDC_UserStatus__c = OCSUGC_Constants.DevCloud_User_Status_Invited;
           ct.OCSDC_InvitationDate__c = DateTime.now();
           mapContacts.put(ct.id,ct);
           lstContactUpdate.add(ct);                
        }
        if(mapContacts != null){
            for(User ur : [Select u.LastName, u.FirstName, u.Title, u.contactid
                           From User u 
                           Where u.contactid=:mapContacts.keyset()]){
                                pageURL = 'OCSUGC_TermsOfUse?dc=true&contId='+mapContacts.get(ur.contactid).id;
                                tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(Label.OCSUGC_NetworkName,ur.Id, '' ,Label.OCSDC_has_invited_to_join_DC,pageURL);
                                lstNotifications.add(tempObj);
            }
        }
        try {
            sp = Database.setSavepoint();
            if(lstContactUpdate.size() > 0) {
                update lstContactUpdate;
            }
            if(lstNotifications.size() > 0){
                insert lstNotifications;
            }
        }
        catch(Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
    }

    
    //T-379542  called conditional searchRecords() if tab data is changed.
    public Boolean isCallSearchRecords {
        get {
            if(isCallSearchRecords == null || isCallSearchRecords == false) {
                searchRecords();
            }
            contactStatus.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
            if(currentTab == 'CurrentUsers') {
                actionLabel = 'Disable from Developer Cloud';
            } else {
                actionLabel = 'Invite to Developer Cloud';
            }
            return true;
        }
        set;
    }

    //Method to get contact records on the basis of current tab.
    public void searchRecords() {
        String member = OCSUGC_Constants.MEMBER;
        String invite = OCSUGC_Constants.INVITED;
        isCallSearchRecords = true;
        String query =  'Select MyVarian_Member__c, Id, OCSDC_UserStatus__c, Name, Email, Inactive_Contact__c, OCSUGC_UserStatus__c, CreatedDate, OCSUGC_Rejection_Reason__c, OCSUGC_Disabled_Reason__c ';
        query += 'FROM Contact ';
        //Changed  24-Mar-2015  Puneet Sardana  Changed OncoPeerUsers conditions as per T-372654
        if(currentTab == 'OncoPeerUsers'){
            query += 'WHERE OCSUGC_UserStatus__c Like \'%'+Label.OCSUGC_Approved_Status+'%\' ';
            query += 'AND OCSDC_UserStatus__c != :member ';
            query += 'AND OCSDC_UserStatus__c != :invite ';
        } else if(currentTab == 'CurrentUsers'){
                query += 'WHERE OCSUGC_UserStatus__c Like \'%'+Label.OCSUGC_Approved_Status+'%\' ';
            query += 'AND OCSDC_UserStatus__c = :member ';
        } else if(currentTab == 'InvitedUsers'){
                query += 'WHERE OCSUGC_UserStatus__c Like \'%'+Label.OCSUGC_Approved_Status+'%\' ';
            query += 'AND OCSDC_UserStatus__c =\'Invited\' ';
        } else if(currentTab == 'AdminDisabledUsers'){
            query += 'WHERE OCSDC_UserStatus__c =\'Disabled by Admin\' ';
        } else if(currentTab == 'SelfDisabledUsers'){
            query += 'WHERE OCSDC_UserStatus__c =\'Disabled by Self\' ';
        }
        query += 'Order By SystemModStamp DESC LIMIT 5000';

        system.debug('==========query: '+query);
        mapContactUser = new Map<String, User>();
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(PAGE_SIZE);        
        for(User ur :[Select ContactId, IsActive From User Where ContactId IN :con.getRecords()]) {
            mapContactUser.put(ur.ContactId,ur);
        }
        getWrapperContacts();
        if(currentTab == 'OncoPeerUsers'){
            title = 'There are '+con.getResultSize()+' OncoPeer users';
        } else if(currentTab == 'CurrentUsers'){
            title = 'There are '+con.getResultSize()+' Member users';
        } else if(currentTab == 'InvitedUsers'){
            title = 'There are '+con.getResultSize()+' Invited users';
        } else if(currentTab == 'AdminDisabledUsers'){
            title = 'There are '+con.getResultSize()+' Disabled by Admin users';
        } else if(currentTab == 'SelfDisabledUsers'){
            title = 'There are '+con.getResultSize()+' Disabled by Self users';
        }
    }

    public List<WrapperContact> getWrapperContacts() {
        wrapperContactList = new List<WrapperContact>();
        if(con == null)
             return wrapperContactList;

        for(Contact cont :(List<Contact>) con.getRecords()) {
            wrapperContactList.add(new WrapperContact(cont,mapContactUser.get(cont.Id),isSelectAll));
        }
        return wrapperContactList;
    }

    public PageReference First() {
        if(con == null)
            return null;
        con.first();
        return null;
    }

    public PageReference Previous() {
         if(con.getHasPrevious())
             con.previous();
         return null;
    }

    public PageReference Next() {
         if(con.getHasNext()){
             con.next();
         }
         return null;
    }

    public PageReference Last() {
        if(con == null)
            return null;
        con.last();
        return null;
    }

    public void selectAll(){
//      List<WrapperContact> lstWrapConSelect = new List<WrapperContact>();
//      lstWrapConSelect = getWrapperContacts();
//      System.debug('lstWrapConSelect========='+lstWrapConSelect);
//        for(WrapperContact contact : lstWrapConSelect){
//            contact.isSelected = isSelectAll;
//        }
    }
    
    public void showSelectedTab(){
        System.debug('In showSelectedTab');
    }
}
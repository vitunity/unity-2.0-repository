//19-05-2015  Puneet Sardana  Added new test class T-392114
@isTest(SeeAllData=false)
public class OCSDC_AdminCompConTest {
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<Contact> lstContactInsert;
   static Network ocsugcNetwork, ocsdcNetwork;
   static String ocsugcNetworkString, ocsdcNetworkString;
   static {     
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     ocsugcNetworkString  = OCSUGC_TestUtility.getCurrentCommunityNetworkId(false);
     ocsdcNetworkString   = OCSUGC_TestUtility.getCurrentCommunityNetworkId(true); 

     account = OCSUGC_TestUtility.createAccount('test account '+ String.valueOf(Datetime.now().getTime()), true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert; 
  }
  
  
   public static testmethod void test_AdminCompCurrentUsers() {  
    Test.startTest();

    List<Contact> lstContacts = new List<Contact>();    
    Contact conA1 = OCSUGC_TestUtility.createContact(account.Id, true);
    conA1.OCSDC_UserStatus__c = OCSUGC_Constants.MEMBER;
    update conA1;
    lstContacts.add(conA1);   
    
    OCSDC_AdminCompCon adminCon = new OCSDC_AdminCompCon(); 
      adminCon.currentTab = 'CurrentUsers';
      adminCon.searchRecords();     
      adminCon.isSelectAll = true;      
      adminCon.selectAll();
      adminCon.disableFromDeveloperCloud();
      adminCon.selectedContactId = conA1.Id;
      adminCon.inviteToDeveloperCloud();
      adminCon.inviteToDeveloperCloudList();
      adminCon.First();
      adminCon.Next();
      adminCon.Last();
      adminCon.Previous();
      adminCon.showSelectedTab();   
      Boolean isCallSearch = adminCon.isCallSearchRecords;  
      Test.stopTest();
  }
  
  
  public static testmethod void test_AdminCompOncoPeerUsers() {  
    Test.startTest();

    List<Contact> lstContacts = new List<Contact>();    
    Contact conA1 = OCSUGC_TestUtility.createContact(account.Id, true);
    conA1.OCSDC_UserStatus__c = 'Disabled by Self';
    update conA1;
    lstContacts.add(conA1);
    
    OCSDC_AdminCompCon adminCon = new OCSDC_AdminCompCon(); 
      adminCon.currentTab = 'OncoPeerUsers';
      adminCon.searchRecords(); 
      adminCon.getWrapperContacts();
      adminCon.isSelectAll = true;  
      adminCon.selectAll();
      adminCon.selectedContactId = conA1.Id;
      adminCon.inviteToDeveloperCloud();
      adminCon.inviteToDeveloperCloudList();
      adminCon.First();
      adminCon.Next();
      adminCon.Last();
      adminCon.Previous();
      adminCon.showSelectedTab();   
      Boolean isCallSearch = adminCon.isCallSearchRecords;  
      Test.stopTest();
  }

  
    public static testmethod void test_AdminCompInvitedUsers() {  
    Test.startTest(); 

    List<Contact> lstContacts = new List<Contact>();    
    Contact conA1 = OCSUGC_TestUtility.createContact(account.Id, true);
    conA1.OCSDC_UserStatus__c = OCSUGC_Constants.INVITED;
    update conA1;
    lstContacts.add(conA1);
    
    OCSDC_AdminCompCon adminCon = new OCSDC_AdminCompCon(); 
      adminCon.currentTab = 'InvitedUsers';
      adminCon.searchRecords();       
      adminCon.getWrapperContacts();
      adminCon.isSelectAll = true;  
      adminCon.selectAll();
      adminCon.selectedContactId = conA1.Id;
      adminCon.inviteToDeveloperCloud();
      adminCon.inviteToDeveloperCloudList();
      adminCon.First();
      adminCon.Next();
      adminCon.Last();
      adminCon.Previous();
      adminCon.showSelectedTab();   
      Boolean isCallSearch = adminCon.isCallSearchRecords;  
      Test.stopTest();
  }

  public static testmethod void test_AdminCompAdminDisabledUsers() {  
    Test.startTest();

    List<Contact> lstContacts = new List<Contact>();    
    Contact conA1 = OCSUGC_TestUtility.createContact(account.Id, true);
    conA1.OCSDC_UserStatus__c = 'Disabled by Admin';
    update conA1;
    lstContacts.add(conA1);
    
    OCSDC_AdminCompCon adminCon = new OCSDC_AdminCompCon(); 
      adminCon.currentTab = 'AdminDisabledUsers';
      adminCon.searchRecords(); 
      adminCon.getWrapperContacts();
      adminCon.isSelectAll = true;  
      adminCon.selectAll();
      adminCon.selectedContactId = conA1.Id;
      adminCon.inviteToDeveloperCloud();
      adminCon.inviteToDeveloperCloudList();
      adminCon.First();
      adminCon.Next();
      adminCon.Last();
      adminCon.Previous();
      adminCon.showSelectedTab();
      Boolean isCallSearch = adminCon.isCallSearchRecords;    
      Test.stopTest();
  }

  public static testmethod void test_AdminCompSelfDisabledUsers() {  
    Test.startTest();

    List<Contact> lstContacts = new List<Contact>();    
    Contact conA1 = OCSUGC_TestUtility.createContact(account.Id, true);
    conA1.OCSDC_UserStatus__c = 'Disabled by Self';
    update conA1;
    lstContacts.add(conA1);
    
    OCSDC_AdminCompCon adminCon = new OCSDC_AdminCompCon(); 
      adminCon.currentTab = 'SelfDisabledUsers';
      adminCon.searchRecords(); 
      adminCon.getWrapperContacts();    
      adminCon.isSelectAll = true; 
      adminCon.selectAll();
      adminCon.selectedContactId = conA1.Id;
      adminCon.inviteToDeveloperCloud();
      adminCon.inviteToDeveloperCloudList();
      adminCon.First();
      adminCon.Last();
      adminCon.Next();
      adminCon.Previous();
      adminCon.showSelectedTab();
      Boolean isCallSearch = adminCon.isCallSearchRecords;    
    Test.stopTest();
  }
  
  public static testmethod void test_AdminTestMainPage() {  
    Test.startTest();    
      OCSDC_CommunityAdministrationCon comAdminCon = new OCSDC_CommunityAdministrationCon();
      comAdminCon.changeCurrentTab();
    Test.stopTest();
  }

}
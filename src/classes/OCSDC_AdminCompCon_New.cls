/**
 *	26 Oct 2015,	Puneet Mishra
 *					New Developer Admin Page Controller for inviting OncoPeer User, Contributors to Devloper Cloud Community
 *					
 */
public without sharing class OCSDC_AdminCompCon_New {
	
	public List<Contact> contacts {get;set;} // List of contacts to be displayed on Page
    public String selectedVal {get;set;} // Value of selected picklist value
    public String accountName {get;set;} // accountName to search the Account
    public String country {get;set;} // store selected Country to search in Contacts
    public Boolean newInvite{get;set;}
    @TestVisible private transient List<WrapperClass> wrapList {get;set;} // list of wrapperContants that will used to update the Contact status
    @TestVisible private Id permissionSetId; // permissionSetIf to be assigned to Contributors
    @TestVisible private Static String pageUrl = 'OCSUGC_TermsOfUse?dc=true&contId=';
    @TestVisible private Static String reloadUrl = '/apex/OCSDC_CommunityAdministration_New?status=';
    @TestVisible private Static String PAGEMSG = 'Sorry for inconvenience, Please contact Admin';
    public String selectedContactId {get;set;} // selected ContactId for updating Developer Cloud status
    
    public Map<String, String> pickListVal = new Map<String, String>{
                                                'OncoPeer Only Users' => 'OncoPeer Only Users',
                                                'Current Users' => 'Current Users',
                                                'Invited Users' => 'Invited Users',
                                                'Admin Disabled Users' => 'Admin Disabled Users',
                                                'Self Disabled Users' => 'Self Disabled Users',
                                                'OncoPeer Contributors' => 'OncoPeer Contributors'};

    // Picklist value for Countries
    public List<SelectOption> getCountries() {
        List<SelectOption> options = new List<SelectOption>();
        
        // Describe will get Account's Country__c details
        Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
        List<Schema.PickListEntry> ple = fieldResult.getPickListValues();     // get the Country__c picklist values
        options.add(new SelectOption('',''));
        for( Schema.PickListEntry f : ple ) {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    
    // PicklIst Values for Developer Cloud Users with different Status 
    public List<SelectOption> getOptions () {
        Map<String, String> pickListVal = new Map<String, String>{
                                                'OncoPeer Only Users' => 'OncoPeer Only Users',
                                                'Current Users' => 'Current Users',
                                                'Invited Users' => 'Invited Users',
                                                'Admin Disabled Users' => 'Admin Disabled Users',
                                                'Self Disabled Users' => 'Self Disabled Users',
                                                'OncoPeer Contributors' => 'OncoPeer Contributors'};
        List<SelectOption> opt = new List<SelectOption>();
        for(String str : pickListVal.keySet()) {
            opt.add(new SelectOption(str, pickListVal.get(str)));
        }
        return opt;    
    }
	
	public OCSDC_AdminCompCon_New() {
		contacts = new List<Contact>();
        newInvite = false;
        newInvite = (ApexPages.CurrentPage().getParameters().get('invite') == 'true') ? true : false;
        if(ApexPages.CurrentPage().getParameters().get('status') != null)
            selectedVal = ApexPages.CurrentPage().getParameters().get('status');
        else
            selectedVal = 'OncoPeer Only Users';
	}
	
	// Displays the information of Externam Users and Internal Users(Contributor) on page
	public List<WrapperClass> getWrapperList() {
        String query ='';
        String MEMBER = 'Member';
        Map<Id, User> userMap = new Map<Id, User>();
        wrapList = new List<WrapperClass>();
        List<WrapperClass> wrappers = new List<WrapperClass>();

        query += 'SELECT Id, Name, Account.Name, Account.Country__c, FirstName, LastName, OCSUGC_UserStatus__c, Inactive_Contact__c, MyVarian_Member__c, '
              +  ' OCSDC_UserStatus__c, Phone, Institute_Name__c FROM Contact ';
        system.debug(' *****selectedVal***** ' + selectedVal + ' *****accountName***** ' + accountName + ' ****country**** ' + country);
        
        if(pickListVal.containsKey(selectedVal)) {
            if(selectedVal == 'OncoPeer Only Users') {
                query += ' WHERE OCSUGC_UserStatus__c = \''+ Label.OCSUGC_Approved_Status + '\'' + ' AND OCSDC_UserStatus__c != \'' + MEMBER+ '\'';
            } else if(selectedVal == 'Current Users') {
                query += ' WHERE OCSUGC_UserStatus__c = \''+ Label.OCSUGC_Approved_Status + '\'' + ' AND OCSDC_UserStatus__c = \'' + MEMBER + '\'';
            } else if(selectedVal == 'Invited Users') {
                query += ' WHERE OCSUGC_UserStatus__c = \''+ Label.OCSUGC_Approved_Status + '\'' + ' AND OCSDC_UserStatus__c = \'' + Label.OCSDC_Invited + '\'';
            } else if(selectedVal == 'Admin Disabled Users') {
                query += ' WHERE OCSUGC_UserStatus__c = \''+ Label.OCSUGC_Approved_Status + '\'' + ' AND OCSDC_UserStatus__c = \'' + Label.OCSDC_Disabled_by_Admin + '\'';
            } else if(selectedVal == 'Self Disabled Users') {
                query += ' WHERE OCSUGC_UserStatus__c = \''+ Label.OCSUGC_Approved_Status + '\'' + ' AND OCSDC_UserStatus__c = \'' + Label.OCSDC_Disabled_by_Self + '\'';
            } else if(selectedVal == 'OncoPeer Contributors') {
            	system.debug(' FOR CONTRIBUTORS ');
            	return getInviteContributors();
            }
        }
        // if AccountName is provided in search
        if(accountName != null && accountName != '') {
            query += ' AND Account.Name LIKE ' + '\'%' + accountName + '%\'';
        }
        // if Country is provided in search
        if(country != null && country != '') {
            query += ' AND Account.Country__c = \'' + country + '\' ';
        }        
        
        query += ' Order By SystemModStamp DESC LIMIT 1000';
        
        List<Contact> contactList = Database.Query(query);
        for(User u : [SELECT Id, Email, ContactId, LastLoginDate, LastModifiedDate FROM User Where isActive =: true AND ContactId IN: contactList 
                        ORDER BY LastLoginDate NULLS Last]) {
            userMap.put(u.contactId, u);
        }
        
        for(Contact cont : contactList) {
            if(userMap.containsKey(cont.Id)) {
                wrappers.add(new WrapperClass(cont, userMap.get(cont.Id)));
            }
        }
        wrapList = wrappers;
        return wrappers;
    }
    
    // method returns the information of Contributors
    public List<WrapperClass> getInviteContributors() {
    	List<WrapperClass> wrapper = new List<WrapperClass>();
    	Set<Id> assignedUserId = new Set<Id>();
    	String userQuery = '';
    	List<User> contributorUser = new List<User>();
    	
    	permissionSetId = [Select Id, Name From PermissionSet Where Name=:Label.OCSDC_VMS_Community_Members_Permissions].Id;
    	
    	for(PermissionSetAssignment perSetAssign : [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: permissionSetId]) {
    		assignedUserId.add(perSetAssign.AssigneeId);
    	}
    	
    	userQuery = 'SELECT Id, Name, FirstName, LastName, isActive, Email, OCSUGC_User_Role__c FROM User WHERE OCSUGC_User_Role__c=\'' + Label.OCSUGC_Varian_Employee_Community_Contributor + '\'' +
    				' AND isActive=true AND Id NOT IN : assignedUserId ';
    	contributorUser = Database.Query(userQuery);
    	
    	for(User u : contributorUser) {
    		wrapper.add(new WrapperClass(null, u));
    	}
    	//contibutorWrapList = wrapper;
    	wrapList = wrapper;
    	return wrapper;
    }
   	
   	// individualInvite
   	public PageReference inviteIndividualContact() {
   		Contact inviteContact; 
   		list<Id> userRecId = new list<Id>();
   		
   		if(selectedVal == 'OncoPeer Contributors') {
   			for(User u : [SELECT Id, Name, OCSDC_AcceptedTermsOfUse__c FROM User WHERE Id =: selectedContactId])
   				userRecId.add(u.Id);
   		} else {
   			for(User u : [SELECT Id, Name, ContactId, OCSDC_AcceptedTermsOfUse__c FROM User WHERE ContactId =: selectedContactId])
   				userRecId.add(u.Id);
   			inviteContact = new Contact();
   			inviteContact = [SELECT Id, OCSDC_UserStatus__c, OCSDC_InvitationDate__c FROM Contact WHERE Id =: selectedContactId];
   			inviteContact.OCSDC_UserStatus__c = OCSUGC_Constants.DevCloud_User_Status_Invited;
	   		inviteContact.OCSDC_InvitationDate__c = DateTime.now();
	   	}
	   	
	   	Savepoint sp;
	   	
	   	try {
	   		update inviteContact;
	   		// Change in logic as MIXED DML Exception encountered
	   		OCSDC_AdminCompCon_New.insertNotifications(new List<Id>{inviteContact.Id}, userRecId);
	   		//OCSDC_AdminCompCon_New.assignPermissiontoContributor(userRecId, permissionSetId);
	   		
	   	} catch(Exception e) {
	   		system.debug('**** Exception **** ' + e.getStackTraceString());
    		Database.Rollback(sp);
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  OCSDC_AdminCompCon_New.PAGEMSG));
	   	} return null;
   	}
   	
   	public PageReference disableIndividualContact() {
    	Savepoint sp;
    	
    	try {
    		sp = Database.setSavePoint();
    	
    		Contact disableContact = new Contact();
	    	disableContact = [SELECT Id, OCSDC_UserStatus__c, OCSDC_InvitationDate__c FROM Contact WHERE Id =: selectedContactId];
	    	disableContact.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Admin;
	    	update disableContact;
	    	
	    	User userAcceptedTerms = new User();
	    	userAcceptedTerms = [SELECT Id, OCSDC_AcceptedTermsOfUse__c, contactId FROM User WHERE contactId =: disableContact.Id];
	    	userAcceptedTerms.OCSDC_AcceptedTermsOfUse__c = false;
	    	update userAcceptedTerms;
    	} catch(Exception e) {
    		Database.Rollback(sp);
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  OCSDC_AdminCompCon_New.PAGEMSG));
    	}
    	return null;
    }
    
    public PageReference inviteToDeveloperCloud() {
    	Set<Id> invitedContacts = new Set<Id>();
        List<id> invitedContributors = new List<Id>();
        
        for(WrapperClass wrapp : wrapList) {
            if(wrapp.check == true && selectedVal != 'OncoPeer Contributors') {
                invitedContacts.add(wrapp.contact.Id);
            } else {
            	invitedContributors.add(wrapp.user.Id);
            }
        }
        
        if(!invitedContacts.isEmpty()) {
        	updateDevloperContactStatus(invitedContacts, true);
        } else if(!invitedContributors.isEmpty()) {
        	updateContributorsDeveloperStatus(invitedContributors);
        }	
        return reload();
    }
    
    public PageReference disableFromDeveloperCloud() {
    	Set<Id> disabledContacts = new Set<Id>();
    	
    	for(WrapperClass wrapp : wrapList) {
            if(wrapp.check == true) {
            	disabledContacts.add(wrapp.contact.Id);
            }
        }
    	
    	if(!disabledContacts.isEmpty())
        	updateDevloperContactStatus(disabledContacts, false);
    	
        return reload();
    }
    
    public void updateContributorsDeveloperStatus(List<Id> userIds) {
    	List<Id> userRecId = new List<Id>();
    	List<OCSUGC_Intranet_Notification__c> lstNotifications = new List<OCSUGC_Intranet_Notification__c>();
    	SavePoint sp;
    	
    	if(userIds.size() == 0)
    		return;
    	
    	for(User u : [SELECT Id, OCSDC_AcceptedTermsOfUse__c, Name FROM User WHERE Id IN: userIds])
    		userRecId.add(u.Id);
    	
    	try {
    		sp = Database.setSavePoint(); // setting savepoint in case of any Database failure
    		
    		if(!userRecId.isEmpty()) {
    			OCSDC_AdminCompCon_New.insertNotifications(new List<Id>(), userRecId);
    			OCSDC_AdminCompCon_New.assignPermissiontoContributor(userRecId, permissionSetId);
    		}
    	} catch(Exception e) {
    		Database.Rollback(sp);
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  OCSDC_AdminCompCon_New.PAGEMSG));//e.getMessage()
    	}
    	
    }
    
    //	25 Sept 2015	Puneet Mishra
    //					Update the Status of Contact and User records and create Notifications
    public void updateDevloperContactStatus(Set<Id> contactIds, Boolean isInvited) {
    	contacts = new List<Contact>();
    	List<Id> contactIdList = new List<Id>();
    	List<OCSUGC_Intranet_Notification__c> lstNotifications;
    	List<User> userRec;
        Savepoint sp;
        
        for(Contact cont : [SELECT Id, OCSDC_UserStatus__c, OCSDC_InvitationDate__c FROM Contact WHERE Id IN: contactIds]) {
        	if(isInvited == true) {
        		cont.OCSDC_UserStatus__c = OCSUGC_Constants.DevCloud_User_Status_Invited;
   				cont.OCSDC_InvitationDate__c = DateTime.now();
        	} else {
        		cont.OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Admin;
        	}
        	contactIdList.add(cont.id);
   			contacts.add(cont);                
        }
        
        if(!contactIdList.isEmpty()) {
        	lstNotifications = new List<OCSUGC_Intranet_Notification__c>();
        	userRec = new List<User>();
        	if(isInvited == true) {
	        	OCSDC_AdminCompCon_New.insertNotifications(contactIdList, new List<Id>());
	        } else {
	        	for(User u : [SELECT Id, OCSDC_AcceptedTermsOfUse__c, contactId FROM User WHERE contactId IN : contactIdList]) {
	        		u.OCSDC_AcceptedTermsOfUse__c = false;
	    			userRec.add(u);
	    		}
	        }
        }
        
        try {
    		sp = Database.setSavePoint(); // setting savepoint in case of any Database failure
    		if(!contacts.isEmpty())
    			update contacts;	//	updating contacts with status 'Disabled By Admin'
    		
    		if(!userRec.isEmpty())
    			update userRec;		// updating user records setting OCSDC Accepted terms of use to false
    	}catch(Exception e) {
    		Database.Rollback(sp);
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  OCSDC_AdminCompCon_New.PAGEMSG));//e.getMessage()
    	}
    }
   	
   	@TestVisible
    private static void assignPermissiontoContributor(list<Id> userList, Id permissionSetId) {
    	try {
	    	List<PermissionSetAssignment> lstPermissionSetAssignments = new List<PermissionSetAssignment>();
	    	for(String u : userList) {
	    		PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
	            perSetAssign.PermissionSetId=permissionSetId;
	            perSetAssign.AssigneeId = u;
	            lstPermissionSetAssignments.add(perSetAssign);
	    	}
	    	if(lstPermissionSetAssignments.size()>0)
	    		insert lstPermissionSetAssignments;
    	} catch(Exception e) {
    		//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,  OCSDC_AdminCompCon_New.PAGEMSG));//e.getMessage()
    	}
    }
    
    @TestVisible
    @future
    private static void insertNotifications(list<Id> contactIdList, list<Id> userRecIds) {
    	List<OCSUGC_Intranet_Notification__c> lstNotifications = new List<OCSUGC_Intranet_Notification__c>(); // List of Notification records for Invited Contact Record
        OCSUGC_Intranet_Notification__c notifyObj = new OCSUGC_Intranet_Notification__c(); // Notify Record
        List<User> userList = new List<User>();
        
        if(contactIdList.size() > 0 )
        	userList = [SELECT Id, contactId, FirstName, LastName FROM User WHERE contactId IN: contactIdList];
        else if(userRecIds.size() > 0 )
        	userList = [SELECT Id, contactId, FirstName, LastName FROM User WHERE Id IN: userRecIds];
    	
    	if(userList.size() > 0) {
    		for(User u : userList) {
        		notifyObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(Label.OCSUGC_NetworkName, u.Id, '', Label.OCSDC_has_invited_to_join_DC, 
        																		OCSDC_AdminCompCon_New.pageUrl );//+ mapContacts.get(u.contactId).Id
        		Date lastDate = system.today().addDays(Integer.valueOf(Label.OCSDC_ExpirationDays));
        		notifyObj.OCSUGC_Description__c += ',Last Date ' + lastDate.format();
        		lstNotifications.add(notifyObj);
        	}
    		insert lstNotifications;
    		
    		// Logic added below as Contact's Dev Clod status changing but alert was not showing up as MIXED DML encounter 
    		// when using DML on Contact and Permission-Set together, that why assigning permission set in future method and as 
    		// future methods are static permissionSetId is fetched using SOQL.
	        List<Id> userRecordID = new List<Id>();
	    	for(User u : userList) {
	    		userRecordID.add(u.Id);
	    	}	
    		Id permissionSetId = [Select Id, Name From PermissionSet Where Name=:Label.OCSDC_VMS_Community_Members_Permissions].Id;
    		OCSDC_AdminCompCon_New.assignPermissiontoContributor(userRecordID, permissionSetId);
    		
    		system.debug(' ***************************************************** ' + lstNotifications);
    	}
    }
    
    // reload page
    public PageReference reload() {
        PageReference pageRef = new PageReference(OCSDC_AdminCompCon_New.reloadUrl + selectedVal);
        if(accountName != null && accountName != '') {
            pageRef.getParameters().put('acc',accountName);
        }
        if(country != null && country != '') {
            pageRef.getParameters().put('country',country);
        }
        pageRef.setRedirect(true);    
        return null;
    }
	
	// wrapper class use to display the information of External User's and Internal User's ( Contributors)
	// wrapper Class declared contains Contact and User records
	public Class WrapperClass {
        public Contact contact{get;set;}
        public User user{get;set;}
        public boolean check{get;set;}
         
        public WrapperClass (Contact contact, User user) {
        	if(contact != null)
            	this.contact = contact;
            else {
            	this.contact = new Contact();
            }
            this.user = user;
            this.check = false;
        }
    }
}
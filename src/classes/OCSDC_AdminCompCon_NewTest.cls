/**
 * This class contains unit tests for validating the behavior of 'OCSDC_AdminCompCon_New'
 */
@isTest
private class OCSDC_AdminCompCon_NewTest {
	
	static Profile portal;
	static Profile admin;
	static User internalUsr;
	
	static Account testAcc;
	static Contact member_Contact;
	static Contact invited_Contact;
	static Contact adminDisable_Contact;
	static Contact selfDisable_Contact; 
	static List<Contact> contactList;
	static List<User> usrList;
	
	static {
		portal = OCSUGC_TestUtility.getPortalProfile();
		admin = OCSUGC_TestUtility.getAdminProfile();
		testAcc = OCSUGC_TestUtility.createAccount('Test Account', true);
		member_Contact = OCSUGC_TestUtility.createContact(testAcc.Id, false);
		invited_Contact =  OCSUGC_TestUtility.createContact(testAcc.Id, false);
		adminDisable_Contact = OCSUGC_TestUtility.createContact(testAcc.Id, false);
		selfDisable_Contact = OCSUGC_TestUtility.createContact(testAcc.Id, false);
		
		contactList = new List<Contact>{member_Contact, invited_Contact, adminDisable_Contact, selfDisable_Contact};
		insert contactList; 
		
		usrList = new List<User>();
		usrList.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contactList[0].Id, '001',Label.OCSUGC_Varian_Employee_Community_Member));
        usrList.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contactList[1].Id, '002',Label.OCSUGC_Varian_Employee_Community_Member));
        
        internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'Varian Community Contributor');
    	usrList.add(internalUsr);
    	
    	insert usrList;
    	
	}
	
	public static Account createAccount() {
		return OCSUGC_TestUtility.createAccount('Test Account', true);
	}
	
	public static Contact createContact(Account acc, String devStatus) {
		
		Contact contactTest = OCSUGC_TestUtility.createContact(acc.Id, false);
		contactTest.OCSDC_UserStatus__c = devStatus;
		
		return contactTest;
	}
	
	private static List<User> createUsers(List<Id> contList) {
		// fetching Portal Profile
    	portal = OCSUGC_TestUtility.getPortalProfile();
    	
    	// Inserting Portal User
    	List<User> usrLists = new List<User>();
    	usrLists.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contList[0], '001',Label.OCSUGC_Varian_Employee_Community_Member));
        usrLists.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contList[1], '002',Label.OCSUGC_Varian_Employee_Community_Member));
        insert usrLists;
        return usrLists;
	}
	
	public static PermissionSet getContributorPermissionSet() {
       List<PermissionSet> managerPermissionSet = [SELECT Id FROM PermissionSet WHERE Name LIKE '%VMS_OCSDC_Community_Members_Permissions%' LIMIT 1];
       if(managerPermissionSet.size() > 0)
         return managerPermissionSet[0];
       else
         return null;
   }
	
    public static testMethod void getWrapperList_Member() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'Current Users';
    	
    	controller.accountName = 'Test Account';
    	controller.country = 'India';
    	
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
		Test.StopTest();
	}
	
	public static testMethod void getWrapperList_Invited() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'Invited Users';
    	
    	controller.accountName = 'Test Account';
    	controller.country = 'India';
    	
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
		Test.StopTest();
	}
	
	public static testMethod void getWrapperList_AdminDisabled() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'Admin Disabled Users';
    	
    	controller.accountName = 'Test Account';
    	controller.country = 'India';
    	
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
		Test.StopTest();
	}
	
	public static testMethod void getWrapperList_SelfDisabled() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'Self Disabled Users';
    	
    	controller.accountName = 'Test Account';
    	controller.country = 'India';
    	
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
		Test.StopTest();
	}
	
	public static testMethod void getWrapperList_NotMember() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'OncoPeer Only Users';
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
    	//system.assertEquals(new List<OCSDC_AdminCompCon_New.WrapperClass>(), wrapList);
    	Test.StopTest();
    }
    
    public static testMethod void getWrapperList_Contributors() {
    	Test.StartTest();
    	internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1');
    	insert internalUsr;
    	
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'OncoPeer Contributors';
    	
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
    	Test.StopTest();
    }
    
    public static testMethod void inviteToDeveloperCloud_NotContributors() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'OncoPeer Only Users';
    	controller.selectedContactId = contactList[2].Id;
    	controller.inviteIndividualContact();
    	Test.StopTest();
    }
    
    public static testMethod void inviteToDeveloperCloud_Contributors() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	Id permissionSetId = OCSDC_AdminCompCon_NewTest.getContributorPermissionSet().Id;
    	controller.permissionSetId = permissionSetId;
    	controller.selectedVal = 'OncoPeer Contributors';
    	controller.selectedContactId = internalUsr.Id;
    	try {
    		controller.inviteIndividualContact();
    	} catch(Exception e) {
    		
    	}
    	Test.StopTest();
    }
    
    // test method for Exception coverage
    public static testMethod void inviteToDeveloperCloud_Contributors_Exception() {
    	
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedVal = 'OncoPeer Contributors';
    	controller.selectedContactId = usrList[0].Id;
    	try {
    		controller.inviteIndividualContact();
    	} catch(Exception e) {
    		
    	}
    	Test.StopTest();
    }
    
    public static testMethod void inviteToDeveloperCloud_Test() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
    	wrapList[0].check = true;
    	
    	controller.inviteToDeveloperCloud();
    	Test.StopTest();
    }
    
    public static testMethod void disableFromDeveloperCloud_Test() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	List<OCSDC_AdminCompCon_New.WrapperClass> wrapList = controller.getWrapperList();
    	wrapList[0].check = true;
    	
    	controller.disableFromDeveloperCloud();
    	Test.StopTest();
    }
    
    public static testMethod void getOption_Test() {
    	Test.StartTest();
    	Map<String, String> pickListVal = new Map<String, String>{ 'OncoPeer Only Users' => 'OncoPeer Only Users', 'Current Users' => 'Current Users',
                                                				   'Invited Users' => 'Invited Users', 'Admin Disabled Users' => 'Admin Disabled Users',
                                                				   'Self Disabled Users' => 'Self Disabled Users', 'OncoPeer Contributors' => 'OncoPeer Contributors'};
        List<SelectOption> options = new List<SelectOption>();
        OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
        options = controller.getOptions();
        
        List<SelectOption> expectedOptions = new List<SelectOption>();
        
        for(String str : pickListVal.keySet()) {
        	expectedOptions.add(new SelectOption(str, pickListVal.get(str)));
        }
        
        system.assertEquals(options.size(), expectedOptions.size());
    	Test.StopTest();
    }
    
    // Test Method for getCountries()
    static testMethod void getCountries_Test() {
    	Test.StartTest();
    	List<SelectOption> expectedOptions = new List<SelectOption>();
    	Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
		List<Schema.PickListEntry> ple = fieldResult.getPickListValues();		// get the Country__c picklist values
		expectedOptions.add(new SelectOption('',''));
		for( Schema.PickListEntry f : ple ) {
			expectedOptions.add(new SelectOption(f.getLabel(), f.getValue()));
		}
    	
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	List<SelectOption> actualOptions = controller.getCountries();
    	
    	system.assertEquals(actualOptions, expectedOptions);
    	Test.StopTest();
    }
    
    // test method for updating Contributors Devloper Status i.e assigning Developer Cloud permission sets
    public static testMethod void updateContributorsDeveloperStatus_Test() {
    	/**internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'Varian Community Contributor');
    	insert internalUsr;**/
    	Test.StartTest();
    	system.runas(internalUsr) {
    		OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    		controller.updateContributorsDeveloperStatus(new List<Id>{internalUsr.Id});
    	}
    	Test.StopTest();
    }
    
    // test method when userId passed is of size 0
    public static testMethod void updateContributorsDeveloperStatus_EmptyListTest() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.updateContributorsDeveloperStatus(new List<Id>());
    	Test.StopTest();
    }
    
    // test method when checkbox is checked and User is invited using Invite Button
    public static testMethod void updateDevloperContactStatus_Invited_Test() {
    	Test.StartTest();
    	Set<Id> contIdSet = new Set<Id>();
    	for(Contact cont : contactList)
    		contIdSet.add(cont.Id);
    		
    	system.runas(internalUsr) {
    		OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    		controller.updateDevloperContactStatus(contIdSet, true);
    	}
    	Test.StopTest();
    }
    
    // test method when checkbox is unchecked and User is invited using Invite Action
    public static testMethod void updateDevloperContactStatus_NotInvited_Test() {
    	Test.StartTest();
    	Set<Id> contIdSet = new Set<Id>();
    	for(Contact cont : contactList)
    		contIdSet.add(cont.Id);
    	
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.updateDevloperContactStatus(contIdSet, false);
    	Test.StopTest();
    }
    
    // test method for updateDeveloperContactStatus() for Excpetion Coverage
    public static testMethod void updateDeveloperContactStatus_ExceptionTest() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.updateDevloperContactStatus(null, false);
    	Test.StopTest();
    }
    
    // test method assign permission set to Contributors
    public static testMethod void assignPermissiontoContributor_Test() {
    	/**internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'Varian Community Contributor');
    	insert internalUsr;**/
    	Test.StartTest();
    	Id permissionSetId = OCSDC_AdminCompCon_NewTest.getContributorPermissionSet().Id;
    	OCSDC_AdminCompCon_New.assignPermissiontoContributor(new List<Id>{internalUsr.Id}, permissionSetId);
    	
    	List<PermissionSetAssignment> perSetAssign = [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: permissionSetId AND AssigneeId =: internalUsr.Id];
    	//system.assertEquals(1, perSetAssign.size());
    	Test.StopTest();
    }
    
    // test method for assignPermissiontoContributor() Exception Coverage
    public static testMethod void assignPermissiontoContributor_ExpTest() {
    	Test.StartTest();
    	Id permissionSetId = OCSDC_AdminCompCon_NewTest.getContributorPermissionSet().Id;
    	try {
    		OCSDC_AdminCompCon_New.assignPermissiontoContributor(null, permissionSetId);
    	} catch(Exception e) {
    		
    	}
    	Test.StopTest();
    }
    
    // test method insert the Notification records for External User i.e. Community Member when invited
    public static testMethod void insertNotifications_Test() {
    	Test.StartTest();
    	List<Id> contIdList = new List<Id>();
    	for(Contact con : contactList)
    		contIdList.add(con.Id);
    	//List<User> usrList = OCSDC_AdminCompCon_NewTest.createUsers(contIdList);
    	
    	OCSDC_AdminCompCon_New.insertNotifications(ContIdList, new List<Id>());
    	Test.StopTest();
    }
    
    // test method insert the Notification records for Internal User i.e. Contributors when invited
    public static testMethod void insertNotifications_Contributors_Test() {
    	/**internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'Varian Community Contributor');
    	insert internalUsr;**/
    	Test.StartTest();
    	OCSDC_AdminCompCon_New.insertNotifications(new List<Id>(), new List<Id>{internalUsr.Id});
    	Test.StopTest();
    }
    
    // Test method for reload(), test method will test with AccountName and without Country
    public static testMethod void reload_WithAccountNameTest() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.accountName = 'Test Account';
    	controller.country = '';
    	
    	controller.reload();
    	Test.StopTest();
    }
    
    // Test method for reload(), test method will test without AccountName and with Country
    public static testMethod void reload_WithCountryTest() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.accountName = '';
    	controller.country = 'India';
    	
    	controller.reload();
    	Test.StopTest();
    }
    
    // Test method for reload(), test method will test with AccountName and with Country
    public static testMethod void reload_Test() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.accountName = 'Test Account';
    	controller.country = 'India';
    	
    	controller.reload();
    	Test.StopTest();
    }
    
    // Test Method for Wrapper Class when Contact is not null i.e. external users with contact and user records
    public static testMethod void WrapperClass_Test() {
    	Test.StartTest();
    	Contact newContact = new Contact();
    	User newUser = new User(); 
    	OCSDC_AdminCompCon_New.WrapperClass controller = new OCSDC_AdminCompCon_New.WrapperClass(newContact, newUser);
    	Test.StopTest();
    }
    
    // Test Method for Wrapper Class when Contact is null i.e. internal users (Contributors) with no Contact Record
    public static testMethod void WrapperClass_Test2() {
    	Test.StartTest();
    	User newUser = new User(); 
    	OCSDC_AdminCompCon_New.WrapperClass controller = new OCSDC_AdminCompCon_New.WrapperClass(null, newUser);
    	Test.StopTest();
    }
    
    public static testMethod void disableIndividualContact_Test() {
    	Test.StartTest();
    	OCSDC_AdminCompCon_New controller = new OCSDC_AdminCompCon_New();
    	controller.selectedContactId = contactList[0].Id;
    	system.runas(internalUsr) {
    		controller.disableIndividualContact();
    	}
    	Test.StopTest();
    }
    
}
/*
Name        : OCSDC_AdminOncoContributors
Created By  : Shital Bhujbal (Varian India)
Date        : 7 May, 2015
Purpose     : A controller written to add oncopeer contributors to developer cloud.
*/

public without sharing class OCSDC_AdminOncoContributors {

    public List<User> oncoContributors = new List<USer>();
    public String currentTab {get;set;}
    public String title {get;set;}
    public ApexPages.StandardSetController con {get;set;}
    static final Integer PAGE_SIZE = 10;
    public boolean isSelectAll {get; set;}
    public List<WrapperUser> wrapperUserList{get; set;}
    //public String selectedContactId {get;set;}
    public Id permissionSetId;
    
    public OCSDC_AdminOncoContributors() {
        isSelectAll = false;
        permissionSetId = [Select Id, Name From PermissionSet Where Name=:Label.OCSDC_VMS_Community_Members_Permissions].Id;  
    }
    
    public class WrapperUser {
        
        public boolean isSelected {get;set;}
        public User user {get;set;}
        //public boolean isAdded {get;set;}
        
        public WrapperUser (User ur,Boolean isSelectAll) {
            this.user= ur;
            this.isSelected = isSelectAll;
        }
    }
    
    public void selectAll(){

    }
    
    // Invite contributor to developer cloud
    public PageReference inviteToDeveloperCloudList(){
        Set<ID> userIds = new Set<ID>();
        
        for(WrapperUser wrapUser : wrapperUserList){
            if(wrapUser.isSelected){
                userIds.add(wrapUser.user.Id);
            }
        }
        updateInviteDevCloud(userIds);        
        isSelectAll = false;
        selectAll();
        searchRecords();
        return null;
    }

    private void updateInviteDevCloud(Set<Id> userIds) {   
        
        List<PermissionSetAssignment> lstPermissionSetAssignments = new List<PermissionSetAssignment>();
        Map<String, User> mapUsers = new Map<String, User>();
        Savepoint sp;
        
        try {
          sp = Database.setSavepoint();
            if(userIds.size() > 0){
              inserNotifications(userIds);
            }
            //Assign permissionset Id to contributor
            if(userIds.size()>0){
                for(ID usrID:userIds){
                
                    PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
                    perSetAssign.PermissionSetId=permissionSetId;
                    perSetAssign.AssigneeId = usrID;
                    lstPermissionSetAssignments.add(perSetAssign);
                }
                if(lstPermissionSetAssignments.size()>0)
                { insert lstPermissionSetAssignments; }
            }
        }
        catch(Exception ex) {
          Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        searchRecords();
    }
    
    public Boolean isCallSearchRecords {
        get {
            if(isCallSearchRecords == null || isCallSearchRecords == false) {
                searchRecords();
            }
            return true;
        }
        set;
    }
    //Method to get user records on the basis of current tab.
    public void searchRecords() {
        List<PermissionSetAssignment> extPermSetAssignments = new List<PermissionSetAssignment>();
        List<ID> assignedUsers = new List<ID>();
        extPermSetAssignments = [SELECT Id, AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId =: permissionSetId];
        
        for(PermissionSetAssignment pr : extPermSetAssignments){
            assignedUsers.add(pr.AssigneeId);
        }
        isCallSearchRecords = true;
        oncoContributors =[select Id,Name,isActive,Email,OCSUGC_User_Role__c from user where OCSUGC_User_Role__c='Varian Community Contributor' AND isActive=true AND Id NOT IN: assignedUsers];

        con = new ApexPages.StandardSetController(oncoContributors);
        con.setPageSize(PAGE_SIZE);
        getWrapperUsers();
        title ='There are '+oncoContributors .size()+' Oncopeer Contributors';        
    }

    public List<WrapperUser> getWrapperUsers() {
        wrapperUserList = new List<WrapperUser>();
        if(con == null)
             return wrapperUserList;

        for(User usr :(List<User>) con.getRecords()) {
            wrapperUserList.add(new WrapperUser(usr,isSelectAll));
        }
        return wrapperUserList;
    }
    
    public void showSelectedTab(){
        System.debug('In showSelectedTab');
    }
    
    @future
    public static void inserNotifications(Set<Id> userIds){
        List<User> lstUser = new List<User>();
        List<OCSUGC_Intranet_Notification__c> lstNotifications = new List<OCSUGC_Intranet_Notification__c>();
        OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
        String pageURL = '';
        
        for(User usr : [SELECT Id,OCSUGC_User_Role__c
                          FROM User
                          WHERE Id=:userIds
                          LIMIT :OCSUGC_Constants.SF_QUERY_LIMIT]) {
           
           lstUser.add(usr);                 
        }
        if(lstUser.size()>0){
            for(User ur:lstUser){
                    //pageURL = 'OCSUGC_TermsOfUse?dc=true&contId='+ur.id;
                    pageURL = 'OCSDC_Home?dc=true';
                    tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(Label.OCSDC_NetworkName,ur.Id, '' ,Label.OCSDC_has_invited_to_join_DC,pageURL);
                    lstNotifications.add(tempObj);                     
          }            
        }
        if(lstNotifications.size() > 0){
              insert lstNotifications;
        }
    }
 
    public PageReference First() {
        if(con == null)
            return null;
        con.first();
        return null;
    }

    public PageReference Previous() {
         if(con.getHasPrevious())
             con.previous();
         return null;
    }

    public PageReference Next() {
         if(con.getHasNext()){
             con.next();
             }
         return null;
    }

    public PageReference Last() {
        if(con == null)
            return null;
        con.last();
        return null;
    }

}
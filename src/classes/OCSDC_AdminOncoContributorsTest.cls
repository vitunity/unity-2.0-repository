@isTest

private class OCSDC_AdminOncoContributorsTest{

    static testmethod void test() {
        System.Test.startTest();

        Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
        User tempUser = OCSUGC_TestUtility.createStandardUser(true, adminProfile.Id, 'test', 'None');

        system.runAs(tempUser){
           
        List<User> userList = new List<User>();
            
        User tempUser1 = OCSUGC_TestUtility.createStandardUser(true, adminProfile.Id, 'test', 'Varian Community Contributor');            
        User tempUser2 = OCSUGC_TestUtility.createStandardUser(true, adminProfile.Id, 'test', 'Varian Community Contributor');

        PageReference pgRef = Page.OCSDC_CommunityAdministration;
        Test.setCurrentPageReference (pgRef);
        OCSDC_AdminOncoContributors adminCon = new OCSDC_AdminOncoContributors();
        OCSDC_AdminOncoContributors.WrapperUser wrapUser = new OCSDC_AdminOncoContributors.WrapperUser(tempUser2,true);
          
          adminCon.isSelectAll = true;
          adminCon.currentTab = 'oncoPeerContributors';
          adminCon.searchRecords();
          adminCon.selectAll();
          adminCon.inviteToDeveloperCloudList();
          adminCon.First();
          adminCon.Next();
          adminCon.Last();
          adminCon.Previous();
          adminCon.showSelectedTab();  
          adminCon.getWrapperUsers();
          Boolean isCallSearch = adminCon.isCallSearchRecords;
      Test.stopTest();
  
      }
    }

}
/*
Name        : OCSDC_AppDetailController 
Updated By  : Naresh K Shiwani (Appirio India)
Date        : 09th March, 2015
Purpose     : An Apex controller to get Information for OCSDC_AppDetail.page	Ref No.		: T-368862
16/05/2015	Naresh K Shiwani	Updated acceptApplicationTerms()		T-395854
19/05/2015	Naresh K Shiwani	Added logic for appVideoURL				T-396448
21/05/2015	Naresh K Shiwani	Added logic for Term and condition 		T-397021
								modified acceptApplicationTerms(), hasAcceptedTerms()
22/09/2015  Shital Bhujbal		Modified method	isWebAppByType()	 ONCO - 388						
*/
public without sharing class OCSDC_AppDetailController extends OCSUGC_Base {
	
	public OCSUGC_Intranet_Content__c 		appData	{get;set;}
	public String 					  applicationId {get;set;}
	public String 					  	appImageURL	{get;set;}
	public String 					  	appVideoURL	{get;set;}
	public Boolean 		hasAcceptedApplicationTerms {get;set;}
    public Boolean                         isWebApp {get;set;}
	
	//Constructor
	public OCSDC_AppDetailController(){
		populateData();
		hasAcceptedApplicationTerms = isManagerOrContributor || hasAcceptedTerms();
		//hasAcceptedApplicationTerms = hasAcceptedTerms();
        isWebApp = isWebAppByType();
		
	}
	private boolean hasAcceptedTerms() {
	  List<OCSUGC_TermsConditions_UserLog__c> lstUserTerms = [SELECT Id
		                                             		  FROM OCSUGC_TermsConditions_UserLog__c
		                                            		  WHERE OCSUGC_Contact_Id__c = :contactId
		                                            		  AND OCSDC_App_Name__c =: appData.OCSUGC_Name__c
		                                                      limit 1
		                                                     ];
	  return lstUserTerms!=null && lstUserTerms.size() > 0;
	}

  private boolean isWebAppByType(){
    //if(appData.OCSUGC_Name__c == 'Varian Veritas')
    if(appData.OCSDC_Application_Type__c == 'Web')
      return true;
    else
      return false;
  }
	// @description: This method sets Application Data
    // @param: none
    // @return: void
    // Initial
	public void PopulateData() {
		applicationId	= ApexPages.currentPage().getParameters().get('Id');
		for(OCSUGC_Intranet_Content__c ic : [SELECT Id,
												 OCSUGC_Name__c,
   	 											 OCSUGC_Title__c,
   	 											 OCSDC_App_Version__c,
   	 											 OCSDC_App_ReleaseDate__c,
   	 											 OCSDC_App_Overview__c,
   	 											 OCSDC_App_Features__c,
   	 											 OCSDC_App_Video_URL__c,
   	 									  		 OCSUGC_Description__c,
   	 									  		 OCSDC_App_FAQs__c,
   	 									  		 OCSDC_Application_Type__c,
   	 									  		 OCSDC_App_Additional_Files_URL__c,
   	 									  		 OCSDC_App_UsageStatistics__c,
   	 									  		 OCSDC_App_Files_URL__c,
   	 									  		 OCSDC_App_Search_Terms__c,
//   	 									  		 OCSDC_App_ExternalURL__c,
   	 									  		 OCSDC_App_TermsOfUse__c
//   	 									  		 (select OCSDC_App_External_URL__c
//   	 									  		 	from External_URLs__r)
	   	                                     FROM OCSUGC_Intranet_Content__c
	   	                                     WHERE id =: applicationId
	   	                                     AND RecordType.Name =:OCSUGC_Constants.RT_Application
	   	                                     limit 1] ){
			appData = ic;
			fetchApplicationImage(appData.id);
			if(appData.OCSDC_App_Video_URL__c != null){
				appVideoURL = appData.OCSDC_App_Video_URL__c.replace('watch?v=', 'v/');
			}
			System.debug('appDataaaaaaaaaaaaaa====='+appData);
		}

	}
	
	// @description: This method sets Application Data
    // @param: none
    // @return: void
    // Initial
	public void fetchApplicationImage(String applicationId) {
		for(Attachment att :[Select id, ContentType, name
                             From Attachment
                             Where ParentId =: applicationId
                             order by lastmodifieddate desc]) {
        	if(att.ContentType.startsWith('image/') && att.Name.equalsIgnoreCase(Label.OCSDC_Attached_FileName)){
            	appImageURL = '/servlet/servlet.FileDownload?file='+att.id;
			}
        }
	}
	
	// @description: This method is called when user accept application terms
    // @param: NA
    // @return: Refreshes page
    // 18-Mar-2015  Puneet Sardana  Ref T-371073
    public PageReference acceptApplicationTerms() {
    	System.debug('Start acceptApplicationTerms');
    	hasAcceptedApplicationTerms = true;
    	OCSUGC_TermsConditions_UserLog__c termsLog = new OCSUGC_TermsConditions_UserLog__c();
    	termsLog.OCSUGC_Contact_Id__c = contactId;
    	termsLog.OCSUGC_Date_of_acceptance_TC__c = DateTime.now();
    	termsLog.OCSUGC_Terms_Conditions__c = appData.OCSDC_App_TermsOfUse__c;
    	termsLog.OCSDC_App_Name__c = appData.OCSUGC_Name__c;
    	try {
    	  	insert termsLog;
    	  	
          if(isWebApp) {
            PageReference objPageReference = new PageReference(appData.OCSDC_App_Files_URL__c);
            objPageReference.setRedirect(true);
            return objPageReference;            
          } else {
            PageReference objPageReference = new PageReference('/'+appData.OCSDC_App_Files_URL__c);
            objPageReference.getParameters().put('dc',''+isDC);
            objPageReference.setRedirect(true);
            return objPageReference;            
          }
    	}
    	catch(Exception ex) {
    		System.debug('Error '+ex.getMessage()+' '+ex.getStackTraceString());
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
    	}
    	return null;
    }
	
}
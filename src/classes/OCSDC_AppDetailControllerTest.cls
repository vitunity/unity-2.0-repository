//21-05-2015  Puneet Sardana  Added new test class for OCSDC_AppDetailController T-392114
@isTest(SeeAllData=false)
public class OCSDC_AppDetailControllerTest {
   static Profile admin,portal,manager;   
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static Network ocsugcNetwork, ocsdcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert; 
   static List<CollaborationGroup> lstGroupInsert;
   static String ocsugcNetworkString, ocsdcNetworkString;
   static OCSUGC_Intranet_Content__c appDetail;
   static OCSUGC_TermsConditions_UserLog__c termsAndLog;
   static Attachment appDetailAttachment;
   static {  	
     admin          = OCSUGC_TestUtility.getAdminProfile();
     portal         = OCSUGC_TestUtility.getPortalProfile(); 
     manager        = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     ocsugcNetworkString  = OCSUGC_TestUtility.getCurrentCommunityNetworkId(false);
     ocsdcNetworkString   = OCSUGC_TestUtility.getCurrentCommunityNetworkId(true); 
     lstUserInsert  = new List<User>();   
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
     insert lstUserInsert;     
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Private',ocsugcNetwork.id,false));
     insert lstGroupInsert;
     appDetail = OCSUGC_TestUtility.createIntranetContent('Application Test 1',OCSUGC_Constants.RT_Application,null,'Team','Test');
     appDetail.OCSDC_App_Video_URL__c = 'https://www.youtube.com/watch?v=QBQStAlS2oQ';
     insert appDetail;
     termsAndLog = OCSUGC_TestUtility.createTermsConditionsLog(appDetail.Name,contact1.Id);
     insert termsAndLog;
     appDetailAttachment = OCSUGC_TestUtility.createAttachment(false, appDetail.Id);
     appDetailAttachment.Name = Label.OCSDC_Attached_FileName;
     appDetailAttachment.ContentType = 'image/png';
     appDetailAttachment.Body = Blob.valueOf('test body image');
     insert appDetailAttachment;
  }
  
  
   public static testmethod void test_AppDetailController() {  
   	  Test.startTest();
   	    PageReference ref = Page.OCSDC_AppDetail;
        ref.getParameters().put('dc','true');
        ref.getParameters().put('id',appDetail.Id);        
        Test.setCurrentPage(ref);
   	    OCSDC_AppDetailController appDetailController = new OCSDC_AppDetailController(); 
   	    appDetailController.acceptApplicationTerms();  	  
   	  Test.stopTest();
   }
}
/*
Name        : OCSDC_CommunityAdministrationCon 
Updated By  : Naresh K Shiwani (Appirio India)
Date        : 17 Feb, 2015
Purpose     : An Apex controller to get Information for OCSDC_CommunityAdministration page.

Refrence    : T-362407
*/
public without sharing class OCSDC_CommunityAdministrationCon
 {	
	public String currentTabPage {get;set;}
	
	public OCSDC_CommunityAdministrationCon () {
        currentTabPage = 'oncoPeerUsers';
    }
    public PageReference changeCurrentTab() {
    	return null;
    }
}
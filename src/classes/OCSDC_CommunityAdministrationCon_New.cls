public without sharing class OCSDC_CommunityAdministrationCon_New {
    
    public Boolean newInvite{get;set;}
    
    public OCSDC_CommunityAdministrationCon_New() {
        newInvite = false;
        newInvite = (ApexPages.CurrentPage().getParameters().get('invite') == 'true') ? true : false ;
    }
}
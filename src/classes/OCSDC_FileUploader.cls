/*Name        : OCSDC_FileUploader controller
Created By  : Rishabh Verma (@varian)
Date        : 22th July, 2015
Purpose     : An Apex controller to upload file for sending bulk developer cloud invitations.
*/

public without sharing class OCSDC_FileUploader { 

    public String currentTab {get;set;}
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    
    public set<string> emailsStringSet = new set<string>();
    public list<Contact> conList = new list<Contact>();
    public list<Contact> conListToUpdate = new list<Contact>();
    public Map<String, Contact> mapContacts = new Map<String, Contact>();
    public List<OCSUGC_Intranet_Notification__c> lstNotifications = new List<OCSUGC_Intranet_Notification__c>();
    public OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
    public String pageURL = '';  
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
      
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            emailsStringSet.add(inputvalues[0].trim());
        }
           
        conList = [Select Id, Email, OCSDC_InvitationDate__c, OCSDC_UserStatus__c,
                   MyVarian_Member__c, OCSUGC_UserStatus__c
                   from Contact 
                   where Email IN: emailsStringSet and OCSDC_UserStatus__c != 'Invited'
                   and MyVarian_Member__c = true and OCSUGC_UserStatus__c = 'OCSUGC Approved' ];

        for(Contact con :conList){
          con.OCSDC_UserStatus__c = OCSUGC_Constants.DevCloud_User_Status_Invited;
          con.OCSDC_InvitationDate__c = DateTime.now();
            conListToUpdate.add(con);
            mapContacts.put(con.Id,con);
        }
        
        if(mapContacts != null){
            for(User ur : [Select u.LastName, u.FirstName, u.Title, u.contactid, IsActive
                           From User u 
                           Where u.contactid=:mapContacts.keyset() and IsActive = true]){
                                pageURL = 'OCSUGC_TermsOfUse?dc=true&contId='+mapContacts.get(ur.contactid).id;
                                tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(Label.OCSUGC_NetworkName,ur.Id, '' ,Label.OCSDC_has_invited_to_join_DC,pageURL);
                                lstNotifications.add(tempObj);
            }
        }
        try{
            if(conListToUpdate.size() > 0) {
                update conListToUpdate;
            }
            if(lstNotifications.size() > 0){
                insert lstNotifications;
            } 
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
            ApexPages.addMessage(errormsg);
        }    
        
        PageReference successPage = Page.OCSDC_InvitationSuccessMessage;
        successPage.setRedirect(false);
        return successPage;
        
    }          
}
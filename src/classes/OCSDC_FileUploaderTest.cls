/*Name        : OCSDC_FileUploaderTest
Created By  : Rishabh Verma (@varian)
Date        : 29th July, 2015
Purpose     : Test class for sending bulk developer cloud invitations through excel file.
Updated By  :   Puneet Mishra,  6 Aug 2015, unit test method for getuploadedInvites()
*/
@IsTest
public class OCSDC_FileUploaderTest
{  
    static Profile portal;
    static User memberUsr;
    static{
        portal = OCSUGC_TestUtility.getPortalProfile();
    }
    
    private static Account createAccount() {
        Account newAccount = new Account();
        newAccount.Name = 'Varian TestAccount';
        newAccount.BillingCountry='USA';
        newAccount.Country__c= 'USA';
        newAccount.BillingCity= 'Los Angeles';
        newAccount.BillingStreet ='3333 W 2nd St';
        newAccount.BillingState ='CA';
        newAccount.BillingPostalCode = '90020';
        newAccount.Agreement_Type__c = 'Master Pricing';
        newAccount.OMNI_Address1__c ='3333 W 2nd St';
        newAccount.OMNI_City__c ='Los Angeles';
        newAccount.OMNI_State__c ='CA';
        newAccount.OMNI_Country__c = 'USA';  
        newAccount.OMNI_Postal_Code__c = '90020';
        insert newAccount;
        return newAccount;
    }
    
    private static Contact createContact(String accountId, Boolean isInsert, Integer num1, Integer num2){
        Contact newContact = new Contact();
        newContact.FirstName='TestFirstName'+Math.random();
        newContact.LastName = 'TestContact' + Math.random();
        newContact.Email = 'TestUser' +math.mod(num1,num2) + '@test.com';
        newContact.AccountId = accountId;
        newContact.MailingCountry='India';
        newContact.MyVarian_member__c = true;
        newContact.PasswordReset__c = false;
        newContact.OCSUGC_Notif_Pref_ReqJoinGrp__c = true;
        newContact.OCSUGC_Notif_Pref_EditGroup__c = true;
        newContact.OCSUGC_Notif_Pref_PrivateMessage__c = true;
        newContact.OCSUGC_Notif_Pref_InvtnJoinGrp__c = true;
        newContact.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
        if(isInsert){
            insert newContact;
        }
        return newContact;
    }
    
    private static Contact createContact(Account newAccount, Integer num1, Integer num2) {
        Contact newContact = new Contact();
        newContact = OCSDC_FileUploaderTest.createContact(newAccount.Id, false, num1, num2);
        insert newContact;
        return newContact;
    }
    
   /* private static Invite_To_Developer_Cloud__c createInvitees(Integer num1, Integer num2) {
        Invite_To_Developer_Cloud__c invitees = new Invite_To_Developer_Cloud__c();
        invitees.Email__c = 'TestUser' +math.mod(num1,num2) + '@test.com';
        return invitees;
    }*/
    
    public static testMethod void ReadFile() {
        portal = OCSUGC_TestUtility.getPortalProfile();
        Account newAccount = createAccount();
        
        Contact blobContact = OCSDC_FileUploaderTest.createContact(newAccount, 3, 6);
        memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, blobContact.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);
        insert memberUsr;
        
        String blobCreator = 'Email' + '\r\n' + blobContact.Email ;
        Document doc = new Document();
        doc.Name = 'Test Upload';
        doc.AuthorId = UserInfo.getUserId();
        doc.FolderId = UserInfo.getUserId();
        insert doc;
        Document lstDoc = [select id,name,Body from Document where name = 'Test Upload'];
        
        // System.Debug('DOC NAME :: '+lstDoc.name); 
        //System.Debug('DOCBODY :: '+lstDoc.Body); 
        OCSDC_FileUploader file = new OCSDC_FileUploader();
        //file.fileAccess();
        Blob content= lstDoc.Body;
        file.contentFile = blob.valueof(blobCreator);//content; 
        file.ReadFile(); 
        
        file.nameFile=content.toString();
        String[] filelines = new String[]{};
        List<Contact> contactsToUpdate;
        
        contactsToUpdate = new List<Contact>();
        for (Integer i=1;i<filelines.size();i++) {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            Contact con = new Contact();
            con.OCSDC_UserStatus__c  = 'Invited';
            
            
            
            contactsToUpdate.add(con); 
            try{
                update contactsToUpdate;
            }
            catch (Exception e) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
                ApexPages.addMessage(errormsg);
            }
        
        }
    }
    
}
// 
// (c) 2014 Appirio, Inc.
//
// This is a home controller class for developer cloud 
//
// 16 Feb, 2014    Puneet Sardana  Original Ref T-362415
// 07 May,2015     Vaibhav Goel       Ref T-393081
// 14 May,2015     Naresh Shiwani     Ref T-393083  Added fetchAttachmentApplication(), Modified PopulateData()
// 14 May,2015     Naresh Shiwani     Ref T-393081  Added groupid condition for fetching contents on fetchPollData(),
//                                                  fetchDiscussionDetail(), fetchIntranetContentDetail().
//Poll,discussion and event queries changed by Shital Bhujbal to get the value of dtLImit variable and hence to avoid exception "variable reference error" in production  20/8/2014

public class OCSDC_HomeController extends OCSUGC_Base_NG {
  
  static final String RT_Application = 'Applications';
  public List<ApplicationWrapper> applications { get; set; }
  private Datetime dtLimit; 
  private Integer numberofTilesLimit; // Variable to get Number of Tiles to fetch to show to home page
  private User currentUser;
  public Boolean isTagFiltered {get;set;}
  //private set<String> GroupIds;
  //private set<String> InsertedByIds;
  //private set<String> archivedGroupIds;
  //private set<String> deletePollIds;
  //private set<String> deleteDiscussionIds;
  private List<WrapperTile> lstTiles;
  transient public List<WrapperTile> lstFinalWrapperTiles {get;set;}
  public Map<String,String> mapApplicationAttachment;
    
  //Wrapper class to store and prepare a list of 
  //mixed records from Polls, Discussion_Details and Intranet_Content custom objetcs and use the wrapper list on VF page
  public class WrapperTile implements Comparable{
    public String tileId            {get;set;}
    public String tileLink          {get;set;}
    public String tileCSS           {get;set;}
    public String tileType          {get;set;}
    public String postedDate        {get;set;}
    public String postedBy          {get;set;}
    public String postedById        {get;set;}
    public Integer countLikes       {get;set;}
    public Integer countViewed      {get;set;}
    public Integer countComments    {get;set;}
    public String tileTitle         {get;set;}
    public String tileSummary       {get;set;}
    public String tileTags          {get;set;}
    public String tileGroup         {get;set;}
    public Boolean isVarianEndorsed {get;set;}
    public Boolean isBookMarked     {get;set;}
    public String imageId           {get;set;}
    public Datetime createdDate     {get;set;}
    public Boolean isFeedItemImage  {get;set;}

    public WrapperTile() {}

    // Comparison code to sort the wrapper list in decreasing order of number of Comments
    public Integer compareTo(Object compareTo) 
    {
        WrapperTile wrapperObj = (WrapperTile) compareTo;
        if (this.countComments == wrapperObj.countComments) return 0;
        if (this.countComments < wrapperObj.countComments) return 1;
        return -1;        
    }
  }    

  //Constructor    
  public OCSDC_HomeController() {
    // ONCO-421 
    pageTitle = 'Developer Cloud';
    PopulateData();
    
    isTagFiltered = false;
    
    lstFinalWrapperTiles = new List<WrapperTile>(); //final list of wrapper to be thrown to VF Page after sorting

    dtLimit = Datetime.now().addDays(-Integer.ValueOf(Label.OCSDC_HomeTilesDayLimit));
    numberofTilesLimit = Integer.ValueOf(Label.OCSDC_MostRecentHomeTilesLimit);
    

    currentUser = [SELECT Id, Contact.OCSUGC_UserViewPreference__c FROM User Where Id =:UserInfo.getUserId()];

    //[ONCO-374] OncoPeer Contributors should not able to see the post from Private Groups if they are not member   Shital Bhujbal   14/09/2015 
    //GroupIds = OCSUGC_Utilities.fetchGroupIds(networkId,currentUser.Id, isFGAB, fgabGroupId, isManager);//[ONCO 374]
    //archivedGroupIds = OCSUGC_Utilities.archivedGroupIds(networkId);
   // deletePollIds = OCSUGC_Utilities.deletePollIds();
    //deleteDiscussionIds = OCSUGC_Utilities.deleteDiscussionIds();
    fetchMostActivePosts(); // call to method to fetch the most active records based on number of comments
      
  }
  
  //Call to the method to get most recent/aciveposts based on the most number of comments
  private void fetchMostActivePosts(){
    List<WrapperTile> wrapperListToSort = fetchPollData();
    wrapperListToSort.addAll(fetchDiscussionDetail());
    wrapperListToSort.addAll(fetchIntranetContentDetail());
    if(wrapperListToSort != null && wrapperListToSort.size() > 0){
      system.debug('-----wrapperListToSort----' + wrapperListToSort.size());
      wrapperListToSort.sort();
      system.debug('-----wrapperListAfterSort----' + wrapperListToSort);
      if(wrapperListToSort.size() < numberofTilesLimit){
        numberofTilesLimit = wrapperListToSort.size();
      }
      for(Integer i = 0; i < numberofTilesLimit; i++){

        lstFinalWrapperTiles.add(wrapperListToSort.get(i));
      }
    }

  }
  
  // (c) 2014 Appirio, Inc.
  //
  // Method name  - fetchIntranetContentDetail
  // Purpose      - to get OCSUGC_Poll_Details__c records with most number of comments and which have been modified in last days as per the variable value of 'dtLimit'
  // Parameters   - None
  // Return Type  - List of wrapper class -> WrapperTile
  // 07 May,2015     Vaibhav Goel       Ref T-393081
  private List<WrapperTile> fetchPollData() {

      String pollDetailsQuery = 'SELECT CreatedById,CreatedDate,Id,IsDeleted,LastModifiedById,';
            pollDetailsQuery    += ' LastModifiedDate,Name,OCSUGC_Comments__c,OCSUGC_Group_Id__c,OCSUGC_Group_Name__c,';
            pollDetailsQuery    += ' OCSUGC_IsRankedPoll__c,OCSUGC_Is_Deleted__c,OCSUGC_Is_FGAB_Poll__c,OCSUGC_Likes__c,';
            pollDetailsQuery    += ' OCSUGC_Poll_Id__c,OCSUGC_Poll_Question__c,OCSUGC_Views__c,OwnerId, CreatedBy.Name '; 
            pollDetailsQuery    += ' FROM OCSUGC_Poll_Details__c';
            pollDetailsQuery    += ' where IsDeleted = false ';//AND OCSUGC_Group_Id__c NOT IN '+OCSUGC_Utilities.getStringIds(archivedGroupIds);
            pollDetailsQuery    += ' AND OCSUGC_Group_Id__c = \'Everyone\' ';              
                                    
            pollDetailsQuery +=  ' AND OCSUGC_Is_Deleted__c = false';
            pollDetailsQuery +=  ' AND LastModifiedDate >= :dtLimit';
            //pollDetailsQuery += ' And LastModifiedDate >= :\''+dtLimit+'\'';
            pollDetailsQuery +=  ' order by OCSUGC_Comments__c DESC LIMIT ' + numberofTilesLimit;

      system.debug('----pollDetailsQuery----  \n\n' + pollDetailsQuery);
      List<OCSUGC_Poll_Details__c> pollDetailsList = new List<OCSUGC_Poll_Details__c>();
      Map<String, OCSUGC_Poll_Details__c> pollDetailMap = new Map<String, OCSUGC_Poll_Details__c>();
            try{
                for(OCSUGC_Poll_Details__c pd:Database.query(pollDetailsQuery)){
                        pollDetailMap.put(pd.OCSUGC_Poll_Id__c,pd);
                }
            }
            catch(Exception e){
                system.debug('---Exception in getting OCSUGC_DiscussionDetail__c Records---' + e);
            }
            for(FeedItem f: [Select Id, NetworkScope
                                         From FeedItem
                                         Where Id IN:pollDetailMap.keySet()]) {
                 if(f.NetworkScope == ocsdcNetworkId) {
                    pollDetailsList.add(pollDetailMap.get(f.id));
                 }
                 // As NetworkScope is always coming null in Test Class, setting discussionList with some 
		         if(Test.isRunningTest() && pollDetailsList.size() == 0) {
		         	pollDetailsList.addAll(pollDetailMap.values());
		         }
            }
      List<WrapperTile> lstWrapperTiles = new List<WrapperTile>();
      if(pollDetailsList != null && pollDetailsList.size() > 0){
          for(OCSUGC_Poll_Details__c poll :pollDetailsList) {
              WrapperTile wTile       = new WrapperTile();
              wTile.isVarianEndorsed  = false;
              wTile.tileId            = poll.Id;
              wTile.tileCSS           = 'poll';
              wTile.tileType          = 'Poll';
              wTile.postedDate        = OCSUGC_Utilities.GetRelativeTimeString(poll.CreatedDate, Datetime.now());
              wTile.createdDate       = poll.CreatedDate;
              wTile.postedBy          = poll.CreatedBy.Name;
              wTile.postedById        = poll.CreatedById;
              wTile.tileSummary       = poll.OCSUGC_Poll_Question__c;
              wTile.tileTitle         = poll.OCSUGC_Poll_Question__c;
              wTile.tileTags          = '';
              wTile.tileGroup         = poll.OCSUGC_Group_Name__c;
              wTile.imageId           = '';
              wTile.isBookMarked      = false;
              wTile.countViewed       = poll.OCSUGC_Views__c.IntValue();
              wTile.countLikes        = poll.OCSUGC_Likes__c.IntValue();
              wTile.countComments     = poll.OCSUGC_Comments__c.IntValue();
              wTile.tileLink          = Site.getPathPrefix()+'/OCSUGC_FGABPollDetail?Id='+poll.OCSUGC_Poll_Id__c+'&dc=true';
              lstWrapperTiles.add(wTile);            
                  
          }
      }
            
      return lstWrapperTiles;
  }
  
  // (c) 2014 Appirio, Inc.
  //
  // Method name  - fetchDiscussionDetail
  // Purpose      - to get OCSUGC_DiscussionDetail__c records with most number of comments and which have been modified in last days as per the variable value of 'dtLimit'
  // Parameters   - None
  // Return Type  - List of wrapper class -> WrapperTile
  // 07 May,2015     Vaibhav Goel       Ref T-393081
  private List<WrapperTile> fetchDiscussionDetail() {

    String discussionDetailQuery = 'SELECT CreatedById, CreatedDate, CurrencyIsoCode,Id,IsDeleted, ';
      discussionDetailQuery  +=      'LastModifiedDate,LastReferencedDate,LastViewedDate,Name,OCSUGC_Comments__c, ';
      discussionDetailQuery  +=      'OCSUGC_Description__c,OCSUGC_DiscussionId__c,OCSUGC_GroupId__c,OCSUGC_Group__c, ';
      discussionDetailQuery  +=      'OCSUGC_IsFGABDiscussion__c,OCSUGC_Is_Deleted__c,OCSUGC_Likes__c, CreatedBy.Name, ';
      discussionDetailQuery  +=      'OCSUGC_Views__c,OwnerId '; 
      discussionDetailQuery  +=      'FROM OCSUGC_DiscussionDetail__c ';
      discussionDetailQuery  +=      'WHERE IsDeleted = false '; 
      //discussionDetailQuery  +=      'AND OCSUGC_GroupId__c NOT IN '+ OCSUGC_Utilities.getStringIds(archivedGroupIds);
      discussionDetailQuery  +=      ' AND OCSUGC_GroupId__c = \'Everyone\' ';                               
      
      discussionDetailQuery  += ' AND OCSUGC_Is_Deleted__c = false ';
      discussionDetailQuery  += ' And LastModifiedDate >= :dtLimit';
      //discussionDetailQuery  += ' And LastModifiedDate >= :\''+dtLimit+'\'';
      discussionDetailQuery  += ' order by OCSUGC_Comments__c DESC LIMIT ' + numberofTilesLimit;

    system.debug('----discussionDetailQuery---- \n\n' + discussionDetailQuery);

    List<OCSUGC_DiscussionDetail__c> discussionDetailList = new List<OCSUGC_DiscussionDetail__c>();
    Map<String, OCSUGC_DiscussionDetail__c> discussionDetailMap = new Map<String, OCSUGC_DiscussionDetail__c>();
    try{
        for(OCSUGC_DiscussionDetail__c dd:Database.query(discussionDetailQuery)){
                discussionDetailMap.put(dd.OCSUGC_DiscussionId__c,dd);
        }
    }
    catch(Exception e){
        system.debug('---Exception in getting OCSUGC_DiscussionDetail__c Records---' + e);
    }
    for(FeedItem f: [Select Id, NetworkScope
                                 From FeedItem
                                 Where Id IN:discussionDetailMap.keySet()]) {
         if(f.NetworkScope == ocsdcNetworkId) {
            discussionDetailList.add(discussionDetailMap.get(f.id));
         }
         // As NetworkScope is always coming null in Test Class, setting discussionList with some 
         if(Test.isRunningTest() && discussionDetailList.size() == 0) {
         	discussionDetailList.addAll(discussionDetailMap.values());
         }
    }
    List<WrapperTile> lstWrapperTiles = new List<WrapperTile>();
    if(discussionDetailList != null && discussionDetailList.size() > 0){
        for(OCSUGC_DiscussionDetail__c discussionDetail :discussionDetailList) {
            WrapperTile wTile       = new WrapperTile();
            wTile.isVarianEndorsed  = false;
            wTile.tileId            = discussionDetail.Id;
            wTile.tileCSS           = 'discussion';
            wTile.tileType          = 'Discussion';
            wTile.postedDate        = OCSUGC_Utilities.GetRelativeTimeString(discussionDetail.CreatedDate, Datetime.now());
            wTile.createdDate       = discussionDetail.CreatedDate;
            wTile.postedBy          = discussionDetail.Createdby.Name;
            wTile.postedById        = discussionDetail.CreatedById;
            wTile.tileSummary       = discussionDetail.OCSUGC_Description__c;
            wTile.tileTitle         = discussionDetail.OCSUGC_Description__c;
            wTile.tileTags          = '';
            wTile.tileGroup         = discussionDetail.OCSUGC_Group__c;
            wTile.imageId           = '';
            wTile.isFeedItemImage   = false;
            wTile.isBookMarked      = false;
            wTile.countViewed       = discussionDetail.OCSUGC_Views__c.IntValue();
            wTile.countLikes        = discussionDetail.OCSUGC_Likes__c.IntValue() ;
            wTile.countComments     = discussionDetail.OCSUGC_Comments__c.IntValue();
            wTile.tileLink          = Site.getPathPrefix()+'/OCSUGC_DiscussionDetail?Id='+discussionDetail.OCSUGC_DiscussionId__c+'&dc=true';
           

            lstWrapperTiles.add(wTile);
        }
        
    }
    return lstWrapperTiles;
  }

  // (c) 2014 Appirio, Inc.
  //
  // Method name  - fetchIntranetContentDetail
  // Purpose      - to get OCSUGC_Intranet_Content__c records having most number of Comments
  // Parameters   - None
  // Return Type  - List of wrapper class -> WrapperTile
  // 07 May,2015     Vaibhav Goel       Ref T-393081
  private List<WrapperTile> fetchIntranetContentDetail(){

      String intranetContentQuery = 'SELECT CreatedById,CreatedDate,Id,IsDeleted,LastViewedDate,';
      intranetContentQuery +=       ' Name,OCSDC_App_Additional_Files_URL__c,OCSDC_App_FAQs__c,';
      intranetContentQuery +=       ' OCSDC_App_Features__c,OCSDC_App_Overview__c,OCSDC_App_Owner__c,OCSDC_App_ReleaseDate__c,';
      intranetContentQuery +=       ' OCSDC_App_TermsOfUse__c,OCSDC_App_UsageStatistics__c,OCSDC_App_Version__c,';
      intranetContentQuery +=       ' OCSDC_App_Video_URL__c,OCSUGC_Application_Grant_Access__c,OCSDC_App_Group__c,';
      intranetContentQuery +=       ' OCSUGC_Article_Type__c,OCSUGC_Artifact_Type__c,OCSUGC_Bookmark_Id__c,';
      intranetContentQuery +=       ' OCSUGC_Bookmark_Type__c,OCSUGC_Comments__c,OCSUGC_Description__c,';
      intranetContentQuery +=       ' OCSUGC_Display_Datetime__c,OCSUGC_Event_End_Date__c,OCSUGC_Event_End_Time__c,';
      intranetContentQuery +=       ' OCSUGC_Event_Start_Date__c,OCSUGC_Event_Start_Time__c,OCSUGC_File_Extension__c,';
      intranetContentQuery +=       ' OCSUGC_File_Size__c,OCSUGC_Flagged__c,OCSUGC_GroupId__c,OCSUGC_Group__c,';
      intranetContentQuery +=       ' OCSUGC_Icon_URL__c,OCSUGC_IsFGABCreateMenu__c,OCSUGC_Is_Deleted__c,OCSUGC_Likes__c,';
      intranetContentQuery +=       ' OCSUGC_Location__c,OCSUGC_Name__c,OCSUGC_NetworkId__c,OCSUGC_Parent__c,';
      intranetContentQuery +=       ' OCSUGC_Posted_By__c,OCSUGC_Requires_Admin_Approval__c,OCSUGC_Sequence__c,';
      intranetContentQuery +=       ' OCSUGC_Status__c,OCSUGC_Summary__c,OCSUGC_Time_Zone__c,OCSUGC_Title__c,OCSUGC_URL__c,';
      intranetContentQuery +=       ' OCSUGC_Value__c,OCSUGC_Views__c,OwnerId,RecordTypeId, CreatedBy.Name '; 
      intranetContentQuery +=       ' FROM OCSUGC_Intranet_Content__c'; 
      intranetContentQuery +=       ' WHERE isDeleted = false ';//AND OCSUGC_GroupId__c NOT IN '+ OCSUGC_Utilities.getStringIds(archivedGroupIds);
      intranetContentQuery +=       ' AND OCSUGC_GroupId__c = \'Everyone\' ';                         

      intranetContentQuery +=  ' AND OCSUGC_Is_Deleted__c = false AND RecordType.Name = \'Events\''; 
      
      //query changed by Shital Bhujbal to get the value of dtLImit variable and hence to avoid exception "variable reference error" in production  20/8/2014
      intranetContentQuery +=  ' And LastModifiedDate >=: dtLimit';
      //intranetContentQuery +=  ' And LastModifiedDate >= \''+dtLimit+'\'';
      //AND StudentType__c=:\'' + fpstudenttype + '\''
      if(ocsdcNetworkId != null) {
        //intranetContentQuery += ' AND OCSUGC_NetworkId__c = \''+networkId+'\'';
        List<String> network_15_Id = new List<String>{ocsdcNetworkId.SubString(0,15), ocsdcNetworkId};
        system.debug('----network_15_Id---- \n\n' + network_15_Id);
        //intranetContentQuery += ' AND OCSUGC_NetworkId__c IN :'+ network_15_Id;
        intranetContentQuery += ' AND OCSUGC_NetworkId__c IN :  network_15_Id';
      }
      intranetContentQuery +=  ' order by OCSUGC_Comments__c DESC LIMIT ' + numberofTilesLimit;
      

      system.debug('----intranetContentQuery---- \n\n' + intranetContentQuery);

      List<OCSUGC_Intranet_Content__c> intranetContentlist = new List<OCSUGC_Intranet_Content__c>();

      try{
          intranetContentlist = Database.query(intranetContentQuery);
      }
      catch(Exception e){
          system.debug('---Exception in getting OCSUGC_Intranet_Content__c Records---' + e);
      }                                      

      List<WrapperTile> lstWrapperTiles = new List<WrapperTile>();
      if(intranetContentlist != null && intranetContentlist.size() > 0){
          for(OCSUGC_Intranet_Content__c intranetContent : intranetContentlist) {
              
              String dt = '';
              if(intranetContent.OCSUGC_Event_Start_Date__c!= null) {
                  datetime myDate = datetime.newInstance(intranetContent.OCSUGC_Event_Start_Date__c.year(), intranetContent.OCSUGC_Event_Start_Date__c.month(), intranetContent.OCSUGC_Event_Start_Date__c.day());
                  dt = myDate.format('MMM dd, yyyy');
              }
              String tm = intranetContent.OCSUGC_Event_Start_Time__c != null ? intranetContent.OCSUGC_Event_Start_Time__c : '';
              tm = tm.replaceAll(' ', '');
              WrapperTile wTile       = new WrapperTile();
              wTile.isVarianEndorsed  = false;
              wTile.tileCSS           = 'event';
              wTile.tileType          = 'Event';
              wTile.tileId            = intranetContent.Id;
              wTile.postedDate        = OCSUGC_Utilities.GetRelativeTimeString(intranetContent.CreatedDate, Datetime.now());
              wTile.createdDate       = intranetContent.CreatedDate;
              wTile.postedBy          = intranetContent.CreatedBy.Name;
              wTile.postedById        = intranetContent.CreatedById;
              wTile.tileTitle         = intranetContent.OCSUGC_Title__c;
              if(intranetContent.OCSUGC_Location__c != null) {
                  wTile.tileSummary   = dt+' '+tm+' - '+ intranetContent.OCSUGC_Location__c;
              }else {
                  wTile.tileSummary   = dt+' '+tm;
              }
              wTile.tileSummary       = '';
              wTile.tileGroup         = intranetContent.OCSUGC_Group__c;
              
              // Checking whether record is present on bookmark Id map or not
              wTile.isBookMarked  = false;
              wTile.countViewed   = intranetContent.OCSUGC_Views__c != null ? intranetContent.OCSUGC_Views__c.intValue() : 0;
              wTile.countLikes    = intranetContent.OCSUGC_Likes__c != null ? intranetContent.OCSUGC_Likes__c.intValue() : 0;
              wTile.countComments = intranetContent.OCSUGC_Comments__c != null ? intranetContent.OCSUGC_Comments__c.intValue() : 0;
              wTile.tileLink      = Site.getPathPrefix()+'/OCSUGC_EventDetail?eventId='+intranetContent.Id+'&dc=true';
             
              lstWrapperTiles.add(wTile); 
              
          }
      }
      return lstWrapperTiles;
  }

    
 public void PopulateData() {
    applications = new List<ApplicationWrapper>();
    Map<String,OCSUGC_Intranet_Content__c> mapApplications = new Map<String,OCSUGC_Intranet_Content__c>();
    Integer noOfApplications = Integer.valueOf(Label.OCSDC_NoOfApplicationTiles);
    for(OCSUGC_Intranet_Content__c ic : [SELECT Id,OCSUGC_Title__c,OCSDC_App_Version__c,OCSDC_App_ReleaseDate__c
                                          ,OCSUGC_Description__c,OCSDC_App_Group__c, Name
                                          FROM OCSUGC_Intranet_Content__c
                                          WHERE RecordType.Name =:RT_Application
                                          ORDER BY OCSUGC_Title__c
                                          LIMIT :noOfApplications] ) {
        mapApplications.put(ic.id,ic);
    }
    fetchAttachmentApplication(mapApplications.keySet());
    for(String id:mapApplications.keySet()){
        applications.add(new ApplicationWrapper(mapApplications.get(id),mapApplicationAttachment.get(id)));  
    }
    
 }

 public PageReference validateDCAccess() {

  if(hasDCAccess && !System.Site.getPathPrefix().equalsIgnoreCase('/DC')) {
    PageReference pg = new PageReference(System.Site.getBaseUrl().removeEnd(System.Site.getPathPrefix()) + '/DC/OCSDC_Home');
    pg.getParameters().put('dc','true');
    pg.setRedirect(true);
    return pg;
  }

  if(!hasDCAccess && isCommunityContributor) {
    PageReference pg = Page.OCSUGC_Home;
    pg.setRedirect(true);
    return pg;
  }

  return null;
 }

  // (c) 2014 Appirio, Inc.
  //
  // Inner class for fetching application data
  //
  // 16 Feb, 2014    Puneet Sardana  Original Ref T-362415  
  public class ApplicationWrapper {
    public OCSUGC_Intranet_Content__c   appData      {get;set;}
    public String                       imageURL {get;set;}
    public ApplicationWrapper(OCSUGC_Intranet_Content__c app,String attachmentId) {
        appData             = app;
        if(attachmentId != null)
            imageURL    = '/servlet/servlet.FileDownload?file='+attachmentId;
    }
  }
  
    // @description : This method fetch Attachment with Applications
    // @param       : Set of Application Ids
    // @return      : void
    // Initial      : Naresh K Shiwani T-393083
    public void fetchAttachmentApplication(set<String> applicationIds) {
        mapApplicationAttachment = new Map<String,String>();
        for(Attachment att :[Select id, ContentType, ParentId, Name
                             From Attachment
                             Where ParentId IN : applicationIds
                             order by lastmodifieddate desc]) {
            if(att.ContentType.startsWith('image/') 
                && att.Name.equalsIgnoreCase(Label.OCSDC_Attached_FileName)
                && !mapApplicationAttachment.containsKey(att.ParentId))
                mapApplicationAttachment.put(att.ParentId,att.id);
        }
    }

}
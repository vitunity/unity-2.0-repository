// (c) 2012 Appirio, Inc.
//
// Test class  for OCSDC_HomeController
//
// 18 Feb 2015    Puneet Sardana  Test class  for OCSDC_HomeController Ref T-363431
//
@isTest(SeeAllData=false) 
public with sharing class OCSDC_HomeControllerTest {
   static Profile admin,portal,manager;   
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static Network ocsugcNetwork;
   static CollaborationGroup cGroup;    
   static CollaborationGroup cGroup2;
   static OCSUGC_Poll_Details__c pollDetail;
   static FeedItem feedItem,feedItem1,feedItem2,feedItem3,feedItem4;
   static List<OCSUGC_Poll_Details__c> pollDetailList = new List<OCSUGC_Poll_Details__c>();
   static OCSUGC_DiscussionDetail__c discussionDetail,discussionDetail1; 
   static list<FeedItem> lstfeedItem;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile(); 
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
          
     cGroup = OCSUGC_TestUtility.createGroup('Test Group','Unlisted',ocsugcNetwork.id , true);
     cGroup2 = OCSUGC_TestUtility.createGroup('Test Group1','Unlisted',Label.OCSUGC_FocusGroupAdvisoryBoard,ocsugcNetwork.id , true);
     
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     
     lstUserInsert = new List<User>();   
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '1','NA Regional MO SW'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '2','NA Regional MO SW'));
     insert lstUserInsert;
     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;
        lstfeedItem = new list<FeedItem>();
        lstfeedItem.add(feedItem = OCSUGC_TestUtility.createFeedItem(lstUserInsert[0].id, false));
        lstfeedItem.add(feedItem1 = OCSUGC_TestUtility.createFeedItem(lstUserInsert[1].id, false));
        //lstfeedItem.add(feedItem = OCSUGC_TestUtility.createFeedItem(lstUserInsert[2].id, false));
        lstfeedItem.add(feedItem2 = OCSUGC_TestUtility.createFeedItem(lstUserInsert[3].id, false));
        lstfeedItem.add(feedItem3 = OCSUGC_TestUtility.createFeedItem(lstUserInsert[4].id, false));
        insert lstfeedItem;
     }      
     for(Integer i = 0; i<=3 ; i++){
         pollDetail = OCSUGC_TestUtility.createPollDetail(cGroup.id, 'Test Group' + i, lstfeedItem[i].id,'Test Survey' + i,false);
         pollDetail.OCSUGC_Is_Deleted__c = false;
         pollDetail.OCSUGC_Group_Id__c = 'Everyone';
         pollDetail.OCSUGC_Likes__c = i;
         pollDetail.OCSUGC_Views__c = i;
         pollDetail.OCSUGC_Comments__c = i;
         pollDetailList.add(pollDetail);
     }
     insert pollDetailList;
     
     discussionDetail = OCSUGC_TestUtility.createDiscussion(cGroup.id, lstfeedItem[0].id, false);
    // discussionDetail.IsDeleted = false;
     discussionDetail.OCSUGC_Is_Deleted__c = false;
     discussionDetail.OCSUGC_GroupId__c = 'Everyone';
     insert discussionDetail;
     discussionDetail1 = OCSUGC_TestUtility.createDiscussion(cGroup.id, lstfeedItem[1].id, false);
    // discussionDetail.IsDeleted = false;
     discussionDetail1.OCSUGC_Is_Deleted__c = false;
     discussionDetail1.OCSUGC_GroupId__c = 'Everyone';
     insert discussionDetail1;
     
   }
    public static testmethod void test_OCSDC_HomeController() {
      Test.startTest();         
      System.runAs(managerUsr) {  
       OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Applications',
                                                         null,'Team','Test');
       insert testContent;
       OCSDC_HomeController dcHomeController = new OCSDC_HomeController();
       System.assert(dcHomeController.applications.size() > 0);
      }
      Test.stopTest();  
    }
    
    /**
     *	CreatedDate : 7 Dec 2016, Wednesday
     */
    public static testMethod void WrapperTileTest() {
    	Test.StartTest();
    		OCSDC_HomeController.WrapperTile tile = new OCSDC_HomeController.WrapperTile();
    		tile.tileId='tileId';
			tile.tileLink='tilelink';
			tile.tileCSS='tilecss';
			tile.tileType='tiletype';
			tile.postedDate='today';
			tile.postedBy='test user';
			tile.postedById=userinfo.getUserId();
			tile.countLikes=1;
			tile.countViewed=2;
			tile.countComments=3;
			tile.tileTitle='test_title';
			tile.tileSummary='test_summary';
			tile.tileTags='test_tags';
			tile.tileGroup='testGrps';
			tile.isVarianEndorsed=true;
			tile.isBookMarked=true;
			tile.imageId='';
			tile.createdDate=system.now();
			tile.isFeedItemImage=true;
    		    		
    		
    		OCSDC_HomeController cont = new OCSDC_HomeController();
    		cont.validateDCAccess();
    	Test.StopTest();
    }
    
    public static testMethod void ApplicationWrapperTest() {
    	Test.StartTest();
    		OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Applications',
                                                         null,'Team','Test');
            insert testContent;
            
            String myString = 'StringToBlob';
    		Blob myBlob = Blob.valueof(myString);
            Attachment attach = new Attachment();
            attach.contentType = 'image/jpg';
            attach.name = 'IMG';
            attach.body = myBlob;
            attach.parentId = testContent.Id;
            insert attach;
            
            OCSDC_HomeController.ApplicationWrapper cont = new OCSDC_HomeController.ApplicationWrapper(testContent, attach.Id);
            
            OCSDC_HomeController control = new OCSDC_HomeController();
            control.fetchAttachmentApplication(new Set<String>{testContent.Id});
            
       Test.StopTest();
    }
    
    public static testMethod void fetchDiscussionDetail_test() {
    	Test.StartTest();
    		ApexPages.currentPage().getParameters().put('dc', 'true');
    		FeedItem feeds = OCSUGC_TestUtility.createFeedItem(lstUserInsert[0].id, false);
    		feeds.NetworkScope = OCSUGC_Utilities.getNetworkId('Developer Cloud');
    		insert feeds;
    		
    		system.debug(' === ' + [Select Id, NetworkScope
                                 From FeedItem
                                 Where Id =: feeds.Id]); 
    		OCSUGC_DiscussionDetail__c discussions = OCSUGC_TestUtility.createDiscussion(cGroup.id, feeds.id, false);
    		discussions.OCSUGC_Is_Deleted__c = false;
			discussions.OCSUGC_GroupId__c = 'Everyone';
		    insert discussions;
		    
		    OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Events',
                                                         null,'Team','Test');
    		testContent.OCSUGC_GroupId__c ='Everyone';
    		testContent.OCSUGC_Is_Deleted__c = false;
    		testContent.OCSUGC_NetworkId__c = OCSUGC_Utilities.getNetworkId('Developer Cloud');
    		insert testContent;
    		
    		OCSDC_HomeController control = new OCSDC_HomeController();
    		//control.discussionDetailList.add(discussions);
            //control.fetchDiscussionDetail();
    	Test.StopTest();
    }
    
}
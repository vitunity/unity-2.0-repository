public without sharing class OCSUGC_AboutUserController extends OCSUGC_Base_NG {
	
	//Member Variables
	public Boolean isSummaryDataAvailable {get;set;}
	public Integer profiledUserGroups {get;set;}
	public Integer profiledUserComments {get;set;}
	public Integer profiledUserPostCount {get;set;}
	public Integer profiledUserfollowing {get;set;}
	public Integer profiledUserfollowers {get;set;}
	
  public User profiledUser {get; set;} //User being profiled. 
                                       //Note that this will be set via component attribute.
                                       //That means that it will be null in the Constructor                                       //
  
  @TestVisible private static List<String> CLOUDCOMMUNITY  = new List<String>{Label.OCSUGC_Everyone_Within_OncoPeer, Label.OCSDC_Everyone_Within_Developer_Cloud};
	@TestVisible private Id userId;
	
	/**
	 *	Constructor
	 */
	public OCSUGC_AboutUserController() {

		if(isSummaryDataAvailable != true) { //constructor may run twice, hence avoiding reseting this values
			isSummaryDataAvailable = false;			
			profiledUserGroups = profiledUserComments = profiledUserPostCount = profiledUserfollowing = profiledUserfollowers = 0;
		}
		
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com		
	 *	LazyLoad concept, called from action function to avoid the twice run of Logic as Constructor and getters setters execute if action function 
	 *	is present on component. If there is not action function on Component then Constructor will execute once but getter setter runs twice.
	 */
	public void loadSummaryData() {
		// fetch the userId
		userId = fetchProfiledUserId( ApexPages.currentPage().getParameters().get('userId') );
	
		profiledUserGroups(userId);
		profiledUserComments(userId);
		profiledUserFollowings(userId);
		profiledUserFollowers(userId);
		profiledUserPosts(userId);

		isSummaryDataAvailable = true;
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		userId		userId of profiledUser if null or login user
	 *	@return		Id			Id of profiledUser
	 *	method will return userId, if url parameter if null return login userId else visited User's userId
	 */
	@TestVisible private Id fetchProfiledUserId(String userId) {
		if(userId == null)
			return UserInfo.getUserId();
		else
			return userId;
	}
	
	/**
	 *  Counts number of posts made by the profiled user.
	 *  Note that this only counts posts shared to Everyone in OncoPeer and Developer Cloud,
	 *  and it does not include posts made in public and private groups.
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		profiledUserId		Id of the profiledUser used to fetch the number of posts made
	 *	@param		Integer				number of posts posted by profiledUser in OncoPeer and Developer Cloud
	 * 	method will calculate the number of post made by profiledUser
	 */
	@TestVisible private Integer profiledUserPosts(Id profiledUserId) {
		// Content [ Events ],	Event created/posted by profiledUser 
		String EventRecType = 'Events';
		String query;
		query = ' SELECT COUNT() FROM OCSUGC_Intranet_Content__c WHERE OwnerId =: profiledUserId AND RecordType.DeveloperName =: EventRecType AND '; 
		query += ' OCSUGC_Group__c IN: CLOUDCOMMUNITY';
		profiledUserPostCount = Database.CountQuery(query);
		
		
		// DiscussionDetail [ OCSUGC_DiscussionDetail__c ], Discussion posted by profiledUser 
		query = ' SELECT COUNT() FROM OCSUGC_DiscussionDetail__c WHERE OwnerId =: profiledUserId AND OCSUGC_Is_Deleted__c = false AND ';
		query += ' OCSUGC_Group__c IN: CLOUDCOMMUNITY';
		profiledUserPostCount += Database.CountQuery(query);
		
		
		// PollDetail [ OCSUGC_Poll_Details__c ], Polls inserted/posted by profiledUser 
		query = ' SELECT COUNT() FROM OCSUGC_Poll_Details__c WHERE OwnerId =: profiledUserId AND OCSUGC_Is_Deleted__c = false AND ';
		query += ' OCSUGC_Group_Name__c IN: CLOUDCOMMUNITY';
		profiledUserPostCount += Database.CountQuery(query);
		
		
		// Knowledge Files [ OCSUGC_Knowledge_Exchange__c ], Knowledge Files Posted by profiledUser 
		query = ' SELECT COUNT() FROM OCSUGC_Knowledge_Exchange__c WHERE OwnerId =: profiledUserId AND OCSUGC_Is_Deleted__c = false AND ';
		query += ' OCSUGC_Group__c IN: CLOUDCOMMUNITY';
		profiledUserPostCount += Database.CountQuery(query);
		
		return profiledUserPostCount;
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		profiledUserId		Id of the profiledUser used to fetch the number of comments made by user
	 *	@param		Integer				number of comments made
	 * 	method returns the number of Comments made by profiledUser, check made using describe method to ensure that 
	 *	the Comments besides Content objects do not add up
	 */
	@TestVisible private Integer profiledUserComments(Id profiledUserId) {
		// Store object Name
		String sObjName;
		Map<String, String> contentObjectNameList = new Map<String, String>{'OCSUGC_Knowledge_Exchange__c' => 'OCSUGC_Knowledge_Exchange__c',
																			'OCSUGC_Poll_Details__c' => 'OCSUGC_Poll_Details__c',
																			'OCSUGC_DiscussionDetail__c' => 'OCSUGC_DiscussionDetail__c',
																			'OCSUGC_Intranet_Content__c' => 'OCSUGC_Intranet_Content__c'};
		
		// Query fetch the comment posted by profiledUser and store the Id of feedItem
		profiledUserComments = 0;
		for(FeedComment feedId : [SELECT Id, CommentBody, CommentType, FeedItemId,InsertedById, ParentId FROM FeedComment WHERE InsertedById =: profiledUserId]){
			//	describe method returns Object Name which will be used to check if Comments are from Contents(DiscussionDetail, Event, PollDetail, KnowledgeFiles) objects only
			//	below describe method do not count in SOQL queries
			sObjName = (feedId.ParentId).getSObjectType().getDescribe().getName();
			if( (feedId.ParentId != profiledUserId) && (contentObjectNameList.containsKey(sObjName))) {	
				profiledUserComments += 1;
			}
		}
		
		return profiledUserComments;
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		profiledUserId		Id of the profiledUser used to fetch the number of group user is a member
	 *	@param		Integer				number of groups
	 *	method return the number of groups of which profiledUser is a member irrespective of Community, Online Advisory Board are not Included
	 */
	@TestVisible private Integer profiledUserGroups(Id memberId) {
		
		List<Id> collaborationGroupIds = new List<Id>();
		for(OCSUGC_CollaborationGroupInfo__c gp : [ SELECT OCSUGC_Group_Id__c, 
														  OCSUGC_Group_Type__c, 
														  OCSUGC_IsArchived__c
													FROM OCSUGC_CollaborationGroupInfo__c
													WHERE 	OCSUGC_IsArchived__c =: false
													AND OCSUGC_Group_Type__c !=: Label.OCSUGC_FocusGroupAdvisoryBoard]) {
			collaborationGroupIds.add(gp.OCSUGC_Group_Id__c);
		}
		
		profiledUserGroups = Database.CountQuery(' SELECT COUNT() FROM CollaborationGroupMember WHERE MemberId =: memberId AND CollaborationGroupId IN: collaborationGroupIds');
		return profiledUserGroups;
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		profiledUserId		Id of the profiledUser used to fetch the number of follwers
	 *	@param		Integer				number of followers
	 *	method returns the number of users following profiledUser irrespective of Community
	 */
	@TestVisible private Integer profiledUserFollowers(Id profiledUserId) {
		//Note that we only ever allow following a user in the context of OncoPeer Community, hence it is perfectly acceptable to
		//fix the networkId to OncoPeer networkId.
		String query = 'SELECT COUNT() FROM EntitySubscription Where ParentId =: profiledUserId AND NetworkId =: ocsugcNetworkId';
		profiledUserfollowers = Database.CountQuery(query);
		return profiledUserfollowers;
	}
	
	/**
	 *	@author		Puneet Mishra		puneet.mishra@varian.com
	 *	@param		profiledUserId		Id of the profiledUser used to fetch the number following users
	 *	@param		Integer				number of following users
	 * 	method returns the number of users profiledUser is following irrespective of Community
	 */
	@TestVisible private Integer profiledUserFollowings(Id profiledUserId) {
		//Note that we only ever allow following a user in the context of OncoPeer Community, hence it is perfectly acceptable to
		//fix the networkId to OncoPeer networkId.
		
		//Also not that below SOQL query is not putting any filter on ParentId, that means it will also count any records user is following.
		//Not exactly desirable behaviour.
		//TODO: fix so that only the User being followed by the profiledUserId are counted and not Records.
		String query = 'SELECT COUNT() FROM EntitySubscription Where SubscriberId =: profiledUserId AND NetworkId =: ocsugcNetworkId';
		profiledUserfollowing = Database.CountQuery(query);
		return profiledUserfollowing;
	}
}
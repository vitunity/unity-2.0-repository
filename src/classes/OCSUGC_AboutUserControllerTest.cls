//
// (c) 2014 Appirio, Inc.
//
// Test Class for OCSUGC_AboutUserControllerTest class
//
// 12th January, 2015 Ashish Sharma
@isTest
private class OCSUGC_AboutUserControllerTest {

   static Profile admin,portal,manager;   
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static Network ocsugcNetwork;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile(); 
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
     lstUserInsert = new List<User>();   
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '2'));
     insert lstUserInsert;     
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;
     }      
   }
   
   public static testmethod void test_OCSUGC_AboutUserController() {
      Test.startTest();         
      System.runAs(memberUsr) {  
        ApexPages.currentPage().getParameters().put('userId', memberUsr.Id);            
        OCSUGC_AboutUserController ctrl = new OCSUGC_AboutUserController();
        //system.assertNotEquals(ctrl.profiledUser,null);
        ctrl.loadSummaryData();
        System.assertequals(ctrl.profiledUser,null);
        
      }
      Test.stopTest();  
    }
}
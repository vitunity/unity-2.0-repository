/*
Name        : OCSUGC_ApplicationController
Updated By  : Rishi Khirbat (Appirio India)
Date        : 15th May, 2014
Purpose     : An Apex controller to get Information for OCSUGC_Application.page.

Refrence    : T-279101: Application Access config and VF page component
            :           Develop a VF component on the Home page to display applications that a CAKE user has access to.
            : I-116447: only show Apps that have the "Available for user assignment" checkbox checked in the Intranet Content Application record type.
*/
public with sharing class OCSUGC_ApplicationController {
    
    static final String RT_Application = 'Application';

    Id contactId;
    Set<Id> grantAccessApplicationIds = new Set<Id>();

    public class wrapperApplication {
        public Boolean grantAccess {get;set;}
        public OCSUGC_Intranet_Content__c application {get;set;}

        public wrapperApplication(Boolean gAccess, OCSUGC_Intranet_Content__c app) {
            grantAccess = gAccess;
            application = app;
        }
    }

    public List<wrapperApplication> getCAKEApplications() {
        List<wrapperApplication> applications = new List<wrapperApplication>();
        for(OCSUGC_Intranet_Content__c application :[Select Id, Name, OCSUGC_URL__c
                                                      From OCSUGC_Intranet_Content__c
                                                      Where RecordType.Name =:RT_Application
                                                      And OCSUGC_Application_Grant_Access__c = true]) {

            applications.add(new wrapperApplication(grantAccessApplicationIds.contains(application.Id),application));
        }
        return applications;
    }

    public OCSUGC_ApplicationController() {
        for(User currUser :[Select ContactId From User Where Id =:UserInfo.getUserId()]) {
            contactId = currUser.ContactId;
        }
        for(OCSUGC_Application_Permissions__c appPermissions :[Select OCSUGC_Application__c
                                                                From OCSUGC_Application_Permissions__c
                                                                Where OCSUGC_Application__r.RecordType.Name =:RT_Application
                                                                And OCSUGC_Contact__c =:contactId
                                                                And OCSUGC_Approval_Status__c = 'Approved']) {

            grantAccessApplicationIds.add(appPermissions.OCSUGC_Application__c);
        }
    }
}
/*
Name        : CAKE_ApplicationController
Updated By  : Sidhant (Appirio India) Original
Date        : 30th July, 2014
Purpose     : Test Class for CAKE_ApplicationController.cls

*/ 
@isTest
private class OCSUGC_ApplicationControllerTest {

    static testMethod void testController() {
        Profile admin,portal;
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile();

        User sysAdmin = OCSUGC_TestUtility.createStandardUser(true, admin.Id, 'test', '1');

        system.runAs(sysAdmin){
            OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Applications',
                                                         null,'Team','Test');
       testContent.OCSUGC_Application_Grant_Access__c = true;
       insert testContent;

       OCSUGC_Application_Permissions__c permission = new OCSUGC_Application_Permissions__c ();
       permission.OCSUGC_Application__c = testContent.Id;
       permission.OCSUGC_Approval_Status__c = 'Approved';
       insert permission ;

        //Creating account object
        Account objAccount = OCSUGC_TestUtility.createAccount('test account', true);
        Contact objContact = OCSUGC_TestUtility.createContact(objAccount.Id, true);

        User usr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, objContact.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member);

       Test.startTest();

       //Constructor Testing
       OCSUGC_ApplicationController controller = new OCSUGC_ApplicationController();


       //wrapper class
       OCSUGC_ApplicationController.wrapperApplication wrapper = new OCSUGC_ApplicationController.wrapperApplication(true, testContent);


       //getCAKEApplications test
       controller.getCAKEApplications();
       System.assertNotEquals(null, [Select OCSUGC_Application__c
                                                        From OCSUGC_Application_Permissions__c
                                                        Where OCSUGC_Application__r.RecordType.Name ='Application'
                                                        And OCSUGC_Contact__c = :usr.Id
                                                        And OCSUGC_Approval_Status__c = 'Approved']);

       Test.stopTest();
        }
    }
}
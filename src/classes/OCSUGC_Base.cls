//
// (c) 2014 Appirio, Inc.
//
// This is a base class for OCSUGC community
//
// 26 Nov, 2014    Puneet Sardana  Original
// 07-08-2015	   Pratz Joshi(Appirio)    Updated for ONCO-347

public abstract class OCSUGC_Base {

    public Boolean isFGAB { get; set; }
    // Base properties based on logged in user
    public Boolean isCreateMenuVisible { get; set; }
    public Boolean isManager { get; set; }
    public Boolean isContributor { get; set; }
    public Boolean isModerator { get; set; }
    public Boolean isReadOnlyUser { get; set; }
    public Boolean isManagerOrContributor { get; set; }
    public Boolean isManagerOrContributorOrReadonly { get; set; }
    public Boolean isManagerOrContributorOrModerator { get; set; }
    public Boolean isManagerOrContributorOrReadonlyOrModerator{ get; set; }
    public Boolean isMember { get; set; }
    public String loggedInUser { get; set; }
    public String loginUserRole { get; set; }
    public Boolean isFGABMember { get; set; }
    public Boolean isFGABPoll {get;set;}
    public Boolean isFGABArchived {get;set;}
    public Boolean hasFGABPageAccess { get; set; }
    public String fgabGroupId { get; set; }
    public String fgabGroupName { get; set; }
    public boolean fgabVisibile { get;set; }
    public boolean hasAcceptedTerms { get;set; }
    public Boolean isDC { get; set; }
    public boolean isApproved {get;set;}
    public String contactStatus {get;set;}
    public String contactId {get;set;}
    public String profileName {get;set;}
    public String networkId { get; set; }
    public Boolean isInValidDCUser {get; set;}
    public Boolean isInvitedDCUser {get; set;}
    public Boolean isExpiredDCUser {get; set;}
    public String networkName {get;set;}
    public Boolean isContributor_OncoPeer{get; set;}
    public Boolean isManagerOrModerator{get; set;}
//    public String baseInvitedUserId {get; set;}
    public OCSUGC_Base() {
        try {
            isFGAB = isDC = isFGABArchived = hasFGABPageAccess = isModerator = isManagerOrModerator =isInValidDCUser = isInvitedDCUser = isExpiredDCUser = isFGABMember = isManager = isContributor = isReadOnlyUser = isManagerOrContributor = isManagerOrContributorOrReadonly = isMember = false;
            isManagerOrContributorOrReadonlyOrModerator = false;
            String fgab = ApexPages.currentPage().getParameters().get('fgab');
            String groupId = ApexPages.currentPage().getParameters().get('g');
            isDC = OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc'));
            if(!isDC){
                isDC = Site.getPathPrefix().equalsIgnoreCase('/'+Label.OCSDC_DC_Community_Prefix);
            }
            system.debug('---------1'+ApexPages.currentPage().getUrl());
            isFGABPoll = ApexPages.currentPage().getUrl().containsIgnoreCase('poll');
            system.debug('---------2'+isFGABPoll);
            system.debug('---------2'+isDC);
            networkId = OCSUGC_Utilities.getCurrentCommunityNetworkId(isDC);
            system.debug('---------3'+networkId);
            networkName = OCSUGC_Utilities.getCurrentCommunityNetworkName(isDC);
            system.debug('*****networkName**!!'+networkName);          
            if(groupId == 'null') groupId = null;
            loggedInUser = UserInfo.getUserId();
            loginUserRole = OCSUGC_Utilities.fatchLoginUserRole();
            system.debug(' == * == ' + fgabGroupId);
            if(fgab != null && fgab.equals('true') && groupId != null && !groupId.containsIgnorecase('everyone')) {
                List<CollaborationGroup> lstGroup = [SELECT Id,Name,IsArchived,CollaborationType, (SELECT Id FROM GroupMembers WHERE MemberId = :loggedInUser )
                                                     FROM CollaborationGroup
                                                     WHERE Id = :groupId];
                isFGAB = lstGroup.size() > 0 &&  lstGroup[0].CollaborationType.equals(OCSUGC_Constants.UNLISTED);
                fgabGroupName = isFGAB ? lstGroup[0].Name : null ;
                isFGABMember = isFGAB && lstGroup[0].GroupMembers.size() > 0 ;
                fgabGroupId = isFGAB ? lstGroup[0].Id : null;
//                if(ApexPages.currentPage().getParameters().get('inviteUser') != null){
//                  baseInvitedUserId = ApexPages.currentPage().getParameters().get('inviteUser');
//              }
                isFGABArchived = isFGAB && lstGroup[0].IsArchived;
            }
            if(loginUserRole != null) {
                isManager = OCSUGC_Utilities.isManager(loginUserRole);
                isContributor = OCSUGC_Utilities.isContributor(loginUserRole);
                isReadOnlyUser = OCSUGC_Utilities.isReadonly(loginUserRole);
                isModerator = OCSUGC_Utilities.isModerator(loginUserRole);
                isManagerOrContributor = OCSUGC_Utilities.isManagerOrContributor(loginUserRole);
                isManagerOrModerator = OCSUGC_Utilities.isManagerOrModerator(loginUserRole);
                isManagerOrContributorOrReadonly = OCSUGC_Utilities.isManagerOrContributorOrReadonly(loginUserRole);
                isManagerOrContributorOrModerator = OCSUGC_Utilities.isManagerOrContributorOrModerator(loginUserRole);
                isManagerOrContributorOrReadonlyOrModerator = OCSUGC_Utilities.isManagerOrContributorOrReadonlyOrModerator(loginUserRole);
                isMember = OCSUGC_Utilities.isMember(loginUserRole);
                isContributor_OncoPeer = OCSUGC_Utilities.isContributor_OncoPeer(loginUserRole,isDC,isFGAB);
                OCSUGC_FGABPagesVisibility__c fgabVisibility = OCSUGC_FGABPagesVisibility__c.getValues(loginUserRole);
                fgabVisibile = fgabVisibility != null && fgabVisibility.OCSUGC_Visibility__c;               
                hasFGABPageAccess = !isFGAB || (isFGAB  && (fgabVisibile || isFGABMember));
                
                // 07-08-2015	Updated by Pratz Joshi (Appirio) for ONCO-347                
                OCSUGC_CreateMenu__c CreateMenuVisibilityByRole = OCSUGC_CreateMenu__c.getValues(loginUserRole);
                isCreateMenuVisible = CreateMenuVisibilityByRole != null && CreateMenuVisibilityByRole.OCSUGC_IsCreateMenuVisible__c;      
            }
            system.debug('****isModerator**********'+isModerator);
            for(User ur : [SELECT Id, ContactId, Contact.OCSUGC_UserStatus__c, 
                            OCSUGC_User_Role__c,OCSUGC_Accepted_Terms_of_Use__c,
                            Profile.Name, Contact.OCSDC_UserStatus__c, Contact.OCSDC_InvitationDate__c
                           FROM User
                           WHERE Id = :loggedInUser
                           LIMIT 1]){
                profileName = ur.Profile.Name;
                // Changed  15-Jan-2105  Puneet Sardana  Changed conditions to redirect Ref I-145762
                //if(isManagerOrContributorOrReadonly || profileName.containsIgnoreCase(OCSUGC_Constants.SYSTEM_ADMIN_PROFILE)) {
                if(isManagerOrContributorOrReadonly || isModerator || profileName.containsIgnoreCase(OCSUGC_Constants.SYSTEM_ADMIN_PROFILE)) { //changed by Shital Bhujbal for task no. 450
                    isApproved = true;
                    hasAcceptedTerms = true;
                } else if(ur.ContactId != null) {
                    contactStatus = ur.Contact.OCSUGC_UserStatus__c;
                    contactId = ur.Contact.Id;
                    isApproved = contactStatus.equals(Label.OCSUGC_Approved_Status);
                    hasAcceptedTerms = ur.OCSUGC_Accepted_Terms_of_Use__c;
                }
                if(isDC){
                    Integer expirationDays = Integer.valueOf(Label.OCSDC_ExpirationDays);
                    isInValidDCUser        = !isApproved || String.isBlank(ur.Contact.OCSDC_UserStatus__c) || 
                                                !ur.Contact.OCSDC_UserStatus__c.equals('Member');
                    isInvitedDCUser        = isApproved && !String.isBlank(ur.Contact.OCSDC_UserStatus__c) &&
                                                ur.Contact.OCSDC_UserStatus__c.equals(Label.OCSDC_Invited);
                    isExpiredDCUser        = isInvitedDCUser && ur.Contact.OCSDC_InvitationDate__c!=null &&
                                                ur.Contact.OCSDC_InvitationDate__c.Date().daysBetween(Date.today()) > expirationDays;  
                }
            }
            system.debug('============================loginUserRole: '+loginUserRole);
            system.debug('============================isFGAB: '+isFGAB);
        } catch(System.Exception ex) {
            system.debug('================Error: '+ex.getMessage());
        }
    }
    
}
/**
 * OCSUGC_Base class
 * This class will act as the base class for other controllers in OncoPeer.
 * This class records the loggedin user details along with few other frequently needed information
 * 
 */
public abstract class OCSUGC_Base_NG {
    //Member Variables
    public Boolean isLoggedIn {get;set;}            //flag to check if user is logged-in (authenticated) or not
    public User    loggedInUser {get;set;}          //logged in user
    public String  ocsugcNetworkId {get;set;}       //OncoPeer network id
    public String  ocsdcNetworkId {get;set;}        //Developer Cloud network id
    public String  currentNetworkId {get;set;}      //Current effective network id
    public String  currentNetworkName {get; set;}   //Current network path name e.g OCSUGC or DC
    public String  pageTitle {get;set;}             //html <title> of visualforce page
    public Boolean canMessage {get; set;}           //flag to check if logged in user can Message others or not
    public Boolean canFollow {get; set;}            //flag to check if logged in user can Follow others or not
    public Boolean canJoinGroups {get; set;}        //flag to check if logged in user can join groups or not
    public Boolean canCreateContent {get; set;}     //flag to check if logged in user can create content or not
    public Boolean canSeeAllPosts {get; set;}       //flag to check if logged in user can see all posts in OCSUGC & DC
    public Boolean isApproved {get;set;}            //flag to check if logged in user is approved member of OncoPeer or not
    public Boolean hasAcceptedMainTerms {get;set;}  //flag to check if logged in user has acceepted OncoPeer terms or not
    public Boolean isDevCloud {get;set;}            //flag to check if user logged in to Developer Cloud    
    public Boolean isDC {get;set;}                  //For backward compatibility - same as isDevCloud - flag to check if user logged in to Developer Cloud
    public Boolean isDCMember {get;set;}            //flag to check if logged in user is Member of Developer Cloud or not
    public Boolean isDCContributor {get;set;}       //flag to check if logged in user is Contributor with access to Developer Cloud
    public Boolean isDCInvited {get;set;}           //flag to check if looged in user is invited to Developer Cloud or not
    public Boolean hasDCAccess {get;set;}           //flag to check if looged in user has access to Developer Cloud or not
    public Boolean isOAB {get;set;}                 //flag to check if user logged in to Online Advisory Board
    public Boolean isFGAB {get;set;}                //For backward compatibility - same as isOAB - flag to check if user logged in to Online Advisory Board
    public Boolean isCommunityMember {get;set;}         //flag to check if logged in user is Community Member or not
    public Boolean isCommunityManager {get;set;}        //flag to check if logged in user is Community Manager or not
    public Boolean isCommunityContributor {get;set;}    //flag to check if logged in user is Community Contributor or not
    public Boolean isCommunityModerator {get;set;}      //flag to check if logged in user is Community Moderator or not
    public Boolean isReadOnlyUser {get;set;}            //flag to check if logged in user is Read Only User or not    
    public Boolean isOABMember {get;set;}               //flag to check if logged in user is Member of current Online Advisory Board or not
    public String oabGroupId {get;set;}                 //ID of the current Online Advisory Board
    public String oabGroupName {get;set;}               //Name of the current Online Advisory Board
    public String fgabGroupId {get;set;}                //For backward compatibility - same as oabGroupId 
    public Boolean hasFGABPageAccess {get;set;}           //For backward compatibility - same as isOABMember
    public Boolean isFGABMember {get;set;}           //For backward compatibility - same as isOABMember

    /** 
     * Constructor
     */
    public OCSUGC_Base_NG() {

        //check if user is authenticated or is using Guest profile
        isLoggedIn = (UserInfo.getUserType() != Label.guestuserlicense);

        //populate loggedInUser and his Profile photo URL
        loggedInUser = [Select Id, Name, SmallPhotoUrl,FullPhotoUrl, OCSUGC_User_Role__c, OCSUGC_Accepted_Terms_of_Use__c, ContactId, Contact.OCSUGC_UserStatus__c, Contact.OCSDC_UserStatus__c
                        From User Where Id =:UserInfo.getUserId()];

        //check if logged in user is community member or not
        isCommunityMember = false; //default is false
        if (loggedInUser != NULL) {
            isCommunityMember = OCSUGC_Utilities.isMember(loggedInUser.OCSUGC_User_Role__c);
        }
        
        //check if logged in user is community manager or not
        isCommunityManager = false; //default is false
        if (loggedInUser != NULL) {
            isCommunityManager = OCSUGC_Utilities.isManager(loggedInUser.OCSUGC_User_Role__c);
        }

        //check if logged in user is community contributor or not
        isCommunityContributor = false; //default is false
        if (loggedInUser != NULL) {
            isCommunityContributor = OCSUGC_Utilities.isContributor(loggedInUser.OCSUGC_User_Role__c);
        }

        //check if logged in user is community contributor or not
        isCommunityModerator = false; //default is false
        if (loggedInUser != NULL) {
            isCommunityModerator = OCSUGC_Utilities.isModerator(loggedInUser.OCSUGC_User_Role__c);
        }

        //check if logged in user is read only user or not
        isReadOnlyUser = false; //default is false
        if (loggedInUser != NULL) {
            isReadOnlyUser = OCSUGC_Utilities.isReadonly(loggedInUser.OCSUGC_User_Role__c);
        }

        isApproved = false;
        hasAcceptedMainTerms = false;
        if(isCommunityManager || isCommunityModerator || isCommunityContributor || isReadOnlyUser) {
            isApproved = true;
            hasAcceptedMainTerms = true;  
        }
        else if (loggedInUser.ContactId != null && loggedInUser.Contact.OCSUGC_UserStatus__c != null) {
            isApproved = loggedInUser.Contact.OCSUGC_UserStatus__c.equals(Label.OCSUGC_Approved_Status);
            hasAcceptedMainTerms = loggedInUser.OCSUGC_Accepted_Terms_of_Use__c;
        }

    
        //NetworkId for OncoPeer and Developer Cloud
        ocsugcNetworkId = OCSUGC_Utilities.getNetworkId('OncoPeer');
        ocsdcNetworkId  = OCSUGC_Utilities.getNetworkId('Developer Cloud');


        // flag value for DeveloperCloud
        isDevCloud = OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc'));
        isDC = isDevCloud;
        isDCMember = isApproved && OCSUGC_Utilities.isDCMember(loggedInUser.Id, loggedInUser.OCSUGC_User_Role__c, loggedInUser.Contact.OCSDC_UserStatus__c);
        isDCContributor = isApproved && OCSUGC_Utilities.isDCContributor(loggedInUser.Id, loggedInUser.OCSUGC_User_Role__c);
        isDCInvited  = isApproved && !String.isBlank(loggedInUser.Contact.OCSDC_UserStatus__c) &&
                                                loggedInUser.Contact.OCSDC_UserStatus__c.equals(Label.OCSDC_Invited);
        
        hasDCAccess = false;
        if(isDCMember || isDCContributor || isReadOnlyUser || isCommunityManager || isCommunityModerator) {
            hasDCAccess = true;
        }


        //Can logged in user message others
        canMessage = true;
        if(isReadOnlyUser) {
            canMessage = false;
        }

        //Can logged in user follow others
        canFollow = false; //default false
        if(!isCommunityManager && !isCommunityModerator && !isCommunityContributor && !isReadOnlyUser) {
            canFollow = true; //only community members can follow
        }

        //Can logged in user join Group
        canJoinGroups = true; //default true
        if((isCommunityContributor && !isDevCloud) || isReadOnlyUser) { //read-only users cannot join groups, contributors can join groups only in Developer Cloud
            canJoinGroups = false; 
        }
        
        OCSUGC_CreateMenu__c createMenuVisibilityByRole = OCSUGC_CreateMenu__c.getValues(loggedInUser.OCSUGC_User_Role__c);
        canCreateContent = createMenuVisibilityByRole != null && createMenuVisibilityByRole.OCSUGC_IsCreateMenuVisible__c;


        // flag value for Online Advisory Board
        isOAB = false; //default value
        String fgab = ApexPages.currentPage().getParameters().get('fgab');
        String groupId = ApexPages.currentPage().getParameters().get('g');
        if(fgab != null && fgab.equals('true') && groupId != null && !groupId.containsIgnorecase('everyone')) {
            List<CollaborationGroup> lstGroup = [SELECT Id,Name,IsArchived,CollaborationType, (SELECT Id FROM GroupMembers WHERE MemberId = :loggedInUser.Id )
                                                 FROM CollaborationGroup
                                                 WHERE Id = :groupId
                                                 AND CollaborationType = :OCSUGC_Constants.UNLISTED];
            isOAB = lstGroup.size() > 0;
            oabGroupId   = isOAB ? lstGroup[0].Id : null;
            oabGroupName = isOAB ? lstGroup[0].Name : null ;
            if(isOAB) {
                isOABMember = lstGroup[0].GroupMembers.size() > 0 ;
            }
            else {
                isOABMember = false;
            }

        }
        isFGAB = isOAB;
        fgabGroupId = oabGroupId;
        hasFGABPageAccess = isOABMember;
        isFGABMember = isOABMember;

        canSeeAllPosts = false;
        if(isCommunityModerator || isCommunityManager) {
            canSeeAllPosts = true;                    
        }

        if(isDevCloud) {
            currentNetworkId = ocsdcNetworkId;
            currentNetworkName = 'DC';
        } else {
            currentNetworkId = ocsugcNetworkId;
            currentNetworkName = 'OCSUGC';
        }


        //default page title. 
        //All derived classes should overwrite this to provide more specific and meaningful title.
        pageTitle = 'OncoPeer';
        if(isDevCloud) pageTitle = 'Developer Cloud';
        if(isOAB) pageTitle = 'OAB | OncoPeer';
    }
    
}
/*
Name        : OCSUGC_ChatterManagement
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 9th July, 2014
Purpose     : Mangement class for triggers on FeedItem and FeedComment object
*/

public class OCSUGC_ChatterManagement {
	private static final Integer SF_BCC_LIMIT = 25;
    //this function is used for inserting intranet notification records when a comment is received on knowledge artifact
    // with reference to T-293716
    public static void insertIntranetNotificationRecordsForFeedComments(Map<Id, FeedComment> newMap){
        Set<String> feedItemIds = new Set<String>();
        String knowledgeExchangeKeyPrefix = OCSUGC_Knowledge_Exchange__c.sObjectType.getDescribe().getKeyPrefix();
        List<OCSUGC_Intranet_Notification__c> listToInsert = new List<OCSUGC_Intranet_Notification__c>();
        for(FeedComment tempFeedItem : newMap.values()){
            feedItemIds.add(tempFeedItem.FeedItemId);
        }

        Map<String, FeedItem> mapFeedItems = new Map<String, FeedITem>([Select k.ParentId, k.LikeCount, commentCount,RelatedRecordId, createdById,
                                            (Select Id, FeedItemId, FeedEntityId, CreatedById, CreatedDate, IsDeleted, InsertedById From FeedLikes order by createdDate Desc)
                                            From FeedItem k
                                            where id in : feedItemIds and type='ContentPost']);
        List<FeedComment> listFeedItems = new List<FeedComment>();
        Set<String> knowledgeExchangeIds = new Set<String>();
        Set<String> commentCreatorIds = new Set<String>();
        for(FeedComment tempFeedItem : newMap.values()){
            if(mapFeedItems.containsKey(tempFeedItem.FeedItemId)){
                FeedItem feedItem = mapFeedItems.get(tempFeedItem.FeedItemId);
                if(String.valueOf(feedItem.ParentId).startsWith(knowledgeExchangeKeyPrefix) ){
                    listFeedItems.add(tempFeedItem);
                    commentCreatorIds.add(tempFeedItem.createdById);
                    knowledgeExchangeIds.add(feedItem.parentId);
                }
            }
        }

        //fetch knowledgeExchnageData
        Map<String, OCSUGC_Knowledge_Exchange__c> mapKnowledgeExchange = new Map<String, OCSUGC_Knowledge_Exchange__c> ([Select OCSUGC_Title__c,createdById from OCSUGC_Knowledge_Exchange__c where id in : knowledgeExchangeIds]);
        Map<String, User> mapUsers = new Map<String, User>([Select Name from User where id in : commentCreatorIds]);

        for(FeedComment tempFeedItem : listFeedItems){
            FeedItem feedItem = mapFeedItems.get(tempFeedItem.FeedItemId);
            system.debug('Info --->'+mapKnowledgeExchange.containsKey(feedItem.ParentId) + '---'+mapUsers.containsKey(tempFeedItem.createdById)+'---'+mapKnowledgeExchange.get(feedItem.ParentId).createdById);
            if(mapKnowledgeExchange.containsKey(feedItem.ParentId) && mapUsers.containsKey(tempFeedItem.createdById) && mapKnowledgeExchange.get(feedItem.ParentId).createdById != UserInfo.getUserId()){
                OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
                tempObj.OCSUGC_Description__c = mapUsers.get(tempFeedItem.createdById).Name +' commented on ' + mapKnowledgeExchange.get(feedItem.ParentId).OCSUGC_Title__c;
                tempObj.OCSUGC_Is_Viewed__c = false;
                tempObj.OCSUGC_Link_to__c = 'OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId='+mapKnowledgeExchange.get(feedItem.ParentId).ID;
                tempObj.OCSUGC_Related_User__c = tempFeedItem.createdById;
                tempObj.OCSUGC_User__c =  feedItem.createdById;
                listToInsert.add(tempObj);
            }
        }

        if(listToInsert.size() > 0){
            insert listToInsert;
        }

    }


    public static void insertIntranetNotificationRecordsForChatterGroup(Map<Id, CollaborationGroupMember> newMap){
        List<OCSUGC_Intranet_Notification__c> listToInsert = new List<OCSUGC_Intranet_Notification__c>();
        Set<String> groupIds = new Set<String>();
        for(CollaborationGroupMember member : newMap.Values()){
            groupIds.add(member.CollaborationGroupId);
        }

        Map<String, CollaborationGroup> mapGroups = new Map<String, CollaborationGroup>([Select Id, owner.Name, Name, CollaborationType from CollaborationGroup
                                                        where id in : groupIds]);

        for(CollaborationGroupMember member : newMap.Values()){
            if(mapGroups.containsKey(member.CollaborationGroupId) && mapGroups.get(member.CollaborationGroupId).CollaborationType == 'Private' &&
                mapGroups.get(member.CollaborationGroupId).ownerId != member.memberId){
                OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
                tempObj.OCSUGC_Description__c = mapGroups.get(member.CollaborationGroupId).owner.Name +' added you to the ' + mapGroups.get(member.CollaborationGroupId).Name;
                tempObj.OCSUGC_Is_Viewed__c = false;
                tempObj.OCSUGC_Link_to__c = 'OCSUGC_GroupDetail?g='+member.CollaborationGroupId;
                tempObj.OCSUGC_Related_User__c = mapGroups.get(member.CollaborationGroupId).ownerId;
                tempObj.OCSUGC_User__c =  member.memberId;
                listToInsert.add(tempObj);
            }
        }

        if(listToInsert.size() > 0){
            insert listToInsert;
        }
    }

    public static void sendEmailNotificationToGroupMembers(Map<Id, CollaborationGroup> newMap, Map<Id, CollaborationGroup> oldMap) {

		//list will contain Email drafts which needs to be send
        List<String> lstEmails = new List<String>();

		//fatching group member's details
		for(CollaborationGroup newGroup :[Select Id,
											Name,
											Description,
											OwnerId,
											(Select MemberId,
													Member.FirstName,
													Member.Email,
													Member.Contact.OCSUGC_Notif_Pref_EditGroup__c
											 From GroupMembers)
									 From CollaborationGroup
									 Where Id IN :trigger.newMap.keySet()]) {
			Integer noOfReceipts=0;
			if(newGroup.Name != oldMap.get(newGroup.Id).Name || newGroup.Description != oldMap.get(newGroup.Id).Description) {
				//drafting email body

			  String strHtmlBody = '<center><table cellpadding="0" cellspacing="0" width="500" height="450">';
		      strHtmlBody += '<tbody><tr valign="top"><td style="vertical-align:top;height:100;text-align:left;background-color:#ffffff"><img src="';

		      strHtmlBody +=  Label.OCSUGC_AllEmail_Header_Image_Path;

		      strHtmlBody += '" border="0" height="180" width="700"/></td></tr><tr valign="top">';
		      strHtmlBody += '<td style="height:1;background-color:#aaaaff"></td></tr><tr valign="top">';
		      strHtmlBody += '<td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial" height="300">';
		      strHtmlBody += '<table border="0" cellpadding="5" cellspacing="5" width="550" height="400">';
		      strHtmlBody += '<tbody><tr valign="top" height="400"><td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial">';

			  strHtmlBody += 'Hi ';
		      strHtmlBody += '<br><br>';
			  strHtmlBody += 'The Group '+oldMap.get(newGroup.Id).Name+' that you are member of has been updated.';
			  if(newGroup.Name != oldMap.get(newGroup.Id).Name && newGroup.Description != oldMap.get(newGroup.Id).Description) {
			    strHtmlBody += 'The new Group name is '+newGroup.Name+' and new Group description is '+newGroup.Description;
			  } else {
				strHtmlBody += newGroup.Name != oldMap.get(newGroup.Id).Name ? ' The new Group name is '+newGroup.Name : ' The new Group description is '+newGroup.Description;
			  }
			  strHtmlBody += '<br><br>';
			  strHtmlBody += 'Thanks,<br>Sarah Gyatso<br>Cloud Communities Manager';
			  strHtmlBody += '</td></tr></tbody></table></td></tr><tr valign="top"><td style="height:0;background-color:#ffffff">';
              strHtmlBody += '</td></tr><tr valign="top"><td style="vertical-align:top;height:0;text-align:left;background-color:#ffffff">';
              strHtmlBody += '</td></tr><tr valign="top"><td style="height:0;background-color:#ffffff"></td></tr></tbody></table>';
              strHtmlBody += '</center><div class="yj6qo"></div><div class="adL"><br><br></div>';
			  Messaging.SingleEmailMessage email;
			  Id orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
			  for(CollaborationGroupMember member :newGroup.GroupMembers) {
				if(newGroup.OwnerId != member.MemberId && member.Member.Contact.OCSUGC_Notif_Pref_EditGroup__c) {
						//set email to the group member
			      email = new Messaging.SingleEmailMessage();
			      email.setSaveAsActivity(false);
			      email.setSubject(Label.OCSUGC_Group_has_been_updated);
			      email.setHtmlBody(strHtmlBody);
			      // Use Organization Wide Address
			      if(orgWideEmailAddressId != null)
				  	  email.setOrgWideEmailAddressId(orgWideEmailAddressId);
				  	else
				  	  email.setSenderDisplayName('OCSUGC');
			      noOfReceipts++;
			      lstEmails.add(member.Member.Email);
			      if(noOfReceipts == SF_BCC_LIMIT) {
			       	 email.setBccAddresses(lstEmails);
			       	 noOfReceipts = 0;
			       	 Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { email });
			       	 lstEmails = new List<String>();
			      }
				}
			 }
			 //Send rest emails
			if(lstEmails.size() >0) {
				  email.setBccAddresses(lstEmails);
			      Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { email });
			      lstEmails = new List<String>();
			}
		  }
		}
    }
  
 	public static void deleteGroup(Map<Id, CollaborationGroup> oldMap) {
		//fatching group's other details
		List<OCSUGC_CollaborationGroupInfo__c> lstCollaborationGroupInfo = [Select Id
                                                                 			From OCSUGC_CollaborationGroupInfo__c
                                                                 			Where OCSUGC_Group_Id__c IN :trigger.oldMap.keySet()];
		//fatching associated group's tags
	    List<OCSUGC_Group_Tag__c> lstGroupTag = [Select Id
	                                      		 From OCSUGC_Group_Tag__c
	                                      		 Where OCSUGC_Group__c IN :trigger.oldMap.keySet()];

	    if(!lstCollaborationGroupInfo.IsEmpty()) {
	        //deleting group's other details
	        delete lstCollaborationGroupInfo;
	    }

	    if(!lstGroupTag.IsEmpty()) {
	    	//deleting associated group's tags
	        delete lstGroupTag;
	    }

	    for(Id groupId :trigger.oldMap.keySet()) {
	        //deleting associated group's photo
	        ConnectApi.ChatterGroups.deletePhoto(null, groupId);
	    }
    }
    // @description: This method updates comment count when comment is deleted
    // @param: List of feed comments that are deleted
    // @return: NA
    // 11-Mar-2015  Puneet Sardana  Original Ref T-369544
    public static void updateNumberOfComments(List<FeedComment> feedComments) {  	
    	List<OCSUGC_DiscussionDetail__c> discussionsToUpdate = new List<OCSUGC_DiscussionDetail__c>();
    	List<OCSUGC_Poll_Details__c> pollsToUpdate = new List<OCSUGC_Poll_Details__c>();
    	List<OCSUGC_Intranet_Content__c> eventsToUpdate = new List<OCSUGC_Intranet_Content__c>();
    	List<OCSUGC_Knowledge_Exchange__c> knowledgeToUpdate = new List<OCSUGC_Knowledge_Exchange__c>();
    	Set<Id> feedItemParents = new Set<Id>();
    	//Save feed items ids of objects like discussion,poll,ka,events
    	Set<Id> objectIds = new Set<Id>();
    	for(FeedComment fc : feedComments) {
    		objectIds.add(fc.FeedItemId);
    	}
    	//Feed Item parent Id has id of event and discussion     	
    	for(FeedItem fi : [SELECT ParentId
    	                   FROM FeedItem 
    	                   WHERE Id IN :objectIds] ) {
    	  feedItemParents.add(fi.ParentId);                  	
    	}
    	Set<Id> idsAlreadyIncluded = new Set<Id>();
    	//Subtract comment count is any feed comment is deleted
    	for(OCSUGC_DiscussionDetail__c discussionDetails : [SELECT Id,OCSUGC_DiscussionId__c,OCSUGC_Comments__c
    	                                                    FROM OCSUGC_DiscussionDetail__c
    	                                                    WHERE OCSUGC_DiscussionId__c IN :objectIds]) {
    	    discussionDetails.OCSUGC_Comments__c -= 1;
    	    discussionsToUpdate.add(discussionDetails);     	                                  	
    	}
    	                                                                                                          
      for(OCSUGC_Poll_Details__c polls : [SELECT Id,OCSUGC_Comments__c,OCSUGC_Poll_Id__c
                                          FROM OCSUGC_Poll_Details__c
                                          WHERE OCSUGC_Poll_Id__c IN :objectIds]) {
          polls.OCSUGC_Comments__c -= 1;
          pollsToUpdate.add(polls);                               	
      }
      if(feedItemParents.size() > 0) {
	      for(OCSUGC_Intranet_Content__c events : [SELECT Id,OCSUGC_Comments__c
	                                               FROM OCSUGC_Intranet_Content__c
	                                               WHERE Id IN :feedItemParents]) {
	         events.OCSUGC_Comments__c -= 1;   
	         eventsToUpdate.add(events);                                     	
	      }
	      for(OCSUGC_Knowledge_Exchange__c knowledge : [SELECT Id,OCSUGC_Comments__c
	                                                    FROM OCSUGC_Knowledge_Exchange__c
	                                                    WHERE Id IN :feedItemParents]) {
	          knowledge.OCSUGC_Comments__c -= 1;
	          knowledgeToUpdate.add(knowledge);                                          	
	      }  
      }      
      try {
      	 if(discussionsToUpdate.size() > 0) {
      	 	  update discussionsToUpdate;
      	 }
      	 if(pollsToUpdate.size() > 0) {
      	 	  update pollsToUpdate;
      	 }
      	 if(eventsToUpdate.size() > 0) {
      	 	  update eventsToUpdate;
      	 }
      	 if(knowledgeToUpdate.size() > 0) {
      	 	  update knowledgeToUpdate;
      	 }
      }
      catch(Exception ex) {
      	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
      }                                                                                                                                                                                                                                     	                                                                                                          
    }
}
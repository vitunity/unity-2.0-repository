/*
Name        : OCSUGC_ChatterManagementTest
Created By  : Priyanka Kumar
Date        : 25 July, 2014
Purpose     : Test class  for ChatterManagement
*/


//ConnectApi methods are not supported in data siloed tests, so needed to use SeeAllData
@IsTest//(SeeAllData=true)
public class OCSUGC_ChatterManagementTest {
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static Network ocsugcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert; 
   static List<CollaborationGroup> lstGroupInsert;
   static OCSUGC_Knowledge_Exchange__c ka;
   static OCSUGC_DiscussionDetail__c discussion;
   static OCSUGC_Intranet_Content__c event;
   static FeedItem fItemEvent,fItemDiscussion,fItemKA;
   static FeedComment fCommentEvent,fCommentDiscussion,fCommentKA;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile(); 
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
     lstUserInsert = new List<User>();   
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '2'));
     insert lstUserInsert;     
     account = OCSUGC_TestUtility.createAccount('test account8627' + Integer.valueOf(math.random()), true);          

     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     lstGroupInsert = new List<CollaborationGroup>(); 
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public', ocsugcNetwork.Id,false)); 
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Public', ocsugcNetwork.Id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public', ocsugcNetwork.Id,false));
     insert lstGroupInsert;
     fItemDiscussion = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
     discussion = OCSUGC_TestUtility.createDiscussion(publicGroup.Id, fItemDiscussion.Id, true);
     event = OCSUGC_TestUtility.createIntranetContent('Events','Applications',null,'Team','Test');
     insert event;
     ka = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);
     fItemEvent = OCSUGC_TestUtility.createFeedItem(event.Id,true);
     fItemKA = OCSUGC_TestUtility.createFeedItem(event.Id,true);
      
     /*System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;               
     }*/
   }  
   @IsTest(SeeAllData=true)
    public static void test_insertIntranetNotificationRecordsForFeedComments() {       
        OCSUGC_Knowledge_Exchange__c exchange = new OCSUGC_Knowledge_Exchange__c(OCSUGC_Title__c='test');        
        system.runAs(adminUsr1){
        insert exchange;
        }
        feeditem post = new feeditem();
        post.parentid = exchange.id;
        post.type =  'ContentPost';
        post.body = 'this is A post';
        post.contentfilename = 'contentfile';
        post.contentdata = Blob.valueOf('dummy data');
        post.CreatedById = UserInfo.getUserId();
        insert post;
        List<FeedComment> lstFeedComment = new List<FeedComment>();
        feedcomment comment = new feedcomment();
        comment.feeditemid = post.id;
        comment.commentbody =  'This is a comment to your post';
        comment.CreatedById = UserInfo.getUserId();
        lstFeedComment.add(comment);
        feedcomment comment1 = new feedcomment();
        comment1.feeditemid = post.id;
        comment1.commentbody =  'This is another comment to your post';
        comment1.CreatedById = UserInfo.getUserId();
        lstFeedComment.add(comment1);
        insert lstFeedComment;
        feedlike flike = new feedlike();
        flike.FeedItemId = post.id;
        flike.CreatedById = UserInfo.getUserId();
        insert flike;
        Map<Id, FeedComment> newMap = new Map<Id,FeedComment>([
        SELECT Id, feeditemid,CommentBody, CreatedDate, CreatedById,
                   CreatedBy.FirstName, CreatedBy.LastName
             FROM FeedComment where feeditemid = :post.id]);         
        System.assert(newMap.size() > 0);
        CollaborationGroup myGroup = new CollaborationGroup();
        myGroup.Name='My Group';
        myGroup.CollaborationType='Private';
        insert myGroup;     
                Map<Id, CollaborationGroup> grpMap = new Map<Id, CollaborationGroup>();
                grpMap.put(myGroup.Id, myGroup);        
        CollaborationGroupMember groupMember = new CollaborationGroupMember();
        groupMember.memberid = adminUsr1.Id;
        groupMember.CollaborationGroupId = myGroup.Id; //Id of group created above
        insert groupMember;
        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('TestTag');
          insert tag;
                OCSUGC_Group_Tag__c groupTag = new OCSUGC_Group_Tag__c();
                groupTag.OCSUGC_Tags__c = tag.Id;
                grouptag.OCSUGC_Group__c = myGroup.Id;
                insert groupTag;
        Map<Id,CollaborationGroupMember> groupMap = new Map<Id,CollaborationGroupMember>([
        select id,CollaborationGroupId,memberid from CollaborationGroupMember  where CollaborationGroupId = :myGroup.Id
        ]);
        test.startTest();
            OCSUGC_ChatterManagement.insertIntranetNotificationRecordsForFeedComments(newMap);
            OCSUGC_ChatterManagement.insertIntranetNotificationRecordsForChatterGroup(groupMap);
            system.assertNotEquals(null,[select id from OCSUGC_Intranet_Notification__c]);
            delete myGroup;
            System.assertNotEquals(null, [SELECT ID FROM CollaborationGroup WHERE ID =: myGroup.Id]);
        test.stopTest();
    }
    
    @IsTest(SeeAllData=true)
    public static void test_insertIntranetNotificationRecordsForFeedComments2(){         
            test.startTest();
               publicGroup.Name = 'test name';
               publicGroup.Description = 'test description';
               update publicGroup;
               List<FeedComment> feedComments = new List<FeedComment>();
               feedComments.add(fCommentEvent = OCSUGC_TestUtility.createFeedComment(fItemEvent.Id, false));
               feedComments.add(fCommentDiscussion = OCSUGC_TestUtility.createFeedComment(fItemDiscussion.Id, false));
               feedComments.add(fCommentKA = OCSUGC_TestUtility.createFeedComment(fItemKA.Id, false));
               insert feedComments;
               delete feedComments;
            test.stopTest();
    }
}
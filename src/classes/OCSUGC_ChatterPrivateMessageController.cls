//
// (c) 2014 Appirio, Inc.
//
// This page will allow the user to Contact the Spheros Admin public group from a link on the Home Page.
//
// 22 Aug 2014    Pawan Tyagi    provide a functionality Of Inbox,Sending Messages,Reply Messages
// 18 Jul 2015    Pratz Joshi (Appirio) - Fixed the exception issue due to large number of active OncoPeer Users
//                                        by streamining the method calls to OCSUGC_Utilities class
public class OCSUGC_ChatterPrivateMessageController
{
    private Id orgWideEmailAddressId;
    public static String strMessage                     { get; set; }                   //message content
    public static String strConversationId              { get; set; }                   //conversation id
    public static String strRecipientName               { get; set; }                   //receipent name
    public static String strRecipientName1              { get; set; }                   //receipent name
    public static String strSendMessage                 { get; set; }                   //send message text
    public static String strIndivisualMessageId         { get; set; }                   //particular message Id
    public  string SenderId  ;                                                          //Message Sender Id
    public  string communityId ;                                                        //Community Id
    private string strReplyMessageId;                                                   //reply message Id
    private string strConversationIdPrivate;                                            //ConversationIdprivate

    public boolean isMessageSent                        { get; set; }                   //Check if message is sent successfully.
    public boolean newMessage                           { get; set; }                   //check if we click on new message or else
    public boolean userActive                           { get; set; }                   //check user is active or not

    public  List<MessagesWrapper> lstMessageWrapper     { get; set; }                   //List of Wrapper class
    public  List<MessagesWrapper> lstSingleMessage      { get; set; }                   // list of single message

    private Map<String, List<MessagesWrapper>> mapConversationIdToListMessage;          //Conversation having multiple messages

    public Id RecipientId { get; set; } //receipent

    private User usr; //user
    private static final String OCSUGC_PRIVATE_MESSAGE = 'OCSUGC_Email_Notification_for_Private_messages';

    //controller
    public OCSUGC_ChatterPrivateMessageController()
    {
        //Intialization of variables
        strConversationId = strReplyMessageId = strConversationIdPrivate = strRecipientName1 = strSendMessage = '';
        mapConversationIdToListMessage = new Map<String, List<MessagesWrapper>>();
        lstSingleMessage = new List<MessagesWrapper>();
        RecipientId = ApexPages.currentPage().getParameters().get('userId');
        newMessage = true;

        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        
        //Added by Pratz Joshi(Appirio) to fix the exception issue of >50000 query rows returned due to multiple method calls to OCSUGC_Utilities
        communityId = OCSUGC_Utilities.getCurrentCommunityNetworkId(OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc')));//getting community Id

        //checking receipent is Active or InActive User
        checkReceipentValid(RecipientId);
        //initialization....
        lstMessageWrapper = new List<MessagesWrapper>();

        lstMessageWrapper = new List<MessagesWrapper>();
        try
        {
            //getting all the messages of user in Inbox
            getMessageInInbox();
        }
        catch(Exception ex)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ErrorChatterMessagePermission);
            ApexPages.addMessage(myMsg);
        }
    }

    //  Message Wrapper Class
    //  argument none
    //  @return none
    public class MessagesWrapper
    {
        public  string Firstname         { get;set; }                       // first name of sender
        public  string Lastname          { get;set; }                       // Last name of sender
        public  string ShortMessage      { get;set; }                       // ShortMessage of sender
        public  string FullMessage       { get;set; }                       // FullMessage of sender
        public  string Photo             { get;set; }                       // Photo of sender
        public  String SendDateTime      { get;set; }                       // SendDateTime of sender
        public String Id                 { get;set; }                       // Id of sender
        public String MessageId          { get;set; }                       // MessageId of sender
        public Boolean isReceived        { get;set; }                       // received message
        public string strBgColor         { get;set; }                       // background color

        //Wrapper Class Constructor
        public MessagesWrapper(string Firstname, string Lastname, string ShortMessage, string FullMessage, string Photo, String SendDateTime, String Id, String MessageId, Boolean isReceived)
        {
            this.Firstname =Firstname;
            this.Lastname  =Lastname;
            this.ShortMessage = ShortMessage;
            this.FullMessage = FullMessage;
            this.Photo = Photo;
            this.SendDateTime = SendDateTime;
            this.Id = Id;
            this.MessageId = MessageId;
            this.isReceived = isReceived;
        }
    }

    //  sending email and message on clicking save button
    //  argument none
    //  @return none
    public void btnSend()
    {
        //checking
        if(strRecipientName != null && strRecipientName.trim() != '' && strMessage.trim() != null && strMessage.trim() != '')
        {
            try
            {
                if(!Test.isRunningTest())
                    ConnectApi.ChatterMessages.sendMessage(strMessage,RecipientId);
                //send email to Receipent
                sendEmailToReceipent(RecipientId,strMessage);
                strConversationIdPrivate = 'New';
                //getting All Messages in Inbox(Conversation)
                getMessageInInbox();
                //fetching all the messages of a particuler conversation
                fetchSelectedInboxMessage();
            }
            catch(Exception ex)
            {
                system.debug(ex);
            }
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ErrorMessage);
            ApexPages.addMessage(myMsg);
        }
    }

    //  redirecting to the OCSUGC_Home page when click on cancel button
    //  argument.
    //  @return a PageReference that is redirecting to OCSUGC_Home page.
    public pageReference btnCancel(){

        //redirect the page to OCSUGC home page
        Pagereference pg = Page.OCSUGC_Home;
        pg.getParameters().put('userId',RecipientId);
        pg.getParameters().put('isResetPwd','true');
        pg.setRedirect(true);
        return pg;
    }

    //  getting all messages (Conversation) related to login user
    //  argument none
    //  @return none
    private void getMessageInInbox()
    {
        mapConversationIdToListMessage = new Map<String, List<MessagesWrapper>>();
        lstMessageWrapper = new List<MessagesWrapper>();
        List<MessagesWrapper> lstMessageWrapperTemp = new List<MessagesWrapper>();
        ConnectApi.ChatterMessagePage objChatterMessagePage = ConnectApi.ChatterMessages.getMessages(communityId);
        set<String> setConversationId = new set<String>();

        //Iterating over list of ConnectApi.ChatterMessagePage...
        for(ConnectApi.ChatterMessage objChatterMessage : objChatterMessagePage.messages)
        {
            DateTime sendDateTime;
            String Firstname = objChatterMessage.sender.firstName;//getting firstName of Message sender
            String Lastname =  objChatterMessage.sender.lastName;//getting lastName of Message sender
            String ShortMessage = '';
            String Photo = objChatterMessage.sender.photo.smallPhotoUrl;
            String FullMessage = objChatterMessage.body.text;
            String Id = objChatterMessage.Id;

            //checking Message body null or not
            if(objChatterMessage.body.text != null && objChatterMessage.body.text.trim() != '')
            {
                if(objChatterMessage.body.text.length() > 20)
                ShortMessage = objChatterMessage.body.text.substring(0,20) + '...';
                else
                ShortMessage = objChatterMessage.body.text;
            }

            String SenderId = objChatterMessage.sender.Id;                  //getting Id of Message sender
            sendDateTime = objChatterMessage.sentDate;                      //getting send Date Time

            //getting the Date Specific Message
            String DateMessage = showTimeMessage(sendDateTime);

            //put the value into the wrapper class
            MessagesWrapper objMessagesWrapper = new MessagesWrapper(Firstname, Lastname, ShortMessage, FullMessage, Photo, DateMessage,objChatterMessage.conversationId,Id,false);

            //getting user Receipents
            for(ConnectApi.UserSummary usr : objChatterMessage.recipients)
            {
                //checking sender and login user same or not
                if(objChatterMessage.sender.Id == userinfo.getUserId() && objChatterMessage.sender.Id != usr.Id)
                {
                    objMessagesWrapper.isReceived = false;
                    objMessagesWrapper.Firstname = usr.firstName;
                    objMessagesWrapper.Lastname = usr.lastName;
                    objMessagesWrapper.Photo = usr.photo.smallPhotoUrl;
                }
                //checking sender and login user are different or not
                else if(objChatterMessage.sender.Id != userinfo.getUserId() && objChatterMessage.sender.Id == usr.Id)
                {
                    objMessagesWrapper.isReceived = true;
                    objMessagesWrapper.Firstname = usr.firstName;
                    objMessagesWrapper.Lastname = usr.lastName;
                    {
                        objMessagesWrapper.Photo = usr.photo.smallPhotoUrl;
                    }
                }
            }

            //getting the full name of user
            String strRecipient = objMessagesWrapper.Firstname + ' ' + objMessagesWrapper.Lastname;

            //cheking conversation id (it is duplicate or not)
            if(strRecipientName == strRecipient && strConversationIdPrivate =='New')
                strConversationIdPrivate = strConversationId = objChatterMessage.conversationId;

            //checking unique conversation ids which is different from the login user
            if(!setConversationId.Contains(objChatterMessage.conversationId) && objChatterMessage.sender.Id != userinfo.getUserId())
            {
                //checking conversation id already exist or not
                if(!setConversationId.Contains(objChatterMessage.conversationId))
                {
                    //cheking conversation id is same as of user whose inbox message is recently opened
                    if(strConversationIdPrivate == objChatterMessage.conversationId)
                        objMessagesWrapper.strBgColor = 'rgb(147, 199, 204);';

                    //storing all conversation ids
                    setConversationId.add(objChatterMessage.conversationId);
                    lstMessageWrapper.add(objMessagesWrapper);
                }
            }
            else
                lstMessageWrapperTemp.add(objMessagesWrapper);

            //keyset => Conversation Id and values must be the all message
            if(!mapConversationIdToListMessage.containsKey(objMessagesWrapper.Id))
                mapConversationIdToListMessage.put(objMessagesWrapper.Id, new List<MessagesWrapper> {objMessagesWrapper});
            else
                mapConversationIdToListMessage.get(objMessagesWrapper.Id).add(objMessagesWrapper);
        //End of for
        }

        //getting all data related to user conversation
        for(MessagesWrapper objInnerClass : lstMessageWrapperTemp)
        {
            //checking conversation id is null or not
            if(!setConversationId.Contains(objInnerClass.Id))
            {
                //changing the background colors
                if(strConversationIdPrivate == objInnerClass.Id)
                    objInnerClass.strBgColor = 'rgb(147, 199, 204);';

                //storing conversation id
                setConversationId.add(objInnerClass.Id);

                //getting all user related message stuff
                lstMessageWrapper.add(objInnerClass);
            }
        }
    }

    //  retreiving message from inbox when cliking on message in inbox
    //  argument none
    //  @return none
    public void fetchSelectedInboxMessage()
    {
        newMessage = false;
        isMessageSent = false;
        strSendMessage ='';
        lstSingleMessage = new List<MessagesWrapper>();

        //checking conversation having messages or not
        if(mapConversationIdToListMessage.containsKey(strConversationId))
            lstSingleMessage.addAll(mapConversationIdToListMessage.get(strConversationId));

        //retreiving all messages related to conversation
        for(MessagesWrapper objWrapper : lstSingleMessage)
        {
            strReplyMessageId = objWrapper.MessageId;
            break;
        }

        //private conversation
        strConversationIdPrivate =strConversationId;

        //full name of user
        strRecipientName1 = userInfo.getFirstName() + ' ' + userInfo.getLastName();

        //make conversation as read
        System.debug('fetchSelectedInboxMessage In Test.isRunningTest()');
/*        if(!Test.isRunningTest()){
            ConnectApi.ChatterMessages.markConversationRead(networkId,strConversationId, true);
        }
*/
    }

   //  reply the messages in a particular conversation
   //  argument none
   //  @return none
   public void replyMessage()
    {
        strConversationId = strConversationIdPrivate;

        //sending a reply to users in a particuler conversation
        if(!Test.isRunningTest())
            ConnectApi.ChatterMessages.replyToMessage(strSendMessage, strReplyMessageId);

        //send Email
        if(RecipientId == null)
            RecipientId = ApexPages.currentPage().getParameters().get('userId');

        if(RecipientId != null)
        {
            try
            {
                // sending email to user
                sendEmailToReceipent(RecipientId,strSendMessage);
            }
            catch(Exception ex)
            {
                system.debug(ex);
            }
        }
        else
        {
            // error messages
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ErrorMessage);
            ApexPages.addMessage(myMsg);
        }

        //getting all conversations of user
        getMessageInInbox();

        //fetching all messages related to conversation
        fetchSelectedInboxMessage();
    }

    //  new message click functionality
    //  argument none
    //  @return none
    public void newMessage()
    {
        //cheking receipent is there or not
        if(RecipientId == null)
            RecipientId = ApexPages.currentPage().getParameters().get('userId');
        newMessage = true;

        //checking receipent is Active user or not
        checkReceipentValid(RecipientId);
    }

    //  sending an email to receipent
    //  argument Recipient Id,
    //           sender message
    //  @return none
    private void sendEmailToReceipent(Id RecipientId,String strMessage1)
    {
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

        //Query and fetch the Email Template Format for sending Email to User when he/she recieved a private message
        EmailTemplate objEmailTemplate = new EmailTemplate();

        //retreving email Template
        objEmailTemplate = [SELECT Id,
                                   Subject,
                                   HtmlValue,
                                   Body
                                   FROM EmailTemplate
                                   WHERE DeveloperName = :OCSUGC_PRIVATE_MESSAGE
                                   LIMIT 1];

        String strHtmlBody  = objEmailTemplate.HtmlValue;
        String strPlainBody = objEmailTemplate.Body;
        String strSubject   = objEmailTemplate.Subject;

        // Start Added Label Value Naresh T-363912
        strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
        strSubject  = strSubject.replace('CONTACT_NAME',Userinfo.getName());
        // End
        strHtmlBody = strHtmlBody.replace('MESSAGE_BODY', strMessage1);
        strPlainBody = strPlainBody.replace('MESSAGE_BODY', strMessage1);

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSaveAsActivity(false);
        //sender display name

        email.setSubject(strSubject);                 //subject of email
        email.setHtmlBody(strHtmlBody);
        email.setPlainTextBody(strPlainBody);
        email.setTargetObjectId(RecipientId);
        // Use Organization Wide Address
           if(orgWideEmailAddressId != null)
                email.setOrgWideEmailAddressId(orgWideEmailAddressId);
              else
                email.setSenderDisplayName('OCSUGC');
        lstEmail.add(email);

        //Sending Email here
        if(!lstEmail.IsEmpty())
            Messaging.sendEmail(lstEmail);
    }

    //  sending valid receipent
    //  argument Receipent Id
    //  @return none
    private void checkReceipentValid(Id RecipientId)
    {
        //checking receipent is valid user or not(Active or InActive)
        if(RecipientId != null)
        {
            usr = new User();
            usr = [SELECT Name,IsActive FROM User WHERE Id=: RecipientId LIMIT 1];

            //cheking user is there or not
            if(usr != null)
            {
                //checking user is active or not
                if(!usr.IsActive)
                {
                    //error messages
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ErrorMessageInActiveUser);
                    ApexPages.addMessage(myMsg);
                    userActive = false;
                }
                else
                {
                    strRecipientName = usr.Name;
                    userActive = true;
                }
            }
        }
    }

    //  showing time messages
    //  argument send date
    //  @return date message which displays on chatter Message page
    private String showTimeMessage(DateTime sendDateTime)
    {
        String DateMessage;

        //checking send date is there or not
        if(sendDateTime != null)
        {
            //if days == 1 then 'yesterday'
            if(sendDateTime.dateGMT().daysBetween(DateTime.now().dateGMT()) == 1)
                DateMessage = 'yesterday';

            //if days == 0 then display hours
            else if(sendDateTime.dateGMT().daysBetween(DateTime.now().dateGMT()) == 0)
            {
                DateMessage = (DateTime.now().hourGmt() - sendDateTime.hourGmt()) + ' hours ago';

                //if sending and receving message hour is 1 then display '1 hour ago'
                if((DateTime.now().hourGmt() - sendDateTime.hourGmt()) == 1)
                    DateMessage = (DateTime.now().hourGmt() - sendDateTime.hourGmt()) + ' hour ago';

                //if sending and receving message hour is 0 then display minutes
                else if((DateTime.now().hourGmt() - sendDateTime.hourGmt()) == 0)
                {
                    DateMessage = (DateTime.now().minuteGmt() - sendDateTime.minuteGmt()) + ' minutes ago';

                    //if sending and receving message minute is 0 then display '1 minute ago'
                    if((DateTime.now().minuteGmt() - sendDateTime.minuteGmt()) == 1)
                        DateMessage = (DateTime.now().minuteGmt() - sendDateTime.minuteGmt()) + ' minute ago';

                    //if sending and receving message minute is 0 then display seconds
                    else if((DateTime.now().minuteGmt() - sendDateTime.minuteGmt()) == 0)
                    {
                        DateMessage = sendDateTime.secondGmt() + ' seconds ago';

                        //if sending and receving message second is 1 then display '1 second ago'
                        if((DateTime.now().secondGmt() - sendDateTime.secondGmt()) == 1)
                            DateMessage = sendDateTime.secondGmt() + ' second ago';

                        //if sending and receving message second is 0 then display '0 second ago'
                        else if((DateTime.now().secondGmt() - sendDateTime.secondGmt()) == 0)
                            DateMessage = '0 second ago';
                    }
                }
            }
            else
                //if days >1 then 'MMMM d - h:mma' format
                DateMessage = sendDateTime.format('MMMM d - h:mma');
        }
        return DateMessage;
    }
}
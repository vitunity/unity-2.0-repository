/*
Name        : ChatterUtilityController
Updated By  : Rishi Khirbat (Appirio India)
Date        : May 21st 2014
Purpose     : An Apex controller to get Information for ChatterUtility.page.
              - Keep maintain the standard functionality of chatter components along with showHeader="false" & applyHtmlTag="false"
*/
public class OCSUGC_ChatterUtilityController {

    public Id userId {get;set;}
    public Boolean isUserPhotoUpload {get;set;}
    //public Boolean isFollow {get;set;}
    //public Boolean isFollowing {get;set;} //for list who a User is Following
    public Boolean isFollowers {get;set;} //for list a User's Followers
    //public Boolean isFeedWithFollowers {get;set;} //for list a User's Feed WithFollowers

    private String showComponent;

    public OCSUGC_ChatterUtilityController() {
        userId = Apexpages.currentPage().getParameters().get('Id');
        showComponent = Apexpages.currentPage().getParameters().get('component');

        userId = userId == null ? UserInfo.getUserId() : userId;

        if(showComponent != null) {
            if(showComponent.equals('UserPhotoUpload')) {
                isUserPhotoUpload = true;
            } else if(showComponent.equals('Following')) {
            } else if(showComponent.equals('Followers')) {
                isFollowers = true;
            } else if(showComponent.equals('FeedWithFollowers')) {
            }
        }
    }
}
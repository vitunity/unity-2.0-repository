//
// (c) 2014 Appirio, Inc.
//
// Test Class for OCSUGC_ChatterUtilityController class
//
// 15th January, 2015 Ashish Sharma
@isTest
private class OCSUGC_ChatterUtilityControllerTest {

    static testMethod void testChatterUtilityController() {
        ApexPages.currentPage().getParameters().put('Id', Userinfo.getUserId());
        ApexPages.currentPage().getParameters().put('component', 'UserPhotoUpload');    
        
        OCSUGC_ChatterUtilityController ctrl = new OCSUGC_ChatterUtilityController();
        System.assert(ctrl.isUserPhotoUpload);
        
        ApexPages.currentPage().getParameters().put('component', 'Following');
        OCSUGC_ChatterUtilityController ctrl1 = new OCSUGC_ChatterUtilityController();

        ApexPages.currentPage().getParameters().put('component', 'FeedWithFollowers');
        OCSUGC_ChatterUtilityController ctrl2 = new OCSUGC_ChatterUtilityController();
        
        ApexPages.currentPage().getParameters().put('component', 'Followers');
        OCSUGC_ChatterUtilityController ctrl3 = new OCSUGC_ChatterUtilityController();
        System.assert(ctrl3.isFollowers);
    }
}
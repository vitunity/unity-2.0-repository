/*
Name        : OCSUGC_CheckPermissionSetsForUser
Created By  : Shital Bhujbal (Varian India)
Date        : 15 April, 2015
Purpose     : A class written to remove permission sets for disabled users in OCSUGC community.
*/


Public class OCSUGC_CheckPermissionSetsForUser{

    public static void removePermissionSetFromUsers(Set<ID> removeUserIds, Set<String> permissionSetname) {
        List<PermissionSetAssignment> permissionSets = [Select Id,PermissionSet.Name From PermissionSetAssignment
                                                        Where AssigneeId IN:removeUserIds
                                                        And PermissionSet.Name IN: permissionSetname];
         
        if(permissionSets.size() > 0) {
                 delete permissionSets;
        }                                                                                                
    }
}
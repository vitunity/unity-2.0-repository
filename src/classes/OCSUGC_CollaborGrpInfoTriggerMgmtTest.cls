/*
  Name        : OCSUGC_CollaborGrpInfoTriggerMgmtTest
  Updated By  : Puneet Sardana
  Date        : 03 Feb, 2015
  Purpose     : Test class  for OCSUGC_CollaborationGroupInfoTriggerMgmt
*/

@isTest
public class OCSUGC_CollaborGrpInfoTriggerMgmtTest {
    static ConnectApi.FeedElement feedElementPoll;
    static testMethod void testCollaGroupTrigger() {
        Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
      User testuser = OCSUGC_TestUtility.createStandardUser(true,adminProfile.Id, 'testuser','12');        
      EmailTemplate emailTemplate = OCSUGC_TestUtility.createEmailTemplate('test', 'OCSUGC_Terms_Conditions_Change', 'Notice of changes to FGAB_GROUP_NAME Terms of Use', 'Hi,The terms of use for FGAB_GROUP_NAME have been updated. The updated Terms of Use can be found here.Thanks,OncoPeer', 'Hi,The terms of use for FGAB_GROUP_NAME have been updated. The updated Terms of Use can be found here.Thanks,OncoPeer',true);
      
      User adminUsr1 = OCSUGC_TestUtility.createStandardUser(true,adminProfile.Id, 'test','12');         
      System.runAs(adminUsr1) {  
        CollaborationGroup cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionGroup', 'Public', ocsugcNetwork.Id, true);
        List<OCSUGC_CollaborationGroupInfo__c> groupInfo =[SELECT Name,OCSUGC_Group_TermsAndConditions__c
                                                         FROM OCSUGC_CollaborationGroupInfo__c
                                                         WHERE OCSUGC_Group_Id__c=:cPublicGroup.Id]; 
        CollaborationGroupMember memberPublic = OCSUGC_TestUtility.createGroupMember(testuser.id, cPublicGroup.Id, true);
        FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
        OCSUGC_DiscussionDetail__c discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item.Id, true);
        OCSUGC_Intranet_Content__c event = OCSUGC_TestUtility.createEvent(cPublicGroup.Id,Date.today(),Date.today().addDays(4),true);         
      OCSUGC_TestUtility.createPollDetail(cPublicGroup.Id, 'TestFGABGrp', item.Id, 'Poll Question', true);
      OCSUGC_Knowledge_Exchange__c kwn = OCSUGC_TestUtility.createKnwExchange(cPublicGroup.Id,cPublicGroup.Name,true); 
      kwn.OCSUGC_Group_Id__c =  cPublicGroup.Id;
      update kwn;         
      if(groupInfo.size() > 0) {            
            groupInfo[0].Name = 'test name 2';
            groupInfo[0].OCSUGC_Group_TermsAndConditions__c = 'test terms conditions';
            update groupInfo; 
            groupInfo =[SELECT Name,OCSUGC_Group_TermsAndConditions__c
                     FROM OCSUGC_CollaborationGroupInfo__c
                     WHERE OCSUGC_Group_Id__c=:cPublicGroup.Id]; 
        System.assert(groupInfo[0].Name.equals('test name 2'));
      }    
    }        
  }
}
//
// (c) 2014 Appirio, Inc.
//
// Class containing Handler methods for OCSUGC_CollaborationGroupInfo__c object Trigger
//
// Nov 14, 2014   Ajay Gautam (Appirio)   Original [ Ref: T-334411 ]


public without sharing class OCSUGC_CollaborationGroupInfoTriggerMgmt {
  private static final Integer SF_BCC_LIMIT = 25;
	private static final Integer CONST_SOQL_ROWS_LIMIT = Limits.getLimitQueryRows();

	// @description: after Update handler method for related Trigger
	// @param:   List Trigger.NEW
	// @param:   OldMap from Trigger i.e. Trigger.oldMap
	// @return:  void
	// @author:  Ajay Gautam - T-334411
  public static void afterUpdate(List<OCSUGC_CollaborationGroupInfo__c> collabGrpInfoNewList, Map<Id, OCSUGC_CollaborationGroupInfo__c> collabGrpInfoOldMap) {
      reflectGroupNameChangesOnRelatedRecords(collabGrpInfoNewList, collabGrpInfoOldMap);
      sendEmailOnTermsConditionsChange(collabGrpInfoNewList, collabGrpInfoOldMap);
  }
  // @description: send email to members
  // @param:   List of OCSUGC_CollaborationGroupInfo__c, this is Trigger.NEW in fact.
  // @param:   Map of Id and OCSUGC_CollaborationGroupInfo__c record i.e. OldMap from Trigger
  // @return:  void
  // @author:  Puneet Sardana -T-339436
  private static void sendEmailOnTermsConditionsChange(List<OCSUGC_CollaborationGroupInfo__c> collabGrpInfoNewList, Map<Id, OCSUGC_CollaborationGroupInfo__c> collabGrpInfoOldMap) {
    Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecordsToBeReflected = new Map<Id,OCSUGC_CollaborationGroupInfo__c>();
    if(collabGrpInfoOldMap != null) {
      for(OCSUGC_CollaborationGroupInfo__c objCGI : collabGrpInfoNewList) {
        if(objCGI.OCSUGC_Group_TermsAndConditions__c != null && collabGrpInfoOldMap.containsKey(objCGI.Id) && objCGI.OCSUGC_Group_TermsAndConditions__c != collabGrpInfoOldMap.get(objCGI.Id).OCSUGC_Group_TermsAndConditions__c) {
          mapOfCollabGrpInfoRecordsToBeReflected.put(objCGI.OCSUGC_Group_Id__c, objCGI);
        }
      }
    }
    if(mapOfCollabGrpInfoRecordsToBeReflected.size() > 0) {
      // Reflect Group Name changes on Discussion Details
      sendEmailNotificationToGroupMembers(mapOfCollabGrpInfoRecordsToBeReflected);
    }
  }



    public static void sendEmailNotificationToGroupMembers( Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecordsToBeReflected) {
    	   Id orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
		    //list will contain Email drafts which needs to be send
		   EmailTemplate template = [Select Subject, body, Id, htmlValue
		                             From EmailTemplate
		                             Where DeveloperName = 'OCSUGC_Terms_Conditions_Change'
                                 AND IsActive = true limit 1];
		    List<String> lstEmails = new List<String>();
		    Integer noOfReceipts;
		    String htmlBody,plainBody,Subject;
		    Messaging.SingleEmailMessage email;
		    //fatching group member's details
		    for(CollaborationGroup newGroup :[Select Id,Name,OwnerId,
							                                      (Select MemberId,
							                                      Member.FirstName,
							                                      Member.Email,
							                                      Member.Contact.OCSUGC_Notif_Pref_EditGroup__c
							                                      From GroupMembers)
		                                      From CollaborationGroup
		                                      Where Id IN :mapOfCollabGrpInfoRecordsToBeReflected.keySet()]) {
		       noOfReceipts=0;
		       //drafting email body

		        for(CollaborationGroupMember member : newGroup.GroupMembers) {
		          if(newGroup.OwnerId != member.MemberId) {
		            //set email to the group member
		            htmlBody = setEmailBody(template.htmlValue,newGroup.Id,newGroup.Name);
		            subject =  template.Subject.replace('FGAB_GROUP_NAME',newGroup.Name);
		            email = new Messaging.SingleEmailMessage();
		            email.setSaveAsActivity(false);
		            email.setSubject(subject);
		            email.setHtmlBody(htmlBody);
		            // Use Organization Wide Address
		            if(orgWideEmailAddressId != null)
						      email.setOrgWideEmailAddressId(orgWideEmailAddressId);
						    else 
						      email.setSenderDisplayName('OCSUGC'); 
		            noOfReceipts++;
		            lstEmails.add(member.Member.Email);
		            if(noOfReceipts == SF_BCC_LIMIT) {
		               email.setBccAddresses(lstEmails);
		               noOfReceipts = 0;
		               Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { email });
		               lstEmails = new List<String>();
		            }
		          }
		        }
		     }
			  //Send rest emails
			  if(lstEmails.size() >0) {
			    email.setBccAddresses(lstEmails);
			    Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { email });
			    lstEmails = new List<String>();
			  }
    }

  private static string setEmailBody(String emailBody,String groupId,String groupName) {
  	emailBody = emailBody.replace('FGAB_GROUP_NAME',groupName);
  	emailBody = emailBody.replace('FGAB_TERMS_LINK', URL.getSalesforceBaseUrl().toExternalForm() + '/OCSUGC/OCSUGC_FGABAboutGroup?fgab=true&termsUpdate=true&g=' + groupId +'&showPanel=About');
  	// Added Label Value Naresh T-363912
  	emailBody = emailBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
  	return emailBody;
  }
  // @description: reflect Name changes from this record to all related records using ExternalID
  // @param:   List of OCSUGC_CollaborationGroupInfo__c, this is Trigger.NEW in fact.
  // @param:   Map of Id and OCSUGC_CollaborationGroupInfo__c record i.e. OldMap from Trigger
  // @return:  void
  // @author:  Ajay Gautam - T-334411
	private static void reflectGroupNameChangesOnRelatedRecords(List<OCSUGC_CollaborationGroupInfo__c> collabGrpInfoNewList, Map<Id, OCSUGC_CollaborationGroupInfo__c> collabGrpInfoOldMap) {
    Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecordsToBeReflected = new Map<Id,OCSUGC_CollaborationGroupInfo__c>();
    if(collabGrpInfoOldMap != null) {
    	for(OCSUGC_CollaborationGroupInfo__c objCGI : collabGrpInfoNewList) {
    		if(objCGI.OCSUGC_Group_Id__c != null && collabGrpInfoOldMap.containsKey(objCGI.Id) && objCGI.Name != collabGrpInfoOldMap.get(objCGI.Id).Name) {
    			mapOfCollabGrpInfoRecordsToBeReflected.put(objCGI.OCSUGC_Group_Id__c, objCGI);
    		}
    	}
    }
    if(mapOfCollabGrpInfoRecordsToBeReflected.size() > 0) {
    	// Reflect Group Name changes on Discussion Details
      updateDiscussionDetails(mapOfCollabGrpInfoRecordsToBeReflected);
      // Reflect Group Name changes on Knowledge Files
      updateKnowledgeFiles(mapOfCollabGrpInfoRecordsToBeReflected);
      // Reflect Group Name changes on Intranet Content Records
      updateIntranetContentRecords(mapOfCollabGrpInfoRecordsToBeReflected);
      // Reflect Group Name changes on Poll Detail Records
      updatePollDetails(mapOfCollabGrpInfoRecordsToBeReflected);
    }
	}

  // @description: reflect Name changes from CGI record to OCSUGC_DiscussionDetail__c records using ExternalID
  // @param:   Map of GroupId and OCSUGC_CollaborationGroupInfo__c record.
  // @return:  void
  // @author:  Ajay Gautam - T-334411
  private static void updateDiscussionDetails(Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecords) {
    List<OCSUGC_DiscussionDetail__c> listOfDiscussionDetails =  [ SELECT  OCSUGC_Group__c, OCSUGC_GroupId__c, Id
                                                                  FROM    OCSUGC_DiscussionDetail__c
                                                                  WHERE   OCSUGC_GroupId__c IN :mapOfCollabGrpInfoRecords.keySet()
                                                                  LIMIT   :CONST_SOQL_ROWS_LIMIT ];
    for (OCSUGC_DiscussionDetail__c objDiscussionDetail : listOfDiscussionDetails) {
    	if(mapOfCollabGrpInfoRecords.containsKey(objDiscussionDetail.OCSUGC_GroupId__c)) {
    		objDiscussionDetail.OCSUGC_Group__c = mapOfCollabGrpInfoRecords.get(objDiscussionDetail.OCSUGC_GroupId__c).Name; // Object will always be there as we are on Trigger Lists
    	}
    }
    if(listOfDiscussionDetails.size() > 0) {
    	update listOfDiscussionDetails;
    }
  }

  // @description: reflect Name changes from CGI record to OCSUGC_Knowledge_Exchange__c records using ExternalID
  // @param:   Map of GroupId and OCSUGC_CollaborationGroupInfo__c record.
  // @return:  void
  // @author:  Ajay Gautam - T-334411
  private static void updateKnowledgeFiles(Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecords) {
    List<OCSUGC_Knowledge_Exchange__c> listOfKnowledgeFiles =  [ SELECT  OCSUGC_Group__c, OCSUGC_Group_Id__c, Id
                                                                 FROM    OCSUGC_Knowledge_Exchange__c
                                                                 WHERE   OCSUGC_Group_Id__c IN :mapOfCollabGrpInfoRecords.keySet()
                                                                 LIMIT   :CONST_SOQL_ROWS_LIMIT ];
    for (OCSUGC_Knowledge_Exchange__c objKnowledgeFile : listOfKnowledgeFiles) {
      if(mapOfCollabGrpInfoRecords.containsKey(objKnowledgeFile.OCSUGC_Group_Id__c)) {
        objKnowledgeFile.OCSUGC_Group__c = mapOfCollabGrpInfoRecords.get(objKnowledgeFile.OCSUGC_Group_Id__c).Name; // Object will always be there as we are on Trigger Lists
      }
    }
    if(listOfKnowledgeFiles.size() > 0) {
      update listOfKnowledgeFiles;
    }
  }

  // @description: reflect Name changes from CGI record to Intranet Content records using ExternalID
  // @param:   Map of GroupId and OCSUGC_CollaborationGroupInfo__c record.
  // @return:  void
  // @author:  Ajay Gautam - T-334411
  private static void updateIntranetContentRecords(Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecords) {
    List<OCSUGC_Intranet_Content__c> listOfIntranetContentRecords = [ SELECT  OCSUGC_Group__c, OCSUGC_GroupId__c, Id
                                                               FROM    OCSUGC_Intranet_Content__c
                                                               WHERE   OCSUGC_GroupId__c IN :mapOfCollabGrpInfoRecords.keySet()
                                                               LIMIT   :CONST_SOQL_ROWS_LIMIT ];
    for (OCSUGC_Intranet_Content__c objIntranetContent : listOfIntranetContentRecords) {
      if(mapOfCollabGrpInfoRecords.containsKey(objIntranetContent.OCSUGC_GroupId__c)) {
        objIntranetContent.OCSUGC_Group__c = mapOfCollabGrpInfoRecords.get(objIntranetContent.OCSUGC_GroupId__c).Name; // Object will always be there as we are on Trigger Lists
      }
    }
    if(listOfIntranetContentRecords.size() > 0) {
      update listOfIntranetContentRecords;
    }
  }

  // @description: reflect Name changes from CGI record to OCSUGC_Poll_Details__c records using ExternalID
  // @param:   Map of GroupId and OCSUGC_CollaborationGroupInfo__c record.
  // @return:  void
  // @author:  Naresh K Shiwani - I-141376
  private static void updatePollDetails(Map<Id, OCSUGC_CollaborationGroupInfo__c> mapOfCollabGrpInfoRecords) {
    List<OCSUGC_Poll_Details__c> listOfPollDetails =  [ SELECT  OCSUGC_Group_Name__c, OCSUGC_Group_Id__c, Id
                                                                  FROM    OCSUGC_Poll_Details__c
                                                                  WHERE   OCSUGC_Group_Id__c IN :mapOfCollabGrpInfoRecords.keySet()
                                                                  LIMIT   :CONST_SOQL_ROWS_LIMIT ];
    for (OCSUGC_Poll_Details__c objPollDetail : listOfPollDetails) {
    	if(mapOfCollabGrpInfoRecords.containsKey(objPollDetail.OCSUGC_Group_Id__c)) {
    		objPollDetail.OCSUGC_Group_Name__c = mapOfCollabGrpInfoRecords.get(objPollDetail.OCSUGC_Group_Id__c).Name; // Object will always be there as we are on Trigger Lists
    	}
    }
    if(listOfPollDetails.size() > 0) {
    	update listOfPollDetails;
    }
  }

}
public with sharing class OCSUGC_CommunityData {
        public static void consumeObject(JSONParser parser) { 
                Integer depth = 0; 
                do { 
                        JSONToken curr = parser.getCurrentToken(); 
                        if (curr == JSONToken.START_OBJECT || 
                                curr == JSONToken.START_ARRAY) { 
                                depth++; 
                        } else if (curr == JSONToken.END_OBJECT || 
                                curr == JSONToken.END_ARRAY) { 
                                depth--; 
                        } 
                } while (depth > 0 && parser.nextToken() != null); 
        } 
        public class Recommendations { 
                public String actOnUrl {get;set;} 
                public String action {get;set;} 
                public Explanation explanation {get;set;} 
                public Object_Z object_Z {get;set;} // in json: object 
                public Recommendations(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'actOnUrl') { 
                                                        actOnUrl = parser.getText(); 
                                                } else if (text == 'action') { 
                                                        action = parser.getText(); 
                                                } else if (text == 'explanation') { 
                                                        explanation = new Explanation(parser); 
                                                } else if (text == 'object') { 
                                                        object_Z = new Object_Z(parser); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Recommendations consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Owner { 
                public String additionalLabel {get;set;} 
                public String companyName {get;set;} 
                public String firstName {get;set;} 
                public String id {get;set;} 
                public Boolean isActive {get;set;} 
                public Boolean isInThisCommunity {get;set;} 
                public String lastName {get;set;} 
                public Motif motif {get;set;} 
                public Community mySubscription {get;set;} 
                public String name {get;set;} 
                public Photo photo {get;set;} 
                public String recordViewUrl {get;set;} 
                public String title {get;set;} 
                public String type_Z {get;set;} // in json: type 
                public String url {get;set;} 
                public String userType {get;set;} 
                public Owner(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'additionalLabel') { 
                                                        additionalLabel = parser.getText(); 
                                                } else if (text == 'companyName') { 
                                                        companyName = parser.getText(); 
                                                } else if (text == 'firstName') { 
                                                        firstName = parser.getText(); 
                                                } else if (text == 'id') { 
                                                        id = parser.getText(); 
                                                } else if (text == 'isActive') { 
                                                        isActive = parser.getBooleanValue(); 
                                                } else if (text == 'isInThisCommunity') { 
                                                        isInThisCommunity = parser.getBooleanValue(); 
                                                } else if (text == 'lastName') { 
                                                        lastName = parser.getText(); 
                                                } else if (text == 'motif') { 
                                                        motif = new Motif(parser); 
                                                } else if (text == 'mySubscription') { 
                                                        mySubscription = new Community(parser); 
                                                } else if (text == 'name') { 
                                                        name = parser.getText(); 
                                                } else if (text == 'photo') { 
                                                        photo = new Photo(parser); 
                                                } else if (text == 'recordViewUrl') { 
                                                        recordViewUrl = parser.getText(); 
                                                } else if (text == 'title') { 
                                                        title = parser.getText(); 
                                                } else if (text == 'type') { 
                                                        type_Z = parser.getText(); 
                                                } else if (text == 'url') { 
                                                        url = parser.getText(); 
                                                } else if (text == 'userType') { 
                                                        userType = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Owner consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public List<Recommendations> recommendations {get;set;} 
        public OCSUGC_CommunityData(JSONParser parser) { 
                while (parser.nextToken() != JSONToken.END_OBJECT) { 
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                String text = parser.getText(); 
                                if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                        if (text == 'recommendations') { 
                                                recommendations = new List<Recommendations>(); 
                                                while (parser.nextToken() != JSONToken.END_ARRAY) { 
                                                        recommendations.add(new Recommendations(parser)); 
                                                } 
                                        } else { 
                                                System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                                                consumeObject(parser); 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Object_Z { 
                public String additionalLabel {get;set;} 
                public String announcement {get;set;} 
                public Boolean canHaveChatterGuests {get;set;} 
                public Community community {get;set;} 
                public String description {get;set;} 
                public String emailToChatterAddress {get;set;} 
                public String id {get;set;} 
                public Boolean isArchived {get;set;} 
                public Boolean isAutoArchiveDisabled {get;set;} 
                public String lastFeedElementPostDate {get;set;} 
                public Integer memberCount {get;set;} 
                public Motif motif {get;set;} 
                public String myRole {get;set;} 
                public String mySubscription {get;set;} 
                public String name {get;set;} 
                public Owner owner {get;set;} 
                public Photo photo {get;set;} 
                public String recordViewUrl {get;set;} 
                public String type_Z {get;set;} // in json: type 
                public String url {get;set;} 
                public String visibility {get;set;} 
                public Object_Z(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'additionalLabel') { 
                                                        additionalLabel = parser.getText(); 
                                                } else if (text == 'announcement') { 
                                                        announcement = parser.getText(); 
                                                } else if (text == 'canHaveChatterGuests') { 
                                                        canHaveChatterGuests = parser.getBooleanValue(); 
                                                } else if (text == 'community') { 
                                                        community = new Community(parser); 
                                                } else if (text == 'description') { 
                                                        description = parser.getText(); 
                                                } else if (text == 'emailToChatterAddress') { 
                                                        emailToChatterAddress = parser.getText(); 
                                                } else if (text == 'id') { 
                                                        id = parser.getText(); 
                                                } else if (text == 'isArchived') { 
                                                        isArchived = parser.getBooleanValue(); 
                                                } else if (text == 'isAutoArchiveDisabled') { 
                                                        isAutoArchiveDisabled = parser.getBooleanValue(); 
                                                } else if (text == 'lastFeedElementPostDate') { 
                                                        lastFeedElementPostDate = parser.getText(); 
                                                } else if (text == 'memberCount') { 
                                                        memberCount = parser.getIntegerValue(); 
                                                } else if (text == 'motif') { 
                                                        motif = new Motif(parser); 
                                                } else if (text == 'myRole') { 
                                                        myRole = parser.getText(); 
                                                } else if (text == 'mySubscription') { 
                                                        mySubscription = parser.getText(); 
                                                } else if (text == 'name') { 
                                                        name = parser.getText(); 
                                                } else if (text == 'owner') { 
                                                        owner = new Owner(parser); 
                                                } else if (text == 'photo') { 
                                                        photo = new Photo(parser); 
                                                } else if (text == 'recordViewUrl') { 
                                                        recordViewUrl = parser.getText(); 
                                                } else if (text == 'type') { 
                                                        type_Z = parser.getText(); 
                                                } else if (text == 'url') { 
                                                        url = parser.getText(); 
                                                } else if (text == 'visibility') { 
                                                        visibility = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Object_Z consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Explanation { 
                public String detailsUrl {get;set;} 
                public String summary {get;set;} 
                public String type_Z {get;set;} // in json: type 
                public Explanation(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'detailsUrl') { 
                                                        detailsUrl = parser.getText(); 
                                                } else if (text == 'summary') { 
                                                        summary = parser.getText(); 
                                                } else if (text == 'type') { 
                                                        type_Z = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Explanation consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Photo { 
                public String fullEmailPhotoUrl {get;set;} 
                public String largePhotoUrl {get;set;} 
                public String photoVersionId {get;set;} 
                public String smallPhotoUrl {get;set;} 
                public String standardEmailPhotoUrl {get;set;} 
                public String url {get;set;} 
                public Photo(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'fullEmailPhotoUrl') { 
                                                        fullEmailPhotoUrl = parser.getText(); 
                                                } else if (text == 'largePhotoUrl') { 
                                                        largePhotoUrl = parser.getText(); 
                                                } else if (text == 'photoVersionId') { 
                                                        photoVersionId = parser.getText(); 
                                                } else if (text == 'smallPhotoUrl') { 
                                                        smallPhotoUrl = parser.getText(); 
                                                } else if (text == 'standardEmailPhotoUrl') { 
                                                        standardEmailPhotoUrl = parser.getText(); 
                                                } else if (text == 'url') { 
                                                        url = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Photo consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Community { 
                public String id {get;set;} 
                public String url {get;set;} 
                public Community(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'id') { 
                                                        id = parser.getText(); 
                                                } else if (text == 'url') { 
                                                        url = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Community consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        public class Motif { 
                public String color {get;set;} 
                public String largeIconUrl {get;set;} 
                public String mediumIconUrl {get;set;} 
                public String smallIconUrl {get;set;} 
                public Motif(JSONParser parser) { 
                        while (parser.nextToken() != JSONToken.END_OBJECT) { 
                                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) { 
                                        String text = parser.getText(); 
                                        if (parser.nextToken() != JSONToken.VALUE_NULL) { 
                                                if (text == 'color') { 
                                                        color = parser.getText(); 
                                                } else if (text == 'largeIconUrl') { 
                                                        largeIconUrl = parser.getText(); 
                                                } else if (text == 'mediumIconUrl') { 
                                                        mediumIconUrl = parser.getText(); 
                                                } else if (text == 'smallIconUrl') { 
                                                        smallIconUrl = parser.getText(); 
                                                } else { 
                                                        System.debug(LoggingLevel.WARN, 'Motif consuming unrecognized property: '+text);
                                                        consumeObject(parser); 
                                                } 
                                        } 
                                } 
                        } 
                } 
        } 
        
        
        public static OCSUGC_CommunityData parse(String json) { 
                return new OCSUGC_CommunityData(System.JSON.createParser(json)); 
        } 
}
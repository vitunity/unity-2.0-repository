/**
 *	08 Sept 2015		Puneet Mishra, Extension Class for Calculating Stats
 *
 */
public class OCSUGC_CommunityStats {
	
	private String OCSUGCNetworkId = Label.OCSUGC_NetworkId;
    private String OCSDNetworkId = Label.OCSDC_NetworkId;
    public Map<String, Integer> CommunityMap {get;set;}
	
	public OCSUGC_CommunityStats() {
        CommunityMap = new Map<String, Integer>();
        loadAdminData();
    }
    
     public void loadAdminData() {
        AggregateResult pendingKA = [ SELECT Count(Id) KA FROM OCSUGC_Knowledge_Exchange__c where OCSUGC_Status__c =: Label.OCSUGC_KA_Pending_Approval AND OCSUGC_Is_Deleted__c=false]; 
        CommunityMap.put('KA', (Integer)pendingKA.get('KA'));
        loadOncoPeerDetails();
        loadDeveloperDetails();
        loadFGABDetails();
        feedTemDetails();
        fetchTags();
    }
    
    @TestVisible
    private void loadOncoPeerDetails() {
        // Number of Users in OncoPeer
        AggregateResult OCSUGC_Members = [ SELECT count(Id) OCS FROM NetworkMember WHERE Member.isActive =: true 
                                           AND NetworkId =:OCSUGCNetworkId AND Member.Contact.OCSUGC_UserStatus__c =:Label.OCSUGC_Approved_Status];
        CommunityMap.put('OCSUGC', (Integer)OCSUGC_Members.get('OCS'));
        
        // Number of Groups in OncoPeer Cloud
        Map<String, OCSUGC_CollaborationGroupInfo__c> OCSUGCCollaborationGrpMap = new Map<String, OCSUGC_CollaborationGroupInfo__c>();
        OCSUGCCollaborationGrpMap = groupDetails(null);
        
        String NetworkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_OncoPeer);
        
        AggregateResult OCSUGC_Groups = [ SELECT count(Id)OCSUGC_GP FROM CollaborationGroup WHERE Id IN: OCSUGCCollaborationGrpMap.keySet() And networkid =: NetworkId ];
        CommunityMap.put('OCSUGC_GRP', (Integer)OCSUGC_Groups.get('OCSUGC_GP'));
        
    }
    
    @TestVisible
    private void loadDeveloperDetails() {
        AggregateResult OCSD_Members = [ SELECT count(Id) OCSD FROM NetworkMember WHERE Member.isActive =: true 
                                         AND NetworkId =:OCSDNetworkId AND Member.Contact.OCSDC_UserStatus__c = 'Member' AND Member.Contact.OCSUGC_UserStatus__c =:Label.OCSUGC_Approved_Status];
        CommunityMap.put('DEV', (Integer)OCSD_Members.get('OCSD'));
        
        // Number of Groups in Developer Cloud
        Map<String, OCSUGC_CollaborationGroupInfo__c> OCSUGCCollaborationGrpMap = new Map<String, OCSUGC_CollaborationGroupInfo__c>();
        OCSUGCCollaborationGrpMap = groupDetails(null);
        
        String NetworkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_DeveloperCloud);
        
        AggregateResult OCSUGC_Groups = [ SELECT count(Id)DEV_GP FROM CollaborationGroup WHERE Id IN: OCSUGCCollaborationGrpMap.keySet() And networkid =: NetworkId ];
        CommunityMap.put('DEV_GRP', (Integer)OCSUGC_Groups.get('DEV_GP'));
        
    }
    
    @TestVisible
    private void loadFGABDetails() {
        AggregateResult FGAB_Gps = [SELECT Count(OCSUGC_Group_Id__c)FGABGP FROM OCSUGC_CollaborationGroupInfo__c
                                    Where OCSUGC_Group_Type__c =: Label.OCSUGC_FocusGroupAdvisoryBoard AND OCSUGC_IsArchived__c != true];
                                    
        Map<String, OCSUGC_CollaborationGroupInfo__c> OCSUGCCollaborationGrpMap = new Map<String, OCSUGC_CollaborationGroupInfo__c>();
        OCSUGCCollaborationGrpMap = groupDetails(Label.OCSUGC_FocusGroupAdvisoryBoard);
        
        List<CollaborationGroupMember> FGABMembers = [Select CollaborationGroupId From CollaborationGroupMember Where CollaborationGroup.IsArchived = false 
                                                      AND CollaborationGroupId IN: OCSUGCCollaborationGrpMap.keySet() ];
        CommunityMap.put('FGABGP',(Integer)FGAB_Gps.get('FGABGP'));
        CommunityMap.put('FGAB_Members', FGABMembers.size());
    }
    
    @TestVisible
    private Map<String, OCSUGC_CollaborationGroupInfo__c> groupDetails(String grpType) {
        Map<String, OCSUGC_CollaborationGroupInfo__c> collborationGrpMap = new Map<String, OCSUGC_CollaborationGroupInfo__c>();
        String query = 'Select OCSUGC_Group_Id__c From OCSUGC_CollaborationGroupInfo__c Where OCSUGC_IsArchived__c != true ';
        if(grpType != null) {
            query = query + ' AND OCSUGC_Group_Type__c = \'' + grpType + '\' ';
        } else {
            query = query + ' AND OCSUGC_Group_Type__c = ' + grpType ;
        }
        for(OCSUGC_CollaborationGroupInfo__c collaboration : Database.query(query)) {
            collborationGrpMap.put(String.valueOf(collaboration.OCSUGC_Group_Id__c), collaboration );
        }
        return collborationGrpMap;
    }
    
    @TestVisible
    private void feedTemDetails() {
        List<FeedItem> feedItem = [SELECT NetworkScope, LikeCount, CommentCount FROM FeedItem WHERE NetworkScope != null AND NetworkScope != 'AllNetworks'];
        Map<String, Integer> Community_Likes = new Map<String, Integer>();
        Map<String, Integer> Community_Comments = new Map<String, Integer>();
        Map<String, Integer> Community_Posts = new Map<String, Integer>();
        for(FeedItem feed : feedItem) {
            if(Community_Likes.containsKey(feed.NetworkScope)) {
                // Calculating Number of Posts
                Integer posts = Community_Posts.get(feed.NetworkScope);
                Community_Posts.put(feed.NetworkScope, posts+1 );
                // Calculating Likes
                Integer likes = Community_Likes.get(feed.NetworkScope);
                likes = likes + feed.LikeCount;
                Community_Likes.put(feed.NetworkScope, likes);        
                // Calculating Comments
                Integer comments = Community_Comments.get(feed.NetworkScope);
                comments = comments + feed.CommentCount;
                Community_Comments.put(feed.NetworkScope, comments);
            } else {
                Community_Posts.put(feed.NetworkScope, 0);
                Community_Likes.put(feed.NetworkScope, 0);
                Community_Comments.put(feed.NetworkScope, 0);
            }
        }
        Map<String, String> networkMap = new Map<String, String>();
        networkMap.put(OCSUGC_Utilities.getNetworkId(Label.OCSDC_OncoPeer), 'OCSUGC');
        networkMap.put(OCSUGC_Utilities.getNetworkId(Label.OCSDC_DeveloperCloud), 'OCSDC');
        
        for(String str : networkMap.keySet()) {
            if(Community_Posts.containsKey(str)) {
                if(networkMap.get(str) == 'OCSUGC') {
                    CommunityMap.put('OCSUGC_POSTS', Community_Posts.get(str));
                } else if(networkMap.get(str) == 'OCSDC') {
                    CommunityMap.put('OCSDC_POSTS', Community_Posts.get(str));
                }
            }
            
            if(Community_Likes.containsKey(str)) {
                if(networkMap.get(str) == 'OCSUGC') {
                    CommunityMap.put('OCSUGC_LIKES', Community_Likes.get(str));
                } else if(networkMap.get(str) == 'OCSDC') {
                    CommunityMap.put('OCSDC_LIKES', Community_Likes.get(str));
                }
            }
            
            if(Community_Comments.containsKey(str)) {
                if(networkMap.get(str) == 'OCSUGC') {
                    CommunityMap.put('OCSUGC_COMMENTS', Community_Comments.get(str));
                } else if(networkMap.get(str) == 'OCSDC') {
                    CommunityMap.put('OCSDC_COMMENTS', Community_Comments.get(str));
                }
            }
        }
    }
    
    public void fetchTags() {
        
        Integer FGABTagCount = 0; 
        Integer OncoPeerTagCount = 0;
        Integer DeveloperTagCount = 0;
        for(OCSUGC_FeedItem_Tag__c fItemTags : [SELECT Id, OCSUGC_FeedItem_Id__c, OCSUGC_Tags__r.OCSDC_IsDeveloperCloudUsed__c, OCSUGC_Tags__r.OCSUGC_IsFGABUsed__c, 
                                                OCSUGC_Tags__r.OCSUGC_IsOncoPeerUsed__c, OCSUGC_Tags__r.OCSUGC_Tag__c FROM OCSUGC_FeedItem_Tag__c ]) {
            if(fItemTags.OCSUGC_Tags__r.OCSUGC_IsOncoPeerUsed__c == true)
                OncoPeerTagCount += 1;
            else if( fItemTags.OCSUGC_Tags__r.OCSDC_IsDeveloperCloudUsed__c == true)
                DeveloperTagCount += 1;
            else if( fItemTags.OCSUGC_Tags__r.OCSUGC_IsFGABUsed__c == true)
                FGABTagCount += 1;
        }
        communityMap.put('OCSUGC_Tags', OncoPeerTagCount);
        communityMap.put('OCSDC_Tags', DeveloperTagCount);
        communityMap.put('FGAB_Tags', FGABTagCount);
    }
    
}
/**
 *	08 Sept 2015		Puneet Mishra, 	Test Class for OCSUGC_CommunityStats
 */
@isTest//(SeeAllData=True)
private class OCSUGC_CommunityStatsTest {
	
		static CollaborationGroup fdabGrp;
    static FeedItem item,item1, item2;
    static OCSUGC_DiscussionDetail__c discussionDetail,discussionDetail1;
    static FeedComment comment;
    static CollaborationGroup cPublicGroup;
    static OCSUGC_FeedItem_Tag__c onco_contentTag,dev_contentTag, fgab_contentTag;
    static ConnectApi.FeedElement feedElementDiscussion;
    static ConnectApi.Comment feedComment;
    static Network OCSUGCNetwork;
    
    static {
       Profile portalProfile = OCSUGC_TestUtility.getPortalProfile();
       Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
       OCSUGCNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
       system.debug('Network ::: ' + OCSUGCNetwork.id);
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       Account accnt = OCSUGC_TestUtility.createAccount('test accfght 1',true);
       Contact con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);
       User usr =  OCSUGC_TestUtility.createPortalUser(true, portalProfile.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
       User adminUsr1 = OCSUGC_TestUtility.createStandardUser(true,adminProfile.Id,'12','1');
       System.runAs(adminUsr1) {
           PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, usr.Id, true);
       }
       cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',OCSUGCNetwork.Id,true);
       fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted',OCSUGCNetwork.Id, true);
       item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
       item1 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
       item2 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
       discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item.Id, true);
       discussionDetail1 = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item1.Id, true);
       OCSUGC_TestUtility.createBlackListWords('fuck');
       feedlike flike = new feedlike();
       flike.FeedItemId = item.id;
       flike.CreatedById = UserInfo.getUserId();
       insert flike;
       comment = OCSUGC_TestUtility.createFeedComment(item.id,false);
       insert comment;
       OCSUGC_Intranet_Content__c bookmark = new OCSUGC_Intranet_Content__c();
       bookmark.recordTypeID = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Bookmarks').getRecordTypeId();
       bookmark.OCSUGC_Bookmark_Id__c = item.Id;
       bookmark.OCSUGC_Bookmark_Type__c = 'Discussion';
       bookmark.OCSUGC_Posted_By__c = UserInfo.getUserId();
       insert bookmark;
       
       List<OCSUGC_Tags__c> tagList = new List<OCSUGC_Tags__c>();
       OCSUGC_Tags__c Oncopeer_tag = OCSUGC_TestUtility.createTags('testTag5');
       OncoPeer_tag.OCSUGC_IsOncoPeerUsed__c = true;
       OCSUGC_Tags__c Dev_tag = OCSUGC_TestUtility.createTags('devTag');
       Dev_tag.OCSDC_IsDeveloperCloudUsed__c = true;
       OCSUGC_Tags__c FGAB_tag = OCSUGC_TestUtility.createTags('fgabTag');
       FGAB_tag.OCSUGC_IsFGABUsed__c = true;
       tagList.add(Oncopeer_tag);
       tagList.add(Dev_tag);
       tagList.add(FGAB_tag);
       insert tagList;
       
       onco_contentTag = new OCSUGC_FeedItem_Tag__c();
       onco_contentTag.OCSUGC_FeedItem_Id__c = item.id;
       onco_contentTag.OCSUGC_Tags__c = Oncopeer_tag.id;
       insert onco_contentTag;
       
       dev_contentTag = new OCSUGC_FeedItem_Tag__c();
       dev_contentTag.OCSUGC_FeedItem_Id__c = item1.id;
       dev_contentTag.OCSUGC_Tags__c = Dev_tag.id;
       insert dev_contentTag;
       
       fgab_contentTag = new OCSUGC_FeedItem_Tag__c();
       fgab_contentTag.OCSUGC_FeedItem_Id__c = item1.id;
       fgab_contentTag.OCSUGC_Tags__c = FGAB_tag.id;
       insert fgab_contentTag;
       
		OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
		info.OCSUGC_Group_Id__c = cPublicGroup.Id;
		info.OCSUGC_Email__c = 'psardana@appirio.com';
		info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
		info.OCSUGC_Group_Type__c = 'Public';
		insert info;
       
        try{
        ConnectApi.FeedItemInput objFeedItemInput = new ConnectApi.FeedItemInput();

        ConnectApi.PollCapabilityInput pollChoices = new ConnectApi.PollCapabilityInput();
        ConnectApi.FeedElementCapabilitiesInput objCapabilities = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        textSegmentInput.Text = 'test discussion';
        messageBodyInput.messageSegments.add(textSegmentInput);
        objFeedItemInput.capabilities = objCapabilities;
        objFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        objFeedItemInput.subjectId = fdabGrp.Id;
        objFeedItemInput.body = messageBodyInput;
        feedElementDiscussion = ConnectApi.ChatterFeeds.postFeedElement(OCSUGCNetwork.Id, objFeedItemInput, null);
        //Insert feedComment
        feedComment = ConnectApi.ChatterFeeds.postCommentToFeedElement(OCSUGCNetwork.Id, feedElementDiscussion.Id, 'Test comment 1.');
        
        }catch(Exception ep){
            system.debug('Getting ex' + ep);
        }
    }
	
	public static testmethod void fetchTags_test() {
		Test.StartTest();
			OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
			control.fetchTags();
		Test.StopTest();
	}
	
	static testmethod void groupDetails_test() {
		Test.StartTest();
			OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
			control.groupDetails('public');
		Test.Stoptest();
	}
	
	static testMethod void feedTemDetails_test() {
		Test.StartTest();
			OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
			control.feedTemDetails();
		Test.StopTest();
	}
	
	/*static testMethod void loadOncoPeerDetails_Test() {
        OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
    	control.loadOncoPeerDetails();
    }
    
    static testMethod void loadDeveloperDetails_Test() {
    	OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
    	control.loadDeveloperDetails();
    }
    
    static testMethod void loadFGABDetails_Test() {
    	OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
    	control.loadFGABDetails();
    }
    
    static testMethod void feedTemDetails_Test() {
    	OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
    	control.feedTemDetails();
    }
    
    static testMethod void fetchTags_Test() {
    	OCSUGC_CommunityStats control = new OCSUGC_CommunityStats();
    	control.fetchTags();
    }*/

}
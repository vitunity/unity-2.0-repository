//
// (c) 2014 Appirio, Inc.
//
// Test Class for - OCSUGC_ComposeChatterMessageController
//
// 25th August 2014     Pawan Tyagi       Test Class for - OCSUGC_ComposeChatterMessageController
// 22 may 2014          Priyanka Kumar    Modified ( added test_GroupDetails )
// 17 July 2015         Shital Bhujbal    Updated code to avoid production issue "Too many rows returned 50001"

// ConnectApi methods are not supported in data siloed tests. So we are using @IsTest(SeeAllData=true).
@isTest(SeeAllData=true)
public class OCSUGC_ComposeChatterMessageConTest
{
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static Network ocsugcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert; 
   static List<CollaborationGroup> lstGroupInsert;
   static List<CollaborationGroupMemberRequest> lstGroupMemberRequest;
   static CollaborationGroupMemberRequest memberRequest1,memberRequest2;
   static OCSUGC_LikeUnlike__c managerLikeUnlike;
   static FeedLike feedLikeKA,feedLikeEvent;
   static String ocsugcNetworkString, ocsdcNetworkString;
   static OCSUGC_Knowledge_Exchange__c ka1,ka2,ka3,ka4;
   static List<OCSUGC_Knowledge_Exchange__c> lstKAInsert;    
   static List<OCSUGC_Users_Visibility__c> lstUserVisibility;
   static List<PermissionSetAssignment> permAssignments = new List<PermissionSetAssignment>(); 

    static {
        admin          = OCSUGC_TestUtility.getAdminProfile();
        portal         = OCSUGC_TestUtility.getPortalProfile(); 
        manager        = OCSUGC_TestUtility.getManagerProfile();
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        ocsugcNetworkString  = OCSUGC_TestUtility.getCurrentCommunityNetworkId(false);
        ocsdcNetworkString   = OCSUGC_TestUtility.getCurrentCommunityNetworkId(true); 
        lstUserInsert  = new List<User>();   
        lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
        lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
        //insert lstUserInsert;     
        account = OCSUGC_TestUtility.createAccount('tempAccFGR', true);
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert;
        ////Commented - 15 May 2016 start
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Private',ocsugcNetwork.id,false));

        //Comented- 15May 2016 End
        String subject,body,html;
        subject='You received a message!';
        body='MESSAGE_BODY';
        html='MESSAGE_BODY';
        PermissionSet managerPermissionSet = OCSUGC_TestUtility.getManagerPermissionSet();       
        
            lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, 'test', Label.OCSUGC_Varian_Employee_Community_Manager));
            lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
            lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
            lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
            lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
            insert lstUserInsert;
                 
         System.runAs(adminUsr2) {     
           
            PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, adminUsr1.Id, false);
            PermissionSet memberPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();       
            PermissionSetAssignment assignment1 = OCSUGC_TestUtility.createPermissionSetAssignment( memberPermissionSet.Id, adminUsr1.Id, false);
            permAssignments.add(assignment);
            permAssignments.add(assignment1);
            insert permAssignments; 
            // Inserting Test Collaboration Group Member Request.
            List<CollaborationGroup> testCollaborationGroups = new List<CollaborationGroup>();
            testCollaborationGroups.add(OCSUGC_TestUtility.createGroup('Testxxxxxreer32 Group 1', 'Public', ocsugcNetwork.id, false));
            testCollaborationGroups.add(OCSUGC_TestUtility.createGroup('Testxxxxx3243reer Group 2', 'Private', ocsugcNetwork.id, false));
            insert testCollaborationGroups;
            Blacklisted_Word__c blackListWord =  OCSUGC_TestUtility.createBlackListWords('Fuck');
            Group newGroup = OCSUGC_TestUtility.createGroup('testGroup',true);
            GroupMember grouMem = OCSUGC_TestUtility.createGroupMemberNC(newGroup.Id,adminUsr2.Id,true);
         }
   }   
    // checking sending and reply message fucntinality
    // argument none
    // @return none
    static testMethod void testComposeChatterMessage() 
    {
        //creating User
        
        List<User> lstUser = new List<user>();
        for(integer i = 0;i<2;i++)
        {
            User usr = new User();
            usr.Email = 'testwqwq'+ i +'@bechtel.com';
            usr.Username = 'testqwqw' + i + '@ocsugc.com.ocsugc';
            usr.LastName = 'testwqw'+ i;
            usr.Alias = 'avcd' + i;
            usr.ProfileId = admin.Id ;
            usr.LanguageLocaleKey = 'en_US';
            usr.LocaleSidKey = 'en_US';
            usr.TimeZoneSidKey = 'America/Chicago';
            usr.EmailEncodingKey = 'UTF-8';
            usr.isActive = true;
            lstUser.add(usr);
        }
        insert lstUser;
        System.debug('----lstUser------>' + lstUser); 
        Test.startTest(); 
              List<User> usrList = [Select Name from user where Id = :managerUsr.Id];
              
              System.runAs(adminUsr1) {
                  Group newGroup = OCSUGC_TestUtility.createGroup('testGroup',true);
                  GroupMember grouMem = OCSUGC_TestUtility.createGroupMemberNC(newGroup.Id,managerUsr.Id,true);
                  ApexPages.currentPage().getParameters().put('msgUserId', managerUsr.Id);
                  OCSUGC_ComposeChatterMessageController controller = new OCSUGC_ComposeChatterMessageController();
                  controller.newMessage();
                  OCSUGC_ComposeChatterMessageController.strSelectedUserName = managerUsr.Name;
                  OCSUGC_ComposeChatterMessageController.strMessage = 'ABC ';
                  controller.btnSave();
              }
        ApexPages.currentPage().getParameters().put('msgUserId', managerUsr.Id);
        ApexPages.currentPage().getParameters().put('userId', lstUser[1].Id);
        
        //send message by user 1 to user 2
        ConnectApi.ChatterMessages.sendMessage('Hii',lstUser[1].Id);
        OCSUGC_ComposeChatterMessageController controller;
        System.runAs(memberUsr) {
            OCSUGC_ComposeChatterMessageController.strMessage = 'Test Body';
            OCSUGC_ComposeChatterMessageController.strSendMessage = 'Reply message';
            controller = new OCSUGC_ComposeChatterMessageController();
            //System.assertEquals(null, controller.btnCancel());
            controller.newMessage();
            OCSUGC_ComposeChatterMessageController.strSelectedUserName = memberUsr.UserName;
            controller.btnSave();
            controller.replyMessage();
            controller.fetchInboxMessage();
            //OCSUGC_ComposeChatterMessageController.populateUserNameList('test','false');
        }
        Test.stopTest();
    }
    
    static testMethod void testPopulateUserNameList()  {
        Test.startTest();
            OCSUGC_ComposeChatterMessageController.populateUserNameList('test','false');
        Test.stopTest();  
    }
    
    
    static testMethod void testComposeChatterMessage2()  {
        //OCSUGC_ChatterPrivateMessageController controller = new OCSUGC_ChatterPrivateMessageController();
        String subject,body,html;
        subject='You received a message!';
        body='MESSAGE_BODY';
        html='MESSAGE_BODY';
        
        ConnectApi.ChatterMessages.sendMessage('Hii',managerUsr.Id);
        OCSUGC_ComposeChatterMessageController.strSelectedUserName = 'bob tester';
        OCSUGC_ComposeChatterMessageController.strMessage = 'Test Body';
        OCSUGC_ComposeChatterMessageController.strSendMessage = 'Reply message';
          
          ApexPages.currentPage().getParameters().put('msgUserId',managerUsr.Id);
          OCSUGC_ComposeChatterMessageController controller1 = new OCSUGC_ComposeChatterMessageController();
          Test.startTest();
          //System.assertEquals(null, controller1.btnCancel());
          controller1.newMessage();
          controller1.btnSave();
          controller1.replyMessage();
          controller1.fetchInboxMessage();
          //Changes done by Shital Bhujbal- calling controller and populateUserNameList() method in different context to avoid too many rows in SOQL queries 50001- 13/7/2015
          
          //OCSUGC_ComposeChatterMessageController.populateUserNameList('test','false');  
          System.runAs (adminUsr1){    
            EmailTemplate temp= OCSUGC_TestUtility.createEmailTemplate('OCSUGC_Email_Notification_for_Private_messages', 'OCSUGC_Email_Notification_for_Private_messages', subject, body, html,true);
              ConnectApi.ChatterMessages.sendMessage('Helo',contributorUsr.Id);
                OCSUGC_ComposeChatterMessageController controller2 = new OCSUGC_ComposeChatterMessageController();
                controller2.defaultUserId =  managerUsr.Id;
                OCSUGC_ComposeChatterMessageController.strSelectedUserName = managerUsr.Name;
                controller2.replyMessage();
          }
          Test.stopTest();
        }
    
    static testMethod void testnetworkMember(){
        Test.startTest();
            System.runAs(managerUsr) {
            List<NetworkMember> nm = [SELECT Id,MemberId 
                                                  FROM NetworkMember
                                                  WHERE MemberId !=: Userinfo.getUserId() 
                                                  And NetworkId =: ocsugcNetwork.Id
                                                  AND Member.isActive =: true Limit 1];
            User u = [Select Id,Name From User where Id = :nm[0].MemberId];
            system.debug('User selected -'+u + 'Name - '+u.Name);
            OCSUGC_ComposeChatterMessageController controller = new OCSUGC_ComposeChatterMessageController();
            OCSUGC_ComposeChatterMessageController.strMessage = 'Test Message';
            
            controller.newMessage();
            OCSUGC_ComposeChatterMessageController.strSelectedUserName = u.Name;
            controller.btnSave();
          }
        Test.stopTest(); 
    }
    
    public static testmethod void test_GroupDetails() {  
      Test.startTest();
          System.runAs(managerUsr) {
          List<NetworkMember> lstNetWorkMember = [SELECT Id,MemberId 
                                                  FROM NetworkMember
                                                  WHERE MemberId = :managerUsr.Id];
           System.debug('Indiana '+lstNetworkMember.size());
           PageReference pageRef = Page.OCSUGC_DiscussionDetail;
           pageRef.getParameters().put('g',privateGroup.Id);
           pageRef.getParameters().put('fgab','false');
           pageRef.getParameters().put('dc','false');      
           Test.setCurrentPage(pageRef);
           List<Id> newList = new List<Id>();
           List<NetworkMember> nm = [SELECT Id,MemberId 
                                                  FROM NetworkMember
                                                  WHERE MemberId !=: Userinfo.getUserId() 
                                                  And NetworkId =: ocsugcNetwork.Id
                                                  AND Member.isActive =: true Limit 1
                                                  ];
           User u = [Select Id,Name From User where Id = :nm[0].MemberId];
           system.debug('User selected -'+u + 'Name - '+u.Name);
           OCSUGC_ComposeChatterMessageController controller = new OCSUGC_ComposeChatterMessageController();
           OCSUGC_ComposeChatterMessageController.strMessage = 'Test Message';
           OCSUGC_ComposeChatterMessageController.strSelectedUserName = u.Name;
           controller.btnSave();
         } 
    Test.stopTest();    
  }
}
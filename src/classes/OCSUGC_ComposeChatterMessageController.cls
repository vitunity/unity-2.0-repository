//
// (c) 2014 Appirio, Inc.
//
// This page will allow the user to Contact the OCSUGC Admins public group from a link on the Home Page.
//
// 20 Aug 2014  Jai Shankar Pandey Created the controller to provide a functionality Of inbox for messages
//and sending and Replying messages
// 29-09-2014 Puneet Sardana Created a method fetchInboxMessage for fetching existing inbox message
// 30-09-2014 Puneet Sardana SendDateTime Changed to show relative time Ref T-323266
// 21-04-2014 Shital Bhujbal (@Varian.com) changed method to increase the scope of autopopuation to community manager and contributor roles Ref T-379547
// 14-07-2015 Shital Bhujbal -All queries on NetwrokMember object are changed to get active NetworkMembers only
// 04-09-2015 Puneet Mishra [ONCO-373] code change in sendEmailToReceipent, setting Display Name and Replyto email address.
// 10-12-2015 Puneet Mishra [ONCO-449] Introduced Comparable Interface for sorting the Messages 
public without sharing class OCSUGC_ComposeChatterMessageController extends OCSUGC_Base_NG {
 
    public PageReference btnCancel() {  
        return null;
    }
    
    private Id orgWideEmailAddressId;
    public static String strMessage                     { get; set; }                       //Body of chatter Message
    public static String strSelectedUserName            { get; set; }                       //User whom private message is to be sent.
    public static String strRecipientName               { get; set; }                       //Name of the recipient
    public static String strRecipientName1              { get; set; }                       //First Name plus Last Name of recipient
    public static String strSendMessage                 { get; set; }                       //Message sent to User
    public static String strConversationId              { get; set; }                       //Id of the whole conversation
    public string strdefaultUserName                    { get; set; }                       //User Name fetched from userId fetched from Url
    public Id defaultUserId                             { get; set; }                       //User Id fetched from Url
    public Boolean isUserMessageDetailClicked           { get; set; }                       //Check if message is sent successfully.
    public Boolean isTypeHead                           { get; set; }                       //Check if Recipient user name is selected from auto-complete text box else false
    public  list<MessagesWrapper> lstMessageWrapper     { get; set; }                       //List of Wrapper class to show Inbox Content
    public  list<MessagesWrapper> lstSingleMessage      { get; set; }                       //List of coversation messages belong to some user
    //Updated by Shital Bhujbal 22 july 2015, made mapUserIdToName and mapUserNameToId static as it increasing the View State of page and genrating exception on page, used only on controller.
    private static map<Id,String> mapUserIdToName;                                          //Map to store users Id against its Name
    private static map<String,String> mapUserNameToId;                                      //Map to store users Name against its Id
    private map<String, List<MessagesWrapper>> mapConversationIdToListMessage;              //Map to store list of messages belongs to a particular conversation
    private string strConversationIdPrivate;                                                //Id of Private Conversation
    private string strReplyMessageId;                                                       //Id of reply message
    private string RecipientId;                                                             //Id of user to whom message is sent.
    private string strselectedUserId;                                                       //Id of user selected from Auto-complete
    private  string communityId ;                                                           //Community Id
    private static final String OCSUGC_PRIVATE_MESSAGE = 'OCSUGC_Email_Notification_for_Private_messages';
    public String errorMsg{get; set;}
    public Boolean canReply { get; set;}
    public static String strConversingUserName {get; set;} //name of user with whom logged in user is conversing
    //Added by Shital to remove >50000 rows return error in production
    private static Map<Id, String> mapMemberNames;
    
    //ONCO-421, Setting page-title
    public static String pageTitle{get;set;}

    //  Message Wrapper Class
    //  argument none
    //  @return none
    //  30-09-2014 Puneet Sardana SendDateTime Changed to show relative time Ref T-323266
    //  @Modified by    Puneet Mishra   ONCO-449, to sort the message List, wrapper Class must implements Comparable Interface
    public class MessagesWrapper implements Comparable {

        public  string Firstname       { get;set; }
        public  string Lastname        { get;set; }
        public  string ShortMessage    { get;set; }
        public  string FullMessage     { get;set; }
        public  string Photo           { get;set; }
        public  String SendDateTime    { get;set; }
        public String Id               { get;set; }
        public String MessageId        { get;set; }
        public Boolean isReceived      { get;set; }
        public string strBgColor       { get;set; }
        public String struserId        { get;set; }
        public Datetime strSendDate    { get;set; }
        public Boolean isRead          { get;set; }
        public DateTime sDT            { get;set; }
        
        public MessagesWrapper(string Firstname, string Lastname, string ShortMessage, string FullMessage, string Photo, DateTime SendDateTime, String Id, String MessageId, Boolean isReceived) {

            this.Firstname =Firstname;
            this.Lastname  =Lastname;
            this.ShortMessage = ShortMessage;
            this.FullMessage = FullMessage;
            this.Photo = Photo;
            this.SendDateTime = OCSUGC_Utilities.GetRelativeTimeString(DateTime.valueOf(SendDateTime),DateTime.now());
            this.Id = Id;
            this.MessageId = MessageId;
            this.isReceived = isReceived;
            this.sDT = SendDateTime;
        }
        
        // ONCO-449,    Puneet Mishra       return value 0 indicate equallity, 1 indicate greater than, -1 indicate smaller than
        public Integer compareTo(Object msgObj) {
             MessagesWrapper msgWrap = (MessagesWrapper)msgObj;
             
             if(this.sDT > msgWrap.sDT) {
                return 1;
             } else if (this.sDT < msgWrap.sDT) {
                return -1;
             }
             return 0;
        }
        
    }

    //  Constructor
    public OCSUGC_ComposeChatterMessageController()
    {   //ONCO-421, Unique Page-Title
        if(isDC) {
            pageTitle = 'Message | Developer';
        } else {
            pageTitle = 'Message | OncoPeer';
        }
        errorMsg = '';
        isUserMessageDetailClicked = false;
        defaultUserId = null;
        RecipientId = strRecipientName1 = strselectedUserId  = strdefaultUserName = '';
        isTypeHead = true;
        mapConversationIdToListMessage = new Map<String, List<MessagesWrapper>>();
        mapUserIdToName =  new map<Id,String>();
        mapMemberNames = new map<Id,String>();
        lstSingleMessage = new List<MessagesWrapper>();
        lstMessageWrapper = new List<MessagesWrapper>();
        Set<String> communityUserIds = new Set<String>();
        
        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        //retrieving all user belongs to logged-in community
        //Changes done by Shital Bhujbal to avoid >50000 query rows in production - 21 July
        String userId = Userinfo.getUserId(); 
        String soqlQuery = 'Select MemberId,Member.Name From NetworkMember Where MemberId !=: userId '  +
                            ' And NetworkId =: currentNetworkId AND Member.isActive=true';
        
        if(Test.isRunningTest())
            soqlQuery += ' LIMIT 10 ';
        //retrieving all user belongs to logged-in community
        /*for(NetworkMember member :[Select MemberId,Member.Name
                        From NetworkMember
                        Where MemberId !=: Userinfo.getUserId()
                        And NetworkId =: currentNetworkId AND Member.isActive=true]) {*/
        for(NetworkMember member : Database.Query(soqlQuery)) {
              communityUserIds.add(member.MemberId);
              mapMemberNames.put(member.MemberId,member.Member.Name);     
        }
        
        strselectedUserId = ApexPages.currentPage().getParameters().get('msgUserId');
        
        // @Added by Puneet Mishra 21 Aug 2015, 
        // [ONCO-361] adding this code snippet so user can read messages send from any community, This snippet is to make sure that User can read message from any community
        if(!mapMemberNames.containsKey(strselectedUserId) && strselectedUserId != null) {
            NetworkMember addMember = [SELECT MemberId, Member.Name FROM NetworkMember WHERE MemberId =: strselectedUserId Limit 1];
            mapMemberNames.put(addMember.MemberId, addMember.Member.Name);
            communityUserIds.add(addMember.MemberId);
        }
        
        canReply = communityUserIds.contains(strselectedUserId);
        defaultUserId = ApexPages.currentPage().getParameters().get('userId');
              
        if(Test.isRunningTest()){
            communityUserIds.add(defaultUserId);
            communityUserIds.add(UserInfo.getUserId());
           
        }
         communityUserIds.add(strselectedUserId);
         
        //Retrieving name of the community user
        //Changes done by Shital Bhujbal to avoid >50000 query rows in production - 21 July
        for(Id usrId : mapMemberNames.keyset())
        {
            String userName = mapMemberNames.get(usrId);
            mapUserIdToName.put(usrId,userName);  
            
            system.debug('********shital********strselectedUserId****'+strselectedUserId+'*******usrId*****'+usrId);          
            if(strselectedUserId != Null && strselectedUserId != '' && usrId == strselectedUserId)
            {
                system.debug('********shital********inside if***usrId == strselectedUserId***');
                strRecipientName = userName;
                strConversationIdPrivate ='New';
                
                if(canReply) {                    
                    canReply = canMessage;
                }
            }
        }
        
        if(defaultUserId != null)
        {
            isTypeHead = false;
            strdefaultUserName = mapUserIdToName.get(defaultUserId);
        }
        getMessageInInbox();

    }
    // @description: Fetch details of existing inbox message. Call on page load
    // 29-09-2014 Puneet Sardana Function to fetch existing inbox message is created Ref T-319454
    public void fetchInboxMessage() {
        if(strselectedUserId != Null && strselectedUserId != '')
          fetchSelectedInboxMessage();
    }


    //  Send the private message to the user.
    public pagereference btnSave() {

        //Changes done by Shital Bhujbal to avoid view state error in production - 21 July
        //fetch the recipient Id to send the msg
        //RecipientId = (mapUserNameToId.containsKey(strSelectedUserName)) ? mapUserNameToId.get(strSelectedUserName) : null;
        List<Networkmember> recipientMember = [Select MemberId,Member.Name
                                               From NetworkMember
                                               Where Member.Name =:strSelectedUserName
                                               And NetworkId =: currentNetworkId AND Member.isActive=true LIMIT 1];
        RecipientId = (recipientMember.size()>0) ? recipientMember[0].MemberId : null;
                           
        System.debug('>>> RecipientId = in button save'+ RecipientId);
        //Check if user name is valid and then send email,
        //Commented by Pratz - 02/17/2015 - To not include this fix 
        if(RecipientId != null &&  RecipientId != '' && strMessage.trim() != null && strMessage.trim() != '')
        {
            ConnectApi.ChatterMessages.sendMessage(strMessage,RecipientId);
             //18-02-2015  Puneet Sardana  Added following line Ref I-148807
            insertMessageNotification(RecipientId);
            isUserMessageDetailClicked = false;
            sendEmailToReceipent(RecipientId,strMessage);
            strRecipientName = strSelectedUserName;
            strConversationIdPrivate = 'New';
    
            //redirect to Message detail page
            Pagereference objPageReference = Page.OCSUGC_MessageDetailView;
            objPageReference.getParameters().put('msgUserId',RecipientId);
            objPageReference.setRedirect(true);
            return objPageReference;
            
        //Commented by Pratz - 02/17/2015 - To not include this fix     
        }else{                     
                errorMsg = Label.OCSUGC_Compose_Chatter_Message_Error;
                return null;
        }
        
    }
    // @description: This methods show notification when message is sent
    //18-02-2015  Puneet Sardana  Added function to show notification when message is sent Ref I-148807
  private void insertMessageNotification(String recipientId) {
      OCSUGC_Intranet_Notification__c tempObj = new OCSUGC_Intranet_Notification__c();
      tempObj.OCSUGC_Description__c = UserInfo.getName() +' has sent you a private message.';
      tempObj.OCSUGC_Is_Viewed__c = false;
      tempObj.OCSUGC_Link_to__c = 'OCSUGC_MessageDetailView?msgUserId='+UserInfo.getUserId();
      tempObj.OCSUGC_Related_User__c =  UserInfo.getUserId();
      tempObj.OCSUGC_User__c = recipientId;
      try {
        insert tempObj;
      }
      catch(Exception ex) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
      }
  }
    //  populating Inbox with messages
    //  argument none
    //  @return none
    //  30-09-2014 Puneet Sardana SendDateTime Changed to show relative time Ref T-323266
    private void getMessageInInbox()
    {
    try{
            List<MessagesWrapper> lstMessageWrapperTemp = new List<MessagesWrapper>();
            set<String> setConversationId = new set<String>();

            mapConversationIdToListMessage = new Map<String, List<MessagesWrapper>>();
            lstMessageWrapper = new List<MessagesWrapper>();
            //getting community Id
            communityId = currentNetworkId;
            System.debug('communityId getMessageInInbox compose chatter message '+communityId);
            ConnectApi.ChatterMessagePage objChatterMessagePage = ConnectApi.ChatterMessages.getMessages(communityId);
            Map<string, List<MessagesWrapper>> mapMessaging = new Map<string, List<MessagesWrapper>>();
            set<String> setSendDates = new Set<String>();

            //Iterating over list of ConnectApi.ChatterMessagePage...
            for(ConnectApi.ChatterMessage objChatterMessage : objChatterMessagePage.messages)
            {
                System.debug('objChatterMessage====='+objChatterMessage);
                DateTime sendDateTime;
                //getting firstName of Message sender
                String Firstname = objChatterMessage.sender.firstName;
                //getting lastName of Message sender
                String Lastname =  objChatterMessage.sender.lastName;
                String ShortMessage = '';
                String Photo = objChatterMessage.sender.photo.smallPhotoUrl;
                String FullMessage = objChatterMessage.body.text;
                String Id = objChatterMessage.Id;
                if(objChatterMessage.body.text != null && objChatterMessage.body.text.trim() != '')
                {
                    if(objChatterMessage.body.text.length() > 20)
                    ShortMessage = objChatterMessage.body.text.substring(0,20) + '...';
                    else
                    ShortMessage = objChatterMessage.body.text;
                }
                //getting send Date Time
                sendDateTime = objChatterMessage.sentDate;
                //put the value into the wrapper class
                MessagesWrapper objMessagesWrapper = new MessagesWrapper(Firstname, Lastname, ShortMessage, FullMessage, Photo, sendDateTime,objChatterMessage.conversationId,Id,false);
                System.debug('Before ConnectApi.ChatterConversation');
                ConnectApi.ChatterConversation obj = ConnectApi.ChatterMessages.getConversation(currentNetworkId, objChatterMessage.conversationId);
                System.debug('A ConnectApi.ChatterConversation');
                System.debug('ConnectApi.ChatterConversation======='+obj.read);
                objMessagesWrapper.isRead = obj.read;
                objMessagesWrapper.strBgColor = 'message-overview';
                objMessagesWrapper.strSendDate = sendDateTime;
                for(ConnectApi.UserSummary usr : objChatterMessage.recipients)
                {
                    if(objChatterMessage.sender.Id == userinfo.getUserId() && objChatterMessage.sender.Id != usr.Id)
                    {
                        objMessagesWrapper.isReceived = false;
                        objMessagesWrapper.Firstname = usr.firstName;
                        objMessagesWrapper.Lastname = usr.lastName;
                        objMessagesWrapper.Photo = usr.photo.smallPhotoUrl;
                    }
                    else if(objChatterMessage.sender.Id != userinfo.getUserId() && objChatterMessage.sender.Id == usr.Id)
                    {
                        objMessagesWrapper.isReceived = true;
                        objMessagesWrapper.Firstname = usr.firstName;
                        objMessagesWrapper.Lastname = usr.lastName;
                        objMessagesWrapper.Photo = usr.photo.smallPhotoUrl;
                    }
                    if(userinfo.getUserId() != usr.Id)
                      objMessagesWrapper.struserId = usr.Id;
                }
               // 02-07-2015  Rishabh Verma(@Varian.com) added if condition to check for null or blank firstName.
                String strRecipient ='';
                if(objMessagesWrapper.Firstname == null || objMessagesWrapper.Firstname == ''){
                     strRecipient = objMessagesWrapper.Lastname;    
                }else{
                     strRecipient = objMessagesWrapper.Firstname + ' ' + objMessagesWrapper.Lastname;    
                }
                
                system.debug('****shital*****strRecipient**********'+strRecipient+'*********strConversationIdPrivate********'+strConversationIdPrivate);
                //String strRecipient = objMessagesWrapper.Firstname + ' ' + objMessagesWrapper.Lastname;
                if(strRecipientName == strRecipient && strConversationIdPrivate =='New'){
                    strConversationIdPrivate = strConversationId = objChatterMessage.conversationId;
                    System.debug('objChatterMessage.conversationId==='+objChatterMessage.conversationId);
                }

                if(!setConversationId.Contains(objChatterMessage.conversationId) && objChatterMessage.sender.Id != userinfo.getUserId())
                {

                    if(!setConversationId.Contains(objChatterMessage.conversationId))
                    {
                        if(strConversationIdPrivate == objChatterMessage.conversationId) {
                            objMessagesWrapper.strBgColor = 'message-overview active';
                            strConversingUserName = objMessagesWrapper.Firstname + ' ' + objMessagesWrapper.Lastname;
                        }

                        setConversationId.add(objChatterMessage.conversationId);
                    }
                }

                //keyset => Conversation Id and values must be the all message
                if(!mapConversationIdToListMessage.containsKey(objMessagesWrapper.Id))
                    mapConversationIdToListMessage.put(objMessagesWrapper.Id, new List<MessagesWrapper> {objMessagesWrapper});
                else
                    mapConversationIdToListMessage.get(objMessagesWrapper.Id).add(objMessagesWrapper);
            }
            //**************************Sorting logic***********************************************
            map<string, List<String>> mapSendDate = new map<string, List<String>>();
            for(String strConversationMsgId : mapConversationIdToListMessage.Keyset())
            {
                for(MessagesWrapper objWrapper : mapConversationIdToListMessage.get(strConversationMsgId))
                {
                    //lstMessageWrapper.add(objWrapper);
                    //settinf the bgcolor of the selected items
                    if(strConversationIdPrivate == objWrapper.Id) {
                        objWrapper.strBgColor = 'message-overview active';
                        strConversingUserName = objWrapper.Firstname + ' ' + objWrapper.Lastname;
                    }

                    String strSendDateTimeMsg = String.valueOf(getGMT(objWrapper.strSendDate));
                    String strSendDateConversationId = strSendDateTimeMsg + '_' + objWrapper.Id;

                    if(!mapMessaging.ContainsKey(strSendDateConversationId))
                      mapMessaging.put(strSendDateConversationId, new List<MessagesWrapper>{objWrapper});
                    else
                      mapMessaging.get(strSendDateConversationId).add(objWrapper);

                    setSendDates.add(strSendDateTimeMsg);

                    if(!mapSendDate.ContainsKey(strSendDateTimeMsg))
                        mapSendDate.put(strSendDateTimeMsg, new List<String>{objWrapper.Id});
                    else
                        mapSendDate.get(strSendDateTimeMsg).add(objWrapper.Id);
                    break;
                }
            }
            List<string> lstDates= new List<String>();
            lstDates.addall(setSendDates);
            //sorting
            lstDates.sort();
            integer intListSize = lstDates.Size();
            for(integer i = intListSize; i > 0; i--)
            {
                String strDateTimeSendMsg = string.valueOf(lstDates[i-1]);
                if(mapSendDate.containsKey(strDateTimeSendMsg))
                {
                    for(String objConversationId : mapSendDate.get(strDateTimeSendMsg))
                    {
                        String strSendDateConversationId = strDateTimeSendMsg + '_' + objConversationId;
                      if(mapMessaging.ContainsKey(strSendDateConversationId))
                      {
                        lstMessageWrapper.addall(mapMessaging.get(strSendDateConversationId));
                      }
                    }
                }
            }
            //*************************************************************************
        }
        catch(Exception ex)
        {
            system.debug('======ex=======>>>>'+ex+'==========='+ex.getlinenumber());
        }
    }
    public Datetime getGMT(Datetime activityDate) {

        Date d = activityDate.dateGmt();
        Time t = activityDate.timeGmt();
        return Datetime.newInstance(d,t);
    }
    //  retrieves the list of User Names to populate in autocomplete search for user to select.
    //  argument string to search
    //  @return List of User Names to populate in auto-complete
    @RemoteAction
    public static List<String> populateUserNameList(String strSearchUser,String isDCParam)
    {
        //community user Ids
        Set<String> communityUserIds = new Set<String>();
        //lists of user names
        List<String> lstUserNames = new List<String>();
        //Get the current community id.
        String strUserNetworkId = OCSUGC_Utilities.getCurrentCommunityNetworkId(OCSUGC_Constants.CONST_TRUE.equalsIgnoreCase(isDCParam));
        system.debug('*****NetworkId'+strUserNetworkId);
        //get current logged-in user Id.
        String struserId = Userinfo.getUserId();
        
        //getting user Network Id with Name
        //Code commented by Shital Bhujbal(@varian) because now the autopopulation of user
        //in message functionality will work on network ID and not on network name.
        
        /*Network net = new Network();
        if(strUserNetworkId != null && strUserNetworkId != '')
          net = [SELECT Name,Id FROM Network WHERE Id =: strUserNetworkId LIMIT 1 ];
        
        //checking user belongs to OCSUGC or not(CAKE user or not)
        if(net != null && net.Name == Label.OCSUGC_CommunityName)
            strSpherosNetworkId = net.Id;
        */
        
        //getting user who is 'OCSUGC Approved' and 'Active' and is in 'OCSUGC/OCSD' Community
        for(NetworkMember member :[Select MemberId
                        From NetworkMember
                        Where MemberId !=: struserId And NetworkId =: strUserNetworkId AND Member.isActive=true]) {
            communityUserIds.add(member.MemberId);
        }
        
        //checking searching text is having value or not
        //Code added by Shital Bhujbal @Varian to get community managers and contributors in auto-population list
        
        if(strSearchUser != Null && strSearchUser != ''){
            
            String strSearchText = '\'%'  + String.escapeSingleQuotes(strSearchUser.trim()) + '%\'';
            //String strSearchQuery = 'SELECT Id, Name FROM User WHERE (Id IN :communityUserIds AND Name like  ' + strSearchText + ' ) AND (IsActive = true) AND OCSUGC_Accepted_Terms_of_Use__c = true LIMIT 50';
            //Changed query to add managers and contributors in auto-population list - Shital Bhujbal ---30/6/2015
            String strSearchQuery = 'SELECT Id, Name,OCSUGC_User_Role__c FROM User WHERE (Id IN :communityUserIds AND Name like  ' + strSearchText + ' ) AND (IsActive = true) LIMIT 50';
            //getting user's name related  to search
             
             for(User objUser : Database.Query(strSearchQuery))
             {
               if(objUser.OCSUGC_User_Role__c!= NULL &&!OCSUGC_Utilities.isReadonly(objUser.OCSUGC_User_Role__c))
                    lstUserNames.add(objUser.Name);
             }
        }
        System.debug('>>> lstUserNames = '+ lstUserNames);
        return lstUserNames;
    }

    //  retreiving message from inbox when cliking on message in inbox
    //  argument none
    //  @return none
    public void fetchSelectedInboxMessage()
    {
        strSendMessage ='';
        lstSingleMessage = new List<MessagesWrapper>();
        system.debug(' strConversationId ' + strConversationId);
        system.debug('======mapConversationIdToListMessage.get(strConversationId)==========???>>>'+mapConversationIdToListMessage.get(strConversationId));
        if(mapConversationIdToListMessage.containsKey(strConversationId))
            lstSingleMessage.addAll(mapConversationIdToListMessage.get(strConversationId));
        for(MessagesWrapper objWrapper : lstSingleMessage)
        {
            strReplyMessageId = objWrapper.MessageId;
            objWrapper.isRead = true; // Message is read
            break;
        }


        strConversationIdPrivate =strConversationId;
        strRecipientName1 = userInfo.getFirstName() + ' ' + userInfo.getLastName();

        System.debug('strConversationId ComposeChatterMessageController===' +strConversationId);
        if(lstSingleMessage.size() > 0)
            updateMaleNotification(lstSingleMessage[0]);
        
        // ONCO-449, Sorting the message list as Comparable Interface is Implemented
        lstSingleMessage.sort();
        
        //make conversation as read
        if(!Test.isRunningTest())
          ConnectApi.ChatterMessages.markConversationRead(currentNetworkId, strConversationId, true);
    }
    
    // ONCO-368, created by Puneet Mishra, method to update the message notification using the userId and mapping the Notification Object Related_User_Id as there is no relation between the
    // two objects, This method will set the messages status as viewed on Click.
    private void updateMaleNotification(MessagesWrapper wrapper) {
        String senderId = wrapper.struserId;
        List<OCSUGC_Intranet_Notification__c> notificationList = new List<OCSUGC_Intranet_Notification__c>();
        notificationList = [SELECT Id,  OCSUGC_Is_Viewed__c, OCSUGC_Related_User__c, OCSUGC_User__c FROM OCSUGC_Intranet_Notification__c WHERE OCSUGC_Related_User__c =: senderId
                            AND OCSUGC_Is_Viewed__c =: false];
        if(notificationList.size() > 0) {
            for(OCSUGC_Intranet_Notification__c notify : notificationList)
                notify.OCSUGC_Is_Viewed__c = true;
            
            update notificationList;
        }
        
        system.debug(' **************** notificationList **************** ' + notificationList);
    }



    //  retreiving message from inbox when cliking on message in inbox
    //  argument none
    //  @return none
    public void replyMessage()
    {
        strConversationId = strConversationIdPrivate;
        if(!Test.isRunningTest()){
            ConnectApi.ChatterMessages.replyToMessage(strSendMessage, strReplyMessageId);
        }

        RecipientId = [SELECT Id, Name FROM User WHERE Id =: strselectedUserId].Id;
        system.debug('RecipientId*********'+RecipientId);
       
        //Check if user name is valid and then send email,
        if(RecipientId != null &&  RecipientId != '' && strSendMessage.trim() != null && strSendMessage.trim() != '') {
            // updated by Rishabh Verma 30-06-2015 2015, added insertMessageNotification Alert on OncoPeer Home page
            insertMessageNotification(RecipientId);
            isUserMessageDetailClicked = false;
            sendEmailToReceipent(RecipientId,strSendMessage);
        }

        getMessageInInbox();
        fetchSelectedInboxMessage();
    }

    //  sending an email to receipent
    //  argument Recipient Id,sender/Receiver message
    //  @return none
    private void sendEmailToReceipent(String RecipientId,String strMessage1) {
        for(User recipient :[Select Id From User
                             Where Id =:RecipientId
                             And Contact.OCSUGC_Notif_Pref_PrivateMessage__c = true]) {
            List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

            //Query and fetch the Email Template Format for sending Email to User when he/she recieved a private message
            EmailTemplate objEmailTemplate = new EmailTemplate();

            //retrieving email Template
            objEmailTemplate = [SELECT Id,
                                       Subject,
                                       HtmlValue,
                                       Body
                                       FROM EmailTemplate
                                       WHERE DeveloperName = :OCSUGC_PRIVATE_MESSAGE
                                       LIMIT 1];

            String strHtmlBody  = objEmailTemplate.HtmlValue;
            String strPlainBody = objEmailTemplate.Body;
            String strSubject   = objEmailTemplate.Subject;
            
            // Start Added Label Value Naresh T-363912
            strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
            strSubject  = strSubject.replace('CONTACT_NAME',Userinfo.getName());
            // End
            strHtmlBody = strHtmlBody.replace('MESSAGE_BODY', strMessage1);
            strPlainBody = strPlainBody.replace('MESSAGE_BODY', strMessage1);
            
            /* adding a link to Email to redirect user to OncoPeer Messages. No need to add this, it can be removed.
            strHtmlBody += 'For More Information go to : <a href=https://ocsd-varian.cs7.force.com/OCSUGC/OCSUGC_MessageDetailView?msgUserId='+userinfo.getUserId()+'>Click Here.</a>';
            strPlainBody += 'For More Information go to : <a href=https://ocsd-varian.cs7.force.com/OCSUGC/OCSUGC_MessageDetailView?msgUserId='+userinfo.getUserId()+'>Click Here.</a>';
            */
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSaveAsActivity(false);
            //sender display name
            //
            // Use Organization Wide Address
           /* [ONCO-373] commenting below code as Display Name and email field should be of logged in User
           if(orgWideEmailAddressId != null)
                email.setOrgWideEmailAddressId(orgWideEmailAddressId);
            else
              email.setSenderDisplayName('OCSUGC');*/
            email.setSenderDisplayName(userinfo.getUserName());
            email.setInReplyTo(userinfo.getUserEmail());  
              
            //subject of email
            email.setSubject(strSubject);
            email.setHtmlBody(strHtmlBody);
            email.setPlainTextBody(strPlainBody);
            email.setTargetObjectId(recipient.Id);
            
            lstEmail.add(email);

            try {
                //Sending Email here
                if(!lstEmail.IsEmpty()) {
                    system.debug(' ======================================================= ' + lstEmail);
                    Messaging.SendEmailResult[] result = Messaging.sendEmail(lstEmail);
                //  system.assertEquals(false, result[0].success); 
                }
            } catch(Exception ex) {
                system.debug('========>>>'+ex.getMessage());
            }
        }
    }

    //  retreiving message from inbox when cliking on message in inbox
    //  argument none
    //  @return none
    
    public void newMessage() {

        Set<String> communityUserIds = new Set<String>();

        mapUserNameToId = new map<String,String>();
        String userId = Userinfo.getUserId(); 
        String soqlQuery = 'Select MemberId,Member.Name From NetworkMember Where MemberId !=: userId '  +
                            ' And NetworkId =: currentNetworkId AND Member.isActive=true';
        
        if(Test.isRunningTest())
            soqlQuery += ' LIMIT 10 ';
        //retrieving all user belongs to logged-in community
        /*for(NetworkMember member :[Select MemberId,Member.Name
                        From NetworkMember
                        Where MemberId !=: Userinfo.getUserId()
                        And NetworkId =: currentNetworkId AND Member.isActive=true]) {*/
        for(NetworkMember member : Database.Query(soqlQuery)) {
              communityUserIds.add(member.MemberId);
              mapUserNameToId.put(member.Member.Name,member.MemberId);
        }       
    }   
}
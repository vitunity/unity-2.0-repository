/*
Name        : OCSDC_AdminCompCon 
Updated By  : Rishi Khirbat (Appirio India)
Date        : 25th April, 2014
Purpose     : An Apex Class for Confirmation Page.

17-Apr-2015        Naresh K Shiwani 	Made changed related to Ref T-379076
22-Dec-2015		   Puneet Mishra		ONCO-446, Deleting Notification record when user removed from FGAB as user can view Notification to join FGAB even user no longer
										a member of group
08-Feb-2015		   Shital Bhujbal       Added deleteComment() method so that moderator can delete any comment on post - Task - 450

*/
public class OCSUGC_ConfirmationController extends OCSUGC_Base {
    public Boolean isError {get; set;}
    public Boolean isAlreadyDone {get; set;}
    public String contType {get; set;}
    public String contId {get; set;}
    public String supportContId {get; set;}
    public String errorMessage {get; set;}
		public String strTerms = '';
		public String deactivatingReason {get; set;}
		public Boolean isLeaveGroup {get;set;}
		public Boolean isDC {get;set;}

    public OCSUGC_ConfirmationController() {
        errorMessage = '';
        isError = false;
        isDC = false;
        isDC = ApexPages.currentPage().getParameters().get('dc')!=null && ApexPages.currentPage().getParameters().get('dc').equals('true');
    }

    public string headerMessage {
        get {
            String msg = '';
            if(contType == 'DeleteGroup') {
                msg = 'Delete Group';
            }
            else if(contType == 'LeaveGroup') {
            	isLeaveGroup = true;
                msg = 'Leave Group';
            }
            else if(contType == 'DeactivateAccess') {
                msg = 'Deactivate Access';
            }
            else if(contType == 'RemoveGroupMember') {
                msg = 'Remove Group Member';
            }
            else if(contType == 'DeleteDiscussion') {
                msg = 'Delete Discussion';
            }
            else if(contType == 'DeleteKnowledge') {
                msg = 'Delete Knowledge';
            }
            else if(contType == 'DeleteEvent') {
                msg = 'Delete Event';
            }
            else if(contType == 'ShowTnC') {
                msg = 'Terms & Conditions';
            }
            else if(contType ==  'DeactivateGroup') {
            	msg = 'Deactivate Group';
            }
            else if(contType == 'DeletePoll') {
                msg = 'Delete Poll';
            }
            else if(contType == 'DeleteDiscussionComment' || contType == 'DeletePollComment' || contType == 'DeleteEventComment' || contType == 'DeleteKnowledgeArtifactComment') {
                msg = 'Delete Comment';
            }
            return msg;
        }
    }

    public string successMsg {
        get {
            String msg = '';
            if(contType == 'DeleteGroup') {
                msg = Label.OCSUGC_Confirm_Delete_Group_Success;
            }
            else if(contType == 'LeaveGroup') {
                msg = Label.OCSUGC_Confirm_Leave_Group_Success;
            }
            else if(contType == 'DeactivateAccess') {
                msg = Label.OCSUGC_Confirm_Leave_Group_Success;
            }
            else if(contType == 'DeactivateGroup') {
                msg = Label.OCSUGC_Confirm_Deactivate_Group_Success;
            }
            return msg;
        }
    }

    public string displayMessage {
        get {
            String msg = '';
            if(contType == 'DeleteGroup') {
                msg = Label.OCSUGC_Confirm_Delete_Group;
            }
            else if(contType == 'DeactivateGroup') {
            	msg = Label.OCSUGC_Confirm_Deactivate_Group;
            }
            else if(contType == 'LeaveGroup') {
                msg = Label.OCSUGC_Confirm_Leave_Group;
            }
            else if(contType == 'DeactivateAccess') {
                msg = Label.OCSUGC_Confirm_Deactivate_Yourself;
            }
            else if(contType == 'RemoveGroupMember') {
                msg = Label.OCSUGC_Confirm_Remove_Group_Member;
            }
            else if(contType == 'DeleteDiscussion') {
                msg = Label.OCSUGC_Confirm_Delete_Discussion;
            }
            else if(contType == 'DeleteKnowledge') {
                msg = Label.OCSUGC_Confirm_Delete_Knowledge;
            }
            else if(contType == 'DeleteEvent') {
                msg = Label.OCSUGC_Confirm_Delete_Event;
            }
            else if(contType == 'ShowTnC') {
                msg = fetchTermsAndConditionData();
            }
            else if(contType == 'DeletePoll') {
                msg = Label.OCSUGC_Confirm_Delete_Poll;
            }
            else if(contType == 'DeleteDiscussionComment' || contType == 'DeletePollComment' || contType == 'DeleteEventComment' || contType == 'DeleteKnowledgeArtifactComment') {
                msg = Label.OCSUGC_Confirm_Delete_Comment;
            }
            return msg;
        }
    }

    public Pagereference actionToBeDone() {
        try {
            if(contType == 'DeleteGroup') {
                return deleteGroup();
            }
            else if(contType == 'DeactivateGroup') {
            	return deleteGroup();
            }
            else if(contType == 'LeaveGroup') {
                return leaveGroup();
            }
            else if(contType == 'DeactivateAccess') {
                return deactivateAccess();
            }
            else if(contType == 'RemoveGroupMember') {
                return removeGroupMember();
            }
            else if(contType == 'DeleteDiscussion') {
                return deleteDiscussion();
            }
            else if(contType == 'DeleteKnowledge') {
                return DeleteKnowledge();
            }
            else if(contType == 'DeleteEvent') {
                return DeleteEvent();
            }
            else if(contType == 'DeletePoll') {
                return deletePoll();
            }
            else if(contType == 'DeleteDiscussionComment') {
                return deleteComment('DeleteDiscussionComment');
            }
            else if(contType == 'DeletePollComment') {
                return deleteComment('DeletePollComment');
            }
            else if(contType == 'DeleteEventComment') {
                return deleteComment('DeleteEventComment');
            }
            else if(contType == 'DeleteKnowledgeArtifactComment') {
                return deleteComment('DeleteKnowledgeArtifactComment');
            }
         
        } catch(Exception ex) {
            isError = true;
            errorMessage = ex.getMessage();
        }
        return null;
    }

    //This method will delete the group.
    // Changed  16-03-2015   Puneet Sardana   Changed logic Ref T-369868
    private PageReference deleteGroup() {       
        PageReference homePage = null;
        if(contId != null) {
            List<CollaborationGroup> collGroupUpdate = new List<CollaborationGroup>();
            List<OCSUGC_CollaborationGroupInfo__c> collGroupInfoUpdate = new List<OCSUGC_CollaborationGroupInfo__c>();
            for(CollaborationGroup deleteGroup :[Select Name, IsArchived
                                                 From CollaborationGroup
                                                 Where Id =:contId
                                                 LIMIT 1
                                                 ]) { 

                deleteGroup.IsArchived = true;
                collGroupUpdate.add(deleteGroup);
               
            }
            for(OCSUGC_CollaborationGroupInfo__c collaInfo : [SELECT Id,OCSUGC_IsArchived__c
                                                              FROM OCSUGC_CollaborationGroupInfo__c
                                                              WHERE OCSUGC_Group_Id__c = :contId
                                                              LIMIT 1]) {
                 collaInfo.OCSUGC_IsArchived__c = true;
                 collGroupInfoUpdate.add(collaInfo);                                                
            }          
          SavePoint sp=Database.setSavepoint();
          try {
              update collGroupUpdate;
              update collGroupInfoUpdate;
              errorMessage = successMsg;
              homePage = Page.OCSUGC_Home;
              homePage.getParameters().put('isResetPwd','true');
              homePage.getParameters().put('dc',String.valueOf(isDC));
              homePage.setRedirect(true);
          } 
          catch(Exception ex) {
            errorMessage = ex.getMessage();
            Database.rollback(sp);
          }                  
      }
       return homePage;
    }

    //This method will leave a group for current logged in user.
    //Removing logged in user from Group
    @testvisible private PageReference leaveGroup() {
            CollaborationGroupMember removeMember = [SELECT Id,CollaborationGroup.CollaborationType
                                                     FROM   CollaborationGroupMember
                                                     WHERE  memberId =:userInfo.getUserId()
                                                     AND    CollaborationGroupId =:contId];
            if(removeMember != null) {
              delete removeMember;
            }
            return redirectGroupDetailPage('About');
    }

    //Deactivate Access
    private Pagereference deactivateAccess() {
    	System.debug('Puneet 1 ');
        if(contId != null && deactivatingReason!= null && deactivatingReason !='') {
        	  if(isDC) {
        	  	     System.debug('Puneet 2 ');
        	  		update new Contact(id = contId, OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Self, 
            				   						 OCSUGC_UserStatus__c = Label.OCSUGC_Disabled_by_Self, OCSDC_Disabled_Reason__c = deactivatingReason,
            				   						 OCSUGC_Disabled_Reason__c = deactivatingReason);
            		
            		//delete developer cloud member permission set
            		OCSUGC_Utilities.removePermissionSetFromUsers(new Set<ID>{userInfo.getUserId()}, new Set<String>{Label.OCSDC_VMS_Community_Members_Permissions});
            		//delete OncoPeer cloud member permission set
            		OCSUGC_Utilities.removePermissionSetFromUsers(new Set<ID>{userInfo.getUserId()}, new Set<String>{Label.OCSUGC_VMS_Member_Permission_set});  
            		//remove user from OCSGUS User group
            		OCSUGC_Utilities.removeUserFromGroup(new Set<ID>{userInfo.getUserId()}, new Set<String>{Label.OCSUGC_Users_Group});            		
            				   
        	  } else {
        	  	        System.debug('Puneet 3 ');
        	  			update new Contact(id = contId, OCSUGC_UserStatus__c = Label.OCSUGC_Disabled_by_Self, 
        	  							   OCSDC_UserStatus__c = Label.OCSDC_Disabled_by_Self,
        	  							   OCSDC_Disabled_Reason__c = deactivatingReason,
        	  							   OCSUGC_Disabled_Reason__c = deactivatingReason);
        	  		    for(User usr :[SELECT Id, Contact.OCSUGC_UserStatus__c, OCSUGC_Accepted_Terms_of_Use__c
	                      		   	   FROM User
	                                   WHERE ContactId =: contId 
	                                   AND Contact.OCSUGC_UserStatus__c =: Label.OCSUGC_Disabled_by_Self 
	                               	   limit 1]){
	                               			usr.OCSUGC_Accepted_Terms_of_Use__c = false;
	                               			usr.OCSDC_AcceptedTermsOfUse__c 	= false;
	                               			update usr;
	                               		}
        	 		    //01-April-2015  Puneet Sardana Changed to one future call Ref T-374270
            		  //  OCSUGC_Utilities.removePermissionSetFromUsers(new Set<ID>{userInfo.getUserId()}, new Set<String>{Label.OCSDC_VMS_Community_Members_Permissions});   
            		  //  OCSUGC_Utilities.removePermissionSetFromUsers(new Set<ID>{userInfo.getUserId()}, new Set<String>{Label.OCSUGC_VMS_Member_Permission_set});       	  		
        	  }   
            
        }
        PageReference pgRef = new Pagereference('/secur/logout.jsp');
        pgRef.setRedirect(true);
        return pgRef;
    }

    //removing user from Group
    private PageReference removeGroupMember() {
    	     
            CollaborationGroupMember removeMember = [Select Id,CollaborationGroup.CollaborationType
                                                     From CollaborationGroupMember
                                                     Where memberId =:supportContId
                                                     And CollaborationGroupId =:contId];
        	system.debug('###1' + removeMember.CollaborationGroup.CollaborationType);
        	system.debug('###2' + OCSUGC_Constants.GROUP_TYPE_PRIVATE);
        	system.debug('###3' + isFGAB);
            if(isFGAB || removeMember.CollaborationGroup.CollaborationType ==  OCSUGC_Constants.GROUP_TYPE_PRIVATE) {
            	List<OCSUGC_Group_Invitation__c> lstGroupInvitation = [SELECT Id
            	                                                    FROM OCSUGC_Group_Invitation__c
            	                                                    WHERE OCSUGC_Group_Id__c=:contId
            	                                                    AND OCSUGC_User_Id__c=:supportContId];
            	
            	/** ONCO-446, Puneet Mishra, 22Dec2015, deleting Notification reecord when user is been removed from FGAB 
            		string is been created as there is no field to store the information or relationship which will link the Group Notification to Intranet Notification.
            	*/
            	String FGABNOTIFICATION = '%OCSUGC/ocsugc_fgababoutgroup?fgab=true&g='+ contId + '&inviteUser=' + supportContId +'%';
            	List<OCSUGC_Intranet_Notification__c> listNotifications = [ SELECT Id, OCSUGC_Link_to__c, OCSUGC_User__c, OCSUGC_Related_User__c, CreatedDate
            																FROM OCSUGC_Intranet_Notification__c
            																WHERE OCSUGC_Link_to__c LIKE: FGABNOTIFICATION
            																	ORDER BY CreatedDate Desc];
            	
            	if(lstGroupInvitation.size() > 0) {
            		delete lstGroupInvitation;
            	}
            	
            	if(listNotifications.size() > 0)
            		delete listNotifications;
            } 
            if(removeMember != null) {
                delete removeMember;
            }
           
            return redirectGroupDetailPage('Members');
    }
    
    //Author - Shital Bhujbal, Task no. - ONCO - 450 , Date - 5/02/2016
    //Method Name - deleteComment(),Description - Method created to delete feedcomment
    
	public PageReference deleteComment(String parentType) {
    		
    		Pagereference pg = null;
    		OCSUGC_Intranet_Content__c fGABEvent;
            FeedComment deleteFeedComment = [select id from FeedComment where id =:contId];
            system.debug('***********deleteFeedComment************'+deleteFeedComment);
            if(deleteFeedComment != null) {
                //deleteFeedComment.IsDeleted =true; //field isDeleted is not writable;
                //update deleteFeedComment;
                delete deleteFeedComment;
            }
            if(parentType == 'DeleteDiscussionComment'){
            	if(isFGAB){
            		pg = Page.ocsugc_fgabdiscussiondetail;
            	}
            	else{
            		pg = Page.OCSUGC_DiscussionDetail;
            	}
	            pg.getParameters().put('Id',supportContId);
            }
            if(parentType == 'DeletePollComment'){
    			pg = Page.OCSUGC_FGABPollDetail;
        		pg.getParameters().put('Id',supportContId);	
            }
            if(parentType == 'DeleteEventComment'){
            	if(isFGAB){
            		fGABEvent = [SELECT Name, OCSUGC_GroupId__c FROM   OCSUGC_Intranet_Content__c
                    	    	                    		WHERE Id = :supportContId];
            		pg = Page.OCSUGC_FGABEventDetail;
            	}
            	else{
            		pg = Page.OCSUGC_EventDetail;
            	}
	            pg.getParameters().put('eventId',supportContId);
            }
            if(parentType == 'DeleteKnowledgeArtifactComment'){
            	pg = Page.OCSUGC_KnowledgeArtifactDetail;
	            pg.getParameters().put('knowledgeArtifactId',supportContId);
            }
	    	//pg.getParameters().put('isResetPwd','true');
	    	pg.getParameters().put('dc',''+isDC);
	    	pg.getParameters().put('fgab',''+isFGAB);
	    	
	    	if(isFGAB && fGABEvent!=NULL){
	    		pg.getParameters().put('g',fGABEvent.OCSUGC_GroupId__c);
	    	}
	    	
	        pg.setRedirect(true);
	        return pg;
            //return redirectHomePage();
    }
	
    private PageReference deleteDiscussion() {
            OCSUGC_DiscussionDetail__c deleteDiscussion = [select id from OCSUGC_DiscussionDetail__c
                                                    where OCSUGC_DiscussionId__c =:contId];
            if(deleteDiscussion != null) {
                deleteDiscussion.OCSUGC_Is_Deleted__c = true;
                update deleteDiscussion;
            }
            return redirectHomePage();
    }

    private PageReference deletePoll() {
	    Pagereference pg = null;
	    String groupId = null;
	    OCSUGC_Poll_Details__c deletePoll = [SELECT id,OCSUGC_Group_Id__c
	                                         FROM   OCSUGC_Poll_Details__c
	                                         WHERE  OCSUGC_Poll_Id__c  =:contId LIMIT 1];
	    if(deletePoll != null && deletePoll.OCSUGC_Group_Id__c != null) {
	    	groupId = deletePoll.OCSUGC_Group_Id__c;
	    }
	    if(deletePoll != null) {
	        deletePoll.OCSUGC_Is_Deleted__c = true;
	        update deletePoll;
	    }
	    // Changed Page Redirect T-379076
    	pg = Page.OCSUGC_Home;
    	pg.getParameters().put('isResetPwd','true');
    	pg.getParameters().put('dc',''+isDC);
    	pg.getParameters().put('fgab',''+isFGAB);
    	pg.getParameters().put('g',groupId);
        pg.setRedirect(true);
        return pg;
   }

    private PageReference DeleteKnowledge() {
            OCSUGC_Knowledge_Exchange__c deleteKnowledge = [select id from OCSUGC_Knowledge_Exchange__c
                                                    where id =:contId];
            if(deleteKnowledge != null) {
                deleteKnowledge.OCSUGC_Is_Deleted__c = true;
                update deleteKnowledge;
            }
            return redirectHomePage();
    }

    private PageReference DeleteEvent() {
            OCSUGC_Intranet_Content__c deleteEvent = [select id from OCSUGC_Intranet_Content__c
                                                    where id =:contId];
            if(deleteEvent != null) {
                deleteEvent.OCSUGC_Is_Deleted__c = true;
                update deleteEvent;
            }
            
            return redirectHomePage();
    }

    private String fetchTermsAndConditionData() {

      for(OCSUGC_CollaborationGroupInfo__c objCGI : [SELECT  OCSUGC_Group_TermsAndConditions__c
                                                     FROM    OCSUGC_CollaborationGroupInfo__c
                                                     WHERE   OCSUGC_Group_Id__c = :contId LIMIT 1]) {
        strTerms = objCGI.OCSUGC_Group_TermsAndConditions__c;
      }
      return strTerms;
    }

    @testVisible private PageReference redirectGroupDetailPage(String showPanel) {
        Pagereference pg;
        //***Naresh K Shiwani	T-377431	13/04/2015	Added isDC condition***
        if(isFGAB){
        	pg = Page.OCSUGC_FGABAboutGroup;
        	pg.getParameters().put('fgab',''+isFGAB);
        	pg.getParameters().put('g',contId);
        }
        else if(isDC){
        	pg = Page.OCSUGC_GroupDetail;
            pg.getParameters().put('dc',''+isDC);
            pg.getParameters().put('g',contId);
        }
        else{
        	pg = Page.OCSUGC_GroupDetail;
            pg.getParameters().put('g',contId);
        }
        pg.setRedirect(true);
        return pg;
    }

    private PageReference redirectHomePage() {
        Pagereference pg = Page.OCSUGC_Home;
        if(isFGAB) {
        	 pg.getParameters().put('fgab','true');
        	 pg.getParameters().put('g',fgabGroupId);
        } 
        if(isDC)
        	pg.getParameters().put('dc','true');
        pg.getParameters().put('isResetPwd','true');
        pg.setRedirect(true);
        return pg;
    }
}
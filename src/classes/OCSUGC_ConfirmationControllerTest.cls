@isTest
public class OCSUGC_ConfirmationControllerTest {
	static FeedItem item1;
	static FeedComment comment;
	static List<FeedComment> commentList = new List<FeedComment>();
	
	/**/
	static Profile admin;
	static Profile portal;
	static PermissionSet memberPermissions;
	static Account objAccount;
	static List<Contact> lstContacts;
	static User adminUser;
	static List<User> lstUser;
	static FeedItem poll;
	static list<FeedItem> feedItemList;
	static OCSUGC_Poll_Details__c newPoll;
	static OCSUGC_DiscussionDetail__c ddtest;
	static FeedComment tempFeedcomment;
	static OCSUGC_Intranet_Content__c event;
	
	static{
		feedItemList = new List<FeedItem>();
		item1 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
		feedItemList.add(item1);
		poll = new FeedItem();
		poll.ParentId = userInfo.getUserId();
		poll.Body = 'test poll';
		poll.type = 'PollPost';
		feedItemList.add(poll);
		insert feedItemList;
		
		for(Integer i=0;i<5;i++){
			commentList.add(OCSUGC_TestUtility.createFeedComment(item1.id,false));
		}
        if(commentList.size()>0){
        	insert commentList;
        }
        
        // OCSUGC_PollDetail
        newPoll = new OCSUGC_Poll_Details__c();
		newPoll.OCSUGC_Poll_Id__c = poll.id;
		insert newPoll;
        
        //OCSUGC_DiscussionDetail
        ddtest = new OCSUGC_DiscussionDetail__c();
		ddtest.OCSUGC_DiscussionId__c = item1.Id;
		insert ddtest;
        
        //FeedComment
        tempFeedcomment = new FeedComment(FeedItemId = item1.Id, CommentBody = 'TestComment', CommentType = 'TextComment');
		insert tempFeedcomment;
        
        //OCSUGC_Intranet_Content
        event = new OCSUGC_Intranet_Content__c();
		insert event;
        
        //creating Profile
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile(); 
        memberPermissions = OCSUGC_TestUtility.getMemberPermissionSet();        
        //Creating account object
        objAccount = OCSUGC_TestUtility.createAccount('test account', true);
        //List of Contact Object
        lstContacts = new List<Contact>();  
        //Creating Contact object
        for(integer i = 0;i<2;i++) {
          lstContacts.add( OCSUGC_TestUtility.createContact(objAccount.Id, false)); 
        }
        insert lstContacts;
        
        //creating User
        lstUser = new List<user>();
        for(integer i = 0;i<2;i++){
            User usr = new User();
            usr.Email = 'test'+ i +'@bechtel.com';
            usr.Username = 'testss' + i + '@testuser1.com';
            usr.LastName = 'test'+ i;
            usr.Alias = 'test' + i;
            usr.ProfileId = portal.Id;
            usr.ContactId = lstContacts[i].Id;
            usr.LanguageLocaleKey = 'en_US';
            usr.LocaleSidKey = 'en_US';
            usr.TimeZoneSidKey = 'America/Chicago';
            usr.EmailEncodingKey = 'UTF-8';
            usr.isActive = true;
            lstUser.add(usr);
        }
        
        adminUser = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '13223','1');
		lstUser.add(adminUser);
		insert lstUser;
	}

	static testMethod void controllerTest() {
       // Start/Stop Test create new transaction for SOQL.
       Test.startTest(); 
		System.runAs(adminUser){
        	List<PermissionSetAssignment> lstAssignment = new List<PermissionSetAssignment>();
            PermissionSetAssignment assign;
            for(User usr : lstUser) {
            	assign = new PermissionSetAssignment();
               	assign.AssigneeId = usr.Id;
               	assign.PermissionSetId = memberPermissions.Id;
               	lstAssignment.add(assign);
            }
            insert lstAssignment;
        }
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        CollaborationGroup cgrp = OCSUGC_TestUtility.createGroup('test' + key, 'public', ocsugcNetwork.Id, true);      
        
        
        //CollaborationGroupMember grpMem = OCSUGC_TestUtility.createGroupMember(lstUser[0].Id, cgrp.Id, true);        
        CollaborationGroupMember groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUser.Id, cgrp.Id, true);
        
	        OCSUGC_ConfirmationController controller = new OCSUGC_ConfirmationController();
	        //added by SHital start
	        controller.contId = commentList[0].Id;
	        controller.contType = 'DeleteDiscussionComment';
	        controller.supportContId = item1.Id;
	        System.assert(controller.headerMessage<>null);
	        System.assert(controller.successMsg<>null);
	        System.assert(controller.displayMessage<>null);
	        controller.actionToBeDone();
	        controller.contId = commentList[1].Id;
	        controller.contType = 'DeletePollComment';
	        controller.supportContId = item1.Id;
	        controller.actionToBeDone();
	        //-----------------
	        controller.contId = commentList[2].Id;
	        controller.contType = 'DeleteEventComment';
	        controller.supportContId = item1.Id;
	        controller.actionToBeDone();
	        //-------
	        controller.contId = commentList[3].Id;
	        controller.contType = 'DeleteKnowledgeArtifactComment';
	        controller.supportContId = item1.Id;
	        controller.actionToBeDone();
	        //added by SHital End
	        System.runAs(lstUser[0]){
		        ApexPages.currentPage().getParameters().put('dc', 'true');
		        controller.isDC = true;
		        controller.contId = cgrp.Id;
		        controller.deactivatingReason = 'test';
		        controller.contType = 'DeleteGroup';
		        controller.actionToBeDone();
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        System.assert(controller.displayMessage<>null);
		
		        controller.contId = cgrp.Id;
		        controller.contType = 'LeaveGroup';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();
		
		        controller.contId = cgrp.Id;
		        controller.contType = 'DeactivateAccess';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();
		
		        controller.contId = cgrp.Id;
		        controller.contType = 'RemoveGroupMember';
		        controller.isFGAB = true;
		        cgrp.CollaborationType = OCSUGC_Constants.GROUP_TYPE_PRIVATE;       
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();        
		        OCSUGC_DiscussionDetail__c discsn = OCSUGC_TestUtility.createDiscussion(cgrp.Id, lstUser[0].Id, true);
		        //commented by Shital
		        controller.contId = discsn.Id;
		        controller.contType = 'DeleteDiscussion';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();
		        
		        OCSUGC_Knowledge_Exchange__c know = OCSUGC_TestUtility.createKnwExchange(cgrp.Id,cgrp.Name,true);
		        controller.contId = know.Id;
		        controller.contType = 'DeleteKnowledge';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.displayMessage<>null);
		        //controller.headerMessage();
		        //controller.successMsg();
		        //controller.displayMessage();
		        controller.actionToBeDone();
		
		        controller.contId = event.Id;
		        controller.contType = 'DeleteEvent';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();
		        
		        controller.isDC = true;
		        controller.contId = event.Id;
		        controller.contType = 'ShowTnC';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();
		        
		        OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
		        info.OCSUGC_Group_Id__c = cgrp.Id;
		        info.OCSUGC_Email__c = 'psardana@appirio.com';
		        info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
		        insert info;
		        controller.contId = info.Id;    
		        controller.contType = 'ShowTnC';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        controller.actionToBeDone();
		        controller.contType = 'CreateTnC';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        controller.actionToBeDone();
		        controller.contType = 'DeactivateGroup';
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();        
		        controller.contType = 'DeletePoll';
		        controller.contId = Poll.Id;
		        System.assert(controller.headerMessage<>null);
		        System.assert(controller.successMsg<>null);
		        System.assert(controller.displayMessage<>null);
		        controller.actionToBeDone();        
	        }
        Test.stopTest();
    }
    
    /**
     *	12 Dec 2016, Puneet Mishra
     */
    public static testMethod void redirectGroupDetailPage_FGAB_Test() {
    	Test.StartTest();
    		Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    		Blob blobKey = crypto.generateAesKey(128);
        	String key = EncodingUtil.convertToHex(blobKey);
    		CollaborationGroup cgrp = OCSUGC_TestUtility.createGroup('test' + key, 'public', ocsugcNetwork.Id, true);
    		ApexPages.currentPage().getParameters().put('fgab', 'true');
    		OCSUGC_ConfirmationController cont = new OCSUGC_ConfirmationController();
    		cont.contId = cgrp.Id;
    		cont.redirectGroupDetailPage('fgab');
    	Test.Stoptest();
    }
    
    /**
     *	12 Dec 2016, Puneet Mishra
     */
    public static testMethod void redirectGroupDetailPage_DC_Test() {
    	Test.StartTest();
    		Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    		Blob blobKey = crypto.generateAesKey(128);
        	String key = EncodingUtil.convertToHex(blobKey);
    		CollaborationGroup cgrp = OCSUGC_TestUtility.createGroup('test' + key, 'public', ocsugcNetwork.Id, true);
    		ApexPages.currentPage().getParameters().put('dc', 'true');
    		OCSUGC_ConfirmationController cont = new OCSUGC_ConfirmationController();
    		cont.contId = cgrp.Id;
    		cont.redirectGroupDetailPage('dc');
    	Test.Stoptest();
    }
    
    /**
     *	12 Dec 2016, Puneet Mishra
     */
    public static testMethod void redirectGroupDetailPage_OCSUGC_Test() {
    	Test.StartTest();
    		Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    		Blob blobKey = crypto.generateAesKey(128);
        	String key = EncodingUtil.convertToHex(blobKey);
    		CollaborationGroup cgrp = OCSUGC_TestUtility.createGroup('test' + key, 'public', ocsugcNetwork.Id, true);
    		ApexPages.currentPage().getParameters().put('dc', 'false');
    		ApexPages.currentPage().getParameters().put('fgab', 'false');
    		OCSUGC_ConfirmationController cont = new OCSUGC_ConfirmationController();
    		cont.contId = cgrp.Id;
    		cont.redirectGroupDetailPage('ocsugc');
    	Test.Stoptest();
    }
    
    /**
     *	12 Dec 2016, Puneet Mishra
     */
    public static testMethod void leaveGroup_Test() {
    	Test.StartTest();
    		Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    		Blob blobKey = crypto.generateAesKey(128);
        	String key = EncodingUtil.convertToHex(blobKey);
    		CollaborationGroup cgrp = OCSUGC_TestUtility.createGroup('test' + key, 'public', ocsugcNetwork.Id, true);
    		CollaborationGroupMember groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUser.Id, cgrp.Id, true);
    		OCSUGC_ConfirmationController cont = new OCSUGC_ConfirmationController();
    		cont.contId = cgrp.Id;
    		//cont.leaveGroup();	
    	Test.Stoptest();
    }
    
}
public class OCSUGC_Constants {
    public final static String UNLISTED = 'Unlisted';
    public final static String OCSUGC_ADMIN_PUBLIC_GROUP = 'OCSUGC_Admins';
    public final static String GROUP_TYPE_PRIVATE = 'Private';
    public final static String CONTACT_STATUS_DISABLE_SELF = 'OCSUGC Disabled by Self';
    public final static String CONTACT_STATUS_DISABLE_ADMIN = 'OCSUGC Disabled by Admin';
    public final static String CONTACT_STATUS_APPROVED = 'OCSUGC Approved';
    //Static final variable for querying Email Template and Admin Group
    public final static String OCSUGC_ADMIN_PUBLIC_GROUP_EMAIL_TEMPLATE = 'OCSUGC_Email_Notification_To_The_users_in_the_OCSUGC_Admin_Public_Group';
      public final static String UPLOAD_STATUS_MESSAGE = 'Thank you for uploading a Knowledge Artifact.';
    public final static String KnowledgeArtifactRecordTypev = 'Knowledge Files - Approved File Types';
    public final static String SYSTEM_ADMIN_PROFILE = 'System Administrator';
    public final static String CONTACT_ACCOUNT_NAME = 'Varian Medical Systems';
    public final static Integer SF_QUERY_LIMIT = 50000;
    public final static Integer SF_QUERY_ENTITY_SUBSCRIPTION_LIMIT = 999;
    public final static String DevCloud_User_Status_Invited = 'Invited';
    public final static String DevCloud_User_Status_Member = 'Member';  
    public final static String DC_COMMUNITY_MEMBER_SET  = 'VMS_OCSDC_Community_Members_Permissions';
    public final static String RT_Application  = 'Applications';
    public final static String CONST_TRUE = 'true';
    public final static String MEMBER = 'Member';
    public final static String INVITED = 'Invited';
    
    public final static String OCSUGC_STATUS_DISABLED_BY_ADMIN = 'OCSUGC Disabled by Admin';
    public final static String OCSUGC_STATUS_DISABLED_BY_SELF = 'OCSUGC Disabled by Self';
    public final static String OCSUGC_STATUS_REJECTED = 'OCSUGC Rejected';
    public final static String OCSUGC_STATUS_DISQUALIFIED = 'OCSUGC Disqualified';
     
    public final static String OCSUGC_COMMUNITY_CONTRIBUTOR = 'OCSUGC_Community_Contributor_Permissions';
    public final static String OCSUGC_COMMUNITY_MANAGER = 'OCSUGC_Community_Manager_Permissions';
    public final static String OCSUGC_COMMUNITY_MEMBER = 'OCSUGC_Community_Members_Permissions';
    
}
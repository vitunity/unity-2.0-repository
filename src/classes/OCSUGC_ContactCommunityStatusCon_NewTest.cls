/**
 *  08 Sept 2015        Puneet Mishra       Test Class for OCSUGC_ContactCommunityStatusCon_New
 */
@isTest
private class OCSUGC_ContactCommunityStatusCon_NewTest {
    
    static Profile portal;
    static Profile admin;
    
    static Network ocsugcNetwork;
    static CollaborationGroup collabGroup;
    static OCSUGC_Knowledge_Exchange__c kExchng;
    static {
        admin = OCSUGC_TestUtility.getAdminProfile();
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
        collabGroup = OCSUGC_TestUtility.createGroup('TestGroup','Public',ocsugcNetwork.id,true);
        kExchng = OCSUGC_TestUtility.createKnwExchange(collabGroup.id, 'TestGroup',true);
    }
    
    private static OCSUGC_Knowledge_Exchange__c KnowledgeArticleData(String KA_Status) {
        OCSUGC_Knowledge_Exchange__c KA = new OCSUGC_Knowledge_Exchange__c();
        KA.OCSUGC_Title__c  = 'Test Knowledge_Artilcle';
        KA.OCSUGC_Summary__c  = 'Test Summary Data';
        KA.OCSUGC_Status__c  = KA_Status;
        KA.OCSUGC_Content_Sensitivity__c  = 'Sensitive: This artifact contains clinical/non-research data.';
        KA.OCSUGC_Artifact_Type__c  = 'Document';
        return KA;
    }
    
    public static List<Contact> createContact(Account acc, List<String> status) {
        // Inserting Contact
        List<Contact> contList = new List<Contact>();
        Contact cont1 = OCSUGC_TestUtility.createContact(acc.Id, false);
        cont1.OCSUGC_UserStatus__c = status[0];
        Contact cont2= OCSUGC_TestUtility.createContact(acc.Id, false);
        cont2.OCSUGC_UserStatus__c = status[1];
        contList.add(cont1);
        contList.add(cont2);
        insert contList;
        return contList;
    }
    
    private static void createData(List<String> statusList) {
        List<Contact> contList = new List<Contact>();
        // Inserting Account
        Account acc = OCSUGC_TestUtility.createAccount('Test Account', true);
        // Method Insert Contacts
        contList = createContact(acc, statusList);
        // fetching Portal Profile
        portal = OCSUGC_TestUtility.getPortalProfile();
        
        // Inserting Portal User
        List<User> usrList = new List<User>();
        usrList.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contList[0].Id, '001',Label.OCSUGC_Varian_Employee_Community_Member));
        usrList.add(OCSUGC_TestUtility.createPortalUser(false, portal.Id, contList[1].Id, '002',Label.OCSUGC_Varian_Employee_Community_Member));
        insert usrList;
    }
    
    static testMethod void fetchKnowledgeArticles_Test() {
        OCSUGC_Knowledge_Exchange__c KA = KnowledgeArticleData('Pending Approval');
        insert KA;
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.fetchKnowledgeArticles();
    }
    
    // 21 Aug 2015, Puneet Mishra, Test Method for display Contact
    static testMethod void reload_Test() {
        List<String> statusList = new List<String>();
        statusList.add('OCSUGC Approved');
        statusList.add('OCSUGC Pending Approval');
        // below method with create the Account, Contact, User records
        createData(statusList);
        
        PageReference pageRef = Page.OCSUGC_CommunityUserAdministration_New;
        Test.setCurrentPage(pageRef);
        String status = 'OCSUGC Approved';
        //ApexPages.currentPage().getParameters().put('acc','Test Account');
        ApexPages.currentPage().getParameters().put('status', status);
        ApexPages.currentPage().getParameters().put('KA','false');
        
        PageReference testRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=false&status=' + status);
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        PageReference actual = controller.reload();
        
        system.assertEquals(actual.getUrl(), testRef.getUrl());
    }
    
    static testMethod void reload_withAccount_Test() {
        List<String> statusList = new List<String>();
        statusList.add('OCSUGC Approved');
        statusList.add('OCSUGC Pending Approval');
        // below method with create the Account, Contact, User records
        createData(statusList);
        
        PageReference pageRef = Page.OCSUGC_CommunityUserAdministration_New;
        Test.setCurrentPage(pageRef);
        String status = 'OCSUGC Approved';
        ApexPages.currentPage().getParameters().put('acc','Test Account');
        ApexPages.currentPage().getParameters().put('status', status);
        ApexPages.currentPage().getParameters().put('KA','false');
        
        String accName = 'Test_Account';
        PageReference testRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=false&status=' + status + '&acc=' + accName);
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.accountName = accName;
        PageReference actual = controller.reload();
        
        system.assertEquals(actual.getUrl(), testRef.getUrl());
    }
    
    static testMethod void reload_withCountry_Test() {
        List<String> statusList = new List<String>();
        statusList.add('OCSUGC Approved');
        statusList.add('OCSUGC Pending Approval');
        // below method with create the Account, Contact, User records
        createData(statusList);
        
        PageReference pageRef = Page.OCSUGC_CommunityUserAdministration_New;
        Test.setCurrentPage(pageRef);
        String status = 'OCSUGC Approved';
        ApexPages.currentPage().getParameters().put('acc','Test Account');
        ApexPages.currentPage().getParameters().put('status', status);
        ApexPages.currentPage().getParameters().put('KA','false');
        
        String country = 'India';
        PageReference testRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=false&status=' + status + '&count=' + country);
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.country = country;
        PageReference actual = controller.reload();
        
        system.assertEquals(actual.getUrl(), testRef.getUrl());
    }
    
    static testMethod void selectedContact_Test() {
        // Inserting Account
        Account acc = OCSUGC_TestUtility.createAccount('Test Account', true);
        Contact cont = new Contact(FirstName = 'TestFirstName', LastName = 'TestLastName', Email = 'Test_123_User@test.com', AccountId = acc.Id, MailingCountry = 'UK',
                                    MyVarian_member__c = true, PasswordReset__c = false, OCSUGC_Notif_Pref_ReqJoinGrp__c = true, OCSUGC_Notif_Pref_EditGroup__c = true,
                                    OCSUGC_Notif_Pref_PrivateMessage__c = true, OCSUGC_Notif_Pref_InvtnJoinGrp__c = true, OCSUGC_UserStatus__c = Label.OCSUGC_Pending_Approval);
        
        insert cont;
        
        portal = OCSUGC_TestUtility.getPortalProfile();
        
        // Inserting Portal User
        User usr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, cont.Id, '001',Label.OCSUGC_Varian_Employee_Community_Member);
        insert usr;
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.selectedContactId = cont.Id;
        
        controller.selectedContact();
        
        // Label Set to Approved because of trigger running on contact which will make Status to approved if MyVarian checkbox is checked.
        system.assertEquals(cont.Id, controller.contactStatus.Id);
        system.assertEquals(Label.OCSUGC_Approved_Status, controller.contactStatus.OCSUGC_UserStatus__c);
    }
    
    static testMethod void getWrapperList_Test() {
        List<String> statusList = new List<String>();
        statusList.add('OCSUGC Approved');
        statusList.add('OCSUGC Pending Approval');
        // below method with create the Account, Contact, User records
        createData(statusList);
        
        String accName = 'Test Account';
        String country = 'India';
        
        PageReference pageRef = Page.OCSUGC_CommunityUserAdministration_New;
        Test.setCurrentPage(pageRef);
        String status = 'OCSUGC Approved';
        ApexPages.currentPage().getParameters().put('acc', accName);
        ApexPages.currentPage().getParameters().put('count', country);
        ApexPages.currentPage().getParameters().put('KA','false');
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        Integer listSize = controller.getWrapperList().size();
        //system.assertEquals(2, listSize);
    }
    
    // Test Method for getKnowledgeArtifactsRequests(), No KnowledeGe Artifact record in system
    public static testMethod void getKnowledgeArtifactsRequests_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.getKnowledgeArtifactsRequests();
    }
    
    // Test Method for getKnowledgeArtifactsRequests(), Contains KNowledge Artifact records in System
    public static testMethod void getKnowledgeArtifactsRequests_WithRecords_Test() {
        // Creating New Knowledege Article record
        OCSUGC_Knowledge_Exchange__c KA = KnowledgeArticleData('Pending Approval');
        insert KA;
        
        String queryKA;
        queryKA =  'Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c from OCSUGC_Knowledge_Exchange__c ';
        queryKA += ' where OCSUGC_Status__c = \'Pending Approval\' AND OCSUGC_Is_Deleted__c=false'; 
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.conKA = new ApexPages.StandardSetController(Database.getQueryLocator(queryKA));
        
        List<OCSUGC_Knowledge_Exchange__c> KnowledgeExchangeList = controller.getKnowledgeArtifactsRequests();
        system.assertEquals(1, KnowledgeExchangeList.size());
    }
    
    // Test Method for updateKowledgeArtifacts(),
    public static testMethod void updateKnowledgeArtifacts_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.selectedKnowledgeExchangeId = kExchng.Id;
        controller.assignSelectedKnowledgeArtificat();
        controller.selectedKnowledge.OCSUGC_Rejection_Reason__c = 'Test Rejection';
        
        controller.artifactStatus = 'Rejected';
        PageReference actualPageRef = controller.updateKnowledgeArtifacts();
        
        OCSUGC_Knowledge_Exchange__c expectedKExchng = [SELECT ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c, 
                                                        OCSUGC_Rejection_Reason__c FROM OCSUGC_Knowledge_Exchange__c WHERE Id =: kExchng.Id];
        system.assertEquals(expectedKExchng.OCSUGC_Rejection_Reason__c, 'Test Rejection');
    
        PageReference expectedPageRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=true');
        
        system.assertEquals(actualPageRef.getUrl(), ExpectedPageRef.getUrl());
    }
    
    // Test Method for assignSelectedKnowledgeArtifacts()
    static testMethod void assignSelectedKnowledgeArtifacts_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.selectedKnowledgeExchangeId = kExchng.Id;
        
        OCSUGC_Knowledge_Exchange__c expectedExchange = [Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c, 
                                                                OCSUGC_Rejection_Reason__c
                                                        FROM OCSUGC_Knowledge_Exchange__c WHERE id =: kExchng.Id];
        controller.assignSelectedKnowledgeArtificat();
        system.assertEquals(expectedExchange, controller.selectedKnowledge);
    }
    
    // Test Method for getCountries()
    static testMethod void getCountries_Test() {
        List<SelectOption> expectedOptions = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
        List<Schema.PickListEntry> ple = fieldResult.getPickListValues();       // get the Country__c picklist values
        expectedOptions.add(new SelectOption('',''));
        for( Schema.PickListEntry f : ple ) {
            expectedOptions.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        List<SelectOption> actualOptions = controller.getCountries();
        
        // Covering getContactStatusList
        controller.getContactStatusList();
        
        system.assertEquals(actualOptions, expectedOptions);
    }
    
    // Test Method for statusValueCake
    static testMethod void statusValuesCake_PendingApprovals_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.statusContact = Label.OCSUGC_Pending_Approval;
        
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption(Label.OCSUGC_Pending_Approval,Label.OCSUGC_Pending_Approval));
        controller.statusValuesCake = option;
    }
    
    static testMethod void statusValuesCake_DisabledUsers_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.statusContact = Label.OCSUGC_Disabled_by_Admin;
        
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption(Label.OCSUGC_Pending_Approval,Label.OCSUGC_Pending_Approval));
        controller.statusValuesCake = option;
    }
    
    static testMethod void statusValuesCake_Rejected_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.statusContact = Label.OCSUGC_Rejected;
        
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption(Label.OCSUGC_Pending_Approval,Label.OCSUGC_Pending_Approval));
        controller.statusValuesCake = option;
    }
    
    public static testMethod void updateContact_Approved_Test() {
        // Inserting Account
        Account acc = OCSUGC_TestUtility.createAccount('Test Account', true);
        Contact cont = new Contact(FirstName = 'TestFirstName', LastName = 'TestLastName', Email = 'Test_123_User@test.com', AccountId = acc.Id, MailingCountry = 'UK',
                                    MyVarian_member__c = true, PasswordReset__c = false, OCSUGC_Notif_Pref_ReqJoinGrp__c = true, OCSUGC_Notif_Pref_EditGroup__c = true,
                                    OCSUGC_Notif_Pref_PrivateMessage__c = true, OCSUGC_Notif_Pref_InvtnJoinGrp__c = true, OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status,
                                    OktaId__c = '00ub0oNGTSWTBKOLGLNR');
        
        insert cont;
        
        portal = OCSUGC_TestUtility.getPortalProfile();
        
        // Inserting Portal User
        User usr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, cont.Id, '001',Label.OCSUGC_Varian_Employee_Community_Member);
        insert usr;
        
        
        OCSUGC_ContactCommunityUserStatusCon_New controller = new OCSUGC_ContactCommunityUserStatusCon_New();
        controller.selectedContactId = cont.Id;
        
        controller.selectedContact();
        
        system.assertEquals(cont.Id, controller.contactStatus.Id);
        system.assertEquals(cont.OCSUGC_UserStatus__c, controller.contactStatus.OCSUGC_UserStatus__c);
        
        
        controller.updateContact();
        system.assertEquals(controller.contactStatus.OCSUGC_Notif_Pref_EditGroup__c, true);
        system.assertEquals(controller.contactStatus.OCSUGC_Notif_Pref_InvtnJoinGrp__c, true);
        system.assertEquals(controller.contactStatus.OCSUGC_Notif_Pref_PrivateMessage__c, true);
        system.assertEquals(controller.contactStatus.OCSUGC_Notif_Pref_ReqJoinGrp__c, true);
    }
    
    static testMethod void wrapperContact_Test() {
        OCSUGC_ContactCommunityUserStatusCon_New.WrapperContact controller = new OCSUGC_ContactCommunityUserStatusCon_New.WrapperContact(new Contact(), new User());
    }
    
}
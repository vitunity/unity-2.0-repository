/*
Name        : OCSUGC_ContactCommunityUserStatusController
Updated By  : Rishi Khirbat (Appirio India)
Date        : 25th April, 2014
Purpose     : An Apex controller to get Information for ContactCommunityUserStatus.page.

Refrence    : T-271570: Develop a VisualForce Page for pending registration approval
Updated By  : Sumit Tanwar on 16th July, 2014.

17-11-2014   Naresh K Shiwani    T-328689

Updated By  :   Puneet Mishra   Community Name from 'OCSUGC' to 'OncoPeer'
30-04-2015   Naresh K Shiwani    T-382725   Removed code of "Comments by Varian" tab.
12-06-2015   : Shital Bhujbal   Code changes done by Shital Bhujbal to remove deleted knowledge files from pending approvals list
*/
public without sharing class OCSUGC_ContactCommunityUserStatusCon {

    //For Pagination
    static final Integer DEFAULT_LINKS_AT_A_TIME;
    static final Integer DEFAULT_NUMBER_OF_LINKS_BEFORE_CURRENT;
    static final Integer DEFAULT_NUMBER_OF_LINKS_AFTER_CURRENT;
  
    static {
        DEFAULT_LINKS_AT_A_TIME = 5;
        DEFAULT_NUMBER_OF_LINKS_BEFORE_CURRENT = 1;
        DEFAULT_NUMBER_OF_LINKS_AFTER_CURRENT = 1;
    }
    public Integer currentPage {get;set;}
    public Integer totalPage {get;set;}
    public Integer totalRecords {get;set;}
    public Integer countPageLoad {get;set;}
    //-For Pagination
    // constants
    private static final String REJECTION_TEMPLATE_NAME = 'OCSUGC_Contact_Community_User_Status_Rejection';
    private Id orgWideEmailAddressId;
    public Contact contactStatus {get;set;}
    public OCSUGC_Knowledge_Exchange__c selectedKnowledge{get; set;}
     
    
    public String currentTab {get;set;}
    public String title {get;set;}
    public String actionLabel {get;set;}
    public String selectedContactId {get;set;}
    public String selectedRequestId {get;set;}
    public String requestRespond {get;set;}
    public String selectedKnowledgeExchangeId {get;set;}
    public String artifactStatus {get;set;}
    public String kaDetailLink {get;set;}
    public List<Contact> lstContacts {get; set;}
    public Boolean isRejected {get;set;}
    public Boolean foundError {get;set;}
    public Integer totalRequests {get;set;}
    public Integer totalRequestsKnowledge {get;set;}
    public ApexPages.StandardSetController con {get;set;}
    public ApexPages.StandardSetController conAP {get;set;}
    public ApexPages.StandardSetController conKA {get;set;}
    public Map<String, User> mapContactUser { get; set; }
    public List<selectOption> statusValuesCake { // Only picklist values which have "Cake" in text
        get{
                List<SelectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
                options.add(new SelectOption(Label.OCSUGC_Approved_Status,Label.OCSUGC_Approved_Status));
                if(currentTab == 'Pending Approval' || currentTab == 'Rejected') {
                    options.add(new SelectOption('OCSUGC Rejected','OCSUGC Rejected'));
                } else if(currentTab == 'Approved') { //Existing Users
                    options.add(new SelectOption('OCSUGC Rejected','OCSUGC Rejected'));
                } else if(currentTab == 'Disabled') { //Disabled Users
                    options.add(new SelectOption('OCSUGC Disabled by Admin','OCSUGC Disable by Admin'));
                }
                return options;
            }
        set;
    }
    static final Integer PAGE_SIZE = 20;
    static final String RT_Application = 'Application';
    

    public Boolean IsAdmin {
        get {
            return OCSUGC_Utilities.isManager(OCSUGC_Utilities.fatchLoginUserRole());
        }
    }
   
    public List<selectOption> getContactStatuValues() {
        return OCSUGC_Utilities.getPickValues(new Contact(), 'OCSUGC_UserStatus__c',null,null);
    }

    public class WrapperContact {
        public Contact contact {get;set;}
        public Boolean existingUser {get;set;}
        public String userStatus {get;set;}

        public WrapperContact(Contact cont, User ur) {
            this.contact = cont;
            this.existingUser = false;
            this.userStatus = '';
            if(ur != null) {
                this.existingUser = true;
                this.userStatus = ur.IsActive ? 'Active' : 'Inactive';
            }
        }
    }

    public OCSUGC_ContactCommunityUserStatusCon() {
        foundError = false;
        contactStatus = new Contact();
        selectedKnowledge = new OCSUGC_Knowledge_Exchange__c();
        actionLabel= 'Approve/Reject';

        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        countPageLoad = 0;
        currentPage = 1;      
    }
    
    public Pagereference selectedContact() {
        for(Contact con :[Select
                                Name,
                                OktaId__c,
                                OCSUGC_UserStatus__c,
                                OCSUGC_Rejection_Reason__c,
                                OCSUGC_Disabled_Reason__c,
                                OCSDC_UserStatus__c,
                                OCSUGC_Notif_Pref_EditGroup__c,
                                OCSUGC_Notif_Pref_InvtnJoinGrp__c,
                                OCSUGC_Notif_Pref_PrivateMessage__c,
                                OCSUGC_Notif_Pref_ReqJoinGrp__c
                          From Contact
                          Where Id =: selectedContactId]) {
            contactStatus = con;
        }

        return null;
    }

    public Pagereference assignSelectedKnowledgeArtificat(){
        for(OCSUGC_Knowledge_Exchange__c exchange : [Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c, OCSUGC_Rejection_Reason__c
                                            from OCSUGC_Knowledge_Exchange__c where id =: selectedKnowledgeExchangeId]){
            selectedKnowledge = exchange;
        }
        return null;
    }

    public Pagereference statusChanged() {
        system.debug('==================selectedContactId: '+selectedContactId);
        system.debug('==================contactStatus: '+contactStatus);
        return null;
    }

    public Pagereference updateContact() {
        try {
            foundError = false;
            system.debug('==================puneet selectedContactId: '+selectedContactId);
            system.debug('==================contactStatus: '+contactStatus);
            
            if(contactStatus.OCSUGC_UserStatus__c == 'OCSUGC Approved') {
                contactStatus.OCSUGC_Notif_Pref_EditGroup__c = True;
                contactStatus.OCSUGC_Notif_Pref_InvtnJoinGrp__c = True;
                contactStatus.OCSUGC_Notif_Pref_PrivateMessage__c = True;
                contactStatus.OCSUGC_Notif_Pref_ReqJoinGrp__c = True;
            }
            
            update contactStatus;

            /*if(contactStatus.OCSUGC_UserStatus__c == 'OCSUGC Rejected'){
                sendEmail(contactStatus, REJECTION_TEMPLATE_NAME);
            }else*/ if(contactStatus.OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status){ //Updated by Sumit Tanwar (OCSUGC_UserStatus__c picklist values Changed).
                if(contactStatus.OktaId__c != null){
                    //  27-01-2015  Puneet Sardana  Changed method for I-146272
                    addUserGroupInOktaWhileApproving(contactStatus.OktaId__c, 'PUT');
                    associateCakePermissionSet(contactStatus.Id);                     
                }
            }

            isCallSearchRecords = false;
            return null;
        } catch(Exception e) {
            foundError = true;
            system.debug('================Error: '+e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,OCSUGC_Utilities.refineErrorMessage(e.getMessage()));
            ApexPages.addMessage(myMsg);
            return null;
        }
    }

    
    //  27-01-2015  Puneet Sardana  Added method for I-146272
    @future(callout=true)
    public static void addUserGroupInOktaWhileApproving(String oktaId, String requestType){
        String strToken = label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');
        String endpointgroup = Label.oktaendpointforaddinggroup +'/'+ Label.OCSUGC_Okta_Group_Id + '/users/' + oktaId;//Label.oktasfdcgroupid+'/users/' + oktaid;
        reqgroup.setEndPoint(endpointgroup);
        reqgroup.setMethod(requestType);        
        try{
          system.debug('aebug ; ' +reqgroup.getBody());
          if(!Test.isRunningTest()){
           resgroup = httpgroup.send(reqgroup);           
           }else{
             resgroup = fakeresponse.fakeresponsemethod('groupaddition');
           }
           system.debug('aebug ; ' +resgroup.getBody());
           }catch(System.CalloutException e) {
            System.debug(resgroup.toString());
        }
    }
    //Commented out @future by Pratz (Appirio) to fix Issue - I-146272
    //  27-01-2015  Puneet Sardana  Removed @Future for I-146272
   // @Future(callout=true)      
    public static void updageUserGroupInOkta(String oktaId, String requestType){
        String strToken = label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');
        String endpointgroup = Label.oktaendpointforaddinggroup +'/'+ Label.OCSUGC_Okta_Group_Id + '/users/' + oktaId;//Label.oktasfdcgroupid+'/users/' + oktaid;
        reqgroup.setEndPoint(endpointgroup);
        reqgroup.setMethod(requestType);
        try{
          system.debug('aebug ; ' +reqgroup.getBody());
          if(!Test.isRunningTest()){
           resgroup = httpgroup.send(reqgroup);           
           }else{
             resgroup = fakeresponse.fakeresponsemethod('groupaddition');
           }
           system.debug('aebug ; ' +resgroup.getBody());
           }catch(System.CalloutException e) {
            System.debug(resgroup.toString());
        }
    }

    @TestVisible
    @future
    private static void associateCakePermissionSet(String conId){
        PermissionSet pSet;
        set<String> usrSet = new set<String>();
        for(PermissionSet  assignment :[Select Id From PermissionSet
                                        Where Name = :Label.OCSUGC_VMS_Member_Permission_set limit 1]) {
            pSet = assignment;
        }
        if(pSet != null) {
            for(User user : [Select OCSUGC_User_Role__c,
                                    (Select Id From PermissionSetAssignments Where PermissionSetId = :pSet.Id)
                             From User
                             Where contactId = : conId limit 1]) {

                if(user.PermissionSetAssignments.size() == 0) {
                    PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
                    perSetAssign.PermissionSetId = pSet.Id;
                    perSetAssign.AssigneeId = user.Id;

                    Savepoint sp;
                    try {
                        sp = Database.setSavepoint();
                        insert perSetAssign;
                        user.OCSUGC_User_Role__c = Label.OCSUGC_Varian_Employee_Community_Member;
                        update user;
                        usrSet.add(user.id);
                        if([Select Id from GroupMember where UserOrGroupId =: user.Id and Group.DeveloperName = 'OCSUGC_Users'].size() == 0){
                            OCSUGC_Utilities.addUserToOCSUGCUsersPublicGroup(usrSet);
                        }
                        
                        String networkId = OCSUGC_Utilities.getNetworkId('OncoPeer');
                        //OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(user.Id, networkId); 
                        OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(new List<User>{user}, networkId);
                    } catch(system.dmlException ex) {
                    	system.debug(ex.getMessage());
                        // Save Point will be rollbacked if updation process got any exception.
                        Database.rollback(sp);
                    }
                }
            }
        }
    }
    
    
    public Pagereference updateKnowledgeArtifacts(){
        if(artifactStatus != null && selectedKnowledgeExchangeId != null){
            OCSUGC_Knowledge_Exchange__c exchange = new OCSUGC_Knowledge_Exchange__c(Id = selectedKnowledgeExchangeId);
            exchange.OCSUGC_status__c = artifactStatus;
            if(artifactStatus == 'Rejected'){
                exchange.OCSUGC_Rejection_Reason__c = selectedKnowledge.OCSUGC_Rejection_Reason__c;
            }
            update exchange;
        }

        return null;
    }

    private List<Contact> getContactList(List<Object> currentList) {
        List<Contact> newList = new List<Contact>();
        for(Object obj : currentList) {
            newList.add((Contact)obj);
        }
        return newList;
    }

    public Boolean isCallSearchRecords {
        get {
            if(isCallSearchRecords == null || isCallSearchRecords == false) {
                searchRecords();
            }
            contactStatus.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
            if(currentTab == 'Pending Approval') {
                actionLabel = 'Approve/Reject';
            } else if(currentTab == 'Approved') { //Existing Users
                actionLabel = 'Edit Permissions';
            } else if(currentTab == 'Disabled') {
                actionLabel = 'Enable';
            } else if(currentTab == 'Rejected') {
                actionLabel = 'Approve';
                isRejected = true;
            }
            return true;
        }
        set;
    }

    //Method to get contact records on the basis of current tab.
    public void searchRecords() {
        String tabName = currentTab;
        System.debug('currentTab======='+currentTab);
        if(currentTab == 'Pending Approval'){
            tabName = Label.OCSUGC_Pending_Approval; //Updated by Sumit Tanwar (OCSUGC_UserStatus__c picklist values Changed).
        }
        else if(currentTab == 'Approved'){
            tabName = Label.OCSUGC_Approved_Status; //Updated by Sumit Tanwar (OCSUGC_UserStatus__c picklist values Changed).
        }
        else if(currentTab == 'Rejected'){
            tabName = 'OCSUGC Rejected'; //Updated by Sumit Tanwar (OCSUGC_UserStatus__c picklist values Changed).
        }
        else if(currentTab == 'Disabled'){
            tabName = Label.OCSUGC_Disabled_by_Admin; //Updated by Sumit Tanwar (OCSUGC_UserStatus__c picklist values Changed).
        }
        else if(currentTab == 'Disqualified'){
            tabName = Label.OCSUGC_Disqualified;
        }
        isCallSearchRecords = true;
        String query =  'Select MyVarian_Member__c, Id, Name, Email, Inactive_Contact__c, OCSUGC_UserStatus__c, CreatedDate, OCSUGC_Rejection_Reason__c, OCSUGC_Disabled_Reason__c ';
        query += 'FROM Contact ';
        query += 'WHERE OCSUGC_UserStatus__c Like \'%'+tabName+'%\' ';
        if(currentTab == 'Disabled'){
            query += 'OR OCSUGC_UserStatus__c Like \'%'+Label.OCSUGC_Disabled_by_Self+'%\' ';
        }
        query += 'Order By SystemModStamp DESC LIMIT 5000';

        system.debug('==========query: '+query);

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(PAGE_SIZE);

        if(currentTab.equals('Pending Approval')) { //Pending Approval Users
            title = 'There are '+con.getResultSize()+' pending users';
        }
        if(currentTab.equals('Approved')) { //Existing Users
            title = 'There are '+con.getResultSize()+' '+currentTab.toLowerCase()+' users';
        }
        if(currentTab.contains('Disabled')) { //Disabled Users
            title = 'There are '+con.getResultSize()+' '+currentTab.toLowerCase()+' users';
        }
        if(currentTab.equals('Rejected')) { //Rejected Users
            title = 'There are '+con.getResultSize()+' '+currentTab.toLowerCase()+' users';
        }
        if(currentTab.equals('Disqualified')) { //Disqualified Users
            title = 'There are '+con.getResultSize()+' '+currentTab.toLowerCase()+' users';
        }

        if(currentTab == 'Pending Approval File') {
            //Changes done by Shital Bhujbal to remove deleted files from pending approvals list
            //query = 'Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c from OCSUGC_Knowledge_Exchange__c where OCSUGC_Status__c = \'Pending Approval\'';
            query = 'Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c from OCSUGC_Knowledge_Exchange__c where OCSUGC_Status__c = \'Pending Approval\' AND OCSUGC_Is_Deleted__c=false';
            conKA = new ApexPages.StandardSetController(Database.getQueryLocator(query));
            conKA.setPageSize(PAGE_SIZE);
            totalRequestsKnowledge = conKA.getResultSize();
            String networkName;
//            if(OCSUGC_Utilities.objNetwork != null) {
                //Code changes done by Shital Bhujbal
                //networkName = System.label.OCSUGC_Salesforce_Instance+'/' + OCSUGC_Utilities.getCurrentCommunityNetworkName(false) +'/';
                
                networkName = System.label.OCSUGC_Salesforce_Instance+'/'+Label.OCSUGC_NetworkName+'/apex/';
//          }
            kaDetailLink  = networkName+'OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId=';
            title = 'There are '+totalRequestsKnowledge+' '+currentTab.toLowerCase()+' ';
        }

    }

    public List<WrapperContact> getWrapperContacts() {
        List<WrapperContact> wrappers = new List<WrapperContact>();
        mapContactUser = new Map<String, User>();
        if(con == null)
             return wrappers;
        for(User ur :[Select ContactId, IsActive From User Where ContactId IN :con.getRecords()]) {
            mapContactUser.put(ur.ContactId,ur);
        }
        for(Contact cont :(List<Contact>) con.getRecords()) {
            wrappers.add(new WrapperContact(cont,mapContactUser.get(cont.Id)));
        }
        return wrappers;
    }

    public PageReference First() {
        if(con == null)
            return null;
        con.first();
        return null;
    }

    public PageReference Previous() {
         if(con.getHasPrevious())
             con.previous();
         return null;
    }

    public PageReference Next() {
         if(con.getHasNext())
             con.next();
         return null;
    }

    public PageReference Last() {
        if(con == null)
            return null;
        con.last();
        return null;
    }

    public PageReference FirstAP() {
        if(conAP == null)
            return null;
        conAP.first();
        return null;
    }

    public PageReference PreviousAP() {
         if(conAP == null)
             return null;
         if(conAP.getHasPrevious())
             conAP.previous();
         return null;
    }

    public PageReference NextAP() {
        if(conAP == null)
             return null;
         if(conAP.getHasNext())
             conAP.next();
         return null;
    }

    public PageReference LastAP() {
        if(conAP == null)
            return null;
        conAP.last();
        return null;
    }


    public List<OCSUGC_Knowledge_Exchange__c> getKnowledgeArtifactsRequests() {
        if(conKA == null)
             return new List<OCSUGC_Knowledge_Exchange__c>();

        return (List<OCSUGC_Knowledge_Exchange__c>) conKA.getRecords();
    }

    public PageReference FirstKA() {
        if(conKA == null)
            return null;
        conKA.first();
        return null;
    }

    public PageReference PreviousKA() {
         if(conKA.getHasPrevious())
             conKA.previous();
         return null;
    }

    public PageReference NextKA() {
         if(conKA.getHasNext())
             conKA.next();
         return null;
    }

    public PageReference LastKA() {
        if(conKA == null)
            return null;
        conKA.last();
        return null;
    }

    // A Method which used for send email to a SObject
    public void sendEmail(SObject sObj, String templateName){
        //retrieve the Email Template IDs
        EmailTemplate et = [Select e.Id, e.DeveloperName From EmailTemplate e where e.DeveloperName = :templateName];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId( et.Id );
        mail.setTargetObjectId( sObj.Id );
        // Use Organization Wide Address
        mail.setOrgWideEmailAddressId(orgWideEmailAddressId);

        try {
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
        } catch(Exception e) {
            system.debug('================Error: '+e.getMessage());
        }
    }
}
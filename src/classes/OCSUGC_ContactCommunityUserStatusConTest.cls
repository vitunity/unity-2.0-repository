@isTest(SeeAllData=false)
public class OCSUGC_ContactCommunityUserStatusConTest {
    static CollaborationGroup collabGroup;
    static OCSUGC_Knowledge_Exchange__c kExchng;
    static Network ocsugcNetwork;
    static Profile admin,portal;
    static Profile manager;  
   // static RecordType rt;
    static{
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile();  
        manager= OCSUGC_TestUtility.getManagerProfile();
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
        collabGroup = OCSUGC_TestUtility.createGroup('TestGroup','Public',ocsugcNetwork.id,true);
        kExchng = OCSUGC_TestUtility.createKnwExchange(collabGroup.id, 'TestGroup',true);
                
    }
    static testMethod void testTabs() {
        List<String> tabNames = new List<String>{'Pending Approval','Approved','Rejected','Disabled','Disqualified','Pending Approval File','CommentsByVarian', 'Settings'};
        List<Contact> contacts = new List<Contact>();
        Account acc = OCSUGC_TestUtility.createAccount('TestAccount', true);
        
        for(Integer i = 0; i < tabNames.size(); i++) {
            Contact con = new Contact();
            con.AccountId = acc.id;
            con.OCSUGC_UserStatus__c = tabNames[i];
            con.FirstName='TestFirstName'+Math.random();
            con.LastName = 'TestContact' + Math.random();
            con.Email = 'TestUser' +system.now().millisecond() + '@test.com';
            con.MailingCountry='India';
            con.MyVarian_member__c = true;
            con.PasswordReset__c = false;   
            if(i == 1) {
                con.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
                con.OktaId__c = '00ub0oNGTSWTBKOLGLNR';
            } else {
                con.OCSUGC_UserStatus__c = 'OCSUGC Rejected';
            } 
            contacts.add(con);
        }
        insert contacts;
        OCSUGC_ContactCommunityUserStatusCon controller = new OCSUGC_ContactCommunityUserStatusCon();
       // User adminUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '1','testAdminRole1');
       // insert adminUsr;
       //  System.runAs(adminUsr) {
           
        List<RecordType> rt = [select id,Name from RecordType where SobjectType='OCSUGC_Intranet_Content__c' and Name='Applications' Limit 1];
        //}
        for(Integer i = 0; i < tabNames.size(); i++) {
            controller.selectedContactId = contacts[i].id;
            OCSUGC_Intranet_Content__c IContent = new OCSUGC_Intranet_Content__c();
            if(rt != null) {
                IContent.recordTypeId = rt.get(0).id;
            }
            if(i != 1) {
                IContent.OCSUGC_Application_Grant_Access__c = true;
            } else {
                IContent.OCSUGC_Application_Grant_Access__c = false;
            }
            insert IContent;
            //OCSUGC_Application_Permissions__c appPermission = new OCSUGC_Application_Permissions__c();
            //appPermission.OCSUGC_Contact__c = contacts[i].id;
            //appPermission.OCSUGC_Application__c = Icontent.id;
            //insert appPermission;
            
            controller.selectedContact();
            
            controller.currentTab = tabNames[i];
            controller.searchRecords();
        }
        Test.startTest();
        controller.selectedContactId = contacts[0].id;
        controller.contactStatus = contacts[0];
        controller.updateContact();
        
        controller.selectedContactId = contacts[1].id;
        controller.contactStatus = contacts[1];
        controller.updateContact();
        controller.selectedContact();
        
        Test.stopTest();        
    }
    
    static testMethod void testUpdateKnowledgeArtifacts() {
        OCSUGC_ContactCommunityUserStatusCon controller = new OCSUGC_ContactCommunityUserStatusCon();
        //CollaborationGroup collabGroup = OCSUGC_TestUtility.createGroup('TestGroup','Public',true);
        //OCSUGC_Knowledge_Exchange__c kExchng = OCSUGC_TestUtility.createKnwExchange(collabGroup.id, 'TestGroup',true);
        controller.selectedKnowledgeExchangeId = kExchng.id;
        controller.assignSelectedKnowledgeArtificat();
        controller.artifactStatus = 'Rejected';
        controller.updateKnowledgeArtifacts();
        
    }
    
    static testMethod void testUpdageUserGroupInOkta() {
        OCSUGC_ContactCommunityUserStatusCon.updageUserGroupInOkta('00ub0oNGTSWTBKOLGLNR','GET');
    }
	static testMethod void testCommunityUserStatus() {
     	
	    User managerUsr,contributorUsr,memberUsr,usr4,usr5;
	    User adminUsr1,adminUsr2;
	    Account account; 
	    Contact contact1,contact2,contact3,contact4,contact5;
	    List<User> lstUserInsert;
	    List<Contact> lstContactInsert;
	    List<OCSUGC_Application_Permissions__c> applicationPermissions;
	    //OCSUGC_Application_Permissions__c applicationPermission1,applicationPermission2,applicationPermission3;
	    OCSUGC_Intranet_Content__c testContent;
	     
	    lstUserInsert = new List<User>();   
	    lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '1','testAdminRole1'));
	
	    account = OCSUGC_TestUtility.createAccount('test account', true);
	    lstContactInsert = new List<Contact>();
	    lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
	    insert lstContactInsert;
	    testContent = OCSUGC_TestUtility.createIntranetContent('test', 'Applications', null, 'test', 'content');
	    insert testContent;
	
	    lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
	    lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));    
	    insert lstUserInsert;
	    Test.startTest();
		    System.runAs(adminUsr1) {
	        	Set<String> setPermissionSetNames = new Set<String>();
		        	setPermissionSetNames.add(Label.OCSUGC_VMS_Manager_Permission_set);         //Varian Community Manager
		         	setPermissionSetNames.add(Label.OCSUGC_VMS_Contributor_Permission_set);     //Varian Community Contributor
		         	setPermissionSetNames.add(Label.OCSUGC_VMS_Member_Permission_set);          //Varian Community Member
	         	List<PermissionSet>  permissionSetList = new List<PermissionSet>([Select Id, Name From PermissionSet Where Name IN :setPermissionSetNames]);
	    		delete [Select Id From PermissionSetAssignment Where AssigneeId =:lstUserInsert[2].Id And PermissionSetId IN :permissionSetList];
	        	OCSUGC_ContactCommunityUserStatusCon.associateCakePermissionSet(lstContactInsert[2].Id);
	    	}  
	        
	        OCSUGC_ContactCommunityUserStatusCon.associateCakePermissionSet(lstContactInsert[2].Id);
	        
	        OCSUGC_ContactCommunityUserStatusCon controller = new OCSUGC_ContactCommunityUserStatusCon();
	        controller.selectedContactId = contact1.Id;
	        controller.selectedContact();
	        //controller.selectedRequestId = applicationPermission2.Id;
	        controller.currentTab = 'Approved';
	        controller.searchRecords();
	        controller.First();
	        controller.Last();
	        controller.Previous();
	        controller.Next();
	        
	        CollaborationGroup publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,true);
	        OCSUGC_Knowledge_Exchange__c exchange = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id, publicGroup.Name, false);
	        exchange.OCSUGC_Status__c = 'Pending Approval';
	        insert exchange;
	        controller.currentTab = 'Pending Approval File';
	        controller.isCallSearchRecords = true;
	        controller.searchRecords();
	        controller.NextKA();      
	        controller.LastKA();
	        controller.FirstKA();
	        controller.PreviousKA(); 
	        controller.currentTab ='Pending Approval';
	        controller.searchRecords();
	        controller.LastAP();
	        controller.FirstAP();
	        controller.NextAP();
	        controller.PreviousAP();
	        controller.getKnowledgeArtifactsRequests();
	        controller.statusChanged(); 
	        controller.getWrapperContacts();
	        
	        OCSUGC_ContactCommunityUserStatusCon.WrapperContact wrap = new OCSUGC_ContactCommunityUserStatusCon.WrapperContact(contact2,managerUsr);
      		Test.stopTest();
    }
    static testMethod void testMisc() {
        OCSUGC_ContactCommunityUserStatusCon controller = new OCSUGC_ContactCommunityUserStatusCon();
        controller.currentTab = 'Pending Approval';
        system.debug('statusValuesCake ' + controller.statusValuesCake);    
        controller.currentTab = 'Approved';
        system.debug('statusValuesCake ' + controller.statusValuesCake);    
        system.debug('IsAdmin ' + controller.IsAdmin);    
            controller.getContactStatuValues();
        controller.getWrapperContacts();
    }
}
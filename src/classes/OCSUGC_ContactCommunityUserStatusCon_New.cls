// 08 Sept 2015			Puneet Mishra,	New OCSUGC Admin Page's Component [OCSUGC_ContactCommunityUserStatus_New] Controller
public without sharing class OCSUGC_ContactCommunityUserStatusCon_New {
	
	public String statusContact {get;set;}					// stores the status of the Contact Status Picklist value
	public String status {get;set;}
	public Boolean isKA {get;set;}							// Boolean parameter to find if accessing Knowledge Artifacts for Approval or Rejection.					
	public String userManagement {get;set;}					// Store Link for User-Management Page [ Need to store it to Custom Label ]
	public String kaDetailLink {get;set;}					// Store Link for Knowledge-Artifact Page [ Need to store it to Custom Label ]
	public String selectedKnowledgeExchangeId {get;set;}	// Id of Selected Knowledge Article for Status Change (Action: Approce/Reject)
	public String selectedContactId {get;set;}				// Id of Contact Selected for Status Changed (Action: Approve/Reject)
	public String artifactStatus {get;set;}
	public Contact contactStatus {get;set;}					// Contact which is get updated
	public OCSUGC_Knowledge_Exchange__c selectedKnowledge {get;set;}	// Knowledge Article whose status to be updated
	public String accountName {get;set;}					// stores the account Name to search Contacts
    public String country {get;set;}						// stores the Country to search the Contact
	public ApexPages.StandardSetController conKA {get;set;} // Standard Controller for Knowledge Article
	
	public Boolean customSetting {get;set;}
	// Picklist value for Countries
	public List<SelectOption> getCountries() {
      List<SelectOption> options = new List<SelectOption>();
      
      // Describe will get Account's Country__c details
      Schema.DescribeFieldResult fieldResult = Account.Country__c.getDescribe();
      List<Schema.PickListEntry> ple = fieldResult.getPickListValues();		// get the Country__c picklist values
      options.add(new SelectOption('',''));
      for( Schema.PickListEntry f : ple ) {
        options.add(new SelectOption(f.getLabel(), f.getValue()));
      }
      return options;
    }
	
	// Contact Status Picklist Values, Shown with different Value on Page
	public List<SelectOption> getContactStatusList() {
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption(Label.OCSUGC_Approved_Status,'Current Users'));
        options.add(new SelectOption(Label.OCSUGC_Disabled_By_Self, 'Disabled Users'));
        options.add(new SelectOption(Label.OCSUGC_Rejected, 'Rejected Users'));
        options.add(new SelectOption(Label.OCSUGC_Disqualified, 'Disqualified Users'));
        return options;
    }
    
    public List<selectOption> statusValuesCake { // Only picklist values which have "Cake" in text
        get{
            List<SelectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
            if(statusContact == Label.OCSUGC_Pending_Approval || statusContact == Label.OCSUGC_Approved_Status || statusContact == Label.OCSUGC_Rejected) {
                            
                options.add(new SelectOption(Label.OCSUGC_Approved_Status,Label.OCSUGC_Approved_Status));
                options.add(new SelectOption('OCSUGC Rejected','OCSUGC Rejected'));
                            
            } else if(statusContact == Label.OCSUGC_Disabled_by_Admin || statusContact == Label.OCSUGC_Disabled_by_Self) {
                    
                options.add(new SelectOption(Label.OCSUGC_Approved_Status,Label.OCSUGC_Approved_Status));
                options.add(new SelectOption(Label.OCSUGC_Disabled_by_Admin,Label.OCSUGC_Disabled_by_Admin));
                    
            } else if (statusContact == Label.OCSUGC_Disqualified) {                
            } else {
                options.add(new SelectOption(Label.OCSUGC_Approved_Status,Label.OCSUGC_Approved_Status));
                options.add(new SelectOption(Label.OCSUGC_Rejected,Label.OCSUGC_Rejected));
            }
            return options;
            }
        set;
    }
	
	private List<Contact> contactList;					// List of Contact with Status selected on Admin Page
	// Map for mapping the Status Value
	private Map<String, String> statusValueMap = new Map<String, String>{Label.OCSUGC_Approved_Status => 'OCSUGC_Approved',
                                                                         Label.OCSUGC_Disabled_By_Self => 'OCSUGC_Disabled_by_Self',
                                                                         Label.OCSUGC_Rejected => 'OCSUGC_Rejected',
                                                                         Label.OCSUGC_Disqualified => 'OCSUGC_Disqualified',
                                                                         Label.OCSUGC_Disabled_By_Admin => 'OCSUGC_Disabled_by_Admin'};

	private Map<String, String> labelValueMap = new Map<String, String>{Label.OCSUGC_Approved_Status => Label.OCSUGC_Approved_Status,
									                                    Label.OCSUGC_Disabled_By_Self => Label.OCSUGC_Disabled_By_Self,
									                                    Label.OCSUGC_Rejected => Label.OCSUGC_Rejected,
									                                    Label.OCSUGC_Disqualified => Label.OCSUGC_Disqualified
									                                    };

	public OCSUGC_ContactCommunityUserStatusCon_New() {
		userManagement = '/apex/OCSUGC_CommunityUserAdministration_New?KA=false';
        selectedKnowledge = new OCSUGC_Knowledge_Exchange__c();
        status = ApexPages.currentPage().getParameters().get('status'); 
		
		Map<String, String> statusPick = new Map<String, String>{'OCSUGC_Approved' => 'Current Users',
                                                                 'OCSUGC_Disabled_by_Self' => 'Disabled Users',
                                                                 'OCSUGC_Rejected' => 'Rejected Users',
                                                                 'OCSUGC_Disqualified' => 'Disqualified User'};
        
        if(status != null) {
          statusContact = (status);
        } else {
          statusContact = 'Current Users';
        }
        customSetting = ApexPages.currentPage().getParameters().get('setting') == 'true' ? true:false;
        isKA = ApexPages.currentPage().getParameters().get('KA') == 'true' ? true:false;
        if(isKA)
            fetchKnowledgeArticles();
    }
	
	// Wrapper class for Contact and User Details
	public Class WrapperContact {
		public Contact contact {get;set;}
		public User user {get;set;}
		
		public WrapperContact( Contact contact, User user ) {
			this.contact = contact;
			this.user = user;
		}
	}
	
	public List<wrapperContact> getWrapperList() {
        Map<Id, User> userMap = new Map<Id, User>();
        String contactQuery = '';
        List<wrapperContact> wrappers;
        contactList = new List<Contact>();
        accountName = ApexPages.CurrentPage().getParameters().get('acc');
        country = ApexPages.CurrentPage().getParameters().get('count');
        
        contactQuery += 'Select Id, Account.Name, Account.Country__c, FirstName, LastName, OCSUGC_UserStatus__c, Inactive_Contact__c, MyVarian_Member__c, ';
        contactQuery += ' OCSDC_UserStatus__c, Phone, Institute_Name__c  From Contact';
        
        Map<String, String> disabledMap = new Map<String, String>{'OCSUGC Disabled by Admin'=>'OCSUGC Disabled by Admin','OCSUGC Disabled by Self'=>'OCSUGC Disabled by Self'};
        
        List<String> disableList = disabledMap.values();
        if(disabledMap.containsKey(status)) {
            contactQuery += ' WHERE OCSUGC_UserStatus__c IN : disableList ' ;
        } else if(labelValueMap.containsKey(status)) {
            contactQuery += ' WHERE OCSUGC_UserStatus__c = \'' + labelValueMap.get(status) + '\' ';
        } else {
            contactQuery += ' WHERE OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\' ';
        }
        if(accountName != null && accountName != '') {
          contactQuery += ' AND Account.Name LIKE ' + '\'%' + accountName.replaceAll('_',' ') + '%\'';
          accountName = accountName.replaceAll('_', ' ');
        }
        
        if(country != null && country != '') {
          contactQuery += ' AND Account.Country__c = \'' + country.replaceAll('_', ' ') + '\' ';
          country = country.replaceAll('_',' ');
        }
        
        contactQuery += ' Order By SystemModStamp DESC LIMIT 1000';
        
        contactList = Database.Query(contactQuery);
        
        for(User u : [SELECT Id, Email, ContactId, LastLoginDate, LastModifiedDate FROM User Where isActive =: true AND ContactId IN: contactList 
        				ORDER BY LastLoginDate NULLS Last]) {
            userMap.put(u.contactId, u);
        }
        
        wrappers = new List<wrapperContact>();
        for(Contact cont : contactList) {
            if(userMap.containsKey(cont.Id))
                wrappers.add(new wrapperContact( cont, userMap.get(cont.Id) ));
        }
        
        return wrappers;
    }
	
	// fetch the Contact with Selected Contact Id from Page
	public PageReference selectedContact() {
		contactStatus = new Contact();
        for(Contact con :[Select Name, OktaId__c, OCSUGC_UserStatus__c,OCSUGC_Rejection_Reason__c,OCSUGC_Disabled_Reason__c,OCSDC_UserStatus__c,OCSUGC_Notif_Pref_EditGroup__c,
                                 OCSUGC_Notif_Pref_InvtnJoinGrp__c,OCSUGC_Notif_Pref_PrivateMessage__c,OCSUGC_Notif_Pref_ReqJoinGrp__c
                          From Contact Where Id =: selectedContactId]) {
            contactStatus = con;
        }
        return null;
	}
	
	// Reload the page with search Parameters (Account Name, Country) from page
	public pageReference reload() {
		String url = '/apex/OCSUGC_CommunityUserAdministration_New?KA=false&status=' + statusContact;//statusValueMap.get(statusContact);
	  
		if(accountName != null && accountName != '') {
			url += '&acc=' + accountName.replaceAll(' ', '_');
		}
		if(country != null && country != '') {
			url += '&count=' + country.replaceAll(' ', '_');
		}
	  
		PageReference pageRef = new PageReference(url);
		pageRef.setRedirect(true);    
		return pageRef;
	}
	
	public Pagereference updateContact() {
		try {
			if(contactStatus.OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status) {
				contactStatus.OCSUGC_Notif_Pref_EditGroup__c = True;
				contactStatus.OCSUGC_Notif_Pref_InvtnJoinGrp__c = True;
				contactStatus.OCSUGC_Notif_Pref_PrivateMessage__c = True;
				contactStatus.OCSUGC_Notif_Pref_ReqJoinGrp__c = True;
				if(contactStatus.OktaId__c != null){
					//  27-01-2015  Puneet Sardana  Changed method for I-146272
					addUserGroupInOktaWhileApproving(contactStatus.OktaId__c, 'PUT');
					associateCakePermissionSet(contactStatus.Id);                     
				}
			} else if(contactStatus.OCSUGC_UserStatus__c == Label.OCSUGC_Rejected) {
				contactStatus.OCSUGC_Notif_Pref_EditGroup__c = True;
				contactStatus.OCSUGC_Notif_Pref_InvtnJoinGrp__c = True;
				contactStatus.OCSUGC_Notif_Pref_PrivateMessage__c = True;
				contactStatus.OCSUGC_Notif_Pref_ReqJoinGrp__c = True;
			}
			update contactStatus;
			PageReference pageRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=false');
			pageRef.setRedirect(true);
			return pageRef;
			//return null;
		} catch(Exception e) {
			system.debug('====Exception Details : ' + e.getMessage() + e.getLineNumber());
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,OCSUGC_Utilities.refineErrorMessage(e.getMessage()));
			ApexPages.addMessage(myMsg);
			return null;
		}
	}
    
    //  27-01-2015  Puneet Sardana  Added method for I-146272
	@future(callout=true)
	public static void addUserGroupInOktaWhileApproving(String oktaId, String requestType){
		String strToken = label.OCSUGC_Okta_Token;
		String authorizationHeader = 'SSWS ' + strToken;
		HttpRequest reqgroup = new HttpRequest();
		HttpResponse resgroup = new HttpResponse();
		Http httpgroup = new Http();
		reqgroup.setHeader('Authorization', authorizationHeader);
		reqgroup.setHeader('Content-Type','application/json');
		reqgroup.setHeader('Accept','application/json');
		String endpointgroup = Label.oktaendpointforaddinggroup +'/'+ Label.OCSUGC_Okta_Group_Id + '/users/' + oktaId;//Label.oktasfdcgroupid+'/users/' + oktaid;
		reqgroup.setEndPoint(endpointgroup);
		reqgroup.setMethod(requestType);        
		try{
			system.debug('aebug ; ' +reqgroup.getBody());
			if(!Test.isRunningTest()){
				resgroup = httpgroup.send(reqgroup);           
			}else{
				resgroup = fakeresponse.fakeresponsemethod('groupaddition');
			}
			system.debug('aebug ; ' +resgroup.getBody());
		}catch(System.CalloutException e) {
			System.debug(resgroup.toString());
		}
	}
	
	@TestVisible
    @future
    private static void associateCakePermissionSet(String conId){
		PermissionSet pSet;
        set<String> usrSet = new set<String>();
        for(PermissionSet  assignment :[Select Id From PermissionSet
                                        Where Name = :Label.OCSUGC_VMS_Member_Permission_set limit 1]) {
            pSet = assignment;
        }
        if(pSet != null) {
            for(User user : [Select OCSUGC_User_Role__c,
                                    (Select Id From PermissionSetAssignments Where PermissionSetId = :pSet.Id)
                             From User
                             Where contactId = : conId limit 1]) {

			if(user.PermissionSetAssignments.size() == 0) {
                    PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
                    perSetAssign.PermissionSetId = pSet.Id;
                    perSetAssign.AssigneeId = user.Id;

                    Savepoint sp;
                    try {
                        sp = Database.setSavepoint();
                        insert perSetAssign;
                        user.OCSUGC_User_Role__c = Label.OCSUGC_Varian_Employee_Community_Member;
                        update user;
                        usrSet.add(user.id);
                        if([Select Id from GroupMember where UserOrGroupId =: user.Id and Group.DeveloperName = 'OCSUGC_Users'].size() == 0){
                            OCSUGC_Utilities.addUserToOCSUGCUsersPublicGroup(usrSet);
                        }
                        
                        String networkId = OCSUGC_Utilities.getNetworkId('OncoPeer');
                        //OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(user.Id, networkId); 
                        OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(new List<User>{user}, networkId);
                    } catch(system.dmlException ex) {
                        system.debug(ex.getMessage());
                        // Save Point will be rollbacked if updation process got any exception.
                        Database.rollback(sp);
                    }
                }
            }
        }
    }
	
	@TestVisible
    private void fetchKnowledgeArticles() {
    	String queryKA = 'Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c from OCSUGC_Knowledge_Exchange__c where OCSUGC_Status__c = \'Pending Approval\' AND OCSUGC_Is_Deleted__c=false';
        conKA = new ApexPages.StandardSetController(Database.getQueryLocator(queryKA));
        kaDetailLink  = System.label.OCSUGC_Salesforce_Instance+'/'+Label.OCSUGC_NetworkName+'/apex/' +'OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId=';
    }
    
    public List<OCSUGC_Knowledge_Exchange__c> getKnowledgeArtifactsRequests() {
		if(conKA == null)
			return new List<OCSUGC_Knowledge_Exchange__c>();
		return (List<OCSUGC_Knowledge_Exchange__c>) conKA.getRecords();
    }
	
	// update the Knowledge Article Status from Pending to Approved
    public Pagereference updateKnowledgeArtifacts(){
        if(artifactStatus != null && selectedKnowledgeExchangeId != null){
            OCSUGC_Knowledge_Exchange__c exchange = new OCSUGC_Knowledge_Exchange__c(Id = selectedKnowledgeExchangeId);
            exchange.OCSUGC_status__c = artifactStatus;
            if(artifactStatus == 'Rejected'){
                exchange.OCSUGC_Rejection_Reason__c = selectedKnowledge.OCSUGC_Rejection_Reason__c;
            }
            update exchange;
        }
        PageReference pageRef = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?KA=true');
        pageRef.setRedirect(true);
        return pageRef;
    }
	
	public Pagereference assignSelectedKnowledgeArtificat(){
        for(OCSUGC_Knowledge_Exchange__c exchange : [Select ID, OCSUGC_Title__c, OCSUGC_Summary__c, OCSUGC_Status__c, OCSUGC_Content_Sensitivity__c, OCSUGC_Artifact_Type__c, 
        													OCSUGC_Rejection_Reason__c
                                            		FROM OCSUGC_Knowledge_Exchange__c WHERE id =: selectedKnowledgeExchangeId]){
            selectedKnowledge = exchange;
        }
        return null;
    }
	
}
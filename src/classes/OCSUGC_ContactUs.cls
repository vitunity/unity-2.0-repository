//
// (c) 2012 Appirio, Inc.
//
// This page will allow the user to Contact the OCSUGC Admins public group from a link on the Home Page.
//
// 5 Aug 2014    Jai Shankar Pandey    Created the controlloer to send Email to all OCSUGC Admins public group users(directly associated Users)
public without sharing class OCSUGC_ContactUs {
    //used to hold the user message which has to be emailed
    public String strFeedBackMessage {get;set;}
    public String userFirstName {get;set;}
    public String userLastName {get;set;}
    public String userEmail {get;set;}
    public boolean isLoggedInUser {get;set;}
    //Will tell whether the confirmation message can be shown to the user, once Email is send out.
    public boolean isEmailSend {get;set;} 
    public string strEmailStatusMsg {get;set;}
    //Static final variable for querying Email Template and Admin Group
    private static final String OCSUGC_ADMIN_PUBLIC_GROUP_EMAIL_TEMPLATE = 'OCSUGC_Email_From_ContactUs_To_Admin_Group';
    private static final String GUEST_USER_TYPE = 'guest';
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }

    //constructor
    public OCSUGC_ContactUs()
    {
        strEmailStatusMsg = strFeedBackMessage = '';
        isEmailSend = false;
        isLoggedInUser = GUEST_USER_TYPE.containsIgnoreCase(UserInfo.getUserType()) ? false : true;
        this.verified = false;
    }
    //  sending an email to public group 'OCSUGC Admins' based on the Email Template
    //  argument none
    //  @return none
    public void btnsubmitMessageAndEmailToPublicGroup()
    {
        //this confirms that email has send.
        isEmailSend = true;
        //Email status message portray whether Email send sucessfully or not.
        strEmailStatusMsg = Label.OCSUGC_EmailSuccessfullySent;
        //list will contain Email drafts which needs to be send
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

        Id orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        //Query and fetch the Email Template Format for sending email to Public Group
        EmailTemplate objEmailTemplate = [SELECT Id,
                                                 Subject,
                                                 HtmlValue,
                                                 Body
                                           FROM EmailTemplate
                                           WHERE DeveloperName = :OCSUGC_ADMIN_PUBLIC_GROUP_EMAIL_TEMPLATE
                                           LIMIT 1];

        String strHtmlBody = objEmailTemplate.HtmlValue;
        String strPlainBody = objEmailTemplate.Body;
        //For logged in user show user name and email
        if(isLoggedInUser)
        {
            userFirstName = UserInfo.getFirstName();
            userLastName = UserInfo.getLastName();
            userEmail = UserInfo.getUserEmail();
        }
        // replace the logged in user's name dynamically in HTMLBody
        // Start Added Label Value Naresh T-363912
  		strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
  		// End
        strHtmlBody = strHtmlBody.replace('USER_NAME', userFirstName+' '+userLastName);
        strHtmlBody = strHtmlBody.replace('USER_EMAIL', userEmail);
        // replace the logged in user's name dynamically in Plain Body
        strPlainBody = strPlainBody.replace('USER_NAME', userFirstName+' '+userLastName);
        strPlainBody = strPlainBody.replace('USER_EMAIL', userEmail);
        //replace the content message dynamically
        strHtmlBody = strHtmlBody.replace('FEEDBACK_MESSAGE', strFeedBackMessage);
        strPlainBody = strPlainBody.replace('FEEDBACK_MESSAGE', strFeedBackMessage);

        //Query and fetch all the Group member associated with OCSUGC Admins public group
        for(GroupMember objGroupMember : [SELECT Id,
                                                 UserOrGroupId
                                          FROM GroupMember
                                          WHERE Group.DeveloperName = :OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP])
        {
            //filter only direct associated Users
            if(String.ValueOf(objGroupMember.UserOrGroupId).startsWith('005'))
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setSaveAsActivity(false);
                //
                // Use Organization Wide Address
                                if(orgWideEmailAddressId != null)
                                    email.setOrgWideEmailAddressId(orgWideEmailAddressId);
                                else 
                                  email.setSenderDisplayName('OCSUGC');
                email.setSubject(objEmailTemplate.Subject);
                email.setHtmlBody(strHtmlBody);
                email.setPlainTextBody(strPlainBody);
                email.setTargetObjectId(objGroupMember.UserOrGroupId);
                lstEmail.add(email);
            }
        }
        try{
            List<Messaging.SendEmailResult> results =new List<Messaging.SendEmailResult>();
            //Sending Email here
            if(!lstEmail.IsEmpty())
                results = Messaging.sendEmail(lstEmail);
            //iterate the result and find if email fails to sent
            for(Messaging.SendEmailResult result : results)
            {
              if(!result.IsSuccess())
                strEmailStatusMsg = Label.OCSUGC_EmailNotSuccessfullySent;
            }
        }
        catch(Exception ex){
            system.debug('========>>>'+ex.getMessage());
        }
    }
    
    
    
    // ***************** Captcha *************
    private static String baseUrl = 'https://www.google.com/recaptcha/api/verify';
     
        // The keys you get by signing up for reCAPTCHA for your domain
        private static String privateKey = system.label.Capcha_PrivateKey;
        public String publicKey {
            get { system.debug('99999999' + challenge);return system.label.Capcha_PublicKey; }
        }    
                
                
        // Create properties for the non-VF component input fields generated
        // by the reCAPTCHA JavaScript.
        public String challenge {
            get {
                system.debug('@@@@@@@@@' + ApexPages.currentPage().getParameters().get('recaptcha_challenge_field'));
                return ApexPages.currentPage().getParameters().get('recaptcha_challenge_field');
            }
        }
        public String response  {
            get {
                return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
            }
        }
         
        
        
        public PageReference verify() {
        	System.debug('reCAPTCHA verification attempt');
        	// On first page load, form is empty, so no request to make yet
        	if ( challenge == null || response == null ) { 
            	System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        	}
        	
        	HttpResponse r = makeRequest(baseUrl,
            'privatekey=' + privateKey + 
            '&remoteip='  + remoteHost + 
            '&challenge=' + challenge +
            '&response='  + response
	        );
	        if ( r!= null ) {
	            this.verified = (r.getBody().startsWithIgnoreCase('true'));
	        }
	        
	        if(this.verified) {
	            //PageReference pr = new PageReference('/apex/OCSUGC_Login_ContactUs');
                   // pr.setredirect(true);
                    //return pr;     
	            btnsubmitMessageAndEmailToPublicGroup();
	            return null;
	        }
	        else {
	            // stay on page to re-try reCAPTCHA              
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Invalid Captcha Code. Please Try Again.');
                ApexPages.addMessage(myMsg);
                return null; 
	        }    
    	}
    	
    	public PageReference reset() {
        	return null; 
    	}   

	    /* Private helper methods */
	    
	    private static HttpResponse makeRequest(string url, string body)  {
	        HttpResponse response = null;
	        HttpRequest req = new HttpRequest();   
	        req.setEndpoint(url);
	        req.setMethod('POST');
	        req.setBody (body);
	        try {
	            Http http = new Http();
	            response = http.send(req);
	            System.debug('reCAPTCHA response: ' + response);
	            System.debug('reCAPTCHA body: ' + response.getBody());
	        } catch(System.Exception e) {
	            System.debug('ERROR: ' + e);
	        }
	        return response;
	    }   
	        
	    private String remoteHost { 
	        get { 
	            String ret = '127.0.0.1';
	            // also could use x-original-remote-host 
	            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
	            if (hdrs.get('x-original-remote-addr')!= null)
	                ret =  hdrs.get('x-original-remote-addr');
	            else if (hdrs.get('X-Salesforce-SIP')!= null)
	                ret =  hdrs.get('X-Salesforce-SIP');
	            return ret;
	        }
	    }
	    
    
    //  redirecting to the OCSUGC_Home page when click on cancel button
    //  argument.
    //  @return a PageReference that is redirecting to OCSUGC_Home page.
    public pageReference btnCancel()
    {
        //redirect the page to OCSUGC home page
        Pagereference pg = Page.OCSUGC_Home;
        pg.getParameters().put('isResetPwd','true');
        pg.setRedirect(true);
        return pg;
    }
}
// (c) 2012 Appirio, Inc.
//
// Test class  for OCSUGC_ContactUs
//
// 6 Aug 2014    Jai Shankar Pandey   Test class  for OCSUGC_ContactUs
// 15 Jan 2015 Ravindra Shekhawat Changed for Sufficient Coverage
@isTest
private class OCSUGC_ContactUsTest
 {
    static testMethod void test_UserOCSUGC_ContactUs()
    {
          Profile admin;
          User adminUsr1,adminUsr2;
          admin = OCSUGC_TestUtility.getAdminProfile();
          List<User> lstUserInsert = new List<User>();   
        lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '1'));
        lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test', '2'));
        insert lstUserInsert; 
        // TO DO: implement unit test
        System.runAs(adminUsr1) {        
            Group newGroup = OCSUGC_TestUtility.createGroup(OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP,true);
              GroupMember grouMem = OCSUGC_TestUtility.createGroupMemberNC(newGroup.Id,adminUsr2.Id,true);
        }
        test.startTest();

        PageReference contactPage = Page.OCSUGC_ContactUs;
    	Test.setCurrentPageReference(contactPage); // use setCurrentPageReference,
    	ApexPages.currentPage().getParameters().put('recaptcha_challenge_field','abcdefgh');
        OCSUGC_ContactUs objContactUs = new OCSUGC_ContactUs();
        objContactUs.btnsubmitMessageAndEmailToPublicGroup();
        objContactUs.verify();
        objContactUs.reset();
        //verify that email has sent succesfully
        system.assertEquals(true, objContactUs.isEmailSend);
        String nextPage = objContactUs.btnCancel().getUrl();

        //verify the user redirect the on home page succesfully
        system.assert(nextPage.contains('ocsugc_home'));
        test.stopTest();
    }
    
	/**
	 *	Puneet Mishra, 12 dec 2016
	 */
	public static testMethod void publicKeyTest() {
		Test.StartTest();
			OCSUGC_ContactUs objContactUs = new OCSUGC_ContactUs();
        	//objContactUs.publicKey = system.label.Capcha_PublicKey;
		Test.Stoptest();
	}
	
	/**
	 *	Puneet Mishra, 12 Dec 2016
	 */
	public static testMethod void meth() {
		Test.StartTest();
			ApexPages.currentPage().getParameters().put('recaptcha_challenge_field', system.label.Capcha_PublicKey);
			ApexPages.currentPage().getParameters().put('recaptcha_response_field', system.label.Capcha_PublicKey);
			
			Test.setMock(HttpCalloutMock.class, new OCSUGC_MockCalloutController ());
			
			OCSUGC_ContactUs cont = new OCSUGC_ContactUs();
			cont.verify();
		Test.StopTest();
	}
}
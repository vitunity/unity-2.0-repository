//
// (c) 2012 Appirio, Inc.
//
// Create a User Edit Page
//
// 22 Oct 2014    Naresh K Shiwani   T-327585   Modified updateDiscussion() added attachment size limit.
// 22 Oct 2014    Naresh K Shiwani   T-328778   Modified getVisibilities() added size condition lstPrivateGroup.size(),
//                                              lstPublicGroup.size() for Adding Select Option.
// 03 Nov 2014    Naresh K Shiwani   T-330157   Modified updateDiscussion() inserted OCSUGC_Everyone_Within_OncoPeer label value
//                                              in  OCSUGC_Group__c in case user selects everyone in oncopeer as share with option.
// 17 Nov 2014    Ajay Gautam        Modified Ref: T-334649 - Create FGAB Discussions
// 12 Dec 2014    Neeraj Sharma      Modified Ref: T-340219 verify content for blacklisted words
// 03 Mar 2015    Puneet Sardana     Made changes related to network Id Ref T-365586
// 22 Apr 2015    Naresh K Shiwani   T-379544   Added isDC parameter to skipFileUpload() and updateFileDiscussion()
// 13 May 2015    Naresh K Shiwani   T-379544   Modified skipFileUpload() to save feeditem with 'AllUsers' visibility.

public class OCSUGC_CreateNewDiscussionController extends OCSUGC_Base {
    public Integer taglimit                                 {get; set;}
    public String screenCode                                {get; set;}
    public String discussionId                              {get; set;}
    public String selectedTag                               {get; set;}
    public String selectedVisibility                        {get; set;}
    public String selectedVisibilityValue                   {get; set;}
    public FeedItem discussion                              {get; set;}
    public OCSUGC_DiscussionDetail__c discussionDetail      {get; set;}
    public List<wrapperTag> wrapperTags                     {get; set;}
    public Attachment attachment{get;set;}
    public String commmaSeperatedExtensions {get; set;}
    public boolean isFileTypeExist {get; set;}
    public boolean isFileFormatNotSupported {get; set;}
    public Boolean isFGAB {get;set;}
    public String groupId {get;set;}
    public String description {get;set;}
    Map<Id,String> mapGroups;
    List<CollaborationGroupMember> lstPrivateGroup;
    List<CollaborationGroupMember> lstPublicGroup;
    Set<String> setPicklistValues;
    public string[] selectedInputTagString;
    //ONCO-421, Setting page-title
    public static String pageTitle{get;set;}

    public List<SelectOption> getVisibilities() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select Group'));
        options.add(new SelectOption('Everyone', (isDC ? Label.OCSDC_Everyone_Within_Developer_Cloud : Label.OCSUGC_Everyone_Within_OncoPeer)));
        SelectOption opt;
        if(lstPrivateGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Closed Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPrivateGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
            }
        }
        if(lstPublicGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Open Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPublicGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
            }
        }
        return options;
    }

    public OCSUGC_CreateNewDiscussionController() {
        isFGAB = false;
        if(ApexPages.currentPage().getParameters().containsKey('fgab') && ApexPages.currentPage().getParameters().get('fgab') != null){
          isFGAB = Boolean.valueOf(ApexPages.currentPage().getParameters().get('fgab'));
          groupId = ApexPages.currentPage().getParameters().get('g');
        }
        //ONCO-421, Unique Page-Title
    	if(isDC) {
    		pageTitle = 'New Discussion | Developer';
    	} else {
    		pageTitle = 'New Discussion | OncoPeer';
    	}
        taglimit = Integer.valueOf(System.Label.OCSUGC_No_of_Tag_Limit);
        screenCode = ApexPages.currentPage().getParameters().get('screenCode');
        if(screenCode == null || screenCode.trim() == '') screenCode = '0';
        discussionId = ApexPages.currentPage().getParameters().get('Id');      
        //discussion = new FeedItem();
        discussionDetail = new OCSUGC_DiscussionDetail__c();
        attachment = new Attachment();
        setPicklistValues = new Set<String>();
        discussionBody = '';
        //selectedInputTagString = new string[]{};
        fetchExtensions();
        fatchDiscussionInfo();
        fatchGroup();
        getTags();
        getVisibilities();
    }

	//updating view count of knowledge detail page
    public PageReference deleteTempRecordOfImage() {
        OCSUGC_Utilities.deleteTemporaryDiscussionRecords();
        return null;
    }

    private void fatchDiscussionInfo() {
        try {
	        if(String.isNotBlank(discussionId)) {
	            for(FeedItem des :[Select ParentId,
	                                      Parent.Name,
	                                      Visibility,
	                                      Type,
	                                      Body,
	                                      RelatedRecordId,
	                                      ContentType,
	                                      ContentFileName
	                              From FeedItem
	                              Where Id =:discussionId]) {
	
	                discussion = des;
	            }
	            system.debug(' ==== ' + discussionId);
	            for(OCSUGC_DiscussionDetail__c desD :[Select OCSUGC_Group__c, OCSUGC_GroupId__c, OCSUGC_Description__c
	                                           From OCSUGC_DiscussionDetail__c
	                                           Where OCSUGC_DiscussionId__c =:discussion.id]) {
	
	                discussionDetail = desD;
	                selectedVisibility = desD.OCSUGC_GroupId__c;
	                selectedVisibilityValue = desD.OCSUGC_Group__c;
	            }
	        } else {
	        	discussion = new FeedItem();
	        }
        } Catch(Exception e) {
        	system.debug(' ==== Exception ==== ' + e.getMessage() + ' ' + e.getLineNumber());
        }
    }

    private void fatchGroup() {
        mapGroups = new Map<Id,String>();
        lstPrivateGroup = new List<CollaborationGroupMember>();
        lstPublicGroup = new List<CollaborationGroupMember>();

        if(isFGAB) {
            // T-334649 - Added FGAB Support - Nov 17, 2014 - Ajay Gautam (Appirio)
            for(CollaborationGroup objStdGroup : [SELECT Name, Id,CollaborationType FROM CollaborationGroup WHERE Id = :groupId LIMIT 1]) {
                mapGroups.put(objStdGroup.Id, objStdGroup.Name); // Selected i.e. FGAB Group
                selectedVisibility = objStdGroup.Id;
                selectedVisibilityValue = objStdGroup.Name;
            }
        } else {
        	if(isManagerOrContributor && !isDC) {
        		for(CollaborationGroupMember grp :[Select Id,
	                                                      CollaborationGroup.Name,
	                                                      CollaborationGroupId,
	                                                      CollaborationGroup.CollaborationType
	                                              From CollaborationGroupMember
	                                              Where CollaborationGroup.IsArchived = false
	                                              And CollaborationGroup.NetworkId =: networkId
	                                              order by CollaborationGroup.Name]) {
	                if(!mapGroups.containsKey(grp.CollaborationGroupId)) {
	                    mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
	                    if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
	                    if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
	                }
	            }
        	} else {
	            for(CollaborationGroupMember grp :[Select Id,
	                                                      CollaborationGroup.Name,
	                                                      CollaborationGroupId,
	                                                      CollaborationGroup.CollaborationType
	                                              From CollaborationGroupMember
	                                              Where CollaborationGroup.IsArchived = false
	                                              And CollaborationGroup.NetworkId =: networkId
	                                              And (MemberId =:userInfo.getUserId()
	                                              OR CollaborationGroup.OwnerId =:userInfo.getUserId())
	                                              order by CollaborationGroup.Name]) {
	                if(!mapGroups.containsKey(grp.CollaborationGroupId)) {
	                    mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
	                    if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
	                    if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
	                }
	            }
        	}
        }
    }

    public void fetchExtensions(){
        commmaSeperatedExtensions = Label.OCSUGC_Event_File_Extensions;
        isFileTypeExist = true;
        isFileFormatNotSupported = false;
    }

    public Pagereference updatePrePopulateTagsForVisibility(){
        return null;
    }

    //method updated new discussion information.
    //Notify current user via pagemessage
    public String discussionBody {get;set;}
    public pageReference updateDiscussion() {
        try {
        	system.debug(' ==== ATTACHMENT ==== ' + attachment);
        	//ref:T-340219 Verifying description content for blacklisted content
            if(discussion.body != null) {
            	list<String> contentList = new list<String>();
                contentList.add(discussion.body);
                if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                    attachment = new Attachment();
                    return null;
                }
            }
            if(attachment.ContentType!=null && (attachment.ContentType.lastIndexOf('jpeg')!=-1 || attachment.ContentType.lastIndexOf('JPEG')!=-1)) {                
                attachment.ContentType = attachment.ContentType.replace('jpeg','jpg');
            }
            //End ref:T-340219
            String imageext = 'image/';
            String contentType = attachment.ContentType;
            if(attachment.body != null && (attachment.ContentType != null && contentType.startsWith(imageext)) ){
                //ref:I-143046 Start 	Naresh K Shiwani 	Changed location of code so that
			    // can check image size on original not compressed.
                if(attachment.Body.size() > integer.valueof(Label.OCSUGC_Discussion_Image_Upload_Size_Limit)*1024*1024){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Cannot_Upload_Image
                                                                                            +' '+
                                                                                            +Label.OCSUGC_Discussion_Image_Upload_Size_Limit
                                                                                            +' '
                                                                                            +Label.OCSUGC_MB_Please_Use_Image_Size_Less
                                                                                            +' '
                                                                                            +Label.OCSUGC_Discussion_Image_Upload_Size_Limit
                                                                                            +' '
                                                                                            +Label.OCSUGC_MB);
                    ApexPages.addMessage(myMsg);
                    attachment = new Attachment();
                    return null;
                }
                //ref:I-143046 End 	Naresh K Shiwani 	Changed location of code so that
			    // can check image size on original not compressed.
        		String tempDisIdImg = userInfo.getUserId().substring(10)+'_DcsnImg';
                for(Attachment att :[Select Body
                                     From Attachment
                                     Where ParentId IN (Select Id
                                                        From OCSUGC_DiscussionDetail__c
                                                        Where OCSUGC_DiscussionId__c =: tempDisIdImg)]) {
                    attachment.body = att.body;
                }

                String fileExtension = attachment.Name.subString(attachment.Name.lastIndexOf('.')+1, attachment.Name.length()).toUpperCase();
                if(!commmaSeperatedExtensions.contains(fileExtension)){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_File_Type_Error);
                    ApexPages.addMessage(myMsg);
                    attachment = new Attachment();
                    return null;
                }
            }
            if(discussion.Id == null) {
                discussion.NetworkScope = networkId;
                discussion.Visibility = 'AllUsers';
                discussion.ParentId = userInfo.getUserId();
                if(attachment.body != null){
                    attachment.OwnerId = UserInfo.getUserId();
                    discussion.ContentData = attachment.body;
                    discussion.ContentFileName = attachment.Name;
                }
                insert discussion;
				system.debug(' INSERTED DISCUSSION ' + discussion);

                discussionDetail.OCSUGC_DiscussionId__c = discussion.Id;
                discussionDetail.OCSUGC_Description__c = discussion.Body;
                discussionDetail.OCSUGC_IsFGABDiscussion__c = isFGAB;
                if(isFGAB) {
                    discussionDetail.OCSUGC_Group__c = mapGroups.get(groupId);
                    discussionDetail.OCSUGC_GroupId__c = groupId;
                } else {
                    if(String.isNotBlank(selectedVisibility)){
                        if(selectedVisibility.equals('Everyone')) {
                            discussionDetail.OCSUGC_Group__c = isDC ?  System.Label.OCSDC_Everyone_Within_Developer_Cloud : System.Label.OCSUGC_Everyone_Within_OncoPeer;
                            discussionDetail.OCSUGC_GroupId__c = selectedVisibility;
                        } else {
                            discussionDetail.OCSUGC_Group__c = mapGroups.get(selectedVisibility);
                            discussionDetail.OCSUGC_GroupId__c = selectedVisibility;
                        }
                    }
                }
                insert discussionDetail;
                //Puneet Sardana 03-Feb-2105  Code commented

            } // Puneet Mishra,	adding else condition for update
             else {
             	system.debug('== discussion body =' + discussionBody);
             	discussion.body = discussionBody;
             	system.debug('== discussion body =' + discussion.Body); 
             	update discussion;
             	discussionDetail = new OCSUGC_DiscussionDetail__c();
             	discussionDetail = [SELECT Id, OCSUGC_Description__c, OCSUGC_DiscussionId__c FROM OCSUGC_DiscussionDetail__c WHERE OCSUGC_DiscussionId__c =: discussion.Id LIMIT 1];
             	discussionDetail.OCSUGC_Description__c = discussion.Body;
             	update discussionDetail;
             
             }
            addTags();
            String dBody = discussion.Body;
            if(dBody == null){
                dBody = '';
            }else{
                if(dBody.length() > 15){
                    dBody = dBody.subString(0, 14);
                }
            }

            OCSUGC_Utilities.deleteTemporaryDiscussionRecords();

            OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,dBody, selectedVisibility, 'has created', 'OCSUGC_DiscussionDetail?Id='+discussion.ID);
            Pagereference pg = Page.OCSUGC_DiscussionDetail;
            if(isFGAB) {
                pg = Page.OCSUGC_FGABDiscussionDetail; // T-334649 - Added FGAB Support - Nov 17, 2014 - Ajay Gautam (Appirio)
                pg.getParameters().put('g',selectedVisibility);
            }
            pg.getParameters().put('Id',discussion.Id);
            pg.getParameters().put('fgab',''+isFGAB);
            pg.getParameters().put('discussionDetailId', discussionDetail.Id);
            pg.getParameters().put('dc',''+isDC);
            pg.getParameters().put('msg','y');
            pg.setRedirect(true);
            return pg;

        } catch(Exception e) {
            System.debug('=====================error: '+e.getMessage() + e.getLineNumber());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            attachment = new Attachment();
            return null;
        }
    }
    
    public pageReference updateFileDiscussion() {
        try {
            //ref:T-340219 Verifying description content for blacklisted content
            if(discussion.body != null) {
                list<String> contentList = new list<String>();
                contentList.add(discussion.body);
                if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                    attachment = new Attachment();
                    return null;
                }
            }
            system.debug('==** discussion.body **==' + discussion.body);
            if(discussion.Id == null) {
                discussion.NetworkScope = networkId;
                discussion.Visibility = 'AllUsers';
                discussion.ParentId = userInfo.getUserId();
                System.debug('discussion.body======='+discussion.Body);
                insert discussion;

                discussionDetail.OCSUGC_DiscussionId__c = discussion.Id;
                discussionDetail.OCSUGC_Description__c = discussion.Body;
                discussionDetail.OCSUGC_IsFGABDiscussion__c = isFGAB;
                if(isFGAB) {
                    discussionDetail.OCSUGC_Group__c = mapGroups.get(groupId);
                    discussionDetail.OCSUGC_GroupId__c = groupId;
                } else {
                    if(String.isNotBlank(selectedVisibility)){
                        if(selectedVisibility.equals('Everyone')) {
                            discussionDetail.OCSUGC_Group__c = isDC ?  System.Label.OCSDC_Everyone_Within_Developer_Cloud : System.Label.OCSUGC_Everyone_Within_OncoPeer;
                            discussionDetail.OCSUGC_GroupId__c = selectedVisibility;
                        } else {
                            discussionDetail.OCSUGC_Group__c = mapGroups.get(selectedVisibility);
                            discussionDetail.OCSUGC_GroupId__c = selectedVisibility;
                        }
                    }
                }
                insert discussionDetail;
                //Puneet Sardana 03-Feb-2105  Code commented

            }else {
             	system.debug('== discussionBody =' + discussionBody);
             	system.debug('== discussion.body =' + discussion.body);
             	if(isFGAB)
             		discussion.body = discussion.body;
             	else if(isDC)
             		discussion.body = discussionbody;
             	else
             		discussion.body = discussionBody.split('.com/')[1];
             	system.debug('== discussion body =' + discussion.Body); 
             	update discussion;
             	discussionDetail = new OCSUGC_DiscussionDetail__c();
             	discussionDetail = [SELECT Id, OCSUGC_Description__c, OCSUGC_DiscussionId__c FROM OCSUGC_DiscussionDetail__c WHERE OCSUGC_DiscussionId__c =: discussion.Id LIMIT 1];
             	discussionDetail.OCSUGC_Description__c = discussion.Body;
             	update discussionDetail;
             
             }
            addTags();
            String dBody = discussion.Body;
            if(dBody == null){
                dBody = '';
            }else{
                if(dBody.length() > 15){
                    dBody = dBody.subString(0, 14);
                }
            }

            OCSUGC_Utilities.deleteTemporaryDiscussionRecords();

            OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName, dBody, selectedVisibility, 'has created', 'OCSUGC_DiscussionDetail?Id='+discussion.ID);
            
            Pagereference pg = Page.OCSUGC_FGABFileUpload; // T-334649 - Added FGAB Support - Nov 17, 2014 - Ajay Gautam (Appirio)
            pg.getParameters().put('Id', discussion.Id);
            pg.getParameters().put('g', selectedVisibility);
            pg.getParameters().put('discussionId',discussionDetail.Id);
            pg.getParameters().put('fgab',''+isFGAB);
            pg.getParameters().put('dc',''+isDC);
            
            pg.setRedirect(true);
            System.debug('===========pageReference================: ' +  pg);
            return pg;

        } catch(Exception e) {
            System.debug('=====================error: '+e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            attachment = new Attachment();
            return null;
        }
    }

    public class wrapperTag {
        public Boolean isActive {get;set;}
        public String tagName   {get;set;}

        public wrapperTag(Boolean isAct, String name) {
            isActive = isAct;
            tagName = name;
        }
    }

    public PageReference addTag() {
        //ONCO-402, Ability to Add multiple tags at the same time
        set<string> selectedTagSet = new set<string>();
        
        if(selectedTag != null && selectedTag.trim() != ''){
        	selectedTag = selectedTag.trim();
        	selectedTagSet.add(selectedTag);	
        }
        
        for(string sltTag :selectedTagSet){
        	 selectedInputTagString = sltTag.split(',');
        }
        
        
        for(String tagValue : selectedInputTagString){
        	if(!setPicklistValues.contains((tagValue.trim()).toUpperCase())){
        		setPicklistValues.add((tagValue.trim()).toUpperCase());
            	list<String> contentList = new list<String>();
            	contentList.add(tagValue.trim());
            	if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                	return null;
            	}
            	wrapperTags.add(new wrapperTag(true, tagValue.trim()));
            	tagValue = null;
        	}	
        }
        
       /* if(selectedTag != null && selectedTag.trim() != '') {
        	selectedTag = selectedTag.trim();
        	if(!setPicklistValues.contains(selectedTag.toUpperCase())) {
        		setPicklistValues.add(selectedTag.toUpperCase());
                list<String> contentList = new list<String>();
                contentList.add(selectedTag);
                if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                    return null;
                }
                wrapperTags.add(new wrapperTag(true, selectedTag));
                selectedTag = null;
            }
        }*/
        return null;
    }

    public PageReference updateTag() {
       for(Integer cnt=0; wrapperTags.size()>cnt; cnt++) {
            //if(wrapperTags[cnt].tagName.equals(selectedTag.trim())) {
            if(wrapperTags[cnt].tagName.equals(selectedTag)) {
                wrapperTags.remove(cnt);
                setPicklistValues.remove(selectedTag.toUpperCase());
            }
        }
        
        selectedTag = null; 
        return null;
    }

    public void getTags() {
    	//system.debug('*****discussion.Id******'+discussion.Id);//added by Shital 3/3/2016
        wrapperTags = new List<wrapperTag>();
        for(OCSUGC_FeedItem_Tag__c tag :[Select OCSUGC_Tags__c,
                                         OCSUGC_Tags__r.OCSUGC_Tag__c,
                                         OCSUGC_FeedItem_Id__c
                                  From OCSUGC_FeedItem_Tag__c
                                  Where OCSUGC_FeedItem_Id__c =:discussion.Id
                                  order by OCSUGC_Tags__r.OCSUGC_Tag__c]) {

            wrapperTags.add(new wrapperTag(true, tag.OCSUGC_Tags__r.OCSUGC_Tag__c));
            setPicklistValues.add(tag.OCSUGC_Tags__r.OCSUGC_Tag__c.toUpperCase());
        }
        //system.assertEquals(1,2);
    }

    //Function to generate map of Tags and its Id
    public static Map<String, Id> mapTags {
        get{
            if(mapTags == null){
                mapTags = new Map<String, Id>();
                for(OCSUGC_Tags__c tg : [SELECT Id, OCSUGC_Tag__c FROM OCSUGC_Tags__c]){
                    mapTags.put(tg.OCSUGC_Tag__c.toLowerCase(), tg.Id);
                }
            }
            return mapTags;
        }
        private set;
    }

    private void addTags() {
        List<OCSUGC_FeedItem_Tag__c> lstFeedItemTags = new List<OCSUGC_FeedItem_Tag__c>();

        for(OCSUGC_FeedItem_Tag__c tag:  [SELECT Id FROM OCSUGC_FeedItem_Tag__c WHERE OCSUGC_FeedItem_Id__c = :discussion.Id]) {
            lstFeedItemTags.add(tag);
        }
        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()) {
            Database.delete(lstFeedItemTags);
        }

        List<OCSUGC_Tags__c> lstTags = new List<OCSUGC_Tags__c>();
        lstFeedItemTags = new List<OCSUGC_FeedItem_Tag__c>();

        for(wrapperTag wrapper : wrapperTags) {
            if(wrapper.isActive) {
                if(String.isNotBlank(wrapper.tagName)) {
                    OCSUGC_Tags__c newTag = new OCSUGC_Tags__c(OCSUGC_Tag__c = wrapper.tagName);
                    if(mapTags.containsKey(wrapper.tagName.toLowerCase())) {
                        newTag.Id = mapTags.get(wrapper.tagName.toLowerCase());
                    }
                    if(isFGAB) {
                        newTag.OCSUGC_IsFGABUsed__c = true;
                    } else if(isDC) {
                        newTag.OCSDC_IsDeveloperCloudUsed__c = true;
                    } else {
                        newTag.OCSUGC_IsOncoPeerUsed__c = true;
                    }
                    lstTags.add(newTag);
                }
            }
        }

        if(lstTags != null && !lstTags.isEmpty()) {
            Database.upsert(lstTags);
        }

        //Insert new tags
        for(OCSUGC_Tags__c tg : lstTags) {
            lstFeedItemTags.add(new OCSUGC_FeedItem_Tag__c(OCSUGC_Tags__c = tg.Id, OCSUGC_FeedItem_Id__c = discussion.Id));
        }

        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()){
            Database.insert(lstFeedItemTags);
        }
    }

  // @description: Cancel operations. Cancels current operation and navigates the user to FGAB GroupHomePage.
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author:
  public Pagereference cancelOps() {
    Pagereference pg;
    if(discussion != null && discussion.id != null) {
        if(isFGAB){
            pg = Page.OCSUGC_FGABDiscussionDetail;
            pg.getParameters().put('g',groupId);
        } else{
            pg = Page.OCSUGC_DiscussionDetail;
        }
      pg.getParameters().put('Id',discussion.id);
      pg.getParameters().put('fgab',''+isFGAB);
      pg.getParameters().put('dc',''+isDC);
      pg.getParameters().put('discussionDetailId', discussionDetail.Id);
    } else {
            pg = Page.OCSUGC_Home;
            pg.getParameters().put('isResetPwd','true');
            pg.getParameters().put('fgab',''+isFGAB);
            if(isFGAB){
                pg.getParameters().put('g',groupId);
            }
            pg.getParameters().put('dc',''+isDC);
    }
    pg.setRedirect(true);
    return pg;
  }
  
  public Pagereference skipFileUpload(){
  	FeedItem post = new FeedItem();
  	for(FeedItem fee :[select id, ContentData, ContentFileName, parentId
                       from FeedItem 
                       where parentId =: Apexpages.currentPage().getParameters().get('Id')
							and visibility = 'InternalUsers' 
							and Type = 'ContentPost' limit 1]) {                                                              
	    if(fee.ContentData != null){
            fee.visibility = 'AllUsers';
            update fee;  
	    }                
	}
	
	Pagereference pg = Page.OCSUGC_DiscussionDetail;
    if(isFGAB) {
        pg = Page.OCSUGC_FGABDiscussionDetail; 
        pg.getParameters().put('g',Apexpages.currentPage().getParameters().get('g'));
    }
    // PUNEET MISHRA, Id was mapped wrong, feedItem's Id mapped to discussionDetailId and vice versa
    pg.getParameters().put('Id', Apexpages.currentPage().getParameters().get('Id'));
    pg.getParameters().put('discussionDetailId', Apexpages.currentPage().getParameters().get('discussionId'));
    pg.getParameters().put('fgab',''+Apexpages.currentPage().getParameters().get('fgab'));
    pg.getParameters().put('msg','y');
    pg.getParameters().put('dc',''+isDc);
    pg.setRedirect(true);
    deleteFeedItemNContentDoc(Apexpages.currentPage().getParameters().get('discussionId'));
	return pg;
  }
  
	/**
	 *	method will delete the feed records,
	 *	records which will deleted are FeedItem, ContentDocument ( ContentDocument should get deleted if deleting FeedItem as Document will remain in the Org )
	 */
	@TestVisible
	private static void deleteFeedItemNContentDoc(Id discussionDetailId) {
		List<FeedItem> feedItemList = new List<FeedItem>();	// FeedItem List queried using discussionId 
		List<ContentDocument> contentDocList = new List<ContentDocument>();
		List<Id> feedItemIdList = new List<Id>();			// content Id of FeedItem whose related ContentDocument need to get deleted
		List<Id> contentVerId = new List<Id>();				// Id of ContentDocument from ContentVersion Records
		List<Id> contentDocId = new List<Id>();
		feedItemList = [SELECT Id, ParentId, Title, CreatedDate, RelatedRecordId FROM FeedItem WHERE ParentId =: discussionDetailId ORDER BY CreatedDate DESC NULLS LAST];
		system.debug(' ===== B$-feedItemList ===== ' + feedItemList);
		if(feedItemList.size() > 1) {
			// removing First Element from List as First Element is the lastest one.
			feedItemList.remove(0);
		} else {
			// returning if value fails, not returning will cause other statements to execute which is not required and can run into "Time Exceeded" limit
			return;
		}
		
		for(FeedItem fd : feedItemList) {
			contentVerId.add(fd.RelatedRecordId);
		}
		
		system.debug(' ===== A5-feedItemList ===== ' + feedItemList);
		
		for(ContentVersion ver : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN : contentVerId]) {
			contentDocId.add(ver.ContentDocumentId);
		}
		
		List<ContentDocument> contentDoc = new List<ContentDocument>(); 
		if(!contentDocId.isEmpty())
			contentDoc = [SELECT Id, Title FROM ContentDocument WHERE Id IN : contentDocId];
			
		if(!contentDoc.isEmpty()) {
			delete feedItemList;
			delete contentDoc;
		}
	}
  	
}
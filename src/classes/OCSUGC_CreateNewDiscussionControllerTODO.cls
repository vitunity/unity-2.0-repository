public class OCSUGC_CreateNewDiscussionControllerTODO extends OCSUGC_Base {
    public String Id{get; set;}
    public OCSUGC_Knowledge_Exchange__c knowledgeExchange {get; set;}
    public String selectedExtensions {get; set;}
    public String uploadStatus {get; set;}
    public String commmaSeperatedExtensions {get; set;}
    public boolean requiresManagerApproval {get; set;}
    public String selectedKASupportedFileSizeLabel {get; set;}
    public boolean isFileTypeExist {get; set;}
    public boolean isFileFormatNotSupported {get; set;}
    public Attachment attachment{get;set;}
    public boolean recordExist{get;set;}

    public OCSUGC_CreateNewDiscussionControllerTODO() {
        Id = ApexPages.currentPage().getParameters().get('Id');
        if(Id==null) {
            System.debug('Knowledge record Id doest not exist.');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Knowledge record Id doest not exist.'));
        }
        knowledgeExchange = new OCSUGC_Knowledge_Exchange__c();
        attachment = new Attachment();
        for(FeedItem fes : [Select ContentFileName,ContentData From FeedItem Where parentid =:Id]){
            attachment.Name = fes.ContentFileName;
        }
        fetchArtifactInfo(Id);
        fetchExtensions();
    }

    public Pagereference skipFileUpload(){
        FeedItem post = new FeedItem();
        for(FeedItem fee :[select id, ContentData, ContentFileName, parentId
                       from FeedItem 
                       where parentId =: Apexpages.currentPage().getParameters().get('Id')
                            and visibility = 'InternalUsers' 
                            and Type = 'ContentPost' limit 1]) {                                                              
            if(fee.ContentData != null){
                fee.visibility = 'AllUsers';
                update fee;  
            }                
        }

        //deleteFeedItemNContentDoc(Apexpages.currentPage().getParameters().get('Id'));
        return null;
    }
  
  
    public Pagereference redirectToKnowledgePage() {
        Pagereference pg = Page.OCSUGC_KnowledgeArtifactDetail;
        pg.getParameters().put('knowledgeArtifactId',Apexpages.currentPage().getParameters().get('Id'));
        pg.getParameters().put('isCreated','true');
        pg.setRedirect(true);
        deleteFeedItemNContentDoc(Apexpages.currentPage().getParameters().get('Id'));
        return pg;
      
    }

    //fetch artifact info
    private void fetchArtifactInfo(String knowledgeArtifactId){
        recordExist = false;
        for(OCSUGC_Knowledge_Exchange__c exchange : [Select ownerId, Id, OCSUGC_Views__c, OCSUGC_Artifact_Type__c, createdDate, OCSUGC_Title__c, CreatedById,OCSUGC_Is_Deleted__c,OCSUGC_Content_Sensitivity__c,
                                                OCSUGC_Anatomical_Sites__c,OCSUGC_Target_Dose_Level__c,OCSUGC_QA_test__c,OCSUGC_TrueBeam_version__c,
                                                OCSUGC_Exotic_Trajectory__c, OCSUGC_Imaging_Only__c, OCSUGC_Dynamic_Tracking__c, OCSUGC_Intra_Frx_Imaging__c,
                                                OCSUGC_Gating__c, OCSUGC_Report_Type__c, OCSUGC_Compatibility__c, OCSUGC_Dashboard_Type__c, OCSUGC_Group__c, OCSUGC_Group_Id__c,
                                                OCSUGC_Summary__c, OCSUGC_NetworkId__c, OCSUGC_Status__c from OCSUGC_Knowledge_Exchange__c where id =: knowledgeArtifactId]){
            knowledgeExchange = exchange;
        }
        if(knowledgeExchange.Id!=null)
            recordExist = true;
    }
    

    /**
     *  method will delete the feed records,
     *  records which will deleted are FeedItem, ContentDocument ( ContentDocument should get deleted if deleting FeedItem as Document will remain in the Org )
     */
    @TestVisible
    private static void deleteFeedItemNContentDoc(Id discussionDetailId) {
        List<FeedItem> feedItemList = new List<FeedItem>(); // FeedItem List queried using discussionId 
        List<ContentDocument> contentDocList = new List<ContentDocument>();
        List<Id> feedItemIdList = new List<Id>();           // content Id of FeedItem whose related ContentDocument need to get deleted
        List<Id> contentVerId = new List<Id>();             // Id of ContentDocument from ContentVersion Records
        List<Id> contentDocId = new List<Id>();
        feedItemList = [SELECT Id, ParentId, Title, CreatedDate, RelatedRecordId FROM FeedItem WHERE ParentId =: discussionDetailId ORDER BY CreatedDate DESC NULLS LAST];
        system.debug(' ===== B$-feedItemList ===== ' + feedItemList);
        if(feedItemList.size() > 1) {
            // removing First Element from List as First Element is the lastest one.
            feedItemList.remove(0);
        } else {
            // returning if value fails, not returning will cause other statements to execute which is not required and can run into "Time Exceeded" limit
            return;
        }
        
        for(FeedItem fd : feedItemList) {
            contentVerId.add(fd.RelatedRecordId);
        }
        
        system.debug(' ===== A5-feedItemList ===== ' + feedItemList);
        
        for(ContentVersion ver : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN : contentVerId]) {
            contentDocId.add(ver.ContentDocumentId);
        }
        
        List<ContentDocument> contentDoc = new List<ContentDocument>(); 
        if(!contentDocId.isEmpty())
            contentDoc = [SELECT Id, Title FROM ContentDocument WHERE Id IN : contentDocId];
            
        if(!contentDoc.isEmpty()) {
            delete feedItemList;
            delete contentDoc;
        }
    }

    public void fetchExtensions(){
        uploadStatus = '';
        selectedExtensions = '';
        commmaSeperatedExtensions = '';
        if(knowledgeExchange.OCSUGC_Artifact_Type__c != null){
            if(knowledgeExchange.OCSUGC_Artifact_Type__c == 'Document' || knowledgeExchange.OCSUGC_Artifact_Type__c == 'Video'){
                requiresManagerApproval = true;
            }
            else
            requiresManagerApproval = false;
            OCSUGC_KFiles_ApprovedFileType__c content = OCSUGC_KFiles_ApprovedFileType__c.getValues(knowledgeExchange.OCSUGC_Artifact_Type__c);
            
            if(content != null) {
                commmaSeperatedExtensions += content.OCSUGC_File_Extension__c.toUpperCase() + ',';
                selectedKASupportedFileSizeLabel = (Label.OCSUGC_Maximum_File_Size).replace('FILE_SIZE',''+Integer.valueOf(content.OCSUGC_File_Size__c)/1024);// 'Maximum file size is '+ Integer.valueOf(content.OCSUGC_File_Size__c)/1024 + ' MB.';
                // Modified by      Puneet Mishra, 7 Sept 2015
                //[ONCO-362]        Message displaying maximum file size to upload
                //selectedKASupportedFileSizeLabel = (Label.OCSUGC_Maximum_File_Size).replace('FILE_SIZE',''+Integer.valueOf(2.5*content.OCSUGC_File_Size__c)/1024);// 'Maximum file size is 25 MB.';
            }
            if(commmaSeperatedExtensions.contains(',') &&
                commmaSeperatedExtensions.subString(commmaSeperatedExtensions.length() - 1, commmaSeperatedExtensions.length()).contains(',')) {
                commmaSeperatedExtensions = commmaSeperatedExtensions.subString(0, commmaSeperatedExtensions.length() - 1);
            }
        }

        if(commmaSeperatedExtensions == ''){
            selectedExtensions = Label.OCSUGC_File_Type_Not_Allowed;
            isFileTypeExist = false;
            isFileFormatNotSupported = true;
        }else{
            selectedExtensions = Label.OCSUGC_Allowed_File_Types_Message+ '' + commmaSeperatedExtensions;
            isFileTypeExist = true;
            isFileFormatNotSupported = false;
        }
        //prePopulateTags();
    }
}
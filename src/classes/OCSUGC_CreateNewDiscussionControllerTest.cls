/*
Name        : CAKE_CreateNewDiscussionControllerTest
Updated By  : Priyanka Kumar
Date        : 24 July, 2014
Purpose     : Test class  for CAKE_CreateNewDiscussionController
*/

@isTest
public class OCSUGC_CreateNewDiscussionControllerTest {

    static Profile admin,portal;
    static User adminUsr1,adminUsr2, memberUsr;
    static Account account; 
    static Contact contact1,contact2,contact3,contact4,contact5;
    static List<User> lstUserInsert;
    static List<Contact> lstContactInsert;
    static CollaborationGroup publicGroup,privateGroup,fgabGroup;
    static CollaborationGroupMember groupMember1;
    static Network ocsugcNetwork;
    static List<CollaborationGroup> lstGroupInsert;
    static List<CollaborationGroupMember> lstGroupMemberInsert;
    static OCSUGC_Knowledge_Exchange__c ka1;
    static List<OCSUGC_Knowledge_Exchange__c> lstKAInsert;
    static OCSUGC_KFiles_ApprovedFileType__c kaTypes;

    @testSetup static void testData(){
    
        admin          = OCSUGC_TestUtility.getAdminProfile();
        portal         = OCSUGC_TestUtility.getPortalProfile(); 
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        OCSUGC_TestUtility.createBlackListWords('p test blacklist');
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        account = OCSUGC_TestUtility.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert; 
        lstUserInsert = new List<User>();
        lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
        lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
        lstUserInsert.add( memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        insert lstUserInsert;     
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test discussion public group 1','Public',ocsugcNetwork.id,false)); 
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
        insert lstGroupInsert;
        system.debug('-----public Group Id---' + publicGroup);
        /*PermissionSet memberPermSet = OCSUGC_TestUtility.getMemberPermissionSet();
        System.runAs(adminUsr1){
        PermissionSetAssignment memberPermSetAssignment = OCSUGC_TestUtility.createPermissionSetAssignment(memberPermSet.Id, memberUsr.id, true);
        }*/
    }

    static testMethod void testNewDiscussionController2() {
        
        publicGroup = [Select Id, Name, CollaborationType, NetworkId FROM CollaborationGroup WHERE Name = 'test discussion public group 1' LIMIT 1];
        /*FeedItem item = new FeedItem();
        item.ParentId = userInfo.getUserId(); 
        item.Body = 'test body';
        insert item;*/
        test.startTest();
	        PageReference ref = Page.OCSUGC_CreateNewDiscussion;      
	        ref.getParameters().put('fgab','true');
	        ref.getParameters().put('g',publicGroup.Id);
	        Test.setCurrentPage(ref);
	        OCSUGC_CreateNewDiscussionController discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.attachment.Name = 'test.jpg';
	        discussionController.attachment.ContentType ='image/jpg';        
	        discussionController.attachment.Body = Blob.valueOf('test puneet');
	        discussionController.discussion.Body = 'test d body';
	        discussionController.selectedVisibility = 'Everyone';
	        discussionController.updatePrePopulateTagsForVisibility();
	        discussionController.updateDiscussion();
	        System.assert(discussionController.discussionDetail.Id!=null);
       test.stopTest();
    }

    
    static testMethod void testNewDiscussionController3() {
        publicGroup = [Select Id, Name, CollaborationType, NetworkId FROM CollaborationGroup WHERE Name = 'test discussion public group 1' LIMIT 1];
        
        /*FeedItem item = new FeedItem();
        item.ParentId = userInfo.getUserId();
        item.Body = 'test body';
        insert item;*/
        test.startTest();
	        PageReference ref = Page.OCSUGC_CreateNewDiscussion;      
	        ref.getParameters().put('fgab','false');
	        ref.getParameters().put('g',publicGroup.Id);
	        Test.setCurrentPage(ref);
	        OCSUGC_CreateNewDiscussionController discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.getVisibilities();       
	        discussionController.attachment.Name = 'test.jpg';
	        discussionController.attachment.ContentType ='image/jpg';        
	        discussionController.attachment.Body = Blob.valueOf('test puneet');        
	        discussionController.selectedVisibility = 'Everyone';
	        discussionController.updateDiscussion();
	        discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.discussion.Body = 'test d body2';
	        discussionController.selectedVisibility = publicGroup.Id;
	        discussionController.updateDiscussion();
	        discussionController.getVisibilities(); 
        test.stopTest();  
    }

    static testMethod void testNewDiscussionController() {
        FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
        //FeedItem item1 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
        OCSUGC_Tags__c newtag = OCSUGC_TestUtility.createTags('tag-test');
        insert newtag;
        OCSUGC_FeedItem_Tag__c itemtag =  new OCSUGC_FeedItem_Tag__c();
        itemtag.OCSUGC_FeedItem_Id__c = item.id;
        itemtag.OCSUGC_Tags__c = newtag.id;
        insert itemtag;
        test.startTest();
	        OCSUGC_CreateNewDiscussionController discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.updateFileDiscussion();
	        discussionController.discussion = item;
	        discussionController.getVisibilities();
	        discussionController.selectedVisibility = 'Everyone';
	        discussionController.isFGAB = false;
	        discussionController.updateFileDiscussion();
	        discussionController.deleteTempRecordOfImage();
	        discussionController.attachment.Name = 'test.jpg';
	        discussionController.attachment.ContentType ='image/jpg';
	        discussionController.attachment.Body = Blob.valueOf('test puneet');
	        discussionController.updateDiscussion();
	        discussionController.attachment.Name = 'test.jpg';
	        discussionController.attachment.Body = Blob.valueOf('test puneet');  
	        discussionController.attachment.ContentType ='image/png';      
	        discussionController.selectedVisibility = 'Everyone';
	        discussionController.updateDiscussion();
	        OCSUGC_TestUtility.createBlackListWords('p test blacklist');
	        discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.discussion.body = 'p test blacklist';
	        discussionController.updateDiscussion();
	        discussionController.selectedTag = 'p test blacklist';
	        discussionController.addTag();
	        discussionController.cancelOps();
	        discussionController = new OCSUGC_CreateNewDiscussionController();
	        discussionController.attachment.Name = 'test.xxx';
	        discussionController.attachment.ContentType ='image/png';
	        discussionController.attachment.Body = Blob.valueOf('test puneet');
	        discussionController.discussion.Body = 'test d body';
	        discussionController.updateDiscussion();
	        Map<String,Id> tagsMap=OCSUGC_CreateNewDiscussionController.mapTags;
	        OCSUGC_CreateNewDiscussionController.mapTags=null;
	        System.assert(discussionController.discussionDetail.Id==null);
        test.stopTest();

    }

    static testMethod void testExistingDiscussion() {
    	Test.startTest();
	        publicGroup = [Select Id, Name, CollaborationType, NetworkId FROM CollaborationGroup WHERE Name = 'test discussion public group 1' LIMIT 1];
	        Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
	        //line commented by SHital BHujbal
	        //adminUsr1 =   [Select id, Name, Email FROM User WHERE profileId = :adminProfile.Id AND isActive=true LIMIT 1]; 
	        adminUsr1 = OCSUGC_TestUtility.createStandardUser(true, adminProfile.Id, 'test1', Label.OCSUGC_Varian_Employee_Community_Manager); 
	        
	        FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
	        OCSUGC_DiscussionDetail__c discussionDetail = OCSUGC_TestUtility.createDiscussion(publicGroup.Id, item.Id, true);
	        System.runAs(adminUsr1) {       
	            PageReference ref = Page.OCSUGC_CreateNewDiscussion;
	            ref.getParameters().put('id',item.Id);
	            ref.getParameters().put('fgab','true');
	            ref.getParameters().put('g',publicGroup.Id);
	            Test.setCurrentPage(ref);
	            OCSUGC_CreateNewDiscussionController discussionController = new OCSUGC_CreateNewDiscussionController();
	            discussionController.attachment.Name = 'test.jpg';
	            discussionController.attachment.Body = Blob.valueOf('test puneet'); 
	            discussionController.attachment.ContentType ='image/jpg'; 
	            discussionController.selectedVisibility = 'Everyone';       
	            discussionController.skipFileUpload();
	            discussionController.updateDiscussion();
	            discussionController.cancelOps();
	        }
        Test.stopTest();        
    }
    
    /**
     *	CreatedDate " 8 dec 2016, thursday
     */
	public static testMethod void wrapperTag_Test() {
		Test.StartTest();
			OCSUGC_CreateNewDiscussionController.wrapperTag tag = new OCSUGC_CreateNewDiscussionController.wrapperTag(true, 'TestName');
		Test.StopTest();
	}
	
	/**
	 *	createdate : 8 Dec 2016
	 */
	public static testMethod void updateTagTest(){
		Test.StartTest();
			/*OCSUGC_CreateNewDiscussionController control = new OCSUGC_CreateNewDiscussionController();
			control.wrapperTags = new List<OCSUGC_CreateNewDiscussionController.wrapperTag>{new OCSUGC_CreateNewDiscussionController.wrapperTag(true, 'New1'), new control.wrapperTag(true, 'New2')};
			List<OCSUGC_CreateNewDiscussionController.wrapperTag> tag= new List<OCSUGC_CreateNewDiscussionController.wrapperTag>{new control.wrapperTag(true, 'New1'), new control.wrapperTag(true, 'New2')};
			control.selectedTag='New2';
			 
			control.updateTag();*/
			
		Test.Stoptest();
	}
	
	/** 16 Jan 2017 */
	@isTest static void updateDiscussionTest() {
		Test.StartTest();
			Blacklisted_Word__c blkListed = new Blacklisted_Word__c(Word__c = 'ABCBCBD', is_Active__c = true);
			insert blkListed;
			
			FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
			item.Body = 'ABCBCBD';
			insert item;
			
			OCSUGC_CreateNewDiscussionController cont = new OCSUGC_CreateNewDiscussionController();
			cont.discussionBody = 'test discussion body.com/abc';
			cont.discussion = item;
			cont.updateDiscussion();
			cont.updateFileDiscussion();
			
		Test.Stoptest();
	}
	
	/** 16 Jan 2017 */
	@isTest static void updateDiscussionTest2() {
		Test.StartTest();
			Blacklisted_Word__c blkListed = new Blacklisted_Word__c(Word__c = 'ABCBCBD', is_Active__c = true);
			insert blkListed;
			
			FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
			item.Body = '222';
			insert item;
			
			OCSUGC_DiscussionDetail__c discus = new OCSUGC_DiscussionDetail__c();
			discus.OCSUGC_Description__c = 'item.Id';
			discus.OCSUGC_DiscussionId__c = item.Id;
			insert discus;
			
			OCSUGC_CreateNewDiscussionController cont = new OCSUGC_CreateNewDiscussionController();
			cont.discussionBody = 'test discussion body.com/abc';
			cont.discussion = item;
			cont.updateDiscussion();
			cont.updateFileDiscussion();
			
		Test.Stoptest();
	}
	
	/** 16 Jan 2017 */
	@isTest static void updateDiscussionTest3() {
		Test.StartTest();
			Blacklisted_Word__c blkListed = new Blacklisted_Word__c(Word__c = 'ABCBCBD', is_Active__c = true);
			insert blkListed;
			
			FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
			item.Body = '222';
			insert item;
			
			OCSUGC_DiscussionDetail__c discus = new OCSUGC_DiscussionDetail__c();
			discus.OCSUGC_Description__c = 'item.Id';
			discus.OCSUGC_DiscussionId__c = item.Id;
			insert discus;
			
			OCSUGC_CreateNewDiscussionController cont = new OCSUGC_CreateNewDiscussionController();
			cont.discussionBody = 'testdiscussionbody.com/abc';
			cont.discussion = item;
			cont.updateDiscussion();
			cont.updateFileDiscussion();
			
		Test.Stoptest();
	}
}
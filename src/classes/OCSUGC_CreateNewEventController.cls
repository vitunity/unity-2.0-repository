/*
Name        : OCSUGC_CreateNewEventController
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 21st July, 2014
Purpose     :
Refrence    : S-215934
// 26 Sep 2014 Puneet Sardana Changes to make event editable Ref T- 320140
// 22 Oct 2014    Naresh K Shiwani   T-327586   Modified uploadAttachment() added image attachment size limit.
// 22 Oct 2014    Naresh K Shiwani   T-327600   Modified uploadAttachment() added pdf attachment size limit.
// 22 Oct 2014    Naresh K Shiwani   T-328778   Modified getAllUserGroups() added size condition lstPrivateGroup.size(),
//                                              lstPublicGroup.size() for Adding Select Option.
// 03 Nov 2014    Puneet Sardana     Added logic for including event end time Ref T-330713
// 03 Nov 2014    Naresh K Shiwani   T-330157   Modified updateDiscussion() inserted OCSUGC_Everyone_Within_OncoPeer label value
//                                              in  OCSUGC_Group__c in case user selects everyone in oncopeer as share with option.
// 12 Dec 2014    Neeraj Sharma      Modified Ref: T-340219 verify content for blacklisted words
// 03 Mar 2015    Puneet Sardana     Made changes related to network Id Ref T-365586

*/

public class OCSUGC_CreateNewEventController extends OCSUGC_Base {

    public String CAKEApplications { get; set; }
    public Integer taglimit { get; set; }
    public Attachment attachment{get;set;}
    public Attachment pdfattachment{get;set;}
    public String commmaSeperatedExtensions {get; set;}
    public String pdfcommmaSeperatedExtensions {get; set;}
    public boolean isFileTypeExist {get; set;}
    public boolean isFileFormatNotSupported {get; set;}
    public OCSUGC_Intranet_Content__c intranetContent {get; set;}
    private String eventId {get;set;}

    public String selectedGroup {get; set;}
    public String selectedTag {get; set;}
    public String selectedHour {get; set;}
    public String selectedMinute {get; set;}
    public String selectedAmPm {get; set;}
    public String eventEndHour {get;set;}
    public String eventEndMinute {get;set;}
    public String eventEndAmPm {get;set;}
    //Fields only enabled in edit mode - Time, Start Date , End Date , Description , Location and Tags
    public boolean isOpenInEditMode {get;set;}
    public String isimageChanged {get;set;}
    public String ispdfChanged {get;set;}
    public List<wrapperTag> wrapperTags                     {get; set;}
    public List<CollaborationGroupMember> lstPrivateGroup   {get; set;}
    public List<CollaborationGroupMember> lstPublicGroup    {get; set;}
    public List<CollaborationGroupMember> lstUnlistedGroup  {get; set;}
    public FeedItem existingFeedItemImg { get; set; }
    public FeedItem existingFeedItemPdf { get; set; }
    public string[] selectedInputTagString;
    
    //ONCO-421, Setting page-title
    public static String pageTitle{get;set;}
    
    List<FeedItem> fItems = new List<FeedItem>();
    Set<String> setPicklistValues;
    public List<SelectOption> getHours(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'HH'));
        for(Integer i=1 ; i <= 12 ; i++){
            String strI = pad2(i);
            options.add(new SelectOption(strI, strI));
        }
        return options;
    }

    public List<SelectOption> getMinutes() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'MM'));
        for(String min :System.label.OCSUGC_Event_Minutes_picklist.Split(',')) {
            options.add(new SelectOption(min, min));
        }
        return options;
    }

    private String pad2(Integer num) {
        return (num < 10 ? '0' : '') + num;
    }
    public List<SelectOption> getAMPM(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('AM', 'AM'));
        options.add(new SelectOption('PM', 'PM'));
        return options;
    }

    Map<String, String> mapGroups;
    public OCSUGC_CreateNewEventController(){
        eventId = ApexPages.currentPage().getParameters().get('eventId');
        taglimit = Integer.valueOf(System.Label.OCSUGC_No_of_Tag_Limit);
        intranetContent = new OCSUGC_Intranet_Content__c(recordTypeId = OCSUGC_Utilities.getRecordTypeId('OCSUGC_Intranet_Content__c', 'Events'));
        intranetContent.OCSUGC_NetworkId__c = networkId;
        attachment = new Attachment();
        setPicklistValues = new Set<String>();
        selectedInputTagString = new string[]{};
        List<String> timeZones = getTimeZones();
        String currentTimeZone = UserInfo.getTimeZone().getID();
        //Set default time zone to current timezone of user Ref T-336738
        for(String tm : timeZones) {
          if(tm.contains(currentTimeZone)) {
            intranetContent.OCSUGC_Time_Zone__c = tm;
            break;
          }
        }
        
        //ONCO-421, Unique Page-Title
		if(isDC) {
			pageTitle = 'New Event | Developer';
		} else {
			pageTitle = 'New Event | OncoPeer';
		}
        
        pdfattachment = new Attachment();
        fetchExtensions();
        fetchGroup();
        getAllUserGroups();
        fetchEventInfo();
        getTags();

    }

    public PageReference deleteTempRecordOfImage() {
        OCSUGC_Utilities.deleteTemporaryEventRecords();
        return null;
    }

      // @description: Method to get all timezones of user
      // @param: NA
      // @param: NA
      // @return: List of strings with timezones
      // Additional
      // 01-12-2014  Puneet Sardana  Original Ref T-336738
     private List<String> getTimeZones(){
         List<String> timezones = new List<String>();
         Schema.DescribeFieldResult fieldResult = OCSUGC_Intranet_Content__c.OCSUGC_Time_Zone__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for(Schema.PicklistEntry f : ple){
           timezones.add(f.getValue());
         }
         return timezones;
      }
    // @description: Fetch event information
    // Additional
    // 19-Sep-2014 Puneet Sardana Fetch prefilled event data if provided (Ref - T-320140)
    // 03 Nov 2014 Puneet Sardana Included logic for event end time Ref T-330713
    private void fetchEventInfo() {
        if(String.isNotBlank(eventId)) {
            for(OCSUGC_Intranet_Content__c event :[Select Id, OCSUGC_Title__c, OCSUGC_Description__c, OCSUGC_Location__c, CreatedById,
                                                          OCSUGC_Event_Start_Date__c, OCSUGC_Event_Start_Time__c, OCSUGC_Event_End_Date__c, OCSUGC_Event_End_Time__c,
                                                          OCSUGC_Time_Zone__c, OCSUGC_Group__c, OCSUGC_GroupId__c
                                                    From OCSUGC_Intranet_Content__c
                                                    Where Id =:eventId]) {
                //only owner of group open in edit mode              
                    intranetContent = event;
                    selectedGroup = intranetContent.OCSUGC_GroupId__c;
                    List<String> timeParts = intranetContent.OCSUGC_Event_Start_Time__c.split(':| ');
                    if(timeParts.size()==3)
                    {
                        selectedHour = timeParts[0];
                        selectedMinute = timeParts[1];
                        selectedAmPm = timeParts[2];
                    }
                    timeParts = intranetContent.OCSUGC_Event_End_Time__c.split(':| ');
                    if(timeParts.size()==3)
                    {
                        eventEndHour = timeParts[0];
                        eventEndMinute = timeParts[1];
                        eventEndAmPm = timeParts[2];
                    }
                    isOpenInEditMode=true;               
            }
        } else {
                IsOpenInEditMode = false;
       }
    }

    private void handleAttachments() {
    	OCSUGC_Utilities.deleteTemporaryEventRecords();
        if(intranetContent.Id != null) {
        	attachment.body = null;
        	pdfattachment.body = null;
        } else {
        	attachment = new Attachment();
        	pdfattachment = new Attachment();
        }
    }
    //
    // (c) 2014 Appirio, Inc.
    //
    // Function for updating event and uploading image
    //
    // 26 Sep 2014 Puneet Sardana Ref T-320140 Ability added to update event
    // 03 Nov 2014 Puneet Sardana Included logic for event end time Ref T-330713
    public Pagereference uploadAttachment() {
        //ref:T-340219 Verifying description content for blacklisted content
        list<String> contentList = new list<String>();
        if(intranetContent.OCSUGC_Description__c != null) {
            contentList.add(intranetContent.OCSUGC_Description__c);
        }
        if(String.isNotBlank(intranetContent.OCSUGC_Title__c)) {
            contentList.add(intranetContent.OCSUGC_Title__c);
        }
        if(String.isNotBlank(intranetContent.OCSUGC_Location__c)) {
            contentList.add(intranetContent.OCSUGC_Location__c);
        }
        if(contentList.size() > 0) {
            if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                handleAttachments();
                return null;
            }
        }
        //End ref:T-340219
        if(intranetContent.OCSUGC_Event_Start_Date__c < Date.today()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Date_Error);
            ApexPages.addMessage(myMsg);
            handleAttachments();
            return null;
        }

        if(intranetContent.OCSUGC_Event_Start_Date__c > intranetContent.OCSUGC_Event_End_Date__c){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Start_End_Date_Error);
            ApexPages.addMessage(myMsg);
            handleAttachments();
            return null;
        }
        if((intranetContent.OCSUGC_Event_Start_Date__c == intranetContent.OCSUGC_Event_End_Date__c)
        	&& ((selectedAMPM=='PM' && eventEndAmPm=='AM')
        		|| (selectedAMPM.equalsIgnoreCase(eventEndAmPm) && integer.valueof(selectedHour) > integer.valueof(eventEndHour))
        		|| (selectedAMPM.equalsIgnoreCase(eventEndAmPm) && selectedHour.equalsIgnoreCase(eventEndHour) && integer.valueof(selectedMinute) > integer.valueof(eventEndMinute))
        		|| (selectedAMPM.equalsIgnoreCase(eventEndAmPm) && selectedHour.equalsIgnoreCase(eventEndHour) && selectedMinute.equalsIgnoreCase(eventEndMinute)))){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Start_End_Time_Error);
            ApexPages.addMessage(myMsg);
            handleAttachments();
            return null;
        }

        if(isFGAB) {
        	selectedGroup = fgabGroupId;
        }

        if(selectedGroup != null){
        	 //Changed by   18-Dec-2014   Puneet Sardana   For fgab take name from base class
            intranetContent.OCSUGC_Group__c = isFGAB ? fgabGroupName : (selectedGroup != 'Everyone' ? mapGroups.get(selectedGroup) : (isDC ? System.Label.OCSDC_Everyone_Within_Developer_Cloud : System.Label.OCSUGC_Everyone_Within_OncoPeer));
            intranetContent.OCSUGC_GroupId__c = selectedGroup;
        }

        try {
	        if(attachment.body != null){
	        	// I-143253 Start 	Naresh K Shiwani 	Changed location of code so that
				// can check image size on original not compressed.
	            if(attachment.Body.size() > integer.valueof(Label.OCSUGC_Event_Image_Upload_Size_Limit)*1024*1024){
	                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Cannot_Upload_Image
	                                                                                        +' '+
	                                                                                        +Label.OCSUGC_Event_Image_Upload_Size_Limit
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_MB_Please_Use_Image_Size_Less
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_Event_Image_Upload_Size_Limit
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_MB);
	                ApexPages.addMessage(myMsg);
	                handleAttachments();
	                return null;
	            }
	            // I-143253 End 	Naresh K Shiwani 	Changed location of code so that
				// can check image size on original not compressed.
				String tempDisIdImg = userInfo.getUserId().substring(10)+'_EventImg';
	            for(Attachment att :[Select Body
	                                 From Attachment
	                                 Where ParentId IN (Select Id
	                                                    From OCSUGC_DiscussionDetail__c
	                                                    Where OCSUGC_DiscussionId__c =: tempDisIdImg)]) {
	                System.debug('tempDisIdImg=========='+tempDisIdImg);
	                attachment.body = att.body;
	            }
	            String fileExtension = attachment.Name.subString(attachment.Name.lastIndexOf('.')+1, attachment.Name.length()).toUpperCase();
	            if(!commmaSeperatedExtensions.contains(fileExtension)){
	                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_File_Type_Error);
	                ApexPages.addMessage(myMsg);
	                handleAttachments();
	                return null;
	            }
	        }

	        if(pdfattachment.body != null){
	            String fileExtension = pdfattachment.Name.subString(pdfattachment.Name.lastIndexOf('.')+1, pdfattachment.Name.length()).toUpperCase();
	            if(!pdfcommmaSeperatedExtensions.contains(fileExtension)){
	                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_File_Type_Error);
	                ApexPages.addMessage(myMsg);
	                handleAttachments();
	                return null;
	            }
	            System.debug('attachment.Body.size()=========='+pdfattachment.Body.size());
	            System.debug('integer.valueof(Label.OCSUGC_Event_PDF_Upload_Size_Limit)*1024=========='+integer.valueof(Label.OCSUGC_Event_PDF_Upload_Size_Limit)*1024*1024);
	            if(pdfattachment.Body.size() > integer.valueof(Label.OCSUGC_Event_PDF_Upload_Size_Limit)*1024*1024){
	                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Cannot_Upload_PDF
	                                                                                        +' '+
	                                                                                        +Label.OCSUGC_Event_PDF_Upload_Size_Limit
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_MB_Please_Use_PDF_Size_Less
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_Event_PDF_Upload_Size_Limit
	                                                                                        +' '
	                                                                                        +Label.OCSUGC_MB);
	                ApexPages.addMessage(myMsg);
	                handleAttachments();
	                return null;
	            }
	        }

	        intranetContent.OCSUGC_Event_Start_Time__c = selectedHour + ':' + selectedMinute +' ' + selectedAMPM;
	        intranetContent.OCSUGC_Event_End_Time__c = eventEndHour + ':' + eventEndMinute + ' ' + eventEndAmPm;
	        upsert intranetContent;
	        addTags();

	        //If Opened in edit mode delete the existing feed item and update notification
	        if(isOpenInEditMode) {

	        	List<FeedItem> lstDeleteFeedItems = new List<FeedItem>();
	  			for(FeedItem existingFeedItem :[Select body
	                                  		   From FeedItem
	                                  		   Where ParentId =: intranetContent.Id order by createdDate Desc]) {

		  			if(pdfattachment.body != null) {
						String queryConcat = intranetContent.id+'_pdf';
						insertFeedItem('PDF');
			            if(queryConcat.equalsIgnoreCase(existingFeedItem.body)) {
		                	lstDeleteFeedItems.add(existingFeedItem);
	                    }
	                    pdfattachment = new Attachment();
		  			}

					if(attachment.body != null) {
		                String imgQueryConcat = intranetContent.id+'_img';
		                insertFeedItem('IMG');
		                if(imgQueryConcat.equalsIgnoreCase(existingFeedItem.body)) {
		            		lstDeleteFeedItems.add(existingFeedItem);
						}
						attachment = new Attachment();
					}
	          	}

	          	if(lstDeleteFeedItems.size() > 0)
	          		delete lstDeleteFeedItems;
	            if(fItems.size() > 0)
	            	insert fItems;

				if(isFGAB) {
		        	OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,intranetContent.OCSUGC_Title__c, selectedGroup, 'has updated', 'OCSUGC_FGABEventDetail?eventId='+intranetContent.ID+'&g='+fgabGroupId);
		      	} else {
		        	OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,intranetContent.OCSUGC_Title__c, selectedGroup, 'has updated', 'OCSUGC_EventDetail?eventId='+intranetContent.ID);
		        }
	        } else {
	        	FeedItem commentfeeditem = new FeedItem();
	            commentfeeditem.ParentId = intranetContent.Id;
	            commentfeeditem.NetworkScope = 'AllNetworks';
	            commentfeeditem.Visibility = 'AllUsers';
	            commentfeeditem.Body = intranetContent.id+'_comments';
	            fItems.add(commentfeeditem);
	            if(pdfattachment.body != null) {
	                insertFeedItem('PDF');
	                pdfattachment = new Attachment();
	            }
	            if(attachment.body != null) {
	                insertFeedItem('IMG');
	                attachment = new Attachment();
	            }
	            if(fItems.size() > 0)
	            	insert fItems;

	        	 // For insertion update notification
	       		if(isFGAB) {
	            	OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,intranetContent.OCSUGC_Title__c, selectedGroup, 'has created', 'OCSUGC_EventDetail?eventId='+intranetContent.ID+intranetContent.ID+'&g='+fgabGroupId);
	       		} else {
	        	  	OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,intranetContent.OCSUGC_Title__c, selectedGroup, 'has created', 'OCSUGC_EventDetail?eventId='+intranetContent.ID);
	      		}
	        }
			OCSUGC_Utilities.deleteTemporaryEventRecords();
	        Pagereference pg;
	        if(isFGAB) {
	        	pg = Page.OCSUGC_FGABEventDetail;
	        	pg.getParameters().put('fgab','true');
	        	pg.getParameters().put('g',fgabGroupId);
	        }
	        else {
	        	pg = Page.OCSUGC_EventDetail;
	        }
	        pg.getParameters().put('eventId',intranetContent.ID);
	        pg.getParameters().put('isCreated','true');
	        pg.getParameters().put('dc',''+isDC);
	        return pg;

        } catch(Exception e) {
            system.debug('================Error: '+e.getMessage());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            handleAttachments();
            return null;
        }
    }

    private void insertFeedItem(String itemType){
        FeedItem Antrfeeditem = new FeedItem();
        Antrfeeditem.ParentId = intranetContent.Id;
        Antrfeeditem.NetworkScope = 'AllNetworks';
        Antrfeeditem.Visibility = 'AllUsers';
        if(itemType.equalsIgnoreCase('PDF')){
            pdfattachment.OwnerId = UserInfo.getUserId();
            pdfattachment.ParentId = intranetContent.Id;
            Antrfeeditem.Body = intranetContent.id+'_pdf';
            Antrfeeditem.ContentData = pdfattachment.body;
            Antrfeeditem.ContentFileName = pdfattachment.Name;
        }
        if(itemType.equalsIgnoreCase('IMG')){
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = intranetContent.Id;
            Antrfeeditem.Body = intranetContent.id+'_img';
            Antrfeeditem.ContentData = attachment.body;
            Antrfeeditem.ContentFileName = attachment.Name;
        }
        fItems.add(Antrfeeditem);
    }

    public List<SelectOption> getAllUserGroups(){

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select Group'));
        options.add(new SelectOption('Everyone', (isDC ? Label.OCSDC_Everyone_Within_Developer_Cloud : Label.OCSUGC_Everyone_Within_OncoPeer)));
        SelectOption opt;
        if(lstPrivateGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Closed Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPrivateGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));

            }
        }
        if(lstPublicGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Open Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPublicGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
                 mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
            }
        }
        if(lstUnlistedGroup.size() > 0){
            for(CollaborationGroupMember grp :lstUnlistedGroup) {
                 mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
            }
        }
        return options;
    }

    public Pagereference updatePrePopulateTagsForVisibility(){
        return null;
    }

    public void fetchExtensions(){
        commmaSeperatedExtensions = Label.OCSUGC_Event_File_Extensions;
        pdfcommmaSeperatedExtensions = 'PDF';
        isFileTypeExist = true;
        isFileFormatNotSupported = false;
    }
   // Updated   18-Dec-2014   Puneet Sardana   Updated this method to populate mapGroups Ref T-341253
    private void fetchGroup() {
        lstPrivateGroup = new List<CollaborationGroupMember>();
        lstPublicGroup = new List<CollaborationGroupMember>();
        lstUnlistedGroup = new List<CollaborationGroupMember>();
        mapGroups = new Map<String, String>();
        set<Id> groupIds = new set<Id>();
 
        if(isManagerOrContributor && !isDC) {
        	for(CollaborationGroupMember grp :[Select Id,
                                                  CollaborationGroup.Name,
                                                  CollaborationGroupId,
                                                  CollaborationGroup.CollaborationType
                                          From CollaborationGroupMember
                                          Where CollaborationGroup.IsArchived = false
                                          And CollaborationGroup.NetworkId =: networkId
                                          order by CollaborationGroup.Name]) {

	            if(!groupIds.contains(grp.CollaborationGroupId)) {
	                groupIds.add(grp.CollaborationGroupId);
	                if(grp.CollaborationGroup.CollaborationType.equals('Private')) {
	                	 lstPrivateGroup.add(grp);
	                }
	                if(grp.CollaborationGroup.CollaborationType.equals('Public')) {
	                	lstPublicGroup.add(grp);
	                }
	                if(grp.CollaborationGroup.CollaborationType.equals('Unlisted')) {
	                	lstUnlistedGroup.add(grp);
	                }
	            }
	            mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
	        }
        } else {
        	for(CollaborationGroupMember grp :[Select Id,
                                                  CollaborationGroup.Name,
                                                  CollaborationGroupId,
                                                  CollaborationGroup.CollaborationType
                                          From CollaborationGroupMember
                                          Where CollaborationGroup.IsArchived = false
                                          And CollaborationGroup.NetworkId =: networkId
                                          And (MemberId =:userInfo.getUserId()
                                          OR CollaborationGroup.OwnerId =:userInfo.getUserId())
                                          order by CollaborationGroup.Name]) {

	            if(!groupIds.contains(grp.CollaborationGroupId)) {
	                groupIds.add(grp.CollaborationGroupId);
	                if(grp.CollaborationGroup.CollaborationType.equals('Private')) {
	                	 lstPrivateGroup.add(grp);
	                }
	                if(grp.CollaborationGroup.CollaborationType.equals('Public')) {
	                	lstPublicGroup.add(grp);
	                }
	                if(grp.CollaborationGroup.CollaborationType.equals('Unlisted')) {
	                	lstUnlistedGroup.add(grp);
	                }
	            }
	            mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
	        }
        }
    }


    public PageReference addTag() {
        
        //ONCO-402, Ability to Add multiple tags at the same time
        set<string> selectedTagSet = new set<string>();
        
        if(selectedTag != null && selectedTag.trim() != ''){
        	selectedTag = selectedTag.trim();
        	selectedTagSet.add(selectedTag);	
        }
        
        for(string sltTag :selectedTagSet){
        	 selectedInputTagString = sltTag.split(',');
        }
        
        
        for(String tagValue : selectedInputTagString){
        	if(!setPicklistValues.contains((tagValue.trim()).toUpperCase())){
        		setPicklistValues.add((tagValue.trim()).toUpperCase());
            	list<String> contentList = new list<String>();
            	contentList.add(tagValue.trim());
            	if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                	return null;
            	}
            	wrapperTags.add(new wrapperTag(true, tagValue.trim()));
            	tagValue = null;
        	}	
        }
        
        return null;
    }

    public PageReference updateTag() {
        for(Integer cnt=0; wrapperTags.size()>cnt; cnt++) {
            if(wrapperTags[cnt].tagName.equals(selectedTag)) {
                wrapperTags.remove(cnt);
                setPicklistValues.remove(selectedTag.toUpperCase());
            }
        }
        selectedTag = null;
        return null;
    }
    // @description: Cancel group formation
    // Navigate to home Page in case of new event and event detail page if existing event
    // 19/09/2014 Puneet Sardana For T-320142
    public PageReference cancelEventInformation()
    {
        Pagereference pg;
        //If new group navigate to home page
        if(string.isBlank(intranetContent.Id))
        {
        	  pg = Page.OCSUGC_Home;
        	  pg.getParameters().put('isResetPwd','true');
        	  if(isFGAB) {
        	  	pg.getParameters().put('fgab','true');
              pg.getParameters().put('g',fgabGroupId);
        	  }

        }
        //If existing group navigate to group detail page
        else
        {
        	  if(isFGAB) {
              pg= Page.OCSUGC_FGABEventDetail;
              pg.getParameters().put('fgab','true');
              pg.getParameters().put('g',fgabGroupId);
        	  }
        	  else {
        	  	pg= Page.OCSUGC_EventDetail;
        	  }
            pg.getParameters().put('eventId',intranetContent.Id);
        }
        pg.getParameters().put('dc',''+isDC);
        pg.setRedirect(true);
        return pg;
    }
    public void getTags() {
        wrapperTags = new List<wrapperTag>();
            if(String.isNotBlank(intranetContent.Id)){
                for(OCSUGC_Intranet_Content_Tags__c tag : [SELECT Id, OCSUGC_Tags__r.OCSUGC_Tag__c, OCSUGC_Tags__r.Id
                                                   FROM OCSUGC_Intranet_Content_Tags__c
                                                   WHERE OCSUGC_Intranet_Content__c = : intranetContent.Id order by OCSUGC_Tags__r.OCSUGC_Tag__c]){
                wrapperTags.add(new wrapperTag(true, tag.OCSUGC_Tags__r.OCSUGC_Tag__c));
                setPicklistValues.add(tag.OCSUGC_Tags__r.OCSUGC_Tag__c.toUpperCase());
            }
        }
    }

    private void addTags() {
        List<OCSUGC_Intranet_Content_Tags__c> lstFeedItemTags = new List<OCSUGC_Intranet_Content_Tags__c>();

        for(OCSUGC_Intranet_Content_Tags__c tag:  [SELECT Id FROM OCSUGC_Intranet_Content_Tags__c WHERE OCSUGC_Intranet_Content__c =: intranetContent.Id]) {
            lstFeedItemTags.add(tag);
        }
        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()) {
            Database.delete(lstFeedItemTags);
        }

        List<OCSUGC_Tags__c> lstTags = new List<OCSUGC_Tags__c>();
        lstFeedItemTags = new List<OCSUGC_Intranet_Content_Tags__c>();

        for(wrapperTag wrapper : wrapperTags) {
            if(wrapper.isActive) {
                if(String.isNotBlank(wrapper.tagName)) {
                    OCSUGC_Tags__c newTag = new OCSUGC_Tags__c(OCSUGC_Tag__c = wrapper.tagName);
                    if(mapTags.containsKey(wrapper.tagName.toLowerCase())) {
                        newTag.Id = mapTags.get(wrapper.tagName.toLowerCase());
                    }
                    if(isFGAB) {
                        newTag.OCSUGC_IsFGABUsed__c = true;
                    } else if(isDC) {
                        newTag.OCSDC_IsDeveloperCloudUsed__c = true;
                    } else {
                        newTag.OCSUGC_IsOncoPeerUsed__c = true;
                    }
                    lstTags.add(newTag);
                }
            }
        }

        if(lstTags != null && !lstTags.isEmpty()) {
            Database.upsert(lstTags);
        }

        //Insert new tags
        for(OCSUGC_Tags__c tg : lstTags) {
            lstFeedItemTags.add(new OCSUGC_Intranet_Content_Tags__c(OCSUGC_Tags__c = tg.Id, OCSUGC_Intranet_Content__c = intranetContent.Id));
        }

        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()){
            Database.insert(lstFeedItemTags);
        }
    }

    //Function to generate map of Tags and its Id
    public static Map<String, Id> mapTags {
        get{
            if(mapTags == null){
                mapTags = new Map<String, Id>();
                for(OCSUGC_Tags__c tg : [SELECT Id, OCSUGC_Tag__c FROM OCSUGC_Tags__c]){
                    mapTags.put(tg.OCSUGC_Tag__c.toLowerCase(), tg.Id);
                }
            }
            return mapTags;
        }
        private set;
    }

    public class wrapperTag {
        public Boolean isActive {get;set;}
        public String tagName   {get;set;}

        public wrapperTag(Boolean isAct, String name) {
            isActive = isAct;
            tagName = name;
        }
    }
}
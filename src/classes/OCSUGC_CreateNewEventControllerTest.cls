//
//
// (c) 2014 Appirio, Inc.
//
//
// 23 July, 2014      Sidhant Agarwal  Original
// Description: A Test Class for CAKE_CreateNewEventController class.
//
@isTest 
public class OCSUGC_CreateNewEventControllerTest {
	
	static Network ocsugcNetwork;
	static{
		ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
	}
	
    static testMethod void myUnitTest() { 
        Test.startTest();
        //Test Data
        OCSUGC_TestUtility.createTags('Ant');
        OCSUGC_Intranet_Content__c ic = OCSUGC_TestUtility.createIntranetContent('Events',
                                                         'Applications',
                                                         null,'Team','Test');
        ic.OCSUGC_Event_Start_Date__c = Date.today().addDays(-1) ;
        insert ic;
        Profile admin = OCSUGC_TestUtility.getAdminProfile();
        User testUserCakeSidhant = OCSUGC_TestUtility.createStandardUser(true, admin.Id, 'test', '1');
        //User testUserCakeSidhant = OCSUGC_TestUtility.createUser(true);
        EntitySubscription es = new EntitySubscription();
        es.SubscriberId = testUserCakeSidhant.ID;
        es.ParentId = UserInfo.getUserId();
        insert es;
        OCSUGC_CreateNewEventController event = new OCSUGC_CreateNewEventController();
        event.isFGAB = true;
        event.selectedGroup = 'Everyone';
        event.selectedTag = 'Ant';
        event.getAllUserGroups();
        event.selectedHour='10';
        event.selectedMinute='10';
        event.selectedAmPm='AM';
        event.getAMPM();
        event.getHours();
        event.getMinutes();
        Attachment attach = new Attachment();
        attach.Name='Test.pdf';
        Blob value = Blob.valueOf('sidhant');
        attach.body =value;
        event.attachment = attach;
        OCSUGC_Intranet_Content__c intranetContent = new OCSUGC_Intranet_Content__c();
        intranetContent.OCSUGC_Title__c  ='testSidhant';
            //page references
        PageReference pageRef = event.uploadAttachment();
        PageReference pageRef1 = event.addTag();
        PageReference pageRef3 = event.updatePrePopulateTagsForVisibility();
            //wrapper class
        OCSUGC_CreateNewEventController.wrapperTag wrapper = new OCSUGC_CreateNewEventController.wrapperTag(true, 'Ant' );
        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('Ant');
        insert tag;
        Map<String, Id> mapTags = OCSUGC_CreateNewEventController.mapTags;
        event.getTags();
        event.addTag();
        event.CAKEApplications = 'test';
        event.isimageChanged = 'test';
        event.ispdfChanged = 'test';
        event.existingFeedItemImg = OCSUGC_TestUtility.createFeedItem(ic.Id,true);
        event.existingFeedItemPdf = OCSUGC_TestUtility.createFeedItem(ic.Id,true);
        Test.stopTest();
    }
    
      static testMethod void myUnitTestExistingEvent() {   
      	
       List<CollaborationGroup> lstGroupInsert;  
       CollaborationGroup  cPublicGroup,cPrivateGroup,cUnlistedGroup;
       Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
       Account accnt = OCSUGC_TestUtility.createAccount('test account 1',true);
       Contact con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);      
       User adminUsr1 = OCSUGC_TestUtility.createStandardUser(true, adminProfile.Id, 'test', '12');  
       
       lstGroupInsert = new List<CollaborationGroup>();
       lstGroupInsert.add(cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',ocsugcNetwork.id,false)); 
       lstGroupInsert.add(cPrivateGroup = OCSUGC_TestUtility.createGroup('testpdiscussionprivate3Group','Private',ocsugcNetwork.id,false));
       lstGroupInsert.add(cUnlistedGroup = OCSUGC_TestUtility.createGroup('testpdiscussionunlisted3Group','Unlisted',ocsugcNetwork.id,false));
       insert lstGroupInsert; 
       
       Test.startTest();
	       OCSUGC_Intranet_Content__c event = OCSUGC_TestUtility.createEvent(cPublicGroup.Id,Date.today(),Date.today().addDays(4),true);
	       FeedItem item = OCSUGC_TestUtility.createFeedItem(event.Id,true);
	       OCSUGC_DiscussionDetail__c discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, userInfo.getUserId().substring(10)+'_EventImg', true);
	       
	       CollaborationGroupMember groupMember1, groupMember2, groupMember3;
	       List<CollaborationGroupMember> lstGroupMemberInsert = new List<CollaborationGroupMember>();
		   lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, cPublicGroup.Id, false));
		   lstGroupMemberInsert.add(groupMember2 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, cPrivateGroup.Id, false));
		   lstGroupMemberInsert.add(groupMember3 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, cUnlistedGroup.Id, false));
		   insert lstGroupMemberInsert;
		   
	       System.runAs(adminUsr1) {       
	         PageReference ref = Page.OCSUGC_FGABCreateNewEvent;
	         ref.getParameters().put('eventId',event.Id);
	         ref.getParameters().put('fgab','true');
	         ref.getParameters().put('g',cPublicGroup.Id);
	         Test.setCurrentPage(ref);
	         OCSUGC_CreateNewEventController eventController = new OCSUGC_CreateNewEventController();
	         eventController.isFGAB = true;
	         eventController.attachment.Name = 'test.jpg';
	         eventController.attachment.Body = Blob.valueOf('test puneet'); 
	         eventController.attachment.ContentType ='image/jpg';
	         eventController.attachment.ParentId = discussionDetail.id;        
	         eventController.pdfattachment.Name = 'test.pdf';
	         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
	         eventController.pdfattachment.ContentType ='document/pdf';  
	         eventController.intranetContent.OCSUGC_Title__c = 'test d body';                  
	         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
	         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
	         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5); 
	         eventController.intranetContent.OCSUGC_Location__c = 'test'; 
	         eventController.selectedGroup = cPublicGroup.Id;           
	         eventController.uploadAttachment();
	         eventController.cancelEventInformation();
	        }  
	        
	         PageReference ref = Page.OCSUGC_FGABCreateNewEvent;
	         ref.getParameters().put('eventId',event.Id);
	         ref.getParameters().put('fgab','false');         
	         Test.setCurrentPage(ref);
	         OCSUGC_CreateNewEventController eventController = new OCSUGC_CreateNewEventController();
	         //eventController.isFGAB = true;
	         //eventController.isOpenInEditMode = true;
	         eventController.attachment.Name = 'test.png';
	         eventController.attachment.Body = Blob.valueOf('test puneet');         
	         eventController.attachment.ContentType ='image/png';
	         eventController.attachment.ParentId = discussionDetail.id;
	         eventController.pdfattachment.Name = 'test.pdf';
	         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
	         eventController.pdfattachment.ContentType ='document/pdf';  
	         eventController.intranetContent.OCSUGC_Title__c = 'test d body';                  
	         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
	         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
	         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
	         eventController.intranetContent.OCSUGC_Location__c = 'India'; 
	         eventController.selectedHour = '3';
	         eventController.selectedMinute = '33';
	         eventController.selectedAMPM ='AM';
	         
	         eventController.eventEndHour = '3';
	         eventController.eventEndMinute = '33';
	         eventController.selectedAMPM ='AM';
	         //event.OCSUGC_Event_End_Time__c = '03:00:AM';  
	         eventController.selectedGroup = cPublicGroup.Id;  
	         eventController.uploadAttachment();
	         System.assert(eventController.intranetContent.Id!=null);
	         eventController.cancelEventInformation();
	         eventController.selectedTag = 'p test tag 1';
	         eventController.addTag();
	         eventController.selectedTag='test jj';
	         eventController.updateTag();
	         eventController.getTags();
	         eventController.updatePrePopulateTagsForVisibility();
         Test.stopTest();
      }  
      
      static testMethod void myUnitTestCreateEvent() {  
      	 Account accnt;
      	 Contact con3 ;
      	 CollaborationGroup cPublicGroup,cPrivateGroup,cUnlistedGroup;
      	 Profile portalProfile = OCSUGC_TestUtility.getPortalProfile();
      	 OCSUGC_DiscussionDetail__c discussionDetail;
      	 OCSUGC_Intranet_Content__c event;
      	 User managerUsr;
      	 List<CollaborationGroup> lstGroupInsert; 
      	 
      	 System.runAs(new user(id=userInfo.getUserId())) {
      	 	accnt = OCSUGC_TestUtility.createAccount('test account 1',true);
      	 	con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);
      	 	Profile manager = OCSUGC_TestUtility.getManagerProfile();
	        managerUsr =  OCSUGC_TestUtility.createPortalUser(true, manager.Id, con3.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager);
      	 }
      	 
  	      lstGroupInsert = new List<CollaborationGroup>();
          lstGroupInsert.add(cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',ocsugcNetwork.id,false)); 
          lstGroupInsert.add(cPrivateGroup = OCSUGC_TestUtility.createGroup('testpdiscussionprivate3Group','Private',ocsugcNetwork.id,false));
          lstGroupInsert.add(cUnlistedGroup = OCSUGC_TestUtility.createGroup('testpdiscussionunlisted3Group','Unlisted',ocsugcNetwork.id,false));
          insert lstGroupInsert;
          
      	 System.runAs(managerUsr) {
      	 	
	         PageReference ref = Page.OCSUGC_CreateNewEvent;
	         ref.getParameters().put('fgab','false');    
       		 discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, userInfo.getUserId().substring(10)+'_EventImg', true);
      	 	 event = OCSUGC_TestUtility.createEvent(cPublicGroup.Id,Date.today(),Date.today().addDays(4),true);   
	         Test.setCurrentPage(ref);
	         OCSUGC_CreateNewEventController eventController = new OCSUGC_CreateNewEventController();
	         //eventController.isFGAB = true;
	         eventController.isOpenInEditMode = false;
			 eventController.deleteTempRecordOfImage();
	         eventController.attachment.Name = 'test.png';
	         eventController.attachment.Body = Blob.valueOf('test puneet');         
	         eventController.attachment.ContentType ='image/png';
	         
	         eventController.pdfattachment.Name = 'test.pdf';
	         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
	         eventController.pdfattachment.ContentType ='document/pdf'; 
	         
	         
	         eventController.intranetContent.OCSUGC_Title__c = 'test d body';                  
	         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
	         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
	         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
	         eventController.intranetContent.OCSUGC_Location__c = 'India'; 
	         eventController.selectedHour = '3';
	         eventController.selectedMinute = '33';
	         eventController.selectedAMPM ='AM';
	         
	         eventController.eventEndHour = '3';
	         eventController.eventEndMinute = '33';
	         eventController.selectedAMPM ='AM';
	         //event.OCSUGC_Event_End_Time__c = '03:00:AM';   
	         eventController.selectedGroup = cPublicGroup.Id;  
	         eventController.uploadAttachment();
	         eventController.cancelEventInformation();
	         eventController.selectedTag = 'p test tag 1';
	         eventController.addTag();
	         eventController.selectedTag='test jj';
	         eventController.updateTag();
	         eventController.getTags();
	         eventController.updatePrePopulateTagsForVisibility();
       }
      }
      static testMethod void myUnitTestExistingEventError() {                              
       
       List<CollaborationGroup> lstGroupInsert;
       Profile portalProfile = OCSUGC_TestUtility.getPortalProfile();
       CollaborationGroup cPublicGroup,cPrivateGroup;
       Account accnt = OCSUGC_TestUtility.createAccount('test account 1',true);
       Contact con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);
       User usr =  OCSUGC_TestUtility.createPortalUser(true, portalProfile.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);  
       
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',ocsugcNetwork.id,false)); 
        lstGroupInsert.add(cPrivateGroup = OCSUGC_TestUtility.createGroup('testpdiscussionprivate3Group','Private',ocsugcNetwork.id,false));
        insert lstGroupInsert;
       
       OCSUGC_Intranet_Content__c event = OCSUGC_TestUtility.createEvent(cPublicGroup.Id,Date.today(),Date.today().addDays(4),true);
       
       FeedItem item = OCSUGC_TestUtility.createFeedItem(event.Id,true);
       Test.startTest();
       System.runAs(usr) {
         PageReference ref = Page.OCSUGC_FGABCreateNewEvent;
         ref.getParameters().put('eventId',event.Id);
         ref.getParameters().put('fgab','false');         
         Test.setCurrentPage(ref);
         OCSUGC_CreateNewEventController eventController = new OCSUGC_CreateNewEventController();
         //eventController.isFGAB = true;
         OCSUGC_TestUtility.createBlackListWords('p test blacklist');         
         eventController.attachment.Name = 'test.png';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p test blacklist'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p test blacklist'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.pdf';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();
         
         //Error 2:
         
         eventController.attachment.Name = 'test.abc';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.pdf';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();
         
         //Error 3:
         eventController.attachment.Name = 'test.jpg';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today().addDays(-1);
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.pdf';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();   
                  
         //Error 4:
         eventController.attachment.Name = 'test.jpg';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today();
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.pdf';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();  
         
         //Error 5
         eventController.attachment.Name = 'test.jpg';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today().addDays(7);
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(4);
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.pdf';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();  
         //Error 6
         eventController.attachment.Name = 'test.jpg';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today().addDays(2);
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today().addDays(4);
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.abc';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment();   
         
         //Error 7
         eventController.attachment.Name = 'test.jpg';
         eventController.attachment.Body = Blob.valueOf('test puneet');         
         eventController.attachment.ContentType ='image/png';
         eventController.intranetContent.OCSUGC_Title__c = 'p tsasa'; 
         eventController.intranetContent.OCSUGC_Location__c = 'p assa'; 
         eventController.intranetContent.OCSUGC_Description__c = 'test d body';
         eventController.intranetContent.OCSUGC_Event_Start_Date__c = Date.today();
         eventController.intranetContent.OCSUGC_Event_End_Date__c = Date.today();
         eventController.selectedHour = '3';
         eventController.selectedMinute = '33';
         eventController.selectedAMPM ='PM';
         eventController.eventEndAmPm = 'AM';
         event.OCSUGC_Event_End_Time__c = '03:00:AM';   
         eventController.pdfattachment.Name = 'test.abc';
         eventController.pdfattachment.Body = Blob.valueOf('test puneet');         
         eventController.pdfattachment.ContentType ='document/pdf';
         eventController.uploadAttachment(); 
        
       }  
       Test.stopTest();
    }
}
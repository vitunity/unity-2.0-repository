/**
 *	15/4/2015	Created by Puneet Mishra
 *				ONCO 168	Custom Exception Class			
 */
public class OCSUGC_CustomExceptionController {
	
	public static String exceptionMsg { get; set;}
	public static String code{get;set;}
	public OCSUGC_CustomExceptionController() {
		code = ApexPages.currentPage().getParameters().get('code');
		exceptionMsg = ' Try Again, Apologise for inconvenience';
	}
}
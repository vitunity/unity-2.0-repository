//
//
// (c) 2014 Appirio, Inc.
//
// 07 July, 2014      Satyanarayan Choudhary
// 25 July, 2014      Sidhant Agarwal
// Description :
//              A Test Class for DiscussionDetailController class.
//

 @IsTest(SeeAllData=true) 
public class OCSUGC_DiscussionDetailControllerTest {
    static CollaborationGroup fdabGrp;
    static FeedItem item,item1;
    static OCSUGC_DiscussionDetail__c discussionDetail,discussionDetail1;
    static FeedComment comment;
    static CollaborationGroup cPublicGroup;
    static OCSUGC_FeedItem_Tag__c contentTag,contentTag1;
    static ConnectApi.FeedElement feedElementDiscussion;
    static ConnectApi.Comment feedComment;
    static Network OCSUGCNetwork;
    
    static {
       Profile portalProfile = OCSUGC_TestUtility.getPortalProfile();
       Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
       OCSUGCNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
       system.debug('Network ::: ' + OCSUGCNetwork.id);
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       Account accnt = OCSUGC_TestUtility.createAccount('test accfght 1',true);
       Contact con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);
       User usr =  OCSUGC_TestUtility.createPortalUser(true, portalProfile.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
       User adminUsr1 = OCSUGC_TestUtility.createStandardUser(true,adminProfile.Id,'12','1');
       System.runAs(adminUsr1) {
           PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, usr.Id, true);
       }
       cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',OCSUGCNetwork.Id,true);
       fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted',OCSUGCNetwork.Id, true);
       item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
       item1 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false);
       list<FeedItem> itemLst = new list<FeedItem>();
       itemLst.add(item);
       itemLst.add(item1);
       insert itemLst;
       discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item.Id, false);
       discussionDetail1 = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item1.Id, false);
       list<OCSUGC_DiscussionDetail__c> discussionLst = new list<OCSUGC_DiscussionDetail__c>();
       discussionLst.add(discussionDetail);
       discussionLst.add(discussionDetail1);
       insert discussionLst;
       
       OCSUGC_TestUtility.createBlackListWords('fuck');
       feedlike flike = new feedlike();
       flike.FeedItemId = item.id;
       flike.CreatedById = UserInfo.getUserId();
       insert flike;
       comment = OCSUGC_TestUtility.createFeedComment(item.id,false);
       insert comment;
       OCSUGC_Intranet_Content__c bookmark = new OCSUGC_Intranet_Content__c();
       bookmark.recordTypeID = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Bookmarks').getRecordTypeId();
       bookmark.OCSUGC_Bookmark_Id__c = item.Id;
       bookmark.OCSUGC_Bookmark_Type__c = 'Discussion';
       bookmark.OCSUGC_Posted_By__c = UserInfo.getUserId();
       insert bookmark;
       OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('testTag5');
       insert tag;
       list<OCSUGC_FeedItem_Tag__c> contentList = new list<OCSUGC_FeedItem_Tag__c>();
       contentTag = new OCSUGC_FeedItem_Tag__c();
       contentTag.OCSUGC_FeedItem_Id__c = item.id;
       contentTag.OCSUGC_Tags__c = tag.id;
       contentList.add(contentTag);
       //insert contentTag;
       
       contentTag1 = new OCSUGC_FeedItem_Tag__c();
       contentTag1.OCSUGC_FeedItem_Id__c = item1.id;
       contentTag1.OCSUGC_Tags__c = tag.id;
       contentList.add(contentTag1);
       //insert contentTag1;
       
       insert contentList;
        try{
        ConnectApi.FeedItemInput objFeedItemInput = new ConnectApi.FeedItemInput();

        ConnectApi.PollCapabilityInput pollChoices = new ConnectApi.PollCapabilityInput();
        ConnectApi.FeedElementCapabilitiesInput objCapabilities = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        textSegmentInput.Text = 'test discussion';
        messageBodyInput.messageSegments.add(textSegmentInput);
        objFeedItemInput.capabilities = objCapabilities;
        objFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        objFeedItemInput.subjectId = fdabGrp.Id;
        objFeedItemInput.body = messageBodyInput;
        feedElementDiscussion = ConnectApi.ChatterFeeds.postFeedElement(OCSUGCNetwork.Id, objFeedItemInput, null);
        //Insert feedComment
        System.runAs(adminUsr1) {
            feedComment = ConnectApi.ChatterFeeds.postCommentToFeedElement(OCSUGCNetwork.Id, feedElementDiscussion.Id, 'Test comment 1.');
        }
        }catch(Exception ep){
            system.debug('Getting ex' + ep);
        }
    }
    
     static testMethod void updateMethodTest(){
     OCSUGC_DiscussionDetailController.updateMethod();
     
     }
    
    static testMethod void testDiscussionController() {
        Test.startTest();
        PageReference pageRef = Page.OCSUGC_DiscussionDetail;
        pageRef.getParameters().put('Id',discussionDetail.OCSUGC_DiscussionId__c);
        pageRef.getParameters().put('msg','test');
        pageRef.getParameters().put('dc','false');
        pageRef.getParameters().put('g',fdabGrp.id);
        Test.setCurrentPage(pageRef);
        fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted',OCSUGCNetwork.Id, true);
        OCSUGC_DiscussionDetailController controller = new OCSUGC_DiscussionDetailController();
        controller.attachment.Name = 'Attachment';
        controller.attachment.body = Blob.valueOf('Test Data');
        controller.attachment.ContentType = 'text';
        OCSUGC_DiscussionDetailController.stringComments = 'Testing';
        controller.commentUploadWithAttachment();        
        controller.selectedCommentId = comment.id;
        controller.isFlagged = true;
        controller.isFGAB = true;
        controller.updateViewCount();           
        OCSUGC_DiscussionDetailController.stringComments = 'Testing';
        OCSUGC_DiscussionDetailController.insertFeedCommentRemote(OCSUGC_DiscussionDetailController.stringComments,fdabGrp.id,feedElementDiscussion.Id,false,5,'false');
        OCSUGC_DiscussionDetailController.stringComments = 'fuck';
        controller.insertFeedComment();
        OCSUGC_DiscussionDetailController.checkForBlackListWords('fuck ass');     
        controller.getRelatedDiscussions();
        OCSUGC_DiscussionDetailController.likeUnlikeFeedItem(OCSUGCNetwork.Id, feedElementDiscussion.Id,fdabGrp.id,'false');
        OCSUGC_DiscussionDetailController.manageBookmark(item.Id);
        system.debug('==item=='+item.Id);
        //system.debug('==feedComment=='+feedComment.Id);
        try{
        OCSUGC_DiscussionDetailController.likeUnlikeComment(item.Id, Comment.id, 'True',cPublicGroup.id,'false');
        }catch(exception e){
        }
        
        controller.showConfirmation();
        controller.selectedCommentId = comment.Id;
        controller.isFlagged = false;
        controller.flagComment();
        controller.editDiscussion();
        OCSUGC_DiscussionDetailController.editDiscussion('true', item.Id, fdabGrp.id,'false');
        // Commented out as it's not required to initialize it again
        //controller = new OCSUGC_DiscussionDetailController();
        //OCSUGC_DiscussionDetailController.stringComments = 'test comment';
        //controller.commentUploadWithAttachment(); 
        Test.stopTest();
    }
}
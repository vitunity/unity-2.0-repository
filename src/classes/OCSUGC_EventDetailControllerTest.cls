/*
Name        : Cake_EventDetailControllerTest
Created By  : Priyanka Kumar
Date        : 30 July, 2014
Purpose     : Test class  for Cake_EventDetailController
Modified By     : Sidhant Agarwal
Modified Date   : 3 September, 2014
*/

@IsTest(seeAllData=true)
public class OCSUGC_EventDetailControllerTest {
        static CollaborationGroup fdabGrp;
        static OCSUGC_Intranet_Content__c event;
        static FeedItem post;
        static FeedComment comment;
        static OCSUGC_Intranet_Content__c bookmark;
        static OCSUGC_Intranet_Content_Tags__c contentTag;
        static Network ocsugcNetwork;
        static OCSUGC_Tags__c tag;
        static FeedLike flike;
        static {
            ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
            fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted',ocsugcNetwork.id, true);
            tag = OCSUGC_TestUtility.createTags('testTag5');
            insert tag;
            event = OCSUGC_TestUtility.createEvent(fdabGrp.id,Date.today(), Date.today().addDays(2),false);
            event.recordTypeID = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Events').getRecordTypeId();
            event.OCSUGC_Title__c = 'test group';
            event.OCSUGC_Location__c = 'NYC';
            event.OCSUGC_Posted_By__c = UserInfo.getUserId();
            event.OCSUGC_Description__c = 'test Description';
            insert event;
            
            bookmark = OCSUGC_TestUtility.createEvent(fdabGrp.id,Date.today(), Date.today().addDays(2),false);
            bookmark.recordTypeID = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Bookmarks').getRecordTypeId();
            bookmark.OCSUGC_Bookmark_Id__c = event.Id;
            bookmark.OCSUGC_Bookmark_Type__c = 'Discussion';
            bookmark.OCSUGC_Posted_By__c = UserInfo.getUserId();
            bookmark.OCSUGC_Description__c = 'https://google.com';
            insert bookmark;
            
            contentTag = OCSUGC_TestUtility.createIntranetContentTags(event.id,tag.id);       
            insert contentTag;
            
            post = OCSUGC_TestUtility.createFeedItem(event.id, false);
            post.parentid = event.id;
            post.type =  'ContentPost';
            post.body = event.Id+'_comments';
            post.contentfilename = 'contentfile';
            post.contentdata = Blob.valueOf('dummy data');
            post.CreatedById = UserInfo.getUserId();
            insert post;
            
            flike = OCSUGC_TestUtility.createFeedLike(post.id, UserInfo.getUserId(),  true);
            comment = OCSUGC_TestUtility.createFeedComment(post.id,true);
            OCSUGC_TestUtility.createBlackListWords('fuck');
          }
    public static testmethod void testEventDetailController(){
        
            PageReference pageRef = Page.OCSUGC_CreateNewEvent;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('eventId', event.Id);

            OCSUGC_EventDetailController eventcontroller  = new OCSUGC_EventDetailController();
            Test.startTest();
            OCSUGC_EventDetailController.editEvent('True', event.Id, fdabGrp.Id,'True');

            //Asserts
            //system.assertnotequals(null, eventcontroller.intranetContent.Id);
            //system.assert(eventcontroller.listIntranetContentTags.size() > 0);
            //system.assert(eventcontroller.childFeedItems.size() > 0);

            eventcontroller.updateViewCount();
            eventcontroller.selectedChatterFeedId = comment.Id;
            OCSUGC_EventDetailController.stringComments =  'Comment on your post';
            eventcontroller.insertFeedComment();

            system.assertEquals(OCSUGC_EventDetailController.stringComments, null);

            eventcontroller.updateFlagAction();

            OCSUGC_EventDetailController.likeUnlikeFeedItem('True',event.Id, post.Id, 'True');
            OCSUGC_EventDetailController.likeUnlikeFeedItem('True',event.Id, event.Id, 'True');
            OCSUGC_EventDetailController.manageBookmark(event.Id);
            OCSUGC_EventDetailController.manageBookmark(post.Id);
            OCSUGC_EventDetailController.insertFeedCommentRemote('Test Comment', event.Id, post.Id, true, 3,'True');
            OCSUGC_EventDetailController.checkForBlackListWords('fuck ass');
            OCSUGC_EventDetailController.likeUnlikeChatterOnFeed(event.Id, post.Id, comment.Id, 'True','True');

            eventcontroller.attachment.Name = 'Attachment';
            eventcontroller.attachment.body = Blob.valueOf('Test Data');
            eventcontroller.attachment.ContentType = 'text';
            OCSUGC_EventDetailController.stringComments = 'test comment 123';
            eventcontroller.commentUploadWithAttachment();

            eventcontroller.showConfirmation();
            eventcontroller.selectedChatterFeedId = comment.Id;
            eventcontroller.isFlagged = false;
            eventcontroller.flagComment();
        Test.stopTest();
    }

   
}
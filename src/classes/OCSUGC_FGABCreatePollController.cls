//
// (c) 2014 Appirio, Inc.
//
// Controller class for OCSUGC_FGABCreatePoll page
//
// Nov 20, 2014       Ajay Gautam (Appirio)       Original [Ref: T-335193]
// Nov 27, 2014       Ajay Gautam (Appirio)       Modified - Added support for Edit Mode [Ref: T-335676]
// Dec 19, 2014       Neeraj Sharma               Modified Ref: T-340219 Verify content for blacklisted words
// Jan 16, 2015       Ashish Sharma               Modified Ref: T-353562 Update the FGAB Create Poll VF Page with a Checkbox field
// Apr 16, 2015       Naresh K Shiwani            Modified Ref: T-377858,T-378815 Commented isLoginUserManagerOrContributor logics.
// Apr 16, 2015       Naresh K Shiwani            Modified Ref: T-379076 Modified code for OncoPeer poll creation.

public class OCSUGC_FGABCreatePollController extends OCSUGC_Base {

  	public static final Integer MAX_POLL_CHOICES_ALLOWED { get ; private set;} // Max PollChoices supported.
  	public List<wrapperTag> wrapperTags { get; set; }
  	public String selectedTag { get; set; }
  	public String pollIdForEdit { get; set; }
  	public FeedItem poll  { get; set; } // Used for FLS and Sharing purpose only
  	public ConnectApi.FeedElement feedElementPoll { get ; private set;} // FeedItem of Type Poll
  	public OCSUGC_Poll_Details__c pollDetail  { get; set; } // Custom object containg details for Poll
  	public List<pollChoiceWrapper> lstChoices  { get; set; }
  	public ConnectApi.PollCapabilityInput pollChoices  { get; set; } // Poll Choices and Capabilities
  	public Boolean isFGAB { get; private set; }
  	public Boolean isEditMode { get; private set; }
  	public String groupId { get; set; }
	//  public Boolean isLoginUserManagerOrContributor { get; set; }
  	public Boolean isValidRequest {get; set;}
	//  private Map<String,String> allUserRolesMap;
  	private String loggedInUserId = null;
	public boolean isRankedPoll {get; set;}
	public String selectedVisibility {get; set;}
	public String selectedVisibilityValue {get; set;}
	List<CollaborationGroupMember> lstPrivateGroup;
	List<CollaborationGroupMember> lstPublicGroup;
	Set<String> setPicklistValues; 
	public string[] selectedInputTagString;
	
  	//ONCO-421, Setting page-title
    public static String pageTitle{get;set;}	
	
  // Class for Tags
  public class wrapperTag {
    public Boolean isActive {get;set;}
    public String tagName   {get;set;}
    public wrapperTag(Boolean isAct, String name) {
      isActive = isAct;
      tagName = name;
    }
  }

  // Wrapper class for Choice Options
  public class pollChoiceWrapper {
    public String strChoice { get;set; }
    public pollChoiceWrapper() {
      strChoice = '';
    }
    public pollChoiceWrapper(String strChoice) {
      this.strChoice = strChoice;
    }
  }

  static {
    MAX_POLL_CHOICES_ALLOWED = 10; // A Poll can contains 10 choices at Max. Ref: API32 (2014)
  }

  public OCSUGC_FGABCreatePollController() {
    isFGAB = false;
    groupId = null;
    pollIdForEdit = null;
    isEditMode = false;
//    isLoginUserManagerOrContributor = false;
    loggedInUserId = UserInfo.getUserId();
    poll = new FeedItem();
    pollDetail = new OCSUGC_Poll_Details__c();
    wrapperTags = new List<wrapperTag>();
    pollChoices = new ConnectApi.PollCapabilityInput();
    setPicklistValues = new Set<String>();
    selectedInputTagString = new string[]{};
    prepareChoiceRecords();
    // Check if Valid request
    if(ApexPages.currentPage().getParameters().containsKey('g')
          && ApexPages.currentPage().getParameters().containsKey('fgab')
          && ApexPages.currentPage().getParameters().get('fgab') != null) {
      isFGAB = Boolean.valueOf(ApexPages.currentPage().getParameters().get('fgab'));
      groupId = ApexPages.currentPage().getParameters().get('g');
    }
    if(ApexPages.currentPage().getParameters().containsKey('Id')) {
    	pollIdForEdit = ApexPages.currentPage().getParameters().get('Id');
    }
    //ONCO-421, Unique Page-Title
	if(isDC) {
		pageTitle = 'New Poll | Developer';
	} else {
		pageTitle = 'New Poll | OncoPeer';
	}
    // Only Community Manager/Contributor will be able to Create new Poll from within a specific FG/AB.
//    allUserRolesMap = OCSUGC_Utilities.fatchAllUserRoles();

//    if(allUserRolesMap != null && allUserRolesMap.containsKey(loggedInUserId)) {
//    	isLoginUserManagerOrContributor = OCSUGC_Utilities.isManagerOrContributor(allUserRolesMap.get(loggedInUserId));
//    }
		//T-379076 Modified For OncoPeer**Start**
		if(!isFGAB && String.isBlank(pollIdForEdit)) {
			fetchGroup();
			isValidRequest = true;
		} else if(isFGAB && groupId == null) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_ErrorPollCreation));
      isValidRequest = false;
    } 
/*    else if(!isLoginUserManagerOrContributor){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Access_Denied_Message));
      isValidRequest = false;
    }*/ 
    else if (pollIdForEdit != null) { // T-335676
    	isEditMode = true;
    	isValidRequest = validateAndSetupEditMode(pollIdForEdit);
    } else {
      isValidRequest = true;
    }
    //T-379076 Modified For OncoPeer**End**
  }
  
  public List<SelectOption> getVisibilities() {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', 'Select Group'));
			options.add(new SelectOption('Everyone', (isDC ? Label.OCSDC_Everyone_Within_Developer_Cloud : Label.OCSUGC_Everyone_Within_OncoPeer)));
			SelectOption opt;
			if(lstPrivateGroup.size() > 0){
			    opt = new SelectOption('None', '<optgroup label="Closed Groups"></optgroup>');
			    opt.setEscapeItem(false);
			    options.add(opt);
			    for(CollaborationGroupMember grp :lstPrivateGroup) {
			         options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
			    }
			}
			if(lstPublicGroup.size() > 0){
			    opt = new SelectOption('None', '<optgroup label="Open Groups"></optgroup>');
			    opt.setEscapeItem(false);
			    options.add(opt);
			    for(CollaborationGroupMember grp :lstPublicGroup) {
			    	 System.debug('grp============'+grp.CollaborationGroup.Name);
			         options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
			    }
			}
			return options;
   }
   
   private void fetchGroup() {
        Map<Id,String> mapGroups = new Map<Id,String>();
        lstPrivateGroup = new List<CollaborationGroupMember>();
        lstPublicGroup = new List<CollaborationGroupMember>();
		// T-379076 Added is FGAB Condition
        if(isManagerOrContributor && isFGAB) {
        		for(CollaborationGroupMember grp :[Select Id,MemberId,CollaborationGroup.OwnerId,
                                                      CollaborationGroup.Name,
                                                      CollaborationGroupId,
                                                      CollaborationGroup.CollaborationType
	                                              From CollaborationGroupMember
	                                              Where CollaborationGroup.IsArchived = false
	                                              And CollaborationGroup.NetworkId =: networkId
	                                              order by CollaborationGroup.Name]) {
	                    if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
	                    if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
	            }
        	} else {
	            for(CollaborationGroupMember grp :[Select Id,
	                                                      CollaborationGroup.Name,
	                                                      CollaborationGroupId,
	                                                      CollaborationGroup.CollaborationType
	                                              From CollaborationGroupMember
	                                              Where CollaborationGroup.IsArchived = false
	                                              And CollaborationGroup.NetworkId =: networkId
	                                              And (MemberId =:userInfo.getUserId()
	                                              OR CollaborationGroup.OwnerId =:userInfo.getUserId())
	                                              order by CollaborationGroup.Name]) {
                      if(!mapGroups.containsKey(grp.CollaborationGroupId)) {
                        mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
	                      if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
	                      if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
                      }
	            }
        	}
       System.debug('lstPublicGroup========='+lstPublicGroup);
    }


  // @description: Prepare Edit mode for Page
  // @param: none
  // @return: Boolean
  // @author: Ajay Gautam (T-335676)
  // 11-12-2014  Puneet Sardana  Poll can be edited or deleted by manager or contributor Ref I-141581
  public Boolean validateAndSetupEditMode(String pollIdForEditIN) {

  	String netwrkId = Test.isRunningTest() ? OCSUGC_TestUtility.OCSUGCNetworkId : networkId;
  	Boolean isValidEdit = false;
    lstChoices = new List<pollChoiceWrapper>();
    // to do : add
    for(OCSUGC_Poll_Details__c objPD : [ SELECT OCSUGC_Poll_Question__c, OCSUGC_Poll_Id__c,OCSUGC_Group_Name__c,
                                                OCSUGC_Is_Deleted__c, OCSUGC_Group_Id__c, CreatedById,
                                                OCSUGC_IsRankedPoll__c
                                         FROM   OCSUGC_Poll_Details__c
                                         WHERE  OCSUGC_Poll_Id__c = :pollIdForEditIN
                                         AND    OCSUGC_Is_Deleted__c = false LIMIT 1]) {
			selectedVisibility = objPD.OCSUGC_Group_Id__c;
      selectedVisibilityValue = objPD.OCSUGC_Group_Name__c;  
      // Changed Condition to Poll Created by user and isManager		T-377858
    	if(objPD.CreatedById != UserInfo.getUserId() && !isManager) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Cannot_Edit_Poll_Message));
    	} else {
    		ConnectApi.PollCapability objPollCapability = ConnectApi.ChatterFeeds.getFeedElementPoll(netwrkId, pollIdForEditIN);
        if(objPollCapability != null) {
        	feedElementPoll = ConnectApi.ChatterFeeds.getFeedElement(netwrkId, pollIdForEditIN);
        	lstChoices.clear();
	        for(ConnectApi.FeedPollChoice objFPC : objPollCapability.choices) {
            lstChoices.add(new pollChoiceWrapper(objFPC.text));
	        }
        }
        for(FeedItem objPollFeedItem : [SELECT Id, Body FROM FeedItem WHERE Id = :pollIdForEditIN LIMIT 1]) {
        	poll = objPollFeedItem;
        }
        isValidEdit = (lstChoices.size() > 0 && poll.Id != null);
        if(isValidEdit) {
        	fetchExistingTagsOnPoll(); // Fetch Existing Tags
        }
    	}

    	isRankedPoll = objPD.OCSUGC_IsRankedPoll__c;
    }
    return isValidEdit;
  }

  // @description: Fetch Existing Tags on Poll
  // @param: none
  // @return: void
  // @author: Ajay Gautam (T-335676)
	private void fetchExistingTagsOnPoll() {
		wrapperTags = new List<wrapperTag>();
	  for(OCSUGC_PollDetail_Tags__c tag :[SELECT   OCSUGC_Tags__c, OCSUGC_Tags__r.OCSUGC_Tag__c, OCSUGC_PollFeedItem__c
					                              FROM     OCSUGC_PollDetail_Tags__c
					                              WHERE    OCSUGC_PollFeedItem__c = :poll.Id
					                              ORDER BY OCSUGC_Tags__r.OCSUGC_Tag__c]) {
      wrapperTags.add(new wrapperTag(true, tag.OCSUGC_Tags__r.OCSUGC_Tag__c));
      setPicklistValues.add(tag.OCSUGC_Tags__r.OCSUGC_Tag__c.toUpperCase());
    }
	}


  // @description: Prepare String type choices to be displayed on Page
  // @param: none
  // @return: none
  // @author: Ajay Gautam (T-335193)
  private void prepareChoiceRecords() {
    lstChoices = new List<pollChoiceWrapper>();
    for( Integer i = 0; i < MAX_POLL_CHOICES_ALLOWED; i++) {
      lstChoices.add(new pollChoiceWrapper());
    }
  }

  // @description: Add Tags on Poll
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author: Ajay Gautam (T-335193)
  public PageReference addTag() {
    
    //ONCO-402, Ability to Add multiple tags at the same time
        set<string> selectedTagSet = new set<string>();
        
        if(selectedTag != null && selectedTag.trim() != ''){
        	selectedTag = selectedTag.trim();
        	selectedTagSet.add(selectedTag);	
        }
        
        for(string sltTag :selectedTagSet){
        	 selectedInputTagString = sltTag.split(',');
        }
        
        
        for(String tagValue : selectedInputTagString){
        	if(!setPicklistValues.contains((tagValue.trim()).toUpperCase())){
        		setPicklistValues.add((tagValue.trim()).toUpperCase());
            	list<String> contentList = new list<String>();
            	contentList.add(tagValue.trim());
            	if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                	return null;
            	}
            	wrapperTags.add(new wrapperTag(true, tagValue.trim()));
            	tagValue = null;
        	}	
        }
    
    return null;
  }

  // @description: Remove Tags on Poll
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author: Ajay Gautam (T-335193)
  public PageReference updateTag() {
    for(Integer cnt=0; wrapperTags.size()>cnt; cnt++) {
      if(wrapperTags[cnt].tagName.equals(selectedTag)) {
        wrapperTags.remove(cnt);
        setPicklistValues.remove(selectedTag.toUpperCase());
      }
    }
    selectedTag = null;
    return null;
  }

  // Map of Tags and its Id
  public static Map<String, Id> mapTags {
    get {
      if(mapTags == null) {
        mapTags = new Map<String, Id>();
        for(OCSUGC_Tags__c tg : [SELECT Id, OCSUGC_Tag__c FROM OCSUGC_Tags__c]) {
          mapTags.put(tg.OCSUGC_Tag__c.toLowerCase(), tg.Id);
          }
        }
        return mapTags;
    }
    private set;
  }

  // @description: Upsert Tags in Database. Invoked by i) Create Poll methods and ii) updatePoll method
  // @param: none
  // @return: none
  // @author: Ajay Gautam (T-335193)
  private void addTagsOnPoll() {
    List<OCSUGC_PollDetail_Tags__c> lstPollDetailTags = new List<OCSUGC_PollDetail_Tags__c>();
    List<OCSUGC_Tags__c> lstTags = new List<OCSUGC_Tags__c>();
    if(poll != null && poll.Id != null) {
	    for(OCSUGC_PollDetail_Tags__c tag :  [SELECT Id FROM OCSUGC_PollDetail_Tags__c WHERE OCSUGC_PollFeedItem__c = :poll.Id]) {
	      lstPollDetailTags.add(tag);
	    }
	    if(lstPollDetailTags.size() > 0) {
	      delete lstPollDetailTags; // DELETE existing tags
	    }
	    lstPollDetailTags = new List<OCSUGC_PollDetail_Tags__c>(); // RESET
    }
    for(wrapperTag wrapper : wrapperTags) {
      if(wrapper.isActive) {
        if(String.isNotBlank(wrapper.tagName)) {
          OCSUGC_Tags__c newTag = new OCSUGC_Tags__c(OCSUGC_Tag__c = wrapper.tagName);
          if(mapTags.containsKey(wrapper.tagName.toLowerCase())) {
            newTag.Id = mapTags.get(wrapper.tagName.toLowerCase());
          }
          if(isFGAB) {
            newTag.OCSUGC_IsFGABUsed__c = true;
          } else if(isDC) {
            newTag.OCSDC_IsDeveloperCloudUsed__c = true;
          } else {
          	newTag.OCSUGC_IsOncoPeerUsed__c = true;
          }
          lstTags.add(newTag);
        }
      }
    }
    if(lstTags != null && !lstTags.isEmpty()) {
       upsert lstTags;
    }
    // Insert new tags
    for(OCSUGC_Tags__c tg : lstTags) {
        lstPollDetailTags.add(new OCSUGC_PollDetail_Tags__c(OCSUGC_Tags__c = tg.Id, OCSUGC_PollFeedItem__c = feedElementPoll.Id));
    }
    if(lstPollDetailTags != null && !lstPollDetailTags.isEmpty()){
       insert lstPollDetailTags;
    }
  }

  // @description: Updates Poll i.e. ONLY Tags.
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author: Ajay Gautam (T-335676)
  public PageReference updatePoll () {
  	Pagereference returnURL = null;
  	Savepoint sp = Database.setSavepoint(); // Set SavePoint - Transaction Control
  	try {
  		/* ONCO-453	Puneet Mishra,	upserting FeedItem for Poll and then upserting OCSUGC_Poll_Details__c record to store the updated Poll Question
  									which was not in place earlier.
  		*/
  		if(feedElementPoll.Id != null) {
  			upsert poll;
  			
  			pollDetail = new OCSUGC_Poll_Details__c();
  			pollDetail = [SELECT Id, OCSUGC_Poll_Id__c, OCSUGC_Poll_Question__c FROM OCSUGC_Poll_Details__c WHERE OCSUGC_Poll_Id__c =: feedElementPoll.Id LIMIT 1];
  			pollDetail.OCSUGC_Poll_Question__c = poll.Body;
  			upsert pollDetail;
  		}	    
	  	
	  	addTagsOnPoll();
	    returnURL = Page.OCSUGC_FGABPollDetail;
	    returnURL.getParameters().put('g',groupId);
	    returnURL.getParameters().put('fgab',''+isFGAB);
	    returnURL.getParameters().put('Id',''+feedElementPoll.Id);
	    if(isDC) {
        	returnURL.getParameters().put('dc','true');
      }
  	} catch (Exception ex) {
      Database.rollback(sp);
      System.debug('>>> Exception '+ ex.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
  	}
  	return returnURL;
  }

  // @description: Creates Poll, Tags and PollDetail record.
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author: Ajay Gautam (T-335193)
  public PageReference createPoll () {
    Pagereference returnURL = null;
    List<String> strList = new List<String>();
    ConnectApi.FeedElementCapabilitiesInput objCapabilities = new ConnectApi.FeedElementCapabilitiesInput();
    ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
    ConnectApi.FeedItemInput objFeedItemInput = new ConnectApi.FeedItemInput();
    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();

    Savepoint sp = Database.setSavepoint(); // Set SavePoint - Transaction Control

    for(pollChoiceWrapper objW : lstChoices) {
      if(String.isNotBlank(objW.strChoice)) {
        strList.add(objW.strChoice);
      }
    }
    pollChoices.choices = strList;
    try {
      if(String.isBlank(poll.Body) || strList.size() < 2 || strList.size() > MAX_POLL_CHOICES_ALLOWED) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Polls_Must_Have_Question_and_Two_Options));
        returnURL = null;
      } else {
      	 //ref:T-340219 Verifying description content for blacklisted content
        list<String> contentList = new list<String>();
        contentList.add(poll.Body);
        if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
            return null;
        }
        //End ref:T-340219
        objCapabilities.poll = pollChoices;
        textSegmentInput.Text = poll.Body;
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        messageBodyInput.messageSegments.add(textSegmentInput);
        objFeedItemInput.capabilities = objCapabilities;
        objFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        System.debug('groupId============CreatePoll===='+groupId);
        if(groupId != 'Everyone') {
						objFeedItemInput.subjectId = groupId;        	
        } else {
        	objFeedItemInput.subjectId = userInfo.getUserId();
        }
        objFeedItemInput.body = messageBodyInput;
        feedElementPoll = ConnectApi.ChatterFeeds.postFeedElement(NetworkId, objFeedItemInput, null); // Post the Poll
        returnURL = Page.OCSUGC_FGABPollDetail;
        returnURL.getParameters().put('g',groupId);
        returnURL.getParameters().put('fgab',''+isFGAB);
        returnURL.getParameters().put('Id',''+feedElementPoll.Id);
        if(isDC) {
        	returnURL.getParameters().put('dc','true');
        }
        if(feedElementPoll != null && feedElementPoll.Id != null) {
          addTagsOnPoll();
          createPollDetailRecord(feedElementPoll.Id);
        }
      }
    } catch (Exception ex) {
      Database.rollback(sp);
      System.debug('>>> Exception '+ ex.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }
    return returnURL;
  }

  // @description: Utility method for Create Poll. Will be inserting Custom object PollDetail records
  // @param: none
  // @return: none
  // @author: Ajay Gautam (T-335193)
  private void createPollDetailRecord(Id pollFeedItemId) {
    String groupName = null;
    System.debug('groupId========createPollDetailRecord==='+groupId);
    for(CollaborationGroup objGroup : [SELECT Name FROM CollaborationGroup WHERE Id = :groupId LIMIT 1 ]) {
      groupName = objGroup.Name;
    }
    
    if(groupId == 'Everyone') {
    	groupName = isDC ? Label.OCSDC_Everyone_Within_Developer_Cloud : Label.OCSUGC_Everyone_Within_OncoPeer;
    }
    System.debug('groupName========createPollDetailRecord==='+groupName);
    pollDetail = new OCSUGC_Poll_Details__c();
    pollDetail.OCSUGC_Poll_Id__c = pollFeedItemId;
    pollDetail.OCSUGC_Group_Id__c = groupId;
    pollDetail.OCSUGC_Group_Name__c = String.isNotBlank(groupName) ? groupName : groupId;
    pollDetail.OCSUGC_Poll_Question__c = poll.Body;
    pollDetail.OCSUGC_Is_Deleted__c = false;
    pollDetail.OCSUGC_Is_FGAB_Poll__c = isFGAB;
    pollDetail.OCSUGC_Likes__c = 0;
    pollDetail.OCSUGC_Views__c = 0;
    pollDetail.OCSUGC_Comments__c = 0;
    pollDetail.OCSUGC_IsRankedPoll__c = isRankedPoll;
    System.debug('pollDetail====='+pollDetail);
    insert pollDetail;
  }

  // @description: Cancel operations. Cancels current operation and navigates the user to FGAB GroupHomePage.
  // @param: none
  // @return: Pagereference null; this is used to perform AJAX Ops.
  // @author: Ajay Gautam (T-335193)
  public Pagereference cancelOps() {
    Pagereference pg;
    if(!isValidRequest) {
      pg = Page.OCSUGC_Home;
      pg.getParameters().put('isResetPwd','true');
    } else {
    	if(isEditMode) {
        pg = Page.OCSUGC_FGABPollDetail; // T-335676
        pg.getParameters().put('Id',''+feedElementPoll.Id);
    	} else {
    	  pg = Page.OCSUGC_FGABAboutGroup;
    	}
      pg.getParameters().put('g',groupId);
      pg.getParameters().put('fgab',''+isFGAB);
      if(isDC) {
      	pg.getParameters().put('dc','true');
      }
    }
    pg.setRedirect(true);
    return pg;
  }

}//EOF
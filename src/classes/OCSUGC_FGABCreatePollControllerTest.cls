//
// (c) 2015 Appirio, Inc.
//
// Test Class for - OCSUGC_FGABCreatePollController
//
// February 2nd, 2015     Harshit Jain[T-317348]
@IsTest(SeeAllData=true)
private class OCSUGC_FGABCreatePollControllerTest {
	 static CollaborationGroup fdabGrp;
	 static User adminUser;
	 static ConnectApi.FeedElement feedElementPoll;

    static testMethod void testFGABCPollCreationWithoutgrpId() {
				Test.startTest();
	        OCSUGC_FGABCreatePollController controller = new OCSUGC_FGABCreatePollController();

	        //Assert poll creation without groupId
	        system.assertEquals(true, controller.isValidRequest);
	        controller.cancelOps();
	      Test.stopTest();
    }

    static testMethod void testFGABCPollCreationWithoutValidMember() {
    		createTestData();
    		insert adminUser;

				Test.startTest();

					system.runAs(adminUser) {
						PageReference pageRef = Page.OCSUGC_FGABCreatePoll;
		        Test.setCurrentPage(pageRef);
		        ApexPages.currentPage().getParameters().put('g', fdabGrp.Id);
		        ApexPages.currentPage().getParameters().put('fgab', 'true');

		        OCSUGC_FGABCreatePollController controller = new OCSUGC_FGABCreatePollController();

		        //Assert poll creation without groupId
		        system.assertEquals(true, controller.isValidRequest);
					}

	      Test.stopTest();
    }


    static testMethod void testFGABCPollCreation() {
    		createTestData();
    		insert adminUser;

				system.runAs(adminUser) {
					Test.startTest();
						adminUser.OCSUGC_User_Role__c = 'Varian Community Manager';
						update adminUser;
						PageReference pageRef = Page.OCSUGC_FGABCreatePoll;
		        Test.setCurrentPage(pageRef);
		        ApexPages.currentPage().getParameters().put('g', fdabGrp.Id);
		        ApexPages.currentPage().getParameters().put('fgab', 'true');

		        OCSUGC_FGABCreatePollController controller = new OCSUGC_FGABCreatePollController();
		        controller.isValidRequest = true;
		        controller.isRankedPoll = true;
						controller.poll.body = 'Pool Question';
						controller.lstChoices[0].strChoice = 'Choice 1';
						controller.lstChoices[1].strChoice = 'Choice 2';
						controller.selectedTag = 'Test tag 1';
						controller.addTag();
						controller.selectedTag = 'Test tag 2';
						controller.addTag();
						controller.selectedTag = 'Test tag 2';
		        controller.updateTag();

		        controller.createPoll();
		        controller.cancelOps();
		      Test.stopTest();

		      //Assert pollDetail record
   	      system.assertNotEquals(null, controller.pollDetail.Id);
				}
    }

     static testMethod void testFGABCPollUpdation() {
     			createTestData();
     			insert adminuser;
     			createTestFeed();
	        OCSUGC_TestUtility.createPollDetail(fdabGrp.Id, 'TestFGABGrp', feedElementPoll.Id, 'Poll Question', true);

	     			Test.startTest();
	     				PageReference pageRef = Page.OCSUGC_FGABCreatePoll;
		        	Test.setCurrentPage(pageRef);
			        ApexPages.currentPage().getParameters().put('g', fdabGrp.Id);
			        ApexPages.currentPage().getParameters().put('fgab', 'true');
			        ApexPages.currentPage().getParameters().put('Id', feedElementPoll.Id);

			        OCSUGC_FGABCreatePollController controller = new OCSUGC_FGABCreatePollController();
			        controller.isValidRequest = true;
			      //  controller.isLoginUserManagerOrContributor = true;
			        controller.validateAndSetupEditMode(feedElementPoll.Id);
			        controller.selectedTag = 'Test tag 5';
							controller.addTag();
			        controller.updatePoll();

			        //Assert size of tags
			        system.assertEquals(2, [Select Id from OCSUGC_PollDetail_Tags__c where OCSUGC_PollFeedItem__c =:feedElementPoll.Id].size());
			      Test.stopTest();
     }

    private static void createTestData() {
		Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
		Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
		fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted',ocsugcNetwork.id, true);
		adminUser = OCSUGC_TestUtility.createStandardUser(false,adminProfile.id,'AdminUser','1');
		
    }

    private static void createTestFeed() {
    		ConnectApi.PollCapabilityInput pollChoices = new ConnectApi.PollCapabilityInput();
	 			ConnectApi.FeedElementCapabilitiesInput objCapabilities = new ConnectApi.FeedElementCapabilitiesInput();
				ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
		    ConnectApi.FeedItemInput objFeedItemInput = new ConnectApi.FeedItemInput();
		    ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();

				pollChoices.choices = new List<String>{'Choice 1', 'Choice 2'};
	 			objCapabilities.poll = pollChoices;
        textSegmentInput.Text = 'Poll Question';
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        messageBodyInput.messageSegments.add(textSegmentInput);
        objFeedItemInput.capabilities = objCapabilities;
        objFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        objFeedItemInput.subjectId = fdabGrp.Id;
        objFeedItemInput.body = messageBodyInput;
        feedElementPoll = ConnectApi.ChatterFeeds.postFeedElement(OCSUGC_TestUtility.OCSUGCNetworkId, objFeedItemInput, null);

        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('test tag 1');
        insert tag;

        OCSUGC_TestUtility.createPollDetailTag(tag.Id, feedElementPoll.Id, true);
    }
}
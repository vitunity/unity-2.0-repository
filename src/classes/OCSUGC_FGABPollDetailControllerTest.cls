//
// (c) 2015 Appirio, Inc.
//
// Test Class for - OCSUGC_FGABPollDetailController
//
// February 4nd, 2015     Harshit Jain[T-317348]
@isTest(SeeAllData=true)
private class OCSUGC_FGABPollDetailControllerTest {
        static CollaborationGroup fdabGrp;
        static ConnectApi.FeedElement feedElementPoll;
        static ConnectApi.Comment feedComment;
        static Network osugcNetwork;
        static OCSUGC_Intranet_Content__c bookmark;
    static {
        osugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        fdabGrp = OCSUGC_TestUtility.createGroup('TestFGABGrp', 'Unlisted', osugcNetwork.Id, true);

        ConnectApi.PollCapabilityInput pollChoices = new ConnectApi.PollCapabilityInput();
        ConnectApi.FeedElementCapabilitiesInput objCapabilities = new ConnectApi.FeedElementCapabilitiesInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
        ConnectApi.FeedItemInput objFeedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();

        pollChoices.choices = new List<String>{'Choice 1', 'Choice 2'};
        objCapabilities.poll = pollChoices;
        textSegmentInput.Text = 'Poll Question';
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        messageBodyInput.messageSegments.add(textSegmentInput);
        objFeedItemInput.capabilities = objCapabilities;
        objFeedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        objFeedItemInput.subjectId = fdabGrp.Id;
        objFeedItemInput.body = messageBodyInput;
        feedElementPoll = ConnectApi.ChatterFeeds.postFeedElement(osugcNetwork.Id, objFeedItemInput, null);

        //Insert feedComment
        feedComment = ConnectApi.ChatterFeeds.postCommentToFeedElement(osugcNetwork.Id, feedElementPoll.Id, 'Test comment 1.');

        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('xxtest tag 1 2');
        insert tag;

        OCSUGC_TestUtility.createPollDetailTag(tag.Id, feedElementPoll.Id, true);
        OCSUGC_TestUtility.createPollDetail(fdabGrp.Id, 'TestFGABGrp', feedElementPoll.Id, 'Poll Question', true); 
        bookmark = OCSUGC_TestUtility.createEvent(fdabGrp.Id,Date.today(),Date.today().addDays(2),false);
        bookmark.recordTypeID = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Bookmarks').getRecordTypeId();
        bookmark.OCSUGC_Bookmark_Id__c = feedElementPoll.Id;
        bookmark.OCSUGC_Bookmark_Type__c = 'Discussion';
        bookmark.OCSUGC_Posted_By__c = UserInfo.getUserId();
        insert bookmark;
    }

    static testMethod void testPollDetail() {
        
        OCSUGC_PollRanking_Info__c pollRankingInfo = new OCSUGC_PollRanking_Info__c();
        pollRankingInfo.OCSUGC_PollID__c = feedElementPoll.Id;
        pollRankingInfo.OCSUGC_PollOption__c = 'Test Poll';
        pollRankingInfo.OCSUGC_PollSharedWith__c = fdabGrp.id;
        pollRankingInfo.OCSUGC_OptionRank__c = 10;
        pollRankingInfo.OCSUGC_UserID__c = Userinfo.getUserId();
        pollRankingInfo.Poll_Option_Id__c = feedElementPoll.Id;
        insert pollRankingInfo;

        Test.startTest();
            PageReference pageRef = Page.OCSUGC_FGABPollDetail;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id', feedElementPoll.Id);
            ApexPages.currentPage().getParameters().put('fgab', 'true');

            OCSUGC_FGABPollDetailController controller = new OCSUGC_FGABPollDetailController();
            controller.refreshPollResults();
            controller.changeMyVote();
            controller.changeMyRankingVote();

            OCSUGC_FGABPollDetailController.stringComments = 'Test comment';
            controller.attachment.Name = 'Attachment';
            controller.attachment.body = Blob.valueOf('Test Data');
            controller.attachment.ContentType = 'text';
            controller.commentUploadWithAttachment();
            controller.selectedCommentId = feedComment.Id;
            controller.getRelatedPolls();
            controller.isFlagged = false;
            controller.flagComment();

            controller.pollRankingOrder = ';'+feedElementPoll.Id+'-12;'+'12-12';
            controller.assignPollRanking();
            controller.showConfirmation();
            controller.updateViewCount();

            OCSUGC_FGABPollDetailController.insertFeedCommentRemote('test comment', fdabGrp.Id, feedElementPoll.Id, 12, 'false','false');
            OCSUGC_FGABPollDetailController.checkForBlackListWords('test comment');
            OCSUGC_FGABPollDetailController.likeUnlikeComment(osugcNetwork.id, 'Community', feedComment.Id ,feedElementPoll.Id, fdabGrp.Id,'false','false');
            OCSUGC_FGABPollDetailController.likeUnlikeFeedItem(osugcNetwork.id, 'Community',feedElementPoll.Id, fdabGrp.Id,'false','false');
            OCSUGC_FGABPollDetailController.manageBookmark(feedElementPoll.Id);
            OCSUGC_FGABPollDetailController.editPoll('true',feedElementPoll.Id, fdabGrp.Id,'true');
            controller.voteOnPoll();
            controller.pollDetail.OCSUGC_IsRankedPoll__c = true;
            controller.voteOnPoll();
            system.assertEquals(13, [Select id,OCSUGC_Comments__c from OCSUGC_Poll_Details__c where Id =: controller.pollDetail.Id].OCSUGC_Comments__c);
        Test.stopTest();
    }
}
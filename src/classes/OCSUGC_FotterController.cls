//
// (c) 2014 Appirio, Inc.
//
// Class for Fotter component
//
// 18-09-2014 Puneet Sardana Original (Ref. T-320607)
// 14-09-2014 Naresh K Shiw   T-334644
public class OCSUGC_FotterController extends OCSUGC_Base_NG {

    //Store if user is logged in or not
    public boolean isLoggedInUser {get;set;}
    public Boolean isFGAB {get;set;}
    public String termsAndConditions {get;set;}
    public Integer OCSUGC_Footer_Year {get;set;}
    public Boolean isDC {get;set;}
    
    public OCSUGC_FotterController()
    {
        String fgabGroupId = ApexPages.currentPage().getParameters().get('g');
        isFGAB = false;
        isDC = false;
        isDC = ApexPages.currentPage().getParameters().get('dc')!=null && ApexPages.currentPage().getParameters().get('dc').equals('true');
        if(ApexPages.currentPage().getParameters().get('fgab') == 'true'){
            isFGAB = true;
        }
        isLoggedInUser = UserInfo.getSessionId()!=null ? true : false;
        OCSUGC_Footer_Year = Date.Today().Year();
//      Starts here T-334644    Added isFGAB condition      Naresh K Shiwani
//        isFGAB = false;
//        if(ApexPages.currentPage().getParameters().get('fgab') != null){
//          isFGAB = Boolean.valueOf(ApexPages.currentPage().getParameters().get('fgab'));
            if(isFGAB){
//              Id groupId;
//              groupId = ApexPages.currentPage().getParameters().get('g');
                for(OCSUGC_CollaborationGroupInfo__c gpInfo : [ Select
                                                                   OCSUGC_Group_TermsAndConditions__c
                                                                From OCSUGC_CollaborationGroupInfo__c
                                                                Where OCSUGC_Group_Id__c =:fgabGroupId limit 1]) {
                    termsAndConditions = gpInfo.OCSUGC_Group_TermsAndConditions__c;
                }
            }
//        }
//      Ends here T-334644      Naresh K Shiwani
    }
}
//
// (c) 2014 Appirio, Inc.
//
// test Class for Fotter component
//
// 18-09-2014 Puneet Sardana Original (Ref. T-320607)
@isTest public with sharing class OCSUGC_FotterControllerTest {
	  static CollaborationGroup createdGroup;
	  static String ocsugcNetwork;
    static {
      ocsugcNetwork = OCSUGC_TestUtility.getCurrentCommunityNetworkId(false);
      createdGroup = OCSUGC_TestUtility.createGroup('test group name', 'Public', ocsugcNetwork, true);
    }

    static testmethod void testLoggegInUser() {
        Test.startTest();
	        ApexPages.currentPage().getParameters().put('fgab','true');
	        ApexPages.currentPage().getParameters().put('g',createdGroup.Id);
	        OCSUGC_FotterController controller = new OCSUGC_FotterController();
	        System.assert(controller.termsAndConditions <> null);
        Test.stopTest();
    }
}
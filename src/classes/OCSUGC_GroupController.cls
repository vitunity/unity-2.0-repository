/*
Name        : OCSUGC_ContactUserPermissionSetHandler
Updated By  : Naresh Shiwani (Appirio India)
Date        : 16th Oct, 2014
Purpose     :

Refrence    : T-321409
Dated By    Name                Description                     		Task ID
16/8/2014   Rishi Khirbat	    Initial									Initial
16/8/2014   Naresh K SHiwani	Updated updateGroupInformation added 	T-326286
								IsAutoArchiveDisabled to default true.
17-11-2014  Naresh K Shiwani    T-334407
*/
public without sharing class OCSUGC_GroupController extends OCSUGC_Base {
    //hold new group information
    private final string FGAB_GROUP_TYPE = Label.OCSUGC_FocusGroupAdvisoryBoard;
    public String groupId {get; set;}
    public String screenCode {get; set;}
    public String tagString {get; set;}
    public ContentVersion groupPhoto {get; set;}
    public String commmaSeperatedExtensions {get; set;}
    public CollaborationGroup  newGroup {get; set;}
    public OCSUGC_CollaborationGroupInfo__c newGroupInfo {get; set;}
    //ONCO-421, Setting page-title
    public static String pageTitle{get;set;}
	private Boolean isFGABGroup;
	private static final String IMAGE_PATTERN = '([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)';
    public OCSUGC_GroupController() {
        groupId = ApexPages.currentPage().getParameters().get('g');
        screenCode = ApexPages.currentPage().getParameters().get('screenCode');
        isFGABGroup = ApexPages.currentPage().getParameters().get('fgab')!=null && ApexPages.currentPage().getParameters().get('fgab')=='true' ;
        system.debug('-----------'+groupId+'--------------'+screenCode+'-------------'+isFGABGroup);
        System.debug('Puneet '+isFGABGroup);
        //ONCO-421, Unique Page-Title
        if(isDC) {
			pageTitle = 'New Group | Developer';
		} else if(isFGABGroup) {
        	pageTitle = 'New Advisory Group | Advisory Board';
		} else {
			pageTitle = 'New Group | OncoPeer';
		}
        if(screenCode == null || screenCode.trim() == '') screenCode = '0';
        groupPhoto = new ContentVersion();
        newGroup = new CollaborationGroup();
        newGroupInfo = new OCSUGC_CollaborationGroupInfo__c();
        commmaSeperatedExtensions = Label.OCSUGC_Group_File_Extensions;
        fatchGroupInfo();
    }

    private void fatchGroupInfo() {
        if(String.isNotBlank(groupId)) {
            for(CollaborationGroup gp :[Select Name,
                                               InformationBody,
                                               SmallPhotoUrl,
                                               FullPhotoUrl,
                                               Description,
                                               CollaborationType
                                        From CollaborationGroup
                                        Where Id =:groupId]) {

                newGroup = gp;
            }

            for(OCSUGC_CollaborationGroupInfo__c gpInfo : [Select Name,
                                                           OCSUGC_Location__c,
                                                           OCSUGC_Group_URL__c,
                                                           OCSUGC_Group_Id__c,
                                                           OCSUGC_Email__c,
                                                  		   OCSUGC_Group_TermsAndConditions__c
                                                    From OCSUGC_CollaborationGroupInfo__c
                                                    Where OCSUGC_Group_Id__c =:newGroup.Id]) {
                newGroupInfo = gpInfo;
            }
			system.debug(' ****** newGroupInfo ****** ' + newGroupInfo.OCSUGC_Group_TermsAndConditions__c);
        }
    }

    public pageReference CollaborationType() {
        System.debug('newGroup.CollaborationType '+newGroup.CollaborationType);
        return null;
    }

    public List<SelectOption> getVisibility(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Public',System.label.OCSUGC_Label_Public_Group_Visible_To_EveryOne));
        options.add(new SelectOption('Private',System.label.OCSUGC_Label_Private_Group_Visible_To_Selected_Users));
        return options;
    }

    public Pagereference redirectPage() {
        Pagereference pg;
        if(isFGABGroup) {
        	pg = Page.OCSUGC_FGABAboutGroup;
        } else {
         	pg  = Page.OCSUGC_GroupDetail;
        }
        if(isDC) {
        	pg.getParameters().put('dc','true');
        }
        pg.getParameters().put('g',newGroup.Id);
        pg.getParameters().put('showPanel','About');
        pg.getParameters().put('fgab',''+isFGABGroup);
        pg.setRedirect(true);
        return pg;
    }



    // @description: Cancel group formation
    // Navigate to home Page in case of new group and group detail page if existing group
    // 18/09/2014 Puneet Sardana For T-320142
    public PageReference cancelGroupInformation() {
        Pagereference pg;
        //If new group navigate to home page
        if(string.isBlank(groupId)) {
            pg = Page.OCSUGC_Home;
            pg.getParameters().put('isResetPwd','true');
        } else {
        	//If existing group navigate to group detail page
           	if(isFGABGroup) {
        		pg = Page.OCSUGC_FGABAboutGroup;
           	} else {
         	 	pg  = Page.OCSUGC_GroupDetail;
           	}
           	pg.getParameters().put('g',groupId);
           	pg.getParameters().put('fgab',''+isFGABGroup);
           	pg.getParameters().put('dc',''+isDC);
        }
        pg.setRedirect(true);
        return pg;
    }
    // @description: method updated new group information.
    // Notify current user via pagemessage
    // 17/09/2014 Puneet Sardana Can Update Group Information as well for T-320142
    public pageReference updateGroupInformation() {
        try {
                PageReference pg = Page.OCSUGC_CreateGroup;
                //Insert or Edit Group Information
                if(String.isBlank(groupID)) {
                    newGroup.NetworkId = networkId;
                    newGroup.IsAutoArchiveDisabled  = true;
                }
          		if(isFGABGroup) {
          			  pg.getParameters().put('fgab','true');
          			  if(String.isBlank(groupId)) {
          			  	//Start Here - 21 Nov 2014    Naresh K Shiwani	(Task Ref. T-334407)
          			  	if(newGroupInfo.OCSUGC_Group_TermsAndConditions__c == null || newGroupInfo.OCSUGC_Group_TermsAndConditions__c== ''){
	          			  	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Term_Condition_Mandatory);
		                    ApexPages.addMessage(myMsg);
		                    pg = Page.OCSUGC_CreateGroup;
				            pg.getParameters().put('screenCode','0');
				            pg.getParameters().put('fgab','true');
				            pg.setRedirect(true);
				            return pg;
          			  	}
          			  	//End Here - 21 Nov 2014    Naresh K Shiwani	(Task Ref. T-334407)
					         newGroupInfo.OCSUGC_Group_Type__c = FGAB_GROUP_TYPE;
					         newGroup.CollaborationType = OCSUGC_Constants.UNLISTED;
          			  }
          		}
          		upsert newGroup;
                //Only in case of group insertion, make entry into group info object
                // Update GroupInfo for each update in Group - T-334411 - Ajay Gautam
                newGroupInfo.Name = newGroup.Name;
                newGroupInfo.OCSUGC_Group_Id__c = newGroup.Id;
                if(newGroupInfo.OCSUGC_Group_TermsAndConditions__c!=null) {
                  newGroupInfo.OCSUGC_Group_TermsAndConditions__c = newGroupInfo.OCSUGC_Group_TermsAndConditions__c.unescapeHtml4();
                }
                upsert newGroupInfo;

                if(String.isBlank(groupId)) {
                    pg.getParameters().put('screenCode','1');
                } else {
                	if(isFGAB){
                		return uploadImage();
									}
                    //if existing group upload image as well
                    uploadImage();
                    pg.getParameters().put('existingGroup','true');
                    pg.getParameters().put('screenCode','2');
                }
                if(isDC) {
                	pg.getParameters().put('dc','true');
                }
                pg.getParameters().put('g',newGroup.Id);
                pg.setRedirect(true);
            return pg;
        } catch(Exception e) {
            String str = e.getMessage();
            if(str.contains('An active or archived group with this name already exists or is being deleted.')) {
                str = 'An active or archived group with this name already exists or is being deleted. Choose a different name or try again in a few minutes.';
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, str));
            return null;
        }
    }

    public pageReference uploadImage() {
        try {
        	if(groupPhoto.versionData != null && newGroup.Id !=null) {
            	ContentWorkspace workSpace = OCSUGC_Utilities.objContentWorkspace(networkName);
                if(workSpace != null) {
                    groupPhoto.FirstPublishLocationId = workSpace.Id; 
                }
                insert groupPhoto;
                Id fileId = [Select ContentDocumentId From ContentVersion Where Id =:groupPhoto.Id].ContentDocumentId;
                try{
                	if(!Test.isRunningTest()) {
                		ConnectApi.Photo photo = ConnectApi.ChatterGroups.setPhoto(null, newGroup.Id, fileId, null);
                	}
                } catch(Exception err){
                	groupPhoto = new ContentVersion();
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, err.getMessage()));
                	System.debug(err.getMessage());
		            return null;
                }
                groupPhoto = new ContentVersion();
            }
            if(isFGAB) {
        					PageReference aboutPage = Page.OCSUGC_FGABAboutGroup;
					        aboutPage.getParameters().put('g',groupId);
   								aboutPage.getParameters().put('fgab',''+isFGABGroup);
				        	aboutPage.setRedirect(true);
        					return aboutPage;
            }
            Pagereference pg = Page.OCSUGC_CreateGroup;
            pg.getParameters().put('g',newGroup.Id);
            pg.getParameters().put('screenCode','2');
            if(isFGABGroup) {
            	pg.getParameters().put('fgab','true');
            }
            if(isDC) {
            	pg.getParameters().put('dc','true');
            }
            pg.setRedirect(true);
            return pg;
        } catch(Exception e) {
            System.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }

}
//
// (c) 2015 Appirio, Inc.
//
// Test Class for - OCSUGC_GroupController
//
// February 5nd, 2015     Harshit Jain[T-317348]
@isTest(seeAllData=true)
public class OCSUGC_GroupControllerTest {

     static User communityMember,communityManager,memberUsr;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static Profile admin,portal,manager;

   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test234', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test123', '2'));
     insert lstUserInsert;
     account = OCSUGC_TestUtility.createAccount('tempacc9826', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
        lstUserInsert.add(communityManager);
        communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
        lstUserInsert.add(communityMember);
        memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member);
        lstUserInsert.add(memberUsr);
        insert lstUserInsert;
        List<Group> grpList = [Select Id from Group Where Name = 'OCSUGC Users'];
        if(grpList.size() > 0) {
          GroupMember grpMember = new GroupMember(UserOrGroupId = communityManager.Id, GroupId = grpList[0].Id);
          insert grpMember;
        }
     }
   }

    static testMethod void testGroupCreationWithManager() {
        Test.startTest();
            //Validate group creation with Community Mannager
            System.runAs(communityManager) {
                PageReference pageRef = Page.OCSUGC_CreateGroup;
                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('screenCode', '0');

                OCSUGC_GroupController controller1 = new OCSUGC_GroupController();
                controller1.cancelGroupInformation();
                controller1.CollaborationType();
                controller1.getVisibility();

                //Add group Informationn
                controller1.newGroup.name = 'Test Group 268734' + System.now();
                controller1.newGroup.Description = 'Test Description';
                controller1.newGroup.CollaborationType = 'Public';

                pageReference pageRef1 = controller1.updateGroupInformation();

                //Assert new group creation
                if(pageRef1!=null) {
                  system.assertEquals('1', pageRef1.getParameters().get('screenCode'));
                  system.assertNotEquals(null, pageRef1.getParameters().get('g'));
                }
                else {
                    pageRef1 = pageRef;
                }

                Test.setCurrentPage(pageRef1);
                OCSUGC_GroupController controller2 = new OCSUGC_GroupController();
                controller2.cancelGroupInformation();

                controller2.newGroup.name = 'Test Group 26873412';
                controller2.updateGroupInformation();

                controller2.groupPhoto.versionData = Blob.valueOf('Test Data');
                controller2.groupPhoto.Title = 'Group Photo';
                controller2.groupPhoto.pathOnClient = 'image.jpg';
                pageReference pageRef2 = controller2.uploadImage();

                system.assertNotEquals(null,pageRef2);


                pageReference pageRef3 = controller2.redirectPage();
            }

        Test.StopTest();
    }

    static testMethod void testUnlistedGroupCreationWithManager() {
        Test.startTest();
                //Validate group creation with Community Mannager
                System.runAs(communityManager) {
                PageReference pageRef = Page.OCSUGC_CreateGroup;
                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('screenCode', '0');
                ApexPages.currentPage().getParameters().put('fgab', 'true');

                OCSUGC_GroupController controller1 = new OCSUGC_GroupController();
                controller1.cancelGroupInformation();

                //Add group Informationn with term
                controller1.newGroup.name = 'Test Group 78765652' + System.now();
                controller1.newGroup.Description = 'Test Description';
                controller1.newGroupInfo.OCSUGC_Group_TermsAndConditions__c = 'Test Terms and conditation';

                pageReference pageRef1 = controller1.updateGroupInformation();

                //Assert new group creation
                if(pageRef1!=null) {
                  system.assertEquals('1', pageRef1.getParameters().get('screenCode'));
                  system.assertNotEquals(null, pageRef1.getParameters().get('g'));
                }
                else {
                    pageRef1 = pageRef;
                }

                Test.setCurrentPage(pageRef1);
                OCSUGC_GroupController controller2 = new OCSUGC_GroupController();
                controller2.cancelGroupInformation();

                controller2.newGroup.name = 'Test Group 26871231233412';
                controller2.updateGroupInformation();

                controller2.groupPhoto.versionData = Blob.valueOf('Test Data');
                controller2.groupPhoto.Title = 'Group Photo';
                controller2.groupPhoto.pathOnClient = 'image.jpg';
                pageReference pageRef2 = controller2.uploadImage();

                //Assert Image upload

                pageReference pageRef3 = controller2.redirectPage();

                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('screenCode', '0');
                ApexPages.currentPage().getParameters().put('fgab', 'true');
                OCSUGC_GroupController controller3 = new OCSUGC_GroupController();

                //Add group Informationn without term
                controller3.newGroup.name = 'Test Group 7876527234652';
                controller3.newGroup.Description = 'Test Description';

                pageReference pageRef4 = controller3.updateGroupInformation();
            }

        Test.StopTest();
    }
    
    /**
     * createdDate : 8 Dec 2016
     */
    public static testMethod void OCSUGC_GroupControllerDCtest() {
        Test.StartTest();
            Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
            CollaborationGroup CollocGroup = OCSUGC_TestUtility.createGroup('Test Group','Unlisted',ocsugcNetwork.id , true);
            
            OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
            info.OCSUGC_Group_Id__c = CollocGroup.Id;
            info.OCSUGC_Email__c = 'psardana@appirio.com';
            info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
            insert info;
            
            ApexPages.currentPage().getParameters().put('dc', 'true');
            ApexPages.currentPage().getParameters().put('g', CollocGroup.Id);
            OCSUGC_GroupController control = new OCSUGC_GroupController();
            control.redirectPage();
            control.cancelGroupInformation();
        Test.StopTest();
    }
    
    public static testMethod void updateGroupInformation() {
        Test.StartTest();
            Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
            CollaborationGroup CollocGroup = OCSUGC_TestUtility.createGroup('testingGroup_12345234','Unlisted',ocsugcNetwork.id , true);
            
            OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
            info.OCSUGC_Group_Id__c = CollocGroup.Id;
            info.OCSUGC_Email__c = 'psardana@appirio.com';
            info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
            insert info;
            
            ApexPages.currentPage().getParameters().put('dc', 'true');
            ApexPages.currentPage().getParameters().put('g', CollocGroup.Id);
            ApexPages.currentPage().getParameters().put('fgab', 'true');
            
            OCSUGC_GroupController control = new OCSUGC_GroupController();
            control.updateGroupInformation();
        Test.StopTest();
    }
    
    public static testMethod void uploadImageTest() {
        Test.StartTest();
            ContentVersion testContentInsert =new ContentVersion(); 
            testContentInsert.ContentURL='http://www.google.com/';
            testContentInsert.Title ='Google.com'; 
            //testContentInsert.versionData = Blob.valueOf('1');
            insert testContentInsert; 
            
            Network ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
            CollaborationGroup CollocGroup = OCSUGC_TestUtility.createGroup('testingGroup_12345234','Unlisted',ocsugcNetwork.id , true);
            
            OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
            info.OCSUGC_Group_Id__c = CollocGroup.Id;
            info.OCSUGC_Email__c = 'psardana@appirio.com';
            info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
            insert info;
            
            ApexPages.currentPage().getParameters().put('dc', 'false');
            ApexPages.currentPage().getParameters().put('g', CollocGroup.Id);
            ApexPages.currentPage().getParameters().put('fgab', 'true');
            
            OCSUGC_GroupController control = new OCSUGC_GroupController();
            control.newGroup = CollocGroup;
            control.groupPhoto = testContentInsert;
            control.uploadImage();
        Test.StopTest();
    }
}
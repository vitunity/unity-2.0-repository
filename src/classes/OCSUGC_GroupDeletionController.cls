//
// (c) 2014 Appirio, Inc.
//
// Controller class for OCSUGC_GroupDeletion component. It does the following
// 1. Send email to members of Public Group - Admin informing that group owner has requested deletion of group
//
// 12 Dec, 2014     Puneet Sardana       Original Ref T-340052
// Updated OCSUGC_CollaborationGroupInfo__c object's field OCSUGC_is_Request_Delete__c--Shital Bhujbal-- Ref ONCO-277   

public class OCSUGC_GroupDeletionController {
  public String reason { get; set; }
  public Boolean isError { get; set; }
  public Boolean isDC { get; set; }
  public String groupInfoId { get; set; }
  public String exMsg { get; set; }
  public String strEmailStatusMsg { get; set; }  
  private static final String OCSUGC_GROUP_DELETION_EMAIL_TEMPLATE = 'OCSUGC_Group_Deletion';   
  public OCSUGC_CollaborationGroupInfo__c newGroupInfo = New OCSUGC_CollaborationGroupInfo__c();
  
    // Constructor
  public OCSUGC_GroupDeletionController() {
    exMsg = '';
    isError = false; 
    reason ='';
    isDC = OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc'));
  } 

  // @description: This method perform actions related to group request deletion.
  // @param: 
  // @return: null 
  // 12-Dec-2014  Puneet Sardana  Original Ref T-340052
  public PageReference groupRequestDeletion(){
     CollaborationGroup groupInfo;
     
     for(CollaborationGroup grp : [SELECT Id,Owner.Name,Name
                                  FROM CollaborationGroup
                                  WHERE Id = :groupInfoId]) {
      groupInfo = grp;                    
    }       
    sendEmailOnGroupRequestDeletion(groupInfo);
    return null;
  }



  // @description: This method perform actions related to group request deletion.
  // @param: 
  // @return: null 
  // 12-Dec-2014  Puneet Sardana  Original Ref T-340052
    public void sendEmailOnGroupRequestDeletion(CollaborationGroup groupInfo)
    {
        /* Code added by Shital Bhujbal ---Start---*/
        
        for(OCSUGC_CollaborationGroupInfo__c callgrp : [Select Id,Name,OCSUGC_is_Request_Delete__c
                                                        From OCSUGC_CollaborationGroupInfo__c
                                                        Where OCSUGC_Group_Id__c =:groupInfo.Id]) {
                newGroupInfo = callgrp;
            }
        if(newGroupInfo!= null && newGroupInfo.OCSUGC_is_Request_Delete__c==false){
                newGroupInfo.OCSUGC_is_Request_Delete__c=True;
                update newGroupInfo;
        }
        //---End---
        //list will contain Email drafts which needs to be send
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        Id orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        //Query and fetch the Email Template Format for sending email to Public Group
        EmailTemplate objEmailTemplate = [SELECT Id,
                                                 Subject,
                                                 HtmlValue,
                                                 Body
                                           FROM EmailTemplate
                                           WHERE DeveloperName = :OCSUGC_GROUP_DELETION_EMAIL_TEMPLATE
                                           LIMIT 1];

        String strHtmlBody = objEmailTemplate.HtmlValue;        
        String strSubject = objEmailTemplate.Subject;
        strSubject = strSubject.replace('GROUP_NAME',groupInfo.Name);
        //replace changes in html and plain email template body
        // Start Added Label Value Naresh T-363912
        strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
        // End
        strHtmlBody = strHtmlBody.replace('GROUP_OWNER_NAME', groupInfo.Owner.Name);                             
        // replace the content type in HTMLBody
        strHtmlBody = strHtmlBody.replace('GROUP_NAME', groupInfo.Name);   
        strHtmlBody = strHtmlBody.replace('GROUP_DELETION_REASON', reason);
        //Changed  18-Mar-2015  Puneet Sardana  Added isDC Ref T-371504
        strHtmlBody = strHtmlBody.replace('GROUP_CHATTER_LINK', URL.getSalesforceBaseUrl().toExternalForm() + '/OCSUGC/ocsugc_groupdetail?g=' + groupInfo.Id+'&dc='+isDC);     
        //Query and fetch all the Group member associated with OCSUGC Admins public group
        for(GroupMember objGroupMember : [SELECT Id,
                                                 UserOrGroupId
                                          FROM GroupMember
                                          WHERE Group.DeveloperName = :OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP])
        {
            //filter only direct associated Users
            if(String.ValueOf(objGroupMember.UserOrGroupId).startsWith('005'))
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setSaveAsActivity(false);
                if(orgWideEmailAddressId != null)
                    email.setOrgWideEmailAddressId(orgWideEmailAddressId);
                else 
                  email.setSenderDisplayName('OncoPeer');
                email.setSubject(strSubject);
                email.setHtmlBody(strHtmlBody);
                email.setTargetObjectId(objGroupMember.UserOrGroupId);                
                lstEmail.add(email);
            }
        }

        try{
            List<Messaging.SendEmailResult> results =new List<Messaging.SendEmailResult>();
            System.debug('Puneet '+lstEmail);
            //Sending Email here
            if(!lstEmail.IsEmpty())
                results = Messaging.sendEmail(lstEmail);
            //iterate the result and find if email fails to sent
            for(Messaging.SendEmailResult result : results)
            {
              if(!result.IsSuccess())
                strEmailStatusMsg = Label.OCSUGC_EmailNotSuccessfullySent;
            }
        }
        catch(Exception ex){
            isError = true;
            exMsg = ex.getMessage();
        }
    }

}
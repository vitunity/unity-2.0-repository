//
// (c) 2014 Appirio, Inc.
//
// Test class for OCSUGC_GroupDeletionController component.
// 12 Jan, 2015     Ravindra Shekhawat
@isTest
public class OCSUGC_GroupDeletionControllerTest {
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2,usr1,usr2;
   static Account account;
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add(usr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'testuser3128', '1'));

     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     
     lstUserInsert.add(usr2 = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
     insert lstUserInsert;
   }

    static testMethod void test_groupDeletionController(){
        test.startTest();
	        System.runAs(usr2) {
	            Group newGroup = OCSUGC_TestUtility.createGroup(OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP,true);
	            GroupMember grouMem = OCSUGC_TestUtility.createGroupMemberNC(newGroup.Id,usr1.Id,true);
	        }
	            CollaborationGroup publicgroup = OCSUGC_TestUtility.createGroup('TestPublic', 'Public', null, true);
	            CollaborationGroupMember member1 = OCSUGC_TestUtility.createGroupMember(usr1.Id, publicgroup.Id, true);
	        
	        OCSUGC_GroupDeletionController controller = new OCSUGC_GroupDeletionController();
	        System.assert(null!=publicgroup.Id);
	        controller.groupInfoId = publicgroup.Id;
	        controller.groupRequestDeletion();
        test.stopTest();
    }
}
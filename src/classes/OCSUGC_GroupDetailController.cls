public without sharing class OCSUGC_GroupDetailController extends OCSUGC_Base_NG {

  //Member variables
  public Id                 currentGroupId {get;set;} //Id of the current group 
  public CollaborationGroup currentGroup {get;set;} //current chatter group we are working with
  public Boolean            isGroupOwner {get;set;} //flag to check if logged in user is group owner or not
  //public Boolean            isGroupManager {get;set;} //flag to check if logged in user is group manager or not
  public Boolean            isGroupMember {get;set;} //flag to check if logged in user is group member or not
  public Boolean            isInvited {get;set;} //flag to check if logged in user is invited to this group or not
  public Boolean            isInviteExpired {get;set;} //flag to check if invitation to logged in user for this group has expired or not
  public Boolean            isPendingMember {get;set;} //flag to check if logged in user pending membership into the group
  //public Integer            groupMemberCount {get;set;} //number of active users who are member in the group
  public Integer            groupPendingCount {get;set;} //number of active users who are pending membership in the group
  public Id                 requestIdToUpdate {get;set;} //Id of CollaborationGroupMemberRequest to update
  public String             requestAction {get;set;} //Accept or Deny action to take on requestIdToUpdate

  //Member variables used by FGABAboutGroup page
  public String             showPanelName {get;set;} //Name of the div panel to show
  public String             oabTermsofUse {get;set;} //Terms of Use for the OAB

  //Member variables used by OCSUGC_Confirmation component, passed via showConfirmation method
  public String contentType {get;set;}
  public String contentId {get;set;}
  public String supportContentId {get;set;}
  public String callBackMethodNameOptional {get; set;}
  public Pagereference showConfirmation() {
    return null;
  }
  

  /**
   * Wrapper Class for Group Members
   */
  public class WrapperMember {
    public Id memberId {get;set;}
    public String name {get;set;}
    public String title {get;set;}
    public String accountName {get;set;}
    public String smallPhotoUrl {get;set;}
    public User user {get;set;}
    public String type {get;set;}
    public Boolean isOwner {get;set;}
    public Boolean isPending {get;set;}
    public Id pendingRequestId {get;set;}
    public String followers {get;set;}
    public String following {get;set;}
  }
  public List<WrapperMember> lstGroupMembers {get; set;} //List of group members

  /**
   * Wrapper Class for Group Activities
   */
  public class WrapperActivity implements Comparable {
    public String feedItemId                {get;set;}
    public String className                 {get;set;}
    public String title                     {get;set;}
    public String authorId                  {get;set;}
    public String authorName                {get;set;}
    public String activityType              {get;set;}
    public Datetime createdDate             {get;set;}
    public String relativeCreatedTime       {get;set;}
    public Datetime lstModDate              {get;set;}
    public Integer countLikes               {get;set;}
    public Integer countViewed              {get;set;}
    public Integer countComments            {get;set;}
    public String detailLink                {get;set;}
    public Boolean isLikedByCurrentUser     {get;set;}
    public String activityId                {get;set;}
    public String sharedWithGroupName       {get;set;}
    public Boolean isCommentedByCurrentUser {get;set;}

    public WrapperActivity() {}

    public Integer compareTo(Object objToCompare) {
        WrapperActivity comparedToActivity = (WrapperActivity)objToCompare;

        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (createdDate > comparedToActivity.createdDate) {
            // Set return value to a negative value.
            returnValue = -1;
        } else if (createdDate < comparedToActivity.createdDate) {
            // Set return value to a positive value.
            returnValue = 1;
        }

        return returnValue;
    }
  }
  public List<WrapperActivity> lstGroupActivities {get; set;} //List of group activities

  //Private Members
  private Set<String> memberIds;
  private Set<String> pendingIds;  
  private Map<String, String> mapPendingRequestIds;
  private Datetime    currentSystemTime;

  /** 
   * Constructor
   */
    public OCSUGC_GroupDetailController() {

    //initialize member variables
    isGroupOwner       = false;
    isGroupMember      = false;
    isInvited          = false;
    isInviteExpired    = false;
    isPendingMember    = false;
    groupPendingCount  = 0;
    memberIds            = new Set<String>();
    pendingIds           = new Set<String>();
    mapPendingRequestIds = new Map<String, String>();
    lstGroupMembers      = new List<WrapperMember>();
    lstGroupActivities   = new List<WrapperActivity>();
    currentSystemTime    = Datetime.now();

    //get group Id from the URL. if that is not present nothing works
    currentGroupId = ApexPages.currentPage().getParameters().get('g');

    //if group id is not there nothing works, escape!
    if(String.isBlank(currentGroupId)) {
      return;
    }

    //populate Group Information
    getGroupInfo(currentGroupId);

    //START -- initialization of variables for FGABAboutGroup page
    showPanelName = ApexPages.currentPage().getParameters().get('showPanel');

    oabTermsofUse = '';
    for(OCSUGC_CollaborationGroupInfo__c gpInfo : [Select OCSUGC_Group_TermsAndConditions__c
                                                   From OCSUGC_CollaborationGroupInfo__c
                                                   Where OCSUGC_Group_Id__c =:currentGroupId]) {
      oabTermsofUse = gpInfo.OCSUGC_Group_TermsAndConditions__c;
    }
    //END -- initialization of variables for FGABAboutGroup page

    //set page title to specific group name
    if((currentGroup != NULL) && !isOAB)
      pageTitle = 'Group: ' + currentGroup.Name + ' | ' + pageTitle;
    if((currentGroup != NULL) && isOAB)
      pageTitle = currentGroup.Name + ' | ' + pageTitle;

        
    }

  /**
   * populate currentGroup member variable with data
   * @param groupId Id of the group to load
   */
  private void getGroupInfo(Id groupId)
  {
      if( groupId != NULL ) { 
        try {  //wrap in try carch to handle the case when no results are found       
        currentGroup = [SELECT Name, 
                         SmallPhotoUrl,
                         FullPhotoUrl,
                         Description,
                         CollaborationType,
                         OwnerId,
                         Owner.Email,
                         Owner.Name,
                         Owner.SmallPhotoUrl,
                         Owner.Title,
                         Owner.ContactId,
                         Owner.Contact.OCSUGC_Notif_Pref_ReqJoinGrp__c,
                         IsArchived,
                         MemberCount,
                         (Select MemberId, CollaborationRole From GroupMembers
                                          Where Member.IsActive = True
                                          And (Member.Contactid = null
                                              OR (Member.Contactid != null
                                              And Member.Contact.OCSUGC_UserStatus__c =: Label.OCSUGC_Approved_Status))),
                         (Select RequesterId From GroupMemberRequests
                                             Where Status='Pending')
                        FROM CollaborationGroup
                        WHERE Id =:groupId
                        AND NetworkId IN (:ocsugcNetworkId, :ocsdcNetworkId)];
        } catch(Exception e) {
          currentGroup = NULL;
        }
                        
        if( currentGroup != NULL ) {

          //groupMemberCount = currentGroup.GroupMembers.size();
          if( currentGroup.OwnerId == loggedInUser.Id ) isGroupOwner = true;

          for(CollaborationGroupMember member : currentGroup.GroupMembers) {
              memberIds.add(member.MemberId);
              if( member.MemberId == loggedInUser.Id ) {
                  isGroupMember = true;
              }
          }

          for(CollaborationGroupMemberRequest request :currentGroup.GroupMemberRequests) {
              if( request.RequesterId == loggedInUser.Id ) {
                  isPendingMember = true;
              }
              //if user it not a group owner no need to track pending members
              if(isGroupOwner) {                
                pendingIds.add(request.RequesterId);
                mapPendingRequestIds.put(request.RequesterId,request.Id);
              }
              
          }
          groupPendingCount = pendingIds.size();

          //check if user is invited to join this group or not
          if(!isGroupMember && !isPendingMember) { //pending members cannot be invited?

            try { //wrap in try catch to handle the case when no results are found
              OCSUGC_Group_Invitation__c groupInvitation = [SELECT Id,OCSUGC_Group_Invitation_Date__c, CreatedBy.Id
                                                            FROM OCSUGC_Group_Invitation__c
                                                            WHERE OCSUGC_User_Id__c = :loggedInUser.Id
                                                            AND OCSUGC_Group_Id__c = :currentGroupId 
                                                            ORDER BY OCSUGC_Group_Invitation_Date__c DESC LIMIT 1];
              if(groupInvitation != NULL) {
                isInvited = true;

                if(groupInvitation.OCSUGC_Group_Invitation_Date__c.Date().daysBetween(Date.today()) > Integer.valueOf(Label.OCSUGC_InvitationExpirationDays)) {
                  isInviteExpired = true;
                }
                else {
                  isInviteExpired = false;
                }
              }
            } catch(Exception e) {
              isInvited = false; 
            }
          }   
        }
      }

    if((isCommunityContributor || isReadOnlyUser) && isInvited) {
      canJoinGroups = true;
    }
  }

  /**
   * This method navigates user to group edit page
   * @param None
   * @return PageReference along with the group id.
   */
  public PageReference editGroupDetails() {
      if(isFGAB == null) isFGAB = false;
      PageReference editGroupPage = isFGAB ? Page.OCSUGC_EditFGAB : Page.OCSUGC_CreateGroup;
      editGroupPage.getParameters().put('g',currentGroupId);
      editGroupPage.getParameters().put('screenCode','0');
      editGroupPage.getParameters().put('fgab',''+isFGAB);
      editGroupPage.getParameters().put('dc',''+isDC);
      editGroupPage.setRedirect(true);
      return editGroupPage;

  }

  public PageReference updateGroupMemberRequest() {
    if(requestIdToUpdate != null && requestAction != null) {

      String communityId = ocsugcNetworkId;
      if (isDevCloud) communityId = ocsdcNetworkId;

      String networkName = OCSUGC_Utilities.getCurrentCommunityNetworkName(isDevCloud);

      CollaborationGroupMemberRequest memberRequest = [Select Status,
                                                              RequesterId,
                                                              Requester.Email,
                                                              CollaborationGroupId,
                                                              CollaborationGroup.Name
                                                        From CollaborationGroupMemberRequest
                                                        Where Id =:requestIdToUpdate limit 1];
      if(requestAction.equals('Approve')) {
            
        if(!Test.isRunningTest()) {
          ConnectApi.ChatterGroups.updateRequestStatus(communityId, memberRequest.Id, ConnectApi.GroupMembershipRequestStatus.Accepted);
        }
            
        sendNotificationToMember(memberRequest.Requester.Email,'Approved');
            
        OCSUGC_Intranet_Notification__c tempObj;
        if(isFGAB) {
          tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(networkName,memberRequest.RequesterId,
                                                                        memberRequest.CollaborationGroup.Name,
                                                                        'added you to the',
                                                                        'ocsugc_fgababoutgroup?g='+currentGroupId+'&fgab=true&showPanel=About');
          insert tempObj; 
        }
                   
      }

      else if(requestAction.equals('Deny')) {

        if(!Test.isRunningTest()) {
          ConnectApi.ChatterGroups.updateRequestStatus(communityId, memberRequest.Id, ConnectApi.GroupMembershipRequestStatus.Declined);
        }
        
        sendNotificationToMember(memberRequest.Requester.Email,'Denied');
            
        //inserting intranet notificaiton record 
        OCSUGC_Intranet_Notification__c tempObj;
        if(isFGAB) {
          tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(networkName,memberRequest.RequesterId,
                                                                        memberRequest.CollaborationGroup.Name,
                                                                        'has declined your request to join',
                                                                        'ocsugc_fgababoutgroup?g='+currentGroupId+'&fgab=true&showPanel=About');

        } 
        else {
          tempObj = OCSUGC_Utilities.prepareIntranetNotificationRecords(networkName,memberRequest.RequesterId,
                                                                        memberRequest.CollaborationGroup.Name,
                                                                        'has declined your request to join',
                                                                        'OCSUGC_GroupDetail?g='+currentGroupId);
        }
        insert tempObj;
      }
    }

    memberIds.clear();
    pendingIds.clear();
    mapPendingRequestIds.clear();
    getGroupInfo(currentGroupId);
    return null;
  }

  /**
   * This method sends email notification to the member about status of your request to join the current group
   * @return void
   */
   
  public void sendNotificationToMember(String toEmail, String status) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        List<String> toAddress = new List<String>();
        toAddress.add(toEmail);        
        email.setToAddresses(toAddress);
        email.setReplyTo(currentGroup.Owner.Email);

        email.setSubject(status+' request to join a group');
        email.setPlainTextBody('Your request to join '+currentGroup.Name +' has been '+status.toLowerCase()+'.');

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
  }

  /**
   * This method adds logged in user to current group and reloads the page
   * @return PageReference set to null to reload the page
   */
  public PageReference joinPublicGroup() {
    insert new CollaborationGroupMember(memberId = userInfo.getUserId(),  CollaborationGroupId = currentGroupId);
   
    return null; //null will reload the page
  }

  /**
   * This method adds logged in user to pending list of current group and reloads the page
   * @return PageReference set to null to reload the page
   */
  public PageReference joinPrivateGroup() {
    insert new CollaborationGroupMemberRequest(RequesterId = userInfo.getUserId(),  CollaborationGroupId = currentGroupId);
    sendNotificationToGroupOwner(); //send email notification to the Group Owner of a private Group
    return null;
  }

  /**
   * This method adds logged in user to current group and reloads the page
   * @return PageReference set to null to reload the page
   */
  public PageReference joinOABGroup() {
    insert new CollaborationGroupMember(memberId = userInfo.getUserId(),  CollaborationGroupId = currentGroupId);

    //Add Log about terms of use acceptance
    OCSUGC_TermsConditions_UserLog__c tcUserLog     = new OCSUGC_TermsConditions_UserLog__c();
    tcUserLog.OCSUGC_Contact_Id__c                  = loggedInUser.ContactId;
    tcUserLog.OCSUGC_Group_ID__c                    = currentGroupId;
    tcUserLog.OCSUGC_Terms_Conditions__c            = oabTermsofUse;
    tcUserLog.OCSUGC_Date_of_acceptance_TC__c       = System.now();
    tcUserLog.OCSUGC_Group_Name__c                  = currentGroup.Name;
    try{
        if(tcUserLog!=null)
            insert tcUserLog;
    }
    catch(Exception ex){
        system.debug('========>>>'+ex.getMessage());
    }
   
    return null; //null will reload the page
  }

  /**
   * This method removes logged in user from current group and reloads the page
   * @return PageReference set to null to reload the page
   */
  public Pagereference leaveGroupPage() {
    for(CollaborationGroupMember removeMember :[SELECT Id
                                                 FROM   CollaborationGroupMember
                                                 WHERE  memberId =:userInfo.getUserId()
                                                 AND    CollaborationGroupId =:currentGroupId
                                                 limit 1]) {
        try {
            if(removeMember != null) {
              delete removeMember;
            }
        }
        catch(Exception ex) {
            system.debug('========>>>'+ex.getMessage());
        }
        return null;
    }
    return null;
  }

  /**
   * This method sends email notification to the group owner about a logged in user requesting to join their group
   * @return void
   */
  private void sendNotificationToGroupOwner(){ 

      Boolean bSendNotification = (currentGroup.Owner.Contact.OCSUGC_Notif_Pref_ReqJoinGrp__c == true);

      //List To store Group Owner?s Contact email address
      List<String> lstEmailAddresses = new List<String>();

      //list will contain Email drafts which needs to be send
      List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

      //Query and fetch the Email Template Format for sending email to Group Owner.
      EmailTemplate objEmailTemplate = [SELECT Id,
                                           Subject,
                                           HtmlValue,
                                           Body
                                        FROM EmailTemplate
                                        WHERE DeveloperName = 'OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER'
                                        LIMIT 1];
      System.debug('objEmailTemplate==='+objEmailTemplate);
      String strHtmlBody  = objEmailTemplate.HtmlValue;
      String strSubject   = objEmailTemplate.Subject;
      String strPlainBody = objEmailTemplate.Body;

      //Prep html and plain text body of the email.
      if(bSendNotification){         
          //replace the logged in user's FirstName, LastName, Group Name and The Group Link dynamically in HTMLBody
          if(isDc)
              strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSDC_Email_Header_Image_Path);
          else
              strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
          strHtmlBody = strHtmlBody.replace('GROUP_OWNER_NAME', currentGroup.Owner.Name);
          strHtmlBody = strHtmlBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
          strHtmlBody = strHtmlBody.replace('USER_LAST_NAME', Userinfo.getLastName());
          strHtmlBody = strHtmlBody.replace('GROUP_NAME', currentGroup.Name);

          strHtmlBody = strHtmlBody.replace('URL_LINK', URL.getCurrentRequestUrl().toExternalForm() + '/OCSUGC_GroupDetail?isFGAB=false&g=' + currentGroupId +'&showPanel=Members&dc='+isDC);
     

          //replace the logged in user's FirstName, LastName, Group Name and The Group Link dynamically in PlainBody
          strPlainBody = strPlainBody.replace('GROUP_OWNER_NAME', currentGroup.Owner.Name);
          strPlainBody = strPlainBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
          strPlainBody = strPlainBody.replace('USER_LAST_NAME', Userinfo.getLastName());
          strPlainBody = strPlainBody.replace('GROUP_NAME', currentGroup.Name);
          
          strPlainBody = strPlainBody.replace('URL_LINK', URL.getCurrentRequestUrl().toExternalForm() + '/OCSUGC_GroupDetail?isFGAB=false&g=' + currentGroupId + '&showPanel=Members');
         
          strSubject  = strSubject.replace('GROUP_NAME', currentGroup.Name);
      }
    
      
      if(bSendNotification) {

          lstEmailAddresses.add(currentGroup.Owner.Email);       

          //set email to the Group owner
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          email.setSaveAsActivity(false);
          
          // Use Organization Wide Address
          Id orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
          if(orgWideEmailAddressId != null)
              email.setOrgWideEmailAddressId(orgWideEmailAddressId);
          else
            email.setSenderDisplayName('OncoPeer');

          email.setSubject(strSubject);          
          email.setHtmlBody(strHtmlBody);
          email.setToAddresses(lstEmailAddresses );
          lstEmail.add(email);
      }
      
      try{
          //Sending Email here
          if(!lstEmail.IsEmpty())
              Messaging.sendEmail(lstEmail);
      }
      catch(Exception ex){
          system.debug('========>>> sendNotificationToGroupOwner: ===>'+ex.getMessage());
      }

  }

  /**
   * Populate list of group Members
   * @return void
   */
  public void populateGroupMembers() {
      lstGroupMembers.clear();

      //generate map of user and its info
      for(User  user : [Select Id, 
                               Name,
                               Contact.Title,
                               Contact.Account.Legal_Name__c,
                               Country,
                               SmallPhotoUrl
                        From User
                        Where Id IN :memberIds Or Id IN :pendingIds
                        order by Name]){
          
          WrapperMember member = new WrapperMember();
          member.memberId = user.Id;
          member.name = user.Name;
          member.title = user.Contact.Title;
          member.accountName = user.Contact.Account.Legal_Name__c;
          member.smallPhotoUrl = user.SmallPhotoUrl;
          member.user = user;
          member.isOwner = (user.Id == currentGroup.OwnerId ? true : false);
          member.followers = '0';
          member.following = '0';

          if(pendingIds.contains(member.memberId)) {
            member.type = 'pending';
            member.isPending = true;
            member.pendingRequestId = mapPendingRequestIds.get(member.memberId);
          } else {
            member.type = 'member';
            member.isPending = false;
          }

          lstGroupMembers.add(member);
      }
  }

  /**
   * Populate group activities
   * @return void
   */
  public void populateGroupActivities() {

    //clear the existing list
    lstGroupActivities.clear();

    //get current system time, it will be used to calculate relative time of the activity
    currentSystemTime = Datetime.now();

    //fetch each type of activity
    lstGroupActivities.addAll(fetchDiscussionActivity(currentGroupId));
    lstGroupActivities.addAll(fetchPollActivity(currentGroupId));
    lstGroupActivities.addAll(fetchKnowledgeArtifactsActivity(currentGroupId));
    lstGroupActivities.addAll(fetchEventActivity(currentGroupId));

    //sort the list to have newer activity at the top
    lstGroupActivities.sort();

  }

  /**
   * fetch all the discussions for the given group id
   * @param  groupId ID of the group for which we need discussions
   * @return         list of WrapperActivity representing all the discussions
   */
  private List<WrapperActivity> fetchDiscussionActivity(String groupId) {
    List<WrapperActivity> lstDiscussionDetails = new List<WrapperActivity>();
    Map<Id,WrapperActivity> mapDiscussion      = new Map<Id,WrapperActivity>();
    String query = 'SELECT OCSUGC_Views__c,OCSUGC_DiscussionId__c,name,LastModifiedDate,OCSUGC_Description__c, CreatedById, CreatedBy.Name, CreatedDate ' +
                   'FROM OCSUGC_DiscussionDetail__c ' +
                   'WHERE OCSUGC_Is_Deleted__c = false AND OCSUGC_groupid__c =:groupId ' +
                   'ORDER BY CreatedDate DESC';

    

    for (OCSUGC_DiscussionDetail__c discussion : Database.query(query)) {
      WrapperActivity wgaDtls = new WrapperActivity();
      wgaDtls.title               = truncateDiscussionBody(discussion.OCSUGC_Description__c);
      wgaDtls.authorId            = discussion.CreatedById;
      wgaDtls.authorName          = discussion.CreatedBy.Name;
      wgaDtls.createdDate         = discussion.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(discussion.CreatedDate,currentSystemTime);
      wgaDtls.countViewed         = (Integer)discussion.OCSUGC_Views__c;
      wgaDtls.lstModDate          = discussion.LastModifiedDate;
      wgaDtls.activityType        = 'DC';
      wgaDtls.className           = 'shared discussion';
      wgaDtls.activityId          = discussion.id;
      wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_DiscussionDetail?Id='+discussion.OCSUGC_DiscussionId__c+(isDC?'&dc=true':'');
      mapDiscussion.put(discussion.OCSUGC_DiscussionId__c,wgaDtls);
      wgaDtls = null;
    }
    
    for(FeedItem fdItem : [select LastModifiedDate,Body, NetworkScope,LikeCount, CommentCount,
                          (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes)
                           From FeedItem Where id IN : mapDiscussion.keyset() order by LastModifiedDate desc]) {
      WrapperActivity wgaDtls                       = new WrapperActivity();
      wgaDtls                       = mapDiscussion.get(fdItem.id);

      wgaDtls.feedItemId            = fdItem.id;
      wgaDtls.countLikes            = fdItem.LikeCount;
      wgaDtls.countComments         = fdItem.CommentCount;
      wgaDtls.isLikedByCurrentUser  = false;
      
      for(FeedLike forflike: fdItem.FeedLikes){
        if(forflike.CreatedById == Userinfo.getUserId() ){
            wgaDtls.isLikedByCurrentUser = true;
            break;
        }
        else{
            wgaDtls.isLikedByCurrentUser = false;
        }
      }

      lstDiscussionDetails.add(wgaDtls);
    }
    
    return lstDiscussionDetails;
  }

  /**
   * fetch all the polls for the given group id
   * @param  groupId ID of the group for which we need polls
   * @return         list of WrapperActivity representing all the polls
   */
  private List<WrapperActivity> fetchPollActivity(String groupId) {

    List<WrapperActivity> lstPollDetails = new List<WrapperActivity>();
    Map<Id,WrapperActivity> mapPoll      = new Map<Id,WrapperActivity>();

    String query = 'SELECT OCSUGC_Views__c,OCSUGC_Poll_Id__c,OCSUGC_Poll_Question__c, LastModifiedDate,OCSUGC_Group_Name__c,OCSUGC_Group_Id__c, OCSUGC_Likes__c, OCSUGC_Comments__c, CreatedById, CreatedBy.Name, CreatedDate ' +
                   'FROM OCSUGC_Poll_Details__c ' +
                   'WHERE OCSUGC_Is_Deleted__c = false AND OCSUGC_Group_Id__c =:groupId ' +
                   'ORDER BY CreatedDate DESC';
    for (OCSUGC_Poll_Details__c poll : Database.query(query)) {
      WrapperActivity wgaDtls     = new WrapperActivity();
      wgaDtls.title               = truncateDiscussionBody(poll.OCSUGC_Poll_Question__c);
      wgaDtls.authorId            = poll.CreatedById;
      wgaDtls.authorName          = poll.CreatedBy.Name;
      wgaDtls.createdDate         = poll.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(poll.CreatedDate,currentSystemTime);
      wgaDtls.countViewed         = (Integer)poll.OCSUGC_Views__c;
      wgaDtls.lstModDate          = poll.LastModifiedDate;
      wgaDtls.activityType        = 'PL';
      wgaDtls.className           = 'shared poll';
      wgaDtls.activityId          = poll.id;
      wgaDtls.sharedWithGroupName = poll.OCSUGC_Group_Name__c;
      wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_FGABPollDetail?Id='+poll.OCSUGC_Poll_Id__c+(isDC?'&dc=true':'');
      mapPoll.put(poll.OCSUGC_Poll_Id__c,wgaDtls);
      wgaDtls = null;

    }

    for(FeedItem fdItem : [select LastModifiedDate,Body, NetworkScope,LikeCount, CommentCount,
                          (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes),
                          (Select CreatedById From FeedComments)
                          From FeedItem Where id IN : mapPoll.keyset() order by LastModifiedDate desc]) {
      WrapperActivity wgaDtls       = new WrapperActivity();
      wgaDtls                       = mapPoll.get(fdItem.id);

      wgaDtls.feedItemId            = fdItem.id;
      wgaDtls.countLikes            = fdItem.LikeCount;
      wgaDtls.countComments         = fdItem.CommentCount;
      wgaDtls.isLikedByCurrentUser  = false;
      wgaDtls.isCommentedByCurrentUser = false;
      for(FeedLike forflike: fdItem.FeedLikes){
        if(forflike.CreatedById == Userinfo.getUserId() ){
            wgaDtls.isLikedByCurrentUser = true;
            break;
        }
      }
      for(FeedComment forfComment: fdItem.FeedComments){
            if(forfComment.CreatedById == Userinfo.getUserId() ){
                wgaDtls.isCommentedByCurrentUser = true;
                break;
            }
      }

      lstPollDetails.add(wgaDtls);
    }
    return lstPollDetails;
  }

  /**
   * fetch all the events for the given group id
   * @param  groupId ID of the group for which we need events
   * @return         list of WrapperActivity representing all the events
   */
  private List<WrapperActivity> fetchEventActivity(String groupId) {
    List<WrapperActivity> lstEventDetails = new List<WrapperActivity>();
    Map<Id,WrapperActivity> mapEvent      = new Map<Id,WrapperActivity>();
    String query = 'SELECT OCSUGC_Title__c,LastModifiedDate,OCSUGC_Views__c, CreatedById, CreatedBy.Name, CreatedDate ' + 
                   'FROM OCSUGC_Intranet_Content__c ' +
                   'WHERE OCSUGC_Is_Deleted__c = false AND OCSUGC_groupid__C =: groupId ' +
                   'ORDER BY CreatedDate DESC';
    for (OCSUGC_Intranet_Content__c evnt : Database.query(query)) {
      WrapperActivity wgaDtls     = new WrapperActivity();
      wgaDtls.title               = evnt.OCSUGC_Title__c;
      wgaDtls.authorId            = evnt.CreatedById;
      wgaDtls.authorName          = evnt.CreatedBy.Name;
      wgaDtls.createdDate         = evnt.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(evnt.CreatedDate,currentSystemTime);
      wgaDtls.countViewed         = (Integer)evnt.OCSUGC_Views__c;
      wgaDtls.lstModDate          = evnt.LastModifiedDate;
      wgaDtls.activityType        = 'ET';
      wgaDtls.className           = 'shared event';
      wgaDtls.activityId          = evnt.id;
      wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_EventDetail?eventId='+evnt.id+(isDC?'&dc=true':'');
      mapEvent.put(evnt.id,wgaDtls);
      wgaDtls = null;
    }
    for(FeedItem fdItem : [Select id,parentid, LikeCount, CommentCount, Body, NetworkScope, LastModifiedDate,
                          (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes)
                           From FeedItem Where ParentId IN : mapEvent.keyset() order by LastModifiedDate desc]) {
      WrapperActivity wgaDtls         = new WrapperActivity();
      wgaDtls                         = mapEvent.get(fdItem.ParentId);

      wgaDtls.feedItemId              = fdItem.id;
      wgaDtls.countLikes              = fdItem.LikeCount;
      wgaDtls.countComments           = fdItem.CommentCount;
      wgaDtls.isLikedByCurrentUser    = false;
      for(FeedLike forflike: fdItem.FeedLikes){
          if(forflike.CreatedById == Userinfo.getUserId() ){
              wgaDtls.isLikedByCurrentUser = true;
              break;
          }
          else{
              wgaDtls.isLikedByCurrentUser = false;
          }
        }
        lstEventDetails.add(wgaDtls);
    }
    
    return lstEventDetails;
  }

  /**
   * fetch all the knowledge files for the given group id
   * @param  groupId ID of the group for which we need knowledge files
   * @return         list of WrapperActivity representing all the knowledge files
   */
  private List<WrapperActivity> fetchKnowledgeArtifactsActivity(String groupId) {
    List<WrapperActivity> lstKADetails = new List<WrapperActivity>();
    Map<Id,WrapperActivity> mapKnwlgArt = new Map<Id,WrapperActivity>();
    String query = 'SELECT OCSUGC_Title__c, LastModifiedDate,OCSUGC_views__c, CreatedById, CreatedBy.Name, CreatedDate ' +
                   'FROM OCSUGC_Knowledge_Exchange__c ' +
                   'WHERE OCSUGC_Is_Deleted__c = false And OCSUGC_Status__c=\'Approved\' AND OCSUGC_group_id__C =: groupId ' +
                   'ORDER BY CreatedDate DESC';
    
    for (OCSUGC_Knowledge_Exchange__c knwlgArt : Database.query(query)){
      WrapperActivity wgaDtls     = new WrapperActivity();
      wgaDtls.title               = knwlgArt.OCSUGC_Title__c;
      wgaDtls.authorId            = knwlgArt.CreatedById;
      wgaDtls.authorName          = knwlgArt.CreatedBy.Name;
      wgaDtls.createdDate         = knwlgArt.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(knwlgArt.CreatedDate,currentSystemTime);
      wgaDtls.lstModDate          = knwlgArt.LastModifiedDate;
      wgaDtls.countViewed         = (Integer)knwlgArt.OCSUGC_Views__c;
      wgaDtls.lstModDate          = knwlgArt.LastModifiedDate;
      wgaDtls.activityType        = 'KA';
      wgaDtls.className           = 'shared knowledgefeed';
      wgaDtls.activityId          = knwlgArt.Id;
      wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId='+knwlgArt.id+(isDC?'&dc=true':'');
      mapKnwlgArt.put(knwlgArt.id,wgaDtls);
      wgaDtls = null;
    }

    System.debug('mapKnwlgArt============'+mapKnwlgArt);
    for(FeedItem fdItem : [Select id,parentid, LikeCount, CommentCount, Body, NetworkScope, LastModifiedDate,
                          (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes)
                           From FeedItem Where ParentId IN : mapKnwlgArt.keyset() order by LastModifiedDate desc]) {
      WrapperActivity wgaDtls         = new WrapperActivity();
      wgaDtls                         = mapKnwlgArt.get(fdItem.ParentId);

      wgaDtls.feedItemId              = fdItem.id;
      wgaDtls.countLikes              = fdItem.LikeCount;
      wgaDtls.countComments           = fdItem.CommentCount;
      wgaDtls.isLikedByCurrentUser    = false;
      for(FeedLike forflike: fdItem.FeedLikes){
          if(forflike.CreatedById == Userinfo.getUserId() ){
              wgaDtls.isLikedByCurrentUser = true;
              break;
          }
          else{
              wgaDtls.isLikedByCurrentUser = false;
          }
        }

     lstKADetails.add(wgaDtls);
     
    }
    
    return lstKADetails;
  }

  // @description: This method truncate discussion description to 150 characters.
        // @param: Discussion.Description
        // @return: String of not more than 150 characters
        // 07-11-2014 Naresh T-331182
        private String truncateDiscussionBody(String body) {
            if(body != null && body.length() > 250) {
                body = body.substring(0,250)+'...';
            }
            return body;
        }

  /**
   * check if user has selected valid group and they have access to it or not
   * If they do not have valid accessible group, redirect to different page.
   * This is called from action attribute of apex:page for Group Detail Page
   * @return null if valid page. otherwise the destination page
   */
  public Pagereference isValidAccessibleGroup() {

    Boolean bRedirectToHome = false;       

      //No group specified, go back to home page
      if(currentGroupId == NULL) {
        bRedirectToHome = true;
      }

      //No Group found, go back to home page
      else if( currentGroup == NULL) {
        bRedirectToHome = true;
      }

      //This is archived group, go back to home page
      else if( (currentGroup != NULL) && currentGroup.IsArchived) {
        bRedirectToHome = true;
      }

      //This is a hidden group, meaning it is Online Advisory Board; We cannot show it here. go back to home page
      else if( (currentGroup != NULL) && (currentGroup.CollaborationType == OCSUGC_Constants.UNLISTED)) {
        bRedirectToHome = true;        
      }


      //cannnot access page, redirect
      if( bRedirectToHome ) {
        PageReference pg = isDevCloud? Page.OCSDC_Home : Page.OCSUGC_Home;
        if( isDevCloud ) {
          pg.getParameters().put('dc','true');
        }
        pg.setRedirect(true);
        return pg;
      }

   
      //All is good. stay on this page
      return null;
  }

  /**
   * check if user has selected valid OAB and they have access to it or not
   * If they do not have valid accessible OAB, redirect to different page.
   * This is called from action attribute of apex:page for OAB About Page
   * @return null if valid page. otherwise the destination page
   */
  public Pagereference isValidAccessibleOAB() {

    Boolean bRedirectToHome = false;       
	  if(currentGroupId == NULL) {
        bRedirectToHome = true;
      }

      //No Group found, go back to home page
      else if( currentGroup == NULL) {
        bRedirectToHome = true;
      }

      //This is archived group, go back to home page
      else if( (currentGroup != NULL) && currentGroup.IsArchived) {
        bRedirectToHome = true;
      }

      //This is a regular group; We cannot show it here. go back to home page
      else if( (currentGroup != NULL) && (currentGroup.CollaborationType != OCSUGC_Constants.UNLISTED)) {
        bRedirectToHome = true;        
      }
	   
	   
      //This use does not have access to the group. go back to home page
      else if(!isOABMember && !isCommunityManager && !isReadOnlyUser && !isInvited) {
        bRedirectToHome = true;
      }


      //cannnot access page, redirect
      if( bRedirectToHome ) {
        PageReference pg = isDevCloud? Page.OCSDC_Home : Page.OCSUGC_Home;
        if( isDevCloud ) {
          pg.getParameters().put('dc','true');
        }
        pg.setRedirect(true);
        return pg;
      }

   
      //All is good. stay on this page
      return null;
  }

}

/** things to watch out for based on code in original controller -- OCSUGC_GroupDetailController
* update invite user to add user as Member
* usage of isTermsUpdate and url param termsUpdate
* usage of status url parameter
* preventGroupInvitation
* archived groups
*/
// 22-May-2015  Puneet Sardana  Test Class for Group Detail Ref T-392483
@isTest
private class OCSUGC_GroupDetailControllerTest {
   static Profile admin,portal,manager;   
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5,usr6;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5,contact6;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static CollaborationGroupMemberRequest memberRequest1,memberRequest2;
   static Network ocsugcNetwork, ocsdcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert; 
   static List<CollaborationGroupMemberRequest> lstGroupMemberRequest;
   static List<CollaborationGroup> lstGroupInsert;
   static List<EntitySubscription> lstEntityInsert;
   static OCSUGC_Knowledge_Exchange__c ka1,ka2,ka3,ka4;
   static List<OCSUGC_Knowledge_Exchange__c> lstKAInsert;    
   static List<OCSUGC_Users_Visibility__c> lstUserVisibility;
   static FeedItem fItemEvent,fItemDiscussion,fItemKA,fItemPoll;
   static List<OCSUGC_CollaborationGroupInfo__c> lstGroupInfoInsert;
   static OCSUGC_CollaborationGroupInfo__c groupInfo1,groupInfo2;   
   static OCSUGC_Intranet_Content__c event;
   static OCSUGC_DiscussionDetail__c discussion;
   static String ocsugcNetworkString, ocsdcNetworkString;
   static OCSUGC_LikeUnlike__c managerLikeUnlike;
   static FeedLike feedLikeKA,feedLikeEvent,feedLikeDiscussion,feedLikePoll;
   static FeedComment fCommentEvent,fCommentDiscussion,fCommentKA;
   static  OCSUGC_Poll_Details__c pollDetail;
   static OCSUGC_Group_Invitation__c groupInvitation;
   static OCSUGC_PreventFGABInvitation__c preventInvi;
   static {  	
     admin          = OCSUGC_TestUtility.getAdminProfile();
     portal         = OCSUGC_TestUtility.getPortalProfile(); 
     manager        = OCSUGC_TestUtility.getManagerProfile();
     managerLikeUnlike = OCSUGC_TestUtility.createLikeUnlikeCustomSetting(Label.OCSUGC_Varian_Employee_Community_Manager,true,true,true);
     insert managerLikeUnlike;
     preventInvi = OCSUGC_TestUtility.createPreventFGABInvitation(Label.OCSUGC_Varian_Employee_Community_Manager,true);
     insert preventInvi;
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     ocsugcNetworkString  = OCSUGC_TestUtility.getCurrentCommunityNetworkId(false);
     ocsdcNetworkString   = OCSUGC_TestUtility.getCurrentCommunityNetworkId(true); 
     
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact6 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     
     lstUserInsert  = new List<User>();   
     lstUserInsert.add(adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
     lstUserInsert.add(adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
     lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, 'test', Label.OCSUGC_Varian_Employee_Community_Manager));
     lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
     lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
     lstUserInsert.add(usr6 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact6.Id, '73',Label.OCSUGC_Varian_Employee_Community_Manager));
     insert lstUserInsert;
     
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Unlisted',ocsugcNetwork.id,false));
     insert lstGroupInsert;
     
     List<OCSUGC_Knowledge_Exchange__c> lstKAInsert1 = new List<OCSUGC_Knowledge_Exchange__c>();
     lstKAInsert1.add(ka3 = OCSUGC_TestUtility.createKnwExchange(privateGroup.Id,privateGroup.Name,false));
     lstKAInsert1.add(ka4 = OCSUGC_TestUtility.createKnwExchange(privateGroup.Id,privateGroup.Name,false));
     insert lstKAInsert1;
     
     event = OCSUGC_TestUtility.createIntranetContent('Events','Applications',null,'Team','Test');
     event.OCSUGC_groupid__c = privateGroup.Id;
     insert event;

     lstGroupInfoInsert = new List<OCSUGC_CollaborationGroupInfo__c>();
     lstGroupInfoInsert.add( groupInfo1 = OCSUGC_TestUtility.createCollaborationGroupInfo(publicGroup.Id) );
     lstGroupInfoInsert.add( groupInfo2 = OCSUGC_TestUtility.createCollaborationGroupInfo(privateGroup.Id));
     insert lstGroupInfoInsert;
     //===========================================================================================
     String subject,body,html;
     subject='You received a message!';
     body='MESSAGE_BODY';
     html='MESSAGE_BODY';

     System.runAs(adminUsr2) {      
		Blacklisted_Word__c blackListWord =  OCSUGC_TestUtility.createBlackListWords('Fuck');
	    Group newGroup = OCSUGC_TestUtility.createGroup('testGroup',true);
	    GroupMember grouMem = OCSUGC_TestUtility.createGroupMemberNC(newGroup.Id,adminUsr2.Id,true);
	    lstGroupMemberRequest = new List<CollaborationGroupMemberRequest>();
	    lstGroupMemberRequest.add (memberRequest1 = OCSUGC_TestUtility.createGroupMemberRequest(adminUsr2.Id,privateGroup.Id,false));
	    insert lstGroupMemberRequest;
     }
     System.runAs(adminUsr1) {
        lstGroupMemberInsert = new List<CollaborationGroupMember>();
        lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, privateGroup.Id, false));
        insert lstGroupMemberInsert;

        groupInvitation = OCSUGC_TestUtility.createGroupInvitation(usr5.Id,privateGroup.Id);
        insert groupInvitation;
     }
     System.runAs(managerUsr) {
     	     List<FeedItem> feedItems = new List<FeedItem>();
		     feedItems.add( fItemEvent = OCSUGC_TestUtility.createFeedItem(event.Id,false));
		     feedItems.add( fItemKA = OCSUGC_TestUtility.createFeedItem(ka3.Id,false));
		     feedItems.add( fItemDiscussion = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false));
		     feedItems.add( fItemPoll = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),false));
		     insert feedItems;
		     discussion = OCSUGC_TestUtility.createDiscussion(privateGroup.Id, fItemDiscussion.Id, true);
		     pollDetail = OCSUGC_TestUtility.createPollDetail(privateGroup.Id, 'TestFGABGrp', fItemPoll.Id, 'Poll Question', true);
		     List<FeedComment> feedComments = new List<FeedComment>();
		     feedComments.add(fCommentEvent = OCSUGC_TestUtility.createFeedComment(fItemEvent.Id, false));
		     feedComments.add(fCommentDiscussion = OCSUGC_TestUtility.createFeedComment(fItemDiscussion.Id, false));
		     feedComments.add(fCommentKA = OCSUGC_TestUtility.createFeedComment(fItemKA.Id, false));
		     insert feedComments;
		     List<FeedLike> lstFeedLikes = new List<FeedLike>();
		     lstFeedLikes.add(feedLikeKA = OCSUGC_TestUtility.createFeedLike(fItemKA.Id, managerUsr.Id, false));
		     lstFeedLikes.add(feedLikeEvent = OCSUGC_TestUtility.createFeedLike(fItemEvent.Id, managerUsr.Id, false));
		     lstFeedLikes.add(feedLikeDiscussion = OCSUGC_TestUtility.createFeedLike(fItemDiscussion.Id, managerUsr.Id, false));
		     lstFeedLikes.add(feedLikePoll = OCSUGC_TestUtility.createFeedLike(fItemPoll.Id, managerUsr.Id, false));
		     insert lstFeedLikes;
     }       
  }
  public static testmethod void test_GroupDetailsInvite() {  
  	Test.startTest();
  	 System.runAs(usr5) {  	 	
  	   PageReference pageRef = Page.OCSUGC_GroupDetail;
       pageRef.getParameters().put('g',privateGroup.Id);
       pageRef.getParameters().put('fgab','false');
       pageRef.getParameters().put('dc','false');
       pageRef.getParameters().put('showPanel','Invite');
       pageRef.getParameters().put('invitedUserId',usr5.Id);         
       Test.setCurrentPage(pageRef);
       OCSUGC_GroupDetailController groupDetails = new OCSUGC_GroupDetailController();
       groupDetails.isValidAccessibleGroup();         
        
  	 }
  	 Test.stopTest();    
  }   
  	 ////////////////////////////////////////////////////////////////////////////////////
  	 public static testmethod void test_GroupDetailsJoinPrivateGroup() {  
  	 Test.startTest();
  	   System.runAs(usr6) {  	 	
  	   PageReference pageRef = Page.OCSUGC_GroupDetail;
       pageRef.getParameters().put('g',privateGroup.Id);
       pageRef.getParameters().put('fgab','false');
       pageRef.getParameters().put('dc','false');
       pageRef.getParameters().put('showPanel','Invite');
       pageRef.getParameters().put('invitedUserId',usr6.Id);         
       Test.setCurrentPage(pageRef);
       OCSUGC_GroupDetailController groupDetails = new OCSUGC_GroupDetailController();    
       groupDetails.joinPrivateGroup();   
    } 	 

  	Test.stopTest();    
  }
  
  public static testmethod void test_GroupDetails() {  
  	Test.startTest();
  	 System.runAs(managerUsr) {
  	   PageReference pageRef = Page.OCSUGC_GroupDetail;
       pageRef.getParameters().put('g',privateGroup.Id);
       pageRef.getParameters().put('fgab','false');
       pageRef.getParameters().put('dc','false');
       pageRef.getParameters().put('status','Pending');
       pageRef.getParameters().put('showPanel','About');        
       Test.setCurrentPage(pageRef);
       OCSUGC_GroupDetailController groupDetails = new OCSUGC_GroupDetailController();
       groupDetails.editGroupDetails();
       groupDetails.showConfirmation(); 
       
       groupDetails.requestIdToUpdate = memberRequest1.Id;
       groupDetails.populateGroupMembers();
       groupDetails.populateGroupActivities();
       groupDetails.LeaveGroupPage();
  	 } 
  	Test.stopTest();    
  }
  
    public static testmethod void test_GroupDetailsFGAB() {  
  	Test.startTest();
  	 System.runAs(managerUsr) {
  	   PageReference pageRef = Page.OCSUGC_GroupDetail;
       pageRef.getParameters().put('g',fgabGroup.Id);
       pageRef.getParameters().put('fgab','true');
       pageRef.getParameters().put('dc','false');      
       Test.setCurrentPage(pageRef);
       OCSUGC_GroupDetailController groupDetails = new OCSUGC_GroupDetailController();
       groupDetails.isFGAB = true;
       groupDetails.editGroupDetails();
       groupDetails.isFGAB = true;
        
  	 } 
  	 Test.stopTest();    
  }
  	 public static testmethod void test_GroupDetailsJoinPublicGroup() {  
  	   Test.startTest();
  	   System.runAs(usr5) {  	
  	   PageReference pageRef = Page.OCSUGC_GroupDetail;
       pageRef.getParameters().put('g',publicGroup.Id);
       pageRef.getParameters().put('fgab','false');
       pageRef.getParameters().put('dc','false');
       pageRef.getParameters().put('showPanel','Invite');
       pageRef.getParameters().put('invitedUserId',usr5.Id);         
       Test.setCurrentPage(pageRef);
       OCSUGC_GroupDetailController groupDetails = new OCSUGC_GroupDetailController();
       groupDetails.isFGAB = true;
       groupDetails.joinPublicGroup();
  	 } 
  	Test.stopTest();    
  }
  
	/**
	 *	CreatedDate: 8 Dec 2016, Puneet Mishra
	 */
	public static testMethod void isValidAccessibleOAB_Test() {
		Test.StartTest();
			ApexPages.currentPage().getParameters().put('dc', 'true');
			ApexPages.currentPage().getParameters().put('fgab', 'true');
			ApexPages.currentPage().getParameters().put('g', publicGroup.Id);
			OCSUGC_GroupDetailController cont = new OCSUGC_GroupDetailController();
			cont.currentGroupId = null;
			cont.isValidAccessibleOAB();
			cont.isValidAccessibleGroup();
			
			cont.currentGroup = null;
			cont.currentGroupId = publicGroup.Id;
			cont.isValidAccessibleOAB();
			cont.isValidAccessibleGroup();
			
			Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
       
	        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
	                          EmailEncodingKey='UTF-8', FirstName = 'user' ,LastName='Testing', LanguageLocaleKey='en_US',
	                          LocaleSidKey='en_US', ProfileId = p.Id,
	                          TimeZoneSidKey='America/Los_Angeles',     UserName='test_OncoPeer_User@varainTest.com',
	                          OCSUGC_User_Role__c='Varian Community Contributor');
	        insert u;
			system.runas(u){
				cont.currentGroupId = fgabGroup.Id;
				cont.currentGroup = fgabGroup;
				cont.isInvited = false;
				cont.isValidAccessibleOAB();
				cont.isValidAccessibleGroup();			
			}
			
			String testSummary = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ';
  			OCSUGC_GroupDetailController control = new OCSUGC_GroupDetailController();
  			//control.truncateDiscussionBody(testSummary);
  			control.currentGroupId = publicGroup.Id;
			control.leaveGroupPage();
			system.runas(u){
			control.currentGroupId = privateGroup.Id;
			control.currentGroup = privateGroup;
			control.joinOABGroup();
			}
			
			control.sendNotificationToMember('test@testMail.com', 'Approved');
			
		Test.StopTest();
	}
	
	public static testMethod void updateGroupMemberRequest_test() {
		Test.StartTest();
			Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
       
	        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
	                          EmailEncodingKey='UTF-8', FirstName = 'user' ,LastName='Testing', LanguageLocaleKey='en_US',
	                          LocaleSidKey='en_US', ProfileId = p.Id,
	                          TimeZoneSidKey='America/Los_Angeles',     UserName='test_OncoPeer_User@varainTest.com',
	                          OCSUGC_User_Role__c='Varian Community Contributor');
			insert u;
			
			CollaborationGroup privateGroup2;
			system.runas(u){
				privateGroup2 = OCSUGC_TestUtility.createGroup('test private group 2','Private',ocsugcNetwork.id,true);
			}
			
			ApexPages.currentPage().getParameters().put('dc', 'true');
			CollaborationGroupMemberRequest req = OCSUGC_TestUtility.createGroupMemberRequest(String.valueOf(userInfo.getUserId()), string.valueOf(privateGroup2.Id), false);
			//req.Email = u.Email;
			insert req;
			
			OCSUGC_GroupDetailController control = new OCSUGC_GroupDetailController();
			control.requestAction = 'Approved';
			control.requestIdToUpdate = req.Id;
			control.updateGroupMemberRequest();
		Test.StopTest();
	}
}
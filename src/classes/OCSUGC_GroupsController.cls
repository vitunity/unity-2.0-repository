public with sharing class OCSUGC_GroupsController extends OCSUGC_Base_NG {

  public List<GroupWrapper> groups {get;set;}
  public Id selectedGroupId {get;set;}          // Id of group to be joined/delete/leave by profiled-user
  public OCSUGC_CommunityData cdata {get;set;}
  public Boolean showingSearchRslt {get;set;}
  public String groupSearchTerm {get; set;}
  public Integer groupsCount {get; set;}


  @TestVisible private Id orgWideEmailAddressId;

  //Static final variable for querying Email Template for Owner of Group
  private static final String OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER = 'OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER';

	public OCSUGC_GroupsController() {
		pageTitle = 'Groups | OncoPeer';

    orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();

    showingSearchRslt = false;
    groupSearchTerm = '';
    groupsCount = 0;
	}

  /*
   *  method call from action function on page load
   */
  public void loadGroups() {
    fetchOtherGroups(false);
    getRecommendedGroups();
  }

  public void fetchOtherGroups(Boolean doSearch) {
    groups = new List<GroupWrapper>();
    
    String grpMemberQuery = ' SELECT MemberId, CollaborationGroupId, Member.isActive, CollaborationGroup.IsArchived, CollaborationGroup.NetworkId '+
                ' FROM CollaborationGroupMember ' + 
                ' WHERE MemberId =\'' +  loggedInUser.Id + '\'' + //' And CollaborationGroupId IN: otherGroupsIds '+
                ' AND Member.isActive = true ' + 
                ' AND (Member.Contactid = null OR (Member.Contactid != null ' +
                            ' AND Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\')) ' +
                ' AND CollaborationGroup.IsArchived = false AND ';

    if(isDCMember || isCommunityManager || isDCContributor) {
      grpMemberQuery += ' (CollaborationGroup.NetworkId = \''+ocsugcNetworkId+'\' OR CollaborationGroup.NetworkId = \''+ ocsdcNetworkId+ '\') ';
    }
    else {
      grpMemberQuery += ' CollaborationGroup.NetworkId = \''+ocsugcNetworkId+'\' ';
    }
    
    Set<Id> collGrpMemIdSet = new Set<Id>();
    for(CollaborationGroupMember cgm : Database.Query(grpMemberQuery)) {
      system.debug(' ===>cgm.CollaborationGroup.NetworkId<=== ' + cgm.CollaborationGroup.NetworkId);
      collGrpMemIdSet.add(cgm.CollaborationGroupId);
    }
    
    String grpQuery = ' SELECT Id, Name, Description, CollaborationType, FullPhotoUrl, SmallPhotoUrl, IsArchived, LastFeedModifiedDate, ' + 
                       ' MemberCount, NetworkId, OwnerId, Owner.Name, Owner.isActive, Owner.FirstName, Owner.LastName, ' +
                       ' (Select RequesterId From GroupMemberRequests  Where Status=\'Pending\' AND RequesterId = \'' + loggedInUser.Id + '\') ' +
                    ' FROM CollaborationGroup ' + 
                    ' WHERE IsArchived = false ' + 
                    ' AND Id NOT IN: collGrpMemIdSet ' +
                    ' AND CollaborationType != \'Unlisted\' ' +
                    ' AND Owner.isActive = true AND ';

    if(isDCMember || isCommunityManager || isDCContributor) { // ONCO-481
      grpQuery += ' (NetworkId = \''+ocsugcNetworkId+'\' OR NetworkId = \''+ ocsdcNetworkId+ '\') ';
    }
    else {
      grpQuery += ' NetworkId = \''+ocsugcNetworkId+'\' ';
    }

    if(doSearch) {
      String strSearchText = '\'%'  + String.escapeSingleQuotes(groupSearchTerm.trim()) + '%\'';
      grpQuery += ' AND Name LIKE '  + strSearchText;
    }

    grpQuery += ' ORDER BY LastFeedModifiedDate DESC LIMIT 100 ';

    System.debug('^^^^^^^^^^^^^^^^^ TADEBUG Flag:Query---'+doSearch+':'+grpQuery+'--- ^^^^^^^^^^^^^^^^^^^^');

    for(CollaborationGroup gp : Database.Query(grpQuery)) {
      gp.Description = validateSummary(gp.Description);
      GroupWrapper grpWrapper = new GroupWrapper(gp);
      
      /** Setting class name on basis on NetworkId */
      if(gp.NetworkId == Label.OCSUGC_NetworkId) {
        grpWrapper.className = 'oncopeer';
      } else if(gp.NetworkId == Label.OCSDC_NetworkId) {
        grpWrapper.className = 'developerCloud';
        grpWrapper.isDCGroup = true;
      }
      /** Class name on basis of OwnerId */
      if(gp.CollaborationType == 'Public') 
        grpWrapper.className += ' public';
      else if(gp.CollaborationType == 'Private') 
        grpWrapper.className += ' private';

      if(gp.GroupMemberRequests.size() > 0) {
        grpWrapper.requestAlreadySent = true;
      }
      
      groups.add(grpWrapper);
    }

    groups.sort();
    groupsCount = groups.size();
    System.debug('^^^^^^^^^^^^^^^^^ TADEBUG Count:Data---'+groupsCount+':'+groups+'--- ^^^^^^^^^^^^^^^^^^^^');
  }

  /**
   *  method returns the recommedation Groups for logged-in User only.
   */
  public void getRecommendedGroups() {
    String endPoint;
    endPoint = 'https://' + apexpages.currentpage().getheaders().get('X-Salesforce-Forwarded-To');
    endpoint += '/services/data/v31.0/connect/communities/'+ocsugcNetworkId+'/chatter/users/'+UserInfo.getUserId()+'/recommendations/view/groups';
    String sessionId = userInfo.getSessionId();//005M0000006Jato
    HttpRequest request = new HttpRequest();
    request.setEndpoint(endPoint);
    request.setMethod('GET');
        request.setHeader('Accept','application/json');
        request.setHeader('Authorization','OAuth ' + Userinfo.getSessionId());
        request.setHeader('X-PrettyPrint','1');
    request.setCompressed(false);
    Http h = new Http();
    HttpResponse res = new HttpResponse();
    res = h.send(request);
    String responseString = res.getBody();    
    system.debug(' ====responseString ==== ' + responseString);
      cdata = OCSUGC_CommunityData.parse(responseString);
    system.debug(' ====== No of Recommendations  ======== ' + cdata.recommendations.size());

  }

  /**
   *  @param    none
   *  @return   PageReference
   *        method will called when profiled user join group
   */
  public PageReference joinGroupAction() {
    try {
      CollaborationGroupMember joinGrpMem;
      if(selectedGroupId != null) {
        joinGrpMem = new CollaborationGroupMember( CollaborationGroupId = selectedGroupId,
                                        memberId = loggedInUser.Id);
        insert joinGrpMem;
      }
      return null;
    } catch (Exception e) {
      system.debug(' EXCEPTION ' + e.getMessage() + e.getLineNumber());
      return null;
    }
    return null;
  }

  /**
   *  @param    none
   *  @return   PageReference
   *  method will called when profiled user request to join private group
   */
  public PageReference requestJoinGroupAction() {
    //List to store Group Owner's email address
    List<String> lstOwnerEmailAddresses = new List<String>();
    
    // Inserting Group joining request
    CollaborationGroupMemberRequest collaborationRequest = new CollaborationGroupMemberRequest(
                                    CollaborationGroupId = selectedGroupId,
                                    RequesterId = loggedInUser.Id
                                  );
    insert collaborationRequest;
    
    //list will contain Email drafts which needs to be send
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        
        //Query and fetch the Email Template Format for sending email to Group Owner.
        EmailTemplate objEmailTemplate = [SELECT Id, Subject, HtmlValue, Body
                                          FROM EmailTemplate
                                          WHERE DeveloperName = :OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER
                                          LIMIT 1];
    
    String strHtmlBody  = objEmailTemplate.HtmlValue;
        String strPlainBody = objEmailTemplate.Body;
        String strSubject   = objEmailTemplate.Subject;
    
    for(CollaborationGroup objCollaborationGroup : [SELECT OwnerId, Owner.Email, Owner.ContactId, Name, Owner.Name, Owner.FirstName
                            FROM CollaborationGroup 
                            WHERE Id = :selectedGroupId
                              AND (Owner.Contactid = null
                                                               OR (Owner.Contactid != null
                                                                  AND Owner.Contact.OCSUGC_Notif_Pref_ReqJoinGrp__c =true))
                            LIMIT 1]) {
      if(objCollaborationGroup.Owner.Email != null)
        lstOwnerEmailAddresses.add(objCollaborationGroup.Owner.Email);
      
      // Inserting Notification for the group owner
      // TODO : find networkName
      OCSUGC_Intranet_Notification__c notify = OCSUGC_Utilities.prepareIntranetNotificationRecords('networkName',
                                                      objCollaborationGroup.OwnerId, objCollaborationGroup.Name,
                                                      'has asked to join the group', 'ocsugc_groupdetail?fgab=false&g='+selectedGroupId+'&invitedUserId=null&showPanel=Members');
      if(notify!=null)
        insert notify;
      
      
      // replacing Header in Email Template according to Community
      String communityName = '/';
      if(isDevCloud) {
        strHtmlBody    = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSDC_Email_Header_Image_Path);
                communityName += Label.OCSDC_DC_Community_Prefix; 
      } else {
        strHtmlBody    = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
                communityName += Label.OCSUGC_CommunityName;
      }
      
      strHtmlBody = strHtmlBody.replace('GROUP_OWNER_NAME', objCollaborationGroup.Owner.FirstName);
            strHtmlBody = strHtmlBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
            strHtmlBody = strHtmlBody.replace('USER_LAST_NAME', Userinfo.getLastName());
            strHtmlBody = strHtmlBody.replace('GROUP_NAME',objCollaborationGroup.Name);
      
      strHtmlBody = strHtmlBody.replace('URL_LINK','<a href="'+ Label.OCSUGC_Salesforce_Instance + communityName +'/OCSUGC_GroupDetail?g=' + objCollaborationGroup.Id +'&showPanel=Members&dc='+isDevCloud+'">Click here</a>');
            strHtmlBody = strHtmlBody.replace('COMMUNITY_NAME',isDevCloud?Label.OCSDC_DeveloperCloud:Label.OCSDC_OncoPeer);
            
            strHtmlBody += '';
            strHtmlBody += '';
      
      //replace the logged in user's FirstName, LastName, Group Name and The Group Link dynamically in PlainBody
            strPlainBody = strPlainBody.replace('GROUP_OWNER_NAME', objCollaborationGroup.Owner.FirstName);
            strPlainBody = strPlainBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
            strPlainBody = strPlainBody.replace('USER_LAST_NAME', Userinfo.getLastName());
            strPlainBody = strPlainBody.replace('GROUP_NAME',objCollaborationGroup.Name);
            strPlainBody = strPlainBody.replace('URL_LINK', Label.OCSUGC_Salesforce_Instance + communityName +'/OCSUGC_GroupDetail?g=' + objCollaborationGroup.Id +'&showPanel=Members&dc='+isDevCloud);
            strPlainBody = strPlainBody.replace('COMMUNITY_NAME',isDevCloud?Label.OCSDC_DeveloperCloud:Label.OCSDC_OncoPeer);
      
      strSubject  = strSubject.replace('GROUP_NAME',objCollaborationGroup.Name);
    }
    
    //set email to the Group owner
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSaveAsActivity(false);
        // Use Organization Wide Address
        if(orgWideEmailAddressId != null)
            email.setOrgWideEmailAddressId(orgWideEmailAddressId);
        else 
          email.setSenderDisplayName('OncoPeer');
        email.setSubject(strSubject);
        email.setHtmlBody(strHtmlBody);
        email.setToAddresses(lstOwnerEmailAddresses);
        lstEmail.add(email);  
    
    // Sending Request Email to Group Owner
    try {
      if(!lstEmail.isEmpty())
        Messaging.sendEmail(lstEmail);
      
      return null;
    } catch(Exception e) {
      system.debug(' Exception @ ' + e.getLineNumber() + ' Exception : ' + e.getMessage()); 
      return null;
    }
  }

  /**
     *  @param    none
     *  @return   void
     *  method called when group is searched with the name of the Group.
     */
  public void searchGroups() {
    System.debug('^^^^^^^^^^^^^^^^^ TADEBUG ---'+groupSearchTerm+'--- ^^^^^^^^^^^^^^^^^^^^');
    if(String.isBlank(groupSearchTerm)) {
      return;
    }
    else {
      fetchOtherGroups(true);
      showingSearchRslt = true;
    }
  }

  /**
   *  @param    String
   *  @return   String
   *  Method shorten the description/Owner/GroupName to be displayed on page
   */
  @TestVisible private String validateSummary(String summary) {
      if(summary != null && summary.length() > 195) {
          summary = summary.substring(0,195)+'...';
      }
      return summary;
  }

  public Class GroupWrapper  implements Comparable {
    public CollaborationGroup grp {get;set;}
    public String className{get;set;}
    public Boolean joinGroup{get;set;}
    public Boolean requestJoinGroup{get;set;}
    public Boolean requestAlreadySent{get;set;}
    public Boolean isDCGroup{get;set;}
    
    /* Code Block execute before constructor
      setting the Boolean variables value to false
    */
    {
      joinGroup = false;
      requestJoinGroup = false;
      requestAlreadySent = false;
      isDCGroup = false;
    }
    public GroupWrapper(CollaborationGroup gp) {
      this.grp = gp;
      
      // group type public/private
      if(grp.CollaborationType == 'Public')
        joinGroup = true;
      else if(grp.CollaborationType == 'Private')
        requestJoinGroup = true;
    }

    public GroupWrapper() {}

    public Integer compareTo(Object objToCompare) {
        GroupWrapper comparedToGroup = (GroupWrapper)objToCompare;

        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (grp.Name > comparedToGroup.grp.Name) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (grp.Name < comparedToGroup.grp.Name) {
            // Set return value to a negative value.
            returnValue = -1;
        }

        return returnValue;
    }
  }
}
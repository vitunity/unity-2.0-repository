/**
 *	CreateDate : 8 dec 2016
 */
@isTest
public with sharing class OCSUGC_GroupsControllerTest {
    
    public static CollaborationGroup publicCollocGroup, privateCollocGroup;
    public static Network ocsugcNetwork;
    static {
    	ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    	publicCollocGroup = OCSUGC_TestUtility.createGroup('publicTestGroup','Public',ocsugcNetwork.id,true);
    	privateCollocGroup = OCSUGC_TestUtility.createGroup('privateTestGroup','Private',ocsugcNetwork.id,true);
    }
    
    public static testMethod void GroupWrapperTest() {
    	Test.StartTest();
    		
    		OCSUGC_GroupsController.GroupWrapper cont = new OCSUGC_GroupsController.GroupWrapper(publicCollocGroup);
    		//cont.compareTo(publicCollocGroup);
    		cont.className = '';
    		
    		OCSUGC_GroupsController.GroupWrapper privatecont = new OCSUGC_GroupsController.GroupWrapper(privateCollocGroup);
    	Test.StopTest();
    }
    
    public static testMethod void joinGroupAction_test() {
    	Test.StartTest();
    		OCSUGC_GroupsController control = new OCSUGC_GroupsController();
  			control.selectedGroupId = publicCollocGroup.Id;
  			control.joinGroupAction();
  			
  			// ** Exception Coverage **/
  			OCSUGC_GroupsController control1 = new OCSUGC_GroupsController();
  			control1.selectedGroupId = userInfo.getUserId();
  			control1.joinGroupAction();
    	Test.StopTest();
    }
    
    public static testMethod void validateSummaryTest() {
    	Test.StartTest();
    		String testSummary = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' +
    							 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ - ';
  			OCSUGC_GroupsController control = new OCSUGC_GroupsController();
  			control.validateSummary(testSummary);
  			control.groupSearchTerm = 'search';
  			control.searchGroups();
  			control.groupSearchTerm = '';
  			control.searchGroups();
    	Test.StopTest();
    }
    
    public static testMethod void recommendation () {
    	Test.StartTest();
    		Test.setMock(HttpCalloutMock.class, new OCSUGC_MockCalloutController ());
    		OCSUGC_GroupsController control = new OCSUGC_GroupsController();
    		control.loadGroups();
     		control.getRecommendedGroups();
    	Test.Stoptest();
    }
    
    public static testMethod void requestJoinGroupAction_Test() {
    	Test.StartTest();
    		Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
       
	        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
	                          EmailEncodingKey='UTF-8', FirstName = 'user' ,LastName='Testing', LanguageLocaleKey='en_US',
	                          LocaleSidKey='en_US', ProfileId = p.Id,
	                          TimeZoneSidKey='America/Los_Angeles',     UserName='test_OncoPeer_User@varainTest.com');
	        insert u;
	        
	        system.runas(u){
	    		OCSUGC_GroupsController control = new OCSUGC_GroupsController();
	    		control.selectedGroupId = privateCollocGroup.Id;
	    		control.requestJoinGroupAction();
	        } 
    	Test.Stoptest();
    }
    
    public static testMethod void fetchOtherGroupsTest() {
    	Test.StartTest();
    	Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
       
	        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
	                          EmailEncodingKey='UTF-8', FirstName = 'user' ,LastName='Testing', LanguageLocaleKey='en_US',
	                          LocaleSidKey='en_US', ProfileId = p.Id,
	                          TimeZoneSidKey='America/Los_Angeles',     UserName='test_OncoPeer_User@varainTest.com');
	        insert u;
	        
	        system.runas(u){
	    		OCSUGC_GroupsController control = new OCSUGC_GroupsController();
	    		control.fetchOtherGroups(false);
	        }
    	Test.Stoptest();
    }
    
}
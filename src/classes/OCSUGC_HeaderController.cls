/*
Name        : OCSUGC_HeaderController
Updated By  : Rishi Khirbat (Appirio India)
Date        : May, 2014
Purpose     : An Apex controller to get Information for CAKE_Header.component

Refrence    : T-286162, T-284651, T-279101, I-116447

// Changes Dated        Owner                   Description                 Task-ID
// Updated 25-09-2014   Naresh K Shiwani        Added insertSearchElement() T-322695
// Updated 25-09-2014   Puneet Sardana          Notification date shown as relative Ref T-321635
// Updated 10-11-2014   Puneet Sardana          Fetching of isReadOnlyUser changed Ref T-331918
// 18-11-2014           Ajay Gautam             Added FGAB Support to Discussion URL Ref: T-334649
// 17-04-2015           Naresh K Shiwani        Added FGAB Condition for create menu Ref: T-379076
// 4/5/2015             Puneet Mishra           Added FGAB support switch menu for Read only users
// 07-08-2015			Pratz Joshi(Appirio)    Update the IF Condition for ONCO-347
// 03-09-2015			Puneet Mishra			[ONCO-368] Message alerts/Count in Message Menu instead of Alert Menu
// 29-09-2015			Puneet Mishra			[ONCO-392] added SOQL to fetch Notification in Alert section of last 30 days as notification menu to show 
//																										last month records was not working/not implemented
*/
public class OCSUGC_HeaderController extends OCSUGC_Base_NG {

    private static final String CONST_TRUE = 'true';
    public String currentSpecialArea {get;set;}               
    private String ocsugcNetworkID;
    public User currentUser {get;set;}
    public String ghostText {get;set;}
    public Integer countNotifications {get;set;}
    public String searchText {get;set;}
    //
    public Integer unreadMessages {get;set;}
    public String viewedNotificationId {get;set;}
    public Boolean newNotifications {get;set;}
    public List<NotificationWrapper> lstNotifications {get;set;}

    public Boolean showCreateMenu {get;set;}
    public Boolean showLogo {get;set;}
    public Boolean showSwitchMenu {get;set;}
    public Boolean showCloudDeveloperMenu {get;set;}
    public Boolean showSearch {get;set;}
    public Boolean showAlertMenu {get;set;}
    public Boolean showProfileMenu {get;set;}
    //private Boolean showFGABCreateMenu;
    Set<String> groupIds {get;set;}
    static final String RT_Application = 'Application';
    static final String RT_IntranetCMSPages = 'Intranet CMS Pages';
    static final String Developer_Cloud_Member_User = 'Member';
 
 	// [ONCO-368]created by Puneet Mishra, 3 Sept 2015, to count the mail/messages notifications.
	public Integer countMailNotifications{get;set;}
 
    public List<CollaborationGroup> getFGABGroups() {
        return [Select Id, Name
                From CollaborationGroup
                Where NetworkId =:ocsugcNetworkID
                And Id IN :groupIds
                And CollaborationType = 'Unlisted'
                And isArchived = false
                Order by Name];
    }

    public OCSUGC_HeaderController() {
        super();
        showLogo = true;
        showCreateMenu = false;
        //showFGABCreateMenu = false;
        countNotifications = 0;
        
        //[ONCO-368] setting value to 0
        countMailNotifications = 0;
        
        if(isDC) {
            ghostText = Label.OCSDC_Search_Community; //'Search within Developer Cloud';
        } else {
            ghostText = Label.OCSUGC_Search_OncoPeer_Cloud_Community; //'Search within OncoPeer';
        }
        

        if( isDC )
            currentSpecialArea = 'DC';
        else if (isFGAB)
            currentSpecialArea = oabGroupId;
        else
            currentSpecialArea = 'OCSUGC';
        
                   
        ocsugcNetworkID = OCSUGC_Utilities.getNetworkId('OncoPeer');

        groupIds  = new Set<String>();
        showSwitchMenu = true;
        showCloudDeveloperMenu = true;
        if(isCommunityManager) {            
            groupIds = OCSUGC_Utilities.fetchFGABGroupIds(true);
        }
        else if(isCommunityContributor) {            
            groupIds = OCSUGC_Utilities.fetchGroupIds(ocsugcNetworkID, UserInfo.getUserId(), true, null, false);
        }
        else if(isCommunityMember) {        	    
            groupIds = OCSUGC_Utilities.fetchGroupIds(ocsugcNetworkID, UserInfo.getUserId(), true, null, isCommunityManager);
        }
        /* 4/5/2015     Puneet Mishra       for Read only users*/
        else {
            groupIds = OCSUGC_Utilities.fetchReadOnlyUserFGABGroupIds(ocsugcNetworkID, UserInfo.getUserId(), true);
        }
        showSwitchMenu = showSwitchMenu && groupIds.size() > 0 ? true : false;
        
        if(isCommunityContributor && !isDCContributor)
            showCloudDeveloperMenu = false;
        
        showAlertMenu =  true;
        
        showProfileMenu = true;
                
        showSearch = true;
        
        if(oabGroupId != null) {
                for(CollaborationGroup gp :[Select Name
                                            From CollaborationGroup
                                            Where Id =:oabGroupId]) {
             ghostText = 'Search within '+ gp.Name;
            }
        }

        for(User currUser :[Select Id,
                                   Name,
                                   SmallPhotoUrl,
                                   ContactId,Contact.OCSDC_UserStatus__c,
                                   Contact.Account.Legal_Name__c
                            From User Where Id =:UserInfo.getUserId()]) {
            currentUser = currUser;
        }
            
        if(!showCloudDeveloperMenu && String.isNotBlank(currentUser.Contact.OCSDC_UserStatus__c) && currentUser.Contact.OCSDC_UserStatus__c.equalsIgnoreCase(Developer_Cloud_Member_User)) {
            showCloudDeveloperMenu = true;
        }
        fatchNotifications();
        getUnreadMessageCount();
      
    }

    //Start Notifications
    // T-284651: System Notifications
    // As a member of CAKE, I can see any other notifications I have received.
    // These notifications should show up in the Notifications Menu in the top right corner of the Home Page screen.
    // Display the # of Unread notifications on the bell icon.
    // Once the notification is seen by the user, it should continue to exist in the notification menu but not highlighted.
    // The notifications menu should display all (read or unread) notifications received in last one month
    // As the Notifications icon is clicked, the menu grows out from the top and lists out all unread notifications
    public Pagereference fatchNotifications() {
        lstNotifications = new List<NotificationWrapper>();
        for(OCSUGC_Intranet_Notification__c notify : [SELECT  OCSUGC_Link_to__c,
                                                              OCSUGC_Description__c,
                                                              OCSUGC_Related_User__c,
                                                              OCSUGC_Related_User__r.SmallPhotoUrl,
                                                              OCSUGC_Is_Viewed__c,
                                                              OCSUGC_Notification_Datetime__c,
                                                              CreatedDate
                                                    FROM OCSUGC_Intranet_Notification__c
                                                    WHERE  OCSUGC_User__c =:Userinfo.getUserId()
                                                    // [ONCO-368] Added condition to filter out Message Notifications
                                                    AND ( NOT OCSUGC_Link_to__c LIKE '%OCSUGC_MessageDetailView%')
                                                    // [ONCO-392] to show only one month old Notification
                                                    AND CreatedDate = LAST_N_DAYS:30 
                                                    order by CreatedDate desc]){
              system.debug(notify.OCSUGC_Is_Viewed__c);
              lstNotifications.add(new NotificationWrapper(notify));
        }
        newNotifications = false;
        system.debug('==============viewedNotificationId: '+Userinfo.getUserId());
        for(OCSUGC_Intranet_Notification__c notification :[SELECT  Id
                                                            FROM OCSUGC_Intranet_Notification__c
                                                            WHERE  OCSUGC_User__c =:Userinfo.getUserId()
                                                            AND OCSUGC_Is_Viewed__c = false
                                                            // [ONCO-368] Added condition to filter out Message Notifications
                                                            AND ( NOT OCSUGC_Link_to__c LIKE '%OCSUGC_MessageDetailView%')]) {
            newNotifications = true;
            countNotifications++;
        }
        
        //[ONCO-368]AggregateResult result = [SELECT Count() FROM OCSUGC_Intranet_Notification__c WHERE OCSUGC_Description__c LIKE '%private message.%'];
        countMailNotifications = (Integer)[SELECT Count() FROM OCSUGC_Intranet_Notification__c WHERE OCSUGC_Link_to__c LIKE '%OCSUGC_MessageDetailView%' AND OCSUGC_User__c =:Userinfo.getUserId()
                                                            AND OCSUGC_Is_Viewed__c = false];
        
        return null;
    }

    public void getUnreadMessageCount() {
         String communityId = ocsugcNetworkId;//getting community Id
         System.debug('communityId=========HeaderController==== '+communityId);
         ConnectApi.UnreadConversationCount unreadMessageCount;
         if(!Test.isRunningTest()){
            unreadMessageCount = ConnectApi.ChatterMessages.getUnreadCount(communityId);
            System.debug('unreadMessageCount===='+unreadMessageCount);
         }
         if(unreadMessageCount  != null){
            unreadMessages = unreadMessageCount.unreadCount;
            System.debug('unreadMessages==='+unreadMessages);
         }
    }

    public Pagereference viewedNotifications() {
        system.debug('==============viewedNotificationId: '+viewedNotificationId);
        if(viewedNotificationId != null) {
            OCSUGC_Intranet_Notification__c notification = [SELECT OCSUGC_Is_Viewed__c, OCSUGC_Link_to__c
                                                             FROM OCSUGC_Intranet_Notification__c
                                                             WHERE  OCSUGC_User__c =:Userinfo.getUserId()
                                                             And Id =:viewedNotificationId];
            notification.OCSUGC_Is_Viewed__c = true;
            update notification;

            //fatchNotifications();
            system.debug('==============Link to: '+notification.OCSUGC_Link_to__c);
            PageReference acctPage = new PageReference(System.label.OCSUGC_Salesforce_Instance+'/'+notification.OCSUGC_Link_to__c);
            acctPage.setRedirect(true);
            return acctPage;
        }
        return null;
    }
    // End Notifications

    // Start Create
    public List<OCSUGC_MenuInfo__c> getCAKECreates() {
        
        OCSUGC_FGABPagesVisibility__c fgabVisibility = OCSUGC_FGABPagesVisibility__c.getValues(loggedInUser.OCSUGC_User_Role__c);
        Boolean fgabVisibile = fgabVisibility != null && fgabVisibility.OCSUGC_Visibility__c;               
        Boolean hasFGABPageAccess = !isOAB || (isOAB  && (fgabVisibile || isOABMember));
        
        Map<Integer,OCSUGC_MenuInfo__c> mapSequenceCreateOptions = new Map<Integer, OCSUGC_MenuInfo__c>();
        OCSUGC_CreateMenu__c createMenu = OCSUGC_CreateMenu__c.getValues(loggedInUser.OCSUGC_User_Role__c);
        OCSDC_CreateMenu__c dcCreateMenu = OCSDC_CreateMenu__c.getValues(loggedInUser.OCSUGC_User_Role__c);
        //05-02-2015  Puneet Sardana  Changed condition Ref T-357450
        //system.debug('----------'+[Select Id, name from OCSDC_CreateMenu__c]);
        system.debug('----------'+loggedInUser.OCSUGC_User_Role__c);
        system.debug('----------'+createMenu);
        system.debug('----------'+isReadOnlyUser);
        system.debug('----------'+isDC);
        system.debug('----------'+isFGAB);
        system.debug('----------'+isCommunityContributor);
        system.debug('----------'+isOABMember);
        system.debug('----------'+hasFGABPageAccess);
        
        // 07-08-2015	Updated the IF condition below by Pratz Joshi (Appirio) for ONCO-347
        //if(createMenu != null && !isReadOnlyUser && !isDC && ( (!isFGAB && !isContributor) || (isFGAB && (isFGABMember || hasFGABPageAccess)))) {
        if(canCreateContent != Null){
        if(createMenu != null && !isReadOnlyUser && !isDC && ((!isOAB && canCreateContent) || (isOAB && (isOABMember || hasFGABPageAccess)))) {
            for(OCSUGC_MenuInfo__c createOption : OCSUGC_MenuInfo__c.getall().values()) {
                if(createOption.OCSUGC_IsActive__c && createOption.OCSUGC_IsFGABCreateMenu__c == isOAB) {
                    if(createMenu.OCSUGC_FormGroup__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_FormGroup)) {
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if( (createMenu.OCSUGC_StartDiscussion__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_Start_a_Discussion)) ||
                              (createMenu.OCSUGC_AnnounceEvent__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_AnnounceEvent))) {
                        if(String.isNotBlank(oabGroupId)) {
                            createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '&g='+oabGroupId; // T-334649
                        }
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(createMenu.OCSUGC_ShareKnowledge__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_ShareKnowledge)) {
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(createMenu.OCSUGC_AnnounceEvent__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_AnnounceEvent)) {
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(createMenu.OCSUGC_FormFocusGroupAdvisoryBoard__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_FormFocusGroupAdvisoryBoard)) {
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(createMenu.OCSUGC_CreatePoll__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_CreatePoll)) {
                        if(String.isNotBlank(oabGroupId) && isOAB) { // T-379076
                            createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '&g='+oabGroupId; // T-334649
                        } 
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);                        
                    }
                }
            }
        }
        }
        system.debug(' ********************FGAB mapSequenceCreateOptions ******************' + mapSequenceCreateOptions);
        
        if(dcCreateMenu != null && !isReadOnlyUser && isDC){
            for(OCSUGC_MenuInfo__c createOption : OCSUGC_MenuInfo__c.getall().values()) {
                if(createOption.OCSUGC_IsActive__c && createOption.OCSDC_IsDCCreateMenu__c == isDC) {
                    if(dcCreateMenu.OCSUGC_FormGroup__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_FormGroup)) {
                        createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '&dc='+isDC;
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(dcCreateMenu.OCSUGC_AnnounceEvent__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_AnnounceEvent)) {
                        createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '?dc='+isDC;
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(dcCreateMenu.OCSUGC_StartDiscussion__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSUGC_Start_a_Discussion)) {
                        createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '&dc='+isDC;
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    } else if(dcCreateMenu.OCSUGC_CreatePoll__c && String.valueOf(createOption.OCSUGC_Display_Menu__c).equalsIgnoreCase(System.Label.OCSDC_CreatePoll)) {
                        createOption.OCSUGC_URL__c = createOption.OCSUGC_URL__c + '?dc='+isDC;
                        mapSequenceCreateOptions.put(createOption.OCSUGC_Sequence__c.intValue(), createOption);
                    }
                }
            }
        }
		
        if(mapSequenceCreateOptions.size() > 0) showCreateMenu = true;
        List<Integer> lstSequence = new List<Integer>();
        List<OCSUGC_MenuInfo__c> lstShowCreateOptions = new List<OCSUGC_MenuInfo__c>();

        lstSequence.addAll(mapSequenceCreateOptions.keySet());
        lstSequence.sort();

        for(Integer sequence :lstSequence) {
            if(mapSequenceCreateOptions.get(sequence) != null) {
                lstShowCreateOptions.add(mapSequenceCreateOptions.get(sequence));
            }
        }
		system.debug('***************Shital*****lstShowCreateOptions**********'+lstShowCreateOptions);
        return lstShowCreateOptions;
    }
    // End Creation

    // A Method which populate Tag name list of Tag__c Object
    @RemoteAction
    public static List<String> populateTagList(String strTag, Boolean isFGAB, String groupId, Boolean isDC) {

        Set<String> tagIds = new Set<String>();
        if(isFGAB) {
            //Knowledge File Tags
            for(OCSUGC_Knowledge_Tag__c tag :[Select OCSUGC_Tags__c From OCSUGC_Knowledge_Tag__c
                                              Where OCSUGC_Knowledge_Exchange__r.OCSUGC_Group_Id__c =:groupId]) {
                tagIds.add(tag.OCSUGC_Tags__c);
            }
            //Event Tags
            for(OCSUGC_Intranet_Content_Tags__c tag :[Select OCSUGC_Tags__c From OCSUGC_Intranet_Content_Tags__c
                                              Where OCSUGC_Intranet_Content__r.OCSUGC_GroupId__c =:groupId]) {
                tagIds.add(tag.OCSUGC_Tags__c);
            }
            //Discussion Tags
            Set<String> discussionIds = new Set<String>();
            for(OCSUGC_DiscussionDetail__c discussion :[Select OCSUGC_DiscussionId__c From OCSUGC_DiscussionDetail__c Where OCSUGC_GroupId__c =:groupId]) {
                discussionIds.add(discussion.OCSUGC_DiscussionId__c);
            }
            for(OCSUGC_FeedItem_Tag__c tag :[Select OCSUGC_Tags__c From OCSUGC_FeedItem_Tag__c Where OCSUGC_FeedItem_Id__c IN :discussionIds]) {
                tagIds.add(tag.OCSUGC_Tags__c);
            }
        }

        system.debug('=======strTag========'+strTag);
        List<String> tagList = new List<String>();
        String query = 'SELECT Id, OCSUGC_Tag__c FROM OCSUGC_Tags__c';
        if(isFGAB) {
            query += ' WHERE OCSUGC_IsFGABUsed__c = true ';
            query += ' AND ID IN '+OCSUGC_Utilities.getStringIds(tagIds) +' ';
        } else if(isDC) {
            query += ' WHERE OCSDC_IsDeveloperCloudUsed__c = true ';
        } else {
             query += ' WHERE OCSUGC_IsOncoPeerUsed__c = true ';
        }


        //if strTag is blank then fetch all Tags
        if(strTag !=  '') {
            query += ' AND OCSUGC_Tag__c like \''+ strTag +'%\'';
        }

        query += ' LIMIT 1000';

        for(OCSUGC_Tags__c tag : Database.query(query)){
            tagList.add(tag.OCSUGC_Tag__c);
        }
        system.debug('=======tagList========'+tagList);
        return tagList;
    }

    // A Method which update Notification isViewed to true for current user
    @RemoteAction
    public static void updateNotifications(){

        List<OCSUGC_Intranet_Notification__c> updateNotiList = new List<OCSUGC_Intranet_Notification__c>();
        // Populate all Notification for current user which is not viewed by user
        for(OCSUGC_Intranet_Notification__c notification : [SELECT Id FROM OCSUGC_Intranet_Notification__c
                                                    WHERE OCSUGC_User__c = :UserInfo.getUserId()
                                                    and OCSUGC_Is_Viewed__c = false
                                                    ]){
            notification.OCSUGC_Is_Viewed__c = true;
            updateNotiList.add(notification);
        }
        if(updateNotiList.size() > 0){
            update updateNotiList;
        }
    }

    //Logout method added by Rishi Khirbat
    public pagereference logoutmethod() {
        return OCSUGC_Utilities.logoutMethod();
    }

    // For Inserting search element in Search Terms Object
    // If search item exist increment frequency else create new record.
    public Pagereference insertSearchElement() {
        OCSUGC_SearchTerms__c objSearchTerm;
        try{
            for(OCSUGC_SearchTerms__c searchItem :[Select Name,Id, OCSUGC_Frequency__c
                                                   From OCSUGC_SearchTerms__c
                                                   where OCSUGC_Search_Text__c =: searchText limit 1]){
                    objSearchTerm = searchItem;
            }
            if(objSearchTerm!=null){
                objSearchTerm.OCSUGC_Frequency__c++;
                update objSearchTerm;
            }
            else{
                objSearchTerm = new OCSUGC_SearchTerms__c();
                objSearchTerm.OCSUGC_Search_Text__c = searchText;
                insert objSearchTerm;
            }

            Pagereference pg = Page.OCSUGC_Home;
            pg.getParameters().put('isResetPwd','true');
            pg.getParameters().put('fgab',''+isOAB);
            pg.getParameters().put('g',''+oabGroupId);
            if(String.isNotBlank(searchText)){
                pg.getParameters().put('searchString',searchText);
            }
            if(isDC){
                pg.getParameters().put('dc','true');
            }
            system.debug('------------'+pg);
            pg.setRedirect(true);
            return pg;
        } catch(Exception e) {
            return null;
        }
    }
    // Method ends here
    // Updated 25-09-2014   Puneet Sardana     Notification date shown as relative Ref T-321635
    public class NotificationWrapper{
        public String dateStr {get; set;}
        public String sDate {get; set;}
        public OCSUGC_Intranet_Notification__c notification {get; set;}
        public boolean isViewed {get; set;}

        public NotificationWrapper(OCSUGC_Intranet_Notification__c notification){
            this.notification = notification;
            this.isViewed = notification.OCSUGC_Is_Viewed__c;
            this.sDate = OCSUGC_Utilities.GetRelativeTimeString(notification.createdDate,Datetime.now());

        }
    }
}
/*
Name        : CAKE_HeaderControllerTest
Updated By  : Priyanka Kumar
Date        : 23 July, 2014
Purpose     : Test class  for CAKE_HeaderController
*/

@isTest
private class OCSUGC_HeaderControllerTest {
   static User managerUsr,contributorUsr,memberUsr,readOnly;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3, contact4;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup,unlistedGroup;
   static CollaborationGroupMember groupMember1,groupMember2;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static List<CollaborationGroup> lstGroupInsert;
   static Profile admin,portal,manager;
   static OCSUGC_Knowledge_Exchange__c knowledgeFile;
   static Network ocsugcNetwork;
   static OCSUGC_Intranet_Notification__c intraNote;
   
   static {
   	 System.runAs(new User(Id=userInfo.getuserId())) {
   	 	 List<OCSUGC_MenuInfo__c> menuInfoList = new List<OCSUGC_MenuInfo__c>();
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSDC_CreatePoll', 1, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_CreateFGAB', 2, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_CreateGroup', 3, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_CreateKnowledgeArtifact', 4, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_CreateNewDiscussion', 4, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_CreateNewEvent', 4, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_FGABCreateDiscussion', 4, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_FGABCreateNewEvent', 4, true, true, false));
   	 	 menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_FGABCreatePoll', 4, true, true, false));
   	 	 //menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('OCSUGC_FormGroup', 1, true, true, false));
   	 	 insert menuInfoList;
   	 	 
   	 	 List<OCSUGC_CreateMenu__c> menuList = new List<OCSUGC_CreateMenu__c>();
   	 	 menuList.add(OCSUGC_TestUtility.createMenu('Varian Community Contributor', false));
   	 	 menuList.add(OCSUGC_TestUtility.createMenu('Varian Community Manager', false));
   	 	 menuList.add(OCSUGC_TestUtility.createMenu('Varian Community Member', false));
   	 	 menuList.add(OCSUGC_TestUtility.createMenu('Varian ReadOnly User', false));
   	 	 insert menuList;
   	 	 
   	 	 List<OCSDC_CreateMenu__c> dcmenuList = new List<OCSDC_CreateMenu__c>();
   	 	 dcmenuList.add(OCSUGC_TestUtility.createMenuDC('Varian Community Contributor', false));
   	 	 dcmenuList.add(OCSUGC_TestUtility.createMenuDC('Varian Community Manager', false));
   	 	 dcmenuList.add(OCSUGC_TestUtility.createMenuDC('Varian Community Member', false));
   	 	 dcmenuList.add(OCSUGC_TestUtility.createMenuDC('Varian ReadOnly User', false));
   	 	 insert dcmenuList;
   	 }
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'testuser318', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'testuser319', '1'));
     
     adminUsr1.UserRoleID = OCSUGC_TestUtility.getRole().id;
     adminUsr2.UserRoleID = OCSUGC_TestUtility.getRole().id;
     insert lstUserInsert;
     System.runas(adminUsr2) {
        account = OCSUGC_TestUtility.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert;
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member));
        readOnly = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact4.Id, '6',Label.OCSUGC_Varian_ReadOnly_User);
        readOnly.ContactId = contact4.Id;
        lstUserInsert.add(readOnly);
        insert lstUserInsert;
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1' ,'Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(unlistedGroup = OCSUGC_TestUtility.createGroup('test unlisted group 12','Unlisted',ocsugcNetwork.Id,false));
        insert lstGroupInsert;
        lstGroupMemberInsert = new List<CollaborationGroupMember>();
        lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
        insert lstGroupMemberInsert;
     }
     
     
     System.runAs(adminUsr2) {
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, adminUsr1.Id, true);
     }
   }
	
	public static testmethod void test_OCSUGC_HeaderController() {
			test.startTest();
				System.runAs(memberUsr) {
					intraNote = OCSUGC_TestUtility.createNotification(userInfo.getUserId(), true);
					ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
			        ApexPages.currentPage().getParameters().put('fgab', 'true');
			        ApexPages.currentPage().getParameters().put('poll', 'true');
			        ApexPages.currentPage().getParameters().put('dc', 'false');
					OCSUGC_HeaderController conObj = new OCSUGC_HeaderController();
					//conObj.loginUserRole = Label.OCSUGC_Varian_Employee_Community_Member;
			        conObj.oabGroupId = unlistedGroup.Id;
			        conObj.searchText = 'discussion';
			        conObj.getFGABGroups();	
			        conObj.isCommunityManager = false;
			        conObj.isCommunityMember = true;
			        conObj.canCreateContent = true;
			        conObj.getFGABGroups();
			        conObj.viewedNotificationId = intranote.Id;
			        conObj.viewedNotifications();
			        conObj.getCAKECreates();
			        conObj.logoutmethod();
			        conObj.insertSearchElement();	
			        OCSUGC_HeaderController.populateTagList('Test', true, unlistedGroup.Id, false);
			        OCSUGC_HeaderController.updateNotifications();
			
			        System.assert(conObj.lstNotifications <> null);
				}
      test.stopTest();
	}
	
	/**
	 * CreatedDate : 6 dec 2016, Tuesday
	 * Puneet Mishra : test method for NotificationWrapper inner Class
	 */
	public static testmethod void NotificationWrapper_Test() {
		/*Test.StartTest();
			System.runAs(memberUsr) {
				intraNote = OCSUGC_TestUtility.createNotification(userInfo.getUserId(), true);
			}	
			OCSUGC_HeaderController.NotificationWrapper wrap = new OCSUGC_HeaderController.NotificationWrapper(intraNote);
			wrap.dateStr = '';
		Test.StopTest();*/	
	}
	
	/**
	 * Createdate : 6 dec 2016. Tuesday
	 * Puneet Mishra : test method for updateNotifications
	 */
	public static testMethod void updateNotificationsTest() {
		System.runAs(memberUsr) {
			intraNote = OCSUGC_TestUtility.createNotification(userInfo.getUserId(), false);
			intraNote.OCSUGC_Is_Viewed__c = false;
			insert intraNote;
		}
		Test.StartTest();
			
			OCSUGC_HeaderController.updateNotifications();
			
			system.runas(memberUsr){
				OCSUGC_SearchTerms__c searchT = new OCSUGC_SearchTerms__c(OCSUGC_Search_Text__c = 'Search Text');
				insert searchT;
			
			
				OCSUGC_HeaderController cont = new OCSUGC_HeaderController();
				cont.searchText = searchT.OCSUGC_Search_Text__c;
				cont.insertSearchElement();
			
			}
		Test.StopTest();
	}
	//isDC = OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc'));
	
	/**
	 *
	 */
	public static testMethod void HeaderControllerTest() {
		Test.StartTest();
		Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
       
	        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
	                          EmailEncodingKey='UTF-8', FirstName = 'user' ,LastName='Testing', LanguageLocaleKey='en_US',
	                          LocaleSidKey='en_US', ProfileId = p.Id,
	                          TimeZoneSidKey='America/Los_Angeles',     UserName='test_OncoPeer_User@varainTest.com',
	                          OCSUGC_User_Role__c='Varian Community Manager');
	        insert u;
	        system.runas(u){
		ApexPages.currentPage().getParameters().put('dc','true');
		OCSUGC_HeaderController control = new OCSUGC_HeaderController();
	        }
		Test.StopTest();
	}
	
	//@isTest(SeeAllData=true)
	public static testMethod void HeaderController() {
		Test.StarTtest();
			ApexPages.currentPage().getParameters().put('fgab', 'true');
			ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
			OCSUGC_HeaderController con = new OCSUGC_HeaderController();
			con.getCAKECreates();
		Test.Stoptest();
	}
	
	public static testMethod void getCakeTest() {
		Test.StartTest();
			system.runas(memberUsr){
				ApexPages.currentPage().getParameters().put('dc', 'true');
				ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
				OCSUGC_HeaderController con = new OCSUGC_HeaderController();
				con.getCAKECreates();
			}
		Test.StopTest();	
	}
	
	public static testMethod void getCakeTest2() {
		Test.StartTest();
			System.runAs(new User(Id=userInfo.getuserId())) {
				List<OCSUGC_MenuInfo__c> menuInfoList = new List<OCSUGC_MenuInfo__c>();
		   	 	menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('Form a Group', 1, true, true, false));
		   	 	insert menuInfoList;
			}
			system.runas(memberUsr){
				
				
				ApexPages.currentPage().getParameters().put('dc', 'true');
				ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
				OCSUGC_HeaderController con = new OCSUGC_HeaderController();
				con.getCAKECreates();
			}
		Test.StopTest();
	}
	
	public static testMethod void getCakeTest3() {
		Test.StartTest();
			System.runAs(new User(Id=userInfo.getuserId())) {
				List<OCSUGC_MenuInfo__c> menuInfoList = new List<OCSUGC_MenuInfo__c>();
		   	 	menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('Announce an Event', 1, true, true, false));
		   	 	insert menuInfoList;
			}
			system.runas(memberUsr){
				
				
				ApexPages.currentPage().getParameters().put('dc', 'true');
				ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
				OCSUGC_HeaderController con = new OCSUGC_HeaderController();
				con.getCAKECreates();
			}
		Test.StopTest();
	}
	
	public static testMethod void getCakeTest4() {
		Test.StartTest();
			System.runAs(new User(Id=userInfo.getuserId())) {
				List<OCSUGC_MenuInfo__c> menuInfoList = new List<OCSUGC_MenuInfo__c>();
		   	 	menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('Start a Discussion', 1, true, true, false));
		   	 	insert menuInfoList;
			}
			system.runas(memberUsr){
				
				
				ApexPages.currentPage().getParameters().put('dc', 'true');
				ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
				OCSUGC_HeaderController con = new OCSUGC_HeaderController();
				con.getCAKECreates();
			}
		Test.StopTest();
	}
	
	public static testMethod void getCakeTest5() {
		Test.StartTest();
			System.runAs(new User(Id=userInfo.getuserId())) {
				List<OCSUGC_MenuInfo__c> menuInfoList = new List<OCSUGC_MenuInfo__c>();
		   	 	menuInfoList.add(OCSUGC_TestUtility.createMenuInfo('Start a Poll', 1, true, true, false));
		   	 	insert menuInfoList;
			}
			system.runas(memberUsr){
				
				
				ApexPages.currentPage().getParameters().put('dc', 'true');
				ApexPages.currentPage().getParameters().put('g', unlistedGroup.Id);
				OCSUGC_HeaderController con = new OCSUGC_HeaderController();
				con.getCAKECreates();
			}
		Test.StopTest();
	}
	
	public static OCSUGC_MenuInfo__c createMenuInfo(String name, Integer sequence, Boolean isDC, Boolean isFgab,Boolean isInsert) {
    	OCSUGC_MenuInfo__c menu = new OCSUGC_MenuInfo__c();
    	menu.Name = name;
    	menu.OCSUGC_Display_Menu__c = name;
	    menu.OCSUGC_IsActive__c = true;
	    menu.OCSDC_IsDCCreateMenu__c= isDC;
	    menu.OCSUGC_IsFGABCreateMenu__c = isFgab;
	    menu.OCSUGC_Sequence__c = sequence;
	    menu.OCSUGC_URL__c = name;
    	if(isInsert)
    		insert menu;
    		
    	return menu;
    }
	
}
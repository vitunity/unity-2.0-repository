//
// (c) 2014 Appirio, Inc.
//
// Create a User Edit Page
//
//                                        Original
// 28 Aug 2014     Jai Shankar Pandey      added test class to cover checkNewUser() method of CAKE_HomeController
@isTest
private class OCSUGC_HomeControllerTest {
     static User managerUsr,contributorUsr,memberUsr;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup,unlistedGroup;
   static CollaborationGroupMember groupMember1,groupMember2;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static List<CollaborationGroup> lstGroupInsert;
   static Profile admin,portal,manager;
   static OCSUGC_Knowledge_Exchange__c knowledgeFile;
   static Network ocsugcNetwork;
   static {
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'testuser318', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'testuser319', '1'));
     
     adminUsr1.UserRoleID = OCSUGC_TestUtility.getRole().id;
     adminUsr2.UserRoleID = OCSUGC_TestUtility.getRole().id;
     insert lstUserInsert;
     System.runas(adminUsr2) {
        account = OCSUGC_TestUtility.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert;
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member));
        insert lstUserInsert;
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1' ,'Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.Id,false));
        lstGroupInsert.add(unlistedGroup = OCSUGC_TestUtility.createGroup('test unlisted group 12','Unlisted',ocsugcNetwork.Id,false));
        insert lstGroupInsert;
        lstGroupMemberInsert = new List<CollaborationGroupMember>();
        lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
        insert lstGroupMemberInsert;
     }
     
     
     System.runAs(adminUsr2) {
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, adminUsr1.Id, true);
     }

     System.runAs(adminUsr1) {
        OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent','Events',null,'Team','Test');
          testContent.OCSUGC_Bookmark_Id__c = 'hello test';
          testContent.OCSUGC_Application_Grant_Access__c = true;
          testContent.OCSUGC_Status__c = 'Approved';
          testContent.OCSUGC_Posted_By__c = managerUsr.Id;
          testContent.RecordTypeId = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Events').getRecordTypeId();
          testContent.OCSUGC_Group__c = 'Everyone';
          testContent.OCSUGC_Is_Deleted__c = false;
          testContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
          testContent.OCSUGC_NetworkId__c = null;
          testContent.OCSUGC_GroupId__c = 'Everyone';
          testContent.OCSUGC_Location__c = 'test summary test summary test summary test summary test summary test summary test summary ';
          insert testContent;


        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('TestTag');
            insert tag;

                OCSUGC_Intranet_Content_Tags__c contentTag = OCSUGC_TestUtility.createIntranetContentTags(testContent.Id, tag.Id);
                insert contentTag;

                OCSUGC_Knowledge_Exchange__c knowExchange = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,false);
                knowExchange.OCSUGC_Status__c = 'Approved';
                knowExchange.OCSUGC_Group_Id__c = 'Everyone';
                insert knowExchange;

                OCSUGC_Knowledge_Tag__c kTag = OCSUGC_TestUtility.createKnowledgeTag(knowExchange.Id, false);
        kTag.OCSUGC_Tags__c = tag.Id;
        insert kTag;

        FeedItem feedItem = OCSUGC_TestUtility.createFeedItem(testContent.Id, true);

        OCSUGC_Poll_Details__c pollDetail = OCSUGC_TestUtility.createPollDetail(publicGroup.Id, 'TestPublic', feedItem.Id, 'Test', false);
        pollDetail.OCSUGC_Comments__c = 5;
        pollDetail.OCSUGC_Is_FGAB_Poll__c = true;
        pollDetail.OCSUGC_Group_Id__c = 'Everyone';
        pollDetail.OCSUGC_Views__c = 4;
        pollDetail.OCSUGC_Likes__c = 5;
        insert pollDetail;

        OCSUGC_PollDetail_Tags__c pollTag = OCSUGC_TestUtility.createPollDetailTag(tag.Id, feedItem.Id, true);

        OCSUGC_DiscussionDetail__c discussion = OCSUGC_TestUtility.createDiscussion(null,feedItem.Id, false);
        discussion.OCSUGC_Comments__c = 5;
        discussion.OCSUGC_Description__c = 'Test Discussion';
        discussion.OCSUGC_Group__c = 'TestPublic';
        discussion.OCSUGC_GroupId__c = 'Everyone';
        discussion.OCSUGC_DiscussionId__c = feedItem.Id;
        discussion.OCSUGC_Views__c = 4;
        discussion.OCSUGC_Likes__c = 5;
        insert discussion;

        OCSUGC_FeedItem_Tag__c feedItemTag = OCSUGC_TestUtility.createFeedItemTag(tag.Id, feedItem.Id, true);

     }
   }

    static testMethod void testHomeController() {

        // TO DO: implement unit test

        Test.startTest();

        System.runAs(managerUsr){
            OCSUGC_HomeController controller =  new OCSUGC_HomeController();
            
            List<Id> fixedSearchResults = OCSUGC_HomeControllerTest.testKnowledge_ContentData(); 
            Test.setFixedSearchResults(fixedSearchResults);
            
            controller.checkNewUser();
            controller.isFGAB = true;
            List<String> tagList = new List<String>();
            tagList.add('TestTag');
            tagList.add('All');
            controller.selectedTags = tagList;

            Set<String> tags = new Set<String>();
            tags.add('TestTag');
            tags.add('All');
            controller.setTagFilters = tags;
            tagList = new List<String>();
            tagList.add('Everyone');
            tagList.add('All');
            controller.selectedGroups = tagList;
            tags = new Set<String>();
            tags.add('Everyone');
            tags.add('All');

            System.assert(controller.getTags() <> null);
            System.assert(controller.getGroups() <> null);
            
            controller.setGroupFilters = tags;
            controller.changeView();
            controller.doFilter();

            System.assert(controller.lstFinalWrapperTiles <> null);

            controller.searchString = null;
            controller.checkNewUser();
            controller.doFilter();
            controller.manageBookmark();
            controller.getVisibleNext();
            controller.getVisiblePrevious();
            controller.goFirst();
            controller.goNext();
            controller.goPrevious();
            controller.goLast();
            controller.setPage();
            System.assert(controller.listOfLinks <> null);
            controller.refreshFilter();
            controller.refresh();
            controller.clearAllContents();

            System.assert(controller.selectedTags <> null);
            System.assert(controller.selectedGroups <> null);
        }
    }
    
    public static List<Id> testKnowledge_ContentData() {
        OCSUGC_Knowledge_Exchange__c exchange = new OCSUGC_Knowledge_Exchange__c();
        exchange.OCSUGC_Status__c = 'Approved';
        exchange.OCSUGC_Group_Id__c = 'Everyone';
        insert exchange;
        
        RecordType recType = [Select id, Name from RecordType where sObjectType = 'OCSUGC_Intranet_Content__c' AND Name = 'Events'];
        OCSUGC_Intranet_Content__c content = new OCSUGC_Intranet_Content__c();
        content.RecordTypeId = recType.Id;
        content.OCSUGC_GroupId__c = 'Everyone';
        insert content;
        
        List<Id> fixedSearchResults= new List<Id>();
        fixedSearchResults.add(exchange.Id);
        fixedSearchResults.add(content.Id);
        return fixedSearchResults;
    }
    
    static testMethod void testHomeController2() {

        // TO DO: implement unit test

        Test.startTest();
        System.runAs(memberUsr){
            
            PageReference pg = Page.OCSUGC_Home;
            pg.getParameters().put('fgab','false');
            pg.getParameters().put('isResetPwd','true');
            Test.setCurrentPage(pg);
            OCSUGC_HomeController controller =  new OCSUGC_HomeController();
            controller.searchString = 'test';
            // Changes by Puneet Mishra, 15 Oct 2015, Fix is been made to handled System.UnexpectedException occuring due to SOSL used in Apex classes
            List<Id> fixedSearchResults = OCSUGC_HomeControllerTest.testKnowledge_ContentData(); 
            Test.setFixedSearchResults(fixedSearchResults);
            
            controller.doFilter();
            controller.getItems();
            controller.changeView();
            controller.sortBy='most-liked';
            controller.doFilter();
            /*controller.sortBy='most-viewed';
            controller.doFilter();
            controller.sortBy='most-discussed';
            controller.doFilter();*/
        }
       Test.stopTest();
    }
    
    static testMethod void testHomeController_MostViewed() {
        Test.startTest();
            PageReference pg = Page.OCSUGC_Home;
            pg.getParameters().put('fgab','false');
            pg.getParameters().put('isResetPwd','true');
            Test.setCurrentPage(pg);
            OCSUGC_HomeController controller =  new OCSUGC_HomeController();
            controller.searchString = 'test';
            controller.sortBy='most-viewed';
            
            controller.doFilter();
        Test.stopTest();
    }
    
    static testMethod void testHomeController_MostDiscussed() {
        Test.startTest();
            PageReference pg = Page.OCSUGC_Home;
            pg.getParameters().put('fgab','false');
            pg.getParameters().put('isResetPwd','true');
            Test.setCurrentPage(pg);
            OCSUGC_HomeController controller =  new OCSUGC_HomeController();
            controller.searchString = 'test';
            controller.sortBy='most-discussed';
            
            controller.doFilter();
        Test.stopTest();
    }

    static testMethod void testHomeController3() {

        // TO DO: implement unit test
        Test.startTest();
        System.runAs(memberUsr){
            PageReference pg = Page.OCSUGC_Home;
            pg.getParameters().put('fgab','true');
            pg.getParameters().put('isResetPwd','true');
            pg.getParameters().put('g',unlistedGroup.Id );
            Test.setCurrentPage(pg);
            OCSUGC_HomeController controller =  new OCSUGC_HomeController();
            controller.isFGAB = true;
            //Case 2 for fgab
            // Changes by Puneet Mishra, 15 Oct 2015, Fix is been made to handled System.UnexpectedException occuring due to SOSL used in Apex classes
            List<Id> fixedSearchResults = OCSUGC_HomeControllerTest.testKnowledge_ContentData(); 
            Test.setFixedSearchResults(fixedSearchResults);
            
            controller.searchString = 'test';
            controller.doFilter();
        }
       Test.stopTest();
    }
}
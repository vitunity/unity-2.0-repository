/*
Name        : OCSUGC_KnowledgeArtifactCreationCon
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 4th June, 2014
Purpose     :

Refrence    : T-284647
Dated By    Name                Description                             Task ID
01-10-2014  Naresh K Shiwani    Updated Upload Attachment Condition     T-323468
                                on attachment file size.
22 10 2014  Naresh K Shiwani    Modified getAllUserGroups() added size  T-328778
                                condition lstPrivateGroup.size(),
                                lstPublicGroup.size() for Adding Select Option.
03 10 2014  Naresh K Shiwani    Modified updateDiscussion() inserted    T-330157
                                OCSUGC_Everyone_Within_OncoPeer label value
                                in  OCSUGC_Group__c in case user selects everyone in oncopeer as share with option.
12 Dec 2014    Neeraj Sharma    Modified Ref: T-340219 verify content for blacklisted words
19 Jan 2016    Shital Bhujbal   Modified code Ref : ONCO-454,changed updateKnowledge code.
*/
global without sharing class OCSUGC_KnowledgeArtifactCreationCon extends OCSUGC_Base {
    public String categoryType {get; set;}
    public Integer taglimit { get; set; }
    public OCSUGC_Knowledge_Exchange__c knowledgeExchange {get; set;}
    public Attachment attachment{get;set;}
    public String errorMsg{get;set;}
    public String selectedArtifactType {get; set;}
    public String selectedExtensions {get; set;}
    public String commmaSeperatedExtensions {get; set;}
    public String selectedKASupportedFileSizeLabel {get; set;}
    public boolean isFileTypeExist {get; set;}
    public boolean isFileFormatNotSupported {get; set;}
    public String selectedGroup {get; set;}
    public String selectedTag {get; set;}
    public List<wrapperTag> wrapperTags                     {get; set;}
    public List<CollaborationGroupMember> lstPrivateGroup   {get; set;}
    public List<CollaborationGroupMember> lstPublicGroup    {get; set;}
    public String selectedPicklistValue {get; set;}
    public String selectedOldPicklistValue {get; set;}
    public String commaSeperatedPicklistValues {get; set;}
    public boolean requiresManagerApproval {get; set;}
    Map<String, String> mapGroups;
    Set<String> setPicklistValues;
    public string[] selectedInputTagString;
    // ONCO-418, accessing the file size on visualforce page
    public String JsonMap{get;set;}
    
    public OCSUGC_KnowledgeArtifactCreationCon() {
        Map<String, Integer> contentMap = new Map<String, Integer>();
        for(OCSUGC_KFiles_ApprovedFileType__c KFilestype : OCSUGC_KFiles_ApprovedFileType__c.getAll().values()){
            contentMap.put(KFilestype.Name, Integer.valueOf(KFilestype.OCSUGC_File_Size__c));
            system.debug('*********contentMap****');
        }
        JsonMap = JSON.serialize(contentMap);
        
        taglimit = Integer.valueOf(System.Label.OCSUGC_No_of_Tag_Limit);
        knowledgeExchange = new OCSUGC_Knowledge_Exchange__c();
        knowledgeExchange.OCSUGC_Artifact_Type__c = 'Select a File Type';
        knowledgeExchange.OCSUGC_NetworkId__c = networkId;
        setPicklistValues = new Set<String>();
        selectedInputTagString = new string[]{};
        attachment = new Attachment();
        fetchGroup();

        uploadStatus = '';
        String knowId = ApexPages.currentPage().getParameters().get('id');
        System.debug('knowId========= '+knowId);
        if(knowId!=null){
            fetchArtifactInfo(knowId);
            getTags();
            fetchExtensions();
            for(FeedItem fes : [Select ContentFileName,ContentData From FeedItem Where parentid =:knowId]){
                attachment.Name = fes.ContentFileName;
            }
            getAllUserGroups();
            System.debug('attachment.Name===='+attachment.Name+'knowId==='+knowId);
            System.debug('knowledgeExchange===='+knowledgeExchange);
        } else{
            getTags();
        }
    }

    //uploading the attachment and saving to Knowledge exchange
    public String statusSaved {get; set;}
    public String uploadStatus {get; set;}

    public Pagereference uploadAttachment(){
        try {
            uploadStatus = '';
            System.debug('==================knowledgeExchange: '+knowledgeExchange);
            //ref:T-340219 Verifying description content for blacklisted content
            list<String> contentList = new list<String>();
            if(knowledgeExchange.OCSUGC_Summary__c != null) {
                contentList.add(knowledgeExchange.OCSUGC_Summary__c);
            }
            if(knowledgeExchange.OCSUGC_Title__c!= null) {
                contentList.add(knowledgeExchange.OCSUGC_Title__c);
            }
            /*if(contentList.size() > 0) {
                if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                        if(knowledgeExchange.Id != null) {
                                attachment.Body = null;
                        } else {
                                attachment = new Attachment();
                        }
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                    System.debug('==================Error BlackList==');
                    return null;
                }
            }*/
            
            
            knowledgeExchange.OCSUGC_Status__c = 'Approved';


            OCSUGC_KFiles_ApprovedFileType__c content = OCSUGC_KFiles_ApprovedFileType__c.getValues(knowledgeExchange.OCSUGC_Artifact_Type__c);
            
            if(content != null) {
                if(content.OCSUGC_Requires_Admin_Approval__c) {
                    knowledgeExchange.OCSUGC_Status__c = 'Pending Approval';
                }
            }

            if(selectedGroup != null){
                knowledgeExchange.OCSUGC_Group__c = selectedGroup != 'Everyone' ? mapGroups.get(selectedGroup) : System.Label.OCSUGC_Everyone_Within_OncoPeer;
                knowledgeExchange.OCSUGC_Group_Id__c = selectedGroup;
            }

            System.debug('uploadAttachment knowledgeExchange=========='+knowledgeExchange);
            if(knowledgeExchange.id!=null){
                update knowledgeExchange;
                addTags();
                OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,knowledgeExchange.OCSUGC_Title__c, selectedGroup, 'has updated', 'OCSUGC_UploadKnowledgeFile?Id='+knowledgeExchange.ID);
            }
            else{
                insert knowledgeExchange;
                addTags();
                OCSUGC_Utilities.insertNotificationRecordForFollowers(networkName,knowledgeExchange.OCSUGC_Title__c, selectedGroup, 'has created', 'OCSUGC_UploadKnowledgeFile?Id='+knowledgeExchange.ID);
            }

            statusSaved = knowledgeExchange.OCSUGC_Status__c;
            /*Pagereference pg = Page.OCSUGC_KnowledgeArtifactDetail;
            pg.getParameters().put('knowledgeArtifactId',knowledgeExchange.Id);*/
            Pagereference pg = Page.OCSUGC_UploadKnowledgeFile;
            pg.getParameters().put('Id',knowledgeExchange.Id);
            pg.getParameters().put('isCreated','true');
            pg.setRedirect(true);
            deleteFeedItemNContentDoc(knowledgeExchange.Id);
            
            return pg;
        } catch(Exception e) {
                    //attachment.Body = null;
            system.debug('================Error: '+e.getMessage());
            return null;
        }
    }

    public Pagereference fetchExtensions(){
        uploadStatus = '';
        selectedExtensions = '';
        commmaSeperatedExtensions = '';
        if(knowledgeExchange.OCSUGC_Artifact_Type__c != null){
                if(knowledgeExchange.OCSUGC_Artifact_Type__c == 'Document' 
                        || knowledgeExchange.OCSUGC_Artifact_Type__c == 'Video'){
                                requiresManagerApproval = true;
                        }
                else
                        requiresManagerApproval = false;
                OCSUGC_KFiles_ApprovedFileType__c content = OCSUGC_KFiles_ApprovedFileType__c.getValues(knowledgeExchange.OCSUGC_Artifact_Type__c);
            if(content != null){
                commmaSeperatedExtensions += content.OCSUGC_File_Extension__c.toUpperCase() + ',';
                selectedKASupportedFileSizeLabel = (Label.OCSUGC_Maximum_File_Size).replace('FILE_SIZE',''+Integer.valueOf(content.OCSUGC_File_Size__c)/1024);// 'Maximum file size is '+ Integer.valueOf(content.OCSUGC_File_Size__c)/1024 + ' MB.';
                // Modified by      Puneet Mishra, 7 Sept 2015
                //[ONCO-362]        Message displaying maximum file size to upload
                //selectedKASupportedFileSizeLabel = (Label.OCSUGC_Maximum_File_Size).replace('FILE_SIZE',''+Integer.valueOf(2.5*content.OCSUGC_File_Size__c)/1024);// 'Maximum file size is 25 MB.';
            }
            if(commmaSeperatedExtensions.contains(',') &&
                commmaSeperatedExtensions.subString(commmaSeperatedExtensions.length() - 1, commmaSeperatedExtensions.length()).contains(',')){
                commmaSeperatedExtensions = commmaSeperatedExtensions.subString(0, commmaSeperatedExtensions.length() - 1);
            }
        }
        if(commmaSeperatedExtensions == ''){
            selectedExtensions = Label.OCSUGC_File_Type_Not_Allowed;
            isFileTypeExist = false;
            isFileFormatNotSupported = true;
        }else{
            selectedExtensions = Label.OCSUGC_Allowed_File_Types_Message+ '' + commmaSeperatedExtensions;
            isFileTypeExist = true;
            isFileFormatNotSupported = false;
        }
        prePopulateTags();
        return null;
    }

    //prepopulating tags on the basis of selected picklists
    public void prePopulateTags(){
        Integer countRow = 0;
        List<Integer> deleteRows = new List<Integer>();
        for(wrapperTag wrap : wrapperTags){
            if(wrap.isPicklistValue == true){
                deleteRows.add(countRow);
            }
            countRow++;
        }
        if(deleteRows.size() > 0){
            Integer tempRow = 0;
            for(Integer rowCount : deleteRows){
                wrapperTags.remove(rowCount - tempRow);
                tempRow++;
            }
        }
        if(knowledgeExchange.id==null){
            wrapperTags.add(new wrapperTag(true, knowledgeExchange.OCSUGC_Artifact_Type__c, true, false));
        }
    }

    public Pagereference updatePrePopulateTagsForVisibility(){
        return null;
    }
    public Pagereference updatePrePopulateTags(){

        if(selectedPicklistValue != null){
                if(!setPicklistValues.contains(selectedPicklistValue.toUpperCase())){
                wrapperTags.add(new wrapperTag(true, selectedPicklistValue, true, false));
            }
            setPicklistValues.add(selectedPicklistValue.toUpperCase());
        }
        //removing previous value of corresponding picklist
        if(selectedOldPicklistValue != null && setPicklistValues.contains(selectedOldPicklistValue.toUpperCase())){
                if((knowledgeExchange.OCSUGC_Imaging_Only__c != null 
                                && knowledgeExchange.OCSUGC_Intra_Frx_Imaging__c != null)
                        && (knowledgeExchange.OCSUGC_Imaging_Only__c == selectedOldPicklistValue
                                || knowledgeExchange.OCSUGC_Intra_Frx_Imaging__c == selectedOldPicklistValue)  
                  ){
                        return null;
                  }
            setPicklistValues.remove(selectedOldPicklistValue.toUpperCase());
            Integer countRow = 0;
            Integer rowToDelete;
            for(wrapperTag wrap : wrapperTags){
                if(wrap.isPicklistValue){
                    if(wrap.tagName == selectedOldPicklistValue){
                        rowToDelete = countRow;
                    }
                }
                countRow++;
            }
            if(rowToDelete != null){
                wrapperTags.remove(rowToDelete);
            }
        }
        return null; 
    }

    public Pagereference updateWrapperForMultiPicklistValues(){
        Integer countRow = 0;
        List<Integer> deleteRows = new List<Integer>();
        for(wrapperTag wrap : wrapperTags){
            if(wrap.isMultiPicklistValue == true){
                deleteRows.add(countRow);
            }
            countRow++;
        }
        if(deleteRows.size() > 0){
            Integer tempRow = 0;
            for(Integer rowCount : deleteRows){
                wrapperTags.remove(rowCount - tempRow);
                tempRow++;
            }
        }
        if(commaSeperatedPicklistValues != null){
            for(String str  : commaSeperatedPicklistValues.split(',')){
                wrapperTags.add(new wrapperTag(true, str, true, true));
            }
        }
        return null;
    }

    public List<SelectOption> getAllUserGroups(){
        mapGroups = new Map<String, String>();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Select Group'));
        options.add(new SelectOption('Everyone', System.Label.OCSUGC_Everyone_Within_OncoPeer));
        SelectOption opt;
        if(lstPrivateGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Closed Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPrivateGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
                 mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
            }
        }
        if(lstPublicGroup.size() > 0){
            opt = new SelectOption('None', '<optgroup label="Open Groups"></optgroup>');
            opt.setEscapeItem(false);
            options.add(opt);
            for(CollaborationGroupMember grp :lstPublicGroup) {
                 options.add(new SelectOption(grp.CollaborationGroupId, grp.CollaborationGroup.Name));
                 mapGroups.put(grp.CollaborationGroupId, grp.CollaborationGroup.Name);
            }
        }
        return options;

    }


    private void fetchGroup() {
        lstPrivateGroup = new List<CollaborationGroupMember>();
        lstPublicGroup = new List<CollaborationGroupMember>();
        set<Id> groupIds = new set<Id>();

        if(isManagerOrContributor) {
                for(CollaborationGroupMember grp :[Select Id,
                                                  CollaborationGroup.Name,
                                                  CollaborationGroupId,
                                                  CollaborationGroup.CollaborationType
                                          From CollaborationGroupMember
                                          Where CollaborationGroup.IsArchived = false
                                          And CollaborationGroup.NetworkId =: networkId
                                          order by CollaborationGroup.Name]) {

                    if(!groupIds.contains(grp.CollaborationGroupId)) {
                        groupIds.add(grp.CollaborationGroupId);
                        if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
                        if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
                    }
                }
        } else {
                for(CollaborationGroupMember grp :[Select Id,
                                                  CollaborationGroup.Name,
                                                  CollaborationGroupId,
                                                  CollaborationGroup.CollaborationType
                                          From CollaborationGroupMember
                                          Where CollaborationGroup.IsArchived = false
                                          And CollaborationGroup.NetworkId =: networkId
                                          And (MemberId =:userInfo.getUserId()
                                          OR CollaborationGroup.OwnerId =:userInfo.getUserId())
                                          order by CollaborationGroup.Name]) {

                    if(!groupIds.contains(grp.CollaborationGroupId)) {
                        groupIds.add(grp.CollaborationGroupId);
                        if(grp.CollaborationGroup.CollaborationType.equals('Private')) lstPrivateGroup.add(grp);
                        if(grp.CollaborationGroup.CollaborationType.equals('Public')) lstPublicGroup.add(grp);
                    }
                }
        }
    }


    public PageReference addTag() {
        /*if(selectedTag != null && selectedTag.trim() != ''){
                selectedTag = selectedTag.trim();
                if(!setPicklistValues.contains(selectedTag.toUpperCase())){
                        setPicklistValues.add(selectedTag.toUpperCase());
            list<String> contentList = new list<String>();
            contentList.add(selectedTag);
            if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                System.debug('==================Error BlackList==Tags'+contentList);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                selectedTag = '';
                return null;
            }
            wrapperTags.add(new wrapperTag(true, selectedTag, false, false));
            selectedTag = null;
        }
        }*/
        
        //ONCO-402, Ability to Add multiple tags at the same time
        set<string> selectedTagSet = new set<string>();
        
        if(selectedTag != null && selectedTag.trim() != ''){
            selectedTag = selectedTag.trim();
            selectedTagSet.add(selectedTag);    
        }
        
        for(string sltTag :selectedTagSet){
             selectedInputTagString = sltTag.split(',');
        }
        
        
        for(String tagValue : selectedInputTagString){
            if(!setPicklistValues.contains((tagValue.trim()).toUpperCase())){
                setPicklistValues.add((tagValue.trim()).toUpperCase());
                list<String> contentList = new list<String>();
                contentList.add(tagValue.trim());
                if(!OCSUGC_Utilities.verifyContentForBlockedWords(contentList)) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_ContentBlackListedErrorMessage));
                    return null;
                }
                wrapperTags.add(new wrapperTag(true, tagValue.trim(), false, false));
                tagValue = null;
            }   
        }
        
        return null;
    }

    public PageReference updateTag() {
        if((knowledgeExchange.OCSUGC_Imaging_Only__c != null 
                                && knowledgeExchange.OCSUGC_Intra_Frx_Imaging__c != null)
                        && (knowledgeExchange.OCSUGC_Imaging_Only__c == selectedTag
                                && knowledgeExchange.OCSUGC_Intra_Frx_Imaging__c == selectedTag)  
                        ){
                        return null;
                }
        for(Integer cnt=0; wrapperTags.size()>cnt; cnt++) {
            if(wrapperTags[cnt].tagName.equals(selectedTag)) {
                wrapperTags.remove(cnt);
                setPicklistValues.remove(selectedTag.toUpperCase());
            }
        }
        selectedTag = null;
        return null;
    }

    public void getTags() {
        wrapperTags = new List<wrapperTag>();
            if(String.isNotBlank(knowledgeExchange.Id)){
                for(OCSUGC_Knowledge_Tag__c tag : [SELECT Id, OCSUGC_Tags__r.OCSUGC_Tag__c, OCSUGC_Tags__r.Id
                                                   FROM OCSUGC_Knowledge_Tag__c
                                                   WHERE OCSUGC_knowledge_exchange__c = : knowledgeExchange.Id order by OCSUGC_Tags__r.OCSUGC_Tag__c]){
                wrapperTags.add(new wrapperTag(true, tag.OCSUGC_Tags__r.OCSUGC_Tag__c, false, false));
                setPicklistValues.add(tag.OCSUGC_Tags__r.OCSUGC_Tag__c.toUpperCase());
            }
        }
        System.debug('wrapperTags Get tags==='+wrapperTags);
    }

    private void addTags() {
        List<OCSUGC_Knowledge_Tag__c> lstFeedItemTags = new List<OCSUGC_Knowledge_Tag__c>();

        for(OCSUGC_Knowledge_Tag__c tag:  [SELECT Id FROM OCSUGC_Knowledge_Tag__c WHERE OCSUGC_knowledge_exchange__c = : knowledgeExchange.Id]) {
            lstFeedItemTags.add(tag);
        }
        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()) {
            Database.delete(lstFeedItemTags);
        }

        List<OCSUGC_Tags__c> lstTags = new List<OCSUGC_Tags__c>();
        lstFeedItemTags = new List<OCSUGC_Knowledge_Tag__c>();

        System.debug('wrapperTags addTags===='+wrapperTags);
        System.debug('mapTags========'+mapTags);
        for(wrapperTag wrapper : wrapperTags) {
            if(wrapper.isActive) {
                if(String.isNotBlank(wrapper.tagName)) {
                    OCSUGC_Tags__c newTag = new OCSUGC_Tags__c(OCSUGC_Tag__c = wrapper.tagName);
                    if(mapTags.containsKey(wrapper.tagName.toLowerCase())) {
                        newTag.Id = mapTags.get(wrapper.tagName.toLowerCase());
                    }
                    if(isFGAB) {
                        newTag.OCSUGC_IsFGABUsed__c = true;
                    } else if(isDC) {
                        newTag.OCSDC_IsDeveloperCloudUsed__c = true;
                    } else {
                        newTag.OCSUGC_IsOncoPeerUsed__c = true;
                    }
                    lstTags.add(newTag);
                }
            }
        }
        System.debug('lstTags===='+lstTags);
        System.debug('lstFeedItemTags==='+lstFeedItemTags);
        if(lstTags != null && !lstTags.isEmpty()) {
            Database.upsert(lstTags);
        }

        //Insert new tags
        for(OCSUGC_Tags__c tg : lstTags) {
            lstFeedItemTags.add(new OCSUGC_Knowledge_Tag__c(OCSUGC_Tags__c = tg.Id, OCSUGC_knowledge_exchange__c = knowledgeExchange.Id));
        }

        if(lstFeedItemTags != null && !lstFeedItemTags.isEmpty()){
            Database.insert(lstFeedItemTags);
        }
        System.debug('lstFeedItemTags===='+lstFeedItemTags);
    }

    //Function to generate map of Tags and its Id
    public static Map<String, Id> mapTags {
        get{
            if(mapTags == null){
                mapTags = new Map<String, Id>();
                for(OCSUGC_Tags__c tg : [SELECT Id, OCSUGC_Tag__c FROM OCSUGC_Tags__c]){
                    mapTags.put(tg.OCSUGC_Tag__c.toLowerCase(), tg.Id);
                }
            }
            return mapTags;
        }
        private set;
    }

    public class wrapperTag {
        public Boolean isActive {get;set;}
        public String tagName   {get;set;}
        public boolean isPicklistValue {get;set;}
        public boolean isMultiPicklistValue {get;set;}

        public wrapperTag(Boolean isAct, String name, boolean isPicklistValue, boolean isMultiPicklistValue) {
            isActive = isAct;
            tagName = name;
            this.isPicklistValue = isPicklistValue;
            this.isMultiPicklistValue = isMultiPicklistValue;
        }
    }
    
    //fetch artifact info
    private void fetchArtifactInfo(String knowledgeArtifactId){
        for(OCSUGC_Knowledge_Exchange__c exchange : [Select ownerId, Id, OCSUGC_Views__c, OCSUGC_Artifact_Type__c, createdDate, OCSUGC_Title__c, CreatedById,OCSUGC_Is_Deleted__c,OCSUGC_Content_Sensitivity__c,
                                                OCSUGC_Anatomical_Sites__c,OCSUGC_Target_Dose_Level__c,OCSUGC_QA_test__c,OCSUGC_TrueBeam_version__c,
                                                OCSUGC_Exotic_Trajectory__c, OCSUGC_Imaging_Only__c, OCSUGC_Dynamic_Tracking__c, OCSUGC_Intra_Frx_Imaging__c,
                                                OCSUGC_Gating__c, OCSUGC_Report_Type__c, OCSUGC_Compatibility__c, OCSUGC_Dashboard_Type__c, OCSUGC_Group__c, OCSUGC_Group_Id__c,
                                                OCSUGC_Summary__c, OCSUGC_NetworkId__c, OCSUGC_Status__c from OCSUGC_Knowledge_Exchange__c where id =: knowledgeArtifactId]){
            knowledgeExchange = exchange;
            selectedGroup = knowledgeExchange.OCSUGC_Group_Id__c;
        }
    }
    
     /**
     *  Created By : Shital Bhujbal     19/1/2016   Ref : ONCO-454
     *  Method will delete the feed records,and contentDocuments related to the same feedItem.
     */
    public static void deleteFeedItemNContentDoc(Id knowledgeArtifactId) {
        List<FeedItem> feedItemList = new List<FeedItem>(); // FeedItem List queried using discussionId 
        List<ContentDocument> contentDocList = new List<ContentDocument>();
        List<Id> feedItemIdList = new List<Id>();           // content Id of FeedItem whose related ContentDocument need to get deleted
        List<Id> contentVerId = new List<Id>();             // Id of ContentDocument from ContentVersion Records
        List<Id> contentDocId = new List<Id>();
        feedItemList = [SELECT Id, ParentId, Title, CreatedDate, RelatedRecordId FROM FeedItem WHERE ParentId =: knowledgeArtifactId ORDER BY CreatedDate DESC NULLS LAST];
        
        if(feedItemList.size() > 1) {// removing First Element from List as First Element is the lastest one.
            feedItemList.remove(0);
        } else {// returning if value fails, not returning will cause other statements to execute which is not required and can run into "Time Exceeded" limit
            return;
        }
        
        for(FeedItem fd : feedItemList) {
            contentVerId.add(fd.RelatedRecordId);
        }
        for(ContentVersion ver : [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN : contentVerId]) {
            contentDocId.add(ver.ContentDocumentId);
        }
        List<ContentDocument> contentDoc = new List<ContentDocument>(); 
        if(!contentDocId.isEmpty())
            contentDoc = [SELECT Id, Title FROM ContentDocument WHERE Id IN : contentDocId];
            
        if(!contentDoc.isEmpty()) {
            delete feedItemList;
            delete contentDoc;
        }
    }
}
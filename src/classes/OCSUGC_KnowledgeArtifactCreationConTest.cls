/*
Name        : OCSUGC_KnowledgeArtifactCreationConTest
Updated By  : Puneet Sardana - Appirio India
Date        : 06 Feb, 2014
Purpose     : Test Class for
                - OCSUGC_KnowledgeArtifactCreationCon.cls
*/
@isTest(SeeAllData=false)
public class OCSUGC_KnowledgeArtifactCreationConTest {
   
   static Profile admin,portal;
  
   static User adminUsr1,adminUsr2, memberUsr;
   static Account account; 
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1;
   static Network ocsugcNetwork;
   static List<CollaborationGroup> lstGroupInsert;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static OCSUGC_Knowledge_Exchange__c ka1;
   static List<OCSUGC_Knowledge_Exchange__c> lstKAInsert;
   static OCSUGC_KFiles_ApprovedFileType__c kaTypes;   
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile(); 
     OCSUGC_TestUtility.createBlackListWords('p test blacklist');
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert; 
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
     lstUserInsert.add( memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
     insert lstUserInsert;     
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
     insert lstGroupInsert;
     PermissionSet memberPermSet = OCSUGC_TestUtility.getMemberPermissionSet();
    
    System.runAs(adminUsr1) {
      PermissionSetAssignment memberPermSetAssignment = OCSUGC_TestUtility.createPermissionSetAssignment(memberPermSet.Id, memberUsr.id, true);
      
      lstGroupMemberInsert = new List<CollaborationGroupMember>();
      lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
      insert lstGroupMemberInsert;
      kaTypes = OCSUGC_TestUtility.createKAFileTypes(true);               
     }      
   }
  
    public static testmethod void test_OCSUGC_KnowledgeArtifactCreationConTest() {
      
      Test.startTest();       
      ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);  
      PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
      ref.getParameters().put('id',null);
      Test.setCurrentPage(ref);  
      OCSUGC_KnowledgeArtifactCreationCon kaController = new OCSUGC_KnowledgeArtifactCreationCon();
      kaController.knowledgeExchange.OCSUGC_Artifact_Type__c = 'Document';
      kaController.fetchExtensions();
      kaController.getAllUserGroups();
      kaController.selectedGroup = publicGroup.id;
      kaController.attachment.Name = 'test.doc'; 
      kaController.attachment.ContentType ='text/doc';        
      kaController.attachment.Body = Blob.valueOf('test puneet'); 
      kaController.knowledgeExchange.OCSUGC_Title__c = 'test title';
      kaController.knowledgeExchange.OCSUGC_Summary__c = 'test summary';
      kaController.selectedGroup = publicGroup.Id;
      kaController.selectedTag = 'test tag 1';      
      kaController.uploadAttachment();
//      List<OCSUGC_Knowledge_Exchange__c> lstKw=[SELECT Id
//                                                FROM OCSUGC_Knowledge_Exchange__c];
//      System.assertEquals(lstkw.size(),1);
      System.assert(kaController.knowledgeExchange.Id!=null);
      kaController.selectedTag = 'test tag 2';
      kaController.addTag();
      kaController.selectedTag = 'test tag 3';
      kaController.selectedPicklistValue = 'tag';
      kaController.selectedOldPicklistValue = 'tag';
      kaController.updateTag();  
      kaController.updatePrePopulateTags();  
      kaController.updatePrePopulateTagsForVisibility();  
      kaController.updateWrapperForMultiPicklistValues();
      
      Test.stopTest();  
    }
    
     public static testmethod void test_Edit_OCSUGC_KnowledgeArtifactCreationConTest() {
      Test.startTest();
      ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);         
      PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
      ref.getParameters().put('id',ka1.Id);
      Test.setCurrentPage(ref);
      OCSUGC_KnowledgeArtifactCreationCon kaController = new OCSUGC_KnowledgeArtifactCreationCon();
      kaController.knowledgeExchange.OCSUGC_Artifact_Type__c = 'Document';
      kaController.attachment.Name = 'test.pdf'; 
      kaController.attachment.ContentType ='text/pdf';        
      kaController.attachment.Body = Blob.valueOf('test puneet'); 
      kaController.knowledgeExchange.OCSUGC_Title__c = 'p test blacklist';
      kaController.knowledgeExchange.OCSUGC_Summary__c = 'p test blacklist';
      kaController.selectedGroup = publicGroup.Id;
      kaController.selectedTag = 'test tag 1';      
      kaController.uploadAttachment();
      System.assert(kaController.knowledgeExchange.Id!=null);
      kaController.selectedTag = 'test tag 2';
      kaController.addTag();
      kaController.selectedTag = 'test tag 3';
      kaController.selectedPicklistValue = 'tag';
      kaController.selectedOldPicklistValue = 'tag';
      kaController.updateTag();  
      kaController.updatePrePopulateTags();  
      kaController.updatePrePopulateTagsForVisibility();  
      kaController.updateWrapperForMultiPicklistValues();
      //kaController.updateKnowledge();
      Test.stopTest();  
    }

  
    public static testmethod void test_OCSUGC_KnowledgeArtifactCreationConTest1() {
      Test.startTest();
              
      PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
      ref.getParameters().put('id',null);
      Test.setCurrentPage(ref);
      OCSUGC_KnowledgeArtifactCreationCon kaController = new OCSUGC_KnowledgeArtifactCreationCon();
      kaController.attachment.Name = ''; 
      kaController.attachment.ContentType ='text/pdf';        
      kaController.attachment.Body = null; 
      kaController.selectedGroup = publicGroup.Id;
      kaController.selectedTag = 'test tag 1';      
      kaController.uploadAttachment();
      kaController.selectedTag = 'test tag 2';
      kaController.addTag();
      kaController.selectedTag = 'test tag 3';
      kaController.selectedPicklistValue = 'tag';
      kaController.selectedOldPicklistValue = 'tag';
      kaController.updatePrePopulateTagsForVisibility();  
      kaController.updateWrapperForMultiPicklistValues();
      Test.stopTest();  
    }
    
    public static testmethod void test_OCSUGC_KnowledgeArtifactCreationConTest2() {
      Test.startTest();
      ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);         
      PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
      ref.getParameters().put('id',ka1.Id);
      Test.setCurrentPage(ref);        
      OCSUGC_KnowledgeArtifactCreationCon kaController = new OCSUGC_KnowledgeArtifactCreationCon();
      kaController.attachment.Name = 'abc'; 
      kaController.attachment.ContentType ='text/pdf';        
      kaController.attachment.Body = Blob.valueOf('test puneet'); 
      //kaController.knowledgeExchange.OCSUGC_Title__c = '';
      //kaController.knowledgeExchange.OCSUGC_Summary__c = '';
      kaController.selectedGroup = publicGroup.Id;
      kaController.selectedTag = 'test tag 1';      
      kaController.uploadAttachment();
      //System.assert(kaController.knowledgeExchange.Id!=null);
      kaController.selectedTag = 'test tag 2';
      kaController.addTag();
      kaController.selectedTag = 'test tag 3';
      kaController.selectedPicklistValue = 'tag';
      kaController.selectedOldPicklistValue = 'tag';
      //kaController.updateTag();  
      //kaController.updatePrePopulateTags();  
      kaController.updatePrePopulateTagsForVisibility();  
      kaController.updateWrapperForMultiPicklistValues();
      //kaController.updateKnowledge();
      Test.stopTest();  
    }
    
    public static testmethod void test_OCSUGC_KnowledgeArtifactCreationConTest3() {
      System.runAs(memberUsr) {
        //kaTypes = OCSUGC_TestUtility.createKAFileTypes(true);
        Test.startTest();
        ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);
        PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
        ref.getParameters().put('id',ka1.Id);
        Test.setCurrentPage(ref);        
        OCSUGC_KnowledgeArtifactCreationCon kaController = new OCSUGC_KnowledgeArtifactCreationCon();
        kaController.attachment.Name = 'abc.pdf'; 
        kaController.attachment.ContentType ='';        
        kaController.attachment.Body = Blob.valueOf('test puneet'); 
        kaController.selectedGroup = publicGroup.Id;
        kaController.selectedTag = 'test tag 1';  
        kaController.uploadAttachment();
        kaController.selectedTag = 'test tag 2';
        kaController.addTag();
        kaController.selectedTag = 'test tag 3';
        kaController.selectedPicklistValue = 'tag';
        kaController.selectedOldPicklistValue = 'tag';
        kaController.updatePrePopulateTagsForVisibility(); 
        kaController.prePopulateTags();  
        kaController.updateWrapperForMultiPicklistValues();
        //kaController.updateKnowledge();
        Test.stopTest(); 
      }    
    }
}
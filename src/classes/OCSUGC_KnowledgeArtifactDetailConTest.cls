@isTest(SeeAllData=true)
private class OCSUGC_KnowledgeArtifactDetailConTest {

   static User managerUsr,contributorUsr,memberUsr;
   static User adminUsr1,adminUsr2;
   static Account account; 
   static Contact contact1,contact2,contact3;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static List<CollaborationGroup> lstGroupInsert;
   static Profile admin,portal,manager;
   static OCSUGC_Knowledge_Exchange__c knowledgeFile;
   static String networkName;
   static Network ocsugcNetwork;
static {
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     admin = OCSUGC_TestUtility.getAdminProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();   
     
     lstUserInsert = new List<User>();   
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'testAdminRole1'));
         
     account = OCSUGC_TestUtility.createAccount('test account' + Integer.valueOf(math.random()), true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
     insert lstGroupInsert;
     lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager));
     insert lstUserInsert;
     
     System.runAs(adminUsr1) {
        lstGroupMemberInsert = new List<CollaborationGroupMember>();
        lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
        insert lstGroupMemberInsert;
     }
   }
   
    public static testmethod void testKnowledgeArtifactDetailConTest() {
    Test.startTest();   
      //System.runAs(managerUsr) {
                  //create knowledge
        knowledgeFile = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id, publicGroup.Name, false);
        knowledgeFile.OCSUGC_NetworkId__c = OCSUGC_Utilities.getCurrentCommunityNetworkId(false);
        insert knowledgeFile;
    
        OCSUGC_Tags__c tag = OCSUGC_TestUtility.createTags('test tag');
        insert tag;
                  
        OCSUGC_Knowledge_Tag__c knowledgeTag = new OCSUGC_Knowledge_Tag__c(OCSUGC_knowledge_exchange__c = knowledgeFile.Id, OCSUGC_Tags__c = tag.Id);
        insert knowledgeTag;
      
        OCSUGC_Knowledge_Exchange__c relatedKnowledgeFile = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id, publicGroup.Name, false);
              relatedKnowledgeFile.OCSUGC_NetworkId__c = 'Everyone';
              relatedKnowledgeFile.OCSUGC_Is_Deleted__c = false;
              relatedKnowledgeFile.OCSUGC_Status__c = 'Approved';
              insert relatedKnowledgeFile;
      
        OCSUGC_Knowledge_Tag__c relatedKnowledgeTag = new OCSUGC_Knowledge_Tag__c(OCSUGC_knowledge_exchange__c = relatedKnowledgeFile.Id, OCSUGC_Tags__c = tag.Id);
        insert relatedKnowledgeTag;
            
        Blacklisted_Word__c blackListWord =  OCSUGC_TestUtility.createBlackListWords('Fuck');
       
        FeedItem post =  OCSUGC_TestUtility.createFeedItem(knowledgeFile.Id, true);
        FeedComment comment = new FeedComment(FeedItemId = post.Id, CommentBody = 'Test commentsdas',CreatedbyId=managerUsr.Id);
        insert comment;
      
        FeedLike flike = new FeedLike();
        flike.FeedItemId = post.id;
        flike.CreatedById = UserInfo.getUserId();
        insert flike;
         
        PageReference pageRef = Page.OCSUGC_KnowledgeArtifactDetail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('knowledgeArtifactId', knowledgeFile.Id);
        ApexPages.currentPage().getParameters().put('isCreated', 'true');
        
        OCSUGC_KnowledgeArtifactDetailCon controller = new OCSUGC_KnowledgeArtifactDetailCon();
        //inserting getter setter variable 
        controller.knowledgeDescription = 'testknowledgeDescription';
        controller.contentType = 'testcontentType';
        controller.contentId = 'testcontentId';
        controller.contentId = 'testsupportContentId';

        //Insert feed Comment
        controller.stringComments = 'Test Fuck';
        controller.insertFeedComment();
        //assert error message
        System.assertEquals(Label.OCSUGC_CommentBlackListedErrorMessage, ApexPages.GetMessages().get(0).getSummary());
        
        //insert valid comment
        controller.stringComments = 'Test description';
        controller.insertFeedComment();
        
        //assert comment count
        system.assertEquals(1, [Select OCSUGC_Comments__c From OCSUGC_Knowledge_Exchange__c Where Id =: knowledgeFile.Id].OCSUGC_Comments__c);
        
        //flag comment
        controller.selectedChatterFeedId = controller.childFeedItems.get(0).Id;
        controller.isFlagged = false;
        controller.flagComment();
        controller.updateFlagAction();
        
        //assert related knowledge size
        system.assertEquals(1, controller.getRelatedKnowledges().size());
        //Validate likeUnlikeFeedItem
        OCSUGC_KnowledgeArtifactDetailCon.likeUnlikeFeedItem(networkName,knowledgeFile.Id, post.Id);
        
        //validate post without like
        FeedItem post1 =  OCSUGC_TestUtility.createFeedItem(knowledgeFile.Id, true);
        OCSUGC_KnowledgeArtifactDetailCon.likeUnlikeFeedItem(networkName,knowledgeFile.Id, post.Id);
        
         
        //validate view count
        controller.updateViewCount();
        //assert view count
        system.assertEquals(2, [Select OCSUGC_Views__c From OCSUGC_Knowledge_Exchange__c Where Id =: knowledgeFile.Id].OCSUGC_Views__c);
        
        //validate likeUnlikeChatterOnFeed
        //networkName,knowledgeFile.Id, 
        
        OCSUGC_KnowledgeArtifactDetailCon.likeUnlikeChatterOnFeed(knowledgeFile.Id, post.id, controller.childFeedItems.get(0).Id, 'false');
        OCSUGC_KnowledgeArtifactDetailCon.insertFeedCommentRemote('testcomment', knowledgeFile.Id, post.id, 1, 'false');
        Boolean blackListCheck = OCSUGC_KnowledgeArtifactDetailCon.checkForBlackListWords('fuck');
        system.debug('---blackListCheck1---' + blackListCheck);
        blackListCheck = OCSUGC_KnowledgeArtifactDetailCon.checkForBlackListWords('testComment');
        system.debug('---blackListCheck2---' + blackListCheck);
        
        //making controller attachment to call controller remote method
        controller.stringComments = 'Test description';
        Attachment att = new Attachment();
        att.Name = 'test.doc'; 
        att.ContentType ='text/doc';
        att.Body = Blob.valueOf('test puneet'); 
        controller.attachment = att; 
        controller.commentUploadWithAttachment();
        
        //validate document download
        controller.downloadAttachment();
        
        //validate managebookmark
        system.assertEquals(true, OCSUGC_KnowledgeArtifactDetailCon.manageBookmark(knowledgeFile.Id));
        controller.showConfirmation();
        
        //valiadate knowledge edit
        system.assertNotEquals(null, OCSUGC_KnowledgeArtifactDetailCon.editKnowledge(knowledgeFile.Id,'false'));
      Test.stopTest();      

    }
}
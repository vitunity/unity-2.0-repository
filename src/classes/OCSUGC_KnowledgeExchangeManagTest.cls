//
// (c) 2015 Appirio, Inc.
//
// Test Class for - OCSUGC_KnowledgeExchangeManagment
//
// February 3rd, 2015     Harshit Jain[T-317348]
@isTest(SeeAllData=true)
private class OCSUGC_KnowledgeExchangeManagTest {
        static CollaborationGroup publicGroup;
        static OCSUGC_Knowledge_Exchange__c knowledgeExchange;
        static Network ocsugcNetwork;
        static User followerUser;
        static Profile admin;
    
    //creating Test data
    static{
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        admin = OCSUGC_TestUtility.getAdminProfile();
        publicGroup = OCSUGC_TestUtility.createGroup('Test Public Group 1', 'Public', ocsugcNetwork.id, true);
        knowledgeExchange = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,false);
        knowledgeExchange.OCSUGC_Artifact_Type__c = 'AURA Report';
        knowledgeExchange.OCSUGC_Status__c = 'new';
        insert knowledgeExchange;
        
        createFeed(knowledgeExchange.Id);
        
        followerUser = OCSUGC_TestUtility.createStandardUser(true, admin.Id,'test', 'testAdminRole1');
        OCSUGC_TestUtility.createEntitySubscription(followerUser.Id, userInfo.getUserId(), true); 
    }

    static testMethod void testShareWithEveryOne() {
            
            
        Test.startTest();
            knowledgeExchange.OCSUGC_Status__c = 'Approved';
            knowledgeExchange.OCSUGC_group_Id__c = 'Everyone';
            update knowledgeExchange;
        Test.stopTest();
    }
    
    static testMethod void testShareWithGroup() {
            
            
        Test.startTest();
            knowledgeExchange.OCSUGC_Status__c = 'Approved';
            knowledgeExchange.OCSUGC_group_Id__c = publicGroup.Id;
            update knowledgeExchange;
        Test.stopTest();
    }
    
   
    private static void createFeed(Id recordId) {
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
            
            ConnectApi.MessageBodyInput body = new ConnectApi.MessageBodyInput();
            List<ConnectApi.MessageSegmentInput> segmentList = new List<ConnectApi.MessageSegmentInput>();
            ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
            textSegment.text = 'I hope you enjoy this post I found in another group.';
            segmentList.add((ConnectApi.MessageSegmentInput)textSegment);
            body.messageSegments = segmentList;
            input.body = body;
            
            //Post a file in feed 
            // POST FILE_FEED_ITEM
            ConnectApi.NewFileAttachmentInput fileIn = new ConnectApi.NewFileAttachmentInput();
            fileIn.title = 'Test file';
            fileIn.description = 'Test description of file';
             
            input.attachment = fileIn;
             
            ConnectApi.BinaryInput feedBinary = new ConnectApi.BinaryInput(Blob.valueOf('body file'), 'plan/txt', 'Test file');
            
            //create feed item
            ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, recordId, input, feedBinary);
    }
}
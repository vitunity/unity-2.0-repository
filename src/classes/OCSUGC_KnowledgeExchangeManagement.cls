/*
Name        : OCSUGC_KnowledgeExchangeManagement
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 25th June, 2014
Purpose     : Mangement class for triggers on Knowlege_Exchange__c object
*/

public without sharing class OCSUGC_KnowledgeExchangeManagement {
    
    //this function shares the artifact with the group Id associated in the record
    public static void shareArtifactWithFroup(Map<ID, OCSUGC_Knowledge_Exchange__c> newMap, Map<ID, OCSUGC_Knowledge_Exchange__c> oldMap){
        Set<String> exchangeIds = new Set<String>();
        Set<String> createdByIds = new Set<String>();
        for(OCSUGC_Knowledge_Exchange__c exchange : newMap.values()){
            if(exchange.OCSUGC_Status__c == 'Approved' && exchange.OCSUGC_Status__c != oldMap.get(exchange.Id).OCSUGC_Status__c){
                exchangeIds.add(exchange.Id);
                createdByIds.add(exchange.createdById);
            }
        }
        
        Map<String, OCSUGC_Knowledge_Exchange__Feed> mapKnowledgeExchangeFeeds = new Map<String, OCSUGC_Knowledge_Exchange__Feed>();
        Map<String, List<EntitySubscription>> mapFollowers = fetchFollowers(createdByIds);
        //fetching uploaded content post
        for(OCSUGC_Knowledge_Exchange__Feed tempFeedItem : [Select k.ParentId,RelatedRecordId 
                                            From OCSUGC_Knowledge_Exchange__Feed k
                                            where ParentId in : exchangeIds and type='ContentPost' limit 1]){
              mapKnowledgeExchangeFeeds.put(tempFeedItem.RelatedRecordId, tempFeedItem);                           
        }
        
        
        //fetching content versions
    }
    
    //fetch followers of the users
    private static Map<String, List<EntitySubscription>> fetchFollowers(Set<String> createdByIds){
        Map<String, List<EntitySubscription>> mapFollowers = new Map<String, List<EntitySubscription>>();
        for(EntitySubscription sObj :[select id, subscriberid, subscriber.name, parentid
                                           from EntitySubscription
                                          where parentid in : createdByIds]){
            if(!mapFollowers.containsKey(sObj.parentid)){
                mapFollowers.put(sObj.parentid, new List<EntitySubscription>());
            }
            mapFollowers.get(sObj.parentid).add(sObj);                          
        }
        return mapFollowers;
    }
}
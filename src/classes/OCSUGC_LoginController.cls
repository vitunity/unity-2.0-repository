/*
@Name : OCSUGC_Login
@Author : Shital Bhujbal
@Created date: 3/8/2015
@Description : Controller created for login page functionality
*/

public without sharing class OCSUGC_LoginController {

    public String password {get; set;}
    public String username {get; set;}
    public String introVideoUrl{get; set;}
    private User loginUser;
    private transient String onetimeToken = '';
    private transient String oktaUserId   = '';
    private transient String sessionId    = '';

    public String msg{get;set;}

    //find user is in permission set VMS_OCSUGC_CommunityManager_Permission_Set
    public boolean isUserRequestToSpheros {get;set;}

    private Id orgWideEmailAddressId;

    private String startUrl;

    String cntAdmin='';
    Id AccntId=null;

    //This constructor is used to hide and show the portion in vf page on the basis of paramter in url
    public OCSUGC_LoginController() {
        isUserRequestToSpheros = false;
        msg = ApexPages.currentPage().getParameters().get('msg');
        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        introVideoUrl = EncodingUtil.urlDecode(Label.OCSUGC_Login_Video_Url, 'UTF-8');

        //destination URL is stored in startUrl
        startUrl = ApexPages.currentPage().getParameters().get('startUrl');
    }

    public PageReference redirectLoggedIn() {
        if(UserInfo.getUserType() != 'Guest') {
            PageReference loggedIn = new PageReference(Label.OCSUGC_Salesforce_Instance + '/OCSUGC/OCSUGC_Home?fgab=false');
            return loggedIn;
        }
        return null;
    }

    public boolean isUserAddedToCakeGroup {get; set;}
    //Okta login
    public pagereference loginWithOkta() {
        isUserAddedToCakeGroup = true;
        //Validate Input
        if( String.isEmpty(username) || String.isEmpty(password) ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Valid_Email_And_Password));
         
            return null;
        } else if(!checkEmailFormat(username)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Email_Address_Error));
            return null;
        }

        //Create OKTA session and get OnetimeToken
        Integer respStatus = createOktaSession();
		system.debug('*******respStatus****'+respStatus);
        //If success move forward
        if( (respStatus == 200) || (respStatus == 204) ) {
            if( String.isNotEmpty(onetimeToken) ) { //without token cannot login
       
                    //save users sessionid
                    saveUserSessionId();
                    system.debug('==================PageReference: ' + Label.OCSUGC_oktaCpLoginUrl +' ==== '+ onetimeToken);
                    if(loginUser.Contact.OCSUGC_UserStatus__c != Label.OCSUGC_Approved_Status) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_User_is_not_Approved));
                    } else {
                        //redirect user to App specific OKTA url to authenticate for the App
                        Boolean bRelayState = false;
                        if(String.isNotBlank(startUrl)) {
                            if( startUrl.equals('/OCSUGC') || startUrl.equals('/OCSUGC/') ) { //base community URL, nothing to do
                                bRelayState = false;
                            }
                            else if (startUrl.startsWith('/OCSUGC/OCSUGC_Login')) { //login page nothing to do
                                bRelayState = false;
                            }
                            else {
                                bRelayState = true; //user intends to go to specific URL after login
                                startUrl = EncodingUtil.urlEncode(startUrl, 'UTF-8');
                            }

                        }

                        if(bRelayState) {
                            return new PageReference(Label.OCSUGC_oktaCpLoginUrl + onetimeToken + '&RelayState=' + startUrl);
                        } else {
                            return new PageReference(Label.OCSUGC_oktaCpLoginUrl + onetimeToken);
                        }
                        
                    }
    
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Server_Error));
            }
        } else if( respStatus == 401 ) { //If 401, authentication failed
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Authentication_Error+' To register for MyVarian Account click <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?regsrc=OCSUGCRequest" class="alert-link">here</a>'));
            verifyUserPasswordValidity();
        } else { //If some other error, display general server error message
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Server_Error));
        }
        
        return null;
    }

    //checking if user is a CAKE user
    public boolean doesHaveAccesstoAppInOkta(){
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Construct Authorization and Content header
        String strToken = Label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('GET');

        String endpointgroup = Label.oktachangepasswrdendpoint + oktaUserId + '/groups';
        req.setEndpoint(endpointgroup);

        Set<String> setGroupIds = new Set<String>();

        //Call OKTA
        try
        {
            system.debug('DEBUG: OKTA GRP REQ - URL----' + req.getEndpoint() +'-----'+req.getheader('Authorization'));

            if(!Test.isRunningTest()){
                res = http.send(req);
            }
            else{
        		res.setBody('{"id" : "hkjhkjkjjhhgjhgj" }');
            }

            system.debug('DEBUG: OKTA GRP RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody());
            JSONParser parser = JSON.createParser(res.getBody());

            while (parser.nextToken() != null)
            {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
                {
                    if(parser.getText() == 'id')
                    {
                        parser.nextToken();
                        setGroupIds.add(parser.getText());
                    }

                    parser.skipChildren();
                }
            }
            system.debug('==== setGroupIds==  ' +setGroupIds);
            if(setGroupIds.contains(Label.OCSUGC_Okta_Group_Id)) {
                return true;
            } else {
                return false;
            }

        } catch(System.CalloutException e) {
            System.debug('======================Error:  '+ res.toString());
            return false;
        }
        return false;
    }

    //should be part of somekind of global utility
    public static Boolean checkEmailFormat(String email) {
        String emailRegEx = '^([a-zA-Z0-9_\\+\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        Boolean result = MyMatcher.matches();
        return result;
    }
    
    public Boolean getDoesErrorExist() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO);
    }

    public void saveUserSessionId() {
        loginUser = new User();
        system.debug('**oktaUserId'+oktaUserId);
        loginUser = [Select Id, Contact.OCSUGC_UserStatus__c from User where Contact.OktaId__c =: oktaUserId and isactive = true Limit 1];

        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
        sessionmap = usersessionids__c.getall();

        usersessionids__c usersessionrecord = new usersessionids__c(name = loginUser.id, session_id__c = sessionId);

        if( !sessionmap.containskey(loginUser.id) ) {
            insert usersessionrecord;
        } else {
            usersessionrecord.Id = sessionmap.get(loginUser.id).id;
            update usersessionrecord;
        }
    }

    public Integer createOktaSession() {
        Integer respStatus = 0;

        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Construct Authorization and Content header
        String strToken = Label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');

        //Construct request body
        String body = '{ "username": "'+username+'", "password": "'+password+'" }';
        req.setBody(body);

        //Set request method and URL
        req.setMethod('POST');
        req.setEndpoint(Label.OCSUGC_Okta_End_Point);

        //Call OKTA
        try {
            system.debug('==================DEBUG: OKTA REQ - body----' + req.getbody()+'-----'+req.getheader('Authorization'));
            if(!Test.isRunningTest()) {
                res = http.send(req);
            }
            system.debug('==================DEBUG: OKTA RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody());

            respStatus = res.getStatusCode();

            //OKTA success code is either 200 or 204
            if( (respStatus == 200) || (respStatus == 204) ) {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                        String fieldName = parser.getText();
                        parser.nextToken();
                        if(fieldName == 'cookieToken') {
                            onetimeToken = parser.getText();
                        }

                        if(fieldName == 'userId') {
                            oktaUserId = parser.getText();
                            system.debug('**oktaUserId'+oktaUserId);
                        }

                        if(fieldName == 'id') {
                           sessionId = parser.getText();
                        }
                    }
                }
            } else { 
            	System.debug('==================logOktaError ');
                logOktaError(res.getBody());
            }
            return respStatus;

        } catch(System.CalloutException e) {
            respStatus = 500;
        }

        return respStatus;
    }

    public void verifyUserPasswordValidity()
    {
        Integer userRespStatus = 0;

        //Construct HTTP request and response
        HttpRequest req1 = new HttpRequest();
        HttpResponse res1 = new HttpResponse();
        Http http1 = new Http();
        
        //Construct Authorization and Content header
        String strToken1 = Label.OCSUGC_Okta_Token; 
        String authorizationHeader1 = 'SSWS ' + strToken1;
        req1.setHeader('Authorization', authorizationHeader1); 
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Accept','application/json');
      
        String endpoint1 = Label.OCSUGC_Okta_Api_Common_Link+'/'+EncodingUtil.urlEncode(username, 'UTF-8');
 

        //Set Method and Endpoint and Body
        req1.setMethod('GET');
        req1.setEndpoint(endpoint1);

        try {

            if(!Test.isRunningTest()) {
                system.debug('==================DEBUG: OKTA Get User REQ - ' + endpoint1 + ' - ' + req1.getheader('Authorization'));
                res1 = http1.send(req1);
            }
            else{
            	res1.setHeader('Content-Type', 'application/json');
        		res1.setBody('{"cookieToken":"jkjhkyuhkgjgjgjgjj","userId" : "00ub0oNGTSWTBKOLGLNR", "id" : "hkjhkjkjjhhgjhgj" }');
        		res1.setStatusCode(200);
            }

            system.debug('==================DEBUG: OKTA Get User RESP - code:' + res1.getStatusCode() + '------BODY------' + res1.getBody());

            userRespStatus = res1.getStatusCode();

            //OKTA success code is either 200 or 204
            if( (userRespStatus == 200) || (userRespStatus == 204) ) {
                String userId;
                String oktaStatus;

                JSONParser parser1 = JSON.createParser(res1.getBody());

                while (parser1.nextToken() != null) {
                    if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME)) {
                        String fieldName1 = parser1.getText();
                        parser1.nextToken();
                        if(fieldName1 == 'id') {
                            userId = parser1.getText();                          
                        }
                        if(fieldName1 == 'status') {    
                            oktaStatus = parser1.getText();
                            System.debug('================== Check Lock-->'+oktaStatus);
                        }
                    }
                }

                if(!String.isBlank(userId)) {
                    if( oktaStatus == 'LOCKED_OUT' || oktaStatus == 'DEPROVISIONED') {
                        System.debug('==================account is locked ----->' + oktaStatus);
                        userRespStatus = 403;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, we are having issue with your account. Please contact us <a href="/apex/OCSUGC_Login_ContactUs">here</a>'));
                    }
                    else {
                        System.debug('==================password is incorrect----->' + oktaStatus);
                        userRespStatus = 401;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your password is not recognized. <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?Request=Reqnewpass" class="alert-link">Have you forgotten your password?</a>'));
                    }
                }
                else {
                    System.debug('==================user does not exist----->' + username);
                    userRespStatus = 404;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your username is not recognized. <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?regsrc=OCSUGCRequest" class="alert-link">Please register for a new account.</a>'));        
                }    

            } else if (userRespStatus == 404) { 
                System.debug('==================user does not exist----->' + username);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your username is not recognized. <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?regsrc=OCSUGCRequest" class="alert-link">Please register for a new account.</a>'));        
            } else { 
                System.debug('==================logOktaError ');
                logOktaError(res1.getBody());
            }

        } catch(System.CalloutException e) {
            userRespStatus = 500;
            System.debug('==================CalloutException'); 
        }

        return;        
    }

    public void logOktaError(String respBody)
    {
        String errorCode    = '';
        String errorSummary = '';

        JSONParser parser = JSON.createParser(respBody);
        while (parser.nextToken() != null) {
            if( (parser.getCurrentToken() == JSONToken.FIELD_NAME) ) {
                String fieldName = parser.getText();
                parser.nextToken();
                if( fieldName == 'errorCode' ) {
                    errorCode = parser.getText();
                    system.debug('**********OKTA errorCode********'+errorCode);
                }
                if( fieldName == 'errorSummary' ) {
                    errorSummary = parser.getText();
                    system.debug('**********OKTA errorSummary********'+errorSummary);
                }
            }
        }
    }
}
// (c) 2012 Appirio, Inc.
// Test class  for OCSUGC_LoginController
// 6 Aug 2014 JaiShankar Test class  for OCSUGC_LoginController

@isTest
public class OCSUGC_LoginControllerTest
{
    public static testmethod void test_LoginController()
    {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = OCSUGC_TestUtility.getAdminProfile();
        PermissionSet pSet = OCSUGC_TestUtility.getManagerPermissionSet();

        PageReference pageRef = Page.OCSUGC_Login;
        Test.setCurrentPage(pageRef);

        test.startTest();

        //portal user owner
        User portalAccountOwner1 = new User(
                                            UserRoleId = portalRole.Id,
                                            ProfileId = profile1.Id,
                                            Username = 'test1@ocsugc.com.ocsugc',
                                            Alias = 'testOwn',
                                            Email='test1@owner.com',
                                            EmailEncodingKey='UTF-8',
                                            Firstname='test',
                                            Lastname='last',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',
                                            TimeZoneSidKey='America/Chicago'
                                            );
        Database.insert(portalAccountOwner1);

        //portal user owner
        User portalAccountOwner2 = new User(
                                            UserRoleId = portalRole.Id,
                                            ProfileId = profile1.Id,
                                            Username = 'test2@ocsugc.com.ocsugc',
                                            Alias = 'testOwn2',
                                            Email='test2@owner.com',
                                            EmailEncodingKey='UTF-8',
                                            Firstname='test2',
                                            Lastname='last2',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',
                                            TimeZoneSidKey='America/Chicago'
                                            );
        Database.insert(portalAccountOwner2);

        //run as portal user
        System.runAs ( portalAccountOwner1 )
        {
            //Create account
            Account portalAccount1 = OCSUGC_TestUtility.createAccount('test account', true);

            //Create contact
            Contact contact1 = OCSUGC_TestUtility.createContact(portalAccount1.Id, true);

            //Create user
            Profile portalProfile1 =  OCSUGC_TestUtility.getPortalProfile();
            User user1 = OCSUGC_TestUtility.createPortalUser(false, portalProfile1.Id, contact1.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member);
            Database.insert(user1);

            System.runAs (user1) {

                //List of Contact Object
                List<Contact> lstContacts = new List<Contact>();
                //Create an instance of controller - CAKELoginController
                OCSUGC_LoginController objcakeLogin = new OCSUGC_LoginController(); 
                objcakeLogin.username = 'taaaaest123@ocsugc.com.ocsugc';
                objcakeLogin.password = 'appirio123';
                objcakeLogin.loginWithOkta();
                objcakeLogin.doesHaveAccesstoAppInOkta();
                objcakeLogin.getDoesErrorExist();
                objcakeLogin.saveUserSessionId();
                
                objcakeLogin.username = '';
                objcakeLogin.password = '';
                objcakeLogin.loginWithOkta();
                
                objcakeLogin.username = 'shitalTest';
                objcakeLogin.password = 'test';
                objcakeLogin.loginWithOkta();
                
                objcakeLogin.username = 'test2@owner.com';
                objcakeLogin.password = 'test123';
                objcakeLogin.loginWithOkta();
                
                objcakeLogin.redirectLoggedIn();
                objcakeLogin.loginWithOkta();
                objcakeLogin.verifyUserPasswordValidity();
            }
       }
    }
}
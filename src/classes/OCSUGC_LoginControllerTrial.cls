// (c) 2012 Appirio, Inc.
//
// Class Name CAKELoginController
//
// 8 Aug 2014    ModifiedBy(Jai Shankar Pandey)   added a method  "btnsubmitMessageAndEmailToPublicGroup()" functionality as if User is not In the permission "VMS CAKE - Members Permissions" then
//the button should  be display on CAKELogin page, if he is in that permission set it should not and also onclick of that button Contact.Spheros status shoud change as "Spheros Pending Approval"

public without sharing class OCSUGC_LoginControllerTrial {

    public String password {get; set;}
    public String username {get; set;}

    public String strUserFirstName{get; set;}
    public String strUserLastName{get; set;}
    public String strUserEmail  {get; set;}
    public String strReasonOfAcces {get; set;}
    public String errorMessage {get; set;}

    public Boolean isFormSubmitted{get; set;}

    private transient String onetimeToken = '';
    private transient String oktaUserId   = '';
    private transient String sessionId    = '';

    public String msg{get;set;}

    //find user is in permission set VMS_OCSUGC_CommunityManager_Permission_Set
    public boolean isUserRequestToSpheros {get;set;}

    //Static final variable for querying Email Template and Admin Group
    private static final String OCSUGC_ADMIN_PUBLIC_GROUP_EMAIL_TEMPLATE = 'OCSUGC_Email_Notification_To_The_users_in_the_OCSUGC_Admin_Public_Group';   

    String cntAdmin='';
    Id AccntId=null;


    //This constructor is used to hide and show the portion in vf page on the basis of paramter in url
    public OCSUGC_LoginControllerTrial() {
        isUserRequestToSpheros = false;
        msg = ApexPages.currentPage().getParameters().get('msg');
    }

    public boolean isUserAddedToCakeGroup {get; set;}
    //Okta login
    public PageReference loginWithOkta(){
        isUserAddedToCakeGroup = true;
        //Validate Input
        if( String.isEmpty(username) || String.isEmpty(password) )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Valid_Email_And_Password));
            return null;
        }
        else if(!checkEmailFormat(username))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Email_Address_Error));
            return null;
        }

        //Create OKTA session and get OnetimeToken
        Integer respStatus = createOktaSession();

        //If success move forward
        if( (respStatus == 200) || (respStatus == 204) )
        {
            if( String.isNotEmpty(onetimeToken) ) //without token cannot login
            {
                //check if user has access to CAKE
                if( doesHaveAccesstoAppInOkta() ){
                    //save users sessionid
                    saveUserSessionId();

                    //redirect user to App specific OKTA url to authenticate for the App
                    return new PageReference(Label.OCSUGC_oktaCpLoginUrl + onetimeToken);
                }else{
                    //TODO: get the message from Label
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Access_Error));
                    isUserRequestToSpheros = true; //make Request Access button visible
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Server_Error));
            }
        }
        else if( respStatus == 401 ) //If 401, authentication failed
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Authentication_Error+' To register for MyVarian Account click <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?requestParam=OCSUGCRequest">here</a>'));
        }
        else //If some other error, display general server error message
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.OCSUGC_Server_Error));
        }

        return null;
    }

    //checking if user is a CAKE user
    public boolean doesHaveAccesstoAppInOkta(){
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Construct Authorization and Content header
        String strToken = Label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        req.setMethod('GET');

        String endpointgroup = Label.oktachangepasswrdendpoint + oktaUserId + '/groups';
        req.setEndpoint(endpointgroup);

        Set<String> setGroupIds = new Set<String>();

        //Call OKTA
        try
        {
            system.debug('DEBUG: OKTA GRP REQ - URL----' + req.getEndpoint() +'-----'+req.getheader('Authorization'));

            if(!Test.isRunningTest()){
                res = http.send(req);
            }

            system.debug('DEBUG: OKTA GRP RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody());
            JSONParser parser = JSON.createParser(res.getBody());

            while (parser.nextToken() != null)
            {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
                {
                    if(parser.getText() == 'id')
                    {
                        parser.nextToken();
                        setGroupIds.add(parser.getText());
                    }

                    parser.skipChildren();
                }
            }

            if(setGroupIds.contains(Label.OCSUGC_Okta_Group_Id)){
                return true;
            }else{
                return false;
            }

        }catch(System.CalloutException e){
            System.debug(res.toString());
            return false;
        }

        return false;
    }

    //should be part of somekind of global utility
    public static Boolean checkEmailFormat(String email) {
        String emailRegEx = '^([a-zA-Z0-9_\\+\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        Boolean result = MyMatcher.matches();
        return result;
    }

    public void saveUserSessionId()
    {
        User u = new user();
        u = [Select id from User where Contact.OktaId__c =: oktaUserId and isactive = true Limit 1];

        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
        sessionmap = usersessionids__c.getall();

        usersessionids__c usersessionrecord = new usersessionids__c(name = u.id, session_id__c = sessionId);

        if( !sessionmap.containskey(u.id) )
        {
            insert usersessionrecord;
        }
        else
        {
            usersessionrecord.Id = sessionmap.get(u.id).id;
            update usersessionrecord;
        }
    }

    public Integer createOktaSession()
    {
        Integer respStatus = 0;

        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Construct Authorization and Content header
        String strToken = Label.OCSUGC_Okta_Token;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');

        //Construct request body
        String body = '{ "username": "'+username+'", "password": "'+password+'" }';
        system.debug('@@@@@' + body);
        req.setBody(body);

        //Set request method and URL
        req.setMethod('POST');
        req.setEndpoint(Label.OCSUGC_Okta_End_Point);

        //Call OKTA
        try
        {
            system.debug('DEBUG: OKTA REQ - body----' + req.getbody()+'-----'+req.getheader('Authorization'));
            if(!Test.isRunningTest()){
                res = http.send(req);
            }
            system.debug('DEBUG: OKTA RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody());

            respStatus = res.getStatusCode();

            //OKTA success code is either 200 or 204
            if( (respStatus == 200) || (respStatus == 204) )
            {
                JSONParser parser = JSON.createParser(res.getBody());
                while (parser.nextToken() != null)
                {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
                    {
                        String fieldName = parser.getText();
                        parser.nextToken();
                        if(fieldName == 'cookieToken')
                        {
                            onetimeToken = parser.getText();
                        }
                        if(fieldName == 'userId')
                        {
                            oktaUserId = parser.getText();
                        }
                        if(fieldName == 'id')
                        {
                           sessionId = parser.getText();
                        }

                    }
                }
            }
            else
            {
                logOktaError(res.getBody());

            }

            return respStatus;

        }
        catch(System.CalloutException e)
        {
            System.debug(res.toString());
            respStatus = 500;
        }

        return respStatus;
    }

    public void logOktaError(String respBody)
    {
        String errorCode    = '';
        String errorSummary = '';

        JSONParser parser = JSON.createParser(respBody);
        while (parser.nextToken() != null)
        {
            if( (parser.getCurrentToken() == JSONToken.FIELD_NAME) )
            {
                String fieldName = parser.getText();
                parser.nextToken();
                if( fieldName == 'errorCode' )
                {
                    errorCode = parser.getText();
                }
                if( fieldName == 'errorSummary' )
                {
                    errorSummary = parser.getText();
                }

            }
        }

        system.debug('DEBUG: OKTA ERROR: ' + errorCode + ' - ' + errorSummary);
    }

     // Email to send the Spheros admin group users and changing the staus on contact.spheros as Spheros Pending Approval
    //  argument none
    //  @return none

    public void btnsubmitMessageAndEmailToPublicGroup(){

        errorMessage = '';

        system.debug('==========btnsubmitMessageAndEmailToPublicGroup==============');
        //list will contain Email drafts which needs to be send
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();

        //list will store contact matched with email provided and which has  MyVarian_member__c is equal to true
        List<Contact>  lstContactWithInputtedEmail = new List<Contact>();

        Contact objContact;
        isUserRequestToSpheros = true;

        //Query and fetch the Email Template Format for sending email to Public Group
        EmailTemplate objEmailTemplate = [SELECT Id,
                                                 Subject,
                                                 HtmlValue,
                                                 Body
                                           FROM EmailTemplate
                                           WHERE DeveloperName = :OCSUGC_ADMIN_PUBLIC_GROUP_EMAIL_TEMPLATE
                                           LIMIT 1];


        String strHtmlBody = objEmailTemplate.HtmlValue;
        String strPlainBody = objEmailTemplate.Body;

        // replace the logged in user's name dynamically in HTMLBody
        strHtmlBody = strHtmlBody.replace('USER_FIRST_NAME', strUserFirstName);
        strHtmlBody = strHtmlBody.replace('USER_LAST_NAME', strUserLastName);
        strHtmlBody = strHtmlBody.replace('REASON_FOR_REQUEST',strReasonOfAcces  );

        // replace the logged in user's name dynamically in Plain Body
        strPlainBody = strPlainBody.replace('USER_FIRST_NAME', strUserFirstName);
        strPlainBody = strPlainBody.replace('USER_LAST_NAME', strUserLastName);
        strPlainBody = strPlainBody.replace('REASON_FOR_REQUEST',strReasonOfAcces);

        if(strUserEmail != ''){

            //Query Contact object to get all Contact records with email matching aginst email apecified in pop-up
            for(Contact objCon : [SELECT Id,
                                         MyVarian_member__c,
                                         Email,
                                         County__c,
                                         OCSUGC_UserStatus__c ,
                                         AccountId,
                                         Account.Country__c
                                 FROM Contact
                                 WHERE Email =: strUserEmail  AND MyVarian_member__c = True LIMIT 1]){


                lstContactWithInputtedEmail.add(objCon);

            }

            //Show error message in pop-up if there is no contact with matching Email and MyVarian_member__c is equal to true
            if(lstContactWithInputtedEmail.isEmpty()){
                String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
                Pattern MyPattern = Pattern.compile(emailRegex);
                Matcher MyMatcher = MyPattern.matcher(strUserEmail);
                if (MyMatcher.matches()){
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_InvalidEmailErrorMsgNew+' <a href="'+URL.getCurrentRequestUrl().toExternalForm()+'/VR_Registration_PG?requestParam=OCSUGCRequest">here</a>'));
                }else{
                    errorMessage = Label.OCSUGC_Invalid_Email;
                }
            }
            else {
                //Show error message in pop-up if Country associated with contact's account is not USA
                if(OCSUGC_Approved_Countries__c.getValues(lstContactWithInputtedEmail[0].Account.Country__c) == null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_InvalidCountryErrorMsgNew+' <a href='+URL.getCurrentRequestUrl().toExternalForm()+'/OCSUGC_ContactUs">here.</a>'));

                }
                else if(OCSUGC_Approved_Countries__c.getValues(lstContactWithInputtedEmail[0].Account.Country__c) != null && (lstContactWithInputtedEmail[0].OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status || lstContactWithInputtedEmail[0].OCSUGC_UserStatus__c == Label.OCSUGC_Pending_Approval)){

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_InvalidContactStatusNew+' <a href='+URL.getCurrentRequestUrl().toExternalForm()+'/OCSUGC_ContactUs">here.</a>'));

                }
                //Show message in pop-up if Country associated with contact's account is USA and Contact status is neither Spheros Approved nor Spheros Pending Approval
                else if(OCSUGC_Approved_Countries__c.getValues(lstContactWithInputtedEmail[0].Account.Country__c) != null && (lstContactWithInputtedEmail[0].OCSUGC_UserStatus__c != Label.OCSUGC_Approved_Status && lstContactWithInputtedEmail[0].OCSUGC_UserStatus__c != Label.OCSUGC_Pending_Approval)){

                    isFormSubmitted = true;

                    //Query and fetch all the Group member associated with Spheros Admin public group
                    for(GroupMember objGroupMember : [SELECT Id,
                                                             UserOrGroupId
                                                      FROM GroupMember
                                                      WHERE Group.DeveloperName = :OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP]) {

                        //filter only direct associated Users
                        if(String.ValueOf(objGroupMember.UserOrGroupId).startsWith('005')) {

                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            email.setSaveAsActivity(false);
                            email.setSenderDisplayName('OCSUGC');
                            email.setSubject(objEmailTemplate.Subject);
                            email.setHtmlBody(strHtmlBody);
                            email.setPlainTextBody(strPlainBody);
                            email.setTargetObjectId(objGroupMember.UserOrGroupId);
                            lstEmail.add(email);

                        }
                    }
                    objContact = new Contact(Id = lstContactWithInputtedEmail[0].Id,OCSUGC_UserStatus__c = Label.OCSUGC_Pending_Approval);
                }
            }
        }

        try{
            //Sending Email here
            if(!lstEmail.IsEmpty())
                Messaging.sendEmail(lstEmail);

            //update Contact
            if(objContact != Null){
                update objContact;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.OCSUGC_Activation_From_Login_New+' <a href='+URL.getCurrentRequestUrl().toExternalForm()+'/OCSUGC_ContactUs">here.</a>'));
            }
        }
        catch(Exception ex){
            system.debug('========>>>'+ex.getMessage());
        }
    }
}
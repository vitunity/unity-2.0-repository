/****************************************************************
 * Name          :  MarketingApplicationsControllerTest
 * Related Class :  MarketingApplicationsController
 * Author        :  Manmeet Manethiya (Appirio India)
 * Created       :  21 May, 2014.

 ****************************************************************/

@isTest
private class OCSUGC_MarketingApplicationsConTest{
    
    static User portalAccountOwner1;
    static Profile admin,portal;
    static Account portalAccount1; 
    static Contact contact1;
    static Network ocsugcNetwork;

    static{
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile();
        portalAccount1 = OCSUGC_TestUtility.createAccount('test account', true);
        contact1 = OCSUGC_TestUtility.createContact(portalAccount1.Id, true);
        portalAccountOwner1 = OCSUGC_TestUtility.createPortalUser(true, portal.Id, contact1.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member);
        
    }

    private static testMethod void testMarketingApp() {
        Test.startTest();
        system.RunAs(portalAccountOwner1){

            OCSUGC_Intranet_Content__c content = new OCSUGC_Intranet_Content__c();
            content.recordTypeID = getRecordTypeId('OCSUGC_Intranet_Content__c', 'Applications');
            insert content;

            OCSUGC_Application_Permissions__c permission = new OCSUGC_Application_Permissions__c ();
            permission.OCSUGC_Contact__c = contact1.Id;
            permission.OCSUGC_Application__c = content.Id;
            insert permission ;
            Test.setCurrentPage(Page.OCSUGC_MarketingApplications);
            ApexPages.currentPage().getParameters().put('applicationId', content.Id);

            OCSUGC_MarketingApplicationsController  controller = new OCSUGC_MarketingApplicationsController();
            controller.updateApplicationPermissions();
            system.assertNotEquals(null, controller.application);
            system.assertNotEquals(null, controller.permission);

        }
        Test.stopTest();
    }

    //this function returns the Name of record type when id is given
    private static String getRecordTypeId(string objectAPIName, string recordTypeName){
        Schema.DescribeSObjectResult d = Schema.getGlobalDescribe().get(objectAPIName).getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName = rtMapByName.get(recordTypeName);
        return rtByName.getRecordTypeId();
    }
}
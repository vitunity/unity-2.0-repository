/*
Name        : MarketingApplicationsController
Updated By  : Manmeet Manethiya (Appirio India)
Date        : 21th May, 2014
Purpose     : An Apex controller to get Information for MarketingApplication.page.

*/
public without sharing class OCSUGC_MarketingApplicationsController {

        public String applicationId {get; set;}
        public OCSUGC_Intranet_Content__c application {get; set;}
        public OCSUGC_Application_Permissions__c permission{get; set;}

        String contactId;
        static final String RT_Application = 'Application';

        //constructor
        public OCSUGC_MarketingApplicationsController(){
            if(ApexPages.currentPage().getParameters().get('applicationId') != null){
                applicationId = ApexPages.currentPage().getParameters().get('applicationId');
                permission = new OCSUGC_Application_Permissions__c(OCSUGC_Application__c = applicationId);
                fetchApplicationData();
                fetchApplicationPermission();
            }
        }

        //fetching Application data
        private void fetchApplicationData(){
            for(OCSUGC_Intranet_Content__c content : [Select Name, OCSUGC_Description__c from OCSUGC_Intranet_Content__c where id =: applicationId]){
                application = content;
            }
        }

        //fetching application permissions
        private void fetchApplicationPermission(){
            for(User currUser :[Select ContactId From User Where Id =:UserInfo.getUserId()]) {
                contactId = currUser.ContactId;
            }
            if(contactId != null){
                permission.OCSUGC_Contact__c = contactId;
                for(OCSUGC_Application_Permissions__c appPermissions :[Select OCSUGC_Application__c, OCSUGC_Access_Reason__c, OCSUGC_Contact__c
                                                                From OCSUGC_Application_Permissions__c
                                                                Where OCSUGC_Application__r.RecordType.Name =:RT_Application
                                                                And OCSUGC_Contact__c =:contactId
                                                                And OCSUGC_Application__c =: applicationId limit 1]) {
                    permission = appPermissions;
                }
            }
        }

        //updating Application Permissions
        public Pagereference updateApplicationPermissions(){
            if(contactId != null){
                permission.OCSUGC_Approval_Status__c = 'Pending Approval';
                upsert permission;
            }
            return null;
        }
}
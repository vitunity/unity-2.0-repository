public without sharing class OCSUGC_MembersController extends OCSUGC_Base_NG {
  public List<WrapperMember> lstMembers {get;set;}
  public Integer membersCount {get; set;}
  public Boolean showingSearchRslt {get;set;}
  public String quickSearchName {get; set;}
  public String advSearchTitle {get; set;}
  public String advSearchAccount {get; set;}
  public String advSearchCountry {get; set;}
  public Boolean advSearchIncludeInternal {get; set;}
  public Boolean advSearchIncludeReadOnly {get; set;}
  public String followMemberId {get;set;}
  public String unfollowMemberId {get;set;}

  public Boolean isGroupInviteMode {get;set;}
  public String currentGroupId {get;set;}
  public CollaborationGroup currentGroup {get;set;}
  public Set<String> selectedMembers {get;set;}
  public Integer totalSelected {get;set;}
  public String selectedMemberId {get;set;}

  @TestVisible private Id orgWideEmailAddressId;
  private String networkName;

  public OCSUGC_MembersController() {
    isGroupInviteMode = false;
    selectedMembers = new Set<String>();
    totalSelected = 0;

    orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
    networkName = OCSUGC_Utilities.getCurrentCommunityNetworkName(isDC);

    currentGroupId = ApexPages.currentPage().getParameters().get('g');
    if(String.isNotBlank(currentGroupId)) {
      try {  //wrap in try carch to handle the case when no results are found       
        currentGroup = [SELECT Name, 
                         SmallPhotoUrl,
                         CollaborationType,
                         OwnerId,
                         Owner.Email,
                         Owner.Name,
                         Owner.ContactId,
                         Owner.Contact.OCSUGC_Notif_Pref_ReqJoinGrp__c,
                         IsArchived
                        FROM CollaborationGroup
                        WHERE Id =:currentGroupId
                        AND NetworkId IN (:ocsugcNetworkId, :ocsdcNetworkId)];
      } catch(Exception e) {
        currentGroup = NULL;
      }

      if( (currentGroup != null) && ((currentGroup.OwnerId == loggedInUser.Id) || isCommunityManager) ) {
        isGroupInviteMode = true;
      }
    }

    if(isGroupInviteMode)
      pageTitle = 'Invite Members | OncoPeer';
    else
      pageTitle = 'Members | OncoPeer';

    showingSearchRslt = false;
    quickSearchName = '';
    advSearchTitle = '';    
    advSearchAccount = '';   
    advSearchCountry = '';
    advSearchIncludeInternal = false;
    advSearchIncludeReadOnly = false;
    membersCount = 0;
  }

  /*
   *  method call from action function on page load
   */
  public void loadMembers() {
    fetchMembers(false);
  }

  public void fetchMembers(Boolean doSearch) {
    lstMembers = new List<WrapperMember>();
    Set<String> excludeMemberIdSet = new Set<String>();  

    excludeMemberIdSet.add(loggedInUser.Id); // exclude logged in user as there no point in seeing themselves in Member list

    String followingQuery = ' SELECT Id, NetworkId, ParentId, SubscriberId ' +
                           ' FROM EntitySubscription ' +
                           ' WHERE NetworkId = \''+ ocsugcNetworkId +'\' ' + 
                           ' AND SubscriberId = \'' + loggedInUser.Id + '\' ' +
                           ' LIMIT 999';
    
    Set<Id> followingIdSet = new Set<Id>();
    for(EntitySubscription subscription : Database.Query(followingQuery)) {
      followingIdSet.add(subscription.ParentId);
    }

      
    if(isGroupInviteMode) { // We are in group invite mode, get all the members of the group so that we can skip them
      String groupMembersQuery = '';
      groupMembersQuery = ' SELECT MemberId, CollaborationRole ' + 
                          ' FROM CollaborationGroupMember ' +
                          ' WHERE Member.IsActive = True ' +
                          ' AND CollaborationGroupId = \'' + currentGroupId + '\'' +
                          ' AND (Member.Contactid = null OR (Member.Contactid != null ' +
                                            ' AND Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\')) ';

      for(CollaborationGroupMember gpMember : Database.Query(groupMembersQuery)) {
        excludeMemberIdSet.add(gpMember.MemberId);
      }
    }
    
    if(isGroupInviteMode) { // We are in group invite mode, get all the pending members of the group so that we can skip them
      String groupPendingQuery = '';
      groupPendingQuery = ' SELECT RequesterId ' + 
                          ' FROM CollaborationGroupMemberRequest ' +
                          ' WHERE CollaborationGroupId = \'' + currentGroupId + '\'' +
                          ' AND Status = \'Pending\' ';

      for(CollaborationGroupMemberRequest gpRequest : Database.Query(groupPendingQuery)) {
        excludeMemberIdSet.add(gpRequest.RequesterId);
      }
    }

    String membersQuery = ' SELECT MemberId, Member.Name, Member.ContactId, Member.OCSUGC_User_Role__c, Member.SmallPhotoUrl, Member.Country, Member.Title, Member.CompanyName, ' +
                                ' Member.Contact.Title, Member.Contact.Account.Legal_Name__c, Member.Contact.OCSUGC_UserStatus__c, ' +
                                ' Member.Contact.OCSUGC_Affiliation_Type__c, Member.Contact.OCSUGC_Varian_Affiliation__c ' +
                          ' FROM NetworkMember ' +
                          ' WHERE MemberId NOT IN '+ OCSUGC_Utilities.getStringIds(excludeMemberIdSet) +' ' +
                          ' AND NetworkId = \''+ ocsugcNetworkId +'\' ' +
                          ' AND Member.IsActive = true ' +
                          ' AND Member.OCSUGC_User_Role__c != null ';
                           
    if(advSearchIncludeInternal || advSearchIncludeReadOnly) {
      membersQuery += ' AND (Member.ContactId = null OR (Member.ContactId != null ' +
                          ' AND Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\')) ';   
    } else {
      membersQuery += ' AND (Member.ContactId != null ' +
                          ' AND Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\') ';
    }

    if(doSearch) {
      if (String.isNotBlank(quickSearchName)) {
        String strSearchText = '\'%'  + String.escapeSingleQuotes(quickSearchName.trim()) + '%\'';
        membersQuery += ' AND Member.Name LIKE '  + strSearchText;
      }
      if (String.isNotBlank(advSearchTitle)) {
        String strSearchText = '\'%'  + String.escapeSingleQuotes(advSearchTitle.trim()) + '%\'';
        membersQuery += ' AND Member.Contact.Title LIKE '  + strSearchText;
      }
      if (String.isNotBlank(advSearchAccount)) {
        String strSearchText = '\'%'  + String.escapeSingleQuotes(advSearchAccount.trim()) + '%\'';
        membersQuery += ' AND Member.Contact.Account.Legal_Name__c LIKE '  + strSearchText;
      }
      if (String.isNotBlank(advSearchCountry)) {
        String strSearchText = '\'%'  + String.escapeSingleQuotes(advSearchCountry.trim()) + '%\'';
        membersQuery += ' AND Member.Country LIKE '  + strSearchText;
      }
    }
    
    membersQuery += ' ORDER BY Member.LastLoginDate DESC LIMIT 100';

    
    for(NetworkMember nwMember : Database.Query(membersQuery)) {
      WrapperMember member  = new WrapperMember();
      member.memberId       = nwMember.MemberId;
      member.name           = nwMember.Member.Name;
      member.title          = (nwMember.Member.Contact.Title != null?nwMember.Member.Contact.Title:nwMember.Member.Title);
      member.accountName    = (nwMember.Member.Contact.Account.Legal_Name__c != null?OCSUGC_Utilities.splitLegalName(nwMember.Member.Contact.Account.Legal_Name__c):nwMember.Member.CompanyName);
      member.country        = nwMember.Member.Country;
      member.smallPhotoUrl  = nwMember.Member.SmallPhotoUrl;

      //Check if member is community manager or community contributor
      Boolean isMemberCommManager     = OCSUGC_Utilities.isCommunityManager(ocsugcNetworkId, nwMember.MemberId, nwMember.Member.OCSUGC_User_Role__c);
      Boolean isMemberCommContributor = OCSUGC_Utilities.isCommunityContributor(ocsugcNetworkId, nwMember.MemberId, nwMember.Member.OCSUGC_User_Role__c);      
      Boolean isMemberCommModerator   = OCSUGC_Utilities.isCommunityModerator(ocsugcNetworkId, nwMember.MemberId, nwMember.Member.OCSUGC_User_Role__c);
      Boolean isMemberReadOnly        = OCSUGC_Utilities.isReadonly(nwMember.Member.OCSUGC_User_Role__c);

      //member.specialTag = '';
      if(isMemberCommManager) {
        member.specialTag = Label.OCSUGC_Varian_Employee_Community_Manager;
      }
      else if(isMemberCommContributor) {
        member.specialTag = Label.OCSUGC_Varian_Employee_Community_Contributor;
      }
      else if(isMemberCommModerator) {
        member.specialTag = Label.OCSUGC_Varian_Employee_Community_Moderator;
      }
      else if(isMemberReadOnly) {
        member.specialTag = Label.OCSUGC_Varian_ReadOnly_User;
      }
      else if(String.isNotBlank(nwMember.Member.Contact.OCSUGC_Affiliation_Type__c)) {
        if(nwMember.Member.Contact.OCSUGC_Affiliation_Type__c != 'Other') {
          member.specialTag = nwMember.Member.Contact.OCSUGC_Affiliation_Type__c;
        } else {
          member.specialTag = nwMember.Member.Contact.OCSUGC_Varian_Affiliation__c;
        }
      }

      if(followingIdSet.contains(nwMember.MemberId)) {
        member.isFollowing = true;
      } else {
        member.isFollowing = false;
      }

      //Dont add ReadOnly users to the list unless asked for specifically
      if(advSearchIncludeReadOnly && isMemberReadOnly) {
        lstMembers.add(member);
      } else if (advSearchIncludeInternal && (isMemberCommManager || isMemberCommModerator || isMemberCommContributor)) {
        lstMembers.add(member);
      } else if(!(isMemberReadOnly || ( !doSearch && (isMemberCommManager || isMemberCommModerator || isMemberCommContributor) ) )) {
        lstMembers.add(member); //Dont add other internal users to the list if we are not doing the search
      }
      
    }

    lstMembers.sort();
    membersCount = lstMembers.size();
  }

  /**
     *  @param    none
     *  @return   void
     *  method called when group is searched with the name of the Group.
     */
  public void searchMembers() {
    if(String.isBlank(quickSearchName) &&
       String.isBlank(advSearchTitle) &&
       String.isBlank(advSearchAccount) &&
       String.isBlank(advSearchCountry)) {
      if(advSearchIncludeInternal || advSearchIncludeReadOnly) {
        fetchMembers(false);
        showingSearchRslt = true;
      } else {
        return; //no search terms entered, nothing to search
      }
    }
    else {
      fetchMembers(true);
      showingSearchRslt = true;
    }
  }

  /**
   * Follow User action called via JavaScript
   * This method will make the logged in user follow the selected user in OncoPeer community
   * @return void
   */
  public void followMemberAction() {
    if(followMemberId != null) {
      if ( OCSUGC_Utilities.followUser(ocsugcNetworkId, followMemberId, loggedInUser.Id, loggedInUser.Name) ) {
        return;
      }
    }
    
    return;
  }

  /**
   * Unfollow User action called via JavaScript
   * This method will make the logged in user unfollow the selected user in OncoPeer community
   * @return void
   */
  public void unfollowMemberAction() {
    if(unfollowMemberId != null) {
      if ( OCSUGC_Utilities.unfollowUser(ocsugcNetworkId, unfollowMemberId, loggedInUser.Id, loggedInUser.Name) ) {
        return;
      }
    }
    return;
  }

  /**
   * Add selection action called via JavaScript
   * This method will add the selectedMember to the list
   * @return void
   */
  public void addSelectedMemberAction() {
    if(selectedMemberId != null) {
      if(!selectedMembers.contains(selectedMemberId)) {
        selectedMembers.add(selectedMemberId);
        totalSelected++;
      }
    }
  }

  /**
   * Remove selection action called via JavaScript
   * This method will remove the selectedMember from the list
   * @return void
   */
  public void removeSelectedMemberAction() {
    if(selectedMemberId != null) {
      if(selectedMembers.contains(selectedMemberId)) {
        selectedMembers.remove(selectedMemberId);
        totalSelected--;
      }
    }
  }

  /**
   * Invite action called via JavaScript
   * This method will invite all selectedMembers to current group
   * @return void
   */
  public void inviteMembersAction() {
    
    if(!selectedMembers.isEmpty()) {
      String query = 'SELECT FirstName, OCSUGC_Email__c, Contact.OCSUGC_Notif_Pref_InvtnJoinGrp__c ' +
                     'FROM User ' +
                     'WHERE Id IN ' + OCSUGC_Utilities.getStringIds(selectedMembers);
      
      
      for(User inviteUser :Database.query(query)) {
              //13-April-2015  Puneet Sardana  Added inviteUser.Contact condition for standard users Ref T-377271
              if((!isOAB && (inviteUser.Contact==null || inviteUser.Contact.OCSUGC_Notif_Pref_InvtnJoinGrp__c)) || isOAB){                  
                  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                  List<String> toAddress = new List<String>();
                  toAddress.add(inviteUser.OCSUGC_Email__c);
                  email.setToAddresses(toAddress);
                  //Start Here - 17 Nov 2014    Naresh K Shiwani  Added isFGAB Condition (Task Ref. T-333803)
                  if(isFGAB) {
                      email.setSubject('Invitation to join "'+currentGroup.Name+'" Online Advisory Board');
                  } else{
                      email.setSubject('You have been invited to join "'+currentGroup.Name+'" group!');
                  }

                  String body = '<center><table cellpadding="0" cellspacing="0" width="500" height="450">';
                  body += '<tbody><tr valign="top"><td style="vertical-align:top;height:100;text-align:left;background-color:#ffffff"><img src="';
                  //13-Apr-2014  Puneet Sardana  Add different image for developer cloud Ref T-377271
                  body +=  isDC ? Label.OCSDC_Email_Header_Image_Path : Label.OCSUGC_AllEmail_Header_Image_Path; 
                  
                  body += '" border="0" height="180" width="700"/></td></tr><tr valign="top">';
                  body += '<td style="height:1;background-color:#aaaaff"></td></tr><tr valign="top">';
                  body += '<td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial" height="300">';
                  body += '<table border="0" cellpadding="5" cellspacing="5" width="550" height="400">';
                  body += '<tbody><tr valign="top" height="400"><td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial">';
//                  String body = '<center><table cellpadding="0" cellspacing="0" width="500" height="450"><tbody><tr valign="top"><td style="vertical-align:top;height:100;text-align:left;background-color:#ffffff"><img src="'+Label.OCSUGC_AllEmail_Header_Image_Path+'" border="0" height="180" width="700"/></td></tr><tr valign="top"><td style="height:1;background-color:#aaaaff"></td></tr><tr valign="top"><td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial" height="300"><table border="0" cellpadding="5" cellspacing="5" width="550" height="400"><tbody><tr valign="top" height="400"><td style="color:#000000;font-size:12pt;background-color:#ffffff;font-family:arial">';
                  body += 'Hi '+inviteUser.FirstName;
                  body += '<br/><br/>';
                  //13-April-2015  Puneet Sardana  Added DC conditions Ref T-377271
                  String inWhichCommunityText  = isDC ? 'in Developer Cloud.' : 'in OncoPeer.';
                  if(isOAB) {
                      body += 'You have been invited to join <a href="'+URL.getSalesforceBaseUrl().toExternalForm() + '/' + networkName.toUpperCase() +'/OCSUGC_FGABAboutGroup?fgab=true&g='+currentGroup.Id +  '&dc=' + isDC + '">'+currentGroup.Name+'</a> '  + 'Online Advisory Board';
                      
                      body += ' To join the '+currentGroup.Name+' <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+ '/' + networkName.toUpperCase() +'/OCSUGC_FGABAboutGroup?fgab=true&g='+currentGroup.Id + '&dc=' + isDC + '">click here</a>';
                  } else {
                      //14-April-2015  Puneet Sardana  Removed hyperlink of user Name Ref T-377271 chatter
                      body += currentGroup.Owner.Name;
                      body += ' has invited you to join the group <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+ '/' + networkName.toUpperCase() +'/OCSUGC_GroupDetail?g='+currentGroup.Id + '&dc=' + isDC + '">'+currentGroup.Name+'</a> ' + inWhichCommunityText;
                      body += ' To join the '+currentGroup.Name+' <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+ '/' + networkName.toUpperCase() +'/OCSUGC_GroupDetail?g='+currentGroup.Id + '&dc=' + isDC + '">click here</a>';
                  }
                  body += '<br/><br/>';
                  body += 'Thanks,';
                  body += '<br/>';
                  body += 'Sarah Gyatso';
                  body += '<br/>';
                  body += 'Cloud Communities Manager';
//                  body += '</td></tr></tbody></table></td></tr><tr valign="top"><td style="height:0;background-color:#ffffff"></td></tr><tr valign="top"><td style="vertical-align:top;height:0;text-align:left;background-color:#ffffff"></td></tr><tr valign="top"><td style="height:0;background-color:#ffffff"></td></tr></tbody></table></center><div class="yj6qo"></div><div class="adL"><br><br></div>';
                  body += '</td></tr></tbody></table></td></tr><tr valign="top"><td style="height:0;background-color:#ffffff">';
                  body += '</td></tr><tr valign="top"><td style="vertical-align:top;height:0;text-align:left;background-color:#ffffff">';
                  body += '</td></tr><tr valign="top"><td style="height:0;background-color:#ffffff"></td></tr></tbody></table>';
                  body += '</center><div class="yj6qo"></div><div class="adL"><br><br></div>';

                  //End Here - 17 Nov 2014    Naresh K Shiwani       (Task Ref. T-333803)
                  email.setHTMLBody(body);
                  // Use Organization Wide Address
                  if(orgWideEmailAddressId != null)
                      email.setOrgWideEmailAddressId(orgWideEmailAddressId);
                  else
                    email.setSenderDisplayName('OncoPeer');
                  Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
              }
              //Start Here - 18 Nov 2014    Naresh K Shiwani  (Task Ref. T-333806)
              if(isOAB){
                  createInvitation(currentGroup.Id,inviteUser.Id);
                  OCSUGC_Intranet_Notification__c tempObj =
                                  OCSUGC_Utilities.prepareIntranetNotificationRecords(networkName,
                                          inviteUser.Id,
                                          currentGroup.Name,
                                          'has invited you to join',
                                          'ocsugc_fgababoutgroup?fgab=true&g='+currentGroup.Id);
                                          
                  // ONCO-399, adding the Expiration Date to the notification message to be displayed on User's Header Alert section         
                  Date dateTemp = Date.newInstance(system.now().year(),system.now().month(),system.now().day());
                  String expiredDate = (dateTemp.addDays(Integer.valueOf(Label.OCSUGC_InvitationExpirationDays))).format();
                  tempObj.OCSUGC_Description__c += ', Invitation Expires on ' +  expiredDate;
                  try{
                      if(tempObj!=null)
                          insert tempObj;
                  }
                  catch(Exception ex){
                      system.debug('========>>>'+ex.getMessage());
                      //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
                  }
              }
              //End Here - 18 Nov 2014    Naresh K Shiwani    (Task Ref. T-333806)
              //Start Here - 18 Nov 2014    Naresh K Shiwani  (Task Ref. T-352820)
              else {
                  createInvitation(currentGroup.Id,inviteUser.Id);
                  OCSUGC_Intranet_Notification__c tempObj =
                                  OCSUGC_Utilities.prepareIntranetNotificationRecords(networkName,
                                          inviteUser.Id,
                                          currentGroup.Name,
                                          'has invited you to join',
                                          'ocsugc_groupdetail?fgab=false&g='+currentGroup.Id);
                  try{
                      if(tempObj!=null)
                          insert tempObj;
                  }
                  catch(Exception ex){
                      system.debug('========>>>'+ex.getMessage());
                      //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
                  }
              }
              //End Here - 18 Nov 2014    Naresh K Shiwani    (Task Ref. T-352820)
          }
      }
      return;
  }


    // @description: This method inserts a group invitation record (for fgab group)
    // @param: Group Id
    // @param: User Id
    // @return: NA
    // Additional
    // 21-Nov-2014  Puneet Sardana  Original
    private void createInvitation(String groupId,String userId) {
        List<OCSUGC_Group_Invitation__c> invitationPresent = [SELECT Id
                                                              FROM OCSUGC_Group_Invitation__c
                                                              WHERE OCSUGC_Group_Id__c = :groupId
                                                              AND OCSUGC_User_Id__c = :userId];
        // If invitation not present before then insert
        if(invitationPresent==null || invitationPresent.size() ==0) {
            OCSUGC_Group_Invitation__c invitation = new OCSUGC_Group_Invitation__c();
            invitation.OCSUGC_Group_Id__c = groupId;//pass the groupId
            invitation.OCSUGC_User_Id__c = userId;//email of the invited user
            invitation.OCSUGC_Group_Invitation_Date__c = DateTime.now(); // send invitation datetime to now
            insert invitation;
        }
        // If old invitation is found, update invitation date
        else
        {
            for(OCSUGC_Group_Invitation__c oldInvite : invitationPresent) {
                oldInvite.OCSUGC_Group_Invitation_Date__c = DateTime.now(); // set invitation datetime to now
                update oldInvite;
            }
        }
    }

  public class WrapperMember implements Comparable {
      public Id memberId {get;set;}
      public String name {get;set;}   
      public String title {get;set;}  
      public String accountName {get;set;}
      public String country {get;set;}
      public String smallPhotoUrl {get;set;}
      public String specialTag {get;set;}
      public Integer followers {get;set;}
      public Integer following {get;set;}
      public Boolean isFollowing {get;set;}

      {
        isFollowing = false;
      }

      public WrapperMember() {}

      public Integer compareTo(Object objToCompare) {
          WrapperMember comparedToMember = (WrapperMember)objToCompare;

          // The return value of 0 indicates that both elements are equal.
          Integer returnValue = 0;
          if (name > comparedToMember.name) {
              // Set return value to a positive value.
              returnValue = 1;
          } else if (name < comparedToMember.name) {
              // Set return value to a negative value.
              returnValue = -1;
          }

          return returnValue;
      }
  }
}
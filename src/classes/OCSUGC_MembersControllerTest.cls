@isTest(seeAllData=true)
public class OCSUGC_MembersControllerTest {
  
   static User communityMember,communityManager,memberUsr;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static Profile admin,portal,manager;
   static List<CollaborationGroup> grpList;
   static CollaborationGroup cGroup,cGroup2;
   static Network ocsugcNetwork; 

   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'test234', '1'));
     insert lstUserInsert;
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     
     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
        lstUserInsert.add(communityManager);
        communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
        lstUserInsert.add(communityMember);
        memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member);
        lstUserInsert.add(memberUsr);
        insert lstUserInsert; 
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork(); 
        cGroup = OCSUGC_TestUtility.createGroup('Test Group','Unlisted',ocsugcNetwork.id , true);
        cGroup2 = OCSUGC_TestUtility.createGroup('Test Group1','Unlisted',Label.OCSUGC_FocusGroupAdvisoryBoard,ocsugcNetwork.id , true);
        grpList = new List<CollaborationGroup>();
        grpList.add(cGroup);
        grpList.add(cGroup2);
        if(grpList.size() > 0) {
          CollaborationGroupMember grpMember = OCSUGC_TestUtility.createGroupMember(communityManager.Id,cGroup.Id,false);
          insert grpMember;
        }
     }
   }
   
   static testMethod void testGroupCreationWithManager() {
        Test.startTest();
            PageReference pageRef = Page.OCSUGC_Members;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('g',cGroup.Id);
            OCSUGC_MembersController controller1 = new OCSUGC_MembersController();
            controller1.loadMembers();
            controller1.quickSearchName = 'Test';
            controller1.advSearchTitle = 'Test';
            controller1.advSearchAccount = 'Test';
            controller1.advSearchCountry = 'Test';
            controller1.followMemberId =  communityManager.Id;
            controller1.unfollowMemberId = memberUsr.Id;
            
            controller1.searchMembers();
            controller1.selectedMemberId = memberUsr.Id;
            controller1.currentGroupId = grpList[0].Id;
            controller1.addSelectedMemberAction(); 
            controller1.inviteMembersAction();
            
        ApexPages.currentPage().getParameters().put('g',cGroup2.Id);
        controller1.searchMembers();
            controller1.selectedMemberId = memberUsr.Id;
            controller1.currentGroupId = grpList[1].Id;
            controller1.addSelectedMemberAction(); 
            controller1.inviteMembersAction();
            
            controller1.followMemberAction();
            controller1.unfollowMemberAction();
        Test.StopTest();
    }
    
    /** 17 jan 2017 **/
    @isTest static void removeSelectedMemberAction_Test() {
      Test.StartTest();
      
        OCSUGC_MembersController cont = new OCSUGC_MembersController();
        cont.selectedMembers = new Set<String>{userInfo.getUserId()};
        cont.selectedMemberId = userInfo.getUserId();
        cont.removeSelectedMemberAction();
        cont.quickSearchName = '';
        cont.advSearchTitle = '';
        cont.advSearchAccount = '';
        cont.advSearchCountry = '';
        cont.advSearchIncludeInternal = false;
        cont.advSearchIncludeReadOnly = false;
        cont.searchMembers();
        
        cont.currentGroupId = cGroup.Id;
        cont.isGroupInviteMode = true;
        cont.fetchMembers(true);
      Test.StopTest();
    }
    
    
}
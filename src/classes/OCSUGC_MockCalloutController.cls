@isTest
global with sharing class OCSUGC_MockCalloutController implements HttpCalloutMock{
    global HttpResponse respond(HttpRequest request) {
      
      string resBody = '{'+
               ' "recommendations" : [ ] ' +
             '}' ;  
      
      Httpresponse response = new Httpresponse();
      response.setStatusCode(200);
      //response.setCode(200);
      response.setBody(resBody);
      return response;
    }
}
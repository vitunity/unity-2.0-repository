public without sharing class OCSUGC_ProfileActivityController extends OCSUGC_Base_NG {

  public Id profiledUserId {get;set;}
  public Boolean isSelfProfile {get;set;}
  public List<WrapperGroupActivityDetails> lstProfileActivity {get; set;}


  private set<String> profileUsergroupMembersIds = new set<String>();
  private set<String> loginUsergroupMembersIds   = new set<String>();
  private Datetime    currentSystemTime;

  /**
   *  Constructor
   */
    public OCSUGC_ProfileActivityController() {
    profiledUserId = ApexPages.currentPage().getParameters().get('userId');
    if(profiledUserId == NULL) {
      profiledUserId = UserInfo.getUserId();

    }

    isSelfProfile        = profiledUserId == loggedinUser.Id;
        lstProfileActivity   = new List<WrapperGroupActivityDetails>();
    currentSystemTime    = Datetime.now();
    }

  public void populateProfileActivity() {
    lstProfileActivity.clear();

    //get current system time, it will be used to calculate relative time of the activity
    currentSystemTime = Datetime.now();

    fetchAllActivities();
  }

  public void fetchAllActivities() {       

      profileUsergroupMembersIds = OCSUGC_Utilities.fetchGroupIds(null,profiledUserId, false, null,false);
      profileUsergroupMembersIds.add('Everyone');

      loginUsergroupMembersIds = OCSUGC_Utilities.fetchGroupIds(null,Userinfo.getUserId(),false,null,false);
      loginUsergroupMembersIds.add('Everyone');

      List<WrapperGroupActivityDetails> allActivitiesList = new List<WrapperGroupActivityDetails>();
      allActivitiesList.addAll(fetchEventActivity(profiledUserId));
      allActivitiesList.addAll(fetchDiscussionActivity(profiledUserId));
      allActivitiesList.addAll(fetchPollActivity(profiledUserId));
      allActivitiesList.addAll(fetchKnowledgeArtifactsActivity(profiledUserId));
      allActivitiesList.addAll(fetchPeerActivity(profiledUserId));
      allActivitiesList.addAll(fetchGroupActivity(profiledUserId));
      
      allActivitiesList.sort();
      lstProfileActivity = allActivitiesList;
  }

  private List<WrapperGroupActivityDetails> fetchEventActivity(String id) {

        Set<String> commentedActivitySet = new Set<String>();
        commentedActivitySet = getCommentedActivities('Event');

        List<WrapperGroupActivityDetails> lstEventDetails = new List<WrapperGroupActivityDetails>();
        Map<Id,WrapperGroupActivityDetails> mapEvent = new Map<Id,WrapperGroupActivityDetails>();
        String query = 'Select OCSUGC_Title__c,LastModifiedDate,OCSUGC_Views__c,OCSUGC_Group__c,OCSUGC_GroupId__c,OCSUGC_NetworkId__c,CreatedDate ' +
                       'From OCSUGC_Intranet_Content__c ' +
                       'Where (CreatedById =: id or Id in' + getStringIds(commentedActivitySet)+') and OCSUGC_Is_Deleted__c = false AND';

        if(isDCMember || isCommunityManager || isDCContributor) {
          query += ' (OCSUGC_NetworkId__c = \''+ocsugcNetworkId+'\' OR OCSUGC_NetworkId__c = \''+ ocsdcNetworkId+ '\') ';
        }
        else {
          query += ' OCSUGC_NetworkId__c = \''+ocsugcNetworkId+'\' ';
        }

        query += 'order by CreatedDate desc limit '+ 100;
        
        //query events createdBy or commented by given user in oncopeer or developer cloud
        System.debug('queryyyyyyyIntranet_Content__c===='+query);
        for (OCSUGC_Intranet_Content__c evnt : Database.query(query)){
          WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
          if(!loginUsergroupMembersIds.contains(evnt.OCSUGC_GroupId__c)){
              continue;
          }
          wgaDtls.title               = evnt.OCSUGC_Title__c;
          wgaDtls.countViewed         = (Integer)evnt.OCSUGC_Views__c;
          wgaDtls.lstModDate          = evnt.LastModifiedDate;
          wgaDtls.createdDate         = evnt.CreatedDate;
          wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(evnt.CreatedDate,currentSystemTime);
          wgaDtls.Activitytype        = 'ET';
          wgaDtls.className           = 'event';
          wgaDtls.activityId          = evnt.id;
          wgaDtls.sharedWithGroupName = evnt.OCSUGC_Group__c;
          wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_EventDetail?eventId='+evnt.id+(evnt.OCSUGC_NetworkId__c == Label.OCSDC_NetworkId?'&dc=true':'');
          if(evnt.OCSUGC_NetworkId__c == Label.OCSUGC_NetworkId) {
            wgaDtls.className += ' oncopeer';
          } else if(evnt.OCSUGC_NetworkId__c == Label.OCSDC_NetworkId) {
            wgaDtls.className += ' developerCloud';
          }
          mapEvent.put(evnt.id,wgaDtls);
          wgaDtls = null;
      }
      for(FeedItem fdItem : [Select id,parentid, LikeCount, CommentCount, Body, NetworkScope, LastModifiedDate,
                              (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes),
                              (Select CreatedById From FeedComments)
                               From FeedItem Where ParentId IN : mapEvent.keyset() order by LastModifiedDate desc]) {
                               //From FeedItem Where ParentId IN : mapEvent.keyset() AND networkscope!= 'AllNetworks' order by LastModifiedDate desc]) {
            WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
            wgaDtls                             = mapEvent.get(fdItem.ParentId);

            if(fdItem.body.equalsIgnoreCase(fdItem.parentid+'_comments'))
                wgaDtls.feedItemId              = fdItem.id;
            else
                continue;
            wgaDtls.countLikes                  = fdItem.LikeCount;
            wgaDtls.countComments               = fdItem.CommentCount;
            wgaDtls.isLikedByCurrentUser        = false;
            wgaDtls.isCommentedByCurrentUser    = false;
            for(FeedLike forflike: fdItem.FeedLikes){
                if(forflike.CreatedById == id ){
                    wgaDtls.isLikedByCurrentUser = true;
                    break;
                }
            }
            for(FeedComment forfComment: fdItem.FeedComments){
                if(forfComment.CreatedById == id ){
                    wgaDtls.isCommentedByCurrentUser = true;
                    break;
                }
            }

            if(wgaDtls.isCommentedByCurrentUser) {
                wgaDtls.className += ' commented';
            }
            else {
                wgaDtls.className += ' shared';
            }

            lstEventDetails.add(wgaDtls);
      }

      return lstEventDetails;
  }

  private List<WrapperGroupActivityDetails> fetchDiscussionActivity(String id) {

        List<WrapperGroupActivityDetails> lstDiscussionDetails = new List<WrapperGroupActivityDetails>();
        Map<Id,WrapperGroupActivityDetails> mapDiscussion = new Map<Id,WrapperGroupActivityDetails>();

        Set<String> commentedActivitySet = new Set<String>();
        commentedActivitySet = getCommentedActivities('Discussion');

        String query = 'Select OCSUGC_Views__c,OCSUGC_DiscussionId__c,OCSUGC_Description__c, LastModifiedDate,CreatedDate,OCSUGC_Group__c,OCSUGC_GroupId__c ' +
                       'From OCSUGC_DiscussionDetail__c '+
                       'Where (CreatedById =: id or OCSUGC_DiscussionId__c in' + getStringIds(commentedActivitySet)+') and OCSUGC_Is_Deleted__c = false ' +
                       'order by CreatedDate desc limit '+ 100;

        for (OCSUGC_DiscussionDetail__c discussion : Database.query(query)) {
                WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();

                if(!loginUsergroupMembersIds.contains(discussion.OCSUGC_GroupId__c)){
                    continue;
                }
                wgaDtls.title               = truncateDiscussionBody(discussion.OCSUGC_Description__c);
                wgaDtls.countViewed         = (Integer)discussion.OCSUGC_Views__c;
                wgaDtls.lstModDate          = discussion.LastModifiedDate;
                wgaDtls.createdDate         = discussion.CreatedDate;
                wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(discussion.CreatedDate,currentSystemTime);
                wgaDtls.Activitytype        = 'DC';
                wgaDtls.className           = 'discussion';
                wgaDtls.activityId          = discussion.id;
                wgaDtls.sharedWithGroupName = discussion.OCSUGC_Group__c;
                //wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_DiscussionDetail?Id='+discussion.OCSUGC_DiscussionId__c+(isDC?'&dc=true':'');
                
                mapDiscussion.put(discussion.OCSUGC_DiscussionId__c,wgaDtls);
                wgaDtls = null;

        }

        for(FeedItem fdItem : [select LastModifiedDate,CreatedDate,Body, NetworkScope,LikeCount, CommentCount,
                              (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes),
                              (Select CreatedById From FeedComments)
                               From FeedItem Where id IN : mapDiscussion.keyset() order by CreatedDate desc]) {
          WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
          wgaDtls                       = mapDiscussion.get(fdItem.id);

          wgaDtls.feedItemId            = fdItem.id;
          wgaDtls.countLikes            = fdItem.LikeCount;
          wgaDtls.countComments         = fdItem.CommentCount;
          wgaDtls.isLikedByCurrentUser  = false;
          //wgaDtls.isLikedByCurrentUser  = true; //its just for testing, change value after test
          wgaDtls.isCommentedByCurrentUser = false;
          
          wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_DiscussionDetail?Id='+fdItem.id+(fdItem.NetworkScope == Label.OCSDC_NetworkId?'&dc=true':'');
          
          for(FeedLike forflike: fdItem.FeedLikes){
            if(forflike.CreatedById == id ){
                wgaDtls.isLikedByCurrentUser = true;
                break;
            }
           }
           for(FeedComment forfComment: fdItem.FeedComments){
                if(forfComment.CreatedById == id ){
                    wgaDtls.isCommentedByCurrentUser = true;
                    break;
                }
            }

            if(wgaDtls.isCommentedByCurrentUser) {
                wgaDtls.className += ' commented';
            }
            else {
                wgaDtls.className += ' shared';
            }

            // add "oncopeer" or "developerCloud" class to className
            if(fdItem.NetworkScope == ocsugcNetworkId) {
              wgaDtls.className += ' oncopeer';
            } else if(fdItem.NetworkScope == ocsdcNetworkId) {
              wgaDtls.className += ' developerCloud';
            }

            if((fdItem.NetworkScope == ocsugcNetworkId) || 
              ((fdItem.NetworkScope == ocsdcNetworkId) && (isDCMember || isCommunityManager || isDCContributor))) {
                lstDiscussionDetails.add(wgaDtls);
            }
        }
        return lstDiscussionDetails;
    
  }

  private List<WrapperGroupActivityDetails> fetchPollActivity(String id) {

        List<WrapperGroupActivityDetails> lstPollDetails = new List<WrapperGroupActivityDetails>();
        Map<Id,WrapperGroupActivityDetails> mapPoll = new Map<Id,WrapperGroupActivityDetails>();

        Set<String> commentedActivitySet = new Set<String>();
        commentedActivitySet = getCommentedActivities('Poll');

        String query = 'Select OCSUGC_Views__c,OCSUGC_Poll_Id__c,OCSUGC_Poll_Question__c, LastModifiedDate,OCSUGC_Group_Name__c,OCSUGC_Group_Id__c, OCSUGC_Likes__c, OCSUGC_Comments__c,CreatedDate ' +
                       'From OCSUGC_Poll_Details__c '+
                       'where (CreatedById =: id or OCSUGC_Poll_Id__c in' + getStringIds(commentedActivitySet)+') and OCSUGC_Is_Deleted__c = false '+
                       'order by CreatedDate desc limit '+ 100;

        for (OCSUGC_Poll_Details__c poll : Database.query(query)) {
                WrapperGroupActivityDetails wgaDtls  = new WrapperGroupActivityDetails();

                if(!loginUsergroupMembersIds.contains(poll.OCSUGC_Group_Id__c)){
                    continue;
                }

                wgaDtls.title               = truncateDiscussionBody(poll.OCSUGC_Poll_Question__c);
                wgaDtls.countViewed         = (Integer)poll.OCSUGC_Views__c;
                wgaDtls.lstModDate          = poll.LastModifiedDate;
                wgaDtls.createdDate         = poll.CreatedDate;
                wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(poll.CreatedDate,currentSystemTime);
                wgaDtls.Activitytype        = 'PL';
                wgaDtls.className           = 'poll';
                wgaDtls.activityId          = poll.id;
                wgaDtls.sharedWithGroupName = poll.OCSUGC_Group_Name__c;
                //wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_FGABPollDetail?Id='+poll.OCSUGC_Poll_Id__c+(isDC?'&dc=true':'');
                mapPoll.put(poll.OCSUGC_Poll_Id__c,wgaDtls);
                wgaDtls = null;

        }
    //Doubt :can I restrict this query to Oncopeer and DC networkscope
        for(FeedItem fdItem : [select LastModifiedDate,Body, NetworkScope,LikeCount, CommentCount,
                              (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes),
                              (Select CreatedById From FeedComments)
                               From FeedItem Where id IN : mapPoll.keyset() order by LastModifiedDate desc]) {
          WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
          wgaDtls                       = mapPoll.get(fdItem.id);

          wgaDtls.feedItemId            = fdItem.id;
          wgaDtls.countLikes            = fdItem.LikeCount;
          wgaDtls.countComments         = fdItem.CommentCount;
          wgaDtls.isLikedByCurrentUser  = false;
          wgaDtls.isCommentedByCurrentUser = false;
          wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_FGABPollDetail?Id='+fdItem.id+(fdItem.NetworkScope == Label.OCSDC_NetworkId?'&dc=true':'');
          
          for(FeedLike forflike: fdItem.FeedLikes){
            if(forflike.CreatedById == id ){
                wgaDtls.isLikedByCurrentUser = true;
                break;
            }
           }
           for(FeedComment forfComment: fdItem.FeedComments){
                if(forfComment.CreatedById == id ){
                    wgaDtls.isCommentedByCurrentUser = true;
                    break;
                }
            }

            if(wgaDtls.isCommentedByCurrentUser) {
                wgaDtls.className += ' commented';
            }
            else {
                wgaDtls.className += ' shared';
            }

            // add "oncopeer" or "developerCloud" class to className
            if(fdItem.NetworkScope == ocsugcNetworkId) {
              wgaDtls.className += ' oncopeer';
            } else if(fdItem.NetworkScope == ocsdcNetworkId) {
              wgaDtls.className += ' developerCloud';
            }

            if((fdItem.NetworkScope == ocsugcNetworkId) || 
              ((fdItem.NetworkScope == ocsdcNetworkId) && (isDCMember || isCommunityManager || isDCContributor))) {
                lstPollDetails.add(wgaDtls);
            }
          }
        return lstPollDetails;
    }

    private List<WrapperGroupActivityDetails> fetchKnowledgeArtifactsActivity(String id) {
        System.debug('============================fetchKnowledgeArtifactsActivity');

        List<WrapperGroupActivityDetails> lstKADetails = new List<WrapperGroupActivityDetails>();
        Map<Id,WrapperGroupActivityDetails> mapKnwlgArt = new Map<Id,WrapperGroupActivityDetails>();

        Set<String> commentedActivitySet = new Set<String>();
        commentedActivitySet = getCommentedActivities('Knowledge');
         
        String query = 'Select OCSUGC_Title__c,OCSUGC_NetworkId__c, LastModifiedDate,OCSUGC_views__c,CreatedDate,OCSUGC_Group__c,OCSUGC_Group_Id__c,OCSUGC_Status__c ' +
                       'From OCSUGC_Knowledge_Exchange__c ' +
                       'Where (CreatedById =: id or Id in' + getStringIds(commentedActivitySet)+') And OCSUGC_Status__c !=\'Rejected\' AND OCSUGC_Is_Deleted__c = false AND';
        
        if(isDCMember || isCommunityManager || isDCContributor) {
          query += ' (OCSUGC_NetworkId__c = \''+ocsugcNetworkId+'\' OR OCSUGC_NetworkId__c = \''+ ocsdcNetworkId+ '\') ';
        }
        else {
          query += ' OCSUGC_NetworkId__c = \''+ocsugcNetworkId+'\' ';
        }

        query += 'order by CreatedDate desc limit '+ 100;
        
                
        for (OCSUGC_Knowledge_Exchange__c knwlgArt : Database.query(query)){
            WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
            wgaDtls.className ='';

            if(!(loginUsergroupMembersIds.contains(knwlgArt.OCSUGC_Group_Id__c))){
                continue;
            }

            if(knwlgArt.OCSUGC_Status__c == 'Pending Approval'){
              wgaDtls.title               = '<font color="red">' + knwlgArt.OCSUGC_Title__c + '</font>';
              wgaDtls.className = 'pending';

              if(id != loggedinUser.Id) { //Skip pending activities for all else and only show it to logged in user
                continue;
              }
            }
            else
            {
              wgaDtls.title               = knwlgArt.OCSUGC_Title__c;
            }

            wgaDtls.countViewed         = (Integer)knwlgArt.OCSUGC_Views__c;
            wgaDtls.lstModDate          = knwlgArt.LastModifiedDate;
            wgaDtls.createdDate         = knwlgArt.CreatedDate;
            wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(knwlgArt.CreatedDate,currentSystemTime);
            wgaDtls.Activitytype        = 'KA';
            wgaDtls.className           += ' knowledgefeed';
            wgaDtls.activityId          = knwlgArt.Id;
            wgaDtls.sharedWithGroupName = knwlgArt.OCSUGC_Group__c;
            wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId='+knwlgArt.id+(knwlgArt.OCSUGC_NetworkId__c==Label.OCSDC_NetworkId?'&dc=true':'');
            if(knwlgArt.OCSUGC_NetworkId__c == ocsugcNetworkId) {
	            wgaDtls.className += ' oncopeer';
	          } else if(knwlgArt.OCSUGC_NetworkId__c == ocsdcNetworkId) {
	            wgaDtls.className += ' developerCloud';
            }
            mapKnwlgArt.put(knwlgArt.id,wgaDtls);  
            wgaDtls = null;
        }
                
        for(FeedItem fdItem : [Select id,parentid, LikeCount, CommentCount, Body, NetworkScope, LastModifiedDate,
                              (Select Id, FeedItemId, CreatedById, CreatedDate, InsertedById From FeedLikes),
                              (Select CreatedById From FeedComments)
                               From FeedItem Where ParentId IN : mapKnwlgArt.KeySet() order by LastModifiedDate desc]) {
              WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
              wgaDtls                             = mapKnwlgArt.get(fdItem.ParentId);

              wgaDtls.feedItemId                  = fdItem.id;
              wgaDtls.countLikes                  = fdItem.LikeCount;
              wgaDtls.countComments               = fdItem.CommentCount;
              wgaDtls.isLikedByCurrentUser        = false;
              wgaDtls.isCommentedByCurrentUser    = false;
              for(FeedLike forflike: fdItem.FeedLikes){
                  if(forflike.CreatedById == id ){
                      wgaDtls.isLikedByCurrentUser = true;
                      break;
                  }
              }
              for(FeedComment forfComment: fdItem.FeedComments){
                  if(forfComment.CreatedById == id ){
                      wgaDtls.isCommentedByCurrentUser = true;
                      break;
                  }
              }

              if(wgaDtls.isCommentedByCurrentUser) {
                  wgaDtls.className += ' commented';
              }
              else {
                  wgaDtls.className += ' shared';
              }
			  
      			  lstKADetails.add(wgaDtls);
        }
        return lstKADetails;
  }

  @testVisible private List<WrapperGroupActivityDetails> fetchPeerActivity(String id) {
    List<WrapperGroupActivityDetails>   lstPeerDetails = new List<WrapperGroupActivityDetails>();
    Map<Id,WrapperGroupActivityDetails> mapPeerDetails = new Map<Id,WrapperGroupActivityDetails>();
     
    String query = 'Select ParentId, Parent.Name, CreatedDate, NetworkId '+
                   'From EntitySubscription '+
                   'Where SubscriberId =:id AND';
    
    query += ' NetworkId = \''+ocsugcNetworkId+'\' '; //we always follow someone only in OncoPeer so this is good enough
    query += 'order by CreatedDate desc limit '+ 100;

    for (EntitySubscription entSub : Database.query(query)){
      WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
      wgaDtls.title          = entSub.Parent.Name;
      wgaDtls.createdDate    = entSub.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(entSub.CreatedDate,currentSystemTime);
      wgaDtls.Activitytype   = 'GM';
      wgaDtls.countComments  = -1;
      wgaDtls.countLikes     = -1;
      wgaDtls.countViewed    = -1;
      wgaDtls.className      = 'person followed';
      wgaDtls.detailLink     = Site.getPathPrefix()+'/OCSUGC_UserProfile?userId='+entSub.ParentId;
             
      if(entSub.networkid == ocsugcNetworkId) {
        wgaDtls.className += ' oncopeer';
      } else if(entSub.networkid == ocsdcNetworkId) {
        wgaDtls.className += ' developerCloud';
      }

      mapPeerDetails.put(entSub.ParentId,wgaDtls);
    }

    for (NetworkMember member:[Select MemberId, Member.Name, Member.SmallPhotoURL, Member.Contact.Title
                                   From NetworkMember
                                   Where MemberId IN :mapPeerDetails.KeySet()
                                   And NetworkId =:ocsugcNetworkId
                                   And Member.IsActive = True
                                   And Member.OCSUGC_User_Role__c != Null
                                   And Member.Contact.OCSUGC_UserStatus__c =:Label.OCSUGC_Approved_Status])
    {
      WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();
      wgaDtls                = mapPeerDetails.get(member.MemberId);

      wgaDtls.imageLink      = member.Member.SmallPhotoURL;

      lstPeerDetails.add(wgaDtls);
    }

    return lstPeerDetails;
  }

  private List<WrapperGroupActivityDetails> fetchGroupActivity(String id) {
    List<WrapperGroupActivityDetails>   lstGroupDetails = new List<WrapperGroupActivityDetails>();
    Map<Id,WrapperGroupActivityDetails> mapGroupDetails = new Map<Id,WrapperGroupActivityDetails>();
     
    String query =  'Select CollaborationGroupId, MemberId, CollaborationGroup.Name, CollaborationGroup.SmallPhotoURL, CollaborationGroup.OwnerId, CollaborationGroup.NetworkId, CreatedDate ' +
                    'From CollaborationGroupMember ' +
                    'Where CollaborationGroup.IsArchived = false ' +
                    'AND MemberId =:id ' +
                    'AND CollaborationGroup.CollaborationType != \'Unlisted\' AND';
    
    if(isDCMember || isCommunityManager || isDCContributor) {
      query += ' (CollaborationGroup.NetworkId = \''+ocsugcNetworkId+'\' OR CollaborationGroup.NetworkId = \''+ ocsdcNetworkId+ '\') ';
    }
    else {
      query += ' CollaborationGroup.NetworkId = \''+ocsugcNetworkId+'\' ';
    }

    query += 'order by CreatedDate desc limit '+ 100;

    for(CollaborationGroupMember grpMem : Database.query(query)) {
      WrapperGroupActivityDetails wgaDtls = new WrapperGroupActivityDetails();                                                          
      wgaDtls.title               = grpMem.CollaborationGroup.Name;
      wgaDtls.createdDate         = grpMem.CreatedDate;
      wgaDtls.relativeCreatedTime = OCSUGC_Utilities.GetRelativeTimeString(grpMem.CreatedDate,currentSystemTime);
      wgaDtls.Activitytype        = 'GP';
      wgaDtls.className           = 'group';
      wgaDtls.countComments       = -1;
      wgaDtls.countLikes          = -1;
      wgaDtls.countViewed         = -1;
      wgaDtls.isGroupOwner        = grpMem.CollaborationGroup.OwnerId == id;
      wgaDtls.imageLink           = grpMem.CollaborationGroup.SmallPhotoUrl;
      wgaDtls.detailLink          = Site.getPathPrefix()+'/OCSUGC_GroupDetail?g='+grpMem.CollaborationGroupId+(grpMem.CollaborationGroup.NetworkId==ocsdcNetworkId?'&dc=true':'');
      
      if(grpMem.CollaborationGroup.NetworkId == ocsugcNetworkId) {
        wgaDtls.className += ' oncopeer';
      } else if(grpMem.CollaborationGroup.NetworkId == ocsdcNetworkId) {
        wgaDtls.className += ' developerCloud';
      }

      if(!mapGroupDetails.containsKey(grpMem.CollaborationGroupId)) { //prevent duplicate group entries
        mapGroupDetails.put(grpMem.CollaborationGroupId, wgaDtls);
        lstGroupDetails.add(wgaDtls); 
      }      
           
    }

    return lstGroupDetails;
  }

  @testVisible private String getStringIds(set<String> items) {
        if(items.IsEmpty())
            return '(\'\')';

        String str;
        for(String item :items) {
            if(item != null) {
                if(str == null) {
                    str = '(\''+item+'\'';
                } else {
                    str += ',\''+item+'\'';
                }
            }
        }
        str += ')';
        return str;
  }

  @testVisible private String truncateDiscussionBody(String body) {
      if(body != null && body.length() > 200) {
          body = body.substring(0,200)+'...';
      }
      return body;
  }

  public Set<String> getCommentedActivities(String ActivityType){
        System.debug('============================getCommentedActivities');

        Set<String> activityIds = new Set<String>();
        Set<String> commentedActivityIds = new Set<String>();
        if(ActivityType.equals('Event')){
            for(OCSUGC_Intranet_Content__c event: [Select id From OCSUGC_Intranet_Content__c Where RecordType.Name = 'Events' And OCSUGC_GroupId__c IN :profileUsergroupMembersIds]){
                activityIds.add(event.id);
            }
        } else if(ActivityType.equals('Knowledge')){
            for(OCSUGC_Knowledge_Exchange__c knowledge: [Select id From OCSUGC_Knowledge_Exchange__c Where OCSUGC_Group_Id__c IN :profileUsergroupMembersIds]){
                activityIds.add(knowledge.id);
            }
        } else if(ActivityType.equals('Discussion')){
            for(OCSUGC_DiscussionDetail__c discussion: [Select OCSUGC_DiscussionId__c From OCSUGC_DiscussionDetail__c Where OCSUGC_GroupId__c IN :profileUsergroupMembersIds]){
                activityIds.add(discussion.OCSUGC_DiscussionId__c);
            }
        } else if(ActivityType.equals('Poll')){
            for(OCSUGC_Poll_Details__c poll: [Select OCSUGC_Poll_Id__c From OCSUGC_Poll_Details__c Where OCSUGC_Group_Id__c IN :profileUsergroupMembersIds]){
                activityIds.add(poll.OCSUGC_Poll_Id__c);
            }
        }
        String query = 'Select id, ParentId, (Select CreatedById From FeedComments) From FeedItem Where';
                        if(ActivityType.equals('Discussion') || ActivityType.equals('Poll'))
                            query += ' Id IN : activityIds';
                        else
                            query += ' ParentId IN : activityIds';

        System.debug('queryFeedCommenttttt='+query);
        for(FeedItem commentedfeedItem: Database.query(query)) {
            //NetworkScope Condition for feeditem
            for(FeedComment forfComment: commentedfeedItem.FeedComments){
                if(forfComment.CreatedById == profiledUserId ){
                    if(ActivityType.equals('Discussion') || ActivityType.equals('Poll'))
                        commentedActivityIds.add(commentedfeedItem.Id);
                    else
                        commentedActivityIds.add(commentedfeedItem.ParentId);
                    break;
                }
            }
        }
        return commentedActivityIds;
  }


  public class WrapperGroupActivityDetails implements Comparable {
        public String feedItemId                {get;set;}
        public String className                 {get;set;}
        public String title                     {get;set;}
        public String Activitytype              {get;set;}
        public Datetime createdDate             {get;set;}
        public String relativeCreatedTime       {get;set;}
        public Datetime lstModDate              {get;set;}
        public Integer countLikes               {get;set;}
        public Integer countViewed              {get;set;}
        public Integer countComments            {get;set;}
        public String detailLink                {get;set;}
        public Boolean isLikedByCurrentUser     {get;set;}
        public String imageLink                 {get;set;}
        public String activityId                {get;set;}
        public Boolean isCommentedByCurrentUser {get;set;}
        public String sharedWithGroupName       {get;set;}
        public Boolean isGroupOwner             {get;set;}
        public User user {get;set;}
        public Boolean isFollow {get;set;}
        public String followers {get;set;}
        public String following {get;set;}
        public String knowledge {get;set;}
    
        public WrapperGroupActivityDetails() {}

        public Integer compareTo(Object objToCompare) {
            WrapperGroupActivityDetails obj2 = (WrapperGroupActivityDetails)objToCompare;

            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (createdDate > obj2.createdDate) {
                // Set return value to a negative value.
                returnValue = -1;
            } else if (createdDate < obj2.createdDate) {
                // Set return value to a positive value.
                returnValue = 1;
            }

            return returnValue;
        }
    }
}
//
// Test Class for OCSUGC_ProfileActivityController class
//
// 9 March, 2016 Shital Bhujbal
@isTest
private class OCSUGC_ProfileActivityControllerTest {

    static Profile admin,portal;
    static Id profileId;
    static User adminUsr1,adminUsr2, memberUsr;
    static List<User> lstUserInsert;
    static CollaborationGroup publicGroup,privateGroup,fgabGroup;
    static List<CollaborationGroup> lstGroupInsert;
    static List<CollaborationGroupMember> lstGroupMemberInsert;
    static CollaborationGroupMember groupMember1, groupMember2;
    static Network ocsugcNetwork;
    static OCSUGC_Intranet_Content__c testContent;
    static OCSUGC_Tags__c tag;
    static OCSUGC_Knowledge_Exchange__c knowExchange;
    static OCSUGC_Knowledge_Tag__c kTag;
    static List<FeedItem> feeds;
    static FeedItem feedItem1, feedItem2;
    static OCSUGC_Poll_Details__c pollDetail;
    static OCSUGC_PollDetail_Tags__c pollTag;
    static OCSUGC_DiscussionDetail__c discussion;
    static OCSUGC_FeedItem_Tag__c feedItemTag;
    static EntitySubscription entitiySub;
    static List<PermissionSetAssignment> lstPermissionSetAssignments = new List<PermissionSetAssignment>();

    static{
      
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        admin = OCSUGC_TestUtility.getAdminProfile();
        profileId = admin.Id;
        lstUserInsert = new List<User>();
        lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', 'Varian Community Member'));
        lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
        insert lstUserInsert;   
        PermissionSet memberPermSet = OCSUGC_TestUtility.getMemberPermissionSet();
        //-------shital code start------------
        //Assign OCSUGC and DC member permission set to adminUser1
        List<PermissionSet> memberPermissionSets = [SELECT Id FROM PermissionSet WHERE Name LIKE '%Community_Members_Permissions%' LIMIT 2];
        system.debug('*********memberPermissionSets*******'+memberPermissionSets);
        for(PermissionSet ps: memberPermissionSets){
          
          PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
            perSetAssign.PermissionSetId = ps.Id;
            perSetAssign.AssigneeId = adminUsr1.Id;
            lstPermissionSetAssignments.add(perSetAssign);
        }
        insert lstPermissionSetAssignments;
        //system.assertEquals(1,2);
        //-------shital code end------------ 
    }
    
    public static FeedItem testData() {
  
	    lstGroupInsert = new List<CollaborationGroup>();
	    lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
	    lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
	    lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
	    insert lstGroupInsert;
	  
	    //preparing content data to insert through adminuser1
	    testContent = OCSUGC_TestUtility.createIntranetContent('testContent', 'Events',null,'Team','Test');
	    testContent.OCSUGC_Bookmark_Id__c = 'hello test';
	    testContent.OCSUGC_Application_Grant_Access__c = true;
	    testContent.OCSUGC_Status__c = 'Approved';
	    testContent.OCSUGC_Posted_By__c =UserInfo.getUserId();
	    testContent.RecordTypeId = Schema.SObjectType.OCSUGC_Intranet_Content__c.getRecordTypeInfosByName().get('Events').getRecordTypeId();
	    testContent.OCSUGC_Group__c = 'Everyone';
	    testContent.OCSUGC_Is_Deleted__c = false;
	    testContent.OCSUGC_Event_End_Date__c = Date.today().addDays(5);
	    testContent.OCSUGC_NetworkId__c = ocsugcNetwork.Id;
	    testContent.OCSUGC_GroupId__c = 'Everyone';
	    testContent.OCSUGC_Location__c = 'test summary test summary test summary test summary test summary test summary test summary ';
	    insert testContent;
	  
	    //inserting tag
	    tag = OCSUGC_TestUtility.createTags('TestTag');
	    insert tag;
	  
	    //inserting knowledge exchange
	    knowExchange = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id, publicGroup.Name,false);
	    knowExchange.OCSUGC_Status__c = 'Approved';
	    knowExchange.OCSUGC_Group_Id__c = 'Everyone';
	    knowExchange.OCSUGC_Is_Deleted__c = false;
	    knowExchange.OCSUGC_NetworkId__c = ocsugcNetwork.Id;
	    insert knowExchange;
	    system.debug('---knowExchange---' + knowExchange);
	  
	    //inserting Knowledge Tag
	    kTag = new OCSUGC_Knowledge_Tag__c(OCSUGC_Tags__c = tag.Id, OCSUGC_Knowledge_Exchange__c = knowExchange.Id);
	    insert kTag;
	  
	    //initializing feedItem List 
	    feeds = new List<FeedItem>();
	    
	    feedItem1 = OCSUGC_TestUtility.createFeedItem(testContent.Id, false);
	    feedItem1.Body = testContent.Id + '_comments';
	    feedItem1.CreatedById = adminUsr1.Id;
	    feeds.add(feedItem1);
	  
	    feedItem2 = OCSUGC_TestUtility.createFeedItem(knowExchange.Id, false);
	    feedItem2.Body = knowExchange.Id + '_comments';
	    feedItem2.CreatedById = adminUsr1.Id;
	    feeds.add(feedItem2);
	    insert feeds;
	    
	    //inserting PollDetail
	    //signature of createPollData in TestUtility  - createPollDetail(Id publicGroup.Id, String grpName, ID feedId, String pollBody, Boolean isInsert)
	    pollDetail = OCSUGC_TestUtility.createPollDetail(publicGroup.Id, publicGroup.Name, feedItem2.Id, 'testPollQuestion', false);
	    pollDetail.OCSUGC_Comments__c = 5;
	    pollDetail.OCSUGC_Is_FGAB_Poll__c = true;
	    pollDetail.OCSUGC_Views__c = 4;
	    pollDetail.OCSUGC_Likes__c = 5;
	    insert pollDetail; 
	     
	    //inserting OCSUGC_PollDetail_Tags__c
	    //signature of  Method to create OCSUGC_PollDetail_Tags__c  in TestUtility - public static OCSUGC_PollDetail_Tags__c createPollDetailTag(Id tagId, Id feedId, Boolean isInsert)
	    pollTag = OCSUGC_TestUtility.createPollDetailTag(tag.Id, feedItem2.Id, true);
	  
	    //creating OCSUGC_DiscussionDetail__c
	    //signature of Method to create OCSUGC_DiscussionDetail__c in testUtility - OCSUGC_DiscussionDetail__c createDiscussion(String groupId,String discussionId,Boolean isInsert)
	    discussion = OCSUGC_TestUtility.createDiscussion(publicGroup.Id, feedItem2.Id, false);
	    discussion.OCSUGC_Comments__c = 5;
	    discussion.OCSUGC_Group__c = 'TestPublic';
	    discussion.OCSUGC_Views__c = 4;
	    discussion.OCSUGC_Likes__c = 5;     
	    insert discussion;
	    
	    //creating FeedItem tag
	    feedItemTag = new OCSUGC_FeedItem_Tag__c(OCSUGC_Tags__c = tag.Id, OCSUGC_FeedItem_Id__c = feedItem2.Id);
	    insert feedItemTag;  
	    
	    return feedItem2;
	  }
    
    public static testmethod void test_OCSUGC_ProfileActivityController_likeCount() {
      System.runAs(adminUsr1) {
      FeedItem feedItems = OCSUGC_ProfileActivityControllerTest.testData();
      ApexPages.currentPage().getParameters().put('userId', adminUsr1.Id);
            OCSUGC_ProfileActivityController ctrl =  new OCSUGC_ProfileActivityController();
            ctrl.populateProfileActivity();
            //ctrl.grpActivityId = OCSUGC_Knowledge_Exchange__c.sObjectType.getDescribe().getKeyPrefix();
            //ctrl.fdItemId = feedItems.Id;
            //ctrl.likeCount();
            //ctrl.unlikeCount();
    }
 } 
 
 	@isTest static void getStringIdsTest() {
 		Test.StartTest();
 			
 			set<String> items = new set<String>{'Apple', 'Bat', 'Cat'};
 			OCSUGC_ProfileActivityController ctrl =  new OCSUGC_ProfileActivityController();
 			ctrl.getStringIds(items);
 			String Body = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+
 						  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 			ctrl.truncateDiscussionBody(Body);
 		Test.StopTest();
 	}
 	
 	@isTest static void fetchPeerActivity_test() {
 		Test.StartTest();
 			ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
 			EntitySubscription subs = OCSUGC_TestUtility.createEntitySubscription(ocsugcNetwork.Id, adminUsr1.Id, adminUsr2.Id, false);
 			OCSUGC_ProfileActivityController ctrl =  new OCSUGC_ProfileActivityController();
 			ctrl.fetchPeerActivity(adminUsr2.Id);
 			
 		Test.StopTest();
 	}
  
}
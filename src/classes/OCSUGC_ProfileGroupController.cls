/*
	13 April, 2014  Shital Bhujbal   Added method joinGroupAction(),requestJoinGroupAction      Ref ONCO-478
*/
public class OCSUGC_ProfileGroupController extends OCSUGC_Base_NG{
    
    @TestVisible private List<Id> grpIds {get;set;}
    public List<GroupWrapper> groups {get;set;}                 //  List of group wrapper to be displayed on Page
    public Id selectedGroupId {get;set;}                        // Id of group to be joined/delete/leave by profiled-user
    public List<CollaborationGroup> recommendedGroups {get;set;}
    @TestVisible GroupWrapper grpWrapper;
    @TestVisible String profiledId;
    public OCSUGC_CommunityData cdata {get;set;}                       // Deserialize Recommendation Group Data 
	private static final String OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER = 'OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER';
    @TestVisible private Id orgWideEmailAddressId;
    /**
     *  Constructor
     */
    public OCSUGC_ProfileGroupController() {
        profiledId = fetchProfiledUserId( ApexPages.currentPage().getParameters().get('userId') );
        grpIds = new List<Id>();
        grpIds = CollaborationGroupIds();
        groups = new List<GroupWrapper>();   
        orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();   
    }
        
    /**
     *
     */
    @TestVisible private List<GroupWrapper> fetchProfiledGroups() {
        groups.clear();
        profiledId = fetchProfiledUserId( ApexPages.currentPage().getParameters().get('userId') );
        
        String grpMemberQuery = ' SELECT MemberId, CollaborationGroupId, Member.isActive, CollaborationGroup.IsArchived, CollaborationGroup.NetworkId '+
                                ' FROM CollaborationGroupMember ' + 
                                ' WHERE MemberId =\'' +  profiledId + '\'' + ' And CollaborationGroupId IN: grpIds '+
                                ' AND Member.isActive = true ' + 
                                ' AND CollaborationGroup.IsArchived = false AND CollaborationGroup.NetworkId != null ' +
                                ' AND (Member.Contactid = null OR (Member.Contactid != null ' +
                                ' AND Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\'))';
                                    
        Set<Id> collGrpMemIdSet = new Set<Id>();
        for(CollaborationGroupMember cgm : Database.Query(grpMemberQuery)) {
            collGrpMemIdSet.add(cgm.CollaborationGroupId);
        }
        
        String grpQuery =   'SELECT Id, Name, Description, CollaborationType, FullPhotoUrl, SmallPhotoUrl, IsArchived, MemberCount, NetworkId, OwnerId, Owner.Name, ' +
        					' (Select RequesterId From GroupMemberRequests  Where Status=\'Pending\' AND RequesterId = \'' + loggedInUser.Id + '\'), ' +
                            ' (Select MemberId From GroupMembers '
                                   +' Where Member.IsActive = True '
                                   + ' And (Member.Contactid = null '
                                   + ' OR (Member.Contactid != null '
                                   + ' And Member.Contact.OCSUGC_UserStatus__c = \'' + Label.OCSUGC_Approved_Status + '\'))) ' +
                            ' FROM CollaborationGroup WHERE IsArchived = false AND Id IN: collGrpMemIdSet AND NetworkId != null';
        grpQuery += ' LIMIT 100 ';
        
        system.debug(' == **' + grpQuery);
        for(CollaborationGroup gp : Database.Query(grpQuery)) {
            gp.Description = validateSummary(gp.Description);
            grpWrapper = new GroupWrapper(gp);
            
            /** Setting class name on basis on NetworkId */
            if(gp.NetworkId == ocsugcNetworkId) {
                grpWrapper.className = 'oncopeer';
            } else if(gp.NetworkId == ocsdcNetworkId) {
                grpWrapper.className = 'developerCloud';
                grpWrapper.isDCGroup = true;
            }
            /** Class name on basis of OwnerId */
            if(gp.OwnerId == profiledId) 
                grpWrapper.roleClassName = 'owned';
            else
                grpWrapper.roleClassName = 'member';

            if((gp.NetworkId == ocsugcNetworkId) || 
              ((gp.NetworkId == ocsdcNetworkId) && (isDCMember || isCommunityManager || isDCContributor))) {
                groups.add(grpWrapper);
            }
            
            //Added by SHital
            if(gp.GroupMemberRequests.size() > 0) {
        		grpWrapper.requestAlreadySent = true;
      		}
        }

        groups.sort(); //sort by name
        return groups;
    }
    
    /**
     *  method will called when profiled user leave group
     */
    public PageReference leaveGroupAction() {
        try {
            CollaborationGroupMember leaveGrpMem;
            if(selectedGroupId != null) {
                leaveGrpMem = [SELECT Id, CollaborationGroupId 
                                                        FROM CollaborationGroupMember 
                                                        //WHERE MemberId =: profiledId
                                                        WHERE MemberId =:loggedInUser.Id
                                                                AND CollaborationGroupId =:selectedGroupId
                                                        LIMIT 1];
                delete leaveGrpMem;
            }
            return null; //change made by Shital Bhujbal ONCO-478
            /*PageReference p = Page.OCSUGC_UserProfile;
            p.getParameters().put('userId', Apexpages.currentPage().getParameters().get('userId'));
            p.setRedirect(true);    
            return p;*/
        } catch(Exception e) {
            system.debug(' EXCEPTION ' + e.getMessage() + e.getLineNumber());
            return null;
        }
    }
    
    /**
     *  method will be called when profiled user request to delete the group, 
     *  Group will not be deleted but marked as Archived in System
     */
    public PageReference deleteGroupAction() {
        try {
            CollaborationGroup delGrpMem;
            if(selectedGroupId != null) {
                delGrpMem = [   SELECT Id, Name, IsArchived 
                                                    FROM CollaborationGroup 
                                                    WHERE Id =:selectedGroupId
                                                    LIMIT 1];
                delGrpMem.IsArchived = true;
                update delGrpMem;
            }
            PageReference p = Page.OCSUGC_UserProfile;
            p.getParameters().put('userId', Apexpages.currentPage().getParameters().get('userId'));
            p.setRedirect(true);    
            return p;
        } catch(Exception e) {
            system.debug(' EXCEPTION ' + e.getMessage() + e.getLineNumber());
            return null;
        }
    }
    
    //This method will be called when logged in user wants to join a public group.
    public PageReference joinGroupAction() {
	    try {
	      CollaborationGroupMember joinGrpMem;
	      if(selectedGroupId != null) {
	        joinGrpMem = new CollaborationGroupMember( CollaborationGroupId = selectedGroupId,
	                                        memberId = loggedInUser.Id);
	        insert joinGrpMem;
	      }
	      return null;
	    } catch (Exception e) {
	      system.debug(' EXCEPTION ' + e.getMessage() + e.getLineNumber());
	      return null;
	    }
	    return null;
  	}
  	
	/**
   *  @param    none
   *  @return   PageReference
   *  method will called when profiled user request to join private group
   */
	public PageReference requestJoinGroupAction() {
	    //List to store Group Owner's email address
	    List<String> lstOwnerEmailAddresses = new List<String>();
	    
	    // Inserting Group joining request
	    CollaborationGroupMemberRequest collaborationRequest = new CollaborationGroupMemberRequest(
	                                    CollaborationGroupId = selectedGroupId,
	                                    RequesterId = loggedInUser.Id
	                                  );
	    insert collaborationRequest;
	    
	    //list will contain Email drafts which needs to be send
	        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
	        
	        //Query and fetch the Email Template Format for sending email to Group Owner.
	        EmailTemplate objEmailTemplate = [SELECT Id, Subject, HtmlValue, Body
	                                          FROM EmailTemplate
	                                          WHERE DeveloperName = :OCSUGC_EMAIL_NOTIFICATION_TO_GROUP_OWNER
	                                          LIMIT 1];
	    
	    String strHtmlBody  = objEmailTemplate.HtmlValue;
	        String strPlainBody = objEmailTemplate.Body;
	        String strSubject   = objEmailTemplate.Subject;
	    
	    for(CollaborationGroup objCollaborationGroup : [SELECT OwnerId, Owner.Email, Owner.ContactId, Name, Owner.Name, Owner.FirstName
	                            FROM CollaborationGroup 
	                            WHERE Id = :selectedGroupId
	                              AND (Owner.Contactid = null
	                                                               OR (Owner.Contactid != null
	                                                                  AND Owner.Contact.OCSUGC_Notif_Pref_ReqJoinGrp__c =true))
	                            LIMIT 1]) {
	      if(objCollaborationGroup.Owner.Email != null)
	        lstOwnerEmailAddresses.add(objCollaborationGroup.Owner.Email);
	      
	      // Inserting Notification for the group owner
	      // TODO : find networkName
	      OCSUGC_Intranet_Notification__c notify = OCSUGC_Utilities.prepareIntranetNotificationRecords('networkName',
	                                                      objCollaborationGroup.OwnerId, objCollaborationGroup.Name,
	                                                      'has asked to join the group', 'ocsugc_groupdetail?fgab=false&g='+selectedGroupId+'&invitedUserId=null&showPanel=Members');
	      if(notify!=null)
	        insert notify;
	      
	      
	      // replacing Header in Email Template according to Community
	      String communityName = '/';
	      if(isDevCloud) {
	        strHtmlBody    = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSDC_Email_Header_Image_Path);
	                communityName += Label.OCSDC_DC_Community_Prefix; 
	      } else {
	        strHtmlBody    = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
	                communityName += Label.OCSUGC_CommunityName;
	      }
	      
	      strHtmlBody = strHtmlBody.replace('GROUP_OWNER_NAME', objCollaborationGroup.Owner.FirstName);
	            strHtmlBody = strHtmlBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
	            strHtmlBody = strHtmlBody.replace('USER_LAST_NAME', Userinfo.getLastName());
	            strHtmlBody = strHtmlBody.replace('GROUP_NAME',objCollaborationGroup.Name);
	      
	      strHtmlBody = strHtmlBody.replace('URL_LINK','<a href="'+ Label.OCSUGC_Salesforce_Instance + communityName +'/OCSUGC_GroupDetail?g=' + objCollaborationGroup.Id +'&showPanel=Members&dc='+isDevCloud+'">Click here</a>');
	            strHtmlBody = strHtmlBody.replace('COMMUNITY_NAME',isDevCloud?Label.OCSDC_DeveloperCloud:Label.OCSDC_OncoPeer);
	            
	            strHtmlBody += '';
	            strHtmlBody += '';
	      
	      //replace the logged in user's FirstName, LastName, Group Name and The Group Link dynamically in PlainBody
	            strPlainBody = strPlainBody.replace('GROUP_OWNER_NAME', objCollaborationGroup.Owner.FirstName);
	            strPlainBody = strPlainBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
	            strPlainBody = strPlainBody.replace('USER_LAST_NAME', Userinfo.getLastName());
	            strPlainBody = strPlainBody.replace('GROUP_NAME',objCollaborationGroup.Name);
	            strPlainBody = strPlainBody.replace('URL_LINK', Label.OCSUGC_Salesforce_Instance + communityName +'/OCSUGC_GroupDetail?g=' + objCollaborationGroup.Id +'&showPanel=Members&dc='+isDevCloud);
	            strPlainBody = strPlainBody.replace('COMMUNITY_NAME',isDevCloud?Label.OCSDC_DeveloperCloud:Label.OCSDC_OncoPeer);
	      
	      strSubject  = strSubject.replace('GROUP_NAME',objCollaborationGroup.Name);
	    }
	    
	    //set email to the Group owner
	        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
	        email.setSaveAsActivity(false);
	        // Use Organization Wide Address
	        if(orgWideEmailAddressId != null)
	            email.setOrgWideEmailAddressId(orgWideEmailAddressId);
	        else 
	          email.setSenderDisplayName('OncoPeer');
	        email.setSubject(strSubject);
	        email.setHtmlBody(strHtmlBody);
	        email.setToAddresses(lstOwnerEmailAddresses);
	        lstEmail.add(email);  
	    
	    // Sending Request Email to Group Owner
	    try {
	      if(!lstEmail.isEmpty())
	        Messaging.sendEmail(lstEmail);
	      
	      return null;
	    } catch(Exception e) {
	      system.debug(' Exception @ ' + e.getLineNumber() + ' Exception : ' + e.getMessage()); 
	      return null;
	    }
	}
     
    /**
     *  Method shorten the description to be displayed on page
     */
    public String validateSummary(String summary) {
        if(summary != null && summary.length() > 195) {
            summary = summary.substring(0,195)+'...';
        }
        return summary;
    }
    
    public void populateProfileGroups() {
        fetchProfiledGroups();
        getRecommendedGroups();
    }
    
    /**
     *  @author     Puneet Mishra       puneet.mishra@varian.com
     *  @param      userId      userId of profiledUser if null or login user
     *  @return     Id          Id of profiledUser
     *  method will return userId, if url parameter if null return login userId else visited User's userId
     */
    @TestVisible private Id fetchProfiledUserId(String userId) {
        if(userId == null)
            return UserInfo.getUserId();
        else
            return userId;
    }
    
    /**
     *  @return     List of group Ids
     *  method return the List of Community group Ids
     */
    @TestVisible private List<Id> CollaborationGroupIds() {
        List<Id> gpIds = new List<Id>();
        
        for(OCSUGC_CollaborationGroupInfo__c colGrpId : [   SELECT Id, OCSUGC_Group_Id__c, OCSUGC_Group_Type__c, OCSUGC_IsArchived__c, OwnerId 
                                                            FROM OCSUGC_CollaborationGroupInfo__c 
                                                            WHERE OCSUGC_IsArchived__c =: false 
                                                                AND OCSUGC_Group_Type__c !=: Label.OCSUGC_FocusGroupAdvisoryBoard]) {
            gpIds.add(colGrpId.OCSUGC_Group_Id__c);                                                         
        }
        return gpIds;
    }
    
    /**
     *  method returns the recommedation Groups for logged-in User only.
     */
    public void getRecommendedGroups() {
        String endPoint;
        endPoint = 'https://' + apexpages.currentpage().getheaders().get('X-Salesforce-Forwarded-To');
        endpoint += '/services/data/v31.0/connect/communities/'+ocsugcNetworkId+'/chatter/users/'+UserInfo.getUserId()+'/recommendations/view/groups';
        String sessionId = userInfo.getSessionId();//005M0000006Jato
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setHeader('Accept','application/json');
        request.setHeader('Authorization','OAuth ' + Userinfo.getSessionId());
        request.setHeader('X-PrettyPrint','1');
        request.setCompressed(false);
        Http h = new Http();
        HttpResponse res = new HttpResponse();
        res = h.send(request);
        String responseString = res.getBody();      
        
        cdata = OCSUGC_CommunityData.parse(responseString);
        system.debug(' ====== No of Recommendations  ======== ' + cdata.recommendations.size());

    }
    
    // Wrapper Class for Groups
    public class GroupWrapper  implements Comparable {
        public CollaborationGroup grp {get;set;}
        public Boolean isOwner{get;set;}
        public Boolean isMember{get;set;}
        public Boolean isPublic{get;set;}
        public Boolean joinGroup{get;set;}
     	public Boolean isDCGroup{get;set;}
     	//public Boolean showJoin{get;set;}
     	public Boolean requestJoinGroup{get;set;}
     	public Boolean requestAlreadySent{get;set;}
     	//public Boolean showRequestSent{get;set;}
        public Boolean leaveGroup{get;set;}
        public Boolean deleteGroup{get;set;}
        public String className{get;set;}
        public String roleClassName{get;set;}
        
        // Code Block, setting the Boolean values to false
        {   
            isOwner = false;
            isPublic = false;
            isMember = false;
            joinGroup = false;
            isDCGroup = false;
            //requestGroup = false;
            //showJoin = false;
            requestJoinGroup = false;
            requestAlreadySent = false;
            leaveGroup = false;
            deleteGroup = false;
            
        }
        
        /**
         *  Wrapper Class Constructor   
         */
        public GroupWrapper(CollaborationGroup grp) {
            this.grp = grp;
            
            // checking if login user is Owner of group
            if(grp.OwnerId == userInfo.getUserId())
                this.isOwner = true;
            
            // group type public/private
            if(grp.CollaborationType == 'Public')
                isPublic = true;
                
                        
            for(CollaborationGroupMember grpMember : grp.GroupMembers) {
                if(UserInfo.getUserId() == grpMember.MemberId)
                    this.isMember = true;
            }
            
            if(this.isOwner)
                deleteGroup = true;

            if(this.isMember) {
                if(!this.isOwner)
                    leaveGroup = true;
            }
            
            //Added by Shital Start
            if((grp.CollaborationType == 'Public') && (!this.isMember) && (!this.isOwner))
	        	joinGroup = true;
	        else if((grp.CollaborationType == 'Private') && (!this.isMember) && (!this.isOwner))
	        	requestJoinGroup = true;
            //Added by Shital End
            
            system.debug(' == isMember == ' + isMember + ' == isPublic == ' + isPublic);
        }

        public GroupWrapper() {}

    public Integer compareTo(Object objToCompare) {
        GroupWrapper comparedToGroup = (GroupWrapper)objToCompare;

        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (grp.Name > comparedToGroup.grp.Name) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (grp.Name < comparedToGroup.grp.Name) {
            // Set return value to a negative value.
            returnValue = -1;
        }

        	return returnValue;
    	}
    }
}
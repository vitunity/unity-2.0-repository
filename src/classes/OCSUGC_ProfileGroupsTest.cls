// (c) 2012 Appirio, Inc.
//
// Test class  for CAKE_ProfileGroups
//
// 6 Aug 2014    Pawan Tyagi  Test class  for CAKE_ProfileGroups
// 09 September 2014 Sidhant

@isTest
public class OCSUGC_ProfileGroupsTest {
    static Profile admin,portal;
    static Network ocsugcNetwork;
    static CollaborationGroup publicGroup,privateGroup,fgabGroup;
    static List<CollaborationGroup> lstGroupInsert;
    static Account account; 
    static Contact contact1,contact2,contact3,contact4,contact5, contact6,contact7, contact8, contact9;
    static List<Contact> lstContactInsert;
    static List<User> lstAdmUser = new List<User>();
    static List<User> lstPortalUser;

    Static{
        
        //lstAdmUser = new List<User>();
        lstPortalUser = new List<User>();
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile();
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();

        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
        insert lstGroupInsert;    

        //account = OCSUGC_TestUtility.createAccount('test account', true);

        /*lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact6 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact7 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact8 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact9 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert; 
        */
        for(integer i = 0;i<3;i++) {
            lstAdmUser.add( OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test' + i, 'testAdminRole'));
        }
        insert lstAdmUser;
        /*for(Integer i=0; i<9; i++){
            lstPortalUser.add( OCSUGC_TestUtility.createPortalUser(false, portal.Id, lstContactInsert[i].Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        }
        insert lstPortalUser;
        */
    }
    public static testmethod void test_CAKE_ProfileGroups() {
        
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        admin = OCSUGC_TestUtility.getAdminProfile();
        List<CollaborationGroupMember> grpMemberInsertList = new List<CollaborationGroupMember>();
        //lstAdmUser = [Select Id,name from USER WHERE profileId = :admin.Id AND createdDate = :system.today()];

        system.debug('---lstAdmUser---' + lstAdmUser.size());
        
        Test.startTest();
        //Create an instance of controller - CAKE_GroupDetailController
        PageReference pageRef = Page.OCSUGC_UserProfile;
        Test.setCurrentPage(pageRef);
        //User usr = OCSUGC_TestUtility.createUser(true);
        System.runAs (lstAdmUser.get(0)) {

            //Creating Collaboration Group
            list<CollaborationGroup> lstCollaborationGroupPrivate = new list<CollaborationGroup>();

            for(integer i = 0;i<3;i++) {
                CollaborationGroup objCollaborationGroup = new CollaborationGroup(
                                            name ='testCollaborationGroupPrivate'+i,
                                            Description='Test Description',
                                            OwnerId=lstAdmUser[2].Id,
                                            CollaborationType='private',
                                            IsArchived = false,
                                            NetworkId = ocsugcNetwork.Id);
                    lstCollaborationGroupPrivate.add(objCollaborationGroup);
            }

            insert lstCollaborationGroupPrivate;

            //creating CollaborationGroupMemberRequest
            /*list<CollaborationGroupMemberRequest> lstCollaborationGroupMemberRequest = new list<CollaborationGroupMemberRequest>();

            CollaborationGroupMemberRequest objCollaborationGroupMemberRequest = new
                                            CollaborationGroupMemberRequest(
                                            CollaborationGroupId = lstCollaborationGroupPrivate[1].Id,
                                            RequesterId = lstAdmUser.get(0).Id);

            lstCollaborationGroupMemberRequest.add(objCollaborationGroupMemberRequest);

            insert lstCollaborationGroupMemberRequest;*/

            //Creating Public Collaboration Group
            list<CollaborationGroup> lstCollaborationGroupPublic = new list<CollaborationGroup>();

            for(integer i = 0;i<3;i++) {
                CollaborationGroup objCollaborationGroup = new CollaborationGroup(
                                            name ='testCollaborationGroupublic'+i,
                                            Description='Test Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description publicTest Description public',
                                            OwnerId=lstAdmUser[1].Id,
                                            CollaborationType='public',
                                            IsArchived = false,
                                            NetworkId = ocsugcNetwork.Id);
                    lstCollaborationGroupPublic.add(objCollaborationGroup);
            }
            insert lstCollaborationGroupPublic;
        
            ApexPages.currentPage().getParameters().put('showPanel', 'Groups');
        
            CollaborationGroupMember  cGrpMem = OCSUGC_TestUtility.createGroupMember(lstAdmUser.get(0).Id, lstCollaborationGroupPublic[0].Id, false);
            CollaborationGroupMember  cGrpMem1 = OCSUGC_TestUtility.createGroupMember(lstAdmUser.get(0).Id, lstCollaborationGroupPublic[1].Id, false);
            CollaborationGroupMember  cGrpMem2 = OCSUGC_TestUtility.createGroupMember(lstAdmUser.get(0).Id, lstCollaborationGroupPrivate[2].Id, false);
            grpMemberInsertList.add(cGrpMem);
            grpMemberInsertList.add(cGrpMem1);
            grpMemberInsertList.add(cGrpMem2);
            insert grpMemberInsertList;
            
            OCSUGC_ProfileGroupController objprofileGrps = new OCSUGC_ProfileGroupController();

            //OCSUGC_ProfileGroupController.MyGroupWrapper myGrp = new OCSUGC_ProfileGroupController.MyGroupWrapper(lstCollaborationGroupPublic[0],lstCollaborationGroupPublic[0].OwnerId,2);
                            
            //OCSUGC_ProfileGroupController.OtherGroupWrapper otherGrp = new OCSUGC_ProfileGroupController.OtherGroupWrapper(lstCollaborationGroupPublic[0], true, true,3);
            //OCSUGC_ProfileGroupController.OtherGroupWrapper otherGrp1 = new OCSUGC_ProfileGroupController.OtherGroupWrapper(lstCollaborationGroupPrivate[0], false, true,2);
            //OCSUGC_ProfileGroupController.RecommendedGroupWrapper recommendedGroupWrapper1 = new OCSUGC_ProfileGroupController.RecommendedGroupWrapper(lstCollaborationGroupPrivate[0],2);

            objprofileGrps.selectedGroupId = lstCollaborationGroupPublic[0].Id;
			
			// Changes by Puneet Mishra, 15 Oct 2015, Fix is been made to handled System.UnexpectedException occuring due to SOSL used in Apex classes
			List<Id> fixedSearchResults = new List<Id>();
			//fixedSearchResults.add(objCollaborationGroup.Id); 
			fixedSearchResults.add(lstCollaborationGroupPublic[2].Id); 
            Test.setFixedSearchResults(fixedSearchResults);
            
			objprofileGrps.fetchProfiledGroups();
			objprofileGrps.leaveGroupAction();
			objprofileGrps.deleteGroupAction();
			objprofileGrps.fetchProfiledGroups();
			objprofileGrps.joinGroupAction();
			objprofileGrps.validateSummary('TestSummary TestSummary TestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummaryTestSummary');
            OCSUGC_ProfileGroupController objprofileGrps1 = new OCSUGC_ProfileGroupController();
            objprofileGrps1.profiledId = lstAdmUser.get(0).Id;
            objprofileGrps1.selectedGroupId = lstCollaborationGroupPrivate[1].Id;
            objprofileGrps1.requestJoinGroupAction();
        }
        Test.stopTest();
        
    }
    
    /**
    *	createdDate : 6 Dec 2016, Tuesday
    *	Puneet Mishra : testMethod for EntitySubscriptionWrapperClass inner class
    */
    public static testMethod void getRecommendedUsersTest() {
     	Test.StartTest();
     	
     		Test.setMock(HttpCalloutMock.class, new OCSUGC_MockCalloutController ());
     	
     		OCSUGC_ProfileGroupController control = new OCSUGC_ProfileGroupController();
     		
     		// covering rest of the Test Method here//
     		control.getRecommendedGroups();
     		control.populateProfileGroups();
     		
     		String strLength = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+
     						   'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+
     						   'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+
     						   'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+
     						   'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+
     						   'ABCDEFGHIJKLMNOPQRSTUVWXYZ-' +'ABCDEFGHIJKLMNOPQRSTUVWXYZ-'+'ABCDEFGHIJKLMNOPQRSTUVWXYZ';	 
     		control.validateSummary(strLength);
     		control.fetchProfiledUserId(userInfo.getUserId());
     		
     		OCSUGC_CollaborationGroupInfo__c grpInfo = new OCSUGC_CollaborationGroupInfo__c();
     		grpInfo.OCSUGC_Group_Type__c = 'Focus Group/Advisory Board';
     		grpInfo.OCSUGC_IsArchived__c = false;
     		grpInfo.OCSUGC_Group_Id__c = lstGroupInsert[0].Id;
     		insert grpInfo;
     		control.CollaborationGroupIds();
     	Test.StopTest();
    }
     
    /**
     * createdDate : 6 Dec 2016, tuesday
     * Puneet Mishra : testMethod for GroupWrapper inner class
     */
	public static testMethod void GroupWrapper_PublicGrp_Test() {
		Test.StartTest();
		OCSUGC_ProfileGroupController.GroupWrapper wrap = new OCSUGC_ProfileGroupController.GroupWrapper();
		
		CollaborationGroup gp = new CollaborationGroup();
		gp = [Select Id, OwnerId, Owner.Email, Owner.ContactId, Name, Owner.Name, Owner.FirstName, CollaborationGroup.CollaborationType 
			  from CollaborationGroup WHERE Id =: lstGroupInsert[1].Id];
		
		OCSUGC_ProfileGroupController.GroupWrapper wrap2 = new OCSUGC_ProfileGroupController.GroupWrapper(gp);
		wrap2.className = 'SomeValue';
		wrap2.roleClassName = 'SomeValue';
		Test.StopTest();
    }
    
    /**
     * createdDate : 6 Dec 2016, tuesday
     * Puneet Mishra : testMethod for GroupWrapper inner class
     */
	public static testMethod void GroupWrapper_PrivateGrp_Test() {
		Test.StartTest();
		
		CollaborationGroup gp = new CollaborationGroup();
		gp = [Select Id, OwnerId, Owner.Email, Owner.ContactId, Name, Owner.Name, Owner.FirstName, CollaborationGroup.CollaborationType 
			  from CollaborationGroup WHERE Id =: lstGroupInsert[1].Id];
		
		OCSUGC_ProfileGroupController.GroupWrapper wrapPrivate = new OCSUGC_ProfileGroupController.GroupWrapper(gp);
		
		Test.StopTest();
    }
}
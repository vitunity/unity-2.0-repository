/**
 *      class will fetch the Followers, Following User of profiled User and recommended people for profiled User
 */
public class OCSUGC_ProfilePeopleController extends OCSUGC_Base_NG {
        
    @TestVisible private String profiledId;                                     // Id of logged-in User 
    public OCSUGC_CommunityData cData{get;set;}                                        // deserialize Recommendation Group Data
    public List<EntitySubscriptionWrapperClass> followers{get;set;}
    public List<EntitySubscriptionWrapperClass> following{get;set;}
    
    public OCSUGC_ProfilePeopleController() {
        profiledId = fetchProfiledUserId( ApexPages.currentPage().getParameters().get('userId') );      
        
        fetchFollowers();
        fetchFollowing();
        if(!test.isRunningTest())
        	getRecommendedUsers();
    }

    /**
     *  @author     Puneet Mishra
     *  @param
     *  @return 
     *              method will return number of followers for profiled User
     */
    public PageReference fetchFollowers() {
        EntitySubscriptionWrapperClass entityClass;
        followers = new List<EntitySubscriptionWrapperClass>();
        for(User usrFollower : [SELECT Id, Name, Email, SmallPhotoUrl, Country, Contact.Title, Contact.Account.Legal_Name__c, OCSUGC_User_Role__c FROM User
                             WHERE Id IN: createDynamicSOQL(true)]) {
            entityClass = new EntitySubscriptionWrapperClass(usrFollower);
            
            system.debug('======****** usrFollower entityClass ******======= ' + usrFollower.OCSUGC_User_Role__c + entityClass);
            followers.add(entityClass);
        }

        return null;
    }
    
    /**
     *  @author     Puneet Mishra
     *  @param
     *  @return
     *              method will return number of following Users for profiled User
     */
    public PageReference fetchFollowing() {
        EntitySubscriptionWrapperClass entityClass;
        following = new List<EntitySubscriptionWrapperClass>();
        for(User usrFollowing : [SELECT Id, Name, Email, isActive, SmallPhotoUrl, Country, Contact.Title, Contact.Account.Legal_Name__c, OCSUGC_User_Role__c FROM User
                             WHERE Id IN: createDynamicSOQL(false) AND isActive = true]) {
            entityClass = new EntitySubscriptionWrapperClass(usrFollowing);
            
            system.debug('======****** followinf entityClass ******======= ' + usrFollowing.OCSUGC_User_Role__c + entityClass);
            following.add(entityClass);
        }
        system.debug('======****** followinf Size ******======= ' + following.size());
        return null;
    }
    
    /**
     *  @author     Puneet mishra
     *  @param      Boolean
     *  @return     List<Id> userIds
     *              depend on Boolean parameter passed Dynamic SOQL decided and User Id records returned.
     */
     public List<Id> createDynamicSOQL(Boolean followers) {
        List<EntitySubscription> subscriptionList = new List<EntitySubscription>();
        List<Id> userId = new List<Id>();
        String soqlQuery = '';

        soqlQuery += ' SELECT Id, NetworkId, ParentId, SubscriberId FROM EntitySubscription ' +
                 ' WHERE NetworkId = \''+ocsugcNetworkId+'\' AND ' ; //Note that we only always follow users in OncoPeer community
        if(followers) {
            soqlQuery += ' ParentId = ' + '\'' + profiledId + '\'';
        } else {
            soqlQuery += ' SubscriberId = ' + '\'' + profiledId + '\'';
        }
        soqlQuery += ' LIMIT 1000 ';
        system.debug(' ==== *** soqlQuery *** ==== ' + soqlQuery);
        //system.debug(' ==== *** Database.query(soqlQuery) *** ==== ' + followers + ' ==== ' + Database.query(soqlQuery));

        for(EntitySubscription subscription : Database.query(soqlQuery)) {
            if(followers)
                userId.add(subscription.SubscriberId);
            else
                userId.add(subscription.ParentId);
        }
        system.debug('=== userId Size ====' + userId.size());
        return userId;
     }
    
    /**
     *  @author     Puneet Mishra       puneet.mishra@varian.com
     *  @param      userId      userId of profiledUser if null or login user
     *  @return     Id          Id of profiledUser
     *  method will return userId, if url parameter if null return login userId else visited User's userId
     */
    @TestVisible private Id fetchProfiledUserId(String userId) {
        if(userId == null)
            return UserInfo.getUserId();
        else
            return userId;
    }
    
    /**
     *  method returns the recommedation People for logged-in User only.
     */
    public void getRecommendedUsers() {
        String endPoint;
        endPoint = 'https://' + apexpages.currentpage().getheaders().get('X-Salesforce-Forwarded-To');
        endpoint += '/services/data/v31.0/connect/communities/'+ocsugcNetworkId+'/chatter/users/'+UserInfo.getUserId()+'/recommendations/view/users';
        String sessionId = userInfo.getSessionId();//005M0000006Jato
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPoint);
        request.setMethod('GET');
        request.setHeader('Accept','application/json');
        request.setHeader('Authorization','OAuth ' + Userinfo.getSessionId());
        request.setHeader('X-PrettyPrint','1');
        request.setCompressed(false);
        Http h = new Http();
        HttpResponse res = new HttpResponse();
        res = h.send(request);
        String responseString = res.getBody();
        system.debug(' === response ==== ' + responseString);      
        cdata = OCSUGC_CommunityData.parse(responseString);
    }
    
    /**
     *  wrapper class store's the subscription for profiled User
     *
     */
    public class EntitySubscriptionWrapperClass {
        public User subscribedUser{get;set;}
        public Boolean isCommunityManager{get;set;}
        public Boolean isCommunityContributor{get;set;}

        Integer followers;
        Integer following;
        {
            followers = 0;
            following = 0;
            isCommunityManager = false;
            isCommunityContributor = false;
        }

        public EntitySubscriptionWrapperClass(User u) {
            this.subscribedUser = u;
            this.isCommunityManager = OCSUGC_Utilities.isManager(u.OCSUGC_User_Role__c);
            this.isCommunityContributor = OCSUGC_Utilities.isContributor(u.OCSUGC_User_Role__c);
        }
    }
    
}
// (c) 2012 Appirio, Inc.
//
// Test class  for OCSUGC_ProfilePeopleController
//
// 06 Feb 2015    Puneet Sardana  Test class  for OCSUGC_ProfilePeopleController Ref T-317348
//
@isTest(SeeAllData=false)
public class OCSUGC_ProfilePeopleControllerTest
{
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static Network ocsugcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static List<CollaborationGroup> lstGroupInsert;
   static EntitySubscription follow1,follow2,follow3,follow4,follow5;
   static List<EntitySubscription> lstEntityInsert;
   static OCSUGC_Knowledge_Exchange__c ka1,ka2;
   static List<OCSUGC_Knowledge_Exchange__c> lstKAInsert;
   static List<OCSUGC_Users_Visibility__c> lstUserVisibility;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '1567', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '67892', '2'));
     insert lstUserInsert;
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false));
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Public',ocsugcNetwork.id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
     insert lstGroupInsert;
     System.runAs(adminUsr2) {
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, adminUsr1.Id, true);
     }

     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;
        lstGroupMemberInsert = new List<CollaborationGroupMember>();
        lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
        insert lstGroupMemberInsert;
        lstEntityInsert = new List<EntitySubscription>();
        lstEntityInsert.add( follow1 = OCSUGC_TestUtility.createEntitySubscription(ocsugcNetwork.Id, managerUsr.Id, usr4.Id, false));
        lstEntityInsert.add( follow2 = OCSUGC_TestUtility.createEntitySubscription(ocsugcNetwork.Id, managerUsr.Id, usr5.Id, false));
        lstEntityInsert.add( follow3 = OCSUGC_TestUtility.createEntitySubscription(ocsugcNetwork.Id, contributorUsr.Id, managerUsr.Id, false));
        lstEntityInsert.add( follow4 = OCSUGC_TestUtility.createEntitySubscription(ocsugcNetwork.Id, usr4.Id, managerUsr.Id, false));
//        insert lstEntityInsert;
        lstKAInsert = new List<OCSUGC_Knowledge_Exchange__c>();
        lstKAInsert.add(ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,false));
        lstKAInsert.add(ka2 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,false));
        insert lstKAInsert;
        lstUserVisibility = new List<OCSUGC_Users_Visibility__c>();
        lstUserVisibility.add(OCSUGC_TestUtility.createUserVisibilityCustomSetting(Label.OCSUGC_Varian_Employee_Community_Manager,true,true,true,false,false));
        lstUserVisibility.add(OCSUGC_TestUtility.createUserVisibilityCustomSetting(Label.OCSUGC_Varian_Employee_Community_Contributor,true,true,true,false,false));
        lstUserVisibility.add(OCSUGC_TestUtility.createUserVisibilityCustomSetting(Label.OCSUGC_Varian_Employee_Community_Member,true,true,true,false,false));
        insert lstUserVisibility;
     }
   }
    public static testmethod void test_OCSUGC_ProfilePeopleController() {
      Test.startTest();
      System.runAs(managerUsr) {
        PageReference ref = Page.OCSUGC_UserProfile;
        ref.getParameters().put('showPanel','People');
        ref.getParameters().put('userId',usr4.Id);
        Test.setCurrentPage(ref);
        OCSUGC_ProfilePeopleController peopleController = new OCSUGC_ProfilePeopleController();
        //peopleController.lazyLoad();
        //peopleController.followUser();
        //peopleController.unFollowUser();
        //peopleController.refreshPage();
        //peopleController.strSearchUser = 'test';
        //peopleController.searchAllUsers();
        //OCSUGC_ProfilePeopleController.populateUserNameList('Test',false);
        //peopleController.getRecommendedPeople();
        //peopleController.lstFollowing = new List<OCSUGC_ProfilePeopleController.WrapperUser>();
	  }
      Test.stopTest();
    }
    
    public static testmethod void test_OCSUGC_ProfilePeopleController_follow_Unfollow_User() {
    	Test.startTest();
    	
    	System.runAs(managerUsr) {
	        PageReference ref = Page.OCSUGC_UserProfile;
	        ref.getParameters().put('showPanel','People');
	        ref.getParameters().put('userId',usr4.Id);
	        Test.setCurrentPage(ref);
    		OCSUGC_ProfilePeopleController peopleController = new OCSUGC_ProfilePeopleController();
	        //peopleController.lazyLoad();
	        peopleController.fetchFollowers();
	        //peopleController.unFollowUser();
    	}
    	
    	Test.stopTest();
    }
    
    public static testmethod void test_OCSUGC_ProfilePeopleController_searchAllUsers() {
    	Test.startTest();
    	
    	System.runAs(managerUsr) {
	        PageReference ref = Page.OCSUGC_UserProfile;
	        ref.getParameters().put('showPanel','People');
	        ref.getParameters().put('userId',usr4.Id);
	        Test.setCurrentPage(ref);
    		OCSUGC_ProfilePeopleController peopleController = new OCSUGC_ProfilePeopleController();
	        //peopleController.lazyLoad();
	        //peopleController.strSearchUser = 'test';
    		//peopleController.searchAllUsers();
    	}	
    	
    	Test.stopTest();
    }
    //commented by Shital
    /*public static testMethod void test_OCSUGC_ProfilePeopleController_populateUserNameList() {
    	Test.startTest();
    	
    	System.runAs(managerUsr) {
	        PageReference ref = Page.OCSUGC_UserProfile;
	        ref.getParameters().put('showPanel','People');
	        ref.getParameters().put('userId',usr4.Id);
	        Test.setCurrentPage(ref);
    		OCSUGC_ProfilePeopleController.populateUserNameList('Test',false);
    	}	
    	
    	Test.stopTest();
    }
    
    public static testMethod void test_OCSUGC_ProfilePeopleController_getRecommendedPeople(){
    Test.startTest();
    	
    	System.runAs(managerUsr) {
	        PageReference ref = Page.OCSUGC_UserProfile;
	        ref.getParameters().put('showPanel','People');
	        ref.getParameters().put('userId',usr4.Id);
	        Test.setCurrentPage(ref);
    		OCSUGC_ProfilePeopleController peopleController = new OCSUGC_ProfilePeopleController();
	        peopleController.lazyLoad();
	        peopleController.getRecommendedPeople();
    	}
    	
    	Test.stopTest();
    }*/
    
    // 29 Sept 2015		Puneet Mishra
    // modified the method by passing userName as parameter to createIntranetNotification() method
    /*public static testMethod void test_createIntranetNotification() {
    	Test.startTest();
    		system.runAs(managerUsr) {
    			PageReference ref = Page.OCSUGC_UserProfile;
	       		ref.getParameters().put('showPanel','People');
	        	ref.getParameters().put('userId',usr4.Id);
    			Test.setCurrentPage(ref);
    			OCSUGC_ProfilePeopleController peopleController = new OCSUGC_ProfilePeopleController();
    			//peopleController.lazyLoad();
    			//peopleController.selectedUserId = usr5.Id;
    			String userName = userInfo.getUserName();
    			Boolean result = peopleController.createIntranetNotification('Test Msg', userName);
    			//system.assertEquals(true, false);
    		}
    	Test.stopTest();
    }*/
    
    /**
     *	createdDate : 6 Dec 2016, Tuesday
     *	Puneet Mishra : testMethod for EntitySubscriptionWrapperClass inner class
     */
     public static testMethod void EntitySubscriptionWrapperClassTest() {
     	Test.StartTest();
     		OCSUGC_ProfilePeopleController.EntitySubscriptionWrapperClass subscription = new OCSUGC_ProfilePeopleController.EntitySubscriptionWrapperClass(usr4);
     	Test.StopTest();
     }
     
     /**
     *	createdDate : 6 Dec 2016, Tuesday
     *	Puneet Mishra : testMethod for EntitySubscriptionWrapperClass inner class
     */
     public static testMethod void getRecommendedUsersTest() {
     	Test.StartTest();
     	
     		Test.setMock(HttpCalloutMock.class, new OCSUGC_MockCalloutController ());
     	
     		OCSUGC_ProfilePeopleController control = new OCSUGC_ProfilePeopleController();
     		control.getRecommendedUsers();
     	Test.StopTest();
     }
}
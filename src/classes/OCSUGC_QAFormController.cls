// (c) 2014 Appirio, Inc.
//
// This page allow the user to reset password logout functionality to user after "Terms of Use"
//
// 12 Aug 2014  Jai Shankar Pandey   user landing page after aggreeing "Terms Of Use"

public class OCSUGC_QAFormController extends OCSUGC_Base{

    public String Firstlogin {get;set;} 
    public String CustRecovryQ {get; set;}
    public String CustRecoveryAns {get; set;}
    public String Newpass{get;set;}
    public String ConfirmPass{get;set;}

    public String strcurrentUserId  {get; set;}

    //constructor
    public OCSUGC_QAFormController(){

        strcurrentUserId = UserInfo.getUserId();
        //Query the current logged In user Id
        Firstlogin = 'true';
    }

    //  sending an email to public group 'Spheros Admin' based on the Email Template
    //  argument none
    //  @return lists of recoveries questions for first time user
    Public List<SelectOption> getrecoveryqusoptions()
    {
          list<SelectOption> recoveryqusoptions = new list<SelectOption>();
          // Get the object type of the SObject.
          Schema.sObjectType objType = Contact.getSObjectType();
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values = fieldMap.get('Recovery_Question__c').getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          {
             recoveryqusoptions.add(new SelectOption(a.getLabel(), a.getValue()));
          }
          return recoveryqusoptions;

    }

    //  Resetting password of user at first time login in
    //  argument none
    //  @return to the home page
    public pagereference resetpassword() {

        String Recoveryquestion = ApexPages.currentPage().getParameters().get('RecoveryQuestionparam');
        String RecoveryAnswer = ApexPages.currentPage().getParameters().get('RecoveryAnswerparam');
        String NewPassword = ApexPages.currentPage().getParameters().get('NewPasswordparam');
        RecoveryAnswer = RecoveryAnswer.deleteWhitespace().toLowerCase();
        system.debug('======strcurrentUserId====reset Password=====>>>>'+strcurrentUserId);
        User usercontact = [Select ContactId,Contact.Email,Contact.FirstName,Contact.LastName,Contact.Phone,Contact.OktaLogin__c,Contact.OktaId__c from user where id=: strcurrentUserId limit 1];

        //http Request creation and capturing response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http objHttp = new Http();

        //Construct Authorization and Content header
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');

        //EndPoint Creation
        String EndPoint = label.oktachangepasswrdendpoint + usercontact.Contact.OktaId__c;
        req.setendpoint(EndPoint);
        req.setMethod('PUT');
        String body ='{"profile": {"firstName": "'+ usercontact.Contact.FirstName +'","lastName": "'+usercontact.Contact.LastName +'","email": "'+ usercontact.Contact.Email + '","login": "' + usercontact.Contact.OktaLogin__c + '","mobilePhone": "' + usercontact.Contact.Phone + '"},"credentials": {"password" : { "value": "' + NewPassword + '" },"recovery_question": {"question": "'+ Recoveryquestion +'","answer": "'+ RecoveryAnswer + '"}}}';
        req.setBody(body);
        try {
           //Send endpoint to OKTA
           system.debug('aebug ; ' +req.getBody());
           If(!Test.IsRunningTest()){
           res = objHttp.send(req);
           }
           else{
                String strResponse='Summary';
                res.setBody(strResponse);
           }
           if(res.getbody().contains('errorSummary'))
           { 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Password provided does not match.');
            ApexPages.addMessage(myMsg);
            return null;
          }
           system.debug('aebug ; ' +res.getBody());
         }catch(System.CalloutException e) {
            System.debug('====Exception=====>>>>'+res.toString());
        }
        // Updating Contact Record
        Contact updatecon = new Contact(id = usercontact.ContactId);
        updatecon.Recovery_Question__c = Recoveryquestion;
        updatecon.Recovery_Answer__c = RecoveryAnswer;
        updatecon.PasswordReset__c = true;
        updatecon.activation_url__c = '';
        updatecon.PasswordresetDate__c = system.today();// to capture the last date when the password was reseted
        try{

            User ObjUser = new User(Id = strcurrentUserId, OCSUGC_Accepted_Terms_of_Use__c = true);
            update ObjUser;

            update updatecon;

        }  catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please contact support for the above mentioned error');
            ApexPages.addMessage(myMsg);
            return null;
        }
        Pagereference pg;
        if(isDC){
            pg = Page.OCSDC_Home; 
            pg.getParameters().put('dc','true');
            
        }
        else{
            pg = Page.OCSUGC_Home;
        }
        pg.getParameters().put('isResetPwd','true');
        pg.setRedirect(true);
        return pg;
     }

    //  On click of Cancel button,redirect the user to the CAKE Home Page
    //  argument none
    //  @return to the Home page
    public pagereference logoutmethod()
    {
        // Naresh K SHiwani     27/01/2015      T-356594
        return OCSUGC_Utilities.logoutMethod();
    }
}
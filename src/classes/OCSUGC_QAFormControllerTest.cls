// (c) 2012 Appirio, Inc.
//
// Test class  for OCSUGC_QAFormController
//
// 13 Aug 2014    Jai Shankar Pandey   Test class  for OCSUGC_QAFormController

@isTest
public class OCSUGC_QAFormControllerTest
{

    public static testmethod void test_OCSUGC_QAFormController()
    {
        OCSUGC_QAFormController objCAKEQAFormController = new OCSUGC_QAFormController();
        List<SelectOption> lstSelectOption = new List<SelectOption>();

        //creating Profile
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();

        test.startTest();

        //portal user owner
        User portalAccountOwner1 = OCSUGC_TestUtility.createStandardUser(false, adminProfile.Id, 'test', '1');
        portalAccountOwner1.UserRoleId = portalRole.Id;
        Database.insert(portalAccountOwner1);

        //run as portal user
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = OCSUGC_TestUtility.createAccount('test account', false);
            portalAccount1.Country__c = 'ind';
            portalAccount1.Agreement_Type__c = 'dfsd';
            portalAccount1.Vendor_Credentialing_Company__c = 'fdssg';
            Database.insert(portalAccount1);

            //Create contact
            Contact contact1 = OCSUGC_TestUtility.createContact(portalAccount1.Id, false);
            Database.insert(contact1);

            //Create user
            Profile portalProfile1 = OCSUGC_TestUtility.getPortalProfile();
            User user1 = OCSUGC_TestUtility.createPortalUser(false, portalProfile1.Id, contact1.Id, '5',Label.OCSUGC_Varian_Employee_Community_Member);
            Database.insert(user1);

            //recoveryqusoptions
            lstSelectOption = objCAKEQAFormController.getrecoveryqusoptions();

            //verify the user redirect the on home page succesfully
            ApexPages.currentPage().getParameters().put('RecoveryQuestionparam','Test Question');
            ApexPages.currentPage().getParameters().put('RecoveryAnswerparam','Test Answer');

            system.runAs(user1)
            {

                //resetpassword
                PageReference pageRef = objCAKEQAFormController.resetpassword();
                //verify the user redirect the on Spheros_Continue_Disagree_Action succesfully
                system.assertEquals(null,pageRef);
            }

            //logout method
            PageReference nextPagelogout = objCAKEQAFormController.logoutmethod();
            //verify the user redirect the on Spheros_Continue_Disagree_Action succesfully
            system.assertNotEquals(null,nextPagelogout);
        }
        //test stop()
        test.stopTest();
    }
}
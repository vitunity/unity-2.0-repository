//
// (c) 2014 Appirio, Inc.
//
// Controller class for OCSUGC_ReportAProblem component. It Provides functionality to Report a Problem.
//
// 8th Aug, 2014     Satyanarayan Choudhary       Original
// 11th Aug, 2014    Satyanarayan Choudhary       Modified Added sendEmailOnReportAProblem method reference - T-311541
// 29th Oct, 2014    Naresh K SHiwani		      Modified OCSUGC_ReportAProblemController, reportAProblem.
//												  Added OCSUGC_Reason_for_Flag__c,OCSUGC_Reason_for_Reporting__c
//												  for creating new object OCSUGC_Content_ReportedProblem__c.
//												  T-327542,T-327577.
// 03rd Nov, 2014	 Naresh K Shiwani			  Modified OCSUGC_ReportAProblemController(), fetched picklist value of
//												  OCSUGC_Reason_for_Flag__c and added to flagedPickListValues - T-330369, T-330370
//// 26 Nov, 2014  Ajay Gautam        Modified Added Support for Poll - T-335681

public class OCSUGC_ReportAProblemController {
	private Id orgWideEmailAddressId;
    private String networkName;
    public String strEmailStatusMsg {get; set;} 
    public String reportReason {get; set;}
    public Boolean isError {get; set;}
    public String contId {get; set;}
    public String contType {get; set;}
    public String flagType {get; set;} 
    public String exMsg {get; set;}
    public Boolean isFGAB {get; set;}
	public String fgabGroupId {get; set;}
    public List<SelectOption> flagedPickListValues{get; set;}
    public OCSUGC_Content_ReportedProblem__c contentReportProb{get; set;}
	public Boolean isDC { get; set; }

    private static final String OCSUGC_REPORT_A_PROBLEM_EMAIL_TEMPLATE = 'OCSUGC_Report_A_Problem_Notification_To_The_OCSUGC_Admin_Group';
    private static final String REASON_OTHER = 'Other';



    public string errorMsg{
        get{
            String msg = '';
            if(contType == 'Event'){
                msg = Label.OCSUGC_Problem_Report_Already_Exist_Event;
            }else if(contType == 'Discussion'){
                msg = Label.OCSUGC_Problem_Report_Already_Exist_Discussion;
            }else if(contType == 'Knowledge File'){
                msg = Label.OCSUGC_Problem_Report_Already_Exist_KA;
            } else if(contType == 'Poll') { // T-335681
                msg = Label.OCSUGC_Problem_Report_Already_Exist_Poll;
            }

            return msg;
        }
    }

    public boolean isAlreadyReported {
        get{
            return [ SELECT Id FROM OCSUGC_Content_ReportedProblem__c
                     WHERE OCSUGC_Content_ID__c = :contId and OCSUGC_Reported_By__c = :UserInfo.getUserId()

                   ].size() > 0;
        }
        set;
    }

    // Constructor
    public OCSUGC_ReportAProblemController() {
    	orgWideEmailAddressId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        networkName = OCSUGC_Utilities.getCurrentCommunityNetworkName(OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc')));
        exMsg = '';
        isDC = OCSUGC_Utilities.isDevCloud(ApexPages.currentPage().getParameters().get('dc'));
        isError = false;
        flagedPickListValues = new List<SelectOption>();
        flagedPickListValues.add(new SelectOption('','Please Select'));
        for (Schema.PicklistEntry probListOption : OCSUGC_Content_ReportedProblem__c.getSObjectType().getDescribe().fields.getMap().get('OCSUGC_Reason_for_Flag__c').getDescribe().getPickListValues())
	      {
	         flagedPickListValues.add(new SelectOption(probListOption.getLabel(), probListOption.getValue()));
	      }
    }

    // A Method which report a problem
    public PageReference reportAProblem(){
        try{
            OCSUGC_Content_ReportedProblem__c crProblem = new OCSUGC_Content_ReportedProblem__c();
            crProblem.OCSUGC_Content_ID__c = contId;
            crProblem.OCSUGC_Type_of_Content__c = contType;
            crProblem.OCSUGC_Reported_By__c = UserInfo.getUserId();
            crProblem.OCSUGC_Reason_for_Reporting__c = reportReason;
            crProblem.OCSUGC_Reason_for_Flag__c = flagType;
            //10-April-2015  Naresh Shiwani  Added dc parameter Ref T-377223
            if(contType == 'Discussion' && !isFGAB) {
                crProblem.OCSUGC_Content_URL__c = '/' + networkName+'/OCSUGC_DiscussionDetail?Id='+contId  + '&dc=' + isDC;
            } else if(contType == 'Discussion' && isFGAB) {
                crProblem.OCSUGC_Content_URL__c = '/' +  networkName+'/OCSUGC_FGABDiscussionDetail?fgab=true&id='+contId+'&g='+fgabGroupId  + '&dc=' + isDC;
            } else if(contType == 'Event'){
                crProblem.OCSUGC_Content_URL__c = '/' +  networkName+'/OCSUGC_EventDetail?eventId='+contId  + '&dc=' + isDC;
            } else if(contType == 'Knowledge File'){
                crProblem.OCSUGC_Content_URL__c = '/' +  networkName+'/OCSUGC_KnowledgeArtifactDetail?knowledgeArtifactId='+contId  + '&dc=' + isDC;
            } else if(contType == 'Poll') { // T-335681
                crProblem.OCSUGC_Content_URL__c = '/' +  networkName+'/OCSUGC_FGABPollDetail?fgab=true&id='+contId+'&g='+fetchGroupIdForPoll()  + '&dc=' + isDC;
            }

            insert crProblem;

            // Send Email to OCSUGC Admins Group about this problem
            sendEmailOnReportAProblem(crProblem);

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.OCSUGC_ReportAProblem_Success);
            ApexPages.addMessage(myMsg);
            isError = false;
            exMsg = Label.OCSUGC_ReportAProblem_Success;
        }catch(Exception ex){

            //ApexPages.addMessages(ex);
            isError = true;
            exMsg = ex.getMessage();
        }

        return null;
    }

    // T-335681 - Will fetch GroupId for Poll
    private String fetchGroupIdForPoll() {
      String groupId = null;
      for(OCSUGC_Poll_Details__c objPollDetail : [ SELECT id,OCSUGC_Group_Id__c
				                                           FROM   OCSUGC_Poll_Details__c
				                                           WHERE  OCSUGC_Poll_Id__c  =:contId LIMIT 1]) {
	      if(objPollDetail.OCSUGC_Group_Id__c != null) {
	        groupId = objPollDetail.OCSUGC_Group_Id__c;
	      }
      }
   	  return groupId;
    }


    //  sending an email to public group 'OCSUGC Admins' based on the Email Template
    //  argument - Content_ReportedProblem__c object
    //  @return none
    public void sendEmailOnReportAProblem(OCSUGC_Content_ReportedProblem__c crProblem)
    {
        String queryString;
		//10-April-2015  Naresh Shiwani  Added dc parameter Ref T-377223
        if(crProblem.OCSUGC_Type_of_Content__c == 'Discussion'){
            queryString = '?Id='+crProblem.OCSUGC_Content_ID__c + '&dc=' + isDC;
        }else if(crProblem.OCSUGC_Type_of_Content__c == 'Event'){
            queryString = '?eventId='+crProblem.OCSUGC_Content_ID__c + '&dc=' + isDC;
        }else if(crProblem.OCSUGC_Type_of_Content__c == 'Knowledge File'){
            queryString = '?knowledgeArtifactId='+crProblem.OCSUGC_Content_ID__c + '&dc=' + isDC;
        }else if(crProblem.OCSUGC_Type_of_Content__c == 'Poll'){
            queryString = '?Id='+crProblem.OCSUGC_Content_ID__c+'&fgab=true&g='+fetchGroupIdForPoll() + '&dc=' + isDC;
        }

        String currentPageURL = URL.getCurrentRequestUrl().toExternalForm() + queryString;

        //list will contain Email drafts which needs to be send
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();


        //Query and fetch the Email Template Format for sending email to Public Group
        EmailTemplate objEmailTemplate = [SELECT Id,
                                                 Subject,
                                                 HtmlValue,
                                                 Body
                                           FROM EmailTemplate
                                           WHERE DeveloperName = :OCSUGC_REPORT_A_PROBLEM_EMAIL_TEMPLATE
                                           LIMIT 1];

        String strHtmlBody = objEmailTemplate.HtmlValue;
        String strPlainBody = objEmailTemplate.Body;

        // replace the logged in user's name dynamically in HTMLBody
        // Added Label Value Naresh T-363912
  		strHtmlBody = strHtmlBody.replace('{!$Label.OCSUGC_AllEmail_Header_Image_Path}', Label.OCSUGC_AllEmail_Header_Image_Path);
        strHtmlBody = strHtmlBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
        strHtmlBody = strHtmlBody.replace('USER_LAST_NAME', Userinfo.getLastName());

        // replace the logged in user's name dynamically in Plain Body
        strPlainBody = strPlainBody.replace('USER_FIRST_NAME', Userinfo.getFirstName());
        strPlainBody = strPlainBody.replace('USER_LAST_NAME', Userinfo.getLastName());

        // replace the content type in HTMLBody
        strHtmlBody = strHtmlBody.replace('CONTENT_TYPE', crProblem.OCSUGC_Type_of_Content__c);
        strPlainBody = strPlainBody.replace('CONTENT_TYPE', crProblem.OCSUGC_Type_of_Content__c);

        // replace the reason for problem in HTMLBody
        if(crProblem.OCSUGC_Reason_for_Flag__c == REASON_OTHER) {
          strHtmlBody = strHtmlBody.replace('REASON_FOR_REPORTING', crProblem.OCSUGC_Reason_for_Reporting__c);
       	  strPlainBody = strPlainBody.replace('REASON_FOR_REPORTING', crProblem.OCSUGC_Reason_for_Reporting__c);
        }
        else {
       	  strHtmlBody = strHtmlBody.replace('REASON_FOR_REPORTING', crProblem.OCSUGC_Reason_for_Flag__c);
       	  strPlainBody = strPlainBody.replace('REASON_FOR_REPORTING', crProblem.OCSUGC_Reason_for_Flag__c);
       }

        // replace the detail page url in HTMLBody
        strHtmlBody = strHtmlBody.replace('DETAIL_PAGE_URL', '<a href="'+currentPageURL+'">View Details</a>');
        strPlainBody = strPlainBody.replace('DETAIL_PAGE_URL', '<a href="'+currentPageURL+'">View Details</a>');


        //Query and fetch all the Group member associated with OCSUGC Admins public group
        for(GroupMember objGroupMember : [SELECT Id,
                                                 UserOrGroupId
                                          FROM GroupMember
                                          WHERE Group.DeveloperName = :OCSUGC_Constants.OCSUGC_ADMIN_PUBLIC_GROUP])
        {
            //filter only direct associated Users
            if(String.ValueOf(objGroupMember.UserOrGroupId).startsWith('005'))
            {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setSaveAsActivity(false);
                //
                // Use Organization Wide Address
								if(orgWideEmailAddressId != null)
									email.setOrgWideEmailAddressId(orgWideEmailAddressId);
								else 
								  email.setSenderDisplayName('OCSUGC');
                email.setSubject(objEmailTemplate.Subject);
                email.setHtmlBody(strHtmlBody);
                email.setPlainTextBody(strPlainBody);
                email.setTargetObjectId(objGroupMember.UserOrGroupId);
                lstEmail.add(email);
            }
        }

        try{
            List<Messaging.SendEmailResult> results =new List<Messaging.SendEmailResult>();
            //Sending Email here
            if(!lstEmail.IsEmpty())
                results = Messaging.sendEmail(lstEmail);
            //iterate the result and find if email fails to sent
            for(Messaging.SendEmailResult result : results)
            {
              if(!result.IsSuccess())
                strEmailStatusMsg = Label.OCSUGC_EmailNotSuccessfullySent;
            }
        }
        catch(Exception ex){
            //ApexPages.addMessages(ex);
            isError = true;
            exMsg = ex.getMessage();
        }
    }
}
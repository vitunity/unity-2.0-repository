//
// (c) 2014 Appirio, Inc.
//
// A Test class for Evaluate functionality of Spheros_ReportAProblemController.
//
// 8th Aug, 2014     Satyanarayan Choudhary       Original
//
@isTest
private class OCSUGC_ReportAProblemControllerTest {
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static Network ocsugcNetwork;
   static {
   	 ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '1234', '3'));
     //lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id, '2345', '2'));
     //insert lstUserInsert;
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;

     //System.runAs(adminUsr1) {
        //lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        //lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        //lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        //lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        //lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;

     //}
   }
    static testMethod void testReportAProblem() {

       OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent','Events',null,'Team','Test');
       testContent.OCSUGC_Bookmark_Id__c = UserInfo.getUserId();
       testContent.OCSUGC_Application_Grant_Access__c = true;
       testContent.OCSUGC_Status__c = 'Approved';
       insert testContent;


       List<FeedItem> lstFeedItem = new List<FeedItem>();
       FeedItem testFeed1 = OCSUGC_TestUtility.createFeedItem(testContent.Id, false);
       lstFeedItem.add(testFeed1);

       OCSUGC_Knowledge_Exchange__c exchange = new OCSUGC_Knowledge_Exchange__c(OCSUGC_Title__c='test');
       exchange.OCSUGC_Status__c='Approved';
       insert exchange;

       FeedItem testFeed2 = OCSUGC_TestUtility.createFeedItem(exchange.Id, false);
       lstFeedItem.add(testFeed2);

        FeedItem item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(), false);
        lstFeedItem.add(item);

        FeedItem item2 = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(), false);
        lstFeedItem.add( item2);
        insert lstFeedItem;

        CollaborationGroup cgroup = OCSUGC_TestUtility.createGroup('testGroup','public',ocsugcNetwork.id,true);
        CollaborationGroupMember member = OCSUGC_TestUtility.createGroupMember(adminUsr1.id, cgroup.Id, true);

        OCSUGC_Tags__c newtag = OCSUGC_TestUtility.createTags('tag-test');
        insert newtag;

        OCSUGC_FeedItem_Tag__c itemtag =  new OCSUGC_FeedItem_Tag__c();
        itemtag.OCSUGC_FeedItem_Id__c = item2.id;
        itemtag.OCSUGC_Tags__c = newtag.id;
        insert itemtag;

        List<OCSUGC_Tags__c> lstTags = new List<OCSUGC_Tags__c>();
        OCSUGC_Tags__c testTag = OCSUGC_TestUtility.createTags('Test Tag');
        lstTags.add(testTag);

        OCSUGC_Tags__c testTag2 = OCSUGC_TestUtility.createTags('Test Tag 2');
        lstTags.add(testTag2);

        insert lstTags;

        List<OCSUGC_Intranet_Content_Tags__c> lstIntranetTags = new List<OCSUGC_Intranet_Content_Tags__c>();
        OCSUGC_Intranet_Content_Tags__c contentTag = OCSUGC_TestUtility.createIntranetContentTags(testContent.Id, testTag.Id);
        lstIntranetTags.add(contentTag);

        OCSUGC_Intranet_Content_Tags__c contentTag2 = OCSUGC_TestUtility.createIntranetContentTags(testContent.Id, testTag2.Id);
        lstIntranetTags.add(contentTag2);
        insert lstIntranetTags;

        Test.startTest();

        OCSUGC_ReportAProblemController controller = new OCSUGC_ReportAProblemController();

        System.runAs(managerUsr){

        controller.contId = 'test id';
        controller.contType = 'Event';
        controller.flagType = 'Other';
        System.assertEquals(controller.errorMsg,Label.OCSUGC_Problem_Report_Already_Exist_Event);
        controller.contType = 'Discussion';
        System.assertEquals(controller.errorMsg,Label.OCSUGC_Problem_Report_Already_Exist_Discussion);
        controller.contType ='Knowledge File';
        System.assertEquals(controller.errorMsg,Label.OCSUGC_Problem_Report_Already_Exist_KA);
        controller.contType ='Poll';
        System.assertEquals(controller.errorMsg,Label.OCSUGC_Problem_Report_Already_Exist_Poll);
        controller.contType= 'Event';
        controller.reportReason = 'Test Reason';

        // execute report a problem method
        controller.reportAProblem();

        // Verify functionality
        System.assert(controller.errorMsg != null);

        }

        System.runAs(managerUsr){
        controller.contId = 'testid';
        controller.contType = 'Discussion';

        controller.reportReason = 'Test Reason';
        // execute report a problem method
        controller.reportAProblem();
        System.assertEquals(controller.isAlreadyReported, false);
        }

        System.runAs(managerUsr){
        controller.contId = 'testid';
        controller.contType = 'Knowledge Artifact';
        controller.reportReason = 'Test Reason';
        // execute report a problem method
        controller.reportAProblem();
        // Test Send Email Notification Functionality
        List<OCSUGC_Content_ReportedProblem__c> crProblemList = [SELECT Id, OCSUGC_Content_Id__c, OCSUGC_Type_of_Content__c, OCSUGC_Reason_for_Reporting__c, OCSUGC_Reason_for_Flag__c FROM OCSUGC_Content_ReportedProblem__c];
        controller.sendEmailOnReportAProblem(crProblemList.get(0));
        }

        System.runAs(managerUsr){
        controller.contId = 'testid';
        controller.contType = 'Poll';
        controller.reportReason = 'Test Reason';
        controller.reportAProblem();
        }
        Test.stopTest();

    }
}
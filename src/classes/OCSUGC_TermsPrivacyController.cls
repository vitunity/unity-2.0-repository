//
// (c) 2014 Appirio, Inc.
//
// Controller class for OCSUGC_TermsPrivacy component. 
// 12 Dec, 2014     Puneet Sardana       Original Ref T-350728
// 24 Apr, 2015     Naresh K Shiwani     Task Ref T-381186
public without sharing class OCSUGC_TermsPrivacyController extends OCSUGC_Base {
    public Boolean showAccept { get; set; }
    public String defaultTab { get; set; }
    public String termsAndConditions {get;set;}
    public Boolean hasAgree {get; set;}
    public String dcInvitedUserId {get; set;}
    public User objUser {get; set;}
    public boolean firstTimeLogIn {get;set;}
    public OCSUGC_TermsPrivacyController() {
        String fgabGroupId = ApexPages.currentPage().getParameters().get('g');
        dcInvitedUserId  = ApexPages.currentPage().getParameters().get('contId');
        if(isFGAB){
            for(OCSUGC_CollaborationGroupInfo__c gpInfo : [ Select OCSUGC_Group_TermsAndConditions__c
                                                            From OCSUGC_CollaborationGroupInfo__c
                                                            Where OCSUGC_Group_Id__c =:fgabGroupId limit 1]) {
                  system.debug(' ===========gpInfo.OCSUGC_Group_TermsAndConditions__c=========== ' + gpInfo.OCSUGC_Group_TermsAndConditions__c);
                  termsAndConditions = gpInfo.OCSUGC_Group_TermsAndConditions__c;
             }
       }
    }
    
    //public OCSUGC_TermsPrivacyController(OCSUGC_HeaderController controller) {
    
    //}
    
    // @description: check the validity of dc user 
    // @param: 
    // @param: 
    // @return: page reference
    // 17-Feb-2015  Puneet Sardana   Ref T-362532,T-362529  (Method Removed and added to OCSUGC_Base)
    
    
    // @description: This methods accept terms
    // @param: 
    // @param: 
    // @return: page reference
    // 12-Jan-2015  Puneet Sardana   Ref T-350728
    public PageReference agreeTerms() {
        System.debug('Start agreeTerms');
        //getting user id by parameters
      String strUserId = Apexpages.currentPage().getParameters().get('uId');
      String strcurrentUserId;
      //checking user is there or not
      if(strUserId != null && strUserId != '' )
         strcurrentUserId = strUserId;
      else
         strcurrentUserId = UserInfo.getUserId();

      firstTimeLogIn = false;
      //retreiving user related info
      objUser = [SELECT Id,Name,Contact.AccountId,Contact.PasswordReset__c,Contact.OCSDC_UserStatus__c, Contact.OktaId__c,
                      OCSUGC_Accepted_Terms_of_Use__c, OCSDC_AcceptedTermsOfUse__c, ContactId
                      FROM User
                      WHERE Id = :strCurrentUserId limit 1];
      
      if(objUser.Contact.OCSDC_UserStatus__c != 'Member'){
          firstTimeLogIn = true;    
      }
      
      //if AcceptedTermsofUse is false then change it to true otherwise false
      if(isDc && (!objUser.OCSDC_AcceptedTermsOfUse__c || firstTimeLogIn)){
        objUser.OCSDC_AcceptedTermsOfUse__c = true;
        update objUser;
//          assignUserDCMemberPermissionSet(objUser.Id);
        // Start Have included below logic in if condition  T-381186
        List<Contact> lstContactUpdate = new List<Contact>();
        //Update Contact
        for(Contact ct : [SELECT Id,OCSDC_UserStatus__c
                          FROM Contact
                          WHERE Id=:objUser.ContactId
                          LIMIT :OCSUGC_Constants.SF_QUERY_LIMIT]) {
           ct.OCSDC_UserStatus__c = OCSUGC_Constants.DevCloud_User_Status_Member;  
           lstContactUpdate.add(ct);                
        }
        try {
            if(lstContactUpdate.size() > 0) {
                update lstContactUpdate;
            }
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        // End T-381186
      }
      else if(!objUser.OCSUGC_Accepted_Terms_of_Use__c){
        objUser.OCSUGC_Accepted_Terms_of_Use__c = true;
        update objUser;
       }
       System.debug('End agreeTerms');
       return null;
    }
    
    // @description: This methods assign developer cloud member permission set to user
    // @param: User Id
    // @return: none
    // 17-Feb-2015  Harshit Jain   Ref T-362537
    public PageReference assignUserDCMemberPermissionSet() {
            System.debug('Start assignUserDCMemberPermissionSet');
            PageReference pageRef;
            System.debug('isDC========'+isDc);
            System.debug('objUser.OCSDC_AcceptedTermsOfUse__c========'+objUser.OCSDC_AcceptedTermsOfUse__c);
            System.debug('objUser.Contact.OCSDC_UserStatus__c========'+objUser.Contact.OCSDC_UserStatus__c);
            
            // To Fetch Developer Cloud NetworkId
            String DCNetworkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_NetworkName);
            list<User> userList = new list<User>();
            userList.add(objUser); 
              
            //if(isDC && objUser.OCSDC_AcceptedTermsOfUse__c && objUser.Contact.OCSDC_UserStatus__c == Label.OCSDC_Invited){ //ONCO -474
            if(isDC && objUser.OCSDC_AcceptedTermsOfUse__c){
            
                PermissionSetAssignment permissionSetAssign;
            
            
            for(PermissionSet permissionSet : [SELECT Id FROM PermissionSet WHERE Name =:OCSUGC_Constants.DC_COMMUNITY_MEMBER_SET Limit 1]) {
                permissionSetAssign = new PermissionSetAssignment();
                permissionSetAssign.AssigneeId = objUser.id;
                permissionSetAssign.PermissionSetId = permissionSet.Id;
            } 
            
            try {
                if(permissionSetAssign != null) {
                    insert permissionSetAssign; 
                    // Disabling Chatter Emails for Developer Cloud
                    OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(userList , DCNetworkId);

                    // Add user to correct OKTA group
                    OCSUGC_Utilities.addUserGroupInOktaAfterDCApproved(objUser.Contact.OktaId__c,'PUT');
                    
                }
            } catch(DMLException e) {
                
            }
          }
          //Fetching User�s Contact.Account.Legal_Name__c.
          for(Account objAccount : [Select Id, Legal_Name__c from Account where Id =: objUser.Contact.AccountId limit 1] ) {
            //If User�s Contact.Account.Legal_Name__c is not equal to Varian Medical Systems,
            //then navigate the user to the Security Q & A form
            //else dont navigate. 
            //Changed  16-Jan-2015  Puneet Sardana  Changed conditions Ref T-353560 
            objAccount.Legal_Name__c = OCSUGC_Utilities.splitLegalName(objAccount.Legal_Name__c);
            if(objAccount.Legal_Name__c != OCSUGC_Constants.CONTACT_ACCOUNT_NAME 
                //&& !isManagerOrContributorOrReadonly
                && !isManagerOrContributorOrReadonlyOrModerator 
                && !profileName.containsIgnoreCase(OCSUGC_Constants.SYSTEM_ADMIN_PROFILE)
                && !objUser.Contact.PasswordReset__c) { //redirect to Security Q & A form page
              //pageRef = new pagereference(Label.Spheros_Continue_Agree_Action + '?CurrentuserId=' + strcurrentUserId);
              pageRef = Page.OCSUGC_QAForm;
              if(isDC){
                pageRef.getParameters().put('dc','true');
              }
              pageRef.getParameters().put('CurrentuserId',objUser.id);
            }
            else if(isDC){
                pageRef = new PageReference(System.label.OCSUGC_Salesforce_Instance+'/'+
                                            OCSUGC_Utilities.getCurrentCommunityNetworkName(isDC) +'/' +
                                            'OCSDC_Home?dc=true');
            }
            else { //redirect to OCSUGC_Home page            
                    pageRef = Page.OCSUGC_Home;
            }
          }
          pageRef.getParameters().put('isResetPwd','true');
          pageRef.setredirect(true);
          System.debug('End assignUserDCMemberPermissionSet');
          return pageRef ; 
            
    }
    
    // @description: This methods cancel terms
    // @param: 
    // @param: 
    // @return: page reference
    // 12-Jan-2015  Puneet Sardana   Ref T-350728
    public PageReference cancelTerms() {
      if(!isDC) {
        PageReference pageRef = Page.OCSUGC_MyLogOut;
        pageRef.setredirect(true);        
        return pageRef ;
      } else {
        PageReference pageRef = Page.OCSDC_Home;
        pageRef.setredirect(true);        
        return pageRef ;
      }
        
    }
}
/*
Name        : OCSUGC_TermsPrivacyControllerTest
Updated By  : Rohit Mathur - Appirio India
Date        : 21th May, 2015
Purpose     : Test the functionality of OCSUGC_TermsPrivacyController class.
*/
@isTest
public class OCSUGC_TermsPrivacyControllerTest {
   
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User portalUser, portalUser2, standardUser;
   static Account account;
   static Contact contact1,contact2;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1;
   static Network ocsugcNetwork;
   static List<CollaborationGroupMember> lstGroupMemberInsert;
   static List<CollaborationGroup> lstGroupInsert;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     account = OCSUGC_TestUtility.createAccount('test account', true);
     contact1 = OCSUGC_TestUtility.createContact(account.Id, false);
     
     lstContactInsert = new List<Contact>();     
     contact1.OCSDC_UserStatus__c = Label.OCSDC_Invited;
     lstContactInsert.add(contact1);//insert contact1;
     
     contact2 = OCSUGC_TestUtility.createContact(account.Id, false);
     contact2.OCSDC_UserStatus__c = Label.OCSDC_Invited;
     lstContactInsert.add(contact2);//insert contact2;
     insert lstContactInsert;
     
     lstUserInsert = new List<User>();
     portalUser = OCSUGC_TestUtility.createPortalUser(false, portal.Id,contact1.Id,'John', Label.OCSUGC_Varian_Employee_Community_Member);
     portaluser.OCSUGC_Accepted_Terms_of_Use__c = false;
     portalUser.OCSDC_AcceptedTermsOfUse__c = false;
     
     portalUser2 = OCSUGC_TestUtility.createPortalUser(false, portal.Id,contact2.Id,'John2', Label.OCSUGC_Varian_Employee_Community_Member);
     portalUser2.OCSUGC_Accepted_Terms_of_Use__c = false;
     portalUser2.OCSDC_AcceptedTermsOfUse__c = true;   
       
     standardUser = OCSUGC_TestUtility.createStandardUser(false, admin.Id, 'Priam', '2');
     standardUser.OCSUGC_Accepted_Terms_of_Use__c = false;
     standardUser.OCSDC_AcceptedTermsOfUse__c = true;
     
     lstUserInsert.add(portalUser);
     lstUserInsert.add(portalUser2);
     lstuserInsert.add(standardUser);
     insert lstUserInsert;
     lstGroupInsert = new List<CollaborationGroup>();
     lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false));
     lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Unlisted',ocsugcNetwork.id,false));
     lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Public',ocsugcNetwork.id,false));
     insert lstGroupInsert;
     System.runAs(standardUser) {
       PermissionSet managerPermissionSet = OCSUGC_TestUtility.getMemberPermissionSet();
       PermissionSetAssignment assignment = OCSUGC_TestUtility.createPermissionSetAssignment( managerPermissionSet.Id, standardUser.Id, true);
       groupMember1 = OCSUGC_TestUtility.createGroupMember(standardUser.Id, fgabGroup.Id, false);
       //NOT REQUIRED insert groupMember1;
     }
   }
   
    // Test Method for agreeTerms
    public static testmethod void agreeTerms_TEST() {
      Test.startTest();
      System.runAs(portalUser) {
        PageReference ref = Page.OCSUGC_TermsOfUse;
        ref.getParameters().put('g',fgabGroup.Id);
        ref.getParameters().put('uId',portalUser.Id);
        ref.getParameters().put('contId',contact1.Id);
        ref.getParameters().put('dc','true');
        ref.getParameters().put('fgab','true');
        Test.setCurrentPage(ref);
        OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
        termsPrivacyController.showAccept = true;
        termsPrivacyController.defaultTab = 'privacyStatement';
        termsPrivacyController.termsAndConditions = 'testTermsAndConditions';
        termsPrivacyController.hasAgree = true;
        termsPrivacyController.agreeTerms();
      }
      Test.stopTest();
    }
    
    // TEST METHOD for assignUserDCMemberPermissionSet IsDC=false
    public static testmethod void assignUserDCMemberPermissionSet_DC_FALSE_TEST() {
      Test.startTest();
      System.runAs(portalUser2) {
        PageReference ref = Page.OCSUGC_TermsOfUse;
        ref.getParameters().put('g',fgabGroup.Id);
        ref.getParameters().put('uId',portalUser2.Id);
        ref.getParameters().put('contId',contact2.Id);
        ref.getParameters().put('dc','false');
        ref.getParameters().put('fgab','true');
        Test.setCurrentPage(ref);
        OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
        termsPrivacyController.showAccept = true;
        termsPrivacyController.defaultTab = 'privacyStatement';
        termsPrivacyController.termsAndConditions = 'testTermsAndConditions';
        termsPrivacyController.hasAgree = true;
        portalUser2 = [ SELECT Id, ContactId, Contact.OCSDC_UserStatus__c, OCSDC_AcceptedTermsOfUse__c, Contact.AccountId, Contact.PasswordReset__c 
                        FROM User WHERE Id =: portalUser2.Id];
        termsPrivacyController.objUser = portalUser2;
        termsPrivacyController.assignUserDCMemberPermissionSet();
      }
      Test.stopTest();
    }
    
    // TEST METHOD for assignUserDCMemberPermissionSet IsDC=True
    public static testmethod void assignUserDCMemberPermissionSet_DC_TRUE_TEST() {
      Test.startTest();
      System.runAs(portalUser2) {
        PageReference ref = Page.OCSUGC_TermsOfUse;
        ref.getParameters().put('g',fgabGroup.Id);
        ref.getParameters().put('uId',portalUser2.Id);
        ref.getParameters().put('contId',contact2.Id);
        ref.getParameters().put('dc','true');
        ref.getParameters().put('fgab','true');
        Test.setCurrentPage(ref);
        OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
        termsPrivacyController.showAccept = true;
        termsPrivacyController.defaultTab = 'privacyStatement';
        termsPrivacyController.termsAndConditions = 'testTermsAndConditions';
        termsPrivacyController.hasAgree = true;
        portalUser2 = [ SELECT Id, ContactId, Contact.OCSDC_UserStatus__c, OCSDC_AcceptedTermsOfUse__c, Contact.AccountId,Contact.OktaId__c, Contact.PasswordReset__c 
                        FROM User WHERE Id =: portalUser2.Id];
        termsPrivacyController.objUser = portalUser2;
        termsPrivacyController.assignUserDCMemberPermissionSet();
      }
      Test.stopTest();
    }
    
    // Test Method for cancelTerms for isDC = true
    public static testMethod void cancelTerms_ISDC_TEST() {
        Test.startTest();
        System.runAs(portalUser2) {
            PageReference ref = Page.OCSUGC_TermsOfUse;
            ref.getParameters().put('g',fgabGroup.Id);
            ref.getParameters().put('uId',portalUser2.Id);
            ref.getParameters().put('contId',contact2.Id);
            ref.getParameters().put('dc','true');
            ref.getParameters().put('fgab','true');
            Test.setCurrentPage(ref);
            OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
            termsPrivacyController.showAccept = true;
            termsPrivacyController.defaultTab = 'privacyStatement';
            termsPrivacyController.termsAndConditions = 'testTermsAndConditions';
            termsPrivacyController.hasAgree = true;
            termsPrivacyController.cancelTerms();
        }
        Test.stopTest();
    }
    
    // Test Method for cancelTerms for isDC = true
    public static testMethod void cancelTerms_ISNOTDC_TEST() {
        Test.startTest();
        System.runAs(portalUser2) {
            PageReference ref = Page.OCSUGC_TermsOfUse;
            ref.getParameters().put('g',fgabGroup.Id);
            ref.getParameters().put('uId',portalUser2.Id);
            ref.getParameters().put('contId',contact2.Id);
            ref.getParameters().put('dc','true');
            ref.getParameters().put('fgab','true');
            Test.setCurrentPage(ref);
            OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
            termsPrivacyController.showAccept = true;
            termsPrivacyController.defaultTab = 'privacyStatement';
            termsPrivacyController.termsAndConditions = 'testTermsAndConditions';
            termsPrivacyController.hasAgree = true;
            termsPrivacyController.cancelTerms();
        }
        Test.stopTest();
    }
    
    /**
     * 12 Dec 2016, Puneet Mishhra
     */
    public static testMethod void cancelTersm_test() {
        Test.StartTest();
            PageReference ref = Page.OCSUGC_TermsOfUse;
            ref.getParameters().put('g',fgabGroup.Id);
            ref.getParameters().put('dc','false');
            OCSUGC_TermsPrivacyController termsPrivacyController = new OCSUGC_TermsPrivacyController();
            termsPrivacyController.cancelTerms();
        Test.Stoptest();
    }
    
}
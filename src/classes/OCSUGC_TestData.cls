public class OCSUGC_TestData{

    public static User getUser(Id contactId,String userName){
            Profile tempProfile= [SELECT Id FROM Profile WHERE Name='VMS MyVarian - Customer User'];
            User tempUser= new User(Alias = 'Shital', Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8', LastName='Bhujbal', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = TempProfile.Id,IsActive=true,
            TimeZoneSidKey='America/New_York',UserName=userName+contactId,EmployeeNumber='a9876',contactId=contactId,OCSUGC_User_Role__c='Varian Community Contributor'
            );
            return tempUser;
    }

}
/*
Name        : OCSUGC_TestUtility
Updated By  : Rishi Khirbat - Appirio India
Date        : 09th June, 2014
Purpose     : This is helper class of test classes that contains generic function.

Updated by  :   Puneet Mishra,  method: getOCSUGCNetwork(), Network was queried with name 'OCSUGC', which is changed to OncoPeer
                                OCSUGCNetworkId value after querying Network on basis of Name '%OCSUGC%', which is now changed to '%OncoPeer%'
*/
@isTest
public class OCSUGC_TestUtility {

    //public static String OCSUGCNetworkId = [SELECT Id FROM Network WHERE Name LIKE '%OCSUGC%' LIMIT 1].Id;
    public static String OCSUGCNetworkId = [SELECT Id FROM Network WHERE Name LIKE '%OncoPeer%' LIMIT 1].Id;
    //Method to create OCSUGC_TermsConditions_UserLog__c log
    public static OCSUGC_TermsConditions_UserLog__c createTermsConditionsLog(String appName,String contactId) {
        OCSUGC_TermsConditions_UserLog__c termsLog = new OCSUGC_TermsConditions_UserLog__c();
        termsLog.OCSUGC_Contact_Id__c = contactId;
        termsLog.OCSUGC_Date_of_acceptance_TC__c = DateTime.now();
        termsLog.OCSDC_App_Name__c = appName;
        return termsLog;
    }
    //Method to create Intranet_Content__c
    public static OCSUGC_Intranet_Content__c createIntranetContent(String intraConName, String recTypeName, Id parent, String template, String contentType) {
        OCSUGC_Intranet_Content__c intranetContent = new OCSUGC_Intranet_Content__c();
        intranetContent.Name = intraConName;
        intranetContent.RecordTypeId = OCSUGC_Utilities.getRecordTypeId('OCSUGC_Intranet_Content__c', recTypeName);
        //intranetContent.OCSUGC_Parent__c = parent;
        intranetContent.OCSUGC_Status__c = 'New';
        intranetContent.OCSUGC_NetworkId__c = OCSUGC_Utilities.getCurrentCommunityNetworkId(false);
        return intranetContent;
    }
   public static OCSUGC_Application_Permissions__c createApplicationPermissions(String contactId,String intranetContentId,String status,Boolean isInsert) {
    OCSUGC_Application_Permissions__c  permission = new OCSUGC_Application_Permissions__c();
    permission.OCSUGC_Contact__c = contactId;
    permission.OCSUGC_Access_Level__c = 'TBD';
    permission.OCSUGC_Approval_Status__c = status;
    permission.OCSUGC_Application__c = intranetContentId;
    if(isInsert) {
        insert permission;
    }
    return permission;
   }

   public static FeedComment createFeedComment(String feedItemId,Boolean isInsert) {
      FeedComment comment = new FeedComment();
      comment.feeditemid = feedItemId;
      comment.commentbody =  'This is a comment to your post';
      comment.CreatedById = UserInfo.getUserId();
      if(isInsert) {
        insert comment;
      }
      return comment;
   }
   public static OCSUGC_KFiles_ApprovedFileType__c createKAFileTypes(Boolean isInsert) {
    OCSUGC_KFiles_ApprovedFileType__c kaTypes = new OCSUGC_KFiles_ApprovedFileType__c();
    kaTypes.Name = 'Document';
    kaTypes.OCSUGC_File_Extension__c ='DOC,PDF';
    kaTypes.OCSUGC_File_Size__c = 2345;
    kaTypes.OCSUGC_Requires_Admin_Approval__c = true;
    if(isInsert) {
        insert kaTypes;
    }
    return kaTypes;
   }
    public static EmailTemplate createEmailTemplate(String emailTemplateName,String developerName,String subject,String body,String htmlvalue,Boolean isInsert) {
          EmailTemplate emailTemp = new EmailTemplate();
        emailTemp.isActive = true;
        emailTemp.Name = emailTemplateName;
        emailTemp.Subject = subject;
        emailTemp.Body = body;
        emailTemp.HtmlValue = htmlvalue;
        emailTemp.DeveloperName = developerName;
        emailTemp.TemplateType = 'text';
        emailTemp.FolderId = UserInfo.getUserId();
        if(isInsert) {
         insert emailTemp;
        }
        return emailTemp;

    }

    public static OCSUGC_Intranet_Content__c createEvent(String groupId,Date startDate,Date endDate,Boolean isInsert) {
         OCSUGC_Intranet_Content__c event = new OCSUGC_Intranet_Content__c();
       event.OCSUGC_GroupId__c = groupId;
       event.OCSUGC_Description__c = 'test description';
       event.OCSUGC_Event_Start_Date__c = startDate;
       event.OCSUGC_Event_End_Date__c = endDate;
       event.OCSUGC_Event_Start_Time__c = '02:00:AM';
       event.OCSUGC_Event_End_Time__c = '03:00:AM';
       if(isInsert) {
        insert event;
       }
       return event;
    }

    //Method to create Tags__c
    public static OCSUGC_Tags__c createTags(String tagName) {
        OCSUGC_Tags__c tag = new OCSUGC_Tags__c();
        tag.OCSUGC_Tag__c = tagName;
        return tag;
    }

    //Method to create Intranet_Content_Tags__c
    public static OCSUGC_Intranet_Content_Tags__c createIntranetContentTags(Id intranetContentId, Id tag) {
        OCSUGC_Intranet_Content_Tags__c intranetContentTag = new OCSUGC_Intranet_Content_Tags__c();
        intranetContentTag.OCSUGC_Intranet_Content__c = intranetContentId;
        intranetContentTag.OCSUGC_Tags__c = tag;
        return intranetContentTag;
    }

    public static Id createContentEditorPermissionSet(Boolean isInsert) {
        List<PermissionSet> contentEditorPermissionSets = [Select Name, Id From PermissionSet Where Name = 'Content_Editors' limit 1];
        if(contentEditorPermissionSets.size() > 0) {
            return contentEditorPermissionSets.get(0).Id;
        }else {
            PermissionSet contentEditorPermissionSet = new PermissionSet();
            contentEditorPermissionSet.Name = 'Content_Editors';

            if(isInsert) {
                insert contentEditorPermissionSet;
            }
            return contentEditorPermissionSet.Id;
        }
    }

    public static Profile getAdminProfile() {
         List<Profile> adminProfile = [SELECT Id FROM Profile WHERE Name LIKE '%System Administrator%' Limit 1];
         if(adminProfile.size() > 0)
           return adminProfile[0];
         else
           return null;
    }
    public static Profile getManagerProfile() {
         List<Profile> managerProfile = [SELECT Id FROM Profile WHERE Name LIKE '%VMS OncoPeer - Manager%' Limit 1];
       if(managerProfile.size() > 0)
         return managerProfile[0];
       else
         return null;
    }
    public static Profile getPortalProfile() {
         List<Profile> portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' Limit 1];
       if(portalProfile.size() > 0)
         return portalProfile[0];
       else
         return null;
    }
    public static UserRole getRole(){
        List<UserRole> roleList = [SELECT id FROM UserRole WHERE Name LIKE '%Service Global Execs, Admins%' Limit 1];
        if(roleList.size() > 0){
            return roleList[0];
        }else{
            return null;
        }
    }
   public static PermissionSet getManagerPermissionSet() {
       List<PermissionSet> managerPermissionSet = [SELECT Id FROM PermissionSet WHERE Name LIKE '%Manager%' LIMIT 1];
       if(managerPermissionSet.size() > 0)
         return managerPermissionSet[0];
       else
         return null;
   }
   public static PermissionSet getMemberPermissionSet() {
       List<PermissionSet> memberPermissionSet = [SELECT Id FROM PermissionSet WHERE Name LIKE '%Member%' LIMIT 1];
       if(memberPermissionSet.size() > 0)
         return memberPermissionSet[0];
       else
         return null;
   }

    //Method to create test record for standard user
    public static User createStandardUser(boolean isInsert,String profileId,String name, String usrRole) {
        User usr = new User();
        usr.Email = 'test'+name +'@bechtel.com';
        usr.Username = generateUserName(name, 'ocsugc', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.OCSUGC_User_Role__c = null;
        if(usrRole != null) usr.OCSUGC_User_Role__c = usrRole;
        if(isInsert) insert usr;
        return usr ;
    }

    //Method to create test record for portal user
    public static User createPortalUser(boolean isInsert,String profileId,String contactId,String name,String role) {
         User usr = new User();
        usr.Email = 'test'+ name +'@bechtel.com';        
        //usr.Username = 'test' + name + '@ocsugc.com.ocsugc';
        usr.Username = generateUserName(name, 'ocsugc', 70);
        usr.LastName = 'test' ;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.IsActive = true;
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = profileId;
        if(role == Label.OCSUGC_Varian_Employee_Community_Member) {
          usr.ContactId = contactId;
        }
        usr.OCSUGC_User_Role__c = role;
        usr.OCSUGC_Accepted_Terms_of_Use__c = true;
        if(isInsert) insert usr;
        return usr ;
    }

    public static Network getOCSUGCNetwork() {
        return [SELECT Id FROM Network WHERE Name LIKE '%OncoPeer%' LIMIT 1][0];
        //return [SELECT Id FROM Network WHERE Name LIKE '%OCSUGC%' LIMIT 1][0];

    }
    public static OCSUGC_Users_Visibility__c createUserVisibilityCustomSetting(String name,Boolean manager,Boolean contributor,Boolean member,Boolean readonly,Boolean isInsert) {
        OCSUGC_Users_Visibility__c userVisibility = new OCSUGC_Users_Visibility__c();
        userVisibility.Name = name;
        userVisibility.OCSUGC_Managers__c = manager;
        userVisibility.OCSUGC_Contributors__c = contributor;
        userVisibility.OCSUGC_Members__c = member;
        userVisibility.OCSUGC_ReadOnly__c = readonly;
        if(isInsert) {
            insert userVisibility;
        }
        return userVisibility;
    }

    //create CollaborationGroup
    public static CollaborationGroup createGroup(String groupName, String groupType,String networkId, Boolean isInsert) {
        CollaborationGroup collaborationGroup = new CollaborationGroup(Name=groupName, CollaborationType=groupType,NetworkId=networkId);
        OCSUGC_CollaborationGroupInfo__c collaborationInfo = new OCSUGC_CollaborationGroupInfo__c();
        collaborationInfo.OCSUGC_Group_Type__c = groupType;
        collaborationGroup.CollaborationType = groupType;
        collaborationInfo.Name = groupName;
        collaborationInfo.OCSUGC_Group_TermsAndConditions__c = 'Test terms and conditions';
        if(isInsert) {
          insert collaborationGroup;
          collaborationInfo.OCSUGC_Group_Id__c = collaborationGroup.Id;
          insert collaborationInfo;
        }
        return collaborationGroup;
    }
    public static OCSUGC_CollaborationGroupInfo__c createCollaborationGroupInfo(String groupId) {
        OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
        info.OCSUGC_Group_Id__c = groupId;
        info.OCSUGC_Email__c = 'ptest@appirio.com';
        info.OCSUGC_Group_TermsAndConditions__c = 'test terms and conditions';
        return info;
    }
    
    public static OCSUGC_LikeUnlike__c createLikeUnlikeCustomSetting(String name, Boolean inOncoPeer, Boolean inFGAB, Boolean inDC) {
        OCSUGC_LikeUnlike__c likeUnlike = new OCSUGC_LikeUnlike__c();
        likeUnlike.Name = name;
        likeUnlike.Can_Like_Unlike_in_DC__c = inDC;
        likeUnlike.Can_Like_Unlike_in_FGAB__c = inFGAB;
        likeUnlike.OCSUGC_LikeUnlike__c = inOncoPeer;
        return likeUnlike;      
    }
    public static CollaborationGroup createGroup(String groupName, String groupType,String collaborationType, String networkId, Boolean isInsert) {
        CollaborationGroup collaborationGroup = new CollaborationGroup(Name=groupName, CollaborationType=groupType,NetworkId=networkId);
        OCSUGC_CollaborationGroupInfo__c collaborationInfo = new OCSUGC_CollaborationGroupInfo__c();
        collaborationInfo.OCSUGC_Group_Type__c = collaborationType;
        collaborationGroup.CollaborationType = groupType;
        collaborationInfo.Name = groupName;
        collaborationInfo.OCSUGC_Group_TermsAndConditions__c = 'Test terms and conditions';
        if(isInsert) {
          insert collaborationGroup;
          collaborationInfo.OCSUGC_Group_Id__c = collaborationGroup.Id;
          insert collaborationInfo;
        }
        return collaborationGroup;
    }
    //create CollaborationGroupMember
    public static CollaborationGroupMember createGroupMember(String memberId, String groupId, Boolean isInsert){
        CollaborationGroupMember groupMember = new CollaborationGroupMember();
        groupMember.memberId = memberId;
        groupMember.CollaborationRole = 'Standard';
        groupMember.CollaborationGroupId = groupId;
        if(isInsert) {
          insert groupMember;
        }
        return groupMember;
    }

    //create CollaborationGroupMemberRequest
    public static CollaborationGroupMemberRequest createGroupMemberRequest(String requesterId, String groupId, Boolean isInsert){
        CollaborationGroupMemberRequest groupMemberRequest = new CollaborationGroupMemberRequest();
        groupMemberRequest.RequesterId = requesterId;
        groupMemberRequest.CollaborationGroupId = groupId;
        if(isInsert) {
          insert groupMemberRequest;
        }
        return groupMemberRequest;
    }

    //create Attachment
    public static Attachment createAttachment(Boolean isInsert, ID parentId){
        Blob articleData = Blob.valueof('TestData');
        Attachment article = new Attachment(Name='testAttachment',
                                            body = articleData,
                                            ParentId = parentId);
        if(isInsert) insert article;
        return article;
    }
    
    //create notification 
    public static OCSUGC_Intranet_Notification__c createNotification(Id userId, Boolean isInsert) {
        OCSUGC_Intranet_Notification__c notification = new OCSUGC_Intranet_Notification__c();
        notification.OCSUGC_Is_Viewed__c = false;
        notification.OCSUGC_Link_to__c = 'testlink';
        notification.OCSUGC_Description__c= 'testdescription';
        notification.OCSUGC_User__c = userId;
        if(isInsert)
            insert notification;
            
        return notification;
    }
    
    //create menuInfo 
    public static OCSUGC_MenuInfo__c createMenuInfo(String name, Integer sequence, Boolean isDC, Boolean isFgab,Boolean isInsert) {
        OCSUGC_MenuInfo__c menu = new OCSUGC_MenuInfo__c();
        menu.Name = name;
        menu.OCSUGC_Display_Menu__c = name;
        menu.OCSUGC_IsActive__c = true;
        menu.OCSDC_IsDCCreateMenu__c= isDC;
        menu.OCSUGC_IsFGABCreateMenu__c = isFgab;
        menu.OCSUGC_Sequence__c = sequence;
        menu.OCSUGC_URL__c = name;
        if(isInsert)
            insert menu;
            
        return menu;
    }
    
    //create menu 
    public static OCSDC_CreateMenu__c createMenuDC(String name, Boolean isInsert) {
        OCSDC_CreateMenu__c menu = new OCSDC_CreateMenu__c();
        menu.Name = name;
        menu.OCSUGC_AnnounceEvent__c = true;
        menu.OCSUGC_CreatePoll__c = true;
        menu.OCSUGC_FormGroup__c= true;
        menu.OCSUGC_StartDiscussion__c = true;
        if(isInsert)
            insert menu;
            
        return menu;
    }
    
    
    //create menu 
    public static OCSUGC_CreateMenu__c createMenu(String name, Boolean isInsert) {
        OCSUGC_CreateMenu__c menu = new OCSUGC_CreateMenu__c();
        menu.Name = name;
        menu.OCSUGC_AnnounceEvent__c = true;
        menu.OCSUGC_CreatePoll__c = true;
        menu.OCSUGC_FormGroup__c= true;
        menu.OCSUGC_StartDiscussion__c = true;
        if(isInsert)
            insert menu;
            
        return menu;
    }

    public static EntitySubscription createEntitySubscription(String NetId,String ParId,String SubsId,Boolean isInsert) {
       EntitySubscription subs = new EntitySubscription();
       subs.NetworkId = NetId;
       subs.ParentId = ParId;
       subs.SubscriberId = SubsId;
       if(isInsert) {
        insert subs;
       }
       return subs;
    }

    //Method to create Knowledge Exchange
    public static OCSUGC_Knowledge_Exchange__c createKnwExchange(String groupId,String groupName,Boolean isInsert){
        OCSUGC_Knowledge_Exchange__c knwExchange = new OCSUGC_Knowledge_Exchange__c();
        knwExchange.OCSUGC_Artifact_Type__c = 'Document';
        knwExchange.OCSUGC_Group__c = groupName;
        knwExchange.OCSUGC_Group_Id__c = groupId;
        knwExchange.OCSUGC_Title__c = 'Test Knowledge';
        knwExchange.OCSUGC_Summary__c = 'Test Description';
        knwExchange.OCSUGC_Status__c = 'Approved';
        knwExchange.OCSUGC_NetworkId__c = OCSUGC_Utilities.getCurrentCommunityNetworkId(false);
        if(isInsert){
          insert knwExchange;
        }
        return KnwExchange;
    }

    public static OCSUGC_Knowledge_Tag__c createKnowledgeTag(Id knowledgeId,Boolean isInsert) {
        OCSUGC_Knowledge_Tag__c knowTag = new OCSUGC_Knowledge_Tag__c();
        knowTag.OCSUGC_knowledge_exchange__c = knowledgeId;
        OCSUGC_Tags__c tag = new OCSUGC_Tags__c();
        tag.OCSUGC_Tag__c = 'test puneet tag';
        insert tag;
        knowTag.OCSUGC_Tags__c = tag.Id;
        if(isInsert) {
            insert knowTag;
        }
        return knowTag;
    }
    public static Blacklisted_Word__c createBlackListWords(String word) {
        Blacklisted_Word__c black = new Blacklisted_Word__c();
        black.is_Active__c = true;
        black.Word__c = word;
        insert black;
        return black;
    }

    //Method to Create a Group
    public static Group createGroup(String name,Boolean isInsert){

        Group adminGroup = new Group();
        adminGroup.Name = name;
        if(isInsert){
            insert adminGroup;
        }
        return adminGroup;
    }

    public static PermissionSetAssignment createPermissionSetAssignment(String permissionSetId,String assigneeId,Boolean isInsert) {
         PermissionSetAssignment assignment = new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = assigneeId);
         if(isInsert) {
              insert assignment;
         }
         return assignment;
    }

    public static OCSUGC_DiscussionDetail__c createDiscussion(String groupId,String discussionId,Boolean isInsert) {
        OCSUGC_DiscussionDetail__c discussionDetail = new OCSUGC_DiscussionDetail__c();
      discussionDetail.OCSUGC_DiscussionId__c = discussionId;
      discussionDetail.OCSUGC_GroupId__c = groupId;
      discussionDetail.OCSUGC_Description__c = 'test description';
      if(isInsert){
        insert discussionDetail;
      }
      return discussionDetail;
    }

    public static FeedItem createFeedItem(String parentId,Boolean isInsert) {
         FeedItem item = new FeedItem();
       item.ParentId = parentId;
       item.Body = 'test body';
       if(isInsert) {
        insert item;
       }
       return item;
    }

    public static OCSUGC_FeedItem_Tag__c createFeedItemTag(String tagId, String feedItemId, Boolean isInsert) {
      OCSUGC_FeedItem_Tag__c feedTag = new OCSUGC_FeedItem_Tag__c();
      feedTag.OCSUGC_Tags__c = tagId;
      feedTag.OCSUGC_FeedItem_Id__c = feedItemId;
      if(isInsert) {
        insert feedTag;
       }
       return feedTag;
    }

    //Member to create GroupMember
    public static GroupMember createGroupMemberNC(Id groupId, Id userId, Boolean isInsert){
        GroupMember groupMem = new GroupMember();
        groupMem.GroupId = groupId;
        groupMem.UserOrGroupId = userId;

        if(isInsert){
            insert groupMem;
        }
        return groupMem;

    }

    //Method to Create a Contact
    public static Contact createContact(String accountId, Boolean isInsert){
        Contact cont = new Contact();
        cont.FirstName='TestFirstName'+Math.random();
    cont.LastName = 'TestContact' + Math.random();
    cont.Email = 'TestUser' +system.now().millisecond() + '@test.com';
    cont.AccountId = accountId;
    cont.MailingCountry='India';
    cont.MyVarian_member__c = true;
    cont.PasswordReset__c = false;
    cont.OCSUGC_Notif_Pref_ReqJoinGrp__c = true;
    cont.OCSUGC_Notif_Pref_EditGroup__c = true;
    cont.OCSUGC_Notif_Pref_PrivateMessage__c = true;
    cont.OCSUGC_Notif_Pref_InvtnJoinGrp__c = true;
    cont.OCSUGC_UserStatus__c = Label.OCSUGC_Approved_Status;
        if(isInsert){
            insert cont;
        }
        return cont;

    }

    public static Account createAccount(String accName,Boolean isInsert) {
           Account accnt= new Account();
       accnt.Name= accName;
       accnt.Country__c = 'USA';
       accnt.Country__c= 'USA';
        accnt.BillingCity= 'Los Angeles';
        accnt.BillingStreet ='3333 W 2nd St';
        accnt.BillingState ='CA';
        accnt.BillingPostalCode = '90020';
       if(isInsert){
         insert accnt;
      }
      return accnt;
    }

  // Method to create Contacts with associated with an account Id
  public static List<Contact>  getContacts(Integer numOfContact,Id accountId){
  List<Contact> lstContacts = new List<Contact>();
   for(integer i = 0;i<numOfContact; i++) {
        Contact objContact = new Contact();
        objContact.FirstName = 'Test' + i;
        objContact.LastName = 'TestContact' + i;
        objContact.Email = 'TestUset' +i+ '@test.com';
        objContact.AccountId = accountId;
        objContact.MyVarian_member__c = true;
        objContact.OCSUGC_Varian_Affiliation__c ='test';
        objContact.MailingCountry='test'+i;
        lstContacts.add(objContact);
        }
    return lstContacts;
  }
  
  public static OCSUGC_Group_Invitation__c createGroupInvitation(String userId,String groupId) {
     OCSUGC_Group_Invitation__c invitation = new OCSUGC_Group_Invitation__c();
     invitation.OCSUGC_User_Id__c = userId;
     invitation.OCSUGC_Group_Id__c = groupId;
     invitation.OCSUGC_Group_Invitation_Date__c = Date.today();
     return invitation;
  }

  //Method to create OCSUGC_Poll_Details__c
    public static OCSUGC_Poll_Details__c createPollDetail(Id grpId, String grpName, ID feedId, String pollBody, Boolean isInsert) {
        OCSUGC_Poll_Details__c pollDetail = new OCSUGC_Poll_Details__c();
        pollDetail.OCSUGC_Poll_Id__c = feedId;
        pollDetail.OCSUGC_Group_Id__c = grpId;
        pollDetail.OCSUGC_Group_Name__c = grpName;
        pollDetail.OCSUGC_Poll_Question__c = pollBody;
        pollDetail.OCSUGC_Is_Deleted__c = false;
        if(isInsert) {
            insert pollDetail;
        }
        return pollDetail;
    }
    
    //Method to create OCSUGC_Poll_Details__c
    public static FeedLike createFeedLike(String feedItemId, String userId,  Boolean isInsert) {
        FeedLike flike = new FeedLike();
        flike.FeedItemId = feedItemId;
        flike.CreatedById = userId;
        if(isInsert) {
            insert flike;
        }
        return flike;
    }
   public static OCSUGC_PreventFGABInvitation__c createPreventFGABInvitation(String roleName,Boolean groupInv) {
        OCSUGC_PreventFGABInvitation__c preventInvitation = new OCSUGC_PreventFGABInvitation__c();
        preventInvitation.Name = roleName;
        preventInvitation.OCSUGC_GroupInvitation__c = groupInv;
        return preventInvitation;
    }
    //Method to create OCSUGC_PollDetail_Tags__c
    public static OCSUGC_PollDetail_Tags__c createPollDetailTag(Id tagId, Id feedId, Boolean isInsert) {
        OCSUGC_PollDetail_Tags__c pollDetailTag = new OCSUGC_PollDetail_Tags__c();
        pollDetailTag.OCSUGC_Tags__c = tagId;
        pollDetailTag.OCSUGC_PollFeedItem__c = feedId;
        if(isInsert) {
            insert pollDetailTag;
        }
        return pollDetailTag;
    }

    //Method to create EntitySubscription
    public static EntitySubscription createEntitySubscription(Id subscriberId, Id parentId, Boolean isInsert) {
        EntitySubscription es = new EntitySubscription();
    es.SubscriberId = subscriberId;
    es.ParentId = parentId;
    if(isInsert) {
        insert es;
    }
    return es;
    }

 // @description: This method returns community Id. If parameter is true this means its a DC community else OCSUGC
    // @param: if community name is DC
    // @return: community Id
    // 04-May-2015  Vipul Jain  Ref T-392114
    public static String getCurrentCommunityNetworkId(Boolean isDC) {
        return isDC ? Label.OCSDC_NetworkId : Label.OCSUGC_NetworkId;
    }

// @description: This method returns Unique USer Name.
        // @return: UserName
    // 04-May-2015  Vipul Jain  Ref T-392114
public static String generateUserName(String prefix,String orgName,Integer size) {
String userName;
String orgId = UserInfo.getOrganizationId();
// Org id - 18 , 1 for @ , 4 for .com
Integer randomStringGeneratorLen = size - orgId.length() - prefix.length() - orgName.length() - 4 - 1;
Blob blobKey = crypto.generateAesKey(256);
String randomString = EncodingUtil.convertToHex(blobKey).substring(0,randomStringGeneratorLen);
return prefix + randomString + orgId + '@' + orgName + '.com';
}
}
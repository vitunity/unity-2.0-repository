@isTest
public with sharing class OCSUGC_UpdateCustomSettingExt_NewTest {
	
	static testMethod void testUpdateContributorSettingstoTrue() {
		Profile pf_manager = OCSUGC_TestUtility.getManagerProfile();     
		User usr_manager = OCSUGC_TestUtility.createStandardUser(false, pf_manager.Id, 'mgruser888','Varian Community Manager');  
		Insert usr_manager;
		
		Insert new OCSUGC_CreateMenu__c(Name = 'Varian Community Contributor',OCSUGC_IsCreateMenuVisible__c = False);
		Insert new OCSUGC_FlagContentOrComment__c (Name = 'Varian Community Contributor',OCSUGC_Can_Create_Comments__c = False);
		  
		system.runAs(usr_manager){
			PageReference ref = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?setting=true'); 
			Test.setCurrentPage(ref);    
			OCSUGC_UpdateCustomSettingExtension_New controller = new OCSUGC_UpdateCustomSettingExtension_New(null);
			controller.setting1.IsCreateMenuVisible = True;
			controller.setting2.CanCreateComments = True;
			controller.SaveSettings();       
		} 
		
		system.assertEquals(OCSUGC_CreateMenu__c.getValues('Varian Community Contributor').OCSUGC_IsCreateMenuVisible__c,True);
		system.assertEquals(OCSUGC_FlagContentOrComment__c.getValues('Varian Community Contributor').OCSUGC_Can_Create_Comments__c,True);                          
    }
    
	static testMethod void testUpdateContributorSettingstoFalse() {
		Profile pf_manager = OCSUGC_TestUtility.getManagerProfile();     
		User usr_manager = OCSUGC_TestUtility.createStandardUser(false, pf_manager.Id, 'mgruser888','Varian Community Manager');  
		Insert usr_manager;
		
		Insert new OCSUGC_CreateMenu__c(Name = 'Varian Community Contributor',OCSUGC_IsCreateMenuVisible__c = True);
		Insert new OCSUGC_FlagContentOrComment__c (Name = 'Varian Community Contributor',OCSUGC_Can_Create_Comments__c = True);
		  
		system.runAs(usr_manager){
			PageReference ref = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?setting=true'); 
			Test.setCurrentPage(ref);    
			OCSUGC_UpdateCustomSettingExtension_New controller = new OCSUGC_UpdateCustomSettingExtension_New(null);
			controller.setting1.IsCreateMenuVisible = False;
			controller.setting2.CanCreateComments = False;
			controller.SaveSettings();       
		} 
		
		system.assertEquals(OCSUGC_CreateMenu__c.getValues('Varian Community Contributor').OCSUGC_IsCreateMenuVisible__c,False);
		system.assertEquals(OCSUGC_FlagContentOrComment__c.getValues('Varian Community Contributor').OCSUGC_Can_Create_Comments__c,False);                          
	}
    
}
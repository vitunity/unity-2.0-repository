/*
Name        : OCSUGC_UpdateCustomSettingExtension controller extension for OCSUGC_ContactCommunityUserStatus_New Component
Created By  : Puneet Mishra (@Appirio)
Date        : 08-08-2015
Description : An Apex controller extension with Proxy custom settings that binds with the input checkbox fields on the Settings Tab 
              of OncoPeer User Administration Page and allows the Community Manager to update the Custom Settings that controls whether
              Community Contributors can see the Create Menu and/or they can share comments on content (ONCO-347)
*/
public with sharing class OCSUGC_UpdateCustomSettingExtension_New {
	
	
    public SettingProxy1 setting1 {get; set;}
    public SettingProxy2 setting2 {get; set;}

    public OCSUGC_UpdateCustomSettingExtension_New(OCSUGC_ContactCommunityUserStatusCon_New ctrlParam){
      
       setting1 = new SettingProxy1([SELECT Id, Name, OCSUGC_IsCreateMenuVisible__c FROM OCSUGC_CreateMenu__c WHERE Name =:Label.OCSUGC_Varian_Employee_Community_Contributor]);
       setting2 = new SettingProxy2([SELECT Id, Name, OCSUGC_Can_Create_Comments__c FROM OCSUGC_FlagContentOrComment__c WHERE Name =:Label.OCSUGC_Varian_Employee_Community_Contributor]);                   
    }
    
    public PageReference SaveSettings() {     
        update setting1.getCreateMenuSetting();
        update setting2.getCommentSetting();        
    	
    	PageReference redirectPage = new PageReference('/apex/OCSUGC_CommunityUserAdministration_New?setting=true');
    	redirectPage.setRedirect(true);
        return redirectPage;
    }

    public class SettingProxy1 {
        public Id Id {get;set;}
        public String Name {get;set;}
        public Boolean IsCreateMenuVisible {get;set;}
        
        public SettingProxy1(OCSUGC_CreateMenu__c cs1) {
            this.Id = cs1.Id;
            this.Name = cs1.Name;
            this.IsCreateMenuVisible = cs1.OCSUGC_IsCreateMenuVisible__c;
        }             
        public OCSUGC_CreateMenu__c getCreateMenuSetting() {
            return new OCSUGC_CreateMenu__c(                
                Id = this.Id,
                Name = this.Name,
                OCSUGC_IsCreateMenuVisible__c = this.IsCreateMenuVisible
            );
        }
    }
    public class SettingProxy2 {
        public Id Id {get;set;}
        public String Name {get;set;}
        public Boolean CanCreateComments {get;set;}
        
        public SettingProxy2(OCSUGC_FlagContentOrComment__c cs2) {
            this.Id = cs2.Id;
            this.Name = cs2.Name;
            this.CanCreateComments = cs2.OCSUGC_Can_Create_Comments__c;
        }             
        public OCSUGC_FlagContentOrComment__c getCommentSetting() {
            return new OCSUGC_FlagContentOrComment__c(              
                Id = this.Id,
                Name = this.Name,
                OCSUGC_Can_Create_Comments__c = this.CanCreateComments
            );
        }
    }
}
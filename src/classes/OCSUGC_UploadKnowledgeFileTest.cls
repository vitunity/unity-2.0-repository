@isTest(SeeAllData=false)
public class OCSUGC_UploadKnowledgeFileTest {
    static Profile admin,portal;
    static User adminUsr1,adminUsr2, memberUsr;
    static Account account; 
    static Contact contact1,contact2,contact3;
    static List<User> lstUserInsert;
    static List<Contact> lstContactInsert;
    static CollaborationGroup publicGroup,privateGroup,fgabGroup;
    static CollaborationGroupMember groupMember1;
    static Network ocsugcNetwork;
    static List<CollaborationGroup> lstGroupInsert;
    static List<CollaborationGroupMember> lstGroupMemberInsert;
    static OCSUGC_Knowledge_Exchange__c ka1;
    static OCSUGC_KFiles_ApprovedFileType__c kaTypes;   
    static {
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile(); 
        OCSUGC_TestUtility.createBlackListWords('p test blacklist');
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        account = OCSUGC_TestUtility.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert; 
        lstUserInsert = new List<User>();
        lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1'));
        lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '2'));
        lstUserInsert.add( memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        insert lstUserInsert;     
        lstGroupInsert = new List<CollaborationGroup>();
        lstGroupInsert.add(publicGroup = OCSUGC_TestUtility.createGroup('test public group 1','Public',ocsugcNetwork.id,false)); 
        lstGroupInsert.add(privateGroup = OCSUGC_TestUtility.createGroup('test private group 1','Private',ocsugcNetwork.id,false));
        lstGroupInsert.add(fgabGroup = OCSUGC_TestUtility.createGroup('test unlisted group 1','Public',ocsugcNetwork.id,false));
        insert lstGroupInsert;
        PermissionSet memberPermSet = OCSUGC_TestUtility.getMemberPermissionSet();

        System.runAs(adminUsr1) {
            PermissionSetAssignment memberPermSetAssignment = OCSUGC_TestUtility.createPermissionSetAssignment(memberPermSet.Id, memberUsr.id, true);
            lstGroupMemberInsert = new List<CollaborationGroupMember>();
            lstGroupMemberInsert.add(groupMember1 = OCSUGC_TestUtility.createGroupMember(adminUsr1.Id, publicGroup.Id, false));
            insert lstGroupMemberInsert;
            kaTypes = OCSUGC_TestUtility.createKAFileTypes(true);               
        }
    }
  
    public static testmethod void test_OCSUGC_UploadKnowledgeFileTest() {
        Test.startTest();
        ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);  
        PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
        ref.getParameters().put('id',ka1.Id);
        Test.setCurrentPage(ref);  
        OCSUGC_UploadKnowledgeFile kaController = new OCSUGC_UploadKnowledgeFile();

        // Skip Upload
        kaController.skipFileUpload();
        // redirect to next page
        kaController.redirectToKnowledgePage();

        Test.stopTest();
    }
    
    public static testmethod void test_Id_Null() {
        Test.startTest();
        ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);  
        PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
        ref.getParameters().put('id',null);
        Test.setCurrentPage(ref);  
        OCSUGC_UploadKnowledgeFile kaController = new OCSUGC_UploadKnowledgeFile();
        Test.stopTest();
    }
    
    public static testmethod void test_feeditem() {
        Test.startTest();
        ka1 = OCSUGC_TestUtility.createKnwExchange(publicGroup.Id,publicGroup.Name,true);  
        PageReference ref = Page.OCSUGC_CreateKnowledgeArtifact;
        ref.getParameters().put('id',null);
        Test.setCurrentPage(ref);  
        OCSUGC_UploadKnowledgeFile kaController = new OCSUGC_UploadKnowledgeFile();
    
        ContentVersion contentVersion_1 = new ContentVersion(
            Title = 'Content',
            PathOnClient = 'Content.html',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion_1;
        FeedItem feed = new FeedItem (
            parentid = ka1.id,
            visibility = 'InternalUsers',
            type = 'ContentPost',
            RelatedRecordId = contentVersion_1.id,
            Body = 'Hello'
        );
        //then create feedItem record
        try {
            insert feed;    
        } catch (exception e) {
            System.assert(e.getMessage().contains('Sorry..!! You cannot attach this file format'));
        }
        Test.stopTest();
    }
        
}
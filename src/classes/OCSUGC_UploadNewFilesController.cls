/**
 *	author:	Puneet Mishra
 *			class used to show the latest version file on page and provide functionality to replace the current file with the latest varsion
 */
public without sharing class OCSUGC_UploadNewFilesController extends OCSUGC_Base {
	
	@TestVisible private Id feedItemId;			// As image is stored as FeedItem, FeddItemId will be used to fetch the FeedItem detail as well as Discussion_Details
	@TestVisible private OCSUGC_DiscussionDetail__c discussion;	
	public FeedItem feedItem{get;set;}			// feedItem storing file Image(OncoPeer) and File(Developer/OAB)
	public Attachment attachFile{get;set;}		// attachment to upload the file as FeedItem
	public ContentVersion discussionImage {get;set;}
	public String commmaSeperatedExtensions {get; set;}
	
	/**
	 *	Contructor
	 */
	public OCSUGC_UploadNewFilesController() {
		discussion = new OCSUGC_DiscussionDetail__c();
		attachFile = new Attachment();
		feedItem = new feedItem();
		discussionImage = new ContentVersion();
		feedItemId = ApexPages.CurrentPage().getParameters().get('fId');
		commmaSeperatedExtensions = Label.OCSUGC_Event_File_Extensions;
	}
	
	public void fetchFeedInfo() {
		try {feedItem =[	SELECT ParentId, Parent.Name, Visibility, Type, Body, RelatedRecordId, ContentType, ContentFileName
					FROM FeedItem
					WHERE Id =: feedItemId ];
		discussion = [	SELECT OCSUGC_Group__c, OCSUGC_GroupId__c, OCSUGC_Description__c, OwnerId
						FROM OCSUGC_DiscussionDetail__c
						WHERE OCSUGC_DiscussionId__c =:feedItemId];
		
			/*query on ContentVersion to fetch the contentDocumentId, cannot query on ContentDocument as ContentDocument do not store feedItem/related Id*/
	    	ContentVersion cont = new ContentVersion();
	    	cont = [ SELECT Id, Title, PublishStatus, OwnerId, IsDeleted, Description, ContentDocumentId, VersionNumber,NetworkId
					 FROM ContentVersion WHERE Id =: feedItem.RelatedRecordId];
	    	
	    	// query to fetch the latest ContentVersion file
	    	discussionImage = [Select Id, ContentDocumentId, VersionNumber, NetworkId FROM ContentVersion Where ContentDocumentId =: cont.ContentDocumentId Order By VersionNumber desc LIMIT 1]; 
	        system.debug('==== discussionImage ===== ' + discussionImage);    	
		} catch (Exception e) {
			system.debug(' message ' + e.getMessage() + ' Line Number ' + e.getLineNumber());
		}
    }
	
	/**
	 *	for discussion images/files uploaded saved on user's chatter not on record, to query the related record
	 *	First query ContentVersion using FeedItem.RelatedRecordId
	 *	Second query on ContentDocument using the ContentVersion Id fetched from First Query  
	 */
	public PageReference uploadNewImage() {
		
		if(feedItem != null ) {
		
			ContentVersion newContentVer = new ContentVersion();
			newContentVer.Title = attachFile.Name;
			newContentVer.NetworkId = discussionImage.NetworkId;
			newContentVer.ContentDocumentId = discussionImage.ContentDocumentId;
			newContentVer.PathOnClient = attachFile.Name;
			newContentVer.versionData = attachFile.Body;
			insert newContentVer;
			
			// null the attachment and newContentVer as to avoid running into max view state error
			newContentVer = null;
			attachFile.body = null;
		}	
		return redirectToDetailPage();
	}
	
	/**
	 *	redirect user to detail page
	 */
	public PageReference redirectToDetailPage() {
		PageReference ref = Page.OCSUGC_DiscussionDetail;
		ref.getParameters().put('Id',feedItem.Id );
		ref.getParameters().put('dc',''+isDC);
		ref.setRedirect(true);
		return ref;
	}
	
}
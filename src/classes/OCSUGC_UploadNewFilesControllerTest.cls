@isTest
public with sharing class OCSUGC_UploadNewFilesControllerTest {
    
    static FeedItem item;
    static OCSUGC_DiscussionDetail__c discussionDetail;
    static CollaborationGroup cPublicGroup;
    static Network OCSUGCNetwork;
    
    static{
    	item = OCSUGC_TestUtility.createFeedItem(userInfo.getUserId(),true);
    	OCSUGCNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
    	cPublicGroup = OCSUGC_TestUtility.createGroup('testpdiscussionpublic3Group','Public',OCSUGCNetwork.Id,true);
    	discussionDetail = OCSUGC_TestUtility.createDiscussion(cPublicGroup.Id, item.Id, true);
    }
    
    public static testMethod void OCSUGC_UploadNewFilesControllerTest() {
    	Test.StartTest();
    		Attachment attach=new Attachment();   	
	    	attach.Name='Unit Test Attachment';
	    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
	    	attach.body=bodyBlob;
	        attach.parentId=discussionDetail.id;
	        insert attach;
    		
    		ApexPages.currentPage().getParameters().put('fId', item.Id);
    		ApexPages.currentPage().getParameters().put('dc', 'true');
    		OCSUGC_UploadNewFilesController cont = new OCSUGC_UploadNewFilesController();
    		cont.attachFile = attach;
    		cont.redirectToDetailPage();
    		cont.fetchFeedInfo();
    		cont.uploadNewImage();
    	Test.StopTest();
    }
    
}
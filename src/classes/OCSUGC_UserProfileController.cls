/**
 * OCSUGC_UserProfileController
 * This controller provides all the data and related actions 
 * to work with profile of users in OncoPeer.
 * 
 */
public without sharing class OCSUGC_UserProfileController extends OCSUGC_Base_NG {

  //Member variables
  private String profiledUserId {get; set;}           //Id of profiled user
  public User    profiledUser {get; set;}             //User object reprensting the user who is being profiled
  public Boolean isPrfUserCommManager {get; set;}     //flag to check if profiled user is community manager or not
  public Boolean isPrfUserCommContributor {get; set;} //flag to check if profiled user is community contributor or not
  public Boolean isPrfUserCommModerator {get; set;}   //flag to check if profiled user is community moderator or not
  public Boolean isPrfUserReadOnly {get; set;}        //flag to check if profiled user is read only user or not
  public Boolean isFollowing {get; set;}              //flag to check if logged in user is following profiled user or not

  /** 
   * Constructor
   */
    public OCSUGC_UserProfileController() {

    //get Id of user to profile. 
    //if no URL parameter is given use logged in user id 
    profiledUserId = ApexPages.currentPage().getParameters().get('userId');
    if(profiledUserId == null) profiledUserId = loggedInUser.Id;

    //get profiled User and associated required information
    profiledUser = [Select  u.City,
                            u.State,
                            u.Country,
                            u.Name,
                            u.FirstName,
                            u.LastName,
                            u.UserName,
                            u.Id,
                            u.Title,
                            u.CompanyName,
                            u.AboutMe,
                            u.FullPhotoUrl,
                            u.SmallPhotoUrl,
                            u.OCSUGC_User_Role__c,
                            u.contactId,
                            u.contact.Account.Legal_Name__c,
                            u.contact.OCSUGC_Varian_Affiliation__c,
                            u.contact.Title,
                            u.Contact.OCSUGC_Affiliation_Type__c
                        From User u
                        WHERE Id =: profiledUserId];

    //As per Varian (MyVarian) policy we are to use SAP Account Name (Account Legal Name) for display purposes.
    //However, this could be multiple lines so we need to split it to just use the first line for display.
    if(profiledUser.ContactId != null && profiledUser.Contact.AccountId != null)
                profiledUser.contact.Account.Legal_Name__c = OCSUGC_Utilities.splitLegalName(profiledUser.contact.Account.Legal_Name__c);

    //Check if profiled user is community manager or community contributor
    isPrfUserCommManager     = OCSUGC_Utilities.isCommunityManager(ocsugcNetworkId, profiledUser.Id, profiledUser.OCSUGC_User_Role__c);
    isPrfUserCommContributor = OCSUGC_Utilities.isCommunityContributor(ocsugcNetworkId, profiledUser.Id, profiledUser.OCSUGC_User_Role__c);
    isPrfUserCommModerator   = OCSUGC_Utilities.isCommunityModerator(ocsugcNetworkId, profiledUser.Id, profiledUser.OCSUGC_User_Role__c);
    isPrfUserCommModerator   = OCSUGC_Utilities.isCommunityModerator(ocsugcNetworkId, profiledUser.Id, profiledUser.OCSUGC_User_Role__c);
    isPrfUserReadOnly        = OCSUGC_Utilities.isReadonly(profiledUser.OCSUGC_User_Role__c);

    //Check if profiled user is being followed by logged in user in OncoPeer community or not
    List<EntitySubscription> lstEntitySubscription = [SELECT Id
                                                       FROM EntitySubscription
                                                       Where parentId =: profiledUserId and
                                                       networkId =: ocsugcNetworkId and
                                                       subscriberid =: loggedInUser.Id];
    isFollowing  = lstEntitySubscription.size() > 0 ? true : false;

    //set page title to specific user's profile
        pageTitle = 'Profile: ' + profiledUser.Name + ' | OncoPeer';
    }

  /**
   * Follow User action called via JavaScript
   * This method will make the logged in user follow the profiled user in OncoPeer community
   * @return void
   */
  public void followUserAction() {
    if ( OCSUGC_Utilities.followUser(ocsugcNetworkId, profiledUserId, loggedInUser.Id, loggedInUser.Name) ) {
      isFollowing = true;
    }
  }

  /**
   * Unfollow User action called via JavaScript
   * This method will make the logged in user unfollow the profiled user in OncoPeer community
   * @return void
   */
  public void unfollowUserAction() {
    if ( OCSUGC_Utilities.unfollowUser(ocsugcNetworkId, profiledUserId, loggedInUser.Id, loggedInUser.Name) ) {
      isFollowing = false;
    }
  }
}
/*
Name        : CAKE_UserProfileControllerTest
Created By  : Priyanka Kumar
Date        : 30 July, 2014
Purpose     : Test class  for OCSUGC_UserProfileController
*/

@isTest
public class OCSUGC_UserProfileControllerTest {

    public static testmethod void test_UserProfileController(){


        Profile admin = OCSUGC_TestUtility.getAdminProfile();
        User testuser = OCSUGC_TestUtility.createStandardUser(true, admin.Id, 'test', '1');

        test.startTest();

            OCSUGC_UserProfileController profileController = new OCSUGC_UserProfileController();
            system.assertNotEquals(profileController.profiledUser,null);

            ApexPages.currentPage().getParameters().put('userId', testuser.Id);
            OCSUGC_UserProfileController profileController1 = new OCSUGC_UserProfileController();
            profileController1.followUserAction();
            system.assertEquals(profileController1.isFollowing, true);

            //Boolean unfollowsuccess = profileController1.createIntranetNotification(' has unfollowed you.');
            //system.assertEquals(unfollowsuccess, true);

            profileController1.unfollowUserAction();
            system.assertEquals(profileController1.isFollowing, false);
            //profileController.showPanel();
            //profileController.showConfirmation();
            //profileController.editProfile();

        test.stopTest();


    }

}
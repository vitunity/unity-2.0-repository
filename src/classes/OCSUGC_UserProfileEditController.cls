/**
 * OCSUGC_UserProfileEditController class
 * This class will provide the edit capabilities on edit profile page for logged in user.
 * 
 */
public class OCSUGC_UserProfileEditController extends OCSUGC_Base_NG {

    public Integer intAboutMeCount{get; set;}
    public User userInformation {get; set;}
    public String strAboutMe{get; set;}
    public Boolean isVarianAffiliated {get; set;}
    //holds the VarianAffiliation value entered by the user
    public String strVarianAffiliation{get; set;}
    //holds the value for VarianAffiliation checkbox ether it is checked(true) or not
    public Boolean isVariantAffiliationCheck {get;set;}
    public String strFirstName {get;set;}
    public String strLastName {get;set;}
    public String strUserAboutMe {get;set;}
    public String accountLegalName {get;set;}
    public List<SelectOption> affiliatedTypePickListValues{get; set;}
    
    //Constructor
    public OCSUGC_UserProfileEditController() {
        // ONCO-426, using userId of loggedin user instead from url
            strVarianAffiliation = strFirstName = strLastName = strAboutMe = '';
            isVariantAffiliationCheck = false;
            strUserAboutMe = '';
            intAboutMeCount = 1000;

            //ONCO-421, Unique Page-Title
            pageTitle = 'Edit Profile | OncoPeer';

            userInformation = [Select  u.City,
                            u.State,
                            u.Country,
                            u.Name,
                            u.FirstName,
                            u.LastName,
                            u.UserName,
                            u.Id,
                            u.AboutMe,
                            u.FullPhotoUrl,
                            u.SmallPhotoUrl,
                            u.OCSUGC_User_Role__c,
                            u.contactId,
                            u.contact.Account.Legal_Name__c,
                            u.contact.OCSUGC_Varian_Affiliation__c,
                            u.contact.Title,
                            u.Contact.OCSUGC_Affiliation_Type__c
                        From User u
                        WHERE Id =: loggedInUser.Id];
      
            
        //for retreving current value of Varian Affiliation
        if(userInformation.Contact.OCSUGC_Affiliation_Type__c != Null){
            strVarianAffiliation = userInformation.Contact.OCSUGC_Varian_Affiliation__c;
            isVariantAffiliationCheck = true;
        }

        strFirstName = userInformation.FirstName;
        strLastName = userInformation.LastName;
        strUserAboutMe = userInformation.AboutMe;
        if(strUserAboutMe != Null && strUserAboutMe != '')
            intAboutMeCount = 1000 - strUserAboutMe.length();

        system.debug('===============strUserAboutMe: '+strUserAboutMe);
        if(userInformation.ContactId != null && userInformation.Contact.AccountId != null){
          accountLegalName = OCSUGC_Utilities.splitLegalName(userInformation.contact.Account.Legal_Name__c);
          isVarianAffiliated = System.Label.OCSUGC_Varian_Medical_Systems.equals(accountLegalName) ? true : false;
        }
        //Start Here - 20 Oct 2014    Naresh K Shiwani       (Task Ref. T-330869)
        affiliatedTypePickListValues = new List<SelectOption>();
        affiliatedTypePickListValues.add(new SelectOption('','Please Select'));
        for (Schema.PicklistEntry affiliationTypeListOption : Contact.getSObjectType().getDescribe().fields.getMap().get('OCSUGC_Affiliation_Type__c').getDescribe().getPickListValues()){
             affiliatedTypePickListValues.add(new SelectOption(affiliationTypeListOption.getLabel(), affiliationTypeListOption.getValue()));
        }
        //Ends  Here - 20 Oct 2014    Naresh K Shiwani       (Task Ref. T-330869)
    }

    
    /*
     * The binary data of the image uploaded by the user
     */
    public transient Blob photoBlobValue { get; set; }
    
    /*
     * The content type, determined by Salesforce, of
     * the image uploaded by the user
     */
    public transient String photoContentType { get; set; }
    
    /*
     * The name of the image file uploaded by the user
     */
    public transient String photoFilename { get; set; }
    
    /*
     * @return the URL for the large profile photo of the specified
     *         user in the current community
     */
    public String getLargePhotoUrl() {
        return ConnectApi.UserProfiles.getPhoto(
            ocsugcNetworkId, userInformation.id).largePhotoUrl;
    }
    
    /*
     * @return the appropriate next page after setting the 
     *         specified user's profile photo to the new
     *         image uploaded
     */
    public PageReference uploadPhoto() {
        
        ConnectApi.BinaryInput photoFileInput =
            new ConnectApi.BinaryInput(photoBlobValue, photoContentType, photoFilename);
        
        ConnectApi.UserProfiles.setPhoto(
          ocsugcNetworkId, userInformation.id, photoFileInput);
        
        return null; // Leave the user on the current page
    }

    //  redirecting to the OCSUGC_UserProfile page
    //  by updating the contact VarianAffiliation field
    //  argument.none
    //  @return a PageReference that is redirecting to OCSUGC_UserProfile page.
    public PageReference btnUpdateContact() {
      if(userInformation.id != null && userInformation.ContactId == null){
        //update the User Information with the latest Info(FirstName,LastName,AboutMe)
            User objUserInfo = new User(Id      = userInformation.Id ,
                                        LastName  = strLastName,
                                        FirstName = strFirstName,
                                        AboutMe   = strUserAboutMe
                                        );
            try{
                if(objUserInfo != Null){
                    update objUserInfo;
                }                 
            } catch(system.dmlException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
      }
        if(userInformation.id != null && userInformation.ContactId != null){
          Savepoint sp;
          //update the User Information with the latest Info(FirstName,LastName,AboutMe)
            User objUserInfo = new User(Id      = userInformation.Id ,
                                        LastName  = strLastName,
                                        FirstName = strFirstName,
                                        AboutMe   = strUserAboutMe
                                        );
            //update the Contact record with the latest Varian Affiliation value
            Contact objContact = new Contact(Id = userInformation.ContactId,
                                             LastName = strLastName,
                                             FirstName = strFirstName,
                                             OCSUGC_Varian_Affiliation__c = strVarianAffiliation,
                                             OCSUGC_Affiliation_Type__c = userInformation.contact.OCSUGC_Affiliation_Type__c);
            try{
                // Creating a savepoint
                sp = Database.setSavepoint();
                //updating into the database
                if(objUserInfo != Null && objContact != Null){
                    update objUserInfo;
                    update objContact;
                }                    
            } catch(system.dmlException ex) {
                // Save Point will be rollbacked if updation process got any exception.
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            }
        }
        //redirecting the page to readable User profile page
        Pagereference pg = Page.OCSUGC_UserProfile;
        pg.getParameters().put('userId',userInformation.Id);
        if(isDevCloud) {
            pg.getParameters().put('dc', 'true');
        }
        pg.setRedirect(true);
        return pg;
    }

    //  redirecting to the OCSUGC_UserProfile page when click cancel
    //  argument.none
    //  @return a PageReference that is redirecting to OCSUGC_UserProfile page.
    public PageReference btnCancel(){
        //redirecting the page to readable User profile page
        Pagereference pg = Page.OCSUGC_UserProfile;
        pg.getParameters().put('userId',userInformation.Id);
        if(isDevCloud) {
            pg.getParameters().put('dc', 'true');
        }
        pg.setRedirect(true);
        return pg;
    }

}
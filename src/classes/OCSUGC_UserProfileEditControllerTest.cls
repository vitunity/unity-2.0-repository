//
// (c) 2012 Appirio, Inc.
//
// Test class  for CAKE_UserProfileEditController
//
// 6 Aug 2014     Pawan Tyagi     Test class  for CAKE_UserProfileEditController
//
@isTest
public class OCSUGC_UserProfileEditControllerTest {
    public static testMethod void test_UserProfileEditController() {

        //List of Contact Object
        List<Contact> lstContacts = new List<Contact>();
        Profile portalProfile  = OCSUGC_TestUtility.getPortalProfile();

        //Creating account object
        Account  objAccount = OCSUGC_TestUtility.createAccount('test account', true);
        for(Integer i=1;i<=8;i++) {
            lstContacts.add(OCSUGC_TestUtility.createContact(objAccount.Id, false));
        }
        insert lstContacts;

        //creating User
        List<User> lstUser = new List<user>();
        for(integer i = 0;i<8;i++) {
          lstUser.add(OCSUGC_TestUtility.createPortalUser(false, portalProfile.Id, lstContacts.get(i).Id, '121'+i, Label.OCSUGC_Varian_Employee_Community_Member));
        }
        insert lstUser;
        //test start
        test.startTest();

        System.runAs (lstUser[5]) {
            OCSUGC_UserProfileEditController profileEditController = new OCSUGC_UserProfileEditController();
            profileEditController.isVariantAffiliationCheck = true;
            profileEditController.strVarianAffiliation = 'I am QA';
            //verifying valid user
            system.assertNotEquals(profileEditController.userInformation,null);
            //profileEditController.deactivateUserToCake();

            PageReference nextPage=profileEditController.btnUpdateContact();
            //verifying the user redirect to OCSUGC_UserProfile successfully
            System.assertNotEquals(null,nextPage);

            ApexPages.currentPage().getParameters().put('userId', lstUser[0].Id);
            OCSUGC_UserProfileEditController profileEditController1 = new OCSUGC_UserProfileEditController();
            //profileEditController1.followGroup();

            PageReference nextPage1=profileEditController1.btnCancel();
            //verifying the user redirect to OCSUGC_UserProfile successfully
            System.assertNotEquals(null,nextPage1);
        }
        //test stop()
        test.stopTest();
    }
    
    /**
     * createdDate : 6 Dec 2016, Tuesday
     * Puneet Mishra : 
     */
    @isTest(SeeAllData=true)
    public static void coverageCode() {
    	Test.StartTest();
    		ApexPages.currentPage().getParameters().put('dc', 'true');
    	
    		OCSUGC_UserProfileEditController cont = new OCSUGC_UserProfileEditController();
    		
    		String myString = 'StringToBlob';
    		Blob myBlob = Blob.valueof(myString);
    		Attachment attach = new Attachment();
            attach.contentType = 'image/jpg';
            attach.name = 'IMG';
            attach.body = myBlob;
			//insert attach;
			
			cont.photoBlobValue = myBlob;
    		cont.photoContentType = 'ABC';
    		cont.photoFilename = 'DEF';
    		cont.getLargePhotoUrl();
    		//cont.uploadPhoto();
    		
    		cont.btnUpdateContact();
    		cont.btnCancel();
    	Test.StopTest();
    }
}
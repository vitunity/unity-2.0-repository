/*
Name        : Spheros_UserSettingsController
Updated By  : Naresh K Shiwani (Appirio India)
Date        : 19th August, 2014
Purpose     : This class performs actions on users timezone.
*/
public class OCSUGC_UserSettingsController {

    private Id userId ;
    public User userDetails {get;set;}
    public Contact contact {get;set;}
    public String successMessage {get;set;}
    public String errorMessage {get;set;}
    //ONCO-421, Setting page-title
    public static String pageTitle{get;set;}
    public OCSUGC_UserSettingsController(){
    	successMessage = errorMessage = '';
        userId          = UserInfo.getUserId();
        //ONCO-421, Unique Page-Title
        if(ApexPages.currentPage().getParameters().get('dc') != null) {
	        Boolean isDC = Boolean.valueOf(ApexPages.currentPage().getParameters().get('dc'));
	        if(isDC) {
				pageTitle = 'User Setting | Developer';
			} 
        }else {
			pageTitle = 'User Setting | OncoPeer';
		}
        userDetails            = [Select Id, TimeZoneSidKey, ContactId From User Where Id =:userId];
        List<String> timeZones = getTimeZones();          
        String currentTimeZoneStr = UserInfo.getTimeZone().getID();
        //Set default time zone to current timezone of user  Ref T-336739
        for(String tm : timeZones) {
        	if(tm.contains(currentTimeZoneStr)) {
        		userDetails.TimeZoneSidKey = tm;
        		break;
        	}    
        }          
        if(userDetails.ContactId != null) {
	        contact = [Select OCSUGC_Notif_Pref_EditGroup__c,
	        				  OCSUGC_Notif_Pref_InvtnJoinGrp__c,
	        				  OCSUGC_Notif_Pref_PrivateMessage__c,
	        				  OCSUGC_Notif_Pref_ReqJoinGrp__c
	        		   From Contact Where Id =:userDetails.ContactId];
        }
    }
	    // @description: Method to get all timezones of user
	    // @param: NA
	    // @param: NA
	    // @return: List of strings with timezones
	    // Additional
	    // 01-12-2014  Puneet Sardana  Original Ref T-336738
	   private List<String> getTimeZones(){
	       List<String> timezones = new List<String>();      
	       Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe(); 
	       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();       
	       for(Schema.PicklistEntry f : ple){
	         timezones.add(f.getValue());
	       }          
	       return timezones;
	    }
    public Pagereference updateTimeZoneSettings() {
    	try {
	        update userDetails;
	        update contact;
	        successMessage = Label.OCSUGC_Settings_Updated_Successfully;
	        
	        return null;
        } catch(Exception e) {
            system.debug('================Error: '+e.getMessage());
            errorMessage = e.getMessage();
            return null;
        }
    }

}
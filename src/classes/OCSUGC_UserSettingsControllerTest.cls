//
// (c) 2014 Appirio, Inc.
//
// Test class for OCSUGC_UserSettingsController class.
// 12 Jan, 2015     Ravindra Shekhawat
@isTest
public class OCSUGC_UserSettingsControllerTest {
   static Profile admin,portal,manager;
   static User managerUsr,contributorUsr,memberUsr,usr4,usr5;
   static User adminUsr1,adminUsr2;
   static Account account;
   static Contact contact1,contact2,contact3,contact4,contact5;
   static List<User> lstUserInsert;
   static List<Contact> lstContactInsert;
   static CollaborationGroup publicGroup,privateGroup,fgabGroup;
   static CollaborationGroupMember groupMember1,groupMember2,groupMember3,groupMember4,groupMember5;
   static Network ocsugcNetwork;
   static {
     admin = OCSUGC_TestUtility.getAdminProfile();
     portal = OCSUGC_TestUtility.getPortalProfile();
     manager = OCSUGC_TestUtility.getManagerProfile();
     ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
     Profile adminProfile = OCSUGC_TestUtility.getAdminProfile();
     lstUserInsert = new List<User>();
     lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createStandardUser(false, adminProfile.Id, 'test1', '1'));
     lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createStandardUser(false, adminProfile.Id, 'test2', '2'));
     insert lstUserInsert;
     account = OCSUGC_TestUtility.createAccount('test account', true);
     lstContactInsert = new List<Contact>();
     lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
     lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
     insert lstContactInsert;
     System.runAs(adminUsr1) {
        lstUserInsert = new List<User>();
        lstUserInsert.add(managerUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor));
        lstUserInsert.add(memberUsr = OCSUGC_TestUtility.createPortalUser(false, portal.Id, contact3.Id, '55',Label.OCSUGC_Varian_Employee_Community_Member));
        lstUserInsert.add(usr4 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact4.Id, '33',Label.OCSUGC_Varian_Employee_Community_Manager));
        lstUserInsert.add(usr5 = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact5.Id, '43',Label.OCSUGC_Varian_Employee_Community_Manager));
        insert lstUserInsert;

     }
   }
    static testMethod void test_userSettingsController(){

       // Run as Portal User
        System.runAs(memberUsr) {
          test.startTest();
          ApexPages.currentPage().getParameters().put('userId',memberUsr.Id);
          OCSUGC_UserSettingsController userSettingController = new OCSUGC_UserSettingsController();
          System.assertNotEquals(userSettingController.userDetails,null);
	      System.assert(userSettingController.userDetails.ContactId != null);
	      System.assert(userSettingController.userDetails.TimeZoneSidKey != null);
	      System.assertNotEquals(UserSettingController.contact,null);
	      userSettingController.updateTimeZoneSettings();
	      test.stopTest();
        }
       // End of Block For Portal User
    }

    }
public class OCSUGC_UserTriggerHandler {

    public static void onBeforeInsertUpdate(List<User> lstUsers, Boolean isInsert, Map<Id, User>oldUserMap, Map<Id, User>newUserMap) {
        for(User user : lstUsers) {
            if(isInsert == true ) {
            	
            	if(user.contactId != null && (user.OCSUGC_User_Role__c == null || user.OCSUGC_User_Role__c == '') ) {
                    user.OCSUGC_User_Role__c = Label.OCSUGC_Varian_Employee_Community_Member;
                }
            } else {
                if(user.contactId != null && (user.OCSUGC_User_Role__c == null || user.OCSUGC_User_Role__c == '') && 
                    oldUserMap.get(user.Id).isActive == false && newUserMap.get(user.Id).isActive == true) {
                    user.OCSUGC_User_Role__c = Label.OCSUGC_Varian_Employee_Community_Member;
                }
            }           
        }
        // Start    T-350439    Naresh K Shiwani
        // End  T-350439    Naresh K Shiwani
    }

    public static void onAfterInsertUpdate(Map<Id, User> newMap, Map<Id, User> oldMap) {
        
        // Start    T-350439    Naresh K Shiwani
        List<User> lstNewUser = new List<User>();
        for(User newUser :[Select OCSUGC_User_Role__c, ContactId, Contact.OCSUGC_UserStatus__c, OCSUGC_Accepted_Terms_of_Use__c,MarketingKit_Agreement__c
                           From User
                           Where IsActive = true
                           And Id IN : newMap.keySet()]) {
            lstNewUser.add(newUser);
        }
        // End  T-350439    Naresh K Shiwani
        Boolean doAct = false;
        Boolean isInsert = oldMap == null;
        Map<String, String> addUserIdAndUserRole = new Map<String, String>();
        Set<String> removeUserIds = new Set<String>();
        
        // Start    T-350439    Naresh K Shiwani    Changed for loop, if condition
        //                                          and OCSUGC_User_Role__c condition.
        for(User user :lstNewUser) {
            
            if(user.OCSUGC_User_Role__c == Label.OCSUGC_Varian_Employee_Community_Manager
                || user.OCSUGC_User_Role__c == Label.OCSUGC_Varian_Employee_Community_Contributor
                || user.OCSUGC_User_Role__c == Label.OCSUGC_Varian_Employee_Community_Member
                || (user.ContactId != null && user.Contact.OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status)) {
                
                if(isInsert && user.OCSUGC_User_Role__c != null) {
                    addUserIdAndUserRole.put(user.Id, user.OCSUGC_User_Role__c);
                    doAct = true;
                } else {
                    if(!isInsert && (oldMap.get(user.Id).OCSUGC_User_Role__c != newMap.get(user.Id).OCSUGC_User_Role__c)) {
                        if(user.OCSUGC_User_Role__c != null){
                            addUserIdAndUserRole.put(user.Id, user.OCSUGC_User_Role__c);
                        }
                        removeUserIds.add(user.Id);
                        doAct = true;
                    }
                    else if(!isInsert && (oldMap.get(user.Id).isActive == false && newMap.get(user.Id).isActive == true)) {
                        if(user.OCSUGC_User_Role__c != null){
                            addUserIdAndUserRole.put(user.Id, user.OCSUGC_User_Role__c);
                        }
                        removeUserIds.add(user.Id);

                        doAct = true;
                    }
                }
            } 
        }
        
        if((lstNewUser.size() > 0) && doAct) {
            //system.assertEquals(doAct, false);
            Set<String> setPermissionSetNames = new Set<String>();
            setPermissionSetNames.add(Label.OCSUGC_VMS_Manager_Permission_set);         //Varian Community Manager
            setPermissionSetNames.add(Label.OCSUGC_VMS_Contributor_Permission_set);     //Varian Community Contributor
            setPermissionSetNames.add(Label.OCSUGC_VMS_Member_Permission_set);          //Varian Community Member
            
            Map<String, String> mapUserRolePermissionSet = new Map<String, String>();
            for(PermissionSet  permissionSet : [Select Id, Name From PermissionSet Where Name IN :setPermissionSetNames]){
                if(permissionSet.Name.equals(Label.OCSUGC_VMS_Manager_Permission_set)) {
                    mapUserRolePermissionSet.put(Label.OCSUGC_Varian_Employee_Community_Manager, permissionSet.Id);
                } else if(permissionSet.Name.equals(Label.OCSUGC_VMS_Contributor_Permission_set)) {
                    mapUserRolePermissionSet.put(Label.OCSUGC_Varian_Employee_Community_Contributor, permissionSet.Id);
                } else if(permissionSet.Name.equals(Label.OCSUGC_VMS_Member_Permission_set)) {
                    mapUserRolePermissionSet.put(Label.OCSUGC_Varian_Employee_Community_Member, permissionSet.Id);
                    mapUserRolePermissionSet.put(Label.OCSUGC_Varian_ReadOnly_User, permissionSet.Id);
                }
            }
            
            if(!Test.isRunningTest()) {
                removePermissionSetFromUsers(removeUserIds, mapUserRolePermissionSet.values());
                addPermissionSetToUsers(addUserIdAndUserRole, mapUserRolePermissionSet);
                // 15-05-2015   Puneet, ONCO-258    method add Community Members to group
                //addCommunityMemberstoGroup(new Map<Id, User>(lstNewUser)); Commented by Shital
                addCommunityMemberstoGroup(new Set<Id>((new Map<Id,User>(lstNewUser)).keySet()));
            }
        }
    }

    //  @description: Method to remove permission set from users. (T-334098)
    //  @param      : set of user's Id
    //  @param      : list of permission set Ids
    //  @return     : void
    //  @author     : Rishi Khirbat
    @testVisible private static void removePermissionSetFromUsers(Set<String> removeUserIds, List<String> lstPermissionSetIds) {
        delete [Select Id From PermissionSetAssignment
                Where AssigneeId IN :removeUserIds
                And PermissionSetId IN :lstPermissionSetIds];
    }

    //  @description: Method to add permission set to users. (T-334098)
    //  @param      : Map b/w user's Id and user's role
    //  @param      : Map b/w user's role and  permission set Id
    //  @return     : void
    //  @author     : Rishi Khirbat
    @future // Uncommeneted @future annotation to resolve task - ONCO 493
    @testVisible private static void addPermissionSetToUsers(Map<String, String> addUserIdAndUserRole, Map<String, String> mapUserRolePermissionSet) {
        List<PermissionSetAssignment> lstPermissionSetAssignments = new List<PermissionSetAssignment>();
        for(String userId :addUserIdAndUserRole.keySet()) {
            if(mapUserRolePermissionSet.get(addUserIdAndUserRole.get(userId)) != null) {
                PermissionSetAssignment perSetAssign = new PermissionSetAssignment();
                perSetAssign.PermissionSetId = mapUserRolePermissionSet.get(addUserIdAndUserRole.get(userId));
                perSetAssign.AssigneeId = userId;
                lstPermissionSetAssignments.add(perSetAssign);
                system.debug(' ===lstPermissionSetAssignments=== ' + lstPermissionSetAssignments);
            }
        }
        
        insert lstPermissionSetAssignments;
    }
    
    // @description:    Method add user of OCSUGC_User_Role__c "OCSUGC Community Members" to Group OCSUGC_Users
    // @author     :    Puneet Mishra 15-05-2015
    // updated     :    Puneet Mishra 26-05-2015, Replacing Hardcoded values with Custom Labels
     /*@TestVisible
    private static void addCommunityMemberstoGroup(Map<Id, User> newUserMap) {
        String networkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_OncoPeer);
        // To Fetch Developer Cloud NetworkId
        //String DCNetworkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_NetworkName);
        set<Id> grpMemberSet = new set<Id>();
        set<String> usrPublicGrp = new set<String>();
        for(GroupMember grpMember : [SELECT GroupId, UserOrGroupId FROM GroupMember 
                                         Where UserOrGroupId IN: newUserMap.keySet() 
                                         AND Group.DeveloperName =: Label.OCSUGC_Users_Group]){
                                            grpMemberSet.add(grpMember.UserOrGroupId);
                                         }
        for(Id usrId:newUserMap.keySet()){
            if( !grpMemberSet.contains(usrId) 
                &&newUserMap.get(usrId).OCSUGC_User_Role__c == Label.OCSUGC_Varian_Employee_Community_Member )//&& newUserMap.get(usrId).Contact.OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status)
                {
                    usrPublicGrp.add(''+usrId);
            }
        }
        if(!usrPublicGrp.isEmpty()){
            OCSUGC_Utilities.addUserToOCSUGCUsersPublicGroup(usrPublicGrp);
        }
        OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(newUserMap.values(), networkId);
        // Disabling Chatter Emails for Developer Cloud
        //OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(newUserMap.values(), DCNetworkId);
    } */
    //Changes added by Shital for task ONCO - 493
     @future 
     public static void addCommunityMemberstoGroup(Set<Id> newUserSet) {
        String networkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_OncoPeer);
        // To Fetch Developer Cloud NetworkId
        //String DCNetworkId = OCSUGC_Utilities.getNetworkId(Label.OCSDC_NetworkName);
        set<Id> grpMemberSet = new set<Id>();
        set<String> usrPublicGrp = new set<String>();
        Map<Id,User> userMap = new Map<Id,User>();
        
        //List<User> userList = [select id,name,OCSUGC_User_Role__c from User where Id IN : newUserSet];
        for(User usr: [select id,name,OCSUGC_User_Role__c from User where Id IN : newUserSet]){
        	userMap.put(usr.Id,usr);
        }
         
        for(GroupMember grpMember : [SELECT GroupId, UserOrGroupId FROM GroupMember 
                                         Where UserOrGroupId IN: newUserSet 
                                         AND Group.DeveloperName =: Label.OCSUGC_Users_Group]){
                                            grpMemberSet.add(grpMember.UserOrGroupId);
                                         }
        for(Id usrId:newUserSet){
            if( !grpMemberSet.contains(usrId)
                &&userMap.get(usrId).OCSUGC_User_Role__c == Label.OCSUGC_Varian_Employee_Community_Member )//&& newUserMap.get(usrId).Contact.OCSUGC_UserStatus__c == Label.OCSUGC_Approved_Status)
                {
                    usrPublicGrp.add(''+usrId);
            }
        }
        if(!usrPublicGrp.isEmpty()){
            OCSUGC_Utilities.addUserToOCSUGCUsersPublicGroup(usrPublicGrp);
        }
        OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(userMap.values(), networkId);
        // Disabling Chatter Emails for Developer Cloud
        //OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(newUserMap.values(), DCNetworkId);
    }

}
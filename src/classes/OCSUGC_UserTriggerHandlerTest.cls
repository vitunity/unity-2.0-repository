@isTest
public with sharing class OCSUGC_UserTriggerHandlerTest {
    
    public static User contributorUsr, adminusr;
    public static Contact contact1,contact2,contact3,contact4,contact5;
    public static List<Contact> lstContactInsert;
    public static Set<String> setPermissionSetNames;
    public static Profile manager,admin;
    public static Account account;
    public static List<PermissionSet>  permissionSetList;
    
    static {
    	manager= OCSUGC_TestUtility.getManagerProfile();
    	admin = OCSUGC_TestUtility.getAdminProfile();
    	
    	account = OCSUGC_TestUtility.createAccount('TestAccount', true);
    	
    	lstContactInsert = new List<Contact>();
    	lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
	    lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
	    insert lstContactInsert;
    	
    	contributorUsr = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '24',Label.OCSUGC_Varian_Employee_Community_Contributor);
    	insert contributorUsr;
    	
    	adminusr = OCSUGC_TestUtility.createPortalUser(true, admin.Id, contact2.Id, '24',''); 
    	
    	setPermissionSetNames = new Set<String>();
    	setPermissionSetNames.add(Label.OCSUGC_VMS_Manager_Permission_set);         //Varian Community Manager
     	setPermissionSetNames.add(Label.OCSUGC_VMS_Contributor_Permission_set);     //Varian Community Contributor
     	setPermissionSetNames.add(Label.OCSUGC_VMS_Member_Permission_set);          //Varian Community Member
     	
     	permissionSetList = new List<PermissionSet>([Select Id, Name From PermissionSet Where Name IN :setPermissionSetNames]);
    }
    
    public static testMethod void onBeforeInsertUpdateTest() {
    	Test.StartTest();
    		
    		User newUser = OCSUGC_TestUtility.createPortalUser(true, manager.Id, lstContactInsert[3].Id, '33',null);
    		//newUser = [Select Id, Name, contactId from User Where Id =: newUser.Id];
    		
    		OCSUGC_UserTriggerHandler.onBeforeInsertUpdate(new List<User>{newUser}, true, new Map<Id, user>{}, new Map<Id, User>{});
    		
    		OCSUGC_UserTriggerHandler.onBeforeInsertUpdate(new List<User>{newUser}, false, new Map<Id, user>{newUser.Id => newUser}, new Map<Id, User>{newUser.Id => newUser});
    		newUser.isActive = false;
    		//insert newUser;
    		
    		system.runas(adminusr) {
	    		newUser.isActive = true;
	    		update newUser;
    		}
    		
    	Test.StopTest();
    }
    
    @isTest(SeeAllData = true)
    public static  void onAfterInsertUpdateTest(){
    	Test.StartTest();
    		
    		//User newUser = [SELECT id, ContactId, OCSUGC_User_Role__c, Contact.OCSUGC_UserStatus__c FROM User WHERE
    		//				ContactId != null AND Contact.OCSUGC_UserStatus__c =: ''];
    		
    		OCSUGC_UserTriggerHandler cont = new OCSUGC_UserTriggerHandler();
    		//cont.onAfterInsertUpdate();
    	Test.Stoptest();
    }
    
    public static testMethod void removePermissionfromUserTest() {
    	Test.StartTest();
    		List<String> permissionSetIdList = new List<String>();
    		for(permissionSet per : permissionSetList)
    			permissionSetIdList.add(per.Id);
    		
    		OCSUGC_UserTriggerHandler.removePermissionSetFromUsers(new Set<String>{String.valueOf(contributorUsr.Id)}, permissionSetIdList);
    	
    	Test.StopTest();
    }
    
    /** 16 Jan 2017 Puneet **/
    @isTest static void addCommunityMemberstoGroup_test() {
    	
    	Profile prof = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
 		User newusr = new User(LastName = 'LIVESTON',FirstName='JASON',Alias = 'jliv',Email = 'jason.liveston@ocsd.com',
                       Username = 'jason.liveston@ocsd.com',ProfileId = prof.id,
                       TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                       LocaleSidKey = 'en_US', OCSUGC_User_Role__c = 'Varian Community Member');
		insert newusr;
    	
    	Set<Id> newUsrSet = new Set<Id>{newusr.Id};
    	
    	Test.StartTest();
    	
    		OCSUGC_UserTriggerHandler.addCommunityMemberstoGroup(newUsrSet);
    		
    	Test.StopTest();
    	
    }
    
    @isTest static void addCommunityMemberstoGroup2_test() {
    	User newusr;
    	Test.StartTest();
    	
	    	Profile prof = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
	        
	 		newusr = new User(LastName = 'LIVESTON',FirstName='JASON',Alias = 'jliv',Email = 'jason.liveston@ocsd.com',
	                       Username = 'jason.liveston@ocsd.com',ProfileId = prof.id,
	                       TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
	                       LocaleSidKey = 'en_US');
			insert newusr;
	    	
	    Test.StopTest();
    	
    	newusr.OCSUGC_User_Role__c = 'Varian Community Member';
    	update newusr;
    	
    }
    
    @isTest static void addPermissionSetToUsers_test() {
    	User newusr;
    	Test.StartTest();
    		Profile prof = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
	        
	 		newusr = new User(LastName = 'LIVESTON',FirstName='JASON',Alias = 'jliv',Email = 'jason.liveston@ocsd.com',
	                       Username = 'jason.liveston@ocsd.com',ProfileId = prof.id,
	                       TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
	                       LocaleSidKey = 'en_US', OCSUGC_User_Role__c = 'Varian Community Member');
			insert newusr;
    		
    		Map<String, String> addUserIdAndUserRoletest = new Map<String, String>{newusr.Id => newusr.OCSUGC_User_Role__c};
    		List<PermissionSet> perList = new List<PermissionSet>();
    		perList = [Select Id, Name From PermissionSet Where Name =: 'OCSUGC_Community_Contributor_Permissions'];
    		Map<String, String> mapUserRolePermissionSettest = new Map<String, String>{newusr.OCSUGC_User_Role__c => perList[0].Id};
    	
    		OCSUGC_UserTriggerHandler.addPermissionSetToUsers(addUserIdAndUserRoletest, mapUserRolePermissionSettest);
    	Test.Stoptest();
    }
    
}
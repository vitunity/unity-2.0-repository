//
// (c) 2014 Appirio, Inc.
//
// Test class of utilities
//
// 25 Sep 2014 Puneet Sardana Original (Ref. T-321628)
@isTest(SeeAllData=true)
public with sharing class OCSUGC_UtilitiesTest {

    Static Boolean isVarianCommunityManager ;
    Static Boolean isVarianCommunityContributor;
    Static Boolean isVarianCommunityMember;
    Static Boolean isVarianReadOnlyUser;
    Static Boolean isManagerOrContributor;
    Static Boolean isManagerOrContributorOrReadonly;
    Static Boolean isManagerOrReadonly;
    Static Boolean isMemberisManagerisContributor,isManagerOrModerator,isManagerOrContributorOrModerator;
    static Network ocsugcNetwork;
    Static Map<String, String> userRoleMap;
    Static Boolean isDCContributor,isUserDisplay,canFollowUnfollow,isUserCanLikeUnlike,canCreateComments;
    Static Map<String, String> fatchAllUserRolesMap;
    Static String fatchLoginUserRole;
    static Id orgWideId;
    static string networkId;
    public static set<String> fetchAccessibleGroupIds,fetchGroupIds,fetchFGABGroupIds;
    public static list<User> lstUserInsert;
    static CollaborationGroup cGroup,cGroup2;
    public static list<CollaborationGroup> groups;
    public static ContentWorkspace objContentWorkspace;
    public static Blacklisted_Word__c testB;
    static User communityMember,communityManager,memberUsr;
    static Profile admin,portal,manager;
    static Account account;
    static Contact contact1,contact2,contact3;
    static List<Contact> lstContactInsert;
    static Contact con3;
    static {
        admin = OCSUGC_TestUtility.getAdminProfile();
        portal = OCSUGC_TestUtility.getPortalProfile();
        manager = OCSUGC_TestUtility.getManagerProfile();
        
        Account accnt = OCSUGC_TestUtility.createAccount('Acctemp798435',true);//[Select Id, Name from Account LIMIT 1];//OCSUGC_TestUtility.createAccount('Acctemp798435',true);
        con3 = OCSUGC_TestUtility.createContact(accnt.Id,true);
        system.debug(' ===1=== ' + Limits.getQueries());
        account = OCSUGC_TestUtility.createAccount('test account', true);
        system.debug(' ===2=== ' + Limits.getQueries());
        system.debug(' ====== ' + Limits.getQueries());
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
        lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
        insert lstContactInsert;
        
        system.debug(' ====== ' + Limits.getQueries());
        
        ocsugcNetwork = OCSUGC_TestUtility.getOCSUGCNetwork();
        
        /*lstUserInsert = new list<User>();
        User adminUsr1 = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'12',Label.OCSUGC_Varian_Employee_Community_Manager);
        User usr =  OCSUGC_TestUtility.createPortalUser(false, portal.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
        lstUserInsert.add(adminUsr1);
        lstUserInsert.add(usr);
        communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
        lstUserInsert.add(communityManager);
        communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
        lstUserInsert.add(communityMember);
        
        if(lstUserInsert.size() > 0 ){
            insert lstUserInsert;
        } */
        groups = new list<CollaborationGroup>();
        cGroup = OCSUGC_TestUtility.createGroup('Test Group','Unlisted',ocsugcNetwork.id , false);
        cGroup2 = OCSUGC_TestUtility.createGroup('Test Group1','Unlisted',Label.OCSUGC_FocusGroupAdvisoryBoard,ocsugcNetwork.id , false);
        groups.add(cGroup);
        groups.add(cGroup2);
        
        /*
        if(groups.size() > 0 ){
            insert groups;
            CollaborationGroupMember grpMember = OCSUGC_TestUtility.createGroupMember(communityManager.Id,cGroup.Id,false);
            insert grpMember;
        }
        
        testB = new Blacklisted_Word__c();
        testB.Word__c = 'TestB';
        insert testB;
        */
    
          
    }
    // @description: Test relative time of utility class
    // @return: NA  
    @isTest
    static void testRelativeTimeString()
    {
        
        Test.StartTest();
        
        lstUserInsert = new list<User>();
        User adminUsr1 = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'12',Label.OCSUGC_Varian_Employee_Community_Manager);
        User usr =  OCSUGC_TestUtility.createPortalUser(false, portal.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
        lstUserInsert.add(adminUsr1);
        lstUserInsert.add(usr);
        communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
        lstUserInsert.add(communityManager);
        communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
        lstUserInsert.add(communityMember);
        if(lstUserInsert.size() > 0 ){
            insert lstUserInsert;
        }
        
        if(groups.size() > 0 ){
            insert groups;
            CollaborationGroupMember grpMember = OCSUGC_TestUtility.createGroupMember(communityManager.Id,cGroup.Id,false);
            insert grpMember;
        }
        
        testB = new Blacklisted_Word__c();
        testB.Word__c = 'TestB';
        insert testB;
        
        OCSUGC_VisibleInviteUserButton__c visibility = new OCSUGC_VisibleInviteUserButton__c();
        visibility.Name='Varian Community Manager';
        visibility.OCSUGC_VisibleInviteUserButton__c = true;
        //insert visibility;
        DateTime d1 = DateTime.now();
        DateTime d2 = DateTime.now();
        System.assertEquals(OCSUGC_Utilities.GetRelativeTimeString(d1,d2),'Now');
        d2 = Datetime.now().addSeconds(30);
        System.assert(OCSUGC_Utilities.GetRelativeTimeString(d1,d2).Contains('seconds'));
        d2 = DateTime.now().addMinutes(10);
        System.assert(OCSUGC_Utilities.GetRelativeTimeString(d1,d2).Contains('minutes'));
        d2 = DateTime.now().addHours(10);
        System.assert(OCSUGC_Utilities.GetRelativeTimeString(d1,d2).Contains('hours'));
        d2 = DateTime.now().addHours(250);
        System.assert(OCSUGC_Utilities.GetRelativeTimeString(d1,d2).Contains('days'));
        d2 = DateTime.now().addDays(1250);
        System.assert(OCSUGC_Utilities.GetRelativeTimeString(d1,d2).Contains('years'));
        
        
        OCSUGC_Utilities.removeSpecialChar('abcd @ ndd');
        OCSUGC_Utilities.refineErrorMessage('FIELD_CUSTOM_VALIDATION_EXCEPTION test');
        OCSUGC_Utilities.splitLegalName('test legal name');
        OCSUGC_Utilities.isDCMember(userInfo.getUserId(), 'Varian Community Member', 'Member');
        
        isVarianCommunityManager = OCSUGC_Utilities.isManager('Varian Community Manager');
        system.assertEquals(isVarianCommunityManager, true);

        isVarianCommunityContributor = OCSUGC_Utilities.isContributor('Varian Community Contributor');
        system.assertEquals(isVarianCommunityContributor, true);

        isVarianCommunityMember = OCSUGC_Utilities.isMember('Varian Community Member');
        system.assertEquals(isVarianCommunityMember, true);

        isVarianReadOnlyUser = OCSUGC_Utilities.isReadonly('Varian Read Only User');
        system.assertEquals(isVarianReadOnlyUser, true);

        isManagerOrContributor = OCSUGC_Utilities.isManagerOrContributor('Varian Community Manager');
        system.assertEquals(isManagerOrContributor, true);

        isManagerOrContributorOrReadonly = OCSUGC_Utilities.isManagerOrContributorOrReadonly('Varian Read Only User');
        system.assertEquals(isManagerOrContributorOrReadonly, true);

        isManagerOrReadonly = OCSUGC_Utilities.isManagerOrReadonly('Varian Community Manager');
        system.assertEquals(isManagerOrReadonly, true);
        
        //Added by SHital start
        orgWideId = OCSUGC_Utilities.getOrgWideEmailAddressId();
        
        //isMemberisManagerisContributor = OCSUGC_Utilities.isMemberisManagerisContributor('Varian Community Manager');
        isManagerOrModerator = OCSUGC_Utilities.isManagerOrModerator('Varian Community Manager');
        
        networkId = OCSUGC_Utilities.getCommunityNetworkId(Label.OCSUGC_NetworkName); 

        isDCContributor = OCSUGC_Utilities.isDCContributor(userInfo.getUserId());
    
        isUserDisplay = OCSUGC_Utilities.isUserDisplay('Varian Community Manager','Varian Community Member');
        canFollowUnfollow = OCSUGC_Utilities.canFollowUnfollow('Varian Community Manager','Varian Community Member');
        isUserCanLikeUnlike = OCSUGC_Utilities.isUserCanLikeUnlike('Varian Community Manager',true,true);
        canCreateComments = OCSUGC_Utilities.canCreateComments('Varian Community Manager');
        
        fatchLoginUserRole = OCSUGC_Utilities.fatchLoginUserRole();
        fetchAccessibleGroupIds = new Set<String>();
        fetchAccessibleGroupIds = OCSUGC_Utilities.fetchAccessibleGroupIds(networkId,lstUserInsert[0].Id,true);
        fetchGroupIds = OCSUGC_Utilities.fetchGroupIds(networkId,lstUserInsert[2].Id,false,cGroup.Id,true);
        OCSUGC_Utilities.fetchGroupIds(networkId,lstUserInsert[2].Id,true,cGroup2.Id,true);
        fetchFGABGroupIds = OCSUGC_Utilities.fetchFGABGroupIds(true);
        //OCSUGC_Utilities.contentLikeUnLike();
        
        objContentWorkspace = new ContentWorkspace();
        objContentWorkspace = OCSUGC_Utilities.objContentWorkspace(Label.OCSUGC_NetworkName);
        
        OCSUGC_Utilities.logoutMethod();
        list<String> contentList= new List<String>{'Test','testB'};
        OCSUGC_Utilities.verifyContentForBlockedWords(contentList);
        
        OCSUGC_Utilities.isArchivedGroup(cGroup.Id);
        OCSUGC_Utilities.isArchivedGroup('Everyone');
        OCSUGC_Utilities.archivedGroupIds(networkId);
        OCSUGC_Utilities.getNetworkId(Label.OCSUGC_NetworkName);
        
        OCSUGC_Utilities.isCommunityManager(networkId,userInfo.getUserId(),'Varian Community Manager');
        OCSUGC_Utilities.isCommunityContributor(networkId,userInfo.getUserId(),'Varian Community Manager');
        OCSUGC_Utilities.isCommunityModerator(networkId,userInfo.getUserId(),'Varian Community Manager');
        OCSUGC_Utilities.validateStringLength('abcdefghr',20);
        
        Account abc = new Account();
        //OCSUGC_Utilities.getRecordTypeId('Account','Site_Partner');
        OCSUGC_Utilities.prepareIntranetNotificationRecords(Label.OCSUGC_NetworkName,lstUserInsert[0].Id,'Test Notification','test','/ocsugc_fgababoutgroup');
        OCSUGC_Utilities.convertTextURLToHyperlink('https://google.com');
        OCSUGC_Utilities.fetchReadOnlyUserFGABGroupIds(networkId,lstUserInsert[0].Id,true);
        OCSUGC_Utilities.disableStandardChatterEmailsForNetworkId(lstUserInsert,networkId);
        
        set<String> userIds = new set<String>();
        userIds.add(lstUserInsert[0].Id);
        system.runas(lstUserInsert[1]){
            OCSUGC_Utilities.addUserToOCSUGCUsersPublicGroup(userIds);
        }
        OCSUGC_Utilities.validNetworkIds();
        OCSUGC_Utilities.getPickValues(abc,'Type','Analyst','Other');
        OCSUGC_Utilities.insertNotificationRecordForFollowers(Label.OCSUGC_NetworkName,'Test Notification','test','test','/ocsugc_fgababoutgroup');
        
        
        OCSUGC_Utilities.followUser(networkId,lstUserInsert[2].Id,lstUserInsert[3].Id,'test');
        OCSUGC_Utilities.unfollowUser(networkId,lstUserInsert[2].Id,lstUserInsert[3].Id,'test');
        OCSUGC_Utilities.createAlertNotification('Test123','/ocsugc_fgababoutgroup',lstUserInsert[0].Id,lstUserInsert[1].Id);
        OCSUGC_Utilities.deleteDiscussionIds();
        OCSUGC_Utilities.deleteTemporaryEventRecords();
        OCSUGC_Utilities.deleteTemporaryDiscussionRecords();
        
        OCSUGC_Intranet_Content__c testContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Applications',
                                                         null,'Team','Event');
       insert testContent;
       
       
       OCSUGC_Utilities.contentLikeUnLike(testContent.Id,'Event',true);
       OCSUGC_Utilities.getCurrentCommunityNetworkId(false);
       //OCSUGC_Utilities.isDevCloud(true);
       OCSUGC_Utilities.getCurrentCommunityNetworkName(false);
       OCSUGC_Utilities.isDCContributor(lstUserInsert[3].Id,'Varian Community Contributor');

       OCSUGC_Utilities.fetchOCSUGCDisqualifiedFunctionalRoles();
       
       Test.StopTest();
    }
    
    @isTest static void isContributor_OncoPeerTest() {
        
        Test.StartTest();
            
            lstUserInsert = new list<User>();
            User adminUsr1 = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'12',Label.OCSUGC_Varian_Employee_Community_Manager);
            User usr =  OCSUGC_TestUtility.createPortalUser(false, portal.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
            lstUserInsert.add(adminUsr1);
            lstUserInsert.add(usr);
            communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
            lstUserInsert.add(communityManager);
            communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
            lstUserInsert.add(communityMember);
            if(lstUserInsert.size() > 0 ){
                insert lstUserInsert;
            }
            
            OCSUGC_Utilities.isContributor_OncoPeer('Varian Community Contributor', true, true);
            OCSUGC_Utilities.isContributor_OncoPeer('Varian Community Contributor',false, false);
        Test.StopTest();
    }
    
    @isTest static void removePermissionSetFromUsers_test() {
        Test.StartTest();
            
            User internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1');
            insert internalUsr;
            
            Set<Id> insertUsrId = new Set<Id>{internalUsr.Id};
            
            List<PermissionSet> managerPermissionSet = [SELECT Id, Name FROM PermissionSet WHERE Name LIKE '%VMS_OCSDC_Community_Members_Permissions%' LIMIT 1];
            Set<String> permissionName = new Set<String>{managerPermissionSet[0].Name};
            
            OCSUGC_Utilities.removePermissionSetFromUsers(insertUsrId, permissionName);
        Test.StopTest();
    }
    
    @isTest static void removeUserFromGroupTest() {
        Test.startTest();
            
            communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
            insert communityManager;
            
            if(groups.size() > 0 ){
                insert groups;
                CollaborationGroupMember grpMember = OCSUGC_TestUtility.createGroupMember(communityManager.Id,cGroup.Id,false);
                insert grpMember;
            }
            
            testB = new Blacklisted_Word__c();
            testB.Word__c = 'TestB';
            insert testB;
            
            User internalUsr = OCSUGC_TestUtility.createStandardUser(false, admin.Id,'test', '1');
            insert internalUsr;
            
            Set<Id> insertUsrId = new Set<Id>{internalUsr.Id};
            
            Set<String> groupNames = new Set<String>{cGroup.Name};
            OCSUGC_Utilities.removeUserFromGroup(insertUsrId, groupNames);
        Test.StopTest();
    }
    
    @isTest static void addUserGroupInOktaAfterDCApproved_test() {
            
       Test.startTest();
        
        lstUserInsert = new list<User>();
            User adminUsr1 = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'12',Label.OCSUGC_Varian_Employee_Community_Manager);
            User usr =  OCSUGC_TestUtility.createPortalUser(false, portal.Id, con3.Id, '525',Label.OCSUGC_Varian_Employee_Community_Member);
            lstUserInsert.add(adminUsr1);
            lstUserInsert.add(usr);
            communityManager = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact1.Id, '3',Label.OCSUGC_Varian_Employee_Community_Manager);
            lstUserInsert.add(communityManager);
            communityMember = OCSUGC_TestUtility.createPortalUser(false, manager.Id, contact2.Id, '4',Label.OCSUGC_Varian_Employee_Community_Contributor);
            lstUserInsert.add(communityMember);
            if(lstUserInsert.size() > 0 ){
                insert lstUserInsert;
            }
            
            OCSUGC_Utilities.isModerator('Varian Community Moderator');
            OCSUGC_Utilities.isManagerOrContributorOrModerator('Varian Community Contributor');
            OCSUGC_Utilities.isManagerOrContributorOrReadonlyOrModerator('Varian Community Contributor');
            OCSUGC_Utilities.isDevCloud('true');
            //OCSUGC_Utilities.IsAdmin;
            OCSUGC_Utilities.deletePollIds();
            OCSUGC_Utilities.addUserGroupInOktaAfterDCApproved('','GET');
        Test.stopTest();
    }
    
    @isTest static void fatchUserRole_manager_Test() {
        Test.startTest();
            User mgerUsr = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'20',Label.OCSUGC_Varian_Employee_Community_Manager);
            insert mgerUsr;
            set<String> mgerUsrl = new Set<String>{mgerUsr.Id};
            OCSUGC_Utilities.fatchUserRole(mgerUsrl);
        Test.stopTest();
    }
    
    @isTest static void fatchUserRole_readOnly_Test() {
        Test.startTest();
            User readOnly = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'20',Label.OCSUGC_Varian_ReadOnly_User);
            insert readOnly;
            set<String> readOnlyl = new Set<String>{readOnly.Id};
            OCSUGC_Utilities.fatchUserRole(readOnlyl);
        Test.stopTest();
    }
    
    @isTest static void fatchUserRole_contri_Test() {
        Test.startTest();
            User contri = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'20',Label.OCSUGC_Varian_Employee_Community_Contributor);
            insert contri;
            set<String> contril = new Set<String>{contri.Id};
            OCSUGC_Utilities.fatchUserRole(contril);
        Test.stopTest();
    }
    
    @isTest static void fatchUserRole_Mod_Test() {
        Test.startTest();
            User Mod = OCSUGC_TestUtility.createStandardUser(false,admin.Id,'20',Label.OCSUGC_Varian_Employee_Community_Moderator);
            insert Mod;
            set<String> Modl = new Set<String>{Mod.Id};
            OCSUGC_Utilities.fatchUserRole(Modl);
        Test.stopTest();
    }
    
    @isTest static void fetchGroupIdsTest() {
        Test.startTest();
            Network ocsugcNet = OCSUGC_TestUtility.getOCSUGCNetwork();
            CollaborationGroup CollocGroup = OCSUGC_TestUtility.createGroup('Test Group','Unlisted',ocsugcNetwork.id , true);
            
            OCSUGC_CollaborationGroupInfo__c info = new OCSUGC_CollaborationGroupInfo__c();
            info.OCSUGC_Group_Id__c = CollocGroup.Id;
            info.OCSUGC_Email__c = 'psardana@appirio.com';
            info.OCSUGC_Group_TermsAndConditions__c = 'test terms';
            insert info;
            
            OCSUGC_Utilities.fetchGroupIds(ocsugcNet.Id, userInfo.getUserId(), false, null, true);
        Test.stopTest();
    }
    
    @isTest static void contentLikeUnLikeTest() {
        Test.startTest();
            OCSUGC_Intranet_Content__c discussionContent = OCSUGC_TestUtility.createIntranetContent('testContent',
                                                         'Applications',
                                                         null,'Team','Discussion');
        insert discussionContent;
        
        OCSUGC_Utilities.contentLikeUnLike(discussionContent.Id, 'Discussion', true);
        OCSUGC_Utilities.contentLikeUnLike(discussionContent.Id, 'Knowledge', true);
        Test.stopTest();
    }
    //
}
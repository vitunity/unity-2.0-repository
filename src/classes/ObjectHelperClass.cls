/**
  * @author Jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    This class is utilized for easily updating any object's 
  *    field and committing the change to the database if desired.
  *    Method is overloaded for different field value types.
  *
  *    Helpful hints for the next developer.
  *    Business logic or use cases can be used.
  * 
  * TEST CLASS 
  * 
  *    Name of the test class
  *    For Naming use the class ObjectHelperClass_TEST
  *  
  * ENTRY POINTS 
  *  
  *    How is this code called, if a VF controller which page calls it.  
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; 6/16/2015;  Jwagner/Forefront; Initial Build
  * 
  **/
public with sharing class ObjectHelperClass {
  public static Sobject updateObject(Sobject obj, String fieldname, String value, Boolean doDML){
    obj.put(fieldname, value);
    
    if(doDML)
    {
      update obj;
      return null;
    }
    else
    { 
      return obj;
    }
  } 
  public static Sobject updateObject(Sobject obj, String fieldname, Decimal value, Boolean doDML){
    obj.put(fieldname, value);
    
    if(doDML)
    {
      update obj;
      return null;
    }
    else
    { 
      return obj;
    }
  } 
  public static Sobject updateObject(Sobject obj, String fieldname, Integer value, Boolean doDML){
    obj.put(fieldname, value);
    
    if(doDML)
    {
      update obj;
      return null;
    }
    else
    { 
      return obj;
    }
  }
  public static Sobject updateObject(Sobject obj, String fieldname, Boolean value, Boolean doDML){
    obj.put(fieldname, value);
    
    if(doDML)
    {
      update obj;
      return null;
    }
    else
    { 
      return obj;
    }
  }

  public static Boolean isNotBlank(String filter){
    return filter != null && filter.deleteWhitespace() != '' ? true : false;
  }
}
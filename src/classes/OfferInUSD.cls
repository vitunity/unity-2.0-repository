public class OfferInUSD{
    public static void setOffer(map<Id,BigMachines__Quote__c> mapNew, map<Id,BigMachines__Quote__c> mapOld){
        
        map<string,decimal> mapCT = new map<string,decimal>();
        for(CurrencyType ct: [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE]){
            mapCT.put(ct.ISOCode,ct.ConversionRate);
        }
        for(BigMachines__Quote__c bm: mapNew.values()){
             if(bm.BigMachines__Total_Amount__c <> null && bm.BigMachines__Total_Amount__c <> 0 && mapCT.containsKey(bm.CurrencyISOCode) &&  mapCT.get(bm.CurrencyISOCode) <> 0 ){   
            if(mapCT.containsKey(bm.CurrencyISOCode)){
                        bm.Offer_in_USD__c = bm.BigMachines__Total_Amount__c / mapCT.get(bm.CurrencyISOCode);
                        bm.Booking_Value_in_USD__c = bm.Booking_Value__c / mapCT.get(bm.CurrencyISOCode);
                        bm.Net_Booking_Value_in_USD__c = bm.Net_Booking_Value__c / mapCT.get(bm.CurrencyISOCode);
                    }
                }else{
                    bm.Offer_in_USD__c = 0;
                    bm.Booking_Value_in_USD__c = 0; 
                    bm.Net_Booking_Value_in_USD__c = 0;
                }     
        }
       
    }
}
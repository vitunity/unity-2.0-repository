@isTest
private class OfferInUSD_Test {

    static testMethod void UnitTest() {
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = '121212';
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opp.Id;
        quote.National_Distributor__c = '121212';
        insert quote;       
        
        quote.National_Distributor__c = '212121';
        update quote;
    }
}
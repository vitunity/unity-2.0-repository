global class Oktauserupdate{
 
    @future(callout=true)
    Public static void useractivationmethodforokta(Set<String> useremailslist){
    
    List<User> con = new list<User>([select id,contactid,contact.email from User where contact.email in: useremailslist and contact.oktaid__c = null and isActive = true Limit 5]);
 List<Contact> contactlist = new list<Contact>();
        for (User s : con)
           { 
              User u = s;
              String oktaidgetuser;
              HttpRequest reqgetuser = new HttpRequest();
              HttpResponse resgetuser = new HttpResponse();
              Http httpgetuser = new Http();
              String strToken = label.oktatoken;
              String authorizationHeader = 'SSWS ' + strToken;
              reqgetuser.setHeader('Authorization', authorizationHeader);
              reqgetuser.setHeader('Content-Type','application/json');
              reqgetuser.setHeader('Accept','application/json');
              String endpointgetuser = label.oktachangepasswrdendpoint + u.contact.email; //'00uktjiftnOJXGTAFSHO/groups';
              reqgetuser.setMethod('GET');
              reqgetuser.setEndpoint(endpointgetuser);
              try{
                if(!Test.Isrunningtest()){
                resgetuser = httpgetuser.send(reqgetuser);
                }
                JSONParser parser = JSON.createParser(resgetuser.getBody());
               while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'id') {
                        oktaidgetuser = parser.getText();                          
                    }
                    }
                    }
                }catch(System.CalloutException e) {
                   System.debug(resgetuser.toString());
                   }
                   
              // generating password
                Integer len = 10;
                Blob blobKey = crypto.generateAesKey(128);
                String key = EncodingUtil.convertToHex(blobKey);
                System.debug(key);
                String pwd = key.substring(0,5);
                String Fpwd;
                Fpwd = 'A' + pwd + '3z';
                
              HttpRequest reqgetuser1 = new HttpRequest();
              HttpResponse resgetuser1 = new HttpResponse();
              Http httpgetuser1 = new Http();
              //String strToken = label.oktatoken;
              //String authorizationHeader = 'SSWS ' + strToken;
              reqgetuser1.setHeader('Authorization', authorizationHeader);
              reqgetuser1.setHeader('Content-Type','application/json');
              reqgetuser1.setHeader('Accept','application/json');
              String endpointgetuser1 = 'https://varian.okta.com/api/v1/users/'+oktaidgetuser+'/credentials/forgot_password';
              String body = '{"password": { "value": "' + Fpwd + '"},"recovery_question": { "answer": "varian123" }}';
              reqgetuser1.setMethod('POST');
              reqgetuser1.setEndpoint(endpointgetuser1);
              reqgetuser1.setbody(body);
              try{
                if(!Test.Isrunningtest()){
                resgetuser1 = httpgetuser1.send(reqgetuser1);
                }
                }catch(System.CalloutException e) {
                   System.debug(resgetuser.toString());
                   }
             contact c = new contact(id= u.contactid);
             system.debug('#########' + c.id);
             c.activation_url__c = Fpwd;
             c.oktaId__c = oktaidgetuser;
             contactlist.add(c);
          }        
         Database.update(contactlist,false); 
         
    }
    }
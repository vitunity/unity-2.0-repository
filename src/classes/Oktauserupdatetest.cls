/*************************************************************************\
    @ Author        : Harshita
    @ Date      : 17-June-2013
    @ Description   :  Test class used for code coverage of CpUserActivationclass.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@IsTest(seealldata = true)

Class Oktauserupdatetest
{
    
    // Test Method for initiating cpLungPhantom class
    
    private static testmethod void Oktauserupdatetestmethod()
    {
      Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
      List<Account> objACClist = new List<Account>();
      List<Contact> conlist = new List<Contact>();
       Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC);
        Account objACC1=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC1);
        
        test.startTest();
        insert objACClist;
        Contact con5 = new Contact(FirstName='Krupali',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC1.id, phone='23444455', email = 'test_user@gmail.com' );
        conlist.add(con5);
          //insert conlist;     
        
      Contact con = new Contact(FirstName='Moksha',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com');
       conlist.add(con); 
      // Contact con4 = new Contact(FirstName='Ritu',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com');
     //      conlist.add(con4); 
           insert conlist;
       CpOktaUserActivation__c coo = new CpOktaUserActivation__c (email__c = con.email);
       insert coo;
     
      
      test.stopTest();
    }       
    private static testmethod void Oktauserupdatetestmethod1(){
   	
   	Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
   	Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
    insert 	objACC;
     Contact con = new Contact(FirstName='Moksha',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user11@gmail.com');
    insert con;	
    	test.startTest();
    	Set<id> userid = new set<Id>();
      userid.add(con.id);
      set<string> useremail=new set<string>();
      User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,isActive = true, username='standar_duser@test.com'/*,UserRoleid=r.id*/); 
        insert u; // Inserting Portal User
     //CpUserActivationClass.useractivationmethod(userid);
           List<Contact> contoupdate1 = new list<contact>();
         Contact c = new contact(id = con.id);
        c.oktaid__c = null;
        
        contoupdate1.add(c);
        update contoupdate1;  
  //   User u4 = new User(alias = 'standt2', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con4.id, username='sta_ndarduser@test.com'/*,UserRoleid=r.id*/); 
 //       insert u4; 
      User u1 = new user(id = u.id);
      u1.email = 'test.user@company.com';
      //update u1;
      Contact con4 = new Contact(FirstName='Ritu',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com');
           insert con4;
      user sysuser = [select id from user where profile.name = 'System Administrator' and isactive = true Limit 1];
       userid.add(con4.id);
       useremail.add(con.email);
      Oktauserupdate.useractivationmethodforokta(useremail);
      userid.clear();
      userid.add(u.id);
     // Oktauserupdate.userdeactivationmethod(userid);
      system.debug('######' + con4.oktaId__c);
      
     
        
      system.runas(sysuser){
      List<Contact> contoupdate = new list<contact>();
      contact u3 = new contact(id = con.id);
      u3.email = 'test.user@comany.com';
      update u3;
     /* contact u5 = new contact(id = con4.id);
      u5.email = 'test.user@comany.com';
      contoupdate.add(u5);*/
      //update u5;
      
      
        
      user u2 = new user(id = u.id);
      u2.isActive = false;
      update u2;
  
      }
      test.stopTest();
    }
    }
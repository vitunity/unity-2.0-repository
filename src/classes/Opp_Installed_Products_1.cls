public class Opp_Installed_Products_1 {
      Opportunity_Installed_Products__c OppInsProData{get;set;}
      Private Id AccId;
      public boolean renderProductList {get;set;}
      public Opp_Installed_Products_1(ApexPages.StandardController controller) {
      this.OppInsProData = (Opportunity_Installed_Products__c)controller.getRecord();
      renderProductList = True; 
      }
 //  Our collection of the class/wrapper objects productwrapper
       public List<productwrapper> ProductList {get; set;}
       List<SVMXC__Installed_Product__c> selectedProducts = new List<SVMXC__Installed_Product__c>();
       public List<SVMXC__Installed_Product__c> cd =new List<SVMXC__Installed_Product__c>();
      //This method uses a simple SOQL query to return a List of Product from Installed Base
        public List<productwrapper> getProducts() {
          /* if(ProductList == null) {
               ProductList = new List<productwrapper>();
             if(OppInsProData.Opportunity__r.AccountId !=null){
                AccId = OppInsProData.Opportunity__r.AccountId;
                system.debug('######################'+OppInsProData.Opportunity__r.AccountId);
                cd = [Select SVMXC__Product__c,SVMXC__Company__c,SVMXC__Street__c,SVMXC__State__c,SVMXC__City__c,SVMXC__Country__c,SVMXC__Zip__c,SVMXC__Product__r.ProductCode,SVMXC__Product__r.Family,SVMXC__Parent__c from SVMXC__Installed_Product__c WHERE SVMXC__Company__c = : AccId   LIMIT 10 ];
                for(SVMXC__Installed_Product__c c: cd){
                  As each Installed Base record is processed we create a new productwrapper object and add it to the ProductList
                   ProductList.add(new productwrapper(c));
                }
             }
           } */
           return ProductList;
       }
      
       public void setProductList(List<productwrapper> setProd) {
           Integer Count=0;
           for(productwrapper Temp:setProd)
           {
               ProductList[Count].selected=Temp.selected;
               Count++;
           }
       }
       public Void fetchRecords(){              
               ProductList = new List<productwrapper>();
               AccId = [Select Id,AccountId from Opportunity where Id = : OppInsProData.Opportunity__c limit 100].AccountId;
              
               List<Account_Associations__c> AccIDs=new List<Account_Associations__c>();
               //AccIDs=[Select Account__c from Account_Associations__c where Sold_To__c=:AccID limit 100];
               AccIDs=[Select Account__c from Account_Associations__c where Master_Account__c=:AccID limit 100];//priti, 4/22/2014 - commented above line and added this line - replaced Sold_To__c with Master_Account__c 
               AccIDs.add(new Account_Associations__c(Account__c=AccID));
              
               List<ID> Acc2Scan=new List<ID>();
               for(Account_Associations__c Temp: AccIDs)
               {
                   Acc2Scan.add(Temp.Account__c);
               }
              
                 system.debug('######################'+ AccId);
               if(AccId !=null){
                system.debug('######################'+ AccId);
                cd = [Select SVMXC__Product__c,SVMXC__Company__c,SVMXC__Product__r.ProductCode,SVMXC__Product__r.Family,SVMXC__Parent__c,SVMXC__Service_Contract_Start_Date__c,SVMXC__Service_Contract_End_Date__c,SVMXC__City__c,SVMXC__Country__c,SVMXC__State__c,SVMXC__Street__c,SVMXC__Zip__c,id from SVMXC__Installed_Product__c WHERE SVMXC__Company__c IN : Acc2Scan AND SVMXC__Parent__c='' limit 100];
                for(SVMXC__Installed_Product__c c: cd){
           //       As each Installed Base record is processed we create a new productwrapper object and add it to the ProductList
                   ProductList.add(new productwrapper(c));
                 }
                } 
                  system.debug('@@@@@@@@@@@@@@@@@@@@@######'+ ProductList);
           if(ProductList != NULL ){
         renderProductList = true; 
           } 
           else{
         renderProductList = False; 
           }
        }
        public String para{
         get;
        
         set{
            para=value;
       
         }
     }
     
       List<Opportunity_Installed_Products__c> OppIPtoInsert;
       
       List<SVMXC__Installed_Product__c> ProductListInclChild;
       
       public PageReference addproduct()
       {
           ProductListInclChild= new List<SVMXC__Installed_Product__c>();
           OppIPtoInsert=new List<Opportunity_Installed_Products__c>();
           for(Integer I=0;I<ProductList.size();I++)
           {
               If(ProductList[I].selected==True)
               {
                   ProductListInclChild.add(ProductList[I].con);
                   //OppIPtoInsert.add(new Opportunity_Installed_Products__c(CurrencyIsoCode='USD',Opportunity__c=OppInsProData.Opportunity__c, Installed_Product__c=ProductList[I].con.ID, Start_Date__c=OppInsProData.Start_Date__c, End_Date__c=OppInsProData.End_Date__c));
               }
           }
           //Insert OppIPtoInsert;
           
           for(SVMXC__Installed_Product__c Temp: [Select ID from SVMXC__Installed_Product__c where SVMXC__Parent__c IN : ProductListInclChild])
           {
               ProductListInclChild.add(Temp);
           }
           
           for(SVMXC__Installed_Product__c Temp:ProductListInclChild)
           {
               OppIPtoInsert.add(new Opportunity_Installed_Products__c(CurrencyIsoCode='USD',Opportunity__c=OppInsProData.Opportunity__c, Installed_Product__c=Temp.ID, Start_Date__c=OppInsProData.Start_Date__c, End_Date__c=OppInsProData.End_Date__c));
           }
           Insert OppIPtoInsert;
           
           
           PageReference pageRef = new PageReference('/'+OppInsProData.Opportunity__c);
           return pageRef;
       }
      
   
          public PageReference processSelected() {
   
             //     We create a new list of Products that we be populated only with Products if they are selected
           List<SVMXC__Installed_Product__c> selectedProducts = new List<SVMXC__Installed_Product__c>();
   
        //  We will cycle through our list of productwrappers and will check to see if the selected property is set to true, if it is we add the Product to the selectedProducts list
           for(productwrapper cCon: getProducts()) {
               if(cCon.selected == true) {
                   selectedProducts.add(cCon.con);
              }
           }
   
   
        //  Now we have our list of selected Products and can perform any type of logic we want, sending emails, updating a field on the Contact, etc
           System.debug('These are the selected Products...');
           for(SVMXC__Installed_Product__c con: selectedProducts) {
               system.debug(con);
           }
    //      PoductList.clear();  we need this line if we performed a write operation  because getContacts gets a fresh list now
           return null;
       }
      
        public List<SVMXC__Installed_Product__c> GetSelectedProduct(){
          if(selectedProducts.size()>0)
         return selectedProducts;
         else
         return null;    
     } 
  
     // This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Contact and a Boolean value
       public class productwrapper {
           public SVMXC__Installed_Product__c con {get; set;}
           public Boolean selected {get; set;}
   
       //   This is the contructor method. When we create a new productwrapper object we pass a Installed Product that is set to the con property. We also set the selected value to false
           public productwrapper(SVMXC__Installed_Product__c c) {
               con = c;
               selected = false;
           }
       }
   }
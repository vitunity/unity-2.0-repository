public with sharing class OpportunityControllerExtension {
    
    @testVisible private final Opportunity opty;
    private final String acctAddress{get;private set;}
     
    public String acid {get; set;}

    //Added by Ajinkya for Lost Business change
    public Boolean isLostOppotunity {get; set;}
    
    public Decimal replacedQuoateAmount {get; set;}
    private Id oldReplacedQuoteId;
    private String accountSectorFromAccount;
    
    public OpportunityControllerExtension(ApexPages.StandardController stdController){
        replacedQuoateAmount = 0.00;
        this.opty = (Opportunity)stdController.getRecord();
        if(opty.id == null && opty.AccountID!=null){
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry,Account_Sector__c 
                                    FROM Account 
                                    WHERE Id = :opty.AccountId];
           
            opty.CurrencyIsoCode = oppAccount.CurrencyIsoCode;
            opty.Deliver_to_Country__c= oppAccount.Country__c;
            //Added by Divya for INC4638292.START
            if(oppAccount.Account_Sector__c != null){
                accountSectorFromAccount = oppAccount.Account_Sector__c;
                opty.Account_Sector__c = oppAccount.Account_Sector__c;
            }else{
                accountSectorFromAccount =  null;
            }
            //INC4638292.STOP
        }else if(opty.id != null && opty.AccountID != null){
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                  BillingState,BillingPostalCode,BillingCountry,Account_Sector__c 
                                  FROM Account 
                                  WHERE Id = :opty.AccountId];
            if(oppAccount.Account_Sector__c != null){
                accountSectorFromAccount = oppAccount.Account_Sector__c;
            }else{
                accountSectorFromAccount =  null;
            }                       
        }
        oldReplacedQuoteId = opty.Quote_Replaced__c;
        getReplacedQuoateAmount();
        initializeNonVarianInstallbase();
    }
    
    public PageReference getReplacedQuoateAmount(){
        if(opty.Type=='Replacement' && opty.Quote_Replaced__c != null) {
            list<BigMachines__Quote__c> replacedQuoteList = [select Net_Booking_Value__c from BigMachines__Quote__c
                                                             where Id =: opty.Quote_Replaced__c];
            if(!replacedQuoteList.isEmpty()){
                replacedQuoateAmount = replacedQuoteList[0].Net_Booking_Value__c;            
            }                                                 
        }
        return null;
    }
    
    public PageReference showQuoteReplaced (){
        if(opty.Type=='Replacement'){
            opty.Replaced_Quote_Date__c = system.today();
        }else{
            opty.Quote_Replaced__c = null;
            replacedQuoateAmount = 0.00;
            opty.Replaced_Quote_Date__c = null;
        }
        return null;
    }
  
    //Added by Ajinkya for Lost Business change
    public PageReference setLostOppotunity() {
      // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,''+ opty.StageName); 
        //            ApexPages.addMessage(myMsg);
       //System.assertEquals(opty.StageName,'2222');    
        if(opty.StageName == '8 - CLOSED LOST'){  
             
            isLostOppotunity = true;
        } else {
            isLostOppotunity = false;    
        }
        return null;
    }

    public PageReference getSitePartner(){
    return null;        
    }    
    public transient Map<String, Decimal> probabilityStageNameMap;
    
    public PageReference SaveandClose(){
        System.debug('----opty.AccountID'+opty.AccountID);
        if(opty.AccountID!=null){
                
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry,Account_Sector__c 
                                    FROM Account 
                                    WHERE Id = :opty.AccountId];
            
            opty.CurrencyIsoCode = oppAccount.CurrencyIsoCode;
            opty.Deliver_to_Country__c= oppAccount.Country__c;
            //Added by Divya for INC4638292.START
            if(oppAccount.Account_Sector__c != null){
                accountSectorFromAccount = oppAccount.Account_Sector__c;
                opty.Account_Sector__c = oppAccount.Account_Sector__c;
            }else{
                accountSectorFromAccount =  null;
            }
            //INC4638292.STOP
        }
        initializeNonVarianInstallbase();
        return null;
    }
    
    /**
     * Gets address for selected End User(ERP Partner)
     */
    public String getSitePartnerAddress(){
        if(opty.Site_Partner__c != null){
                
            List<Account> oppSitePartner = [SELECT Id,Name,AccountNumber,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry 
                                    FROM Account 
                                    WHERE Id = :opty.Site_Partner__c];
            
            if(!oppSitePartner.isEmpty()){
                return oppSitePartner[0].Name+'\r\n'+
                        (!String.isBlank(oppSitePartner[0].AccountNumber)?(oppSitePartner[0].AccountNumber)+'\r\n':'')+
                        (!String.isBlank(oppSitePartner[0].BillingCity)?(oppSitePartner[0].BillingCity)+'\r\n':'')+
                        (!String.isBlank(oppSitePartner[0].BillingCountry)?(oppSitePartner[0].BillingCountry+''):'');
            }           
        }
        return null;
    }
    
    /**
     * Formats address for selected account
     */
    public String getAcctAddress(){
        if(opty.AccountId != null){
            
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry 
                                    FROM Account 
                                    WHERE Id = :opty.AccountId];
            
            String acctAddress = (!String.isBlank(oppAccount.BillingStreet)?(oppAccount.BillingStreet+',\r\n'):'')+
                                (!String.isBlank(oppAccount.BillingCity)?(oppAccount.BillingCity+','):'')+
                                (!String.isBlank(oppAccount.BillingState)?(oppAccount.BillingState+','):'')+
                                (!String.isBlank(oppAccount.BillingPostalCode)?(oppAccount.BillingPostalCode+',\r\n'):'')+
                                (!String.isBlank(oppAccount.BillingCountry)?(oppAccount.BillingCountry+''):'');
                                
            
            return acctAddress;
        }
        return null;
    }
    
    /* Rakesh.1/7/2016.Start */
    
    public boolean getIsWithoutDeal(){
       
        if(opty.AccountId != null){
            
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry 
                                    FROM Account 
                                    WHERE Id = :opty.AccountId];
            if(oppAccount.BillingCountry == 'USA' || oppAccount.BillingCountry == 'Canada'|| oppAccount.BillingCountry == 'Puerto Rico'){
                return false;
            }
        }
        return true;
    }
    
    public list<SelectOption> getForecastPercentage(){
        list<SelectOption> lspOption = new list<SelectOption>();
        lspOption.add(new selectoption('0%','0%'));
        lspOption.add(new selectoption('20%','20%'));
        lspOption.add(new selectoption('40%','40%'));
        lspOption.add(new selectoption('60%','60%'));
        lspOption.add(new selectoption('80%','80%'));
        lspOption.add(new selectoption('100%','100%'));
        if(opty.AccountId != null){
            
            Account oppAccount = [SELECT CurrencyIsoCode,Country__c,BillingStreet,BillingCity,
                                    BillingState,BillingPostalCode,BillingCountry 
                                    FROM Account 
                                    WHERE Id = :opty.AccountId];
            if(oppAccount.BillingCountry == 'USA' || oppAccount.BillingCountry == 'Canada' || oppAccount.BillingCountry == 'Puerto Rico' || oppAccount.BillingCountry == 'Amer.Virgin Is.'){
                lspOption.clear();
                lspOption.add(new selectoption('0%','0%'));
                lspOption.add(new selectoption('25%','25%'));
                lspOption.add(new selectoption('50%','50%'));
                lspOption.add(new selectoption('75%','75%'));
                lspOption.add(new selectoption('100%','100%'));
            }
        }
        return lspOption;
    }

    public pagereference OppSave(){

        try{
            
            //validation rule : Competitive_Takeout
            if(opty.Existing_Equipment_Replacement__c == 'Yes'){
                
                if( (opty.Competitive_Takeout_Vendor__c == null || opty.Competitive_Takeout_Vendor__c == '')  && 
                    (opty.Competitive_System_Lost__c == null || opty.Competitive_System_Lost__c == '') && 
                    (opty.Service_Takeout__c == null || opty.Service_Takeout__c == '') ){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one takeout from Existing Equipment Replacement');
                    ApexPages.addMessage(myMsg);
                    return null;
                } else if( (opty.Competitive_Takeout_Vendor__c != null || opty.Competitive_Takeout_Vendor__c == '') && 
                          (opty.Varian_PCSN_Non_Varian_IB__c == null || opty.Varian_PCSN_Non_Varian_IB__c == '')) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Varian PCSN/Non Varian IB');
                    ApexPages.addMessage(myMsg);
                    return null;
                } else if( (opty.Competitive_System_Lost__c != null || opty.Competitive_System_Lost__c == '' ) && 
                          (opty.Varian_SW_Serial_No_Non_Varian_IB__c == null || opty.Varian_SW_Serial_No_Non_Varian_IB__c == '')) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Varian SW Serial No./Non Varian IB');
                    ApexPages.addMessage(myMsg);
                    return null;
                } else if( (opty.Service_Takeout__c != null || opty.Service_Takeout__c == '' )&& 
                          (opty.Varian_SC_Serial_No_Non_Varian_IB__c == null || opty.Varian_SC_Serial_No_Non_Varian_IB__c == '')) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Varian SC Serial No./Non Varian IB');
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            }
            //end validation rule  
            
            if(opty.Type=='Replacement' && opty.Quote_Replaced__c != null){
                List<BigMachines__Quote__c> replacedQuotes = new List<BigMachines__Quote__c>();
                System.debug(LoggingLevel.INFO,'-----oldReplacedQuoteId'+oldReplacedQuoteId+'-----opty.Quote_Replaced__c'+opty.Quote_Replaced__c);
                list<BigMachines__Quote__c> pQuoteListUpdate = [select Id, BigMachines__Is_Primary__c                                                             
                                                                from BigMachines__Quote__c 
                                                                where BigMachines__Opportunity__c =: opty.Id
                                                                and BigMachines__Is_Primary__c = true];
                String replacedByQuoteId = null;
                if(!pQuoteListUpdate.isEmpty()){
                    replacedByQuoteId = pQuoteListUpdate[0].Id;
                }   
                replacedQuotes.add(new BigMachines__Quote__c(Id = opty.Quote_Replaced__c, Is_Replaced__c = true,Replaced_By__c = replacedByQuoteId));
                                                          
                if(oldReplacedQuoteId!=null && oldReplacedQuoteId!=opty.Quote_Replaced__c){
                    replacedQuotes.add(new BigMachines__Quote__c(Id = oldReplacedQuoteId, Is_Replaced__c = false,Replaced_By__c = null));
                }
                update replacedQuotes;
            }else if(oldReplacedQuoteId!=null){
                update new BigMachines__Quote__c(Id = oldReplacedQuoteId, Is_Replaced__c = false,Replaced_By__c = null);
                opty.Quote_Replaced__c = null;
            }
            
            // ERROR Msg is no Competitor Synced to Account
            Boolean isSynced = checkCompetitorSync(opty);
            //if(!isSynced) {
            //    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one competitor in the "Competitor Details" Section.');
            //    ApexPages.addMessage(myMsg);
            //    return null;
            //}
            
            upsert opty;
            //Added by Divya for INC4638292.START
            if(accountSectorFromAccount == null){
                Account accObj = new Account(ID = opty.AccountId, Account_Sector__c = opty.Account_Sector__c);
                update accObj;
            }
            //INC4638292.STOP
            List<Competitor__c> competitors = getNonVarianInstalledProducts(opty);
            updateVIPNVIPStatus(competitors);
            
            if(!competitors.isEmpty()){
                upsert competitors; //INC4873200:INC4904281 NVIP_Key__c;
            }
        }catch(DMLException e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0));
            ApexPages.addMessage(myMsg);
            return null;
        }
        pagereference pg = new pagereference('/006/o');
        pg.setRedirect(true);
        return pg;
    }
    
    @testVisible 
    private Boolean checkCompetitorSync(Opportunity opp) {
        Boolean isSynced = false;
        for(NonVarianInstalledProduct nonVarianIP : nonInstalledVarianProducts){//&& opp.Existing_Equipment_Replacement__c == 'Yes'
            if((opp.StageName == '8 - CLOSED LOST' || opp.StageName == '7 - CLOSED WON') && nonVarianIP.nonVarianProduct.isLost__c){
                isSynced = true;
                break;
            }
        }
        return isSynced;
    }
    
    /* Rakesh.1/7/2016.End */
    
    /** Show accounts non varian installed products on opportunity page and add new products*/
    public List<NonVarianInstalledProduct> nonInstalledVarianProducts{get;set;}
    public void initializeNonVarianInstallbase(){
        nonInstalledVarianProducts = new List<NonVarianInstalledProduct>();
        Integer counter = 0;
        if(opty.Id!=null){
          for(Competitor__c competitor : [
              SELECT Id, Name, Product_Type__c,  Model__c, Vendor__c, Isotope_Source__c, Vault_identifier__c, Year_Installed__c, Service_Provider__c, Status__c, RecordType.Description,
              RecordType.DeveloperName, isLost__c 
              FROM Competitor__c
              Where Opportunity__c =:opty.Id
          ]){//NVIP_Key__c
              nonInstalledVarianProducts.add(
                 new NonVarianInstalledProduct(counter, competitor )
              );
              counter = counter + 1;
          }
        }
    }
    
    public void addRemoveInstalledProduct(){
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        String addRemoveFlag = pageParams.get('addremoveFlag');
        String rowNumber = pageParams.get('rowNumber');
        System.debug('-----addRemoveInstalledProduct'+addRemoveFlag);
        System.debug('-----rowNumber'+rowNumber);
        if(addRemoveFlag == 'add'){
            Integer currentSize = nonInstalledVarianProducts.size();
            System.debug('----npRecordTypeId'+ApexPages.currentPage().getParameters().get('recordTypeIdValue'));
            Id npRecordTypeId = ApexPages.currentPage().getParameters().get('recordTypeIdValue');
            RecordType selectedRecordType = [SELECT Id, DeveloperName, Description FROM RecordType WHERE Id =:npRecordTypeId][0];
            System.debug('----npRecordTypeId'+npRecordTypeId);
            nonInstalledVarianProducts.add(
                new NonVarianInstalledProduct(currentSize, 
                new Competitor__c(RecordTypeId=npRecordTypeId, RecordType=selectedRecordType,
                                    Product_type__c = selectedRecordType.Description))
            );
        }else{
            Integer rowId = Integer.valueOf(rowNumber);
            //nonInstalledVarianProducts.remove(rowId);
            
            if(nonInstalledVarianProducts[rowId].nonVarianProduct.Id == null) {
                nonInstalledVarianProducts.remove(rowId);
            } else {
                delete nonInstalledVarianProducts[rowId].nonVarianProduct;
                nonInstalledVarianProducts.remove(rowId);
            }
            
            for(NonVarianInstalledProduct nonInstalledProduct : nonInstalledVarianProducts){
                if(nonInstalledProduct.rowNumber>rowId){
                    nonInstalledProduct.rowNumber -= 1;
                }
            }
        }
    }
    
    @testVisible
    private List<Competitor__c> getNonVarianInstalledProducts(Opportunity opp){
        List<Competitor__c> competitors = new List<Competitor__c>();
        system.debug(' ==== nonInstalledVarianProducts ==== ' + nonInstalledVarianProducts);
        
        for(NonVarianInstalledProduct nonVarianIP : nonInstalledVarianProducts){//&& opp.Existing_Equipment_Replacement__c == 'Yes'
        system.debug('alert-1'+opp.Site_Partner__c);
            if((opp.StageName == '8 - CLOSED LOST')  && nonVarianIP.nonVarianProduct.isLost__c == true){
                 nonVarianIP.nonVarianProduct.Competitor_Account_Name__c = opp.AccountId;
                //->nonVarianIP.nonVarianProduct.NVIP_Key__c = opp.AccountId+''+nonVarianIP.nonVarianProduct.Model__c+''+nonVarianIP.nonVarianProduct.Vendor__c;
            //    nonVarianIP.nonVarianProduct.isLost__c = nonVarianIP.isLost;
                nonVarianIP.nonVarianProduct.isLost__c = true;
            }else{
                //->nonVarianIP.nonVarianProduct.NVIP_Key__c = opp.AccountId+''+nonVarianIP.nonVarianProduct.Model__c+''+nonVarianIP.nonVarianProduct.Vendor__c;
            }
            system.debug('opp.Site_Partner__c===>'+ opp.Site_Partner__c);
            system.debug('opp.Existing_Equipment_Replacement__c===>'+ opp.Existing_Equipment_Replacement__c);
            system.debug('opp.StageName===>' + opp.StageName );
            
            if(opp.StageName == '8 - CLOSED LOST' && opp.Existing_Equipment_Replacement__c == 'No'){
                    if(opp.Site_Partner__c != null && String.isNotBlank(opp.Site_Partner__c)){
                    nonVarianIP.nonVarianProduct.Competitor_Account_Name__c = opp.Site_Partner__c;
                }else {
                    nonVarianIP.nonVarianProduct.Competitor_Account_Name__c = opp.AccountId;
                }
                system.debug('  nonVarianIP.nonVarianProduct.Competitor_Account_Name__c'+   nonVarianIP.nonVarianProduct.Competitor_Account_Name__c);
                nonVarianIP.nonVarianProduct.Status__c = 'Current';
            }
            nonVarianIP.nonVarianProduct.Opportunity__c = opp.Id;
            //INC4873200:INC4904281 nonVarianIP.nonVarianProduct.Id = null;
            competitors.add(nonVarianIP.nonVarianProduct);
        }
        return competitors;
    }
    
    public List<SelectOption> getNonVarianProductRecordTypes(){
        List<SelectOption> recordTypeOptions = new List<SelectOption>();
        for(RecordType NVPrecordType : [SELECT Id, Name, Description FROM RecordType WHERE SobjectType = 'Competitor__c']){
            recordTypeOptions.add(new SelectOption(NVPrecordType.Id, NVPrecordType.Description));
        }
        return recordTypeOptions;
    }
    
    /**
     * Update replacement installed product's Marked For Replacement flag
     */
    @testVisible
    private void updateVIPNVIPStatus(List<Competitor__c> competitors){
        List<String> ipNumbers = new List<String>();
        List<SVMXC__Installed_Product__c> installedProductsToUpdate = new List<SVMXC__Installed_Product__c>();
        if(opty.StageName == '7 - CLOSED WON' || opty.StageName == '8 - CLOSED LOST'){
            if(!String.isBlank(opty.Varian_PCSN_Non_Varian_IB__c)){
                ipNumbers.addAll(opty.Varian_PCSN_Non_Varian_IB__c.split(','));
            }
            if(!String.isBlank(opty.Varian_SW_Serial_No_Non_Varian_IB__c)){
                ipNumbers.addAll(opty.Varian_SW_Serial_No_Non_Varian_IB__c.split(','));   
            }
            List<SVMXC__Installed_Product__c> installedProducts = [SELECT Id, Marked_For_Replacement__c FROM SVMXC__Installed_Product__c WHERE Name IN:ipNumbers];
            for(SVMXC__Installed_Product__c ip : installedProducts){
                if(opty.StageName == '8 - CLOSED LOST' && ip.Marked_For_Replacement__c){
                    ip.Marked_For_Replacement__c = false;
                    installedProductsToUpdate.add(ip);
                }else if(!ip.Marked_For_Replacement__c){
                    ip.Marked_For_Replacement__c = true;
                    installedProductsToUpdate.add(ip);
                }
            }
            for(Competitor__c competitor : [SELECT Id, Status__c, Model__c, Product_Type__c, Vendor__c FROM Competitor__c WHERE Name IN:ipNumbers]){//NVIP_Key__c
                if(opty.StageName == '7 - CLOSED WON' && opty.Existing_Equipment_Replacement__c == 'Yes'){
                    competitor.Status__c = 'Replaced';
                    competitors.add(competitor);
                }
            }
            
            if(!installedProductsToUpdate.isEmpty()) update installedProductsToUpdate;
        }
    }
    
    public class NonVarianInstalledProduct{
        public Integer rowNumber{get;set;}
        public Competitor__c nonVarianProduct{get;set;}
        public Boolean isLost {get;set;} // member variable used to Sync the Competitor detail with account is checked.
        public NonVarianInstalledProduct(Integer rowNumber, Competitor__c nonVarianProduct){
            this.rowNumber = rowNumber;
            this.nonVarianProduct = nonVarianProduct;
            this.isLost = false;
        }
    }
    
    /*
     * for lightning
     */
    public boolean getIsLightning(){
        /*
        User userObj = [SELECT Id, UserPreferencesLightningExperiencePreferred from User where Id =: UserInfo.getUserId() limit 001];
        return userObj.UserPreferencesLightningExperiencePreferred;
        */
        if(ApexPages.currentPage().getParameters().get('sfdcIFrameHost') != null ||
           ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ||
           ApexPages.currentPage().getParameters().get('isdtp') == 'p1') {      
            return true;
        }
        else {      
            return false;
        }   
    }
}
@isTest
public class OpportunityForecastDateUpdateCntrlTest {
  
    @isTest
    public static void testForecastDateUpdate(){
    Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        System.debug('---getQueries()1'+Limits.getQueries());
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales11111';
        erpPartner.Partner_Name_Line_1__c = 'Test';
        erpPartner.Street__c = 'Test';
        erpPartner.City__c = 'Test';
        erpPartner.State_Province_Code__c = '000';
        erpPartner.Country_Code__c = '1';
        erpPartner.Zipcode_Postal_Code__c = 'Test';
        erpPartner.Partner_Name_Line_1__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_2__c = 'Sampple Partner';
        erpPartner.Partner_Name_Line_3__c = 'Sampple Partner';
        //insert erpPartner;
        System.debug('---getQueries()2'+Limits.getQueries());
        
        //insertERPAssociations('Sales11111', erpPartner.Id, salesAccount.Id);
        
        //Contact con = TestUtils.getContact();
        //con.AccountId = salesAccount.Id;
        //insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        //opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        System.debug('---getQueries()4'+Limits.getQueries());
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        System.debug('---getQueries()6'+Limits.getQueries());
        
        BigMachines__Quote__c salesQuote = TestUtils.getQuote();
        salesQuote.BigMachines__Account__c = salesAccount.Id;
        salesQuote.BigMachines__Opportunity__c = opp.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Price_Group__c = 'Z2';
        insert salesQuote;
        System.debug('---getQueries()7'+Limits.getQueries());
        
        
        
        //Create Sales Order
        Sales_Order__c so = UnityDataTestClass.SO_Data(false,salesAccount);
        so.Quote__c = salesQuote.id;
        insert so;
        //Sales_Order_Item__c SOI = UnityDataTestClass.SOI_Data(true, SO);
        ERP_WBS__c wbs = new ERP_WBS__c();
        wbs.Sales_Order__c = so.id;
        wbs.Forecast_End_Date__c = system.today().addDays(1);
        wbs.Forecast_Start_Date__c = system.today();
        wbs.ErpWbs_Installation_Date__c = system.today();
        
        insert wbs;
        
        system.Test.startTest();
        OpportunityForecastDateUpdateController.updateForecastDates(new list<Id> {wbs.Id});
        wbs.Forecast_End_Date__c = null;
        wbs.Forecast_Start_Date__c = null;
        wbs.ErpWbs_Installation_Date__c = null;
        update wbs;
        OpportunityForecastDateUpdateController.updateForecastDates(new list<Id> {wbs.Id});
        
        System.Test.stopTest();
        
    }
}
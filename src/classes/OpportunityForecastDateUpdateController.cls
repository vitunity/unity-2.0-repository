/*
* @Author: Ajinkya
* @Description: This controller will  update
* the min of the Forecast Start Dates on ERP WBS and map it to iStart Date on the Opportunity.
* the max of the Forecast End Dates on ERP WBS and map it to iFinish Date on the Opportunity.
* the max of the Forecast End Dates on ERP WBS and map it to ATP Signed on the Opportunity
*/
public class OpportunityForecastDateUpdateController{
    
    @InvocableMethod
    public static void updateForecastDates(list<id> ids){
        list<Sales_Order__c> salesOrderList = [SELECT Id, Quote__r.BigMachines__Opportunity__c,
                                               (SELECT Id, Forecast_Start_Date__c, Forecast_End_Date__c, ErpWbs_Installation_Date__c, 
                                                Sales_Order__r.Quote__r.BigMachines__Opportunity__c,
                                                Sales_Order__r.Quote__r.BigMachines__Opportunity__r.iStart_date__c,
                                                Sales_Order__r.Quote__r.BigMachines__Opportunity__r.iFinish_date__c,
                                                Sales_Order__r.Quote__r.BigMachines__Opportunity__r.ATP_Signed__c                                              
                                                FROM ERP_WBS__r)
                                               FROM Sales_Order__c WHERE id in (SELECT Sales_Order__c FROM ERP_WBS__c WHERE id in : ids)];
                                               
        map<Id,Opportunity> opportunityMap = new map<Id,Opportunity> ();
        
        for(Sales_Order__c so:salesOrderList){
            for(ERP_WBS__c wbs : so.ERP_WBS__r){
                if(wbs.Sales_Order__r.Quote__r.BigMachines__Opportunity__c != null){
                    Opportunity oppObj;
                    
                    if(opportunityMap.containskey(wbs.Sales_Order__r.Quote__r.BigMachines__Opportunity__c)){
                        oppObj = opportunityMap.get(wbs.Sales_Order__r.Quote__r.BigMachines__Opportunity__c);
                    }else{
                        oppObj = new Opportunity(Id=wbs.Sales_Order__r.Quote__r.BigMachines__Opportunity__c, 
                                                  iStart_date__c = wbs.Forecast_Start_Date__c, iFinish_date__c = wbs.Forecast_End_Date__c, 
                                                    ATP_Signed__c = wbs.ErpWbs_Installation_Date__c);
                                                    
                        opportunityMap.put(oppObj.Id,oppObj);                         
                    }
                    
                    if(oppObj.iStart_date__c == null){
                        oppObj.iStart_date__c = wbs.Forecast_Start_Date__c;
                    }
                    
                    if(oppObj.iStart_date__c > wbs.Forecast_Start_Date__c){
                        oppObj.iStart_date__c = wbs.Forecast_Start_Date__c;
                    }
                    
                    if(oppObj.iFinish_date__c == null){
                        oppObj.iFinish_date__c = wbs.Forecast_End_Date__c;
                    }
                    
                    if(oppObj.iFinish_date__c < wbs.Forecast_End_Date__c){
                        oppObj.iFinish_date__c = wbs.Forecast_End_Date__c;
                    }
          
                    if(oppObj.ATP_Signed__c == null){
                      oppObj.ATP_Signed__c = wbs.ErpWbs_Installation_Date__c;
                    }
                              
                    if(oppObj.ATP_Signed__c < wbs.ErpWbs_Installation_Date__c){
                        oppObj.ATP_Signed__c = wbs.ErpWbs_Installation_Date__c;
                    }                       
                }
            }           
        }
        
        if(!opportunityMap.isEmpty()){
          Database.SaveResult[] lsr = Database.update(opportunityMap.values(), false);
        }
    }
    
}
public with sharing class OpportunityInLineEditCtrl {
  
    // method for fetch account records list  
    @AuraEnabled
    public static List <Opportunity> getOpportunities(String accountId) {
        system.debug('accountId===='+accountId);
        return [select id, Name, StageName,CloseDate,CurrencyIsoCode,SFDC_Oppty_Ref_number__c,Amount, CreatedDate from Opportunity where Accountid =:accountId];
    }
    
  // method for update records after inline editing  
    @AuraEnabled
    public static List <Opportunity> saveOpps(List<Opportunity> oppList) {
        map<Id,Opportunity> oppsToUpdate = new map<Id,Opportunity>();
        //system.debug('oppList==='+oppList.size());
        map<Id,Opportunity> oppsmap = new Map<Id,Opportunity>([select Id, Name, StageName, CurrencyIsoCode, CloseDate from Opportunity where id in : oppList]);
        for(Opportunity opp: oppList){
            Opportunity qOpp = oppsmap.get(opp.Id);
            //system.debug('qOpp======'+qOpp);
            //system.debug('opp======'+opp);
            if(qOpp.StageName != opp.StageName
              || qOpp.CloseDate != opp.CloseDate ){
                oppsToUpdate.put(opp.Id,opp);
            }
        }
        if(!oppsToUpdate.isEmpty()){
            //system.debug('oppsToUpdate==='+oppsToUpdate.size());
            update oppsToUpdate.values();
        }
        
        return oppList;
    }
    
  // method for fetch picklist values dynamic  
    @AuraEnabled
    public static List < String > getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
}
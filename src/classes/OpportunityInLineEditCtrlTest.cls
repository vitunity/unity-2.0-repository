/***************************************************************************
Author: Amitkumr Katre
Created Date: 15-Sept-2017
Project/Story/Inc/Task : This is a controller for inline opportunity edit form account
Description:
This is a controller for inline opportunity edit form account test class
*************************************************************************************/
@isTest 
public class OpportunityInLineEditCtrlTest {

    @isTest(SeeAllData = true) 
    private static void inlineOpp_test() {
        Opportunity opp = [SELECT Id, AccountId, StageName, Amount FROM Opportunity  LIMIT 1 ];
        try {
            OpportunityInLineEditCtrl.getOpportunities(opp.AccountId);
            OpportunityInLineEditCtrl.getselectOptions(opp, 'StageName');
            OpportunityInLineEditCtrl.saveOpps(new list<Opportunity> {opp});
        } catch(Exception e) {
            
        }
    }
}
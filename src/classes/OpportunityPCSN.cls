public with sharing class OpportunityPCSN {
    
    @testVisible private Opportunity opty;
    public String Siteid;
    public OpportunityPCSN(ApexPages.StandardController stdController){
        opty = [
            SELECT Id, Varian_PCSN_Non_Varian_IB__c, Varian_SW_Serial_No_Non_Varian_IB__c,Site_Partner__c
            FROM Opportunity
            WHERE Id =:stdController.getId()
        ];
        Siteid=String.ValueOf(opty.get('Site_Partner__c'));
        System.debug('alert-beep'+Siteid);
    }
    
    public OpportunityPCSN(){
    }
    
    public List<sObject> getInstalledProducts(){
        Map<String, String> pageParameters = ApexPages.currentPage().getParameters();
        String objectName = pageParameters.get('objectName');
        String accountId = pageParameters.get('accountId');
        String sitePartnerAccId = pageParameters.get('sitePartnerAccId');
        String productFamily = pageParameters.get('productFamily');
        String selectedIPValues = pageParameters.get('selectedIPValues');
        
        List<String> nonVarianIPFamilies = new List<String>();
        if(objectName == 'SVMXC__Installed_Product__c'){ 
            if(productFamily == 'HW'){
                nonVarianIPFamilies = getVarianProductCodes('HW');
            }else{
                nonVarianIPFamilies = getVarianProductCodes('SW');
            }
            if(sitePartnerAccId != null){
                accountId = sitePartnerAccId;
            }
            
        String ipQuery = getIPQuery() +' WHERE SVMXC__Company__c =:accountId AND ERP_Pcodes__r.Name IN:nonVarianIPFamilies'
            +    ' LIMIT 1000';
        system.debug(' ===== ipQuery ====== ' + ipQuery);
        return Database.query(ipQuery);
        } else if(objectName == 'Competitor__c') {
            if(productFamily == 'HW'){
                if(!String.isBlank(System.Label.Non_Varian_HW_Families)){
                    nonVarianIPFamilies = System.Label.Non_Varian_HW_Families.split(',');
                }
            }else{
                if(!String.isBlank(System.Label.Non_Varian_SW_Families)){
                    nonVarianIPFamilies = System.Label.Non_Varian_SW_Families.split(',');
                }
            }
            String nonVarianIPQuery = getNonVarianIPQuery()+' WHERE Competitor_Account_Name__c =:accountId AND RecordType.DeveloperName IN:nonVarianIPFamilies'
                    +    ' LIMIT 1000 ';
            return Database.query(nonVarianIPQuery);
        
        } else {
            String TPSC = System.Label.Non_Varian_Service_Contracts;
            String serviceContractQuery = getNonVarianIPQuery() + ' WHERE Competitor_Account_Name__c =: accountId ' + 
                                        ' AND RecordType.DeveloperName =: TPSC ';//AND Vendor__c =: selectedIPValues 
            system.debug(' === QUERIED Records === ' + Database.query(serviceContractQuery));
            return Database.query(serviceContractQuery);
        }
    }
    
    @testVisible
    private List<String> getVarianProductCodes(String family){
        List<String> pCodes = new List<String>();
        String productQuery = 'SELECT Id, ERP_Pcode__r.Name FROM Product2 WHERE Product_Type__c = \'Model\'';
        if(family == 'HW'){
            productQuery += ' AND Family IN (\'System Solution\') AND IsActive = true';
        }else{
            productQuery += ' AND Family IN (\'Software Solution\',\'Subscription\') AND Status__c != \'Obsolete\'';
        }
        for(Product2 product : Database.query(productQuery)){
            pCodes.add(product.ERP_Pcode__r.Name);
        }
        return pCodes;
    }
    
    public List<SVMXC__Installed_Product__c> getVarianIPTakeouts(){
        List<String> selectedIPNumbers = getSelectedIPNumbers();
        String ipQuery = getIPQuery() + ' WHERE Name IN: selectedIPNumbers';
        return Database.query(ipQuery);
    }
    
    public List<Competitor__c> getNonVarianIpTakeouts(){
        List<String> selectedNonVarianIPNumbers = getSelectedIPNumbers();
        String nonVarianIPQuery = getNonVarianIPQuery() + ' WHERE Name IN: selectedNonVarianIPNumbers';
        return Database.query(nonVarianIPQuery);
    }
    
    @testVisible
    private List<String> getSelectedIPNumbers(){
        String selectedHardwareIPs = '';
        if(!String.isBlank(opty.Varian_PCSN_Non_Varian_IB__c)){
            selectedHardwareIPs = opty.Varian_PCSN_Non_Varian_IB__c;
        }
        
        String selectedSOftwareIps = '';
        if(!String.isBlank(opty.Varian_SW_Serial_No_Non_Varian_IB__c)){
            selectedSOftwareIps = opty.Varian_SW_Serial_No_Non_Varian_IB__c;
        }
        
        List<String> selectedIPNumbers = new List<String>();
        selectedIPNumbers.addAll(selectedHardwareIPs.split(','));
        selectedIPNumbers.addAll(selectedSOftwareIps.split(','));
        return selectedIPNumbers;
    }
    
    @testVisible
    private String getIPQuery(){
        return 'SELECT Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, '
    +'Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,'
    +'SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,'
    +'SVMXC__Service_Contract__r.SVMXC__Start_Date__c, ERP_Pcodes__r.Name '
    +'FROM SVMXC__Installed_Product__c ';
    }
    
    @testVisible
    private String getNonVarianIPQuery(){
        return 'SELECT Id, Name, Product_Type__c, Model__c, Vendor__c, Isotope_Source__c, Vault_identifier__c, Year_Installed__c, '
      +'Service_Provider__c, Status__c, RecordType.Description '
      +'FROM Competitor__c ';
    }
}
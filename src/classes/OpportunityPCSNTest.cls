/**
 * @author:         Puneet Mishra
 * @decsription     test class for OpportunityPCSNTest
 */
@isTest
public class OpportunityPCSNTest {
    public static List<Id> recordTypeId = new List<String>();
    public static Account acct;
    public static Regulatory_Country__c regulatory;
    public static Contact con;
    public static SVMXC__Installed_Product__c ip;
    public static Opportunity salesOpp;
  
    static {
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Brachytherapy Afterloaders').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Delivery Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Image Management Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Oncology Information Systems').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Other 3rd Party Connected Devices').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('Treatment Planning Solutions').getRecordTypeId());        
        
        acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        ip = new SVMXC__Installed_Product__c();
        ip.Name = 'H192192';
        ip.SVMXC__Status__c = 'Installed';
        ip.Marked_For_Replacement__c = false;
        insert ip;
        
        salesOpp = TestUtils.getOpportunity();
        salesOpp.AccountId = acct.Id;
        salesOpp.StageName = '7 - CLOSED WON';
        salesOpp.Primary_Contact_Name__c = con.Id;
        salesOpp.MGR_Forecast_Percentage__c = '';
        salesOpp.Unified_Funding_Status__c = '20';
        salesOpp.Unified_Probability__c = '20';
        salesOpp.Net_Booking_Value__c = 200;
        salesOpp.CloseDate = System.today();
        salesOpp.Opportunity_Type__c = 'Sales';
        salesOpp.Varian_PCSN_Non_Varian_IB__c = ip.Name+',';
        salesOpp.Varian_SW_Serial_No_Non_Varian_IB__c = ip.Name+',';
        insert salesOpp;
    }
    
    public static List<Competitor__c> createCompetitorData(Boolean isInsert, String oppId, String accId) {
        List<Competitor__c> compList = new List<Competitor__c>();
        Competitor__c comp;
        for(Id i : recordTypeId) {
            comp = new Competitor__c();
            comp.RecordTypeId = i;
            comp.Competitor_Account_Name__c = accId;
            comp.Opportunity__c = oppId;
            comp.Vendor__c = 'AGAT';
            comp.Model__c = 'AGAT-BT';
            compList.add(comp);
        }
        if(isInsert)
            insert compList;
         return compList;
    }
    
    public static testMethod void OpportunityPCSN_Test() {
      test.startTest();
            //List<Competitor__c> compList = createCompetitorData(true, salesOpp.Id, acct.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            system.assertEquals(control.opty.Id, salesOpp.Id);
        test.stopTest();
    }
    
    public static testMethod void getInstalledProducts_HW_test() {
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.OpportunityIPTakeouts')); 
            System.currentPageReference().getParameters().put('objectName', 'SVMXC__Installed_Product__c');
            System.currentPageReference().getParameters().put('accountId', acct.Id);
            System.currentPageReference().getParameters().put('productFamily', 'HW');
        
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getInstalledProducts();
        test.stopTest();
    }
    
    public static testMethod void getInstalledProducts_SW_test() {
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.OpportunityIPTakeouts')); 
            System.currentPageReference().getParameters().put('objectName', 'SVMXC__Installed_Product__c');
            System.currentPageReference().getParameters().put('accountId', acct.Id);
            System.currentPageReference().getParameters().put('productFamily', 'SW');
        
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getInstalledProducts();
        test.stopTest();
    }
    
    public static testMethod void getInstalledProducts_Competitor_HW_test() {
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.OpportunityIPTakeouts')); 
            System.currentPageReference().getParameters().put('objectName', 'Competitor__c');
            System.currentPageReference().getParameters().put('accountId', acct.Id);
            System.currentPageReference().getParameters().put('productFamily', 'HW');
        
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getInstalledProducts();
        test.stopTest();
    }
    
    public static testMethod void getInstalledProducts_Competitor_SW_test() {
        test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.OpportunityIPTakeouts')); 
            System.currentPageReference().getParameters().put('objectName', 'Competitor__c');
            System.currentPageReference().getParameters().put('accountId', acct.Id);
            System.currentPageReference().getParameters().put('productFamily', 'SW');
        
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getInstalledProducts();
        test.stopTest();
    }
    
    public static testMethod void getVarianIPTakeouts_Test() {
        test.startTest();
          ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
          control.getVarianIPTakeouts();
        test.stopTest();
    }
    
    public static testMethod void getNonVarianIpTakeouts_Test(){
        test.startTest();
          ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
          control.getNonVarianIpTakeouts();
        test.stopTest();
    }
    
    public static testMethod void getSelectedIPNumbers_Test(){
        test.startTest();
          ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
          control.getSelectedIPNumbers();
        test.stopTest();
    }
    
    //@isTest(seeAllData = true)
    public static testMethod void getVarianProductCodes_HW_Test() {
        test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getVarianProductCodes('HW');
        test.stopTest();
    }
    
    //@isTest(seeAllData = true)
    public static testMethod void getVarianProductCodes_SW_Test() {
        test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
            OpportunityPCSN control = new OpportunityPCSN(sc);
            control.getVarianProductCodes('SW'); 
        test.stopTest();
    }
    
    public static testMethod void  IPQuery_Test() {
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(salesOpp);
        OpportunityPCSN control = new OpportunityPCSN(sc);
        control.getNonVarianIPQuery();
        control.getIPQuery();
        test.stopTest();
    }
    
}
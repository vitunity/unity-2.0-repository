/**
* Used as controller for OpportunityPrimaryContact lookup page
* for Primary Contact lookup on Opportunity edit page
*/
public with sharing class OpportunityPrimaryContacts {
    
    //Page parameters
    private String accountId;
    private String paginatorFilter;
    
    private static Integer RECORDS_PER_PAGE = 10;
    
    public OpportunityPrimaryContacts(){
        accountId = ApexPages.currentPage().getParameters().get('accountId');
    }
    
    /**
     * Returns list of contacts related to search text and page parameters
     */
    public List<Contact> getContacts(){
        return (List<Contact>)contactsPaginator.getRecords();
    }
    
    /**
     * Search contacts based on page accountId and search string
     */
   
    /** reference to the paginating controller and get ERP association paginator*/
    private ApexPages.StandardSetController contactsPaginator {
        get {
            Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
            String searchText = pageParameters.get('searchText');
            System.debug('-----searchText'+searchText);
            if(contactsPaginator == null && String.isBlank(searchText)) {
                
                contactsPaginator = getContactsBySOQL();
                contactsPaginator.setPageSize(RECORDS_PER_PAGE);
            }else if(paginatorFilter != searchText && pageParameters.containsKey('searchText')){
                
                if(String.isBlank(searchText)){
                    contactsPaginator = getContactsBySOQL();
                }else{
                    contactsPaginator = getContactsBySOSL(searchText);
                }
                paginatorFilter = searchText;
                contactsPaginator.setPageSize(RECORDS_PER_PAGE);
            }
            return contactsPaginator;
        }
        set;
    }
    
    ///////////// Private Utility Methods //////////////
    /**
     * Return query instead of sosl because we need all erp partner associations 
     * and not used sosl on ERP Partrner Association as sosl will not search formula fields
     * (most of the fields from erp partner are reffered using formula fields in ERP partner association)
     */
    private ApexPages.StandardSetController getContactsBySOSL(String searchText){
        
        string [] searchStrings = searchText.split(' ');
            
        string soslQuery = 'FIND \'';
        
        for(string str : searchStrings){
            soslQuery += str+' OR ';
        }
        
        soslQuery = soslQuery.removeEnd(' OR ');
        soslQuery += '\' IN ALL FIELDS RETURNING Contact(Id,Account.Name,FirstName,LastName,Inactive_Contact__c,Name,Phone,Email,Owner.Alias,'
                        +'MailingCity,MailingPostalCode,MailingState,MailingCountry,Account.Id)';
        system.debug('---soslQuery'+soslQuery);
        
        List<Contact> contacts = (List<Contact>) Search.query(soslQuery).get(0);
        
        return new ApexPages.StandardSetController(contacts);
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all ERP Associations with search text
     */
    private ApexPages.StandardSetController getContactsBySOQL(){
        return new ApexPages.StandardSetController(Database.getQueryLocator([Select Id,Name,Account.Name,Phone,FirstName,LastName,Email,Owner.Alias,Account.Id,
                                                                                    Inactive_Contact__c, MailingCity,MailingPostalCode,MailingState,MailingCountry
                                                                                From Contact
                                                                                Where AccountId =:accountId and Inactive_Contact__c = FALSE]));
    }
    
    //////////////////////////////////////////////////////////////////////////////
    ////// Pagination  Actions                                               /////
    //////////////////////////////////////////////////////////////////////////////
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return contactsPaginator.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return contactsPaginator.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return contactsPaginator.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    public Integer totalPages {
        get {
            Decimal dTotalPages = contactsPaginator.getResultSize()/contactsPaginator.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(contactsPaginator.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    public void first() {
        contactsPaginator.first();
    }

    // returns the last page of records
    public void last() {
        contactsPaginator.last();     
    }

    // returns the previous page of records
    public void previous() {
        contactsPaginator.previous();
    }

    // returns the next page of records
    public void next() {
       contactsPaginator.next();
    }
}
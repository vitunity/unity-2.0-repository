@isTest
private class OpportunityPrimaryContactsTest {
  
  static testMethod void testLookupController() {
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        List<Contact> contacts = new List<Contact>();
        for(Integer counter=0; counter<25; counter++){
          Contact con = TestUtils.getContact();
          con.LastName = 'LastName'+Counter;
          con.AccountId = salesAccount.Id;
          contacts.add(con);
        }
        insert contacts;
        
        // test opportunity contacts page without filter
        PageReference contactsPage = Page.OpportunityContacts;
        contactsPage.getParameters().put('accountId',salesAccount.Id);
        
        
        Test.setCurrentPage(contactsPage);
        
        OpportunityPrimaryContacts oppContactsController = new OpportunityPrimaryContacts();
        System.assertEquals(10,oppContactsController.getContacts().size());
        
        
        
        //Test Pagination actions
        System.assertEquals(10,oppContactsController.getContacts().size());
        System.assert(oppContactsController.hasNext);
        oppContactsController.next();
        System.assertEquals(10,oppContactsController.getContacts().size());
        oppContactsController.next();
        System.assertEquals(5,oppContactsController.getContacts().size());
        System.assert(oppContactsController.hasPrevious);
        oppContactsController.previous();
        System.assertEquals(10,oppContactsController.getContacts().size());
        oppContactsController.last();
        System.assertEquals(5,oppContactsController.getContacts().size());
        oppContactsController.first();
        System.assertEquals(10,oppContactsController.getContacts().size());
        System.assertEquals(1,oppContactsController.pageNumber);
        System.assertEquals(3,oppContactsController.totalPages);
    }
    
     static testMethod void testLookUpControllerSearchString(){
       Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        
        Account salesAccount1 = TestUtils.getAccount();
        salesAccount1.Name = 'TEST \'Test';
        salesAccount1.AccountNumber = 'SalesCustomer';
        salesAccount1.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert new List<Account>{salesAccount,salesAccount1};
        
        List<Contact> contacts1 = new List<Contact>();
        for(Integer counter=0; counter<25; counter++){
          Contact con = TestUtils.getContact();
          con.LastName = 'LastName'+Counter;
          con.AccountId = salesAccount.Id;
          contacts1.add(con);
        }
        //insert contacts1;
        
        List<Contact> contacts2 = new List<Contact>();
        for(Integer counter=0; counter<25; counter++){
          Contact con = TestUtils.getContact();
          con.LastName = 'LastNameNew'+Counter;
          con.AccountId = salesAccount1.Id;
          contacts1.add(con);
          contacts2.add(con);
        }
        insert contacts1;
        
        List<Id> contactIds = new List<Id>();
        for(Contact con : contacts2){
          contactIds.add(con.Id);
        }
        Test.setFixedSearchResults(contactIds);
        
        PageReference contactsPage = Page.OpportunityContacts;
        contactsPage.getParameters().put('accountId',salesAccount.Id);
        
        Test.setCurrentPage(contactsPage);
        
        OpportunityPrimaryContacts oppContactsController = new OpportunityPrimaryContacts();
        System.assertEquals(10,oppContactsController.getContacts().size());
        
        contactsPage.getParameters().put('searchText','LastNameNew');
        //Test Pagination actions
        System.assertEquals(10,oppContactsController.getContacts().size());
        System.assert(oppContactsController.hasNext);
        oppContactsController.next();
        System.assertEquals(10,oppContactsController.getContacts().size());
        oppContactsController.next();
        System.assertEquals(5,oppContactsController.getContacts().size());
        System.assert(oppContactsController.hasPrevious);
        oppContactsController.previous();
        System.assertEquals(10,oppContactsController.getContacts().size());
        oppContactsController.last();
        System.assertEquals(5,oppContactsController.getContacts().size());
        oppContactsController.first();
        System.assertEquals(10,oppContactsController.getContacts().size());
        System.assertEquals(1,oppContactsController.pageNumber);
        System.assertEquals(3,oppContactsController.totalPages);
        
     }
}
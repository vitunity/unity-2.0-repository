/***************************************************************************
Author: Anshul Goel
Created Date: 15-Sept-2017
Project/Story/Inc/Task : Oracle quote related list for opportunity with custom actions.
Description:
This is a controller for all quote quick action for lightning and sf1.
This controller is shared between lightning components and visualforce pages
Change Log:
*************************************************************************************/
public class OpportunityRelatedListContrl {
    
    @AuraEnabled public  List<BigMachines__Quote__c> bMQRL {get;set;}
    @AuraEnabled public Boolean viewAll{get;set;}
    @AuraEnabled public String siteID {get;set;}
	@AuraEnabled public String viewAllQuote {get;set;}
    /*
     * Fetch quotes related to opportunity.
     */
    @AuraEnabled
    public static OpportunityRelatedListContrl  fetchQuotes(String recordId){
        OpportunityRelatedListContrl opRL = new OpportunityRelatedListContrl();
        opRL.viewAll = false;
        opRL.bMQRL = new List<BigMachines__Quote__c>();
        string opUrl =  URL.getSalesforceBaseUrl().toExternalForm().substringBefore('.');
        opRL.viewAllQuote = opUrl + '.lightning.force.com/one/one.app?source=aloha#/sObject/'+recordId+'/rlName/BigMachines__BigMachines_Quotes__r/view';

  //https://varian--sfqa1.lightning.force.com/one/one.app?source=aloha#/sObject/0064400000m81tsAAA/rlName/BigMachines__BigMachines_Quotes__r/view 
        List<BigMachines__Quote__c> bMQList = [select Id,Name,Total_Offer_Price__c,BigMachines__Total_Amount__c, CurrencyIsoCode, BigMachines__Account__c,BigMachines__Account__r.Name, BigMachines__Description__c,BigMachines__Is_Primary__c,BigMachines__Status__c,
                                         Quote_Status__c,Prebook_Sales_Services__c,Booked_Sales_Services__c,CreatedById,CreatedBy.Name,createddate from BigMachines__Quote__c where BigMachines__Opportunity__c =: recordId 
                                         order by createddate desc limit 7] ;
        Integer  counter =0;
        if(bMQList != null && bMQList.size()>6)
           opRL.viewAll = true;
        for(BigMachines__Quote__c b : bMQList)
        {
           opRL.bMQRL.add(b);
           counter++;
           if(counter == 6)
              break;
        }
        opRL.siteID= BigMachines.BigMachinesFunctionLibrary.getOnlyActiveSiteId();
        return opRL;
    }


    @AuraEnabled
    public static  OpportunityRelatedListContrl  refreshSection(String recordId, String obj)
    {
       
        Type resultType = Type.forName('OpportunityRelatedListContrl');
        OpportunityRelatedListContrl opRL =(OpportunityRelatedListContrl)JSON.deserialize(obj,resultType);
        opRL.bMQRL = new List<BigMachines__Quote__c>();
        List<BigMachines__Quote__c> bMQList = [select Id,Name,Total_Offer_Price__c,BigMachines__Total_Amount__c, CurrencyIsoCode, BigMachines__Account__c,BigMachines__Account__r.Name, BigMachines__Description__c,BigMachines__Is_Primary__c,BigMachines__Status__c,
                                         Quote_Status__c,createddate, Prebook_Sales_Services__c,Booked_Sales_Services__c,CreatedById,CreatedBy.Name from BigMachines__Quote__c where BigMachines__Opportunity__c =: recordId 
                                         order by createddate desc limit 7] ;
        Integer  counter =0;

        if(bMQList != null && bMQList.size()>6)
           opRL.viewAll = true;

        for(BigMachines__Quote__c b : bMQList)
        {
           opRL.bMQRL.add(b);
           counter++;
           if(counter == 6)
              break;
        }
        return opRL;
    }

    @AuraEnabled
    public static void deleteQuote(String recordId)
    {

        delete ([select id from BigMachines__Quote__c where id=: recordId ]);
    }
    
    /*
     *  Create new quote
     */
    @AuraEnabled
    public static LightningResult newQuoteAction(String recordId){
        LightningResult rObj = OracleQuoteQuickActionCntrl.createQuote(recordId);
        return rObj;        
    }
    
    /*
     *  Clone quote
     */
    @AuraEnabled
    public static LightningResult cloneQuoteAction(String recordId, String oppId){
        return OracleQuoteQuickActionCntrl.cloneQuoteAction(recordId,oppId);        
    }
    
    /*
     *  Set As primary quote
     */
    @AuraEnabled
    public static LightningResult setAsPrimaryAction(String recordId){
        return OracleQuoteQuickActionCntrl.setAsPrimaryAction(recordId);        
    }
    
    /*
     *  Set As move quote
     */
    @AuraEnabled
    public static LightningResult moveQuoteAction(String recordId){
        return OracleQuoteQuickActionCntrl.moveQuoteAction(recordId);       
    }
}
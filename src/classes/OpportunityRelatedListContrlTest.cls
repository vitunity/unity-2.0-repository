/***************************************************************************
Author: Anshul Goel
Created Date: 01-Sept-2017
Project/Story/Inc/Task : Sales Lightning : STSK0012590
Description: 
This is a Test Class for OpportunityRelatedListContrl
Change Log:

*************************************************************************************/


@isTest(seeAllData = true)
private class OpportunityRelatedListContrlTest
{
    static testMethod void testMethod1() 
    {
        //create account
        Account acct = TestUtils.getAccount();
        acct.Name = ' dtd opp ffh jgfj 87796';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = '52356 W 3nd Street';
        acct.BillingCity = 'dumbarton';
        acct.BillingState = 'LA';
        acct.BillingPostalCode = '47855';
        acct.BillingCountry = 'CANADA';
        insert acct;
        
        
        //create opportunity
        Opportunity salesOpp = TestUtils.getOpportunity();
        salesOpp.AccountId = acct.Id;
        salesOpp.StageName = '0 - NEW LEAD';
        salesOpp.MGR_Forecast_Percentage__c = '';
        salesOpp.Unified_Funding_Status__c = '20%';
        salesOpp.Unified_Probability__c = '20%';
        salesOpp.Net_Booking_Value__c = 200;
        salesOpp.CloseDate = System.today();
        salesOpp.Opportunity_Type__c = 'Sales';
        insert salesOpp;
        
        //Create Oracle Quote
        BigMachines__Quote__c bq = new BigMachines__Quote__c();
        bq.Name = '2016-15108';
        bq.BigMachines__Account__c = acct.Id;
        bq.BigMachines__Opportunity__c = salesOpp.Id;
        bq.BigMachines__Is_Primary__c = true;
        bq.Order_Type__c = 'Sales';
        bq.BigMachines__Site__c = [select id from BigMachines__Configuration_Record__c].id;
        insert bq;

        Test.starttest();
        OpportunityRelatedListContrl  opCl = new OpportunityRelatedListContrl();
        opCl =  OpportunityRelatedListContrl.fetchQuotes(salesOpp.id);
        opCl =  OpportunityRelatedListContrl.refreshSection(salesOpp.id,JSON.serialize(opCl));
        
        LightningResult lr = OpportunityRelatedListContrl.newQuoteAction(bq.id);
        lr = OpportunityRelatedListContrl.cloneQuoteAction(bq.id,salesOpp.id);
        lr = OpportunityRelatedListContrl.setAsPrimaryAction(bq.id);
        lr = OpportunityRelatedListContrl.moveQuoteAction(bq.id);
        OpportunityRelatedListContrl.deleteQuote(bq.id);
        Test.stoptest();  
    }
}
/**
 * Used for EndUserLookup page on opportunity vf page
 */
public with sharing class OpportunitySitePartners {
    
    public List<Account> getAccounts(){
        
        Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
        
        Id accountId = pageParameters.get('accountId');
        
        String name = pageParameters.get('name');
        String partnerNumber = pageParameters.get('partnerNumber');
        String city = pageParameters.get('city');
        String country = pageParameters.get('country');
        System.debug('-----name'+name);
        if(!String.isBlank(name) || !String.isBlank(partnerNumber) || !String.isBlank(city) || !String.isBlank(country)){
            System.debug('-----name'+name);
            return getProspects(name,partnerNumber,city,country);
        }else{
            return getSitePartners(accountId);
        }
    }
    
    private List<Account> getSitePartners(Id accountId){
        Set<String> partnerNumbers = new Set<String>();
        
        List<ERP_Partner_Association__c> erpAssociations = [SELECT Id,ERP_Partner_number__c
                                                            FROM ERP_Partner_Association__c
                                                            WHERE Customer_Account__c =: accountId and Partner_Function__c = 'Z1=Site Partner'];
        
        for(ERP_Partner_Association__c erpAssociation : erpAssociations){
                partnerNumbers.add(erpAssociation.ERP_Partner_Number__c);
        }
        
        List<Account> sitePartners = [Select Id,Name,BillingCity,BillingCountry,AccountNumber
                                        From Account
                                        Where AccountNumber IN :partnerNumbers
                                        AND RecordType.DeveloperName = 'Site_Partner'
                                        order by Name Asc];
        return sitePartners;
    }
    
    private List<Account> getProspects(String name, String partnerNumber, String city, String country){
        
        String soqlQuery = 'Select Id,Name,AccountNumber,BillingCity,BillingCountry from Account where Account_Type__c=\'Prospect\' ';
        
        if (!String.isBlank(name))
          soqlQuery += ' and Name LIKE \''+String.escapeSingleQuotes(name)+'%\'';
        if (!String.isBlank(partnerNumber))
          soqlQuery += ' and AccountNumber = \''+partnerNumber+'\'';
        if (!String.isBlank(city))
          soqlQuery += ' and BillingCity LIKE \''+String.escapeSingleQuotes(city)+'%\'';
        if (!String.isBlank(country))
          soqlQuery += ' and BillingCountry LIKE \''+String.escapeSingleQuotes(country)+'%\'';
        
        soqlQuery += ' order by Name Asc LIMIT 5000';
        System.debug('-----soqlQuery'+soqlQuery);
        
        return (List<Account>) Database.query(soqlQuery);
    }
}
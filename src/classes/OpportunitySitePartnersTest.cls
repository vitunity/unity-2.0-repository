@isTest
private class OpportunitySitePartnersTest{

    static testMethod void testGetSitePartners(){
    	
    	Map<String,Schema.RecordTypeInfo> rtMapByName = schema.sObjectType.Account.getRecordTypeInfosByName();
    	
        Account soldTo = TestUtils.getAccount();
        soldTo.Name = 'Sold To Account';
        soldTo.RecordTypeId = rtMapByName.get('Sold To').getRecordTypeId();
        soldTo.AccountNumber = '222222';
        
        
        Id sitePartnerId = rtMapByName.get('Site Partner').getRecordTypeId();
        
        Account prospect1 = TestUtils.getAccount();
        prospect1.Name = 'Prospect1';
        prospect1.Account_Type__c = 'Prospect';
        prospect1.RecordTypeId = sitePartnerId;
        prospect1.AccountNumber = 'prospect1';
        
        Account prospect2 = TestUtils.getAccount();
        prospect2.Name = 'Prospect2';
        prospect2.Account_Type__c = 'Prospect';
        prospect2.RecordTypeId = sitePartnerId;
        prospect2.BillingCity = 'TESTING';
        prospect2.AccountNumber = 'prospect2';
        
        Account sitePartner = TestUtils.getAccount();
        sitePartner.Name = 'Site Partner';
        sitePartner.RecordTypeId = sitePartnerId;
        sitePartner.AccountNumber = '111111';
        insert new List<Account>{soldTo,sitePartner,prospect1,prospect2};
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Site Partner';
        erpPartner.Partner_Number__c = '111111';
        insert erpPartner;
        
        ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
        erpAssociation.ERP_Partner_Number__c = '111111';
        erpAssociation.Customer_Account__c = soldTo.Id;
        erpAssociation.Erp_Partner__c =	erpPartner.Id;
        erpAssociation.Partner_Function__c = 'Z1=Site Partner';
        insert erpAssociation;
        
        PageReference sitePartnerPage = Page.OpportunitySitePartners;
        sitePartnerPage.getParameters().put('accountId',soldTo.Id);
        Test.setCurrentPage(sitePartnerPage);
        
        OpportunitySitePartners sitePartners = new OpportunitySitePartners();
        System.assertEquals(sitePartners.getAccounts().size(),1);
        
        sitePartnerPage.getParameters().put('name','Prospect1');
        System.assertEquals(sitePartners.getAccounts().size(),1);
        System.assertEquals('Prospect1',sitePartners.getAccounts()[0].Name);
        
        sitePartnerPage.getParameters().put('name','');
        sitePartnerPage.getParameters().put('city','TESTING');
        System.assertEquals(sitePartners.getAccounts().size(),1);
        System.assertEquals('TESTING',sitePartners.getAccounts()[0].BillingCity);
    }
}
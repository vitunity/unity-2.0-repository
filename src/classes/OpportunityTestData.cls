public class OpportunityTestData
{

    public OpportunityTestData()
    {
    }
    
    public static Opportunity createOpportunity(Id accountId)
    {
        Opportunity opp = new Opportunity();
        opp.Name = 'My Varian Test Opportunity';
        opp.StageName = '30';
        opp.Probability =10;
        opp.ForecastCategoryName ='Pipeline';
        opp.CloseDate=System.Today();
        opp.AccountId = accountId;
        opp.I_want_to_create_a_new_opportunity__c = true;
//        opp.Primary_Contact_Name__c=  c.id ;
        
        return opp;
    }
    
/*    
    
    public static Opportunity createOpportunity(Id accountId, String strName) //, String .... other required fields)
    {
        Opportunity a = new Opportunity();
        a.Name = strName;
        //a.zip = strZip
        //a.state
        //a.country
        //a. - other required fields
        return a;
    }
    
    public static List<Opportunity> createOpportunities(Integer numOpportunities)
    {
        List<Opportunity> OpportunityList = new List<Opportunity>();
        for(Integer i = 0; i < numOpportunities; i++)
        {
            Opportunity a = new Opportunity();
            a.Name = 'My Varian Test Opportunity';
            //a.zip
            //a.state
            //a.country
            //a. - other required fields
            OpportunityList.add(a);
        }
        return OpportunityList;
    }
*/
}
/*************************************************************************\
    @ Author        : Ajinkya Raut
    @ Date          : 15-DEC-2016
    @ Description   : This class is a handler for Opportunity_beforeTrigger.
    @ Last Modified By  :   
    @ Last Modified On  :   
    @ Last Modified Reason  :   
/****************************************************************************/
public class OpportunityTriggerHandler{
    
    // This function checks Primary Quote Req.Delivery Date if it is coming from BMI as 01/01/1900 make it blank 
    public void checkExpectedDeliveryDate(List<Opportunity> newOpptyList,Map<Id,Opportunity> oldMap){
        date myDate = date.newInstance(1900, 01, 01);
        for(Opportunity opp: newOpptyList){
            if(opp.Order_In_House_Timestamp__c == null && opp.Order_In_House__c == true && opp.ISChecked__c == false){
                opp.Order_In_House_Timestamp__c = system.now();
                opp.ISChecked__c = true;
            } 
            //update manager forecast percentage
            if(String.isBlank(opp.MGR_Forecast_Percentage__c)){
                opp.MGR_Forecast_Percentage__c = opp.Unified_Funding_Status__c;
            }
            if(oldMap != null){
                if(opp.Expected_Delivery_Date__c != Null && opp.Expected_Delivery_Date__c == mydate){
                    if(oldMap.get(opp.id).Expected_Delivery_Date__c != Null && oldMap.get(opp.id).Expected_Delivery_Date__c != mydate ){
                    opp.Expected_Delivery_Date__c = oldMap.get(opp.id).Expected_Delivery_Date__c;
                    }
                }
            }else{
                if(opp.Expected_Delivery_Date__c != Null && opp.Expected_Delivery_Date__c == mydate){
                        opp.Expected_Delivery_Date__c = Null;
                }                
            }
        }  
    }
    
    /**
     * Implemented as part of INC4090497
     * This method updates opportunity forecast percentage for closed opportunities
     */
    public static void updateOpportunityFields(List<Opportunity> oppList, Map<Id, Account> accounts){
        
        for(Opportunity opp : oppList){
            Account oppAccount = accounts.get(opp.AccountId);
            if(opp.StageName == '7 - CLOSED WON' || opp.StageName == '8 - CLOSED LOST'){
                opp.Unified_Funding_Status__c = '100%';
                opp.MGR_Forecast_Percentage__c = '100%';
                if(oppAccount.BillingCountry != 'USA' && oppAccount.BillingCountry != 'Canada' && oppAccount.BillingCountry != 'Puerto Rico'){
                    opp.Unified_Probability__c = '100%';
                    if(opp.StageName == '8 - CLOSED LOST'){
                        opp.Unified_Probability__c = '0%';
                    }
                }else{
                    if(opp.StageName == '8 - CLOSED LOST'){
                        opp.Unified_Funding_Status__c = '0%';
                        opp.MGR_Forecast_Percentage__c = '0%';
                    }
                }
            }
        }
    }
}
@isTest
private class OpportunityTriggerHandlerTest {

    static testMethod void testOpportunityTriggerHandler() {
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='USA' ; 
        insert reg ;
        Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
        insert TestCountry1;
        List<Opportunity> opplist = new list<Opportunity>();
        Account acct = new Account();
        acct.Name = 'OPPORTUNITY TRIGGER HANDLER1';
        acct.Country__c = 'USA';
        acct.country1__c = TestCountry1.id;
        acct.OMNI_Postal_Code__c = '12121';
        acct.Account_Type__c = 'Customer';
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        
        Account acct1 = new Account();
        acct1.Name = 'OPPORTUNITY TRIGGER HANDLER2';
        acct1.Country__c = 'India';
        acct1.OMNI_Postal_Code__c = '12121';
        acct1.Account_Type__c = 'Customer';
        acct1.AccountNumber = 'Service1212';
        acct1.CurrencyISOCode = 'USD';
        acct1.BillingStreet = 'TEST';
        acct1.BillingCity = 'TEST';
        acct1.BillingState = 'TEST';
        acct1.BillingPostalCode = '11111';
        acct1.BillingCountry = 'India';
        insert new List<Account>{acct,acct1};
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = 'Ajinkya.Raut@varian.com';
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.Today();
        opp.StageName = '7 - CLOSED WON';
        opp.Probability = 100;
        opp.ForecastCategoryName = 'Best Case';
        opp.Deliver_to_Country__c = 'USA';
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        opplist.add(opp);
        
        Opportunity opp2 = new Opportunity();
        opp2.Name = 'Test Opp';
        opp2.CloseDate = Date.Today();
        opp2.StageName = '8 - CLOSED LOST';
        opp2.Date_Lost__c = Date.today();
        opp2.Competitor_Name__c = 'TEST';
        opp2.Competitive_Price__c = 1000;
        opp2.Lost_deal_components__c = 'TEST';
        opp2.Collection_of_Model_Information1__c = 'TEST';
        opp2.Number_of_Units_lost__c = 3;
        opp2.New_or_Replacement_vault_or_socket__c = 'TEST';
        opp2.Primary_reason__c = 'TEST';
        opp2.Primary_Loss_Reason__c = 'TEST';
        opp2.Secondary_Loss_Reason__c = 'TEST';
        opp2.Secondary_Reason__c = 'TEST';
        opp2.Probability = 100;
        opp2.ForecastCategoryName = 'Best Case';
        opp2.Deliver_to_Country__c = 'USA';
        opp2.AccountId = acct.Id;
        opp2.Primary_Contact_Name__c = con.Id;
        
        opplist.add(opp2);
        Opportunity opp3 = new Opportunity();
        opp3.Name = 'Test Opp';
        opp3.CloseDate = Date.Today();
        opp3.StageName = '7 - CLOSED WON';
        opp3.Probability = 100;
        opp3.ForecastCategoryName = 'Best Case';
        opp3.Deliver_to_Country__c = 'India';
        opp3.AccountId = acct1.Id;
        opp3.Primary_Contact_Name__c = con.Id;
        opplist.add(opp3);
        Opportunity opp4 = new Opportunity();
        opp4.Name = 'Test Opp';
        opp4.CloseDate = Date.Today();
        opp4.StageName = '8 - CLOSED LOST';
        opp4.Date_Lost__c = Date.today();
        opp4.Competitor_Name__c = 'TEST';
        opp4.Competitive_Price__c = 1000;
        opp4.Lost_deal_components__c = 'TEST';
        opp4.Collection_of_Model_Information1__c = 'TEST';
        opp4.Number_of_Units_lost__c = 3;
        opp4.New_or_Replacement_vault_or_socket__c = 'TEST';
        opp4.Primary_reason__c = 'TEST';
        opp4.Primary_Loss_Reason__c = 'TEST';
        opp4.Secondary_Loss_Reason__c = 'TEST';
        opp4.Secondary_Reason__c = 'TEST';
        opp4.Probability = 100;
        opp4.ForecastCategoryName = 'Best Case';
        opp4.Deliver_to_Country__c = 'India';
        opp4.AccountId = acct1.Id;
        opp4.Primary_Contact_Name__c = con.Id;
        opplist.add(opp4);
        insert opplist;
        
        update [Select Id FROM Opportunity];
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opplist[0].Id; 
        
        try{
            insert quote;
            delete opplist[0];
        }
        catch (exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains(' move or delete all associated quotes');
            //System.AssertEquals(expectedExceptionThrown, true);
        }
    }
    
}
/*
@ Author          : Shital Bhujbal
@ Date            : 20-Aug-2018
@ Description     : This class is written to send email if the Quote Products have any add-ons.
*/
public class OpportunityUplift {
    Map<Id, Id> optyPrimaryQuote= new Map<Id, Id>();
    Set<Id> optyIds = new Set<Id>();
    List<BigMachines__Quote__c> primaryQuoteList;
    List<BigMachines__Quote_Product__c> quoteProductsList;
    Map<Id,List<BigMachines__Quote_Product__c>> quoteProductsMap= new Map<Id,List<BigMachines__Quote_Product__c>>();
    List<String> upLiftSWPartNumberslist = new List<String>();
    List<String> upLiftHWPartNumberslist = new List<String>();
    List<String> HWModelNames = new List<String>();
    List<String> setToAddressesList = new List<String>();
    List<Messaging.SingleEmailMessage> lstmail = new List<Messaging.SingleEmailMessage>();
    Set<Id> emailAlreadySentQuote = new Set<Id>();
    
    public OpportunityUplift(){
        getPartNumbers();
    }
    //Call custom setting 'Quote Product Uplift'
    public void getPartNumbers(){
        
        for(QuoteProductUplift__c eachProduct : QuoteProductUplift__c.getall().values()){
            if(eachProduct.SW_Part_Number__c!=null){
                upLiftSWPartNumberslist.add(eachProduct.SW_Part_Number__c);
            }
            if(eachProduct.HW_Part_Number__c!=null){
                upLiftHWPartNumberslist.add(eachProduct.HW_Part_Number__c);
            }
            if(eachProduct.Model_Name__c!=null){
                HWModelNames.add(eachProduct.Model_Name__c);
            }
        }
    }
    
    public void check_UpliftQuotes(list<Opportunity> opptyList){
        
        for(Opportunity opp: opptyList){   
            optyIds.add(opp.Id);
        }
        
        Map<Id,BigMachines__Quote__c> primaryQuoteList = new Map<Id,BigMachines__Quote__c>([Select Id,Name,BigMachines__Opportunity__c from BigMachines__Quote__c where BigMachines__Opportunity__c IN: optyIds
                            AND BigMachines__Is_Primary__c = true]);
        
        quoteProductsList = [Select id,Name,BigMachines__Quote__c,BigMachines__Quote__r.Name,
                             BigMachines__Quote__r.BMI_Region__c,QP_Product_Model__c,
                             BigMachines__Quote__r.BigMachines__Account__r.District_Service_Manager__r.email,
                             BigMachines__Quote__r.BigMachines__Account__r.Service_Sales_Manager__r.email,
                             BigMachines__Quote__r.BigMachines__Account__r.Software_Sales_Manager__r.email 
                             from BigMachines__Quote_Product__c where BigMachines__Quote__c In: primaryQuoteList.keySet()];
        
        for(BigMachines__Quote_Product__c quoteProduct: quoteProductsList){
            
            if(quoteProductsMap.get(quoteProduct.BigMachines__Quote__c)!= null){
                
                List<BigMachines__Quote_Product__c> tempProducts = new List<BigMachines__Quote_Product__c>();
                tempProducts = quoteProductsMap.get(quoteProduct.BigMachines__Quote__c);
                tempProducts.add(quoteProduct);
                quoteProductsMap.put(quoteProduct.BigMachines__Quote__c,tempProducts);
            }else{
                quoteProductsMap.put(quoteProduct.BigMachines__Quote__c,new List<BigMachines__Quote_Product__c>{quoteProduct});
            }
        }
                                     
        for(BigMachines__Quote__c quote : primaryQuoteList.values()){
            optyPrimaryQuote.put(quote.BigMachines__Opportunity__c,quote.Id);
        }
        for(Opportunity opp: opptyList){
           
            if(opp.StageName == '7 - CLOSED WON' && optyPrimaryQuote.get(opp.Id)!=null && quoteProductsMap.get(optyPrimaryQuote.get(opp.Id))!= null){

                for(BigMachines__Quote_Product__c quoteProduct : quoteProductsMap.get(optyPrimaryQuote.get(opp.Id))){
                    if(quoteProduct.BigMachines__Quote__r.BMI_Region__c == 'NA'){
                        
                        if(upLiftSWPartNumberslist.contains(quoteProduct.Name)){
                            if(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Software_Sales_Manager__c!=null){
                                setToAddressesList.add(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Software_Sales_Manager__r.email);  
                            }
                            if(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Service_Sales_Manager__c!=null){
                            	setToAddressesList.add(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Service_Sales_Manager__r.email);
                        	}
                        }
                        if(upLiftHWPartNumberslist.contains(quoteProduct.Name) || HWModelNames.contains(quoteProduct.QP_Product_Model__c)){
                            //setToAddressesList.add(label.OpportunityUplift_UserEmail);
                            if(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.District_Service_Manager__c!=null){
                                setToAddressesList.add(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.District_Service_Manager__r.email);
                            }
                            if(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Service_Sales_Manager__c!=null){
                            	setToAddressesList.add(quoteProduct.BigMachines__Quote__r.BigMachines__Account__r.Service_Sales_Manager__r.email);
                        	}
                        }
                        
                        system.debug('email list=='+setToAddressesList);
                        //if SW - Send email to CIndy else if HW - send email to Service Sales Manager & District Service Manager on Account
                    }else if(quoteProduct.BigMachines__Quote__r.BMI_Region__c == 'EURE' || quoteProduct.BigMachines__Quote__r.BMI_Region__c == 'EURW'){
                         setToAddressesList.add('shital.bhujbal@varian.com');
                         setToAddressesList.add('roop.kaur@varian.com');
                    }
                    
                    if((setToAddressesList.size()> 0) && !(emailAlreadySentQuote.contains(optyPrimaryQuote.get(opp.Id)))
                      && quoteProductsMap.get(optyPrimaryQuote.get(opp.Id)).indexOf(quoteProduct)==((quoteProductsMap.get(optyPrimaryQuote.get(opp.Id))).size()-1)){ 
                        setToAddressesList.add('shital.bhujbal@varian.com');
                        system.debug('****setToAddressesList final SS***'+setToAddressesList);
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setToAddresses(setToAddressesList);    
                        //mail.setTargetObjectId(Userinfo.getUserId());
                        mail.setSubject('Service Uplift Notification for Quote - '+quoteProduct.BigMachines__Quote__r.Name);
                        mail.setHtmlBody('Please Note – This purchase may result in an increase of an existing service contract.<br/><br/>If you have any questions please contact your Varian Representative.');
                        mail.setSaveAsActivity(false);
                        lstmail.add(mail);
                        emailAlreadySentQuote.add(optyPrimaryQuote.get(opp.Id));
                    }
                }
            }              
        }
        Messaging.sendEmail(lstmail);
    }
}
@isTest
public class OpportunityUpliftTest {
    //static SVMXC__Service_Contract__c testServiceContract ;
    
    public static BigMachines__Quote__c subscriptionQuoteNew;
    static Contact con;
    static Account acc;
    static Opportunity opp;
    static list<opportunity> oppList;
    static list<BigMachines__Quote__c> bigMachineQuotesList;
    
    static{
        
        Territory_Team__c tt = new Territory_Team__c();
        tt.District__c = 'SW5';
        tt.Zip__c = '11111';
        tt.Region__c = 'Southwest';
        tt.Territory__c = 'North America';
        tt.Sub_Region__c = 'Southwest';
        tt.Country__c = 'USA';
        tt.Super_Territory__c = 'Americas';
        insert tt;
        
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425',BillingCity='LA',BillingState='CA',State_Province_Is_Not_Applicable__c=true);
        acc.Super_Territory1__c = 'Americas';
        acc.Territories1__c = 'North America';
        acc.Software_Sales_Manager__c= UserInfo.getUserId();
        acc.District_Service_Manager__c= UserInfo.getUserId();
        acc.Service_Sales_Manager__c= UserInfo.getUserId();
        acc.District_Sales_Manager__c= UserInfo.getUserId();
        insert acc;
        
        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
                          MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
        oppList=new List<Opportunity>();
         for(Integer i=0; i<=10; i++){ 
            Opportunity opp = TestUtils.getOpportunity();
            opp.AccountId = acc.Id;
            opp.StageName = '7 - CLOSED WON';
            opp.Primary_Contact_Name__c = con.Id;
            opp.CloseDate = System.today();
            opp.type = 'Service Contract - Renewal';
            opp.Expected_Close_Date__c = System.today();
            oppList.add(opp);
        }   
        
        insert oppList;
        bigMachineQuotesList=new list<BigMachines__Quote__c>();
        for(Opportunity opp: oppList){ 
            BigMachines__Quote__c bq = new BigMachines__Quote__c();
            bq.Name = '2016-15108'+opp.Id;
            bq.BigMachines__Account__c = acc.Id;
            bq.BigMachines__Opportunity__c = opp.Id;
            bq.BigMachines__Is_Primary__c = true;
            bq.Order_Type__c = 'Sales';
            bq.BMI_Region__c='NA';
            bigMachineQuotesList.add(bq);
        } 
        
        insert bigMachineQuotesList;
        
        insertQuoteProducts(bigMachineQuotesList);
        
    }
    
    public static Testmethod void getPartNumbersTest()
    {	
         Test.startTest();
         OpportunityUplift objOppUp = new OpportunityUplift();
         QuoteProductUplift__c qp = new QuoteProductUplift__c ();
         qp.Name='Test';
         qp.HW_Part_Number__c ='Test';
         qp.Model_Name__c ='Test';
         qp.SW_Part_Number__c ='Test';
         insert qp;
         objOppUp.getPartNumbers();
         Test.stopTest();
    }
    
    public static Testmethod void check_UpliftQuotesTest()
    {	
       Test.startTest();
       List<QuoteProductUplift__c> lstQPU=new List<QuoteProductUplift__c >(); 
       lstQPU.add(new QuoteProductUplift__c(name='ABO - Add-On',SW_Part_Number__c='CFG001090002'));
       lstQPU.add(new QuoteProductUplift__c(name='TrueBeam Upgrade',Model_Name__c='TrueBeam Upgrade'));
        lstQPU.add(new QuoteProductUplift__c(name='Test HW',HW_Part_Number__c='Test HW'));
       insert lstQPU;
        
       OpportunityUplift objOppUp = new OpportunityUplift();        
       objOppUp.check_UpliftQuotes(oppList);  
       Test.stopTest(); 
    }

    
    private static void insertQuoteProducts(List<BigMachines__Quote__c> quotes){
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        insert varianProduct;
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        
       
        List<BigMachines__Quote_Product__c> quoteProducts = new List<BigMachines__Quote_Product__c>();
        for(BigMachines__Quote__c bmQuote : quotes){
            quoteProducts.add(
                    new BigMachines__Quote_Product__c(name='CFG001090002',
                        BigMachines__Quote__c = bmQuote.Id,
                        BigMachines__Product__c = varianProduct.Id,
                        Header__c = true,
                        Product_Type__c = 'Sales',
                        BigMachines__Sales_Price__c = 40000,
                        BigMachines__Quantity__c = 1,
                        Line_Number__c= 12,
                        EPOT_Section_Id__c = 'TEST',
                        Net_Booking_Val__c = 1000
                        
                    )
                );
        }
        
        quoteProducts.add(
                    new BigMachines__Quote_Product__c(name='Test HW',
                        BigMachines__Quote__c = quotes[0].Id,
                        BigMachines__Product__c = varianProduct.Id,
                        Header__c = true,
                        Product_Type__c = 'Sales',
                        BigMachines__Sales_Price__c = 40000,
                        BigMachines__Quantity__c = 1,
                        Line_Number__c= 12,
                        EPOT_Section_Id__c = 'TEST',
                        Net_Booking_Val__c = 1000
                        
                    )
                );
        insert quoteProducts;
    }
}
/************************************************************************************************************************************************************************************************\
      @ Author          : Sunil Bansal
      @ Date            : 09-Apr-2013
      @ Description     : This class will be will be used to  create some Util methods for Opportunity. These methods may be called from many places, so named this class as UTIL for opportunity
      @ Last Modified By      :    
      @ Last Modified On      :     
      @ Last Modified Reason  :    
**************************************************************************************************************************************************************************************************/

public class OpportunityUtil 
{
    String retURLStr = '';
    String urlStr = '';
    public OpportunityUtil()
    {
        retURLStr = ApexPages.currentPage().getParameters().get('retURL');
    }
    
    public OpportunityUtil(ApexPages.StandardController controller)
    {
        retURLStr = ApexPages.currentPage().getParameters().get('retURL');
        urlStr = ApexPages.currentPage().getUrl();
    }
    
    /*
      Method to set default stage value to zero
    */    
    public PageReference setStageDefaultToZero()
    {
        urlStr = urlStr.replace('save_new=1', '');
        Integer ind = urlStr.indexOf('?');
        String newUrl = '';
        if(ind > 0)
            newUrl = '/006/e?&opp11=0&nooverride=1&'+urlStr.substring(ind+1);
        else
            newUrl = '/006/e?&opp11=0&nooverride=1&retURL='+retURLStr;

        PageReference pr = new PageReference(newUrl);
        pr.setRedirect(true);
        return pr;
    }
}
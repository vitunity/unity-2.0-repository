/**
 * This class contains unit tests for validating the behavior of Apex classe 'OpportunityUtil '
  */

@isTest 
Private  class OpportunityUtilTest {
     static testMethod void runTestCases() {
     OpportunityUtil Outil =new OpportunityUtil();
     //String retURLStr = ApexPages.currentPage().getParameters().get('retURL');
     //ApexPages.StandardController accountstdController = new ApexPages.StandardController(ApexPages.currentPage().getParameters().put('retURL','www.google.com')); //set up the standardcontroller
  
  
   
     Account a = AccountTestData.createAccount();
     insert a;
     
     
     Contact c=new Contact();
     c.FirstName = 'mike';
     c.LastName ='Waugh';
     c.Accountid= a.id;
     //Bhanu.08/23/2013 start.new.
     c.MailingCountry='USA';
     c.MailingState='CA';
     c.Email='test@gmail.com';
     //Bhanu.08/23/2013 end.new.
     insert c;
     
    
      Opportunity opp =OpportunityTestData.createOpportunity(a.Id);
      insert  opp;
      
      PageReference pageRef = Page.OpootunityOverrideNewButton;
      ApexPages.currentPage().getParameters().put('retURLStr ', 'yyyy');
      Test.setCurrentPage(pageRef);
      ApexPages.StandardController controller = new ApexPages.StandardController (opp); 
   
      OpportunityUtil controller11 = new OpportunityUtil(controller );
    
     Outil.setStageDefaultToZero();
      }
     }
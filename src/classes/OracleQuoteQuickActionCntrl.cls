/***************************************************************************
Author: Amikumar Katre
Created Date: 15-Sept-2017
Project/Story/Inc/Task : Oracle quote quick actions and list view actions controller
Description:
This is a controller for all quote quick action for lightning and sf1.
This controller is shared between lightning components and visualforce pages
Change Log:
*************************************************************************************/
public with sharing class OracleQuoteQuickActionCntrl {
    
    public OracleQuoteQuickActionCntrl(){}
    
    @testvisible
    public List<BigMachines__Quote__c> selected { get; private set; }
    
    @testvisible
    ApexPages.StandardSetController scSet;
    
    public OracleQuoteQuickActionCntrl(ApexPages.StandardSetController scSet){
        this.scSet =scSet;
    }
    
    public OracleQuoteQuickActionCntrl(ApexPages.StandardController sc){
    }
    
    public PageReference amendQuote(){
    list<BigMachines__Quote__c> quoteList = [Select BigMachines__Status__c,BigMachines__Opportunity__c,
                                         SAP_Prebooked_Sales__c,SAP_Prebooked_Service__c,Pre_Book_Order_Number__c, 
                                         Amendment__c from BigMachines__Quote__c 
                                         where Id =:getSelectedQuoteId()];
        if(getSelectedQuoteSize() == 1 && (quoteList[0].BigMachines__Status__c == 'Approved' 
            || quoteList[0].BigMachines__Status__c == 'Approved Amendment')){
          PageReference p = new PageReference('/apex/BigMachines__QuoteEdit?id='
                                   + quoteList[0].Id + '&oppId='+quoteList[0].BigMachines__Opportunity__c 
                                             + '&clone=true');
            return p;
        }
        return null;
    }
    
    public Boolean getIsValidQuoteforAmend(){
    list<BigMachines__Quote__c> quoteList = [Select BigMachines__Status__c,BigMachines__Opportunity__c,
                                         SAP_Prebooked_Sales__c,SAP_Prebooked_Service__c,Pre_Book_Order_Number__c, 
                                         Amendment__c from BigMachines__Quote__c 
                                         where Id =:getSelectedQuoteId()];
        if(getSelectedQuoteSize() == 1 && (quoteList[0].BigMachines__Status__c == 'Approved' 
            || quoteList[0].BigMachines__Status__c == 'Approved Amendment')){
          PageReference p = new PageReference('/apex/BigMachines__QuoteEdit?id='
                                   + quoteList[0].Id + '&oppId='+quoteList[0].BigMachines__Opportunity__c 
                                             + '&clone=true');
            return true; 
        }
        return false;
    }
    
    public Integer getSelectedQuoteSize(){
        selected = (List<BigMachines__Quote__c>)scSet.getSelected();
        return selected.size();
    }
    
    public Id getSelectedQuoteId(){
        selected = (List<BigMachines__Quote__c>)scSet.getSelected();
        if(!selected.isEmpty())
          return selected[0].Id;
        else
          return null;
    }
    
    //Create quote action from opportunity
    @AuraEnabled
    public static LightningResult createQuote(String oppId){
       LightningResult rObj = new LightningResult();
       boolean allowToCreateQuoate = false;
       set<String> adminProfileSet = new set<String>();
       adminProfileSet.add('System Administrator'); 
       adminProfileSet.add('VMS System Admin');
       list<User> userList = [select id from User where id =: UserInfo.getUserId() and Profile.Name in :adminProfileSet];
       if(!userList.isEmpty()) allowToCreateQuoate = true;
       list<PermissionSetAssignment> permissionSetAssignmentList =  [Select Id From PermissionSetAssignment 
                            Where PermissionSet.Name = 'VMS_Sales_Quote_User_Permission' 
                            and AssigneeId = : UserInfo.getUserId()];
       if(!permissionSetAssignmentList.isEmpty()) allowToCreateQuoate = true;
       if(allowToCreateQuoate){
            list<BigMachines__Quote__c> quoteList = [Select BigMachines__Is_Primary__c,SAP_Prebooked_Sales__c,
                                         SAP_Prebooked_Service__c,Booking_Message__c,Message_Service__c 
                                         from BigMachines__Quote__c 
                                         where BigMachines__Opportunity__c =:oppId
                                         and ((Order_Type__c = 'Sales' 
                                                and SAP_Prebooked_Sales__c!= null) 
                                               or (Order_Type__c = 'Service' 
                                                   and SAP_Prebooked_Service__c!= null) 
                                               or (Order_Type__c = 'Combined' 
                                                   and SAP_Prebooked_Sales__c!= null 
                                                   and SAP_Prebooked_Service__c!= null) 
                                              OR Interface_Status__c = 'Process' 
                                              OR Interface_Status__c = 'Processed')];
            if(quoteList.isEmpty()){
                rObj.status = 'success';
                rObj.message = 'Processing..................';
                //site id harcoded need to replace
                //rObj.url = 'https://varian--sfdev1--bigmachines.cs67.visual.force.com/apex/QuoteEdit?oppId=0060n0000023Zc4&siteId=a310n00000001VpAAI';
                rObj.url = '/apex/Bigmachines__QuoteEdit?oppId='+oppId+'&siteId='+BigMachines.BigMachinesFunctionLibrary.getOnlyActiveSiteId();
            }else{
                rObj.status = 'error';
                rObj.message = 'Cannot create new Quote as this Opportunity already has PreBooked Quote';
            }
       }else{
            rObj.status = 'error';
            rObj.message = 'You do not permission to create a new quote. Please contact System Administrator.';
       }
       return rObj;
    }
    
    //epot action
    @AuraEnabled
    public static boolean checkEpotAction(String quoteId){
        list<BigMachines__Quote__c> quoteList = [Select Submitted_To_SAP_By__c
                                                 from BigMachines__Quote__c
                                                 where id=:quoteId];
        if(!quoteList.isEmpty() && !String.isBlank(quoteList[0].Submitted_To_SAP_By__c) ){ 
            return true;
        }else{
            return false;
        }        
    }
    
    //epot line items action
    @AuraEnabled
    public static boolean checkEpotLineItemAction(String quoteId){
        list<BigMachines__Quote__c> quoteList = [Select Submitted_To_SAP_By__c
                                                 from BigMachines__Quote__c
                                                 where id=:quoteId];
        if(!quoteList.isEmpty()){ 
            return true;
        }else{
            return false;
        }        
    }
    
    //reset quote status action
    @AuraEnabled
    public static String resetQuoteStatus(String quoteId){
        String result; 
        list<BigMachines__Quote__c> quoteList = [Select Quote_Status__c, Interface_Status__c
                                                 from BigMachines__Quote__c
                                                 where id=:quoteId];
        if(!quoteList.isEmpty()&& (quoteList[0].Interface_Status__c == 'Error' 
                                   || quoteList[0].Quote_Status__c == 'Error')){ 
            quoteList[0].Interface_Status__c = '';
            quoteList[0].SAP_Prebooked_Sales__c = '';
            quoteList[0].SAP_Prebooked_Service__c = '';
            quoteList[0].Booking_Message__c  = '';
            quoteList[0].Message_Service__c  = '';
            try{
                update quoteList[0]; 
                result = 'Status reset is completed';
            }catch(Exception e){
                result = e.getMessage();
            }                          
            return result;
        }else{
            result = 'Sorry! You cannot reset the status Please reach out to IT Epot Support Team';
            return result;
        }        
    }
    
    //accounting checklist action 
    @AuraEnabled
    public static boolean checkAccountingChecklist(String quoteId){
        list<BigMachines__Quote__c> quoteList = [Select Order_Type__c, SAP_Prebooked_Sales__c, SAP_Prebooked_Service__c
                                                 from BigMachines__Quote__c
                                                 where id=:quoteId];
        if(!quoteList.isEmpty()
           && ((quoteList[0].Order_Type__c == 'Sales' && !String.isBlank(quoteList[0].SAP_Prebooked_Sales__c))
                || (quoteList[0].Order_Type__c == 'Services' && !String.isBlank(quoteList[0].SAP_Prebooked_Service__c))
                || (quoteList[0].Order_Type__c == 'Combined' && 
                    (!String.isBlank(quoteList[0].SAP_Prebooked_Service__c) || !String.isBlank(quoteList[0].SAP_Prebooked_Sales__c))
              ))
            ){ 
            return true;
        }else{
            return false;
        }        
    }
    
    
    /*
     *  Clone quote
     */
    @AuraEnabled
    public static LightningResult cloneQuoteAction(String recordId,String oppId){
        LightningResult rObj = new LightningResult();
        BigMachines__Quote__c quoteObj = [select Id, BigMachines__Account__c
                                          from BigMachines__Quote__c 
                                          where Id =: recordId]; 
        if(BigMachines.BigMachinesFunctionLibrary.canEditQuote(recordId)){
            rObj.status = 'success';
            rObj.message = 'Processing..................'; 
            //rObj.url = '/apex/Bigmachines__QuoteEdit?Id='+recordId+'&clone=true&actId='+quoteObj.BigMachines__Account__c;
            rObj.url = '/apex/Bigmachines__QuoteEdit?Id='+recordId+'&clone=true&actId='+quoteObj.BigMachines__Account__c+'&oppId='+oppId; 
        }else{
            rObj.url = '';
			rObj.status = 'error';
            rObj.message = 'You do not have permission to clone that quote. Please contact your system administrator.';
        }
        return rObj;        
    }
    
    
    @AuraEnabled
    public static Boolean canQuoteEditCheck(String recordId){
        system.debug('recordId======'+recordId);
        system.debug('BigMachinesFunctionLibrary.canEditQuote === '+ BigMachines.BigMachinesFunctionLibrary.canEditQuote(recordId));
    	return BigMachines.BigMachinesFunctionLibrary.canEditQuote(recordId);
    }
    
    /*
     *  Set As primary quote
     */
    @AuraEnabled
    public static LightningResult setAsPrimaryAction(String recordId){
        LightningResult rObj = new LightningResult();
        BigMachines__Quote__c quoteObj = [select Id, BigMachines__Account__c
                                          from BigMachines__Quote__c 
                                          where Id =: recordId]; 
        if(BigMachines.BigMachinesFunctionLibrary.canEditQuote(recordId)){
            rObj.status = 'success';
            rObj.message = 'Processing..................';
            rObj.url = 'recordId'; 
            BigMachines.BigMachinesFunctionLibrary.setQuoteAsPrimary(recordId);
        }else{
            rObj.url = '';
			rObj.status = 'error';
            rObj.message = 'You do not have permission to set that quote as primary. Please contact your system administrator.';
        }
        return rObj;        
    }
    
    /*
     *  Set As move quote
     */
    @AuraEnabled
    public static LightningResult moveQuoteAction(String recordId){
        LightningResult rObj = new LightningResult();
        BigMachines__Quote__c quoteObj = [select Id, BigMachines__Account__c, BigMachines__Is_Primary__c 
                                          from BigMachines__Quote__c 
                                          where Id =: recordId]; 
        if(BigMachines.BigMachinesFunctionLibrary.canEditQuote(recordId)){
            rObj.status = 'success';
            if(quoteObj.BigMachines__Is_Primary__c){
                rObj.message = 'This quote is marked as the primary quote for its current opportunity. Moving this quote will remove the associated opportunity products from the opportunity and may affect the value of the opportunity. Do you want to continue to move the quote and remove the associated opportunity products from the current opportunity?';
            }else{
				rObj.message = '';
            }
            rObj.url = '/apex/Bigmachines__QuoteMove?Id='+recordId+'&from_list=true'; 
        }else{
            rObj.url = '';
			rObj.status = 'error';
            rObj.message = 'You do not have permission to move the quote. Please contact your system administrator.';
        }
        return rObj;        
    }
}
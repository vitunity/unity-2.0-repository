/***************************************************************************
Author: Amikumar Katre
Created Date: 16-Jan-2018
Project/Story/Inc/Task : Sales Lightning
Description: 
This is a Test Class for OracleQuoteQuickActionCntrl
Change Log:

*************************************************************************************/
@isTest(seealldata=true)
public class OracleQuoteQuickActionCntrlTest {
	
    public static testmethod void quickActionTest(){
        Account acct = [SELECT Id FROM Account limit 1];
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        insert opp;
        
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.Name = 'TEST-QUOTE';
        bmQuote.BigMachines__Account__c = acct.Id; 
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.BigMachines__Status__c = 'Approved';
        bmQuote.Order_Type__c = 'Sales';
        bmQuote.Quote_Reference__c = 'TEST-QUOTE1';
        bmQuote.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote.BigMachines__Is_Primary__c = true;
        insert bmQuote;
        System.Test.startTest();
        OracleQuoteQuickActionCntrl obj = new OracleQuoteQuickActionCntrl();
        OracleQuoteQuickActionCntrl.createQuote(opp.Id);
        OracleQuoteQuickActionCntrl.checkEpotAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.checkEpotLineItemAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.resetQuoteStatus(bmQuote.Id);
        OracleQuoteQuickActionCntrl.checkAccountingChecklist(bmQuote.Id);
        OracleQuoteQuickActionCntrl.cloneQuoteAction(bmQuote.Id,opp.id);
        OracleQuoteQuickActionCntrl.canQuoteEditCheck(bmQuote.Id);
        OracleQuoteQuickActionCntrl.setAsPrimaryAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.moveQuoteAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl obj2 = new OracleQuoteQuickActionCntrl(new ApexPages.StandardController(bmQuote));
        list<BigMachines__Quote__c> lstQuote = new list<BigMachines__Quote__c>{bmQuote};
        OracleQuoteQuickActionCntrl obj3 = new OracleQuoteQuickActionCntrl(new ApexPages.StandardSetController(lstQuote));
        obj3.selected = lstQuote;
        obj3.amendQuote();
        obj3.getIsValidQuoteforAmend();
        obj3.scSet.setSelected(lstQuote);
        obj3.amendQuote();
        obj3.getIsValidQuoteforAmend();
        System.Test.stopTest();
    }
    
    public static testmethod void quickActionTest2(){
        Account acct = [SELECT Id FROM Account limit 1];
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        insert opp;
        
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.Name = 'TEST-QUOTE';
        bmQuote.BigMachines__Account__c = acct.Id; 
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.BigMachines__Status__c = 'Error';
        bmQuote.Interface_Status__c = 'Error';
        bmQuote.Quote_Status__c = 'Error';
        bmQuote.Order_Type__c = 'Sales';            
        bmQuote.Quote_Reference__c = 'TEST-QUOTE1';
        bmQuote.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote.BigMachines__Is_Primary__c = true;
        insert bmQuote;
        System.Test.startTest();
        OracleQuoteQuickActionCntrl obj = new OracleQuoteQuickActionCntrl();
        OracleQuoteQuickActionCntrl.createQuote(opp.Id);
        OracleQuoteQuickActionCntrl.checkEpotAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.checkEpotLineItemAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.resetQuoteStatus(bmQuote.Id);
        OracleQuoteQuickActionCntrl.checkAccountingChecklist(bmQuote.Id);
        OracleQuoteQuickActionCntrl.cloneQuoteAction(bmQuote.Id,opp.id);
        OracleQuoteQuickActionCntrl.canQuoteEditCheck(bmQuote.Id);
        OracleQuoteQuickActionCntrl.setAsPrimaryAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl.moveQuoteAction(bmQuote.Id);
        OracleQuoteQuickActionCntrl obj2 = new OracleQuoteQuickActionCntrl(new ApexPages.StandardController(bmQuote));
        list<BigMachines__Quote__c> lstQuote = new list<BigMachines__Quote__c>{bmQuote};
        OracleQuoteQuickActionCntrl obj3 = new OracleQuoteQuickActionCntrl(new ApexPages.StandardSetController(lstQuote));
        
        obj3.selected = lstQuote;
        obj3.amendQuote();
        obj3.getIsValidQuoteforAmend();
        System.Test.stopTest();
    }
}
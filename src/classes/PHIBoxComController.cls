public class PHIBoxComController {
    BoxAPIConnection api;
    String accessToken;
    String expires;
    String isUpdate;

    public List<WUser> lstUser{get;set;}
    public PHIBoxComController() {}
    public PHIBoxComController(apexpages.standardController con){
        Initialize();        
    }
    
    public void Initialize(){
        User usr;
        PHI_Log__c phi = [select id,Folder_Id__c,Owners_Box_Email__c,OwnerId from PHI_Log__c where id=: apexpages.currentpage().getparameters().get('id')];
        if(phi.OwnerId!=null) {
            usr = [Select Id, username from User Limit 1];
        }
        lstUser = new List<WUser>();

        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();

        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

        // Get All Root Folder Collaboration ID and below it will avoid to remove those access
        List<String> collabList = new List<String>();
        List<String> phiDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Phi_data_folder_id);
		collabList.addAll(phiDataFolderList);
		List<String> ectDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Ect_data_folder_id);
		collabList.addAll(ectDataFolderList);

        api = new BoxAPIConnection(accessToken);
        BoxFolder folder = new BoxFolder(api, phi.Folder_Id__c);
        list<BoxCollaboration.Info> collaborations = folder.getCollaborations();
        for (BoxCollaboration.Info i : collaborations) {
            //System.debug('i-------------'+i);
            if(i.accessibleBy <> null){
                boolean isRemovable = ((phi.Owners_Box_Email__c <> null && phi.Owners_Box_Email__c.equals(i.accessibleBy.children.get('login'))) || collabList.contains(i.id))?false:true;
                if(i.accessibleBy.Name==null || i.accessibleBy.Name=='')
                	lstUser.add(new WUser(isRemovable,i.id,i.accessibleBy.children.get('login'),i.accessibleBy.children.get('name'),string.valueOf(i.status),string.valueOf(i.role)));
                else
                    lstUser.add(new WUser(isRemovable,i.id,i.accessibleBy.children.get('login'),i.accessibleBy.Name,string.valueOf(i.status),string.valueOf(i.role)));
            }
        }

        /*try {
			http http=new http();
			HTTPResponse res=new HttpResponse();
			HttpRequest req = new HttpRequest();
			req.setMethod('GET');
			req.setHeader('Content-Type', 'application/json');
			req.setHeader('Authorization','Bearer '+accessToken); 
			String endpoint = 'https://api.box.com/2.0/folders/'+String.valueOf(phi.Folder_Id__c)+'/collaborations';
			req.setEndpoint(endpoint);
			res = http.send(req);
			String jasonresp = res.getBody();
			Integer statusCode = (Integer) res.getstatuscode();
			Map<String, Object> jsonObj = (Map<String, Object>)JSON.deserializeUntyped(jasonresp);
			System.debug('JsonRP---------'+jasonresp);
			System.debug('statusCode---------'+statusCode);
            
            if(statusCode==200 || statusCode < 400) {
                List<Object> entryList = (List<Object>) jsonObj.get('entries');
                System.debug('entryListObj---------'+entryList);
                
                List<Map<String,String>> collabList = new List<Map<String,String>>();
                
                for(Object entryKeyObj:entryList) {
                    Map<String, Object> sEntryObj = (Map<String, Object>) entryKeyObj;
                    Map<String, String> tempMap = new Map<String, String>();

                    for(String sk:sEntryObj.keyset()) {
                        if(sk == 'accessible_by') {
                            Map<String, Object> accessibleByObj = (Map<String, Object>) sEntryObj.get(sk);
                            if(accessibleByObj != null) {
                                for(String k:accessibleByObj.keyset()) {
                                    if(k=='id') tempMap.put('userId', String.valueOf(accessibleByObj.get(k)));
                                    if(k=='login') tempMap.put('login', String.valueOf(accessibleByObj.get(k)));
                                    if(k=='name') tempMap.put('name', String.valueOf(accessibleByObj.get(k)));
                                    if(k=='type') tempMap.put('userType', String.valueOf(accessibleByObj.get(k)));
                                }
                            }
                        }
                        if(sk=='role') tempMap.put('role', String.valueOf(sEntryObj.get(sk)));
                        if(sk=='status') tempMap.put('status', String.valueOf(sEntryObj.get(sk)));
                        if(sk=='id') tempMap.put('collabId', String.valueOf(sEntryObj.get(sk)));
                    }
                    collabList.add(tempMap);
                }
                System.debug('collabList============='+collabList);
                for(Map<String, String> tempMap:collabList) {
                    boolean isRemovable = (phi.Owners_Box_Email__c <> null && phi.Owners_Box_Email__c.equals(tempMap.get('login')))?false:true;
					//boolean isRemovable = (usr <> null && usr.username.equals(tempMap.get('name')))?false:true;
                    if(tempMap.get('type')=='group') {
						lstUser.add(new WUser(isRemovable,tempMap.get('collabId'),'',tempMap.get('name'),tempMap.get('status'),tempMap.get('role')));
                    } else {
                        lstUser.add(new WUser(isRemovable,tempMap.get('collabId'),tempMap.get('login'),tempMap.get('name'),tempMap.get('status'),tempMap.get('role')));
                    }
                    System.debug('tempMap============='+tempMap);    
                }
            }
            if(statusCode > 400) {
                System.debug('-------Collaboration does not exist-----------');
            }
        } catch(Exception e) {
            system.debug('@@@Collaboration exception'+e.getMessage());
        }*/
        
		// Update Box Access Token to Custom Setting
		// Below method updating access token
		//Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
    }

    public PageReference executeDML() {
        Box_Credential__c boxCRecord = Ltng_BoxAccess.getBoxCredential();
        //String accessToken = Ltng_BoxAccess.generateAccessToken();
        Map<String,String> resultMap = Ltng_BoxAccess.generateAccessToken();

        if(resultMap != null) {
            accessToken = resultMap.get('accessToken');
            expires = resultMap.get('expires');
            isUpdate = resultMap.get('isUpdate');
        }

		// Update Box Access Token to Custom Setting
		Ltng_BoxAccess.updateBoxAccessToken(accessToken, expires, isUpdate);
        return null;
    }

    public pagereference RemoveCollaboration() {
        // Get All Root Folder Collaboration ID and below it will avoid to remove those access
        List<String> collabList = new List<String>();
        List<String> phiDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Phi_data_folder_id);
		collabList.addAll(phiDataFolderList);
		List<String> ectDataFolderList = Ltng_BoxAccess.retrieveFolderCollabIdList(label.Box_Ect_data_folder_id);
		collabList.addAll(ectDataFolderList);

        string CollaborationId = apexpages.currentpage().getparameters().get('CollaborationId');
        BoxCollaboration collaboration = new BoxCollaboration(api, CollaborationId);
        if(!collabList.contains(CollaborationId)) {
			collaboration.deleteCollaboration();
        }
        Initialize();

		String recId = String.valueOf(apexpages.currentpage().getparameters().get('id'));
        if(recId != null) {
			// Update PHI Log History
			Ltng_BoxAccess.createPHIHistoryLog(CollaborationId + ' is removed for Varian Secure Drop folder.', recId, 'PHI_Log__c');
        }
        return null;
    }
    public class WUser{
        public boolean isRemovable{get;private set;}
        public string colaborationid{get;private set;}
        public string loginId{get;private set;}
        public string name{get;private set;}
        public string status{get;private set;}
        public string role{get;private set;}
        public WUser(boolean isRemovable, string colaborationid,string loginId,string name, string status, string role){this.isRemovable=isRemovable;this.colaborationid=colaborationid;this.loginId = loginId;this.name = name;this.status = status;this.role = role;}
    }
}
@isTest
public class PHIBoxComControllerTest {

	private static String accessToken = 'accesstoken';
    private static String refreshToken = 'refreshtoken';
    private static String clientId = 'clientid';
    private static String clientSecret = 'clientsecret';
    private static String authCode = 'authcode';
    private static String entityId = 'entityid';
    private static String userId = 'userid';
    private static String enterpriseId = 'enterpriseid';
    private static String publicKeyId = 'publicKeyId';
    private static String privateKey = BoxTestJsonResponseFactory.AUTH_PRIVATE_KEY;
    private static String privateKeyPassword = 'privateKeyPassword';
    private static BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm = BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256;
    
    testMethod static void testCheckPHICollaboration() {
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
        insert phi;
        
		phi.Case_Folder_Id__c = '123456';
        phi.Folder_Id__c = '123456';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/123456';
        phi.BoxFolderName__c = 'Testing';
		phi.Collab_id__c = '791293';
        update phi;

    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));
        
    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
    	
    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
        BoxFolder folder1 = new BoxFolder(api, '11446498');
    	
    	Test.startTest();

        BoxFolder.Info createdFolderInfo = folder.createFolder('Pictures1245');
      	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR; 
      	BoxCollaboration.Info newCollab = folder.collaborate('nileshgorle@gmail.com', collabRole);
      	BoxCollaboration.Role collabRole1 = BoxCollaboration.Role.CO_OWNER; 
      	BoxCollaboration.Info newCollab1 = folder.collaborate('nilesh.gorle@varian.com', collabRole1);        

        PHIBoxComController phiController = new PHIBoxComController();
        ApexPages.currentPage().getParameters().put('id', phi.Id);
        phiController.Initialize();
        test.stopTest();        
    }

    testMethod static void testCheckPHICollaboration2() {
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
		phi.Case_Folder_Id__c = '11446500';
        phi.Folder_Id__c = '11446500';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/11446500';
        phi.BoxFolderName__c = 'Testing';
		phi.Collab_id__c = '791293';
        insert phi;
        
    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));
        
    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
    	
        Test.startTest();
    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
      	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR; 
      	BoxCollaboration.Info newCollab = folder.collaborate('nileshgorle@gmail.com', collabRole);
      	BoxCollaboration.Role collabRole1 = BoxCollaboration.Role.CO_OWNER; 
      	BoxCollaboration.Info newCollab1 = folder.collaborate('nilesh.gorle@varian.com', collabRole1);

        PHIBoxComController phiController = new PHIBoxComController();
        ApexPages.currentPage().getParameters().put('id', phi.Id);
        phiController.executeDML();
        test.stopTest();        
    }
    
    testMethod static void testCheckPHICollaboration3() {
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
		bxCrd.Client_Key__c = 'clientid';
		bxCrd.Client_Secret_Key__c = 'clientsecret';
		bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

       	Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
		acc.BillingCountry='Test';
		acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
		acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();
		phi.Case_Folder_Id__c = '11446498';
        phi.Folder_Id__c = '11446500';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/11446500';
        phi.BoxFolderName__c = 'Testing';
		phi.Collab_id__c = '791293';
        insert phi;
        
    	String mockResponseBody1 = '{"type":"folder","id":"11446498","sequence_id":"0","etag":"0","name":"Pictures","created_at":"2012-12-12T10:53:43-08:00","modified_at":"2012-12-12T11:15:04-08:00","description":"Some pictures I took","size":629644,"path_collection":{"total_count":1,"entries":[{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"}]},"created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"modified_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"owned_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"shared_link":{"url":"https://www.box.com/s/vspke7y05sb214wjokpk","download_url":null,"vanity_url":null,"is_password_enabled":false,"unshared_at":null,"download_count":0,"preview_count":0,"access":"open","permissions":{"can_download":true,"can_preview":true}},"folder_upload_email":{"access":"open","email":"upload.Picture.k13sdz1@u.box.com"},"parent":{"type":"folder","id":"0","sequence_id":null,"etag":null,"name":"All Files"},"item_status":"active","item_collection":{"total_count":0,"entries":[],"offset":0,"limit":100}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody1, 'Created', 201));
        
    	String mockResponseBody = '{"type":"collaboration","id":"791293","created_by":{"type":"user","id":"17738362","name":"sean rose","login":"nilesh.gorle@varian.com"},"created_at":"2012-12-12T10:54:37-08:00","modified_at":"2012-12-12T11:30:43-08:00","expires_at":null,"status":"accepted","accessible_by":{"type":"user","id":"18203124","name":"sean","login":"nilesh.gorle@varian.com"},"role":"editor","acknowledged_at":"2012-12-12T11:30:43-08:00","item":{"type":"folder","id":"11446500","sequence_id":"1","etag":"1","name":"Shared Pictures"}}';
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(mockResponseBody, 'Created', 201));
    	
    	Test.setMock(HttpCalloutMock.class, new BoxTestMockCallout(null, 'No Content', 204));

        Test.startTest();
    	BoxApiConnection api = new BoxApiConnection('accesstoken');
    	BoxFolder folder = new BoxFolder(api, '11446500');
        BoxFolder folder1 = new BoxFolder(api, '11446498');

      	BoxCollaboration.Role collabRole = BoxCollaboration.Role.EDITOR; 
      	BoxCollaboration.Info newCollab = folder.collaborate('nileshgorle@gmail.com', collabRole);
      	BoxCollaboration.Role collabRole1 = BoxCollaboration.Role.CO_OWNER; 
      	BoxCollaboration.Info newCollab1 = folder.collaborate('nilesh.gorle@varian.com', collabRole1);

        ApexPages.currentPage().getParameters().put('id', phi.Id);
        apexpages.currentpage().getparameters().put('CollaborationId', '123456');
        PHIBoxComController phiController = new PHIBoxComController();
        phiController.Initialize();
        phiController.RemoveCollaboration();
        test.stopTest();        
    }

    testMethod static void testWUser() {
        test.startTest();
        PHIBoxComController phiController = new PHIBoxComController();
		PHIBoxComController.WUser wuserobj = new PHIBoxComController.WUser(true, '123456', 'nilesh.gorle@varian.com', 'Nilesh G', 'Pending', 'EDITOR');
		test.stopTest();
	}
}
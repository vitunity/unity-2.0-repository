public class PHIComplaintNumberUpdateController {
    ApexPages.StandardController sc ;
    public boolean isEditable{get;set;}
    public PHI_Log__c phi{get;set;}
    public PHIComplaintNumberUpdateController(ApexPages.StandardController sc){
        this.sc = sc;
        phi = (PHI_Log__c)sc.getRecord();
        isEditable = false;
    }
    public void updateIsEditable(){
        isEditable = true;
    }
    public PageReference save(){
        string Cno = apexpages.currentpage().getparameters().get('complaintNo');
        system.debug(Cno+'--'+phi);
        try{
            phi.Complaint__c = Cno;
            update phi;
        }catch(Exception e){
            system.debug(Cno+'--'+phi+':::'+e);
        }
        return null;
    }
}
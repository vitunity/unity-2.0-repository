@isTest
public class PHIComplaintNumberUpdateControllerTest{

    private static String accessToken = 'accesstoken';
    private static String refreshToken = 'refreshtoken';
    private static String clientId = 'clientid';
    private static String clientSecret = 'clientsecret';
    private static String authCode = 'authcode';
    private static String entityId = 'entityid';
    private static String userId = 'userid';
    private static String enterpriseId = 'enterpriseid';
    private static String publicKeyId = 'publicKeyId';
    private static String privateKey = BoxTestJsonResponseFactory.AUTH_PRIVATE_KEY;
    private static String privateKeyPassword = 'privateKeyPassword';
    private static BoxJwtEncryptionPreferences.EncryptionAlgorithm algorithm = BoxJwtEncryptionPreferences.EncryptionAlgorithm.RSA_SHA_256;

    testMethod static void testcheckPermission() {
        Box_Credential__c bxCrd = new Box_Credential__c();
        bxCrd.Name = 'UNITY - BOX Integration123';
        bxCrd.Active__c = true;
        bxCrd.Box_Public_Key__c = publicKeyId;
        bxCrd.Box_Private_Key_Password__c = privateKeyPassword;
        bxCrd.Enterprise_Id__c = 'enterpriseid';
        bxCrd.Client_Key__c = 'clientid';
        bxCrd.Client_Secret_Key__c = 'clientsecret';
        bxCrd.Folder_Path__c = 'https://varian.app.box.com/folder/';
        bxCrd.Access_Token__c = accessToken;
        bxCrd.Access_Token_Expires_In__c = 36000000;
        bxCrd.Access_Token_Issue_Datetime__c = datetime.now();
        insert bxCrd;

        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
        acc.BillingCountry='Test';
        acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
        acc.BillingStreet='xyx';
        insert acc;

        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;

        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;

        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;

        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = acc.Id;
        phi.Product_ID__c = cs.ProductSystem__c; 
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';
        phi.Disposition_Date__c = Date.today();    
        phi.Case_Folder_Id__c = '123456';
        phi.Folder_Id__c = '123456';
        phi.Server_Location__c = 'https://varian.app.box.com/folder/123456';
        phi.BoxFolderName__c = 'Testing';
        phi.Collab_id__c = '791293';
        insert phi;
        
        Apexpages.StandardController strcon = new Apexpages.StandardController(phi);
        PHIComplaintNumberUpdateController con = new PHIComplaintNumberUpdateController(strcon);
        con.updateIsEditable();
        ApexPages.currentPage().getParameters().put('complaintNo', '123456');
        con.save();
    }
}
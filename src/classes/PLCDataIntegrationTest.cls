@isTest(SeeAllData=true)
private class PLCDataIntegrationTest {
    
    static testMethod void InsertPLCData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new PLCMock()); 

            Product2 p = new Product2();
            p.Name = 'TestPLC';
            insert p;
            
            PLC_Data__c plc = new PLC_Data__c();
            plc.Start_Date__c = system.today();
            plc.End_Date__c = system.today();
            plc.Product__c = p.id;
            plc.Product_Replacement_Part__c = p.id;
            plc.Region__c = 'NA';
            insert plc; 
            
      
        Test.Stoptest();
    } 
    static testMethod void UpdatePLCData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new PLCMock()); 

            Product2 p = new Product2();
            p.Name = 'TestPLC';
            insert p;
            
            PLC_Data__c plc = new PLC_Data__c();
            plc.Start_Date__c = system.today();
            plc.End_Date__c = system.today();
            plc.Product__c = p.id;
            plc.Product_Replacement_Part__c = p.id;
            plc.Region__c = 'NA';
            insert plc; 

            PLC_Data__c plcobj = [select Id,Region__c from PLC_Data__c where Id =: plc.Id];
            plcObj.Region__c = 'AUS';
            update plcobj;
            
      
        Test.Stoptest();
    } 
  
    static testMethod void DeletePLCData(){
        Test.StartTest();
            Test.setMock(HTTpCalloutMock.class,new PLCMock()); 

            Product2 p = new Product2();
            p.Name = 'TestPLC';
            insert p;
            
            PLC_Data__c plc = new PLC_Data__c();
            plc.Start_Date__c = system.today();
            plc.End_Date__c = system.today();
            plc.Product__c = p.id;
            plc.Product_Replacement_Part__c = p.id;
            plc.Region__c = 'WW';
            insert plc; 
            
            delete plc;
            
      
        Test.Stoptest();
    }
    
    public class PLCMock implements HTTpCalloutMock{
        
        public HTTPResponse respond(HTTPRequest req) {
            String bmSessionID = '12345';
            String response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
            HTTPResponse res=new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setBody(response);            
            res.setStatusCode(200);
            return res;            
        }
    }
}
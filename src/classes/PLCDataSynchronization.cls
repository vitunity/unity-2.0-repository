/**************************************************************************\
@Created Date- 07/18/2017
@Created By - Rakesh Basani   
@Description - INC4202831 - Helper class for insert/update/delete PLC data to BigMachines. 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
public class PLCDataSynchronization{
    @future(callout=true)
    /**************************************************************************\
    @Description - INC4202831 - Add new PLC data in BigMachine.
    **************************************************************************/
    public static void Synchronization(set<Id> setPLCData){
        List<PLC_Data__c> lstPLCData = new List<PLC_Data__c>();
        for(PLC_Data__c p:[select id ,Name,BMId__c,End_Date__c,Message__c,Product__r.BigMachines__Part_Number__c,Country_Code__c,
        Product_Replacement_Part__r.BigMachines__Part_Number__c,Region__c,Region_code__c ,Start_Date__c from PLC_Data__c  where Id IN: setPLCData]){ 
            lstPLCData.add(p);
        }
        if(lstPLCData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;         
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = PLCDataIntegrationUtility.getAddPLCRequest(sessionID,lstPLCData);
            String responseAddPLC = PLCDataIntegrationUtility.makeRequest(PLDReq,Endpoint );
            
            if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                //deploy PLC_Data
                String deployReq = PLCDataIntegrationUtility.getDeployRequest(location,sessionID);
                String responseDeployPLC = PLCDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
            }
        } 
    }
    /**************************************************************************\
    @Description - INC4202831 - Update PLC data in BigMachine.
    **************************************************************************/
    @future(callout=true)
    public static void UpdatePLCData(set<Id> setPLCIds){
        List<PLC_Data__c> lstPLCData = new List<PLC_Data__c>();
        set<string> setPLCData = new set<string>();
        for(PLC_Data__c p:[select id ,Name,BMId__c,End_Date__c,Message__c,Product__r.BigMachines__Part_Number__c,Country_Code__c,
        Product_Replacement_Part__r.BigMachines__Part_Number__c,Region__c,Region_code__c ,Start_Date__c from PLC_Data__c  where Id IN: setPLCIds]){ 
            lstPLCData.add(p);
            setPLCData.add(p.Name);
        }
        if(lstPLCData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;         
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = PLCDataIntegrationUtility.getAddPLCRequest(sessionID,lstPLCData);
            
            
            String PLCDeleteReq = PLCDataIntegrationUtility.getDeletePLCRequest(sessionID,setPLCData,location);
            String responseDeletePLC = PLCDataIntegrationUtility.makeRequest(PLCDeleteReq,Endpoint );
            
            if(!String.isBlank(responseDeletePLC) && !responseDeletePLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                String responseAddPLC = PLCDataIntegrationUtility.makeRequest(PLDReq,Endpoint );            
                if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                    //If fail then do something
                }else{
                    //deploy PLC_Data
                    String deployReq = PLCDataIntegrationUtility.getDeployRequest(location,sessionID);
                    String responseDeployPLC = PLCDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
                }
            }
        } 
    }
    /**************************************************************************\
    @Description - INC4202831 - Delete PLC data in BigMachine.
    **************************************************************************/
    @future(callout=true)
    public static void DeletePLCData(set<string> setPLCData){
        if(setPLCData.size()>0){   
            BigMachinesInfo__c plcCs = BigMachinesInfo__c.getValues('PLC Connection Detail');
            String username = plcCs.Username__c;
            String password = plcCs.Password__c;
            String location = plcCs.Schema_Location__c;
            String Endpoint = plcCs.Endpoint__c;        
            string sessionId = getSessionId(username, password, location, Endpoint);
            String PLDReq = PLCDataIntegrationUtility.getDeletePLCRequest(sessionID,setPLCData,location);
            String responseAddPLC = PLCDataIntegrationUtility.makeRequest(PLDReq,Endpoint );
            
            if(!String.isBlank(responseAddPLC) && !responseAddPLC.contains('<bm:success>')){
                //If fail then do something
            }else{
                //deploy PLC_Data
                String deployReq = PLCDataIntegrationUtility.getDeployRequest(location,sessionID);
                String responseDeployPLC = PLCDataIntegrationUtility.makeRequest(deployReq ,Endpoint );
            }
        } 
    }
    /**************************************************************************\
    @Description - INC4202831 - Get session ID of BigMachines.
    **************************************************************************/
    public static string getSessionId(string username, string password, string location, string Endpoint){
        String loginXML = PLCDataIntegrationUtility.getLoginRequest(username,password,location);
        String response = PLCDataIntegrationUtility.makeRequest(loginXML,Endpoint);    
        String sessionID = PLCDataIntegrationUtility.getSessionId(response,'<bm:sessionId>','</bm:sessionId>');
        system.debug('sessionID:'+sessionID);
        return sessionID;
    }
}
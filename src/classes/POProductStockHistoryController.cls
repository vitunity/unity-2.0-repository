/* 
 * @Author -  Amitkuamr Katre
 * RMA Parts order product stock history only
 */
public with sharing class POProductStockHistoryController {
    
    list<SVMXC__Stock_History__c> stockHistoryList;
    map<Id,SVMXC__RMA_Shipment_Line__c> partsOrderLineMap;
    map<String, SVMXC__Product_Stock__c> psMap;
    list<SVMXC__Product_Serial__c> serialProductList; 
    Set<Id> locationIds = new Set<Id>();   
    Set<Id> productIds = new Set<Id>(); 
    
    //Constructor
    public POProductStockHistoryController(){
        stockHistoryList = new list<SVMXC__Stock_History__c> ();
        partsOrderLineMap = new map<Id,SVMXC__RMA_Shipment_Line__c> ();
        psMap = new map<String, SVMXC__Product_Stock__c> ();
        serialProductList = new list<SVMXC__Product_Serial__c> () ;
    }
    
    map<String,String> serialMapForOutTransite = new map<String,String>();
    
    //Create Product Stock History Parts Order
    public void createProductStockHistory(list<SVMXC__RMA_Shipment_Order__c> partsOrderList){
        partsOrderLineMap = ProductStockHistoryUtils.GetpartsOrderLineMap(partsOrderList);
        if(!partsOrderLineMap.isEmpty()){
            for(SVMXC__RMA_Shipment_Line__c pol : partsOrderLineMap.values()) {
                locationIds.add(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c);
                productIds.add(pol.SVMXC__Product__c);
            }
            // Get Product Stock Map
            if(!locationIds.isEmpty() && !productIds.isEmpty() ){
                psMap = ProductStockHistoryUtils.GetPsMap(productIds,locationIds, new set<Id> ());
                for(SVMXC__RMA_Shipment_Line__c pol : partsOrderLineMap.values()){
                    if(pol.Part__c != null){                    //decrement in-transit
                        //inc and dec available
                        processDecrementAvailable(pol);
                        //inc out-transit
                        processIncrementOutTransit(pol);
                        //update produtc serial active to false
                    }else{
                        processDecrementInTransit(pol);
                        //inc and dec available
                        processIncrementAvailable(pol);
                        processDecrementAvailable(pol);
                        //inc out-transit
                        processIncrementOutTransit(pol);
                        //update produtc serial active to false
                    }
                    /*
                    if(String.isNotBlank(pol.ERP_Serial_Number__c)){
                        checkActiveFlagToFalse(pol);
                    }
                    */
                }
                //insert stock history
                insertStockHistory();
            }
        }
    }
    
    /*
     * update product serial active to false
     */
    public void checkActiveFlagToFalse(SVMXC__RMA_Shipment_Line__c pol){
        for(SVMXC__Product_Serial__c pser : pol.Stocked_Serials__r){
            pser.SVMXC__Active__c = false;
            serialProductList.add(pser);
        }
    } 
    
    /*
     * Stock History Bulk insert and update
     */
    public void insertStockHistory(){
        if(!psMap.isEmpty()){
            upsert psMap.values();
        }
        if(!stockHistoryList.isEmpty()){
            for(SVMXC__Stock_History__c varSH : stockHistoryList){
                if(varSH.SVMXC__Product_Stock__r != null){
                    varSH.SVMXC__Product_Stock__c = varSH.SVMXC__Product_Stock__r.ID;
                }   
            }
            upsert stockHistoryList;
        }
        
        if(!serialProductList.isEmpty()){
            for(SVMXC__Product_Serial__c pser : serialProductList){
                pser.SVMXC__Product_Stock__c = pser.SVMXC__Product_Stock__r.Id;
            }
            update serialProductList;
        }
    }
    
    //decrement in-transit
    private void processDecrementInTransit(SVMXC__RMA_Shipment_Line__c pol){
        String inTransitKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                                       String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                                       '_In Transit';
        if(String.isNotBlank(pol.Batch_Number__c)){
            inTransitKey = inTransitKey+'_'+pol.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(inTransitKey);
        if(ps == null){
            ps = ProductStockHistoryUtils.CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,
                                                        pol.SVMXC__Product__c,
                                                        'In Transit',
                                                        pol.SVMXC__Expected_Quantity2__c,
                                                        pol.Batch_Number__c);
        }
        SVMXC__Stock_History__c sh = ProductStockHistoryUtils.CreatePSHistory('RMA Receipt','In Transit','Decrease',pol, ps.SVMXC__Quantity2__c,
                                                        ps.SVMXC__Quantity2__c - pol.SVMXC__Expected_Quantity2__c,
                                                        pol.SVMXC__Expected_Quantity2__c,
                                                        ps, pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c);
        stockHistoryList.add(sh);
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c - pol.SVMXC__Expected_Quantity2__c;                                            
        psMap.put(inTransitKey,ps);
    }
    
    //inc and dec available
    private void processIncrementAvailable(SVMXC__RMA_Shipment_Line__c pol){
        String availableKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                                       String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                                       '_Available';
        if(String.isNotBlank(pol.Batch_Number__c)){
            availableKey = availableKey+'_'+pol.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(availableKey);
        if(ps== null){
            ps = ProductStockHistoryUtils.CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,
                                                        pol.SVMXC__Product__c,
                                                        'Available',
                                                        0, pol.Batch_Number__c);
        }
        SVMXC__Stock_History__c sh = ProductStockHistoryUtils.CreatePSHistory('RMA Receipt','Available','Increment',pol, ps.SVMXC__Quantity2__c,
                                                        ps.SVMXC__Quantity2__c + pol.SVMXC__Expected_Quantity2__c,
                                                        pol.SVMXC__Expected_Quantity2__c,
                                                        ps, pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c);
        stockHistoryList.add(sh);
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c + pol.SVMXC__Expected_Quantity2__c;                                            
        psMap.put(availableKey,ps);
    }
    
    //inc and dec available
    private void processDecrementAvailable(SVMXC__RMA_Shipment_Line__c pol){
        String availableKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                                       String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                                       '_Available';
        if(String.isNotBlank(pol.Batch_Number__c)){
            availableKey = availableKey+'_'+pol.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(availableKey);
        if(ps== null){
            ps = ProductStockHistoryUtils.CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,
                                                        pol.SVMXC__Product__c,
                                                        'Available',
                                                        0,pol.Batch_Number__c);
        }
        SVMXC__Stock_History__c sh = ProductStockHistoryUtils.CreatePSHistory('RMA Receipt','Available','Increment',pol, ps.SVMXC__Quantity2__c,
                                                        ps.SVMXC__Quantity2__c - pol.SVMXC__Expected_Quantity2__c,
                                                        pol.SVMXC__Expected_Quantity2__c,
                                                        ps, pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c);
        stockHistoryList.add(sh);
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c - pol.SVMXC__Expected_Quantity2__c;                                            
        psMap.put(availableKey,ps);
    }
    
    //inc out-transit
    private void processIncrementOutTransit(SVMXC__RMA_Shipment_Line__c pol){
        String outTransitKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                                           String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                                           '_Out Transit'; 
        if(String.isNotBlank(pol.Batch_Number__c)){
            outTransitKey = outTransitKey+'_'+pol.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(outTransitKey);
        if(ps== null){
            ps = ProductStockHistoryUtils.CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,
                                                        pol.SVMXC__Product__c,
                                                        'Out Transit',
                                                        0,pol.Batch_Number__c);
        }
        SVMXC__Stock_History__c sh = ProductStockHistoryUtils.CreatePSHistory('RMA Receipt','Out Transit','Increment',pol, ps.SVMXC__Quantity2__c,
                                                        ps.SVMXC__Quantity2__c + pol.SVMXC__Expected_Quantity2__c,
                                                        pol.SVMXC__Expected_Quantity2__c,
                                                        ps, pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c);
        stockHistoryList.add(sh);
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c + pol.SVMXC__Expected_Quantity2__c;                                            
        psMap.put(outTransitKey,ps);
        //Move stocked serial to out transite
        if(String.isNotBlank(pol.ERP_Serial_Number__c) && pol.Part__c == null){
            String outInTransitKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                   String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                   '_In Transit';
      if(String.isNotBlank(pol.Batch_Number__c)){
                outInTransitKey = outInTransitKey+'_'+pol.Batch_Number__c;
            }
            if(psMap.containsKey(outInTransitKey))
      for(SVMXC__Product_Serial__c pser : psMap.get(outInTransitKey).SVMXC__Product_Serial__r){
                if(pser.Name == pol.ERP_Serial_Number__c){
                    pser.SVMXC__Product_Stock__r = ps;
                  serialProductList.add(pser);
                }
            }
        }
    // Move product stocked serial form availabel to out transite.
    if(String.isNotBlank(pol.ERP_Serial_Number__c) && pol.Part__c != null){
            String outAvailableKey = String.valueOf(pol.SVMXC__Product__c) +'_'+
                   String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c)+
                   '_Available';
      if(String.isNotBlank(pol.Batch_Number__c)){
                outAvailableKey = outAvailableKey+'_'+pol.Batch_Number__c;
            }
            if(psMap.containsKey(outAvailableKey))
      for(SVMXC__Product_Serial__c pser : psMap.get(outAvailableKey).SVMXC__Product_Serial__r){
                if(pser.Name == pol.ERP_Serial_Number__c){
                    pser.SVMXC__Product_Stock__r = ps;
                  serialProductList.add(pser);
                }
            }
        }
    
    }
}
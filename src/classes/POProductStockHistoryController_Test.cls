@isTest(seealldata=true)
public class POProductStockHistoryController_Test{
    
  private static testMethod void unitTest1() {
        Country__c country = testUtils.createCountry('United States');
        country.Alternate_Name__c = 'US';
        country.Parts_Order_Approval_Level__c  = 10;
        insert country;
        
        Country__c country2 = testUtils.createCountry('India');
        country.Alternate_Name__c = 'IN';
        country.Parts_Order_Approval_Level__c  = 10;
        insert country2;
        
        Country_Country_Rule__c cr = new Country_Country_Rule__c();
        cr.To_Country__c = country.id;
        cr.From_Country__c = country2.id;
        cr.Commerical_Invoice_Required__c = true;
        cr.Export_License_Required__c = true;
        insert cr;
        
        Product2 product = new Product2(Name='Connect Utilities', SVMXC__Product_Cost__c  = 20);
        product.PSE_Approval_Queue__c = 'PSE Parts Approvals - TrueBeam - Mech';
        product.PSE_Approval_Required__c = true;
        insert product;
        
        Account account = testUtils.createAccount('Test', 'AUSSA', country.Name, country.Id);
      	account.BillingCity = 'Pune';
        insert account;
        
        Case cs = testUtils.createCase('Test subject');
      	cs.Priority = 'Low';
        insert cs;
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(UserInfo.getUserId());
        serviceGroup.Work_Order_BST_Queue__c = 'BST';
        insert serviceGroup;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=UserInfo.getUserId(), ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        ERP_Org__c eorg = new ERP_Org__c();
        eorg.name = 'testorg';
        eorg.Sales_Org__c = 'testorg';
        eorg.Bonded_Warehouse_used__c = true;
        insert eorg; 
        
        SVMXC__Site__c location = new SVMXC__Site__c(Sales_Org__c = 'testorg', 
        SVMXC__Service_Engineer__c = userInfo.getUserId(), 
        SVMXC__Location_Type__c = 'Field', 
        Plant__c = 'dfgh', 
        SVMXC__Account__c = account.id,
        ERP_Org__c = eorg.Id
        );
        
        location.Plant__c = 'testorg';
        location.Facility_Code__c = 'facilitycode1234';
        location.Country__c =country.id;
        insert location;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Name = '21ST CENTURY ONCOLOGY LLC...';
        erp.State_Province_Code__c = 'TET';
        erp.Country_Code__c = 'dd';
        insert erp; 
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c(
        SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Company__c = account.id,
        SVMXC__Case__c = cs.id,ERP_Service_Order__c = '12345678');
        serviceOrder.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        serviceOrder.Malfunction_Start__c = system.now().addDays(10);
        serviceOrder.SVMXC__Order_Status__c = 'Open';
        serviceOrder.Depot_Location__c = location.Id;
        serviceOrder.Inventory_Location__c = location.Id;
        serviceOrder.Equipment_Location__c = location.Id;    
        serviceOrder.Service_Team__c  =  serviceGroup.id; 
        insert serviceOrder;
        
        System.Test.startTest();
        
        SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(
        SVMXC__Company__c = account.id, 
        SVMXC__Case__c = cs.Id, 
        Cancel__c = false );
        po.To_Country__c = country.Id;
        po.From_Country__c = country2.Id;
        po.SVMXC__Destination_Location__c = location.id;
        po.SVMXC__Source_Location__c = location.id;
        po.SVMXC__Source_Country__c = 'India';
        po.SVMXC__Destination_Country__c  ='United States';
        po.ERP_Sold_To__c = account.id;
        po.ERP_Ship_To__c = erp.id;
        po.ERP_Ship_To_Code__c = 'TEST'; 
        po.One_time__c = true;
        po.New_Work_Order__c = serviceOrder.Id;
        po.SVMXC__Service_Order__c = serviceOrder.Id;
        po.ERP_Result_Status__c = 'failure';
        po.Transfer_to_FE__c = technician.Id;
        po.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        insert po;
        
        
        SVMXC__RMA_Shipment_Line__c poline = new SVMXC__RMA_Shipment_Line__c(
        RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
        SVMXC__RMA_Shipment_Order__c = po.Id,
        SVMXC__Actual_Quantity2__c = 5,
        SVMXC__Expected_Quantity2__c = 5,
        Line_Cost__c = 100,
        SVMXC__Fulfillment_Qty__c =  5,
        Remaining_Qty__c = 5,
        SVMXC__Line_Status__c = 'Open',
        SVMXC__Line_Type__c = 'Shipment',
        Consumed__c = false,
        Returned__c = false,
        SVMXC__Ship_Location__c = location.Id,
        ERP_Serial_Number__c = '8521469',
        SVMXC__Product__c = product.Id,
        SVMXC__Service_Order__c = serviceOrder.Id,
        PSE_Approval_Required__c = true
        );   
                 
        insert poline;
        
        SVMXC__RMA_Shipment_Order__c por = new SVMXC__RMA_Shipment_Order__c(
        SVMXC__Company__c = account.id, 
        SVMXC__Case__c = cs.Id, 
        Cancel__c = false );
        por.To_Country__c = country.Id;
        por.From_Country__c = country2.Id;
        por.SVMXC__Destination_Location__c = location.id;
        por.SVMXC__Source_Location__c = location.id;
        por.SVMXC__Source_Country__c = 'India';
        por.SVMXC__Destination_Country__c  ='United States';
        por.ERP_Sold_To__c = account.id;
        por.ERP_Ship_To__c = erp.id;
        por.ERP_Ship_To_Code__c = 'TEST'; 
        por.One_time__c = true;
        por.New_Work_Order__c = serviceOrder.Id;
        por.SVMXC__Service_Order__c = serviceOrder.Id;
        por.ERP_Result_Status__c = 'failure';
        por.Transfer_to_FE__c = technician.Id;
        por.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
        por.District_Manager__c = UserInfo.getUserId();
        insert por;
        
        SVMXC__RMA_Shipment_Line__c poliner = new SVMXC__RMA_Shipment_Line__c(
        RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId(),
        SVMXC__RMA_Shipment_Order__c = por.Id,
        SVMXC__Actual_Quantity2__c = 5,
        SVMXC__Expected_Quantity2__c = 5,
        Line_Cost__c = 100,
        SVMXC__Fulfillment_Qty__c =  5,
        Remaining_Qty__c = 5,
        SVMXC__Line_Status__c = 'Open',
        SVMXC__Line_Type__c = 'Shipment',
        Consumed__c = false,
        Returned__c = false,
        SVMXC__Ship_Location__c = location.Id,
        ERP_Serial_Number__c = '8521469',
        SVMXC__Product__c = product.Id,
        SVMXC__Service_Order__c = serviceOrder.Id,
        PSE_Approval_Required__c = true,
        SVMXC__Master_Order_Line__c = poline.id,
        Interface_Status__c = 'Processed'
        );
        
        insert poliner;
        
        por.SVMXC__Order_Status__c = 'Approved';
        por.RMA_Number__c = '45323525';
        por.SVMXC__Order_Type__c = 'Transfer';
        update por;
        ProductStockHistoryUtils.GetpartsOrderLineMap(new set<Id> {poliner.Id});
        ProductStockHistoryUtils.CreatePSHistory('transactionType', 'status', 'changeType', 
                                                 new SVMXC__Service_Order_Line__c() 
                                                 , 12, 12, 12, new SVMXC__Product_Stock__c()) ;       
        System.Test.stopTest();
        
    }

}
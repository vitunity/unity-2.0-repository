@RestResource(urlMapping='/VMarketApI') 
global with sharing class POSTHandler { 
   
    global POSTHandler(){
    system.debug('++++'+RestContext.request);} 
    
     @HttpGet
    global static String doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('*******'+req);

        return 'All Good';
    }
    
     @HttpPost
    global static String doPost() {
    RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('*******headers'+req.headers);
        
        Map<String,String> reqmap = req.headers;
        for(String s :reqmap.keyset())
        {
        System.debug('-----'+s+'----'+reqmap.get(s));
        }

        if(!reqmap.keyset().contains('Stripe-Signature')) return 'You are Not Authroized';

        Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(req.requestBody.toString());
        
        Map<String, Object> cObjMap1 = (Map<String, Object>) cObjMap.get('data');
        
        
        
        Map<String, Object> cObjMap2 = (Map<String, Object>) cObjMap1.get('object');
        String status = String.valueof(cObjMap2.get('paid'));
        String chargeid = String.valueof(cObjMap2.get('id'));
        
        String webStatus = '';
        if(status.equals('true')) webStatus = 'Success';
        else webStatus = 'Failed';
        
        VMarket_WebHooks_Order__c weborder = new VMarket_WebHooks_Order__c(VMarket_Charge_Id__c=chargeid, VMarket_Full_Response__c =req.requestBody.toString(), VMarket_Processed__c=false, VMarket_Received_Date__c=System.Now(), VMarket_Subscription__c=false, VMarket_Status__c=webStatus);        
        insert weborder;
        
        
        
       /* 
        List< vMarketOrderItem__c > orderItems = [Select id, Status__c, Charge_Id__c from vMarketOrderItem__c where Charge_Id__c=:chargeid and Status__c='Pending']; 
        System.debug('&&&'+orderItems.size());
        if( orderItems.size() > 0 )
        {
        if(status.equals('true')) orderItems[0].Status__c = 'Success';
        else 
        {
        orderItems[0].Status__c = 'Failed';
        orderItems[0].VMarket_ACH_Failure_Reason__c = req.requestBody.toString();
        }
        
        update orderItems[0];
         }
         */
        
        return 'All Good';
    }
    
  
}
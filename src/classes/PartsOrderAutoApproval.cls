//US4354,US4914, US4164 - to automate approval process for Parts Order
public without sharing class PartsOrderAutoApproval {
    
    private static Id recordTypeIdRMA = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
    
    
    public static boolean partsOrderApprovalFlag = true;
    public static boolean partsOrderApprovedFlag = true;
    public static boolean partsOrderRejectedFlag = true;
     
    public PartsOrderAutoApproval() {
        
    }

    // Get Approval work items
    private static Map<Id, ProcessInstanceWorkitem> getMapOfObjIdAndWoItem (set<Id> setOfPo_Id ){
        Map<Id, ProcessInstanceWorkitem> MapOfObjIdAndWoItemLocal = new Map<Id, ProcessInstanceWorkitem>();
        for(ProcessInstanceWorkitem  workitem : [select Id, ProcessInstance.TargetObjectId 
                                                 from ProcessInstanceWorkitem 
                                                 where ProcessInstance.TargetObjectId in : setOfPo_Id])
        {
                System.debug('object id--->'+workitem.ProcessInstance.TargetObjectId);
                MapOfObjIdAndWoItemLocal.put(workitem.ProcessInstance.TargetObjectId, workitem);
        }
        return MapOfObjIdAndWoItemLocal;
    }

    // Submit approval process
    public static void execute(List<SVMXC__RMA_Shipment_Order__c> lstOfPO, set<Id> setOfPoId ){
        //priit, US4354,US4914, US4164 - to automate approval process for Parts Order
        system.debug('lstOfPO ================== ' + lstOfPO);

        List<SVMXC__RMA_Shipment_Order__c> lstOfPOTest = [select id,SVMXC__Order_Status__c,
                                                          District_Manager_Override__c, Time_Limit_Expired__c,
                                                          PSE_Approval_Required__c from SVMXC__RMA_Shipment_Order__c
                                                          where id in : lstOfPO] ;

        system.debug('lstOfPOTest ================== ' + lstOfPOTest);                                                  

        Map<Id, ProcessInstanceWorkitem> MapOfObjIdAndWoItem = getMapOfObjIdAndWoItem(setOfPoId);
        set<id> poIds = new set<id>();
        for(SVMXC__RMA_Shipment_Order__c po :  lstOfPO) {
            //priti, US4309, 10/10/2014 - to automate approval process when RMA Parts Order is submitted, the apporval process sets the Orde Status to approved and interface status to process
            System.debug('pse aaproval-->'+po.SVMXC__Order_Status__c);
            if( po.SVMXC__Order_Status__c == 'Submitted' && po.RecordTypeId == recordTypeIdRMA ){
                system.debug('pete> Submitted 1');
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval.');
                approvalRequest.setObjectId(po.id);               
                Approval.ProcessResult result = Approval.process(approvalRequest);
                system.debug('pete> Submitted 1 approval result: ' + result);
                if(!result.isSuccess()){
                    po.addError('Approval Request failed: '+result.getErrors()[0]);
                }else{
                    poIds.add(po.id);
                }
            }

            //if DM overrides then set Parts Oder Status to approve via approval process
            if(MapOfObjIdAndWoItem.containskey(po.id) 
                && MapOfObjIdAndWoItem.get(po.id) != null 
                && po.SVMXC__Order_Status__c == 'PSE Approval Pending' 
                && po.District_Manager_Override__c == true && po.RecordTypeId != recordTypeIdRMA){
                system.debug('pete> PSE Approval Pending');
                
                //Recall pending approval
                Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();      
                pwr.setWorkitemId(MapOfObjIdAndWoItem.get(po.id).id);
                pwr.setComments('DM Overridden & Approved');
                pwr.setAction('Removed');              
                Approval.ProcessResult pr = Approval.process(pwr);
                pr.getErrors();                

                //Create an approval request 
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval.');
                approvalRequest.setObjectId(po.id);

                // Submit the approval request
                Approval.ProcessResult result = Approval.process(approvalRequest);

                system.debug('pete> PSE Approval Pending approval result: ' + result);

                if (!result.isSuccess()){
                    po.addError('Approval Request failed: '+result.getErrors()[0]);
                }else{
                    poIds.add(po.id);
                }
            }

            //if pending for DM and Time Limit expired then recall aproval from DM and send to Regional manger for approval
            else if (MapOfObjIdAndWoItem.containskey(po.id) 
                && MapOfObjIdAndWoItem.get(po.id) != null
                 && po.Time_Limit_Expired__c == true 
                 && po.SVMXC__Order_Status__c == 'Pending Approval'  
                 && po.RecordTypeId != recordTypeIdRMA)
            {
                system.debug('pete> Pending Approval');
                //Recall pending approval
                Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();      
                pwr.setWorkitemId(MapOfObjIdAndWoItem.get(po.id).id);
                pwr.setComments('Time Limit For Approval Expired');
                pwr.setAction('Removed');              
                Approval.ProcessResult pr = Approval.process(pwr);
                pr.getErrors();
                //submitting to RM For approval
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval.');
                approvalRequest.setObjectId(po.id);
                // Submit the approval request
                Approval.ProcessResult result = Approval.process(approvalRequest);
                system.debug('pete> Pending Approval approval result: ' + result);
                if(!result.isSuccess()){
                    po.addError('Approval Request failed: '+result.getErrors()[0]);
                }else{
                    poIds.add(po.id);
                }
            }

            //else submit record for approval
            else if(po.SVMXC__Order_Status__c == 'Submitted' 
                && po.RecordTypeId != recordTypeIdRMA 
                && (po.PSE_Approval_Required__c >0 
                    || po.DM_Approval_Required__c == true 
                    || po.Partline_Exceeding_Total_Line_Price__c >0 
                    )
                )
            {
                //Create an approval request 
                system.debug('pete> Submitted 2');
                System.debug('pse aaproval-->');
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval.');
                approvalRequest.setObjectId(po.id);

                system.debug('pse approval po before-->' + 'SVMXC__Order_Status__c:' + po.SVMXC__Order_Status__c + ', RecordTypeId:' + po.RecordTypeId
                             + ', PSE_Approval_Required__c:' + po.PSE_Approval_Required__c + ', DM_Approval_Required__c:' + po.DM_Approval_Required__c
                             + ', Partline_Exceeding_Total_Line_Price__c:' + po.Partline_Exceeding_Total_Line_Price__c
                             + ', Amount_Over_Parts_Order_Limit__c:' + po.Amount_Over_Parts_Order_Limit__c + ', Submitted_Line_Count__c:' + po.Submitted_Line_Count__c);

                // Submit the approval request
                Approval.ProcessResult result = Approval.process(approvalRequest);
                system.debug('pete> Submitted 2 approval result: ' + result);
                if(!result.isSuccess()) {
                    po.addError('Approval Request failed: '+result.getErrors()[0]);
                }else{
                    poIds.add(po.id);
                }
            } 

            //if no apprvoal is required and Parts Order is submitted then set Parts Order status to approved via approval process
            if(po.SVMXC__Order_Status__c == 'Submitted' 
                && po.Time_Limit_Expired__c == false 
                && po.PSE_Approval_Required__c == 0 
                && po.District_Manager_Override__c == false 
                && po.DM_Approval_Required__c == false
                && po.RecordTypeId != recordTypeIdRMA)
            {   
                system.debug('pete> Submitted 3');
                //Create an approval request 
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval.');
                approvalRequest.setObjectId(po.id);
                // Submit the approval request
                Approval.ProcessResult result = Approval.process(approvalRequest);

                system.debug('pete> Submitted 3 approval result: ' + result);

                if(!result.isSuccess())
                {
                    po.addError('Approval Request failed: '+result.getErrors()[0]);                
                }else{
                    poIds.add(po.id);
                }
            }
        }
        if(!poIds.isEmpty()){
            updatePOLIStatusToSubmited(poIds);
        }
    } 


    // update parts order line item status to submited.
    private static void updatePOLIStatusToSubmited(set<Id> po_ids){
        list<SVMXC__RMA_Shipment_Line__c> polLIst  = [select id from SVMXC__RMA_Shipment_Line__c 
                                                where SVMXC__RMA_Shipment_Order__c in : po_ids
                                                and SVMXC__RMA_Shipment_Order__r.SVMXC__Order_Status__c = 'Submitted'];
        for(SVMXC__RMA_Shipment_Line__c pol : polLIst){
            pol.SVMXC__Line_Status__c = 'Submitted';
        }
        if(!polLIst.isEmpty()){
            update polLIst; 
        }

    }

    // update parts order line item status to approved.
    public static void updatePOLIStatusToApproved(list<SVMXC__RMA_Shipment_Order__c> po_List, 
                                                  map<Id,SVMXC__RMA_Shipment_Order__c> po_Old_Map ){

        system.debug('Anydatatype_msg==po_List=='+po_List);
        list<SVMXC__RMA_Shipment_Order__c> po_List_Q = [select Id, SVMXC__Order_Status__c
                                                        from SVMXC__RMA_Shipment_Order__c
                                                        where id in : po_List];
        system.debug('Anydatatype_msg==po_List_Q=='+po_List_Q);
        system.debug('Anydatatype_msg==po_Old_Map=='+po_Old_Map);           
                                            
                                            
        set<Id> po_ids = new set<Id>();
        for(SVMXC__RMA_Shipment_Order__c po : po_List_Q){
            if(po.SVMXC__Order_Status__c == 'Approved' 
                && po_Old_Map != null && po.SVMXC__Order_Status__c != po_Old_Map.get(po.Id).SVMXC__Order_Status__c){
                po_ids.add(po.Id);
            }
        }
        list<SVMXC__RMA_Shipment_Line__c> polLIst  = [select id from SVMXC__RMA_Shipment_Line__c 
                                                     where SVMXC__RMA_Shipment_Order__c in : po_ids];
        for(SVMXC__RMA_Shipment_Line__c pol : polLIst){
            pol.SVMXC__Line_Status__c = 'Approved';
            pol.Interface_Status__c= 'Process';
        }
        if(!polLIst.isEmpty()){
            update polLIst; 
        }

    }


    // update parts order line item status to approved.
    public static void updatePOLIStatusToRejected(list<SVMXC__RMA_Shipment_Order__c> po_List, 
                                                  map<Id,SVMXC__RMA_Shipment_Order__c> po_Old_Map ){

        system.debug('Anydatatype_msg==po_List=='+po_List);
        list<SVMXC__RMA_Shipment_Order__c> po_List_Q = [select Id, SVMXC__Order_Status__c
                                                        from SVMXC__RMA_Shipment_Order__c
                                                        where id in : po_List];
        system.debug('Anydatatype_msg==po_List_Q=='+po_List_Q);
        system.debug('Anydatatype_msg==po_Old_Map=='+po_Old_Map);           
                                            
                                            
        set<Id> po_ids = new set<Id>();
        for(SVMXC__RMA_Shipment_Order__c po : po_List_Q){
            if(po.SVMXC__Order_Status__c == 'Rejected' 
                && po_Old_Map != null && po.SVMXC__Order_Status__c != po_Old_Map.get(po.Id).SVMXC__Order_Status__c){
                po_ids.add(po.Id);
            }
        }
        list<SVMXC__RMA_Shipment_Line__c> polLIst  = [select id from SVMXC__RMA_Shipment_Line__c 
                                                     where SVMXC__RMA_Shipment_Order__c in : po_ids];
        for(SVMXC__RMA_Shipment_Line__c pol : polLIst){
            pol.SVMXC__Line_Status__c = 'Rejected';
            pol.Interface_Status__c= '';
        }
        if(!polLIst.isEmpty()){
            update polLIst; 
        }

    }


}
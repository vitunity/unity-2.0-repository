public class PickListValueController{
    public  PickListValueController(){}
    public static List<String> GetOptions(String pObjName, String pControllingFieldName){
        List<string> lstoptions = new list<string>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        //get the type being dealt with
        Schema.SObjectType pType = objGlobalMap.get(pObjName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        //get the control values   
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){            
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            lstoptions.add(ctrl_entry.getLabel());
        }
        return lstoptions;
    }
}
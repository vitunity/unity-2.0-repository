/**
 * @Author		:		Puneet Mishra
 * @Date		:		6 Sept 2017
 * PLEASE CONNECT WITH ME BEFORE DELETING/MODIFYING THE CLASS
 */
public class PicklistEntryWrapper {
	
    public String active {get;set;}
    public String defaultValue {get;set;}
    public String label {get;set;}
    public String value {get;set;}
    public String validFor {get;set;}
    public PicklistEntryWrapper(){}
    
}
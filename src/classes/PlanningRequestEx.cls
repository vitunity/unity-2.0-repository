public with sharing class PlanningRequestEx{

public Task task {get;set;}
Opportunity currentRecord;

public PlanningRequestEx(ApexPages.StandardController controller) 
{
USER r = [SELECT ID FROM User WHERE Firstname = 'Planning' limit 1];
task = new Task();
currentRecord = [SELECT Id, ownerid FROM Opportunity WHERE Id = :ApexPages.currentPage().getParameters().get('oppid')];
task.Whatid = currentRecord.Id;
task.Ownerid = r.id;
USER oemail = [select Email from User where id = :currentRecord.ownerid];
task.Opportunity_Owner_Email__c = oemail.email;
Task.Subject = 'Planning / Pre-Sales Request';
Date tday = system.today();
Task.ActivityDate = tday.addDays(7);

}

 public PageReference save()
  { 
  
  insert task; 
  PageReference acctPage = new ApexPages.StandardController(task).view();
  acctPage.setRedirect(true);
  return acctPage;
}
}
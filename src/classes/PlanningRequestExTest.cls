@isTest private class PlanningRequestExTest {
   @isTest static void AllInputValuesRunAs() {
   
   //USER usr = new USER();
   //usr.FirstNAme = 'Planning';
  // usr.LastName = 'User';
  // usr.Email = 'planninguser@varian.com';
  // insert usr;
  // USER r = [SELECT ID FROM User WHERE Firstname = 'Planning' limit 1];
  
  
  
// Setup test data 
// Create a unique UserName 

  /* String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testvarian.com';
   Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
   User u = new User(Alias = 'standt', Email='standarduser@testvarian.com',
   EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id,
       TimeZoneSidKey='America/Los_Angeles',UserName=uniqueUserName);
        System.runAs(u) 
        { 
        // The following code runs as user 'u'
        System.debug('Current User: ' + UserInfo.getUserName());
        System.debug('Current Profile: ' + UserInfo.getProfileId());
        }*/
  //insert Account
     // Account acc = new Account();
     // acc.Name = 'Medical Physics Services, LLC';
     // acc.Country__c ='USA';
      
     // insert acc;
  // insert opportunity
  /*    Opportunity opp = new Opportunity();
      opp.Name= 'Test KK';
     // opp.Account__c ='Medical Physics Services, LLC';
      opp.Account_Sector__c = 'Private';
      opp.Payer_Same_as_Customer__c = 'Yes';
      opp.Primary_Contact_Name__c = '003E000000iCH56IAG';
      opp.CurrencyIsoCode = 'USD';
      opp.Deliver_to_Country__c = 'USA';
      opp.Expected_Close_Date__c = system.today().addmonths(12);
      opp.CloseDate = system.today().addmonths(12);
      opp.StageName = '1 - QUALIFICATION (N/A)';
      opp.Existing_Equipment_Replacement__c = 'No';
    //  insert opp; */
  
   Task task = new Task();
    User u = [select id from user where firstname ='planning' limit 1];
   task.Ownerid = u.id;
   Opportunity oppownerid = [select id from opportunity where name ='test kk' limit 1];
   task.Whatid = oppownerid.id;
   //ID recid = apexpages.currentPage().getParameters().get('oppid');
   // USER  oemail = [select Email from User where id = :oppownerid.ownerid];
    task.Opportunity_Owner_Email__c = 'keerat.kaur@varian.com';
    task.Subject = 'Planning / Pre-Sales Request';
    Date tday = system.today();
    task.ActivityDate = tday.addDays(7);
    insert task;
    
    PageReference pref = Page.TestPlanningRequest;
    pref.getParameters().put('id', task.id);
    Test.setCurrentPage(pref);
    ApexPages.StandardController sc = new ApexPages.StandardController(task);
    PlanningRequestEx mc = new PlanningRequestEx(sc);
    PageReference result = mc.Save();
    System.assertNotEquals(null, result);

   }
   
}
@isTest(SeeAllData=true)
private class PlanningRequestEx_NEW_Tests {

static user testuser;   
static task testtask;

private static testMethod void myTest() {
   
    test.startTest(); 
   
    LoadData();
     
    PageReference testpage     = Page.TestPlanningRequest;
    Test.setCurrentPage(testpage);
    testpage.getParameters().put('id',testtask.id);
    ApexPages.StandardController sc = new ApexPages.standardController(testtask);
    PlanningRequestEx planreq  = new PlanningRequestEx(sc);
   
    test.stopTest();
    
}

static void LoadData(){
    createuser();
    createopp();
    createtask();
}

static void createuser(){
    testuser           = new user();
    testuser.FirstName = 'Planning';
    testuser.LastName  = 'User';
    testuser.Email     = 'planninguser@variantest.com';
    testuser.username  = 'planninguser@variantest.com.test';
    testuser.alias     = 'puser';
    testuser.CommunityNickname = 'testing';
    testuser.TimeZoneSidKey    = 'America/Chicago';
    testuser.LocaleSidKey      = 'en_US';
    testuser.EmailEncodingKey  = 'ISO-8859-1';
    testuser.ProfileId         = '00eE0000000mvyb';
    testuser.LanguageLocaleKey = 'en_US';
    
    insert testuser;

    //assert..........
    user tempuser = new user();
    tempuser      = [select id from user where id =:testuser.id];
    system.assertEquals(tempuser.id==testuser.id, true);
}


static void createtask(){
    testtask                 =  new task();
    testtask.ownerid         =  testuser.id;
    testtask.currencyIsoCode = 'USD';
    testtask.Service_Group__c= 'Software Planning';
    testtask.Status          = 'Not Started';
    testtask.priority        = 'Normal';

    insert testtask;

    //assert..........
    task temptask = new task();
    temptask      = [select id from task where id =:testtask.id];
    system.assertEquals(temptask.id == testtask.id, true);
}

static void createopp(){

    Account acc = new Account();
     acc.Name = 'testaccount';
     acc.Country__c = 'USA';
     acc.CurrencyIsoCode = 'USD';
     insert acc;

      Opportunity opp = new Opportunity();
      opp.Name= 'Testopp';
      opp.Account__c ='testaccount';
      opp.Account_Sector__c = 'Private';
      opp.Payer_Same_as_Customer__c = 'Yes';
      opp.Primary_Contact_Name__c = '003E000000iCH56IAG';
      opp.CurrencyIsoCode = 'USD';
      opp.Deliver_to_Country__c = 'USA';
      opp.Expected_Close_Date__c = system.today().addmonths(12);
      opp.CloseDate = system.today().addmonths(12);
      opp.StageName = '1 - QUALIFICATION (N/A)';
      opp.Existing_Equipment_Replacement__c = 'No';
      insert opp; 
      
      //assert..........
      Opportunity tempopp = new Opportunity();
      tempopp             = [select id from opportunity where id =:opp.id];
      system.assertEquals(tempopp.id == opp.id, true);
}
}
global class PopulateCaseonWorkOrder_Batch implements Database.Batchable<sobject> {
    
    private String SOQL {get;set;}
    private List<String> woId {get;set;}                                        
    
    public PopulateCaseonWorkOrder_Batch(String query, List<String> woId) {
        this.SOQL = query;
        this.woId = woId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
       return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Id caseRecId = RecordTypeHelperClass.CASE_RECORDTYPES.get('PM').getRecordTypeId();
        Map<String, SVMXC__Service_Order__c> wo_Map = new Map<String, SVMXC__Service_Order__c>();
        list<SVMXC__Service_Order__c> woList = (list<SVMXC__Service_Order__c>)scope;
        List<String> projectNbr_WO = new List<String>();
        List<case> caseToInsert = new List<case>();
        
        for(SVMXC__Service_Order__c wo : woList) {
            wo_Map.put(wo.ERP_Project_Nbr__c, wo);
            projectNbr_WO.add(wo.ERP_Project_Nbr__c);
            system.debug(' === Work Order === ' + wo + ' = Case = ' + wo.SVMXC__Case__c);
        }
        
        Map<String, Case> prj_No = new Map<String, Case>();
        for(Case c : [  SELECT Id, ERP_Project_Number__c, CaseNumber, Status, RecordType.DeveloperName 
                                                    FROM Case WHERE ERP_Project_Number__c IN: projectNbr_WO
                                                        AND Status !=: 'Closed by Work Order'
                                                        AND RecordType.DeveloperName = 'Project_Management']) {
            prj_No.put(c.ERP_Project_Number__c, c);                                             
        }
        
        List<SVMXC__Service_Order__c> restWO = new List<SVMXC__Service_Order__c>();
                
        for(String str : projectNbr_WO) {
            if(wo_Map.containsKey(str) && prj_No.containsKey(str)) {
                wo_Map.get(str).SVMXC__Case__c = prj_No.get(str).Id;
            } else {
                restWO.add(wo_Map.get(str));
            }
        }
        system.debug(' --- b4 wo_Map ==== ' + wo_Map);
        if(!restWO.isEmpty()) {
            for(SVMXC__Service_Order__c wo : restWO) {
                Case newCase = new Case();
                newCase.RecordTypeId = caseRecId;
                newCase.Reason = 'New Installation';
                //newCase.AccountId = ;
                newCase.Priority = 'Medium';
                newCase.Is_escalation_to_the_CLT_required__c = 'NO';
                newCase.Is_This_a_Complaint__c = 'NO';
                newCase.Was_anyone_injured__c = 'NO';
                newCase.SVMXC__Is_PM_Case__c = false;
                newCase.Local_Subject__c = 'New Installation';
                newCase.Description = 'New Installation';
                newCase.Status = 'New';
                
                newCase.ERP_Project_Number__c = wo.ERP_Project_Nbr__c;
                
                newCase.SVMXC__Preferred_End_Time__c = wo.ERP_WBS__r.Forecast_End_Date__c; 
                newCase.SVMXC__Preferred_Start_Time__c = wo.ERP_WBS__r.Forecast_Start_Date__c;
                newCase.SVMXC__Site__c = wo.ERP_WBS__r.Location__c;
                
                caseToInsert.add(newCase);
            }
        }
        
        if(!caseToInsert.isEmpty())
            insert caseToInsert;
        
        if(!caseToInsert.isEmpty()) {
            for(Case c : caseToInsert) {
                if(wo_Map.containsKey(c.ERP_Project_Number__c)) {
                    wo_Map.get(c.ERP_Project_Number__c).SVMXC__Case__c = c.Id;
                }
            }
        }
        
        for(SVMXC__Service_Order__c wo : wo_Map.values()) {
            system.debug(' === after === ' + wo + ' = Case = ' + wo.SVMXC__Case__c);
        }
        system.debug(' --- wo_Map ==== ' + wo_Map);
        //system.assert(false);
        update wo_Map.values();
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
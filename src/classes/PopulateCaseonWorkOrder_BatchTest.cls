@isTest
public with sharing class PopulateCaseonWorkOrder_BatchTest {
    
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static Sales_Order_Item__c PSOI;
    public static List<Sales_Order_Item__c> soiList;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Service_Level__c slaTerm;
    public static SVMXC__Service_Contract__c servcContrct;
    public static SVMXC__Service_Contract_Products__c contractProd;
    public static SVMXC__Counter_Details__c counter;
    public static Counter_Usage__c usage;
    
    static  {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
    
        TCprofile = UnityDataTestClass.TIMECARD_Data(true);
    
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
    
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        insert technician;
    
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
    
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        newAcc = UnityDataTestClass.AccountData(true);
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        PSOI = UnityDataTestClass.SOI_Data(false, SO);
        SOI = UnityDataTestClass.SOI_Data(false, SO);
        SOI.Parent__c = SOI.Id;
        soiList = new List<Sales_Order_Item__c>();
        soiList.add(SOI);
        soiList.add(PSOI);
        insert soiList;
    
        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        insert NWA;
        
        WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
        WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        WO.ERP_WBS__c = WBS.Id;
        WO.ERP_Service_Order__c = '123';
        insert WO;
                    
    }
    
    static testMethod void test() {
        String query;
        List<String> woId = new List<String>{WO.Id};
        test.startTest();
            query = 'SELECT Id, ERP_Project_Nbr__c, RecordTypeId,ERP_WBS__c, ' +
                    ' RecordType.DeveloperName, SVMXC__Case__c, ' +
                    ' ERP_WBS__r.Location__c, ERP_WBS__r.Forecast_Start_Date__c, ' +
                    ' ERP_WBS__r.Forecast_End_Date__c FROM SVMXC__Service_Order__c WHERE ' +  ' Id IN: woId' ;
                    //' Id = ' + '\'' +  WO.Id + '\'' ;
            
            PopulateCaseonWorkOrder_Batch EXE_batch = new PopulateCaseonWorkOrder_Batch(query, woId);
            
            Database.executeBatch(EXE_batch);
                        
        test.stopTest();
        
    }
    
}
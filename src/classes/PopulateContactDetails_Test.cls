/********* modified by Bushra to increase coverage
*************/

@isTest
private class PopulateContactDetails_Test
 {
 
    static testmethod void myconstructorTest1(){
     
    Profile p = [SELECT Id FROM Profile WHERE Name='System administrator'];
    /*
    User u1 = new User(Alias = 'newUser', Email='bushra11@wipro.com',
                     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                     LocaleSidKey='en_US', ProfileId = p.Id,
                     TimeZoneSidKey='America/Los_Angeles',UserName='demo1@wipro.com',Country='india'); 
        insert u1;
        
 */
        
        test.startTest();
        //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234',Prefered_Language__c ='English');
        insert accnt;
        list<Complaint__c> listComp = new list<Complaint__c>();
        Complaint__c comp1 = new Complaint__c(Name='test1',Language__c='French',RA_Region__c='North');
        listComp.add(comp1);
        Complaint__c comp2 = new Complaint__c(Name = 'test11',Language__c='Hindi',RA_Region__c='North');
        listComp.add(comp2);
        Complaint__c comp3 = new Complaint__c(Name = 'test13');
        listComp.add(comp3);
        Complaint__c comp4 = new Complaint__c(Name = 'test11',Language__c='Hindi',RA_Region__c='North');
        listComp.add(comp4);
        
       
        insert listComp;
        
        list<Consignee__c> listCon = new list<Consignee__c>();
        Consignee__c consObj1 = new Consignee__c(Name ='TestData', RA_Region__c='North', Language__c ='French',Complaint_Number__c = comp1.id, PCSN__c ='test',Customer_Name__c= accnt.id);
        listCon.add(consObj1);
        Consignee__c consObj2 = new Consignee__c(Name ='TestData', RA_Region__c='South', Language__c ='Hindi',Complaint_Number__c = comp1.id, PCSN__c ='test1',Customer_Name__c= accnt.id);
        listCon.add(consObj2);
        Consignee__c consObj3 = new Consignee__c(Name ='TestData', RA_Region__c='North', Language__c ='French', multiple_language__c ='Russian,German,Italian',Complaint_Number__c = comp1.id, PCSN__c ='test2',Customer_Name__c= accnt.id);
        listCon.add(consObj3);
        Consignee__c consObj4 = new Consignee__c(Name ='TestData11', RA_Region__c='South', Language__c ='English', multiple_language__c ='Hindi,French,English', Complaint_Number__c=comp2.id, PCSN__c='tset',Customer_Name__c= accnt.id);
        listCon.add(consObj4);
        Consignee__c consObj5 = new Consignee__c(Name ='TestData13', RA_Region__c='South', Language__c ='French', multiple_language__c='',Complaint_Number__c=comp3.id,PCSN__c='ert',Customer_Name__c= accnt.id);
        listCon.add(consObj5);
        Consignee__c consObj6 = new Consignee__c(Name ='TestData13', RA_Region__c='South', Language__c ='German', multiple_language__c='English, French, German',Complaint_Number__c=comp3.id,PCSN__c='ert',Customer_Name__c= accnt.id);
        listCon.add(consObj6);
        Consignee__c consObj7 = new Consignee__c(Name ='TestData13', RA_Region__c='North', Language__c ='Italian',Complaint_Number__c=comp3.id,PCSN__c='ert',Customer_Name__c= accnt.id);
        listCon.add(consObj7);
        Consignee__c consObj8 = new Consignee__c(Name ='TestData13', RA_Region__c='North', Language__c ='German',Complaint_Number__c=comp3.id,PCSN__c='ert',Customer_Name__c= accnt.id);
        listCon.add(consObj8);
        
        Consignee__c consObj9 = new Consignee__c(Name ='TestData13', RA_Region__c='South', Language__c ='French',Complaint_Number__c=comp4.id,PCSN__c='ert',Customer_Name__c= accnt.id);
        listCon.add(consObj9);
        insert listCon;
    
        
    
     
     }
}
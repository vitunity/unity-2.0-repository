public class PreferredServiceTeam{    
    public static void UpdateField(List<Case> lstCaseTemp, Map<Id,Case> oldMapCase){
        Id HDRT = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Helpdesk'); 
        List<Case> lstCase = new List<case>();
        set<Id> setPS = new set<Id>();
        for(Case c: lstCaseTemp ){          
            if(c.Preferred_Service_Team__c == null && c.ProductSystem__c  <> null)
            {
                if(oldMapCase==null || (oldMapCase <> null && c.ProductSystem__c <>  oldMapCase.get(c.Id).ProductSystem__c)){
                    lstCase.add(c);
                    setPS.add(c.ProductSystem__c);
                }
            }
        }
        if(!setPS.isEmpty()){
            map<Id,SVMXC__Installed_Product__c > mapProductSystem = new map<Id,SVMXC__Installed_Product__c >([select id,Service_Team__c from SVMXC__Installed_Product__c where Id in:setPS  and Service_Team__c <> null]);
            for(case c : lstcase){
                if(mapProductSystem.containsKey(c.ProductSystem__c)){
                    c.Preferred_Service_Team__c = mapProductSystem.get(c.ProductSystem__c).Service_Team__c;

                }           
            }
        }
    }
}
@RestResource(urlMapping='/Presentation/*')
       global class PresentationController 
       {
       @HttpGet    
       global static List<ContentVersion> retrieve()
       {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
      //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String Id= RestContext.request.params.get('workspaceId') ;
        List <ContentVersion> allDocuments = [SELECT ID, ContentDocument.ParentId,FileType, FileExtension, Title FROM ContentVersion WHERE ContentDocument.ParentId = :Id and FileType='PDF'];        
        return allDocuments;
       }
       }
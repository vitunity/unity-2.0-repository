/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 14-Aug-2017
    @ Description   : STSK0012636 - Update Product Version field when the Product Version Build field is updated.
****************************************************************************/
public class ProduceVersionClass{
    /***************************************************************************
    Description: 
    This method is used to Update Product Version field when the Product Version Build field is updated.
    *************************************************************************************/
    public static void VersionUpdate(List<SVMXC__Installed_Product__c> lstNewIP, map<Id,SVMXC__Installed_Product__c> Oldmap){
    
        List<SVMXC__Installed_Product__c> lstInstalledProduct = new List<SVMXC__Installed_Product__c>();
        set<Id> setPBIds = new set<Id>();
        for(SVMXC__Installed_Product__c i: lstNewIP){
            if((i.Product_Version__c == null && Oldmap == null) || (Oldmap <> null && i.Product_Version_Build__c <> Oldmap.get(i.id).Product_Version_Build__c)){
                if(i.Product_Version_Build__c <> null){
                    lstInstalledProduct.add(i);
                    setPBIds.add(i.Product_Version_Build__c);
                }
            }           
        }
        
        map<Id,Product_Version_Build__c> mapProductVersionBuild = new map<Id,Product_Version_Build__c>([select Id,Product_Version__c from Product_Version_Build__c where id IN: setPBIds]);
        for(SVMXC__Installed_Product__c i: lstInstalledProduct){
            if(mapProductVersionBuild.containsKey(i.Product_Version_Build__c)){
                i.Product_Version__c = mapProductVersionBuild.get(i.Product_Version_Build__c).Product_Version__c;
            }
        }
        
    }
}
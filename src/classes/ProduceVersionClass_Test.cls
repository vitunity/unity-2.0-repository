@isTest
public class ProduceVersionClass_Test{

    public static testmethod void UnitTest(){
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;
        
        //Product record
        Product2 prod = new Product2(name = 'testprod');
        insert prod;
        
        //Product Version record
        Product_Version__c prodver = new Product_Version__c(Major_Release__c = 1,Minor_Release__c = 0);
        insert prodver;
        
        //Product Version Build record
        Product_Version_Build__c prodverbuild = new Product_Version_Build__c(name = 'prodverbuildtest',Product__c = prod.id, Build_Number__c = 1,Product_Version__c=prodver.id);
        insert prodverbuild;
        
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id,
        Product_Version_Build__c = prodverbuild.Id);
        insert objIP;  
        
        
    }
}
/*@RestResource(urlMapping='/ProductCategories/*')
    global class ProductCategoriesController 
    {
    @HttpGet
    global static List<RecordType> getCategory() {
    List<RecordType> widgets1;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Language= RestContext.request.params.get('Language');
        if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){  
      widgets1 = [SELECT Name,Description  FROM RecordType WHERE SobjectType = 'VU_Products__c' AND (Description='English(en)'OR Description='') ORDER BY Name DESC NULLS FIRST];
}
else
{
    widgets1 = [SELECT Name,Description  FROM RecordType WHERE SobjectType = 'VU_Products__c' AND Description=:Language  ORDER BY Name DESC NULLS FIRST];
   }
    return widgets1;
    }
    global class ReturnClass {
    global String success;
    global String message;
    global ReturnClass(String success, String message) 
    {
    this.success = success;
    this.message = message;
    }
    }
    }
    */
    
    @RestResource(urlMapping='/ProductCategories/*')
    global class ProductCategoriesController 
    {
    @HttpGet
    global static List<RecordType> getCategory() {
    List<RecordType> widgets1;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Language= RestContext.request.params.get('Language');
         if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(ja)' && Language !='Portuguese(pt-BR)' ){  
      widgets1 = [SELECT Name,Description  FROM RecordType WHERE SobjectType = 'VU_Products__c' AND (Description='English(en)'OR Description='') ORDER BY Name DESC NULLS FIRST];
}
else
{
    widgets1 = [SELECT Name,Description  FROM RecordType WHERE SobjectType = 'VU_Products__c' AND Description=:Language  ORDER BY Name DESC NULLS FIRST];
   }
    return widgets1;
    }
    global class ReturnClass {
    global String success;
    global String message;
    global ReturnClass(String success, String message) 
    {
    this.success = success;
    this.message = message;
    }
    }
    }
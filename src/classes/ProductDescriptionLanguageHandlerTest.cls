@isTest
private class ProductDescriptionLanguageHandlerTest {

    static testMethod void myUnitTest() {
        Language__c language = new Language__c();
        language.Name = 'English';
        language.BMI_Code__c = 'TESTBMI';
        language.BMI_Code__c = 'English1';
        language.SAP_Code__c = 'TESTSAP';
        insert language;
        
        Product2 prod1 = new Product2();
        prod1.Name = 'Sample Product 1';
        prod1.isActive = true;
        
        Product2 prod2 = new Product2();
        prod2.Name = 'Sample Product 2';
        prod2.IsActive = true;
        insert new List<Product2>{prod1,prod2};
        
        Product_Description_Language__c pdl1 = new Product_Description_Language__c();
        pdl1.Product__c = prod1.Id;
        pdl1.Language__c = language.id;
        
        Product_Description_Language__c pdl2 = new Product_Description_Language__c();
        pdl2.Product__c = prod2.Id;
        pdl2.Language__c = language.id;
        insert new List<Product_Description_Language__c>{pdl1,pdl2};
        
        Product_Description_Language__c pdl3 = new Product_Description_Language__c();
        pdl3.Product__c = prod1.Id;
        pdl3.Language__c = language.id;
        
        try{
            insert pdl3;
        }catch(Exception e){
            System.debug('----ErrorMessage'+e.getMessage());
           
        }
    }
}
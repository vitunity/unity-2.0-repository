public with sharing class ProductDescriptionLanguageTriggerHandler {
  /*
   * Update unique key field with product id + language code
   * added by amit
   */
   public static void updateUniqueKey(List<Product_Description_Language__c> pdlList){
     for(Product_Description_Language__c pdl :pdlList){
       if(pdl.Product__c != null && pdl.Language__c!= null){
         pdl.Unique_Key__c = pdl.Product__c+''+pdl.Language__c;
       }
     }
   }
   
}
/*************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 16-May-2013
    @ Description   : An Apex class related to Prodcut Documnetation.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class ProductDocChdController {

    //List of Properties    
    List<ContentVersion> lContentVersions ;
    public String Productvalue {get;set;}
    public String DocumentTypes {get;set;}
    public String VersionInfo {get;set;} 
    public String Document_TypeSearch {get;set;}
    public Date cntDate {get; set;}
    public String Document_Number {get; set;}
    public string cntTitle {get; set;}
    public String shows {get;set;}
    public String Usrname{get;set;} 
    public String ids{get;set;}
     
    public ProductDocChdController () 
    { 
      User un = [Select ContactId,alias from user where id =: UserInfo.getUserId() limit 1];
      shows = 'none';
       if(un.contactid == null)
        {
          Usrname = un.alias;
          shows = 'block';
        }
       
        try{
            system.debug(' --------- content Id ---------- ' + ApexPages.currentPage().getParameters().get('Id'));
           ContentVersion cntVr = [select Title,Date__c, Document_Number__c from ContentVersion where ContentDocumentId = :ApexPages.currentPage().getParameters().get('Id') AND IsLatest = true limit 1];           
                       //cntTitle = cntVr.Title;
           //cntDate = cntVr.Date__c.format('MMMM - yyyy');
            cntDate = cntVr.Date__c;
            cntTitle = cntVr.Title;
            Document_Number = cntVr.Document_Number__c;
            ids=cntVr.ContentDocumentId;
            System.debug('>>>>>' + ids);
        }
        catch(Exception E){}
        
    } 
    
    public List<ContentVersion> getlContentVersions() 
    {
        try 
        {
            lContentVersions  = new List<ContentVersion>();         
            string whr='';
            set<Id> contntId = new set<Id>();
            String temp = '*' + Document_TypeSearch + '*';
            String Query='';
            ids=ApexPages.currentPage().getParameters().get('Id');
            if(ids!=null)
            {
             System.debug('>>>>>' + ids);
            Query = 'select Title, ContentSize,Document_Language__c, Region__c, ContentDocumentId from ContentVersion where (ContentDocumentId  =:ids) and IsLatest = true and (RecordType.name = \'Product Document\' OR RecordType.name = \'Partner Document\') ';
           
            User currentUser = [SELECT Id, Name, ContactId, Contact.Territory__c FROM User WHERE Id = :Userinfo.getUserId()];
            System.debug('currentUser----->>>>>' + currentUser.ContactId);
            System.debug('currentUser Territory----->>>>>' + currentUser.Contact.Territory__c);
            if(currentUser.ContactId != NULL)
            {
                if(currentUser.Contact.Territory__c != NULL)
                {
                    Query = Query + 'and Region__c INCLUDES (\'' + currentUser.Contact.Territory__c + '\', \'ALL\') ';
                }
                else
                {
                    Query = Query + 'and Region__c INCLUDES (\'ALL\') ';
                    System.debug('Contact Id of curren---------->>');
                }
            }
          //  Query = Query + 'ORDER BY Document_language__c ASC' ;
             List<Sobject> sobj = DataBase.Query(Query);           
            For(Sobject s : Sobj)
            {
                lContentVersions.add((ContentVersion)s);
                
            }
            }
           if(cntTitle != null && Document_Number!=null){
            String Query1 = 'select Title, ContentSize,Document_Language__c, Region__c, ContentDocumentId from ContentVersion where (Parent_Documentation__c = :Document_Number) and IsLatest = true and (RecordType.name = \'Product Document\' OR RecordType.name = \'Partner Document\') ';
            
            User currentUser1 = [SELECT Id, Name, ContactId, Contact.Territory__c FROM User WHERE Id = :Userinfo.getUserId()];
            System.debug('currentUser----->>>>>' + currentUser1.ContactId);
            System.debug('currentUser Territory----->>>>>' + currentUser1.Contact.Territory__c);
            if(currentUser1.ContactId != NULL)
            {
                if(currentUser1.Contact.Territory__c != NULL)
                {
                    Query1 = Query1 + 'and Region__c INCLUDES (\'' + currentUser1.Contact.Territory__c + '\', \'ALL\') ';
                }
                else
                {
                    Query1 = Query1 + 'and Region__c INCLUDES (\'ALL\') ';
                    System.debug('Contact Id of curren---------->>');
                }
            }
           
            Query1 = Query1 + 'ORDER BY Document_language__c,Title' ;
            List<Sobject> sobj1 = DataBase.Query(Query1);           
            For(Sobject s : Sobj1)
            {
                lContentVersions.add((ContentVersion)s);
                
            }
            }
             return lContentVersions;
            }
            
       // } 
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
    }
  

}
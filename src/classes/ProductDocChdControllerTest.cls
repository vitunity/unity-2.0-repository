/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for ProductDocChdController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest(SeeAllData=true)

Public class ProductDocChdControllerTest{

    //Test Method for ProductDocChdController class
    
    static testmethod void testProductDocChdController (){
        Test.StartTest();
        
        
        RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
        ContentVersion testContentInsert =new ContentVersion(); 
             testContentInsert.ContentURL='http://www.google.com/'; 
             testContentInsert.Title ='Google.com'; 
             testContentInsert.RecordTypeId = ContentRT.Id; 
             insert testContentInsert; 
             ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id];
              
             ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
             ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
             newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
             newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
             insert newWorkspaceDoc;
             
             //testContentInsert.FirstPublishLocationId = library.id;
             testContentInsert.Parent_Documentation__c = testContentInsert.Title;
             testContentInsert.Document_Number__c = '121345';
			 testContentInsert.Region__c = 'ALL';
             Update testContentInsert;
			 
			 ContentVersion testContentInsert2 =new ContentVersion(); 
             testContentInsert2.ContentURL='http://www.google.com/'; 
             testContentInsert2.Title ='Google.com'; 
             testContentInsert2.RecordTypeId = ContentRT.Id; 
             insert testContentInsert2; 
             ContentVersion testContent2 = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert2.Id];
			 ContentWorkspaceDoc newWorkspaceDoc2 =new ContentWorkspaceDoc(); 
             newWorkspaceDoc2.ContentWorkspaceId = testWorkspace.Id; 
             newWorkspaceDoc2.ContentDocumentId = testContent2.ContentDocumentId; 
             insert newWorkspaceDoc2;
			 testContentInsert2.Parent_Documentation__c = '121345';
             testContentInsert2.Document_Number__c = '342234';
			 testContentInsert2.Region__c = 'ALL';
             Update testContentInsert2;
			 
             ProductDocChdController  PrdoDoc1 =  new ProductDocChdController ();
			 User u = [SELECT Id, Name FROM User WHERE ContactId != NULL LIMIT 1];
			 System.RunAs(u)
			 {
        ApexPages.currentPage().getParameters().put('Id',testContent.ContentDocumentId);
        ProductDocChdController  PrdoDoc =  new ProductDocChdController ();
		PrdoDoc.Document_Number = testContentInsert.Document_Number__c;
        List<ContentVersion> contvers = PrdoDoc.getlContentVersions();
        }
            
             Test.StopTest();
         }
     }
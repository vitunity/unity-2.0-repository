/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 07-May-2013
    @ Description   : An Apex controller to get Product documentation data from Content Type Prodcut Documentation.
    @ Last Modified By  :   Anupam Tripathi
    @ Last Modified On  :   13-Aug-2013
    @ Last Modified Reason  :   Product version
****************************************************************************/
public class ProductDocumentControllerPartIntUsers {

    //List of Properties    
    List<ContentVersion> ContentVersions ;
    public String Productvalue {get;set;}
    public String DocumentTypes {get;set;}
    public String RegionAll {get;set;}
    public String VersionInfo {get;set;}
    public String MaterialTypeInfo {get;set;}   
     public String TreatmentTechniquesInfo {get;set;}
    public String Document_TypeSearch {get;set;}
    public Decimal Document_Version {get;set;}
    public string strBanner{get; set;}
    public boolean hdNext {get;set;}
    public boolean hdPrvs {get;set;}
    public  list<selectoption> lstContentVersion{get;set;}
    public  list<selectoption> lstContentDoctype{get;set;} 
    public Id accountId;
    public string Usrname{get;set;}
    public string shows{get;set;}
    public Boolean isVisible{get;set;}
    public Boolean isRegionvisible{get;set;}

    String gblStrBanner='';
    // ******* New Code start VVVV

    //For Product Info.
    Public List<SelectOption> options;// = new List<SelectOption>();
    Public List<SelectOption> getoptions()
    {  
           set<string> ProductGroupSet = new set<String>();
             options = new List<SelectOption>();
        //   if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId != null)
      // {
            for(ContentVersion  cv :[select ContentDocumentId,Product_Group__c from ContentVersion where id != null and IsLatest = true and Parent_Documentation__c = null and RecordType.name ='Partner Document' ])
            {
            if(cv.Product_Group__c!=null)
            {
             productGroupSet.addAll(cv.Product_Group__c.split(';'));
            }
            //productGroupSet.add(cv.Product_Group__c); 
            System.debug('$$$--->'+cv.ContentDocumentId+ 'Group%%%%%'+cv.Product_Group__c);
            } 
     //   }
     /*  else
        {
  // for internal user AND PARTNER USERS
          
           
           Schema.DescribeFieldResult fieldResult = ContentVersion.Product_Group__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
               {
                  ProductGroupSet.add(f.getValue());
               }       
         }*/
       System.debug('ProductGroupSet@@@@'+ProductGroupSet);
         // Logic ends
        options.add(new selectoption('--Any--','--Any--'));
        for(string sr : ProductGroupSet) {
        options.add(new selectoption(sr, sr)); 
        }
       
       options.sort();
      return options; 
      }
          

    //For Doc Types.      
   /* Public List<SelectOption> DocTypes;//= new List<SelectOption>();
    Public List<SelectOption> getDocTypes(){
        DocTypes = new List<SelectOption>();
        Schema.Describefieldresult DocTypesDesc = ContentVersion.Document_Type__c.getDescribe();
        system.debug('%%%%%%%%'+ DocTypesDesc);
        DocTypes.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
        DocTypes.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return DocTypes;
    } */

    //For Version Types.
  /*  Public List<SelectOption> Versions= new List<SelectOption>();
    Public List<SelectOption> getVersions(){
        Schema.Describefieldresult VersionsDesc = ContentVersion.Document_Version__c.getDescribe();
        system.debug('%%%%%%%%'+ VersionsDesc);
        Versions.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: VersionsDesc.getPicklistValues()) {
        Versions.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return Versions;
    } */

    
    
   /* public static Set<String> fetchProductGroup(){
        set<String> productGroupSet = new set<String>();
        
        Id accountId = [Select Id, AccountId From User where id = : Userinfo.getUserId() limit 1].AccountId;
        
        if(accountId != null){
          
            for(SVMXC__Installed_Product__c ip :[Select s.SVMXC__Product__r.Product_Group__c, s.SVMXC__Product__c, s.Id, s.SVMXC__Company__c 
                    From SVMXC__Installed_Product__c s where s.SVMXC__Company__c =: accountId and SVMXC__Product__r.Name != null]){
                    productGroupSet.addAll(ip.SVMXC__Product__r.Product_Group__c.split(';'));                                             
            }
           
        } 
        System.debug('------mohit---->'+productGroupSet);
        
      //stFinalGroup.addall(productGroupSet);
       
        return productGroupSet;
    }*/
    
    
      //Code Added By Mohit
      public  static void gtContentNmber()
      {
        
        //System.debug('-----Mohit----->'+lstContentVersion);
     }
        //Code End By Mohit
    
    private integer counter=0;  //keeps track of the offset
    private integer list_size=20; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
  
    public ProductDocumentControllerPartIntUsers (){   
        accountId = [Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].AccountId;
        lstContentDoctype=new list<selectoption>();
        //added by mohit
       // stFinalGroup  =new set<string>();           //added by mohit
       isVisible=false;
       isRegionvisible=false;
      System.debug('@@@Call');
        gtBannerRep('All');
        gblStrBanner = strBanner;
        sortField = 'date__c';
        sortDir = 'desc';
        shows = 'none';
     //   Productvalue='--Any--';
     // for community global header
     if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
       {
          Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
          shows='block';
          isRegionvisible=True;
          system.debug('############' + shows);
          //isGuest=false;
       }
    }
    
    private String soql {get;set;}
    public void gtBannerRep(string prm)
    {      
       soql='select Image__c from BannerRepository__c where  Recordtype.Name =\'Partner Documentation\'';
       BannerRepository__c[] bn=Database.query(soql);
       // BannerRepository__c[] bn = [select Image__c from BannerRepository__c where Product_Affiliation__c includes(\''+prm+'\') and Recordtype.Name = 'Product Documentation'];
        system.debug(' ---- banner URL -----  --- ' + prm + ' ----- size ----- ' + bn.size());
        if(bn.size() > 0){
             //Added by harshita
             
             if(bn[0].Image__c != null ){
                        System.debug('aebug ; bn.Image__c == '+bn[0].Image__c.containsIgnoreCase('https'));
                       
                         /* Code UPdated by Anupam */
                        strBanner = bn[0].Image__c;
 
                        //System.debug(S1.substring(S1.indexof('src')+5));
 
                        List<String> ss = strBanner.substring(strBanner.indexof('src')+5).split('"');
 
                        if(ss.size()>0){
                               system.debug(ss[0]);
                               strBanner=ss[0];
                        }       
 
 
//s2 = s2.replace('amp;','');
                    }
  //End of code                 
           /* if(bn[0].Image__c != null && bn[0].Image__c.length() > 93){     
                if(bn[0].Image__c.containsIgnoreCase('https')){
                    strBanner = bn[0].Image__c.substring(bn[0].Image__c.indexof('src')+5,bn[0].Image__c.indexof('src')+126);
                }else{
                    strBanner = bn[0].Image__c.substring(bn[0].Image__c.indexof('src')+5,bn[0].Image__c.indexof('src')+94);
                }
            }  */         
        }
        else
        strBanner = gblStrBanner;
        system.debug(' ---- banner URL ----- ' + strBanner + ' --- ' + prm);
    }
    

    public List<ContentVersion> ContentVersionsList {get;set;}
    public void SearchContentVersions() 
    {          
        ContentVersions= [select Title, Mutli_Languages__c,Document_Language__c,Document_Type__c,Document_Number__c,Document_Version__c from ContentVersion where Document_Type__c=:DocumentTypes  ];
    }

    public List<ContentVersion> lContentVersions;

    public PageReference showrecords() 
    {
        getlContentVersions();
              return null;
    }
    public PageReference showrecords1() 
    {
            Beginning();
            
           return null;
    }
    Public List<SelectOption> TreatmentTechniques;
    Public List<SelectOption> getTreatmentTechniques()
    {
        TreatmentTechniques = new List<SelectOption>();
        Schema.Describefieldresult TreatmentTechniquesDesc = ContentVersion.Treatment_Technique__c  .getDescribe();
        system.debug('%%%%%%%%'+ TreatmentTechniquesDesc);
        TreatmentTechniques.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: TreatmentTechniquesDesc.getPicklistValues()) {
        TreatmentTechniques.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return TreatmentTechniques;

    }
     Public List<SelectOption> MaterialTypes;
    Public List<SelectOption> getMaterialTypes()
    {
        MaterialTypes = new List<SelectOption>();
        Schema.Describefieldresult MaterialTypesDesc = ContentVersion.Material_Type__c.getDescribe();
        system.debug('%%%%%%%%'+ MaterialTypesDesc);
        MaterialTypes.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: MaterialTypesDesc.getPicklistValues()) {
        MaterialTypes.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return MaterialTypes;

    }
    Public List<SelectOption> Regions;//= new List<SelectOption>();
    Public List<SelectOption> getRegions()
    {
    
     Regions= new List<SelectOption>();
        Schema.Describefieldresult RegionsDesc = ContentVersion.Region__c.getDescribe();
        system.debug('%%%%%%%%'+ RegionsDesc );
        Regions.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: RegionsDesc.getPicklistValues()) {
        if(picklistvalues.getvalue()!='All')
        {
        Regions.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }}
        return Regions;   
    }
   Public List<SelectOption> DocTypes;//= new List<SelectOption>();
    Public List<SelectOption> getDocTypes()
    {
    
     DocTypes = new List<SelectOption>();
        Schema.Describefieldresult DocTypesDesc = ContentVersion.Document_Type2__c.getDescribe();
        system.debug('%%%%%%%%'+ DocTypesDesc);
        DocTypes.add(new selectoption('--Any--','--Any--'));
        for(Schema.Picklistentry picklistvalues: DocTypesDesc.getPicklistValues()) {
        DocTypes.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel())); }
        return DocTypes;   
    }
    public List<ContentVersion> getlContentVersions() 
    {
        try 
        {
        
      system.debug('testss');
            set<Id> conDocID = new set<Id>();
         List<selectoption> CvDoctype= new list<selectoption>();
         List<selectoption> CvDoctypeList= new List<selectoption>();
            lContentVersions  = new List<ContentVersion>();         
            string whr='';
            set<Id> contntId = new set<Id>();
            String temp =  Document_TypeSearch + '*';
            
            String Query = 'select Mutli_Languages__c,ContentDocumentId,Title,Date__c,Document_Language__c,Document_Type__c,Recently_Updated__c, Document_Number__c,Newly_Created__c,Document_Version__c,Document_Type2__c,Product_Group__c,Material_Type__c,Treatment_Technique__c from ContentVersion where id != null and IsLatest = true and Parent_Documentation__c = null and RecordType.name = \'Partner Document\' '; 
            // + ' and VersionNumber =' + VersionInfo + '  limit ' + list_size + ' offset ' +counter;
            String QueryCnt = 'select count() from ContentVersion where id != null and Parent_Documentation__c = null and RecordType.name = \'Partner Document\''; 
            if((Productvalue != '--Any--') && (Productvalue != null))
            {
                /*  String Pv;
                  Pv= '%' + Productvalue + '%';
                  System.debug('@@ProductValueCheck'+Pv);
                  whr = whr + ' and Product_Group__c LIKE  :Pv'; */
            /*    for(ContentWorkspaceDoc conWorDoc :[Select c.ContentDocumentId From ContentWorkspaceDoc c where ContentWorkspace.Name ='Partner Channel' ]){
                    System.debug(' ----------- ----------------  : featWebinars =' + conWorDoc);
                    conDocID.add(conWorDoc.ContentDocumentId);                                  
                }
                //if(conDocID.size() > 0)
                whr = 'and ContentDocumentId in : conDocID';*/
                Query = Query + 'and Product_Group__c INCLUDES (\'' + Productvalue + '\', \'ALL\') ';

                gtBannerRep(Productvalue);
               
                        }
            else {
                strBanner = gblStrBanner;
                total_size = null;
                }
    //   **************************************Code End**********************************************************************//
            
            if((DocumentTypes != '--Any--') && (DocumentTypes != null))
            {   
                  String s;
                  s = '%' + DocumentTypes + '%';
                  System.debug('@@ValueCheck'+s);
                  whr = whr + ' and Document_Type2__c LIKE  :s';            
               // whr = whr + ' and Document_Type__c=: DocumentTypes';
            }
            if((MaterialTypeInfo != '--Any--')  && (MaterialTypeInfo != null))
            {           
               
                  String st;
                  st = '%' + MaterialTypeInfo + '%';
                  whr = whr + '  and Material_Type__c LIKE  : st';
            }
            if((TreatmentTechniquesInfo != '--Any--')  && (TreatmentTechniquesInfo != null))
            {           
                //whr = whr + ' and Document_Version__c =' + VersionInfo;
               /*   String strs;
                  strs = '%' + TreatmentTechniquesInfo + '%';
                  whr = whr + '  and Treatment_Technique__c LIKE  : strs';*/
                   Query = Query + 'and Treatment_Technique__c INCLUDES (\'' + TreatmentTechniquesInfo  + '\', \'ALL\') ';
            }
            if(Document_TypeSearch != '' && Document_TypeSearch!= null)
            {
                String searchquery = 'FIND \'' + temp +'\' IN ALL FIELDS RETURNING ContentVersion(ID)';
                List<List<SObject>> searchList = search.query(searchquery);
                ContentVersion[] cn = ((List<ContentVersion>)searchList[0]);
                //for(searchList[0] cn : ContentVersion)                
                //contntId.add(cn.Id);
                system.debug(' --- Check File Column ----  -- ' + cn + ' --- ----- ' + searchList);
                whr = whr + ' and Id in : cn';
            }
            
            /* Code by Nikhil to make Region field on Content functional (20-May-14)---------- */ 
            User currentUser = [SELECT Id, Name, ContactId, Contact.Territory__c FROM User WHERE Id = :Userinfo.getUserId()];
            System.debug('currentUser----->>>>>' + currentUser.ContactId);
            System.debug('currentUser Territory----->>>>>' + currentUser.Contact.Territory__c);
            if(currentUser.ContactId != NULL)
            {
                if(currentUser.Contact.Territory__c != NULL)
                {
                    Query = Query + 'and Region__c INCLUDES (\'' + currentUser.Contact.Territory__c + '\', \'ALL\') ';
                }
                else
                {
                    Query = Query + 'and Region__c INCLUDES (\'ALL\') ';
                    System.debug('Contact Id of curren---------->>');
                }
            }
            else
            {
            if(RegionAll!='--Any--' && RegionAll!= null)
            {
             Query = Query + 'and Region__c INCLUDES (\'' + RegionAll + '\', \'ALL\') ';
            }
            
            }
            /* Code by Nikhil ends here------------- */
            QueryCnt = QueryCnt + ' ' + whr;
            system.debug('----- query constricted is---->'+total_size);
            if(whr == '' || whr == null)
             whr = 'order by ' + sortField + ' '+ sortDir;
            else if(!whr.contains('order'))
              whr = whr + ' order by ' + sortField + ' '+ sortDir;        
            if(counter < 2000){
             if (counter == total_size)
              Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + (counter - list_size);
             else
              Query = Query + ' ' + whr +' limit ' + list_size + ' offset ' + counter;
             }
            else
             Query = Query + ' ' + whr +' limit ' + counter + 20;
            System.debug('Query ------- 123 --------  >>>>>' + QueryCnt);
            System.debug('Query ------- 123 --------  >>>>>' + Query);
            total_size = DataBase.countQuery(QueryCnt);
            system.debug('----- query constricted is---->'+total_size);
            List<Sobject> sobj = DataBase.Query(Query);
            //System.debug('---Sobject size----'+sobj.size());
            //total_size = sobj.size();
            
            if(counter < 2000){
            For(Sobject s : Sobj)
            {
            
               
                lContentVersions.add((ContentVersion)s);
                System.debug('List lstcontentversion'+lContentVersions.size());
            }
            }else{
              For(Integer i = counter; i <= counter + 9;i++)
               {
                if(i < Sobj.size())
                lContentVersions.add((ContentVersion)Sobj[i]);
                System.debug('List lstcontentversion'+lContentVersions.size());
               }
            }
             
            return lContentVersions;
        } 
        catch (QueryException e) 
        {
            ApexPages.addMessages(e);   
            return null;
        }
    }

  
  
/*  
   public ContentVersion[] getNumbers() 
   {
      try 
      {
         ContentVersion[] numbers = [select Mutli_Languages__c,ContentDocumentId,Title, Language,Date__c,Document_Type__c,Document_Number__c,Document_Version__c from ContentVersion  limit :list_size offset :counter];
         return numbers;
      } 
      catch (QueryException e) 
      {
        ApexPages.addMessages(e);   
         return null;
      }
   }
 */
 
    public PageReference reSet() { //user clicked beginning
   
      
      getoptions();
       Productvalue='--Any--'; 
       DocumentTypes='--Any--'; 
       MaterialTypeInfo='--Any--'; 
       TreatmentTechniquesInfo='--Any--'; 
      counter = 0;
   
      return (new pagereference('/apex/CpProductDocumentation_Part_IntUsers'));
   }
 
   public PageReference Beginning() { //user clicked beginning
      counter = 0;
      System.debug('##DocumentTypes'+DocumentTypes);
     if(DocumentTypes=='Product Material')
    {
    
    isVisible=true;
    }
    else
    {
    isVisible=false;
    }
    System.debug('##DocumentTypes'+isVisible);
      //total_size = 0;
      return null;
   }
 
   public PageReference Previous() { //user clicked previous button
      counter -= list_size;     
      return null;
   }
 
   public PageReference Next() { //user clicked next button
      counter += list_size;     
      return null;
   }
 
   public PageReference End() { //user clicked end
      //if(math.mod(total_size, list_size) > 0)
       counter = total_size - math.mod(total_size, list_size);
      return null;
   }
 
   public Boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
       system.debug('getPageNumber()= = = '+getPageNumber());
       system.debug('getTotalPages()= = = '+getTotalPages());
       if(getPageNumber() != getTotalPages())
       return false;
     else
       return true;
      //if (counter + list_size < total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
     if(total_size == 0)
     {
      return 0;
     }
    else if(total_size == list_size)
     {
      return total_size/list_size;
     }else{
       if(counter == total_size)
        {
        return counter/list_size; // + 1;
        }else{
         return counter/list_size +1;
        }
          }
   }
 
   public Integer getTotalPages() {
   
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;         
      } else {
         return (total_size/list_size);
      }
   }
   
   
   
   
   
   
   /*code used by harshita for the ascending and descending in productdocument*/
   
   public String soqlsort {get;set;}
   public List <ContentVersion> CandidateList2 = New List <ContentVersion>();
   //Toggles the sorting of query from asc<-->desc
    public void  toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
                            /*
                                // run the query again for sorting other columns
                                soqlsort = 'select Title, Mutli_Languages__c,Date__c,Document_Type__c,Document_Number__c,Document_Version__c from ContentVersion'; 

                                // Adding String array to a List array
                                System.debug('_______^^^^___ ' + soqlsort + ' order by ' + sortField + ' ' + sortDir );
                                ContentVersionsList = Database.query(soqlsort + ' order by ' + sortField + ' ' + sortDir ); */
    }

    // the current sort direction. defaults to asc
    public String sortDir {
        // To set a Direction either in ascending order or descending order.
                                get  { if (sortDir == null) {  sortDir = 'desc'; } return sortDir;}
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField {
        // To set a Field for sorting.
                                get  { if (sortField == null) {sortField = 'Date__c'; } return sortField;  }
        set;
    } 
/*code ended */
         
 

}
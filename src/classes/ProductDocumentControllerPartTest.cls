@isTest(seeAllData = true)
Public class ProductDocumentControllerPartTest{
static testmethod void testProductDocumentControllerPartIntUsers(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            Group group1 = [Select id from group where name = 'Acuity'];
            Groupmember gm = new groupmember(GroupId = group1.id,UserOrGroupId = UserInfo.getuserId());
            insert gm;
            
            }
             user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
             
            Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity');
             insert pr;
             Product_Version__c pv = new Product_Version__c(product__c = pr.id,Major_Release__c=2,Minor_Release__c=1);
             insert pv;
             
            Product_Version__c prod_ver = [SELECT Version__c FROM Product_Version__c where Id =: pv.Id];
            RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];
             ContentVersion testContentInsert =new ContentVersion(); 
             testContentInsert.ContentURL='http://www.google.com/'; 
             testContentInsert.Title ='Google.com'; 
             testContentInsert.RecordTypeId = ContentRT.Id;
             insert testContentInsert; 
            
            List<Id> fixedSearchResults = new List<Id>();
            fixedSearchResults.add(testContentInsert.id);
            Test.setFixedSearchResults(fixedSearchResults);
            
         
         System.runAs (usr){


                
        ProductDocumentControllerPartIntUsers proddoc = new ProductDocumentControllerPartIntUsers();
         
        proddoc.Productvalue ='Acuity'; 
        List<SelectOption> Selopt = proddoc.getDocTypes();
        List<SelectOption> Selopts = proddoc.getTreatmentTechniques();
        List<SelectOption> Selops = proddoc.getoptions();
        List<SelectOption> Selmat=proddoc.getMaterialTypes();
        ProductDocumentControllerPartIntUsers.gtContentNmber();
         
        proddoc.DocumentTypes = 'Safety Notification';
        proddoc.versioninfo= '1';
        proddoc.Document_TypeSearch = 'Testing';
        proddoc.MaterialTypeInfo = 'Test';
        proddoc.TreatmentTechniquesInfo='TestTreatment';
        proddoc.total_size= 10;
        proddoc.SearchContentVersions();
        proddoc.showrecords();
        proddoc.reSet();
        proddoc.Beginning();
        proddoc.Previous();
        proddoc.Next();
        proddoc.End();
        proddoc.showrecords1();
        proddoc.getDisablePrevious();
        proddoc.getDisableNext();
        proddoc.getTotal_size();
        proddoc.getPageNumber();
        proddoc.getTotalPages();
        proddoc.getlContentVersions();
        
        proddoc.gtBannerRep('prodaff');
        proddoc.toggleSort();
        
         
        
         }
            
             Test.StopTest();
            
            }
}
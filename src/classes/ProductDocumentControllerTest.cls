/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 20-June-2013
    @ Description   :  Test class for ProductDocumentController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
    
Change Log:
26-Aug-2017 - Divya Hargunani - STSK0012692: MyVarian: CRA enhancement not working - Added test method for newly added method in a class
****************************************************************************/

@isTest(seeAllData = true)

Public class ProductDocumentControllerTest{

    
    //Test Method for ProductDocumentController class
    
    static testmethod void testProductDocumentController (){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
         
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar0.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com');//,UserRoleid=r.id); 
            
            insert u; // Inserting Portal User
            
            Group group1 = [Select id from group where name = 'Acuity'];
            Groupmember gm = new groupmember(GroupId = group1.id, UserOrGroupId = UserInfo.getuserId());
            insert gm;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
        
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             Product_Version__c prodver=new Product_Version__c();
             //prodver.Version__c='12';
             prodver.Product__c=prod.id;
             insert prodver;
             
            
       }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


                
        ProductDocumentController proddoc = new ProductDocumentController ();
         
        proddoc.Productvalue ='Acuity'; 
        List<SelectOption> Selopt = proddoc.getDocTypes();
        List<SelectOption> Selopts = proddoc.getVersions();
        List<SelectOption> Selops = proddoc.getoptions();
        ProductDocumentController.gtContentNmber();
        
        proddoc.DocumentTypes = 'Safety Notification';
        proddoc.versioninfo= '1';
        proddoc.Document_TypeSearch = 'Testing';
        proddoc.total_size= 10;
        proddoc.SearchContentVersions();

         }
            
             Test.StopTest();
             
        
        }
        
        static testmethod void testProductDocumentController4(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar1.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Group group1 = [Select id from group where name = 'Acuity'];
            Groupmember gm = new groupmember(GroupId = group1.id, UserOrGroupId = UserInfo.getuserId());
            insert gm;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
        
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             Product_Version__c prodver=new Product_Version__c();
             //prodver.Version__c='12';
             prodver.Product__c=prod.id;
             insert prodver;
             
            
       }
         Test.StartTest();
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


                
        ProductDocumentController proddoc = new ProductDocumentController ();         
         proddoc.Productvalue ='Acuity'; 
        proddoc.showrecords();
        proddoc.reSet();
        proddoc.Beginning();
        proddoc.Previous();
        proddoc.Next();
        proddoc.End();
        proddoc.showrecords1();
        proddoc.getDisablePrevious();
        proddoc.getDisableNext();
        proddoc.getTotal_size();
        proddoc.getPageNumber();
        proddoc.getTotalPages();
        proddoc.getlContentVersions();
        /*ProductDocumentController.fetchProductGroup();
        proddoc.gtBannerRep('prodaff');
        proddoc.toggleSort();*/
        
         
        
         }
            
             Test.StopTest();
             
        
        }
        
        static testmethod void testProductDocumentController1 (){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
             
            
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar2.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
        
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
       
       }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


                
        ProductDocumentController proddoc = new ProductDocumentController ();
       
        proddoc.Productvalue ='Acuity';
        proddoc.DocumentTypes = 'Safety Notification';
        proddoc.versioninfo= '1';
        proddoc.Document_TypeSearch = 'Testing';
        proddoc.total_size= 215;
        proddoc.End();
         }
         // for internal user
         ProductDocumentController proddoc = new ProductDocumentController ();
       
        proddoc.Productvalue ='Acuity';
        proddoc.DocumentTypes = 'Safety Notification';
        proddoc.versioninfo= '1';
        proddoc.Document_TypeSearch = 'Testing';
        proddoc.total_size= 215;
        proddoc.End();
        proddoc.getoptions();
            
             Test.StopTest();
     }
     
     static testmethod void testProductDocumentController2 (){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar3.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
            Groupmember gm = new groupmember(GroupId = group1.id, UserOrGroupId = UserInfo.getuserId());
            insert gm;
            
            Groupmember gm2 = new groupmember(GroupId = group1.id, UserOrGroupId = u.Id);
            insert gm2;
            
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Product Documentation' AND sobjecttype='BannerRepository__c'];
            BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert bn;
            
            ProductDocumentController inst = new ProductDocumentController();
            List<SelectOption> options = inst.getoptions();
            inst.gtBannerRep('All');
        }
        
            User u = [SELECT Id, Email FROM User WHERE Email = 'standarduser@testorg.com'];
            System.runAs ( u ) {
            ProductDocumentController inst1 = new ProductDocumentController();
            inst1.Productvalue = '--Any--';
            List<SelectOption> op = inst1.getVersions();
            List<SelectOption> doct = inst1.getDocTypes();
        }
        Test.StopTest();
    }
    
     static testmethod void testProductDocumentController5(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar4.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Group group1 = [Select id from group where name = 'Acuity'];
            Groupmember gm = new groupmember(GroupId = group1.id, UserOrGroupId = UserInfo.getuserId());
            insert gm;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
        
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             Product_Version__c prodver=new Product_Version__c();
             //prodver.Version__c='12';
             prodver.Product__c=prod.id;
             insert prodver;
             
            
       }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


                
        ProductDocumentController proddoc = new ProductDocumentController ();   
        System.debug(proddoc.ContentVersionsList);      
        proddoc.Productvalue ='Acuity'; 
        ProductDocumentController.fetchProductGroup();
        proddoc.gtBannerRep('prodaff');
        proddoc.toggleSort();
        
         
        
         }
            
             Test.StopTest();
             
        
        }
        
         static testmethod void testProductDocumentController6(){
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar5.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User                       
            Test.StartTest();
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Product Documentation' AND sobjecttype='BannerRepository__c'];
            BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert bn;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
            
            SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             Product_Version__c prodver=new Product_Version__c();
             //prodver.Version__c='12';
             prodver.Product__c=prod.id;
             insert prodver;
             
            ProductDocumentController inst = new ProductDocumentController();
            List<SelectOption> options = inst.getoptions();
            inst.gtBannerRep('All');
        }
        
            User u = [SELECT Id, Email FROM User WHERE Email = 'standarduser@testorg.com'];
            System.runAs ( u ) {
            ProductDocumentController inst1 = new ProductDocumentController();
            inst1.Productvalue = '--Any--';
            List<SelectOption> op = inst1.getVersions();
            List<SelectOption> doct = inst1.getDocTypes();
        }
        Test.StopTest();
    }
    
    static testmethod void testProductDocumentController7(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar6.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User                                   
            RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Product Documentation' AND sobjecttype='BannerRepository__c'];
            BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert bn;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
            
            SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             Product_Version__c prodver=new Product_Version__c();
             //prodver.Version__c='12';
             prodver.Product__c=prod.id;
             insert prodver;
             
            ProductDocumentController inst = new ProductDocumentController();
            List<SelectOption> options = inst.getoptions();
            inst.gtBannerRep('All');
        }
        List<SelectOption> op = new ProductDocumentController().getVersions();
            User u = [SELECT Id, Email FROM User WHERE Email = 'standarduser@testorg.com'];
            System.runAs ( u ) {
            //ProductDocumentController inst1 = new ProductDocumentController();
            //ProductDocumentController.Productvalue = '--Any--';
            
            //List<SelectOption> doct = new ProductDocumentController().getDocTypes();
        }
        Test.StopTest();
    }
    
    //STSK0012692: Added test method for newly added code in class
    static testmethod void testProductDocumentController8(){
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar7.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testsate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Group group1 = [Select id from group where name = 'Acuity'];
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
        
             SVMXC__Installed_Product__c Insprd = new SVMXC__Installed_Product__c();
             Insprd.Name = 'Testing';
             Insprd.SVMXC__Product__c = prod.Id;
             Insprd.SVMXC__Serial_Lot_Number__c = '1234';
             Insprd.SVMXC__Status__c = 'Installed';
             Insprd.SVMXC__Company__c = con.AccountId;
             
             Insert Insprd;
             
             user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
             System.runAs (usr){
                             
             ProductDocumentController proddoc = new ProductDocumentController ();         
             proddoc.Productvalue ='Acuity';
             proddoc.productGroupsCheck();
             }
            }
             
  }  
 }
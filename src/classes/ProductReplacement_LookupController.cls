/**
* @author Puneet Mishra
* @description Currently used in prepare order lightning component for Product Replacement Lookups
*/
public class ProductReplacement_LookupController {
    
    @AuraEnabled public List<sObject> objectRecords;
    
    @AuraEnabled public String accId {get;set;}
    @AuraEnabled public String objName{get;set;}
    @AuraEnabled public String objLabel;
    @AuraEnabled public String productFamily{get;set;}
    @testVisible private Static Opportunity opty;
    
    @AuraEnabled
    public static List<sObject> getReplacementProducts(String accId, String productFamily, String objName, String objLabel
                                                      ,String vendor) {
        List<sObject> objectRecords = new List<sObject>();
        List<String> nonVarianIPFamilies = new List<String>();
        String isBlank = '';
        String topLevel = '';
		system.debug(' ===== objLabel ===== ' + objLabel);
		system.debug(' ===== productFamily ===== ' + productFamily);
		system.debug(' ===== objName ===== ' + objName);
        if(objName == 'SVMXC__Installed_Product__c') {
            if(productFamily == 'HW'){
                nonVarianIPFamilies = getVarianProductCodes('HW');
                topLevel = ' AND SVMXC__Top_Level__c =: isBlank ';
            }else{
                nonVarianIPFamilies = getVarianProductCodes('SW');
            }
            system.debug(' ===== nonVarianIPFamilies ===== ' + nonVarianIPFamilies);
            String ipQuery = getIPQuery() +' WHERE SVMXC__Company__c =:accId AND ERP_Pcodes__r.Name IN:nonVarianIPFamilies '
                +   topLevel + ' LIMIT 1000';
                //+
                //							' AND SVMXC__Top_Level__c =: isBlank ';
            objectRecords = Database.query(ipQuery);
        } else if(objName == 'Competitor__c' && objLabel == 'Competitors') {
            //String nonVarianIPQuery;
            if(productFamily == 'HW') {
                if(!String.isBlank(System.Label.Non_Varian_HW_Families)){
                    nonVarianIPFamilies = System.Label.Non_Varian_HW_Families.split(',');
                }
                /*nonVarianIPQuery = getNonVarianIPQuery()+
                    				' WHERE Competitor_Account_Name__c =:accId AND ' +
                    				' RecordType.DeveloperName IN:nonVarianIPFamilies ' + 
                    				' AND Vendor__c =: vendor ';*/
            }else{
                if(!String.isBlank(System.Label.Non_Varian_SW_Families)){
                    nonVarianIPFamilies = System.Label.Non_Varian_SW_Families.split(',');
                }
            }
            system.debug(' ===== nonVarianIPFamilies ===== ' + nonVarianIPFamilies);
        	system.debug(' ===== accountId ====== ' + accId);
            String nonVarianIPQuery;
            nonVarianIPQuery = getNonVarianIPQuery()+' WHERE Competitor_Account_Name__c =:accId ' +
                			' AND RecordType.DeveloperName IN:nonVarianIPFamilies LIMIT 1000 ';// AND Vendor__c =: vendor
            system.debug(' ===== nonVarianIPQuery ====== ' + nonVarianIPQuery);
            objectRecords = Database.query(nonVarianIPQuery);
        } else if(objName == 'Competitor__c' && objLabel == 'Service Takeout'){
            String TPSC = System.Label.Non_Varian_Service_Contracts;
            system.debug(' ===== vendor ====== ' + vendor);
            //system.assert(false);
            String serviceTakeoutQuery = getNonVarianIPQuery()+' WHERE Competitor_Account_Name__c =:accId ' +
                						' AND Vendor__c =: vendor AND RecordType.DeveloperName =:TPSC LIMIT 1000' ;// 
            
            objectRecords = Database.query(serviceTakeoutQuery);
        }
        return objectRecords;
    }
    
    @testVisible
    private static List<String> getVarianProductCodes(String family){
        List<String> pCodes = new List<String>();
        String productQuery = 'SELECT Id, ERP_Pcode__r.Name FROM Product2 WHERE Product_Type__c = \'Model\'';
        if(family == 'HW'){
            productQuery += ' AND Family IN (\'System Solution\') AND IsActive = true';
        }else{
            productQuery += ' AND Family IN (\'Software Solution\',\'Subscription\') AND Status__c != \'Obsolete\'';
        }
        for(Product2 product : Database.query(productQuery)){
            pCodes.add(product.ERP_Pcode__r.Name);
            system.debug(' ==== pCodproduct.ERP_Pcode__r.Namees ==== ' + product.ERP_Pcode__r.Name);
        }
        system.debug(' ==== pCodes ==== ' + pCodes);
        return pCodes;
    }
    
    @testVisible
    private static List<String> getSelectedIPNumbers(){
        String selectedHardwareIPs = '';
        if(!String.isBlank(opty.Varian_PCSN_Non_Varian_IB__c)){
            selectedHardwareIPs = opty.Varian_PCSN_Non_Varian_IB__c;
        }
        
        String selectedSOftwareIps = '';
        if(!String.isBlank(opty.Varian_SW_Serial_No_Non_Varian_IB__c)){
            selectedSOftwareIps = opty.Varian_SW_Serial_No_Non_Varian_IB__c;
        }
        
        List<String> selectedIPNumbers = new List<String>();
        selectedIPNumbers.addAll(selectedHardwareIPs.split(','));
        selectedIPNumbers.addAll(selectedSOftwareIps.split(','));
        return selectedIPNumbers;
    }
    
    @testVisible
    private static String getIPQuery(){
        return 'SELECT Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, '
            +'Product_Version_Build__r.Name, SVMXC__Product__r.Name,SVMXC__Country__c, SVMXC__City__c,'
            +'SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,'
            +'SVMXC__Service_Contract__r.SVMXC__Start_Date__c '
            +'FROM SVMXC__Installed_Product__c ';
    }
    
    @testVisible
    private static String getNonVarianIPQuery(){
        return 'SELECT Id, Name, Product_Type__c, Model__c, Vendor__c, Isotope_Source__c, Vault_identifier__c, Year_Installed__c, '
            +'Service_Provider__c, Status__c, RecordType.Description '
            +'FROM Competitor__c ';
    }
    
}
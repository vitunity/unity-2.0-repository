/**
 * @author      :       Puneet Mishra
 * @description :       Test Class for ProductReplacement_LookupController
 */
@isTest
public class ProductReplacement_LookupController_Test {
    
    private static Account acc;
    private static Opportunity opp;
     public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    
    private static Account createAcc(Boolean isInsert) {
        Account newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.State__C = 'California';        
        newAcc.Country__c = 'USA';
        newAcc.BillingCity = 'Milpitas';
        System.debug('Account####'+UserInfo.getUserId());
        if(isInsert)
            insert newAcc;
        return newAcc;
    }
    
    private static Opportunity createOpp(Boolean isInsert, String accId) {
        Opportunity opp = new Opportunity();
        opp.AccountId = accId;
        opp.Name = 'Test Opp';
        opp.CloseDate = system.today();
        opp.StageName = '5 - NEGOTIATION (TENDER AWARDED)';
        if(isInsert)
            insert opp;
        return opp;
    }
    
    private static List<Competitor__c> createCompetitorData(String oppId) {
        List<Competitor__c> commList = new List<Competitor__c>();
        Competitor__c comp;
        Schema.DescribeSObjectResult R = Competitor__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        system.debug(' ==== RT ==== ' + RT);
        Integer i = 1;
        for(Schema.RecordTypeInfo rec : RT) {
            comp = new Competitor__c();
            comp.Vendor__c = 'Elekta';
            comp.Model__c = 'MOSAIQ';
            comp.RecordTypeId = rec.getRecordTypeId();
            comp.Opportunity__c = oppId;
            commList.add(comp);
        }
        
        return commList;        
    }
    
    @isTest(SeeAllData = true)
    private static List<String> productData() {
        list<String> pCodes = new list<String>();
        ProductReplacement_LookupController c = new ProductReplacement_LookupController();
        c.accId = '';
        c.objName = '';
        c.productFamily = '';
        return pCodes;
    }
    
    private static testMethod void getVarianProductCodes_HW_Test() {
        test.startTest();
        ProductReplacement_LookupController.getVarianProductCodes('HW');
        test.stopTest();
    }
    
    private static testMethod void getVarianProductCodes_SW_Test() {
        test.startTest();
        ProductReplacement_LookupController.getVarianProductCodes('SW');
        test.stopTest();
    }
    
    private static testMethod void getReplacementProducts_IP_HW_test() {
        test.startTest();
            Account acc = createAcc(true);
            Opportunity opp = createOpp(true, acc.Id);
            list<Competitor__c> compList = createCompetitorData(opp.Id);
            ProductReplacement_LookupController.getReplacementProducts(acc.Id, 'HW', 'SVMXC__Installed_Product__c','','');
        test.stopTest();
    }
    
    private static testMethod void getReplacementProducts_IP_SW_test() {
        test.startTest();
            Account acc = createAcc(true);
            Opportunity opp = createOpp(true, acc.Id);
            list<Competitor__c> compList = createCompetitorData(opp.Id);
            ProductReplacement_LookupController.getReplacementProducts(acc.Id, 'SW', 'SVMXC__Installed_Product__c','','');
        test.stopTest();
    }
    
    private static testMethod void getReplacementProducts_Comp_HW_test() {
        test.startTest();
            Account acc = createAcc(true);
            Opportunity opp = createOpp(true, acc.Id);
            list<Competitor__c> compList = createCompetitorData(opp.Id);
            ProductReplacement_LookupController.getReplacementProducts(acc.Id, 'HW', 'Competitor__c','Competitors','Elekta');
        test.stopTest();
    }
    
    private static testMethod void getReplacementProducts_Comp_SW_test() {
        test.startTest();
            Account acc = createAcc(true);
            Opportunity opp = createOpp(true, acc.Id);
            list<Competitor__c> compList = createCompetitorData(opp.Id);
            ProductReplacement_LookupController.getReplacementProducts(acc.Id, 'SW', 'Competitor__c','Competitors','Elekta');
        test.stopTest();
    }
    
    private static  testmethod void getSelectedIPNumbers_test() {
        acc = createAcc(true);
        opp = createOpp(true, acc.Id);
        
        test.startTest();
            List<Competitor__c> compList = createCompetitorData(opp.Id);
            opp.Varian_SW_Serial_No_Non_Varian_IB__c = compList[0].Name;
            opp.Varian_PCSN_Non_Varian_IB__c = compList[1].Name;
            ProductReplacement_LookupController.opty = opp;
            ProductReplacement_LookupController.getSelectedIPNumbers();
        test.stopTest();
    }
    
    private static testmethod void getIPQuery_Test() {
        system.assertNotEquals('' ,ProductReplacement_LookupController.getIPQuery());
    }
    
    private static testmethod void getNonVarianIPQuery_Test() {
        system.assertNotEquals('' ,ProductReplacement_LookupController.getNonVarianIPQuery());
    }
}
/* 
 * @Author -  Amitkuamr Katre
 * Product stock history Utils
 */
public with sharing class ProductStockHistoryUtils {
  
  public static Id polShipmentRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__RMA_Shipment_Line__c').get('Shipment');
  
  /* 
     * Create Product Stock 
     */
     
    public static SVMXC__Product_Stock__c CreateProductStock(String locationId, String ProductId, String status, Decimal qty){
        SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
        ps.SVMXC__Location__c = locationId;
        ps.SVMXC__Product__c = ProductId;
        ps.SVMXC__Status__c = status;
        ps.SVMXC__Quantity2__c = qty;
        return ps;
    }
    
    public static SVMXC__Product_Stock__c CreateProductStock(String locationId, String ProductId, String status, Decimal qty, String batchPart){
        SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
        ps.SVMXC__Location__c = locationId;
        ps.SVMXC__Product__c = ProductId;
        ps.SVMXC__Status__c = status;
        ps.SVMXC__Quantity2__c = qty;
        if(String.isNotBlank(batchPart)){
            ps.Batch_Number__c =  batchPart;
        }
        return ps;
    }
    
    /*
     * Create Stock History from Work Detail
     */
     
    public static SVMXC__Stock_History__c CreatePSHistory(
        String transactionType,
        String status,
        String changeType,
        SVMXC__Service_Order_Line__c wdl,
        Decimal beforeQty,
        Decimal afterQty,
        Decimal txnQty,
        SVMXC__Product_Stock__c ps){
            SVMXC__Stock_History__c sh = new SVMXC__Stock_History__c();
            sh.SVMXC__Location__c = wdl.SVMXC__Consumed_From_Location__c;
            sh.SVMXC__Date_Changed__c = wdl.Related_Activity__r.SVMXC__End_Date_and_Time__c;
            sh.SVMXC__Transaction_Type__c = transactionType;
            sh.SVMXC__Status__c = status;
            sh.SVMXC__Change_Type__c = changeType;
            sh.SVMXC__Product_Stock__r = ps;
            sh.SVMXC__Quantity_before_change2__c = beforeQty;
            sh.SVMXC__Quantity_after_change2__c = afterQty;
            sh.SVMXC__Transaction_Quantity2__c = txnQty;
            sh.SVMXC__Service_Order_Line__c = wdl.Id;
            sh.SVMXC__Product__c = wdl.SVMXC__Product__c;
            sh.SVMXC__Service_Order__c = wdl.SVMXC__Service_Order__c;
            sh.Parts_Order_Line__c = wdl.Parts_Order_Line__c;
            sh.SVMXC__Shipment_Line__c = wdl.Parts_Order_Line__c;
            return sh;
    }

    /*
     * Create Stock History From POL and Workorder
     */
    public static SVMXC__Stock_History__c CreatePSHistory(
        String transactionType,
        String status,
        String changeType,
        SVMXC__RMA_Shipment_Line__c pol,
        Decimal beforeQty,
        Decimal afterQty,
        Decimal txnQty,
        SVMXC__Product_Stock__c ps,
        Id locationId){
            SVMXC__Stock_History__c sh = new SVMXC__Stock_History__c();
            sh.SVMXC__Location__c = locationId;
            sh.SVMXC__Transaction_Type__c = transactionType;
            sh.SVMXC__Status__c = status;
            sh.SVMXC__Change_Type__c = changeType;
            sh.SVMXC__Product_Stock__r = ps;
            sh.SVMXC__Quantity_before_change2__c = beforeQty;
            sh.SVMXC__Quantity_after_change2__c = afterQty;
            sh.SVMXC__Transaction_Quantity2__c = txnQty;
            sh.Parts_Order_Line__c = pol.Id;
            sh.SVMXC__Product__c = pol.SVMXC__Product__c;
            sh.SVMXC__Service_Order__c = pol.SVMXC__Service_Order__c;
            sh.SVMXC__RMA_Line__c = pol.Id;
            sh.SVMXC__RMA__c = pol.SVMXC__RMA_Shipment_Order__c;
            return sh;
    }
    
     /* 
     * Get Prats Order Lines from database 
     */
    public static map<Id,SVMXC__RMA_Shipment_Line__c> GetpartsOrderLineMap(list<SVMXC__RMA_Shipment_Order__c> partsOrderList){
        return new map<Id,SVMXC__RMA_Shipment_Line__c>([select Id, SVMXC__RMA_Shipment_Order__c,SVMXC__Fulfillment_Qty__c, 
                                                        SVMXC__Actual_Quantity2__c, Remaining_Qty__c, SVMXC__Product__r.SVMXC__Product_Cost__c,
                                                        SVMXC__Product__c, SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Service_Order__c, SVMXC__Service_Order__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,SVMXC__Expected_Quantity2__c,
                                                        ERP_Serial_Number__c,Part__c,Batch_Number__c,
                                                        (select Id from Stocked_Serials__r)
                                                        from SVMXC__RMA_Shipment_Line__c
                                                        where SVMXC__RMA_Shipment_Order__c in : partsOrderList]);
    }
    
     /* 
     * Get Prats Order Lines from database
     */
    public static map<Id,SVMXC__RMA_Shipment_Line__c> GetpartsOrderLineMap(set<Id> partsOrderLine){
        return new map<Id,SVMXC__RMA_Shipment_Line__c>([select Id, SVMXC__RMA_Shipment_Order__c,SVMXC__Fulfillment_Qty__c, 
                                                        SVMXC__Actual_Quantity2__c, Remaining_Qty__c, SVMXC__Product__r.SVMXC__Product_Cost__c,
                                                        SVMXC__Product__c, SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Service_Order__c, SVMXC__Service_Order__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,Batch_Number__c,
                                                        (select Id,Name from Stocked_Serials__r)
                                                        from SVMXC__RMA_Shipment_Line__c
                                                        where id in : partsOrderLine]);
    }
    
    /*
     * Get Product Stock From Database -
     */
    public static map<String, SVMXC__Product_Stock__c> GetPsMap(Set<id> productIds, set<Id> locationIds, set<Id> productStockIds){
        map<String, SVMXC__Product_Stock__c> psMapLocal = new map<String, SVMXC__Product_Stock__c>();
        List<SVMXC__Product_Stock__c> listProdStock = [select Id, SVMXC__Location__c, SVMXC__Product__c,SVMXC__Status__c, SVMXC__Quantity2__c,
                                                       SVMXC__ProdStockCompositeKey__c,Batch_Number__c,
                                                       (select Id, Name from SVMXC__Product_Serial__r)
                                                       from SVMXC__Product_Stock__c
                                                       where (SVMXC__Location__c in : locationIds AND SVMXC__Product__c in : productIds)
                                                       or ( Id in : productStockIds)];
        for(SVMXC__Product_Stock__c ps : listProdStock){
            psMapLocal.put(ps.SVMXC__ProdStockCompositeKey__c,ps);          
        }
        return psMapLocal;
    } 
}
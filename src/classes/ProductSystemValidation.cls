/*
 * Installed Product Is Not Eligible For Use Validation
 *
 */
public class ProductSystemValidation{
    
    public static void validate(List<Case> lstCaseTemp, Map<Id,Case> oldMapCase){
        Id HDRT = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Helpdesk'); 
        List<Case> lstCase = new List<case>();
        system.debug('#D1:'+HDRT);
        set<Id> setPS = new set<Id>();
        for(Case c: lstCaseTemp ){          
            if(c.ProductSystem__c <> null && c.RecordTypeId == HDRT){
                if((oldMapCase==null) || (oldMapCase <> null && c.ProductSystem__c <>  oldMapCase.get(c.Id).ProductSystem__c)){
                    lstCase.add(c);
                    setPS.add(c.ProductSystem__c);
                }
            }
        }
        if(!setPS.isEmpty()){
            system.debug('#D2:'+setPS+''+lstCase);
            map<Id,SVMXC__Installed_Product__c > mapProductSystem = new map<Id,SVMXC__Installed_Product__c >([select id,SVMXC__Company__c,SVMXC__Status__c from SVMXC__Installed_Product__c where Id in:setPS  ]);
            system.debug('#D3:'+mapProductSystem );
            for(case c : lstcase){
                Id PSAccountId;
                string PSStatus;
                if(mapProductSystem.containsKey(c.ProductSystem__c)){
                    PSAccountId = mapProductSystem.get(c.ProductSystem__c).SVMXC__Company__c;
                    PSStatus = mapProductSystem.get(c.ProductSystem__c).SVMXC__Status__c;           
                }
                if(!Test.isRunningTest() && ((c.AccountId <> null && PSAccountId <> c.AccountId) || (PSStatus <> 'Installed' && PSStatus <> 'Installation' ) ) ){
                    c.AddError('Installed Product Is Not Eligible For Use.'); //'+PSAccountId+'--'+c.AccountId +'--'+ PSStatus
                }
            }
        }
    }
}
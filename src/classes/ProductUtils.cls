/***
This util class will have methods related to all public groups and installed products. 

****/
public with sharing class ProductUtils {



    public static set < String > getInstalledBasePublicGroupList(String AccId) {

        set < String > installedGroupSet = new Set < String > ();
        for (SVMXC__Installed_Product__c insProds: [select Id, SVMXC__Product__c, SVMXC__Company__c, SVMXC__Product__r.Product_Group__c
                from SVMXC__Installed_Product__c where SVMXC__Company__c =: AccId
            ]) {
            system.debug('----------------insProds.SVMXC__Product__c----------' + insProds.SVMXC__Product__c);
            if ((insProds.SVMXC__Product__c == null ? false : (insProds.SVMXC__Product__r.Product_Group__c == null ? false : true))) {
                system.debug('----------------insProds.SVMXC__Product__r.Product_Group__c----------' + insProds.SVMXC__Product__r.Product_Group__c);
                for (String str: insProds.SVMXC__Product__r.Product_Group__c.split('; ')) {
                    installedGroupSet.add(insProds.SVMXC__Product__r.Product_Group__c);
                }
            }
        }
        return installedGroupSet;
    }


    public static Map < Id, set < string >> getAccountSelectedGroups(set < ID > accntLst) {
        system.debug('inside getAccountSelectedGroups');
        Map < Id, set < string >> accountSelectedGroupMap = new Map < Id, set < string >> ();
        set < string > groupSet = new set < string > ();


        for (Account acc: [Select Id, Selected_Groups__c, Name, IsPartner, IsCustomerPortal, Quality__c, Peer_to_Peer__c, RapidArc__c, IGRT__c, RPM__c, IMRT__c, SRS_SBRT__c, Paperless__c, Latitude__c, Longitude__c from Account where Id IN: accntLst]) {
            if (acc.Selected_Groups__c != null) {
                for (String str: acc.Selected_Groups__c.split('; ')) {
                    groupSet.add(str);
                    System.debug('###### STRING = ' + str);
                }
                accountSelectedGroupMap.put(acc.id, groupSet);
                System.debug('###### ProdUtils Account Id = ' + acc.id);
            }
        }
        return accountSelectedGroupMap;
    }

    public static List < GroupMember > getAddUserToSelectedPublicGroup(List < User > newUser, Map < Id, set < String >> userGroupSetMap) {
        system.debug('inside getAddUserToSelectedPublicGroup');
        List < GroupMember > grpMemInsert = new List < GroupMember > ();
        for (User ur: newUser) {
            if (userGroupSetMap.ContainsKey(ur.Id)) {
                for (Id grpId: userGroupSetMap.get(ur.Id)) {
                    GroupMember Obj = new Groupmember();
                    Obj.GroupID = grpId;
                    Obj.UserOrGroupId = ur.Id;
                    grpMemInsert.add(Obj);
                }
            }
        }
        system.debug('---------grpMemInsert------------' + grpMemInsert.size());
        return grpMemInsert;
    }

    // send account id and the set of public groups
    public static List < GroupMember > getPublicGroupMemberListForAccount(Id accntId, set < string > publicGroupSet) {
		
        //get related contacts
        Set < Id > ContactSet = new Set < Id > ();
        for (Contact c: [select Id, AccountID from contact where AccountId =: accntId]) {
            ContactSet.add(c.Id);
        }
        
        //get cra contacts - Added by Amit for CRA enhancement
        for (Contact_Role_Association__c cra : [select Id, Contact__c from Contact_Role_Association__c 
                                                where Account__c =: accntId]) {
            ContactSet.add(cra.Contact__c);
        }
        		        
        //Get Group List
        List < Group > publicGroupLst = [SELECT Id, Name, Type FROM Group Where Name in: publicGroupSet AND(Type = 'Regular')];
        List < GroupMember > grpMemberLst = new List < GroupMember > ();
        //Get portal contact list

        system.debug('=====ContactSet=====>' + ContactSet);
        for (user u: [select id, name, ContactId from User where contactId in: ContactSet and Profile.Name =: Label.VMS_Customer_Portal_User and isActive != false]) {
            for (Group grp: publicGroupLst) {
                GroupMember Obj = new Groupmember();
                Obj.GroupID = grp.Id;
                Obj.UserOrGroupId = u.Id;
                grpMemberLst.add(Obj);
            }
        }
        return grpMemberLst;
    }



    public static boolean isPortalAdmin(String userId) {
        string VMS_MyVarian_AdminPermissionsSet = '0PSE0000000HXgx';
        List < PermissionSetAssignment > Permission = [select PermissionSetId From PermissionSetAssignment p where p.PermissionSetId =: VMS_MyVarian_AdminPermissionsSet and p.AssigneeId =: userId];

        if (permission.size() > 0) return true;
        else return false;

    }

    public static boolean isSystemAdmin(Id profileId) {
        string SystemAdminProfile = '00eE0000000mvyb';

        if (profileId == SystemAdminProfile) return true;
        else return false;

    }
    /*  @future (Callout=false)
     public static void updateContactClearValues(set<Id> stContactIds){
          List<contact> contacts = [select id, accountId,MyVarian_Member__c from contact where ID in :stContactIds];
       for (contact cons: contacts) {
              cons.TrueBeam_Accepted__c= False;
              cons.Monte_Carlo_Registered__c='';
              cons.MarketingKit_Agreement__c=null;
              cons.Recovery_Question__c='';
              cons.Recovery_Answer__c='';
              cons.activation_url__c='';
              cons.PasswordReset__c=false;
              cons.PasswordresetDate__c=null;
              }
              if(!Test.isRunningTest()){
              
        update contacts ;}
     }*/

    @future(Callout = false)
    public static void UpdateUserActiveFlag(set < Id > stContactIds) {
        List < User > Lstusersids = new List < User > ();

        Lstusersids = [Select id, Contactid from user where Contactid in: stContactIds];
        /* CpUserActivationClass.userdeactivationmethod(Setofids);
            ProductUtils.updateContactPartnerFlag(Setofids,false);
            ProductUtils.DeleteContactFromPublicgroups(Setofids);*/

        for (User varuser: Lstusersids) {
            varuser.IsActive = False;
            varuser.IsPortalEnabled = False;
        }
        if (Lstusersids.size() > 0) {
            Update Lstusersids;
            System.debug('Working code*****');
        }
    }

    @future(Callout = false)
    public static void updateContactPartnerFlag(set < Id > stContactIds, boolean myVarianMemberFlag) {

        List < contact > contacts = [select id, accountId, MyVarian_Member__c from contact where ID in: stContactIds];
        for (contact cons: contacts) {
            cons.MyVarian_Member__c = myVarianMemberFlag;
            if (myVarianMemberFlag == false) {
                cons.TrueBeam_Accepted__c = False;
                cons.Monte_Carlo_Registered__c = '';
                cons.MarketingKit_Agreement__c = null;
                cons.Recovery_Question__c = '';
                cons.Recovery_Answer__c = '';
                cons.activation_url__c = '';
                cons.PasswordReset__c = false;
                cons.PasswordresetDate__c = null;
            }
            system.debug('##################### Contact Partner Flag' + myVarianMemberFlag);
        }
        if (!Test.isRunningTest()) {
            update contacts;
        }
        /*  Set<id> Userids=new Set<id>();
          Set<id> GroupMemb=new Set<id>();
          Set<id> Accountids=new Set<id>(); 
          if(stContactIds.size()>0)
          {
          for(User u:[Select id,Contactid,Contact.accountid from User where Contactid in:stContactIds])
          {
          Userids.add(u.id);
          Accountids.add(u.contact.accountid);
          }
          }
          
          for (GroupMember Gm:[Select id from GroupMember  where userorgroupid in:Userids])
          {
          GroupMemb.add(Gm.id);
          }
          List<GroupMember> DelGroupMem=new  List<GroupMember>();
          for(GroupMember GmFinalList:[Select id from GroupMember where id in:GroupMemb])
          {
           DelGroupMem.add(GmFinalList);
          }
          if(DelGroupMem.size()>0)
          {
           Delete DelGroupMem;
          }*/

    }
}
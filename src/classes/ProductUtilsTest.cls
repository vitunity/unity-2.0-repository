@isTest//(SeeAllData=true)

Public class ProductUtilsTest{
Static
{
        
}
    //Test Method for ProductDocChdController class
    
    static testmethod void testProductUtilController (){
        
        Product2 objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true,Product_Group__c='prod1,prod2');   
        insert objProd;
        
        Country__c con = new Country__c(Name = 'United States');
        insert con;

        Account acc = new Account(Name = 'testsubmitmethod acc',Selected_Groups__c='prod1,prod2', ERP_Timezone__c = 'AUSSA', country__c = 'United States', BillingCity='SanMateo', Country1__c = con.id); 
        insert acc;
        
        contact cont = new contact(AccountId =acc.id, LastName ='abhi', FirstName ='Shek',email='abhi@yopmail.com');
        insert cont;
        
        SVMXC__Installed_Product__c objIns = new SVMXC__Installed_Product__c(SVMXC__Serial_Lot_Number__c ='abc', ERP_Reference__c = 'H1234', SVMXC__Company__c = Acc.id, Name = 'H1234',SVMXC__Product__c = objProd.id);
        insert objIns;
        //Account acc = AccountTestData.createAccount();
        set<Id> accIdset = new set<Id>();
        accIdset.add(acc.Id);
        set<String> groupNames = new set<String>();
        List<Group> publicGroupLst = [SELECT Id, Name, Type FROM Group where Type= 'Regular' limit 10];
        String str = '';
        for(Group g : publicGroupLst){
            groupNames.add(g.Name);
            str = str+'; '+g.Name;
        }
        
        
        User u = [select Id from User limit 1];
        Group grp = [SELECT Id, Name, Type FROM Group where type = 'Regular' limit 1];
        List<User> user = new List<User>();
        user.add(u);
        Map<Id,set<String>> userGroupSetMap = new Map<Id,set<String>>();
        String grpId = String.valueOf(grp.Id);
        userGroupSetMap.put(u.Id,new Set<String>{grpId});
//        System.assertequals([select count() from contact],1);
        ProductUtils.getInstalledBasePublicGroupList(acc.Id);
        ProductUtils.getAccountSelectedGroups(accIdset);
        List<GroupMember> gm = ProductUtils.getPublicGroupMemberListForAccount(acc.Id,groupNames);
        Boolean b = ProductUtils.isPortalAdmin(u.Id);
        Boolean b1 = ProductUtils.isSystemAdmin('00eE0000000mvyb');
        ProductUtils.getAddUserToSelectedPublicGroup(user,userGroupSetMap);
        ProductUtils pu = new ProductUtils();
        ProductUtils.UpdateUserActiveFlag(new set <Id>{cont.id});
        ProductUtils.updateContactPartnerFlag(new set <Id>{cont.id},false);
        
    }
}
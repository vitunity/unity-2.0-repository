/*---------------------------------------------------------------------
- Author        : Pranjul Maindola 
- Created Date  :29/08/2017
- Description   :PonTriggerHandler is used to update PON__c object's field Installed_Product__c. 

-----------------------------------------------------------------------*/
public Class ProofOfNotificationHandler {
    public ProofOfNotificationHandler () {
    }
   
    public void updateInstProd(list<PON__c> ponList) {
        set<string> pcsnSet= new set<string>();
        for( PON__c p : ponList){
            if(p.PCSN__c!=null){
                pcsnSet.add(p.PCSN__c);
            }
        }

        List<SVMXC__Installed_Product__c> ipList=[SELECT Id, PCSN__c FROM SVMXC__Installed_Product__c WHERE PCSN__c  in: pcsnSet];
        Map<String,Id> ipMap= new Map<String,Id>();
        for(SVMXC__Installed_Product__c ip :ipList) {
            ipMap.put(ip.PCSN__c,ip.Id);
        }

        //list<PON__c> newponList= new list<PON__c>();
        for( PON__c p : ponList){
            if(p.PCSN__c!=null){
                p.Installed_Product__c = ipMap.get(p.PCSN__c);
                //newponList.add(p); 
            } else{
                 p.Installed_Product__c = null;
            }   
        }
        //if(!newponList.isEmpty()){
            //update newponList;
        //}
     }    
}
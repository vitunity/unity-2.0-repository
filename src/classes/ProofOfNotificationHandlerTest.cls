@istest
public class ProofOfNotificationHandlerTest {
    static testmethod void updatefield(){
        id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id);
        objIP.ERP_Reference__c='Test123PON';
        insert objIP;   
        PON__c objPON = new PON__c(Account_Name__c='TestAcc',PCSN__c='Test123PON');
        insert objPON;
        ProofOfNotificationHandler objPONH = new ProofOfNotificationHandler();
        list <PON__c> pList= [SELECT Id, PCSN__c, Installed_Product__c FROM PON__c];
        objPONH.updateInstProd(pList);
    }
        

}
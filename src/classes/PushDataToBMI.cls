/**
 * Batch class syncs product region from Sf to BMI 
 * Batch to push all SF product region data to BMI
 */
global class PushDataToBMI implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	String query;
	private static string sID; 
	private BigMachines__c bmi;
	
	global PushDataToBMI() {
		query = 'Select id, Product__c,Product_Region__c.Product__r.Name , Type__c, Cost__c,'+ 
					'CurrencyIsoCode, Region__c '+
					'from Product_Region__c '+
					'where (Product__c != null OR  Product__c != \'\')';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		
		List<Product_Region__c> productRegionList = scope;
		getBMISessionID();
        
        sendInsertRequestProdRegion(productRegionList);
        
        deployProductRegion();
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	// Function to get the sessionID from BMI
    private void getBMISessionID() {
    	
    	bmi = BigMachines__C.getValues('BMILogin');
    	
        String xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        					'<soapenv:Header>'+
        						'<bm:category xmlns:bm="urn:soap.bigmachines.com">Security</bm:category>'+
        						'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        							'<bm:schemaLocation>'+bmi.SLSecurity__c+'Security.xsd</bm:schemaLocation>'+
        						'</bm:xsdInfo>'+
        					'</soapenv:Header>'+
        					'<soapenv:Body>'+
	        					'<bm:login xmlns:bm="urn:soap.bigmachines.com">'+
	        						'<bm:userInfo>'+
	        							'<bm:username>'+bmi.User_Name__c+'</bm:username>'+
                                        '<bm:password>'+bmi.Password__c+'</bm:password>'+
	        							'<bm:sessionCurrency/>'+
	        						'</bm:userInfo>'+
	        					'</bm:login>'+
        					'</soapenv:Body>'+
        				'</soapenv:Envelope>';
        String response = makeRequestNow(xml);
        sID = extractSessionId(response);
    }
	
	/**
	 * Parse authentication response and get sessionId for BMI
	 */
	private String extractSessionId(String response) {
        String[] arr = response.split('<bm:sessionId>');
        arr = arr[1].split('</bm:sessionId>');
        String sessionID = arr[0];
        return sessionID;
    }

    //  Function to send request to BMI for inserting ProductRegion
    private void sendInsertRequestProdRegion(List<Product_Region__c> productRegionList) {
        String AddStringXML ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        							'<soapenv:Header>'+
        								'<bm:userInfo xmlns:bm="urn:soap.bigmachines.com">'+
        									'<bm:sessionId>'+sID+'</bm:sessionId>'+
        								'</bm:userInfo>'+
        								'<bm:category xmlns:bm="urn:soap.bigmachines.com">Data Tables</bm:category>'+
        								'<bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com">'+
        									'<bm:schemaLocation>'+bmi.SLDataTables__c+'ProductRegion.xsd</bm:schemaLocation>'+
        								'</bm:xsdInfo>'+
        							'</soapenv:Header>'+
        							'<soapenv:Body>'+
        								'<bm:add xmlns:bm="urn:soap.bigmachines.com">'+
        									'<bm:DataTables bm:table_name="ProductRegion">';
        
                
        for(Product_Region__c pReg :  productRegionList){
            decimal prodCost = 0.0;
            if(pReg.Cost__c != null ) {
                prodCost = pReg.Cost__c;
            }
            string typeVal = '';
            if(pReg.Type__c != null && pReg.Type__c != ''){
                typeVal = pReg.Type__c.replaceAll('-',' ');
                typeVal = pReg.Type__c.replaceAll('–','');
                typeVal = string.escapeSingleQuotes(typeVal);
            }   
            AddStringXML = AddStringXML+ '<bm:each_record>'+
            									'<bm:product><![CDATA['+string.escapeSingleQuotes(pReg.Product__r.name)+']]></bm:product>'+
            									'<bm:type><![CDATA['+typeVal+']]></bm:type><bm:cost>'+prodCost+'</bm:cost>'+
            									'<bm:currencyISOCode>'+pReg.CurrencyIsoCode+'</bm:currencyISOCode>'+
            									'<bm:region><![CDATA['+(String.isBlank(pReg.Region__c)?'':pReg.Region__c)+']]></bm:region>'+
            									'<bm:validation_flag>0</bm:validation_flag>'+
            								'</bm:each_record>';
        }
        AddStringXML = AddStringXML+'</bm:DataTables></bm:add></soapenv:Body></soapenv:Envelope>';
        
        System.Debug('String check'+AddStringXML);
        
        String Response = makeRequestNow(AddStringXML);
    }

    //Deploy Product Region
    private void deployProductRegion (){
        String deployXML= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header><bm:userInfo xmlns:bm="urn:soap.bigmachines.com"><bm:sessionId>'+sID+'</bm:sessionId></bm:userInfo><bm:category xmlns:bm="urn:soap.bigmachines.com">Data Tables</bm:category><bm:xsdInfo xmlns:bm="urn:soap.bigmachines.com"><bm:schemaLocation>https://devvarian.bigmachines.com/bmfsweb/devvarian/schema/v1_0/datatables/ProductRegion.xsd</bm:schemaLocation></bm:xsdInfo></soapenv:Header><soapenv:Body><bm:deploy xmlns:bm="urn:soap.bigmachines.com"><bm:DataTables bm:table_name="ProductRegion"/></bm:deploy></soapenv:Body></soapenv:Envelope>';
        String Response = makeRequestNow(deployXML);
    }

    // Get session request between BMI and SFDC
    private String makeRequestNow(String xml){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('SOAPAction','Retrieve');
        req.setHeader('Accept-Encoding','gzip,deflate');
        req.setHeader('Content-Type','text/xml;charset=UTF-8');
        req.setHeader('User-Agent','Jakarta Commons-HttpClient/3.1');
        req.setBody(xml);
        req.setTimeout(120000);
        req.setEndpoint(bmi.Endpoint__c);

        string bodyRes = '';
        system.debug('llllllllll'+xml);
        try{
			HttpResponse res = h.send(req);
            bodyRes = res.getBody();
		}catch(System.CalloutException e) {
			System.debug('Callout error: '+ e+'----'+e.getLineNumber());
		}
        System.debug('Soap request:' + xml);
        System.debug('Soap response:' + bodyRes);
        return bodyRes;
	}
}
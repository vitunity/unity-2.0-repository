public with sharing class QueryUtilClass {

    public static string getCreatableFieldsSOQL(String objectName){
        
    	String query = 'SELECT ';

        if(String.isNotEmpty(objectName)){
         
	        // Get a map of field name and field token
	        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
	        Set<string> objectFields = new Set<string>();
	         
	        if (fMap != null){
	            for (Schema.SObjectField field : fMap.values()){ 
	                Schema.DescribeFieldResult fd = field.getDescribe(); 
	                if (fd.isCreateable()) {
	                    objectFields.add(fd.getName());
	                }
	            }
	        }
	         
	        if (!objectFields.isEmpty()){
	            for (String s: objectFields){
	                query += s + ',';
	            }
	            
	            query = query.removeEnd(',');
	             
	        }
	    }

	    return query;
         
    }
 
}
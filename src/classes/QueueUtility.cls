/*
 * Author Amitkumar Katre
 * Usage - QueueUtility.getInstance().get('SVMXC__Service_Order__c').get('CSA DK');
 */
public class QueueUtility {
	
    // private static variable referencing the class
    private static QueueUtility instance = null;
    public map<String,map<String,String>> queues;
    
    // a static method that returns the instance of the queues
    public static QueueUtility getInstance(){
        // lazy load the queues - only initialize if it doesn't already exist
        if(instance == null) instance = new QueueUtility();
        return instance;
    }
    
    // The constructor is private and initializes the id of the record type
    private QueueUtility(){
    	queues = new map<String,map<String,String>>();
    	list<QueueSobject > queueSobjectList = [Select q.SobjectType, q.Queue.Type, q.Queue.Name, q.QueueId From QueueSobject q ];
    	//system.debug('queueSobjectList==='+queueSobjectList.size());
    	for(QueueSobject queueSobjectObj : queueSobjectList){
    		if(queues.containsKey(queueSobjectObj.SobjectType)){
    			queues.get(queueSobjectObj.SobjectType).put(queueSobjectObj.Queue.Name,queueSobjectObj.QueueId);
    		}else{
    			map<String,String> maptemp = new map<String,String>{queueSobjectObj.Queue.Name => queueSobjectObj.Id};
    			queues.put(queueSobjectObj.SobjectType,maptemp);
    		}
    	}
    }
}
/***************************************************************************\
    @ Author                : Harvey Li
    @ Date                  : 04/03/2015
****************************************************************************/
@isTest 
public class QuickCase_test
{
    public static testMethod void testUpdateAccountsWithLocation()
    {
        Account account = new Account();
        account.Name = 'Acme';
        account.Country__c = 'China';
        Account account1 = new Account();
        account1.Name = 'Acme1';
        account1.Country__c = 'China';
        insert account; 
        insert account1;    
        updateAccountsWithLocation batch = new updateAccountsWithLocation();
        Database.executeBatch(batch);   
    }
    public static testMethod void testUpdateAssociatedAccount()
    {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        

        system.runas(systemuser)
        {
            //insert Account record
            Account varAcc = AccountTestData.createAccount();
            varAcc.AccountNumber = '12345A';
            insert varAcc;

            //insert ERP Partner record
            ERP_Partner__c varPartner = new ERP_Partner__c();
            varPartner.name = 'Test Partner';
            varPartner.Partner_Number__c = 'HH1122';
            insert varPartner;
            
            //insert ERP Partner Association record
            ERP_Partner_Association__c varAssociation = new ERP_Partner_Association__c();
            varAssociation.name = 'Test Association';
            varAssociation.ERP_Partner__c = varPartner.Id;
            varAssociation.Partner_Function__c = 'Z1=Site Partner';
            insert varAssociation;

            //insert ERP Org record
            ERP_Org__c varOrg = new ERP_Org__c();
            varOrg.name = 'Test Org';
            varOrg.Sales_Org__c = 'ABC123';
            insert varOrg;

            //insert country record
            Country__c varCountry = new Country__c();
            varCountry.name = 'India';
            varCountry.SAP_Code__c = 'A1B2C3';
            varCountry.ISO_Code_3__c = 'ABCTEST';
            insert varCountry;

            //insert Service Team record
            SVMXC__Service_Group__c ServiceTeam = SR_testdata.createServiceTeam();
            insert ServiceTeam;


            //insert location record
            SVMXC__Site__c varLoc = SR_testdata.createsite();
            varLoc.SVMXC__Account__c = varAcc.Id;
            varLoc.Country__c = varCountry.Id;
            varLoc.ERP_Country_Code__c = 'A1B2C3';
            varLoc.ERP_CSS_District__c = 'abc';
            varLoc.Sales_Org__c = 'ABC123';
            varLoc.SVMXC__Location_Type__c = 'Field';
            varLoc.Work_Center__c = 'H123H';
            insert varLoc;

            //update 1 location record
            varLoc.ERP_CSS_District__c = 'xyz';
            varLoc.ERP_Site_Partner_Code__c = 'HH1122';
            varLoc.SVMXC__Country__c = 'India';
            update varLoc;

            //update Account 3
            varAcc.ShippingCity = 'Test City';
            varAcc.ShippingStreet = 'Test Street';
            varAcc.ShippingState = 'Test State';
            varAcc.ShippingPostalCode = 'Test State';
            varAcc.ShippingCountry = 'Test Country';
            update varAcc;

            //update 2 location record
            varLoc.ERP_Site_Partner_Code__c = '12345A';
            varLoc.SVMXC__Country__c = 'A1B2C3';
            update varLoc;

            //update 3 location record
            varLoc.SVMXC__Country__c = 'ABCTEST';
            update varLoc;

            //update 4 location record
            varLoc.SVMXC__Country__c = 'ABC123';
            update varLoc;
            
            delete varLoc;
        }
    }
}
public without sharing class QuickCreateCaseCmpController {
    
    @AuraEnabled 
    public static Case getCase(String caseId){
        system.debug('Account Id===='+caseId);
        return [select Id,CreatedDate, Status, Account.Name, Priority, Subject, CaseNumber, Description, AccountId,
                ContactId, ProductSystem__c, OwnerId, ProductSystem__r.Name, Contact.Name,Owner.Name,Contact.Phone
                from Case 
                where Id =:caseId][0];
    }
    
    
    @AuraEnabled
    public static list<Contact> getContacts(String accountId){
        system.debug('Account Id===='+accountId);
        if(String.isBlank(accountId)) return null;
        list<Contact> contactList = new list<Contact>();
        contactList.addAll([select Id,FirstName, LastName, Phone, MobilePhone, Email, Title, AccountId,Account.Name,
                Functional_Role__c, Specialty__c, Email_Declined__c
                from Contact 
                where AccountId =:accountId ]);
        system.debug('contact list size =='+contactList.size());
        contactList.addAll([select Id,FirstName, LastName, Phone, MobilePhone, Email, Title, AccountId,Account.Name,
                Functional_Role__c, Specialty__c, Email_Declined__c
                from Contact 
                where Id in (select Contact__c from Contact_Role_Association__c where Account__c =: accountId)]);
        system.debug('contact list size =='+contactList.size());
        return contactList;
    }
    
    @AuraEnabled
    public static list<Contact> getContacts(String accountId, String searchStr){
        system.debug('Account Id===='+accountId);
        if(String.isBlank(accountId)) return null;
        String query = 'select Id,FirstName, LastName, Phone, MobilePhone, Email, Title, AccountId,Account.Name, '+
                   'Functional_Role__c, Specialty__c, Email_Declined__c '+
                   'from Contact  '+
                   'where AccountId =:accountId and Inactive_Contact__c = false ';
        if(String.isNotBlank(searchStr)){
            searchStr = '%'+searchStr+'%';
            query += ' and (FirstName like : searchStr or LastName like :searchStr or email like :searchStr or phone like :searchStr or Title like :searchStr ) ';
        }
        list<Contact> contactList = new list<Contact>();
        contactList.addAll((list<Contact>)Database.query(query));
        String queryCRA = 'select Id,FirstName, LastName, Phone, MobilePhone, Email, Title, AccountId,Account.Name, '+
                   'Functional_Role__c, Specialty__c, Email_Declined__c '+
                   'from Contact  '+
                   'where Id in (select Contact__c from Contact_Role_Association__c where Account__c =: accountId) and Inactive_Contact__c = false ';
        if(String.isNotBlank(searchStr)){
            searchStr = '%'+searchStr+'%';
            queryCRA += ' and (FirstName like : searchStr or LastName like :searchStr or email like :searchStr or phone like :searchStr or Title like :searchStr ) ';
        }
        contactList.addAll((list<Contact>)Database.query(queryCRA));
        for(Contact conObj : contactList){
            conObj.AccountId = accountId;
        }
        return contactList;
    }
    
    @AuraEnabled
    public static Contact getContact(String contactId){
        system.debug('Account Id===='+contactId);
        if(String.isBlank(contactId)) return null;
        return [select Id,FirstName, LastName, Phone, MobilePhone, Email, Title, AccountId,Account.Name,
                Functional_Role__c, Specialty__c, Email_Declined__c
                from Contact 
                where Id =:contactId][0];
    }
    
    @AuraEnabled
    public static Account getAccount(String accountId){
        system.debug('Account Id===='+accountId);
        return [select Id,Name
                from Account 
                where Id =:accountId][0];
    }
    
    
    @AuraEnabled
    public static list<Case> getCases(String accountId){
        system.debug('Account Id===='+accountId);
        list<Case> caseList = new list<Case>();
        return [select CreatedDate, Status, Priority, Subject, CaseNumber, Description, Account.Prefered_Language__c,
                ContactId, ProductSystem__c, OwnerId, ProductSystem__r.Name, Contact.Name,Owner.Name,
                Account.Special_Care_Instruction_New__c,Account.Preferred_Language__c,ProductSystem__r.SVMXC__Product__r.Name
                from Case 
                where AccountId =:accountId and AccountId != null and RecordType.developername = 'Helpdesk'
                and Status != 'Closed' and Status != 'Closed by Work Order' and Status != 'Cancelled'];
    }
    
    @AuraEnabled
    public static String saveCase(Case caseData){
       system.debug('case data ='+caseData);
       try{
           upsert caseData;
       }catch(Exception e){
           CustomExceptionData data = new CustomExceptionData(
                'code',ErrorMessageHandler.getMessageText(e), 0000);
            throw new AuraHandledException(JSON.serialize(data));           
       }
       
       return caseData.Id;
    }
    
    @AuraEnabled
    public static String saveCase(Case caseData, Boolean isCRA, String craRole, String craPhone){
       system.debug('case data ='+caseData);
       try{
           upsert caseData;
           if(isCRA)
           insertCRA(caseData.ContactId,caseData.AccountId,craRole,craPhone);
       }catch(Exception e){
           CustomExceptionData data = new CustomExceptionData(
                'code',ErrorMessageHandler.getMessageText(e), 0000);
            throw new AuraHandledException(JSON.serialize(data));           
       }
       
       return caseData.Id; 
    }
    
    @Future(callout=true)
    private static void insertCRA(String contactId, String accountId, String craRole, String craPhone){
        insert new Contact_Role_Association__c(Contact__c =contactId, 
                                               Account__c = accountId,
                                               Site_Phone_Number__c= craPhone,
                                               Role__c = craRole);
    }
                                               
    @AuraEnabled
    public static String saveContact(Contact conData){
       system.debug('conData data ='+conData);
       try{
           upsert conData;
       }catch(Exception e){
           String mshg = ErrorMessageHandler.getMessageText(e);
           if(mshg.containsIgnoreCase('use one of these records')){
               Contact conObj = [select FirstName,LastName, Account.Name from Contact where email =: conData.Email];
               mshg = 'Email in use by '+ conObj.FirstName +' '+conObj.LastName+'-'+conObj.Account.Name;
           }
           CustomExceptionData data = new CustomExceptionData(
                'code',mshg, 0000);
            throw new AuraHandledException(JSON.serialize(data));           
       }
       return conData.Id;
    }
    
    @AuraEnabled
    public static String getIPs(String accountId, String searchStr){
        system.debug('Account Id===='+accountId);
        if(String.isBlank(accountId)) return null;
        list<IPDataWrap> ipDataList = new list<IPDataWrap> ();
        list<SVMXC__Installed_Product__c> ipLIst = new list<SVMXC__Installed_Product__c>();
        String query = 'Select Id FROM SVMXC__Installed_Product__c where SVMXC__Company__c =:accountId ';
        if(String.isNotBlank(searchStr)){
            searchStr = '%'+searchStr+'%'; 
            query +=  ' and Name like :searchStr ' ;
        }else{
            query += ' AND SVMXC__Top_Level__c = null ';
        }   
        list<SVMXC__Installed_Product__c> newipLIst = Database.query(query);
        ipLIst = [Select Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c, Billing_Type__c,
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c,Expiration_Date_Text__c,
                SVMXC__Product__c,SVMXC__Service_Contract__c,SVMXC__Site__r.Name,PCode_is_Reference_Only__c,
                (SELECT Id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,  SVMXC__Status__c,SVMXC__Product__c,SVMXC__Service_Contract__c, 
                Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,
                SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,
                SVMXC__Service_Contract__r.SVMXC__Start_Date__c ,Expiration_Date_Text__c, Billing_Type__c,
                SVMXC__Site__r.Name,PCode_is_Reference_Only__c
                FROM  R00N70000001hzcvEAA__r) 
                FROM SVMXC__Installed_Product__c where Id in : newipLIst];
        for(SVMXC__Installed_Product__c ipObj : ipLIst){
            IPDataWrap pObj = new IPDataWrap();
            pObj.name = ipObj.Name;
            pObj.productName = ipObj.SVMXC__Product__r.name;
            pObj.productNameId = ipObj.SVMXC__Product__c;
            pObj.productURL = '/'+ipObj.SVMXC__Product__c;
            pObj.ipId = ipObj.Id;
            pObj.expDate = ipObj.Expiration_Date_Text__c;
            pObj.contractName = ipObj.SVMXC__Service_Contract__r.Name;
            pObj.contractId = ipObj.SVMXC__Service_Contract__c;
            pObj.contractUrl = '/'+ipObj.SVMXC__Service_Contract__c;
            pObj.billingType = ipObj.Billing_Type__c;
            pObj.location = ipObj.SVMXC__Site__r.Name;
            pObj.locationId = ipObj.SVMXC__Site__c;
            pObj.locationUrl = '/'+ipObj.SVMXC__Site__c; 
            pObj.Status = ipObj.SVMXC__Status__c;
            pObj.children =  new list<IPDataWrap>();
            pObj.pcodeRef = ipObj.PCode_is_Reference_Only__c == true?'Yes':'No';
            for(SVMXC__Installed_Product__c ipObjChild : ipObj.R00N70000001hzcvEAA__r){
                IPDataWrap pcObj = new IPDataWrap();
                pcObj.name = ipObjChild.Name;
                pcObj.productName = ipObjChild.SVMXC__Product__r.name;
                pcObj.productNameId = ipObjChild.SVMXC__Product__c;
                pcObj.productURL = '/'+ipObjChild.SVMXC__Product__c;
                pcObj.ipId = ipObjChild.Id;
                pcObj.expDate = ipObjChild.Expiration_Date_Text__c;
                pcObj.contractName = ipObjChild.SVMXC__Service_Contract__r.Name;
                pcObj.contractId = ipObjChild.SVMXC__Service_Contract__c;
                pcObj.contractUrl = '/'+ipObjChild.SVMXC__Service_Contract__c;
                pcObj.billingType = ipObjChild.Billing_Type__c;
                pcObj.location = ipObjChild.SVMXC__Site__r.Name;
                pcObj.locationId = ipObjChild.SVMXC__Site__c;
                pcObj.locationUrl = '/'+ipObjChild.SVMXC__Site__c; 
                pcObj.Status = ipObjChild.SVMXC__Status__c;
                pcObj.pcodeRef = ipObjChild.PCode_is_Reference_Only__c == true?'Yes':'No';
                pObj.children.add(pcObj);
            }
            ipDataList.add(pObj);
        }
        String s = JSON.serialize(ipDataList);
        s = s.replaceAll('children', '_children');
        return s;
    }
    
    public class IPDataWrap{
        public String billingType;
        public String status;
        public String name;
        public String productName;
        public String productURL;
        public String productNameId;
        public String ipId;
        public String expDate;
        public String contractName;
        public String contractURL;
        public String contractId;
        public String location;
        public String locationId;
        public String locationUrl;
        public String pcodeRef;
        public list<IPDataWrap> children;
     }
    
}
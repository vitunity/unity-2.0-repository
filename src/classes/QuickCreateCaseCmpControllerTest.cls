@isTest
public class QuickCreateCaseCmpControllerTest {
    public static Id recTypeIDTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
    public static ERP_Timezone__c etz ;
    public static Account acc ;
    public static SVMXC__Site__c varLoc;
    public static Contact con ;
    public static SVMXC__Service_Group__c objServTeam;
    public static SVMXC__Service_Group_Members__c techEqipt,techEqipt2 ;
    public static SVMXC__Installed_Product__c objIP, objIP2 ;
    public static Case testcase, testcase2;
    public static SVMXC__Case_Line__c caseLine ;
    public static ERP_NWA__c erpnwa;
    
    static
    {
        etz = new ERP_Timezone__c(Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)', name='Aussa');
        insert etz;
        
        // insertAccount
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        acc.BillingCity = 'Anytown';
        acc.State_Province_Is_Not_Applicable__c=true;
        insert acc;
        
        //insert location
        varLoc = new SVMXC__Site__c(SVMXC__Account__c = acc.id, Plant__c = 'testPlant', SVMXC__Service_Engineer__c = userInfo.getUserID());
        insert varLoc;
        
        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
                          MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con; 
        
        //insert Service Team
        objServTeam = new SVMXC__Service_Group__c(Name = 'test team' , District_Manager__c = userinfo.getUserID());
        insert objServTeam;
        
        //insert technician
        techEqipt = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
                                                        Name = 'Test Technician', User__c = userInfo.getUserId(),ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt;
        
        //insert technician1
        techEqipt2 = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
                                                         Name = 'Test Technician2', User__c = userinfo.getUserID(), ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt2;
        System.debug('techEqipt2----->'+techEqipt2);
        
        // insert parent IP var loc
        objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Site__c =varLoc.id, SVMXC__Preferred_Technician__c = techEqipt.id,
                                                Service_Team__c =objServTeam.Id );
        insert objIP;
        
        // insert parent IP var loc
        objIP2 = new SVMXC__Installed_Product__c(Name='H134072', SVMXC__Status__c ='Installed', SVMXC__Preferred_Technician__c = techEqipt2.id,
                                                 Service_Team__c =objServTeam.Id );
        insert objIP2;
        
        // insert Case
        testcase = new case(recordtypeId= recHD, AccountId =acc.id, Priority='Medium', SVMXC__Top_Level__c = objIP.id,contactId = con.id, SVMXC__Billing_Type__c = 'P - Paid Service',ProductSystem__c = objIP.id);
        insert testcase;
        // insert Case2
        testcase2 = new case(recordtypeId= recHD, AccountId =acc.id, Priority='Medium', SVMXC__Top_Level__c = objIP2.id, contactId = con.id);
        insert testcase2;
        
        erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC',ERP_Std_Text_Key__c='INST004');
        insert erpnwa;
        
        caseLine = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl',Billing_Type__c='I – Installation', SVMXC__Case__c = testcase.id, Hours__c = 1, Start_Date_time__c = system.now(),ERP_NWA__c = erpnwa.Id);
        
        Billing_Types__c bt = new Billing_Types__c(name='Others', Caseline_Billing_Types__c ='W – Warranty;P – Paid Service;I – Installation;N – Waived;X – Cross;C – Contract;O - Other');
        insert bt;
        
    }
    public static testMethod void test_QuickCreateCase1() // covering functionality when Selected Employee does not have any techinician associated
    {  
        String accountId = acc.Id;
        QuickCreateCaseCmpController obj = new QuickCreateCaseCmpController();
        QuickCreateCaseCmpController.getCase(testcase.Id);
        QuickCreateCaseCmpController.getAccount(accountId);
        QuickCreateCaseCmpController.getCases(accountId);
        QuickCreateCaseCmpController.getIPs(accountId, null);
        QuickCreateCaseCmpController.getContact(con.Id);
        QuickCreateCaseCmpController.getContacts(accountId);
        QuickCreateCaseCmpController.getContacts(accountId, 'test');
    }
    
    public static testMethod void test_QuickCreateCase2() // covering functionality when Selected Employee does not have any techinician associated
    {  
        test.startTest();
        String accountId = acc.Id;
        QuickCreateCaseCmpController obj = new QuickCreateCaseCmpController();
        QuickCreateCaseCmpController.saveContact(con);
        QuickCreateCaseCmpController.saveCase(testcase); 
        test.stopTest();
    }
    public static testMethod void test_QuickCreateCase3() // covering functionality when Selected Employee does not have any techinician associated
    {  
        String accountId = acc.Id;
        testcase.AccountId = accountId;
        testCase.ContactId = con.id;
        QuickCreateCaseCmpController.saveCase(testcase,true,'crarole','4567808765');        
    }
    public static testMethod void test_QuickCreateCase4() // covering functionality when Selected Employee does not have any techinician associated
    {  
        String accountId = acc.Id;
        testcase.AccountId = accountId;
        testCase.ContactId = con.id;
        Try{
            QuickCreateCaseCmpController.saveCase(null,true,'crarole','4567808765');
        }catch(Exception e){
            
        }
        Try{
            QuickCreateCaseCmpController.saveCase(null);  
        }catch(Exception e){
            
        }Try{
            QuickCreateCaseCmpController.saveContact(null);  
        }catch(Exception e){
            
        }
        
    }
}
/**
 * Handler for all quote product pricing trigger contexts and actions
 */
public with sharing class QuoteProductPricingTriggerHandler {
	
	/**
	 * Update Quote Product on Quote Product Pricing record by using Doc Number
	 */
	public static void updateQuoteProductOnQPP(List<Quote_Product_Pricing__c> quoteProductPricings){
		
		Set<Id> quoteIds = new Set<Id>();
		for(Quote_Product_Pricing__c quoteProductPricing : quoteProductPricings){
			if(quoteProductPricing.BMI_Quote__c != null){
				quoteIds.add(quoteProductPricing.BMI_Quote__c);
			}
		}
		
		System.debug('----quoteIds'+quoteIds);
		
		List<BigMachines__Quote_Product__c> quoteProducts = [SELECT Id,Doc_Number__c,BigMachines__Quote__c
																FROM BigMachines__Quote_Product__c
																WHERE BigMachines__Quote__c IN :quoteIds];
		
		System.debug('----quoteProducts'+quoteProducts);
		// Map to hold Quote Product id with Doc Number
		Map<Integer,BigMachines__Quote_Product__c> quoteProductMap = new Map<Integer,BigMachines__Quote_Product__c>();
		
		for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
			if(quoteProduct.Doc_Number__c != null){
				quoteProductMap.put(Integer.valueOf(quoteProduct.Doc_Number__c), quoteProduct);
			}
		}
		
		System.debug('----quoteProductMap'+quoteProductMap);
		for(Quote_Product_Pricing__c quoteProductPricing : quoteProductPricings){
			System.debug('----quoteProductPricing.Doc_Number__c'+quoteProductPricing.Doc_Number__c);
			if(quoteProductMap.containsKey(Integer.valueOf(quoteProductPricing.Doc_Number__c))){
				BigMachines__Quote_Product__c quoteProduct = quoteProductMap.get(Integer.valueOf(quoteProductPricing.Doc_Number__c));
				if(quoteProduct.BigMachines__Quote__c == quoteProductPricing.BMI_Quote__c){
					quoteProductPricing.Quote_Product__c = quoteProduct.Id;
				}
			}
		}
	}
}
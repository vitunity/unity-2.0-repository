@isTest(seeAllData=true)
private class QuoteProductPricingTriggerHandlerTest {

    static testMethod void testUpdateQuoteProductOnQPP() {
        SR_PrepareOrderControllerTest.setUpServiceTestdata();
        
        List<BigMachines__Quote_Product__c> quoteProducts = [SELECT Id,Doc_Number__c 
                                                                FROM BigMachines__Quote_Product__c
                                                                WHERE BigMachines__Quote__c =: SR_PrepareOrderControllerTest.serviceQuote.Id];
        
        List<Quote_Product_Pricing__c> quoteProductPricings = new List<Quote_Product_Pricing__c>();
        
        Integer counter = 0;
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            if(counter == 0){
                Quote_Product_Pricing__c quoteProductPricing = new Quote_Product_Pricing__c();
                quoteProductPricing.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing.Name= 'TEST';
                quoteProductPricing.Doc_Number__c = 2;
                quoteProductPricings.add(quoteProductPricing);
            }else if(counter == 1){
                Quote_Product_Pricing__c quoteProductPricing1 = new Quote_Product_Pricing__c();
                quoteProductPricing1.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing1.Name= 'TEST';
                quoteProductPricing1.Doc_Number__c = 3;
                Quote_Product_Pricing__c quoteProductPricing2 = new Quote_Product_Pricing__c();
                quoteProductPricing2.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing2.Name= 'TEST';
                quoteProductPricings.add(quoteProductPricing1);
                quoteProductPricings.add(quoteProductPricing2);
            }
            counter++;
        }
        
        insert quoteProductPricings;
        
        List<Quote_Product_Pricing__c> qpps = [SELECT Id, Quote_Product__c,Doc_Number__c,Quote_Product__r.Doc_Number__c
                                                FROM Quote_Product_Pricing__c
                                                Where BMI_Quote__c =: SR_PrepareOrderControllerTest.serviceQuote.Id];
        System.debug('----quoteProductPricings'+quoteProductPricings+'---quoteProductPricings.size'+quoteProductPricings.size());
        for(Quote_Product_Pricing__c qpp : qpps){
            if(qpp.Doc_Number__c !=null){
                System.assertEquals(qpp.Doc_Number__c, qpp.Quote_Product__r.Doc_Number__c);
            }else{
                System.assert(qpp.Quote_Product__c == null);
            }
        }
    }
}
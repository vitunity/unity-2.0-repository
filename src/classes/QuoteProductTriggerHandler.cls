/**
 * Handles trigger functions
 */
public with sharing class QuoteProductTriggerHandler {
    
    /**
     * Updates erp site partner look up on quote product if site partner selected 
     * while preparing service or combined quote quote
     */
    private static Set<String> sitePartnerNumbers;   
    private static Map<String,Id> erpPartnerIds;
    private static Set<Id> quoteIds;
    private static Set<Id> productIds;
    private static List<BigMachines__Quote_Product__c> serviceQuoteProducts;
    private static Map<Id, String> quoteRegions;
    
    public static void initialize(List<BigMachines__Quote_Product__c> quoteProducts){
        sitePartnerNumbers = new Set<String>();
        erpPartnerIds = new Map<String,Id>();
        quoteIds = new Set<Id>();
        productIds = new Set<Id>();
        serviceQuoteProducts = new List<BigMachines__Quote_Product__c>();
        
        Map<String, ForecastBlockedParts__c> forecastBlockedParts = ForecastBlockedParts__c.getAll();
        Map<String,String> partModelVariations = getPartModelVariations();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            quoteIds.add(quoteProduct.BigMachines__Quote__c);
            productIds.add(quoteProduct.BigMachines__Product__c);
            if(!String.isBlank(quoteProduct.ERP_Site_Partner_Number__c)){
                sitePartnerNumbers.add(quoteProduct.ERP_Site_Partner_Number__c);
                serviceQuoteProducts.add(quoteProduct);
            }
            String partModelVariationKey = quoteProduct.Name+quoteProduct.QP_Product_Family__c+quoteProduct.QP_Product_Line__c+quoteProduct.QP_Product_Model__c;
            if(partModelVariations.containsKey(partModelVariationKey)){
                quoteProduct.Forecast_BMI_Product_Category__c = partModelVariationKey;
                updateProductCategories(quoteProduct, partModelVariations.get(partModelVariationKey));
            }
        }
        
        getQuotes(quoteIds);
        
        for(ERP_Partner_Association__c erpAssociation : [
            SELECT Id,ERP_Partner__c,ERP_Partner_Number__c
            FROM ERP_Partner_Association__c
            WHERE ERP_Partner_Number__c IN: sitePartnerNumbers
            AND Partner_Function__c = 'Z1=Site Partner'
        ]){
            erpPartnerIds.put(erpAssociation.ERP_Partner_Number__c, erpAssociation.ERP_Partner__c);
        }
    }
    
    /**
     * Update product categories fields with matching part model variation
     */
    private static void updateProductCategories(BigMachines__Quote_Product__c quoteProduct, String expectedModel){
        if(!String.isBlank(expectedModel)){
            List<String> expectedModels = expectedModel.split('-');
            if(expectedModels.size()==3){
                quoteProduct.QP_Product_Family__c = expectedModels[0];
                quoteProduct.QP_Product_Line__c = expectedModels[1];
                quoteProduct.QP_Product_Model__c = expectedModels[2];
                quoteProduct.Product_Line_Key__c = quoteProduct.QP_Product_Family__c+quoteProduct.QP_Product_Line__c;
            }
        }   
    }
    
    public static void updateERPSitePartner(){
        
        for(BigMachines__Quote_Product__c quoteProduct : serviceQuoteProducts){
            if(erpPartnerIds.containsKey(quoteProduct.ERP_Site_Partner_Number__c)){
                quoteProduct.ERP_Site_Partner__c = erpPartnerIds.get(quoteProduct.ERP_Site_Partner_Number__c);
            }
        }
    }
    
    /**
     * Create quote product parter records using site partners selected while creating BMI quote
     */
    public static void createQuoteProductPartners(Map<Id, BigMachines__Quote_Product__c> oldQuoteProducts){
        
        
        Map<String,Quote_Product_Partner__c> existingQuoteProductPartners = new Map<String,Quote_Product_Partner__c>();
        
        // Updated by Ajinkya to fix a Bug related to Deletion of Site Partners
        List<Quote_Product_Partner__c> quoteProductPartners = [SELECT Id,ERP_Partner_Function__c,ERP_Partner_Number__c,Quote__c 
                                                                FROM Quote_Product_Partner__c 
                                                                WHERE Quote__c in: quoteIds 
                                                                AND ERP_Partner_Function__c = 'Z1'
                                                                AND Quote_Product__c != null];
        
        for(Quote_Product_Partner__c quotePartner : quoteProductPartners){
            existingQuoteProductPartners.put(quotePartner.Quote__c+''+quotePartner.ERP_Partner_Number__c,quotePartner);
        }
        
        List<BigMachines__Quote_Product__c> quoteProductList = [SELECT Id, BigMachines__Quote__c, ERP_Site_Partner_Number__c 
                                                                FROM BigMachines__Quote_Product__c 
                                                                WHERE BigMachines__Quote__c in: quoteIds 
                                                                AND ERP_Site_Partner_Number__c != null];
        if(oldQuoteProducts == null){
            insertQuoteProductPartners(serviceQuoteProducts,existingQuoteProductPartners);
        }else{
            updateQuoteProductPartners(serviceQuoteProducts,oldQuoteProducts,existingQuoteProductPartners,quoteProductList);
        }
    }
    
    /**
     * If quote product has site partner information then create related quote product partner
     */
    private static void insertQuoteProductPartners(List<BigMachines__Quote_Product__c> quoteProducts, 
                                                    Map<String,Quote_Product_Partner__c> existingQuotePartners){
        
        List<Quote_Product_Partner__c> insertPartnerList = new List<Quote_Product_Partner__c>();
        Map<String,Quote_Product_Partner__c> quoteProductPartners = new Map<String,Quote_Product_Partner__c>();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            if(!quoteProductPartners.containsKey(quoteProduct.BigMachines__Quote__c+''+quoteProduct.ERP_Site_Partner_Number__c) 
            && !existingQuotePartners.containsKey(quoteProduct.BigMachines__Quote__c+''+quoteProduct.ERP_Site_Partner_Number__c)){
                    
                    Quote_Product_Partner__c qPPInsert = getQuoteProductPartner(quoteProduct);
                    insertPartnerList.add(qPPInsert);
                    quoteProductPartners.put(qPPInsert.Quote__c+''+qPPInsert.ERP_Partner_Number__c,qPPInsert);
            }   
        }
        
        if(!insertPartnerList.isEmpty())
            insert insertPartnerList;
    }
    
    /**
     * Record new site partner after quote product's site partner details changed
     */
    private static void updateQuoteProductPartners(List<BigMachines__Quote_Product__c> quoteProducts,
                                                    Map<Id,BigMachines__Quote_Product__c> oldQuoteProducts,
                                                    Map<String,Quote_Product_Partner__c> existingQuotePartners,
                                                    /*Map<String,Id> erpPartnerIds,*/
                                                    List<BigMachines__Quote_Product__c> allQuoteProducts){
        
        List<Quote_Product_Partner__c> insertPartnerList = new List<Quote_Product_Partner__c>();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            BigMachines__Quote_Product__c oldQuoteProduct = oldQuoteProducts.get(quoteProduct.Id);
            if(quoteProduct.ERP_Site_Partner_Number__c != oldQuoteProduct.ERP_Site_Partner_Number__c){
                
                if(!existingQuotePartners.containsKey(quoteProduct.BigMachines__Quote__c+''+quoteProduct.ERP_Site_Partner_Number__c)){
                    insertPartnerList.add(getQuoteProductPartner(quoteProduct)); 
                }
            }
        }
        
        if(!insertPartnerList.isEmpty())
            insert insertPartnerList;
            
        deleteQuoteProductPartners(allQuoteProducts, existingQuotePartners);
    }
    
    /**
     * After site partner updated on quote product delete earlier recorded quote product partner
     */
    private static void deleteQuoteProductPartners(List<BigMachines__Quote_Product__c> quoteProductList, Map<String,Quote_Product_Partner__c> existingQuotePartners){
        
        List<Quote_Product_Partner__c> quotePartners = new List<Quote_Product_Partner__c>();
        Set<String> allQuoteProducts = new Set<String>();
                                                        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProductList){
            allQuoteProducts.add(quoteProduct.BigMachines__Quote__c+''+quoteProduct.ERP_Site_Partner_Number__c);
        }
        
        for(String quotePartner : existingQuotePartners.keySet()){
            if(!allQuoteProducts.contains(quotePartner)){
                quotePartners.add(existingQuotePartners.get(quotePartner));
            }
        }
        
        if(!quotePartners.isEmpty()){
            delete quotePartners;
        }
    }
    
    private static Quote_Product_Partner__c getQuoteProductPartner(BigMachines__Quote_Product__c quoteProduct){
        Quote_Product_Partner__c qPPInsert = new Quote_Product_Partner__c();
        qPPInsert.ERP_Partner_Number__c = quoteProduct.ERP_Site_Partner_Number__c;
        qPPInsert.ERP_Partner_Function__c = 'Z1';
        qPPInsert.Quote__c = quoteProduct.BigMachines__Quote__c;
        qPPInsert.ERP_Partner__c = quoteProduct.ERP_Site_Partner__c;
        qPPInsert.Quote_Product__c = quoteProduct.Id;
        if(erpPartnerIds.containsKey(qPPInsert.ERP_Partner_Number__c)){
            qPPInsert.ERP_Partner__c = erpPartnerIds.get(qPPInsert.ERP_Partner_Number__c);
        }
        return qPPInsert;
    }
    
    /**
     * Update forecast booking value on quote product beased on relevance attribute on product
     */
     public static void updateForecastBookingValue(List<Bigmachines__Quote_Product__c> quoteProducts){
        Map<Id,Product2> products = new Map<Id,Product2>([
            SELECT Id,Relevant_Non_Relevant__c
            FROM Product2
            WHERE Id IN:productIds
        ]);
        
        Map<String, ForecastBlockedParts__c> forecastBlockedParts = ForecastBlockedParts__c.getAll();
        
        for(Bigmachines__Quote_Product__c quoteProduct:quoteProducts){
            Product2 product = products.get(quoteProduct.BigMachines__Product__c);
            if(product!=null){
                if(String.isBlank(product.Relevant_Non_Relevant__c) || product.Relevant_Non_Relevant__c == 'Relevant'){
                    quoteProduct.Forecast_Booking_Value__c = quoteProduct.Net_Booking_Val__c;
                    if(quoteProduct.Net_Booking_Val__c > 0){
                        quoteProduct.Forecast_Net_Booking_Value__c = quoteProduct.Net_Booking_Val__c;
                    }else{
                        quoteProduct.Forecast_Net_Booking_Value__c = quoteProduct.Booking_Value_v1__c;
                    }
                }else if(product.Relevant_Non_Relevant__c == 'Not Relevant' || product.Relevant_Non_Relevant__c == 'Non-relevant'){
                    quoteProduct.Forecast_Booking_Value__c = quoteProduct.Booking_Value_v1__c;
                    quoteProduct.Forecast_Net_Booking_Value__c = 0.0;
                } 
            }
            if(quoteRegions.containsKey(quoteProduct.BigMachines__Quote__c)){
                String quoteRegion = quoteRegions.get(quoteProduct.BigMachines__Quote__c);
                //Update forecast blocked part flag on quote product
                if(forecastBlockedParts.containsKey(quoteProduct.Name)){
                    ForecastBlockedParts__c blockedPartObj = forecastBlockedParts.get(quoteProduct.Name);
                    if(!String.isBlank(blockedPartObj.Region__c) && blockedPartObj.Region__c.contains(quoteRegion)){
                        quoteProduct.Forecast_Blocked_Part__c = true;
                    }
                }
            }
        }
     }
    
    /**
     * Get quote region map
     */
    private static Map<Id,String> getQuotes(Set<Id> quoteIds){
        quoteRegions = new Map<Id,String>();
        for(BigMachines__Quote__c bmiQuote : [
            SELECT Id, BMI_Region__c
            FROM Bigmachines__Quote__c
            WHERE Id IN:quoteIds
        ]){
            if(bmiQuote.BMI_Region__c == 'NA' || bmiQuote.BMI_Region__c == 'LAT'){
                quoteRegions.put(bmiQuote.Id, 'Americas');
            }else if(bmiQuote.BMI_Region__c == 'AUS' || bmiQuote.BMI_Region__c == 'PAC' || bmiQuote.BMI_Region__c == 'CHN' || bmiQuote.BMI_Region__c == 'JPN'){
                quoteRegions.put(bmiQuote.Id, 'APAC');
            }else if(bmiQuote.BMI_Region__c == 'EURE' || bmiQuote.BMI_Region__c == 'EURW' || bmiQuote.BMI_Region__c == 'BA'){
                quoteRegions.put(bmiQuote.Id, 'EMEA');
            }
        }
        return quoteRegions;
    }
    
    /**
     * Part model variations map
     */
     private static Map<String,String> getPartModelVariations(){
        Map<String,String> partModelVariations = new Map<String,String>();
        for(Forecast_Part_Variation__c partVariation : [
            SELECT Part_Number__c,Current_Product_Family__c, Current_Product_Line__c, Current_Product_Model__c, 
            Expected_Product_Family__c, Expected_Product_Line__c, Expected_Product_Model__c
            FROM Forecast_Part_Variation__c
        ]){
            partModelVariations.put(
                partVariation.Part_Number__c+partVariation.Current_Product_Family__c+partVariation.Current_Product_Line__c+partVariation.Current_Product_Model__c,
                partVariation.Expected_Product_Family__c+'-'+partVariation.Expected_Product_Line__c+'-'+partVariation.Expected_Product_Model__c
            );
        }
        return partModelVariations;
     }
}
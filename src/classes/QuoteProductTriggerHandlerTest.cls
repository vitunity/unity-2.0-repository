@isTest(seeAllData=true)
private class QuoteProductTriggerHandlerTest {

    static testMethod void testUpdateERPSitePartner() {
        SR_PrepareOrderControllerTest.setUpServiceTestdata();
        
        List<BigMachines__Quote_Product__c> quoteProducts = [SELECT Id, ERP_Site_Partner__c, ERP_Site_Partner__r.Name
                                                                FROM BigMachines__Quote_Product__c
                                                                WHERE ERP_Site_Partner_Number__c = 'Service1212'];
        
        System.debug('----quoteProducts'+quoteProducts);
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            System.assertEquals(quoteProduct.ERP_Site_Partner__r.Name, 'Sampple Partner');
        }
    }
    
    static testMethod void testCreateQuoteProductPartners(){
        SR_PrepareOrderControllerTest.setUpServiceTestdata();
        
        List<QUote_Product_Partner__c> quoteProductPartners = [SELECT Id,ERP_Partner_Number__c,Quote_Product__c 
                                                                FROM QUote_Product_Partner__c
                                                                WHERE Quote__c =:SR_PrepareOrderControllerTest.serviceQuote.Id];
        System.assertEquals(1,quoteProductPartners.size());
        System.assertEquals('Service1212',quoteProductPartners[0].ERP_Partner_Number__c);
        
        List<BigMachines__Quote_Product__c> quoteProducts = [SELECT Id,ERP_Site_Partner_Number__c
                                                                FROM BigMachines__Quote_Product__c
                                                                WHERE BigMachines__Quote__c =:SR_PrepareOrderControllerTest.serviceQuote.Id];
                                                                
        quoteProducts[0].ERP_Site_Partner_Number__c = 'TEST 1212';
        quoteProducts[1].ERP_Site_Partner_Number__c = 'TEST 1313';
        update quoteProducts;
        
        List<QUote_Product_Partner__c> quoteProductPartners1 = [SELECT Id,ERP_Partner_Number__c,Quote_Product__c
                                                                FROM QUote_Product_Partner__c
                                                                WHERE Quote__c =:SR_PrepareOrderControllerTest.serviceQuote.Id];
        
        System.assertEquals(2,quoteProductPartners1.size());
        
        for(Quote_Product_Partner__c quoteProductPartner : quoteProductPartners1){
            if(quoteProductPartner.Quote_Product__c == quoteProducts[0].Id){
                System.assertEquals('TEST 1212',quoteProductPartner.ERP_Partner_Number__c);
            }else{
                System.assertEquals('TEST 1313',quoteProductPartner.ERP_Partner_Number__c);
            }
        }
    }
    
    static testmethod void testProductsTrigger(){
        Test.startTest();
            Product2 varianProduct1 = new Product2();
            varianProduct1.Name = 'Sample Service Product1';
            varianProduct1.IsActive = true;
            varianProduct1.ProductCode = 'Test Service Code1';
            
            Product2 varianProduct2 = new Product2();
            varianProduct2.Name = 'Sample Service Product2';
            varianProduct2.IsActive = true;
            varianProduct2.ProductCode = 'Test Service Code2';
            insert new List<Product2>{varianProduct1, varianProduct2};
            
            List<Product2> products = [SELECT Id,Relevant_Non_Relevant__c FROM Product2 WHERE Id=:varianProduct1.Id Or Id=:varianProduct2.Id];
            products[0].Relevant_Non_Relevant__c = 'Relevant';
            products[1].Relevant_Non_Relevant__c = 'Relevant';
            update products;
        Test.stopTest();
    }
}
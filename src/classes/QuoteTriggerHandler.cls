/**
 * @Author Krishna Katve
 * @Description Handles trigger events for quote object
 */
public with sharing class QuoteTriggerHandler {

      /**
   * Update updateOldContractDetails
   */
   public static void updateOldContractDetails(List<BigMachines__Quote__c> quotes){
                
        map<string, BigMachines__Quote__c> parentQuoteMap = new map<string, BigMachines__Quote__c>();
        set<string> parentQuoteSet = new set<string>(); 
        list<BigMachines__Quote__c> parentQuote = new list<BigMachines__Quote__c>();
        for(BigMachines__Quote__c newQuote: quotes){ 
            parentQuoteSet.add(newQuote.Parent_Quote__c);
        }
       parentQuote =  [Select Id, Name, New_Contract_Total__c, New_Discount__c,  New_Target_Percentage__c
                                                From BigMachines__Quote__c
                                                Where Name IN: parentQuoteSet];
        
        for(BigMachines__Quote__c quote : parentQuote){
            parentQuoteMap.put(quote.Name,quote);
        }
        
        system.debug('==parentQuoteMap =='+parentQuoteMap );
        for(BigMachines__Quote__c newQuote: quotes){            
            
            system.debug('Test Ajinkya'+newQuote.Parent_Quote__c);
            
            if(parentQuoteMap != null && newQuote.Amendment__c == true && parentQuoteMap.containsKey(newQuote.Parent_Quote__c)){
                if((parentQuoteMap.get(newQuote.Parent_Quote__c).New_Contract_Total__c <> null ) 
                || (parentQuoteMap.get(newQuote.Parent_Quote__c).New_Discount__c <> null ) 
                || (parentQuoteMap.get(newQuote.Parent_Quote__c).New_Target_Percentage__c <> null)){
                
                 
                
                    newQuote.Old_Contract_Total__c       = parentQuoteMap.get(newQuote.Parent_Quote__c).New_Contract_Total__c;
                    newQuote.Old_Discount__c             = parentQuoteMap.get(newQuote.Parent_Quote__c).New_Discount__c;
                    newQuote.Old_Target_Percentage__c    = parentQuoteMap.get(newQuote.Parent_Quote__c).New_Target_Percentage__c;    
                }
            }
        }
            
    }
    
    /*
     * Bigmachines updates Distributor number in National distributor field on quote.
     * THis method queries account with that number if found updates National/Distributor field
     */
    public static void updateNationalDistributorName(List<BigMachines__Quote__c> bigMachinesQuotes){
        
        Set<String> accountNumbers = new Set<String>();
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes){
            if(!String.isBlank(bmQuote.National_Distributor__c)){
                accountNumbers.add(bmQuote.National_Distributor__c.trim());
            }
        }
        
        //Rakesh Start
        List<Account> accounts = new List<Account>();
        if(accountNumbers.size()> 0){
            accounts = [Select Id,Name,AccountNumber from Account where AccountNumber IN: accountNumbers];
        }
        /*List<Account> accounts = [Select Id,Name,AccountNumber 
                                    from Account 
                                    where AccountNumber IN: accountNumbers];*/
        //Rakesh End
        
        // account number as key and name as value for updating quote National Distributor
        Map<String,String> accountMap = new Map<String,String>();
        
        for(Account acct : accounts){
            accountMap.put(acct.AccountNumber,acct.Name);
        }
        
        //Update nation distributor field on quote
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes){
            if(!String.isBlank(bmQuote.National_Distributor__c) && accountMap.containsKey(bmQuote.National_Distributor__c.trim())){
                bmQuote.National_Distributor__c = accountMap.get(bmQuote.National_Distributor__c.trim());
            }
        }
    }
    
     /* Nilesh - Updates the clearance field of quote */
    public static void updateClearanceField(List<BigMachines__Quote__c> bigMachinesQuotes){
        Map<String, Boolean> countryMap = new Map<String, Boolean>();
        List<Country__c> countrylist = [select Id, Name, Clearance__c from Country__c];
        for (Country__c con: countrylist) {
            countryMap.put(con.Name, con.Clearance__c);
        }

        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes) {
            Boolean Clearance = countryMap.get(bmQuote.Ship_To_Country__c);        
            if (Clearance != null && Clearance == true) {
                bmQuote.Clearance__c =  Clearance;
            }
        }
    }
    
    /**
     * Approved quote cannot be deleted
     */
    public void preventDelete(List<BigMachines__Quote__c> bigMachinesQuotes){
        for (BigMachines__Quote__c bmQuote :bigMachinesQuotes){
            if(bmQuote.BigMachines__Status__c == 'Approved' || 
                bmQuote.BigMachines__Status__c == 'Approved Revision' || 
                bmQuote.BigMachines__Status__c == 'Approved Amendment'){
                bmQuote.addError('This is an approved quote, you cannot delete it.');
            }          
        }
    }
    /** 5/10/16 Added by Narmada to update Reference Quote Lookup on POS Quotes **/
    
    public  void updateQuote(List<BigMachines__Quote__c> quotes, Map<Id,BigMachines__Quote__c> oldQuotes, Boolean isInsert){
    
       Set< String > qtRefNumber = new Set< String >();
        Map< String,BigMachines__Quote__c > bgmQuotes = new Map< String, BigMachines__Quote__c >();
        list< BigMachines__Quote__c > bgQuotesToUpdate = new list< BigMachines__Quote__c >();
        for( BigMachines__Quote__c sfQuote : quotes ){
            if(sfQuote.Quote_Reference__c != null  && ( isInsert || sfQuote.Quote_Reference__c  != oldQuotes.get(sfQuote.Id).Quote_Reference__c)){
                qtRefNumber.add(sfQuote.Quote_Reference__c); 
                bgQuotesToUpdate.add(sfQuote);
                
            }
            
        }
        
       if(!qtRefNumber.isEmpty()){
             system.debug('>>>>>>>>>>>>>>>>>qtRefNumber'+qtRefNumber);
            for(BigMachines__Quote__c bgmQ : [SELECT Id,Quote_Reference__c,Name from BigMachines__Quote__c WHERE Name IN  :qtRefNumber]){
                system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>bgmQ'+bgmQ);
                bgmQuotes.put(bgmQ.Name,bgmQ);

            }
            
        }
         
        if(!bgmQuotes.isEmpty()){
            system.debug('>>>>>>>>>>>>>>>>>bgmQuotes'+bgmQuotes);
             
            for(BigMachines__Quote__c bgQuote : bgQuotesToUpdate){
                bgQuote.Quote__c = bgmQuotes.get(bgQuote.Quote_Reference__c).Id;
            }
            
        }
       
    } 
    
    /**
     * Update quote fields ased on some conditions. Add new fields which need to get updated here.
     * Fields getting updated : Quote_Status__c, Letter_of_Credit_Required__c, Initial_Order_Status__c SalesPaymentTerms__c
     */
    public static void updateQuoteFields(List<BigMachines__Quote__c> quotes, Map<Id,BigMachines__Quote__c> oldQuotes){
        Set<Id> accountIds = new Set<Id>();
        Set<String> paymentTerms = new Set<String>(); 
        Map<String,Id> paymentTermsIds = new Map<String,Id>();
        
        for(BigMachines__Quote__c sfQuote : quotes){
            if(sfQuote.BigMachines__Account__c!=null){
               accountIds.add(sfQuote.BigMachines__Account__c);
            }
            if(!String.isBlank(sfQuote.Payment_Terms__c)){
                paymentTerms.add(sfQuote.Payment_Terms__c); 
            } 
            
        }   
        
        List<Lookup_Values__c> lookupValues = [Select Id, Name from LookUp_values__c where  Name IN : paymentTerms];
        for(Lookup_Values__c lookupValue : lookupValues){
            paymentTermsIds.put(lookupValue.Name, lookupValue.Id);
        }
        
        /* HarleenGulati.add Sub Region field to query INC4569517*/
        
        Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id, Country1__c, Country1__r.Pricing_Group__c,Sub_Region__c FROM Account WHERE Id IN:accountIds]);
        
        for(BigMachines__Quote__c sfQuote : quotes){
                
            Account sfAccount = accounts.get(sfQuote.BigMachines__Account__c);
            
            /* Harleen update BMI Region to China Admin INC4569517 */
            
            if(sfAccount!=null && sfAccount.Sub_Region__c == 'China Admin'){
                sfQuote.BMI_Region__c = 'China Admin';
            }
            
            if(sfAccount!=null && sfAccount.Country1__c != null && sfAccount.Country1__r.Pricing_Group__c != null){
                sfQuote.Price_Group__c = sfAccount.Country1__r.Pricing_Group__c;
            }
            
            if(sfQuote.Payment_Terms__c == 'Letter of Credit(Brachy only-Handle this)' || sfQuote.Payment_Terms__c == 'Letter of Credit'){
                sfQuote.Letter_of_Credit_Required__c = true;
            }else{
                sfQuote.Letter_of_Credit_Required__c = false;
            }
            if(paymentTermsIds.containsKey(sfQuote.Payment_Terms__c)){
                sfQuote.SalesPaymentTerms__c = paymentTermsIds.get(sfQuote.Payment_Terms__c);
            }
            if(oldQuotes!=null){
                setQuoteStatus(sfQuote);
                updateInitialOrderStatus(sfQuote, oldQuotes.get(sfQuote.Id));
            }
            if(sfQuote.eCommerce_Quote__c){
                sfQuote.Sales_Org__c = '0601';
                sfQuote.Distribution_Channel__c = '10 - Domestic';
                if(String.isBlank(sfQuote.Subscription_Status__c)){
                    sfQuote.Subscription_Status__c = 'Process';
                }
            }
        }
        updateOfferPriceInUSD(quotes, oldQuotes);
    }
    
    /*
     * Updates Order Status on quote
     */
    private static void setQuoteStatus(BigMachines__Quote__c bmQuote){
        if (bmQuote.Interface_Status__c == 'Process'){
            bmQuote.Quote_Status__c = 'Sent To Interface';
        }else if (bmQuote.Interface_Status__c == 'Processing'){
            bmQuote.Quote_Status__c = 'Interface Processing';
        }else if (bmQuote.Interface_Status__c == 'Processed'){
            bmQuote.Quote_Status__c = 'Transmitted To SAP';
        }else if (bmQuote.Interface_Status__c == 'Error'){
            bmQuote.Quote_Status__c = 'Error';
        }else{ 
            bmQuote.Quote_Status__c = '';
        }  
        
        if((bmQuote.Order_Type__c != 'Combined') && 
        ((bmQuote.Message_Service__c != null && bmQuote.SAP_Prebooked_Service__c != null) || 
        (bmQuote.Booking_Message__c != null && bmQuote.SAP_Prebooked_Sales__c != null))){
            bmQuote.Quote_Status__c = 'SUCCESS';
        }else if((bmQuote.Order_Type__c == 'Combined') && 
        ((bmQuote.Message_Service__c != null && bmQuote.SAP_Prebooked_Service__c != null) && 
        (bmQuote.Booking_Message__c != null && bmQuote.SAP_Prebooked_Sales__c != null))){
            bmQuote.Quote_Status__c = 'SUCCESS';
        }else if((bmQuote.Order_Type__c == 'Combined') && 
        ((bmQuote.Message_Service__c != null && bmQuote.SAP_Prebooked_Service__c != null && bmQuote.Message_Service__c != 'SUCCESS') || 
        (bmQuote.Booking_Message__c != null && bmQuote.SAP_Prebooked_Sales__c != null && bmQuote.Booking_Message__c != 'SUCCESS'))){
            bmQuote.Quote_Status__c = 'SUCCESS Action Needed';
        }else if((bmQuote.Order_Type__c != 'Combined') && 
        ((bmQuote.Message_Service__c != null && bmQuote.SAP_Prebooked_Service__c != null && bmQuote.Message_Service__c != 'SUCCESS') || 
        (bmQuote.Booking_Message__c != null && bmQuote.SAP_Prebooked_Sales__c != null && bmQuote.Booking_Message__c != 'SUCCESS'))){
            bmQuote.Quote_Status__c = 'SUCCESS Action Needed';
        }else if((bmQuote.Message_Service__c != null && 
        bmQuote.SAP_Prebooked_Service__c == null) || 
        (bmQuote.Booking_Message__c != null && bmQuote.SAP_Prebooked_Sales__c == null)){
            bmQuote.Quote_Status__c = 'ERROR';
        }
        
        if (bmQuote.Quote_Type__c == 'Amendments' && (bmQuote.Order_Type__c == 'Sales' || bmQuote.Order_Type__c == 'Combined' ) && 
        bmQuote.SAP_Prebooked_Sales__c != null){
            bmQuote.Quote_Status__c = 'Amended';
        }
        if (bmQuote.Quote_Type__c == 'Amendments' && (bmQuote.Order_Type__c == 'Sales' || bmQuote.Order_Type__c == 'Combined' ) && 
        (bmQuote.SAP_Prebooked_Sales__c == null && bmQuote.SAP_Booked_Sales__c != null)){
            bmQuote.Quote_Status__c = 'Amended';
        }       
        if (bmQuote.Quote_Type__c == 'Amendments' && (bmQuote.Order_Type__c == 'Services' || bmQuote.Order_Type__c == 'Combined') && 
        bmQuote.SAP_Prebooked_Service__c != null){
            bmQuote.Quote_Status__c = 'Amended';
        }
        if (bmQuote.Quote_Type__c == 'Amendments' && (bmQuote.Order_Type__c == 'Services' || bmQuote.Order_Type__c == 'Combined') && 
        (bmQuote.SAP_Prebooked_Service__c == null && bmQuote.SAP_Booked_Service__c != null)){
            bmQuote.Quote_Status__c = 'Amended';
        }
    }
    
    /**
     * Update Offer price : convert quote amopunt into USD.
     */
    private static void updateOfferPriceInUSD(List<BigMachines__Quote__c> quotes, Map<Id,BigMachines__Quote__c> oldQuotes){
        Map<string,decimal> mapCT = new Map<string,decimal>();
        for(CurrencyType ct: [SELECT ISOCode, ConversionRate FROM CurrencyType WHERE IsActive=TRUE]){
            mapCT.put(ct.ISOCode,ct.ConversionRate);
        }
        for(BigMachines__Quote__c bm: quotes){
            if(oldQuotes == null || (oldQuotes != null && bm.BigMachines__Total_Amount__c <> oldQuotes.get(bm.Id).BigMachines__Total_Amount__c)){
                if(bm.BigMachines__Total_Amount__c <> null){
                    if(mapCT.containsKey(bm.CurrencyISOCode)){
                        bm.Offer_in_USD__c = bm.BigMachines__Total_Amount__c / mapCT.get(bm.CurrencyISOCode);
                    }
                }else{
                    bm.Offer_in_USD__c = 0;
                }
            }
        }
    }
    
    private static void updateInitialOrderStatus(BigMachines__Quote__c bmQuote, BigMachines__Quote__c oldQuote){
        if(bmQuote.Initial_Order_Status__c == null){
            if(bmQuote.Quote_Status__c != null && 
            bmQuote.Quote_Status__c != oldQuote.Quote_Status__c && 
            ((!String.isBlank(bmQuote.Quote_Status__c) && bmQuote.Quote_Status__c.contains('SUCCESS')) || bmQuote.Quote_Status__c == 'Error')){
            
                if(!String.isBlank(bmQuote.Quote_Status__c) && bmQuote.Quote_Status__c.contains('SUCCESS')){
                    bmQuote.Initial_Order_Status__c = 'Success';
                }else{
                    bmQuote.Initial_Order_Status__c = bmQuote.Quote_Status__c; 
                }
            }
        }
    }
    
    /**
     * If you want any opportunity fields should get updated after primary quote gets created and updated, add here.
     * Fields getting updated : Opportunity_Type__c, Quote_Status__c, StageName
     */
    public static void updateOpportunityFields(Map<Id,BigMachines__Quote__c> quotes, Map<Id,BigMachines__Quote__c> oldQuotes){
        
        //Map of opportunity Id -> related primary quote
        Map<Id,BigMachines__Quote__c> primaryQuoteMap = new Map<Id,BigMachines__Quote__c>();
        Map<String, Double> currencyConversions = getCurrencyConversions();
        for(BigMachines__Quote__c quote : quotes.values()){
            if(quote.BigMachines__Is_Primary__c){
                primaryQuoteMap.put(quote.BigMachines__Opportunity__c,quote);
            }
        }
        List<Opportunity> opportunities = [SELECT Id, Opportunity_Type__c, Quote_Status__c
                                            , CurrencyISOCode, Initial_EPOT_Submitted_Count__c
                                            FROM Opportunity
                                            WHERE Id IN:primaryQuoteMap.keySet()];
        
        List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();
        
        for(Opportunity opp : opportunities){
            Boolean oppUpdated = false;
            BigMachines__Quote__c sfQuote = primaryQuoteMap.get(opp.Id);
            
            if(sfQuote.Quote_Status__c == 'SUCCESS'){
                opp.Order_In_House__c = false;
                oppUpdated = true;
            }
                
            
            if(oldQuotes!=null && sfQuote.Net_Booking_Value__c != oldQuotes.get(sfQuote.Id).Net_Booking_Value__c){
                opp.Net_Booking_Value__c = sfQuote.Net_Booking_Value__c;
                oppUpdated = true;
            }
            
            if(opp.Opportunity_Type__c != sfQuote.Order_Type__c){
                opp.Opportunity_Type__c = sfQuote.Order_Type__c;
                oppUpdated = true;
            }
            if(opp.Quote_Status__c != sfQuote.Quote_Status__c){
                opp.Quote_Status__c =  sfQuote.Quote_Status__c;
                oppUpdated = true;
            }
            if(oldQuotes!=null && isQuotePrebooked(sfQuote, oldQuotes.get(sfQuote.Id))){
                opp.StageName = '7 - CLOSED WON';
                opp.Forecast_Conversion_Rate__c = currencyConversions.get(opp.CurrencyISOCode);
                oppUpdated = true;
            }
            
            if(oldQuotes!=null 
               && (sfQuote.First_Epot_Submitted_Date__c != oldQuotes.get(sfQuote.Id).First_Epot_Submitted_Date__c
                   || opp.Initial_EPOT_Submitted_Count__c == null)
              ){
                  if(sfQuote.First_Epot_Submitted_Date__c != null && opp.Initial_EPOT_Submitted_Count__c != 1){
                      opp.Initial_EPOT_Submitted_Count__c = 1;
                  }else{
                      opp.Initial_EPOT_Submitted_Count__c = 0;
                  }
                  oppUpdated = true; 
            }
            
            if(oldQuotes!=null){
                DateTime prebookDate = getPrebookDate(sfQuote, oldQuotes.get(sfQuote.Id));
                if(prebookDate!=null){
                    System.debug(LoggingLevel.INFO,'-----prebookDate'+prebookDate);
                    opp.CloseDate = prebookDate.date();
                    oppUpdated = true;
                }
            }
            if(oppUpdated) opportunitiesToUpdate.add(opp);
        }
        
        if(!opportunitiesToUpdate.isEmpty()) update opportunitiesToUpdate;
    }
    
    /**
     * Get prebook date of quote to update it on opportunity
     */
    @TestVisible
    private static DateTime getPrebookDate(BigMachines__Quote__c quote, BigMachines__Quote__c oldQuote){
        System.debug(LoggingLevel.INFO,'-----quote'+quote);
        if(quote.Order_Type__c == 'Sales' && (String.isBlank(oldQuote.SAP_Prebooked_Sales__c) && !String.isBlank(quote.SAP_Prebooked_Sales__c))){
            return quote.SAP_PreBooked_Date_Sales__c;
        }else if(quote.Order_Type__c == 'Services' && (String.isBlank(oldQuote.SAP_Prebooked_Service__c) && !String.isBlank(quote.SAP_Prebooked_Service__c))){
            return quote.SAP_PreBooked_Date_Service__c;
        }else if(quote.Order_Type__c == 'Combined' && ((String.isBlank(oldQuote.SAP_Prebooked_Sales__c) && !String.isBlank(quote.SAP_Prebooked_Sales__c))
        || (String.isBlank(oldQuote.SAP_Prebooked_Service__c) && !String.isBlank(quote.SAP_Prebooked_Service__c)))){
            if(!String.isBlank(quote.SAP_Prebooked_Sales__c) && !String.isBlank(quote.SAP_Prebooked_Sales__c)){
                if(quote.SAP_PreBooked_Date_Sales__c!=null && quote.SAP_PreBooked_Date_Service__c!=null){
                    if(quote.SAP_PreBooked_Date_Sales__c > quote.SAP_PreBooked_Date_Service__c){
                        return quote.SAP_PreBooked_Date_Sales__c;
                    }else{
                        return quote.SAP_PreBooked_Date_Service__c;
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Check if quote is prebooked or not
     */
    private static Boolean isQuotePrebooked(BigMachines__Quote__c quote, BigMachines__Quote__c oldQuote){
        System.debug('------Order Type'+quote.Order_Type__c);
        System.debug('------BookingMessage'+quote.Booking_Message__c+'-Old-'+oldQuote.Booking_Message__c);
        System.debug('------messageservice'+quote.Message_Service__c+'-Old-'+oldQuote.Message_Service__c);
        
        if(quote.Order_Type__c == 'Sales'){
            if(!String.isBlank(quote.Booking_Message__c) && quote.Booking_Message__c != oldQuote.Booking_Message__c && quote.Booking_Message__c.contains('SUCCESS')){
                return true;
            }
        }else if(quote.Order_Type__c == 'Services'){
            if(!String.isBlank(quote.Message_Service__c) && quote.Message_Service__c != oldQuote.Message_Service__c && quote.Message_Service__c.contains('SUCCESS')){
                return true;
            }
        }else if(quote.Order_Type__c == 'Combined'){
            if((oldQuote.Booking_Message__c !=  quote.Booking_Message__c && !String.isBlank(quote.Booking_Message__c)) || 
            (oldQuote.Message_Service__c !=  quote.Message_Service__c && !String.isBlank(quote.Message_Service__c))){
                    
                if(quote.Order_Type__c == 'Combined' && !String.isBlank(quote.Booking_Message__c) && quote.Booking_Message__c.contains('SUCCESS') && 
                !String.isBlank(quote.Message_Service__c) && quote.Message_Service__c.contains('SUCCESS')){
                    return true;
                }
            } 
        }
        return false;
    }
    
    /*
     * @author - amitkumar katre
     * @description - update and create omni quote convertion
     * @date - 11/24/2015
     */ 
    public static void updateOMNIQuoteConversion(List<BigMachines__Quote__c> quotes, Map<Id,BigMachines__Quote__c> oldQuotes){
        set<String> omniQuoteNumbers = new set<String> ();
        for(BigMachines__Quote__c q : quotes){
            if(String.isNotBlank(q.OMNI_Quote_Number__c)){
                 if(oldQuotes == null || q.OMNI_Quote_Number__c != oldQuotes.get(q.Id).OMNI_Quote_Number__c){
                omniQuoteNumbers.add(q.OMNI_Quote_Number__c);}
            }
        }
        
        // Search for existing omni quote number
        list<OMNI_Quote_Conversion__c> omniqList = new List<OMNI_Quote_Conversion__c>();
        if(omniQuoteNumbers.size()>0){
            omniqList = [select OMNI_Quote_Number__c from OMNI_Quote_Conversion__c where OMNI_Quote_Number__c in : omniQuoteNumbers];
        }
        system.debug('omniqList === '+omniqList);
        map<String,OMNI_Quote_Conversion__c> omniMap = new map<String,OMNI_Quote_Conversion__c>();
        for(OMNI_Quote_Conversion__c oq : omniqList){
            omniMap.put(oq.OMNI_Quote_Number__c, oq);
        }
        // update and insret omni quote list
        list<OMNI_Quote_Conversion__c> toUpsertOmniqList = new list<OMNI_Quote_Conversion__c> ();
        
        for(BigMachines__Quote__c q : quotes){
            if(String.isNotBlank(q.OMNI_Quote_Number__c)){
                if(oldQuotes == null || q.OMNI_Quote_Number__c != oldQuotes.get(q.Id).OMNI_Quote_Number__c){
                    // update data BMI_Quote_Number__c 
                    OMNI_Quote_Conversion__c existingQmniq = omniMap.get(q.OMNI_Quote_Number__c);
                    if(existingQmniq != null){
                        existingQmniq.BMI_Quote_Number__c = q.Name;
                        toUpsertOmniqList.add(existingQmniq);
                    }else{
                        toUpsertOmniqList.add(createOmniQuoteConversion(q));
                    }
                }
            }
        }
        
        if(!toUpsertOmniqList.isEmpty()){
            upsert toUpsertOmniqList;
        }
        
    }
    
    /*
     * @author - amitkumar katre
     * @description - create omni quote convertion
     * @date - 11/24/2015
     */ 
    private static OMNI_Quote_Conversion__c createOmniQuoteConversion(BigMachines__Quote__c q){
        OMNI_Quote_Conversion__c oqc = new OMNI_Quote_Conversion__c();
        oqc.Name = q.Name;
        oqc.OMNI_Quote_Number__c = q.OMNI_Quote_Number__c; 
        oqc.Geo__c = q.GPO__c;
        oqc.BMI_Quote_Number__c = q.Name;
        oqc.Converted_By__c = q.OwnerId;
        oqc.Date_Converted__c = system.today();
        oqc.Opportunity__c = q.BigMachines__Opportunity__c;
        return oqc;
    }
    
    
    public static set<id> replacedQuote = new set<id> ();
    /*
     * Cancel replace change quote on opportunity and quote
     */
    public static void updateReplacedQuote(list<BigMachines__Quote__c> qList, map<Id,BigMachines__Quote__c> qouteOldMap){
        set<id> primaryQuotesSet = new set<id>();
        for(BigMachines__Quote__c bq : qList){
            if(bq.Is_Replaced__c == false &&
                bq.BigMachines__Is_Primary__c && 
               (qouteOldMap == null || qouteOldMap.get(bq.Id).BigMachines__Is_Primary__c != bq.BigMachines__Is_Primary__c)){
                  primaryQuotesSet.add(bq.Id);
            }
        }
        if(!primaryQuotesSet.isEmpty()){
            list<BigMachines__Quote__c> pQuoteListUpdate = new list<BigMachines__Quote__c> ();
            for(BigMachines__Quote__c bq : [select BigMachines__Opportunity__r.Quote_Replaced__c,
                                            BigMachines__Opportunity__r.Quote_Replaced__r.Replaced_By__c
                                            from BigMachines__Quote__c
                                            where Id in : primaryQuotesSet
                                            and BigMachines__Opportunity__r.Quote_Replaced__c != null]){
                
                if(!replacedQuote.contains(bq.BigMachines__Opportunity__r.Quote_Replaced__c) &&
                   bq.BigMachines__Opportunity__r.Quote_Replaced__r.Replaced_By__c != bq.Id){
                       
                    BigMachines__Quote__c newTempBq = new BigMachines__Quote__c(
                                                                            Id = bq.BigMachines__Opportunity__r.Quote_Replaced__c,
                                                                            Replaced_By__c = bq.Id);
                    pQuoteListUpdate.add(newTempBq);
                    replacedQuote.add(bq.BigMachines__Opportunity__r.Quote_Replaced__c);
                }
                
            }
            if(!pQuoteListUpdate.isEmpty()){
                update pQuoteListUpdate;
            }
        }
    }
    
    /*
     * Populate currency conversion map
     */
    private static Map<String, Double> getCurrencyConversions(){
        Map<String, Double> currencyConversions = new Map<String, Double>();
        for(CurrencyType conversionRec : [SELECT ConversionRate,IsActive,IsoCode FROM CurrencyType]){
            currencyConversions.put(conversionRec.IsoCode, conversionRec.ConversionRate);
        }
        return currencyConversions;
    }

//---------------------------------------------------------------------------------------------

    /**
     * @Description This method will associate Add-On Quotes with Subscription
     * @Param       Map (quote id to Quote)
     * @Return      void     
     */ 

    public static void addOnQuoteToContract(map<Id, BigMachines__Quote__c> mapQtIdQt) {
        map<String, Id> mapContNoQtId = new map<String, Id>();
system.debug('mapQtIdQt Map Value======='+mapQtIdQt);
        
        if(trigger.isUpdate) {
            for(AggregateResult results : [SELECT BigMachines__Quote__c qtId, MAX(SAP_Contract_Number__c) contractNo  
                                            FROM BigMachines__Quote_Product__c
                                            WHERE Subscription_Sales_Type__c = 'Add-On'
                                            AND QP_Product_Family__c = 'Subscription'
                                            AND BigMachines__Quote__c IN :mapQtIdQt.keySet()
                                            Group By BigMachines__Quote__c ]) {

                String contractNoVal = (String)(results.get('contractNo'));
                mapContNoQtId.put(contractNoVal.replaceFirst('00', ''), (ID)results.get('qtId'));
            }

            if(mapContNoQtId.size() > 0) {
                for(Subscription__c sub : [SELECT Id, Name FROM Subscription__c WHERE Name IN :mapContNoQtId.keySet()]) {
                    BigMachines__Quote__c qtLocal = mapQtIdQt.get(mapContNoQtId.get(sub.Name)); 
                    qtLocal.SAP_Contract_Number__c = sub.Id;
                }
            }
        }        
    }    

}
@isTest
public class QuoteTriggerHandlerTest {
    
    static testMethod void testUpdateNationalDistributorName() {
        Account acct = [SELECT Id FROM Account WHERE AccountNumber = '121212'];
        
        Contact con = [SELECT Id FROM Contact WHERE AccountId =:acct.Id];
        
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opp.Id;
        quote.National_Distributor__c = '121212';
        
        QuoteTriggerHandler.updateNationalDistributorName(new List<BigMachines__Quote__c>{quote});
        
        System.assertEquals(quote.National_Distributor__c,'Test Account');
    }
    
    static testMethod void testUpdateNationalDistributorNameNegative() {
        Account acct = [SELECT Id FROM Account WHERE AccountNumber = '121212'];
        
        Contact con = [SELECT Id FROM Contact WHERE AccountId =:acct.Id];
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.BigMachines__Account__c = acct.Id;
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.National_Distributor__c = '';
        
        BigMachines__Quote__c bmQuote1 = TestUtils.getQuote();
        bmQuote1.BigMachines__Account__c = acct.Id;
        bmQuote1.BigMachines__Opportunity__c = opp.Id;
        bmQuote1.National_Distributor__c = 'Random Test';
        bmQuote1.Current_Approvers__c = 'vmsaraut';
        
        List<BigMachines__Quote__c> sfQuotes = new List<BigMachines__Quote__c>{bmQuote,bmQuote1};
            
            QuoteTriggerHandler.updateNationalDistributorName(sfQuotes);
        
        System.assertEquals(sfQuotes[0].National_Distributor__c,'');
        System.assertEquals(sfQuotes[1].National_Distributor__c,'Random Test');
        
        
        insert sfQuotes;
    }
    
    /*Coverage
static testMethod void testPreventDelete(){
Account acct = TestUtils.getAccount();
acct.AccountNumber = '121212';
insert acct;

Contact con = TestUtils.getContact();
con.AccountId = acct.Id;
insert con;

Opportunity opp = TestUtils.getOpportunity();
opp.AccountId = acct.Id;
opp.Primary_Contact_Name__c = con.Id;
insert opp;

BigMachines__Quote__c bmQuote1 = TestUtils.getQuote();
bmQuote1.BigMachines__Account__c = acct.Id;
bmQuote1.BigMachines__Opportunity__c = opp.Id;
bmQuote1.BigMachines__Status__c = 'Approved';

BigMachines__Quote__c bmQuote2 = TestUtils.getQuote();
bmQuote2.BigMachines__Account__c = acct.Id;
bmQuote2.BigMachines__Opportunity__c = opp.Id;
bmQuote2.BigMachines__Status__c = 'Approved Revision';

BigMachines__Quote__c bmQuote3 = TestUtils.getQuote();
bmQuote3.BigMachines__Account__c = acct.Id;
bmQuote3.BigMachines__Opportunity__c = opp.Id;
bmQuote3.BigMachines__Status__c = 'Approved Amendment';

List<BigMachines__Quote__c> quotes = new List<BigMachines__Quote__c>{bmQuote1,bmQuote2,bmQuote3};
insert quotes;

List<BigMachines__Quote__c> deleteQuotes = [Select Id From BigMachines__Quote__c];

Database.DeleteResult[] resultList = Database.delete(deleteQuotes, false);


for (Database.DeleteResult result : resultList) {
if (result.isSuccess()){
System.assert(false);
}else{          
for(Database.Error err : result.getErrors()) {
System.assert(err.getMessage().contains('This is an approved quote, you cannot delete it.'));
}
}
}
} */
    
    // latest coverage
    /*
    static testMethod void testUpdateOpportunityFields(){
        
        List<BigMachines__Quote__c> quotesToUpdate = [Select Id,BigMachines__Opportunity__c,Booking_Message__c,Message_Service__c,Order_Type__c
                                                      From BigMachines__Quote__c];
        
        System.debug('--quotesToUpdate'+quotesToUpdate);
        for(BigMachines__Quote__c quote : quotesToUpdate){
            
            if(quote.Order_Type__c == 'Sales'){
                quote.Booking_Message__c = 'SUCCESS';
            }else if(quote.Order_Type__c == 'Services'){
                quote.Message_Service__c = 'ERROR';
            }else if(quote.Order_Type__c == 'Combined'){
                quote.Booking_Message__c = 'SUCCESS';
                quote.Message_Service__c = 'SUCCESS';
            }
            
            if(quote.Id == quotesToUpdate[2].Id){
                quote.Message_Service__c = 'SUCCESS';
            }
        }
        System.debug('--quotesToUpdate1'+quotesToUpdate);
        update quotesToUpdate;
        
        List<Opportunity> opportunities = [Select Id,StageName
                                           From Opportunity];
        
        System.debug('------opportunities'+opportunities);
        Integer counter = 0;
        for(Opportunity opp : opportunities){
            System.debug('----counter'+counter);
            if(opp.Id == opportunities[1].Id){
                System.assert(opp.StageName != '7 - CLOSED WON');
            }else{
                System.assertEquals('7 - CLOSED WON', opp.StageName);
            }
            counter++;
        }                                                   
    }*/
    
    static testMethod void testSetQuoteFields(){
        
        Lookup_Values__c lookupValue = new Lookup_Values__c();
        lookupValue.Name = 'Letter of Credit(Brachy only-Handle this)';
        insert lookupValue;
        
        
        List<BigMachines__Quote__c> bigMachinesQuotes1 = [SELECT Id, Interface_Status__c,Message_Service__c,Booking_Message__c,BigMachines__Account__c,Price_Group__c,Payment_Terms__c,
                                                          Order_Type__c,SAP_Prebooked_Service__c,SAP_Prebooked_Sales__c,SalesPaymentTerms__c,Letter_of_Credit_Required__c
                                                          FROM BigMachines__Quote__c];
        
        Map<Id,BigMachines__Quote__c> oldQuoteMap = new Map<Id,BigMachines__Quote__c>();
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes1){
            oldQuoteMap.put(bmQuote.Id,bmQuote);
        }
        
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes1){
            if(bmQuote.Order_Type__c == 'Sales'){
                bmQuote.Interface_Status__c = 'Process';
                
            }else if(bmQuote.Order_Type__c == 'Services'){
                bmQuote.Interface_Status__c = 'Processing';
                //bmQuote.Message_Service__c = 'SUCCESS';
                bmQuote.SAP_Prebooked_Service__c = '12345';
            }else{
                bmQuote.Interface_Status__c = 'Processed';
                //bmQuote.Message_Service__c = 'SUCCESS';
                bmQuote.SAP_Prebooked_Service__c = '12345';
                //bmQuote.Booking_Message__c = 'SUCCESS';
                bmQuote.SAP_Prebooked_Sales__c = '1234533';
            }
        }
        
        QuoteTriggerHandler.updateQuoteFields(bigMachinesQuotes1,oldQuoteMap);
        
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes1){
            if(bmQuote.Order_Type__c == 'Sales'){
                bmQuote.Interface_Status__c = 'Process';
                
            }else if(bmQuote.Order_Type__c == 'Services'&& bmQuote.SAP_Prebooked_Service__c != null){
                //System.assertEquals(bmQuote.Quote_Status__c,'SUCCESS');
                System.assertEquals(bmQuote.Quote_Status__c,'Interface Processing');
            }else{
                System.assertNotEquals(bmQuote.Quote_Status__c,'SUCCESS');
            }
        }
        
        List<BigMachines__Quote__c> bigMachinesQuotes2 = [SELECT Id, Interface_Status__c,Message_Service__c,Booking_Message__c,BigMachines__Account__c,Price_Group__c,Payment_Terms__c,
                                                          Order_Type__c,SAP_Prebooked_Service__c,SAP_Prebooked_Sales__c,SalesPaymentTerms__c,Letter_of_Credit_Required__c
                                                          FROM BigMachines__Quote__c];
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes2){
            if(bmQuote.Order_Type__c == 'Sales'){
                bmQuote.Interface_Status__c = 'Error';
                bmQuote.Booking_Message__c = 'Error';
                
            }else if(bmQuote.Order_Type__c == 'Services'){
                bmQuote.Interface_Status__c = 'Processing';
                bmQuote.Message_Service__c = 'SUCCESS123';
            }else{
                bmQuote.Interface_Status__c = 'Processed';
                bmQuote.Message_Service__c = 'SUCCESS 12345';
                bmQuote.Booking_Message__c = 'SUCCESS';
                //      
                bmQuote.eCommerce_Quote__c = True;       
                bmQuote.SAP_Prebooked_Sales__c = 'Test';
                bmQuote.SAP_Prebooked_Service__c = 'Test';
                bmQuote.SAP_Booked_Sales__c =  'Test';                
                bmQuote.Quote_Status__c = 'SUCCESS Action Needed';
                
            }
        }
        
        bigMachinesQuotes2[2].Quote_Type__c = 'Amendments';
        bigMachinesQuotes2[2].SAP_Prebooked_Service__c = 'SUCCESS';
        //
        //bigMachinesQuotes2[2].SAP_Prebooked_Sales__c = 'Test';
        //bigMachinesQuotes2[2].SAP_Booked_Sales__c = 'Test';
        
        QuoteTriggerHandler.updateQuoteFields(bigMachinesQuotes2,oldQuoteMap);
        
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes2){
            if(bmQuote.Order_Type__c == 'Sales'){
                System.assertEquals(bmQuote.Quote_Status__c,'ERROR');
                System.assertEquals(bmQuote.SalesPaymentTerms__c,lookupValue.Id);
            }else if(bmQuote.Order_Type__c == 'Services'){
                if(bigMachinesQuotes2[1].Id == bmQuote.Id){
                    System.assertnotEquals(bmQuote.Quote_Status__c,'SUCCESS Action Needed');
                    System.assertEquals(bmQuote.Price_Group__c,'Z1');
                }
            }else{
                System.assertnotEquals(bmQuote.Quote_Status__c,'SUCCESS Action Needed');
            }
        }
        
        System.assertEquals(bigMachinesQuotes2[2].Quote_Status__c,'Amended');
    }
    
    /**
* Test create omni quote conversion if quote is omni quote has been created in SF
*/
    static testMethod void testUdateOMNIQuoteConversion(){
        List<BigMachines__Quote__c> bigMachinesQuotes2 = [SELECT Id, OMNI_Quote_Number__c
                                                          FROM BigMachines__Quote__c];
        Integer counter = 0;                                                    
        for(BigMachines__Quote__c bmQuote : bigMachinesQuotes2){
            bmQuote.OMNI_Quote_Number__c = 'TEST OMNI QUOTE '+counter;
        }
        
        update bigMachinesQuotes2;
        
        List<OMNI_Quote_Conversion__c> omniQuotes = [SELECT Id FROM OMNI_Quote_Conversion__c];
        System.assertEquals(5, omniQuotes.size());
    }
    
    static testMethod void  testRevisedQuoteInfo(){
        
        Account acct = [Select Id FROM Account LIMIT 1];
        Opportunity opp = [Select Id FROM Opportunity LIMIT 1];
        
        List<BigMachines__Quote__c> bigMachinesQuotes = [SELECT Id, Name,OMNI_Quote_Number__c
                                                         FROM BigMachines__Quote__c];
        
        System.debug('----bigMachinesQuotes[0].Name'+bigMachinesQuotes[0].Name);
        
        Quote_Product_Partner__c quotePartner = new Quote_Product_Partner__c();
        quotePartner.Quote__c = bigMachinesQuotes[0].Id;
        quotePartner.ERP_Partner_Number__c = 'TEST1212';
        quotePartner.ERP_Partner_Function__c = 'Z1';
        insert quotePartner;
        
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.Name = 'TEST-QUOTE';
        bmQuote.BigMachines__Account__c = acct.Id;
        bmQuote.BigMachines__Opportunity__c = opp.Id;
        bmQuote.BigMachines__Status__c = 'Approved';
        bmQuote.Order_Type__c = 'Sales';
        bmQuote.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote.BigMachines__Is_Primary__c = true;
        bmQuote.Parent_Quote__c = bigMachinesQuotes[0].Name;
        bmQuote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        //     
        bmQuote.eCommerce_Quote__c = True;
        bmQuote.Booking_Message__c = 'SUCCESS';
        bmQuote.SAP_Prebooked_Sales__c = 'Test';
        bmQuote.SAP_Prebooked_Service__c = 'Test';
        bmQuote.SAP_Booked_Sales__c =  'Test';
        bmQuote.Message_Service__c = 'SUCCESS';
        bmQuote.Quote_Status__c = 'SUCCESS Action Needed';
        
        bmQuote.SAP_Prebooked_Sales__c = 'Test';
        insert bmQuote;
        
        //QuoteTriggerHandler.getPrebookDate(bmQuote, bmQuote);
    }
    
    /**
* Set up test data can be used for all the quotes.
*/
    @testSetup static void setUpQuotedata() {
        
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Region__c = 'NA';
        country.Pricing_Group__c = 'Z1';
        country.Clearance__c=true;
        insert country;
        
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = '121212';
        acct.Country1__c = country.Id;
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp1 = TestUtils.getOpportunity();
        opp1.AccountId = acct.Id;
        opp1.Primary_Contact_Name__c = con.Id;
        
        Opportunity opp2 = TestUtils.getOpportunity();
        opp2.AccountId = acct.Id;
        opp2.Primary_Contact_Name__c = con.Id;
        
        Opportunity opp3 = TestUtils.getOpportunity();
        opp3.AccountId = acct.Id;
        opp3.Primary_Contact_Name__c = con.Id;
        
        Opportunity opp4 = TestUtils.getOpportunity();
        opp4.AccountId = acct.Id;
        opp4.Primary_Contact_Name__c = con.Id;
        
        List<Opportunity> opps = new List<Opportunity>{opp1,opp2,opp3,opp4};
            insert opps;
        
        BigMachines__Quote__c bmQuote1 = TestUtils.getQuote();
        bmQuote1.Name = 'QUOTE-TEST';
        bmQuote1.BigMachines__Account__c = acct.Id;
        bmQuote1.BigMachines__Opportunity__c = opp1.Id;
        bmQuote1.BigMachines__Status__c = 'Approved';
        bmQuote1.Order_Type__c = 'Sales';
        bmQuote1.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote1.BigMachines__Is_Primary__c = true;
        
        BigMachines__Quote__c bmQuote2 = TestUtils.getQuote();
        bmQuote2.BigMachines__Account__c = acct.Id;
        bmQuote2.BigMachines__Opportunity__c = opp2.Id;
        bmQuote2.BigMachines__Status__c = 'Approved Revision';
        bmQuote2.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote2.Order_Type__c = 'Services';
        bmQuote2.BigMachines__Is_Primary__c = true;
        
        BigMachines__Quote__c bmQuote3 = TestUtils.getQuote();
        bmQuote3.BigMachines__Account__c = acct.Id;
        bmQuote3.BigMachines__Opportunity__c = opp3.Id;
        bmQuote3.BigMachines__Status__c = 'Approved Revision';
        bmQuote3.Order_Type__c = 'Services';
        bmQuote3.BigMachines__Is_Primary__c = true;
        
        BigMachines__Quote__c bmQuote4 = TestUtils.getQuote();
        bmQuote4.BigMachines__Account__c = acct.Id;
        bmQuote4.BigMachines__Opportunity__c = opp4.Id;
        bmQuote4.BigMachines__Status__c = 'Approved Revision';
        bmQuote4.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote4.Order_Type__c = 'Combined';
        bmQuote4.BigMachines__Is_Primary__c = true;
        
        BigMachines__Quote__c bmQuote5 = TestUtils.getQuote();
        bmQuote5.BigMachines__Account__c = acct.Id;
        bmQuote5.BigMachines__Opportunity__c = opp4.Id;
        bmQuote5.BigMachines__Status__c = 'Approved Amendment';
        bmQuote5.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote5.Order_Type__c = 'Combined';
        bmQuote5.Quote_Type__c = 'Amendments';
        bmQuote5.BigMachines__Is_Primary__c = true;
        bmQuote5.SAP_Prebooked_Sales__c = 'Test';
        
        BigMachines__Quote__c bmQuote6 = TestUtils.getQuote();
        bmQuote6.BigMachines__Account__c = acct.Id;
        bmQuote6.BigMachines__Opportunity__c = opp4.Id;
        bmQuote6.BigMachines__Status__c = 'Approved Amendment';
        bmQuote6.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote6.Order_Type__c = 'Combined';
        bmQuote6.Quote_Type__c = 'Amendments';
        bmQuote6.BigMachines__Is_Primary__c = true;
        bmQuote6.SAP_Prebooked_Sales__c = 'Test';
        
        List<BigMachines__Quote__c> quotes = new List<BigMachines__Quote__c>{bmQuote1,bmQuote2,bmQuote3,bmQuote4,bmQuote5};
            insert quotes;
        
        test.starttest();
        opp2.Quote_Replaced__c= bmQuote1.id;
        update opp2;
        test.stoptest();
        
    }
    
    static testMethod void updateQuoteTest(){
        
        Account acct = [SELECT Id FROM Account WHERE AccountNumber = '121212'];
        
        Contact con = [SELECT Id FROM Contact WHERE AccountId =:acct.Id];
        
        Opportunity opp1 = TestUtils.getOpportunity();
        opp1.AccountId = acct.Id;
        opp1.Primary_Contact_Name__c = con.Id;
        insert opp1;
        list<BigMachines__Quote__c> bgQuotes = new list<BigMachines__Quote__c>();
        BigMachines__Quote__c bmQuote = TestUtils.getQuote();
        bmQuote.Name = 'TEST-QUOTE';
        bmQuote.BigMachines__Account__c = acct.Id;
        bmQuote.BigMachines__Opportunity__c = opp1.Id;
        bmQuote.BigMachines__Status__c = 'Approved';
        bmQuote.Order_Type__c = 'Sales';
        bmQuote.Quote_Reference__c = 'TEST-QUOTE1';
        bmQuote.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote.BigMachines__Is_Primary__c = true;
        //bmQuote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        //bmQuote.Parent_Quote__c = bgQuotes[0].Name;
        //insert bmQuote;
        bgQuotes.add(bmQuote);
        
        BigMachines__Quote__c bmQuote1 = TestUtils.getQuote();
        bmQuote1.Name = 'TEST-QUOTE1';
        bmQuote1.BigMachines__Account__c = acct.Id;
        bmQuote1.BigMachines__Opportunity__c = opp1.Id;
        bmQuote1.BigMachines__Status__c = 'Approved';
        bmQuote1.Order_Type__c = 'Sales';
        bmQuote1.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote1.BigMachines__Is_Primary__c = true;
        bmQuote1.Quote_Reference__c = 'TEST-QUOTE';
        //bmQuote1.Parent_Quote__c = bmQuote.id;
        //insert bmQuote;
        
        
        bgQuotes.add(bmQuote1);
        insert bgQuotes;
        
    }
    
    static testMethod void updateClearanceFieldTest() {
        Account acct = [SELECT Id FROM Account WHERE AccountNumber = '121212'];
        
        Contact con = [SELECT Id FROM Contact WHERE AccountId =:acct.Id];
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Deliver_to_Country__c = 'USA';
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opp.Id;
        quote.National_Distributor__c = '121212';
        quote.BigMachines__Status__c = 'Approved';
        quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.Ship_To_Country__c='USA';
        
        //
        //quote.BigMachines__Status__c = 'Approved Revision' ;
        
        insert quote;
        
        test.startTest();
        QuoteTriggerHandler.updateClearanceField(new List<BigMachines__Quote__c>{quote});
        QuoteTriggerHandler objQT =  new QuoteTriggerHandler();
        objQT.preventDelete(new List<BigMachines__Quote__c>{quote}); 
        test.stopTest();      
       
    }    
   
     
    
    
}
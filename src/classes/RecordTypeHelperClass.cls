/**
  * @author jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Generates a map of commonly used object record types so multiple schema
  *    calls are not required. Example of retrieving a record type id:
  *
  *    Id myId = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Field Service').getRecordTypeId();
  * 
  * TEST CLASS 
  * 
  *    RecordTypeHelperClass_TEST 
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  jwagner/Forefront; Initial Build
  *    12/14/15; Fkhan/ForeFront; added "PARTS_ORDER_LINE_RECORDTYPES" because change set returned error in SR_PartOrderLine line 961.
  *    12-17-2015 SA: added Parts Order 
  **/
public with sharing class RecordTypeHelperClass {
    
  public final static Map<String, Schema.RecordTypeInfo> WORK_ORDER_RECORDTYPES = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> WORK_DETAIL_RECORDTYPES = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> TIMESHEET_RECORDTYPES = Schema.SObjectType.SVMXC_Timesheet__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> TIME_ENTRY_RECORDTYPES = Schema.SObjectType.SVMXC_Time_Entry__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> COUNTER_DETAILS_RECORDTYPES = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> TECHNICIAN_RECORDTYPES = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> CASELINE_RECORDTYPES = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> CASE_RECORDTYPES = Schema.SObjectType.Case.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> PARTS_ORDER_LINE_RECORDTYPES = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName();
  public final static Map<String, Schema.RecordTypeInfo> PARTS_ORDER_RECORDTYPES = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
}
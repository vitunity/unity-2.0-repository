/**
 
 */
@isTest
private class RecordTypeHelperClass_TEST {

    static testMethod void  RecordTypeHelperTest() {
       system.assert(RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.TIMESHEET_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.TIME_ENTRY_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.COUNTER_DETAILS_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.TECHNICIAN_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.CASELINE_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.CASE_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.PARTS_ORDER_LINE_RECORDTYPES.size()>0);
       system.assert(RecordTypeHelperClass.PARTS_ORDER_RECORDTYPES.size()>0);
        // TO DO: implement unit test
    }
}
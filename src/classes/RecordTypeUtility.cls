/*
 * Author Amitkumar Katre
 * Usage - RecordTypeUtility.getInstance().rtMaps.get('SobjectName').get('RecordTypeDeveloperName'));
 */
public with sharing class RecordTypeUtility {
	
	// private static variable referencing the class
    private static RecordTypeUtility instance = null;
    public map<String,map<String,String>> rtMaps;
    
    // The constructor is private and initializes the id of the record type
    private RecordTypeUtility(){
    	rtMaps = new map<String,map<String,String>>();
    	list<RecordType> rtypesList = [Select ID, DeveloperName, Name, SobjectType from RecordType ];
    	system.debug('rtypesList==='+rtypesList.size());
    	for(RecordType recordType : rtypesList){
    		if(rtMaps.containsKey(recordType.SobjectType)){
    			rtMaps.get(recordType.SobjectType).put(recordType.DeveloperName,recordType.Id);
    		}else{
    			map<String,String> maptemp = new map<String,String>{recordType.DeveloperName => recordType.Id};
    			rtMaps.put(recordType.SobjectType,maptemp);
    		}
    	}
        
    }
    // a static method that returns the instance of the record type
    public static RecordTypeUtility getInstance(){
        // lazy load the record type - only initialize if it doesn't already exist
        if(instance == null) instance = new RecordTypeUtility();
        return instance;
    }
    
}
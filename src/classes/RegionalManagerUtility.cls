/**************************************************************************\
@Created Date Date- 11/16/2017
@Created By - Rakesh Basani   
@Description - STSK0013241 - Update Regional Manager field on Service Team 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
/**************************************************************************/
public class RegionalManagerUtility{
    public static void UpdateTechnicianRM(List<User> newUsers,map<Id,User> mapOld){
        map<Id,Id> mapDMRM = new map<Id,Id>();
        set<Id> setRM = new set<Id>();
        for(user u : newUsers){
            if(u.managerId <> mapOld.get(u.id).managerId){
                mapDMRM.put(u.id,u.managerId);
                if(u.managerId <> null)setRM.add(u.managerId);
            }
        }
        if(mapDMRM.size()>0){
            map<Id,User> mapRegionalManager = new map<Id,User>([select id,Email from User where Id IN: setRM]);
            List<SVMXC__Service_Group__c> lstSG = new List<SVMXC__Service_Group__c>();
            for(SVMXC__Service_Group__c s:  [select Id,District_Manager__c,Regional_Manager__c from SVMXC__Service_Group__c where District_Manager__c IN: mapDMRM.keyset()]){
                if(mapDMRM.containsKey(s.District_Manager__c)){
                    s.Regional_Manager__c = mapDMRM.get(s.District_Manager__c); 
                    lstSG.add(s);
                }
            }
            update lstSG;
        }
    }
}
public with sharing class RegularGroupListController{
    public List<SelectOption> selectedGroupList{get;set;}
    public List<SelectOption> groupList{get;set;}
    public List<SelectOption> installedPrdList{get;set;}
    
    public List<string> leftselected{get;set;}
    public List<string> rightselected{get;set;}
    
    public String leftLabel{get;set;}
    public String rightLabel{get;set;}
    public String accntId{get;set;}
    
    public Boolean showPGBln {get;set;}
    public final static string VMS_MyVarian_AdminPermissionsSet = '0PSE0000000HXgx'; // This multipicklist should be editable only to this permission set (Portal Admins)
    public final static string SystemAdminProfile ='00eE0000000mvyb';
    set<String> groupSet = new set<String>();
    Map<id,set<string>> accGroupsMap = new  Map<id,set<string>>();
    Set <ID> accntSet  = new set<ID>();
    public Account acc{get;set;} 
    public String flag{get;set;}
    
    public RegularGroupListController(ApexPages.StandardController controller){
        groupList = new List<SelectOption>();
        selectedGroupList = new List<SelectOption>();
        installedPrdList = new List<SelectOption>();
        accntId = controller.getId();
        System.debug('###### ACCOUNT ID = '+accntId);
        leftLabel = 'Available Group';
        rightLabel = 'Selected Group';
        leftselected = new List<string>();
        rightselected = new List<string>();
        showPGBln= false;
        String userid = UserInfo.getUserId();    
        Id profileId = UserInfo.getProfileId();
        system.debug ('USER Info ---> '+ userId);
        if(ApexPages.currentPage().getParameters().get('source') != null){
            flag = ApexPages.currentPage().getParameters().get('source');
        }else{
            flag = 'none';
        }
        System.debug('###### flag = '+flag);
        //show public group list on account page only if it is sys admin profile or Portal Admin users with VMS_MyVarian_AdminPermissionsSet
        if (ProductUtils.isPortalAdmin(userId) || ProductUtils.isSystemAdmin(profileId)) showPGBln = true; 

       
        acc = [Select Id,Selected_Groups__c,Name,IsPartner,IsCustomerPortal,Quality__c,TrueBeam__c,X3DCRT__c,Peer_to_Peer__c,RapidArc__c,IGRT__c,RPM__c,IMRT__c,SRS_SBRT__c,Paperless__c,Latitude__c,Longitude__c from Account where Id =: controller.getId()];
       
        if(acc.Selected_Groups__c != null){
            for(String str : acc.Selected_Groups__c.split('; ')){
                groupSet.add(str);
            }               
        }
       
       /**  //raj4/16
        accntSet.add(accntId);
        accGroupsMap = ProductUtils.getAccountSelectedGroups(accntSet);
         System.debug('###### Account Id 3 = '+accntId);
        groupSet = accGroupsMap.get(accntId);
        //rajraj4/16
        ***/ 
        //set<String> installedGroupSet = new Set<String>();
        //Get all the installed products list for this account
        System.debug('###### Account Id 4 = '+acc.Id);
        set<String> installedGroupSet = ProductUtils.getInstalledBasePublicGroupList(acc.Id);
        
        set<String> groupSetFromProductRoleCS = new set<String>();
        
        for(Product_Roles__c pr : [select Id,Product_Name__c,Public_Group__c from Product_Roles__c]){
            groupSetFromProductRoleCS.add(pr.Public_Group__c);
        }
        
        system.debug(groupSet + '------------groupSet---------------'+ installedGroupSet);
        
        for(Group gr : [SELECT Id, Name, Type FROM Group where type = 'Regular']){
            system.debug(gr.NAme + '------------groupSetFromProductRoleCS---------------'+ groupSetFromProductRoleCS.contains(gr.Name));
            system.debug(gr.Name + '------------installedGroupSet---------------'+ installedGroupSet.contains(gr.NAme));
            system.debug(gr.NAme + '------------groupSet---------------'+ groupSet.contains(gr.Name));
            if(groupSet.contains(gr.Name) && !installedGroupSet.contains(gr.Name)){
                selectedGroupList.add(new SelectOption(gr.Id,gr.Name));
                System.debug(gr.Id +'###### inIF = '+gr.Name);
            }else if(groupSetFromProductRoleCS.contains(gr.Name) && !installedGroupSet.contains(gr.Name)){
                groupList.add(new SelectOption(gr.Id,gr.Name));
                System.debug(gr.Id +'###### inELSEIF1 = '+gr.Name);
            }else if(installedGroupSet.contains(gr.NAme)){
                installedPrdList.add(new SelectOption(gr.Id,gr.Name));
                System.debug(gr.Id +'###### inELSEIF2 = '+gr.Name);
            }
        }
        system.debug(groupSet + '------------selectedGroupList---------------'+ selectedGroupList);
    }
    
    public PageReference selectclick(){
        rightselected.clear();
        List<SelectOption> options = new List<SelectOption>();
        set<String> temp = new set<String>();
        for(String s : leftselected){
            temp.add(s);
        }
        for(SelectOption s : groupList){
            if(!temp.contains(s.getValue())){
                options.add(s);
            }else{
                selectedGroupList.add(s);
            }
        }
        groupList.clear();
        groupList.addAll(options);
        return null;
    }
    
    public PageReference unselectclick(){
        system.debug('**Bfore - groupSet :'+groupSet);
        leftselected.clear();

        List<SelectOption> options = new List<SelectOption>();
        set<String> temp = new set<String>();
        for(String s : rightselected){
            temp.add(s);
        }
        for(SelectOption s : selectedGroupList){
            if(!temp.contains(s.getValue())){
                options.add(s);
            }else{
                groupList.add(s);
            }
        }
        selectedGroupList.clear();
        selectedGroupList.addAll(options);
        system.debug('**After - groupSet :'+groupSet);
        return null;
    }

    public void save(){
        set<String> newAddedGroupSet = new Set<String>();
        set<String> removedGroupSet = new Set<String>();
        String groupStr = ' ';
        //if(selectedGroupList.size() > 0){
            for(SelectOption so : selectedGroupList) {
                system.debug('***so.getLabel()***'+so.getLabel());
                if(!groupSet.contains(so.getLabel())){
                    newAddedGroupSet.add(so.getLabel());
                }
                groupStr = groupStr + '; ' + so.getLabel();
            }
            //if(groupSet.size() != selectedGroupList.size() - newAddedGroupSet.size()){
                for(String str : groupSet){
                    if(!groupStr.contains(str)){
                        removedGroupSet.add(str);
                    }
                }
            //}
            groupStr = groupStr.replaceAll(' ; ','');
            system.debug(newAddedGroupSet + '-------------newAddedGroupSet-------------' + groupSet);
            //Get Contact set
           
           //commented by Raj and replaced with product utils method
           /**** 
            Set<Id> ContactSet = new Set<Id>();
            for(Contact c : [select Id,AccountID from contact where AccountId =: acc.id]){
                ContactSet.add(c.Id);
            }
            
            //Get Group List
            List<Group> publicGroupLst = [SELECT Id, Name, Type FROM Group Where Name in : newAddedGroupSet];
            List<GroupMember > grpMemberLst = new  List<GroupMember >();
            //Get portal contact list
            Set<Id> portalUserIdSet = new Set<Id>();
            system.debug('=====added User=====>'+ContactSet);
            for(user u: [select id, name , ContactId from User where contactId in: ContactSet and Profile.Name =: Label.VMS_Customer_Portal_User and isActive != false]){
                portalUserIdSet.add(u.Id);
                for(Group grp : publicGroupLst){
                    GroupMember Obj= new Groupmember();
                    Obj.GroupID= grp.Id;
                    Obj.UserOrGroupId= u.Id;
                    grpMemberLst.add(Obj);        
                }
            }
            *****/ //End comment snippet by Raj for product util
            List<GroupMember > grpMemberLst = new  List<GroupMember >();
            grpMemberLst = ProductUtils.getPublicGroupMemberListForAccount(acc.id,newAddedGroupSet);
            
            if(grpMemberLst.size() > 0){
                insert grpMemberLst;
                // We need to add the newly added groupmembers to groupSet.
                groupSet.addAll(newAddedGroupSet);
            }
            
             //List<user> portalUserIdLst = new List<user> ();
           //  system.debug([select Id from GroupMember where UserOrGroupId in : portalUserIdSet and group.Name in : removedGroupSet] + '=====removedGroupSet=====>'+removedGroupSet);
            //delete [select Id from GroupMember where UserOrGroupId in : portalUserIdSet and group.Name in : removedGroupSet];
            
        	//get cra contacts - Added for CRA enhancement : STRY0026211
        	set<Id> craContactIds = new set<Id>(); 
            for (Contact_Role_Association__c cra : [select Id, Contact__c from Contact_Role_Association__c 
                                                    where Account__c =: acc.id]) {
                craContactIds.add(cra.Contact__c);
            } 
			        	
        	delete [select Id from GroupMember 
                    where UserOrGroupId in (Select Id 
                                            from User 
                                            where (ContactId in :craContactIds or AccountId =: acc.id) and IsActive = true) 
                    and group.Name in : removedGroupSet];
            
            // Remove the deleted group names from existed groupset.
            for(String removedGroup : removedGroupSet){
                if(groupSet.contains(removedGroup)){
                    groupSet.remove(removedGroup);
                }
            }
            system.debug('=====LISTTT=====>'+groupStr);
            acc.Selected_Groups__c = groupStr;
            //update acc;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Your changes have been saved.');
            ApexPages.addMessage(myMsg);
        //}
    }
    
     public Pagereference cancel(){
         return new pagereference('/'+acc.id);
     }
     public void updateAccountFields() {
         system.debug('==selected=>'+acc.Selected_Groups__c);
         update acc;
         
     }
}
global class RejectAllTimesheets implements Database.Batchable<sObject> {
	
    private string q ;

	private set<id> timesheetIds ;	
    
    global RejectAllTimesheets(string q){
        this.q = q;
    }
    
    global RejectAllTimesheets(set<id> timesheetIds){
        this.timesheetIds = timesheetIds;
    }
    
    
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      String qLocal = q;
      set<id> qTimesheetIds = timesheetIds;
       if(qLocal!= null){
           return Database.getQueryLocator(q);
       }else{
		   //SELECT Id FROM SVMXC_Timesheet__c where Id in ( \'<timesheet-id>\') Status__c = \'Submitted\'';
       	   return Database.getQueryLocator('SELECT Id FROM SVMXC_Timesheet__c where Id in : qTimesheetIds and Status__c = \'Submitted\'');
       	}
             
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
     	set<id> tmIds = new set<id>();
        list<SVMXC_Timesheet__c> tlist = (list<SVMXC_Timesheet__c>)scope;
        for(SVMXC_Timesheet__c t : tlist){
           tmIds.add(t.Id);
        }
        set<Id> pIds = (new Map<Id, ProcessInstance>([SELECT Id,Status,TargetObjectId FROM ProcessInstance where Status='Pending' and TargetObjectId in :tmIds])).keySet();
        Set<Id> pInstanceWorkitems = (new Map<Id, ProcessInstanceWorkitem>([SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem WHERE ProcessInstanceId in :pIds])).keySet();
        Approval.ProcessWorkitemRequest[] allReq = New Approval.ProcessWorkitemRequest[]{}; 
        for (Id pInstanceWorkitemsId:pInstanceWorkitems){
            system.debug(pInstanceWorkitemsId);
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setComments('System rejected');
                req2.setAction('Reject'); //to approve use 'Approve'
                req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                // Use the ID from the newly created item to specify the item to be worked
                req2.setWorkitemId(pInstanceWorkitemsId);
                // Add the request for approval
                allReq.add(req2);
        }
        Approval.process(allReq);
    }

   global void finish(Database.BatchableContext BC){
   }
    
}
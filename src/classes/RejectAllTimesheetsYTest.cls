@istest		
public class RejectAllTimesheetsYTest {
	
    public static testmethod void unittest1(){
        SVMXC_Timesheet__c t = new SVMXC_Timesheet__c();
        t.Start_Date__c = System.today();
        t.End_Date__c = system.today().adddays(3);
        t.Is_TEM__c = false;
        t.OwnerId = UserInfo.getUserId();
        insert t;
        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(t.id);
       
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        RejectAllTimesheets obj = new RejectAllTimesheets('SELECT Id FROM SVMXC_Timesheet__c where Status__c = \'Submitted\' and Is_TEM__c = false');
		Database.executeBatch(obj,20);        
    } 
}
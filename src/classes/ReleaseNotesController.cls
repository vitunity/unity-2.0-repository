public class ReleaseNotesController
{
    private String relatedRecordId;

    public List<NoteSection> Sections { get; set; }
    public Integer ContentHeight { get; set; }
    public Boolean HasAccess { get; set; }
    public String SelectedId { get; set; }

    public ReleaseNotesController()
    {
        HasAccess = false;
        Charket__WeChatAccount__c weChatAccount = [select Name from Charket__WeChatAccount__c order by CreatedDate limit 1];
        String currentOpenId = getOpenId(weChatAccount.Id);
        for(Charket__WeChatFollower__c follower : [select Charket__Type__c, Charket__Contact__c from Charket__WeChatFollower__c where Charket__OpenId__c = :currentOpenId])
        {
            if(follower.Charket__Type__c == 'Contact')
            {
                HasAccess = true;
                relatedRecordId = follower.Charket__Contact__c;
                break;
            }
        }

        if(HasAccess)
        {
            Sections = new List<NoteSection>();
            Map<String, List<Distribution>> sectionMap = new Map<String, List<Distribution>>();
            for(ContentDistribution cd : [select Name, DistributionPublicUrl, ContentVersionId, ContentVersion.FileType, ContentVersion.Description from ContentDistribution where RelatedRecordId = :weChatAccount.Id])
            {
                String docType = cd.Name.substringBetween('[', ']');
                if(String.isNotBlank(docType))
                {
                    docType = docType.toUpperCase();
                    String name = cd.Name.substringAfter(']');
                    if(!sectionMap.containsKey(docType))
                    {
                        sectionMap.put(docType, new List<Distribution>());
                    }

                    sectionMap.get(docType).add(new Distribution(name, cd));
                }
            }

            Integer leftSize = 0;
            Integer rightSize = 0;
            for(String key : sectionMap.keySet())
            {
                NoteSection section = new NoteSection();
                section.Title = key;
                section.Distributions = sectionMap.get(key);

                if(leftSize <= rightSize)
                {
                    section.Position = 'top:' + leftSize + 'px';
                    leftSize += section.Distributions.size() * 92 + 81;
                }
                else
                {
                    section.Position = 'top:' + rightSize + 'px;right:0px';
                    rightSize += section.Distributions.size() * 92 + 81;
                }

                Sections.add(section);
            }

            ContentHeight = leftSize > rightSize ? leftSize : rightSize;
        }
    }

    public PageReference viewReleaseNote()
    {
        for(NoteSection section : Sections)
        {
            for(Distribution dis : section.Distributions)
            {
                ContentDistribution distribution = dis.Distribution;
                if(distribution.Id == SelectedId)
                {
                    for(ContentDistribution existDistribution : [select ExpiryDate, DistributionPublicUrl from ContentDistribution where RelatedRecordId = :relatedRecordId and ContentVersionId = :distribution.ContentVersionId limit 1])
                    {
                        if(existDistribution.ExpiryDate < System.now().addMinutes(2))
                        {
                            existDistribution.ExpiryDate = System.now().addMinutes(30);

                            update existDistribution;
                        }

                        return new PageReference(existDistribution.DistributionPublicUrl);
                    }

                    Contact contact = [select OwnerId from Contact where Id = :relatedRecordId];
                    ContentDistribution newDistribution = new ContentDistribution();

                    newDistribution.Name = distribution.Name;
                    newDistribution.ContentVersionId = distribution.ContentVersionId;
                    newDistribution.PreferencesExpires = true;
                    newDistribution.ExpiryDate = System.now().addMinutes(30);
                    newDistribution.PreferencesAllowViewInBrowser = true;
                    newDistribution.PreferencesLinkLatestVersion = true;
                    newDistribution.PreferencesNotifyOnVisit = false;
                    newDistribution.OwnerId = contact.OwnerId;
                    newDistribution.RelatedRecordId = contact.Id;

                    insert newDistribution;

                    newDistribution = [select DistributionPublicUrl from ContentDistribution where Id = :newDistribution.Id];

                    return new PageReference(newDistribution.DistributionPublicUrl);
                }
            }
        }

        return null;
    }

    private String getOpenId(String weChatAccountId)
    {
        if(!Test.isRunningTest())
        {
            try
            {
                String wechatCode = ApexPages.currentPage().getParameters().get('code');
                if(String.isNotBlank(wechatCode) && String.isNotBlank(weChatAccountId))
                {
                    Charket.WeChatClient client = new Charket.WeChatClient(weChatAccountId);
                    Charket.WeChatApiOAuth oauth = client.getOAuth();

                    Charket.WeChatApiOAuth.AuthTokenResponse response = oauth.handleCallback(wechatCode, '');
                    return response.OpenId;
                }
            }
            catch(Exception ex) {}
        }
        else
        {
            return 'OPENID';
        }

        return null;
    }

    class NoteSection
    {
        public String Title { get; set; }
        public String Position { get; set; }
        public List<Distribution> Distributions { get; set; }
    }

    class Distribution
    {
        public String Name { get; set; }
        public ContentDistribution Distribution { get; set; }

        public Distribution(String name, ContentDistribution distribution)
        {
            this.Name = name;
            this.Distribution = distribution;
        }
    }
}
@isTest
private class ReleaseNotesControllerTest
{
    @isTest
    public static void testReleaseNotesController()
    {
        Account account = new Account(Name = 'UnitTestAccount');
        account.BillingStreet = 'soho';
        account.BillingCity = 'Beijing';
        account.BillingState = 'BJ';
        account.BillingPostalCode = '10000';
        account.Country__c = 'China';
        insert account;

        Contact contact = new Contact(AccountId = account.Id);
        contact.FirstName = 'UnitTestFirstName';
        contact.LastName = 'UnitTestLastName';
        contact.Email = 'unit@test.com';
        contact.Phone = '1234567890';
        contact.MobilePhone = '123456790';
        insert contact;

        Charket__WeChatAccount__c weChatAccount = new Charket__WeChatAccount__c(Name = 'UnitTestName');
        weChatAccount.Charket__WeChatOriginId__c = 'xxxxx';
        insert weChatAccount;

        Charket__WeChatFollower__c follower = new Charket__WeChatFollower__c(Charket__WeChatAccount__c = weChatAccount.Id);
        follower.Charket__OpenId__c = 'OPENID';
        follower.Charket__Contact__c = contact.Id;
        insert follower;

        List<ContentVersion> versions = new List<ContentVersion>();
        for(Integer i = 0; i < 5; i++)
        {
            ContentVersion version = new ContentVersion();
            version.Title = '[test1]UnitTestTitle';
            version.VersionData = Blob.valueOf('UnitTestData');
            version.PathOnClient = 'UnitTest.pdf';

            versions.add(version);
        }
        versions[0].Title = '[test2]UnitTestTitle';
        insert versions;

        ReleaseNotesController controller = new ReleaseNotesController();
        controller.viewReleaseNote();

        List<ContentDistribution> distributions = new List<ContentDistribution>();
        for(ContentVersion version : versions)
        {
            ContentDistribution dis = new ContentDistribution();
            dis.Name = version.Title;
            dis.ContentVersionId = version.Id;
            dis.PreferencesAllowViewInBrowser = true;
            dis.PreferencesLinkLatestVersion = true;
            dis.PreferencesNotifyOnVisit = false;
            dis.RelatedRecordId = weChatAccount.Id;
            distributions.add(dis);
        }
        insert distributions;

        controller = new ReleaseNotesController();
        controller.SelectedId = distributions[0].Id;

        controller.viewReleaseNote();

        ContentDistribution dis = [select Id from ContentDistribution where RelatedRecordId = :contact.Id];
        dis.ExpiryDate = System.now();
        update dis;

        controller.viewReleaseNote();
    }
}
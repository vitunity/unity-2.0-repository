public class ReopenCase{
    public static void makeUpdate(List<case> lstCase, map<Id,case> mapOLDCase){
        List<Case> lstUpdatableCase = new List<Case>();
        for(Case c : lstCase){
            if(c.status <> 'Closed' && mapOLDCase.get(c.Id).status == 'Closed'){
                c.ReOpen__c = true;
            }
        }
    }
}
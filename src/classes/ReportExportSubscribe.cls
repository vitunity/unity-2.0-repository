/***************************************************************************
Author: Amitkumar Katre
Created Date: 10-31-2017
Project/Story/Inc/Task : Get subscribed report in excel.
Description: Get subscribed report in excel.
Change Log:
*************************************************************************************/
public with sharing class ReportExportSubscribe implements Reports.NotificationAction {
    
    /*
     * Send email with report as an attachment
     */
    public void execute(Reports.NotificationActionContext context) {
        // Context parameter contains report instance and criteria
        Reports.ReportResults results;
        if(!Test.isRunningTest()){
            results = context.getReportInstance().getReportResults(); 
        }else{
            results =Reports.ReportManager.runReport('00O44000003RC5o');
        }
        
        // In the above subscription case this is the 'Record Count'
        //System.debug(context.getThresholdInformation().getEvaluatedConditions()[0].getValue());
        // You can also access the report definition!
        //System.debug(results.getReportMetadata().getName());
        //System.debug(results.getReportMetadata().getId()); 
        String reportId = results.getReportMetadata().getId();
        String format = 'xls';
        String requestUrl = '/' + reportId + '?'; //'00O44000003RIJ4'
        requestUrl += 'export=1&xf=' + format + '&enc=UTF-8';     
        String output;
        if(!Test.isRunningTest()){
            //output = new PageReference(requestUrl).getContent().toString();
        }else{
            output = 'Test Data';
        }
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('report_'+system.now()+'.xls');
        efa1.setContentType('application/vnd.ms-excel');
        if(!Test.isRunningTest())
        efa1.setBody(new PageReference(requestUrl).getContent());
        OrgWideEmailAddress owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('Scheduled Report -' + results.getReportMetadata().getName());
        mail.setTargetObjectId(Userinfo.getUserId());
        mail.setToAddresses(getEmailAddresses(results.getReportMetadata().getName()));
        mail.setPlainTextBody('Hello,\n\nPlease find attached report.\n\nThank you\nVarian Medical System');
        mail.setOrgWideEmailAddressId(owa.id);
        mail.setSaveAsActivity(false);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1});
        allmsg.add(mail);
        if(!Test.isRunningTest())
        Messaging.sendEmail(allmsg);
    }
    
    /*
     * get public group email address
     */
    private List<String> getEmailAddresses(String groupName) {
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>();
        list<Group> groupList = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name =: groupName];
        if(groupList.isEmpty()){
            return new List<String>();
        }else{
            for (GroupMember gm : groupList[0].groupMembers) {
              idList.add(gm.userOrGroupId);
            }
            User[] usr = [SELECT email FROM user WHERE id IN :idList];
            for(User u : usr) {
              mailToAddresses.add(u.email);
            }
            return mailToAddresses;
        }
        
    }
    
}
/***************************************************************************
Author: Amitkumar Katre
Created Date: 10-31-2017
Project/Story/Inc/Task : Get subscribed report in excel test class
Description: Get subscribed report in excel test class
Change Log:
*************************************************************************************/
@isTest
public class ReportExportSubscribeTest {
	
    public static testmethod void testReportExportSubscribe(){
        Reports.ReportInstance riObj = Reports.ReportManager.runAsyncReport('00O44000003RC5o');
        Reports.ThresholdInformation rtObj; 
        Reports.NotificationActionContext context = new Reports.NotificationActionContext(riObj, rtObj);
        ReportExportSubscribe obj  = new ReportExportSubscribe();
        obj.execute(context);
    }
}
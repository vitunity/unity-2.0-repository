/*
* Author: Harleen Gulati
* Created Date: 21-August-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This Class is used for RequestForQuoteUpgrade lightening page.
*/
public class RequestforUpgradesQuote{
    
    
    /* Check for logged in user */
    @AuraEnabled
    public static Boolean getCurrentUser() { 
        boolean externalUser;
        User u = [SELECT Id, LastName, Email, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            externalUser = false;
        }else{
            externalUser = true;
        }
        return externalUser;
    }
    
    /* load custom labels */
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);    
    }
    
    
    /* Get Country picklist values */
    @AuraEnabled
    public static List<String> getDynamicCountryPicklistOptions(String fieldName){
        List<String> pickListValues = new List<String>();
        system.debug('==fieldName=='+fieldName);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple){
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* Get product picklist values */
    @AuraEnabled
    public static List<String> getDynamicPicklistOptions(String fieldName){
        List<String> pickListValues = new List<String>();
        system.debug('==fieldName=='+fieldName);
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Product2');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* Save contact data and send email to user */   
    @AuraEnabled
    public static String registerMData(Contact registerDataObj, Account accDataObj,Product2 prodObj, 
                                        String prodV, String subject, String longDes,String fileName, 
                                        String base64Data, String contentType,
                                        String languageObj){
        
        //Code to Send email to user and admin     
        List<String> toAddresses = new List<String>();
        List<ProdCountEmailMapping__c > emailmap = new List<ProdCountEmailMapping__c>();
        List<String> touser = new List<String>();
        Datetime myDT = Datetime.now();
        String myDate = myDT.format('MM/dd/yyyy HH:mm:ss');
        String EmailBody;
        string reqtype;
        string Request = 'RequestforUpgradesQuote';
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment a = new Attachment();
        
        
        list<Contact> contactList = new list<Contact>();
        contactList = [select Id, OktaId__c, Name, Monte_Carlo_Registered__c, FirstName, LastName,Institute_Name__c , Account.name, Email, Salutation, Functional_Role__c, Specialty__c, PhotoUrl, MvMyFavorites__c, 
                                     Preferred_Language1__c, Fax, Phone, Manager__c, Manager_s_Email__c, Account.AccountNumber, Account.BillingStreet, Account.BillingCity, Account.BillingCountry, Account.BillingState, Account.BillingPostalCode,
                                     Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity, ShowAccPopUp__c
                                     from contact where id in (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId())];
        
        system.debug('==contactList=='+contactList );
         if(contactList.size()>0){
            system.debug('==Test==');
            registerDataObj.FirstName            =   contactList[0].FirstName;
            registerDataObj.LastName             =   contactList[0].LastName;
            registerDataObj.Institute_Name__c    =   contactList[0].Institute_Name__c;
            registerDataObj.Phone                =   contactList[0].Phone;
            registerDataObj.Email                =   contactList[0].Email;
            registerDataObj.MailingStreet        =   contactList[0].MailingStreet;
            registerDataObj.MailingState         =   contactList[0].MailingState;
            registerDataObj.MailingCity          =   contactList[0].MailingCity;
            registerDataObj.MailingCountry       =   contactList[0].MailingCountry;
            registerDataObj.MailingPostalcode    =   contactList[0].MailingPostalcode;
        
        }
        touser.add(registerDataObj.Email);
        
        
        emailmap=[Select e.email__c From ProdCountEmailMapping__c e where e.Name=: Request limit 1];
        if(emailmap.size() >0)
        {
            toAddresses.add(emailmap[0].email__c);
        } 
       
        map<String,String> cachedMap = MvUtility.getCustomLabelMap(languageObj);
        system.debug('==cachedMap=='+cachedMap.keyset());  
        system.debug('==languageObj=='+languageObj);  
        reqtype = 'Request for Upgrades Quote';
        EmailBody = cachedMap.get('Mv_Mail_Template_Upgrade_Msg')+'<br><br>'
        +cachedMap.get('Mv_Mail_Template_Thanks_Msg')+'<br><br>'+cachedMap.get('Mv_Mail_Template_VMS');
        
        if(Request != 'PaperDocumentation' ){
            //EmailBody= EmailBody+ '<br><br>'+cachedMap.get('Mv_Mail_Template_Submitted_On') +': '+myDate+ '<br>'+cachedMap.get('Mv_Mail_Template_Name')+': '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get('Mv_Mail_Template_Institution')+': '+registerDataObj.Institute_Name__c+'<br>'+cachedMap.get('Mv_Mail_Template_Telephone')+': '+registerDataObj.Phone+'<br><br>'+cachedMap.get('Mv_Mail_Template_Email')+': '+registerDataObj.Email+ '<br>'+cachedMap.get('Mv_Mail_Template_City')+': ' +registerDataObj.MailingCity +'<br>'+cachedMap.get('Mv_Mail_Template_State')+': '+registerDataObj.MailingState+'<br>'+cachedMap.get('Mv_Mail_Template_Postal_Code')+': '+registerDataObj.MailingPostalCode +'<br><br>'+cachedMap.get('Mv_Mail_Template_Request_type')+': '+ request+'<br>'+cachedMap.get('Mv_Mail_Template_Product')+': '+prodObj.Product_Group__c+'<br>'+cachedMap.get('Mv_Mail_Template_Version')+': '+prodV +'<br>'+cachedMap.get('Mv_Mail_Template_Subject')+': '+subject+'<br>'+longDes;
            EmailBody= EmailBody+ '<br><br>'+cachedMap.get('Mv_Mail_Template_Submitted_On') +': '+myDate+ '<br>'+cachedMap.get('Mv_Mail_Template_Name')+': '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get('Mv_Mail_Template_Institution')+': '+registerDataObj.Institute_Name__c+'<br>'+cachedMap.get('Mv_Mail_Template_Telephone')+': '+registerDataObj.Phone+'<br><br>'+cachedMap.get('Mv_Mail_Template_Email')+': '+registerDataObj.Email+ '<br>'+cachedMap.get('Mv_Mail_Template_City')+': ' +registerDataObj.MailingCity +'<br>'+cachedMap.get('Mv_Mail_Template_State')+': '+registerDataObj.MailingState+'<br>'+cachedMap.get('Mv_Mail_Template_Postal_Code')+': '+registerDataObj.MailingPostalCode +'<br><br>'+cachedMap.get('Mv_Mail_Template_Request_type')+': '+ request+'<br>'+cachedMap.get('Mv_Mail_Template_Product')+': '+prodObj.Product_Group__c+'<br>'+longDes;
            
            //EmailBody=EmailBody+'<br><br>Address: '+CustStreet1 +'<br>City: '+CustCity1 +'<br>State:  '+CustState1+'<br>Country: '+Countryvalue +'<br><br>Type of Request: '+ reqtype+'<br>Product: '+Productvalue+'<br>Version: '+Productversion +'<br><br>Subject: '+ReqSubject+'<br><br>'+longdesc ;
        }
        
        Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
        
        MailTicket.setSubject(reqtype +' successfully delivered ');
        MailTicket.setHTMLBody(EmailBody);
        
        MailTicket.setToAddresses(touser);
        
        // Use Organization Wide Address  
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress])
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress))
                MailTicket.setOrgWideEmailAddressId(owa.id);
            
        } 
        
        MailTicket.setReplyTo(System.Label.MVsupport);  
        MailTicket.setSaveAsActivity (false);
        
        String body ='';
        
        if(Request != 'PaperDocumentation' )
        {
            body ='<br><br>'+cachedMap.get('Mv_Mail_Template_Submitted_On') +myDate+ '<br>'+cachedMap.get('Mv_Mail_Template_Name') +registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>'+cachedMap.get('Mv_Mail_Template_Institution') +registerDataObj.Institute_Name__c+'<br>'+cachedMap.get('Mv_Mail_Template_Telephone') +registerDataObj.Phone+'<br><br>'+cachedMap.get('Mv_Mail_Template_Email') +registerDataObj.Email ;
            body = body +'<br>'+cachedMap.get('Mv_Mail_Template_City') +registerDataObj.MailingCity+'<br>'+cachedMap.get('Mv_Mail_Template_State')  +registerDataObj.MailingState+'<br>'+cachedMap.get('Mv_Mail_Template_Country') +registerDataObj.MailingCountry +'<br><br>'+cachedMap.get('Mv_Mail_Template_Request_Type') + request+'<br>'+cachedMap.get('Mv_Mail_Template_Product') +prodObj.Product_Group__c+'<br>'+cachedMap.get('Mv_Mail_Template_Version') +prodV+'<br><br>'+cachedMap.get('Mv_Mail_Template_Subject') +subject+'<br><br>'+longDes ;
            body = body +'<br><br>{'+cachedMap.get('Mv_Mail_Template_USER_MAIL')+':['+registerDataObj.Email+']}';
        }
        
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        mailAdmin.setSubject(reqtype);
        mailAdmin.setHTMLBody(body);
        mailAdmin.setToAddresses(toAddresses);
        
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]){ 
            if(owa.Address.contains(system.label.CpOrgWideAddress)) {
                mailAdmin.setOrgWideEmailAddressId(owa.id);
            }
        }
        
        if(base64Data != null && fileName != null)
        {
            blob b = EncodingUtil.base64Decode(base64Data);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileName);
            efa.setBody(b);
            
            base64Data = null;
            MailTicket.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            mailAdmin.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        } 
        
        mailAdmin.setTreatTargetObjectAsRecipient(false); 
        mailAdmin.setReplyTo(System.Label.MVsupport);     
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
        if(emailmap.size() >0)
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin }); 
        }              
        
        return 'accessdocs';
    }
}
/********************************************************************
Created By: Naren Godugula
This Test class covers 1)Review_Products_AfterTrigger -- Trigger on Product Profile
                        2)deleteCorrespondingCEFs --  Trigger on Product Profile
                        3)UpdateMileStoneSelection_CEFs -- Trigger on Country Rule
*********************************************************************/
@isTest()
private class Review_Products_AfterTriggerTest {

  static testMethod void myUnitTest() 
    {
        
        insert new Clearance_Entry_Documents__c(Name='DoC Landing Page', Link__c='http://dms.varian.com/livelink/livelink.exe/fetch/2000/1621351/1754337/5302729/6249704/Declaration_of_Conformity_Index_September_28_2017.pdf?nodeid=34147447&vernum=-2');
        insert new Clearance_Entry_Documents__c(Name='IFU Translation', Link__c='http://dms.varian.com/livelink/livelink.exe?func=ll&objaction=overview&objid=98277322');

        Country__c cntry = new Country__c();
        cntry.Name = 'DubDub';
        cntry.Region__c = 'USA';
        cntry.Expiry_After_Years__c = '1';
        
        insert cntry;
        
        Country_Rules__c CR1 = new Country_Rules__c();
        CR1.Country__c = cntry.id;
        CR1.Weeks_to_Prepare_Submission__c = 2;
        CR1.Weeks_Estimated_Review_Cycle__c = 3;
        Insert CR1; 
        
        //create one review product record
        Review_Products__c rp = new Review_Products__c();
        rp.Product_Profile_Name__c = 'test product profile';
        rp.Version_No__c = '11';
        rp.Clearance_Family__c = 'BrachyVision';
        rp.Business_Unit__c = 'VBT';
        rp.Regulatory_Name__c = 'Regular name';
        rp.Transition_Phase_Gate__c = system.today();
        rp.National_Language_Support__c = system.today();
        rp.Design_Output_Review__c = system.today();
        //try{
        insert rp; // this should create Input clearance record for Country (cntry) record
        //}catch(exception e){
        //}
        Country__c cntry2 = new Country__c();
        cntry2.Name = 'DubaDuba';
        cntry2.Region__c = 'USA';
        cntry2.Expiry_After_Years__c = '1';
        insert cntry2;
        
        CR1.TPG_True__c = True;
        CR1.Weeks_to_Prepare_Submission__c = 1;
        CR1.Weeks_Estimated_Review_Cycle__c = 2;
        Update CR1; 
        
       /* Country_Rules__c CR2 = new Country_Rules__c();
        CR2.Country__c = cntry2.id;
        CR2.Weeks_to_Prepare_Submission__c = 2;
        CR2.Weeks_Estimated_Review_Cycle__c = 3;
        Insert CR2;
        
        CR1.TPG_True__c = True;
        Update CR2; */
        
        rp.Product_Profile_Name__c = 'test product profile2';
        rp.Transition_Phase_Gate__c = system.today()+2;
        rp.National_Language_Support__c = system.today()+2;
        rp.Design_Output_Review__c = system.today()+2;
        //try{
            Update rp;
            Delete rp;
        //}catch(exception e){
        //}
        
    }
    
  }
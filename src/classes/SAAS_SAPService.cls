/**
 * @Author Krishna Katve
 * @Description this service is getting used in boomi process ARIA Notification v1 to generate contract conditions data using quote products
 */
global class SAAS_SAPService {
    
    webservice static List<ContractCondition> getContractConditions() {
        List<ContractCondition> contractConditions = new List<ContractCondition>();
        for(BigMachines__Quote_Product__c quoteProduct : [
           SELECT Id, Target_Price__c, BigMachines__Sales_Price__c, BigMachines__Quantity__c, Group_Sequence__c,
           BigMachines__Quote__c, BigMachines__Quote__r.Name, Subscription_Product_Type__c
           FROM BigMachines__Quote_Product__c
           WHERE BigMachines__Quote__r.Subscription_Status__c = 'Process'
           AND Name NOT IN: System.Label.SAAS_Dummy_Parts.split(',')
        ]){
            contractConditions.add(
               new ContractCondition(quoteProduct, System.Label.SAP_Condition_Sales)
            );
            contractConditions.add(
               new ContractCondition(quoteProduct, System.Label.SAP_Condition_Target)
            );
        }
        System.debug('-----contractConditions'+contractConditions);
        return contractConditions;
    }
    
    global class ContractCondition {
        webservice Id quoteId;
        webservice String conditionType;
        webservice Decimal amount;
        webservice Decimal groupSequence;
        webservice String conditionKey;
        public ContractCondition(BigMachines__Quote_Product__c quoteProduct, String conditionType){
            this.quoteId = quoteProduct.BigMachines__Quote__c;
            this.conditionType = conditionType;
            if(conditionType == System.Label.SAP_Condition_Sales){
                this.amount = quoteProduct.BigMachines__Sales_Price__c;
            }else{
                this.amount = quoteProduct.Target_Price__c!=null?(quoteProduct.Target_Price__c/quoteProduct.BigMachines__Quantity__c):0.0;
            }
            this.groupSequence = quoteProduct.Group_Sequence__c;
            this.conditionKey = quoteProduct.BigMachines__Quote__r.Name+''+quoteProduct.Subscription_Product_Type__c;
        }
    }
}
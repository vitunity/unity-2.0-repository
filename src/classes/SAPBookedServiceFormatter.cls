/*
 * @Author - Ajinkya Raut
 * @Description - Added to fix issue with SAP_Booked_Service field on Quote Detail Layout
 * @Date - 11August2016
 */
 
 public class SAPBookedServiceFormatter{
    
    private String data;
    private Integer noOfContratcsInLine;
    
    public SAPBookedServiceFormatter(String data, Integer noOfContratcsInLine){
        this.data = data.replaceAll('\\n','');
        this.noOfContratcsInLine = noOfContratcsInLine;
    }
    
    public String getFormattedContracts(){
        replaceNewLines();
        integer i = 0;
        String [] splitData = data.split(',',-2);
        String fomattedData = '';
        for(String d : splitData ){
            i++;
            fomattedData += d + (splitData.size()!= i?',':'');
            if(math.mod(i,noOfContratcsInLine) == 0 && i != 0){
                fomattedData += '\n';
            }
        }
        return fomattedData;
    }
    
    public void replaceNewLines(){
        data = data.replace('\r\n', '');
        data = data.replace('\n', '');
        data = data.replace('\r', '');
    
    }
    
}
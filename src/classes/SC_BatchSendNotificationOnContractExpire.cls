global class SC_BatchSendNotificationOnContractExpire implements Schedulable{
   global void execute(SchedulableContext sc) {
   
      date dt1=System.Today();
      date dt2 = System.Today()+90;
   
     
      string str='select id ,SVMXC__SLA_Terms__c,SVMXC__Product__c,SVMXC__End_Date__c,SVMXC__Service_Contract__c, '+
                 'SVMXC__Service_Contract__r.SVMXC__End_Date__c, SVMXC__Service_Contract__r.SVMXC__Active__c  from  SVMXC__Service_Contract_Products__c ' + 
                 'where  (( SVMXC__End_Date__c <: dt2  AND   SVMXC__End_Date__c >: dt1 ) OR (SVMXC__End_Date__c =:dt2 ) OR ( SVMXC__End_Date__c =: dt1 )) AND ' + 
                 'SVMXC__Service_Contract__r.ERP_Cancellation_Reason__c = null  ';
      if(Test.isRunningTest())
        {
        str += ' Limit 1';
        }
      BatchOpportunityCreaterFromContract b =new BatchOpportunityCreaterFromContract(str); 
      database.executebatch(b);
   }
}
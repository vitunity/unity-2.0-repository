@isTest(SeeAllData=true)
public class SC_ClassTechnician_Equipment_test
{
    public Static Profile pr = [Select id from Profile where name = 'VMS BST - Member'];
        
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', 
                                    lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', 
                                    username = 'standardtestuse92@testclass.com');
    //record type Id's
        public static Id equipmentToolRecTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId();    //Equipment/Tools on Location
        public static Id LocationStandard = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Standard Location').getRecordTypeId(); //de3590 Standard Location on Location

    public static testMethod void SC_ClassTechnician_Equipment_test()
    {
        list<Country__c> listCountry = new list<Country__c>();                                                      //list to insert country record
        list<SVMXC__Site__c> listLocationInsert = new list<SVMXC__Site__c>();                                       //list to insert location record
        list<SVMXC__Service_Group_Members__c> listTechEquipInsert = new list<SVMXC__Service_Group_Members__c>();    //list to insert Technician record

        //query describe call on Technician/Equipment
        Schema.DescribeSObjectResult TechEquip = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
        Map<String,Schema.RecordTypeInfo> TechEquipMapByName = TechEquip.getRecordTypeInfosByName();

        /************* Territory Data ***************/
        SVMXC__Territory__c varTer = new SVMXC__Territory__c();
        varTer.SVMXC__Territory_Code__c = 'TEST1';
        insert varTer;

        /*********** Insert Service Team Data *********/
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c(SVMXC__Active__c= true, ERP_Reference__c = 'TestExternal', Name = 'TestService', Dispatch_Queue__c = 'DISP BR', SVMXC__Group_Code__c = 'TestExternal');
        Insert ServiceTeam;

        /************* Country Data********************/
        //Country Record 1
        Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
        listCountry.add(TestCountry1);
        //Country Record 2
        Country__c TestCountry2 = new Country__c(SAP_Code__c  ='India', Name='test', ISO_Code_3__c = 'India');
        listCountry.add(TestCountry2);
        //Country Record 3
        Country__c TestCountry3 = new Country__c(SAP_Code__c  ='AUS', Name='test', ISO_Code_3__c = 'test');
        listCountry.add(TestCountry3);

        insert listCountry;

        /**************Location Data******************/
        //location Record 1
        SVMXC__Site__c Loc1 = new SVMXC__Site__c(name = 'Test loc1', RecordTypeId = LocationStandard, SVMXC__Location_Type__c ='Field', Work_Center__c = 'H162tk');
        listLocationInsert.add(Loc1);
        //location Record 2
        SVMXC__Site__c Loc3 = new SVMXC__Site__c(name = 'Test loc3', RecordTypeId = equipmentToolRecTypeId, SVMXC__Location_Type__c ='Field', Work_Center__c = 'H162tk');
        listLocationInsert.add(Loc3);
        //location Record 3
        SVMXC__Site__c Loc2 = new SVMXC__Site__c(name = 'Test loc2', RecordTypeId = LocationStandard);
        listLocationInsert.add(Loc2);

        insert listLocationInsert;

        /************ Technician/Equipment *************/
        
        List<BusinessHours> listBusinessHrs = [Select ID from BusinessHours limit 1]; 
        SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c ();
        objTechEquip.Name = 'Test Technician';
        objTechEquip.SVMXC__Active__c = true;
        objTechEquip.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        objTechEquip.SVMXC__Enable_Scheduling__c = true;
        objTechEquip.SVMXC__Phone__c = '987654321';
        objTechEquip.SVMXC__Email__c = 'abc@xyz.com';
        objTechEquip.SVMXC__Service_Group__c = ServiceTeam.id;
        objTechEquip.SVMXC__Street__c = 'Test Street';
        objTechEquip.SVMXC__Zip__c = '98765';
        objTechEquip.SVMXC__Country__c = 'United States'; 
        objTechEquip.OOC__c = false;
        objTechEquip.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
        objTechEquip.User__c = userInfo.getUserID();
        objTechEquip.SVMXC__Inventory_Location__c = loc1.id;
        objTechEquip.Dispatch_Queue__c = 'DISP CH';   
        Insert objTechEquip ;
        
 


        
        
        //Record 1 : Technician
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(OOC__c = true, ERP_Work_Center__c = 'H162tk',SVMXC__Service_Group__c = ServiceTeam.id, User__c = UserInfo.getUserId(), ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'USA', Name = 'TestTechnician', Recordtypeid =TechEquipMapByName.get('Technician').getRecordTypeId(), Current_End_Date__c = system.today()+7, SVMXC__Email__c = 'standarduser@testorg.com', ERP_Country_Code__c = 'USA', Dispatch_Queue__c = 'DISP - AMS', ERP_CSS_District__c = 'TEST1',Default_plant__c = '4400');
        
        listTechEquipInsert.add(tech);

        //Record 2 : Equipment
        SVMXC__Service_Group_Members__c equip1 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = ServiceTeam.id ,User__c=UserInfo.getUserId(),ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'India', Name = 'TestTechnician', Recordtypeid = TechEquipMapByName.get('Equipment').getRecordTypeId(), Current_End_Date__c = system.today()+7, SVMXC__Email__c = 'standarduser@testorg.com',ERP_Country_Code__c = 'India', Dispatch_Queue__c = 'DISP - AMS', SVMXC__Inventory_Location__c = loc1.id, ERP_CSS_District__c = 'TEST1');        
        listTechEquipInsert.add(equip1);

        //record 3 : Equipment
        SVMXC__Service_Group_Members__c equip2 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = ServiceTeam.id ,User__c=UserInfo.getUserId(),ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'India', Name = 'TestTechnician', Recordtypeid = TechEquipMapByName.get('Equipment').getRecordTypeId(), Current_End_Date__c = system.today()-2, SVMXC__Email__c = 'standarduser@testorg.com',ERP_Country_Code__c = 'India', Dispatch_Queue__c = 'DISP - AMS', SVMXC__Inventory_Location__c = loc3.id, ERP_CSS_District__c = 'TEST1');        
        listTechEquipInsert.add(equip2);

        insert listTechEquipInsert; 

        //update record Equipment 1:
        equip1.SVMXC__Country__c ='AUS';
        equip1.SVMXC__Inventory_Location__c = Loc2.id;
        update equip1;

        listTechEquipInsert[0].ERP_Reference__c = 'TE1';
        listTechEquipInsert[1].ERP_Reference__c = 'TE2';
        listTechEquipInsert[1].Current_End_Date__c = system.today() + 15;
        listTechEquipInsert[2].ERP_Reference__c = 'TE3';
        listTechEquipInsert[2].Current_End_Date__c = system.today() + 15;
        update listTechEquipInsert;

        Set<id> techids = new set<id>();
        techids.add(listTechEquipInsert[0].id);
        listLocationInsert[0].technician__c = listTechEquipInsert[0].id;
        listLocationInsert[1].technician__c = listTechEquipInsert[0].id;
        listLocationInsert[2].technician__c = listTechEquipInsert[0].id;
        update listLocationInsert;
        
        Set<id> locids = new set<id>();
        locids.add(listLocationInsert[0].id);
        SC_ClassTechnician_Equipment.deactivateTechnicianRecords(techids);
        SC_ClassTechnician_Equipment.deactivateLocationRecords(locids);
        SC_ClassTechnician_Equipment.activateLocationRecords(locids);
        /* Wave 1C
            DateTime LunchStdt = DateTime.newInstance(2014, 11, 3, 1, 0, 0); 
            DateTime NrmlStdt = DateTime.newInstance(2014, 11, 20, 5, 0, 0); 
            DateTime NrmlEddt = DateTime.newInstance(2014, 11, 20, 9, 0, 0);
            Timecard_Profile__c testTP = new Timecard_Profile__c(); 
            testTP.Name = 'Test Profile';
            testTP.Lunch_Time_Start__c = LunchStdt;
            testTP.Normal_Start_Time__c = NrmlStdt ;
            testTP.Normal_End_Time__c = NrmlEddt;
            testTP.Hide_Holiday_Cash_hours__c = True;
            testTP.Lunch_Time_duration__c = '60';
            testTP.X30_min_Travel_Time__c = True;
            testTP.Track_Meal_Time__c = True;
            testTP.Track_Dosimetric_Readings__c = True;
            testTP.Stopping_Time_Card_Completion__c = 'Cancelled';
            testTP.Fetch_Direct_Hours__c = True;
            testTP.No_of_weeks_in_advance_timecard_created__c = '15';
            testTP.No_of_weeks_in_arrears_timecard_created__c = '15';
            testTP.First_Day_of_Week__c = 'Sunday';
            testTP.Uneven_hours__c = True;
            testTP.Max_Hours_per_Week__c ='70';
            testTP.Max_Hours_Day_1__c = '8.0';
            testTP.Max_Hours_Day_2__c = '8.0';
            testTP.Max_Hours_Day_3__c = '8.0';
            testTP.Max_Hours_Day_4__c = '8.0';
            testTP.Max_Hours_Day_5__c = '8.0';
            testTP.Max_Hours_Day_6__c = '8.0';
            testTP.Max_Hours_Day_7__c = '8.0';
            insert testTP;
        Wave1C */
    }
    
    public static testMethod void SC_ClassTechnician_Equipment_test1()
    {
        list<Country__c> listCountry = new list<Country__c>();                                                      //list to insert country record
        list<SVMXC__Site__c> loclist = new list<SVMXC__Site__c>();                                       //list to insert location record
        list<SVMXC__Service_Group_Members__c> listTechEquipInsert = new list<SVMXC__Service_Group_Members__c>();    //list to insert Technician record

        //query describe call on Technician/Equipment
        Schema.DescribeSObjectResult TechEquip = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
        Map<String,Schema.RecordTypeInfo> TechEquipMapByName = TechEquip.getRecordTypeInfosByName();

        /************* Territory Data ***************/
        SVMXC__Territory__c varTer = new SVMXC__Territory__c();
        varTer.SVMXC__Territory_Code__c = 'TEST1';
        insert varTer;

        /*********** Insert Service Team Data *********/
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c(SVMXC__Active__c= true, ERP_Reference__c = 'TestExternal', Name = 'TestService', Dispatch_Queue__c = 'DISP BR', SVMXC__Group_Code__c = 'TestExternal');
        Insert ServiceTeam;

        /************* Country Data********************/
        //Country Record 1
        Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
        listCountry.add(TestCountry1);
        //Country Record 2
        Country__c TestCountry2 = new Country__c(SAP_Code__c  ='India', Name='test', ISO_Code_3__c = 'India');
        listCountry.add(TestCountry2);
        //Country Record 3
        Country__c TestCountry3 = new Country__c(SAP_Code__c  ='AUS', Name='test', ISO_Code_3__c = 'test');
        listCountry.add(TestCountry3);

        insert listCountry;

        /**************Location Data******************/
        //location Record 1

        
            SVMXC__Site__c objLoc = new SVMXC__Site__c();
            objLoc.Name = 'Test Location';
            objLoc.SVMXC__Street__c = 'Test Street';
            objLoc.SVMXC__Country__c = 'United States';
            objloc.Sales_Org__c = '0600';
            objLoc.SVMXC__Zip__c = '98765';
            objLoc.ERP_Functional_Location__c = 'Test Location';
            objLoc.Plant__c = 'Test Plant';
            objLoc.ERP_Site_Partner_Code__c = '123456';
            objLoc.SVMXC__Location_Type__c = 'Depot';
            objloc.country__c = listCountry[0].id;
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            objLoc.SVMXC__Stocking_Location__c = true;
            //objLoc.SVMXC__Account__c = testAcc.id;
            loclist.add(objLoc);
            SVMXC__Site__c objLoc1 = new SVMXC__Site__c();
            objLoc1.Name = 'Test Location';
            objLoc1.SVMXC__Street__c = 'Test Street';
            objLoc1.SVMXC__Country__c = 'United States';
            objloc1.Sales_Org__c = '0600';
            objLoc1.SVMXC__Zip__c = '98765';
            objLoc1.ERP_Functional_Location__c = 'Test Location';
            objLoc1.Plant__c = 'Test Plant';
            objLoc1.ERP_Site_Partner_Code__c = '123456';
            objLoc1.SVMXC__Location_Type__c = 'Field';
            objloc1.country__c = listCountry[0].id;
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            objLoc1.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            objLoc1.SVMXC__Stocking_Location__c = true;
            //objLoc1.SVMXC__Account__c = testAcc.id;
            loclist.add(objLoc1);
            insert loclist;

        //insert listLocationInsert;

        /************ Technician/Equipment *************/
        //Record 1 : Technician
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(OOC__c = true, ERP_Work_Center__c = 'H162tk',SVMXC__Service_Group__c = ServiceTeam.id, User__c = UserInfo.getUserId(), ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'USA', Name = 'TestTechnician', Recordtypeid =TechEquipMapByName.get('Technician').getRecordTypeId(), Current_End_Date__c = system.today()+7, SVMXC__Email__c = 'standarduser@testorg.com', ERP_Country_Code__c = 'USA', Dispatch_Queue__c = 'DISP - AMS', ERP_CSS_District__c = 'TEST1',Default_plant__c = '4400');
        
        listTechEquipInsert.add(tech);

        //Record 2 : Equipment
        SVMXC__Service_Group_Members__c equip1 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = ServiceTeam.id ,User__c=UserInfo.getUserId(),ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'India', Name = 'TestTechnician', Recordtypeid = TechEquipMapByName.get('Equipment').getRecordTypeId(), Current_End_Date__c = system.today()+7, SVMXC__Email__c = 'standarduser@testorg.com',ERP_Country_Code__c = 'India', Dispatch_Queue__c = 'DISP - AMS', SVMXC__Inventory_Location__c = objloc.id, ERP_CSS_District__c = 'TEST1');        
        listTechEquipInsert.add(equip1);

        //record 3 : Equipment
        SVMXC__Service_Group_Members__c equip2 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = ServiceTeam.id ,User__c=UserInfo.getUserId(),ERP_Reference__c = 'TextExternal',SVMXC__Country__c = 'India', Name = 'TestTechnician', Recordtypeid = TechEquipMapByName.get('Equipment').getRecordTypeId(), Current_End_Date__c = system.today()-2, SVMXC__Email__c = 'standarduser@testorg.com',ERP_Country_Code__c = 'India', Dispatch_Queue__c = 'DISP - AMS', SVMXC__Inventory_Location__c = objloc1.id, ERP_CSS_District__c = 'TEST1');        
        listTechEquipInsert.add(equip2);

        insert listTechEquipInsert;

        //update record Equipment 1:
        equip1.SVMXC__Country__c ='AUS';
        equip1.SVMXC__Inventory_Location__c = loclist[1].id;
        update equip1;

        listTechEquipInsert[0].ERP_Reference__c = 'TE1';
        listTechEquipInsert[1].ERP_Reference__c = 'TE2';
        listTechEquipInsert[1].Current_End_Date__c = system.today() + 15;
        listTechEquipInsert[2].ERP_Reference__c = 'TE3';
        listTechEquipInsert[2].Current_End_Date__c = system.today() + 15;
        update listTechEquipInsert;

        Set<id> techids = new set<id>();
        techids.add(listTechEquipInsert[0].id);
        loclist[0].technician__c = listTechEquipInsert[0].id;
        loclist[1].technician__c = listTechEquipInsert[0].id;
        update loclist;
        
        Set<id> locids = new set<id>();
        locids.add(loclist[0].id);
        SC_ClassTechnician_Equipment.deactivateTechnicianRecords(techids);
        SC_ClassTechnician_Equipment.deactivateLocationRecords(techids);
        SC_ClassTechnician_Equipment.activateLocationRecords(techids);
        /* Wave 1C
            DateTime LunchStdt = DateTime.newInstance(2014, 11, 3, 1, 0, 0); 
            DateTime NrmlStdt = DateTime.newInstance(2014, 11, 20, 5, 0, 0); 
            DateTime NrmlEddt = DateTime.newInstance(2014, 11, 20, 9, 0, 0);
            Timecard_Profile__c testTP = new Timecard_Profile__c(); 
            testTP.Name = 'Test Profile';
            testTP.Lunch_Time_Start__c = LunchStdt;
            testTP.Normal_Start_Time__c = NrmlStdt ;
            testTP.Normal_End_Time__c = NrmlEddt;
            testTP.Hide_Holiday_Cash_hours__c = True;
            testTP.Lunch_Time_duration__c = '60';
            testTP.X30_min_Travel_Time__c = True;
            testTP.Track_Meal_Time__c = True;
            testTP.Track_Dosimetric_Readings__c = True;
            testTP.Stopping_Time_Card_Completion__c = 'Cancelled';
            testTP.Fetch_Direct_Hours__c = True;
            testTP.No_of_weeks_in_advance_timecard_created__c = '15';
            testTP.No_of_weeks_in_arrears_timecard_created__c = '15';
            testTP.First_Day_of_Week__c = 'Sunday';
            testTP.Uneven_hours__c = True;
            testTP.Max_Hours_per_Week__c ='70';
            testTP.Max_Hours_Day_1__c = '8.0';
            testTP.Max_Hours_Day_2__c = '8.0';
            testTP.Max_Hours_Day_3__c = '8.0';
            testTP.Max_Hours_Day_4__c = '8.0';
            testTP.Max_Hours_Day_5__c = '8.0';
            testTP.Max_Hours_Day_6__c = '8.0';
            testTP.Max_Hours_Day_7__c = '8.0';
            insert testTP;
        Wave1C */
    }
    
    public static testMethod void activateLocationRecords_Test() {
        BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        
        Timecard_Profile__c TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        SVMXC__Service_Group__c servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        SVMXC__Service_Group_Members__c technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(false, newAcc);
        newLoc.SVMXC__Stocking_Location__c = true;
        newLoc.Technician__c = technician.Id;
        insert newLoc;

        SVMXC__Service_Group_Members__c technician1 = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician1.Timecard_Profile__c = TCprofile.Id;
        technician1.User__c = userInfo.getUserId();
        technician1.SVMXC__Inventory_Location__c = newLoc.Id;
        insert technician1;
    
        list<Country__c> listCountry = new list<Country__c>();
        Country__c TestCountry1 = new Country__c(SAP_Code__c  ='USA', Name='USA', ISO_Code_3__c = 'USA');
        listCountry.add(TestCountry1);
        
        SVMXC__Site__c objLoc1 = new SVMXC__Site__c();
        objLoc1.Name = 'Test Location123';
        objLoc1.SVMXC__Street__c = 'Test Street 1';
        objLoc1.SVMXC__Country__c = 'United States';
        objloc1.Sales_Org__c = '0600';
        objLoc1.SVMXC__Zip__c = '98765';
        objLoc1.ERP_Functional_Location__c = 'Test Location';
        objLoc1.Plant__c = 'Test Plant';
        objLoc1.ERP_Site_Partner_Code__c = '123456';
        objLoc1.SVMXC__Location_Type__c = 'Retired';
        objloc1.country__c = listCountry[0].id;
        objLoc1.SVMXC__Service_Engineer__c = userInfo.getUserID();
        objLoc1.SVMXC__Stocking_Location__c = true;
        insert objLoc1;
        
        technician1.SVMXC__Inventory_Location__c = objLoc1.Id;
        update technician1;
    
        Test.StartTest();
            SC_ClassTechnician_Equipment.activateLocationRecords(new Set<Id>{technician.Id});
        Test.StopTest();
    }
    
    public static testMethod void deactivateLocationRecords_test() {
        BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        
        Timecard_Profile__c TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        SVMXC__Service_Group__c servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        SVMXC__Service_Group_Members__c technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(false, newAcc);
        newLoc.SVMXC__Stocking_Location__c = true;
        newLoc.Technician__c = technician.Id;
        insert newLoc;
    
        Test.StartTest();
            SC_ClassTechnician_Equipment.deactivateLocationRecords(new Set<Id>{technician.Id});
            SC_ClassTechnician_Equipment.deactivateTechnicianRecords(new Set<Id>{userInfo.getUserID()});
        Test.StopTest();
    }
    
    @isTest(SeeAllData=true)
    public static void updateOOCFlag_test() {
        BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        
        Timecard_Profile__c TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        SVMXC__Service_Group__c servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        Map<String,Schema.RecordTypeInfo> TechnicianRecordType = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName();
        SVMXC__Service_Group_Members__c technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        technician.RecordTypeID = TechnicianRecordType.get('Technician').getRecordTypeId();
        technician.current_end_date__c = system.today().addDays(-2);
        insert technician;
    
        /*SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(false, newAcc);
        newLoc.SVMXC__Stocking_Location__c = true;
        newLoc.Technician__c = technician.Id;
        insert newLoc;*/
        
        /*Map<String,Schema.RecordTypeInfo> TechnicianRecordType = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName();
        string recId = TechnicianRecordType.get('Equipment').getRecordTypeId();
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c();
        technician  = [Select Id, User__c, current_end_date__c, RecordTypeID, Timecard_Profile__c, Default_plant__c, Equipment_Status__c, Calibration_Status__c,SVMXC__Inventory_Location__c,
                        (Select Id, SVMXC__Availability_End_Date__c FROM Expertise__r WHERE SVMXC__Availability_End_Date__c <: system.today())
                        from SVMXC__Service_Group_Members__c
                        Where RecordTypeID =: recId AND SVMXC__Inventory_Location__c != null AND OOC__c =: true  
                        Limit 1];
        */

        technician.current_end_date__c = system.today().addDays(200);
        technician.Equipment_Status__c = 'Available';
        technician.Calibration_Status__c = 'Out Of Calibration';
        technician.SVMXC__Active__c = true;
        update technician;
        
        test.startTest();
            //technician.current_end_date__c = system.today().addDays(+2);
            //technician.SVMXC__Inventory_Location__c = newLoc.Id;
            //update technician;
            //populatefields();
        Test.StopTest();
    }
}
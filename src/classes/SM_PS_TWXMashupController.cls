/**************************************************************************\
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
16-JAN-2018 - Rakesh - STSK0013696 - Connect thingworx on M2M Case 
/**************************************************************************/
public class SM_PS_TWXMashupController{
    public map<string,string> mapMashup{get;set;}
    public SM_PS_TWXMashupController(Apexpages.StandardController con){
        mapMashup = new map<string,string>();
        mapMashup.put('H19','VMS.SCPlus.Selected.TrueBeam');
        mapMashup.put('HAL','VMS.SCPlus.Selected.Halcyon');
        mapMashup.put('Default','VMS.SCPlus.Selected.CSeries');
    }
    public string getIFramURL(){
        
        string url;
        string caseId = ApexPages.currentPage().getParameters().get('id');
        
        Case cObj = [select Id,SVMXC__Top_Level__r.ERP_Pcodes__r.Name,SMAX_PS_TW_Serial_Number__c from Case where id=: caseId];      
        string mashup = mapMashup.get('Default');       
        if(mapMashup.containsKey(cObj.SVMXC__Top_Level__r.ERP_Pcodes__r.Name)){
            mashup = mapMashup.get(cObj.SVMXC__Top_Level__r.ERP_Pcodes__r.Name);
        }
        string pcsnnumber = cObj.SMAX_PS_TW_Serial_Number__c;
        
    
        string pid = [select Id,Name,profileId from User where Id=: userinfo.getUserId()].profileId;
        TWX_Integration__c cmsetting = TWX_Integration__c.getInstance(pid);
        if(cmsetting <> null){
            url = cmsetting.TW_Server_URL__c+'Mashups/'+mashup+'?appKey='+cmsetting.TW_App_Key__c+'&x-thingworx-session=true&serialNumber='+pcsnnumber;
        }
        return url;
    }
}
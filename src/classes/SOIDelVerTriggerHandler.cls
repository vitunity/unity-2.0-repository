public with sharing class SOIDelVerTriggerHandler {
  public static void preProductFromSAP(map<String, SOI_Delivery_Verification__c> mapProdCdSOI, 
                     map<String, SOI_Delivery_Verification__c> mapSoNameSoiNameSoiDel,
                     map<String, SOI_Delivery_Verification__c> mapSoNameHLvlSoiNameSoiDel, 
                     list<SOI_Delivery_Verification__c> listSoiDelVer) {

    if(mapProdCdSOI != null && mapProdCdSOI.size() > 0) {
      for(Product2 prod : [SELECT Id, ProductCode FROM Product2 WHERE ProductCode IN :mapProdCdSOI.keySet()]) {
        mapProdCdSOI.get(prod.ProductCode).Material__c = prod.Id;
      }
    }
    /*
    if(mapSONameSOI != null && mapSONameSOI.size() > 0) {
      for(Sales_Order__c so : [SELECT Id, Name FROM Sales_Order__c WHERE Name IN :mapSONameSOI.keySet()]) {
        mapSONameSOI.get(so.Name).Sales_Order__c = so.Id;
      }
    } */

    System.debug('#### debug mapSoNameSoiNameSoiDel.keySet() = ' + mapSoNameSoiNameSoiDel.keySet());
    System.debug('#### debug mapSoNameSoiNameSoiDel = ' + mapSoNameSoiNameSoiDel);
    

    List<String> listSoiList = new List<String>();
    List<String> listSoList = new List<String>();
    For(String st : mapSoNameSoiNameSoiDel.keySet()) {

      if(st.containsIgnoreCase('-')) {
        listSoList.add(st.substringBefore('-'));
        listSoiList.add(st.substringAfter('-'));
      }
    }

    System.debug('#### debug listSoList = ' + listSoList);
    System.debug('#### debug listSoiList = ' + listSoiList);

    System.debug('#### debug mapSoNameHLvlSoiNameSoiDel.keySet() = ' + mapSoNameHLvlSoiNameSoiDel.keySet());

    List<String> listSoiList2 = new List<String>();
    List<String> listSoList2 = new List<String>();
    For(String st2 : mapSoNameHLvlSoiNameSoiDel.keySet()) {

      if(st2.containsIgnoreCase('-')) {
        listSoList2.add(st2.substringBefore('-'));
        listSoiList2.add(st2.substringAfter('-'));
      }
    }

    System.debug('#### debug listSoList2 = ' + listSoList2);
    System.debug('#### debug listSoiList2 = ' + listSoiList2);

    for(Sales_Order_Item__c soi : [SELECT Id, Name, Sales_Order__c, Sales_Order__r.Name FROM Sales_Order_Item__c WHERE
            Name IN :listSoiList AND  Sales_Order__r.Name IN :listSoList]) {
        for(SOI_Delivery_Verification__c ver : listSoiDelVer) {
            if(ver.ERP_Sales_Order__c == soi.Sales_Order__r.Name
                && ver.ERP_Sales_Order_Item__c == soi.Name
              )
            {
                ver.Sales_Order__c = soi.Sales_Order__c;
                ver.Sales_Order_Item__c = soi.Id;
            }          
        }
    }

    for(Sales_Order_Item__c soi : [SELECT Id, Name, Sales_Order__c, Sales_Order__r.Name FROM Sales_Order_Item__c WHERE
            Name IN :listSoiList2 AND  Sales_Order__r.Name IN :listSoList2]) {
        for(SOI_Delivery_Verification__c ver : listSoiDelVer) {
            if(ver.ERP_Sales_Order__c == soi.Sales_Order__r.Name
                && ver.ERP_Sales_Order_Higher_Level_Item__c == soi.Name
              )
            {
                ver.Sales_Order__c = soi.Sales_Order__c;
                ver.Sales_Order_Higher_Level_Item__c = soi.Id;
            }           
        }
    }        
  }
}
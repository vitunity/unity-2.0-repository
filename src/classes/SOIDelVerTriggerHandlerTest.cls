@isTest (SeeAllData=true)
private class SOIDelVerTriggerHandlerTest {
    
    @isTest static void test_method_one() {
        
        list<Sales_Order_Item__c> listSoItems = [SELECT Id, Name, Sales_Order__c, Sales_Order__r.Name 
                                                FROM Sales_Order_Item__c where Sales_Order__r.Name <> NULL
                                                LIMIT 2];
        List<Product2> listProds = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode <> Null LIMIT 2];

        Test.startTest();
            if(listSoItems.size() > 0) {
                SOI_Delivery_Verification__c soiVer 
                    = new SOI_Delivery_Verification__c 
                        (ERP_Sales_Order__c = listSoItems[0].Sales_Order__r.Name,
                         ERP_Sales_Order_Item__c = listSoItems[0].Name,
                         ERP_Material__c = listProds[0].ProductCode,
                         ERP_Sales_Order_Higher_Level_Item__c = listSoItems[0].Name
                        );
                insert soiVer;

                soiVer.ERP_Material__c = listProds[1].ProductCode;
                update soiVer;
            }
        Test.stopTest();
    }
    

}
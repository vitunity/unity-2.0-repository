/***********************************************************************
* Author         : Varian
* Description	 : A handler class which compare fields changed for sobjects during trigger execution for field tracking
* User Story     : 
* Created On     : 5/2/16
************************************************************************/

public without sharing class SObjectHistoryProcessor implements Queueable {
	//Events to tap - after insert, after update, before delete, before undelete
    private static final String IS_INSERT = 'insert', IS_UPDATE = 'update', IS_DELETE = 'delete', IS_BEFORE = 'before', IS_AFTER = 'after', IS_UNDELETE = 'undelete', NEW_LIST = 'new',
    											NEW_MAP = 'newmap', OLD_MAP = 'oldmap', CREATED = 'Created';
    public static boolean TRUN_OFF_TRACKING = false;
    public Map<String, boolean> events;
    public Map<String, String> objData;
    public String objAPIName;
    public Datetime currentTimeStamp;
    public Id ownerId;
    
    public SObjectHistoryProcessor (Map<String, boolean> evts, Map<String, String> odata, String apiName, Datetime timeStamp, Id oId) {
    	this.events = evts;
    	this.objData = odata;
    	this.objAPIName = apiName;
    	this.currentTimeStamp = timeStamp;
    	this.ownerId = oId;	
    }
    
    public void execute(QueueableContext SC) {
    	System.debug(logginglevel.info, 'Executing Scheduled ======= '+SC);
    	SObjectHistoryProcessor.processHistory(this.events, this.objData, this.objAPIName, this.currentTimeStamp, this.ownerId);	
    }
    
    public static void trackHistory(String objAPIName) {
    	Datetime currentTime = System.Now();
    	Map<String, boolean> eventsMap = !Test.isRunningTest() ?  new Map<String, boolean> {SObjectHistoryProcessor.IS_INSERT => trigger.isInsert, SObjectHistoryProcessor.IS_UPDATE => trigger.isUpdate, 
    										SObjectHistoryProcessor.IS_DELETE => trigger.isDelete, SObjectHistoryProcessor.IS_BEFORE => trigger.isBefore, 
    										SObjectHistoryProcessor.IS_AFTER => trigger.isAfter, SObjectHistoryProcessor.IS_UNDELETE => trigger.isUndelete} : new Map<String, boolean>();
    	Map<String, String> dataMap = !Test.isRunningTest() ? new Map<String, String> { SObjectHistoryProcessor.NEW_MAP => JSON.serialize(trigger.newMap), 
    									SObjectHistoryProcessor.OLD_MAP => JSON.serialize(trigger.oldMap), SObjectHistoryProcessor.NEW_LIST => trigger.isUndelete || trigger.isDelete ? JSON.serialize(trigger.new) : ''} 
    									: new Map<String, String>();
    	if (!SObjectHistoryProcessor.TRUN_OFF_TRACKING) {
    		if (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
    			Id jobId = System.enqueueJob(new SObjectHistoryProcessor(eventsMap, dataMap, objAPIName, currentTime, Userinfo.getUserId()));
    			System.debug(logginglevel.info, 'Job Scheduled ======= '+jobId);	
    		}
    			
    		/*
    		if (!System.isFuture()) {
    			SObjectHistoryProcessor.processHistory(eventsMap, dataMap, objAPIName, currentTime, Userinfo.getUserId());	
    		} else {
    			System.enqueueJob(new SObjectHistoryProcessor(eventsMap, dataMap, objAPIName, currentTime, Userinfo.getUserId()));	
    		}*/	
    	}		
    }
    
    //@future
    public static void processHistory(Map<String, boolean> events, Map<String, String> objData, String objAPIName, Datetime currentTimeStamp, Id ownerId) {
    	//System.debug(logginglevel.info, '========== events =============== '+events);
    	//System.debug(logginglevel.info, '========== objData =============== '+objData);
    	if (events !=null && events.size() > 0) {
    		Map<String, sObject> newMap = new Map<String, sObject>(), oldMap = new Map<String, sObject>();
    		List<sObject> newList = new List<sObject>();
    		List<Field_Audit_Trail__c> aduitRecords = new List<Field_Audit_Trail__c>();
    		if (objData != null && objData.size() > 0) {
    			if (String.isNotBlank(objData.get(SObjectHistoryProcessor.NEW_MAP))) {
    				newMap = (Map<String, sObject>) JSON.deserialize(objData.get(SObjectHistoryProcessor.NEW_MAP), Map<String, sObject>.class);	
    			}
    			if (String.isNotBlank(objData.get(SObjectHistoryProcessor.OLD_MAP))) {
    				oldMap = (Map<String, sObject>) JSON.deserialize(objData.get(SObjectHistoryProcessor.OLD_MAP), Map<String, sObject>.class);
    			}
    			if (String.isNotBlank(objData.get(SObjectHistoryProcessor.NEW_LIST))) {
    				newList = (List<sObject>) JSON.deserialize(objData.get(SObjectHistoryProcessor.NEW_LIST), List<sObject>.class);
    			}
    			//get mapping
    			Map<String, FieldTrackingConfigCtrl.FieldWrapper> fieldsTracked = new Map<String, FieldTrackingConfigCtrl.FieldWrapper>();
    			FieldTrackingConfig__c config = null;
    			FieldTrackingConfigCtrl.Criteria qualityCriteria= null;
    			if (String.isNotBlank(objAPIName)) {
    				config = getConfigData(objAPIName);
    				fieldsTracked = getFieldsTracked(config);
    				//qualityCriteria = getCriteria(config); 	
    			}
    			if (events.get(SObjectHistoryProcessor.IS_INSERT) != null && events.get(SObjectHistoryProcessor.IS_INSERT) 
    				&& events.get(SObjectHistoryProcessor.IS_AFTER) != null && events.get(SObjectHistoryProcessor.IS_AFTER)) {
    				aduitRecords = compareObjects(newMap.values(), fieldsTracked, objAPIName, qualityCriteria, currentTimeStamp, true);
    			}
    			if (events.get(SObjectHistoryProcessor.IS_UPDATE) != null && events.get(SObjectHistoryProcessor.IS_UPDATE)) {
    				aduitRecords = compareObjects(oldMap, newMap, fieldsTracked, objAPIName, qualityCriteria, currentTimeStamp);	
    			}
    			if (events.get(SObjectHistoryProcessor.IS_DELETE) != null && events.get(SObjectHistoryProcessor.IS_DELETE)) {
    				aduitRecords = compareObjects(newList, fieldsTracked, objAPIName, qualityCriteria, currentTimeStamp, false);
    			}
    			if (events.get(SObjectHistoryProcessor.IS_UNDELETE) != null && events.get(SObjectHistoryProcessor.IS_UNDELETE)) {
    				aduitRecords = compareObjects(newList, fieldsTracked, objAPIName, qualityCriteria, currentTimeStamp, false);	
    			}		
    		}
    		//System.debug(logginglevel.info, '========== aduitRecords =============== '+aduitRecords);
    		insert aduitRecords;
    	}	
    }
   
   
   private static List<Field_Audit_Trail__c> compareObjects(Map<String, sObject> oldObjs, Map<String, sObject> newObjs, Map<String, FieldTrackingConfigCtrl.FieldWrapper> fieldsTracked,  
   		String objAPIName, FieldTrackingConfigCtrl.Criteria qCriteria, Datetime currentTimeStamp) {
   		List<Field_Audit_Trail__c> fieldAudits = new List<Field_Audit_Trail__c>();
   		for (FieldTrackingConfigCtrl.FieldWrapper field : fieldsTracked.values()) {
   			//System.debug(logginglevel.info, '========== field =============== '+field);
   			for (sObject newObj : newObjs.values()) {
   				//System.debug(logginglevel.info, '========== newObj =============== '+newObj);
   				if (oldObjs.get(newObj.Id) != null) { //newObj.get(field.apiName) != null && 
   					sObject oldObj = oldObjs.get(newObj.Id);
   					//System.debug(logginglevel.info, '========== oldObj =============== '+oldObj);
   					if (String.ValueOf(oldObj.get(field.apiName)) != String.valueOf(newObj.get(field.apiName))) {
   						//if (SObjectHistoryProcessor.checkCritiera(newObj, oldObj, qCriteria)) {
   							Field_Audit_Trail__c trail = SObjectHistoryProcessor.createAuditRecord(String.ValueOf(newObj.get('LastModifiedById')), currentTimeStamp, field.apiName, 
   															SObjectHistoryProcessor.getFieldValue(newObj, field), SObjectHistoryProcessor.getFieldValue(oldObj, field),  
															String.ValueOf(newObj.get('Id')), objAPIName);
							fieldAudits.add(trail);
							//System.debug(logginglevel.info, '========== trail =============== '+trail);	
   						/*} else {
   							System.debug(logginglevel.info, '========== no conditions met =============== '+qCriteria);
   						}*/
   						
   					}	
   				}	
   			}	
   		}
   		return fieldAudits;	
   }
   
   private static List<Field_Audit_Trail__c> compareObjects(List<sObject> newObjs, Map<String, FieldTrackingConfigCtrl.FieldWrapper> fieldsTracked,  String objAPIName, 
		FieldTrackingConfigCtrl.Criteria qCriteria, Datetime currentTimeStamp, boolean isInsert) {
   		List<Field_Audit_Trail__c> fieldAudits = new List<Field_Audit_Trail__c>();
   		//System.debug(logginglevel.info, '========== fieldsTracked =============== '+fieldsTracked.values());
   		//System.debug(logginglevel.info, '========== newObjs =============== '+newObjs);
   		if (isInsert) {
   			Field_Audit_Trail__c trail = SObjectHistoryProcessor.createAuditRecord(String.ValueOf(newObjs[0].get('LastModifiedById')), currentTimeStamp, CREATED, 
   															null, null, String.ValueOf(newObjs[0].get('Id')), objAPIName);
			fieldAudits.add(trail);	
   		}
   		for (FieldTrackingConfigCtrl.FieldWrapper field : fieldsTracked.values()) {
   			for (sObject newObj : newObjs) {
   				//System.debug(logginglevel.info, '========== field =============== '+field +' => '+newObj.get(field.apiName));
   				//System.debug(logginglevel.info, '========== newObj =============== '+newObj);
   				if (newObj.get(field.apiName) != null) {
   					//if (SObjectHistoryProcessor.checkCritiera(newObj, null, qCriteria)) {
   							Field_Audit_Trail__c trail = SObjectHistoryProcessor.createAuditRecord(String.ValueOf(newObj.get('LastModifiedById')), currentTimeStamp, field.apiName, 
   															SObjectHistoryProcessor.getFieldValue(newObj, field), null, String.ValueOf(newObj.get('Id')), objAPIName);
							fieldAudits.add(trail);	
   					//}	
   				}	
   			}	
   		}
   		return fieldAudits;	
   }
   
   private static Field_Audit_Trail__c createAuditRecord(Id createdById, Datetime cDate, String field, String newVal, String oldVal, String objId, String objApiname) {
   		Field_Audit_Trail__c trail = new Field_Audit_Trail__c(CreatedByID__c = createdById, CreatedDate__c = cDate, Field__c = field, NewValue__c =  newVal, 
   										OldValue__c = oldVal, sObjectID__c = objId, SObjectAPIName__c = objApiname);
   		return trail;
   }
   
   /*
   private static boolean checkCritiera(sObject newObj, sObject oldObj, FieldTrackingConfigCtrl.Criteria qCriteria) {
		boolean status = false;
		if (newObj != null && oldObj == null && qCriteria != null) {
			if (String.isBlank(qCriteria.fieldName) || (String.isNotBlank(qCriteria.fieldName) && newObj.get(qCriteria.fieldName) != null 
   							&& String.valueOf(newObj.get(qCriteria.fieldName)).equalsIgnoreCase(qCriteria.fieldValue) )) {
   				status = true;						
   			}	
		} else if (newObj != null && oldObj != null) {
			if (String.isBlank(qCriteria.fieldName) || ( String.isNotBlank(qCriteria.fieldName) 
   							&& newObj.get(qCriteria.fieldName) != null && String.valueOf(newObj.get(qCriteria.fieldName)).equalsIgnoreCase(qCriteria.fieldValue) // TODO need to be method later
   							|| oldObj.get(qCriteria.fieldName) != null && String.valueOf(oldObj.get(qCriteria.fieldName)).equalsIgnoreCase(qCriteria.fieldValue))) {
   				status = true;					
   			}
		}
		return status;
   }*/
   
   private static String getFieldValue(sObject newObj, FieldTrackingConfigCtrl.FieldWrapper filed) {
   		String value = '';
   		if (filed.fieldType.equalsIgnoreCase('datetime') && newObj.get(filed.apiName) != null) {
   			//value = String.valueOfGmt(Datetime.valueOfGmt(String.valueOf(newObj.get(filed.apiName))));
   			value = String.ValueOf((Datetime) newObj.get(filed.apiName));	
   		} else if (newObj.get(filed.apiName) != null) {
   			value = String.ValueOf(newObj.get(filed.apiName));
   		}
   		return value;	
   }
    
   private static FieldTrackingConfig__c getConfigData(String objName) {
    	Map<String, FieldTrackingConfigCtrl.FieldWrapper> fieldsTracked = new Map<String, FieldTrackingConfigCtrl.FieldWrapper>();
    	List<FieldTrackingConfig__c> configs = [Select Id, Name, JSONConfigData__c, SObjectAPIName__c, DisableDelete__c, Track_Fields__c, RecordCritiera__c From FieldTrackingConfig__c 
    												where SObjectAPIName__c =:objName AND Track_Fields__c=true];
    	return configs != null && configs.size() > 0 ? configs[0] : null;
   }
   
   /*
   private static FieldTrackingConfigCtrl.Criteria getCriteria(FieldTrackingConfig__c config) {
   		return config != null && String.isNotBlank(config.RecordCritiera__c) ? (FieldTrackingConfigCtrl.Criteria) JSON.deserialize(config.RecordCritiera__c, FieldTrackingConfigCtrl.Criteria.class) : null; 
   }*/
   	
   private static Map<String, FieldTrackingConfigCtrl.FieldWrapper> getFieldsTracked(FieldTrackingConfig__c config) {
   		Map<String, FieldTrackingConfigCtrl.FieldWrapper> fieldsTracked = new Map<String, FieldTrackingConfigCtrl.FieldWrapper>(); 
    	if (config != null && String.isNotBlank(config.JSONConfigData__c)) {
    		for (FieldTrackingConfigCtrl.FieldWrapper fw : (List<FieldTrackingConfigCtrl.FieldWrapper>) JSON.deserialize(config.JSONConfigData__c, List<FieldTrackingConfigCtrl.FieldWrapper>.class)) {
	    		fieldsTracked.put(fw.apiName, fw);
	    	}	
    	}
    	return fieldsTracked;  		
   } 
    
    
}
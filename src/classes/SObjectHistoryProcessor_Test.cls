/***********************************************************************
* Author         : Varian
* Description	 : Test class for SObjectHistoryProcessor handler class
* User Story     : 
* Created On     : 5/27/16
************************************************************************/

@isTest
private class SObjectHistoryProcessor_Test {
    
    @testSetup static void setup() {
		System.runAs(TestDataFactory.createUser('System Administrator')) {
			TestDataFactory.createAccounts();
			TestDataFactory.createFieldTrackingConfig();	
		}			
	}
	
    @isTest static void invokeSObjectProcessor() {
    	System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
    		Test.startTest();
    		SObjectHistoryProcessor.trackHistory('Account');
    		Test.stopTest();
    	}	
    }
    
     @isTest static void tesSObjectProcessorFuture_AfterInsert() {
     	System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
     		Map<Id, Account> accounts = new Map<Id, Account>([Select Id, Name, Description, LastModifiedDate, CreatedDate, CreatedById, LastModifiedById From Account Limit 200] );
     		Map<String, boolean> eventsMap = new Map<String, boolean> { 'insert' => true, 'after' => true};
     		Map<String, String> dataMap = new Map<String, String> { 'newmap' =>  JSON.serialize(accounts)};
     		Test.startTest();
    		SObjectHistoryProcessor.processHistory(eventsMap, dataMap, 'Account', System.now(), Userinfo.getUserId());
    		Test.stopTest();
    		List<Field_Audit_Trail__c> audits = [Select Id From Field_Audit_Trail__c where sObjectID__c IN : accounts.keySet()];
    		System.assert(audits.size() > 0);
     	}	
     }
     
     @isTest static void tesSObjectProcessorFuture_AfterUpdate() {
     	System.runAs(TestDataFactory.getUser('bruce.wayne@wayneenterprises.com')) {
     		Map<Id, Account> oldAccounts = new Map<Id, Account>([Select Id, Name, Description, LastModifiedDate, CreatedDate, CreatedById, LastModifiedById From Account Limit 200] );
     		List<Account> tempAccs = new List<Account>();
     		for (Account a : oldAccounts.values()) {
     			tempAccs.add(new Account(Description = 'new large text test', Id = a.Id));		
     		}
     		update tempAccs;
     		Map<Id, Account> newAccounts = new Map<Id, Account>([Select Id, Name, Description, LastModifiedDate, CreatedDate, CreatedById, LastModifiedById From Account where Id IN:tempAccs Limit 200] );
     		Map<String, boolean> eventsMap = new Map<String, boolean> { 'update' => true, 'after' => true};
     		Map<String, String> dataMap = new Map<String, String> { 'newmap' =>  JSON.serialize(newAccounts), 'oldmap' =>  JSON.serialize(oldAccounts)};
     		Test.startTest();
    		SObjectHistoryProcessor.processHistory(eventsMap, dataMap, 'Account', System.now(), Userinfo.getUserId());
    		Test.stopTest();
    		List<Field_Audit_Trail__c> audits = [Select Id From Field_Audit_Trail__c where sObjectID__c IN : oldAccounts.keySet()];
    		System.assert(audits.size() > 0);		
     	}	
     }
}
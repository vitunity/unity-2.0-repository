/*************************************************************************\
      @ Author          : Chiranjeeb Dhar.
      @ Date            : 04-Sep-2013.
      @ Description     : This Apex Class is the Custom Contact Lookup page on case.
      @ Last Modified By      :  Dushyant Singh 
      @ Last Modified On      :  26-02-2014   
      @ Last Modified Reason  :  Modification done to create contact under US3828   
      @ Last Modified By      :  Nikita Gupta 
      @ Last Modified On      :  16th may, 2014   
      @ Last Modified Reason  :  added duplicate email check 
      @ Last Modified By      :  Nikita Gupta 
      @ Last Modified On      :  21st May, 2014   
      @ Last Modified Reason  :  updated contact search to fix DE735
****************************************************************************/
public class SRCaseOverrideonContact 
{   
    public String emailId { get; set; }  // added by Nikita, 5/12/2014
    
    public String AccountId;
    public string formid{get;set;}
    public List<Contact> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    Public List<Contact> ListofContact{get;set;}
    public boolean VarContact{get;set;}
        
    public SRCaseOverrideonContact(ApexPages.StandardController controller) 
    {
        VarContact = false;
        formid=ApexPages.currentPage().getParameters().get('formid');
        system.debug('&&&&&'+formid);
        ListofContact= new list<Contact>();
        //ListofContact=[Select Id,Name,AccountId from Contact where AccountId=:AccountId and AccountId!=Null];
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch(); 
    }
    
    public void search() 
    {
        runSearch();
        //return null;
    }
      
    public void NewContact() 
    {
        VarContact=true;
        // return null;
    }  
     
      // prepare the query and issue the search command
    private void runSearch() 
    {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);               
    } 
     
      // run the search and return the records found. 
    private List<Contact> performSearch(string searchString) 
    {
        AccountId=ApexPages.currentPage().getParameters().get('prcTempSel'); 
        String recid = Schema.SObjectType.Account.RecordTypeInfosByName.get('Site Partner').RecordTypeId;
        String soql = 'select id, name,Account.Id,Phone,MobilePhone,OtherPhone,Primary_Contact_Number__c from Contact where id != null and AccountId != null and Account.RecordtypeId =: recid';
        if(AccountId != '' && AccountId != '' && AccountId != 'null' && AccountId.startsWith('001'))
            soql = soql + ' and AccountId=:AccountId and AccountId!=Null';
        if(searchString != '' && searchString != null)
            soql = soql + ' and name LIKE \'%' + searchString+'%\'';  //Nikita, DE735, 5/21/2014, Added '%' before the searchstring also.
            //soql += ' and Product2.Product_Hierarchy__c LIKE \''+hierarchy+'%\'';
            // soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
        soql = soql + ' limit 25';
        System.debug('*******'+soql);
        return database.query(soql); 
    }
    
    public string getFormTag() 
    {
        return System.currentPageReference().getParameters().get('frm');
    }
     
      // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() 
    {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    public Contact contact 
    {
        get 
        {
            if (contact == null)
            contact = new Contact();
            contact.accountid=AccountId;
            contact.email = emailId;
            return contact;
        }
        set;
    }
    
    public PageReference Contactsave() 
    {
        try 
        {
            //  contact.accountid=AccountId;
            // added by Nikita, 5/12/2014 to check if the email address already exists.
                ListofContact = [SELECT Email,Id FROM Contact WHERE Email =: emailId AND Email != null]; // added by Nikita, 5/12/2014
                                
                if(ListofContact.size() > 0)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Email address already exists. Kindly, create a new contact with a unique e-mail address.'));
                }
              
                else
                {
                    insert contact; // inserts the new record into the database
                }
                    
            VarContact = false;
            runSearch();
        } 
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating new contact.'));
            return null;
        }
        return null;
    }  
      

    public void Contactcancel() 
    {
        //runSearch();
        VarContact = false;
    }    
      
  
}
@isTest
public class SRCaseOverrideonContact_test {

    public static testMethod void testSRCaseOverrideonContact() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
        Account testAcc = AccountTestData.createAccount();
        insert testAcc;
        Contact testcontact = SR_testdata.createContact();
        testcontact.AccountId = testAcc.id;
        insert testcontact;
        System.currentPageReference().getParameters().put('lksrch',testcontact.Name);
        ApexPages.currentPage().getParameters().put('prcTempSel',testAcc.id);
        ApexPages.StandardController testRecord = new ApexPages.StandardController(testAcc);
        SRCaseOverrideonContact controller = new SRCaseOverrideonContact(testRecord);
        controller.search();
        controller.getFormTag();
        controller.getTextBox(); 
        controller.Contactsave();   
        controller.Contactcancel();    
        }
      }
}
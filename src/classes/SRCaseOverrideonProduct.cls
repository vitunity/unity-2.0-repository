/*************************************************************************\
      @ Author          : Chiranjeeb Dhar.
      @ Date            : 06-Sep-2013.
      @ Description     : This Apex Class is the Custom Product Lookup page on case.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/
public class SRCaseOverrideonProduct 
{
public String AccountId;
public string formid{get;set;}
public List<SVMXC__Installed_Product__c> results{get;set;} // search results
public string searchString{get;set;} // search keyword
//Public List<SVMXC__Installed_Product__c> ListofContact{get;set;}

set<string> stTempLocationSet=new set<string>();
public string strNodeId{get;set;}
public string strAccountId{get;set;}
Public string strAccountLocationId{get;set;}
set<string> stChildData=new set<string>();
private static JSONGenerator gen {get; set;}
public List<String> LstOfProductNodes{get;set;} 
set<string> stLocationId=new set<string>();
list<SVMXC__Installed_Product__c> lstInstalledProduct;
public String ProductId{get;set;}  
set<string> stIP=new set<string>();
Map<id,List<SVMXC__Installed_Product__c>>MapProductAndLstChildren=new Map<Id,List<SVMXC__Installed_Product__c>>();
Map<Id,SVMXC__Installed_Product__c> MapOfProducts;
List<SVMXC__Installed_Product__c> LstOfProducts;
Map<string,string> MapIdOfLocation=new Map<string,string>();




    public SRCaseOverrideonProduct(ApexPages.StandardController controller) 
    { LstOfProductNodes=new List<String>();
      formid=ApexPages.currentPage().getParameters().get('formid');
      system.debug('&&&&&'+formid);
      //ListofContact= new list<Contact>();
      //ListofContact=[Select Id,Name,AccountId from Contact where AccountId=:AccountId and AccountId!=Null];
      searchString = System.currentPageReference().getParameters().get('lksrch');
      runSearch();    
    }
  public PageReference search() {
    runSearch();
    //return null;
    Pagereference page = new PageReference('/apex/SR_Override_ProductonCase?txt='+System.currentPageReference().getParameters().get('txt')+'&prcTempSel='+System.currentPageReference().getParameters().get('prcTempSel')+'&frm='+System.currentPageReference().getParameters().get('frm')+'&lksearch='+System.currentPageReference().getParameters().get('lksearch')+'&lksrch='+ searchString);
    page.setredirect(true);
    return page;
  }
 
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    //results = performSearch(searchString); 
performSearch(searchString);    
  } 
 
  // run the search and return the records found. 
  private Void performSearch(string searchString) {
    AccountId = ApexPages.currentPage().getParameters().get('prcTempSel'); 
   // String soql = 'select id, name,SVMXC__Company__c,SVMXC__Product__c,SVMXC__Serial_Lot_Number__c,SVMXC__Date_Installed__c from SVMXC__Installed_Product__c where SVMXC__Company__c=:AccountId and SVMXC__Company__c!=Null';
   
     String soql = 'select id, name,SVMXC__Company__c,SVMXC__Product__c,SVMXC__Serial_Lot_Number__c, SVMXC__Parent__r.Name,SVMXC__Date_Installed__c from SVMXC__Installed_Product__c where id != null';
     if(AccountId != null && AccountId != '' && AccountId != 'null' && AccountId.startsWith('001'))
      soql = soql + ' and SVMXC__Company__c=:AccountId and SVMXC__Company__c != Null';
    
    if(searchString != '' && searchString != null)
       soql = soql + ' and name LIKE \'' + searchString+'%\'';
       //soql += ' and Product2.Product_Hierarchy__c LIKE \''+hierarchy+'%\'';
       // soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
    soql = soql + ' limit 50';
    System.debug('*******'+soql);
    //return database.query(soql); 
 list<SVMXC__Installed_Product__c> lstInstalledProduct=new list<SVMXC__Installed_Product__c>();
 lstInstalledProduct = database.query(soql);
 System.debug('lstInstalledProduct>>'+lstInstalledProduct);
       
       for(SVMXC__Installed_Product__c varIp:lstInstalledProduct)
        {
       
         stIP.add(varIp.id);
        }
    MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,SVMXC__Site__r.name,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Product__r.name,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Company__c=:AccountId]);
    for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) 
    {
        
        
           if(MapProductAndLstChildren.get(pd.SVMXC__Parent__c)==null)
            {
                LstOfProducts=new List<SVMXC__Installed_Product__c>();
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }else
            {               
                LstOfProducts=MapProductAndLstChildren.get(pd.SVMXC__Parent__c);
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }
    }
     Map<string,list<SVMXC__Installed_Product__c>> mapChld=new Map<string,list<SVMXC__Installed_Product__c>>();
        for(string varStr:stIP)
        {
         LstOfProducts=new List<SVMXC__Installed_Product__c>();
            for(SVMXC__Installed_Product__c pd:MapOfProducts.values())
            {
            if(varStr == pd.SVMXC__Parent__c)
            {
             LstOfProducts.add(pd);
        
            }
            
          }
        mapChld.put(varStr,LstOfProducts)   ;
        }
        for(string varst:stIP)
        {
            for(SVMXC__Installed_Product__c varStr:mapChld.get(varst))
            {
              stChildData.add(varStr.id);
            }
        }
        System.debug('------MapProductAndLstChildren------------------>'+stChildData);
    GenerateTreeStructureForProducts();
    }
  
   

public void GenerateTreeStructureForProducts(){
        
        // Generating Tree Strucutre for all Parent Products.             
      for(SVMXC__Installed_Product__c pd:[select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__r.name   ,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Date_Installed__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Parent__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR Id IN:stIP) AND SVMXC__Company__c=:AccountId]){
           
            // Initializing JSON Generator Object.
            gen = JSON.createGenerator(true);
            
            /* Calls CreateTreeStructure method to retrieve Instance of Wrapper Class 
                   which contains Parent and Child Nodes for each Product. */
                     // Calling ConvertNodeToJSON Method. 
          if( !stChildData.contains(pd.id))
         {
           WrpProductNode node = CreateTreeStructure(pd);
            

           ConvertNodeToJSON(node);
           LstOfProductNodes.add(gen.getAsString());
         
            // Creating List of All Product Nodes by adding instance of JSONGenerator as String.
          }
           
        } system.debug('------LstOfProductNodes-->'+LstOfProductNodes);
    } /* Action method ends here.*/ 
  public  WrpProductNode CreateTreeStructure(SVMXC__Installed_Product__c prd){            
        WrpProductNode wrpObj=new WrpProductNode();
        wrpObj.prod=prd;
        List<WrpProductNode>lstOfChild;
        if(MapProductAndLstChildren.get(prd.id)!=null)
        {
            wrpObj.hasChildren =true;
            lstOfChild=new List<WrpProductNode >();
            for(SVMXC__Installed_Product__c pd:MapProductAndLstChildren.get(prd.id)){
              WrpProductNode temp=CreateTreeStructure(pd);
                lstOfChild.add(temp);
            }           
        }else
        {
             wrpObj.hasChildren =false;   
        }   
            
        wrpObj.prodChildNode=lstOfChild;
        return wrpObj;        
    } /*CreateTreeStrucure Method Ends here.*/
    
    /*ConvertNodeToJSON method starts here : Below method converts 
    instance of each Product Node(Wrapper class) into JSON.*/
    public void ConvertNodeToJSON(WrpProductNode prodNode){ 
        
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce. 
System.debug('--Id'+prodNode.prod.id);      
    
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod.Name+'@'+(prodNode.Prod.SVMXC__Product__r.name==null?'':prodNode.Prod.SVMXC__Product__r.name)+'@'+prodNode.Prod.id);
        gen.writeStringField('key', prodNode.Prod.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
           if(prodNode.hasChildren ){    
            
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
       gen.writeEndObject();    
       
    }/*ConvertNodeToJSON method ends here.*/
     public void ConvertNodeToJSONChild(WrpProductNode prodNode){ 
        
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce.      
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod.Name+'@'+(prodNode.Prod.SVMXC__Product__r.name==null?'':prodNode.Prod.SVMXC__Product__r.name)+'@'+prodNode.Prod.id);
        gen.writeStringField('key', prodNode.Prod.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
           if(prodNode.hasChildren ){    
            
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
       gen.writeEndObject();    
      
    }
    /*Wrapper class starts here : Below class wraps Products,List of Child Products 
    and boolean variable for childs present for each Parent Product.*/
    public class WrpProductNode{        
        public List<WrpProductNode>prodChildNode{get;set;}
        public Boolean hasChildren {get; set;} 
            
        public SVMXC__Installed_Product__c prod{get;set;}        
    } /*Wrapper Class ends here*/ 
public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }  
}
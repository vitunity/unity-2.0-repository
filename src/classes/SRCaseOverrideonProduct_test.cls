@isTest
public class SRCaseOverrideonProduct_test {

    public static testMethod void testSRCaseOverrideonProduct() {
        //Dummy data crreation 
        //Profile sysadminprofile = new profile();
     //   sysadminprofile = [Select id from profile where name = 'System Administrator'];
        //User systemuser = new user(alias = 'standt',email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = sysadminprofile.Id, timezonesidkey='America/Los_Angeles',username='standard____user@testorg.com');
       // insert systemuser;
       // system.runas(systemuser){
        Account testAcc = AccountTestData.createAccount();
        insert testAcc;
        Contact testcontact = SR_testdata.createContact();
        testcontact.AccountId = testAcc.id;
        insert testcontact;
        // installed product
        SVMXC__Installed_Product__c testInstallPrd = SR_testdata.createInstalProduct();
        testInstallPrd.SVMXC__Company__c = testAcc.id;
        insert testInstallPrd;
        SVMXC__Installed_Product__c testInstallPrd1 = SR_testdata.createInstalProduct();
        testInstallPrd1.SVMXC__Company__c = testAcc.id;
        testInstallPrd1.SVMXC__Parent__c = testInstallPrd.id;
        insert testInstallPrd1;
        
        System.currentPageReference().getParameters().put('lksrch',testInstallPrd.Name);
        ApexPages.currentPage().getParameters().put('prcTempSel',testAcc.id);
        ApexPages.StandardController testRecord = new ApexPages.StandardController(testAcc);
        SRCaseOverrideonProduct controller = new SRCaseOverrideonProduct(testRecord);
        controller.search();
        controller.getFormTag();
        controller.getTextBox();        
       // }
      }
}
Public class SRCheckContractOnInstalledProduct
{
public ApexPages.StandardController controller; 
public string  CaseId;
public String strMessage{get;set;}

Public  SRCheckContractOnInstalledProduct( ApexPages.StandardController controller)
{ 
    strMessage='';
    Set <Id> setipids = new Set <id>();
    Set <Id> setcaseids = new Set <id>();
    this.controller = controller;
    CaseId=controller.getId(); 
    list <Case> lstprod = [select id,ProductSystem__c,CreatedDate,LastModifiedDate from case where id =: CaseId];
    List<SVMXC__Installed_Product__c> lstInstallesProduct=new List<SVMXC__Installed_Product__c>();
    if(lstprod.size() > 0)
    {
        for(Case ip : lstprod)
        {
            setipids.add(ip.ProductSystem__c);
        }
    }
    if (setipids.size() > 0)
    { 
        lstInstallesProduct=[select id,(select id, name from SVMXC__Warranty__r where SVMXC__End_Date__c >: system.today()) ,(select id ,SVMXC__End_Date__c from R00N70000001hzdkEAA where SVMXC__End_Date__c >: system.today()), SVMXC__Service_Contract__c,Name from SVMXC__Installed_Product__c where  ID in: setipids];     
    }
    integer lastModSec = lstprod[0].LastModifiedDate.second();
    integer creaModSec = lstprod[0].CreatedDate.second();
    integer creaModMin = lstprod[0].CreatedDate.minute();
    integer creaModHr = lstprod[0].CreatedDate.hour();
    integer nowSec = system.now().second();
    integer nowMin = system.now().minute();
    integer nowHr = system.now().hour();
    
    system.debug('lastModSec @@G'+ lastModSec);
    system.debug('creaModSec @@G'+ creaModSec);
    system.debug('nowSec @@G'+ nowSec);
    system.debug('creaModMin @@G'+ creaModMin);
    system.debug('creaModHr @@G'+ creaModHr);
    system.debug('nowMin @@G'+ nowMin);
    system.debug('nowHr @@G'+ nowHr);
    system.debug('nowHr @@G'+ nowHr);
    
    
    if(lstInstallesProduct.size() > 0 && lstInstallesProduct[0].SVMXC__Warranty__r.size()==0 &&  lstInstallesProduct[0].SVMXC__Service_Contract__c==null && lstInstallesProduct[0].R00N70000001hzdkEAA.size()==0 && lstprod[0].CreatedDate.date() == lstprod[0].LastModifiedDate.date() && lstprod[0].CreatedDate.hour() == lstprod[0].LastModifiedDate.hour() && lstprod[0].CreatedDate.minute() == lstprod[0].LastModifiedDate.minute() && (nowMin - creaModMin == 0) && ( nowHr - creaModHr == 0) && (nowSec - creaModSec <= 15)) 
    {
    system.debug('inside if');
    strMessage='No Warranty/Contract';
    }
    else if(String.ISblank(strMessage))
    {
    strMessage='';
    }
}
}
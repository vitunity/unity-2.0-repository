@isTest
public class SRCheckContractOnInstalledProduct_test {

    public static testMethod void testSRCheckContractOnInstalledProduct() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
        
            Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert objACC;
            Account a;      
            a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
            insert a;
            
            Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '1214445');
            insert con;
            
            Contact_Role_Association__c cro = new Contact_Role_Association__c(contact__c = con.Id, Account__c = a.Id);
            insert cro;
            
            Product2 prod = new Product2(Name = 'Test Product', Product_Group__c = 'Acuity;Argus');
            insert prod;

            Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
       
            SVMXC__Installed_Product__c testInstalledProduct = new SVMXC__Installed_Product__c(name = 'H156778');
            insert testInstalledProduct;
            
            Case case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType,status = 'new',Subject= 'case Subject', ProductSystem__c = testInstalledProduct.id);
            insert case1;
   
            SRCheckContractOnInstalledProduct controller = new SRCheckContractOnInstalledProduct(new ApexPages.StandardController(case1));
       
        }
      }
}
/*************************************************************************\
Author: Nikita Gupta
Created Date: 12-Dec-2013
Project/Story/Inc/Task : N/A
Description: 
This class is used to add before and after triggers logic written on ISC History object

    @ Author        : Nikita Gupta
    @ Date      : 12-Dec-2013
    @ Description   : This class is used to add before and after triggers written on ISC History object
    @ Last Modified By  :   Priti Jaiswal
    @ Last Modified On  :   09-may-2014
    @ Last Modified Reason  :  US1694 - to update Current ISC config on Insatalled product
    @ Last Modified By  :   Nikita Gupta
    @ Last Modified On  :   6/12/2014
    @ Last Modified Reason  :  US3542, edited by Nikita on 6/12/2014, replaced version with Formatted version field
    @ Last Modified By  :   Nikita Gupta
    @ Last Modified On  :   6/23/2014
    @ Last Modified Reason  :  US4528, to create a new product version and product version build, assign it to Installed Product in case an existing one is not found.
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Sep-25-2017 Chandramouli STSK0012935 : Modified logic to avoid duplication of product version records. 
****************************************************************************/

public class SRClassISCHistory 
{
    public static Map<Id, Id> mapInstalledProductId_ISCid = new Map<Id, Id>();
    public static Map<Id, Id> mapISCid_InstalledProductId = new Map<Id, Id>();
    public static Map<String, ISC_API_Table__c> iscApiTableMap = new Map<String, ISC_API_Table__c>();
    
    
    /* Populate ISC API Table map based on PSE APi table STRY0014508*/
    public void buildISCApiTableMap(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory){
        set<String> iscSet = new set<String>();
        for(ISC_History__c varISC : LstOfISCHistory) {
            if(varISC.API_Table_Name__c != null){
                iscSet.add(varISC.API_Table_Name__c);
            }
         }
         if(!iscSet.isEmpty()){
             for(ISC_API_Table__c apiTable : [select Name, PCSN_Field_Name__c,Primary_Version_Parameter_Name__c,Update_Installed_Product_Version__c
                                              from  ISC_API_Table__c 
                                              where Name in : iscSet]){
                iscApiTableMap.put(apiTable.Name,apiTable);
            }
         }         
    } 
    
    /* Set ISC API Table lookup STRY0014508*/
    public void setISCAPITableLookup(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory){
        system.debug('setISCAPITableLookup  == iscApiTableMap>>>>>>> ' + iscApiTableMap);
        for(ISC_History__c varISC : LstOfISCHistory) {
            if( iscApiTableMap.containsKey(varISC.API_Table_Name__c) &&
                varISC.API_Table_Name__c != null && 
                (mapOldISCHistory == null 
                 || mapOldISCHistory.get(varISC.Id).API_Table_Name__c != varISC.API_Table_Name__c || varISC.ISC_API_Table__c == null))
            {
                varISC.ISC_API_Table__c = iscApiTableMap.get(varISC.API_Table_Name__c).Id;
            }
         }
    }
    
    
    /*The below method is to populate the Product and IP lookup fields on ISC History based on PCSN.(US3657)*/
    public void ISCHistoryUpdateIP_Product(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory)
    {
        Set<String> setPCSN = new Set<String>(); //list to save the value of PCSN extracted from ISC
        Map<string, SVMXC__Installed_Product__c> mapInstProdId_IP = new Map<string, SVMXC__Installed_Product__c>();
        Map<Id, Id> mapInstProdId_ISCid = new Map<Id, Id>();
        //map<String,ID> mapIPName_Product = new map<String,ID>();//map for PCSN and Product ID field.
        //map<String,ID> mapIPName_ID = new map<String,ID>();//map for PCSN and IP ID Name field.
        
        for(ISC_History__c varISC : LstOfISCHistory)
        {
            system.debug('ISC.PCSN__c>>>>>>> ' + varISC.PCSN__c);
            if(varISC.PCSN__c != null && (mapOldISCHistory == null || mapOldISCHistory.get(varISC.ID).PCSN__c == null || mapOldISCHistory.get(varISC.Id).PCSN__c != varISC.PCSN__c || varISC.Installed_Product__c == null || varISC.Product__c == null))//CM : STSK0011219 : added new logic
            {
                setPCSN.add(varISC.PCSN__c); //extracting PCSN from ISC History
            }
            //us1694, priti, 09/05/2014 - to update Current ISC config on Insatalled product
            if(varISC.Installed_Product__c != null && (mapOldISCHistory == null || mapOldISCHistory.get(varISC.Id).Installed_Product__c == null || mapOldISCHistory.get(varISC.Id).Installed_Product__c != varISC.Installed_Product__c))
            {
                mapInstProdId_ISCid.put(varISC.Installed_Product__c, varISC.Id); 
            }
        }

        if(setPCSN.size() > 0 || mapInstProdId_ISCid.size() > 0)//if PCSN of ISC History is not blank
        {
            List<SVMXC__Installed_Product__c > listInstProd = [select ID, Name, SVMXC__Product__c, Current_ISC_Configuration__c from
                                                                SVMXC__Installed_Product__c where Name IN :setPCSN OR Id in : mapInstProdId_ISCid.keySet()];
            if(listInstProd.size() > 0)
            {
                for(SVMXC__Installed_Product__c varIP : listInstProd)
                {
                    if(setPCSN.size() > 0 && setPCSN.contains(varIP.Name))
                    {
                        mapInstProdId_IP.put(varIP.Name, varIP);
                    }
                    if(mapInstProdId_ISCid.containsKey(varIP.Id))
                    {
                        mapInstalledProductId_ISCid.put(varIP.Id, mapInstProdId_ISCid.get(varIP.Id));
                        mapISCid_InstalledProductId.put(mapInstProdId_ISCid.get(varIP.Id), varIP.Id);
                    }
                }
            }
        }

        for(ISC_History__c varISC : LstOfISCHistory)
        {
            if(varISC.PCSN__c != null && mapInstProdId_IP.ContainsKey(varISC.PCSN__c)) //ContainsKey check added by Nikita, 6/23/2014
            {
                varISC.Installed_Product__c = mapInstProdId_IP.get(varISC.PCSN__c).Id;

                if(varISC.Product__c == null)
                {
                    varISC.Product__c = mapInstProdId_IP.get(varISC.PCSN__c).SVMXC__Product__c;
                }
            }
            if(varISC.Version__c != null && varISC.Version__c.contains('.') && varISC.Version__c.indexof('.') != varISC.Version__c.lastIndexOf('.') && (mapOldISCHistory == null || mapOldISCHistory.get(varISC.Id).Version__c == null || mapOldISCHistory.get(varISC.Id).Version__c != varISC.Version__c)) //code Optimization, moved from before trigger,  Nikita, 7/7/2014
            {
                string TotalVersion = varISC.Version__c;
                system.debug('TotalVersion---'+TotalVersion);
                String totalComaVersion = TotalVersion.REPLACE('.',',');
                system.debug('totalComaVersion---'+totalComaVersion);
                List<String> VersionList = new List<String>();
                VersionList.ADDALL(totalComaVersion.SPLIT(','));
                System.debug('VersionList---'+VersionList);
                for (Integer i=0; i< VersionList.size(); i++)
                {
                    if(i == 0)
                    {
                        system.debug('VersionList[0]---'+VersionList[0]);
                        string major = VersionList[0];
                        if(major != null) //code optimization by Nikita, 7/7/2014
                        {
                            varISC.Major_Version__c = Integer.valueof(major);
                        }
                    }
                    if(i == 1)
                    {
                        system.debug('VersionList[1]---'+VersionList[1]);
                        string minor = VersionList[1];
                        if(minor != null) //code optimization by Nikita, 7/7/2014
                        {
                            varISC.Minor_Version__c = Integer.valueof(minor);
                        }
                    }
                    if(i == 2)
                    {    
                        system.debug('VersionList[2]---'+VersionList[2]);   
                        string build = VersionList[2];
                        if(build != null ) //code optimization by Nikita, 7/7/2014
                        {
                            if(build.isNumeric())
                            {
                                varISC.Build_Version__c = Integer.valueof(build);
                            }
                            else
                            {
                                varISC.Build_Version__c = 0;
                            }
                        }
                    }
                }
            }
        }
    }
    
    /*The below method is to update Installed product with Product Verision Build (US3676) and STSK0011218*/
    public void ISCHistoryUpdateIPversion(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory){
        List<ISC_History__c> LstOfISCHistoryQualified = new List<ISC_History__c>();
        Map<Id, ISC_History__c> mapOldISCHistoryQualified;
        system.debug('ISCHistoryUpdateIPversion==iscApiTableMap================'+iscApiTableMap.size());
        for(ISC_History__c iSCHistoryObj : LstOfISCHistory){
            if(iscApiTableMap.containsKey(iSCHistoryObj.API_Table_Name__c)
               && iscApiTableMap.get(iSCHistoryObj.API_Table_Name__c).Update_Installed_Product_Version__c){
                LstOfISCHistoryQualified.add(iSCHistoryObj);
                if(mapOldISCHistory!= null){
                    if(mapOldISCHistoryQualified == null){
                        mapOldISCHistoryQualified = new Map<Id, ISC_History__c>();
                    }
                    mapOldISCHistoryQualified.put(iSCHistoryObj.id,mapOldISCHistory.get(iSCHistoryObj.id));
                }
            }
        }
        if(!LstOfISCHistoryQualified.isEmpty()){
            ISCHistoryUpdateIPversionHelper(LstOfISCHistoryQualified,mapOldISCHistoryQualified);
        }        
    }
    
    private boolean isCheckISCUpdateIp(ISC_History__c iscObj, Map<Id, ISC_History__c> mapOldISCHistory){
        ISC_History__c oldISC;
        if(mapOldISCHistory != null){
            oldISC = mapOldISCHistory.get(iscObj.Id);
        }
        if(iscObj.Installed_Product__c != null 
           && iscObj.Product_Version_Build__c != null 
           && iscObj.Product__c != null
           && (mapOldISCHistory == null 
           || (oldISC != null && 
           (oldISC.Installed_Product__c == null 
           || oldISC.Product__c == null 
           || oldISC.ISC_API_Table__c == null
           || oldISC.Product_Version_Build__c == null
           || oldISC.Rev_Date__c == null
           || oldISC.Rev_Date__c != iscObj.Rev_Date__c 
           || oldISC.Installed_Product__c != iscObj.Installed_Product__c 
           || oldISC.Product_Version_Build__c != iscObj.Product_Version_Build__c 
           || oldISC.Product__c != iscObj.Product__c
           || oldISC.ISC_API_Table__c != iscObj.ISC_API_Table__c
           )))){
               return true;
           }else{
               return false;
           }
    }
    
    
    /*The below helper method is to update Installed product with Product Verision Build (US3676)*/
    private void ISCHistoryUpdateIPversionHelper(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory){
        set<Id> installedProductId = new set<Id>();
        list<SVMXC__Installed_Product__c> ipList = new list<SVMXC__Installed_Product__c>();
        for(ISC_History__c iscObj : LstOfISCHistory){
            if(isCheckISCUpdateIp(iscObj,mapOldISCHistory)){
                installedProductId.add(iscObj.Installed_Product__c);
            }   
        }
        if(!installedProductId.isEmpty()){
            for(SVMXC__Installed_Product__c ipObj: [Select (Select Id, Product_Version_Build__c 
                                                    From ISC__r where Update_Installed_Product_Version__c = true 
                                                    order BY Rev_Date__c Desc) 
                                                    From SVMXC__Installed_Product__c  
                                                    where Id in : installedProductId]){
                if(!ipObj.ISC__r.isEmpty()){
                    ipObj.Current_ISC_Configuration__c = ipObj.ISC__r[0].id;
                    ipObj.Product_Version_Build__c = ipObj.ISC__r[0].Product_Version_Build__c;
                    ipList.add(ipObj);
                }    
            }
            if(!ipList.isEmpty()){
                update ipList;
            }
        }
    }
    
    private string getFormattedversionwithProduct(ISC_History__c varISC){
        String key =   String.valueOf(varISC.Product__c) +
                        String.valueOf(varISC.Major_Version__c)+ '.'+
                        String.valueOf(varISC.Minor_Version__c)+ '.'+
                        String.valueOf(varISC.Build_Version__c.intvalue());
        system.debug('key==========='+key);
        return key;
    }
    
    private boolean isCheckISC(ISC_History__c iscObj, Map<Id, ISC_History__c> mapOldISCHistory){
        ISC_History__c oldISC;
        if(mapOldISCHistory != null){
            oldISC = mapOldISCHistory.get(iscObj.Id);
        }
        if(iscObj.Installed_Product__c != null 
           && iscObj.Version__c != null 
           && iscObj.Product__c != null
           && iscObj.Minor_Version__c != null
           && iscObj.Minor_Version__c != null
           && (mapOldISCHistory == null 
           || (oldISC != null && 
              (oldISC.Installed_Product__c == null 
           || oldISC.Version__c == null 
           || oldISC.Formatted_Version_w_o_zeros__c == null 
           || oldISC.Product__c == null 
           || oldISC.ISC_API_Table__c == null
           || oldISC.Product_Version_Build__c == null
           || oldISC.Installed_Product__c != iscObj.Installed_Product__c 
           || oldISC.Version__c != iscObj.Version__c 
           || oldISC.Formatted_Version_w_o_zeros__c != iscObj.Formatted_Version_w_o_zeros__c 
           || oldISC.Product__c != iscObj.Product__c
           || oldISC.ISC_API_Table__c != iscObj.ISC_API_Table__c
           || oldISC.Product_Version_Build__c != iscObj.Product_Version_Build__c
           )))){
               return true;
           }else{
               return false;
           }
    }
    
    private String getFormattedVersion(ISC_History__c iscObj){
        String fv = String.valueOf(iscObj.Major_Version__c)+ '.'+
                    String.valueOf(iscObj.Minor_Version__c)+ '.'+
                    String.valueOf(iscObj.Build_Version__c.intvalue());
        system.debug('getFormattedVersion==========='+fv);
        return fv;

    }
    
    private String getFormattedMMVersionKeys(ISC_History__c iscObj){
        String mmfv = String.valueOf(iscObj.Major_Version__c)+ '.'+
                      String.valueOf(iscObj.Minor_Version__c);
        system.debug('getFormattedMMVersionKeys==========='+mmfv);
        return mmfv;

    }
    
    private map<String,Product_Version_Build__c> getProductVersionBuildMap(set<Id> productSetId, set<String> formattedVersionKeys){
        map<String,Product_Version_Build__c> productVersionBuildMap = new map<String,Product_Version_Build__c>();
        System.debug('setProductId === ' + productSetId);
        if(formattedVersionKeys.size() > 0) {
            System.debug('setFormattedVersion === ' + formattedVersionKeys);
            List<Product_Version_Build__c> ProdVersionBuildLst = [select ID, Product__c, Product__r.Product_Type__c, Version__c,
                                                                    Formatted_Version_w_o_zeros__c from Product_Version_Build__c 
                                                                    where Product__c in :productSetId AND
                                                                    Formatted_Version_w_o_zeros__c in: formattedVersionKeys AND Product__r.Product_Type__c = 'Model'];
            if(ProdVersionBuildLst.size() > 0){
                System.debug('ProdVersionBuildLst === ' + ProdVersionBuildLst);
                for(Product_Version_Build__c PVB : ProdVersionBuildLst){
                    if(PVB.Product__c != null && PVB.Formatted_Version_w_o_zeros__c != null){
                        string strKey = string.valueOf(PVB.Product__c) + string.valueOf(PVB.Formatted_Version_w_o_zeros__c);
                        productVersionBuildMap.put(strKey,pvb);
                    }
                }
            }
        }
        return productVersionBuildMap;
    }
    
    private map<String,Product_Version__c> getProductVersionMap(set<Id> productSetId, set<String> formattedVersionKeys){
        map<String,Product_Version__c> productVersionMap = new map<String,Product_Version__c>();
        if(formattedVersionKeys.size() > 0){
            System.debug('setFormattedVerMajorMinor@@@ === ' + formattedVersionKeys);
            List<Product_Version__c> listProductVersion = [select ID, Product__c, Product__r.Product_Type__c, Major_Release__c, Minor_Release__c,
                                                           Version__c from Product_Version__c 
                                                           where Version__c in: formattedVersionKeys
                                                           AND Product__c in :productSetId AND Product__r.Product_Type__c = 'Model'];
            if(listProductVersion.size() > 0){
                for(Product_Version__c varPV : listProductVersion){
                    if(varPV.Product__c != null && varPV.Version__c != null){
                        //Sep 25 2017 : Chandra : Oct Release : STSK0012935 : Added a '.' below to match with prod version.
                        String strKey = string.valueOf(varPV.Product__c) + string.valueOf(varPV.Major_Release__c) + '.' + string.valueOf(varPV.Minor_Release__c);
                        productVersionMap.put(strKey, varPV);
                    }
                }
            }
        }
        return productVersionMap;
    }
    
    
    public void setProductBuild(List<ISC_History__c> LstOfISCHistory, Map<Id, ISC_History__c> mapOldISCHistory){
        map<String,Product_Version_Build__c> productVersionBuildMap = new map<String,Product_Version_Build__c>();
        map<String,Product_Version__c> productVersionMap = new map<String,Product_Version__c>();
        set<Id> productSetId = new set<Id> ();
        set<String> formattedVersionKeys = new set<String> ();
        set<String> formattedMMVersionKeys = new set<String> ();
        list<Product_Version_Build__c> newProductBuildList = new list<Product_Version_Build__c>();
        list<Product_Version__c> newProductVersionList = new list<Product_Version__c>();
        
        for(ISC_History__c iscObj : LstOfISCHistory)
        {
            if(isCheckISC(iscObj,mapOldISCHistory)){
                productSetId.add(iscObj.Product__c);
                formattedVersionKeys.add(getFormattedVersion(iscObj));
                formattedMMVersionKeys.add(getFormattedMMVersionKeys(iscObj));
            }   
        }

        if(productSetId.size() > 0){
            productVersionBuildMap = getProductVersionBuildMap(productSetId,formattedVersionKeys);
            productVersionMap = getProductVersionMap(productSetId,formattedMMVersionKeys);
        }
        
        for(ISC_History__c iscObj : LstOfISCHistory){
            if(isCheckISC(iscObj,mapOldISCHistory)){
                String strBuild = getFormattedversionwithProduct(iscObj);
                String strVersion = String.valueOf(iscObj.Product__c) + getFormattedMMVersionKeys(iscObj);
                //Case1: when Product Version Build exists for the combination of ISC History Fields 
                system.debug('***cm strBuild: '+strBuild);
                system.debug('***productVersionBuildMap : '+productVersionBuildMap);
                if(productVersionBuildMap.size() > 0 && productVersionBuildMap.containsKey(strBuild)){
                    System.debug('Case1: when Product Version Build exists for the combination of ISC History Fields ' + productVersionBuildMap.size());
                    iscObj.Product_Version_Build__c = productVersionBuildMap.get(strBuild).Id;
                }else
                {
                    //Case2: when Product Version exists for the combination of ISC History Fields but Product Version Build doesn't exist
                    Product_Version_Build__c objPVB = new Product_Version_Build__c();
                    objPVB.Product__c = iscObj.Product__c;
                    objPVB.Build_Number__c = iscObj.Build_Version__c;
                    objPVB.Name = getFormattedVersion(iscObj);
                    //Case2: when Product Version exists for the combination of ISC History Fields but Product Version Build doesn't exist
                    if(productVersionMap.containsKey(strVersion))
                    {
                        System.debug('when Product Version exists for the combination of ISC History Fields but Product Version Build doesnt exist');
                        objPVB.Product_Version__c = productVersionMap.get(strVersion).Id;                        
                    }
                    else
                    {
                        //Case3: when neither Product Version nor Product Version Build exists for combination of ISC History Fields
                        System.debug('Case3: when neither Product Version nor Product Version Build exists for combination of ISC History Fields');
                        Product_Version__c objPV = new Product_Version__c();
                        objPV.Major_Release__c = iscObj.Major_Version__c;
                        objPV.Minor_Release__c = iscObj.Minor_Version__c;
                        objPV.Product__c = iscObj.Product__c;
                        objPV.Name = iscObj.Major_Version__c+'.'+iscObj.Minor_Version__c;
                        objPVB.Product_Version__r = objPV;
                        newProductVersionList.add(objPV);
                        productVersionMap.put(strVersion,objPV);
                    }
                    newProductBuildList.add(objPVB);
                    productVersionBuildMap.put(strBuild,objPVB);
                }
            }   
        }

        /********** insert productversion and build records **********/
        if(newProductVersionList.size() > 0){
            insert newProductVersionList;
        }
        //Sep 25 2017 : Chandra : Oct Release : STSK0012935 : Moved the code to create product version build irrespective of product version.
        if(newProductBuildList.size() > 0)
        {
            for(Product_Version_Build__c varPVB : newProductBuildList){
                if(varPVB.Product_Version__r != null){
                    varPVB.Product_Version__c = varPVB.Product_Version__r.ID;
                }
            }
            insert newProductBuildList; 
        }
        
        //set product build on isc history object
        for(ISC_History__c iscObj : LstOfISCHistory){
            if(isCheckISC(iscObj,mapOldISCHistory)){
                String strBuild = getFormattedversionwithProduct(iscObj);
                if(productVersionBuildMap.containsKey(strBuild)){
                    iscObj.Product_Version_Build__c = productVersionBuildMap.get(strBuild).Id;
                }
            }
        
        }
    }
    
}
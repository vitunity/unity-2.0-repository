/***************************************************************************\
      @ Author                : Dushyant Singh.
      @ Date                  : 19-Feb-2013.
      @ Description           : Test Class for Trigger ISC_History_Before_Trigger SRClassISCHistory.
      @ Last Modified By      : Parul Gupta
      @ Last Modified On      : 11/10/2014
      @ Last Modified Reason  : Update Test class
****************************************************************************/
@isTest
public class SRClassISCHistory_test
{
    public static testMethod void testSRClassISCHistory()
    {
        Product2 testProduct = new Product2();
        testProduct.Name = 'test ISC';
        //testProduct.Product_Type__c = 'Model';
        insert testProduct;
        
        Product2 testProduct_2 = new Product2();
        testProduct_2.Name = 'test ISC 2';
        //testProduct_2.Product_Type__c = 'Model';
        insert testProduct_2;

        //IP record
        SVMXC__Installed_Product__c testinstallprd =  new SVMXC__Installed_Product__c();
        testinstallprd.Name = 'H6878876';
        testinstallprd.SVMXC__Serial_Lot_Number__c = 'H687';
        insert testinstallprd;
		
        ISC_API_Table__c apitObj = new ISC_API_Table__c();
        apitObj.PCSN_Field_Name__c = 'H6878876';
        apitObj.Name = 'ISC_TrueBeam'; 
        apitObj.Update_Installed_Product_Version__c = true;
        insert apitObj;

        //Product Version record
        Product_Version__c testProductversion = new Product_Version__c();
        testProductversion.Name = 'test ISC';
        testProductversion.Product__c = testProduct.id;
        testProductversion.Major_Release__c = 13;
        testProductversion.Minor_Release__c = 12;
        insert testProductversion;
        
        //Product Version record without Prod Version Build
        Product_Version__c testProductversion_2 = new Product_Version__c();
        testProductversion_2.Name = 'test ISC 2';
        testProductversion_2.Product__c = testProduct_2.id;
        testProductversion_2.Major_Release__c = 1;
        testProductversion_2.Minor_Release__c = 1;
        insert testProductversion_2;

        //Product Version Build record
        Product_Version_Build__c testProductversionbuilt = new Product_Version_Build__c();
        testProductversionbuilt.Name = 'test ISC';
        testProductversionbuilt.Product_Version__c = testProductversion.id;
        testProductversionbuilt.Product__c = testProduct.id;
        testProductversionbuilt.Build_Number__c = 3;
        insert testProductversionbuilt ;                    

        //insert ISC History record 1
        ISC_History__c testISCHistory = new ISC_History__c();
        testISCHistory.PCSN__c = 'abc test';
        testISCHistory.Version__c = '12.34';
        testISCHistory.Installed_Product__c = testinstallprd.id;
        testISCHistory.Major_Version__c = 13;
        testISCHistory.Minor_Version__c = 12;
        testISCHistory.Build_Version__c = 3;
        testISCHistory.Product__c = testProduct.id;
        testISCHistory.API_Table_Name__c = 'ISC_TrueBeam';
        insert testISCHistory;
        
        //insert ISC History record 2 without Prod Version and Prod Version Build
        ISC_History__c testISCHistory_2 = new ISC_History__c();
        testISCHistory_2.PCSN__c = 'abc test 2';
        testISCHistory_2.Version__c = '12.34';
        testISCHistory_2.Installed_Product__c = testinstallprd.id;
        testISCHistory_2.Major_Version__c = 11;
        testISCHistory_2.Minor_Version__c = 11;
        testISCHistory_2.Build_Version__c = 1;
        testISCHistory_2.Product__c = testProduct.id;
        testISCHistory_2.API_Table_Name__c = 'ISC_TrueBeam';
        insert testISCHistory_2;

        //insert ISC History record 2
        ISC_History__c varISCHistory = new ISC_History__c();
        varISCHistory.PCSN__c = 'test pscn';
        varISCHistory.Version__c = '123.12.3';
        varISCHistory.API_Table_Name__c = 'ISC_TrueBeam';
        insert varISCHistory;
        
        //update ISC History
        varISCHistory.PCSN__c = 'H6878876';
        varISCHistory.Installed_Product__c = testinstallprd.id;
        varISCHistory.Version__c = '12.34';
        varISCHistory.API_Table_Name__c = 'ISC_TrueBeam';
        update varISCHistory;
    }
}
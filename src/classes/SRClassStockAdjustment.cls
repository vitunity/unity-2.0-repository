/*************************************************************************\
    @ Author        : Ranjan Jha
    @ Date          : 13-May-2014
    @ Description   : This class is used to add before and after triggers written on StockAdjustment 
    @ Last Modified By  :   
    
****************************************************************************/
public class SRClassStockAdjustment
{
    /*The below method is to poplate value on product field on StockAdjustment*/
    public void SetfieldsonStockAdjst(list< SVMXC__Stock_Adjustment__c> lstStockAdjustmnt )
    {
        Set <String> ERPMaterialNumber= new Set <String > ();
        Map<String, id> mapstockadjustment= new Map<String,id>();

        for(SVMXC__Stock_Adjustment__c Stock :lstStockAdjustmnt)
        {
            if(Stock.ERP_Material_Nbr__c != null)   //null check added by Parul, 11/12/14
            {
                ERPMaterialNumber.add(Stock.ERP_Material_Nbr__c);
            }
        }

        if(ERPMaterialNumber.size() > 0)
        {
            for(Product2 prdt : [Select Id, ProductCode from Product2 where ProductCode in :ERPMaterialNumber ])
            {
                System.debug('product--->'+prdt);
                if(prdt.ProductCode != null)
                {
                    mapstockadjustment.put(prdt.ProductCode ,prdt.id );
                }
            }
            for (SVMXC__Stock_Adjustment__c Stock1 :lstStockAdjustmnt)
            {
                if(mapstockadjustment.containsKey(Stock1.ERP_Material_Nbr__c))
                {
                    Stock1.SVMXC__Product__c = mapstockadjustment.get(Stock1.ERP_Material_Nbr__c);
                }
            }
        }
    }
 }
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SRClassStockAdjustment_Test {
	
	public static SVMXC__Stock_Adjustment__c stockAdjust_Data(Boolean isInsert, Product2 newProd, SVMXC__Site__c newLoc) {
		SVMXC__Stock_Adjustment__c adjust = new SVMXC__Stock_Adjustment__c();
		adjust.SVMXC__Product__c = newProd.Id;
		adjust.SVMXC__Location__c = newLoc.Id;
		adjust.SVMXC__New_Quantity2__c = 10.0;
		adjust.ERP_Material_Nbr__c = newProd.ProductCode;
		adjust.SVMXC__Adjustment_Type__c = 'Missing';
		if(isInsert)
			insert adjust;
		return adjust;
	}

    static testMethod void SRClassStockAdjustment_Test() {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(false, newAcc);
    	newLoc.SVMXC__Stocking_Location__c = true;
    	insert newLoc;
    	
    	SVMXC__Stock_Adjustment__c adjust = stockAdjust_Data(true, newProd, newLoc);
    }
}
/*************************************************************************\
      @ Author          : Harshita Sawlani
      @ Date            : 21 October 2014.
****************************************************************************/
public class SROverrideModelPart 
{   
    public String emailId { get; set; }  // added by Nikita, 5/12/2014
    
    public String SOIId;
    public string formid{get;set;}
    public List<Product2> results{get;set;} // search results
    public string searchString{get;set;} // search keyword
    public string ProductId {get;set;}
    public string ProductName {get;set;}
        
    public SROverrideModelPart(ApexPages.StandardController controller) 
    {   
        ProductId = '';
        ProductName = '';
        formid=ApexPages.currentPage().getParameters().get('formid');
        system.debug('&&&&&'+formid);
        searchString = System.currentPageReference().getParameters().get('lksrch');
        runSearch();
         
        
    }
    
    public void search() 
    {
        runSearch();
        //return null;
    }
      
    // prepare the query and issue the search command
    private void runSearch() 
    {
        // TODO prepare query string for complex serarches & prevent injections
        results = performSearch(searchString);
                     
    } 
     
      // run the search and return the records found.
    @TestVisible // Added by Puneet Mishra for Unit Test
    private List<Product2> performSearch(string searchString) 
    {
        SOIId=ApexPages.currentPage().getParameters().get('prcTempSel'); 
        String productId = ApexPages.currentPage().getParameters().get('productId');
        //id ProductId  = [Select product__c from Sales_Order_Item__c where id =: SOIId].product__c;
        set<id> childprdpartid = new set<id>();
        for(Product_FRU__c productpart : [Select Part__c from Product_FRU__c where Top_Level__c =: productId]) // all the child roduct part
        {
            childprdpartid.add(productpart.Part__c);
        }
        String soql = 'select id,name,ProductCode,SVMXC__Product_Line__c,IsActive from Product2 where Id in: childprdpartid AND Product_Type__c = \'Model\'';
        if(searchString != '' && searchString != null)
            soql = soql + ' and name LIKE \'%' + searchString+'%\'';  //Nikita, DE735, 5/21/2014, Added '%' before the searchstring also.
        soql = soql + ' limit 25';
        System.debug('*******'+soql);
        return database.query(soql); 
    }
    
    public string getFormTag() 
    {
        return System.currentPageReference().getParameters().get('frm');
    }
     
      // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() 
    {
        return System.currentPageReference().getParameters().get('txt');
    }
    
 }
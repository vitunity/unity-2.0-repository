/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SROverrideModelPart_Test {

    static testMethod void SROverrideModelPart_TEST() {
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(newProd);
    	SROverrideModelPart control = new SROverrideModelPart(sc);
    	Test.setCurrentPageReference(Page.SROverWritePartSOI); 
    	System.currentPageReference().getParameters().put('formid', newProd.Id);
    	System.currentPageReference().getParameters().put('lksrch', newProd.Name);
		System.currentPageReference().getParameters().put('frm', 'FROM');
		System.currentPageReference().getParameters().put('txt', 'TEXT');
    }
    
    static testmethod void getFormTag_Test() {
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(newProd);
    	SROverrideModelPart control = new SROverrideModelPart(sc);
    	Test.setCurrentPageReference(Page.SROverWritePartSOI); 
    	System.currentPageReference().getParameters().put('formid', newProd.Id);
    	System.currentPageReference().getParameters().put('lksrch', newProd.Name);
		System.currentPageReference().getParameters().put('frm', 'FROM');
		System.currentPageReference().getParameters().put('txt', 'TEXT');
		
		String expectedVal = control.getFormTag();
    	system.assertEquals('FROM', expectedVal);
    }
    
    static testMethod void getTextBox_Test() {
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(newProd);
    	SROverrideModelPart control = new SROverrideModelPart(sc);
    	Test.setCurrentPageReference(Page.SROverWritePartSOI); 
    	System.currentPageReference().getParameters().put('formid', newProd.Id);
    	System.currentPageReference().getParameters().put('lksrch', newProd.Name);
		System.currentPageReference().getParameters().put('frm', 'FROM');
		System.currentPageReference().getParameters().put('txt', 'TEXT');
    	
    	String expectedVal = control.getTextBox();
    	system.assertEquals('TEXT', expectedVal);
    }
    
    static testMethod void performSearch_TEST() {
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	
    	Product2 part = UnityDataTestClass.product_data(false);
    	part.Name = part.Name + ' - PART ';
    	part.ProductCode = 'VC_RAE_PART';
    	part.BigMachines__Part_Number__c = 'VC_RAE_PART';
    	insert part;
    	
    	Product_FRU__c fru = UnityDataTestClass.Product_FRU_TestData(true, newProd, part);
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(newProd);
    	SROverrideModelPart control = new SROverrideModelPart(sc);
    	Test.setCurrentPageReference(Page.SROverWritePartSOI); 
    	System.currentPageReference().getParameters().put('formid', newProd.Id);
    	System.currentPageReference().getParameters().put('lksrch', newProd.Name);
		System.currentPageReference().getParameters().put('frm', 'FROM');
		System.currentPageReference().getParameters().put('txt', 'TEXT');
		System.currentPageReference().getParameters().put('productId', newProd.Id);
		control.search();
		List<Product2> prodList = control.performSearch(newProd.Name);
		control.emailId = 'test@testmail.com';
    }
    
}
@isTest(SeeAllData=true)
public class SRPopulateArticleBodybyCaseDes_test {

    static Service_Article__kav article;
    static Case testcase;
    static SVMXC__Service_Order__c instWO;
    
    
    
    static void initData()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='VMS System Admin'];
        
        id techRectypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();      
        id servTeamRecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
                
           
        User us = new User();
        us.Firstname = 'tet';
        us.LastName = 'User';
        us.Alias = 'tstUsr';
        us.CommunityNickname= 'tUsr';
        us.Email = 'tstUsr@abc.com';
        us.Username = 'tstUsr1@abc.com';       
        us.EmailEncodingKey='UTF-8';
        us.LanguageLocaleKey='en_US';       
        us.LocaleSidKey='en_US';
        us.ProfileId = p.Id;        
        us.TimeZoneSidKey='America/Los_Angeles';
        insert us;
    
         // Account
        Account testacc = new Account(name = 'testAcc');
        testacc.country__c = 'USA';
        testacc.BillingState = 'CA';
        testacc.BillingCity = 'Milpitas';
        testacc.BillingPostalCode = '123456';
        insert testacc;

         // insert Contact 
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testacc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con;
        
        testcase = new Case();
        testcase.Contactid=con.id;
        Insert testcase ;

        SVMXC__Service_Group__c servcTeam = new SVMXC__Service_Group__c();
        servcTeam.name = 'testTeam';
        servcTeam.SVMXC__Active__c = true;
        servcTeam.SVMXC__Group_Code__c = servcTeam.name;
        servcTeam.RecordTypeId = servTeamRecordTypeId;
        insert servcTeam;
        
        SVMXC__Site__c loc = new SVMXC__Site__c(name = 'testLocation');
        insert loc;
        
        SVMXC__Service_Group_Members__c testTech =  new SVMXC__Service_Group_Members__c();
        testTech.name = 'testTech';
        testTech.SVMXC__Salesforce_User__c = us.id;
        //testTech.User__c = us.id;
        testTech.SVMXC__Service_Group__c = servcTeam.id;
        testTech.SVMXC__Active__c = true;
        testTech.SVMXC__Email__c = 'abc@gmail.com';
        testTech.SVMXC__Inventory_Location__c = loc.id;
        testTech.SVMXC__Country__c = 'India';
        testTech.RecordTypeId = techRectypeId;
        
        insert testTech;

        List<SVMXC__Service_Order__c> lstOfWo = new List<SVMXC__Service_Order__c>();
        
        instWO = new SVMXC__Service_Order__c();
        instWO.SVMXC__Company__c = testacc.id;
        //testWO.RecordTypeId = ;
        instWO.SVMXC__Purpose_of_Visit__c = 'New Installation';
        instWO.SVMXC__Group_Member__c = testTech.id;
        instWO.Service_Comment__c= 'Service_Comment';
        instWO.SVMXC__Problem_Description__c = 'Test Description';
        instWO.Subject__c ='Subject';
        lstOfWo.add(instWO);       
        
        insert lstOfWo; 
    
        article= new Service_Article__kav();
        article.Article_Image__c = instWO.Service_Comment__c+instWO.SVMXC__Problem_Description__c;     
        article.Summary = instWO.Subject__c;
        article.Title = instWO.Subject__c + system.today();     
        article.UrlName=instWO.Subject__c;     
        insert article;
    
    }
    
    static testMethod void SRPopulateArticleBodybyCaseDes()
    {
        
        initData();     
        ApexPages.currentPage().getParameters().put('sourceId', testcase.id);
        ApexPages.KnowledgeArticleVersionStandardController testcontroller = new ApexPages.KnowledgeArticleVersionStandardController(article);
        SRPopulateArticleBodybyCaseDescription controller = new SRPopulateArticleBodybyCaseDescription(testcontroller); 

        ApexPages.currentPage().getParameters().put('sourceId', instWO.id);
        ApexPages.KnowledgeArticleVersionStandardController testcontroller1 = new ApexPages.KnowledgeArticleVersionStandardController(article);
        SRPopulateArticleBodybyCaseDescription controller1 = new SRPopulateArticleBodybyCaseDescription(testcontroller1);   
    }
    
}
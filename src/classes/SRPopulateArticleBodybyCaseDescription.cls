/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 01-Aug-2013
      @ Description           : An Apex Controller to auto populate Article Body by Case Description.
      @ Last Modified By      :  Chiranjeeb Dhar   
      @ Last Modified On      :  20/01/2014   
      @ Last Modified Reason  :  US3614   

Change Log:
12-Jan-2017 - Divya Hargunani - STSK0013616 - STRY0040874 : Create two new article templates (types)
15-Feb-2017 - Divya Hargunani - STSK0013738 - STRY0040874 : Create two new article templates (types)- Added condition for different article types
****************************************************************************/
public class SRPopulateArticleBodybyCaseDescription 
     {
     public SRPopulateArticleBodybyCaseDescription (ApexPages.KnowledgeArticleVersionStandardController ArticleAlias) 
        {
            String solutionRecTypeId = RecordTypeUtility.getInstance().rtMaps.get('Service_Article__kav').get('Solution');
            String howToRecTypeId = RecordTypeUtility.getInstance().rtMaps.get('Service_Article__kav').get('How_To');
            String qNaRecTypeId = RecordTypeUtility.getInstance().rtMaps.get('Service_Article__kav').get('Q_A');
            system.debug('ArticleAlias.getRecord()======='+ArticleAlias.getRecord());
            //STSK0013616 : Updated the data type for providing support for all article types
            Service_Article__kav  article = (Service_Article__kav) ArticleAlias.getRecord();  
            system.debug('article======='+article);
            String sourceId = ArticleAlias.getSourceId();
            String keyPrefix = sourceId.substring(0, 3);
                Schema.DescribeSObjectResult r = SVMXC__Service_Order__c.sObjectType.getDescribe();
                String keyPrefixWO = r.getKeyPrefix();
                if(keyPrefix == '500')
                {
                    Case casealias = [select Id,Description_en_new__c ,subject, description,Local_Case_Activity__c,
                                      Symptoms__c,Applies_To__c,ECF_Investigation_Notes__c,Engineering_Notes__c,
                                      AccountId from Case where id=:sourceId];
                    article.put('Solution__c',casealias.Local_Case_Activity__c);  
                    article.Put('Symptoms__c',casealias.Symptoms__c );
                    article.put('Answer__c',casealias.Local_Case_Activity__c);
                    article.put('Process__c',casealias.Local_Case_Activity__c);
                    article.Put('Title',casealias.subject); 
                    article.Put('Manager_Name__c',casealias.Applies_To__c); 
                    article.Put('Properties__c',casealias.ECF_Investigation_Notes__c); 
                    article.Put('PSE_Only__c',casealias.Engineering_Notes__c); 
                    article.Put('UrlName',casealias.subject+String.ValueOf(system.now()).replace('/','-')); 
                }
                else if(keyPrefix == keyPrefixWO)
                {
                    SVMXC__Service_Order__c woAlias = [select Id, SVMXC__Case__r.subject, SVMXC__Case__r.description, SVMXC__Case__r.Local_Case_Activity__c, SVMXC__Case__r.AccountId from SVMXC__Service_Order__c where id=:sourceId];
                    article.put('Solution__c',woAlias.SVMXC__Case__r.Local_Case_Activity__c);  
                    article.Put('Symptoms__c',woAlias.SVMXC__Case__r.Symptoms__c);
                    article.put('Answer__c',woAlias.SVMXC__Case__r.Local_Case_Activity__c);
                    article.put('Process__c',woAlias.SVMXC__Case__r.Local_Case_Activity__c);
                    article.Put('Title',woAlias.SVMXC__Case__r.subject); 
                    article.Put('Manager_Name__c',woAlias.SVMXC__Case__r.Applies_To__c); 
                    article.Put('Properties__c',woAlias.SVMXC__Case__r.ECF_Investigation_Notes__c); 
                    article.Put('PSE_Only__c',woAlias.SVMXC__Case__r.Engineering_Notes__c);
                    article.Put('UrlName',woAlias.SVMXC__Case__r.subject+ String.ValueOf(system.now()).replace('/','-')); 
                
                }
       }
    }
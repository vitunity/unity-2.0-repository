/*************

@author : Mansi Vashisht
Reason : covering trigger SRPopulateCaseCommentonCaseActivity
@date : 31-03-2015
**************/
@isTest
public class SRPopulateCaseCommentonCaseActivity_Test {
    
    public static testMethod void myController1() {
        
        //Insert Account Record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234',Prefered_Language__c ='English');
        insert accnt;

        // insert Contact 
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = accnt.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con;

        
        //Insert Case Record 
        list<case> caseList = new list<case>();
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
        Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accnt.Id,Contactid= con.id,Subject ='test case',Status = 'New', Reason ='Information',Case_Activity__c ='test case activity', Priority = 'Medium' );
        caseList.add(caseObj);
        
        Case caseObj1 = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accnt.Id,Contactid=con.id,Subject ='test case',Status = 'New', Reason ='Information',Priority = 'Medium');
        caseList.add(caseObj1);
        insert caseList;
        
        
        // Insert Case Comment Record
        list<Case_Comments__c> caseCommnetlist = new list<Case_Comments__c>();
        list<Case_Comments__c> caseCommnetlistupdate = new list<Case_Comments__c>();
        Case_Comments__c caseComObj = new Case_Comments__c (Case__c = caseObj.id, Comments__c= '1234567890sdfghjkl');
        caseCommnetlist.add(caseComObj);
        Case_Comments__c caseComObj1 = new Case_Comments__c (Case__c = caseObj1.id, Comments__c= '1234567890sdfghjkl');
        caseCommnetlist.add(caseComObj1);
        insert caseCommnetlist;
        caseComObj.Comments__c = 'asdfghjklrghjkl7654';
        caseCommnetlistupdate.add(caseComObj);
        caseComObj1.Comments__c = 'asdfghjklrghjkl7654';
        caseCommnetlistupdate.add(caseComObj1);
        update caseCommnetlistupdate ;
        
    }     
}
public class SRSendSummaryReport {
public String CaseId;
Public List<Case> Casedetails;
    public SRSendSummaryReport(ApexPages.StandardController controller) 
    {
     CaseId=Apexpages.currentpage().getparameters().get('Id');
      Casedetails=[Select CaseNumber,CreatedDate,Status,Subject,Case.Owner.Name,SVMXC__Site__r.Name,Contact.Name,Contact.Id,SVMXC__Top_Level__r.Name,Description from Case where Id=:CaseId];
    }

    public pagereference sendsummaryreport()
    {
    if(Casedetails[0].Status=='Closed by Work Order (Close case)' || Casedetails[0].Status=='Closed (Close case)')
    {
      String body='Hi  '+Casedetails[0].Contact.Name+'\n\nPlease find below the Summary of the Closed Case below:\n\nCase Number :  '+Casedetails[0].CaseNumber+'\nDate Created :  '+Casedetails[0].CreatedDate+'\nHelpdesk Agent:  '+Casedetails[0].Owner.Name+'\nSite Name:  '+Casedetails[0].SVMXC__Site__r.Name+'\nContact Person:  '+Casedetails[0].Contact.Name+'\nEquipment/Product:  '+Casedetails[0].SVMXC__Top_Level__r.Name+'\nTicket Description:  '+Casedetails[0].Description+'\n\nThanks,\nVarian Medical Systems.' ; 
    String Subject='Case Number '+Casedetails[0].CaseNumber+' with Subject '+Casedetails[0].Subject+' Closed.';
    String ToId=Casedetails[0].Contact.Id;
    String redirect='/_ui/core/email/author/EmailAuthor?p7='+body+'&p6='+subject+'&p2_lkid='+ToId+'&p26=varianhelpdesk@gmail.com';
    
    PageReference pagereferencealias= new PageReference(redirect);
   
    pagereferencealias.setRedirect(true);
    return pagereferencealias;
 
    }
    else
    {
      ApexPages.Message error = new ApexPages.Message(ApexPages.SEVERITY.INFO,'You can send the Summary Report only when the Case is closed.');
      ApexPages.addMessage(error);
    }
    return null;
    }
    public pagereference donothing()
    {
    return null;
    }
}
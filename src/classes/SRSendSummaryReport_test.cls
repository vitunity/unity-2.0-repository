@isTest
public class SRSendSummaryReport_test {

    public static testMethod void mytestController1() {
        //Dummy data crreation 
        //Profile sysadminprofile = new profile();
       // sysadminprofile = [Select id from profile where name = 'System Administrator'];
       // User systemuser = new user(alias = 'stant',email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = sysadminprofile.Id, timezonesidkey='America/Los_Angeles',username='stan123daruser@testorg.com');
       // insert systemuser;
        //system.runas(systemuser){
            //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234');
        insert accnt;

        // insert Contact 
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = accnt.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con;

                
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
         Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accnt.Id,Contactid=con.id, Subject ='test case', Status = 'Closed by Work Order (Close case)' );
         insert caseObj ;
           
            Apexpages.currentpage().getparameters().put('Id',caseObj.id);
            ApexPages.StandardController testcontroller = new ApexPages.StandardController(caseObj);
            SRSendSummaryReport testrecord = new SRSendSummaryReport(testcontroller);
            testrecord.sendsummaryreport();
            testrecord.donothing();
       // }
      }
      
      public static testMethod void mytestController() {
        //Dummy data crreation 
        //Profile sysadminprofile = new profile();
       // sysadminprofile = [Select id from profile where name = 'VMS System Admin'];
        //User systemuser = new user(alias = 'stupid',email='stan@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = sysadminprofile.Id, timezonesidkey='America/Los_Angeles',username='stan@testorg.com');
        //insert systemuser;
        //system.runas(systemuser){
            //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234');
        insert accnt;
        
        // insert Contact 
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = accnt.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con;
        
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
         Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accnt.Id,Contactid=con.id,Subject ='test case', Status = 'New' );
         insert caseObj ;
            Apexpages.currentpage().getparameters().put('Id',caseObj.id);
            ApexPages.StandardController testcontroller = new ApexPages.StandardController(caseObj);
            SRSendSummaryReport testrecord = new SRSendSummaryReport(testcontroller);
            testrecord.sendsummaryreport();
            testrecord.donothing();
             List<Apexpages.Message> msgs = ApexPages.getMessages();

        boolean isErrorMessage = false;

        for(Apexpages.Message msg : msgs){
            if (msg.getDetail().contains('You can send the Summary Report only when the Case is closed') )
                isErrorMessage  = true;
        }
        //Assert that the Page Message was Properly Displayed
        system.assert(isErrorMessage );

    //}
            
        }
      }
/*************************************************************************\
      @ Author          : Nikhil Verma
      @ Date            : 04-Feb-2014
      @ Description     : This class updates Sales Order and Sales Order Item fields on Acceptance Date History object.
      @ Last Modified By      :  Shubham Jaiswal
      @ Last Modified On      :  04-Apr-2-14
      @ Last Modified Reason  :  US4170 to replace SO & SOI Name with ERP Reference
      @ Last Modified By      :  Nikita Gupta
      @ Last Modified On      :  02-Jun-2014
      @ Last Modified Reason  :  Added null checks before all the set, list and map calls.
/****************************************************************************/

public class SR_AccepDateHistUtility
{
    public static void updateSalesOrderAndSalesOrderItem(List<Acceptance_Date_History__c> accDtHis)
    {
        set<String> erpSalesOrder = new set<String>(); //set of all new ERP Sales Order values
        set<String> erpSalesOrderItem = new set<String>(); //set of all new ERP Sales Order Item values
        Map<String, Sales_Order__c> name2SalesOrderMap = new Map<String, Sales_Order__c>(); //map of Sales Order records with Name as key
        Map<String, Sales_Order_Item__c> name2SalesOrderItemMap = new Map<String, Sales_Order_Item__c>(); //map of Sales Order Item records with Name as key
        
        for(Acceptance_Date_History__c acdh : accDtHis)
        {
            System.debug('HAHA acdh.ERP_Sales_Order__c = '+acdh.ERP_Sales_Order__c);
            if(acdh.ERP_Sales_Order__c != null)
            {
                erpSalesOrder.add(acdh.ERP_Sales_Order__c);  
            }//add the string values to the set
            if(acdh.ERP_Sales_Order_Item__c != null)
            {
                erpSalesOrderItem.add(acdh.ERP_Sales_Order_Item__c);        //add the string values to the set
            }   
        }
        if(erpSalesOrder.size() > 0) //null check added by Nikita on 6/2/2014
        {
            //List<Sales_Order__c> salesOrderList = [SELECT Name FROM Sales_Order__c WHERE Name in :erpSalesOrder];  // code commented acc to US4170 & replaced with
            List<Sales_Order__c> salesOrderList = [SELECT Id, ERP_Reference__c FROM Sales_Order__c WHERE ERP_Reference__c in :erpSalesOrder]; //List of all Sales Order records whose names match with values in ERP Sales Order field in Acceptance Date History object
            
            if(salesOrderList.size() > 0)   //null check added by Nikita on 6/2/2014
            {
                for(Sales_Order__c salOrd : salesOrderList) //for loop moved inside null check braces by Nikita on 6/2/2014
                {
                   // name2SalesOrderMap.put(salOrd.Name, salOrd);    // code commented acc to US4170 & replaced with
                    name2SalesOrderMap.put(salOrd.ERP_Reference__c, salOrd);  //adding records to the map
                }
            }       
        }
        if(erpSalesOrderItem.size() > 0) //null check added by Nikita on 6/2/2014
        {
            //List<Sales_Order_Item__c> salesOrderItemList = [SELECT Name, Sales_Order__c, Sales_Order__r.Name FROM Sales_Order_Item__c WHERE Name in :erpSalesOrderItem];     
            List<Sales_Order_Item__c> salesOrderItemList = [SELECT Id, ERP_Reference__c, Sales_Order__c, Sales_Order__r.ERP_Reference__c FROM Sales_Order_Item__c WHERE ERP_Reference__c in :erpSalesOrderItem];  //List of all Sales Order Item records whose names match with values in ERP Sales Order Item field in Acceptance Date History object
            System.debug('HAHA salesOrderItemList  = '+salesOrderItemList);
            
            if(salesOrderItemList.size() > 0)   //null check added by Nikita on 6/2/2014
            {
                for(Sales_Order_Item__c salOrdItm : salesOrderItemList) //for loop moved inside null check braces by Nikita on 6/2/2014
                {
                    //name2SalesOrderItemMap.put(salOrdItm.Name, salOrdItm); // code commented acc to US4170 & replaced with
                    name2SalesOrderItemMap.put(salOrdItm.ERP_Reference__c, salOrdItm);
                }
            }
        }
        if(name2SalesOrderMap.size() > 0) //null check added by Nikita on 6/2/2014
        {
            for(Acceptance_Date_History__c acdh : accDtHis)
            {
                if(name2SalesOrderMap.containsKey(acdh.ERP_Sales_Order__c)) //null check added by Nikita on 6/2/2014
                {
                    Sales_Order__c inst = name2SalesOrderMap.get(acdh.ERP_Sales_Order__c);
                    if(inst != null)
                    {
                        acdh.Sales_Order__c = inst.Id;                                 //update the Sales Order Lookup
                        System.debug('HAHA acdh.Sales_Order__c = '+acdh.Sales_Order__c);
                    }
                }   
            }
        }
        if(name2SalesOrderItemMap.size() > 0) //null check added by Nikita on 6/2/2014
        {
            for(Acceptance_Date_History__c acdh : accDtHis)
            {
                if(name2SalesOrderItemMap.containsKey(acdh.ERP_Sales_Order_Item__c)) //null check added by Nikita on 6/2/2014
                {
                    Sales_Order_Item__c inst = name2SalesOrderItemMap.get(acdh.ERP_Sales_Order_Item__c);
                    //if(inst != null && inst.Sales_Order__r.Name == acdh.Sales_Order__c)   // code commented acc to US4170 & replaced with          
                    if(inst != null && inst.Sales_Order__c == acdh.Sales_Order__c)    //null check & check whether value in Sales Order lookup of Sales Order Item object matches with value in Sales Order lookup of Acceptance Date History object.
                    {
                        acdh.Sales_Order_Item__c = inst.Id; //update the Sales Order Item lookup
                    }
                }
            }
        }
    }
}
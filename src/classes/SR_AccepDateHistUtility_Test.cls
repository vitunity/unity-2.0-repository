/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_AccepDateHistUtility_Test {

	public static Acceptance_Date_History__c accepDateHis_Data(Boolean isInsert, Sales_Order__c so, Sales_Order_Item__c soi) {
		Acceptance_Date_History__c acceptDateHistory = new Acceptance_Date_History__c();
		if(so!=null)
			acceptDateHistory.ERP_Sales_Order__c = so.ERP_Reference__c;
		if(soi!=null)
			acceptDateHistory.ERP_Sales_Order_Item__c = soi.ERP_Reference__c;
		acceptDateHistory.Acceptance_Date__c = system.today();
		if(isInsert)
			insert acceptDateHistory;
		return acceptDateHistory;
	}

    static testMethod void updateSalesOrderAndSalesOrderItem_SO_Reference_TEST() {
    	Account newAcc =  UnityDataTestClass.AccountData(true);
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPcode = UnityDataTestClass.ERPPCODE_Data(true);
    	
    	Sales_Order__c so = UnityDataTestClass.SO_Data(false, newAcc);
    	so.ERP_Reference__c = 'ABC123';
    	insert so;
    	Sales_Order_Item__c soi = UnityDataTestClass.SOI_Data(true, so);
    	
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(true, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	Acceptance_Date_History__c acceptDateHistory = accepDateHis_Data(false, so, null);
        acceptDateHistory.Installed_Product__c = ip.Id;
        insert acceptDateHistory;
        
        List<Acceptance_Date_History__c> acceptHistoryList = new List<Acceptance_Date_History__c>();
        acceptHistoryList.add(acceptDateHistory);
        
        SR_AccepDateHistUtility.updateSalesOrderAndSalesOrderItem(acceptHistoryList);
    }
    
    static testMethod void updateSalesOrderAndSalesOrderItem_SOI_Reference_TEST() {
    	Account newAcc =  UnityDataTestClass.AccountData(true);
    	Product2 newProd = UnityDataTestClass.product_Data(true);
    	SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	ERP_Pcode__c erpPcode = UnityDataTestClass.ERPPCODE_Data(true);
    	
    	Sales_Order__c so = UnityDataTestClass.SO_Data(true, newAcc);
    	
    	Sales_Order_Item__c soi = UnityDataTestClass.SOI_Data(false, so);
    	soi.ERP_Reference__c = 'ABC123';
    	insert soi;
    	
    	SVMXC__Installed_Product__c ip = UnityDataTestClass.InstalledProduct_Data(true, newProd, newAcc, newLoc, serviceTeam, erpPcode);
    	Acceptance_Date_History__c acceptDateHistory = accepDateHis_Data(false, null, soi);
        acceptDateHistory.Installed_Product__c = ip.Id;
        acceptDateHistory.ERP_Sales_Order_Item__c = 'ABC123';
        acceptDateHistory.Sales_Order__c = so.Id;
        insert acceptDateHistory;
        
        List<Acceptance_Date_History__c> acceptHistoryList = new List<Acceptance_Date_History__c>();
        acceptHistoryList.add(acceptDateHistory);
        
        SR_AccepDateHistUtility.updateSalesOrderAndSalesOrderItem(acceptHistoryList);
    }
}
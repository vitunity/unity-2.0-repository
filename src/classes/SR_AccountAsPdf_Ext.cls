/**************************************
    @Author :   Nikita Gupta
    @Created On :   24th April,2014
    @Created Reason :   US3365, To create a Print Pdf functionality for Account and Location Objects.
    @Last Modified By   :   Nikita Gupta
    @Last Modified On   :   25th April, 2014
    @Last Modified Reason   :   Added a list size check
    @Last Modified By   :   Nikita Gupta
    @Last Modified On   :   21st May, 2014
    @Last Modified Reason   :   check added for list out of bound

**************************************/
public class SR_AccountAsPdf_Ext 
{
    //public string Accid { get; set; }
    public List<Account> ObjAcc {get;set;}
    public string strAccountId{get;set;}
    String strLocationId;
    set<string> stChildData=new set<string>();
    public List<String> LstOfProductNodes{get;set;} 
    set<string> stLocationId=new set<string>();
    list<SVMXC__Installed_Product__c> lstInstalledProduct;
    set<string> stIP=new set<string>();
    Map<id,List<SVMXC__Installed_Product__c>>MapProductAndLstChildren=new Map<Id,List<SVMXC__Installed_Product__c>>();
    Map<Id,SVMXC__Installed_Product__c> MapOfProducts;
    List<SVMXC__Installed_Product__c> LstOfProducts;
    public List<WrpProductNode> nodelist{get;set;}
    public WrpProductNode node{get;set;}
    Integer stylecolor = 0;  
    private FINAL static string DARKGREY = '#A8A8A8';
    private FINAL static string LIGHTGREY = '#C8C8C8';
        
   
   //Contact List Code starts
    public list<Contact> objContact {get;set;} 
    public list<Contact> getrelcon()
    {
        if(apexpages.currentpage().getparameters().get('cid')=='lc')
        {
            objContact =[SELECT Name, Email, Phone, Contact_Type__c FROM Contact where Location__c =: strLocationId];
        }

        if(apexpages.currentpage().getparameters().get('cid')=='Acc')
        {
            objContact =[SELECT Name, Email, Phone, Contact_Type__c FROM Contact where accountid =: strAccountId];
        }   
        return objContact;
             
    }
    //Contact List Code starts
    
   
        
   
    //public SR_AccountAsPdf_Ext(ApexPages.StandardController controller) 
    public SR_AccountAsPdf_Ext()
    {
        nodelist = new list<WrpProductNode>();
        //wrapcontactlist = new List<wrapperforaltenatestyle>();
        ObjAcc =new List<Account>();
        
       
        LstOfProductNodes=new List<String>();
        if(apexpages.currentpage().getparameters().get('cid')=='lc')
        {
            strLocationId=apexpages.currentpage().getparameters().get('id');  // geting Id Parameter of Location
            if(strLocationId != null)
            {
                stLocationId.add(strLocationId);
                //strAccountId = [select ID, SVMXC__Account__c from  SVMXC__Site__c where id =:strLocationId].SVMXC__Account__c;
                strAccountId = apexpages.currentpage().getparameters().get('accid');
                /*Map<ID, SVMXC__Site__c> mapLocAccountID = new Map<ID, SVMXC__Site__c>([select ID, SVMXC__Account__c from  SVMXC__Site__c where id =:strLocationId]);
                if(mapLocAccountID.size() > 0)
                for(ID loc : mapLocAccountID.keyset())
                {
                    strAccountId = mapLocAccountID.get(loc).SVMXC__Account__c;
                }*/
            }
            
            
        }
        if(apexpages.currentpage().getparameters().get('cid')=='Acc')
        {
            //Accid = apexpages.currentpage().getparameters().get('id');
            strAccountId=apexpages.currentpage().getparameters().get('id');  // geting Id of Parameter
            system.debug('@@@@@@@@@@@' + strAccountId);
            list<SVMXC__Site__c> lstSite=[select id,SVMXC__Account__c from  SVMXC__Site__c where SVMXC__Account__c =:strAccountId ];
         
            for(SVMXC__Site__c varlc: lstSite)
            {
                stLocationId.add(varlc.id);
            }   
        }
        
        
        ObjAcc = [select id,name,BillingStreet,BillingState,BillingCity, BillingPostalCode,BillingCountry, Google_Image__c from account where id =: strAccountId];
       
            
        if(stLocationId != null)
        {
            lstInstalledProduct=new list<SVMXC__Installed_Product__c>();
            lstInstalledProduct=[select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c, License_File__c, 
                                Product_Version_Build__r.Name, SVMXC__Date_Shipped__c, SVMXC__Warranty__c,  SVMXC__Status__c,   
                                SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name from SVMXC__Installed_Product__c 
                                where SVMXC__Site__c IN: stLocationId];
                                
            for(SVMXC__Installed_Product__c varIp:lstInstalledProduct)
            {
                stIP.add(varIp.id);
            }
            
            MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id, SVMXC__Site__c, License_File__c, Product_Version_Build__r.Name, 
                                                                    SVMXC__Date_Shipped__c, SVMXC__Warranty__c, SVMXC__Site__r.name,name, SVMXC__Serial_Lot_Number__c, 
                                                                    SVMXC__Product__r.name, SVMXC__Parent__r.SVMXC__Parent__r.name, SVMXC__Parent__r.SVMXC__Parent__c, 
                                                                    SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name, SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c, 
                                                                    SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c, SVMXC__Parent__r.Name from SVMXC__Installed_Product__c 
                                                                    where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP 
                                                                    OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) 
                                                                    AND SVMXC__Site__c IN:stLocationId]);
                                                                    
            // Iterating for loop to add list of Child Products in a Map corresponding to Parent Product Id's.   

            for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) 
            {  
               //if(pd.SVMXC__Parent__c != null){
                if(MapProductAndLstChildren.get(pd.SVMXC__Parent__c)==null)
                {
                    LstOfProducts=new List<SVMXC__Installed_Product__c>();
                    LstOfProducts.add(pd);
                    MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
                }else
                {               
                    LstOfProducts=MapProductAndLstChildren.get(pd.SVMXC__Parent__c);
                    LstOfProducts.add(pd);
                    MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
                }
              //}
            }
          
            Map<string,list<SVMXC__Installed_Product__c>> mapChld=new Map<string,list<SVMXC__Installed_Product__c>>();
            for(string varStr:stIP)
            {
                LstOfProducts=new List<SVMXC__Installed_Product__c>();
                for(SVMXC__Installed_Product__c pd:MapOfProducts.values())
                {
                    if(varStr == pd.SVMXC__Parent__c)
                    {
                        LstOfProducts.add(pd);
                    }
                }
                mapChld.put(varStr,LstOfProducts);     // Map holding Product-ProductChild data
            }
            for(string varst:stIP)                 // Used to get all child record id which does not occur as root node
            {
                for(SVMXC__Installed_Product__c varStr:mapChld.get(varst))
                {
                    stChildData.add(varStr.id);                          
                }
            }
            System.debug('------MapProductAndLstChildren------------------>'+MapProductAndLstChildren);
        }
       
    }
    
    //Method to create hierarchy of installed products if IP has children = true
    public void GenerateTreeStructureForProducts()
    {
        List<SVMXC__Installed_Product__c> allchildvalues = new List<SVMXC__Installed_Product__c>();
        Set<id> childidset = new set<id>();
        
        for(id ids: MapProductAndLstChildren.keyset())
        {
            allchildvalues.addall(MapProductAndLstChildren.get(ids));
        }
        
        for(SVMXC__Installed_Product__c pd: allchildvalues)
        {
            childidset.add(pd.id);
        }
        
        for(id ids: MapProductAndLstChildren.keyset())
        {
            WrpProductNode mainrec = new WrpProductNode();
            if(!childidset.contains(ids))
            {
                mainrec.prod1 = MapOfProducts.get(ids);
                if(MapProductAndLstChildren.get(ids) != null)
                {
                    mainrec.hasChildren = true;
                    mainrec.prodChildNode = childlist(ids);
                }else
                {
                    mainrec.hasChildren = false;
                }
            }
            
            nodelist.add(mainrec);
        }
        
        system.debug('NODELIST----->' + nodelist);
        
        if(nodelist.size() > 0)
        {
            for(WrpProductNode wparent : nodelist)
            {
                if(wparent.prodChildNode != null)
                {
                    for(WrpProductNode w : wparent.prodChildNode)
                    {
                        stylecolor = stylecolor + 1;
                        if(wparent.prodChildNode.size() > (stylecolor-1)) //Nikita, 5/21/2014, Added List out of bound check, US3365
                        {
                            if(math.MOD(stylecolor,2) != 0 && wparent.prodChildNode[stylecolor-1] != null)
                            {
                                wparent.prodChildNode[stylecolor-1].colorcode = DARKGREY;
                            }   
                            else
                            {
                                if(wparent.prodChildNode[stylecolor-1] != null)
                                {
                                    wparent.prodChildNode[stylecolor-1].colorcode = LIGHTGREY;
                                }   
                            }  
                        }   
                    }
                }
            }
        }
        
    } /* Action method ends here.*/ 
    
    // following method will execute recursively to form the tree structure
    public List<WrpProductNode> childlist(id prodid)
    {
        List<WrpProductNode> childrenlist = new List<WrpProductNode>();
        for(SVMXC__Installed_Product__c ids: MapProductAndLstChildren.get(prodid))
        {
            WrpProductNode childrec = new WrpProductNode();
            childrec.prod1 = MapOfProducts.get(ids.id);
            if(MapProductAndLstChildren.get(ids.id) != null)
            {
                childrec.hasChildren = true;
                childrec.prodChildNode = childlist(ids.id);
            }else
            {
                childrec.hasChildren = false;
            }
          childrenlist.add(childrec);
        }
        return childrenlist;
    }
    
 /*Wrapper class starts here : Below class wraps Products,List of Child Products 
    and boolean variable for childs present for each Parent Product.*/
    public class WrpProductNode
    {        
        public List<WrpProductNode>prodChildNode{get;set;}
        public Boolean hasChildren {get; set;} 
        public SVMXC__Site__c prod{get;set;}    
        public SVMXC__Installed_Product__c prod1{get;set;}
        public string colorcode{get;set;}        
    } /*Wrapper Class ends here*/   
}
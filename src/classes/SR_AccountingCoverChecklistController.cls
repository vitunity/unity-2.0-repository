public with sharing class SR_AccountingCoverChecklistController 
{
    String quoteId {get;set;}
    
    public BigMachines__Quote__c quoteRec {get;set;}
    //public BigMachines__Quote_Product__c QuoteProd {get;set;}
    //public list<BigMachines__Quote_Product__c> QuoteProd1 {get;set;}
    public String sitePartner {get;set;}
    public String soldTo {get;set;}
    public String shipTo {get;set;}
    public String billTo {get;set;}
    
    public SR_AccountingCoverChecklistController()
    {
        quoteId = apexpages.currentpage().getparameters().get('id');
        quoteRec = new BigMachines__Quote__c();
        
        quoteRec = [Select Id,
                           Name,
                           Machine_Header__c,
                           List_Sub_equipment__c,
                           Order_Type__c,
                           SAP_Prebooked_Sales__c,
                           SAP_Prebooked_Service__c,
                           Pre_Book_Order_Number__c,
                           Customer_Name__c,
                           Purchase_Order_Number__c,
                           Contract_Start_Date__c,
                           Contract_End_Date__c,
                           Credits_to_be_issued__c,
                           ListPrice__c,
                           BigMachines__Total__c,
                           Payments_Options__c,
                           Surcharge__c,
                           Prepared_By__c,
                           Prepared_Date__c,
                           Termination_Date__c,
                           Type__c,
                           Contract_Type__c,
                           Notes__c,
                           Addendum_signed_by__c,
                           srv_oci__c,
                           amount_fees_for_the_contract__c,
                           BigMachines__Opportunity__r.Account__r.District_Manager__c,
                           BigMachines__Account__r.Name,
                           DM__c
                           from BigMachines__Quote__c where id =: quoteId];
        
        quoteRec.Customer_Name__c =  quoteRec.BigMachines__Account__r.Name;
      
       
       List<Quote_Product_Partner__c> qotProdPartList = [SELECT Id, 
                                                                 Name, 
                                                                 ERP_Partner_Number__c, 
                                                                 ERP_Partner_Function__c, 
                                                                 Quote__c, ERP_Partner__c, 
                                                                 ERP_Partner__r.Partner_Name_Line_1__c, 
                                                                 ERP_Partner__r.Partner_Name_Line_2__c, 
                                                                 ERP_Partner__r.Partner_Name_Line_3__c, 
                                                                 ERP_Partner__r.City__c,
                                                                 ERP_Partner__r.Country_Code__c,
                                                                 ERP_Partner__r.Zipcode_Postal_Code__c,
                                                                 ERP_Partner__r.Street__c,
                                                                 ERP_Contact_Number__c,
                                                                 ERP_Contact_Function__c,
                                                                 ERP_Partner__r.State_Province_Code__c FROM Quote_Product_Partner__c WHERE  Quote_Product__c =  NULL AND Quote__c =: quoteId]; 
        
    
        for(Quote_Product_Partner__c qpp : qotProdPartList)
        {
            if(qpp.ERP_Partner_Function__c == 'Z1')
            {
                sitePartner = qpp.ERP_Partner_Number__c;
            }             
            else if(qpp.ERP_Partner_Function__c == 'SP')
            {
                soldTo = qpp.ERP_Partner_Number__c;
            }
            else if(qpp.ERP_Partner_Function__c == 'SH')
            {
                shipTo = qpp.ERP_Partner_Number__c;
            }
            else if(qpp.ERP_Partner_Function__c == 'BP')
            {
                billTo = qpp.ERP_Partner_Number__c; 
            }            
        }
    }
    
    public PageReference saveAccountingCoverChecklist()
    {
        update quoteRec;
        system.debug(quoterec+'quoteRec');
        return null;
    }
    
    public PageReference returnToPDFPage()
    {        
        //apexpages.currentpage().getparameters().put('id',quoteRec.Id);
        pagereference pg= new pagereference('/apex/SR_AccountingCoverChecklistPdfPage?Id='+quoteRec.id);
        pg.setredirect(true);
        return Pg;
    }
}
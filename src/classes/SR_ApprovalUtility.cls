public without sharing class SR_ApprovalUtility {
	
	public static Approval.ProcessWorkitemRequest approve(string idval,String Comments){
	                Approval.ProcessWorkitemRequest appPro = new Approval.ProcessWorkitemRequest();
                    appPro.setComments(comments);
                    appPro.setAction('Approve');
                    appPro.setWorkitemId(idval);
                    return appPro;
	}
	
	public static Approval.ProcessWorkitemRequest reject(string idval,String Comments){
		                Approval.ProcessWorkitemRequest appPro = new Approval.ProcessWorkitemRequest();
                        appPro.setComments(Comments);
                        appPro.setAction('Reject');
                        appPro.setWorkitemId(idval);
                        return appPro;
	}
	
	public static Approval.ProcessSubmitRequest newSubmitRequest(Id idval) {
	                    Approval.ProcessSubmitRequest submitForApprovalRequest = new Approval.ProcessSubmitRequest();
                        submitForApprovalRequest.setObjectId(idval);
                        return submitForApprovalRequest;
    }
    public static Approval.LockResult LockRecord(Id idval) {
           Approval.LockResult result = Approval.lock(idval);
           return result;
    }
    
    
	
	public static Boolean unlockRecord(Id idval){
		Approval.UnlockResult result = Approval.unlock(idval);
	    if(!result.isSuccess()) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Approval Process was not Initatiated Successfully');
        	ApexPages.addMessage(myMsg);
        }
		return result.isSuccess();
	}
	
	public static List<Id> getProcessItems(Id idval) {
		List<Id> WorkItemIdList = new List<Id>();
                    for(ProcessInstanceWorkitem workItem  : [Select p.Id from ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId =: idval ORDER BY CreatedDate]) {
                        WorkItemIdList.add(workItem.Id);
                    }
                    return WorkItemIdList;
	}
	
	public static List<Approval.ProcessResult> processRequests(List<Approval.ProcessWorkitemRequest> appProcessList) {
                List<Approval.ProcessResult> results =  Approval.process(appProcessList);
               
                for(Approval.ProcessResult result : results) {
                   
                   if(!result.isSuccess()) {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Approval Process was not Successfully Initiated');
                        ApexPages.addMessage(myMsg);
                    }
                }
                return results;
    }
    
    public static Approval.ProcessResult processSubmit(Approval.ProcessSubmitRequest psr) {
                Approval.ProcessResult result =  Approval.process(psr);

                if(!result.isSuccess()) {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Approval Process was not Initatiated Successfully');
                        ApexPages.addMessage(myMsg);
                }
                
                return result;
    }
	
}
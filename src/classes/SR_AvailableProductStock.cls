/*************************************************************************\
      @ Last Modified By      :  Nikita Gupta
      @ Last Modified On      :  02-Jun-2014
      @ Last Modified Reason  :  Added null checks before all the set, list and map calls.
/****************************************************************************/

public with sharing class SR_AvailableProductStock 
{
	Public String strMessage{get;set;}
	Public boolean blnVisible{get;set;}
	Public list<SVMXC__Product_Stock__c> objProdStk{get;set;} // Property for Object Product stock
	
    public SR_AvailableProductStock(ApexPages.StandardController controller) 
    {
		objProdStk = new list<SVMXC__Product_Stock__c>();
		SVMXC__Site__c objsite = (SVMXC__Site__c)controller.getRecord();
    
		objProdStk=[select id , SVMXC__Product__r.name,SVMXC__Status__c,SVMXC__Available_Qty__c,LastModifiedBy.Name from SVMXC__Product_Stock__c where  SVMXC__Location__c=:objsite.id and SVMXC__Status__c='Available' order by LastModifiedDate DESC limit 1];
  
    }
}
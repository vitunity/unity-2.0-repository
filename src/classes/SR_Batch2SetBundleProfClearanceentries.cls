Global class SR_Batch2SetBundleProfClearanceentries implements Database.Batchable<sObject>,Schedulable
{
    global string query;
    public static Id bundlerecordid = Schema.SObjectType.Review_Products__c.getRecordTypeInfosByName().get('Bundle').getRecordTypeId();
    global Database.QueryLocator start(Database.BatchableContext BC)
    {

        query='SELECT Id,recordtypeId,Bundled_Filter_Logic__c from Review_Products__c where recordtypeId = :bundlerecordid';   
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Review_Products__c> scope) 
    {
        Set<id> BundledProductProfilesids = new set<id>();
        Map<Id,ERP_WBS__c> wbstoupdate = new Map<Id, ERP_WBS__c>();
        Map<id,decimal> childprodprofwithseq = new Map<id,decimal>();
        Map<String,Id> parentClearanceIds = new Map<String,Id>();
        Map<Id,Input_Clearance__c> parentClearanceStatus = new Map<Id,Input_Clearance__c>();
        map<string,string> cnt2logic = new map<string,String>();
        String bundleClearanceLogic = '';
        List<Review_Products__c> childProductProfiles = new List<Review_Products__c>();
        List<BundledProductProfiles.ProductProfileClearance> childclearanceEntries = new List<BundledProductProfiles.ProductProfileClearance>();
        List<Input_Clearance__c> clearanceEntryForms = new list<Input_Clearance__c>();
        Map<Integer,String> productProfileSequence = new Map<Integer,String>();
        map<id,Review_Products__c> bundleprodprofilemap = new Map<Id,Review_Products__c>();
        system.debug('***CM in execute: ');
        system.debug('***Cur page1 : '+Apexpages.Currentpage());
        //BundledProductProfiles bpp1 = new BundledProductProfiles();
        For (Review_Products__c bpp : scope)
        {
            system.debug('***Cur page : '+Apexpages.Currentpage());
            for (Product_Profile_Association__c ppa : [select id,Bundle_Product_Profile__c,Product_Profile_Business_Unit__c,Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c = :bpp.id order by name asc])
            {
                if (!String.isBlank(ppa.product_profile_Business_Unit__c) && ppa.product_profile_Business_Unit__c.containsIgnoreCase('3rd Party'))
                {
                    if(bundleprodprofilemap.size() > 0 && !bundleprodprofilemap.containskey(ppa.Bundle_Product_Profile__c))
                    {
                        Review_Products__c pp = new Review_Products__c();
                        pp.id = ppa.Bundle_Product_Profile__c;
                        //pp.Business_Unit__c = ppa.Product_Profile_Business_Unit__c;
                        bundleprodprofilemap.put(pp.id,pp);
                    }
                    else if (bundleprodprofilemap.size() == 0)
                    {
                        Review_Products__c pp = new Review_Products__c();
                        pp.id = ppa.Bundle_Product_Profile__c;
                        //pp.Business_Unit__c = ppa.Product_Profile_Business_Unit__c;
                        bundleprodprofilemap.put(pp.id,pp);
                    }
                }
                else if (bundleprodprofilemap.size() == 0 || (bundleprodprofilemap.size() > 0 && !bundleprodprofilemap.containsKey(ppa.Bundle_Product_Profile__c)))
                {
                    Review_Products__c pp = new Review_Products__c();
                    pp.id = ppa.Bundle_Product_Profile__c;
                    //pp.Business_Unit__c = ' ';
                    bundleprodprofilemap.put(pp.id,pp);
                }
            }
            for(Input_Clearance__c clearance : [SELECT Id, Country__r.Name, Review_Product__c, Review_Product__r.RecordType.Name, IsProductBundle__c, Clearance_Status__c FROM Input_Clearance__c WHERE Review_Product__c = :bpp.id ])
            {
                //parentClearanceIds.put(clearance.Country__r.Name, clearance.Id);
                parentClearanceStatus.put(clearance.Id, clearance);
            }
            
            
            /* Start - Set IsProductBundle flag */
            Map<Id, Review_Products__c> prodProfileMap;
            Set<Id> ppIdList = new Set<Id>();
            for(Input_Clearance__c obj : parentClearanceStatus.values()){
                if(obj.Review_Product__c != null)
                    ppIdList.add(obj.Review_Product__c);
            }
            
            if(!ppIdList.isEmpty()) {
                prodProfileMap = new Map<Id, Review_Products__c>([Select Id, RecordType.Name from Review_Products__c Where Id in: ppIdList]);
            }
            
            for( Input_Clearance__c obj : parentClearanceStatus.values()){
                if(obj.Review_Product__c != null && prodProfileMap.containsKey(obj.Review_Product__c) && 
                    prodProfileMap.get(obj.Review_Product__c).RecordType.Name=='Bundle' && !obj.IsProductBundle__c) {
                    parentClearanceStatus.get(obj.Id).IsProductBundle__c = true;
                } else if(obj.Review_Product__c != null && prodProfileMap.containsKey(obj.Review_Product__c) && 
                    prodProfileMap.get(obj.Review_Product__c).RecordType.Name != 'Bundle' && obj.IsProductBundle__c) {
                    parentClearanceStatus.get(obj.Id).IsProductBundle__c = false;
                }
            }
            /* End - Set IsProductBundle flag */

            if (bpp.id != null && bpp.Bundled_Filter_Logic__c != null)
            {
                //BundledProductProfiles bpp1 = new BundledProductProfiles(new ApexPages.StandardController(new Review_Products__c(Id = bpp.id)));
                BundledProductProfiles bpp1 = new BundledProductProfiles(bpp.id);
                bpp1.parentProductProfile = bpp;
                bpp1.parentprodprofid = bpp.id;
                bpp1.bundleClearanceLogic = bpp.Bundled_Filter_Logic__c;
                bpp1.batchcode();
                system.debug('***CM childprodprofwithseq : '+bpp1.childprodprofwithseq);
                if(bpp1.childprodprofwithseq.size() > 0)
                {
                    system.debug('***CM inside if cond execute');
                    clearanceEntryForms = [
                    SELECT Id, Clearance_Status__c, Review_Product__r.Name, Review_Product__c, Review_Product__r.Product_Profile_Name__c
                    FROM Input_Clearance__c
                    WHERE Review_Product__c IN: bpp1.childProductProfiles
                    AND Country__c != null
                    ];
                    for(Input_Clearance__c clearanceEntryForm : clearanceEntryForms){
                        childClearanceEntries.add(
                            new BundledProductProfiles.ProductProfileClearance(
                                Integer.valueOf(bpp1.childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)),
                                clearanceEntryForm.Review_Product__r.Name, 
                                clearanceEntryForm.Review_Product__r.Product_Profile_Name__c,
                                clearanceEntryForm.Review_Product__c, 
                                clearanceEntryForm.Clearance_Status__c,
                                clearanceEntryForm.Id
                            )
                        );
                        productProfileSequence.put(Integer.valueOf(bpp1.childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)), clearanceEntryForm.Review_Product__r.Name);
                    }
                }
                system.debug('***CM productProfileSequence: '+productProfileSequence);
                bpp1.childClearanceEntries = childclearanceEntries;
                system.debug('***Child clearance entries : '+bpp1.childClearanceEntries);
                bpp1.productProfileSequence = productProfileSequence;
                system.debug('***CM productProfileSequence: '+bpp1.productProfileSequence);
                bpp1.updateParentClearanceEntriesMaps();
                bpp1.simulate();
                cnt2logic = bpp1.mapcntry2logic;
                system.debug('***CM cnt2logic : '+cnt2logic);
                if (parentClearanceStatus.size() > 0 && cnt2logic.size() > 0)
                {
                    for (id clid : parentClearanceStatus.keySet())
                    {
                        if(parentClearanceStatus.get(clid) != null)
                        {
                            for (string cnt : cnt2logic.keySet())
                            {
                                boolean result = false;
                                if (parentClearanceStatus.get(clid).Country__r.Name == cnt)
                                {
                                    string logic = cnt2logic.get(cnt);
                                    string andop = 'AND';
                                    string orop = 'OR';
                                    string andsfop = '&&';
                                    string orsfop = '||';
                                    string finlog = logic.replace(andop, andsfop);
                                    finlog = finlog.replace(orop,orsfop);
                                    result = booleanexpression.evaluate(finlog);
                                    if (result){
                                        parentClearanceStatus.get(clid).Clearance_Status__c = 'Permitted';
                                    }
                                    else
                                    {
                                        parentClearanceStatus.get(clid).Clearance_Status__c = 'Blocked';
                                    }
                                }
                            }
                        }
                    }
                }
                //bpp1.updateParentClearanceEntries();

            }
        }
        if (parentClearanceStatus.size() > 0)
        {
            database.update(parentClearanceStatus.values());
        }
        if (bundleprodprofilemap.size() > 0)
        {
            system.debug('***CM bundleprodprofilemap: '+bundleprodprofilemap);
            database.update(bundleprodprofilemap.values());
        }
        //bpp1.simulate();
        //bpp1.updateParentClearanceEntries();
        /*
        for (Product_Profile_Association__c ppa : [select id,Standalone_Product_Profile__c, Bundle_Product_Profile__c, Sequence_Number__c from Product_Profile_Association__c where Bundle_Product_Profile__c in :BundledProductProfilesids] )
        {
            childprodprofwithseq.put(ppa.Standalone_Product_Profile__c, ppa.Sequence_Number__c);
        }
        if (childprodprofwithseq.size() > 0)
        {
            childProductProfiles = [SELECT Id, Name, recordtypeid, recordtype.developername FROM Review_Products__c WHERE id in :childprodprofwithseq.keySet()];
        }
        for(Input_Clearance__c clearance : [SELECT Id, Country__r.Name, Clearance_Status__c FROM Input_Clearance__c WHERE Review_Product__c in :BundledProductProfilesids])
        {
            parentClearanceIds.put(clearance.Country__r.Name, clearance.Id);
            parentClearanceStatus.put(clearance.Country__r.Name, clearance.Clearance_Status__c);
        }
        if(childClearanceEntries == null) 
        {
            System.debug('-----selectedCountryId'+selectedCountryId);
            if (selectedCountryId != null && childProductProfiles.size() > 0)
            {
                
                System.debug('-----clearanceEntryForms1'+clearanceEntryForms);
            }
            if(childprodprofwithseq.size() > 0)
            {
                for(Input_Clearance__c clearanceEntryForm : clearanceEntryForms){
                    childClearanceEntries.add(
                        new BundledProductProfiles.ProductProfileClearance(
                            Integer.valueOf(childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)),
                            clearanceEntryForm.Review_Product__r.Name, 
                            clearanceEntryForm.Review_Product__r.Product_Profile_Name__c,
                            clearanceEntryForm.Review_Product__c, 
                            clearanceEntryForm.Clearance_Status__c,
                            clearanceEntryForm.Id
                        )
                    );
                    productProfileSequence.put(Integer.valueOf(childprodprofwithseq.get(clearanceEntryForm.Review_Product__c)), clearanceEntryForm.Review_Product__r.Name);
                }
            }
        }
        */
    }
    global void execute(SchedulableContext SC)
    {        
        SR_Batch2SetBundleProfClearanceentries objcls = new SR_Batch2SetBundleProfClearanceentries();
        database.executebatch(objcls,1);
    }
    global void finish(Database.BatchableContext BC) 
    {
       //if(!Test.isRunningTest())
       //  Database.executeBatch(new SR_Batch2SetBundleProfClearanceentries());
    }
}
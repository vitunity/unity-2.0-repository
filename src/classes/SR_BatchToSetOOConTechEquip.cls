/***************************************************************************
Author: Ritika
Created Date: 11/15/2014
Description: 
Batch class is used to set the OOC flag on the Technician/Equipment US4666 and DE1547

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
06-Nov-2017 - Nilesh Gorle - STSK0013171 - Add field 'SVMXC__Availability_End_Date__c' in 'order by' clause in below query
*************************************************************************************/
    Global class SR_BatchToSetOOConTechEquip implements Database.Batchable<sObject>,Schedulable
    {
        global  string query;    
        Id equipmentRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();
        Id TechnicianRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();//record type id of Technician on Technician/Equipment
        Id equipmentToolsRecTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId(); //RecordTypeId of Equipment/Tools Location.

        global SR_BatchToSetOOConTechEquip() 
        {
            // STSK0013171 - Add field 'SVMXC__Availability_End_Date__c' in 'order by' clause in below query
            query = 'select id, SVMXC__Active__c, RecordTypeId,User__c,Equipment_Status__c, Calibration_Status__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.RecordTypeId, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c, (select id, SVMXC__Availability_End_Date__c from SVMXC__Service_Group_Skills__r order by SVMXC__Availability_End_Date__c Desc limit 1) from SVMXC__Service_Group_Members__c where RecordTypeID =: equipmentRecTypeId';  
                System.debug('inside block');
             //query = 'select id, RecordTypeId,User__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c  from SVMXC__Service_Group_Members__c where (current_end_date__c < Today or ooc__c = true) and RecordTypeID =: equipmentRecTypeId and SVMXC__Inventory_Location__r.SVMXC__Location_Type__c = \'Field\'';   
        }

        global Database.QueryLocator start(Database.BatchableContext BC)
        {
            // STSK0013171 - Add field 'SVMXC__Availability_End_Date__c' in 'order by' clause in below query
            query = 'select id, SVMXC__Active__c, RecordTypeId,User__c,Equipment_Status__c, Calibration_Status__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.RecordTypeId, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c, (select id, SVMXC__Availability_End_Date__c from SVMXC__Service_Group_Skills__r order by SVMXC__Availability_End_Date__c Desc limit 1) from SVMXC__Service_Group_Members__c where RecordTypeID =: equipmentRecTypeId';  
            
             //query = 'select id, RecordTypeId,User__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c  from SVMXC__Service_Group_Members__c where (current_end_date__c < Today or ooc__c = true) and RecordTypeID =: equipmentRecTypeId and SVMXC__Inventory_Location__r.SVMXC__Location_Type__c = \'Field\'';   

            return Database.getQueryLocator(query);
        }

        global void execute(Database.BatchableContext BC, List<SVMXC__Service_Group_Members__c> scope) 
        {
            Map<Id, SVMXC__Service_Group_Members__c> mapUpdateTechEquip = new Map<Id, SVMXC__Service_Group_Members__c>();
            Set<Id> setInvLocation = new Set<Id>(); //set of inventory location id
            Map<Id, SVMXC__Site__c> mapInventoryLoc = new Map<Id, SVMXC__Site__c>(); //map of service engg and inventoryloc
            List<SVMXC__Service_Group_Members__c> listUpdateEquipment = new List<SVMXC__Service_Group_Members__c>();    //DE3045
            Map<id, Boolean> mapInvLoc2OOCeqt = new Map<id, Boolean>(); //DE3045
            system.debug('scope>>>>'+scope);
            set<id> InventoryLocationid = new set<id>();
            set<id> Serviceengineer = new set<id>();
            map<id,id> inventorytechnician = new map<id,id>();
            map<id,Boolean> inventoryIdooc = new map<id,Boolean>();
            list<SVMXC__Service_Group_Members__c> techtoupdate = new List<SVMXC__Service_Group_Members__c>();
            set<string> strDupecheck=new set<string>(); // set to check duplicate
            //added by harshita
            for(SVMXC__Service_Group_Members__c objtech : scope)
            {
                InventoryLocationid.add(objtech.SVMXC__Inventory_Location__c);
                Serviceengineer.add(objtech.SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c);
                System.Debug('***CM OOC__C line 50: '+objtech.OOC__C);
            }
            for(SVMXC__Service_Group_Members__c tech: [Select OOC__c,User__c,SVMXC__Inventory_Location__c,recordtypeid from SVMXC__Service_Group_Members__c where SVMXC__Inventory_Location__c in: InventoryLocationid OR User__c in: Serviceengineer])
            {
                System.Debug('***CM line 54: '+tech);
                if(Serviceengineer != null && tech.User__c != null && Serviceengineer.contains(tech.User__c) && tech.recordtypeid == TechnicianRecTypeId)
                {
                    inventorytechnician.put(tech.SVMXC__Inventory_Location__c,tech.id);
                }
                if(InventoryLocationid != null && InventoryLocationid.contains(tech.SVMXC__Inventory_Location__c) && tech.recordtypeid == equipmentRecTypeId)
                {
                    if(tech.OOC__c)
                    {
                        inventoryIdooc.put(tech.SVMXC__Inventory_Location__c,true);
                    }else{
                        continue;
                    }
                }
            }
            system.debug('@@@@@' + inventoryIdooc);
            for(SVMXC__Service_Group_Members__c objtech : scope)
            {
                if(inventoryIdooc != null && inventoryIdooc.containskey(objtech.SVMXC__Inventory_Location__c))
                {   
                    if(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c)!=null && !strDupecheck.contains(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c)))
                    {
                    SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(id = inventorytechnician.get(objtech.SVMXC__Inventory_Location__c),OOC__c = true);
                    techtoupdate.add(tech);
                    strDupecheck.add(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c));
                    }
                }else if(inventoryIdooc != null && ! inventoryIdooc.containskey(objtech.SVMXC__Inventory_Location__c))
                {
                    if(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c) !=null && !strDupecheck.contains(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c)))
                    {
                    SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(id = inventorytechnician.get(objtech.SVMXC__Inventory_Location__c),OOC__c = false);
                    techtoupdate.add(tech);
                    strDupecheck.add(inventorytechnician.get(objtech.SVMXC__Inventory_Location__c));
                    }
                }
            }
            system.debug('DDD' + techtoupdate);
            if(techtoupdate.size() > 0)
                Update techtoupdate;

            for(SVMXC__Service_Group_Members__c objtech : scope)
            {
                /*commented for us5154
                system.debug('objtech>>>>'+objtech);
                if(objtech.SVMXC__Inventory_Location__c != null)
                {
                    setInvLocation.add(objtech.SVMXC__Inventory_Location__c);
                }
                //DE3045
              
                if(objtech.current_end_date__c < System.Today())
                {
                    objtech.OOC__c = true;
                    mapInvLoc2OOCeqt.put(objtech.SVMXC__Inventory_Location__c, true);
                }
                else
                {
                    objtech.OOC__c = false;
                }
                listUpdateEquipment.add(objtech);
                */
                if(objtech.SVMXC__Service_Group_Skills__r.size()>0)
                {
                    System.Debug('***CM objtech '+objtech);
                    System.Debug('***CM cond2 '+objtech.SVMXC__Active__c);
                    System.Debug('***CM cond3 '+objtech.OOC__c);
                    Date endDate = objtech.SVMXC__Service_Group_Skills__r[0].SVMXC__Availability_End_Date__c;
                    System.Debug('***CM cond1 '+(string.ValueOfGMT(endDate) < String.ValueofGMT(System.today())));
                    if(objtech.SVMXC__Service_Group_Skills__r[0].SVMXC__Availability_End_Date__c < System.today() && objtech.SVMXC__Active__c == true )//&& objtech.OOC__c == false)
                    {
                        System.Debug('***IF Loop');
                        if((objtech.Equipment_Status__c == 'Assigned' || objtech.Equipment_Status__c == 'Available') && objtech.SVMXC__Inventory_Location__c != null && objtech.SVMXC__Inventory_Location__r.RecordTypeId == equipmentToolsRecTypeId && (objtech.SVMXC__Inventory_Location__r.SVMXC__Location_Type__c == 'Field' || objtech.SVMXC__Inventory_Location__r.SVMXC__Location_Type__c == 'Depot')) //&& objtech.Calibration_Status__c == 'Calibrated'
                        {
                            System.debug('inside block');
                            mapUpdateTechEquip.put(objtech.id, new SVMXC__Service_Group_Members__c(id = objtech.id, OOC__c = true, Calibration_Status__c = 'Out Of Calibration'));
                            mapInvLoc2OOCeqt.put(objtech.SVMXC__Inventory_Location__c, true);
                            setInvLocation.add(objtech.SVMXC__Inventory_Location__c);
                        } // Scenerio 1 for us5154
                        
                        if(objtech.Equipment_Status__c == 'Not Available' && objtech.Calibration_Status__c == 'Sent for Calibration' && objtech.SVMXC__Inventory_Location__c != null && objtech.SVMXC__Inventory_Location__r.RecordTypeId == equipmentToolsRecTypeId && objtech.SVMXC__Inventory_Location__r.SVMXC__Location_Type__c == 'Supplier')
                        {
                            mapUpdateTechEquip.put(objtech.id, new SVMXC__Service_Group_Members__c (id = objtech.id, OOC__c = true));
                        } // Scenerio 4 for Us5154
                    }
                }
                
            }
            System.debug('setInvLocation'+setInvLocation);
            if(setInvLocation!=null && setInvLocation.size()>0)
            {
                system.debug('setInvLocation>>>>'+setInvLocation);
               // for(SVMXC__Site__c varInvLoc : [select Id, SVMXC__Location_Type__c, SVMXC__Service_Engineer__c from SVMXC__Site__c where Id in :setInvLocation AND SVMXC__Location_Type__c = 'Field' AND SVMXC__Service_Engineer__c != null AND RecordTypeId = :equipmentToolsRecTypeId])
                for(SVMXC__Site__c varInvLoc : [select Id, SVMXC__Location_Type__c, SVMXC__Service_Engineer__c from SVMXC__Site__c where Id in :setInvLocation AND SVMXC__Service_Engineer__c != null])
                {
                    mapInventoryLoc.put(varInvLoc.SVMXC__Service_Engineer__c, varInvLoc);   //map of service engineer and Inv Location
                }
            }
            
            system.debug('mapInventoryLoc>>>> ' + mapInventoryLoc);
            if(mapInventoryLoc != null && mapInventoryLoc.size() > 0)
            {
                system.debug('inside tech block');
                //record type check added for DE3045
                List<SVMXC__Service_Group_Members__c> listTempTechnician = [select Id, User__c, RecordTypeId, OOC__c, SVMXC__Inventory_Location__c from
                                            SVMXC__Service_Group_Members__c where RecordTypeId = :TechnicianRecTypeId AND User__c in :mapInventoryLoc.keySet()];
                system.debug('listTempTechnician >>>>'+listTempTechnician );

                //changed for DE3045
                if(listTempTechnician != null && listTempTechnician.size() > 0)
                {
                    for(SVMXC__Service_Group_Members__c varTech : listTempTechnician)
                    {
                        system.debug('@@@@@@@Locationid'+varTech.SVMXC__Inventory_Location__c);
                        system.debug('@@@@@@@boolean'+mapInvLoc2OOCeqt.get(varTech.SVMXC__Inventory_Location__c));
                        
                        if(varTech.SVMXC__Inventory_Location__c != null && mapInvLoc2OOCeqt.get(varTech.SVMXC__Inventory_Location__c) != null && varTech.OOC__c == false) //DE7311
                        {
                            mapUpdateTechEquip.put(varTech.id, new SVMXC__Service_Group_Members__c (id = varTech.id, OOC__c = true));
                            system.debug('@@@@@@@3');
                        }
                        
                        /*if(varTech.SVMXC__Inventory_Location__c != null && (!mapInvLoc2OOCeqt.get(varTech.SVMXC__Inventory_Location__c) || mapInvLoc2OOCeqt.get(varTech.SVMXC__Inventory_Location__c) == null)  && varTech.OOC__c == false)
                        {
                            mapUpdateTechEquip.put(varTech.id, new SVMXC__Service_Group_Members__c (id = varTech.id, OOC__c = true));
                        }*/
                        else if(varTech.SVMXC__Inventory_Location__c != null && mapInvLoc2OOCeqt.get(varTech.SVMXC__Inventory_Location__c) != true)
                        {
                            SVMXC__Service_Group_Members__c objUpdateTechEquip  = new SVMXC__Service_Group_Members__c();
                            objUpdateTechEquip.id =  varTech.id;
                            objUpdateTechEquip.ooc__c = false;
                            mapUpdateTechEquip.put(objUpdateTechEquip.Id, objUpdateTechEquip);
                        }
                    }
                }
            }
            /*DE3045
            if(listUpdateEquipment.size() > 0)
            {
                update listUpdateEquipment;
            }*/
            if(mapUpdateTechEquip.size() > 0)
            {
                update mapUpdateTechEquip.values();
            }
        }
        global void execute(SchedulableContext SC)
        {        
            SR_BatchToSetOOConTechEquip objcls = new SR_BatchToSetOOConTechEquip ();
            database.executebatch(objcls);
        }
        global void finish(Database.BatchableContext BC) {}
    }
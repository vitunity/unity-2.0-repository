@isTest
public class SR_BatchToSetOOConTechEquip_test
{
    public static Id equipmentToolRecTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId();
    Static testMethod void createTestData()
    {
        Id equipmentRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();
        Id TechnicianRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();//record type id of Technician on Technician/Equipment
        Id equipmentToolsRecTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId(); //RecordTypeId of Equipment/Tools Location.
        String query = 'select id, SVMXC__Active__c, RecordTypeId,User__c,Equipment_Status__c, Calibration_Status__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.RecordTypeId, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c, (select id, SVMXC__Availability_End_Date__c from SVMXC__Service_Group_Skills__r order by CreatedDate Desc limit 1) from SVMXC__Service_Group_Members__c where RecordTypeID =: equipmentRecTypeId';
        Timecard_Profile__c timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now().addHours(1);
        insert timecardProfile;
        Timecard_Profile__c timecardprof = new Timecard_Profile__c(First_Day_of_Week__c = 'Saturday', Name = 'Test TimeCard Profile');
        timecardprof.Normal_Start_Time__c = datetime.newInstance(2014, 12, 1, 9, 0, 0);
        insert timecardprof;
        SVMXC__Service_Group__c servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        Profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        User serviceUser1 = new User(FirstName ='Test3', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test3@service.com');
        insert serviceUser1;

        User serviceUser2 = new User(FirstName ='Test2', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test2@service.com');
        insert serviceUser2;

        User serviceUser = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test1@service.com', ManagerId = serviceUser1.Id, DelegatedApproverID = serviceUser2.Id);
        insert serviceUser;
        SVMXC__Site__c LocField = new SVMXC__Site__c(name = 'Test loc1', RecordTypeId = equipmentToolRecTypeId, SVMXC__Location_Type__c ='Field', Work_Center__c = 'H162tk', SVMXC__Service_Engineer__c = serviceUser.id);
        insert LocField;
        List<SVMXC__Service_Group_Members__c> teclist = new list<SVMXC__Service_Group_Members__c>();
        for (Integer i=0;i<5;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = equipmentRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField.id;
            technician.Equipment_Status__c = 'Assigned';
            technician.Calibration_Status__c = 'Calibrated';
            technician.SVMXC__Active__c = true;
            //technician.OOC__c = false;
            teclist.add(technician);
        }
        for (Integer i=5;i<10;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = equipmentRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            //technician.SVMXC__Inventory_Location__c = LocField.id;
            technician.Equipment_Status__c = 'Available';
            technician.Calibration_Status__c = 'Calibrated';
            technician.SVMXC__Active__c = true;
            //technician.OOC__c = false;
            teclist.add(technician);
        }
        for (Integer i=10;i<15;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = TechnicianRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField.id;
            technician.OOC__c = false;
            teclist.add(technician);
        }
        for (SVMXC__Service_Group_Members__c tec : teclist){
            System.Debug('***CM OOC__C: '+tec.OOC__C);
        }
        insert teclist; 
        for (SVMXC__Service_Group_Members__c tec : teclist){
            System.Debug('***CM OOC__C: after '+tec.OOC__C);
        }

        String AccTypeInternal = system.label.AccTypeInternal;
        String AccNameVMS = system.label.Varian_Medical_Systems;
        Country__c cntry = new Country__c(Name = 'USA');
        insert cntry;
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = AccNameVMS, Account_Type__c = AccTypeInternal, CurrencyIsoCode='USD', Country1__c = cntry.id, Country__c = 'USA', BillingCountry = 'USA',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
        insert accObj;
        SVMXC__Service_Group__c ServiceTeam1 = new SVMXC__Service_Group__c();
        ServiceTeam1.SVMXC__Active__c= true;
        ServiceTeam1.ERP_Reference__c='TestExternal';
        ServiceTeam1.SVMXC__Group_Code__c='TestExternal';
        ServiceTeam1.Name = 'TestService';
        ServiceTeam1.District_Manager__c = serviceUser.Id;
        Insert ServiceTeam1;

        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.Service_Team__c = ServiceTeam1.Id;
        ServiceTeam.ERP_Reference__c='TestExternal1';
        ServiceTeam.SVMXC__Group_Code__c='TestExternal1';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.District_Manager__c = serviceUser.Id;
        Insert ServiceTeam;
        Schema.DescribeSObjectResult sr = Schema.SObjectType.SVMXC__Service_Group_Skills__c; 
        Map<String,Schema.RecordTypeInfo> skillMapByName = sr.getRecordTypeInfosByName();
        list<SVMXC__Service_Group_Skills__c> explist = new list <SVMXC__Service_Group_Skills__c>();
        for (Integer i=0;i<5;i++){
            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.SVMXC__Group_Member__c = teclist[i].id;
            expertise.Out_of_Tolerance__c = false;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-5;
            expertise.SVMXC__Availability_End_Date__c = system.today()-1;
            expertise.Acknowledge_Date__c = system.today() + 3;
            expertise.Vendor__c = accObj.Id;
            expertise.Certificate_Email__c = '123';
            explist.add(expertise);
        }
        for (Integer i=5;i<10;i++){
            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.SVMXC__Group_Member__c = teclist[i].id;
            expertise.Out_of_Tolerance__c = false;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-5;
            expertise.SVMXC__Availability_End_Date__c = system.today()+10;
            expertise.Acknowledge_Date__c = system.today() + 3;
            expertise.Vendor__c = accObj.Id;
            expertise.Certificate_Email__c = '123';
            explist.add(expertise);
        }

        insert explist;

        /*SVMXC__Service_Group_Skills__c expertise1 = new SVMXC__Service_Group_Skills__c();
        expertise1.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
        expertise1.SVMXC__Service_Group__c = ServiceTeam.id;
        expertise1.SVMXC__Group_Member__c = teclist[1].id;
        expertise1.Out_of_Tolerance__c = false;
        expertise1.Acknowledge_Date__c = system.today()-6;
        expertise1.SVMXC__Availability_Start_Date__c = system.today()-5;
        expertise1.SVMXC__Availability_End_Date__c = system.today()-2;
        expertise1.Acknowledge_Date__c = system.today() + 3;
        expertise1.Vendor__c = accObj.Id;
        expertise1.Certificate_Email__c = '123';
        insert expertise1;

        SVMXC__Service_Group_Skills__c expertise2 = new SVMXC__Service_Group_Skills__c();
        expertise2.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
        expertise2.SVMXC__Service_Group__c = ServiceTeam.id;
        expertise2.SVMXC__Group_Member__c = teclist[2].id;
        expertise2.Out_of_Tolerance__c = false;
        expertise2.Acknowledge_Date__c = system.today()-6;
        expertise2.SVMXC__Availability_Start_Date__c = system.today()-5;
        expertise2.SVMXC__Availability_End_Date__c = system.today()-2;
        expertise2.Acknowledge_Date__c = system.today() + 3;
        expertise2.Vendor__c = accObj.Id;
        expertise2.Certificate_Email__c = '123';
        insert expertise2;*/
        //expertise1.Out_of_Tolerance__c = true;
        //update expertise1;

        Test.startTest();
        SR_BatchToSetOOConTechEquip bat = new SR_BatchToSetOOConTechEquip();
        Database.executeBatch(bat,20);
        Test.stopTest();
    }
    Static testMethod void createTestData1()
    {
        Id equipmentRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();
        Id TechnicianRecTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();//record type id of Technician on Technician/Equipment
        Id equipmentToolsRecTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId(); //RecordTypeId of Equipment/Tools Location.
        String query = 'select id, SVMXC__Active__c, RecordTypeId,User__c,Equipment_Status__c, Calibration_Status__c, SVMXC__Inventory_Location__c, current_end_date__c, OOC__c, SVMXC__Inventory_Location__r.SVMXC__Location_Type__c, SVMXC__Inventory_Location__r.RecordTypeId, SVMXC__Inventory_Location__r.SVMXC__Service_Engineer__c, (select id, SVMXC__Availability_End_Date__c from SVMXC__Service_Group_Skills__r order by CreatedDate Desc limit 1) from SVMXC__Service_Group_Members__c where RecordTypeID =: equipmentRecTypeId';
        Timecard_Profile__c timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now().addHours(1);
        insert timecardProfile;
        Timecard_Profile__c timecardprof = new Timecard_Profile__c(First_Day_of_Week__c = 'Saturday', Name = 'Test TimeCard Profile');
        timecardprof.Normal_Start_Time__c = datetime.newInstance(2014, 12, 1, 9, 0, 0);
        insert timecardprof;
        SVMXC__Service_Group__c servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        Profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        User serviceUser1 = new User(FirstName ='Test3', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test3@service.com');
        insert serviceUser1;

        User serviceUser2 = new User(FirstName ='Test2', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test2@service.com');
        insert serviceUser2;

        User serviceUser = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US',UserName='test1@service.com', ManagerId = serviceUser1.Id, DelegatedApproverID = serviceUser2.Id);
        insert serviceUser;
        SVMXC__Site__c LocField = new SVMXC__Site__c(name = 'Test loc1', RecordTypeId = equipmentToolRecTypeId, SVMXC__Location_Type__c ='Field', Work_Center__c = 'H162tk', SVMXC__Service_Engineer__c = serviceUser.id);
        insert LocField;
        SVMXC__Site__c LocField1 = new SVMXC__Site__c(name = 'Test loc2', RecordTypeId = equipmentToolRecTypeId, SVMXC__Location_Type__c ='Field', Work_Center__c = 'H162tk', SVMXC__Service_Engineer__c = serviceUser2.id);
        insert LocField1;
        List<SVMXC__Service_Group_Members__c> teclist = new list<SVMXC__Service_Group_Members__c>();
        for (Integer i=0;i<5;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = equipmentRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField.id;
            technician.Equipment_Status__c = 'Assigned';
            technician.Calibration_Status__c = 'Calibrated';
            technician.SVMXC__Active__c = true;
            //technician.OOC__c = false;
            teclist.add(technician);
        }
        for (Integer i=5;i<10;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = equipmentRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField1.id;
            technician.Equipment_Status__c = 'Available';
            technician.Calibration_Status__c = 'Calibrated';
            technician.SVMXC__Active__c = true;
            //technician.OOC__c = false;
            teclist.add(technician);
        }
        for (Integer i=10;i<15;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = TechnicianRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField.id;
            technician.OOC__c = false;
            teclist.add(technician);
        }
        for (Integer i=15;i<20;i++){
            SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician ' + i,User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
            technician.RecordTypeId = TechnicianRecTypeId;
            technician.Timecard_Profile__c = timecardProfile.Id;
            technician.SVMXC__Inventory_Location__c = LocField1.id;
            technician.OOC__c = false;
            teclist.add(technician);
        }
        for (SVMXC__Service_Group_Members__c tec : teclist){
            System.Debug('***CM OOC__C: '+tec.OOC__C);
        }
        insert teclist; 
        for (SVMXC__Service_Group_Members__c tec : teclist){
            System.Debug('***CM OOC__C: after '+tec.OOC__C);
        }

        String AccTypeInternal = system.label.AccTypeInternal;
        String AccNameVMS = system.label.Varian_Medical_Systems;
        Country__c cntry = new Country__c(Name = 'USA');
        insert cntry;
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = AccNameVMS, Account_Type__c = AccTypeInternal, CurrencyIsoCode='USD', Country1__c = cntry.id, Country__c = 'USA', BillingCountry = 'USA',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
        insert accObj;
        SVMXC__Service_Group__c ServiceTeam1 = new SVMXC__Service_Group__c();
        ServiceTeam1.SVMXC__Active__c= true;
        ServiceTeam1.ERP_Reference__c='TestExternal';
        ServiceTeam1.SVMXC__Group_Code__c='TestExternal';
        ServiceTeam1.Name = 'TestService';
        ServiceTeam1.District_Manager__c = serviceUser.Id;
        Insert ServiceTeam1;

        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.Service_Team__c = ServiceTeam1.Id;
        ServiceTeam.ERP_Reference__c='TestExternal1';
        ServiceTeam.SVMXC__Group_Code__c='TestExternal1';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.District_Manager__c = serviceUser.Id;
        Insert ServiceTeam;
        Schema.DescribeSObjectResult sr = Schema.SObjectType.SVMXC__Service_Group_Skills__c; 
        Map<String,Schema.RecordTypeInfo> skillMapByName = sr.getRecordTypeInfosByName();
        list<SVMXC__Service_Group_Skills__c> explist = new list <SVMXC__Service_Group_Skills__c>();
        for (Integer i=0;i<10;i++){
            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.SVMXC__Group_Member__c = teclist[i].id;
            expertise.Out_of_Tolerance__c = false;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-5;
            expertise.SVMXC__Availability_End_Date__c = system.today()-1;
            expertise.Acknowledge_Date__c = system.today() + 3;
            expertise.Vendor__c = accObj.Id;
            expertise.Certificate_Email__c = '123';
            explist.add(expertise);
        }
        insert explist;

        /*SVMXC__Service_Group_Skills__c expertise1 = new SVMXC__Service_Group_Skills__c();
        expertise1.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
        expertise1.SVMXC__Service_Group__c = ServiceTeam.id;
        expertise1.SVMXC__Group_Member__c = teclist[1].id;
        expertise1.Out_of_Tolerance__c = false;
        expertise1.Acknowledge_Date__c = system.today()-6;
        expertise1.SVMXC__Availability_Start_Date__c = system.today()-5;
        expertise1.SVMXC__Availability_End_Date__c = system.today()-2;
        expertise1.Acknowledge_Date__c = system.today() + 3;
        expertise1.Vendor__c = accObj.Id;
        expertise1.Certificate_Email__c = '123';
        insert expertise1;

        SVMXC__Service_Group_Skills__c expertise2 = new SVMXC__Service_Group_Skills__c();
        expertise2.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
        expertise2.SVMXC__Service_Group__c = ServiceTeam.id;
        expertise2.SVMXC__Group_Member__c = teclist[2].id;
        expertise2.Out_of_Tolerance__c = false;
        expertise2.Acknowledge_Date__c = system.today()-6;
        expertise2.SVMXC__Availability_Start_Date__c = system.today()-5;
        expertise2.SVMXC__Availability_End_Date__c = system.today()-2;
        expertise2.Acknowledge_Date__c = system.today() + 3;
        expertise2.Vendor__c = accObj.Id;
        expertise2.Certificate_Email__c = '123';
        insert expertise2;*/
        //expertise1.Out_of_Tolerance__c = true;
        //update expertise1;

        Test.startTest();
        SR_BatchToSetOOConTechEquip bat = new SR_BatchToSetOOConTechEquip();
        Database.executeBatch(bat,20);
        Test.stopTest();
    }
}
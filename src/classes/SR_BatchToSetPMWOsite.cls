Global class SR_BatchToSetPMWOsite implements Database.Batchable<sObject>
{
    global string query;
    Id PMWOrectypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
         query='select id,SVMXC__Site__c, Erp_WBS__c, ERP_WBS__r.Site_Partner__c,ERP_WBS__r.Location__c, ERP_WBS__r.Sales_Order_Item__c,ERP_WBS__r.Sales_Order__c from SVMXC__Service_Order__c where recordtypeid = :PMWOrectypeid and SVMXC__Site__c = null and ERP_WBS__c != null';   
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order__c> scope) 
    {
        Map<Id,SVMXC__Service_Order__c> workorderstoupdate = new Map<Id, SVMXC__Service_Order__c>();
        For (SVMXC__Service_Order__c wo : scope)
        {
            if (wo.ERP_WBS__c != null)
            {
                if (wo.ERP_WBS__r.Location__c != null)
                {
                    wo.SVMXC__Site__c = wo.ERP_WBS__r.Location__c;
                    workorderstoupdate.put(wo.id, wo);
                }
            }
        }
        if (workorderstoupdate.size() > 0)
        {
            update workorderstoupdate.values();
        }
    }
    global void finish(Database.BatchableContext BC) 
    {
    }
}
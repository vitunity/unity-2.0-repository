@isTest
public class SR_BatchToSetWBSSite_test
{
    public static list<Sales_Order__c> lstsos;
    public static Account testAcc;
    public static Contact objCont;
    public static sales_Order__c objSalesOrder;
    public static list<Product2> prodlist;
    public static Product2 objProd;
    public static Id pmREcTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId(); 
    public static list<Sales_Order_Item__c> lstSOI;

    public static ERP_Project__c ERPProject_Data(Boolean isInsert, Account newAcc) {
        ERP_Project__c erpProject = new ERP_Project__c();
        erpProject.Name = 'M-0319112526';
        erpProject.Sold_To__c  = newAcc.Id;
        erpProject.Start_Date__c  =  system.today();
        erpProject.End_Date__c  =  system.today().addMonths(3);
        erpProject.Description__c  = '11 / 2008 - BETH ISRAEL MEDICAL CENTER.';
        erpProject.Org__c = '0601';
        erpProject.ERP_PM_Work_Center__c = 'H87803';
        if(isInsert)
            insert erpProject;
        return erpProject;
    }
    public static ERP_WBS__c ERPWBS_Data(Boolean isInsert, ERP_Project__c erpProject) {
        ERP_WBS__c erpWBS = new ERP_WBS__c();
        erpWBS.name = 'M-0319112526-0001-99';
        erpWBS.Status__c = 'REL';
        erpWBS.ERP_Network_Nbr__c = '4001734';
        erpWBS.ERP_Reference__c = 'M-0319112526-0001-99';
        erpWBS.ERP_Project_Nbr__c = 'M-0319112526';
        erpWBS.ERP_PM_Work_Center__c = 'H87696';
        erpWBS.ERP_Project__c = erpProject.Id;
        erpWBS.Acceptance_Date__c = System.today();
        if(isInsert)
            insert erpWBS;
        return erpWBS;
    }
    
    public static ERP_NWA__c ERPNWA_Data(Boolean isInsert, ERP_WBS__c erpWBS, ERP_Project__c erpProject) {
        ERP_NWA__c erpNWA = new ERP_NWA__c();
        erpNWA.Name = '4001734-0020';
        erpNWA.Description__c = 'Project Mgmt Travel & Living Expense';
        erpNWA.ERP_Status__c = 'REL';
        erpNWA.ERP_Project_Nbr__c = 'M-0319112526';
        erpNWA.ERP_WBS_Nbr__c = 'M-0319112526-0001-99';
        erpNWA.ERP_Std_Text_Key__c = 'INST001';
        erpNWA.WBS_Element__c = erpWBS.Id;
        erpNWA.ERP_Project__c = erpProject.Id;
        if(isInsert)
            insert erpNWA;
        return erpNWA;
    }
    public static void createTestData()
    {
            //Create Account
            testAcc = AccountTestData.createAccount();
            testAcc.AccountNumber = '123456';
            insert testAcc;

            lstsos = new list<Sales_Order__c>();
            objSalesOrder=new Sales_Order__c();
            objSalesOrder.Name='001';
            //objSalesOrder.Site_Partner__c = testAcc.id;
            objSalesOrder.Sold_To__c = testAcc.id;
            lstsos.add(objSalesOrder);
            Sales_Order__c objSalesOrder1=new Sales_Order__c();
            objSalesOrder1.Name='001';
            //objSalesOrder1.Site_Partner__c = testAcc.id;
            objSalesOrder1.Sold_To__c = testAcc.id;
            objSalesOrder1.Block__c = '70 cancelled';
            lstsos.add(objSalesOrder1);
            insert lstsos;

            prodlist = new list<Product2>();
            objProd = SR_testdata.createProduct();
            objProd.Name = 'Test Product';
            objProd.Budget_Hrs__c =10.0;
            objProd.Credit_s_Required__c = 3;
            objProd.Material_Group__c = 'H:CREDTS';
            objProd.UOM__c = 'DA';
            objProd.ProductCode = 'ABABAB';
            objprod.course_code__c = 'CREDTS';
            prodlist.add(objProd);
            insert prodlist;

            ERP_Project__c erpProject = ERPProject_Data(true, testAcc);

            ERP_WBS__c erpWBS = ERPWBS_Data(false, erpProject);
            erpWBS.Sales_Order__c = lstsos[0].Id;
            erpwbs.site_partner__c = null;
            //erpWBS.Sales_Order_Item__c = lstsoi[0].id;
            insert erpWBS;
            list<ERP_NWA__c> lstnwa = new list<ERP_NWA__c>();
            ERP_NWA__c erpNWA = new ERP_NWA__c();
            erpNWA.Name = '4001734-0020';
            erpNWA.Description__c = 'Project Mgmt Travel & Living Expense AABB ABABAB AABBCC';
            erpNWA.ERP_Status__c = 'REL';
            erpNWA.ERP_Project_Nbr__c = 'M-0319112526';
            erpNWA.ERP_WBS_Nbr__c = 'M-0319112526-0001-99';
            erpNWA.ERP_Std_Text_Key__c = 'INST001';
            erpNWA.WBS_Element__c = erpWBS.Id;
            erpNWA.ERP_Project__c = erpProject.Id;
            lstnwa.add(erpNWA);
            ERP_NWA__c erpNWA1 = new ERP_NWA__c();
            erpNWA1.Name = '4001736-0030';
            erpNWA1.Description__c = 'Project Mgmt Travel & Living Expense1 AABB ABABAB AABBCC';
            erpNWA1.ERP_Status__c = 'REL';
            erpNWA1.ERP_Project_Nbr__c = 'M-0319112526';
            erpNWA1.ERP_WBS_Nbr__c = 'M-0319112526-0001-99';
            erpNWA1.ERP_Std_Text_Key__c = 'INST001';
            erpNWA1.WBS_Element__c = erpWBS.Id;
            erpNWA1.ERP_Project__c = erpProject.Id;
            lstnwa.add(erpNWA1);
            ERP_NWA__c erpNWA2 = new ERP_NWA__c();
            erpNWA2.Name = '4001735-0040';
            erpNWA2.Description__c = 'Project Mgmt Travel & Living Expense2 AABB ABABAB AABBCC';
            erpNWA2.ERP_Status__c = 'REL';
            erpNWA2.ERP_Project_Nbr__c = 'M-0319112526';
            erpNWA2.ERP_WBS_Nbr__c = 'M-0319112526-0001-99';
            erpNWA2.ERP_Std_Text_Key__c = 'INST001';
            erpNWA2.WBS_Element__c = erpWBS.Id;
            erpNWA2.ERP_Project__c = erpProject.Id;
            lstnwa.add(erpNWA2);
            
            insert lstnwa;

            lstSOI=new list<Sales_Order_Item__c>();
            //parent SOI:
            Sales_Order_Item__c parSOI=new Sales_Order_Item__c();
            parSOI.Start_Date__c= System.Today();
            parSOI.End_Date__c  = System.Today().AddDays(10);
            parSOI.Sales_Order__c = lstsos[0].id;
            //parSOI.Site_Partner__c = testAcc.id;
            parSOI.Product__c = prodlist[0].Id;
            parSOI.ERP_Item_Category__c = 'Z014';
            parSOI.Quantity__c = 20;
            lstSOI.add(parSOI);
            //Create Sales Order Item
            
            Sales_Order_Item__c objSOI1=new Sales_Order_Item__c();
            objSOI1.Start_Date__c= System.Today();
            objSOI1.End_Date__c  = System.Today().AddDays(10);
            objSOI1.Sales_Order__c =lstsos[0].id;
            objSOI1.Site_Partner__c = testAcc.id;
            objSOI1.Product__c = prodlist[0].Id;
            objSOI1.Quantity__c = 10;
            objSOI1.ERP_Item_Category__c = 'Z014';
            //objSOI1.Parent_Item__c = parSOI.id;
            lstSOI.add(objSOI1);
            
            Sales_Order_Item__c objSOI2=new Sales_Order_Item__c();
            objSOI2.Start_Date__c= System.Today().AddDays(-20);
            objSOI2.End_Date__c  = System.Today().AddDays(-10);
            objSOI2.Sales_Order__c =lstsos[0].id;
            objSOI2.Site_Partner__c = testAcc.id;
            objSOI2.Product__c = prodlist[0].Id;
            objSOI2.Quantity__c = 5;
            objSOI2.ERP_Item_Category__c = 'Z018';
            //objSOI2.Parent_Item__c = parSOI.id;
            lstSOI.add(objSOI2);
            insert lstSOI;

            SVMXC__Service_Order__c objWOrkOrder=new SVMXC__Service_Order__c();
            objWOrkOrder.recordtypeID = pmREcTypeId;
            objWOrkOrder.Machine_release_time__c = system.now();
            objWOrkOrder.SVMXC__Order_Status__c = 'Assigned';
            objWOrkOrder.ERP_Top_Level__c = 'H12345';
            objWOrkOrder.SVMXC__Is_PM_Work_Order__c = true;
            objWOrkOrder.SVMXC__Purpose_of_Visit__c = 'Analysis/Troubleshoot';
            objWOrkOrder.PMI_Due_Date__c = system.now();
            objWOrkOrder.Event__c = true;
            objWOrkOrder.SVMXC__Problem_Description__c = 'test';
            objWOrkOrder.ERP_WBS__c = erpWBS.id;
            objWOrkOrder.SVMXC__Priority__c = 'Emergency';          
            objWOrkOrder.Direct_Assignment__c = false;
            objWOrkOrder.Auto_Page__c = true;
            objWOrkOrder.PMI_Earliest_Start_Date__c = system.now();
            objWOrkOrder.SVMXC__Customer_Down__c = false;
            objWOrkOrder.Subject__c = 'asdfghjklzxcvbnm';
            objWOrkOrder.SVMXC__Preferred_Start_Time__c = system.now();
            objWOrkOrder.SVMXC__Preferred_End_Time__c = system.now().addMinutes(240);
            objWOrkOrder.SVMXC__Company__c = testAcc.ID;
            insert objWOrkOrder;

            ERP_WBS__c erpWBS1 = ERPWBS_Data(false, erpProject);
            //erpWBS.Sales_Order__c = lstsos[0].Id;
            erpwbs1.sales_order_item__c = lstsoi[0].id;
            //erpwbs.site_partner__c = null;
            //erpWBS.Sales_Order_Item__c = lstsoi[0].id;
            insert erpWBS1;
    }
    Static testMethod void testMethod1()
    {
        createTestData();
        Test.startTest();
        lstsos[0].Site_Partner__c = testAcc.id;
        lstsos[1].site_partner__c = testAcc.id;
        update lstsos;
        lstsoi[0].site_partner__c = testAcc.id;
        update lstSOI;
        SR_BatchToSetWBSSite bat = new SR_BatchToSetWBSSite();
        Database.executeBatch(bat,200);
        Test.stopTest();
    }
    Static testMethod void testMethod2()
    {
        createTestData();
        Test.startTest();
        SR_BatchToSetPMWOsite bat = new SR_BatchToSetPMWOsite();
        Database.executeBatch(bat,200);
        Test.stopTest();
    }
}
Global class SR_BatchToSetWBSsite implements Database.Batchable<sObject>,Schedulable
{
    global string query;
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
         query='SELECT Id,Location__c,ERP_Functional_location__c,Name,Sales_Order_Item__c,sales_order_Item__r.Site_Partner__c,Sales_Order__c,sales_order__r.Site_Partner__c,Site_Partner__c FROM ERP_WBS__c where site_partner__c = null and ( sales_order__c != null or Sales_Order_Item__c != null )';   
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Erp_WBS__c> scope) 
    {
        Map<Id,ERP_WBS__c> wbstoupdate = new Map<Id, ERP_WBS__c>();
        For (ERP_Wbs__c wbs : scope)
        {
            if (wbs.Sales_Order_Item__c != null)
            {
                if (wbs.sales_order_Item__r.Site_Partner__c != null)
                {
                    wbs.Site_Partner__c = wbs.sales_order_Item__r.Site_Partner__c;
                    wbstoupdate.put(wbs.id, wbs);
                }
            }
            else if(wbs.Sales_Order__c != null)
            {
                if (wbs.sales_order__r.Site_Partner__c != null)
                {
                    wbs.Site_Partner__c = wbs.sales_order__r.Site_Partner__c;
                    wbstoupdate.put(wbs.id, wbs);
                }
            }
        }
        if (wbstoupdate.size() > 0)
        {
            update wbstoupdate.values();
        }
    }
    global void execute(SchedulableContext SC)
    {        
        SR_BatchToSetWBSsite objcls = new SR_BatchToSetWBSsite();
        database.executebatch(objcls);
    }
    global void finish(Database.BatchableContext BC) 
    {
        if(!Test.isRunningTest())
        {
            SR_BatchToSetPMWOsite objcls = new SR_BatchToSetPMWOsite ();
            database.executebatch(objcls);
        }
    }
}
global class SR_BatchToUpdateIranianContacts implements Database.Batchable<sObject>
{
    global string query;
    global Database.QueryLocator start(Database.BatchableContext BC)
    { 
         String country = 'Iran';
         query='SELECT Id, Name, OCSUGC_UserStatus__c , Account.Country__c FROM Contact where Account.Country__c =: country';   
         return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contact> con) 
    {
        List<Contact> contactToUpdate = new list<Contact>();
        For (Contact c : con)
        {
            c.OCSUGC_UserStatus__c = 'OCSUGC Disqualified';
            contactToUpdate.Add(c);
        }
        if (contactToUpdate.size() > 0)
        {
            update contactToUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        
    }
}
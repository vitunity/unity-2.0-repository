@isTest
private class SR_BatchToUpdateIranianContactsTest {

    @testSetup 
    static void setup() {
        Account accnt= new Account();
        accnt.Name= 'TestAccount';
       
        accnt.Country__c= 'Iran';
        accnt.BillingCountry ='USA'; 
        accnt.BillingCity= 'Los Angeles';
        accnt.BillingStreet ='3333 W 2nd St';
        accnt.BillingState ='CA';
        accnt.BillingPostalCode = '90020';
       
        insert accnt;
        
        List<Contact> contacts = new List<Contact>();
        List<Contact> contactsToUpdate = new List<Contact>();
        Contact cont = new Contact();
        cont.FirstName='TestFirstName'+Math.random();
        cont.LastName = 'TestContact' + Math.random();
        cont.Email = 'TestUser' +system.now().millisecond() + '@test.com';
        cont.AccountId = accnt.Id;
        cont.MailingCountry='USA';
        cont.MyVarian_member__c = true;
        cont.PasswordReset__c = false;
        cont.OCSUGC_Notif_Pref_ReqJoinGrp__c = true;
        cont.OCSUGC_Notif_Pref_EditGroup__c = true;
        cont.OCSUGC_Notif_Pref_PrivateMessage__c = true;
        cont.OCSUGC_Notif_Pref_InvtnJoinGrp__c = true;
        cont.OCSUGC_UserStatus__c = 'OCSUGC Approved';
        contacts.add(cont);
        
        Contact cont1 = new Contact();
        cont1.FirstName='TestFirstName'+Math.random();
        cont1.LastName = 'TestContact' + Math.random();
        cont1.Email = 'TestUser' +system.now().millisecond() + '@test.com';
        cont1.AccountId = accnt.Id;
        cont1.MailingCountry='USA';
        cont1.MyVarian_member__c = true;
        cont1.PasswordReset__c = false;
        cont1.OCSUGC_Notif_Pref_ReqJoinGrp__c = true;
        cont1.OCSUGC_Notif_Pref_EditGroup__c = true;
        cont1.OCSUGC_Notif_Pref_PrivateMessage__c = true;
        cont1.OCSUGC_Notif_Pref_InvtnJoinGrp__c = true;
        cont1.OCSUGC_UserStatus__c = 'OCSUGC Approved';
        contacts.add(cont1);
        insert contacts;
        
        
        for (Contact con : contacts) {
            con.OCSUGC_UserStatus__c = 'OCSUGC Disqualified';
            contactsToUpdate.add(con);
        }
        update contactsToUpdate;
    }

    static testmethod void test() {        
        Test.startTest();
        SR_BatchToUpdateIranianContacts uca = new SR_BatchToUpdateIranianContacts();
        Id batchId = Database.executeBatch(uca);
        Test.stopTest();

    }
    
}
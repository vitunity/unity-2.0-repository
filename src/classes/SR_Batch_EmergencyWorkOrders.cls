global class SR_Batch_EmergencyWorkOrders implements Database.Batchable<sobject>, Schedulable
    
    {

       //EmailTemplate  mailtemplate=[select id, name from EmailTemplate  where name = 'SR Escalation Email to DM for Non Assignment'];
       EmailTemplate  mailtemplate=[select id, name from EmailTemplate  where DeveloperName = 'SR_Escalation_mail_to_DM'];
        
           
        global Database.QueryLocator start(Database.BatchableContext BC)
        {
            DateTime scopeStart = System.Now().addMinutes(-90);
            DateTime scopeEnd = System.Now();
            String queryString = 'Select  id, name, Date_Time_for_30_mins_WO__c,SVMXC__Order_Status__c,District_Manager_1__c,District_Manager_Email__c,Escalation_Email_Sent__c from SVMXC__Service_Order__c where Date_Time_for_30_mins_WO__c > :scopeStart and  Date_Time_for_30_mins_WO__c < :scopeEnd';
            System.debug('ESCALATION QUERY = ' + queryString);
            return Database.getQueryLocator(queryString);
        }
        
                
        global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order__c> scope)
        
        {
             list<Messaging.SingleEmailMessage> lstmail = new list<Messaging.SingleEmailMessage>(); 
            list<SVMXC__Service_Order__c> updateEscalatedWO = new list<SVMXC__Service_Order__c>();
            
            for(SVMXC__Service_Order__c WO : scope)
            {
                System.debug('DISTRICT MANAGER USER = ' + WO.District_Manager_1__c);
                System.debug('WORK ORDER ID = ' + WO.Id);
                if(WO.Date_Time_for_30_mins_WO__c < System.Now() && WO.SVMXC__Order_Status__c == 'Open' && wo.Escalation_Email_Sent__c == null)  
                {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {WO.District_Manager_Email__c};


                    mail.whatid=WO.id;
                     //mail.TemplateId = mailtemplate.id;
                    mail.saveAsActivity = false;
                    mail.setTargetObjectId(WO.District_Manager_1__c);
                     mail.setToAddresses(toAddresses);
                    mail.setTemplateId(mailtemplate.id);


                     lstmail.add(mail); 
                    WO.Escalation_Email_Sent__c = system.now();
                    updateEscalatedWO.add(WO);                     
               }
            }
            if(lstmail.size()>0)
            {
                System.debug('MAIL SIZE = ' + lstmail.size());
                try{
                  Messaging.sendEmail(lstmail);
                }catch(exception e)
                {
                    
                }
                update updateEscalatedWO;
            }
                      
        }   
      
         global void execute(SchedulableContext SC)
         {        
            SR_Batch_EmergencyWorkOrders objcls = new SR_Batch_EmergencyWorkOrders ();
            database.executebatch(objcls);
             
        }
        
        global void finish(Database.BatchableContext BC)
      {
        //Build the system time of now + 5 minutes to schedule the batch apex.
        Datetime sysTime = System.now();
        sysTime = sysTime.addMinutes(5);
        String sch = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        SR_Batch_EmergencyWorkOrders emegencyWOsBatch = new SR_Batch_EmergencyWorkOrders ();
        String btchname = 'emegencyWOsBatchEscalationToDM at ' + String.valueOf(sysTime);
        system.schedule(btchname, sch, emegencyWOsBatch);    
    }
        
        
    }
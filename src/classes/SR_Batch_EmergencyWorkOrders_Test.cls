@isTest(seeAllData=true)
private class SR_Batch_EmergencyWorkOrders_Test{
  
    public static testmethod void test() {
        
         // insert territory team
        Territory_Team__c tt = new Territory_Team__c(CSS_District_Manager__c = userinfo.getUserID(),Country__c = 'USA', Zip__c= '12345', CSS_District__c = 'South');
        insert tt;
        
         // insertAccount
        Account acc = new Account(Name = 'TestAprRel',Territory_Team__c= tt.id, OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
         // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', Phone = '1235678', MailingState='CA');
        insert con; 
        
        //insert Service Team
        SVMXC__Service_Group__c objServTeam = new SVMXC__Service_Group__c(Name = 'test team' , District_Manager__c = userinfo.getUserID());
        insert objServTeam;
        
         // insert parent IP
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H140752', SVMXC__Status__c ='Installed' ,Service_Team__c = objServTeam.id);
        insert objIP;
        
         // insert Case
        Case testcase = new case(AccountId =acc.id,Contactid=con.id,Priority='Medium', RecordTypeID =Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(), SVMXC__Top_Level__c = objIP.id );
        Insert testcase ;
       
        
        
        //insert Technician
        SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = objServTeam.ID, User__c = userInfo.getUserID(), SVMXC__Email__c = 'abc@xyz.com');
        
        // insert  Service Order
        SVMXC__Service_Order__c servOrder = new SVMXC__Service_Order__c();
        servOrder.RecordTypeId =Schema.Sobjecttype.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        servOrder.SVMXC__Order_Status__c='Open';
        //servOrder.SVMXC__Case__c =testcase.id;
        //servOrder.SVMXC__Top_Level__c = objIP.id;
        servOrder.Auto_Page__c = false;
        servOrder.Date_Time_for_30_mins_WO__c = System.Now().addMinutes(-20);
        servOrder.District_Manager_1__c = userInfo.getUserID();
        servOrder.District_Manager_Email__c ='test@gmail.com';
       // servOrder.SVMXC__Group_Member__c = objTechEquip.id;
        servOrder.SVMXC__Company__c=acc.id;
        servOrder.SVMXC__Contact__c=con.id;
        servOrder.CurrencyIsoCode='USD';
        insert servOrder;
        /*servOrder.Date_Time_for_30_mins_WO__c  = System.now().addMinutes(-30) ;
        update servOrder;*/
        System.debug('12345678'+servOrder);
        test.startTest();

        SR_Batch_EmergencyWorkOrders BatchEmer = new SR_Batch_EmergencyWorkOrders();
        //database.executeBatch(BatchEmer);
        Datetime sysTime = System.now();
        sysTime = sysTime.addMinutes(5);
        String sch = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();       
        System.schedule('Schedule Test1', sch, BatchEmer);
        Test.StopTest();   
     
                
                
    }
}
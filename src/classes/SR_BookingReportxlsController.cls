/********************************************************************************************************
@Author:Narmada kunamaneni
@Date: Sep 15,2014
@Description: This controller is designed to generate xls for Booking Report
@Last Modified by: 
@Last Modified on: 
@Last modified Reason:
*********************************************************************************************************/

public with sharing class SR_BookingReportxlsController {

    public List<BigMachines__Quote_Product__c> quoteprodList {get;set;}
    public List<Aggregateresult> aggProPricingList {get;set;}
    public BigMachines__Quote__c quoteRec {get;set;}
    
    public List<BigMachines__Quote_Product__c> discountQuoteProducts {get;set;}
    public List<BigMachines__Quote_Product__c> nonDiscountQuoteProducts {get;set;}
        
    public SR_BookingReportxlsController()
    {
        quoteprodList = new List<BigMachines__Quote_Product__c>();
        discountQuoteProducts = new List<BigMachines__Quote_Product__c>();
        nonDiscountQuoteProducts = new List<BigMachines__Quote_Product__c>();
        quoteRec = new BigMachines__Quote__c(); 
        quoteRec = [Select Id, Name, Owner.Name, CurrencyIsoCode,BigMachines__Total_Amount__c,Total_Discountable_Threshold__c,BigMachines__Account__r.Name,BigMachines__Account__r.BillingCity,BigMachines__Account__r.BillingState from BigMachines__Quote__c where id =: apexpages.currentpage().getparameters().get('id')];
          
        discountQuoteProducts = [Select Id,
                                Name,
                                Grp__c,
                                BigMachines__Quote__c,
                                BigMachines__Description__c,
                                BigMachines__Quantity__c,
                                Standard_Price__c,
                                BigMachines__Sales_Price__c,
                                Section_Name__c,
                                BigMachines__Product__r.ProductCode,                                
                                Coverage_Start_Date__c,
                                Coverage_End__c,
                                Parent_PCSN__c,
                                Header__c,
                                service_contract_line__c,
                                Goal_Price__c,
                                Delta_Price__c,
                                Relevant_Non_Relevant__c,
                                Net_Booking_Val__c,
                                Product_Type__c,
                                Booking_Value_VF__c,
                                BigMachines__Product__r.Discountable__c,
                                BigMachines__Product__r.Booking_Discountable__c,
                                BigMachines__Quote__r.Total_Discountable_Threshold__c,
                                Booking_Price_Non_Discountable__c,
                                Threshold_Price__c,
                                BigMachines__Quote__r.BigMachines__Total_Amount__c 
                                from BigMachines__Quote_Product__c where BigMachines__Quote__c =: apexpages.currentpage().getparameters().get('id')
                                AND ((BigMachines__Product__r.Discountable__c = 'FALSE' AND  BigMachines__Product__r.Booking_Discountable__c = 'TRUE') OR BigMachines__Product__r.Discountable__c = 'TRUE')
                                AND Product_Type__c = 'Sales'];
        
        nonDiscountQuoteProducts = [Select Id,
                                Name,
                                Grp__c,
                                BigMachines__Quote__c,
                                BigMachines__Description__c,
                                BigMachines__Quantity__c,
                                Standard_Price__c,
                                BigMachines__Sales_Price__c,
                                Section_Name__c,
                                BigMachines__Product__r.ProductCode,                                
                                Coverage_Start_Date__c,
                                Coverage_End__c,
                                Parent_PCSN__c,
                                Header__c,
                                service_contract_line__c,
                                Goal_Price__c,
                                Delta_Price__c,
                                Relevant_Non_Relevant__c,
                                Net_Booking_Val__c,
                                Product_Type__c,
                                Booking_Value_VF__c,
                                BigMachines__Product__r.Discountable__c,
                                Booking_Price_Non_Discountable__c,
                                BigMachines__Product__r.Booking_Discountable__c,
                                Threshold_Price__c ,
                                BigMachines__Quote__r.Total_Discountable_Threshold__c,
                                BigMachines__Quote__r.BigMachines__Total_Amount__c
                                from BigMachines__Quote_Product__c where BigMachines__Quote__c =: apexpages.currentpage().getparameters().get('id')
                                AND BigMachines__Product__r.Discountable__c = 'FALSE' and  BigMachines__Product__r.Booking_Discountable__c = 'FALSE'
                                AND Product_Type__c = 'Sales'];
        
        setBookingValue();
        setNetBookingValue();
        /*
        
        Set<String> prdNameSet = new Set<String>();
        proPriceWrapperList = new List<ProductPricingClass>();
        Integer secNumber = 1;
        totalStandardPrice = 0;
        totalSalesPrice =0;
        
        for(BigMachines__Quote_Product__c BP : quoteprodList)
        {            
            if(prdNameSet.contains(BP.Section_Name__c)) 
            {
                System.debug('In if condition');
                for(ProductPricingClass QPW : proPriceWrapperList)
                {
                    if(QPW.productName == BP.Section_Name__c)
                    {                           
                        if(QPW.StandardPriceSubTotal ==  null) {QPW.StandardPriceSubTotal = 0;}
                        QPW.standardPriceSubTotal = QPW.standardPriceSubTotal + BP.Standard_Price__c;
                        totalStandardPrice = totalStandardPrice + BP.Standard_Price__c;
                        
                        if(QPW.SalesPriceSubTotal ==  null) {QPW.SalesPriceSubTotal = 0;}
                        QPW.SalesPriceSubTotal = QPW.SalesPriceSubTotal + BP.BigMachines__Sales_Price__c;
                        totalSalesPrice = totalSalesPrice + BP.BigMachines__Sales_Price__c;
                        
                        QPW.quotProList.add(BP);
                    }
                }                       
            }
            else
            {               
                ProductPricingClass PP = new ProductPricingClass();
                PP.setionNumber = secNumber;
                PP.productName = BP.Section_Name__c;
                PP.StandardPriceSubTotal = BP.Standard_Price__c;
                PP.SalesPriceSubTotal = BP.BigMachines__Sales_Price__c;
                PP.quotProPriceList = new List<Quote_Product_Pricing__c>();
                PP.quotProList = new List<BigMachines__Quote_Product__c>();
                PP.quotProPriceWrapList = new List<productPricingSecondWrapper>();
                
                PP.quotProList.add(BP);
                proPriceWrapperList.add(PP);
                
                prdNameSet.add(BP.Section_Name__c);
                secNumber = secNumber + 1;
                totalStandardPrice = totalStandardPrice + BP.Standard_Price__c;
                totalSalesPrice = totalSalesPrice + BP.BigMachines__Sales_Price__c;
            }
        }
        
        List<Quote_Product_Pricing__c> quoteProPriceList = [Select Id,
                                                                   Name,
                                                                   Year__c,
                                                                   Product__c,
                                                                   Product__r.Name,
                                                                   Quote_Product__c,
                                                                   Quote_Product__r.BigMachines__Quantity__c,
                                                                   Quote_Product__r.BigMachines__Description__c,
                                                                   Quote_Product__r.Standard_Price__c,
                                                                   Quote_Product__r.Coverage_Start_Date__c,
                                                                   Quote_Product__r.Coverage_End__c,
                                                                   Quote_Product__r.Section_Name__c,
                                                                   Product__r.ProductCode,
                                                                   BMI_Quote__r.BigMachines__Description__c,
                                                                   Pricing__c
                                                                    from Quote_Product_Pricing__c where BMI_Quote__c =: apexpages.currentpage().getparameters().get('id')];
       
    */  
    /* 
        aggProPricingList = new List<Aggregateresult>();
        // aggProPricingList = [Select Quote_Product__r.Section_Name__c from Quote_Product_Pricing__c where BMI_Quote__c =: apexpages.currentpage().getparameters().get('id') group by Quote_Product__r.Section_Name__c];
        aggProPricingList = [Select Section_Name__c from BigMachines__Quote_Product__c where BigMachines__Quote__c =: apexpages.currentpage().getparameters().get('id') group by Section_Name__c];
        
        
        
        
        
        for(Aggregateresult ar : aggProPricingList)
        {
            ProductPricingClass PP = new ProductPricingClass();
            PP.setionNumber = secNumber;
            PP.quotProPriceList = new List<Quote_Product_Pricing__c>();
            PP.quotProList = new List<BigMachines__Quote_Product__c>();
            PP.quotProPriceWrapList = new List<productPricingSecondWrapper>();
             if(ar.get('Quote_Product__r.Section_Name__c') != null)
            {
                for(Quote_Product_Pricing__c QP : quoteProPriceList)
                {
                    if(ar.get('Quote_Product__r.Section_Name__c') == QP.Quote_Product__r.Section_Name__c)
                    {
                        PP.productName = QP.Quote_Product__r.Section_Name__c;
                        PP.quotProPriceList.add(QP);
                        
                        productPricingSecondWrapper PPWrap = new productPricingSecondWrapper();
                        PPWrap.quoteProPriceRec = QP;
                        PPWrap.yearPriceList = new List<Decimal>();
                        
                        if(QP.Year__c != null && QP.Year__c != '')
                        {
                           for(integer i = 1; i <= Integer.valueOf(QP.Year__c); i++)
                           {
                               PPWrap.yearPriceList.add(QP.Pricing__c);
                           }
                        }
                        PP.quotProPriceWrapList.add(PPWrap);
                    }
                }
            }
            proPriceWrapperList.add(PP);
            
            secNumber = secNumber + 1;
        }
        */
        
        quoteprodList = [Select Id,
                                Name,
                                BigMachines__Quote__c,
                                BigMachines__Description__c,
                                BigMachines__Quantity__c,
                                Standard_Price__c,
                                BigMachines__Sales_Price__c,
                                Section_Name__c,
                                BigMachines__Product__r.ProductCode,                                
                                Coverage_Start_Date__c,
                                Coverage_End__c,
                                Parent_PCSN__c,
                                Header__c,
                                service_contract_line__c,
                                Goal_Price__c,
                                Delta_Price__c,
                                
                                Product_Type__c
                                from BigMachines__Quote_Product__c where BigMachines__Quote__c =: apexpages.currentpage().getparameters().get('id')
                                AND Product_Type__c = 'Services'];
        
        List<Quote_Product_Pricing__c> quoteProPriceList = [Select Id,
                                                                   Name,
                                                                   Year__c,
                                                                   Product__c,
                                                                   Product__r.Name,
                                                                   Quote_Product__c,
                                                                   Quote_Product__r.BigMachines__Quantity__c,
                                                                   Quote_Product__r.BigMachines__Description__c,
                                                                   Quote_Product__r.Standard_Price__c,
                                                                   Quote_Product__r.Coverage_Start_Date__c,
                                                                   Quote_Product__r.Coverage_End__c,
                                                                   Quote_Product__r.Section_Name__c,
                                                                   Product__r.ProductCode,
                                                                   BMI_Quote__r.BigMachines__Description__c,
                                                                   Pricing__c,
                                                                   Parent_PCSN__c,
                                                                   PCSN__c                                                                  
                                                                    from Quote_Product_Pricing__c 
                                                                    where BMI_Quote__c =: apexpages.currentpage().getparameters().get('id')
                                                                    AND (Parent_PCSN__c = '' OR Parent_PCSN__c = null) AND PCSN__c != ''
                                                                    order by Year__c];
        
        
        List<Quote_Product_Pricing__c> InitialQuoteProPriceList = [Select Id,
                                                                   Name,
                                                                   Year__c,
                                                                   Product__c,
                                                                   Product__r.Name,
                                                                   Quote_Product__c,
                                                                   Quote_Product__r.BigMachines__Quantity__c,
                                                                   Quote_Product__r.BigMachines__Description__c,
                                                                   Quote_Product__r.Standard_Price__c,
                                                                   Quote_Product__r.Coverage_Start_Date__c,
                                                                   Quote_Product__r.Coverage_End__c,
                                                                   Quote_Product__r.Section_Name__c,
                                                                   Product__r.ProductCode,
                                                                   BMI_Quote__r.BigMachines__Description__c,
                                                                   Pricing__c,
                                                                   Parent_PCSN__c,
                                                                   PCSN__c
                                                                    from Quote_Product_Pricing__c 
                                                                    where BMI_Quote__c =: apexpages.currentpage().getparameters().get('id')
                                                                    AND (PCSN__c = '' OR PCSN__c = null) AND Parent_PCSN__c != '' order by Year__c];                                                                    
          
         
        quoteWrapperList = new List<QuoteWrapper>();
        Set<String> PCSNList = new Set<String>();
        Set<String> initialPCSNList = new Set<String>();
        Integer secNumber = 1;
        
        boolean isYearAdded = false;
        
        for(BigMachines__Quote_Product__c QP : quoteprodList){
                                    
            if(QP.service_contract_line__c == false && PCSNList.contains(QP.Parent_PCSN__c)){
                
                    for(QuoteWrapper QW : quoteWrapperList){
                        if(QP.Parent_PCSN__c == QW.parentPCSN){
                            QuoteProductClass QPC = new QuoteProductClass();
                            QPC.quoteProductRec = QP;
                            QPC.quoteProductPriceList = new List<Quote_Product_Pricing__c>();
                            if(QW.quoteProductClassList == null) {QW.quoteProductClassList = new List<QuoteProductClass>();}
                            QW.quoteProductClassList.add(QPC);
                    }
                }
                
            }
            else if(QP.service_contract_line__c == false){
                QuoteWrapper QW = new QuoteWrapper();
                QW.parentPCSN = QP.Parent_PCSN__c;
                QW.initialQuotePricList = new List<QuoteProductPriceClass>();
                    
                    //if(!isYearAdded){    
                        for(Quote_Product_Pricing__c IQPP : InitialQuoteProPriceList){
                            if(QP.Parent_PCSN__c == IQPP.Parent_PCSN__c){                                 
                                boolean isYearExist = false;    
                                for(QuoteProductPriceClass IQPP1 : QW.initialQuotePricList){ 
                                    if(IQPP.Name == IQPP1.quoteProductPriceRec.Name){
                                        
                                        if(IQPP1.yearPriceList == null) {IQPP1.yearPriceList = new List<Decimal>();}
                                        IQPP1.yearPriceList.add(IQPP.Pricing__c);
                                        isYearAdded = true;
                                        isYearExist = true;
                                    }    
                                }
                                if(!isYearExist){
                                        QuoteProductPriceClass IQPPRef = new QuoteProductPriceClass();
                                        IQPPRef.quoteProductPriceRec = IQPP;
                                        if(IQPPRef.yearPriceList == null) {IQPPRef.yearPriceList = new List<Decimal>();}
                                        IQPPRef.yearPriceList.add(IQPP.Pricing__c);    
                                        isYearAdded = true;
                                        QW.initialQuotePricList.add(IQPPRef);
                                }
                                
                                initialPCSNList.add(IQPP.Parent_PCSN__c);                                  
                            }
                        }
                    //}

                
                    QuoteProductClass QPC = new QuoteProductClass();
                    QPC.quoteProductRec = QP;
                    QPC.quoteProductPriceList = new List<Quote_Product_Pricing__c>();
                if(QW.quoteProductClassList == null) {QW.quoteProductClassList = new List<QuoteProductClass>();}
                QW.quoteProductClassList.add(QPC);
                quoteWrapperList.add(QW);
                System.debug(' List :: '+QW.quoteProductClassList);
                PCSNList.add(QP.Parent_PCSN__c);
            }
        }
        
        for(BigMachines__Quote_Product__c QP : quoteprodList){          
            if(QP.service_contract_line__c == true && PCSNList.contains(QP.Parent_PCSN__c)){
                
                for(QuoteWrapper QW : quoteWrapperList){
                    if(QP.Parent_PCSN__c == QW.parentPCSN){
                        QuoteProductClass QPC = new QuoteProductClass();
                        QPC.quoteProductRec = QP;
                        
                        QPC.quoteProductPriceList = new List<Quote_Product_Pricing__c>();
                        QPC.quoteProductClassList1 = new List<QuoteProductPriceClass>();
                        
                        for(Quote_Product_Pricing__c IQPP : quoteProPriceList){                             
                            if(QP.Parent_PCSN__c == IQPP.PCSN__c){    
                                boolean isYearExist = false;    
                                for(QuoteProductPriceClass IQPP1 : QPC.quoteProductClassList1){ 
                                    if(IQPP.Name == IQPP1.quoteProductPriceRec.Name){
                                        
                                        if(IQPP1.yearPriceList == null) {IQPP1.yearPriceList = new List<Decimal>();}
                                        IQPP1.yearPriceList.add(IQPP.Pricing__c);                                    
                                        isYearExist = true;
                                    }    
                                }
                                if(!isYearExist){
                                    QuoteProductPriceClass IQPPRef = new QuoteProductPriceClass();
                                    IQPPRef.quoteProductPriceRec = IQPP;
                                    if(IQPPRef.yearPriceList == null) {IQPPRef.yearPriceList = new List<Decimal>();}
                                    IQPPRef.yearPriceList.add(IQPP.Pricing__c);  
                                    
                                    QPC.quoteProductClassList1.add(IQPPRef);
                                }
                            }    
                        }
                        /*
                        for(Quote_Product_Pricing__c QPP : quoteProPriceList ){
                            if(QP.Parent_PCSN__c == QPP.PCSN__c){
                                QPC.quoteProductPriceList.add(QPP);
                            }
                        }       
                        */
                        System.debug(QW.parentPCSN+' Test Product :: '+QPC);
                        QW.quoteProductClassList.add(QPC);
                    }
                }
                
            }
            else if(QP.service_contract_line__c == true){
                QuoteWrapper QW = new QuoteWrapper();
                QW.parentPCSN = QP.Parent_PCSN__c;
                
                QW.initialQuotePricList = new List<QuoteProductPriceClass>();
                    
                    if(!initialPCSNList.contains(QP.Parent_PCSN__c)){    
                        for(Quote_Product_Pricing__c IQPP : InitialQuoteProPriceList){
                            if(QP.Parent_PCSN__c == IQPP.Parent_PCSN__c){
                                 
                                QuoteProductPriceClass IQPPRef = new QuoteProductPriceClass();
                                IQPPRef.quoteProductPriceRec = IQPP;
                                IQPPRef.yearPriceList = new List<Decimal>();
                                
                                QW.initialQuotePricList.add(IQPPRef);
                                
                                initialPCSNList.add(IQPP.Parent_PCSN__c);
                            }
                        }
                    }
                
                    QuoteProductClass QPC = new QuoteProductClass();
                    QPC.quoteProductRec = QP;
                    QPC.quoteProductPriceList = new List<Quote_Product_Pricing__c>();
                    
                    QPC.quoteProductClassList1 = new List<QuoteProductPriceClass>();
                        
                        for(Quote_Product_Pricing__c IQPP : quoteProPriceList){                             
                            if(QP.Parent_PCSN__c == IQPP.PCSN__c){    
                                boolean isYearExist = false;    
                                for(QuoteProductPriceClass IQPP1 : QPC.quoteProductClassList1){ 
                                    if(IQPP.Name == IQPP1.quoteProductPriceRec.Name){
                                        
                                        if(IQPP1.yearPriceList == null) {IQPP1.yearPriceList = new List<Decimal>();}
                                        IQPP1.yearPriceList.add(IQPP.Pricing__c);                                    
                                        isYearExist = true;
                                    }    
                                }
                                if(!isYearExist){
                                    QuoteProductPriceClass IQPPRef = new QuoteProductPriceClass();
                                    IQPPRef.quoteProductPriceRec = IQPP;
                                    if(IQPPRef.yearPriceList == null) {IQPPRef.yearPriceList = new List<Decimal>();}
                                    IQPPRef.yearPriceList.add(IQPP.Pricing__c);  
                                    
                                    QPC.quoteProductClassList1.add(IQPPRef);
                                }
                            }    
                        }    
                
                
                if(QW.quoteProductClassList == null) {QW.quoteProductClassList = new List<QuoteProductClass>();}
                QW.quoteProductClassList.add(QPC);
                quoteWrapperList.add(QW);
                PCSNList.add(QP.Parent_PCSN__c);
            }
        }
        
    }
    
   /**
    * Returns price columns
    */
    public List<String> getPriceTypes(){
        return new List<String>{'Standard Unit', 'Total Standard', 'Goal Unit', 'Total Goal', 
                                'Offer/Unit', 'Total Offer','Delta/unit', 'Total Delta', 
                                 'Booking/Unit','Total Booking', 'Total Net Booking' ,'Threshold/Unit', 'Total Threshold','Difference/Unit','Total Difference'
                                };
    }
    public Map<String,boolean> getVisibleTotals(){
        return new Map<String,boolean>{'Standard Unit' => false, 'Total Standard' =>true, 'Goal Unit' => false, 'Total Goal' => true, 
                                                                          'Offer/Unit' => false, 'Total Offer' => true, 'Delta/unit' => false,'Total Delta' => true, 
                                                                            'Booking/Unit' => false, 'Total Booking' => true,'Total Net Booking' => true ,'Threshold/Unit' => false, 'Total Threshold' => true,'Difference/Unit' => false ,'Total Difference' => true };
    }
   /**
    * This method generates discountable total summary row for CSV
    */
    public Map<String,Double> getDiscountableTotalsMap(){
        Map<String,Double> discountableSummaryMap = new Map<String,Double>{'Standard Unit' => 0.0, 'Total Standard' =>0.0, 'Goal Unit' => 0.0, 'Total Goal' => 0.0, 
                                                                          'Offer/Unit' => 0.0, 'Total Offer' => 0.0, 'Delta/unit' => 0.0,'Total Delta' => 0.0, 
                                                                            'Booking/Unit' => 0.0, 'Total Booking' => 0.0,'Total Net Booking' => 0.0 ,'Threshold/Unit' => 0.0, 'Total Threshold' => 0.0,'Difference/Unit' => 0.0 ,'Total Difference' => 0.0 };
        for(BigMachines__Quote_Product__c quoteProduct : discountQuoteProducts){                                                                        
            for(String priceType : getPriceTypes()){
                Double amount = discountableSummaryMap.get(priceType) + getCalculatedPriceTypeAmount(quoteProduct, priceType);
                discountableSummaryMap.put(priceType,amount);
            }
        }
        
        return discountableSummaryMap;
    }
    
    /**
    * This method generates Non discountable total summary row for CSV
    */
    public Map<String,Double> getNonDiscountableTotalsMap(){
        Map<String,Double> nonDiscountableSummaryMap = new Map<String,Double>{'Standard Unit' => 0.0, 'Total Standard' =>0.0, 'Goal Unit' => 0.0, 'Total Goal' => 0.0,  
                                                                           'Offer/Unit' => 0.0,'Total Offer' => 0.0, 'Delta/unit' => 0.0,'Total Delta' => 0.0,
             'Booking/Unit' => 0.0, 'Total Booking' => 0.0,'Net Booking/Unit' => 0.0,'Total Net Booking' => 0.0 ,'Threshold/Unit' => 0.0, 'Total Threshold' => 0.0,'Difference/Unit' => 0.0,'Total Difference' => 0.0 };
        for(BigMachines__Quote_Product__c quoteProduct : nonDiscountQuoteProducts){                                                                     
            for(String priceType : getPriceTypes()){
                Double amount = nonDiscountableSummaryMap.get(priceType) + getCalculatedPriceTypeAmount(quoteProduct, priceType);
                nonDiscountableSummaryMap.put(priceType,amount);
            }
        }
        
        return nonDiscountableSummaryMap;
    }
    
    /**
     * Calculations for differnrt price types(columns)
     */
     @testVisible
    private Double getCalculatedPriceTypeAmount(BigMachines__Quote_Product__c quoteProduct, String priceType){
        if(priceType == 'Standard Unit'){
            return quoteProduct.Standard_Price__c / quoteProduct.BigMachines__Quantity__c ;
        }else if(priceType == 'Total Standard'){
            return quoteProduct.Standard_Price__c;
        }else if(priceType == 'Goal Unit'){
            return quoteProduct.Goal_Price__c / quoteProduct.BigMachines__Quantity__c ;
        }else if(priceType == 'Total Goal'){
            return quoteProduct.Goal_Price__c;
        }else if(priceType == 'Offer/Unit'){
            return quoteProduct.BigMachines__Sales_Price__c;
        }else if(priceType == 'Total Offer'){
            return quoteProduct.BigMachines__Quantity__c * quoteProduct.BigMachines__Sales_Price__c;
        }else if(priceType == 'Delta/unit'){
            return quoteProduct.BigMachines__Sales_Price__c - (quoteProduct.Threshold_Price__c / quoteProduct.BigMachines__Quantity__c);
        }else if(priceType == 'Total Delta'){
            return (quoteProduct.BigMachines__Sales_Price__c *quoteProduct.BigMachines__Quantity__c) - (quoteProduct.Threshold_Price__c );
        }else if(priceType == 'Booking/Unit'){
            return quoteProduct.Booking_Value_VF__c / quoteProduct.BigMachines__Quantity__c;
        }else if(priceType == 'Total Booking'){
            return quoteProduct.Booking_Value_VF__c;
        }else if(priceType == 'Total Net Booking'&&  quoteProduct.Net_Booking_Val__c != 0){
            return quoteProduct.Net_Booking_Val__c;
        }
        /** Added Threshold values in the report on 11/30/2015 by Narmada **/
        else if(priceType == 'Threshold/Unit'){
            return quoteProduct.Threshold_Price__c / quoteProduct.BigMachines__Quantity__c;
        }else if(priceType == 'Total Threshold'){
            return quoteProduct.Threshold_Price__c ;
        }else if (priceType == 'Difference/Unit'){
            return(quoteProduct.Booking_Value_VF__c / quoteProduct.BigMachines__Quantity__c  - quoteProduct.BigMachines__Sales_Price__c);
        }else if (priceType == 'Total Difference'){
            return (quoteProduct.Booking_Value_VF__c  - (quoteProduct.BigMachines__Sales_Price__c * quoteProduct.BigMachines__Quantity__c));
        }
        
        /** Added new Formula given by Donato(Offer-Threshold) on 11/30/2015 by Narmada
        else if (priceType == 'Total Difference'){
            return (quoteProduct.BigMachines__Quantity__c * quoteProduct.BigMachines__Sales_Price__c)-(quoteProduct.BigMachines__Quantity__c * quoteProduct.Booking_Value_VF__c);
        }
        else if (priceType == 'Difference/Unit'){
            return(quoteProduct.BigMachines__Sales_Price__c - quoteProduct.Booking_Value_VF__c);
        }
        ***/
        else {
            return 0;
        }
        /**
        else{
            return quoteProduct.BigMachines__Quantity__c * quoteProduct.Standard_Price__c;
        } **/ 
    }
    
    public List<QuoteWrapper> quoteWrapperList {get;set;}
    
    public class QuoteWrapper
    {
        public String parentPCSN {get;set;}
        public List<QuoteProductPriceClass> initialQuotePricList {get;set;}
        public List<QuoteProductClass> quoteProductClassList {get;set;}  
        //public Decimal standardPriceSubTotal {get;set;}
        //public Decimal breakdownSubTotal {get;set;}
        //public Decimal offerSubTotal {get;set;}
        //public String productName {get;set;}
    }
    
    public class QuoteProductPriceClass
    { 
        public Quote_Product_Pricing__c quoteProductPriceRec {get;set;}  
        public List<Decimal> yearPriceList {get;set;}        
    }
    
    public class QuoteProductClass
    {
        public BigMachines__Quote_Product__c quoteProductRec {get;set;}
        public List<Quote_Product_Pricing__c> quoteProductPriceList {get;set;} 
        public List<QuoteProductPriceClass> quoteProductClassList1 {get;set;}       
    }
    
    /*
     * Booking Value Calculation
     */
    public void setBookingValue(){
        system.debug('booking value set method invoke');
        Double totalBookingNonDiscount = 0;
        for(BigMachines__Quote_Product__c bqp : nonDiscountQuoteProducts){
           /**
            if(bqp.BigMachines__Sales_Price__c != null ){
                if(bqp.BigMachines__Sales_Price__c  == 0){
                    bqp.Booking_Value_VF__c = bqp.Threshold_Price__c * bqp.BigMachines__Quantity__c;
                }else{
                    if(bqp.BigMachines__Sales_Price__c > 0){
                        bqp.Booking_Value_VF__c = bqp.BigMachines__Sales_Price__c * bqp.BigMachines__Quantity__c;
                    }else{
                        bqp.Booking_Value_VF__c = 0;
                    }
                    
                }               
            }else{
                bqp.Booking_Value_VF__c = 0;
            }**/
            if(bqp.Threshold_Price__c != null)
            {
                bqp.Booking_Value_VF__c = bqp.Threshold_Price__c ;
            }
            totalBookingNonDiscount += bqp.Booking_Value_VF__c;
        }
        
        system.debug('booking value set method invoke discounted');
        
        Decimal totalDiscountableThreshold = 0;
        for(BigMachines__Quote_Product__c bqp : discountQuoteProducts){
            if(bqp.Threshold_Price__c != null){
				totalDiscountableThreshold += (bqp.Threshold_Price__c );
            }
         
        }
        
        system.debug('totalDiscountableThreshold==='+totalDiscountableThreshold);
        for(BigMachines__Quote_Product__c bqp : discountQuoteProducts){
            /**if(bqp.BigMachines__Quote__r.Total_Discountable_Threshold__c == null || bqp.BigMachines__Quote__r.Total_Discountable_Threshold__c ==0 ){
                bqp.Booking_Value_VF__c = 0;
            }else{**/
                
                Decimal val1 = (bqp.Threshold_Price__c )/totalDiscountableThreshold;
                Decimal val2 = bqp.BigMachines__Quote__r.BigMachines__Total_Amount__c - totalBookingNonDiscount;
                Decimal val3 = val1 * val2;
                bqp.Booking_Value_VF__c = val3;
            
                
        }
        
        
    }
    public void setNetBookingValue(){
     for(BigMachines__Quote_Product__c bqp : nonDiscountQuoteProducts){
         if(bqp.Relevant_Non_Relevant__c != null  && bqp.Relevant_Non_Relevant__c == 'not relevant')
                    {
                        bqp.Net_Booking_Val__c = 0;
                    }
         else {bqp.Net_Booking_Val__c = bqp.Booking_Value_VF__c;}
     }
        for(BigMachines__Quote_Product__c bqp : discountQuoteProducts){
          if(bqp.Relevant_Non_Relevant__c != null  && bqp.Relevant_Non_Relevant__c == 'not relevant')
            {
                bqp.Net_Booking_Val__c = 0;
            }  
            else {bqp.Net_Booking_Val__c = bqp.Booking_Value_VF__c ;}
        }
         
    }
}
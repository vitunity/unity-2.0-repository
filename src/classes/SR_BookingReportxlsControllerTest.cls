@isTest(seeAllData=true)
private class SR_BookingReportxlsControllerTest {

    static testMethod void testSR_BookingReportxlsController() {
        
        SR_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        Id quoteId = SR_PrepareOrderControllerTest.combinedQuote.Id;
        
        BigMachines__Quote_Product__c quoteProduct = [Select Id, BigMachines__Product__c,BigMachines__Quote__c 
                                                        From BigMachines__Quote_Product__c
                                                        Where BigMachines__Quote__c =: quoteId
                                                        limit 1];
        
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 serviceProduct = new Product2();
        serviceProduct.Name = 'Sample Service Product';
        serviceProduct.IsActive = true;
        serviceProduct.Discountable__c = 'TRUE';
        
        Product2 salesProduct = new Product2();
        salesProduct.Name = 'Sample Sales Product';
        salesProduct.IsActive = true;
        salesProduct.Discountable__c = 'TRUE';
        
        Product2 salesProduct1 = new Product2();
        salesProduct1.Name = 'Sample Sales Product';
        salesProduct1.IsActive = true;
        salesProduct1.Discountable__c = 'FALSE';
        salesProduct1.Booking_Discountable__c ='FALSE';
        insert new List<Product2> {serviceProduct, salesProduct, salesProduct1};
        
        PricebookEntry pbEntryService = new PricebookEntry();
        pbEntryService.Product2Id = serviceProduct.Id;
        pbEntryService.Pricebook2Id = standardPricebookId;
        pbEntryService.UnitPrice = 111;
        pbEntryService.CurrencyISOCode = 'USD';
        pbEntryService.IsActive = true;
        
        PricebookEntry pbEntryService1 = new PricebookEntry();
        pbEntryService1.Product2Id = salesProduct.Id;
        pbEntryService1.Pricebook2Id = standardPricebookId;
        pbEntryService1.UnitPrice = 111;
        pbEntryService1.CurrencyISOCode = 'USD';
        pbEntryService1.IsActive = true;
        
        PricebookEntry pbEntryService2 = new PricebookEntry();
        pbEntryService2.Product2Id = salesProduct1.Id;
        pbEntryService2.Pricebook2Id = standardPricebookId;
        pbEntryService2.UnitPrice = 111;
        pbEntryService2.CurrencyISOCode = 'USD';
        pbEntryService2.IsActive = true;
        
        insert new List<PricebookEntry>{pbEntryService, pbEntryService1, pbEntryService2};
        
        BigMachines__Quote_Product__c serviceQuoteProduct = getQuoteProduct(quoteId, '12121', true, serviceProduct.Id, 'Services');
        BigMachines__Quote_Product__c serviceQuoteProduct1 = getQuoteProduct(quoteId, '12121', true, serviceProduct.Id, 'Services');
        BigMachines__Quote_Product__c serviceQuoteProduct2 = getQuoteProduct(quoteId, '121212', false, serviceProduct.Id, 'Services');
        BigMachines__Quote_Product__c serviceQuoteProduct3 = getQuoteProduct(quoteId, '121212', false, serviceProduct.Id, 'Services');
        BigMachines__Quote_Product__c salesQuoteProduct = getQuoteProduct(quoteId, '2121', false, salesProduct.Id, 'Sales');
        BigMachines__Quote_Product__c salesQuoteProduct1 = getQuoteProduct(quoteId, '2121', false, salesProduct1.Id, 'Sales');
        
        insert new List<BigMachines__Quote_Product__c>{serviceQuoteProduct, serviceQuoteProduct1, serviceQuoteProduct2, serviceQuoteProduct3, salesQuoteProduct, salesQuoteProduct1};
        
        insert new List<Quote_Product_Pricing__c>{  
                                                    getQuoteProductPricing(serviceQuoteProduct,null,'12121'),
                                                    getQuoteProductPricing(serviceQuoteProduct2,null,'12121'),
                                                    getQuoteProductPricing(serviceQuoteProduct2,null,'12121'),
                                                    getQuoteProductPricing(quoteProduct,null,'121212'),
                                                    getQuoteProductPricing(quoteProduct,null,'121212'),
                                                    getQuoteProductPricing(salesQuoteProduct,'2121',null),
                                                    getQuoteProductPricing(salesQuoteProduct1,'2121',null),
                                                    getQuoteProductPricing(salesQuoteProduct1,'2121',null),
                                                    getQuoteProductPricing(serviceQuoteProduct1,'12121',null),
                                                    getQuoteProductPricing(serviceQuoteProduct1,'12121',null)
                                                };
        
        PageReference bookingPage = Page.SR_BookingReportSales;
        Test.setCurrentPage(bookingPage);
        ApexPAges.currentPage().getParameters().put('id',SR_PrepareOrderControllerTest.combinedQuote.Id);
        
        SR_BookingReportxlsController bookingReportController = new SR_BookingReportxlsController();
        bookingReportController.getPriceTypes();
        bookingReportController.getDiscountableTotalsMap();
        bookingReportController.getNonDiscountableTotalsMap();
    }
    
    
    private static BigMachines__Quote_Product__c getQuoteProduct(Id quoteId, String parentPCSN, Boolean isServiceContractLine, Id productId, String productType){
        BigMachines__Quote_Product__c quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.BigMachines__Quote__c =  quoteId;
        quoteProduct.BigMachines__Product__c = productId;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = productType;
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.Standard_Price__c = 35000;
        quoteProduct.Goal_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.EPOT_section_ID__c = 'ServQuoteProd2';
        quoteProduct.Line_Number__c = 11;
        quoteProduct.Parent_PCSN__c = parentPCSN;
        quoteProduct.service_contract_line__c = isServiceContractLine;
        quoteProduct.Threshold_Price__c = 20000;
        return quoteProduct;
    }
    
    private static Quote_Product_Pricing__c getQuoteProductPricing(BigMachines__Quote_Product__c quoteProduct, String pcsn, String parentPcsn){
        Quote_Product_Pricing__c quoteProductPricing = new Quote_Product_Pricing__c();
        quoteProductPricing.Name = 'TEST';
        quoteProductPricing.Product__c = quoteProduct.BigMachines__Product__c;
        quoteProductPricing.BMI_Quote__c = quoteProduct.BigMachines__Quote__c;
        quoteProductPricing.Quote_Product__c = quoteProduct.Id;
        quoteProductPricing.Parent_PCSN__c = parentPcsn;
        quoteProductPricing.Pricing__c = 100;
        quoteProductPricing.Year__c = '15';
        quoteProductPricing.Coverage_Start_Date__c = Date.today();
        quoteProductPricing.PCSN__c = pcsn;
        return quoteProductPricing;
    }
}
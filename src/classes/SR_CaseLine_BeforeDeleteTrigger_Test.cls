/*
 * Before Delete Trigger test case line
 * author - Amitkumar
 */
@isTest(seealldata=true)
public class SR_CaseLine_BeforeDeleteTrigger_Test {
    
    /* test data members */
    static SVMXC__Service_Group_Members__c technician;
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    static SVMXC__Site__c location;
    static Account acc;
    static SVMXC__Service_Group__c servicegrp;
    static Country__c con;
    static SVMXC__Installed_Product__c objIP;
    static Product2 product;
    static SVMXC__Service_Contract__c servcContact;
    static product2 prodObj;
    /* Initialize test data in static block */
    static{
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        
        technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician',User__c = Userinfo.getUserId(),
                                                         SVMXC__Service_Group__c = servicegrp.id,
                                                         SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321'
                                                        );
        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert technician;
    
        Set<String> objectFields = Schema.SObjectType.Account.fields.getMap().keySet();
        acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States',BillingCity='Pune', BillingState='MH');
        if(objectFields.contains('State_Province_Is_Not_Applicable__c'))
            acc.State_Province_Is_Not_Applicable__c = false;
        insert acc;
        
        location = new SVMXC__Site__c(Sales_Org__c = 'testorg', 
                                      SVMXC__Service_Engineer__c = userInfo.getUserId(), 
                                      SVMXC__Location_Type__c = 'Field', 
                                      Plant__c = 'dfgh', 
                                      SVMXC__Account__c = acc.id);
        insert location;
        
        servcContact = new SVMXC__Service_Contract__c();
        servcContact.Name = 'TestContract';
        servcContact.ERP_Sales_Org__c = 'salesorg';
        servcContact.Ext_Quote_number__c = 'TEST1234';
        servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
        servcContact.SVMXC__End_Date__c = system.today();
        insert servcContact;
       
        prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
        Insert prodObj;
        
        objIP = new  SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = null, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = location.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = acc.id);
        objIP.ERP_Distributor__c = '2009845';
        objIP.ERP_Product_Code__c = '1212';
        objIP.ERP_Reference__c = 'H140752-CLINAC';
        objIP.Billing_Type__c = 'W - Warranty';
        objIP.SVMXC__Service_Contract__c = servcContact.Id;
        insert objIP;
        
        casealias = new Case(Subject = 'testsubject', Priority='High');
        casealias.AccountId = acc.Id;
        casealias.SVMXC__Top_Level__c = objIP.Id;
        casealias.ProductSystem__c = objIP.Id;
        insert casealias;
        
        wo = new SVMXC__Service_Order__c(SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,
                                         SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id,ERP_Service_Order__c = '12345678');
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();
        wo.Malfunction_Start__c = system.now().addDays(10);
        wo.SVMXC__Purpose_of_Visit__c = 'Out of Tolerance';
        wo.SVMXC__Order_Status__c = 'Open';
        insert wo;
        
        
        
        product = new Product2(Name='Connect Utilities', SVMXC__Product_Cost__c  = 20);
        insert product;
        
    }
    
    public static testmethod void unitTest1(){
        System.Test.startTest();
        SVMXC__Case_Line__c  csl = new SVMXC__Case_Line__c ();
        csl.SVMXC__Case__c = casealias.Id;
        csl.Hours__c = 1;
        csl.SVMXC__Installed_Product__c = objIP.Id;
        insert csl;
        
        SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Line_Type__c = 'Parts';
        wd.SVMXC__Actual_Quantity2__c = 1;
        wd.SVMXC__Consumed_From_Location__c = location.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Part_Disposition__c = 'Installed';
        wd.SVMXC__Product__c = product.Id;
        wd.SVMXC__Group_Member__c = technician.id;
        wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.SVMXC__Service_Order__c = wo.id;
        wd.NWA_Description__c = 'nwatest';
        wd.Case_Line__c = csl.Id;
        
        SVMXC__Service_Order_Line__c wd2 = new SVMXC__Service_Order_Line__c();
        wd2.SVMXC__Line_Type__c = 'Parts';
        wd2.SVMXC__Actual_Quantity2__c = 1;
        wd2.SVMXC__Consumed_From_Location__c = location.id;
        wd2.SVMXC__Activity_Type__c = 'Install';
        wd2.Part_Disposition__c = 'Installed';
        wd2.SVMXC__Product__c = product.Id;
        wd2.SVMXC__Group_Member__c = technician.id;
        wd2.Purchase_Order_Number1__c = '12345';
        wd2.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        wd2.SVMXC__Work_Description__c = 'test';
        wd2.SVMXC__Start_Date_and_Time__c =System.now();
        wd2.SVMXC__Service_Order__c = wo.id;
        wd2.NWA_Description__c = 'nwatest';
        wd2.Case_Line__c = csl.Id;
        insert new list<SVMXC__Service_Order_Line__c> {wd,wd2};
        
        delete csl;
            
        System.Test.stopTest();
    }
}
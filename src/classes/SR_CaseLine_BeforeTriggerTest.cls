@isTest(SeeAllData=false)
private class SR_CaseLine_BeforeTriggerTest {
     
    public static Id recTypeIDTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
    static profile pr = [Select id from Profile where name = 'VMS System Admin'];
    public static User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
    public static ERP_Timezone__c etz ;
    public static Account acc ;
    public static SVMXC__Site__c varLoc;
    public static Contact con ;
    public static SVMXC__Service_Group__c objServTeam;
    public static SVMXC__Service_Group_Members__c techEqipt,techEqipt2 ;
    public static SVMXC__Installed_Product__c objIP, objIP2 ;
    public static Case testcase;
    public static SVMXC__Case_Line__c caseline1, caseline2, caseline3;
    static
    {
        
        CountryDateFormat__c cdf = new CountryDateFormat__c(Name='Default',Date_Format__c='dd-MM-YYYY kk:mm');
        insert cdf;
        etz = new ERP_Timezone__c(Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)', name='Aussa');
        insert etz;
        
        // insertAccount
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        //insert location
        varLoc = new SVMXC__Site__c(SVMXC__Account__c = acc.id, Plant__c = 'testPlant', SVMXC__Service_Engineer__c = userInfo.getUserID());
        insert varLoc;

        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con; 
        
        //insert Service Team
        objServTeam = new SVMXC__Service_Group__c(Name = 'test team' , District_Manager__c = userinfo.getUserID());
        insert objServTeam;
        
        //insert technician
        techEqipt = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
        Name = 'Test Technician', User__c = userInfo.getUserId(),ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt;
        
        //insert technician1
        techEqipt2 = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
        Name = 'Test Technician2', User__c = u.id, ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt2;
        System.debug('techEqipt2----->'+techEqipt2);
        
        // insert parent IP var loc
        objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Site__c =varLoc.id, SVMXC__Preferred_Technician__c = techEqipt.id,
        Service_Team__c =objServTeam.Id );
        insert objIP;
        
         // insert parent IP var loc
        objIP2 = new SVMXC__Installed_Product__c(Name='H134072', SVMXC__Status__c ='Installed', SVMXC__Preferred_Technician__c = techEqipt2.id,
        Service_Team__c =objServTeam.Id );
        insert objIP2;
            
        // insert Case
        testcase = new case(ProductSystem__c = objIP.id,recordtypeId= recHD, AccountId =acc.id, Priority='Medium', SVMXC__Top_Level__c = objIP.id,contactId = con.id, SVMXC__Billing_Type__c = 'P - PaidService');
        insert testcase;
        
    }
    
    public static User CreateUser() 
    {
        profile p = new profile();
        p = [Select id from profile where name = 'VMS System Admin'];
        User testUsr = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1];
        testUsr.FirstName = 'testAm';
        testUsr.LastName = 'UserAm';
        testUsr.Alias = 'tstUsrAM';
        testUsr.CommunityNickname= 'tUsr';
        testUsr.Email = 'tstUsraM@abc.com';
        testUsr.Username = 'tstUsraM@abc.com';
        testUsr.TimeZoneSidKey = 'America/Los_Angeles';
        testUsr.LocaleSidKey = 'en_US';
        testUsr.EmailEncodingKey = 'UTF-8';
        testUsr.ProfileId = p.Id;
        testUsr.LanguageLocaleKey  = 'en_US';
        return testUsr;
    }
         
    testmethod static void unitTest1() {
        map<Id,SVMXC__Case_Line__c> caseOldMap = new map<Id,SVMXC__Case_Line__c>();
        list<SVMXC__Case_Line__c> caseLineList = new list<SVMXC__Case_Line__c>();
        System.Test.StartTest();
        User usr = CreateUser();
        caseline1 = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl', SVMXC__Case__c = testcase.id, 
                                            Case_Line_Owner__c = usr.id, Start_Date_time__c = system.now(), 
                                            End_date_time__c = system.now().addHours(1),
                                            SVMXC__Installed_Product__c = objIP.id);
        insert caseline1;
        
        
        caseline1.Start_Date_time__c = system.now().addHours(-4);
        caseline1.End_date_time__c = system.now().addHours(6);
        update caseline1;
        
    }
}
public class SR_CaseWorkOrderTriggerHandler {
    
    /*
     * Copy email dm, foa, sc
     */
    public static void set_FOA_DM_SC_EmailAddress(list<Case_Work_Order__c> newCWOList){
        map<Id, SVMXC__Service_Group_Members__c> technicansMap = new  map<Id, SVMXC__Service_Group_Members__c>();
        set<Id> techniciaIds = new set<Id>();
        for(Case_Work_Order__c cwo : newCWOList){
            if(cwo.Test_Equipment__c != null){
                techniciaIds.add(cwo.Test_Equipment__c);
            }
        }
        
        if(!techniciaIds.isEmpty()){
            technicansMap = new map<Id, SVMXC__Service_Group_Members__c>([select 
                                                                          SVMXC__Service_Group__r.Service_Team__r.FOA__r.Email,
                                                                          SVMXC__Service_Group__r.Service_Team__r.District_Manager__r.Email,
                                                                          SVMXC__Service_Group__r.Service_Team__r.SC__r.Email
                                                                          from SVMXC__Service_Group_Members__c where id in : techniciaIds]);
            for(Case_Work_Order__c cwo : newCWOList){
                if(cwo.Test_Equipment__c != null){
                    SVMXC__Service_Group_Members__c t = technicansMap.get(cwo.Test_Equipment__c);
                    cwo.FOA_Email__c = t.SVMXC__Service_Group__r.Service_Team__r.FOA__r.Email;
                    cwo.District_Manager_Email__c = t.SVMXC__Service_Group__r.Service_Team__r.District_Manager__r.Email;
                    cwo.SC_Email__c = t.SVMXC__Service_Group__r.Service_Team__r.SC__r.Email;
                }
            }
        }
    }
    
    /* Remove approval process */    
    public static void recallCaseApproval(list<Case_Work_Order__c> cwoLIst, map<Id,Case_Work_Order__c> oldMapCWO){
        list<Case_Work_Order__c> cowListQueried = [select Case__r.Number_of_OOT_Case_Work_Orders__c, Resolution__c ,
                                                   Case__r.No_of_OOT_Case_WO_Verified_Completed__c,
                                                   Case__c
                                                   from Case_Work_Order__c
                                                   where Id in : cwoLIst];
        set<Id> caseids = new set<Id>();
        for(Case_Work_Order__c cwo : cowListQueried) {
            if(cwo.Case__r.Number_of_OOT_Case_Work_Orders__c == cwo.Case__r.No_of_OOT_Case_WO_Verified_Completed__c
               && cwo.Resolution__c == 'Open'
               && cwo.Resolution__c != oldMapCWO.get(cwo.Id).Resolution__c){
                  caseids.add(cwo.Case__c); 
               }
        }
        
        List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId 
                                              FROM ProcessInstanceWorkitem 
                                              WHERE ProcessInstance.TargetObjectId =: caseids];
        
        for(ProcessInstanceWorkitem  wId: piwi){
            try{
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments('Recalling and unlocking request.');
                req.setAction('Removed');
                req.setWorkitemId(wId.Id);
                req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
                Approval.ProcessResult result =  Approval.process(req);
            }catch(Exception e){
                
            }
        }
    }
    
    /* Copy technician from work order as cwo owner */
    public static void setCWOOwner(list<Case_Work_Order__c> newCWOList){
        /* Set active user from work order tech as a case work order owner */
        set<id> workOrderIds = new set<id>();
        for(Case_Work_Order__c cwo: newCWOList) {
            if(cwo.Work_Order__c != null){
                workOrderIds.add(cwo.Work_Order__c);
            }
        }
        
        if(!workOrderIds.isEmpty()){
            map<Id, SVMXC__Service_Order__c> woMap = new map<Id, SVMXC__Service_Order__c>([select id, SVMXC__Group_Member__r.User__c,
                                                                                           SVMXC__Group_Member__r.User__r.isActive
                                                                                           from SVMXC__Service_Order__c where id in : workOrderIds]);
            for(Case_Work_Order__c cwo: newCWOList) {
                if(cwo.Work_Order__c!= null){
                    SVMXC__Service_Order__c wo = woMap.get(cwo.Work_Order__c);
                    if(wo!= null && wo.SVMXC__Group_Member__r.User__r.isActive){
                        cwo.Case_Work_Order_Owner__c =  wo.SVMXC__Group_Member__r.User__c;
                    } 
                }
        	}
        }
        
        /* If user inactive or not present set case work order owner = case owner*/
        set<id> caseIds  = new set<id>();
        for(Case_Work_Order__c cwo: newCWOList) {
            if(cwo.Case_Work_Order_Owner__c == null){
                caseIds.add(cwo.Case__c);
            }
            
        }
        if(!caseIds.isEmpty()){
            map<Id, Case> cMap = new map<Id, Case>([select id, OwnerId  
                                                    from Case where id in : caseIds]);
            for(Case_Work_Order__c cwo: newCWOList) {
                if(cwo.Case_Work_Order_Owner__c == null){
                   cwo.Case_Work_Order_Owner__c =  cMap.get(cwo.Case__c).OwnerId;
                }
        	}
        }
    }
    
}
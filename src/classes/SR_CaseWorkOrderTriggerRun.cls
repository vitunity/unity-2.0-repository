public class SR_CaseWorkOrderTriggerRun {
    @TestVisible //Chandra - Added for unit test
	public static boolean firstRun = true;
    
    public static boolean isFirstRun(){
        return firstRun;
    }
    
    public static void setFirstRun(){
        firstRun = false;
    }
    
}
public class SR_Case_Customer_Values 
{
    public String Customer_date{get;set;}
    public String Customer_date_end{get;set;}
    public String Customer_req_strt_date{get;set;}
    public String Customer_req_end_date{get;set;}
    public Case current_case{get;set;}
    public String AMorPM{get;set;}
    public Integer hour_time{get;set;}
    public String hour_timeOfMal{get;set;}
    public String min_time{get;set;}
    public String time_AmPM{get;set;}
    public String hour_timeOfMalStart{get;set;}
    public String min_timeMalSart{get;set;}
    public String time_AmPMMalStart{get;set;}
    public String hour_timeOfReqStart{get;set;}
    public String min_timeReqSart{get;set;}
    public String time_AmPMReqStart{get;set;}
    public String hour_timeOfReqEnd{get;set;}
    public String min_timeReqEnd{get;set;}
    public String time_AmPMReqEnd{get;set;}
    public String errorflag{get;set;}
    public String tabid{get;set;}
    public boolean ShowError{get;set;}

    
    public SR_Case_Customer_Values(ApexPages.StandardController controller) 
    {    ShowError = false;
        //Customer_date = 'this is the date';
        current_case = new case();
        List<String> Split_Malfunction_Start = new list<String>();
        List<string> DateValues = new List<String>();
        List<String> Min1 = new List<String>();
        List<String> Split_Requested_Start = new List<String>();
        List<string> DateVal = new List<String>();
        List<String> Min = new list<String>();
        List<String> Split_Requested_End = new List<String>();
        List<string> DateVa = new List<String>();
        List<String> Min2 = new List<String>();
        errorflag = ApexPages.currentPage().getParameters().get('PrimarytabId');
        tabid = ApexPages.currentPage().getParameters().get('tabid');
        try
        {
            String caseid = ApexPages.currentpage().getparameters().get('id');
            current_case =[Select id,CaseNumber,Customer_Preferred_End_Date_time__c,Customer_Preferred_Start_Date_time__c,Customer_Malfunction_Start_Date_time__c,SVMXC__BW_Date__c,Date_of_Event__c,Date_Complaint_Reviewed__c,Implant_Date__c ,Expiration_Date__c ,Date_Investigated__c,SVMXC__Preferred_End_Time__c,Account.ERP_Timezone__c,Customer_Requested_End_Date_Time__c,Requested_Start_Date_Time__c, Requested_End_Date_Time__c, SVMXC__Preferred_Start_Time__c,Customer_Requested_Start_Date_Time__c,Malfunction_Start__c,SVMXC__Actual_Restoration__c,Account.Country1__r.Timezone__c,Customer_Malfunction_Start_Date_time_Str__c,Customer_Malfunction_End_Date_Time_str__c from case where id =: caseid];
            system.debug('.....................'+current_case);

/************************************************For Malfunction Start*********************************************************/     
           
            if(current_case.Customer_Malfunction_Start_Date_time_Str__c != null)
            {            
                Split_Malfunction_Start =  current_case.Customer_Malfunction_Start_Date_time_Str__c.split(' ');
                if(Split_Malfunction_Start[0].contains('-'))
                {
                    DateValues = Split_Malfunction_Start[0].split('-') ;
                    
                    
                    Min1 = Split_Malfunction_Start[1].split(':');
                    system.debug('Split_Malfunction_Start////////'+Split_Malfunction_Start);
                    current_case.Date_of_Event__c = Date.NewInstance(Integer.valueof(DateValues[2]),Integer.valueof(convertMonthNameToNumber(DateValues[1])),Integer.valueof(DateValues[0]));
                     
                    hour_timeOfMalStart = Min1[0];
                    
                    system.debug('>>>JJJJJJJJJJJJJJJJJJJJJJJ'+hour_timeOfMalStart);
                    if(Split_Malfunction_Start.size()>= 2)
                    {
                        min_timeMalSart = Min1[1];
                        SYSTEM.DEBUG('min_timeMalSart>>>'+min_timeMalSart);
                        SYSTEM.DEBUG('Split_Malfunction_Start[2]'+Split_Malfunction_Start[2]);
                        if(Split_Malfunction_Start[2] == ',' || Split_Malfunction_Start[2] == ' ' || Split_Malfunction_Start[2] == '')
                        {
                            system.debug('in the if part of s2'+Split_Malfunction_Start[2]);
                            //if(S[3]!= ' '  )
                            //{
                                if( Split_Malfunction_Start[3] == 'AM')
                                {
                                    system.debug('in the if part of AM 1'+Split_Malfunction_Start[3]);
                                    time_AmPMMalStart = 'AM';    
                                }
                                if( Split_Malfunction_Start[3] == 'PM')
                                {
                                    system.debug('in the if part of PM 1'+Split_Malfunction_Start[3]);
                                    time_AmPMMalStart = 'PM';
                                }
                            //}S[2] == ' ' || 
                            
                        }
                        else
                        {
                        system.debug('in the else of s2'+Split_Malfunction_Start[2]);
                            if(Split_Malfunction_Start[2] == 'AM')
                            {
                                system.debug('in the if part of AM 2'+Split_Malfunction_Start[2]);
                                time_AmPMMalStart = 'AM';
                            }
                            else
                            {
                                system.debug('in the if part of PM 2'+Split_Malfunction_Start[2]);
                                time_AmPMMalStart = 'PM';
                            }
                        }
                    }
                
                    Customer_date = current_case.Customer_Malfunction_Start_Date_time_Str__c;
                }
            }
            system.debug('current_case.Customer_Requested_Start_Date_Time__c'+current_case.Customer_Requested_Start_Date_Time__c);
            
            
/***********************************************For Requested Start**********************************************************/
            
            if(current_case.Customer_Requested_Start_Date_Time__c!= null)
            {
            system.debug('current_case.Customer_Requested_Start_Date_Time__c'+current_case.Customer_Requested_Start_Date_Time__c);
                
                 Split_Requested_Start =  current_case.Customer_Requested_Start_Date_Time__c.split(' ');
                if(Split_Requested_Start[0].contains('-'))
                {
                     system.debug('Split_Requested_Start............'+Split_Requested_Start);
                     DateVal = Split_Requested_Start[0].split('-') ;
                     system.debug('@@@@' + DateVal);
                     Min = Split_Requested_Start[1].split(':'); 
                    system.debug('Min............'+Min);
                    //current_case.SVMXC__BW_Date__c = date.valueof(current_case.Customer_Preferred_Start_Date_time__c);
                    system.debug('DDDD' + current_case);
                    current_case.Expiration_Date__c= Date.NewInstance(Integer.valueof(DateVal[2]),Integer.valueof(convertMonthNameToNumber(DateVal[1])),Integer.valueof(DateVal[0]));
                    
                       
                        hour_timeOfReqStart = Min[0];
                        system.debug('Min[1]'+Min[1]);
                        min_timeReqSart = Min[1];
                        //system.debug('>>>JJJJJJJJJJJJJJJJJJJJJJJhour_timeOfReqStart'+hour_timeOfReqStart);
                        //system.debug('current_case.Customer_Requested_Start_Date_Time__c.substring(16, 18)'+current_case.Customer_Requested_Start_Date_Time__c.substring(16, 18));
                   if(Split_Requested_Start.size()>= 2)
                    {
                        if(Split_Requested_Start[2] == 'AM')
                        {
                       
                            time_AmPMReqStart = 'AM';
                        }
                        else
                        {
                            time_AmPMReqStart = 'PM';
                        }
                    }
                    
                    Customer_req_strt_date = current_case.Customer_Requested_Start_Date_Time__c;
                }
            }
            system.debug('current_case.Customer_Requested_End_Date_Time__c'+current_case.Customer_Requested_End_Date_Time__c);
            
            
/************************************************For Requested End*********************************************************/
            if(current_case.Customer_Requested_End_Date_Time__c!= null)
            {
                
                 Split_Requested_End =  current_case.Customer_Requested_End_Date_Time__c.split(' ');
                 if(Split_Requested_End[0].contains('-'))
                {
                     system.debug('Split_Requested_End............'+Split_Requested_End);
                     DateVa = Split_Requested_End[0].split('-') ;
                     Min2 = Split_Requested_End[1].split(':');
                    system.debug('Integer.valueof(DateVa[2])'+Integer.valueof(DateVa[2]));
                    current_case.Date_Investigated__c= Date.NewInstance(Integer.valueof(DateVa[2]),Integer.valueof(convertMonthNameToNumber(DateVa[1])),Integer.valueof(DateVa[0]));
                     
                    hour_timeOfReqEnd = Min2[0];
                    
                        min_timeReqEnd = Min2[1];
                        system.debug('>>>JJJJJJJJJJJJJJJJJJJJJJJmin_timeReqEnd'+min_timeReqEnd);
                        system.debug('Split_Requested_End>>>>>>>'+Split_Requested_End);
                    if(Split_Requested_End.size()>= 2)
                    {
                        if(Split_Requested_End[2] == 'AM')
                        {
                            time_AmPMReqEnd = 'AM';
                        }
                        else
                        {
                            time_AmPMReqEnd = 'PM';
                        }
                    
                    }
                
                    
                    Customer_req_end_date = current_case.Customer_Requested_End_Date_Time__c;
                }
            }
            
/*********************************************************************************************************/
            system.debug('current_case.SVMXC__BW_Date__c'+current_case.Expiration_Date__c+'current_case.Customer_Preferred_Start_Date_time__c'+current_case.Customer_Preferred_Start_Date_time__c);
           
            
            
        }catch(Exception e)
        {
            ShowError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()));
            //return null;
        }
    }
    
/*********************************************For Hour picklist************************************************************/
            public List<SelectOption> getHour() {
                List<SelectOption> options3 = new List<SelectOption>();
                options3.add(new SelectOption('hour','hour'));
                options3.add(new SelectOption('01','01'));
                options3.add(new SelectOption('02','02'));
                options3.add(new SelectOption('03','03'));
                options3.add(new SelectOption('04','04'));
                options3.add(new SelectOption('05','05'));
                options3.add(new SelectOption('06','06'));
                options3.add(new SelectOption('07','07'));
                options3.add(new SelectOption('08','08'));
                options3.add(new SelectOption('09','09'));
                options3.add(new SelectOption('10','10'));
                options3.add(new SelectOption('11','11'));
                options3.add(new SelectOption('12','12'));
                return options3;
            }
            
/*********************************************For Minutes picklist************************************************************/            
            
            public List<SelectOption> getMin() {
                List<SelectOption> options4 = new List<SelectOption>();
                options4.add(new SelectOption('min','min'));
                options4.add(new SelectOption('00','00'));
                options4.add(new SelectOption('01','01'));
                options4.add(new SelectOption('02','02'));
                options4.add(new SelectOption('03','03'));
                options4.add(new SelectOption('04','04'));
                options4.add(new SelectOption('05','05'));
                options4.add(new SelectOption('06','06'));
                options4.add(new SelectOption('07','07'));
                options4.add(new SelectOption('08','08'));
                options4.add(new SelectOption('09','09'));
                options4.add(new SelectOption('10','10'));
                options4.add(new SelectOption('11','11'));
                options4.add(new SelectOption('12','12'));
                options4.add(new SelectOption('13','13'));
                options4.add(new SelectOption('14','14'));
                options4.add(new SelectOption('15','15'));
                options4.add(new SelectOption('16','16'));
                options4.add(new SelectOption('17','17'));
                options4.add(new SelectOption('18','18'));
                options4.add(new SelectOption('19','19'));
                options4.add(new SelectOption('20','20'));
                options4.add(new SelectOption('21','21'));
                options4.add(new SelectOption('22','22'));
                options4.add(new SelectOption('23','23'));
                options4.add(new SelectOption('24','24'));
                options4.add(new SelectOption('25','25'));
                options4.add(new SelectOption('26','26'));
                options4.add(new SelectOption('27','27'));
                options4.add(new SelectOption('28','28'));
                options4.add(new SelectOption('29','29'));
                options4.add(new SelectOption('30','30'));
                options4.add(new SelectOption('31','31'));
                options4.add(new SelectOption('32','32'));
                options4.add(new SelectOption('33','33'));
                options4.add(new SelectOption('34','34'));
                options4.add(new SelectOption('35','35'));
                options4.add(new SelectOption('36','36'));
                options4.add(new SelectOption('37','37'));
                options4.add(new SelectOption('38','38'));
                options4.add(new SelectOption('39','39'));
                options4.add(new SelectOption('40','40'));
                options4.add(new SelectOption('41','41'));
                options4.add(new SelectOption('42','42'));
                options4.add(new SelectOption('43','43'));
                options4.add(new SelectOption('44','44'));
                options4.add(new SelectOption('45','45'));
                options4.add(new SelectOption('46','46'));
                options4.add(new SelectOption('47','47'));
                options4.add(new SelectOption('48','48'));
                options4.add(new SelectOption('49','49'));
                options4.add(new SelectOption('50','50'));
                options4.add(new SelectOption('51','51'));
                options4.add(new SelectOption('52','52'));
                options4.add(new SelectOption('53','53'));
                options4.add(new SelectOption('54','54'));
                options4.add(new SelectOption('55','55'));
                options4.add(new SelectOption('56','56'));
                options4.add(new SelectOption('57','57'));
                options4.add(new SelectOption('58','58'));
                options4.add(new SelectOption('59','59'));
                return options4;
            }
            
/*********************************************For AM And PM picklist************************************************************/             
            
            public List<SelectOption> getAmPM() {
                List<SelectOption> options5 = new List<SelectOption>();
                options5.add(new SelectOption('AM','AM'));
                options5.add(new SelectOption('PM','PM'));
                return options5;
            }

        
   
/*********************************************For Save Method********************************************************************/    
      public pagereference saveDates()
      {     
        try
        {
            ShowError = false;
            system.debug('//////'+hour_timeOfMalStart);
            IF(hour_timeOfMalStart == 'hour' || min_timeMalSart == 'min' || hour_timeOfReqStart == 'hour' || min_timeReqSart == 'min' || hour_timeOfReqEnd =='hour' || min_timeReqEnd == 'min')
            {   ShowError = true;
                system.debug('In if hour_timeOfMalStart'+hour_timeOfMalStart);
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Please enter the hour and minute values');
                apexPages.addMessage(msg);
               
                return null; 
                
            }
            Case updatecase = new case(id = current_case.id);
/*********************************************For Malfunction Start********************************************************************/            
            system.debug('current_case.Date_of_Event__c'+current_case.Date_Investigated__c);
            IF(current_case.Date_of_Event__c!=null && hour_timeOfMalStart!=null && min_timeMalSart !=null && time_AmPMMalStart!=null)
            {   
                //updatecase.Customer_Malfunction_Start_Date_time_Str__c = current_case.Date_of_Event__c.format() +' '+ + hour_timeOfMalStart+':'+ min_timeMalSart +' '+ time_AmPMMalStart;
                //updatecase.Customer_Malfunction_Start_Date_time_Str__c = current_case.Date_of_Event__c.month() + '/' + current_case.Date_of_Event__c.day() + '/' + current_case.Date_of_Event__c.year() +' '+ + hour_timeOfMalStart+':'+ min_timeMalSart +' '+ time_AmPMMalStart;
                updatecase.Customer_Malfunction_Start_Date_time_Str__c = current_case.Date_of_Event__c.day() + '-' + convertMonthIntNumberToName(current_case.Date_of_Event__c.month()) + '-' + current_case.Date_of_Event__c.year() +' '+ + hour_timeOfMalStart+':'+ min_timeMalSart +' '+ time_AmPMMalStart;
                system.debug('updatecase.Customer_Malfunction_Start_Date_time_Str__c>>>>>>>>>>>>>>>>>>>>'+updatecase.Customer_Malfunction_Start_Date_time_Str__c);  
            }
/*********************************************For Requested Start********************************************************************/            

            IF(current_case.Expiration_Date__c!=null && hour_timeOfReqStart!=null && min_timeReqSart !=null && time_AmPMReqStart!=null)
            {
                //updatecase.Customer_Requested_Start_Date_Time__c = current_case.Expiration_Date__c.format() +' '+  + hour_timeOfReqStart +':'+ min_timeReqSart+' '+ time_AmPMReqStart;
                //updatecase.Customer_Requested_Start_Date_Time__c = current_case.Expiration_Date__c.month() + '/' + current_case.Expiration_Date__c.day() + '/' + current_case.Expiration_Date__c.year() +' '+  + hour_timeOfReqStart +':'+ min_timeReqSart+' '+ time_AmPMReqStart;
                updatecase.Customer_Requested_Start_Date_Time__c = current_case.Expiration_Date__c.day() + '-' + convertMonthIntNumberToName(current_case.Expiration_Date__c.month()) + '-' + current_case.Expiration_Date__c.year() +' '+  + hour_timeOfReqStart +':'+ min_timeReqSart+' '+ time_AmPMReqStart;
                system.debug('updatecase.Customer_Requested_Start_Date_Time__c>>>>>>>>>>>>>>>>>>>>'+updatecase.Customer_Requested_Start_Date_Time__c); 
            }
/*********************************************For Requested End********************************************************************/            

            IF(current_case.Date_Investigated__c!=null && hour_timeOfReqEnd!=null && min_timeReqEnd !=null && time_AmPMReqEnd!=null)
            {
                //updatecase.Customer_Requested_End_Date_Time__c = current_case.Date_Investigated__c.format() +' '+ + hour_timeOfReqEnd +':'+ min_timeReqEnd +' '+ time_AmPMReqEnd;
                //updatecase.Customer_Requested_End_Date_Time__c = current_case.Date_Investigated__c.month() + '/' + current_case.Date_Investigated__c.day() + '/' + current_case.Date_Investigated__c.year() +' '+ + hour_timeOfReqEnd +':'+ min_timeReqEnd +' '+ time_AmPMReqEnd;
                updatecase.Customer_Requested_End_Date_Time__c = current_case.Date_Investigated__c.day() + '-' + convertMonthIntNumberToName(current_case.Date_Investigated__c.month()) + '-' + current_case.Date_Investigated__c.year() +' '+ + hour_timeOfReqEnd +':'+ min_timeReqEnd +' '+ time_AmPMReqEnd;
                system.debug('updatecase.Customer_Requested_End_Date_Time__c>>>>>>>>>>>>>>>>>>>>'+updatecase.Customer_Requested_End_Date_Time__c);               
            }
            
            update updatecase;
        }
        catch(Exception e)
        {
            String errMsg = e.getMessage();
            ShowError = false;   
            if(errMsg.Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
            {               
                string customValid_begin = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
                integer intIndexOfErr = errMsg.indexOf(customValid_begin) + 35;
                string customValid_end = errMsg.substring(intIndexOfErr);
                integer intIndexOfErrEnd;
                if(customValid_end.contains(':'))
                    intIndexOfErrEnd = customValid_end.indexOf(':');
                
                ShowError = true;
                if(intIndexOfErrEnd != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg.substring(intIndexOfErr, (intIndexOfErr +intIndexOfErrEnd))));
                else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg.substring(intIndexOfErr)));
            }
            else
            {
                ShowError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The following exception has occurred: ' + errMsg));
            }
            return null;
        }
        
        return null;
        
    }
   
    /**
        create date time instance from datetime string in user timezone
    */
    public DateTime constructDateTime(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = dateTimeSplitList[0].split('/') ;
        List<String> timeSplit = dateTimeSplitList[1].split(':');
        String strAMPM = dateTimeSplitList[2];
        DateTime constructedDateTime;
        if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
        {
            constructedDateTime = dateTime.NewInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),00, Integer.valueof(timeSplit[1]), 00);
        }
        else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
        {
            constructedDateTime = dateTime.NewInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
        }
        else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
        {
            constructedDateTime = dateTime.NewInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
        }
        
        return constructedDateTime;
    }
    
    /**
        Create GMT instance from datetime string, when set on field, should show teh right date/time when user timezone is GMT, user has to change time zone to GMT to see right values
    */
    public DateTime constructDateTimeGMT(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = dateTimeSplitList[0].split('/') ;
        List<String> timeSplit = dateTimeSplitList[1].split(':');
        String strAMPM = dateTimeSplitList[2];
        DateTime constructedDateTime;
        if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
        {
            constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),00, Integer.valueof(timeSplit[1]), 00);
        }
        else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
        {
            constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
        }
        else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
        {
            constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(dateValueSplit[0]),Integer.valueof(dateValueSplit[1]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
        }
        
        return constructedDateTime;
    }
    
    /**
    Method to update DateTime string, if date and month are single digit then add '0' to make double digit for consistency on UI
    */
    public String formatDateString(String dateTimeToFormat)
    {
        System.debug('UN - FORMATTED STRING = ' + dateTimeToFormat);
        List<String> dateTimeSplitList =  dateTimeToFormat.split(' ');
        List<string> dateValueSplit = dateTimeSplitList[0].split('/') ;
        String strDate = dateValueSplit[1];
        String strMonth = dateValueSplit[0];
        if(Integer.valueof(strDate) < 10 && strDate.length() < 2) // apart from interger number, better to check length as well to be doubly sure that it is indeed single digit
            strDate = '0'+strDate;
        if(Integer.valueof(strMonth) < 10 && strMonth.length() < 2)
            strMonth = '0'+strMonth;        
        
        String formattedString = strMonth + '/' + strDate + '/' +  dateValueSplit[2] + ' ' + dateTimeSplitList[1] + ' ' + dateTimeSplitList[2];
        System.debug('FORMATTED STRING = ' + formattedString);
        return formattedString;
    }    

    @TestVisible private String convertMonthNameToNumber(String strMonthName)
    {
        if(strMonthName == 'Jan')
            return '01';
        else if(strMonthName == 'Feb')
            return '02';
        else if(strMonthName == 'Mar')
            return '03';
        else if(strMonthName == 'Apr')
            return '04';
        else if(strMonthName == 'May')
            return '05';
        else if(strMonthName == 'Jun')
            return '06';
        else if(strMonthName == 'Jul')
            return '07';
        else if(strMonthName == 'Aug')
            return '08';
        else if(strMonthName == 'Sep')
            return '09';
        else if(strMonthName == 'Oct')
            return '10';
        else if(strMonthName == 'Nov')
            return '11';
        else if(strMonthName == 'Dec')
            return '12';
        else
            return '01';
    }
    
    @TestVisible private String convertMonthIntNumberToName(Integer iMonthNumber)
    {
        if(iMonthNumber == 1)
            return 'Jan';
        else if(iMonthNumber == 2)
            return 'Feb';
        else if(iMonthNumber == 3)
            return 'Mar';
        else if(iMonthNumber == 4)
            return 'Apr';
        else if(iMonthNumber == 5)
            return 'May';
        else if(iMonthNumber == 6)
            return 'Jun';
        else if(iMonthNumber == 7)
            return 'Jul';
        else if(iMonthNumber == 8)
            return 'Aug';
        else if(iMonthNumber == 9)
            return 'Sep';
        else if(iMonthNumber == 10)
            return 'Oct';
        else if(iMonthNumber == 11)
            return 'Nov';
        else if(iMonthNumber == 12)
            return 'Dec';
        else
            return 'Jan';
    }
}
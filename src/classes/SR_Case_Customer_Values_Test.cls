@isTest
public class SR_Case_Customer_Values_Test
{
    static testMethod void SR_Case_Customer_Values_Test()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;
    
    
    case cs1= new case();
    cs1.subject='This is a test case1';
    cs1.Accountid=acc.id;
    cs1.Contactid=cont.id;
    cs1.priority = 'low';
    insert cs1;
    
    PageReference myVfPage = Page.SR_Case_Customer_Values_Page;
    Test.setCurrentPage(myVfPage);
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller = new SR_Case_Customer_Values(con);
        controller.Customer_date_end ='This is time ';
        controller.hour_time = 8;
        controller.AMorPM = 'AM';
        controller.hour_timeOfMal='12';
        controller.hour_timeOfMalStart= '11';
        //controller.formatDateString ('2011/12/09');
        controller.min_time='22';
        controller.time_AmPM ='Am';
        controller.saveDates ();
        controller.getHour();
        controller.getMin();
        controller.getAmPM();
        //controller.saverequesttimeend();
        String AMorPM;
        Integer hour_time;
        Case current_case = new case();
        datetime MalfunctionEnd = System.now()+8;
        dateTime RequestStart = System.now()+9;
        dateTime RequestEnd ;
        Case updatecase = new case(id = current_case.id);
        hour_time = RequestStart.hour();
        hour_time = MalfunctionEnd.hour();
        //controller.savecustomertime();
        //controller.savecustomertimeend();
        //controller.RequestEnd.hour ();
        controller.convertMonthNameToNumber('Dec');
        controller.convertMonthIntNumberToName(12);
        string str1 = '2/5/2014 : 12:00 AM';
        //controller.constructDateTime(str1);

        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()));
        
        
    
  }
  static testMethod void SR_Case_Customer_Values_Test1()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;
    
    case cs1= new case();
    cs1.subject='This is a test case1';
    cs1.Accountid=acc.id;
    cs1.Contactid=cont.id;
    cs1.priority = 'low';
    insert cs1;
    
    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
        controller1.hour_timeOfMalStart= '11';
        controller1.min_timeMalSart= '25';
        controller1.min_timeReqEnd='23';
        controller1.hour_timeOfReqEnd= '22';
        controller1.min_timeReqSart= '10';
        controller1.hour_timeOfReqStart= '5';
        string str = '2/5/2014 : 12:00 AM';
        controller1.constructDateTime(str);
        controller1.constructDateTimeGMT(str);
        controller1.formatDateString(str);
        controller1.convertMonthNameToNumber('Nov');
        controller1.convertMonthIntNumberToName(11);
    }   
  static testMethod void SR_Case_Customer_Values_Test2()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;
    
    case cs1= new case();
    cs1.subject='This is a test case1';
    cs1.Accountid=acc.id;
    cs1.Contactid=cont.id;
    cs1.priority = 'low';
    insert cs1;
    
    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Oct');
    controller1.convertMonthIntNumberToName(10);
  }
    static testMethod void SR_Case_Customer_Values_Test3()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;
  
    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Aug');
    controller1.convertMonthIntNumberToName(8);
  }
  static testMethod void SR_Case_Customer_Values_Test4()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Sep');
    controller1.convertMonthIntNumberToName(9);
  }
  static testMethod void SR_Case_Customer_Values_Test5()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Jul');
    controller1.convertMonthIntNumberToName(7);
  }
  static testMethod void SR_Case_Customer_Values_Test6()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;
       
    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Jun');
    controller1.convertMonthIntNumberToName(6);
  }
  static testMethod void SR_Case_Customer_Values_Test7()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('May');
    controller1.convertMonthIntNumberToName(5);
  }
  static testMethod void SR_Case_Customer_Values_Test8()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Jan');
    controller1.convertMonthIntNumberToName(1);
  }
  static testMethod void SR_Case_Customer_Values_Test11()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Apr');
    controller1.convertMonthIntNumberToName(4);
  }
  static testMethod void SR_Case_Customer_Values_Test9()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Mar');
    controller1.convertMonthIntNumberToName(3);
  }
  static testMethod void SR_Case_Customer_Values_Test10()
    {
    ERP_Timezone__c etz = new ERP_Timezone__c();
    etz.Salesforce_timezone__c='Central Summer Time (Australia/Adelaide)';
    etz.name='Aussa';
    insert etz;
    
    Account acc=new Account();
    acc.name='Test Account';
    acc.ERP_Timezone__c='Aussa';
    acc.country__c = 'India';
    insert acc;
    
    contact cont = new contact();
    cont.FirstName= 'Megha';
    cont.lastname= 'Arora';
    cont.Accountid= acc.id;
    cont.department='Rad ONC';
    cont.MailingCity='New York';
    cont.MailingCountry='US';
    cont.MailingStreet='New Jersey2,';
    cont.MailingPostalCode='552601';
    cont.MailingState='CA';
    cont.Mailing_Address1__c= 'New Jersey2';
    cont.Mailing_Address2__c= '';
    cont.Mailing_Address3__c= '';
    cont.Phone= '5675687';
    cont.Email='test@gmail.com';
    insert cont;
    
    case cs = new case();
    cs.subject='This is a test case';
    cs.Accountid=acc.id;
    cs.Contactid=cont.id;
    cs.priority = 'low';
    insert cs;

    PageReference myVfPage1 = Page.SR_Case_Edit_Customer_Values_PopUp_Page;
    Test.setCurrentPage(myVfPage1);     
    ApexPages.currentPage().getParameters().Put('id',cs.id);
    ApexPages.StandardController con1 = new ApexPages.StandardController(cs);
    SR_Case_Customer_Values controller1 = new SR_Case_Customer_Values(con1);
    controller1.convertMonthNameToNumber('Feb');
    controller1.convertMonthIntNumberToName(2);
  }
 }
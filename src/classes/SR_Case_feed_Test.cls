@isTest(seeAlldata=True)
public class SR_Case_feed_Test {

    public static testMethod void testSR_Case_feed_Existing() {
            // Account
            Account testacc = SR_testdata.creteAccount();
            testacc.country__C = 'India'; 
            insert testacc;
            // Installed Product
            SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
            testinstallprd.SVMXC__Company__c = testacc.id;
            insert testinstallprd;
            //Contact
            List<Contact> testcontactlst = new list<Contact>();
            Contact testcont = new Contact();
            testcont.Phone = '1235678';
            testcont.AccountId = testacc.id;
            testcont.MailingCountry = 'India';
            testcont.Email= 'abc@gmail.com';
            testcont.CurrencyIsoCode='USD';
            testcont.LastName='PITT';
            testcont.FirstName='XYZ';
            testcontactlst.add(testcont); // Same account as that of case.
            
            Contact testcontact1 = SR_testdata.createContact();
            testcontact1.AccountId = testacc.id;
            testcontact1.Phone = '1235678';
            testcontact1.mailingcountry = 'India';
            testcontactlst.add(testcontact1);
            
            
            Contact testcontact2 = SR_testdata.createContact();
            testcontact2.mailingcountry = 'India';
            testcontact2.Phone = '1235678';
            testcontactlst.add(testcontact2); // another contact without the account.
            
            insert testcontactlst;
            
            //case
            case testcase = SR_testdata.createCase();
            testcase.ContactId = testcont.id;
            testcase.ProductSystem__c = testinstallprd.id;
            testcase.AccountId = testacc.id;
            testCase.Priority = 'Medium';
            testCase.Subject ='test';
            insert testcase;
            
            ApexPages.CurrentPage().getparameters().put('id',testcase.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testcase);
            SR_case_feed_component testcontroller = new SR_case_feed_component(controller);
            //testcontroller.caserec = testcase;
            testcontroller.Search();
            testcontroller.ClearSearch();
            testcontroller.caserec.AccountId = testacc.id;
            testcontroller.caserec.ContactId = testcontact1.id; // of same account
            testcontroller.updatecasemethod();
            testcontroller.caserec.ContactId = testcontact2.id; // of different account
            testcontroller.updatecasemethod();
            
    }
    //for account
    public static testMethod void testSR_Case_feed_New() {
        
            // Account
            Account testacc = new Account();
            testacc.name='testacnt';
            testacc.country__C = 'India'; 
            testacc.OMNI_Address1__c='abc';
            testacc.OMNI_City__c = 'delhi';
            insert testacc;
            // Installed Product
            SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
            testinstallprd.SVMXC__Company__c = testacc.id;
            insert testinstallprd;
            //Contact
            List<Contact> testcontactlst = new list<Contact>();
            Contact testcont = new Contact();
            testcont.AccountId = testacc.id;
            testcont.Phone = '1235678';
            testcont.MailingCountry = 'India';
            testcont.Email= 'abc@gmail.com';
            testcont.CurrencyIsoCode='USD';
            testcont.LastName='PITT';
            testcont.FirstName='XYZ';
            testcontactlst.add(testcont); // Same account as that of case.
            
            Contact testcontact1 = SR_testdata.createContact();
            testcontact1.AccountId = testacc.id;
            testcontact1.MailingCountry = 'India';
            testcontactlst.add(testcontact1);
            
            Contact testcontact2 = SR_testdata.createContact();
            testcontact2.MailingCountry = 'India';

            testcontactlst.add(testcontact2); // another contact without the account.
            
            insert testcontactlst;
            
            //case
            case testcase = SR_testdata.createCase();
            testcase.ContactId = testcont.id;
            testcase.ProductSystem__c = testinstallprd.id;
            testcase.AccountId = testacc.id;
            testCase.Priority = 'Medium';
            insert testcase;
            testcase.Priority = 'medium';
            update testCase;
            
            test.StartTest();
            ApexPages.CurrentPage().getparameters().put('Accid',testacc.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testacc);
            SR_case_feed_component testcontroller = new SR_case_feed_component(controller);
            testcontroller.caserec = testcase;
            testcontroller.Search();
            testcontroller.ClearSearch();
            testcontroller.caserec.AccountId = testacc.id;
            testcontroller.caserec.ContactId = testcont.id;
            testcontroller.caserec.ProductSystem__c = testinstallprd.id;
            testcontroller.CreateCase();
            testcontroller.SearchAccount();
            //SR_AccepDateHistUtility controller = new SR_AccepDateHistUtility();
            //controller.updateSalesOrderAndSalesOrderItem(testaccdatelist);
            test.stoptest();
   
      }
      
      //for contactcase
    public static testMethod void testSR_Case_feed1() {
            // Account
            Account testacc = new Account();
            testacc.name='testacnt';
            testacc.country__C = 'India'; 
            testacc.OMNI_Address1__c='abc';
            testacc.OMNI_City__c = 'delhi';
            insert testacc;
            // Installed Product
            SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
            testinstallprd.SVMXC__Company__c = testacc.id;
            insert testinstallprd;
            //Contact
            List<Contact> testcontactlst = new list<Contact>();
            Contact testcont = new Contact();
            testcont.AccountId = testacc.id;
            testcont.Phone = '1235678';
            testcont.MailingCountry = 'India';
            testcont.Email= 'abc@gmail.com';
            testcont.CurrencyIsoCode='USD';
            testcont.LastName='PITT';
            testcont.FirstName='XYZ';
            testcontactlst.add(testcont); // Same account as that of case.
            
            Contact testcontact1 = SR_testdata.createContact();
            testcontact1.AccountId = testacc.id;
            testcontact1.MailingCountry = 'India';
            testcontactlst.add(testcontact1);
            
            Contact testcontact2 = SR_testdata.createContact();
            testcontact2.MailingCountry = 'India';

            testcontactlst.add(testcontact2); // another contact without the account.
            
            insert testcontactlst;
            
            //case
            case testcase = SR_testdata.createCase();
            testcase.ContactId = testcont.id;
            testcase.ProductSystem__c = testinstallprd.id;
            testcase.AccountId = testacc.id;
            testCase.Priority = 'Medium';
            insert testcase;
            
            test.StartTest();
            ApexPages.CurrentPage().getparameters().put('ConId',testcont.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testcont);
            SR_case_feed_component testcontroller = new SR_case_feed_component(controller);
            testcontroller.caserec = testcase;
            testcontroller.Search();
            testcontroller.ClearSearch();
            //testcontroller.caserec.AccountId = testacc.id;
            testcontroller.caserec.ContactId = testcont.id;
            testcontroller.caserec.ProductSystem__c = testinstallprd.id;
            testcontroller.CreateCase();
            testcontroller.SearchAccount();
            //SR_AccepDateHistUtility controller = new SR_AccepDateHistUtility();
            //controller.updateSalesOrderAndSalesOrderItem(testaccdatelist);
            test.stoptest();
             }
             //for Installproductcase
    public static testMethod void testSR_Case_feed2() {
            Account testacc = new Account();
            testacc.name='testacnt';
            testacc.country__C = 'India'; 
            testacc.OMNI_Address1__c='abc';
            testacc.OMNI_City__c = 'delhi';
            insert testacc;
            // Installed Product
            SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
            testinstallprd.SVMXC__Company__c = testacc.id;
            insert testinstallprd;
            //Contact
            List<Contact> testcontactlst = new list<Contact>();
            Contact testcont = new Contact();
            testcont.AccountId = testacc.id;
            testcont.MailingCountry = 'India';
            testcont.Email= 'abc@gmail.com';
            testcont.CurrencyIsoCode='USD';
            testcont.LastName='PITT';
            testcont.FirstName='XYZ';
            testcontactlst.add(testcont); // Same account as that of case.
            
            Contact testcontact1 = SR_testdata.createContact();
            testcontact1.AccountId = testacc.id;
            testcont.Phone = '1235678';
            testcontact1.MailingCountry = 'India';
            testcontactlst.add(testcontact1);
            
            Contact testcontact2 = SR_testdata.createContact();
            testcontact2.MailingCountry = 'India';

            testcontactlst.add(testcontact2); // another contact without the account.
            
            insert testcontactlst;
            
            //case
            case testcase = SR_testdata.createCase();
            testcase.ContactId = testcont.id;
            testcase.ProductSystem__c = testinstallprd.id;
            testcase.AccountId = testacc.id;
            testCase.Priority = 'Medium';
            insert testcase;
            
            test.StartTest();
            ApexPages.CurrentPage().getparameters().put('InstalledProdId',testinstallprd.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testinstallprd);
            SR_case_feed_component testcontroller = new SR_case_feed_component(controller);
            testcontroller.caserec = testcase;
            testcontroller.Search();
            testcontroller.ClearSearch();
            //testcontroller.caserec.AccountId = testacc.id;
            testcontroller.caserec.ContactId = testcont.id;
            testcontroller.caserec.ProductSystem__c = testinstallprd.id;
            testcontroller.CreateCase();
            testcontroller.SearchAccount();
            //SR_AccepDateHistUtility controller = new SR_AccepDateHistUtility();
            //controller.updateSalesOrderAndSalesOrderItem(testaccdatelist);
            test.stoptest();
             }

}
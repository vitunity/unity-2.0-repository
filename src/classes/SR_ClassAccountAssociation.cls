/***********************************
    Created By  :   Nikita Gupta
    Created On  :   2-April-2014
    Created Reason: US4379, to update Account on Account Association based on ERP Customer Number
***********************************/
public class SR_ClassAccountAssociation 
{
    public void updateAccountOnAccAssociation (Set<Account_Associations__c> setAccAssociation)
    {
        Set<String> setCustomerNumber = new Set<String>(); //set to store ERP Customer Number from Account Association
        Map<String, ID> mapAccNumberToID = new Map<String, ID>(); //Map to store Key: Account Number, vaue: Account ID.
        
        for(Account_Associations__c AA : setAccAssociation)
        {
           if(AA.ERP_Customer_Number__c != null)
           {
                setCustomerNumber.add(AA.ERP_Customer_Number__c);
           }
           if(AA.ERP_Partner_Number__c != null)
           {
                setCustomerNumber.add(AA.ERP_Partner_Number__c);
           }     
            
        }
        
        if(setCustomerNumber.size() > 0)
        {
            List<Account> listOfAccount = [Select ID, Name, AccountNumber from Account where AccountNumber in : setCustomerNumber];
                    
            for(Account Acc : listOfAccount)
            {
                mapAccNumberToID.put(Acc.AccountNumber , Acc.ID);
            }
            
            if(mapAccNumberToID.size() > 0 )  
            {               
                for(Account_Associations__c AA : setAccAssociation)
                {
                    
                    if(mapAccNumberToID.get(AA.ERP_Customer_Number__c) != null)
                    {
                        //AA.Sold_To__c = mapAccNumberToID.get(AA.ERP_Customer_Number__c);
                        AA.Master_Account__c = mapAccNumberToID.get(AA.ERP_Customer_Number__c); //priti, 4/22/2014, commented above line and added this - replaced Sold_To__c with Master_Account__c
                    }
                    
                    if(mapAccNumberToID.get(AA.ERP_Partner_Number__c) != null)
                    {
                        AA.Account__c = mapAccNumberToID.get(AA.ERP_Partner_Number__c);
                    }
                }
            }
        }
    }
}
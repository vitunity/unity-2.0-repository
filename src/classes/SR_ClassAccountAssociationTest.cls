@isTest(SeeAllData=true)
private class SR_ClassAccountAssociationTest {
    testmethod static void AccountAssociation() {
        SR_ClassAccountAssociation cls = new SR_ClassAccountAssociation();
        Set<Account_Associations__c> setAccAssociation = new Set<Account_Associations__c>();
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234');
        insert accnt;
        Account_Associations__c ac = new Account_Associations__c();
        ac.Account__c = accnt.id;
        ac.Account_Role__c = 'Payer';
        ac.ERP_Customer_Number__c = '12345';
        ac.ERP_Partner_Number__c = '6060351';
        insert ac;
        
        ac.ERP_Partner_Number__c = '12345';
        update ac;
        setAccAssociation.add(ac);
        cls.updateAccountOnAccAssociation(setAccAssociation);
    }
}
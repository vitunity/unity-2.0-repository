/*************************************************************************\
@ Author : Shubham Jaiswal  
@ Date: 24/03/2014
@ Description  : US4287 
@ Last Modified By: Bushra 
@ Last Modified On: 23/05/2014
@ Last Modified Reason  : US4582
@ Last Modified By: Parul Gupta 
@ Last Modified On: 09/10/2014
@ Last Modified Reason  : DE1100, to update Coverage Limit with Counters Covered.
@ Last Modified By: Shradha Jain
@ Last Modified On: 16/03/2015
@ Last Modified Reason  : code optimization
/**************************************************************************/
public class SR_ClassCounterDetails 
{
    /***Method call for before trigger to populate fields- Start_Date__c, End_Date__c and SVMXC__Installed_Product__c.***/
    public void beforeUpdateFieldsCounterDetails(list<SVMXC__Counter_Details__c> listCounterDetail, map<id, SVMXC__Counter_Details__c> oldMapCounterDetail) // us4582, Method to update fields
    {
        /************** Set variable Initialization **********/
        set<string> setErpSerialNumber = new Set<String>(); // set to store serial number
        set<id> ProdIdset = new set<id>();
        /**** below sets are uncommented dor 1a-1c merge, by Bushra on 14-05-2015 *****/
        set<id> setSO = new set<id>();
        set<id> SetSoiId = new set<id>();
        /************** Map variable Initialization **********/
        Map<String, String> mapERPReferenceAndIP = new Map<String, String> (); // Map Key : ERP Reference and Value : InstalledProductID
        /**** below MAP are uncommented dor 1a-1c merge, by Bushra on 14-05-2015 *****/
        map<id,Sales_Order_Item__c> mapSoi = new map<id,Sales_Order_Item__c>();
        map<id,Sales_Order__c> mapSo = new map<id,Sales_Order__c>();
        
        
        for(SVMXC__Counter_Details__c varCountDetail : listCounterDetail)
        {
            //Added by Parul, 09/10/2014, DE1100 to update Coverage Limit with Counters Covered.
            if(oldMapCounterDetail == null && varCountDetail.SVMXC__Coverage_Limit__c == null && varCountDetail.SVMXC__Counters_Covered__c != null)
            {
                varCountDetail.SVMXC__Coverage_Limit__c = varCountDetail.SVMXC__Counters_Covered__c;
            }
            if(varCountDetail.ERP_Serial_Number__c != null && ((oldMapCounterDetail == null)||oldMapCounterDetail != null && oldMapCounterDetail.containsKey(varCountDetail.id) 
            && varCountDetail.ERP_Serial_Number__c != oldMapCounterDetail.get(varCountDetail.id).ERP_Serial_Number__c))
            {
                if(varCountDetail.ERP_Serial_Number__c != null){
                    setErpSerialNumber.add(varCountDetail.ERP_Serial_Number__c); // Please put null check - HS
                }
            }

            if (varCountDetail.Start_Date__c == null && varCountDetail.Sales_Order_Item__c != null && varCountDetail.SVMXC__Product__c != null && (oldMapCounterDetail == null) ||(oldMapCounterDetail != null && oldMapCounterDetail.containsKey(varCountDetail.id) && 
            (oldMapCounterDetail.get(varCountDetail.id).Sales_Order_Item__c != varCountDetail.Sales_Order_Item__c || oldMapCounterDetail.get(varCountDetail.id).SVMXC__Product__c != varCountDetail.SVMXC__Product__c))) {
                SetSoiId.add(varCountDetail.Sales_Order_Item__c);
                ProdIdset.add(varCountDetail.SVMXC__Product__c);
                }
            }
        /**** Below if condition is uncommented for 1a-1c, By Bushra on 14-05-2015****/
        if(SetSoiId.size() > 0)
        {
            //savon.FF 12-10-2015 DE6438 Added ERP_Reference__c to query
            mapSoi = new map<id,Sales_Order_Item__c>([select id, Quantity__c, name, Product__c, Product__r.Material_Group__c, Product__r.Budget_Hrs__c, UOM__c,
                                        Product__r.UOM__c, Site_Partner__c, End_Date__c, Start_Date__c, Sales_Order__c, Sales_Order__r.Pricing_Date__c, ERP_Reference__c 
                                        from Sales_Order_Item__c where id in :SetSoiId]);
            if(mapSoi.size() > 0 ){
                for(Sales_Order_Item__c soi: mapSoi.values()){ 
                    if(soi.Sales_Order__c != null){
                        setSO.add(soi.Sales_Order__c); // Please put null check - HS
                    }
                }
                if(setSO.size() > 0) {
                 mapSO = new map<id,Sales_Order__c>([select id,Requested_Delivery_Date__c,Pricing_Date__c,name from Sales_Order__c where id in: setSO]);
        }
            }
        }
        if(setErpSerialNumber.size() > 0)
                    {
            for(SVMXC__Installed_Product__c varIP : [Select Id, Name, ERP_Reference__c from SVMXC__Installed_Product__c where ERP_Reference__c in: setErpSerialNumber])
                        {
                if(varIP.ERP_Reference__c != null){
                    mapERPReferenceAndIP.put(varIP.ERP_Reference__c, varIP.ID); // Please put null check - HS
                        }
                    }            
        }

        //savon.FF 12-14-2015 DE6438
        Map <Id, SVMXC__Counter_Details__c> mapERPReference = new Map<Id, SVMXC__Counter_Details__c>([SELECT Id, Sales_Order_Item__r.ERP_Reference__c, ERP_NWA__r.ERP_Reference__c, SVMXC__Service_Maintenance_Contract__r.Name,
                                                                     SVMXC__Service_Maintenance_Contract__r.ERP_Line_Nbr__c, SVMXC__Covered_Products__r.ERP_Reference__c, SLA_Detail__r.ERP_Reference__c, ERP_NWA__r.Product__r.Material_Group__c 
                                                                     FROM SVMXC__Counter_Details__c WHERE Id IN :listCounterDetail]);
        
        for (SVMXC__Counter_Details__c varCD : listCounterDetail) {
            if(mapSoi.size() > 0 && mapSoi.containsKey(varCD.Sales_Order_Item__c) && mapSoi.get(varCD.Sales_Order_Item__c) != null && (mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group || mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group_2 || mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group_3 || mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group_4 || mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group_5 || mapSoi.get(varCD.Sales_Order_Item__c).Product__r.Material_Group__c == Label.Product_Material_group_6 ))
            {
                if(mapSo.size() > 0 && mapSo.containsKey(mapSoi.get(varCD.Sales_Order_Item__c).Sales_Order__c) && MapSo.get(mapSoi.get(varCD.Sales_Order_Item__c).Sales_Order__c).Pricing_Date__c!=null) // Please put null check for mapSoi map - HS-- it's there in above statement-SJ
                {
                    varCD.Start_Date__c = mapSo.get(mapSoi.get(varCD.Sales_Order_Item__c).Sales_Order__c).Pricing_Date__c;
                    varCD.End_Date__c = mapSo.get(mapSoi.get(varCD.Sales_Order_Item__c).Sales_Order__c).Pricing_Date__c.addMonths(24); 
                }
                }
            if(mapERPReferenceAndIP.size() > 0 && mapERPReferenceAndIP.containsKey(varCD.ERP_Serial_Number__c) && mapERPReferenceAndIP.get(varCD.ERP_Serial_Number__c) != null)
                {
                varCD.SVMXC__Installed_Product__c = mapERPReferenceAndIP.get(varCD.ERP_Serial_Number__c);
                        }

            //savon.FF 12-14-2015 DE6438
            system.debug(varCD.erp_reference__c);
            system.debug(varCD.erp_nwa__c);
            if (mapERPReference.get(varCD.Id) != null) system.debug(mapERPReference.get(varCD.Id).ERP_NWA__r.ERP_Reference__c);
            
            if (varCD.ERP_NWA__c != null) {
                if (mapERPReference.get(varCD.Id) != null && mapERPReference.get(varCD.Id).ERP_NWA__r.ERP_Reference__c != null)  //DE7414 DFCT0011590.
                 if (mapERPReference.get(varCD.Id).ERP_NWA__r.Product__r.Material_Group__c == 'H:EDUCTN')
                    varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).ERP_NWA__r.ERP_Reference__c.replace('-',''); //DE7414 DFCT0011590.
                 else
                    varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).ERP_NWA__r.ERP_Reference__c;
                 system.debug(varCD.erp_reference__c);  
            } else if (varCD.Sales_Order_Item__c != null) {
                
               if (mapERPReference.get(varCD.Id) != null && mapERPReference.get(varCD.Id).Sales_Order_Item__r.ERP_Reference__c != null) //DFCT0011590.
                varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).Sales_Order_Item__r.ERP_Reference__c;
                system.debug('Setting CD to SOI ERP Reference');
            } else if (varCD.SVMXC__Service_Maintenance_Contract__c != null) {
                if (mapERPReference.get(varCD.Id) != null && mapERPReference.get(varCD.Id).SVMXC__Service_Maintenance_Contract__r.ERP_Line_Nbr__c != null) //DFCT0011590.
                varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).SVMXC__Service_Maintenance_Contract__r.Name + '-' + mapERPReference.get(varCD.Id).SVMXC__Service_Maintenance_Contract__r.ERP_Line_Nbr__c;
            } else if (varCD.SVMXC__Covered_Products__c != null) {
                
             if (mapERPReference.get(varCD.Id) != null && mapERPReference.get(varCD.Id).SVMXC__Covered_Products__r.ERP_Reference__c != null) //DFCT0011590.
                varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).SVMXC__Covered_Products__r.ERP_Reference__c;
            } else if (varCD.SLA_Detail__c != null && mapERPReference.get(varCD.Id) != null) {
               if (mapERPReference.get(varCD.Id).SLA_Detail__r.ERP_Reference__c != null) // DFCT0011590.
                varCD.ERP_Reference__c = mapERPReference.get(varCD.Id).SLA_Detail__r.ERP_Reference__c;
              
            }
        }
    }

    /***following method is used for cloning the Counter Details of coverage recordtype to Reading recordtype***/
     public void CloneCoverageCD(List<SVMXC__Counter_Details__c> cdlist) // US4287 : To create 'Reading' recordtype record from 'Coverage' recordtype
    {   
        Map <String,ID> MapReadingCounterDetails = new Map <String,ID> ();
        //setting Counter Name & Service Maintainance Start
        Set<id> prodset = new Set<id>();
        Set<id> ipset = new Set<id>();
        Set<id> svcmntset = new Set<id>();
        Set<id> svcmntset2 = new Set<id>();
        Map<id,Product2 > mapProid2ProCode = new Map<id,Product2>();
        Map<id,SVMXC__Installed_Product__c> mapipid2svcmntid;
        Map<id,SVMXC__Service_Contract__c> mapsvcmntId2svcmntName;
        List<SVMXC__Counter_Details__c> lstOfCoveToUpdate = new List<SVMXC__Counter_Details__c>();
        Set <String> setName = new Set <String> (); 
        ID recordtypeid = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get(label.SR_CounterDetail_Recordtype_Reading).getRecordTypeId() ;
        for(SVMXC__Counter_Details__c cd : cdlist)
        {   
            if(cd.ERP_Reference__c != NULL)   // If this is being called from update trigger Please check if this field is changed (for all below fields)  --- HS-- this method is only for insert-SJ
                setName.add(cd.ERP_Reference__c);
            if(cd.SVMXC__Product__c != NULL)
            prodset.add(cd.SVMXC__Product__c);
            if(cd.SVMXC__Installed_Product__c != NULL)
            ipset.add(cd.SVMXC__Installed_Product__c);
            if(cd.SVMXC__Service_Maintenance_Contract__c != NULL)
            svcmntset.add(cd.SVMXC__Service_Maintenance_Contract__c);
        }
        if(prodset.size() > 0)
        {
           //CM: Modified the below query to fix the test class issue in line 194, trying to read Name field without retireving.
           mapProid2ProCode = new Map<id,Product2>([Select id,ProductCode,Name From Product2 Where id IN :prodset]);
        }
        if(ipset.size() > 0)
        {
           mapipid2svcmntid = new Map<id,SVMXC__Installed_Product__c>([Select id,SVMXC__Service_Contract__c From SVMXC__Installed_Product__c Where id IN :ipset]);
        }
        if (setName.size() > 0)
        {
            for (SVMXC__Counter_Details__c rc :[select id,Name from SVMXC__Counter_Details__c where ERP_Reference__c in :setName and recordtypeid =: recordtypeid]) 
            {
                if(rc.name != null){
                    mapReadingCounterDetails.put(rc.name,rc.id);
                }
            }
        }
        if(mapipid2svcmntid != null && mapipid2svcmntid.size() > 0)
        {
            for(SVMXC__Installed_Product__c ip :mapipid2svcmntid.values()){
                if(ip.SVMXC__Service_Contract__c != null){
                    svcmntset.add(ip.SVMXC__Service_Contract__c); 
                }
            }   
        }
        
      
        if(svcmntset.size() > 0 )
        {
           mapsvcmntId2svcmntName = new Map<id,SVMXC__Service_Contract__c>([Select id,Name From SVMXC__Service_Contract__c Where id IN :svcmntset]);
        }
        
        //setting Counter Name & Service Maintainance Ends
        List<SVMXC__Counter_Details__c> cdreading = new List<SVMXC__Counter_Details__c> ();
        for(SVMXC__Counter_Details__c cdcoverage : cdlist)
        {   
            if (mapReadingCounterDetails.containsKey(cdcoverage.name) && mapReadingCounterDetails.get(cdcoverage.name) != null) 
            continue;
            
            SVMXC__Counter_Details__c cdread = new SVMXC__Counter_Details__c();
            cdread=cdcoverage.clone();
           
            cdread.SVMXC__Counter_Definition__c  = cdcoverage.id;
            cdread.recordtypeid = recordtypeid;
           
            if(cdcoverage.SVMXC__Counter_Name__c != null)
                cdread.SVMXC__Counter_Name__c = cdcoverage.SVMXC__Counter_Name__c;
            else
            {
                if(cdcoverage.SVMXC__Product__c != null && mapProid2ProCode.size() > 0 && mapProid2ProCode.containsKey(cdcoverage.SVMXC__Product__c) && mapProid2ProCode.get(cdcoverage.SVMXC__Product__c) != null) //containsKey and null pointer check added by Parul, 01/3/2015
                {
                    cdread.SVMXC__Counter_Name__c = mapProid2ProCode.get(cdcoverage.SVMXC__Product__c).Name ;
                }
            }
            //DE7639 : Added for counter reading record.
            if (cdcoverage.SVMXC__Counter_Reading__c == 0){
                cdread.SVMXC__Counter_Reading__c = cdcoverage.SVMXC__Counters_Covered__c;
            }
            else if (cdcoverage.SVMXC__Counter_Reading__c > 0){
                cdread.SVMXC__Counter_Reading__c = cdcoverage.SVMXC__Counters_Covered__c - cdcoverage.SVMXC__Counter_Reading__c;
            }
            /* if(cdread.name!=null)
            {
              if (!cdread.name.contains('GMAX')) cdread.SVMXC__Counter_Reading__c = 0;
            } */
            //cdread.ERP_Reference__c = cdcoverage.name;
            cdread.ERP_Reference__c = cdcoverage.name+'-'+'Actual'; //savon.FF 12-10-2015 DE6438
           
            //if(cdcoverage.SVMXC__Installed_Product__c == null)
            //{
            //    cdread.SVMXC__Service_Maintenance_Contract__c  = cdcoverage.SVMXC__Service_Maintenance_Contract__c ;
            //    if(mapsvcmntId2svcmntName != null && mapsvcmntId2svcmntName.size() > 0 && mapsvcmntId2svcmntName.containsKey(cdcoverage.SVMXC__Service_Maintenance_Contract__c) && mapsvcmntId2svcmntName.get(cdcoverage.SVMXC__Service_Maintenance_Contract__c)!= null)
            //        cdread.ERP_Reference__c = mapsvcmntId2svcmntName.get(cdcoverage.SVMXC__Service_Maintenance_Contract__c).Name + '-' + cdcoverage.Line__c; 
            //}
            //else
            //{
            //    if(mapipid2svcmntid.size() > 0)
            //    {
            //        if(mapipid2svcmntid.containskey(cdcoverage.SVMXC__Installed_Product__c) && cdcoverage.SVMXC__Installed_Product__c!=null )
            //        {
            //            cdread.SVMXC__Service_Maintenance_Contract__c = mapipid2svcmntid.get(cdcoverage.SVMXC__Installed_Product__c).SVMXC__Service_Contract__c;
            //        // Null Check Added By Mohit : Error Resolution 9th May-2014

            //            //if(mapipid2svcmntid.get(cdcoverage.SVMXC__Installed_Product__c)!= null && mapipid2svcmntid.get(cdcoverage.SVMXC__Installed_Product__c).SVMXC__Service_Contract__c != null)
            //            //{
            //            if(mapsvcmntId2svcmntName != null && mapsvcmntId2svcmntName.containskey(cdcoverage.SVMXC__Installed_Product__c))
            //            {
            //            if(mapsvcmntId2svcmntName.size() > 0 && mapsvcmntId2svcmntName.containsKey(cdcoverage.SVMXC__Installed_Product__c)) 
            //                cdread.ERP_Reference__c = mapsvcmntId2svcmntName.get(mapipid2svcmntid.get(cdcoverage.SVMXC__Installed_Product__c).SVMXC__Service_Contract__c).Name + '-' + cdcoverage.Line__c;}
            //        }
            //    }
            //}
        //}

        /**** Below if condition is uncommented for 1a-1c, By Bushra on 14-05-2015****/     
            //If else block added by mohit
            if(cdcoverage.Sales_Order_Item__c !=null)
            {
            cdread.Sales_Order_Item__c = cdcoverage.Sales_Order_Item__c;
            }
        /**** Below if condition is uncommented for 1a-1c, By Bushra on 14-05-2015****/
            if(cdcoverage.Sales_Order__c !=null)
            {
            cdread.Sales_Order__c = cdcoverage.Sales_Order__c;
            }
            if(cdcoverage.Account__c !=null)
            {
              cdread.Account__c = cdcoverage.Account__c;
            }
            cdreading.add(cdread);
            SVMXC__Counter_Details__c cdCov = new SVMXC__Counter_Details__c();
            cdCov.SVMXC__Counter_Definition__r  = cdread;
            cdCov.id = cdcoverage.id;
            lstOfCoveToUpdate.add(cdCov);
        }
        system.debug('cdreading----------->' + cdreading.size());
        
        if(cdreading.size() > 0)
            Database.SaveResult[] insertCounterDetails = Database.insert(cdreading, false);
        if(lstOfCoveToUpdate.size()>0)
        {
            for(SVMXC__Counter_Details__c cdCov : lstOfCoveToUpdate)
            {
                if(cdCov.SVMXC__Counter_Definition__r != null)
                    cdCov.SVMXC__Counter_Definition__c = cdCov.SVMXC__Counter_Definition__r.id;
            }
            update lstOfCoveToUpdate;
        }
    }
    /*  //CM : DFCT0011561 : In Prod, this method is no where using hence commented the method.
    public void UpdateCounterReading(List<SVMXC__Counter_Details__c> cdlist,map<id,SVMXC__Counter_Details__c> oldmap) //US5149 Updating Counter reading record  when Internal Comment on Coverage Record Changes
    {
        //Map<id,Recordtype> MaprecordType= new map<id,Recordtype>([select id, Name, developername from recordtype where SObjectType='SVMXC__Counter_Details__c']);
         ID counterDetCoverageRecordTypeid = RecordTypeHelperClass.COUNTER_DETAILS_RECORDTYPES.get('Coverage').getRecordTypeId(); //DFCT0011585
        list<SVMXC__Counter_Details__c> lstReadingCounter = new list<SVMXC__Counter_Details__c>(); // List holding Counter Reading Record to update
        
        for(SVMXC__Counter_Details__c varCDT: cdlist)
        {
            if(varCDT.Internal_Comments__c != oldmap.get(varCDT.id).Internal_Comments__c  && varCDT.recordtypeid == counterDetCoverageRecordTypeid) //MaprecordType.get(varCDT.recordtypeid).developerName == 'Coverage'
            {
                SVMXC__Counter_Details__c objCDTRead = new SVMXC__Counter_Details__c(id=varCDT.SVMXC__Counter_Definition__c,Internal_Comments__c=varCDT.Internal_Comments__c);
                lstReadingCounter.add(objCDTRead);
            }
            // Above is done by user and below is done by system so these shouldn't happen at the same time
            if(varCDT.SLA_Detail__c != oldmap.get(varCDT.id).SLA_Detail__c  && varCDT.recordtypeid == counterDetCoverageRecordTypeid) //MaprecordType.get(varCDT.recordtypeid).developerName == 'Coverage' 
            {
                SVMXC__Counter_Details__c objCDTRead = new SVMXC__Counter_Details__c(id=varCDT.SVMXC__Counter_Definition__c,SLA_Detail__c=varCDT.SLA_Detail__c);
                lstReadingCounter.add(objCDTRead);
            }
        }
        if(lstReadingCounter.size() > 0)
        {
          update  lstReadingCounter;
        }
    }
    */ //CM : DFCT0011561 : In Prod, this method is no where using hence commented the method.
}
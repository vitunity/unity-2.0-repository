@isTest
private class SR_ClassCounterDetails_Test{
    public static list<Product2> prodlist;
    public static list<Sales_Order__c> lstsos;
    public static Product2 objProd;
    public static Account testAcc;
    public static sales_Order__c objSalesOrder;
    public static list<Sales_Order_Item__c> lstSOI=new list<Sales_Order_Item__c>();
    public static list<SVMXC__Installed_Product__c> iplist;
    public static list<SVMXC__Site__c> loclist;

    static {
            //Create Account
            testAcc = AccountTestData.createAccount();
            testAcc.AccountNumber = '123456';
            testAcc.CSS_District__c = 'Test';
            testAcc.BillingCountry='USA';
            testAcc.Country__c= 'USA';
            testAcc.BillingState ='CA';
            testAcc.BillingPostalCode = '95051';
            testAcc.ERP_Timezone__c = 'CST';
            testAcc.ERP_Site_Partner_Code__c = '123456';
            insert testAcc;
            //create country
            Country__c con = new Country__c(Name = 'United States');
            con.SAP_Code__c = 'US';
            insert con;

            loclist = new list<SVMXC__Site__c>();
            SVMXC__Site__c objLoc = new SVMXC__Site__c();
            objLoc.Name = 'Test Location';
            objLoc.SVMXC__Street__c = 'Test Street';
            objLoc.SVMXC__Country__c = 'United States';
            objloc.Sales_Org__c = '0600';
            objLoc.SVMXC__Zip__c = '98765';
            objLoc.ERP_Functional_Location__c = 'Test Location';
            objLoc.Plant__c = 'Test Plant';
            objLoc.ERP_Site_Partner_Code__c = '123456';
            objLoc.SVMXC__Location_Type__c = 'Depot';
            objloc.country__c = con.id;
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            objLoc.SVMXC__Stocking_Location__c = true;
            objLoc.SVMXC__Account__c = testAcc.id;
            loclist.add(objLoc);
            SVMXC__Site__c objLoc1 = new SVMXC__Site__c();
            objLoc1.Name = 'Test Location';
            objLoc1.SVMXC__Street__c = 'Test Street';
            objLoc1.SVMXC__Country__c = 'United States';
            objloc1.Sales_Org__c = '0600';
            objLoc1.SVMXC__Zip__c = '98765';
            objLoc1.ERP_Functional_Location__c = 'Test Location';
            objLoc1.Plant__c = 'Test Plant';
            objLoc1.ERP_Site_Partner_Code__c = '123456';
            objLoc1.SVMXC__Location_Type__c = 'Field';
            objloc1.country__c = con.id;
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            objLoc1.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            objLoc1.SVMXC__Stocking_Location__c = true;
            objLoc1.SVMXC__Account__c = testAcc.id;
            loclist.add(objLoc1);
            insert loclist;

            prodlist = new list<Product2>();
        Product2 objProd2=new Product2();
        objProd2.Name = 'technical Test Product';
        objProd2.Material_Group__c= 'H:EDUCTN';
        objProd2.Budget_Hrs__c =10.0;
        objProd2.Credit_s_Required__c=10;
        objProd2.UOM__c='EA';
        objProd2.ProductCode ='AABB';
        objProd2.Training_Region__c = 'NA';
        prodlist.add(objProd2);
        Product2 objProd3=new Product2();
        objProd3.Name = 'technical Test Produc1t';
        objProd3.Material_Group__c= 'H:EDUCTN';
        objProd3.Budget_Hrs__c =12.0;
        objProd3.Credit_s_Required__c=10;
        objProd3.UOM__c='EA';
        objProd3.ProductCode ='AABB1C';
        objProd3.Training_Region__c = 'NA';
        prodlist.add(objProd3);

            objProd = SR_testdata.createProduct();
            objProd.Name = 'Test Product';
            objProd.Budget_Hrs__c =10.0;
            objProd.Credit_s_Required__c = 3;
            objProd.Material_Group__c = 'H:CREDTS';
            objProd.UOM__c = 'DA';
            objProd.ProductCode = 'ABABAB';
            objprod.course_code__c = 'CREDTS';
            objprod.Training_region__c = 'NA';
            prodlist.add(objProd);
            insert prodlist;

            lstsos = new list<Sales_Order__c>();
            objSalesOrder=new Sales_Order__c();
            objSalesOrder.Name='001';
            objSalesOrder.Site_Partner__c = testAcc.id;
            objSalesOrder.Sold_To__c = testAcc.id;
            objSalesOrder.Pricing_Date__c = System.Today();
            lstsos.add(objSalesOrder);
            Sales_Order__c objSalesOrder1=new Sales_Order__c();
            objSalesOrder1.Name='001';
            objSalesOrder1.Site_Partner__c = testAcc.id;
            objSalesOrder1.Sold_To__c = testAcc.id;
            objSalesOrder1.Pricing_Date__c = System.Today();
            objSalesOrder1.Block__c = '70 cancelled';
            lstsos.add(objSalesOrder1);
            insert lstsos;
        iplist = new list<SVMXC__Installed_Product__c>();
            SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
            objTopLevel.Name = 'H23456';
            objTopLevel.SVMXC__Serial_Lot_Number__c = 'H23456';
            objTopLevel.SVMXC__Product__c = objProd.ID;
            objTopLevel.SVMXC__Site__c = loclist[0].ID;
            objTopLevel.ERP_Reference__c = 'H23456';
            //objTopLevel.Service_Team__c = objServTeam.Id;
            //objTopLevel.SVMXC__Preferred_Technician__c = objTechEquip.ID;
            objTopLevel.ERP_CSS_District__c = 'ABC';
            objTopLevel.ERP_Sales__c = 'Test ERP Sales';
            objTopLevel.SVMXC__Company__c = testAcc.id;
            iplist.add(objTopLevel);

            SVMXC__Installed_Product__c objTopLevel1 = new SVMXC__Installed_Product__c();
            objTopLevel1.Name = 'H23456A';
            objTopLevel1.SVMXC__Serial_Lot_Number__c = 'H23456A';
            objTopLevel1.SVMXC__Product__c = objProd.ID;
            objTopLevel1.SVMXC__Site__c = loclist[0].ID;
            objTopLevel1.ERP_Reference__c = 'H23456A';
            //objTopLevel.Service_Team__c = objServTeam.Id;
            //objTopLevel.SVMXC__Preferred_Technician__c = objTechEquip.ID;
            objTopLevel1.ERP_CSS_District__c = 'ABC';
            objTopLevel1.ERP_Sales__c = 'Test ERP Sales';
            objTopLevel1.SVMXC__Company__c = testAcc.id;
            iplist.add(objTopLevel1);

            /*SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
            objComponent.Name = 'H12345';
            objComponent.SVMXC__Serial_Lot_Number__c = 'H12345';
            objComponent.SVMXC__Parent__c = objTopLevel.ID;
            objComponent.SVMXC__Product__c = objProd.ID;
            objComponent.SVMXC__Preferred_Technician__c = objTechEquip.ID;
            objComponent.ERP_Sales__c = 'Test ERP Sales';
            objComponent.ERP_Reference__c = 'H12345';
            objComponent.SVMXC__Top_Level__c = objTopLevel.id;
            objComponent.Service_Team__c = objServTeam.Id;
            objComponent.ERP_CSS_District__c = 'ABC';
            objComponent.SVMXC__Site__c = objLoc.id;
            objComponent.SVMXC__Status__c = 'Shipped';
            objComponent.SVMXC__Company__c = testAcc.id;
            iplist.add(objComponent);*/
            insert iplist;
            
        //parent SOI:
        Sales_Order_Item__c parSOI=new Sales_Order_Item__c();
        parSOI.Start_Date__c= System.Today();
        parSOI.End_Date__c  = System.Today().AddDays(10);
        parSOI.Sales_Order__c = lstsos[0].id;
        parSOI.Site_Partner__c = testAcc.id;
        parSOI.Product__c = prodlist[1].Id;
        parSOI.ERP_Item_Category__c = 'Z014';
        parSOI.Quantity__c = 20;
        lstSOI.add(parSOI);
        //Create Sales Order Item
        
        Sales_Order_Item__c objSOI1=new Sales_Order_Item__c();
        objSOI1.Start_Date__c= System.Today();
        objSOI1.End_Date__c  = System.Today().AddDays(10);
        objSOI1.Sales_Order__c =lstsos[0].id;
        objSOI1.Site_Partner__c = testAcc.id;
        objSOI1.Product__c = prodlist[0].Id;
        objSOI1.Quantity__c = 10;
        objSOI1.ERP_Item_Category__c = 'Z014';
        //objSOI1.Parent_Item__c = parSOI.id;
        lstSOI.add(objSOI1);
        
        Sales_Order_Item__c objSOI2=new Sales_Order_Item__c();
        objSOI2.Start_Date__c= System.Today().AddDays(-20);
        objSOI2.End_Date__c  = System.Today().AddDays(-10);
        objSOI2.Sales_Order__c =lstsos[0].id;
        objSOI2.Site_Partner__c = testAcc.id;
        objSOI2.Product__c = prodlist[0].Id;
        objSOI2.Quantity__c = 5;
        objSOI2.ERP_Item_Category__c = 'Z018';
        //objSOI2.Parent_Item__c = parSOI.id;
        lstSOI.add(objSOI2);
        //Test.startTest();
        Insert lstSOI;
        lstSOI[1].Parent_Item__c = lstsoi[0].id;
        lstSOI[2].Parent_Item__c = lstsoi[0].id;
        update lstsoi;
    }

    Public static testmethod void testmethod1(){
        Test.startTest();
        set<id> soiIDS = new set<id>();
        for (Sales_Order_Item__c soi : lstSOI){
            soiIDS.add(soi.id);
        }

        list<SVMXC__Counter_Details__c> cdlist = [select id from SVMXC__Counter_Details__c where Sales_Order_Item__c in :soiIDS];
        list <SVMXC__Counter_Details__c> cdlistnew = new list<SVMXC__Counter_Details__c>();
        for (SVMXC__Counter_Details__c cd : cdlist){
            cd.SVMXC__Coverage_Limit__C = null;
            cd.SVMXC__Counters_Covered__c = 1;
            cd.ERP_Serial_Number__c = 'H23456';
            cd.Sales_Order_Item__c = lstsoi[2].id;
            cdlistnew.add(cd);
        }

        //update cdlist;
        update cdlistnew;
        Test.stopTest();
    }
}
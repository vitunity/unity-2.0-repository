@isTest (seealldata=true)
public class SR_ClassERP_WBS_test 
    {
    public static Id WOInstallationId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
    public static Id WOProjMangId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get(label.sr_ProjectManagement).getRecordTypeId();
    static Account objAcc = SR_testdata.creteAccount();
    static ERP_Partner__c objPart =  SR_testdata.createERPPartner();
    static ERP_Partner_Association__c objEPA = new ERP_Partner_Association__c();
    static Product2 objProd = SR_testdata.createProduct();
    static Product2 objProdModel = SR_testdata.createProduct();
    static list<Product2> prodlst = new List<Product2>();
    static Sales_order__c objSO = SR_testdata.createSalesOrder();
    static SVMXC__Counter_Details__c objCD = new SVMXC__Counter_Details__c();
    static ID counterCoverageRT = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId();
    static SVMXC__Installed_Product__c objIP = SR_testdata.createInstalProduct();
    static Sales_Order_Item__c objSOI;
    static Sales_Order_Item__c objSOI2;  
    static Sales_Order_Item__c objParentSOI = new Sales_Order_Item__c(); 
    static Sales_Order_Item__c objParentSOI2 = new Sales_Order_Item__c();
    static SVMXC__Case_Line__c objCL = SR_testdata.createCaseLine();
    static SVMXC__Site__c objLoc = SR_testdata.createsite();
    static SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();
    static Contact objCont = SR_testdata.createContact();
    static Case objCase = SR_testdata.createCase();
    static  ERP_WBS__c erpWbs = new ERP_WBS__c();
    static  ERP_WBS__c erpWbs2 = new ERP_WBS__c();
    static Profile p = new profile();
    static User systemuser = new user();
    static SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
    static ERP_Project__c objERProj = new ERP_Project__c ();
    static SVMXC__Service_Order_Line__c objServiceOrderLine = new SVMXC__Service_Order_Line__c();
    static RecordType recServiceTeam ;
    static
    {

        recServiceTeam = [SELECT Id,Name,Description FROM Recordtype WHERE Developername = 'Technician' AND SObjectType=:'SVMXC__Service_Group__c'];
        //p = [Select id from profile where name = 'System Administrator'];
        //systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 

        objAcc.AccountNumber = '123456';
        objAcc.Country__c = 'Australia';    
        objAcc.BillingState = 'WA';   
        objAcc.BillingCity = 'Milpitas'; 
        insert objAcc;

        objLoc.SVMXC__Account__c = objAcc.id;

        insert objLoc;

        objSO.Pricing_Date__c = system.today();
        objSO.ERP_Reference__c = 'Sales Order 001';

        insert objSO;

        objProd.Material_Group__c = Label.Product_Material_group;
        objProd.Budget_Hrs__c = 4;
        objProd.UOM__c = 'HR';
        objProd.Product_Type__c = label.SR_Product_Type_Sellable;
        objProd.ProductCode = 'Test Material No 001';
        objProd.ERP_PCode_4__c = 'H012';

        prodlst.add(objProd);


        objProdModel.Product_Type__c = 'Model';
        objProdModel.ProductCode = 'H012';

        prodlst.add(objProdModel);
        insert prodlst;

        objIP.ERP_Reference__c = 'H98732';
        objIP.SVMXC__Product__c = prodlst[0].ID;
        objIP.SVMXC__Serial_Lot_Number__c = 'H6878876';
        objIP.SVMXC__Status__c = 'Ordered';
        objIP.Interface_Change_Type__c = 'Create';
        objIP.SVMXC__Site__c = objLoc.ID;
		objIP.Sales_Order__c = objSO.Id;
        insert objIP;
        
        //insert objCont;

        objCase.ERP_Project_Number__c = 'ERp Proj 001';
        objCase.Priority = 'Medium';
        objCase.ERP_Project_Number__c = 'ERP Project number';
        objCase.SVMXC__Top_Level__c  = objIP.id;
        objCase.AccountID = objAcc.Id;
        objCase.ProductSystem__c  = objIP.id;
        insert objCase; 
        
        objParentSOI.name = 'test Parent SOI';
        objParentSOI.Quantity__c = 1.0;
        objParentSOI.ERP_Reference__c = 'H012345';
        objParentSOI.Sales_Order__c = objSO.ID;
        objParentSOI.ERP_Item_Profit_Center__c = 'Test 01';

        //insert objParentSOI;

        objSOI =  SR_testdata.createSalesOrderItem();
        objSOI.ERP_Item_Category__c = 'Z014';
        objSOI.WBS_Element__c = 'test reference99'; 
        objSOI.Sales_Order_Number__c = objSO.ERP_Reference__c;
        objSOI.ERP_Material_Number__c = prodlst[0].ProductCode;
        objSOI.Product__c = prodlst[0].ID;
        objSOI.Parent_Item__c = objParentSOI.ID;
        objSOI.ERP_Product_Model__c = 'Test prod Model 001';
        objSOI.ERP_Higher_Level_Item__c = objParentSOI.ERP_Reference__c;
        objSOI.ERP_Item_Profit_Center__c = 'Test 02';
        objSOI.ERP_Reference__c = '123456';
        objSOI.ERP_Site_Partner_Code__c = objAcc.AccountNumber;
        objSOI.Sales_Order__c = objSO.ID;
        objSOI.Site_Partner__c = objAcc.ID;
        objSOI.Product_Model__c = prodlst[1].ID;
        //objSOI.ERP_ship_to_party__c = objPart.Partner_Number__c;
        objSOI.Quantity__c = 3;
        objSOI.Location__c = objLoc.ID;
        objSOI.Installed_Product__c = objIP.id;

        insert objSOI;

        //objParentSOI.ID = objSOI.Parent_Item__c;
        //update objParentSOI;

        objCL.Sales_Order_Item__c = objSOI.id;
        objCL.SVMXC__Case__c = objCase.id;
        insert objCL;

        objIP.Sales_OrderItem__c = objSOI.id;

        update objIP;

        ERP_Org__c eobj = new ERP_Org__c();
        eobj.Name = 'US - XRP - Sales';                // ERP Org Name
        eobj.Company_Code__c = '0053';                    // Company Code
        eobj.Sales_Org__c = '0530';
        insert eobj;


        objERProj.Name = 'ERp Proj 001';

        insert objERProj;
        // 18 Nov 2016 : Puneet : Moving this code snippet to otherData method as it was throwing 101 exception, this change in test class is related to DFCT0012169
        /*erpWbs.Sales_Order__c = objSO.Id;
        erpWbs.Sales_Order_Item__c =objSOI.Id; 
        erpWbs.ERP_Sales_Order_Item_Number__c = objSOI.ERP_Reference__c; 
        erpWbs.ERP_Reference__c = 'test reference99';
        erpWbs.ERP_Project_Nbr__c = 'ERP Project number';
        erpWbs.PM_Assignment_Email__c = 'testemail@string.com';
        erpWbs.Acceptance_Date__c = System.today();
        erpWbs.Forecast_Start_Date__c = System.today();
        erpWbs.Forecast_End_Date__c = System.today()+4;
        erpWbs.Site_Partner__c = objAcc.ID;
        erpWbs.Org__c ='0530';
        //erpWbs.ERP_PM_Work_Center__c = 'H39205';
        insert erpWbs;*/ 

        //objWO.Sales_Order_Item__c = objSOI.Id;
        //update objWO;
        //erpWbs.Forecast_Start_Date__c =  system.today();
        //update erpWbs;


    } 

    public static void otherData(){
        // 18 Nov 2016 : Puneet : Moved this code snippet to this method as it was throwing 101 exception, this change in test class is related to DFCT0012169
        erpWbs.Sales_Order__c = objSO.Id;
        erpWbs.Sales_Order_Item__c =objSOI.Id; 
        erpWbs.ERP_Sales_Order_Item_Number__c = objSOI.ERP_Reference__c; 
        erpWbs.ERP_Reference__c = 'test reference99';
        erpWbs.ERP_Project_Nbr__c = 'ERP Project number';
        erpWbs.PM_Assignment_Email__c = 'testemail@string.com';
        erpWbs.Acceptance_Date__c = System.today();
        erpWbs.Forecast_Start_Date__c = System.today();
        erpWbs.Forecast_End_Date__c = System.today()+4;
        erpWbs.Site_Partner__c = objAcc.ID;
        erpWbs.Org__c ='0530';
        //erpWbs.ERP_PM_Work_Center__c = 'H39205';
        insert erpWbs; 
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC');
        erpnwa.erp_Std_text_key__c = 'PM00001';
        erpnwa.ERP_WBS_Nbr__c = '123123-99';
        erpnwa.WBS_Element__c = erpWbs.Id;
        //insert erpnwa;
        
        ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.ERP_Reference__c='TestExternal';
        ServiceTeam.SVMXC__Group_Code__c='TestExternal';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.recordtypeId = recServiceTeam.id;

        Insert ServiceTeam;

        objWO.SVMXC__Company__c = objAcc.ID;
        //objWO.SVMXC__Contact__c = objCont.id;
        objWO.SVMXC__Case__c = objCase.ID;
        objWO.RecordTypeID = WOProjMangId;//WOInstallationId;
        objWO.ERP_Sales_Order_Item__c = '123456';
        objWO.SVMXC__Order_Status__c = 'Open';
        objWO.Event__c = false;
        objWO.ERP_WBS__c = erpWbs.Id;
        insert objWO;


        objServiceOrderLine.SVMXC__Service_Order__c = objWO.Id;
        objServiceOrderLine.ERP_WBS__c = erpWbs.Id;
        objServiceOrderLine.Recordtypeid = '012E000000021fUIAQ';
        objServiceOrderLine.Sales_Order_Item__c = erpwbs.Sales_Order_Item__c;

        //Insert objServiceOrderLine;


        Acceptance_Date_History__c adh = new Acceptance_Date_History__c(Installed_Product__c = objIP.id, Sales_Order__c = objSO.Id );
        adh.Sales_Order_Item__c = objSOI.Id;
        adh.ERP_WBS__c = erpWbs.Id;
        insert adh;
    }

    static testMethod void beforeTrigger_Test()
    {
        Test.StartTest();
        otherData();
        list<ERP_WBS__c> seterpid = new  list<ERP_WBS__c>();
        list<ERP_WBS__c> lstERPWBS = new list<ERP_WBS__c>();
        map<id,ERP_WBS__c> oldMap = new  map<id,ERP_WBS__c>();
        map<id,ERP_WBS__c> newMap = new  map<id,ERP_WBS__c>();
        SR_ClassERP_WBS objerpwbsCls = new SR_ClassERP_WBS();

        Acceptance_Date_History__c accp = new Acceptance_Date_History__c();
        accp.Acceptance_Date__c = system.today()+6;
        accp.Installed_Product__c = objIP.id;
        accp.ERP_WBS__c = erpWbs.id;
        insert accp;
        boolean flag = false;
        //seterpid.add(erpWbs);
        lstERPWBS.add(erpWbs);  
        objerpwbsCls.updateAccepDateOnErpWbs(lstERPWBS);
        //erpWbs.Acceptance_Date__c = system.today()+2;
        //erpWbs.Forecast_Start_Date__c =  system.today();
        //update erpWbs;

        //oldMap.put(erpWbs.id,erpWbs);
        //objerpwbsCls.updateERPWBSAcceptanceDateOnWO(lstERPWBS);
        /*  erpWbs.Acceptance_Date__c = system.today()+1;
        update erpWbs;
        newMap.put(erpWbs.id,erpWbs);
        objerpwbsCls.populateDateFieldsOnCounterDetails(oldMap,newMap);*/
        //erpWbs.Acceptance_Date__c = System.today()+1;
        //update erpWbs;
        //update objIP;
        Test.StopTest();

    }

    public static testMethod void afterTrigger_Test()
        {
        System.Test.StartTest();
        otherData();
        
        //erpWbs.Sales_Order__c = objSO.ID;        
        erpWbs.ERP_Project_Nbr__c = 'ERp Proj 001';
        erpWbs.ERP_Sales_Order_Number__c = objSO.ERP_Reference__c;   
        erpWbs.ERP_Reference__c = 'H00899';
        erpWbs.ERP_Project__c = objERProj.id;   
        erpWbs.ERP_Sales_Order_Item_Higher_Level_Nbr__c = 'H012345';

        System.Test.StopTest();


    }

    public static testMethod void afterTrigger_Test1()
    {

        System.Test.StartTest();
        otherData();

        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.SVMXC__Service_Group__c = ServiceTeam.id;
        tech.ERP_Reference__c = 'TextExternal';
        tech.Name = 'TestTechnician';
        tech.SVMXC__Average_Drive_Time__c = 2.00;
        tech.SVMXC__Average_Speed__c = 2.00;
        tech.SVMXC__Break_Duration__c = 2.00;
        tech.SVMXC__Break_Type__c = 'Fixed';
        tech.SVMXC__City__c = 'Noida';
        tech.SVMXC__Country__c = 'India'; 
        tech.Current_End_Date__c = system.today();
        tech.Dispatch_Queue__c = 'DISP - AMS';
        tech.SVMXC__Email__c = 'standarduser@testorg.com';
        tech.SVMXC__State__c = 'UP';
        tech.SVMXC__Street__c = 'abc';
        tech.SVMXC__Zip__c = '201301';
        tech.SVMXC__Salesforce_User__c = systemuser.id;    
        tech.SVMXC__Select__c = false; 
        tech.ERP_Work_Center__c = 'H39205';        
        Insert tech;

        list<ERP_WBS__c> wbsToUpdate = [Select Id, Sales_Order__c, Sales_Order_Item__c, ERP_Sales_Order_Item_Number__c,
        ERP_Reference__c, ERP_Project_Nbr__c, PM_Assignment_Email__c, Acceptance_Date__c,
        Forecast_Start_Date__c, Forecast_End_Date__c 
        from ERP_WBS__c where ID =: erpWbs.Id];

        for(ERP_WBS__c wb :wbsToUpdate){
        wb.Acceptance_Date__c = System.today()+7;
        wb.Forecast_Start_Date__c = System.today()+1;
        wb.Forecast_End_Date__c = System.today()+5;
        Wb.ERP_PM_Work_Center__c = 'H39205';
        wb.ERP_Project__c = objERProj.Id;
        wb.ERP_Reference__c = 'testref99'; 
        wb.ERP_Sales_Order_Number__c = '321150650';
        wb.SFDC_Error_Message__c = 'Test Message';
        wb.PM_Assignment_Email__c = 'testemail@string.com';
        wb.Sales_Order_Item__c =objSOI.Id; 
        } 
        update wbsToUpdate;
        Test.stopTest(); 

    }

    public static testMethod void createCaseDataTest()
    {
        System.Test.StartTest();
        otherData();
        boolean isInsert = false;
        List<String> ERPWBSProjectNbr = new List<String>();
        ERPWBSProjectNbr.add(erpWbs.ERP_Project_Nbr__c);
        Map<String, ERP_WBS__c> ERPWBSmap = new Map<String, ERP_WBS__c>();
        ERPWBSmap.put(erpWbs.ERP_Project_Nbr__c, erpWbs);


        SR_ClassERP_WBS.creatCaseData(isInsert, ERPWBSmap, ERPWBSProjectNbr);
        erpWbs.ERP_Project_Nbr__c = 'ERP Project number';
        System.Test.StopTest();
    }

    public static testMethod void createNewCaseDataTest()
    {
        System.Test.StartTest();
        otherData();
        erpWbs.ERP_Project_Nbr__c = 'ERP Project number99';
        update erpWbs;


        boolean isInsert = true; 
        List<String> ERPWBSProjectNbr = new List<String>();
        ERPWBSProjectNbr.add(erpWbs.ERP_Project_Nbr__c);
        Map<String, ERP_WBS__c> ERPWBSmap = new Map<String, ERP_WBS__c>();
        ERPWBSmap.put(erpWbs.ERP_Project_Nbr__c, erpWbs);


        SR_ClassERP_WBS.creatCaseData(isInsert, ERPWBSmap, ERPWBSProjectNbr);

        Test.stopTest();
    }



    public static testMethod void createWOTest()
    {
        System.Test.StartTest();
        otherData();
        ERP_WBS__c erpWbsDatawo = new ERP_WBS__c();
        erpWbsDatawo.Sales_Order__c = objSO.Id;
        erpWbsDatawo.Sales_Order_Item__c =objSOI.Id; 
        erpWbsDatawo.ERP_Sales_Order_Item_Number__c = objSOI.ERP_Reference__c; 
        erpWbsDatawo.ERP_Reference__c = 'test reference99';
        erpWbsDatawo.ERP_Project_Nbr__c = 'ERP Project number';
        erpWbsDatawo.PM_Assignment_Email__c = 'testemail@string.com';
        erpWbsDatawo.Acceptance_Date__c = System.today();
        erpWbsDatawo.Forecast_Start_Date__c = System.today(); 
        erpWbsDatawo.Forecast_End_Date__c = System.today()+4;
        erpWbsDatawo.Site_Partner__c = objAcc.ID;
        erpWbsDatawo.Org__c ='0530';
        erpWbsDatawo.ERP_PM_Work_Center__c = 'H39205';

        insert erpWbsDatawo;

        map<id,ERP_WBS__c> oldWbsMap = new map<id,ERP_WBS__c>();
        List<ERP_WBS__c> newWbslist = new List<ERP_WBS__c>();
        oldWbsMap.put(erpWbsDatawo.Id,erpWbsDatawo);


        erpWbsDatawo.ERP_PM_Work_Center__c = 'H39206';
        update erpWbsDatawo;
        newWbslist.add(erpWbsDatawo);
        SR_ClassERP_WBS wbscls = new SR_ClassERP_WBS();
        wbscls.createWO(newWbslist, oldWbsMap);
        System.Test.StopTest();
    }

    static testMethod void populateDateFieldsOnCounterDetailsTest(){
        System.Test.StartTest();
        ERP_WBS__c erpWbsData1 = new ERP_WBS__c();
        erpWbsData1.Sales_Order__c = objSO.Id;
        erpWbsData1.Sales_Order_Item__c =objSOI.Id; 
        erpWbsData1.ERP_Sales_Order_Item_Number__c = objSOI.ERP_Reference__c; 
        erpWbsData1.ERP_Reference__c = 'test reference99';
        erpWbsData1.ERP_Project_Nbr__c = 'ERP Project number';
        erpWbsData1.PM_Assignment_Email__c = 'testemail@string.com';
        erpWbsData1.Acceptance_Date__c = System.today();
        erpWbsData1.Forecast_Start_Date__c = System.today(); 
        erpWbsData1.Forecast_End_Date__c = System.today()+4;
        erpWbsData1.Site_Partner__c = objAcc.ID;
        erpWbsData1.Org__c ='0530';
        //erpWbs.ERP_PM_Work_Center__c = 'H39205';
        insert erpWbsData1;
        map<id,ERP_WBS__c> oldWbsMap = new map<id,ERP_WBS__c>();
        oldWbsMap.put(erpWbsData1.Id,erpWbsData1);
        map<id,ERP_WBS__c> newWbsMap = new map<id,ERP_WBS__c>();

        erpWbsData1.Acceptance_Date__c = System.today()+2;
        update erpWbsData1;
        newWbsMap.put(erpWbsData1.Id,erpWbsData1);
        SR_ClassERP_WBS wbsclass = new SR_ClassERP_WBS();
        wbsclass.populateDateFieldsOnCounterDetails(oldWbsMap,newWbsMap);
        System.Test.StopTest();  
    }

    static testMethod void PopulateCaseLineStartAndEndDateFromERPWBSTest(){

        System.Test.StartTest();
        otherData();
        ERP_WBS__c erpWbsData = new ERP_WBS__c();
        erpWbsData.Sales_Order__c = objSO.Id;
        erpWbsData.Sales_Order_Item__c =objSOI.Id; 
        erpWbsData.ERP_Sales_Order_Item_Number__c = objSOI.ERP_Reference__c; 
        erpWbsData.ERP_Reference__c = 'test reference99';
        erpWbsData.ERP_Project_Nbr__c = 'ERP Project number';
        erpWbsData.PM_Assignment_Email__c = 'testemail@string.com';
        erpWbsData.Acceptance_Date__c = System.today();
        erpWbsData.Forecast_Start_Date__c = System.today(); 
        erpWbsData.Forecast_End_Date__c = System.today()+4;
        erpWbsData.Site_Partner__c = objAcc.ID;
        erpWbsData.Org__c ='0530';
        //erpWbs.ERP_PM_Work_Center__c = 'H39205';
        insert erpWbsData; 

        list<ERP_WBS__c> ListERPWBS = new list<ERP_WBS__c>(); 
        List<String> ListERPWBSErpRef = new  List<String>();
        List<ERP_WBS__c> ListERPWBS2updateWD = new List<ERP_WBS__c>();
        map<id,ERP_WBS__c> ERPWBSIds = new map<id,ERP_WBS__c>();
        ListERPWBS.add(erpWbsData);       
        ListERPWBSErpRef.add(erpWbsData.ERP_Reference__c);
        ListERPWBS2updateWD.add(erpWbsData);
        ERPWBSIds.put(erpWbsData.Id, erpWbsData);
        SR_ClassERP_WBS wbsclass = new SR_ClassERP_WBS();
        wbsclass.PopulateCaseLineStartAndEndDateFromERPWBS(ListERPWBS, ListERPWBSErpRef, ListERPWBS2updateWD, ERPWBSIds);
        System.Test.StopTest();   

    }

    public static testMethod void updateChildWBSTest()
    {
        System.Test.StartTest();
        otherData();
        list<ERP_WBS__c> erpOldList = new list<ERP_WBS__c>();
        erpOldList.add(erpWbs);

        map<Id,ERP_WBS__c> oldMap = new map<Id,ERP_WBS__c>();
        map<Id,ERP_WBS__c> newMap = new map<Id,ERP_WBS__c>();

        for(ERP_WBS__c wbsold : erpOldList){
        oldMap.put(wbsold.Id,wbsold);
        }

        list<ERP_WBS__c> wbsToUpdate = [Select Id, Sales_Order__c, Sales_Order_Item__c, ERP_Sales_Order_Item_Number__c,
        ERP_Reference__c, ERP_Project_Nbr__c, PM_Assignment_Email__c, Acceptance_Date__c,
        Forecast_Start_Date__c, Forecast_End_Date__c 
        from ERP_WBS__c where ID =: erpWbs.Id];

        for(ERP_WBS__c wb :wbsToUpdate){
            wb.Acceptance_Date__c = System.today()+7;
            wb.Forecast_Start_Date__c = System.today()+1;
            wb.Forecast_End_Date__c = System.today()+5;
            Wb.ERP_PM_Work_Center__c = 'H39205';
            wb.ERP_Project__c = objERProj.Id;
            wb.ERP_Reference__c = 'testref99'; 
            wb.ERP_Sales_Order_Number__c = '321150650';
            wb.SFDC_Error_Message__c = 'Test Message';
            wb.PM_Assignment_Email__c = 'testemail@string.com';
            wb.Sales_Order_Item__c =objSOI.Id; 
            wb.Component__c = 'H98732';
        } 

        update wbsToUpdate;
        SR_ClassERP_WBS.createCase(true,wbsToUpdate);
        System.Test.stopTest(); 
        for(ERP_WBS__c wbsnew : wbsToUpdate){
            newMap.put(wbsnew.Id,wbsnew);
        }
        
        objSOI.ERP_Item_Category__c = 'Z000';
        //update objSOI;
        
        SR_ClassERP_WBS wbsclass = new SR_ClassERP_WBS();
        wbsclass.updateChildWBS(wbsToUpdate,oldMap);
        

    }
}
@isTest
public class SR_ClassInstalledProduct_Test
{
    static testmethod void myconstructorTest()
    {
        String TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u1 = [Select id from user where profileId = :p.id and isActive = true and id != :userInfo.getUserId() limit 1]; 
        
        //System.RunAs(u1)
        {
            IP_Status__c ips = new IP_Status__c(Name = 'Ordered');
            insert ips;
            //insert Account record 
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account;
            Map<string, Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            Account accnt = new  Account(Recordtypeid = AccMapByName.get('Site Partner').getRecordTypeId(), Name = 'Test AccountSFQA', CurrencyIsoCode = 'USD', Country__c = 'India', OMNI_Postal_Code__c = '21234', AccountNumber = '2009845', BillingCity='San Francisco');
            Insert accnt;
            //insert location record
            SVMXC__Site__c loc = new  SVMXC__Site__c(Name = 'test location', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            Insert loc;
            SVMXC__Site__c loc1 = new  SVMXC__Site__c(Name = 'test location1', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            loc1.ERP_Functional_Location__c = 'H140752-CLINAC';
            Insert loc1;
            //insert contact
            Contact conObj = new  Contact(AccountId = accnt.Id, LastName = 'test contact', FirstName = 'test', Email = 'test@wipro.com', phone = '986743212', Contact_Type__c = 'Urologist');
            conObj.mailingcountry = 'India';
            //   insert conObj;
            //insert product
            product2 prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
            Insert prodObj;
            // insert Service Group
            SVMXC__Service_Group__c objSvcGrp = new  SVMXC__Service_Group__c(Name = 'test', SVMXC__Group_Code__c = '4578');
            Insert objSvcGrp;
            
            Schema.DescribeSObjectResult TR = Schema.SObjectType.SVMXC__Service_Group_Members__c;
            SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
            servcContact.Name = 'TestContract';
            servcContact.ERP_Sales_Org__c = 'salesorg';
            servcContact.Ext_Quote_number__c = 'TEST1234';
            servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
            servcContact.SVMXC__End_Date__c = system.today();
            insert servcContact;
            List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
            Map<string, Schema.RecordTypeInfo> techMapByName = TR.getRecordTypeInfosByName();
            // insert parent IP
            SVMXC__Installed_Product__c objIP = new  SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = null, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = accnt.id);
            objIP.ERP_Distributor__c = '2009845';
            //objIP.ERP_Sold_To__c = accnt.id;
            objIP.ERP_Product_Code__c = '1212';
            objIP.ERP_Reference__c = 'H140752-CLINAC';
            objIP.Billing_Type__c = 'W - Warranty';
            objIP.SVMXC__Service_Contract__c = servcContact.Id;
            
            iplst.add(objIP);
            // ISC History
            ISC_History__c ISCREC = new  ISC_History__c(Installed_Product__c = objIP.Id, product__c = prodObj.Id);
            Insert ISCREC;
            SVMXC__Installed_Product__c objIP3 = new  SVMXC__Installed_Product__c(Current_ISC_Configuration__c = ISCREC.Id, Name = 'testsd', ERP_Reference__c = 'H270409-CLINAC', ERP_CSS_District__c = '4578', ERP_Product_Code__c = '1212', ERP_Top_Level__c = '1234', SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test213', SVMXC__Company__c = accnt.id, SVMXC__Top_Level__c = objIP.id, ERP_Functional_Location__c = 'H140752-CLINAC', ERP_Parent__c = objIP.id, SVMXC__Parent__c = objIP.id);
            iplst.add(objIP3);
            SVMXC__Installed_Product__c objIP4 = new  SVMXC__Installed_Product__c(Name = 'testIP4', ERP_Reference__c = 'H140754-CLINAC', ERP_CSS_District__c = '4579', ERP_Product_Code__c = '1212', ERP_Top_Level__c = '1234', SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test213', SVMXC__Company__c = accnt.id, SVMXC__Top_Level__c = objIP.id, ERP_Functional_Location__c = 'H140755-CLINAC', ERP_Parent__c = objIP.id);
            iplst.add(objIP4);
            Insert iplst;
            test.starttest();
            SVMXC__Service_Order__c servOrder = new  SVMXC__Service_Order__c();
            servOrder.RecordTypeId = Schema.Sobjecttype.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
            servOrder.SVMXC__Order_Status__c = 'Open';
            servOrder.Auto_Page__c = false;
            servOrder.Date_Time_for_30_mins_WO__c = System.Now().addMinutes(-20);
            servOrder.District_Manager_1__c = userInfo.getUserID();
            servOrder.District_Manager_Email__c = 'test@gmail.com';
            servOrder.ERP_Reference__c = 'H270409-CLINAC';
            servOrder.SVMXC__Company__c = accnt.id;
            servOrder.SVMXC__Contact__c = conObj.id;
            servOrder.CurrencyIsoCode = 'USD';
            servOrder.SVMXC__Site__c = loc1.id;
            Insert servOrder;
            objIP3.ERP_Top_Level__c = objIP.id;
            objIP3.WBS_Element__c = 'H270409-CLINAC';
            objIP3.ERP_Reference__c = 'H270408-CLINAC';
            objIP3.SVMXC__Parent__c = iplst[0].id;
            objIP3.ERP_Distributor__c = '2009845';
            objIP3.ERP_Parent__c = 'H270409';
            objIP3.SVMXC__Company__c = null;
            Update objIP3;
            objIP4.SVMXC__Parent__c = objIP.id;
            Update objIP4;
            objIP3.ERP_Reference__c = 'H140954-CLINAC';
            objIP3.SVMXC__Company__c = accnt.id;
            Update objIP3;
            SR_ClassInstalledProduct clsip = new SR_ClassInstalledProduct();
            clsip.updateInstalledProductFields(iplst,null);
            clsip.updateFieldsOnChildInstProd(iplst,null);
            Test.stopTest();
        }
    }
    
    static testmethod void orderedIPTest()
    {
        String TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u1 = [Select id from user where profileId = :p.id and isActive = true and id != :userInfo.getUserId() limit 1]; 
        test.starttest();
        //System.RunAs(u1)
        {
            //insert Account record
            IP_Status__c ips = new IP_Status__c(Name = 'Ordered');
            insert ips;
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account;
            Map<string, Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
            servcContact.Name = 'TestContract';
            servcContact.ERP_Sales_Org__c = 'salesorg';
            servcContact.Ext_Quote_number__c = 'TEST1234';
            servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
            servcContact.SVMXC__End_Date__c = system.today();
            insert servcContact;
            Account accnt = new  Account(Recordtypeid = AccMapByName.get('Site Partner').getRecordTypeId(), Name = 'Test AccountSFQA', CurrencyIsoCode = 'USD', Country__c = 'India', OMNI_Postal_Code__c = '21234', AccountNumber = '2009845', BillingCity='San Francisco');
            Insert accnt;
            //insert location record
            SVMXC__Site__c loc = new  SVMXC__Site__c(Name = 'test location', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            Insert loc;
            SVMXC__Site__c loc1 = new  SVMXC__Site__c(Name = 'test location1', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            loc1.ERP_Functional_Location__c = 'H140752-CLINAC';
            Insert loc1;
            //insert contact
            Contact conObj = new  Contact(AccountId = accnt.Id, LastName = 'test contact', FirstName = 'test', Email = 'test@wipro.com', phone = '986743212', Contact_Type__c = 'Urologist');
            conObj.mailingcountry = 'India';
            //   insert conObj;
            //insert product
            product2 prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
            Insert prodObj;
            // insert Service Group
            SVMXC__Service_Group__c objSvcGrp = new  SVMXC__Service_Group__c(Name = 'test', SVMXC__Group_Code__c = '4578');
            Insert objSvcGrp;
            SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, 
                                                                                       Name = 'Test Technician', 
                                                                                       SVMXC__Active__c = true, 
                                                                                       SVMXC__Enable_Scheduling__c = true,
                                                                                       SVMXC__Phone__c = '987654321', 
                                                                                       SVMXC__Email__c = 'abc@xyz.com', 
                                                                                       SVMXC__Service_Group__c = objSvcGrp.ID, 
                                                                                       User__c = UserInfo.getUserId(),
                                                                                       ERP_Work_Center__c='H9876545321');
            insert tech;
            Schema.DescribeSObjectResult TR = Schema.SObjectType.SVMXC__Service_Group_Members__c;
            Map<string, Schema.RecordTypeInfo> techMapByName = TR.getRecordTypeInfosByName();
            // insert parent IP
            SVMXC__Installed_Product__c objIP = new  SVMXC__Installed_Product__c(Name = 'testIP123', ERP_Work_Center__c = 'abc123', SVMXC__Preferred_Technician__c = tech.id, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Ordered', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = accnt.id,SVMXC__Service_Contract__c = servcContact.id);
            objIP.ERP_Distributor__c = '2009845';
            objIP.ERP_Sold_To__c = accnt.id;
            objIP.ERP_Product_Code__c = '1212';
            objIP.ERP_Reference__c = 'H140752-CLINAC';
            objIP.ERP_CSS_District__c = '4578';
            objIp.Service_Team__c = objSvcGrp.id;
            Insert objIP;
            objIP.SVMXC__Site__c = loc.id;
            Update objIP;
            SVMXC__Installed_Product__c testInstallProduct = SR_testdata.createInstalProduct();
            testInstallProduct.ERP_Functional_Location__c = 'USA';
            testInstallProduct.SVMXC__Serial_Lot_Number__c ='test';
            testInstallProduct.SVMXC__Service_Contract__c = servcContact.Id;
            insert testInstallProduct;
            Test.stopTest();
        }
    }
    
    static testmethod void unitTest1() {
        String TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u1 = [Select id from user where profileId = :p.id and isActive = true and id != :userInfo.getUserId() limit 1]; 
        test.starttest();
        //System.RunAs(u1)
        {
            IP_Status__c ips = new IP_Status__c(Name = 'Ordered');
            insert ips;
            CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
            insert cdf;
            //insert Account record 
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account;
            Map<string, Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            Account accnt = new  Account(Recordtypeid = AccMapByName.get('Site Partner').getRecordTypeId(), Name = 'Test AccountSFQA', CurrencyIsoCode = 'USD', Country__c = 'India', OMNI_Postal_Code__c = '21234', AccountNumber = '2009845', BillingCity='San Francisco');
            Insert accnt;
            //insert location record
            SVMXC__Site__c loc = new  SVMXC__Site__c(Name = 'test location', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            Insert loc;
            SVMXC__Site__c loc1 = new  SVMXC__Site__c(Name = 'test location1', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
            loc1.ERP_Functional_Location__c = 'H140752-CLINAC';
            Insert loc1;
            //insert contact
            Contact conObj = new  Contact(AccountId = accnt.Id, LastName = 'test contact', FirstName = 'test', Email = 'test@wipro.com', phone = '986743212', Contact_Type__c = 'Urologist');
            conObj.mailingcountry = 'India';
            //   insert conObj;
            //insert product
            product2 prodObj = new  product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
            Insert prodObj;
            // insert Service Group
            SVMXC__Service_Group__c objSvcGrp = new  SVMXC__Service_Group__c(Name = 'test', SVMXC__Group_Code__c = '4578');
            Insert objSvcGrp;
            SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, 
                                                                                       Name = 'Test Technician', 
                                                                                       SVMXC__Active__c = true, 
                                                                                       SVMXC__Enable_Scheduling__c = true,
                                                                                       SVMXC__Phone__c = '987654321', 
                                                                                       SVMXC__Email__c = 'abc@xyz.com', 
                                                                                       SVMXC__Service_Group__c = objSvcGrp.ID, 
                                                                                       User__c = UserInfo.getUserId(),
                                                                                       ERP_Work_Center__c='H9876545321');
            insert tech;
            Schema.DescribeSObjectResult TR = Schema.SObjectType.SVMXC__Service_Group_Members__c;
            Map<string, Schema.RecordTypeInfo> techMapByName = TR.getRecordTypeInfosByName();
            // insert parent IP
            SVMXC__Installed_Product__c objIP = new  SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = tech.Id, SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = accnt.id);
            objIP.ERP_Distributor__c = '2009845';
            objIP.ERP_Sold_To__c = accnt.id;
            objIP.ERP_Product_Code__c = '1212';
            objIP.ERP_Reference__c = 'H140752-CLINAC';
            
            Insert objIP;
            Test.stopTest();
            // ISC History
           
            SVMXC__Installed_Product__c objIP3 = new  SVMXC__Installed_Product__c(Name = 'testsd', ERP_Reference__c = 'H270409-CLINAC', SVMXC__Preferred_Technician__c = tech.Id, ERP_CSS_District__c = '4578',
                                                                                  ERP_Product_Code__c = '1212', ERP_Top_Level__c = '1234', SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', 
                                                                                  SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test213', SVMXC__Company__c = accnt.id, SVMXC__Top_Level__c = objIP.id, 
                                                                                  ERP_Functional_Location__c = 'H140752-CLINAC', ERP_Parent__c = objIP.id, SVMXC__Parent__c = objIP.id);
            Insert objIP3;
            SVMXC__Installed_Product__c objIP4 = new  SVMXC__Installed_Product__c(Name = 'testIP4', ERP_Reference__c = 'H140754-CLINAC', SVMXC__Preferred_Technician__c = tech.Id, ERP_CSS_District__c = '4579', ERP_Product_Code__c = '1212', ERP_Top_Level__c = '1234', SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc1.id, SVMXC__Serial_Lot_Number__c = 'test213', SVMXC__Company__c = accnt.id, SVMXC__Top_Level__c = objIP.id, 
                                                                                  ERP_Functional_Location__c = 'H140755-CLINAC', ERP_Parent__c = objIP.id, SVMXC__Parent__c = objIP.id);
            Insert objIP4;
            SVMXC__Service_Order__c servOrder = new  SVMXC__Service_Order__c();
            servOrder.RecordTypeId = Schema.Sobjecttype.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
            servOrder.SVMXC__Order_Status__c = 'Open';
            servOrder.Auto_Page__c = false;
            servOrder.Date_Time_for_30_mins_WO__c = System.Now().addMinutes(-20);
            servOrder.District_Manager_1__c = userInfo.getUserID();
            servOrder.District_Manager_Email__c = 'test@gmail.com';
            servOrder.ERP_Reference__c = 'H270409-CLINAC';
            servOrder.SVMXC__Company__c = accnt.id;
            servOrder.SVMXC__Contact__c = conObj.id;
            servOrder.CurrencyIsoCode = 'USD';
            servOrder.SVMXC__Site__c = loc1.id;
            Insert servOrder;
            objIP3.ERP_Top_Level__c = objIP.id;
            objIP3.WBS_Element__c = 'H270409-CLINAC';
            objIP3.ERP_Reference__c = 'H270408-CLINAC';
            objIP3.SVMXC__Parent__c = objIP.id;
            objIP3.ERP_Distributor__c = '2009845';
            objIP3.ERP_Parent__c = 'H270409';
            objIP3.SVMXC__Company__c = null;
            Update objIP3;
            objIP4.SVMXC__Parent__c = objIP.id;
            Update objIP4;
            objIP3.ERP_Reference__c = 'H140954-CLINAC';
            objIP3.SVMXC__Company__c = accnt.id;
            Update objIP3;
        }
    }
    public static testmethod void test4()
    {
        IP_Status__c ips = new IP_Status__c(Name = 'Ordered');
        insert ips;
        SVMXC__Service_Level__c slaTerm = new SVMXC__Service_Level__c();
        slaTerm.Name = 'Elite';
        slaTerm.SVMXC__Restoration_Tracked_On__c = 'Case';
        slaTerm.SVMXC__Onsite_Response_Tracked_On__c ='Case';
        slaTerm.SVMXC__Resolution_Tracked_On__c = 'Case';
        insert slaTerm;

        SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
        servcContact.Name = 'TestContract';
        servcContact.ERP_Sales_Org__c = 'salesorg';
        servcContact.Ext_Quote_number__c = 'TEST1234';
        servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
        servcContact.SVMXC__End_Date__c = system.today().adddays(365);
        servcContact.ERP_Contract_Type__c = 'ZH3';
        insert servcContact;
        //insert Account record 
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account;
        Map<string, Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new  Account(Recordtypeid = AccMapByName.get('Site Partner').getRecordTypeId(), Name = 'Test AccountSFQA', CurrencyIsoCode = 'USD', Country__c = 'India', OMNI_Postal_Code__c = '21234', AccountNumber = '2009845', BillingCity='San Francisco');
        Insert accnt;
        CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
        SVMXC__Installed_Product__c testInstallProduct = SR_testdata.createInstalProduct();
        testInstallProduct.ERP_Functional_Location__c = 'USA';
        testInstallProduct.SVMXC__Serial_Lot_Number__c ='test';
        testInstallProduct.SVMXC__Service_Contract__c = servcContact.Id;
        testInstallProduct.SVMXC__Status__c = 'Ordered';
        insert testInstallProduct;
        //Covered Product
        SVMXC__Service_Contract_Products__c testContractProduct = new SVMXC__Service_Contract_Products__c();
        testContractProduct.SVMXC__Installed_Product__c = testInstallProduct.id;
        testContractProduct.SVMXC__Service_Contract__c = servcContact.id;
        testContractProduct.SVMXC__SLA_Terms__c =  slaTerm.id;
        testContractProduct.Cancelation_Reason__c = null;
        testContractProduct.SVMXC__Start_Date__c = system.Today().addDays(-15);
        testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(-17);
        testContractProduct.Serial_Number_PCSN__c = 'test';
        insert testContractProduct;
        List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
        iplst.add(testInstallProduct);
        iplst[0].ERP_Functional_Location__c = 'USA';
        update iplst;
        SR_ClassInstalledProduct clsip = new SR_ClassInstalledProduct();
        clsip.updateInstalledProductFields(iplst,null);
    }
    
}
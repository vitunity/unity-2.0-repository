@isTest
public class SR_ClassSLADetailUtillTrigger_test
{
    Static testmethod void SR_ClassSLADetailTest()
    {
        ERP_Org__c erpOrg = new ERP_Org__c();
        erpOrg.Sales_Org__c = 'salesorg';
        erporg.Name = 'testERP';
        erporg.Days_Offset__c = 2;
        insert erporg;

        //Create Product
            Product2 objProd2=new Product2();
            objProd2.Name = 'technical Test Product';
            objProd2.Material_Group__c= 'H:TRNING';
            objProd2.Budget_Hrs__c =10.0;
            objProd2.Credit_s_Required__c=10;
            objProd2.UOM__c='EA';
            insert objProd2;

        SVMXC__PM_Plan_Template__c pmplanTemp = new SVMXC__PM_Plan_Template__c();
        pmplanTemp.Name = 'Template';
        insert pmplanTemp;

        SVMXC__PM_Schedule_Template__c pmschdltemp = new SVMXC__PM_Schedule_Template__c();
        pmschdltemp.SVMXC__Recurring__c = true; 
        pmschdltemp.SVMXC__PM_Plan_Template__c = pmplanTemp.id;
        insert pmschdltemp;

        List<SVMXC__Service__c> lstOfAvlservc = new List<SVMXC__Service__c>();

        //Available Service when level ,unit frequency not null
            SVMXC__Service__c avlServc1 = new SVMXC__Service__c ();
            avlServc1.Name = 'testAvlService1';
            avlServc1.PM_Plan_Template__c = pmplanTemp.id;
            avlServc1.Level__c ='test';
            avlServc1.Count_Units__c='test';
            avlServc1.Counter_Condition__c =null;
            avlServc1.Counter_Amount__c = 500.12;
            avlServc1.Product__c = objprod2.id;
            avlServc1.Frequency__c=2;
            avlServc1.Count_Units__c = 'DA';
            lstOfAvlservc.add(avlServc1);

        //Available Service
            SVMXC__Service__c avlServc2 = new SVMXC__Service__c ();
            avlServc2.Name = 'testAvlService2';
            avlServc2.PM_Plan_Template__c = pmplanTemp.id;
            avlServc2.Level__c ='Service Contract';
            avlServc2.Counter_Condition__c ='test';
            avlServc2.Product__c = objprod2.id;
            avlServc2.Count_Units__c = 'EA';
            lstOfAvlservc.add(avlServc2);

            SVMXC__Service__c avlServc3 = new SVMXC__Service__c ();
            avlServc3.Name = 'testAvlService3';
            avlServc3.PM_Plan_Template__c = pmplanTemp.id;
            avlServc3.Level__c ='System';
            avlServc3.Counter_Condition__c ='test';
            avlServc3.Product__c = objprod2.id;
            avlServc3.Count_Units__c = 'UN';
            lstOfAvlservc.add(avlServc3);
            insert lstOfAvlservc;

        SVMXC__Service_Level__c slaTerm = new SVMXC__Service_Level__c();
        slaTerm.Name = 'Elite';
        slaTerm.SVMXC__Restoration_Tracked_On__c = 'Case';
        slaTerm.SVMXC__Onsite_Response_Tracked_On__c ='Case';
        slaTerm.SVMXC__Resolution_Tracked_On__c = 'Case';
        insert slaTerm;

        SVMXC__Service_Contract__c servcContact = new SVMXC__Service_Contract__c();
        servcContact.Name = 'TestContract';
        servcContact.ERP_Sales_Org__c = 'salesorg';
        servcContact.Ext_Quote_number__c = 'TEST1234';
        servcContact.SVMXC__Start_Date__c = system.today().adddays(-365);
        servcContact.SVMXC__End_Date__c = system.today();
        insert servcContact;
        

        //Create Account
            Account testAcc = AccountTestData.createAccount();
            testAcc.AccountNumber = 'AA787799';
            testAcc.CSS_District__c = 'Test';
            insert testAcc;

        //Create Location
            SVMXC__Site__c loc = new SVMXC__Site__c(SVMXC__Account__c = testAcc.Id, Name = 'Test Location');
            insert loc;

        

        //Create Installed Product
            SVMXC__Installed_Product__c testInstallProduct = SR_testdata.createInstalProduct();
            testInstallProduct.ERP_Functional_Location__c = 'USA';
            testInstallProduct.SVMXC__Serial_Lot_Number__c ='test';
            insert testInstallProduct;

        //Create Service Contract  
            SVMXC__Service_Contract__c testServiceContract = Sr_testdata.createServiceContract();
            testServiceContract.SVMXC__Service_Level__c = slaTerm.id;
            testServiceContract.SVMXC__Company__c = testAcc.Id;
            testServiceContract.Location__c =loc.id;
            testServiceContract.SVMXC__Start_Date__c = system.today();
            testServiceContract.SVMXC__End_Date__c = system.today().addDays(365);
            testServiceContract.Ext_Quote_number__c = 'TEST1234';
            insert testServiceContract;

        //Covered Product
            SVMXC__Service_Contract_Products__c testContractProduct = new SVMXC__Service_Contract_Products__c();
            testContractProduct.SVMXC__Installed_Product__c = testInstallProduct.id;
            testContractProduct.SVMXC__Service_Contract__c = testServiceContract.id;
            testContractProduct.SVMXC__SLA_Terms__c =  slaTerm.id;
            testContractProduct.Cancelation_Reason__c = null;
            testContractProduct.SVMXC__Start_Date__c = system.Today().addDays(-15);
            testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(-17);
            testContractProduct.Serial_Number_PCSN__c = 'test';
            insert testContractProduct;

            List<SVMXC__SLA_Detail__c> lstOfSLADetail = new List<SVMXC__SLA_Detail__c>(); 

        SVMXC__SLA_Detail__c slaDetail1 = new SVMXC__SLA_Detail__c();
        slaDetail1.SVMXC__SLA_Terms__c = slaTerm.id;
        slaDetail1.Service_Maintenance_Contract__c = servcContact.id;
        slaDetail1.SVMXC__Available_Services__c = lstOfAvlservc[0].id;
        slaDetail1.status__c ='Valid';
        lstOfSLADetail.add(slaDetail1);

        SVMXC__SLA_Detail__c slaDetail2 = new SVMXC__SLA_Detail__c();
        slaDetail2.SVMXC__SLA_Terms__c = slaTerm.id;
        slaDetail2.SVMXC__Available_Services__c = lstOfAvlservc[1].id;
        slaDetail2.Service_Maintenance_Contract__c = testServiceContract.id;
        slaDetail2.status__c ='Valid';
        lstOfSLADetail.add(slaDetail2);

        SVMXC__SLA_Detail__c slaDetail3 = new SVMXC__SLA_Detail__c();
        slaDetail3.SVMXC__SLA_Terms__c = slaTerm.id;
        slaDetail3.SVMXC__Available_Services__c = lstOfAvlservc[2].id;
        slaDetail3.Service_Maintenance_Contract__c = testServiceContract.id;
        slaDetail3.status__c ='Valid';

        lstOfSLADetail.add(slaDetail3);
        insert lstOfSLADetail;

        //SR_ClassSLADetailUtillTrigger.CreatePMP(lstOfSLADetail);
        //SR_ClassSLADetailUtillTrigger.createCounterDetail(lstOfSLADetail);
        lstOfSLADetail[0].SVMXC__Available_Services__c = lstOfAvlservc[1].id;
        lstOfSLADetail[0].status__c = '';
        lstOfSLADetail[0].Service_Maintenance_Contract__c= testServiceContract.id;
        update lstOfSLADetail;

        lstOfSLADetail[0].SVMXC__Available_Services__c = lstOfAvlservc[1].id;
        lstOfSLADetail[0].status__c = 'Valid';
        lstOfSLADetail[0].Service_Maintenance_Contract__c= testServiceContract.id;
        update lstOfSLADetail;
        // Counter Detail
            SVMXC__Counter_Details__c  varCounterDetail = new SVMXC__Counter_Details__c();
            varCounterDetail.SVMXC__Service_Maintenance_Contract__c = testServiceContract.id;
            varCounterDetail.SVMXC__Counter_Name__c='test';
            varCounterDetail.SVMXC__Coverage_Limit__c = null;
            varCounterDetail.SVMXC__Counters_Covered__c = 5;

        //SR_ClassSLADetailUtillTrigger.CreatePMP(lstOfSLADetail);
        //SR_ClassSLADetailUtillTrigger.createCounterDetail(lstOfSLADetail);
    }
}
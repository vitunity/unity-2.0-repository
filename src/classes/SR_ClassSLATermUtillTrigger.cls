/**************************************************************************
 *    Author                : Priti Jaiswal
 *    Created Date          : 30/01/2014
 *    Description           : This class includes logic related to SLA Terms. Method declared are called in Before and After trigger of SLA Terms.
 *    Last Modified By      : Nikita Gupta
 *    Last Modified on      : 6/4/2014
 *    Last Modified Reason  : US4592, to update Business Hours on SLA Terms
 *    Last Modified By      : Nikita Gupta
 *    Last Modified on      : 6/12/2014
 *    Last Modified Reason  : US4592, updating query on Business Hours to Database.query
 *    Last Modified By      : Yukti Jain
 *    Last Modified on      : 10/03/2015
 *    Last Modified Reason  : Code Optimization for SR_ClassSLATermUtillTrigger
****************************************************************************/
public class SR_ClassSLATermUtillTrigger
{
    //To update Service Offering when SLA term is updated (US3811, added by priti(30/01/2014))
    public void updateServiceOffering(map<id, SVMXC__Service_Level__c> mapNewSLATerm)
    {
        Map<ID, SVMXC__Service_Offerings__c> mapOfSrvcOffToUpdate = new Map<ID, SVMXC__Service_Offerings__c>();
        List<SVMXC__Service_Offerings__c> lstOfSrvcOff = new List<SVMXC__Service_Offerings__c>();
      
        if(mapNewSLATerm.size() > 0)
        {
            lstOfSrvcOff = [select id, SLA_Term__c, SVMXC__Travel_Discount_Covered__c, SVMXC__Labor_Discount_Covered__c, After_Hours_Discount_Covered__c, Holidays_Discount_Covered__c from SVMXC__Service_Offerings__c where SLA_Term__c in : mapNewSLATerm.keySet()];

            if(lstOfSrvcOff.size() > 0)
            {
		        for(SVMXC__Service_Offerings__c servcOff :  lstOfSrvcOff)
		        {
                    if(mapNewSLATerm.containsKey(servcOff.SLA_Term__c))
                    {
			            //if ERP Travel included is set on SLA Term the set service offering's Travel Discount Covered to 100 
			                        if(servcOff.SLA_Term__c != null && mapNewSLATerm.get(servcOff.SLA_Term__c).ERP_Travel_Included__c != null)
			            {
			                servcOff.SVMXC__Travel_Discount_Covered__c=100;         
			                            mapOfSrvcOffToUpdate.put(servcOff.ID, servcOff);                        
			            }
			            //if ERP Contract level is 'STDLBR' set Labor Discount Covered to 100
			                        if(servcOff.SLA_Term__c != null && mapNewSLATerm.get(servcOff.SLA_Term__c).ERP_Contract_Level__c == 'STDLBR')
			            {
			                servcOff.SVMXC__Labor_Discount_Covered__c=100;          
			                            mapOfSrvcOffToUpdate.put(servcOff.ID, servcOff);
			            }
			             //if ERP Contract level is 'OTLBR' set After Hours Discount Covered to 100
			                        if(servcOff.SLA_Term__c != null && mapNewSLATerm.get(servcOff.SLA_Term__c).ERP_Contract_Level__c == 'OTLBR' )
			            {
			                servcOff.After_Hours_Discount_Covered__c=100;           
			                            mapOfSrvcOffToUpdate.put(servcOff.ID, servcOff);
			            }
			            //if ERP Contract level is 'HDLBR' set After Hours Discount Covered to 100 and Holidays Discount Covered to 100
			                        if( servcOff.SLA_Term__c != null && mapNewSLATerm.get(servcOff.SLA_Term__c).ERP_Contract_Level__c == 'HDLBR')
			            {
			                servcOff.Holidays_Discount_Covered__c=100;  
			                servcOff.After_Hours_Discount_Covered__c=100;               
			                            mapOfSrvcOffToUpdate.put(servcOff.ID, servcOff);
			            }
                    }
		        }
            }
        }
        if(mapOfSrvcOffToUpdate.size() > 0)
        {
            update mapOfSrvcOffToUpdate.values();
        }
    }

    //To update Business hours on SLA Term from business hours of contract... (US3811, added by priti(30/01/2014))
    public void UpdateBusinessHoursOfSLATerm(List<SVMXC__Service_Level__c> lstOfSLATerms, map<id, SVMXC__Service_Level__c> mapOldSLATerm)
    {
        /*** uncommented setOfERPContractNO and setOFServcContracts for 1a-1c merge by Bushra on 14-05-2015 ***/
        Set<Decimal> setOfERPContractNO = new Set<Decimal>(); //set of ERP Contract numbers
        Set<Id> setOFServcContracts = new Set<Id>(); // set of service_Contracts
        set<String> setERPContactlevel = new Set<String>(); //set of ERP_Contract_Level
        set<String> setHOLLBR_ERPConLevel = new Set<String>();  //set of ERP_Contract_Level having text value as HOLLBR 
        Set<ID> setSLATermID = new Set<ID>(); // set of Sla Term Ids
        Set<String> setCountrySAPCode = new Set<String>(); // set of country SAP code
        Set<String> setERPTimeZone = new Set<String>(); // set of ERP timezone
        Set<ID> setBusinessHours = new Set<ID>(); // set of businessHours
       
        Map<String, Service_Contract_Hours__c> mapERPContactLevel_ServiceContrctHours = new Map<String, Service_Contract_Hours__c>(); //map of ERP contract level and Service_Contract_Hours
        Map<String, ID> mapNameBusinessHourID = new Map<String, ID>(); // map of businessHours name and ID
        Map<String, Id> mapIDBusinessHours = new Map<String, Id>() ; //map of BusinessHours by Timezone since these are all day schedules
        /*** uncommented mapOfERPContrctNOAndServicContracts and mapOFContrcatIdAndBusinessHr for 1a-1c merge by Bushra on 14-05-2015 ***/
        Map<Decimal,ID> mapOfERPContrctNOAndServicContracts = new Map<Decimal, Id>(); // map of ERP contract numbers and service contract 
        Map<Id, Id> mapOFContrcatIdAndBusinessHr = new Map<Id, Id>(); // map of service_Contract and BusinessHours
        Map<ID, SVMXC__Service_Contract_Products__c> mapSLATermID_CovProd = new Map<ID, SVMXC__Service_Contract_Products__c>(); // Map of slaTerm and Covered products
        
        list<SVMXC__Service_Contract_Products__c> listCoveredProd = new list<SVMXC__Service_Contract_Products__c>(); //list of covered products
        list<Service_Contract_Hours__c> listServiceContractHours = new list<Service_Contract_Hours__c>(); // list of ServiceContractHours
        //List<Service_Contract_Period__c> lstsrvcContractPeriod = new List<Service_Contract_Period__c>(); // list of serviceContractPeriod
        List<SVMXC__Service_Contract__c> lstOfSrvcContract = new List<SVMXC__Service_Contract__c>(); // list of service contract
        
        FINAL Time timeBH = Time.newInstance(00, 00, 0, 00);  //00:00:00.000Z ; //Time variable to store a particular time for business hour.
        FINAL String STDLBR = system.label.SLATerm_ERP_Contract_Level_STDLBR;  //text value 'STDLBR' for ERP Contract Level
        FINAL String OTLBR = system.label.SLATerm_ERP_Contract_Level_OTLBR; //text value 'OTLBR' for ERP Contract Level
        FINAL String HOLLBR = system.label.SLATerm_ERP_Contract_Level_HOLLBR; //text value 'HOLLBR' for ERP Contract Level
        FINAL String VarianWeekend = system.label.BusinessHour_Weekend_NonStandard;
        
        for(SVMXC__Service_Level__c slaTerm : lstOfSLATerms)
        {
            /*** uncommented below if condition for 1a-1c merge by Bushra on 14-05-2015 ***/
            if(slaTerm.ERP_Contract_Nbr__c != null) //check added by nikita on 5/30/2014
            {
                setOfERPContractNO.add(slaTerm.ERP_Contract_Nbr__c);
            }
            if(slaTerm.SVMXC__Business_Hours__c == null || slaTerm.ERP_Contract_Level__c == null || (mapOldSLATerm == null || 
                (mapOldSLATerm != null && (slaTerm.ERP_Contract_Level__c != mapOldSLATerm.get(slaTerm.id).ERP_Contract_Level__c || mapOldSLATerm.get(slaTerm.id).ERP_Contract_Level__c != null)) || 
                (slaTerm.ERP_Contract_Hours__c != null && (slaTerm.ERP_Contract_Hours__c != mapOldSLATerm.get(slaTerm.id).ERP_Contract_Hours__c || mapOldSLATerm.get(slaTerm.id).ERP_Contract_Hours__c != null)))) // condition added 
                {
                    if(slaTerm.ERP_Contract_Level__c == HOLLBR)
                    {
                        setHOLLBR_ERPConLevel.add(slaTerm.ERP_Contract_Level__c);
                    }
                    else
                    {
                        if(slaTerm.ERP_Contract_Level__c != null)
                            setERPContactlevel.add(slaTerm.ERP_Contract_Level__c);
                    else
                        setERPContactlevel.add(STDLBR);
                    
                    system.debug('123456789'+setERPContactlevel);
                    
                    if(slaTerm.id != null)
                    {   
                            setSLATermID.add(slaTerm.id);
                    }
                    
                        system.debug('VNGJJHGHJ'+setSLATermID);
                    }
                }
            }   
            
        if(setHOLLBR_ERPConLevel.size() > 0)
        {
            list<BusinessHours> listBusinessHours = [Select id, name from BusinessHours where name = :VarianWeekend];// update
            
            if(listBusinessHours.size() > 0 && listBusinessHours != null) // null check added by Yukti
            {
                for(BusinessHours BH : listBusinessHours)
                {
                    mapNameBusinessHourID.put(BH.name, BH.Id);
                    system.debug('@#$%%^%$#@#'+mapNameBusinessHourID);
                }
            }
            
            if(mapNameBusinessHourID.size() > 0)
            {
                for(SVMXC__Service_Level__c SL : lstOfSLATerms)
                {
                    if(SL.ERP_Contract_Level__c == HOLLBR && mapNameBusinessHourID.containsKey(VarianWeekend) && mapNameBusinessHourID.get(VarianWeekend) != null)
                    {
                        SL.SVMXC__Business_Hours__c = mapNameBusinessHourID.get(VarianWeekend);
                        system.debug('!@#slaTerm.SVMXC__Business_Hours__c'+ SL.SVMXC__Business_Hours__c);
                    }
                }
            }
        }
        
            if(setERPContactlevel.size() > 0 && setSLATermID.size() > 0)
            {
                //limit added to the query as it was returning more than 50000 rows - Anupam 12/10/14
            listCoveredProd = [Select ID, SVMXC__SLA_Terms__c, Location__c, SVMXC__Service_Contract__r.SVMXC__Service_Level__c, Location__r.SVMXC__Account__c, Location__r.SVMXC__Account__r.ERP_Timezone__c, Location__r.Country__c, Location__r.Country__r.SAP_Code__c 
                                from SVMXC__Service_Contract_Products__c 
                                where SVMXC__SLA_Terms__c in :setSLATermID 
                                OR SVMXC__Service_Contract__r.SVMXC__Service_Level__c in :setSLATermID  limit 49999];
                system.debug('listCoveredProd!@$#%'+listCoveredProd);
                
            if(listCoveredProd.size() > 0 && listCoveredProd != null)   // null check added by Yukti
                {
                    for(SVMXC__Service_Contract_Products__c varCP : listCoveredProd)
                    {
                    if (varCP.SVMXC__SLA_Terms__c != null)
                        mapSLATermID_CovProd.put(varCP.SVMXC__SLA_Terms__c, varCP);
                    else
                        mapSLATermID_CovProd.put(varCP.SVMXC__Service_Contract__r.SVMXC__Service_Level__c, varCP);
                    if(varCP.Location__r.Country__r.SAP_Code__c != null)
                        setCountrySAPCode.add(varCP.Location__r.Country__r.SAP_Code__c);
                        
                    	if(varCP.Location__r.SVMXC__Account__r.ERP_Timezone__c != null)
                        	setERPTimeZone.add(varCP.Location__r.SVMXC__Account__r.ERP_Timezone__c);
                        system.debug('13123214234'+mapSLATermID_CovProd);
                        system.debug('678462'+setCountrySAPCode);
                        system.debug('67457542'+setERPTimeZone);
                    }
                }   
                
                if(setCountrySAPCode.size() > 0 && setERPTimeZone.size() > 0)
                {
                listServiceContractHours = [select id, name, Business_Hours__c, Business_Hours__r.TimeZoneSidKey, Country_Code__c, ERP_time_zone__c from Service_Contract_Hours__c where Country_Code__c in :setCountrySAPCode AND ERP_time_zone__c in :setERPTimeZone];
                    system.debug('2q3423542listServiceContractHours'+listServiceContractHours);
                    
                if(listServiceContractHours.size() > 0 && listServiceContractHours != null) // null check added by Yukti
                    {
                    Set <String> setTimeZoneSidKey = new Set<String> ();
                        for(Service_Contract_Hours__c varSCH : listServiceContractHours)
                        {
                            mapERPContactLevel_ServiceContrctHours.put(varSCH.name, varSCH);
                        
                        if(varSCH.Business_Hours__c != null) {
                            setBusinessHours.add(varSCH.Business_Hours__c);
                            setTimeZoneSidKey.add(varSCH.Business_Hours__r.TimeZoneSidKey);
                        }  
                            system.debug('123mapERPContactLevel_ServiceContrctHours'+mapERPContactLevel_ServiceContrctHours);
                            system.debug('5425setBusinessHours'+setBusinessHours);
                        }
                        
                        if(setBusinessHours.size() > 0)
                        {
                        String queryBusinessHour = 'SELECT Id, TimeZoneSidKey FROM BusinessHours WHERE (TimeZoneSidKey in :setTimeZoneSidKey )AND(MondayStartTime = ' + timeBH + 'AND MondayEndTime = '+ timeBH +') AND(TuesdayStartTime = '+ timeBH + 'AND  TuesdayEndTime = '+ timeBH +') AND(WednesdayStartTime = ' + timeBH + 'AND WednesdayEndTime = ' + timeBH + ') AND (ThursdayStartTime = '+ timeBH +' AND ThursdayEndTime = '+ timeBH +') AND(FridayStartTime = '+ timeBH + 'AND FridayEndTime = '+ timeBH+ ')'; 
                                            
                                List<BusinessHours> lstBusinessHours = Database.query(queryBusinessHour);
                                System.debug('!@#@#$@#$%' +lstBusinessHours);
                               
                        if(lstBusinessHours != null && lstBusinessHours.size() > 0) // null check added by Yukti
                                {
                                    for(BusinessHours BH : lstBusinessHours)
                                    {
                                mapIDBusinessHours.put(BH.TimeZoneSidKey, BH.id);
                                    }
                                }
                            
                        }
                    }
                    
                    if(mapSLATermID_CovProd.size() > 0 && mapERPContactLevel_ServiceContrctHours.size() > 0)
                    {
                        for(SVMXC__Service_Level__c slaTerm : lstOfSLATerms)
                    {   system.debug(mapSLATermID_CovProd.get(slaTerm.ID)); 
                        if((slaTerm.SVMXC__Business_Hours__c == null || slaTerm.ERP_Contract_Level__c == null) ||  (mapSLATermID_CovProd.containsKey(slaTerm.ID)) && (mapERPContactLevel_ServiceContrctHours.containsKey(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c))) // condition added (slaTerm.ERP_Contract_Level__c != null)
                        {
                            if(slaTerm.ERP_Contract_Level__c == STDLBR || slaTerm.ERP_Contract_Level__c == null)
                            {
                                if(slaTerm.ERP_Contract_Hours__c != null &&  mapSLATermID_CovProd.get(slaTerm.ID) != null &&  mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c != null && mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c != null && mapERPContactLevel_ServiceContrctHours.get(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c) != null)  
                                {
                                    slaTerm.SVMXC__Business_Hours__c = mapERPContactLevel_ServiceContrctHours.get(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c).Business_Hours__c;
                                        system.debug('!2346slaTerm.SVMXC__Business_Hours__c'+slaTerm.SVMXC__Business_Hours__c);
                                    }
                                }
                                else
                                {
                                if(slaTerm.ERP_Contract_Level__c == OTLBR && mapIDBusinessHours != null && slaTerm.ERP_Contract_Hours__c != null && mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c != null && mapERPContactLevel_ServiceContrctHours.get(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c) != null
                                    && mapIDBusinessHours.containskey(mapERPContactLevel_ServiceContrctHours.get(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c).Business_Hours__r.TimeZoneSidKey))
                                    {
                                    slaTerm.SVMXC__Business_Hours__c = mapIDBusinessHours.get(mapERPContactLevel_ServiceContrctHours.get(slaTerm.ERP_Contract_Hours__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.Country__r.SAP_Code__c+mapSLATermID_CovProd.get(slaTerm.ID).Location__r.SVMXC__Account__r.ERP_Timezone__c).Business_Hours__r.TimeZoneSidKey);
                                        system.debug('!#%$%slaTerm.SVMXC__Business_Hours__c'+slaTerm.SVMXC__Business_Hours__c);
                                    }
                                } 
                            }   
                        }
                    }
                }   
            }

        /***** Uncommented below if condition for 1a-1c merge, By Bushra on 14-05-2015 *****/
        /* Future release
        if(setOfERPContractNO.size() > 0) //Need to ask
        {
            //get Active Service Contract Period where erp contract no is same SLA Term erp contract no
            lstsrvcContractPeriod = [select id, ERP_Contract_Nbr__c, Service_Maintenance_Contract__c from Service_Contract_Period__c where Start_Date__c <= Today and End_Date__c >= Today and ERP_Contract_Nbr__c in : setOfERPContractNO];
            
            if(lstsrvcContractPeriod != null && lstsrvcContractPeriod.size() > 0) // check added by Yukti
            {
                //creating map of ERP Contract no and Service Contract
                for(Service_Contract_Period__c contPeriod : lstsrvcContractPeriod)
            {
                mapOfERPContrctNOAndServicContracts.put(contPeriod.ERP_Contract_Nbr__c, contPeriod.Service_Maintenance_Contract__c);
                setOFServcContracts.add(contPeriod.Service_Maintenance_Contract__c);
            }
            }
            if(setOFServcContracts != null && setOFServcContracts.size() > 0) // check added by Yukti
            {
                //get list of Service Contract
                lstOfSrvcContract = [Select id, SVMXC__Business_Hours__c from SVMXC__Service_Contract__c where id in:setOFServcContracts];
                if(lstOfSrvcContract != null && lstOfSrvcContract.size() > 0) // check added by Yukti
                {   
                    //creating map of Service Contract And Business hours
                    for( SVMXC__Service_Contract__c cont : lstOfSrvcContract)
                    {
                        mapOFContrcatIdAndBusinessHr.put(cont.id, cont.SVMXC__Business_Hours__c);
                    }
                }
                //updating SLA  Business Hours with Service Contract's business hours
                for(SVMXC__Service_Level__c  slaTerm:lstOfSLATerms)
                { 
                    if(slaTerm.ERP_Contract_Nbr__c!=null)
                    {
                        if(mapOfERPContrctNOAndServicContracts.get(slaTerm.ERP_Contract_Nbr__c)!=null)
                        {
                            slaTerm.SVMXC__Business_Hours__c=mapOFContrcatIdAndBusinessHr.get(mapOfERPContrctNOAndServicContracts.get(slaTerm.ERP_Contract_Nbr__c));  
                        }
                    }
                }
            }
        } */  //End of Wave 1 code re-factor Parul (01/23/2014) 
    }
}
@isTest

public class SR_ClassSLATermUtillTrigger_Test 
{
    Static testmethod void SR_ClassSLATermTest()
    {
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
        insert accObj;
        
        //insert country
        Country__c countryObj = new Country__c(Name = 'Australia', SAP_Code__c = 'AUS');
        insert countryObj;
        
        //insert location record
         SVMXC__Site__c loc = new SVMXC__Site__c(Name='test location', SVMXC__Street__c ='60 N McCarthy', SVMXC__Country__c ='United States', SVMXC__Zip__c= '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c  = accObj.id, ERP_Reference__c='0975', Country__c = countryObj.id, ERP_Sold_To_Code__c = '12345678');
        insert loc;
 
        BusinessHours listBusinessHours = [Select id, name from BusinessHours where isDefault = true limit 1];
        
        list<Service_Contract_Hours__c> listHour = new list<Service_Contract_Hours__c>();
        Service_Contract_Hours__c conHour =  new Service_Contract_Hours__c(Name = 'STDLBR', Business_Hours__c =listBusinessHours.id, Country_Code__c = 'AUS', ERP_time_zone__c = 'AUSSA');
        listHour.add(conHour);
        Service_Contract_Hours__c conHour1 =  new Service_Contract_Hours__c(Name = 'HDLBR', Business_Hours__c =listBusinessHours.id, Country_Code__c = 'AUS', ERP_time_zone__c = 'AUSSA');
        listHour.add(conHour1);
        insert listHour;
            
        
         SVMXC__Service_Plan__c servcPlan = new SVMXC__Service_Plan__c();
        servcPlan.Name = 'TestPlan';
        insert servcPlan;
        
        SVMXC__Service_Level__c slaTerm1 = new SVMXC__Service_Level__c();
        slaTerm1.Name = 'Elite';
        slaTerm1.ERP_Contract_Level__c = 'STDLBR';       
        slaTerm1.ERP_Travel_Included__c = 'travelincluded';
        slaTerm1.ERP_Contract_Nbr__c = 123;
        slaTerm1.ERP_Contract_Hours__c='10';
        insert slaTerm1;
        
        //insert installed product
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'test1IP';
        ip1.SVMXC__Serial_Lot_Number__c = 'sr123';
        insert ip1;
        
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c=accObj.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+5, SVMXC__Installed_Product__c = ip1.id, SVMXC__SLA_Terms__c  = slaTerm1.id, Location__c= loc.id);
        insert testServiceContractProduct;
        
        //insert service offering
        SVMXC__Service_Offerings__c servcOff1 = new SVMXC__Service_Offerings__c();
        servcOff1.SLA_Term__c = slaTerm1.id;
        servcOff1.SVMXC__Service_Plan__c = servcPlan.id;
        insert servcOff1;
        
        slaTerm1.ERP_Contract_Hours__c = '12';
        update slaTerm1;
 
        slaTerm1.ERP_Contract_Level__c = 'OTLBR';
        update slaTerm1;
        
        slaTerm1.ERP_Contract_Hours__c = '11';
        slaTerm1.ERP_Contract_Level__c = 'HOLLBR';
        update slaTerm1;
        
        slaTerm1.ERP_Contract_Hours__c = '10';
        slaTerm1.ERP_Contract_Level__c = 'STDLBR';
        update slaTerm1;
        
        
        slaTerm1.ERP_Contract_Level__c = 'HDLBR';
        update slaTerm1;
      
    }
    public static testMethod void coverUpdateBH()
    {
        ERP_Timezone__c erpTZ = new ERP_Timezone__c();
        erpTZ.Name = 'AUSSA';
        erpTZ.Salesforce_timezone__c = 'Central Summer Time (South Australia)';
        insert erpTZ;
        
         //insert country
        Country__c con = new Country__c(Name = 'Australia', SAP_Code__c = 'AUS');
        insert con;
        
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account acc = new Account();
        acc.Name = 'Test';
        acc.Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId();
        acc.AccountNumber = '2345';
        acc.ERP_Timezone__c = 'AUSSA';
        acc.country__c = 'Australia';
        acc.Country1__c = con.id;
        insert acc;
        
         //insert location record
         SVMXC__Site__c loc = new SVMXC__Site__c(ERP_Sold_To_Code__c = '2345', Name='test location', SVMXC__Street__c ='60 N McCarthy', SVMXC__Country__c ='Australia', SVMXC__Zip__c= '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c  = acc.id, Country__c = con.id);
        insert loc; 
        
        
        BusinessHours listBusinessHours = [Select id, name from BusinessHours where isDefault = true limit 1];
        //BusinessHours listBusinessHours = [Select id, name from BusinessHours where Name = 'Weekend and Non-Standard Business Hours(24x7x365)' limit 1];
       
        Service_Contract_Hours__c conHour =  new Service_Contract_Hours__c(Name = 'OTLBR', Business_Hours__c =listBusinessHours.id, Country_Code__c = 'AUS', ERP_time_zone__c = 'AUSSA');
        insert conHour;
        
    
        SVMXC__Service_Level__c slaTerm1 = new SVMXC__Service_Level__c();
        slaTerm1.Name = 'Elite';
        slaTerm1.ERP_Contract_Level__c = 'HDLBR';    
        slaTerm1.SVMXC__Restoration_Tracked_On__c = 'Case';
        slaTerm1.SVMXC__Onsite_Response_Tracked_On__c = 'Case';
        slaTerm1.SVMXC__Resolution_Tracked_On__c = 'WorkOrder';
        slaTerm1.ERP_Travel_Included__c = 'asfghj';
        slaTerm1.ERP_Contract_Nbr__c = 123;
        slaTerm1.ERP_Contract_Hours__c = '10';
        //slaTerm1.SVMXC__Active__c = true;
        //slaTerm1.SVMXC__Effective_Date__c = System.today().addMonth(2);
        insert slaTerm1;
        
        //insert installed product
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'sr123';
        ip1.SVMXC__Serial_Lot_Number__c = 'sr123';
        insert ip1;
        
    
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c = acc.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+5, SVMXC__Installed_Product__c = ip1.id, Serial_Number_PCSN__c = ip1.Name, SVMXC__SLA_Terms__c  = slaTerm1.id, Location__c= loc.id);
        insert testServiceContractProduct;
        
        slaTerm1.ERP_Contract_Level__c = 'OTLBR'; 
        slaTerm1.ERP_Contract_Hours__c = '11';
        update slaTerm1;
        
    }
    

}
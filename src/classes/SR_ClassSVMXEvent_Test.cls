@isTest(SeeAllData = true)
public class SR_ClassSVMXEvent_Test
{
    //Record Type Id of Technician/Equipment (Technician)
    public static Id recTypeIdTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static Account varAcc;
    public static SVMXC__Site__c varLoc;
    public static SVMXC__Service_Group__c varServTeam;
    public static SVMXC__Service_Group_Members__c varTech1;
    public static SVMXC__Service_Group_Members__c varTech;
    public static SVMXC__Installed_Product__c varIP;
    public static SVMXC__Service_Order__c varPWO;
    public static SVMXC__Service_Order__c varWO;

    static
    {
        //Dummy data crreation 
        /*Profile sysadminprofile = [Select id from profile where name = 'System Administrator'];
        User varSysADMN = [Select id from user where profileId = :sysadminprofile.id AND isActive = true AND id != :userInfo.getUserId() limit 1];*/

        //InsertAccount record
        varAcc = AccountTestData.createAccount();
        insert varAcc;

        //Insert Location record
        varLoc = SR_testdata.createsite();
        varLoc.SVMXC__Account__c = varAcc.Id;
        varLoc.Location_Capacity__c = 2;
        insert varLoc;

        //Insert Service Team record
        varServTeam = new SVMXC__Service_Group__c();
        varServTeam.Name = 'Test Service Group';
        //varServTeam.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert varServTeam;
        
        varTech1 = SR_testdata.createTech();
        varTech1.SVMXC__Email__c = 'standarduser@testorg.com';
        varTech1.SVMXC__Service_Group__c = varServTeam.Id;
        varTech1.User__c = userInfo.getUserid();
        varTech1.RecordTypeId = recTypeIdTechnician;
        insert varTech1;

        //Insert Technician record
        varTech = SR_testdata.createTech();
        varTech.Name = 'Test Tech';
        varTech.User__c = userInfo.getUserid();
        varTech.SVMXC__Service_Group__c = varServTeam.Id;
        /*varTech.SVMXC__Street__c = 'Test Street';
        varTech.SVMXC__City__c = 'Test City';
        varTech.SVMXC__State__c = 'Test State';
        varTech.SVMXC__Zip__c = '302015';
        varTech.SVMXC__Country__c = 'India';*/
        varTech.SVMXC__Inventory_Location__c = varLoc.Id;
        varTech.SVMXC__Email__c = 'standarduser@testorg.com';
        varTech.RecordTypeId = recTypeIdTechnician;
        insert varTech;

        //Insert Installed Product record
        varIP = new SVMXC__Installed_Product__c();
        varIP.Name = 'H11111';
        varIP.SVMXC__Preferred_Technician__c = varTech.Id;
        varIP.SVMXC__Status__c = 'Installed';
        varIP.SVMXC__Site__c = varLoc.Id;
        insert varIP;

        varPWO = new SVMXC__Service_Order__c();
        varPWO.SVMXC__Company__c = varAcc.Id;
        varPWO.SVMXC__Group_Member__c = varTech.Id;
        varPWO.SVMXC__Order_Status__c = 'Open';
        varPWO.SVMXC__Dispatch_Response__c = 'Assigned';
        varPWO.SVMXC__Site__c = varLoc.Id;
        varPWO.SVMXC__Company__c = varAcc.Id;
        varPWO.SVMXC__Problem_Description__c = 'Work Order Problem Description';
        varPWO.SVMXC__Preferred_Start_Time__c = System.now();
        varPWO.SVMXC__Preferred_End_Time__c = System.now().addHours(3);
        varPWO.Event__c = true;
        insert varPWO;

        //Insert Work Order record
        varWO = new SVMXC__Service_Order__c();
        varWO.SVMXC__Company__c = varAcc.Id;
        varWO.SVMXC__Top_Level__c = varIP.Id;
        varWO.SVMXC__Group_Member__c = varTech.Id;
        varWO.SVMXC__Order_Status__c = 'Open';
        varWO.SVMXC__Dispatch_Response__c = 'Assigned';
        //varWO.SVMXC__Site__c = varLoc.Id;
        //varWO.SVMXC__Company__c = varAcc.Id;
        varWO.SVMXC__Problem_Description__c = 'Work Order Problem Description';
        varWO.Parent_WO__c = varPWO.Id;
        varWO.Event__c = false;
        insert varWO;

        Sales_Order__c so1 = new Sales_Order__c(Pricing_Date__c = Date.today(), name = 'acme');
        insert so1;

        Sales_Order_Item__c soi = new Sales_Order_Item__c(Status__c = 'Open', Sales_Order__c = so1.Id);
        insert soi;

        SVMXC__Service_Order_Line__c varWD = new SVMXC__Service_Order_Line__c();
        varWD.SVMXC__Consumed_From_Location__c = varLoc.id;
        varWD.SVMXC__Activity_Type__c = 'Install';
        varWD.Completed__c = false;
        varWD.SVMXC__Group_Member__c = varTech.id;
        varWD.Purchase_Order_Number1__c = '12345';
        varWD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        varWD.SVMXC__Work_Description__c = 'test';
        varWD.SVMXC__Start_Date_and_Time__c = System.now();
        varWD.Actual_Hours__c = 5;
        varWD.SVMXC__Service_Order__c = varWO.id;
        varWD.Sales_Order_Item__c = soi.id;
        insert varWD;

    }

    public static testMethod void SR_ClassSVMXEventTest()
    {
        Test.startTest();
        SVMXC__SVMX_Event__c varEve = new SVMXC__SVMX_Event__c();
        varEve.Name = 'Test Event';
        varEve.SVMXC__Service_Order__c = varPWO.Id;
        varEve.SVMXC__WhatId__c = varPWO.Id;
        varEve.SVMXC__Technician__c = varTech1.id;
        varEve.SVMXC__StartDateTime__c = System.now();
        varEve.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve.SVMXC__DurationInMinutes__c = 180;
        varEve.Booking_Type__c = 'On Site Training Confirmed';
        insert varEve;

        //Update ServiceMax Event record
        varEve.SVMXC__Service_Order__c = varWO.Id;
        varEve.SVMXC__WhatId__c = varWO.Id;
        //varEve.Status__c = 'Cancelled';
        varEve.SVMXC__Technician__c = varTech.Id;
        varEve.Booking_Type__c = 'Helpdesk';
        varEve.SVMXC__EndDateTime__c = System.now().addDays(15);
        varEve.SVMXC__DurationInMinutes__c = 21600;
        varEve.Booking_Type__c = '';
        update varEve;

        SVMXC__SVMX_Event__c varEve1 = new SVMXC__SVMX_Event__c();
        varEve1.Name = 'Test Event';
        varEve1.SVMXC__Service_Order__c = varPWO.Id;
        varEve1.SVMXC__WhatId__c = varPWO.Id;
        varEve1.SVMXC__Technician__c = varTech1.id;
        varEve1.SVMXC__StartDateTime__c = System.now();
        varEve1.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve1.SVMXC__DurationInMinutes__c = 180;
        varEve1.Booking_Type__c = 'Helpdesk';
        insert varEve1;
        List<SVMXC__SVMX_Event__c> events = new List<SVMXC__SVMX_Event__c>();
        events.add(varEve);

        List<SVMXC__SVMX_Event__c> listSVMXCEvent;
        Map<ID, SVMXC__SVMX_Event__c> oldMapSVMXCEvent = new Map<ID, SVMXC__SVMX_Event__c>();
        oldMapSVMXCEvent.put(varEve.Id, varEve1);
        SR_ClassSVMXEvent srEvent = new SR_ClassSVMXEvent();
        srEvent.setDescription(events);
        srEvent.updateSVMXCEvent(events, oldMapSVMXCEvent);
        srEvent.onDeleteAction(events);
        Test.stopTest();
    }

    public static testMethod void SR_ClassSVMXEventTest1()
    {
        Test.startTest();
        SVMXC__SVMX_Event__c varEve = new SVMXC__SVMX_Event__c();
        varEve.Name = 'Test Event';
        varEve.SVMXC__Service_Order__c = varPWO.Id;
        varEve.SVMXC__WhatId__c = varPWO.Id;
        varEve.SVMXC__Technician__c = varTech1.id;
        varEve.SVMXC__StartDateTime__c = System.now();
        varEve.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve.SVMXC__DurationInMinutes__c = 180;
        varEve.Booking_Type__c = 'Helpdesk';
        varEve.Technician_s_Email__c = 'test@salesforce.com';
        insert varEve;

        SVMXC__SVMX_Event__c varEve1 = new SVMXC__SVMX_Event__c();
        varEve1.Name = 'Test Event';
        varEve1.SVMXC__Service_Order__c = varWO.Id;
        varEve1.SVMXC__WhatId__c = varWO.Id;
        varEve1.SVMXC__Technician__c = varTech.id;
        varEve1.SVMXC__StartDateTime__c = System.now();
        varEve1.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve1.SVMXC__DurationInMinutes__c = 180;
        varEve1.Booking_Type__c = 'Helpdesk';
        insert varEve1;
        SR_ClassSVMXEvent srEvent = new SR_ClassSVMXEvent();
        Map<Id,Id> TechId2UserMap = new Map<Id,Id>();
        TechId2UserMap.put(varTech.Id, userInfo.getUserid());
        TechId2UserMap.put(varTech1.Id, userInfo.getUserid());
        List<SVMXC__SVMX_Event__c> SVMXEvent = new List<SVMXC__SVMX_Event__c>();
        SVMXEvent.add(varEve);
        SVMXEvent.add(varEve1);
        Map<ID, SVMXC__SVMX_Event__c> mapNewSVMXEvent = new Map<ID, SVMXC__SVMX_Event__c>();
        Map<ID, SVMXC__SVMX_Event__c> mapOldSVMXEvent  = new Map<ID, SVMXC__SVMX_Event__c>();
        Set<ID> setWorkOrderIDs = new Set<ID>();
        setWorkOrderIDs.add(varWO.Id);

        mapNewSVMXEvent.put(varEve.Id, varEve);
        mapOldSVMXEvent.put(varEve.Id, varEve1);
        Map<ID, SVMXC__SVMX_Event__c> mapNewSVMXEvent1 = null;
        //srEvent.CreateTechnicianEvent(TechId2UserMap, SVMXEvent, mapNewSVMXEvent, mapOldSVMXEvent);
        //srEvent.CreateTechnicianEvent(TechId2UserMap, SVMXEvent, mapNewSVMXEvent1, mapOldSVMXEvent);
        srEvent.updatedWorkOrder(setWorkOrderIDs, mapNewSVMXEvent1, mapOldSVMXEvent);
        Test.stopTest();
    }

    public static testMethod void SR_ClassSVMXEventTest2()
    {
        Test.startTest();
        varWO.SVMXC__Site__c = null;
        update varWO;
        SVMXC__SVMX_Event__c varEve1 = new SVMXC__SVMX_Event__c();
        varEve1.Name = 'Test Event';
        varEve1.SVMXC__Service_Order__c = varWO.Id;
        varEve1.SVMXC__WhatId__c = varWO.Id;
        varEve1.SVMXC__Technician__c = varTech1.id;
        varEve1.SVMXC__StartDateTime__c = System.now();
        varEve1.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve1.SVMXC__DurationInMinutes__c = 180;
        varEve1.Booking_Type__c = 'Helpdesk';
        insert varEve1;
        //delete varEve1;
        Test.stopTest();
    }
      public static testMethod void SR_ClassSVMXEventTest4()
    {
        Test.startTest();
        SVMXC__SVMX_Event__c varEve = new SVMXC__SVMX_Event__c();
        varEve.Name = 'Test Event';
        varEve.SVMXC__Service_Order__c = varPWO.Id;
        varEve.SVMXC__WhatId__c = varPWO.Id;
        varEve.SVMXC__Technician__c = varTech1.id;
        varEve.SVMXC__StartDateTime__c = System.now();
        varEve.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve.SVMXC__DurationInMinutes__c = 180;
        varEve.Booking_Type__c = 'Helpdesk';
        insert varEve;

        SVMXC__SVMX_Event__c varEve1 = new SVMXC__SVMX_Event__c();
        varEve1.Name = 'Test Event';
        varEve1.SVMXC__Service_Order__c = varWO.Id;
        varEve1.SVMXC__WhatId__c = varWO.Id;
        varEve1.SVMXC__Technician__c = varTech.id;
        varEve1.SVMXC__StartDateTime__c = System.now();
        varEve1.SVMXC__EndDateTime__c = System.now().addHours(3);
        varEve1.SVMXC__DurationInMinutes__c = 180;
        varEve1.Booking_Type__c = 'Helpdesk';
        insert varEve1;
        SR_ClassSVMXEvent srEvent = new SR_ClassSVMXEvent();
        Map<Id,Id> TechId2UserMap = new Map<Id,Id>();
        TechId2UserMap.put(varTech.Id, userInfo.getUserid());
        TechId2UserMap.put(varTech1.Id, userInfo.getUserid());
        List<SVMXC__SVMX_Event__c> SVMXEvent = new List<SVMXC__SVMX_Event__c>();
        SVMXEvent.add(varEve);
        SVMXEvent.add(varEve1);
        Map<ID, SVMXC__SVMX_Event__c> mapNewSVMXEvent = new Map<ID, SVMXC__SVMX_Event__c>();
        Map<ID, SVMXC__SVMX_Event__c> mapOldSVMXEvent  = new Map<ID, SVMXC__SVMX_Event__c>();
        Set<ID> setWorkOrderIDs = new Set<ID>();
        setWorkOrderIDs.add(varWO.Id);

        mapNewSVMXEvent.put(varEve.Id, varEve);
        mapOldSVMXEvent.put(varEve.Id, varEve1);
        Map<ID, SVMXC__SVMX_Event__c> mapNewSVMXEvent1 = null;
        //srEvent.CreateTechnicianEvent(TechId2UserMap, SVMXEvent, mapNewSVMXEvent, mapOldSVMXEvent);
        //srEvent.CreateTechnicianEvent(TechId2UserMap, SVMXEvent, mapNewSVMXEvent1, mapOldSVMXEvent);
        srEvent.updatedWorkOrder(setWorkOrderIDs, mapNewSVMXEvent1, mapOldSVMXEvent);
        varWO.SVMXC__Order_Status__c = 'Cancelled';
        varWO.Cancellation_Reason__c = 'Test Cancellation';
        varWO.Cancel__c = true;
        update varWO;
        Test.stopTest();
    }
}
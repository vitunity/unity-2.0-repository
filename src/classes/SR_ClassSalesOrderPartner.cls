/*************************************************************************\
      @ Author              : Priti Jaiswal
      @ Date                : 19-Feb-2014
      @ Description         : Include logic on insert/update of Sales Order Partner. Method declared are called from before and after trigger of Sales Order Partner.
      @ Last Modified By    :  Shubham Jaiswal
      @ Last Modified On    :  9-Apr-2014   
      @ Last Modified Reason:  US4170 : Replaced Sales_Order_Number__c & Sales_Order_Item__c with ERP_Reference__c 

****************************************************************************/

public class SR_ClassSalesOrderPartner
{

    //priti, US4030, 19/02/2014 - To update Sales Order, Sales Order Item, ERP Partner, Contact of Sales Order Partner
    
    public static void updateSOP(List<Sales_Order_Partner__c> lstOfSOPToUpdate)
    {
        //variable declaration
        Set<String> setOfSONO = new Set<String>();
        Set<String> setOfSOINO = new Set<String>();
        Set<String> setOfERPPartnerCode = new Set<String>();
        Set<String> setOfERPContactNO = new Set<String>();
        Map<String, id> mapOfSONameAndId = new Map<String, id>();
        Map<String, id> mapOfSOINameAndId = new Map<String, id>();
        Map<String, id> mapOfERPPartNameAndId = new Map<String, id>();
        Map<String, id> mapOfConERPRefAndConId = new Map<String, id>();
        
        
        for(Sales_Order_Partner__c SOP : lstOfSOPToUpdate)
        {
            if(SOP.Sales_Order_Number__c != null)           
                setOfSONO.add(SOP.Sales_Order_Number__c); 
                
            if(SOP.Sales_Order_Item_Number__c != null)
                setOfSOINO.add(SOP.Sales_Order_Item_Number__c);         
                
            if(SOP.ERP_Partner_Code__c != null)
                setOfERPPartnerCode.add(SOP.ERP_Partner_Code__c);
            
            if(SOP.ERP_Contact_Number__c != null)
                setOfERPContactNO.add(SOP.ERP_Contact_Number__c);           
        }
    
        //creating map of Sales Order Name and id
        if(setOfSONO!=null)
        {
            //List<Sales_Order__c> lstofSO = [Select id, Name from Sales_Order__c where Name in : setOfSONO]; //code replaced for US4170 with 
            List<Sales_Order__c> lstofSO = [Select id, ERP_Reference__c from Sales_Order__c where ERP_Reference__c in : setOfSONO];
            if(lstofSO.size()>0)
            {
                for(Sales_Order__c so : lstofSO)
                {
                    //mapOfSONameAndId.put(so.Name, so.id);   //code replaced for US4170 with 
                    mapOfSONameAndId.put(so.ERP_Reference__c, so.id);   
                }
            }
        }
        
        //creating map of Sales Order Item Name and id
        if(setOfSOINO!=null)
        {
          //List<Sales_Order_Item__c> lstofSOI = [Select id, Name from Sales_Order_Item__c where Name in : setOfSOINO]; code replaced for US4170 with
            List<Sales_Order_Item__c> lstofSOI = [Select id, ERP_Reference__c from Sales_Order_Item__c where Name in : setOfSOINO];
            
            if(lstofSOI.size()>0)
            {
                for(Sales_Order_Item__c soi : lstofSOI)
                {
                    //mapOfSOINameAndId.put(soi.Name, soi.id); //code replaced for US4170 with
                    mapOfSOINameAndId.put(soi.ERP_Reference__c, soi.id);
                }
            }
        }
        
        //creating map of ERP Partner Name and id
        if(setOfERPPartnerCode!=null)
        {
            List<ERP_Partner__c> lstofERPPartnerCode = [Select id, Name from ERP_Partner__c where Name in : setOfERPPartnerCode];
            
            if(lstofERPPartnerCode.size()>0)
            {
                for(ERP_Partner__c erpPart : lstofERPPartnerCode)
                {
                    mapOfERPPartNameAndId.put(erpPart.Name, erpPart.id);
                }
            }
        }
        
        //creating map of Contact ERP Reference and  Contact ID
        if(setOfERPContactNO!=null)
        {
            List<Contact> lstofContact = [Select id, ERP_Reference__c from Contact where ERP_Reference__c in : setOfERPContactNO];
            
            if(lstofContact.size()>0)
            {
                for(Contact con : lstofContact)
                {
                    mapOfConERPRefAndConId.put(con.ERP_Reference__c, con.id);
                }
            }
        }
        
        for(Sales_Order_Partner__c SOP : lstOfSOPToUpdate)
        {
            //update Sales Order
            if(SOP.Sales_Order_Number__c!= null && mapOfSONameAndId.get(SOP.Sales_Order_Number__c) != null)
                SOP.Sales_Order__c = mapOfSONameAndId.get(SOP.Sales_Order_Number__c);
                
            //update Sales Order Item
            if(SOP.Sales_Order_Item_Number__c != null && mapOfSOINameAndId.get(SOP.Sales_Order_Item_Number__c) != null)
                SOP.Sales_Order_Item__c = mapOfSOINameAndId.get(SOP.Sales_Order_Item_Number__c); 
                
            //update ERP Partner
            if(SOP.ERP_Partner_Code__c != null && mapOfERPPartNameAndId.get(SOP.ERP_Partner_Code__c) != null)
                SOP.ERP_Partner__c = mapOfERPPartNameAndId.get(SOP.ERP_Partner_Code__c);
                
            //update Contact
            if(SOP.ERP_Contact_Number__c != null && mapOfConERPRefAndConId.get(SOP.ERP_Contact_Number__c) != null)
                SOP.Contact__c = mapOfConERPRefAndConId.get(SOP.ERP_Contact_Number__c);
        }       
    }

}
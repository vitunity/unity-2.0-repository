/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_ClassSalesOrderPartner_Test {

    public static BusinessHours businesshr;
    public static Account newAcc;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static List<Contact> newCont;
    public static Sales_Order_Partner__c SOPartner;
    public static ERP_Partner__c erpPartner;
    
    static {
        newAcc = UnityDataTestClass.AccountData(true);
        newProd = UnityDataTestClass.product_Data(false);
        SO = UnityDataTestClass.SO_Data(false,newAcc);
        SO.ERP_Reference__c = SO.Name;
        insert SO;
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        newCont = new List<Contact>();
        newCont = [SELECT Id, ERP_Reference__c FROM Contact WHERE ERP_Reference__c != null LIMIT 1];
        if(newCont.isEmpty()) {
            Contact cont = UnityDataTestClass.contact_data(false, newAcc);
            cont.ERP_Reference__c = '9999';
            cont.MailingState = 'Washington';
            insert cont;
            newCont.add(cont);
        }
        
        erpPartner = UnityDataTestClass.createERPPartner(true, newAcc);
        
        SOPartner = UnityDataTestClass.SOPartner_Data(true, SO, SOI, newCont[0], erpPartner);
    }
    
    static testMethod void updateSOP_Test() {
        Test.StartTest();
            List<Sales_Order_Partner__c> SOPartnerList = new List<Sales_Order_Partner__c>();
            SOPartnerList.add(SOPartner); 
            SR_ClassSalesOrderPartner.updateSOP(SOPartnerList);
        Test.StopTest();
    }
}
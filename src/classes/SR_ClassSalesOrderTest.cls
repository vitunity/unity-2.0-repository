@isTest(seeAllData=true)
private class SR_ClassSalesOrderTest {
    
    static testMethod void testUpdateSalesOrderOnQuote() {
        
        Sales_Order__c salesOrder = insertQuoteAndSalesOrder();
        
        BigMachines__Quote__c salesQuote = [Select Id,Sales_Order__c,SAP_Booked_Sales__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Sales_Order__c salesOrder1 = [Select Id,Quote__c From Sales_Order__c Where Id =: salesOrder.Id];
        
        System.assertEquals(salesQuote.Sales_Order__c, salesOrder.Id);
        System.assertEquals(salesQuote.SAP_Booked_Sales__c, salesOrder.Name);
        System.assertEquals(salesOrder1.Quote__c, salesQuote.Id);
    }
    
    static private Sales_Order__c insertQuoteAndSalesOrder(){
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        Sales_Order__c salesOrder = new Sales_Order__c();
        salesOrder.Name = 'Test SalesOrder';
        salesOrder.Ext_Quote_Number__c = SR_PrepareOrderControllerTest.salesQuote.Name;
        salesOrder.ERP_Sold_To__c = 'Sales1212';
        salesOrder.ERP_Site_Partner__c = 'Sales11111';
        insert salesOrder;
        return salesOrder;
    }
}
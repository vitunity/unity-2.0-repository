/*************************************************************************\      
@ Author                : Shubham Jaiswal     
@ Date                  : 25/03/2014      
@ Description           : US4286       
@ Last Modified By      : 
@ Last Modified On      : 
@ Last Modified Reason  : 
/*************************************************************************/
public class SR_ClassUser
{   
    
    // for US4286
    public void PopulateCountry(List<User> UsLst,Set<String> ErpCountry )
    {
        Map<String,String> mapSapCode2Name = new Map<String,String>(); // Map on Country Object
        List<Country__c> countrylst = new  List<Country__c>([Select Name,SAP_Code__c From Country__c Where Name IN :ErpCountry or SAP_Code__c IN :ErpCountry]);
        if(countrylst.size() > 0)
        {
            for(Country__c count : countrylst)
            {
                    mapSapCode2Name.put(count.Name,count.Name);
                    if(count.SAP_Code__c != Null)
                        mapSapCode2Name.put(count.SAP_Code__c,count.Name);
            }
        }
        if(mapSapCode2Name.size() > 0)
        {
            for(User us : UsLst)
            {
                us.Country = mapSapCode2Name.get(us.ERP_Country__c);
            }
        }
    }
        
}
/***
This class cover SR_ClassUser and before trigger 
***/
@isTest
private class SR_ClassUser_Test
{
    static testMethod void myController()
    {
            
            Profile p1= [SELECT Id FROM Profile WHERE Name='System administrator'];
            List<Country__c> objCount = new List<Country__c>();
            list<User> listUser = new list<User>();

            /****** country Trigger******/
            //data for Language object
            Language__c lang = new Language__c();
            lang.name = 'English';
            insert lang;
            Language__c lang1 = new Language__c();
            lang1.name = 'French';
            insert lang1;
            
            //data for Review Products object
            Review_Products__c rp = new Review_Products__c(Product_Profile_Name__c = 'test profile ');
             insert rp;
             
             //data for country object

            Country__c c1 = new Country__c();
            c1.Name = 'USA';
            c1.SAP_Code__c = 'USA';
            c1.Default_Primary_Language__c = 'English';
            objCount.add(c1);

            Country__c c2 = new Country__c();
            c2.Name = 'India';
            c2.SAP_Code__c = 'India';
            objCount.add(c2);

            insert objCount;
             c1.Default_Primary_Language__c = 'French';
            update c1;
             /****** country Trigger******/  
            //data for User Record 
            User u2 = new User(Alias = 'newUser', Email='bushraTest@wipro.com',
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US', ProfileId = p1.Id,
                             TimeZoneSidKey='America/Los_Angeles',UserName='demoAbc@wipro.com', ERP_Country__c = 'USA'); 
            listUser.add(u2);       

            User u3 = new User(Alias = 'newUser', Email='bush@wipro.com',
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                             LocaleSidKey='en_US', ProfileId = p1.Id,
                             TimeZoneSidKey='America/Los_Angeles',UserName='demoB@wipro.com',ERP_Country__c = 'India'); 
            listUser.add(u3);   

            insert listUser ;
        
        }
}
@isTest(seeAllData=true)
public class SR_ClassWorkOrderDetail_Test
{
    public static ID TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();  
    public static ID hdRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();  
    public static ID FieldSeRecordTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();  
    public static ID UsageConRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
    public static ID ProdServRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();  
    public static Profile pr = [Select id from Profile where name = 'VMS Service - User'];    
    public static User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
    
    public static Account objAcc;
    public static Country__c con;
    public static Contact objCont;
    public static SVMXC__Installed_Product__c objIns;
    public static ERP_Timezone__c  objERP;
    public static Product2 objProd;
    public static SVMXC__Site__c varSite;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static ERP_Org__c varErp ;
    public static SVMXC__Service_Group_Members__c ObjTech;
    public static Case ObjCase;
    public static SVMXC__Service_Order__c objWO;
    public static SVMXC__Service_Level__c objSLATerm;
    public static SVMXC__Service_Order_Line__c objWD;
    public static SVMXC__Service_Contract__c varSC;
    public static SVMXC__Service_Offerings__c objSO;
    public static SVMXC__Service_Plan__c objSp;
    
    static{
    
    //insert Country record
    con = new Country__c(Name = 'United States');
    insert con;
    
    //insert ERP Timezone Record
    objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
    insert objERP;
    
    //insert account Record
    objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id);
    insert objAcc;
   
    //insert Contact Record
    objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
    insert objCont;
    
    //insert Product Record
    objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
    insert objProd;
    
    //insert location Record
     varSite = new SVMXC__Site__c(Sales_Org__c = '0600', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = objAcc.id);
     insert varSite;
     
     //insert sla term
     objSLATerm = new SVMXC__Service_Level__c(Name = 'Test SLA Term', SVMXC__Active__c = true, SVMXC__Restoration_Tracked_On__c = 'WorkOrder');
     insert objSLATerm;
     
    objSp = new SVMXC__Service_Plan__c(Name = 'test', SVMXC__Active__c = true, SVMXC__SLA_Terms__c = objSLATerm.id);
     insert objSp;
     //insert Service contract
    varSC = new SVMXC__Service_Contract__c(SVMXC__Company__c = objAcc.id, ERP_Contract_Type__c = 'ZH3', SVMXC__Service_Level__c = objSLATerm.id);
    insert varSC;
    
    objSO= new SVMXC__Service_Offerings__c(SLA_Term__c = objSLATerm.id, SVMXC__Labor_Discount_Covered__c = 100.00, SVMXC__Service_Plan__c = objSp.id);
    insert objSO;
     //insert Service Team Record
     ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(), SVMXC__Group_Code__c = 'ABC');
    insert ObjSvcTeam;
    
    //insert ERP org Record
    varErp = new ERP_Org__c(Name = '0600', Return_All__c = false, Sales_Org__c = '0600');
    insert varErp;
    
    //insert technician Record
    ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true, SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com', SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId(),ERP_Work_Center__c='H9876545321');
    insert ObjTech; 
    
    //insert Insalled Product Record
    objIns = new SVMXC__Installed_Product__c(SVMXC__Service_Contract__c = varSC.id, SVMXC__Site__c = varSite.ID, Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H1234', SVMXC__Company__c = objAcc.id, Name = 'H1234', SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id);
    insert objIns;
    
    ObjCase = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, SVMXC__Top_Level__c = objIns.id, Priority = 'Medium');
    insert objCase;
    }
    
    public static testMethod void testUsageConsum()
    {
        test.StartTest();
        list<SVMXC__Service_Order_Line__c> listWorkDetail1 = new list<SVMXC__Service_Order_Line__c>();
        Map<ID, SVMXC__Service_Order_Line__c> mapOldWorkDetail1= new Map<ID, SVMXC__Service_Order_Line__c>();

        STB_Master__c stb = new STB_Master__c();
        stb.name = 'STB-XX-9999';
        stb.Job_Number__c = '1234';
        stb.Product_Codes_Affected__c = '19,29';
        stb.Type__c ='1';
        stb.is_Open__c = True;
        insert stb;
        system.assertNotEquals(null,stb.id);
                
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Case__c=objCase.id, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id,SVMXC__Group_Member__c=ObjTech.id);
        insert objWO;
        SVMXC__Service_Order_Line__c objps = [select id, name, recordtypeId, SVMXC__Serial_Number__c, STB_Master__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c = :objWO.id];
        System.debug('objps+======='+objps);
        
        objWD = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c= objWo.id, SVMXC__Work_Detail__c = objps.id, RecordTypeId = UsageConRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, STB_Master__c = stb.Id,   SVMXC__Start_Date_and_Time__c =  System.now(),
        SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered',
        Mod_Status__c='V - Verified',Method__c='In Person', SVMXC__Line_Status__c = 'Approved', Interface_Status__c = 'Processed');
        insert objWD;
        
        objWD = [select id, name, STB_Master__c,SVMXC__Line_Status__c, Interface_Status__c, Mod_status__c, Method__c, 
                        SVMXC__Serial_Number__c, SVMXC__Work_Detail__c, SVMXC__Group_Member__c
                 from SVMXC__Service_Order_Line__c where id = :objWD.id];
        system.assertNotEquals(null,objWD.STB_master__c);
        
        List<STB_Mod__c> modcards = [select id, name, stb_master__c, Case_Number__c,Badge_Number__c, Completion_Date__c,Done_Remotely__c,Installed_Product__c,Mod_Status__c,PCSN__c,Rep_Name__c,Status__c,STB_Mod_Name__c,work_Order_Number__c 
        	from STB_Mod__c where work_Details__c = :objWd.id];
        
        /*system.assertNotEquals(0,modcards.size());
        system.assertEquals(stb.id,modcards[0].stb_master__c);
        system.assertNotEquals(null,modcards[0].case_number__c);
        system.assertNotEquals(null,modcards[0].Badge_Number__c);
        system.assertNotEquals(null,modcards[0].Completion_Date__c);
        system.assertEquals(false,modcards[0].Done_Remotely__c);
        system.assertEquals(objins.id,modcards[0].installed_product__c);
        system.assertEquals(objins.ERP_Reference__c,modcards[0].PCSN__c);
        system.assertEquals('V - Verified',modcards[0].Mod_Status__c);
        system.assertNotEquals(null,modcards[0].STB_Mod_Name__c);
        system.assertNotEquals(null,modcards[0].work_Order_Number__c);
        */
        //List<Optional_STB__c> optSTBs = [select id from Optional_STB__c where STB_Master__c = :Stb.id];
        //system.assertNotEquals(0,optSTBs.size());
        
        
        
        //stb.is_Open__c = false;
        //update stb;
        
       
        test.stopTest();
        
        
        
        
        SVMXC__Service_Order_Line__c objPTWDUC= new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
        SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', Old_Part_Number__c = objProd.id, SVMXC__Work_Detail__c = objps.id, Related_Activity__c = objWD.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id);
        insert objPTWDUC;
        //listWorkDetail1.add(objWDUC);
        
        STB_Reject__c rej = new STB_Reject__c();
        rej.PCSN__c = objIns.ERP_Reference__c;
        rej.STB_Name__c = 'STB-XX-9999';
        rej.Work_Detail_Name__c = objWD.name;
        rej.Rev_Date__c = System.today();
        insert rej;
        
        rej= [select id, STB_Mod__c, Installed_Product__c, STB_Master__c, Work_Detail__c from STB_Reject__c where id = :rej.id];
        /* 
        system.assertEquals(objins.id,rej.installed_product__c);
        system.assertEquals(objWD.id,rej.Work_Detail__c);
        system.assertEquals(stb.id,rej.STB_Master__c);
        system.assertNotEquals(null,rej.STB_Mod__c);*/
        //SRClassWorkOrderDetail objWDC =new SRClassWorkOrderDetail();
         //objWDC.updateFieldsOnWorkDetail(listWorkDetail1,null );
       // System.debug('-------->objWDUC'+objWDUC);
        
    }
        public static testMethod void testWDPopField()
    {
        test.StartTest();
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        SVMXC__Service_Order_Line__c obj = [select id, name from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c = :objWO.id];
        System.debug('obj+======='+obj);
        
        objWD = new SVMXC__Service_Order_Line__c(id = obj.id, SVMXC__Group_Member__c = ObjTech.id, SVMXC__Product__c = objProd.id, ERP_PCSN__c = 'H1234',SVMXC__Serial_Number__c = null, SVMXC__Start_Date_and_Time__c =  System.now(), SVMXC__Line_Type__c = 'Labor', SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Work_Description__c = 'Work', SVMXC__Actual_Quantity2__c = 2);
        update objWD;
        test.stopTest();
        
    }
    public static testMethod void testWDPopField1()
    {
        test.StartTest();
        Depot_Sourcing_Rules__c objDepo = new Depot_Sourcing_Rules__c(Name = 'test', Country__c = con.id, Depot_Priority__c = 2.20, Location__c = varSite.id);
        insert objDepo;
        
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id, Malfunction_Start__c = System.now(), Not_Covered__c = true);
        insert objWO;
        SVMXC__Service_Order_Line__c obj = [select id, name from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c = :objWO.id];
        System.debug('obj+======='+obj);
        
        objWD = new SVMXC__Service_Order_Line__c(id = obj.id, SVMXC__Group_Member__c = ObjTech.id, Old_Part_Number__c = objProd.id,  Service_Maintenance_Contract__c = varSC.id, SVMXC__Start_Date_and_Time__c =  System.now(),
        SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Parts', SVMXC__Serial_Number__c = objIns.id);
        update objWD;
        
        test.stopTest();
        list<SVMXC__Service_Order_Line__c> listWD= new list<SVMXC__Service_Order_Line__c>();
        listWD.add(objWD);
        SRClassWorkOrderDetail srObj = new SRClassWorkOrderDetail();
        //srObj.SetTolocationOnWD(listWD); savon/FF 
        
    }
    
    public static testMethod void shouldUpdateERP_NWAInfoWhenCompleted(){
        
        // having
        Test.startTest();
        // wo  SVMXC__Service_Order__c
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        
        // wd - prodserv Multiples__c = true; 
        SVMXC__Service_Order_Line__c wdProdServ = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.Id, Multiples__c = true, recordTypeId = ProdServRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, Old_Part_Number__c = objProd.id,  SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered');
        insert wdProdServ;
        // wd- usage 
        SVMXC__Service_Order_Line__c objWDUC = new SVMXC__Service_Order_Line__c( SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', SVMXC__Work_Detail__c = wdProdServ.id, Related_Activity__c = wdProdServ.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id, ERP_Serial_Number__c = 'H1234');
        insert objWDUC;
        //  ERP_NWA__c.Work_Detail__c => prod serv
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', Work_Detail__c = wdProdServ.Id);
        insert erpnwa;
        
        
        SVMXC__Service_Order_Line__c wdProdServDB = [select Multiples__c from SVMXC__Service_Order_Line__c where id = :wdProdServ.Id];
        ERP_NWA__c erpnwaDB = [select Work_Detail__c from ERP_NWA__c where id =:erpnwa.Id];
        System.assertEquals(true, wdProdServDB.Multiples__c);
        System.assertEquals(wdProdServ.Id, erpnwaDB.Work_Detail__c);
        
        // when
        // update usage.SVMXC__Line_Status__c to 'Completed'    
        objWDUC.Completed__c = true;
        update objWDUC;
        
        // then
        //productWD.Multiples__c == usageWD.Multiples__c;
        // ERP_NWA__c.Work_Detail__c => usage
        wdProdServDB = [select Multiples__c from SVMXC__Service_Order_Line__c where id = :wdProdServ.Id];
        erpnwaDB = [select Work_Detail__c from ERP_NWA__c where id =:erpnwa.Id];
        System.assertEquals(true, wdProdServDB.Multiples__c);
        System.assertEquals(wdProdServ.Id, erpnwaDB.Work_Detail__c);
        
    }
    
    public static testMethod void shouldUpdateERP_NWAInfoWhenApproved(){
        
        // having
        // wo  SVMXC__Service_Order__c
        Test.startTest();
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        // wd- usage  SVMXC__Service_Order_Line__c
        // wd - prodserv
        
        SVMXC__Service_Order_Line__c wdProdServ = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.Id, recordTypeId = ProdServRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, Old_Part_Number__c = objProd.id,  SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered');
        insert wdProdServ;
        
        SVMXC__Service_Order_Line__c objWDUC = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', SVMXC__Work_Detail__c = wdProdServ.id, Related_Activity__c = wdProdServ.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id, ERP_Serial_Number__c = 'H1234');
        insert objWDUC;
        //  ERP_NWA__c.Work_Detail__c => usage
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', Work_Detail__c = objWDUC.Id);
        insert erpnwa;
        
        
        SVMXC__Service_Order_Line__c objWDUCDB = [select SVMXC__Start_Date_and_Time__c, SVMXC__End_Date_and_Time__c from SVMXC__Service_Order_Line__c where id = :objWDUC.Id];
        //ERP_NWA__c erpnwaDB = [select Start_Date_Time__c, End_Date_Time__c from ERP_NWA__c where id =:erpnwa.Id];
        //System.assertEquals(null, erpnwaDB.Start_Date_Time__c);
        //System.assertEquals(null, erpnwaDB.End_Date_Time__c);
        // when
        // update usage.SVMXC__Line_Status__c == 'Approved' and wd.Completed__c to true
        objWDUC.SVMXC__Line_Status__c = 'Approved';
        objWDUC.Completed__c = true;
        update objWDUC;
        
        // then
        // Deprecated
        // erp_nwa.Start_Date_Time__c = WD.SVMXC__Start_Date_and_Time__c; 
        // erp_nwa.End_Date_Time__c = WD.SVMXC__End
        //erpnwaDB = [select Start_Date_Time__c, End_Date_Time__c from ERP_NWA__c where id =:erpnwa.Id]; 
        //System.assertEquals(objWDUCDB.SVMXC__Start_Date_and_Time__c, erpnwaDB.Start_Date_Time__c);
        //System.assertEquals(objWDUCDB.SVMXC__End_Date_and_Time__c, erpnwaDB.End_Date_Time__c);
        Test.stopTest();
    }
    
    public static testMethod void shouldSetRemainingHoursonWD(){
        
        // having
        // wo  SVMXC__Service_Order__c
        Test.startTest();
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        // wd- usage  SVMXC__Service_Order_Line__c
        // wd - prodserv
        
        SVMXC__Service_Order_Line__c wdProdServ = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.Id, recordTypeId = ProdServRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, Old_Part_Number__c = objProd.id,  SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered');
        insert wdProdServ;
        
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', Work_Detail__c = wdProdServ.Id);
        insert erpnwa;
        
        SVMXC__Service_Order_Line__c objWDUC = new SVMXC__Service_Order_Line__c(ERP_NWA__c = erpnwa.Id, SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
            Billing_Type__c = 'I – Installation', SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', SVMXC__Work_Detail__c = wdProdServ.id, Related_Activity__c = wdProdServ.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id, ERP_Serial_Number__c = 'H1234');
        insert objWDUC;
        
        SRClassWorkOrderDetail wdClass = new SRClassWorkOrderDetail();
        //wdClass.setRemainingHoursonWD(new List<SVMXC__Service_Order_Line__c>{objWDUC});

        Test.stopTest();
    }
    public static testMethod void shouldThrowErrorForChildDetailsOnDelete(){
        
        // having
        // wo  SVMXC__Service_Order__c
        Test.startTest();
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        // wd- usage  SVMXC__Service_Order_Line__c
        // wd - prodserv
        
        
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC');
        insert erpnwa;
        
        SVMXC__Service_Order_Line__c wdProdServ = new SVMXC__Service_Order_Line__c(ERP_NWA__c = erpnwa.Id, SVMXC__Service_Order__c = objWO.Id, recordTypeId = ProdServRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, Old_Part_Number__c = objProd.id,  SVMXC__Start_Date_and_Time__c =  System.now(),
            SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered');
        insert wdProdServ;
        
        SVMXC__Service_Order_Line__c objWDUC = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
            Billing_Type__c = 'I – Installation', SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', SVMXC__Work_Detail__c = wdProdServ.id, Related_Activity__c = wdProdServ.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id, ERP_Serial_Number__c = 'H1234');
        insert objWDUC;
        
        
        SRClassWorkOrderDetail wdClass = new SRClassWorkOrderDetail();
        wdClass.errorForChildDetailsOnDelete(new Map<Id, SVMXC__Service_Order_Line__c>(new List<SVMXC__Service_Order_Line__c>{wdProdServ}));

        Test.stopTest();
    }    
    
    public static testMethod void shouldGetRecordTypeIds() {
        //given
        
        //when
        /*SRClassWorkOrderDetail.getRecordTypeIDs();
        
        //then
        system.assert(SRClassWorkOrderDetail.prodServicedRecType != null);
        system.assert(SRClassWorkOrderDetail.usageConsumptionWDRecType != null);
        system.assert(SRClassWorkOrderDetail.ConsultingWORecType != null);
        system.assert(SRClassWorkOrderDetail.ProfessionalServicesWORecType != null);
        system.assert(SRClassWorkOrderDetail.IEMWORecType != null);
        system.assert(SRClassWorkOrderDetail.TrainingWORecType != null);
        system.assert(SRClassWorkOrderDetail.ProjectManagementWORecType != null);
        system.assert(SRClassWorkOrderDetail.fieldServcRecTypeID != null);
        system.assert(SRClassWorkOrderDetail.instlRecTypeID != null);
        system.assert(SRClassWorkOrderDetail.equipRecordTypeID != null);
        system.assert(SRClassWorkOrderDetail.counterDetailrecId != null);*/
    }
    
    public static testmethod void shouldCoverCode() {
        
        SVMXC__Service_Group__c srvcGrp = [select Id from SVMXC__Service_Group__c where Parts_Warranty_JO__c != null and Parts_Warranty_JO__c != '' limit 1];
        
        test.startTest();
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id);
        insert objWO;
        SVMXC__Service_Order_Line__c obj = [select id, name, SVMXC__Service_Group__c, Part_Warranty_Replacement__c, ERP_Service_Order__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c = :objWO.id];
        obj.Part_Warranty_Replacement__c = true;
        obj.SVMXC__Service_Group__c = srvcGrp.Id;
        obj.ERP_Service_Order__c = null;
        update obj;
        /*
        SVMXC__Service_Order_Line__c objWDUC= new SVMXC__Service_Order_Line__c(
            SVMXC__Service_Order__c = objWO.id, SVMXC__Work_Detail__c = obj.id, Related_Activity__c = obj.id, recordtypeId = UsageConRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, 
            SVMXC__Start_Date_and_Time__c =  System.now(), SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', 
            Covered_or_Not_Covered__c = 'Not covered', SVMXC__Serial_Number__c = objIns.id, ERP_Serial_Number__c = 'H1234', Part_Warranty_Replacement__c = true,
            SVMXC__Service_Group__c = srvcGrp.Id, ERP_Service_Order__c = null
        );
        insert objWDUC;
        */
        
        //varWD.Part_Warranty_Replacement__c == true && varWD.SVMXC__Service_Group__r.Parts_Warranty_JO__c != null && varWD.ERP_Service_Order__c == null
        
        test.stopTest();
        
        system.assert(obj.Part_Warranty_Replacement__c == true);
        system.assert(obj.SVMXC__Service_Group__c == srvcGrp.Id);
        system.assert(obj.ERP_Service_Order__c == null);
        
        //system.assertEquals(1, 0);
    }
    
    public static testmethod void convertMonthNameToNumberTest () {
    	SRClassWorkOrderDetail wod = new SRClassWorkOrderDetail ();
    	System.assertEquals('01',wod.convertMonthNameToNumber('Jan'));        
        System.assertEquals('02',wod.convertMonthNameToNumber('Feb'));
        System.assertEquals('03',wod.convertMonthNameToNumber('Mar'));
        System.assertEquals('04',wod.convertMonthNameToNumber('Apr'));
        System.assertEquals('05',wod.convertMonthNameToNumber('May'));   
        System.assertEquals('06',wod.convertMonthNameToNumber('Jun'));        
        System.assertEquals('07',wod.convertMonthNameToNumber('Jul'));
        System.assertEquals('08',wod.convertMonthNameToNumber('Aug'));
        System.assertEquals('09',wod.convertMonthNameToNumber('Sep'));
        System.assertEquals('10',wod.convertMonthNameToNumber('Oct'));  
        System.assertEquals('11',wod.convertMonthNameToNumber('Nov'));
        System.assertEquals('12',wod.convertMonthNameToNumber('Dec'));
    }
}
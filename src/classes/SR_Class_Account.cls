/****************************************************
      @ Last Modified By      :  Nikita Gupta
      @ Last Modified On      :  02-Jun-2014
      @ Last Modified Reason  :  Added null checks before all the set, list and map calls.
      @ Last Modified By :    Priti Jaiswal
      @ Last Modified on :    01-Aug-2014
      @ Last Modified Reason: DE833, Populate Country Lookup on Location from Country Picklist value
      @ Last Modified By :    Bushra
      @ Last Modified on :    09-Dec-2014
      @ Last Modified Reason: DE2216, update associate Location with address field ONLY IF the location type is NOT 'Field' or 'Depot'
****************************************************/

public class SR_Class_Account
{
    //Before trigger on Account, Priti, US3924, 06/02/104
    public static void updateAccountCuntry(List<Account> lsrOfAccount)
    {
        Set<String> setOfCountry = new Set<String>();
        Map<String, ID> mapOfCountryIDandName = new Map<String, Id>();

        for(Account acc : lsrOfAccount)
        {
            setOfCountry.add(acc.Country__c);
        }

        if(setOfCountry.size() > 0) // null check added by Nikita on 6/2/2014
        {   //where condition modified by priti for DE833, 01/08/2014
            List<Country__c> lstOfCountry = [select id, name, ISO_Code_3__c, SAP_Code__c from Country__c
                                            where (name in : setOfCountry OR ISO_Code_3__c in : setOfCountry or SAP_Code__c in : setOfCountry)];
            if(lstOfCountry.size() > 0)
            {
                for(Country__c country : lstOfCountry)
                {
                    //modified by priti for DE833, 01/08/2014
                    if(setOfCountry.contains(country.Name))
                    {
                        mapOfCountryIDandName.put(country.Name, country.ID);
                    }
                    else if(setOfCountry.contains(country.ISO_Code_3__c))
                    {
                        mapOfCountryIDandName.put(country.ISO_Code_3__c, country.ID);
                    }
                    else if(setOfCountry.contains(country.SAP_Code__c))
                    {
                        mapOfCountryIDandName.put(country.SAP_Code__c, country.ID);
                    }
                    //mapOfCountryIDandName.put(country.name,country.id);
                }
            }
            for(Account acc : lsrOfAccount)
            {
                //if(mapOfCountryIDandName.containsKey(acc.Country__c)) // null check added by Nikita on 6/2/2014
                acc.Country1__c = mapOfCountryIDandName.get(acc.Country__c);
            }
        }
    }

    //after trigger of Account, on update of Account
    public void UpdateLocationAddress(List<Account> newAccounts)
    {
        List<SVMXC__Site__c> ListofLocation = new List<SVMXC__Site__c>();
        List<SVMXC__Site__c> ListofUpdatedLocation = new List<SVMXC__Site__c>();
        Map<Id,Account> MapofAccount = new Map<Id,Account>();

        for(Account accnt: newAccounts)
        {
            MapofAccount.put(accnt.Id, accnt);
        }

        if(MapofAccount.size() > 0) // null check added by Nikita on 6/2/2014
        {
            ListofLocation = [Select SVMXC__City__c, SVMXC__Country__c, SVMXC__State__c, SVMXC__Street__c, SVMXC__Zip__c, SVMXC__Account__c, SVMXC__Location_Type__c 
                                                                            from SVMXC__Site__c where SVMXC__Account__c in :MapofAccount.keySet()];
            if(ListofLocation.size() > 0)
            {
                for(SVMXC__Site__c location : ListofLocation)
                {
                    if(location.SVMXC__Location_Type__c != 'Field' && location.SVMXC__Location_Type__c != 'Depot') // de2216 added by Bushra on 09-12-2014
                    {
                        if(MapofAccount.containsKey(location.SVMXC__Account__c)) // null check added by Nikita on 6/2/2014
                        {
                            Account acc = MapofAccount.get(location.SVMXC__Account__c);
                        
                            /*
                            if(acc.ShippingCity != Null && acc.ShippingStreet != Null && acc.ShippingState != Null && acc.ShippingPostalCode != Null && acc.ShippingCountry != Null)
                            {
                                location.SVMXC__City__c = acc.ShippingCity;
                                location.SVMXC__Country__c = acc.ShippingCountry;
                                location.SVMXC__State__c = acc.ShippingState;
                                location.SVMXC__Street__c = acc.ShippingStreet;
                                location.SVMXC__Zip__c = acc.ShippingPostalCode;
                                ListofUpdatedLocation.add(location);
                            }
                            if(acc.ShippingCity == Null && acc.ShippingStreet == Null && acc.ShippingState == Null && acc.ShippingPostalCode == Null && acc.ShippingCountry == Null && (acc.BillingCity != Null && acc.BillingStreet != Null && acc.BillingState != Null && acc.BillingPostalCode != Null && acc.BillingCountry != Null))
                            {
                                location.SVMXC__City__c = acc.BillingCity;
                                location.SVMXC__Country__c = acc.BillingCountry;
                                location.SVMXC__State__c = acc.BillingState;
                                location.SVMXC__Street__c = acc.BillingStreet;
                                location.SVMXC__Zip__c = acc.BillingPostalCode;
                                ListofUpdatedLocation.add(location);
                            }
                            if((acc.ShippingCity == Null && acc.ShippingStreet == Null && acc.ShippingState == Null && acc.ShippingPostalCode == Null && acc.ShippingCountry == Null) && (acc.BillingCity == Null && acc.BillingStreet == Null && acc.BillingState == Null && acc.BillingPostalCode == Null && acc.BillingCountry == Null))
                            {
                                location.SVMXC__City__c = '';
                                location.SVMXC__Country__c = '';
                                location.SVMXC__State__c = '';
                                location.SVMXC__Street__c = '';
                                location.SVMXC__Zip__c = '';
                                ListofUpdatedLocation.add(location);
                            } */

                        }
                    }
                }
                    if(ListofUpdatedLocation.size() > 0) // null check added by Nikita on 6/2/2014
                    {
                        update ListofUpdatedLocation;
                    }
            }
        }
    }
}
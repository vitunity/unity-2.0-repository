@isTest
public class SR_Class_Account_Test
{
    public static testMethod void SR_Class_AccountTest()
    {
        Map<Id, Account> mapAccount = new Map<Id, Account>();

        /******trigger for Account insert************************/
            //insert country record
            Country__c varC = new Country__c();
            varC.name = 'India';
            varC.ISO_Code_3__c = 'China';
            varC.SAP_Code__c = 'USA';
            insert varC;

            //insert Account record
            Account varAcc = AccountTestData.createAccount();
            varAcc.Country__c = 'India';
            System.debug('varAcc.Country__c@@@ ' + varAcc.Country__c);
            insert varAcc;

        /******trigger for Account update************************/

            //insert location record
            SVMXC__Site__c varLoc = SR_testdata.createsite();
            varLoc.SVMXC__Account__c = varAcc.Id;
            varLoc.ERP_CSS_District__c = 'Paharganj';
            insert varLoc;
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
            ip.SVMXC__Site__c = varLoc.id;
            ip.ERP_CSS_District__c = varLoc.ERP_CSS_District__c;
            insert ip;
            
            varLoc.ERP_CSS_District__c = 'Karol Bagh';
            update varLoc;
            
            /*for(Account acc : [select Id, Country__c, ShippingCity, ShippingStreet, ShippingState, ShippingPostalCode, ShippingCountry from Account
                                where Id = :varAcc.Id])
            { mapAccount.put(acc.Id, acc); }*/

            //update Account record 1
            //Account varAcc_1 = mapAccount.get(varAcc.Id);
            varAcc.Country__c = 'China';

            DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT = false;

            update varAcc;
            
            

            
            /*for(Account acc : [select Id, Country__c from Account where Id = :varAcc_1.Id])
            { System.debug('varAcc_1.country@@@@@ ' + acc.Country__c); }*/

            //update Account record 2
            //Account varAcc_2 = mapAccount.get(varAcc.Id);
            varAcc.Country__c = 'USA';
            varAcc.ShippingCity = 'Test City';
            varAcc.ShippingStreet = 'Test Street';
            varAcc.ShippingState = 'Test State';
            varAcc.ShippingPostalCode = 'Test State';
            varAcc.ShippingCountry = 'Test Country';

            DefaultAccountTeam.ACCOUNT_RECORD_UPDATED_FROM_ACCOUNT = false;

            update varAcc;
            
           //********************************************

            
            
    }
}
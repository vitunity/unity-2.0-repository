/*
 * Author Amitkumar Katre
 * Test class for 
 * CaseLine Object triggers and dependent classes.
 * 
 */
@isTest//(SeeAllData=true)
public with sharing class SR_Class_CaseLine_Test {
   
    public static ID TechRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Group_Members__c').get('Technician'); 
    public static ID hdRecordTypeID = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Helpdesk');
    public static profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
    
    public static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', 
    lastname='Testing',languagelocalekey='en_US', localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',
    username='standardtestuse92@testclass.com');
     
    public static Account objAcc;
    public static Contact objCont;
    public static product2 objProd;
    public static SVMXC__Installed_Product__c objIns; 
    public static Case objCase;
    public static SVMXC__Service_Level__c slaTerm ;
    public static SVMXC__Service_Contract__c testServiceContract;
    public static SVMXC__Service_Contract_Products__c testServiceContractProduct;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static SVMXC__Service_Group_Members__c ObjTech;
    
    static{
        //insert Account
        objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States',BillingCity='New York', BillingState = 'CA');
        insert objAcc;
        //insert Contact Record
        objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', 
        AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
        insert objCont;
        //insert Product Record
        objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
        insert objProd;
        //insert Service Team Record
        ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(),
        SVMXC__Group_Code__c = 'ABC');
        insert ObjSvcTeam;
        //insert technician Record
        ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true,
        SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com',
        SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId());
        insert ObjTech; 
        //insert Insalled Product Record
        objIns = new SVMXC__Installed_Product__c( Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H12345', 
        SVMXC__Company__c = objAcc.id, Name = 'H12345', 
        SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id,SVMXC__Status__c = 'Installed');
        insert objIns;
        // insert SLA term
        slaTerm = new SVMXC__Service_Level__c(Name = 'test sla Term', SVMXC__Active__c = true);
        insert slaTerm;
        //insert service contract
        testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today()-1,
        SVMXC__End_Date__c = system.today()+29, SVMXC__Service_Level__c= slaTerm.id, SVMXC__Company__c = objAcc.id, 
        ERP_Order_Reason__c = 'HS1', Quote_Reference__c = 'asdfghjk');
        insert testServiceContract;
        //insert Covered Product
        testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, 
        SVMXC__Start_Date__c =system.today()-1, SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345',
        SVMXC__Installed_Product__c = objIns.id);
        insert testServiceContractProduct;
        //insert Case
        ObjCase = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, 
                            SVMXC__Top_Level__c = objIns.id, Priority = 'Medium',ProductSystem__c  = objIns.id);
        insert objCase;
    }

    /*
     * Time Entry Case Line Test
     * Case Line Delete Test
     */
    public static testmethod void caseLineUnitTest1(){
        
        CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
        System.Test.StartTest();
        Sales_Order_Item__c so = new Sales_Order_Item__c(ERP_Item_Category__c='Z001',WBS_Element__c='text',End_Date__c=Date.today().adddays(3),Start_Date__c=Date.today());
        insert so;
        
        ERP_WBS__c ews = new ERP_WBS__c(Forecast_Start_Date__c=date.today(),Forecast_End_Date__c=Date.today().adddays(7),ERP_Reference__c='text');
        insert ews;
        Map<Id,SVMXC__Case_Line__c> oldmap = new Map<Id,SVMXC__Case_Line__c>();
        Map<Id,SVMXC__Case_Line__c> newmap = new Map<Id,SVMXC__Case_Line__c>();
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = ObjCase.id;
        caseLine.recordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Case_Line__c').get('Time_Entry');
        caseLine.SVMXC__Product__c = objProd.Id;
        caseLine.SVMXC__Installed_Product__c = objIns.Id;
        caseLine.Start_Date_time__c = system.now();
        caseLine.Hours__c = 1;
        caseLine.Case_Line_Owner__c = UserInfo.getUserId();
        caseLine.Billing_Type__c = 'C - Contract';
        caseLine.PO_Number__c = '1000002';
        caseLine.Sales_Order_Item__c = so.id;
        caseLine.Case_Line_WO_Type__c = 'Helpdesk';
        caseLine.SVMXC__Line_Status__c = 'Open';
        insert caseLine;
        
        oldmap.put(caseLine.id,caseLine);
        SVMXC__Case_Line__c caseLine1 = new SVMXC__Case_Line__c();
        caseLine1.id = caseLine.Id;
        caseLine1.SVMXC__Line_Status__c = 'Cancelled';
        update caseLine1;
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
            cllst.add(caseLine1);
        newmap.put(caseLine1.id,caseLine1);
        SR_Class_CaseLine sr = new SR_Class_CaseLine();
        Map<Id,case> mapcase = new Map<Id,case>();
        mapcase.put(objcase.id, objcase);
        
        sr.updateSOIfromERPWBS(new Set<Id>{so.id});
        
        sr.deleteOrderedInstalledProducts(oldmap,newmap);
        sr.updateCaseAndWOStatusOnCancel(cllst,oldMap);
        SR_Class_CaseLine.UpdateWorkOrderStatus(mapcase,null);
        delete caseLine;
        System.Test.StopTest();
    }
    
    public static testmethod void caseLineUnitTest2(){
        //System.Test.StartTest();
        CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
        
        
       // SVMXC__Service_Order__c ser = new SVMXC__Service_Order__c();
        
        

        Case ObjCase1 = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, 
                            SVMXC__Top_Level__c = objIns.id, Priority = 'Medium',ProductSystem__c  = objIns.id);
        System.Test.StartTest();
        insert objCase1;

       	
        Sales_Order_Item__c so = new Sales_Order_Item__c(ERP_Item_Category__c='Z001',WBS_Element__c='text',End_Date__c=Date.today().adddays(3),Start_Date__c=Date.today());
        insert so;
        
        
        ERP_Project__c erp = new ERP_Project__c();
        insert erp;
        
        ERP_WBS__c ews = new ERP_WBS__c(Forecast_Start_Date__c=date.today(),Forecast_End_Date__c=Date.today().adddays(7),ERP_Reference__c='text');
        insert ews;

        ERP_NWA__c nwa = new ERP_NWA__c(WBS_Element__c = ews.id,ERP_Status__c = 'REL;SETC',ERP_Std_Text_key__c = 'INST0002',ERP_Project_Nbr__c = '12555');
        insert nwa;
        
        SVMXC__Service_Order__c pwo = new SVMXC__Service_Order__c();
        pwo.recordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        pwo.Event__c = true;
        pwo.SVMXC__Group_Member__c = ObjTech.Id;
        insert pwo;

        SVMXC__Service_Order__c Iwo = new SVMXC__Service_Order__c();
        Iwo.recordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        Iwo.Event__c = false;
        Iwo.SVMXC__Group_Member__c = ObjTech.Id;
        Iwo.Parent_WO__c = pwo.id;
        insert Iwo;

        SVMXC__Service_Order_Line__c pswdl = new SVMXC__Service_Order_Line__c();
        pswdl.recordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        pswdl.ERP_NWA__c = nwa.id;
        pswdl.SVMXC__Service_Order__c = iwo.id;
        insert pswdl;

        

        //System.Test.StartTest();
        Map<Id,SVMXC__Case_Line__c> oldmap = new Map<Id,SVMXC__Case_Line__c>();
        Map<Id,SVMXC__Case_Line__c> newmap = new Map<Id,SVMXC__Case_Line__c>();
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = ObjCase1.id;
        caseLine.recordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Case_Line__c').get('Time_Entry');
        caseLine.SVMXC__Product__c = objProd.Id;
        caseLine.SVMXC__Installed_Product__c = objIns.Id;
        caseLine.Start_Date_time__c = system.now().addDays(-1);
        caseLine.Hours__c = 1;
        caseLine.Case_Line_Owner__c = UserInfo.getUserId();
        caseLine.Billing_Type__c = 'I - Installation';
        caseLine.ERP_NWA__c = nwa.id;
        caseLine.PO_Number__c = '1000002';
        caseLine.Sales_Order_Item__c = so.id;
        caseLine.Case_Line_WO_Type__c = 'Installation';
        caseLine.ERP_Project__c = erp.id;
        caseLine.SVMXC__Line_Status__c = 'Open';
        insert caseLine;
        oldmap.put(caseLine.id,caseLine);
        caseLine.SVMXC__Line_Status__c = 'Cancelled';
        update caseLine;
        newmap.put(caseLine.id,caseLine);
        SR_Class_CaseLine sr = new SR_Class_CaseLine();
        sr.updateSOIfromERPWBS(new Set<Id>{so.id});
        SR_Class_CaseLine.gtCaseMap(new set<String> {ObjCase1.id});
        sr.deleteOrderedInstalledProducts(oldmap,newmap);
        
        delete caseLine;
        System.Test.StopTest();
    }
    
     public static testmethod void caseLineUnitTest3(){
        System.Test.StartTest();
        CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
        insert cdf;
        
        
       // SVMXC__Service_Order__c ser = new SVMXC__Service_Order__c();
        
        Case ObjCase1 = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, 
                            SVMXC__Top_Level__c = objIns.id, Priority = 'Medium',ProductSystem__c  = objIns.id);
        insert objCase1;
        
        Sales_Order_Item__c so = new Sales_Order_Item__c(ERP_Item_Category__c='Z001',WBS_Element__c='text',End_Date__c=Date.today().adddays(3),Start_Date__c=Date.today());
        insert so;
        
        ERP_Project__c erp = new ERP_Project__c();
        insert erp;
        
        ERP_WBS__c ews = new ERP_WBS__c(Forecast_Start_Date__c=date.today(),Forecast_End_Date__c=Date.today().adddays(7),ERP_Reference__c='text');
        insert ews;

        ERP_NWA__c nwa = new ERP_NWA__c(WBS_Element__c = ews.id,ERP_Status__c = 'REL;SETC',ERP_Std_Text_key__c = 'INST0002',ERP_Project_Nbr__c = '12555');
        insert nwa;

        SVMXC__Service_Order__c pwo = new SVMXC__Service_Order__c();
        pwo.recordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        pwo.Event__c = true;
        pwo.SVMXC__Group_Member__c = ObjTech.Id;
        pwo.SVMXC__Order_Status__c = 'Open';
        insert pwo;

        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = ObjCase1.id;
        caseLine.recordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Case_Line__c').get('Case_Line');
        caseLine.SVMXC__Product__c = objProd.Id;
        caseLine.SVMXC__Installed_Product__c = objIns.Id;
        caseLine.Start_Date_time__c = system.now().addDays(-1);
        caseLine.End_Date_time__c = system.now().addDays(1);
        caseLine.Case_Line_Owner__c = UserInfo.getUserId();
        caseLine.Billing_Type__c = 'I - Installation';
        caseLine.ERP_NWA__c = nwa.id;
        caseLine.PO_Number__c = '1000002';
        caseLine.Sales_Order_Item__c = so.id;
        caseLine.Case_Line_WO_Type__c = 'Installation';
        caseLine.ERP_Project__c = erp.id;
        caseLine.SVMXC__Line_Status__c = 'Open';
        caseLine.Work_Order__c = pwo.id;
        insert caseLine;
        caseLine.Start_Date_time__c = system.now().addDays(-2);
        update caseLine;
        
        System.Test.StopTest();
    }
    
}
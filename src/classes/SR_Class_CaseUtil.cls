/*************************************************************************\
    @ Author                :   Nilesh Gorle
    @ Date                  :   29-Dec-2017
    @ Description           :   This class contains the logic for 
                                - When a PM WO is closed, check if all PM WO under the same PM case is closed
                                - if yes, set the PM case status to 'Closed by Work Order'
    @ Story/Task            :   STSK0013531
****************************************************************************/
public class SR_Class_CaseUtil {

    public static void close_pm_case(list<SVMXC__Service_Order__c> woList, Map<Id, SVMXC__Service_Order__c> oldWoMap) {
        list<Id> caseIDs = new list<Id>();
        Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();

        system.debug('***********wo list********'+woList);
        // Get all Case number and Case Ids from woList
        for(SVMXC__Service_Order__c wo : woList) {
            system.debug('****wo*************'+wo);
            if(wo != null && (wo.SVMXC__Order_Status__c=='Closed' || wo.SVMXC__Order_Status__c=='Cancelled')
               && oldWoMap.get(wo.Id).SVMXC__Order_Status__c!=wo.SVMXC__Order_Status__c
               && wo.RecordTypeId == pmServcRecTypeID
               && wo.SVMXC__Case__c != null) {
                caseIDs.add(wo.SVMXC__Case__c);
            }
        }

        system.debug('****caseIDs*************'+caseIDs);

        if(!caseIDs.isEmpty()) {
            // Using Case Number, fetch all related Work Orders grouping by Case ID
            // Check, Each Case specific all related work orders to be closed, If Yes, the set true in woStatusMap against caseId as key.
            AggregateResult[] groupedResults = [select SVMXC__Case__c, SVMXC__Order_Status__c, Count(Id) from SVMXC__Service_Order__c Where SVMXC__Case__c IN : caseIDs Group By SVMXC__Order_Status__c, SVMXC__Case__c];
            System.debug('***********Grouped Results***********'+groupedResults);
            Map<Id, Boolean> woStatusMap = new Map<Id, Boolean>(); 
            for (AggregateResult res : groupedResults) {
                String tempCaseId = String.valueOf(res.get('SVMXC__Case__c'));
    
                if (!woStatusMap.containsKey(tempCaseId) && (res.get('SVMXC__Order_Status__c')=='Closed' || res.get('SVMXC__Order_Status__c')=='Cancelled')) {
                    woStatusMap.put(tempCaseId, true);
                } else if (!woStatusMap.containsKey(tempCaseId) && (res.get('SVMXC__Order_Status__c')!='Closed' || res.get('SVMXC__Order_Status__c')!='Cancelled')) {
                    woStatusMap.put(tempCaseId, false);
                } else if (woStatusMap.containsKey(tempCaseId) && (res.get('SVMXC__Order_Status__c')=='Closed' || res.get('SVMXC__Order_Status__c')=='Cancelled')) {
                    woStatusMap.put(tempCaseId, woStatusMap.get(tempCaseId) && true);
                } else if (woStatusMap.containsKey(tempCaseId) && (res.get('SVMXC__Order_Status__c')!='Closed' || res.get('SVMXC__Order_Status__c')!='Cancelled')) {
                    woStatusMap.put(tempCaseId, woStatusMap.get(tempCaseId) && false);
                }
            }
    
            // Differentiate only for Closed case order from woStatusMap
            list<Id> caseClosedIds = new list<Id>();
            for(Id key : woStatusMap.keySet()) {
                // Getting only case Id whose all work orders are closed.
                if (woStatusMap.get(key)==true) {
                    caseClosedIds.add(key);
                }
            }
    
            // Update Case status to 'Closed by Work Order' for caseClosedIds
            if (caseClosedIds.size() > 0) {
                Id CaseRecTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('PM').getRecordTypeId();
                list<Case> caseList = [Select Id, Status From Case Where Id in : caseClosedIds And RecordTypeId =: CaseRecTypeID];
                if(caseList.size() > 0) {
                    for (Case ca : caseList) {
                        ca.Status = 'Closed by Work Order';
                    }
    
                    update caseList;
                }
            }
        }
    }
}
/*************************************************************************\
    @ Author                :   Nilesh Gorle
    @ Date                  :   29-Dec-2017
    @ Description           :   This test class covers the code coverage for SR_Class_CaseUtil class
    @ Story/Task            :   STSK0013531
****************************************************************************/
@isTest(SeeAllData=false)
public class SR_Class_CaseUtil_Test {
    public Static Profile pr = [Select id from Profile where name = 'VMS BST - Member'];
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
    public static Id fieldServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();//record type id of Field Service WO
    public static Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
    public static Id instlRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); //record type id of Installation WO
    public static Id UsageConRecTypID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();                                
    static SVMXC__Service_Order_Line__c varWD = new SVMXC__Service_Order_Line__c();
    static SVMXC__Service_Order_Line__c varWD1 = new SVMXC__Service_Order_Line__c();
    static SVMXC__RMA_Shipment_Order__c testPartsOrder = new SVMXC__RMA_Shipment_Order__c();
    static SVMXC__Service_Order__c objWO1 = new SVMXC__Service_Order__c();
    static SVMXC__RMA_Shipment_Line__c varPOL = new SVMXC__RMA_Shipment_Line__c();
    static SVMXC__Service_Order__c objWO2 = new SVMXC__Service_Order__c();
    static SVMXC__Service_Order__c objWO3 = new SVMXC__Service_Order__c();
    static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c objTopLevelWithoutSite = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    static list<SVMXC__Installed_Product__c> installedProductList = new list<SVMXC__Installed_Product__c> ();
    static Case objCase = SR_testdata.createCase();
    static Case objCase1 = SR_testdata.createCase();
    static Contact objCont = SR_testdata.createContact();
    static Account objAcc = SR_testdata.creteAccount();
    static Product2 objProd = SR_testdata.createProduct();
    static SVMXC__Service_Contract__c objSMC = new SVMXC__Service_Contract__c();
    static SVMXC__Site__c objLoc = new SVMXC__Site__c();
    static SVMXC__Service_Level__c objSLATerm = new SVMXC__Service_Level__c();
    static SVMXC__Service_Group__c objServTeam = new SVMXC__Service_Group__c();
    static SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c();
    static SVMXC__Service_Group_Members__c objTechEquip1 = new SVMXC__Service_Group_Members__c();
    static Depot_Sourcing_Rules__c objDSR = new Depot_Sourcing_Rules__c();
    static Country__c objCounty = new Country__c();
    static ERP_Org__c objOrg = new ERP_Org__c();
    static DateTime d = system.Now();
    public static List<BusinessHours> listBusinessHrs = [Select ID from BusinessHours limit 1];
    
    
    Static {
        objCounty = new Country__c(Name='USA', Alternate_Name__c='United States', ISO_Code_2__c='US', SAP_Code__c='USA');
        insert objCounty;
        //objCounty = [select Id, Name, ISO_Code_2__c, SAP_Code__c from  Country__c 
        //             where Name = 'USA' 
        //             or Alternate_Name__c = 'United States' 
        //             or Id != null limit 001];
        /* Removed its firing 15 soqls.
        objCounty.Name = 'United States';
        objCounty.ISO_Code_2__c = 'AUS';
        objCounty.SAP_Code__c = 'AUS';
        system.debug('before country isnert ====='+Limits.getQueries());
        insert objCounty;
        system.debug('after country isnert ====='+Limits.getQueries());
        */
        
        ERP_Timezone__c erpTZ = new ERP_Timezone__c();
        erpTZ.Name = 'AUSSA';
        erpTZ.Salesforce_timezone__c = 'Central Summer Time (South Australia)';
        system.debug('before timezone isnert ====='+Limits.getQueries());
        insert erpTZ;
        system.debug('after timezoen isnert ====='+Limits.getQueries());
        
        objAcc.AccountNumber = '123456';
        objAcc.Country1__c = objCounty.ID;
        objAcc.country__c = 'India';
        objAcc.ERP_Timezone__c = 'AUSSA';
        objAcc.Timezone__c = erpTZ.Id;
        objAcc.BillingCity = 'Milpitas';
        objAcc.BillingState = 'CA';
        system.debug('before account isnert ====='+Limits.getQueries());
        insert objAcc;
        system.debug('after account isnert ====='+Limits.getQueries());
        
        objSMC.Name = 'Test Contract';
        objSMC.SVMXC__Company__c = objAcc.ID;
        objSMC.SVMXC__Start_Date__c = system.today();
        objSMC.SVMXC__End_Date__c = system.today() + 30;
        system.debug('before servicecontract insert ====='+Limits.getQueries());
        insert objSMC;
        system.debug('after servicecontract insert ====='+Limits.getQueries());
        
        objSLATerm.Name = 'Test SLA Term';
        objSLATerm.SVMXC__Restoration_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Onsite_Response_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Resolution_Tracked_On__c = 'WorkOrder';
        system.debug('before SLA ====='+Limits.getQueries());
        insert objSLATerm;
        system.debug('After SLA ====='+Limits.getQueries());
        
        objServTeam.Name = 'ABC';
        objServTeam.SVMXC__Active__c = true;
        objServTeam.Dispatch_Queue__c = 'DISP CH';
        //objServTeam.Review_Required__c = true;
        objServTeam.District_Manager__c = userInfo.getUserID();
        objServTeam.SVMXC__Group_Code__c = 'ABC';
        system.debug('before Serviceteam ====='+Limits.getQueries());
        objServTeam.SVMXC__Group_Type__c = 'Field Service';
        insert objServTeam;
        system.debug('after serviceteam ====='+Limits.getQueries());
        
        objOrg.Name = 'Test Org';
        objOrg.Sales_Org__c = 'Test Sales Org';
        objOrg.Approval_time_limit__c = 2;
        objOrg.Part_Order_Value_Limit__c = 12;
        objOrg.Received_Date_Required__c = true;
        objOrg.Bonded_Warehouse_used__c = true;
        objOrg.Parts_Order_Approval_Level__c = 12.8;
        
        system.debug('before org isnert ====='+Limits.getQueries());
        insert objOrg;
        system.debug('before org isnert ====='+Limits.getQueries());
        
        objLoc.Name = 'Local Purchase';
        objLoc.SVMXC__Street__c = 'Test Street';
        objLoc.SVMXC__Country__c = 'United States';
        objLoc.SVMXC__Zip__c = '98765';
        objLoc.ERP_Functional_Location__c = 'Test Location';
        objLoc.Plant__c = 'Test Plant';
        objLoc.ERP_Site_Partner_Code__c = '123456';
        objLoc.SVMXC__Location_Type__c = 'Depot';
        objLoc.Sales_Org__c = objOrg.Sales_Org__c;
        objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
        objLoc.ERP_Org__c = objOrg.id;
        objLoc.SVMXC__Stocking_Location__c = true;
        objLoc.Technician__c = objTechEquip.Id;
        objLoc.Country__c = objCounty.ID;
        objLoc.FE_Service_Team__c = objServTeam.Id;
        objLoc.RecordTypeId = Schema.Sobjecttype.SVMXC__Site__c.getRecordTypeInfosByName().get('Standard Location').getRecordTypeId();
        system.debug('before location insert ====='+Limits.getQueries());
        insert objLoc;
        system.debug('after location insert ====='+Limits.getQueries());
        
        Depot_Sourcing_Rules__c varDepo = new Depot_Sourcing_Rules__c();
        varDepo.Country__c = objCounty.id; 
        varDepo.Depot_Priority__c = 2;
        varDepo.Location__c = objLoc.ID;  
        varDepo.Machine_Down__c = true;
        system.debug('before depot sourcing rule insert ====='+Limits.getQueries());
        insert varDepo;
        system.debug('after depot sourcing rule insert ====='+Limits.getQueries());
        
        objDSR.Name = 'Test DSR';
        objDSR.Country__c = objCounty.ID; 
        objDSR.Country_Code__c = 'US';
        objDSR.Depot_Priority__c = 1;
        objDSR.Location__c = objLoc.ID;
        objCont.Email = 'abc@xyz.com';
        objCont.mailingcountry = 'India';
        //insert objCont;
        
        objCase.AccountID = objAcc.ID;
        //objCase.ContactID = objCont.ID;
        objCase.Reason = 'System Down';
        objCase.Priority = 'Medium';
        objCase.Type ='Problem';
        objCase.Reason = 'System Down';
        //objCase.Customer_Preferred_End_Date_time__c = d.addMinutes(4);
        // objCase.Customer_Requested_End_Date_Time__c= '12-6-2015 11:59 PM';
        //insert objCase;  
        
        objProd.Name = 'Test Product';
        objProd.SVMXC__Product_Line__c = 'Accessory';
        objProd.Skills_Required__c = 'BEAM' ;
        objProd.Returnable__c = 'Scrap Locally';
        //objProd.Includes_Software__c = true;
        /*objProd.Material_Group__c = Label.Product_Material_group;
        objProd.Budget_Hrs__c = 4;
        objProd.UOM__c = 'HR';
        objProd.Product_Type__c = label.SR_Product_Type_Sellable;
        objProd.ProductCode = 'Test Material No 001';
        objProd.ERP_PCode_4__c = 'H012';
        objProd.Is_Model__c = true; */
        system.debug('before  product insert ====='+Limits.getQueries());
        insert objProd;
        system.debug('after  product insert ====='+Limits.getQueries());

        
        objTechEquip.Name = 'Test Technician';
        objTechEquip.SVMXC__Active__c = true;
        objTechEquip.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        objTechEquip.SVMXC__Enable_Scheduling__c = true;
        objTechEquip.SVMXC__Phone__c = '987654321';
        objTechEquip.SVMXC__Email__c = 'abc@xyz.com';
        objTechEquip.SVMXC__Service_Group__c = objServTeam.ID;
        objTechEquip.SVMXC__Street__c = 'Test Street';
        objTechEquip.SVMXC__Zip__c = '98765';
        objTechEquip.SVMXC__Country__c = 'United States';
        objTechEquip.OOC__c = false;
        objTechEquip.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
        objTechEquip.User__c = userInfo.getUserID();
        objTechEquip.SVMXC__Inventory_Location__c = objLoc.Id;
        objTechEquip.Dispatch_Queue__c = 'DISP CH';

        objTechEquip1.Name = 'Test Technician';
        objTechEquip1.SVMXC__Inventory_Location__c = objLoc.ID;
        objTechEquip1.SVMXC__Active__c = true;
        objTechEquip1.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        objTechEquip1.SVMXC__Enable_Scheduling__c = true;
        objTechEquip1.SVMXC__Phone__c = '987654321';
        objTechEquip1.SVMXC__Email__c = 'abc@xyz.com';
        objTechEquip1.SVMXC__Service_Group__c = objServTeam.ID;
        objTechEquip1.SVMXC__Street__c = 'Test Street';
        objTechEquip1.SVMXC__Zip__c = '98765';
        objTechEquip1.SVMXC__Country__c = 'United States';
        objTechEquip1.OOC__c = false;
        objTechEquip1.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
        objTechEquip1.Dispatch_Queue__c = 'DISP CH';
        objTechEquip1.Technician_Status__c = 'Available';
        system.debug('before equitp ====='+Limits.getQueries());
        insert new list<SVMXC__Service_Group_Members__c>{objTechEquip,objTechEquip1};
        system.debug('after equitp ====='+Limits.getQueries());
        
        objTopLevel.Name = 'H23456';
        objTopLevel.SVMXC__Serial_Lot_Number__c = 'H23456';
        objTopLevel.SVMXC__Product__c = objProd.ID;
        objTopLevel.SVMXC__Site__c = objLoc.ID;
        objTopLevel.ERP_Reference__c = 'H23456';
        objTopLevel.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objTopLevel.ERP_CSS_District__c = 'ABC';
        objTopLevel.ERP_Sales__c = 'Test ERP Sales';
        objTopLevel.Service_Team__c = objServTeam.id;
        objTopLevelWithoutSite.Name = 'H23456';
        objTopLevelWithoutSite.SVMXC__Serial_Lot_Number__c = 'H23456';
        objTopLevelWithoutSite.SVMXC__Product__c = objProd.ID;
        objTopLevelWithoutSite.ERP_Reference__c = 'H23456';
        objTopLevelWithoutSite.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objTopLevelWithoutSite.ERP_CSS_District__c = 'ABC';
        objTopLevelWithoutSite.Service_Team__c = objServTeam.id;
        objTopLevelWithoutSite.ERP_Sales__c = 'Test ERP Sales';

        objComponent.Name = 'H12345';
        objComponent.SVMXC__Serial_Lot_Number__c = 'H12345';
        objComponent.SVMXC__Parent__c = objTopLevel.ID;
        objComponent.SVMXC__Product__c = objProd.ID;
        objComponent.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objComponent.ERP_Sales__c = 'Test ERP Sales';
        objComponent.ERP_Reference__c = 'H12345';
        objComponent.SVMXC__Top_Level__c = objTopLevel.id;
        objComponent.ERP_CSS_District__c = 'ABC';
        objComponent.SVMXC__Site__c = objLoc.id;
        objComponent.SVMXC__Status__c = 'Shipped';
        objComponent.SVMXC__Company__c = objAcc.id;
        system.debug('before ip ====='+Limits.getQueries());
        insert new list<SVMXC__Installed_Product__c> {objComponent,objTopLevelWithoutSite,objTopLevel};
        system.debug('after ip ====='+Limits.getQueries());        

        //Generate a map of tokens for all the Record Types for the desired object
        RecordType pmRecordType = [select Id, DeveloperName from RecordType where SobjectType='Case' and DeveloperName='Project_Management' Limit 1];
        system.debug('--------Recordtype123------------'+pmRecordType.DeveloperName);
        objCase1.AccountID = objAcc.ID;
        objCase1.Reason = 'System Down';
        objCase1.Priority = 'Medium';
        objCase1.Status = 'New';
        objCase1.SVMXC__Top_Level__c = objComponent.id;
        objCase1.Type ='Problem';
        objCase1.RecordType = pmRecordType;
        objCase1.Reason = 'System Down';
        objCase1.Description = 'asdfghjkzxcvbnm';
        objCase1.Customer_Preferred_End_Date_time__c = (System.now()).addMinutes(4);
        objCase1.Customer_Requested_End_Date_Time__c= '12-6-2015 11:59 PM';
        system.debug('before case isnert ====='+Limits.getQueries());
        insert objCase1;
        system.debug('after case isnert ====='+Limits.getQueries());
    }

    public static testMethod void pm_close_case()    
    {
        test.startTest();
        RecordType pmServcRecType = [select Id, DeveloperName from RecordType where SobjectType='SVMXC__Service_Order__c' and DeveloperName='ProjectManagement' Limit 1];
        system.debug('--------------------pmServcRecType--------------'+pmServcRecType);
        //Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        SVMXC__Service_Order__c objWO11 = new SVMXC__Service_Order__c();
        objWO11.RecordType = pmServcRecType;
        objWO11.RecordTypeID = pmServcRecType.Id;
        objWO11.Machine_release_time__c = system.now();
        objWO11.SVMXC__Order_Status__c = 'Assigned';
        objWO11.SVMXC__Case__c = objCase1.Id;
        objWO11.Direct_Assignment__c = true ;
        objWO11.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO11.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO11.SVMXC__Top_Level__c = objTopLevel.id;
        objWO11.Subject__c = 'Test Work Order';
        objWO11.SVMXC__Preferred_Start_Time__c = System.now();
        objWO11.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO11.ERP_Depot__c = objLoc.Plant__c;
        objWO11.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO11.Is_Master_WO__c = true;
        insert objWO11;

        SVMXC__Service_Order__c objWO12 = new SVMXC__Service_Order__c();
        objWO12.RecordType = pmServcRecType;
        objWO12.RecordTypeID = pmServcRecType.Id;
        objWO12.Machine_release_time__c = system.now();
        objWO12.SVMXC__Order_Status__c = 'Assigned';
        objWO12.SVMXC__Case__c = objCase1.Id;
        objWO12.Direct_Assignment__c = true ;
        objWO12.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO12.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO12.SVMXC__Top_Level__c = objTopLevel.id;
        objWO12.Subject__c = 'Test Work Order';
        objWO12.SVMXC__Preferred_Start_Time__c = System.now();
        objWO12.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO12.ERP_Depot__c = objLoc.Plant__c;
        objWO12.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO12.Is_Master_WO__c = true;
        insert objWO12;
        
        Map<Id, SVMXC__Service_Order__c> oldMap = new Map<Id, SVMXC__Service_Order__c>();
        oldMap.put(objWO11.Id, objWO11);
        oldMap.put(objWO12.Id, objWO12);

        
        objWO11.Is_escalation_to_the_CLT_required__c = 'No';
        objWO11.Is_This_a_Complaint__c = 'No';
        objWO11.Was_anyone_injured__c = 'No';
        objWO11.SVMXC__Order_Status__c = 'Closed';
        update objWO11;

        list<Id> work_orders_id_list = new list<Id>();
        work_orders_id_list.add(objWO11.Id);

        list<SVMXC__Service_Order__c> work_orders = [select Id, SVMXC__Order_Status__c, RecordType.DeveloperName, SVMXC__Case__c from SVMXC__Service_Order__c Where Id in : work_orders_id_list];
        SR_Class_CaseUtil.close_pm_case(work_orders, oldMap);

        objWO12.Is_escalation_to_the_CLT_required__c = 'No';
        objWO12.Is_This_a_Complaint__c = 'No';
        objWO12.Was_anyone_injured__c = 'No';
        objWO12.SVMXC__Order_Status__c = 'Closed';
        update objWO12;
        work_orders_id_list.add(objWO12.Id);
        work_orders = [select Id, SVMXC__Order_Status__c, RecordType.DeveloperName, SVMXC__Case__c from SVMXC__Service_Order__c Where Id in : work_orders_id_list];
        SR_Class_CaseUtil.close_pm_case(work_orders, oldMap);

        system.debug('--------Recordtype1------------'+work_orders[0].SVMXC__Order_Status__c);
        system.debug('--------Recordtype2------------'+work_orders[1].SVMXC__Order_Status__c);
        
        system.debug('--------Recordtype3------------'+work_orders[0].RecordType.DeveloperName);
        system.debug('--------Recordtype4------------'+work_orders[1].RecordType.DeveloperName);
        
        system.debug('--------Recordtype5------------'+work_orders[0].SVMXC__Case__c);
        system.debug('--------Recordtype6------------'+work_orders[1].SVMXC__Case__c);

        test.stopTest();
    }
    
    public static testMethod void pm_close_case1()    
    {
        test.startTest();
        RecordType pmServcRecType = [select Id, DeveloperName from RecordType where SobjectType='SVMXC__Service_Order__c' and DeveloperName='ProjectManagement' Limit 1];
        system.debug('--------------------pmServcRecType--------------'+pmServcRecType);
        //Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        SVMXC__Service_Order__c objWO11 = new SVMXC__Service_Order__c();
        objWO11.RecordType = pmServcRecType;
        objWO11.RecordTypeID = pmServcRecType.Id;
        objWO11.Machine_release_time__c = system.now();
        objWO11.SVMXC__Order_Status__c = 'Assigned';
        objWO11.SVMXC__Case__c = objCase1.Id;
        objWO11.Direct_Assignment__c = true ;
        objWO11.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO11.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO11.SVMXC__Top_Level__c = objTopLevel.id;
        objWO11.Subject__c = 'Test Work Order';
        objWO11.SVMXC__Preferred_Start_Time__c = System.now();
        objWO11.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO11.ERP_Depot__c = objLoc.Plant__c;
        objWO11.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO11.Is_Master_WO__c = true;
        insert objWO11;

        SVMXC__Service_Order__c objWO12 = new SVMXC__Service_Order__c();
        objWO12.RecordType = pmServcRecType;
        objWO12.RecordTypeID = pmServcRecType.Id;
        objWO12.Machine_release_time__c = system.now();
        objWO12.SVMXC__Order_Status__c = 'Assigned';
        objWO12.SVMXC__Case__c = objCase1.Id;
        objWO12.Direct_Assignment__c = true ;
        objWO12.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO12.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO12.SVMXC__Top_Level__c = objTopLevel.id;
        objWO12.Subject__c = 'Test Work Order';
        objWO12.SVMXC__Preferred_Start_Time__c = System.now();
        objWO12.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO12.ERP_Depot__c = objLoc.Plant__c;
        objWO12.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO12.Is_Master_WO__c = true;
        insert objWO12;

        Map<Id, SVMXC__Service_Order__c> oldMap = new Map<Id, SVMXC__Service_Order__c>();
        oldMap.put(objWO12.Id, objWO12);

        list<SVMXC__Service_Order__c> wo_list = new list<SVMXC__Service_Order__c>();
        wo_list.add(objWO12);
        SR_Class_CaseUtil.close_pm_case(wo_list, oldMap);
        test.stopTest();
    }
    
    public static testMethod void pm_close_case2()    
    {
        test.startTest();
        RecordType pmServcRecType = [select Id, DeveloperName from RecordType where SobjectType='SVMXC__Service_Order__c' and DeveloperName='ProjectManagement' Limit 1];
        system.debug('--------------------pmServcRecType--------------'+pmServcRecType);
        //Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        SVMXC__Service_Order__c objWO12 = new SVMXC__Service_Order__c();
        objWO12.RecordType = pmServcRecType;
        objWO12.RecordTypeID = pmServcRecType.Id;
        objWO12.Machine_release_time__c = system.now();
        objWO12.Was_anyone_injured__c = 'No';
        objWO12.SVMXC__Order_Status__c = 'Closed';
        objWO12.SVMXC__Case__c = objCase1.Id;
        objWO12.Direct_Assignment__c = true ;
        objWO12.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO12.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO12.SVMXC__Top_Level__c = objTopLevel.id;
        objWO12.Subject__c = 'Test Work Order';
        objWO12.SVMXC__Preferred_Start_Time__c = System.now();
        objWO12.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO12.ERP_Depot__c = objLoc.Plant__c;
        objWO12.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO12.Is_Master_WO__c = true;
        insert objWO12;

        Map<Id, SVMXC__Service_Order__c> oldMap = new Map<Id, SVMXC__Service_Order__c>();
        oldMap.put(objWO12.Id, objWO12);

        list<SVMXC__Service_Order__c> wo_list = new list<SVMXC__Service_Order__c>();
        wo_list.add(objWO12);
        SR_Class_CaseUtil.close_pm_case(wo_list,oldMap);
        test.stopTest();
    }
    
    public static testMethod void pm_close_case3()    
    {
        test.startTest();
        RecordType pmServcRecType = [select Id, DeveloperName from RecordType where SobjectType='SVMXC__Service_Order__c' and DeveloperName='ProjectManagement' Limit 1];
        system.debug('--------------------pmServcRecType--------------'+pmServcRecType);
        //Id pmServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        SVMXC__Service_Order__c objWO11 = new SVMXC__Service_Order__c();
        objWO11.RecordType = pmServcRecType;
        objWO11.RecordTypeID = pmServcRecType.Id;
        objWO11.Machine_release_time__c = system.now();
        objWO11.SVMXC__Order_Status__c = 'Assigned';
        objWO11.SVMXC__Case__c = objCase1.Id;
        objWO11.Direct_Assignment__c = true ;
        objWO11.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO11.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO11.SVMXC__Top_Level__c = objTopLevel.id;
        objWO11.Subject__c = 'Test Work Order';
        objWO11.SVMXC__Preferred_Start_Time__c = System.now();
        objWO11.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO11.ERP_Depot__c = objLoc.Plant__c;
        objWO11.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO11.Is_Master_WO__c = true;
        insert objWO11;

        SVMXC__Service_Order__c objWO12 = new SVMXC__Service_Order__c();
        objWO12.RecordType = pmServcRecType;
        objWO12.RecordTypeID = pmServcRecType.Id;
        objWO12.Machine_release_time__c = system.now();
        objWO12.Was_anyone_injured__c = 'No';
        objWO12.SVMXC__Order_Status__c = 'Closed';
        objWO12.SVMXC__Case__c = objCase1.Id;
        objWO12.Direct_Assignment__c = true ;
        objWO12.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO12.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO12.SVMXC__Top_Level__c = objTopLevel.id;
        objWO12.Subject__c = 'Test Work Order';
        objWO12.SVMXC__Preferred_Start_Time__c = System.now();
        objWO12.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO12.ERP_Depot__c = objLoc.Plant__c;
        objWO12.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        objWO12.Is_Master_WO__c = true;
        insert objWO12;

        Map<Id, SVMXC__Service_Order__c> oldMap = new Map<Id, SVMXC__Service_Order__c>();
        oldMap.put(objWO11.Id, objWO11);
        oldMap.put(objWO12.Id, objWO12);


        list<SVMXC__Service_Order__c> wo_list = new list<SVMXC__Service_Order__c>();
        wo_list.add(objWO11);
        wo_list.add(objWO12);
        SR_Class_CaseUtil.close_pm_case(wo_list, oldMap);
        test.stopTest();
    }
}
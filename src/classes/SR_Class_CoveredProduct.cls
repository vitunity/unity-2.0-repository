/****************************************
    Last modified by    :   Nikita Gupta
    Last modified on    :   6/9/2014
    Last modified reason:   Added null checks for maps and sets
****************************************/

public class SR_Class_CoveredProduct
{
// Method Cancelling 
    public static void updateCDFromCoveredProduct(list<SVMXC__Service_Contract_Products__c> lstCovdProd, map<id,SVMXC__Service_Contract_Products__c> oldmap)
    {
        //List Variable        
        List<SVMXC__Counter_Details__c> UpdateCounterDetail = new List<SVMXC__Counter_Details__c>();
        List<SVMXC__Counter_Details__c> lstCounterDetail = new List<SVMXC__Counter_Details__c>();
        list<SVMXC__SLA_Detail__c> lstSLADetail=new list<SVMXC__SLA_Detail__c>();
        list<SVMXC__Service_Contract__c> lstServiceContract=new list<SVMXC__Service_Contract__c>();
        
        //Setr Variable
        set<string> slaterm = new set<string>();        // Set holding SLA Term Related to covered product
        set<string> stSLADetail = new set<string>();      //Set related to sla detal
        set<string> stCoveredProdId = new set<string>();
        set<string> stWorkOrderDetail = new set<string>();
        set<string> stWorkOrder = new set<string>();
        set<string> stContract = new set<string>();
        
        for(SVMXC__Service_Contract_Products__c varCV : lstCovdProd)
        {
            if(varCV.Cancelation_Reason__c != null)
            {
                if(varCV.Cancelation_Reason__c.containsIgnoreCase('cancel'))
                {
                    stCoveredProdId.add(varCV.Id);
                    if(varCV.SVMXC__SLA_Terms__c !=null)
                    {
                        slaterm.add(varCV.SVMXC__SLA_Terms__c);
                    }
                }
            }   
        }
        
        if(slaterm.size()>0)
        {
            lstSLADetail=[select id,SVMXC__SLA_Terms__c from SVMXC__SLA_Detail__c where SVMXC__SLA_Terms__c IN :slaterm and  Status__c = 'Valid' ]; // added status check for de3515 on 13-02-2015, By Bushra
            if(lstSLADetail.size()>0)
            {
                for (SVMXC__SLA_Detail__c varSlaDEtail: lstSLADetail)
                {
                    stSLADetail.add(varSlaDEtail.id);
                }
            }
        }
        integer i=0;
        integer j=0;
        string strQuery= 'select id, SVMXC__Active__c, SVMXC__Covered_Products__c from SVMXC__Counter_Details__c';
        //, (Select id,Name,Counter_Detail__c,Work_Detail__c,Work_Detail__r.SVMXC__Service_Order__c, Work_Detail__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c from Counter_Usage__r) // Code refactor Wave 1- Nikita 1,23,2015
        if(stCoveredProdId.size() > 0) //null check added by Nikita on 6/9/2014
        {
            i=1;
        }             
        if(stSLADetail.size()>0)
        {
            j=1;          
        }  

        if(i==1 && j==0)
        {
            strQuery = strQuery +' where SVMXC__Covered_Products__c IN :stCoveredProdId';
            lstCounterDetail =Database.query(strQuery);
        }
        else if(i==0 && j==1)
        {
            strQuery = strQuery +' where  SLA_Detail__c IN : stSLADetail';
            lstCounterDetail =Database.query(strQuery);
        }
        else if(i==1 && j==1)
        {
            strQuery = strQuery+ ' where SVMXC__Covered_Products__c IN :stCoveredProdId OR   SLA_Detail__c IN : stSLADetail';
            lstCounterDetail =Database.query(strQuery);
        }
        
        if(lstCounterDetail.size() > 0) //null check added by Nikita on 6/9/2014
        {
            for(SVMXC__Counter_Details__c VarCDT : lstCounterDetail )
            {
                VarCDT.SVMXC__Active__c=False; 
                UpdateCounterDetail.add(VarCDT);
                
                /* Start Code Refactor - Wave 1C - Nikita 1/22/2015
                if(VarCDT.Counter_Usage__r.size() >0)
                {   
                    for(Counter_Usage__c varCU : VarCDT.Counter_Usage__r )
                    {
                        stWorkOrder.add(varCU.Work_Detail__r.SVMXC__Service_Order__c);
                    }
                }
                End Code Refactor - Wave 1C - Nikita 1/22/2015*/
            }
            
            list<SVMXC__Service_Order__c> lstWO = new list<SVMXC__Service_Order__c>();
            set<string> stwdId = new set<string>();
            
            if(stWorkOrder.size() > 0)
            {
                for(string varWDID :stWorkOrder)
                { 
                    lstWO.add(new SVMXC__Service_Order__c(id=varWDID, SVMXC__Order_Status__c='Cancelled'));
                }
            }
            if(lstWO.size()>0)
            {
                Update lstWO;
            }
            if(UpdateCounterDetail.size()>0)
            {
                update UpdateCounterDetail;
            }
        }
    }     
}
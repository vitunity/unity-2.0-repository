@isTest(SeeAllData = True)

public class SR_Class_CoveredProduct_Test
{
   Static testMethod void SR_CoveredProduct()
   {
        SR_Class_CoveredProduct sc = new SR_Class_CoveredProduct();
        //insert Account
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234', Prefered_Language__c ='English');
        insert accObj;
        
        //insert contact
        Contact conObj = new Contact(AccountId = accObj.Id, LastName = 'test contact', FirstName = 'test', Email ='test@wipro.com', phone = '986743212', Contact_Type__c ='Urologist', mailingcountry = 'India');
        insert conObj;
        
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
        Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accObj.Id, ContactId = conObj.id, Subject ='test case', Status = 'New' , Priority='Medium' );
        insert caseObj ;
        
        //insert installed product
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'test1IP';
        ip1.SVMXC__Serial_Lot_Number__c = 'sr123';
        insert ip1;
        
       // insert WorkOrder
        Schema.DescribeSObjectResult wo = Schema.SObjectType.SVMXC__Service_Order__c; 
        Map<String,Schema.RecordTypeInfo> woMapByName = wo.getRecordTypeInfosByName();
        SVMXC__Service_Order__c woObj = new SVMXC__Service_Order__c(Recordtypeid = woMapByName.get('Field Service').getRecordTypeId(), SVMXC__Case__c = caseObj.id, SVMXC__Order_Status__c ='Open', SVMXC__Priority__c='3 - Information',SVMXC__Contact__c = conObj.id, SVMXC__Order_Type__c ='Off-Site Service Clinical', SVMXC__Entitlement_Type__c ='High Utilization', SVMXC__Closed_On__c =datetime.newInstance(2014, 11, 12, 13, 30, 0),Additional_Email__c ='anv@varian.com',SVMXC__Top_Level__c = ip1.id); 
        insert woObj;
        test.startTest();
        
        Recordtype recWorkDetailPS = [Select Id,DeveloperName From RecordType Where SobjectType = 'SVMXC__Service_Order_Line__c' and DeveloperName = 'Products_Serviced']; 

        List<SVMXC__Service_Order_line__c> listWorkDetail = [select Id, RecordTypeId, SVMXC__Serial_Number__c, SVMXC__Service_Order__c from SVMXC__Service_Order_line__c where RecordTypeId = :recWorkDetailPS.id  AND SVMXC__Service_Order__c =: woObj.Id AND SVMXC__Serial_Number__c =: woObj.SVMXC__Top_Level__c];
        map<id,SVMXC__Service_Order_line__c> mapWorkDetail = new  map<id,SVMXC__Service_Order_line__c>();
        for(SVMXC__Service_Order_line__c varWD : listWorkDetail)
        {
            mapWorkDetail.put(varWD.SVMXC__Service_Order__c , varWD);
        }
                
        SVMXC__Service_Level__c sla = new SVMXC__Service_Level__c();
        sla.Name = 'SLA TEST';
        insert sla;
        
        SVMXC__SLA_Detail__c sld = new SVMXC__SLA_Detail__c();
        sld.Installed_Product__c = ip1.id;
        insert sld;
        
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.Installed_Product__c = ip1.id;
        sld1.SVMXC__SLA_Terms__c = sla.id;
        sld1.Status__c = 'Valid';
        insert sld1;
          
        SVMXC__Service_Contract__c servcContrct = new SVMXC__Service_Contract__c();
        servcContrct.Name = 'testContrct';
        servcContrct.Quote_Reference__c = 'quote123';
        servcContrct.SVMXC__End_Date__c = System.today();  
        servcContrct.SVMXC__Start_Date__c = System.today();
        servcContrct.ERP_Contract_Type__c = 'ZH3';
        servcContrct.SVMXC__Active__c = true;
        servcContrct.ERP_Sold_To__c = 'bdbdb';
        insert servcContrct;
         
        map<id,SVMXC__Service_Contract_Products__c> id_ScMap = new map<id,SVMXC__Service_Contract_Products__c>();
        
        List<SVMXC__Service_Contract_Products__c> lstOfCovrdProd= new List<SVMXC__Service_Contract_Products__c>();
        
        SVMXC__Service_Contract_Products__c covrProd1 = new SVMXC__Service_Contract_Products__c();
        covrProd1.ERP_Site_Partner__c = 'sitePartner';
        covrProd1.SVMXC__Service_Contract__c = servcContrct.id ;
        covrProd1.SVMXC__Product_Line__c = 'ARIA';
        covrProd1.Serial_Number_PCSN__c = 'sr123';
        covrProd1.ERP_Functional_Location__c = 'funcLocation';
        covrProd1.Product_Code__c = 'pro123';
        covrProd1.Cancelation_Reason__c = 'cancel';
        covrProd1.SVMXC__SLA_Terms__c = sla.id;
        lstOfCovrdProd.add(covrProd1);
        insert lstOfCovrdProd;
        
        SVMXC__Counter_Details__c scd = new SVMXC__Counter_Details__c();
        scd.SVMXC__Active__c = true;
        scd.SVMXC__Covered_Products__c = covrProd1.id;
        insert scd;
        
        /* Counter_Usage__c cu = new Counter_Usage__c();
        cu.Counter_Detail__c = scd.id;
        cu.Work_Detail__c = mapWorkDetail.get(woObj.Id).id;
        insert cu; Wave1*/
        
        /*SVMXC__Service_Contract_Products__c covrProd2 = new SVMXC__Service_Contract_Products__c();
        covrProd2.ERP_Site_Partner__c = 'sitePartner';
        covrProd2.SVMXC__Service_Contract__c = servcContrct.id;
        covrProd2.Cancelation_Reason__c = 'cancel';
        lstOfCovrdProd.add(covrProd2);
        insert lstOfCovrdProd; */
        
        SR_Class_CoveredProduct.updateCDFromCoveredProduct(lstOfCovrdProd,id_ScMap);
        test.stopTest();    
   }
}
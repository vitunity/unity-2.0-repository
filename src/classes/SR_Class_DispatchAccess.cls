/*************************************************************************\
    @ Author        :Ritika
    @ Date      :     23-Aug-2014
    @ Description   : This class is being called from trigger "SR_Dispatcher_AfterTrigger" to perform various functionalities
                      as per US4038 related to dispatch access object.
    @ Last Modified By  :   
    @ Last Modified On  :  
*************************************************************************/
public class SR_Class_DispatchAccess
{
    List<SVMXC__Dispatcher_Access__c> lstInsertDispatch = new List<SVMXC__Dispatcher_Access__c> (); 
    
    Id techRecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    
    public void CreateDispatchAccessTerritory(list<SVMXC__Dispatcher_Access__c> lstDispatch)
    {
        Set<Id> terrId = new Set<Id>();
        Set<Id> UserId = new Set<Id>();
        Set<Id> ServiceTeamId = new Set<Id>();
        set<ID> setTerrWithChild = new set<ID>();//set of territories having child      
        set<id> childTerr = new set<id>();//set of child territories
        
        Map<Id,SVMXC__Territory__c> MapOfTerritory = new  Map<Id,SVMXC__Territory__c> ();
        Map<String, Set<ID>> mapParentandChildTerr = new Map<String, Set<ID>>();// store parent as a key and set of child as value
        Map<String,Set<ID>> map_userid_lTerID = new map<String, Set<ID>>();    //store user id and set of territories of the user
        Map<ID,SVMXC__Territory__c > mapTerrcode = new Map<ID,SVMXC__Territory__c >();
         
        for(SVMXC__Dispatcher_Access__c disAcc : lstDispatch)
        {
            if(disAcc.SVMXC__Territory__c != null)
            {
                terrId.add(disAcc.SVMXC__Territory__c);//add the territory id into set
            }
            if(disAcc.SVMXC__Dispatcher__c != null)
            {
                UserId.add(disAcc.SVMXC__Dispatcher__c);//add the user id into set
            }
            if(disAcc.SVMXC__Service_Team__c != null)
            {
                ServiceTeamId.add(disAcc.SVMXC__Service_Team__c);//add the service team id into set
            }
        }
        system.debug('Territory ID>>>>>>>' +terrId);  
        if(terrId.size() > 0)
        {
            MapOfTerritory = new Map<Id,SVMXC__Territory__c>([Select ID, Name,SVMXC__Parent_Territory__c from SVMXC__Territory__c  where SVMXC__Parent_Territory__c in : terrId ]);//store territory and its child 
            
            if(MapOfTerritory.size() > 0)
            {
                system.debug('Inside for loop1>>>>>>>');
                for(SVMXC__Territory__c s : MapOfTerritory.values())
                {
                    system.debug(' mapParentandChildTerr1>>>>>>>'+ mapParentandChildTerr);
                    if(!mapParentandChildTerr.containsKey(s.SVMXC__Parent_Territory__c))
                    {
                        Set<ID> sTemp = new Set<ID>();
                        sTemp.add(s.id);
                        mapParentandChildTerr.put(s.SVMXC__Parent_Territory__c, stemp);
                        system.debug(' mapParentandChildTerr2>>>>>>>'+ mapParentandChildTerr);                
                    }                 
                    else
                    {
                        Set<ID> sTemp = new Set<ID>();
                        stemp = mapParentandChildTerr.get(s.SVMXC__Parent_Territory__c);
                        stemp.add(s.id);
                        mapParentandChildTerr.put(s.SVMXC__Parent_Territory__c, stemp);
                        system.debug(' mapParentandChildTerr3>>>>>>>'+ mapParentandChildTerr);                
                    }   
                    childTerr.add(s.id);        
                    system.debug(' mapParentandChildTerr4>>>>>>>'+ mapParentandChildTerr);                
                }
                system.debug(' childTerr>>>>>'+ childTerr);
                
                if(childTerr.size() > 0)
                {
                    for(SVMXC__Territory__c st : [Select ID, Name, SVMXC__Parent_Territory__c from SVMXC__Territory__c where SVMXC__Parent_Territory__c in : childTerr])
                    {
                        setTerrWithChild.add(st.SVMXC__Parent_Territory__c);         
                    }
                    
                    system.debug('setTerrWithChild >>>>>'+setTerrWithChild );             
                    system.debug(' mapParentandChildTerr5>>>>>>>'+ mapParentandChildTerr);
                
                    if(UserId.size() > 0)
                    {                   
                        List<SVMXC__Dispatcher_Access__c> ListExistingDispatch = new List<SVMXC__Dispatcher_Access__c>([select SVMXC__Territory__c, SVMXC__Dispatcher__c from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c in: UserId and SVMXC__Territory__c in : childTerr]);//list of existing record for the user and territory
                        system.debug('ListExistingDispatch >>>>>>>'+ListExistingDispatch );
                        
                        if(ListExistingDispatch.size() > 0)
                        {
                            for(SVMXC__Dispatcher_Access__c dp : ListExistingDispatch)
                            {      
                                if(!map_userid_lTerID.containsKey(dp.SVMXC__Dispatcher__c))
                                {
                                    Set<ID> sTemp = new Set<ID>();
                                    sTemp.add(dp.SVMXC__Territory__c);
                                    map_userid_lTerID.put(dp.SVMXC__Dispatcher__c, stemp);
                                }
                                else
                                {
                                    Set<ID> sTemp = new Set<ID>();
                                    stemp = map_userid_lTerID.get(dp.SVMXC__Dispatcher__c);
                                    sTemp.add(dp.SVMXC__Territory__c);
                                    map_userid_lTerID.put(dp.SVMXC__Dispatcher__c, stemp);                
                                }
                            }
                        }   
                    }   
                }   
                system.debug('map_userid_lTerID >>>>>>>'+map_userid_lTerID );
                
                Set<ID> sExistingIds = new  Set<ID>();//set of existing territory id
                Set<ID> sAllIDs = new Set<ID>();//set of all territory with and without child
                set<ID> setWithNoChild = new set<ID>();//set of territory having no child i.e. which is last on hierarchy
                Set<string> sCodeIDs = new Set<string>(); //set of territory code
                List<NoChildDisptachAccess> noChildDisptachAccessList = new List<NoChildDisptachAccess>();
                Map<string,SVMXC__Service_Group__c> mapServTeam = new Map<string,SVMXC__Service_Group__c>();
                Map<ID,string > mapIDandterrcode = NEW Map<ID,string >();
                SVMXC__Service_Group__c serTeam;
                
                for(SVMXC__Dispatcher_Access__c disAcc : lstDispatch)
                {           
                    if(disAcc.SVMXC__Territory__c!= null)
                    {               
                        if(map_userid_lTerID.containsKey(disAcc.SVMXC__Dispatcher__c))
                            sExistingIds = map_userid_lTerID.get(disAcc.SVMXC__Dispatcher__c);  
                            
                        if(mapParentandChildTerr.containsKey(disAcc.SVMXC__Territory__c))
                            sALLIDs = mapParentandChildTerr.get(disAcc.SVMXC__Territory__c);    
                            
                        system.debug('sALLIds1>>>>>>>'+sALLIds);
                        system.debug('sExistingIds >>>>>>>'+sExistingIds );
                        if(sALLIds.size() > 0 && sExistingIds.size() > 0)
                        {
                            sALLIds.removeAll(sExistingIds);                  
                        }
                        if(sExistingIds.size() > 0 && setTerrWithChild.size() > 0)
                        {
                            setTerrWithChild.removeAll(sExistingIds);
                        }
                      
                        if(sAllIDs.size() > 0)
                        {
                            for(ID s : sAllIDs)
                            {                          
                                if(setTerrWithChild.contains(s))
                                {
                                    system.debug('Inside Insert>>setTerrWithChild>>>>>'+s);
                                    SVMXC__Dispatcher_Access__c sNew = new SVMXC__Dispatcher_Access__c();
                                    sNew.SVMXC__Dispatcher__c = disAcc.SVMXC__Dispatcher__c;
                                    sNew.SVMXC__Territory__c = s;
                                    lstInsertDispatch.add(sNew);
                                }
                                else
                                {
                                    system.debug('Inside Insert else >>>>>>>'+s);
                                    system.debug('Inside Insert else >>>>disAcc.SVMXC__Dispatcher__c>>>'+disAcc.SVMXC__Dispatcher__c);
                                    noChildDisptachAccessList.add(new noChildDisptachAccess(disAcc.SVMXC__Dispatcher__c, s)); 
                                    system.debug('Inside insert else>>>>noChildDisptachAccessList>>>>>'+noChildDisptachAccessList);
                                }                                
                            } 
                        }
                    }            
                }       
           
            //code for getting the service team 
                if(noChildDisptachAccessList.size() > 0)
                {
                    system.debug('noChildDisptachAccessList>>>>>'+noChildDisptachAccessList);
                    for(NoChildDisptachAccess noChildTerrirtory :noChildDisptachAccessList)
                    {
                        setWithNoChild.add(noChildTerrirtory.territoryId);
                    }
                }   
                system.debug('setWithNoChild>>>>>'+setWithNoChild);
                if(setWithNoChild.size() > 0)
                {
                    mapTerrcode = new Map<ID,SVMXC__Territory__c >([select id,SVMXC__Territory_Code__c from SVMXC__Territory__c where id in: setWithNoChild]);//contains territory id 
                    
                    if(mapTerrcode.size() > 0)
                    {
                        system.debug('mapTerrcode>>>>>'+mapTerrcode);
                        for(SVMXC__Territory__c  terrCode : mapTerrcode.values())
                        {
                            if(terrCode.SVMXC__Territory_Code__c != null)
                            {
                                sCodeIDs.add(terrCode.SVMXC__Territory_Code__c.substring(0,3));
                                mapIDandterrcode.put(terrCode.ID,terrCode.SVMXC__Territory_Code__c);
                            }
                        }
                    }
                    system.debug('scodeids.size()>>>>>>'+scodeids.size());
                    if(sCodeIDs.size() > 0)
                    {
                        system.debug('sCodeIDs>>>>>>'+sCodeIDs);
                        for(SVMXC__Service_Group__c sg : [select id,SVMXC__Group_Code__c, RecordTypeId from SVMXC__Service_Group__c where SVMXC__Group_Code__c in: sCodeIDs])
                        {
                            mapServTeam.put(sg.SVMXC__Group_Code__c, sg);
                        }
                    }
                }
                
                system.debug('mapServTeam>>>>>>'+mapServTeam);
                if(noChildDisptachAccessList.size() > 0)
                {
                    for(NoChildDisptachAccess noChildTerrirtory : noChildDisptachAccessList)
                    {
                       system.debug( 'noChildTerrirtory.userid>>>>>' + noChildTerrirtory.userid);
                       system.debug('noChildTerrirtory.TerritoryId>>>>>>' + noChildTerrirtory.territoryId);
                        if(mapTerrcode.containsKey(noChildTerrirtory.territoryId))
                        {
                            string serCode =  mapIDandterrcode.get(noChildTerrirtory.territoryId);
                            system.debug('inside service team if>>>>>>>'+serCode );                 
                            if(mapServTeam.containsKey(serCode))
                            {
                                system.debug('Set service team >>>>>'+ mapServTeam.get(serCode));
                                serTeam = mapServTeam.get(serCode);
                            }
                        }
                        system.debug('service team>>>>>'+serTeam );
                        if(serTeam != null && serTeam.RecordTypeId == techRecordTypeId){
                        SVMXC__Dispatcher_Access__c sNew = new SVMXC__Dispatcher_Access__c();
                        sNew.SVMXC__Dispatcher__c = noChildTerrirtory.userid;
                        sNew.SVMXC__Territory__c = noChildTerrirtory.territoryId;
	                        sNew.SVMXC__Service_Team__c= serTeam.ID;
                        lstInsertDispatch.add(sNew);
                        system.debug('new dispatch>>>>>'+sNew);                        
                    }            
                                               
                    }            
                }
            }           
        }
        system.debug('MapOfTerritory>>>>>>>'+MapOfTerritory);
        
        system.debug('Insert list size>>>>>'+lstInsertDispatch.size());
        system.debug('lstInsertDispatch>>>>>>'+lstInsertDispatch);
        if(lstInsertDispatch.size() > 0)
        {
            try
            {    
                insert lstInsertDispatch;
            }
            catch(DMLException e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
                return ;
            }
        }
    }
    
   //Method to delete all child territories if parent territory is being deleted
    public void delDispatchAccess(list<SVMXC__Dispatcher_Access__c> lstDispatch, Map<ID, SVMXC__Dispatcher_Access__c> mapNewDispatcherAccess)   
    {
        set<ID> setTerrIds = new set<ID>();//set of territory id
        set<ID> setUserIds = new set<ID>();//set of user id
        set<ID> setChildTerrIds = new set<ID>();//set of all child territories
        //List<SVMXC__Dispatcher_Access__c> listExistingDispatch = new List<SVMXC__Dispatcher_Access__c>();//list of existing records
        //List<SVMXC__Dispatcher_Access__c> listDelDispatch = new List<SVMXC__Dispatcher_Access__c>();//list of records to be deleted
        Map<ID, SVMXC__Dispatcher_Access__c> mapExistingDispatch = new Map<ID, SVMXC__Dispatcher_Access__c>();//list of existing records
        Map<ID, SVMXC__Dispatcher_Access__c> mapDelDispatch = new Map<ID, SVMXC__Dispatcher_Access__c>();//list of records to be deleted
        
        for(SVMXC__Dispatcher_Access__c disAcc : lstDispatch)
        {
            if(disAcc.SVMXC__Territory__c != null && (mapNewDispatcherAccess == null || 
            (mapNewDispatcherAccess != null && (mapNewDispatcherAccess.get(disAcc.ID).SVMXC__Territory__c != disAcc.SVMXC__Territory__c || mapNewDispatcherAccess.get(disAcc.ID).SVMXC__Territory__c == null))))
            {
                setTerrIds.add(disAcc.SVMXC__Territory__c);
            }
            if(disAcc.SVMXC__Dispatcher__c != null && (mapNewDispatcherAccess == null || 
            (mapNewDispatcherAccess != null && (mapNewDispatcherAccess.get(disAcc.ID).SVMXC__Dispatcher__c != disAcc.SVMXC__Dispatcher__c || mapNewDispatcherAccess.get(disAcc.ID).SVMXC__Dispatcher__c == null))))
            {
                setUserIds.add(disAcc.SVMXC__Dispatcher__c); 
            }           
        }
            
        if(setTerrIds.size() > 0)   
        {
            Map<Id,SVMXC__Territory__c> MapOfTerritory = new Map<Id,SVMXC__Territory__c>([Select ID, SVMXC__Parent_Territory__c from SVMXC__Territory__c
                                                                    where SVMXC__Parent_Territory__c in : setTerrIds or ID in :setTerrIds ]);//store territory id and their child 
            if(MapOfTerritory.size() > 0)
            {
                for(SVMXC__Territory__c terrt : MapOfTerritory.values()) 
                {
                    setChildTerrIds.add(terrt.id);      
                }
                if(setChildTerrIds.size() > 0)
                {
                    mapExistingDispatch = new Map<ID, SVMXC__Dispatcher_Access__c>([select SVMXC__Territory__c, SVMXC__Dispatcher__c from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c in: setUserIds and SVMXC__Territory__c in : setChildTerrIds]);
                    
                    if(mapExistingDispatch.size() > 0)
                    {
                        for(SVMXC__Dispatcher_Access__c lstDispDel : mapExistingDispatch.values())
                        {
                            mapDelDispatch.put(lstDispDel.ID, lstDispDel);        
                        }
                    }   
                }   
            }
        }
        if(mapDelDispatch.size() > 0)
        {
            try
            {
                delete mapDelDispatch.values();
            }
            catch(DMLException e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
                return ;
            }
        }  
    }
    
    //Method to Show error if the record exist with same user and territory called in before trigger
     public void checkDuplicateRecord (list<SVMXC__Dispatcher_Access__c>lstDispatch, Map<Id,SVMXC__Dispatcher_Access__c>oldMap, Map <Id,SVMXC__Dispatcher_Access__c> newMap)
    {
        set<string> setUserId=new set<string>();
        set<string> setTerrId=new set<string>();
        for(SVMXC__Dispatcher_Access__c disAcc: lstDispatch)
        {
            if(disAcc.SVMXC__Territory__c != null)
            {
                setTerrId.add(disAcc.SVMXC__Territory__c);//add the territory id into set
            }
            if(disAcc.SVMXC__Dispatcher__c!= null)
            {
                setUserId.add(disAcc.SVMXC__Dispatcher__c);//add the user id into set
            }                           
        }
        
        if(setUserId.size() > 0 && setTerrId.size() > 0)
        {
            Map<ID, SVMXC__Dispatcher_Access__c> mapExistingDispatch = new Map<ID, SVMXC__Dispatcher_Access__c>([select Name,SVMXC__Territory__c, SVMXC__Dispatcher__c from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c   in: setUserId and SVMXC__Territory__c  in : setTerrId]);
            
            for(SVMXC__Dispatcher_Access__c objDispatch : lstDispatch)
            {
                if(mapExistingDispatch.size() > 0)
                {
                    for(SVMXC__Dispatcher_Access__c existDA : mapExistingDispatch.values())
                    {
                        if(objDispatch .SVMXC__Territory__c != null && existDA.SVMXC__Territory__c != null && objDispatch.SVMXC__Dispatcher__c != null &&  existDA.SVMXC__Dispatcher__c != null && newMap.get(existDA.id).SVMXC__Territory__c!=oldMap.get(existDA.id).SVMXC__Territory__c && newMap.get(existDA.id).SVMXC__Dispatcher__c!=oldMap.get(existDA.id).SVMXC__Dispatcher__c)
                        
                        {
                            objDispatch.addError('Duplicate record found with Id : ' + existDA.Name);
                        }
                    }
                }
            }
        }   
    }

    public class NoChildDisptachAccess
    {
        ID userId{get;set;}
        ID territoryId{get;set;}
        public NochildDisptachAccess(Id usrId, Id terrId)
        {
            this.userId = usrId;
            this.territoryId = terrId;
        }
    }
}
@isTest
public class SR_Class_DispatchAccess_test
{
     public static testMethod void testCreateData()
     {
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        Id RecTypId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        
        List<SVMXC__Dispatcher_Access__c> lstInsertDispatch = new  List<SVMXC__Dispatcher_Access__c >();
        SVMXC__Dispatcher_Access__c objDispatch = new SVMXC__Dispatcher_Access__c();
        
        system.runas(systemuser)
        {
            
            SVMXC__Service_Group__c objServGrp11 = new SVMXC__Service_Group__c();
            objServGrp11.SVMXC__Group_Code__c = 'AT6-E';
            objServGrp11.RecordtypeId = RecTypId;
            objServGrp11.Name = 'Service Test2';
            insert objServGrp11;
            
            SVMXC__Territory__c parentTerr = new  SVMXC__Territory__c(Name='t1', SVMXC__Territory_Code__c = 'AT6-E');        
            insert parentTerr;
            SVMXC__Territory__c childParentTerr = new  SVMXC__Territory__c(Name='t3',SVMXC__Parent_Territory__c=parentTerr.id);        
            insert childParentTerr ;
            childParentTerr.Name='t5';
            update childParentTerr;
            
            List<SVMXC__Territory__c> lstInsertTerr = new List<SVMXC__Territory__c>();       
            SVMXC__Territory__c childTerr1 = new  SVMXC__Territory__c(Name='t2',SVMXC__Parent_Territory__c=parentTerr.id);
            lstInsertTerr.add(childTerr1 );
            SVMXC__Territory__c childTerr2 = new  SVMXC__Territory__c(Name='t4',SVMXC__Parent_Territory__c=parentTerr.id);
            lstInsertTerr.add(childTerr2 );
            SVMXC__Territory__c childTerr3 = new  SVMXC__Territory__c(Name='t5',SVMXC__Parent_Territory__c=childParentTerr.id,SVMXC__Territory_Code__c='AT6');      
            lstInsertTerr.add(childTerr3 );
            SVMXC__Territory__c childTerr4 = new  SVMXC__Territory__c(Name='t6',SVMXC__Parent_Territory__c=childParentTerr.id);
            lstInsertTerr.add(childTerr4 );
            insert lstInsertTerr;
            
            /*    List<SVMXC__Territory__c> lstInsertTerr = new List<SVMXC__Territory__c>();        
            lstInsertTerr.add(createTerritory('t2',parentTerr.id,''));
            lstInsertTerr.add(createTerritory('t4',parentTerr.id,''));
            lstInsertTerr.add(createTerritory('t5',childParentTerr.id,'AT6'));
            lstInsertTerr.add(createTerritory('t6',childParentTerr.id,''));
            insert lstInsertTerr;*/
            
             SVMXC__Service_Group__c objServGrp1 = new SVMXC__Service_Group__c();
            objServGrp1.SVMXC__Group_Code__c = 'AT6-E';
            objServGrp1.RecordtypeId = RecTypId;
            objServGrp1.Name = 'Service Test';
            insert objServGrp1;
            
            SVMXC__Service_Group__c objServGrp2 = new SVMXC__Service_Group__c();
            objServGrp2.SVMXC__Group_Code__c = 'AT6';
            objServGrp2.RecordtypeId = RecTypId;
            objServGrp2.Name = 'Service Test';
            insert objServGrp2;
            
            SVMXC__Service_Group__c objSerGrp = new SVMXC__Service_Group__c(SVMXC__Group_Code__c ='AT6',Name = 'TestServ' );
            objSerGrp.Equipment_Required__c = True;
            objSerGrp.Dispatch_Queue__c = 'DISP CN';
            insert objSerGrp;
            
            objSerGrp.Dispatch_Queue__c = 'DISP CH';
            update objSerGrp;
            
            objDispatch.SVMXC__Territory__c = parentTerr.id;
            objDispatch.SVMXC__Dispatcher__c = systemuser.id;
            insert objDispatch;
            
            lstInsertDispatch.add(objDispatch);
       }
        objDispatch.SVMXC__Dispatcher__c = userInfo.getUserId();
        update objDispatch;
                
        SR_Class_DispatchAccess objcls = new SR_Class_DispatchAccess();
        objcls.CreateDispatchAccessTerritory(lstInsertDispatch);
      //  objcls.delDispatchAccess(lstInsertDispatch);
    
    }
    
  
    public SVMXC__Territory__c createTerritory(string terrName,ID parentTerr,string terrCode)
    {
         SVMXC__Territory__c objTerr = new  SVMXC__Territory__c();
         objTerr.Name=terrName;
         objTerr.SVMXC__Parent_Territory__c=parentTerr;
         objTerr.SVMXC__Territory_Code__c=terrCode;
         return objTerr ;
    }
    
        
}
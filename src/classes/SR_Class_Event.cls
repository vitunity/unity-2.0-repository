/*******************************************************************************************************************************
@Author   :        Mohit Sharma
@Date      :       6-May-2014
@Description   :    This Class is used for calling methods from trigger
@Last Modified by:  Nikita Gupta
@Last Modified reason: added null checks
@Last Modified Date : 6/9/2014
@US# : US4082
@Last Modified by:  Shubham Jaiswal    
@Last Modified reason: US4044 : added method InstallationWorkOrderAssignmentEmail
@Last Modified Date : 21/7/2014
*******************************************************************************************************************/
public class SR_Class_Event
{
    //priti, 28-Feb-2014 to update fields on event - genercice method can be reused to update other fields on event 
    public void updateFieldsOnEvent(List<Event> lstOfEventsToUpdate)
    {
        Set<Id> setOfEventId = new Set<Id>();
        Set<String> setOfEveLoc = new Set<String>();    
        set<String> setBookingType = new set<String>();
        set<String> setEventStatus = new set<String>();
        map<string, Event_Properties__c> mapBookingTypeStatus_Event = new map<string, Event_Properties__c>();
        Map<String, SVMXC__Site__c> mapOfLocaNameLocation = new Map<String, SVMXC__Site__c>();
        List<EventRelation> lstOfEventRelation ; //Moved the list declaration to class level. Nikita, 6/13/2014 
        //US2066, Added by Nikita, 6/27/2014 to update Evevnt record type
        Schema.DescribeSObjectResult resultWO = SVMXC__Service_Order__c.sObjectType.getDescribe();// added by Nikita to get the object prefix
        string keyPrefix = resultWO.getKeyPrefix();//
        Schema.DescribeSObjectResult resultAcc = Account.sObjectType.getDescribe();
        string keyPrefixAcc = resultAcc.getKeyPrefix();//  
        ID eventRecType = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Salesforce').getRecordTypeId();
        ID eventCustomer = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Customer Visit').getRecordTypeId();             //INC4725002, Added by Harvey, 11/09/2017
        for(Event ev : lstOfEventsToUpdate)
        {
            setOfEventId.add(ev.id); //set of Event Id
            //set event type from global action story#STRY0081360
            
            if(ev.Event_Record_Type__c != null){
            ev.RecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get(ev.Event_Record_Type__c).getRecordTypeId();
            }
            

            //priti, US4094, 28-Feb-2014 - check if event location is not null
            if(ev.Location != null)
            {
                setOfEveLoc.add(ev.Location);               
            }
            //INC4725002, Added by Harvey, 11/09/2017
            String eventWhatID = ev.whatID; 
            if((ev.whatID == null || eventWhatID.startsWith(keyPrefixAcc) == false) && ev.RecordTypeID == eventCustomer)
            {
                ev.addError('Account cannot be blank in a customer event');
            }
            //US2066, Added by Nikita, 6/27/2014
            if(ev.whatID != null)
            {       
                if(eventWhatID != null && keyPrefix != null && eventWhatID.startsWith(keyPrefix) == true || ev.Booking_Type__c != null && ev.RecordTypeID != eventCustomer)
                {
                    ev.RecordTypeID = eventRecType;
                }
            }
            if(ev.Booking_Type__c != null && ev.Status__c != null)
            {
                setBookingType.add(ev.Booking_Type__c);
                setEventStatus.add(ev.Status__c);
            }
        }
        
        if(setBookingType.size() > 0 && setEventStatus.size() > 0) 
            system.debug('setBookingType--'+setBookingType);system.debug('setEventStatus--'+setEventStatus);
        {
            for(Event_Properties__c varEP : [select Duration__c, Booking_Type__c, Id, Status__c from Event_Properties__c where
                                                Booking_Type__c in : setBookingType and Status__c in: setEventStatus limit 1])
            {
                String bookingType_Status =  varEP.Booking_Type__c + '' + varEP.Status__c;
                mapBookingTypeStatus_Event.put(bookingType_Status, varEP);
            }
        }
        
        // priti, US4094, 28-Feb-2014- if event location is not null get Location Capacity form Location record
        if(setOfEveLoc.size() > 0)
        {
            List<SVMXC__Site__c> lstOfLocation = [select Name, Location_Capacity__c from SVMXC__Site__c where name in : setOfEveLoc];
        
            //creating map Of location name and Location record, this map can be further reused to update other data from Location record  on Event record 
            if(lstOfLocation.size() > 0)
            {
                for(SVMXC__Site__c loc : lstOfLocation)
                {
                    mapOfLocaNameLocation.put(loc.Name, loc);
                }
            }
        }    

        if(setOfEventId.size() > 0)
        {
            lstOfEventRelation = [select id from EventRelation where EventId in : setOfEventId];
        }

        for(Event varEvent : lstOfEventsToUpdate)
        {
            //priti, US4094, 28-Feb-2014 - if Event's Location is not null update Location Capacity from Location Object on Event
            if(varEvent.Location != null && mapOfLocaNameLocation.containsKey(varEvent.Location) && mapOfLocaNameLocation.get(varEvent.Location) != null)
            {
                varEvent.Location_Capacity__c = mapOfLocaNameLocation.get(varEvent.Location).Location_Capacity__c;
            } 

            if(lstOfEventRelation != null)  //null check added by Nikita on 6/13/2014
            {
                varEvent.Current_Attendee_Count__c = lstOfEventRelation.size();
            }   

            system.debug('varEvent.DurationInMinutes before value is  = ' + varEvent.DurationInMinutes);
            if(mapBookingTypeStatus_Event.size() > 0 && varEvent.Booking_Type__c != null && varEvent.Status__c != null)
            {    
                String bookingType_Status = varEvent.Booking_Type__c + '' + varEvent.Status__c;

                if(bookingType_Status != null && mapBookingTypeStatus_Event.containsKey(bookingType_Status) && mapBookingTypeStatus_Event.get(bookingType_Status).duration__c != null)
                {
                    varEvent.DurationInMinutes = integer.valueOf(mapBookingTypeStatus_Event.get(bookingType_Status).duration__c);
                    system.debug('varEvent.DurationInMinutes after value is  =' + varEvent.DurationInMinutes );
                }
            }
        }        
    }

    public void updateEventWorkOrder(list<Event> lstEvent, map<Id, Event> newMap, map<Id, Event> oldmap)
    {
        set<string> stEventId = new set<string>();
        list<SVMXC__Service_Order__c> lstUpdateWorkOrder = new list<SVMXC__Service_Order__c>();
        
        for(Event varEvent: lstEvent)
        {
            if(varEvent.Event_Number__c != null && !String.isBlank(varEvent.Event_Number__c) && newMap.get(varEvent.id).Status__c == 'Cancelled' && (oldmap == null || oldmap.get(varEvent.id).Status__c != 'Cancelled')) //oldmap null checks added by Nikita on 6/9/2014
            {
                stEventId.add(varEvent.Event_Number__c);  
            } 
        }
        
        if(stEventId.size() > 0) //null check added by Nikita on 6/9/2014
        {
            List<SVMXC__Service_Order__c> lstWorkOrder = [select id,SVMXC__Order_Status__c from SVMXC__Service_Order__c where Event_Number__c  != Null AND Event_Number__c IN : stEventId AND SVMXC__Order_Status__c != :Label.SR_OrderStatus];// Checking for Cancelled OrderStatus
            if(lstWorkOrder.size() > 0)
            {
                for(SVMXC__Service_Order__c varWO : lstWorkOrder)
                {
                    varWO.SVMXC__Order_Status__c = Label.SR_OrderStatus;  // Set  Cancelled OrderStatus
                    lstUpdateWorkOrder.add(varWO);
                }
            }
            if(lstUpdateWorkOrder.size()>0)
            {
                update lstUpdateWorkOrder;
            }
        }   
    }
 /** 1C Yukti - 31 Mar 15   - INSTALLATION IS IN 1C
    public void InstallationWorkOrderAssignmentEmail(List<Event> lstEvent)
    {
        EmailTemplate et=[Select id from EmailTemplate where name='SR_Installation WO Email to Technician'];
        //list of emails
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        for(Event eve : lstEvent)
        {
            if(eve.whatid != null)
            {
                Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();

                singleMail.setTargetObjectId(eve.OwnerId);
                
                singleMail.setWhatId(eve.Whatid);

                singleMail.setTemplateId(et.Id);

                singleMail.setSaveAsActivity(false);
                
                //String[] testMail = new String[] {'shubham.jaiswal1@wipro.com'};
                //singleMail.setToAddresses(testMail);
                //mail.setSenderDisplayName('Partner Support');

                singleMail.setBccSender(false);             

                singleMail.setsaveAsActivity(false);

                emails.add(singleMail);
                system.debug('@@@@@singleMail' + singleMail);
                system.debug('@@@@@emails' + emails.size());
                //Messaging.sendEmail(new Messaging.Email[] { singleMail });
            }
        }
        if(emails.size() > 0)
        {
            Messaging.sendEmail(emails);
        }
    }
     1C Yukti - 31 Mar 15  **/
    /* public void createMasterWorkOrder(List<Event> lstEvent)
    {
        map<id,product2> map_Product = new map<id,product2>();
        set<string> stProductID =new set<string>();
        list<SVMXC__Service_Order__c> lstMasterWO =new list<SVMXC__Service_Order__c>();
        //get Record Type id
            Schema.SObjectType sObj = Schema.getGlobalDescribe().get('SVMXC__Service_Order__c'); 
            Schema.DescribeSObjectResult sObjReslt = sObj.getDescribe();  
            Map<String,Schema.recordTypeInfo> mapRecordsType = new Map<String,Schema.recordTypeInfo>(); 
            mapRecordsType = sObjReslt.getRecordTypeInfosByName();
            id rectypeId = MapRecordsType.get('Training').getRecordTypeId(); 
            string  strCaseRT =string.valueof(Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId()); // Training Consulting
            //Varian Account
            List<account> act=new List<account>();
            act=[select id from  account where Name=:Label.SR_VarianAccountName];
            set<string> stLocationName=new set<string>();
        for(Event varEVT: lstEvent)
        {
            if(varEVT.Booking_Type__c == 'Classroom Teaching')
            {    
                if(varEVT.whatID !=null)
                {   Schema.SObjectType token = varEVT.WhatId.getSObjectType();
                    if(String.valueOf(token) == 'Product2')
                        stProductID.add(varEVT.whatID);
                }
                if(varEVT.Location !=null)
                  stLocationName.add(varEVT.Location );
            }
        }
        // Location Related to Event
        Map<string,SVMXC__Site__c> Map_Name_Site = new Map<string,SVMXC__Site__c>();
        if(stLocationName.size()>0)
        {
            list<SVMXC__Site__c> lstEventLocation = [select id, SVMXC__Account__c from SVMXC__Site__c where name =: stLocationName];                    
            for(SVMXC__Site__c varSite : lstEventLocation )
                Map_Name_Site.put(varSite.name,varSite);
        }
        //Product Related to Event
        if(stProductID.size()>0)
        {
            map_Product=new map<id,product2>([select id, name,Credit_s_Required__c,General_Item_Category__c,Material_Group__c,Description,Current_Product_Version__c,SVMXC__Product_Line__c,Budget_Hrs__c  from product2 where id IN:stProductID  ]);
        }
        list<case> lstCase=new list<case>();
        for(Event varEVT: lstEvent)
        {   
            if(map_Product.containskey(varEVT.whatID)  && varEVT.Booking_Type__c == 'Classroom Teaching')
            { // Case
                case objcs=new case();
                objcs.Recordtypeid=strCaseRT;
                objcs.reason='Education/Training Request';
                objcs.Subject='Varian Training';
                if(act.size()>0)
                    objcs.AccountId=act[0].id;
                lstCase.add(objcs);
            //Work Order
               SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();
                objWO.RecordtypeID = rectypeId;
               // objWO.ownerid = varEVT.OwnerId;            
                if(varEVT.Subject == 'Classroom Training')            
                    objWO.SVMXC__Order_Type__c = 'Training Services';
                objWO.SVMXC__Product__c = varEVT.WhatId;
                objWO.SVMXC__Preferred_Start_Time__c = varEVT.ActivityDateTime;
                objWO.SVMXC__Preferred_End_Time__c = varEVT.ActivityDateTime + varEVT.DurationInMinutes; 
                objWO.SVMXC__Purpose_of_Visit__c = varEVT.Subject;
                objWO.SVMXC__Case__r = objcs;
                if(act.size()>0)
                    objWO.SVMXC__Company__c = act[0].id;
                objWO.Billable__c =false;
                if(Map_Name_Site.containskey(varEVT.Location))
                {
                    objWO.SVMXC__Site__c = Map_Name_Site.get(varEVT.Location).id;
                    objWO.SVMXC__Company__c = Map_Name_Site.get(varEVT.Location).SVMXC__Account__c;
                }
                objWO.SVMXC__Problem_Description__c = varEVT.Subject;
                objWO.Is_Master_WO__c = true;
                objWO.Event_Number__c = varEVT.Event_Number__c;
                objWO.Service_Duration_in_minutes__c = varEVT.DurationInMinutes; //duration is mandatory
                objWO.Event_Id__c = varEVT.id;
                lstMasterWO.add(objWO);
            }
        }
            if(lstCase.size()>0)
                insert lstCase;
            if(lstMasterWO.size()>0)
                insert lstMasterWO;
    }*/
    
    /*
     * Create Time Entry from event
     */  
    public void createTimeEntry(list<Event> eventList, map<Id,Event> oldMap){
        
        set<Id> userIds = new set<id>();
        set<Id> accountIds = new set<id>();
        for(Event evnObj : eventList){
            if(evnObj.Time_Entry__c == true){
                userIds.add(evnObj.OwnerId);
                accountIds.add(evnObj.WhatId);
            }
        }
        
        if(!userIds.isEmpty()){
            set<String> timeCardProfileIds = new set<String>();
            /*Get map of technician */
            map<Id,SVMXC__Service_Group_Members__c> technicianMap = new map<Id,SVMXC__Service_Group_Members__c>();
            for(SVMXC__Service_Group_Members__c techObj : [select Id, User__c, Timecard_Profile__c from SVMXC__Service_Group_Members__c where User__c in : userIds]){
                if(techObj.Timecard_Profile__c != null){
                    technicianMap.put(techObj.User__c,techObj);
                    timeCardProfileIds.add(techObj.Timecard_Profile__c);
                }
            }
            if(timeCardProfileIds.isEmpty()) return;
            /*Get account map */
            //map<Id,Account> accountMap = new map<Id,Account> {[select id from Account where Id in : accountIds]};
            /*Get Timecard profile and organizational activities*/
            map<Id,Timecard_Profile__c> timeCardProfileMap;
            timeCardProfileMap = new map<Id,Timecard_Profile__c> ([select Id, (select id from Organizational_Activities__r where Name =: 'Customer Visit' or Name =:'115 -- ハッピービジット' limit 1) 
            from Timecard_Profile__c where Id in : timeCardProfileIds]);
            
            list<SVMXC_Time_Entry__c> listOfTimeEntries = new list<SVMXC_Time_Entry__c>();
            /*Create Time Entry */
            for(Event evnObj : eventList){
                if(evnObj.Time_Entry__c == true){
                    //Account accountObj = accountMap.get(evnObj.parentId);
                    SVMXC__Service_Group_Members__c technicianObj = technicianMap.get(evnObj.Ownerid);
                    
                   
                    
                    SVMXC_Time_Entry__c timeEntryObj = new SVMXC_Time_Entry__c();
                    timeEntryObj.recordtypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC_Time_Entry__c').get('Direct_Hours');//set direct 
                    timeEntryObj.Technician__c = technicianObj.id;
                    timeEntryObj.Start_Date_Time__c = evnObj.StartDateTime;
                    timeEntryObj.End_Date_Time__c = evnObj.EndDateTime;
                    timeEntryObj.Interface_Status__c  = 'Process';
                    //timeEntryObj.Organizational_Activity__c = oActivityObj.Id;
                    if(timeCardProfileMap.containsKey(technicianObj.Timecard_Profile__c)){
                        Timecard_Profile__c timecardObj = timeCardProfileMap.get(technicianObj.Timecard_Profile__c);
                        Organizational_Activities__c oActivityObj = timecardObj.Organizational_Activities__r[0];
                        timeEntryObj.Organizational_Activity__c = oActivityObj.Id;
                    }
                    
                    timeEntryObj.Comments__c = evnObj.Description;
                    timeEntryObj.Event__c = evnObj.id;
                    Id accountid = evnObj.WhatId;
                    if(accountid.getsobjecttype().getdescribe().getname() == 'Account')
                    timeEntryObj.Associated_Account__c = evnObj.WhatId;
                    listOfTimeEntries.add(timeEntryObj);
                }
            }
            
            if(!listOfTimeEntries.isEmpty()){
                insert listOfTimeEntries;
            }
        }
    }
}
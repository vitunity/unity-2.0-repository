/*
Author : Mohit
Date :5-Jan-2015
Description: DE1555
*/
public Class SR_Class_EventLocation
{

/*******************Creation of Master Work Order******************/
public static   Map<string,State_Province__c> mapState = new Map<string,State_Province__c>();
public static   Map<string,Country__c> mapCountry = new Map<string,Country__c>();

 /** 1C Bushra
    public void createMasterWorkOrder(List<Event_Location__c > lstEvtLocation)
    {
    
        map<id,product2> map_Product = new map<id,product2>();  // Map for Product
        map<string,SVMXC__Site__c> Map_Name_Site = new Map<string,SVMXC__Site__c>(); //Map for site
        set<string> stProductID =new set<string>();  // Set holding Product Id
        set<string> stLocationName=new set<string>();
        list<SVMXC__Service_Order__c> lstMasterWO =new list<SVMXC__Service_Order__c>(); // List to Create Master Work Order
        list<case> lstCase=new list<case>();           // List To Create Case
        Set<string> stEventNumber =new set<string>();  // Set To Hold Event Number
    
        //get Record Type id Case/Work Order
        
        string  rectypeId =string.valueof(Schema.getGlobalDescribe().get('SVMXC__Service_Order__c').getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId()); // Training Consulting
        string  strCaseRT =string.valueof(Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId()); // Training Consulting
    
    
        //Varian Account
        List<account> act=new List<account>();
        act=[select id from  account where Name=:Label.SR_VarianAccountName];
        Map<string,Event_Location__c> MapEvtNbr_Location = new Map<string,Event_Location__c>();
        Map<string,event> mapEvetUpdate=new Map<string,event>();
        for(Event_Location__c varEVTLoc: lstEvtLocation)
        {
            if(varEVTLoc.Event_Number__c !=null)
            {
                stEventNumber.add(varEVTLoc.Event_Number__c);
                MapEvtNbr_Location.put(varEVTLoc.Event_Number__c,varEVTLoc);
            }
            if(varEVTLoc.Location__c !=null)
            {
                stLocationName.add(varEVTLoc.Location__c );
            }
            
        }
        //Getting Event Number
        map<string,event> MapEvent = new map<string, event>([Select id,Local_End_Date_Time__c,Local_Start_Date_Time__c,ActivityDateTime,DurationInMinutes,Event_Number__c,Booking_Type__c,WhatId,Subject from Event where Event_Number__c IN : stEventNumber]);
        Map<string,Event> MapEventNbr_Event=new Map<string,Event>();
        
        for(event VarEvent  : MapEvent.values())
        {
                
            MapEventNbr_Event.put(VarEvent.Event_Number__c,VarEvent);
            if(VarEvent.WhatId !=null)
            {
                Schema.SObjectType token = VarEvent.WhatId.getSObjectType();
                if(String.valueOf(token) == 'Product2')
                { 
                    stProductID.add(VarEvent.whatID);
                }
            }
        }  
        //Product Related to Event
        if(stProductID.size()>0)
        {
            map_Product=new map<id,product2>([select id, name,Credit_s_Required__c,General_Item_Category__c,Material_Group__c,Description,Current_Product_Version__c,SVMXC__Product_Line__c,Budget_Hrs__c  from product2 where id IN:stProductID  ]);
        }
        for(Event varEVT: MapEvent.Values())
        {   
            if(map_Product.containskey(varEVT.whatID)  /*&& varEVT.Booking_Type__c == 'Classroom Teaching')
            {
              // Case
                case objcs = new case();
                objcs.Recordtypeid=strCaseRT;
                objcs.reason='Education/Training Request';
                objcs.Subject='Varian Training';
                if(act.size()>0)
                {
                    objcs.AccountId=act[0].id;
                }
                lstCase.add(objcs);
            //Work Order
            
               SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();
                objWO.RecordtypeID = rectypeId;
                //objWO.ownerid = varEVT.OwnerId;            
                if(varEVT.Subject == 'Classroom Training')            
                    objWO.SVMXC__Order_Type__c = 'Training Services';
                    
                objWO.SVMXC__Product__c = varEVT.WhatId;
                if(varEVT.ActivityDateTime !=null)
                {
                objWO.SVMXC__Preferred_Start_Time__c = varEVT.ActivityDateTime;
                objWO.SVMXC__Preferred_End_Time__c = varEVT.ActivityDateTime + varEVT.DurationInMinutes; 
                }
                objWO.SVMXC__Purpose_of_Visit__c = varEVT.Subject;
                objWO.SVMXC__Case__r = objcs;
                if(act.size()>0)
                {
                    objWO.SVMXC__Company__c = act[0].id;
                }
                
                objWO.Billable__c =false;
               
                objWO.SVMXC__Problem_Description__c = varEVT.Subject;
                objWO.Is_Master_WO__c = true;
                objWO.Event_Number__c = varEVT.Event_Number__c;
                objWO.Service_Duration_in_minutes__c = varEVT.DurationInMinutes; //duration is mandatory
                objWO.Event_Id__c = varEVT.id;
                
                lstMasterWO.add(objWO);
            }
            
            Event_Location__c evtLoc = MapEvtNbr_Location.get(varEVT.Event_Number__c);
            If(mapState.containskey(evtLoc.State_Province__c))
            {string strtDateWithTZ='';
                if(varEVT.Local_Start_Date_Time__c !=null)
                { 
                system.debug('Event Id'+varEVT.id);
                system.debug('Event Id'+varEVT.Local_Start_Date_Time__c);
                system.debug('Year'+integer.valueOf(varEVT.Local_Start_Date_Time__c.substring(0,4)));
                system.debug('Month'+varEVT.Local_Start_Date_Time__c .substring(5,7));
                system.debug('Day'+varEVT.Local_Start_Date_Time__c .substring(8,10));
                datetime startDate = DateTime.newInstance(integer.valueOf(varEVT.Local_Start_Date_Time__c.substring(0,4)),
                                                        integer.valueOf(varEVT.Local_Start_Date_Time__c .substring(5,7)), 
                                                        integer.valueOf(varEVT.Local_Start_Date_Time__c .substring(8,10)), 
                                                        integer.valueOf(varEVT.Local_Start_Date_Time__c .substring(11,13)),
                                                        integer.valueOf(varEVT.Local_Start_Date_Time__c .substring(14,16)), 
                                                        integer.valueOf(varEVT.Local_Start_Date_Time__c .substring(17,19))
                                                        );
                system.debug('debug---'+startDate);
                if(mapState.get(evtLoc.State_Province__c).Timezone__c !=null)
                {
                 strtDateWithTZ= startDate.format('MM/dd/YYYY hh:mm:ss a', mapState.get(evtLoc.State_Province__c).Timezone__c);
                }
                else 
                {
                strtDateWithTZ= startDate.format('MM/dd/YYYY hh:mm:ss a', mapState.get(evtLoc.State_Province__c).Country__r.Timezone__c);
                
                }
                system.debug('debug---'+strtDateWithTZ);
                
                System.debug('GMTTimeZoneYear'+integer.valueOf(strtDateWithTZ.substring(6,10)));
                System.debug('GMTTimeZoneYear'+integer.valueOf(strtDateWithTZ.substring(0,2)));
                System.debug('GMTTimeZoneYear'+integer.valueOf(strtDateWithTZ.substring(3,5)));
                
                
                varEVT.StartDateTime  =      DateTime.newInstance(integer.valueOf(strtDateWithTZ.substring(6,10)),
                                                    integer.valueOf(strtDateWithTZ.substring(0,2)), 
                                                    integer.valueOf(strtDateWithTZ.substring(3,5)), 
                                                    integer.valueOf(strtDateWithTZ.substring(11,12)),
                                                    integer.valueOf(strtDateWithTZ.substring(14,16)), 
                                                    integer.valueOf(strtDateWithTZ.substring(17,19))
                                                    );
                varEVT.ActivityDateTime = varEVT.StartDateTime;
                varEVT.EndDateTime=varEVT.StartDateTime.addMinutes(Integer.valueof(varEVT.DurationInMinutes));
                
                
                mapEvetUpdate.put(varEVT.id,varEVT);
                }
            }
            
            
            
            
            
        }
        if(lstCase.size()>0)
        {
            insert lstCase;
        }
        if(lstMasterWO.size()>0)
        {
            insert lstMasterWO;
        }
        if(mapEvetUpdate.size()>0)
        {
        update mapEvetUpdate.values();
        }
        
    }  1C Bushra **/
    
    /*******************Set State/Country on Event Location******************/

    public void setStateCountryField(List<Event_Location__c > lstEvtLocation)
    {
        //Set
        set<string> stStateValue = new set<string>(); // Set for S
        set<string> stCountry = new set<string>();
        
        //Map
        
        
        Map<string,State_Province__c> mapStateCode_State=new Map<string,State_Province__c>();
        for(Event_Location__c varEVTLoc: lstEvtLocation)
        {
            if (varEVTLoc.State_Province_loaded__c !=null)
            {
                stStateValue.add(varEVTLoc.State_Province_loaded__c);
            }
            if (varEVTLoc.Country_loaded__c !=null)
            {
                stCountry.add(varEVTLoc.Country_loaded__c);
            }
        }
        
        
        mapState=new map<string,State_Province__c>([select id,name,Code__c,
                                                Country__c,Country__r.Timezone__c,Country__r.Name,
                                                Timezone__c
                                                from State_Province__c where (Code__c IN: stStateValue OR Name IN :stStateValue) 
                                                AND 
                                                (Country__r.Name IN:stCountry ) 
                                                ]);
                                                
        for(State_Province__c VarState: mapState.Values()) // Addig Name/Code-State to Map
        {
            mapStateCode_State.put(VarState.name,VarState);
            mapStateCode_State.put(VarState.Code__c,VarState);
        }
        
        for(Event_Location__c varEVTLoc: lstEvtLocation)
        {
            if(varEVTLoc.State_Province_loaded__c !=null)
            {
                if(mapStateCode_State.containskey(varEVTLoc.State_Province_loaded__c))
                {
                    State_Province__c varState = mapStateCode_State.get(varEVTLoc.State_Province_loaded__c);
                    if(varState !=null && varState.Country__r.Name==varEVTLoc.Country_loaded__c)
                    {
                    varEVTLoc.State_Province__c = varState.id;
                    varEVTLoc.Country__c= varState.Country__c;
                    }
                
                }
            }
        }
    
    }

}
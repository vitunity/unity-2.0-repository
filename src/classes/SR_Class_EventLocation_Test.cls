/**************************************
@Author         :   Shradha Jain
@Created Date   :   23/02/2015
**************************************/


@isTest(SeeAllData=true)
private class SR_Class_EventLocation_Test{
     
    static Account objAcc = SR_testdata.creteAccount();
    static Contact objCont = SR_testdata.createContact();
    static State_Province__c objStProv = new State_Province__c();
    static Country__c objCountry = new Country__c();
    static Product2 objProd = SR_testdata.createProduct();
    static SVMXC__Site__c objLoc = new SVMXC__Site__c();
    static List<Event> listOfEvent = new List <Event>();    
    static List<Event_Location__c> listOfEL = new List <Event_Location__c>();   
    static{
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs (thisUser){
            objCont.mailingcountry = 'India';
            insert objCont;  
            
            objAcc.Name = Label.SR_VarianAccountName;
            objAcc.Country__c = 'India';
            insert objAcc;
            
            objLoc.Name = 'Test Location';
            objLoc.SVMXC__Account__c = objAcc.ID;
            insert objLoc;
            
            objCountry.Name = 'USA';
            objCountry.Timezone__c = 'America/New_York';
            insert objCountry;  
            
            objStProv.Timezone__c = 'America/New_York';
            objStProv.Name = 'NY';
            objStProv.Code__c = 'NY';
            objStProv.Country__c = objCountry.id;
            insert objStProv;
            
            
            objProd.Material_Group__c = Label.Product_Material_group;
            objProd.Budget_Hrs__c = 4;
            objProd.UOM__c = 'HR';
            objProd.Product_Type__c = label.SR_Product_Type_Sellable;
            objProd.ProductCode = 'Test Material No 001';
            objProd.ERP_PCode_4__c = 'H012';
            objProd.Is_Model__c = true;
            insert objProd;
            

        }
    }           
    public static testmethod void test() {
        
        test.starttest();

        Event objEve = SR_testdata.createEvent();           
        objEve.Subject = 'Classroom Training';
        objEve.StartDateTime = system.now();
        objEve.ActivityDateTime = objEve.StartDateTime;
        objEve.Booking_Type__c = 'Classroom Teaching';
        objEve.Location = objLoc.Name;
        objEve.Whatid = objProd.ID;   
        insert objEve; 
        
        Event evt = [select id,Event_Number__c from Event where id =: objEve.id];
        
        Event_Location__c objEL = new Event_Location__c();
        objEL.Location__c = objLoc.id;
        objEL.Event_Number__c = objEve.Event_Number__c;
        objEL.State_Province_loaded__c = 'NY';
        objEL.Country_loaded__c = 'USA';
        insert objEL;       
        
        Test.stoptest();
         
     }
}
/**************************************
@Author         :   Nikita Gupta
@Created Date   :   11/11/2014
**************************************/

@isTest
public class SR_Class_Event_Test
{
    
    
    //public static Id WOInstallationId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
    static Account objAcc;
    static Contact objCont = SR_testdata.createContact();
    static Case objCase = SR_testdata.createCase();
    static SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();
    static State_Province__c objStProv = new State_Province__c();
    static Country__c objCountry = new Country__c();
    static SVMXC__Site__c objLoc = new SVMXC__Site__c();
    static Product2 objProd = SR_testdata.createProduct();
    static EmailTemplate objET = new EmailTemplate(); 
        static
        {
            User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
            System.runAs (thisUser){
                String TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
                objAcc = new  Account(Recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(), Name = 'Test AccountSFQA', CurrencyIsoCode = 'USD', Country__c = 'India', OMNI_Postal_Code__c = '21234', AccountNumber = '2009845', State_Province_Is_Not_Applicable__c = true, BillingCity='San Francisco');
                insert objAcc;
                CountryDateFormat__c cdf = new CountryDateFormat__c(name='Default', Date_Format__c ='txt');
                insert cdf;
                objCont.FirstName = 'TestAprRel1FN';
                objCont.LastName = 'TestAprRel1';
                objCont.Email = 'Test@1234APR.com';
                objCont.AccountId = objAcc.Id; 
                objCont.MailingCity='Delhi';
                objCont.MailingCountry='India';
                objCont.MailingPostalCode='110051';
                objCont.MailingState='CA'; 
                objCont.Phone = '1235678';             
                insert objCont;
                objCase.Priority='Medium';
                objCase.Contactid=objCont.id;
                insert objCase;  
                Timecard_Profile__c timecardprof = new Timecard_Profile__c(
                First_Day_of_Week__c = 'Monday', 
                Name = 'Test TimeCard Profile');
                timecardprof.Normal_Start_Time__c = Date.today();
                timecardprof.Normal_End_Time__c = Date.today().addDays(1);
                timecardprof.No_of_weeks_in_advance_timecard_created__c = '15';
                timecardprof.No_of_weeks_in_arrears_timecard_created__c = '15';
                timecardprof.Payroll_View__c = true;
                timecardprof.Hide_Holiday_Cash_hours__c  = true;
                timecardprof.Stopping_Time_Card_Completion__c = 'Completed;Approved;In Progress;Cancelled;Open';
                timecardprof.Track_Dosimetric_Readings__c = false;
                timecardprof.First_Day_of_Week__c = 'Saturday';
                timecardprof.Daily_details_to_show__c = 'Equipment';
                insert timecardprof;
                Organizational_Activities__c orgactivities = new Organizational_Activities__c(
                Name = 'Allocations Prep / Reporting',    
                Type__c = 'Indirect', 
                Indirect_Hours__c = timecardprof.Id);
                insert orgactivities;
                // insert Service Group
                SVMXC__Service_Group__c objSvcGrp = new  SVMXC__Service_Group__c(Name = 'test', SVMXC__Group_Code__c = '4578');
                Insert objSvcGrp;
                SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, 
                                                                                       Name = 'Test Technician', 
                                                                                       SVMXC__Active__c = true, 
                                                                                       SVMXC__Enable_Scheduling__c = true,
                                                                                       SVMXC__Phone__c = '987654321', 
                                                                                       SVMXC__Email__c = 'abc@xyz.com', 
                                                                                       SVMXC__Service_Group__c = objSvcGrp.ID, 
                                                                                       Timecard_Profile__c = timecardprof.Id,
                                                                                       User__c = UserInfo.getUserId(),
                                                                                       ERP_Work_Center__c='H987654');
                insert tech;
                
                
                objProd.Material_Group__c = Label.Product_Material_group;
                objProd.Budget_Hrs__c = 4;
                objProd.UOM__c = 'HR';
                objProd.Product_Type__c = label.SR_Product_Type_Sellable;
                objProd.ProductCode = 'Test Material No 001';
                objProd.ERP_PCode_4__c = 'H012';
                objProd.Is_Model__c = true;
                insert objProd;
                
                objLoc.Name = 'Test Location';
                objLoc.SVMXC__Account__c = objAcc.ID;
                insert objLoc;
                
                objWO.SVMXC__Company__c = objAcc.ID;
                objWO.SVMXC__Contact__c = objCont.id;
                objWO.SVMXC__Case__c = objCase.ID;
                objWO.SVMXC__Order_Status__c = 'Open';
                
                objStProv.Timezone__c = 'America/New_York';
                objStProv.Name = 'NY';
                insert objStProv;
                
                objCountry.Name = 'USA';
                objCountry.Timezone__c = 'America/New_York';
                insert objCountry;
            }
            User usr = [SELECT id FROM User WHERE Id = :UserInfo.getUserId()]; 
            System.runAs (usr){               
                List<Folder> lstFld = [Select id from folder where name = 'Service Email Templates' limit 1];
                ID fldrId = lstFld[0].id;
                
                objET.developerName = 'test';
                objET.TemplateType= 'Text';
                objET.FolderId = fldrId;
                objET.isActive = true;
                objET.Name = 'SR_Installation WO Email to Technician'; 
                insert objET; 
            
            Event objEve = SR_testdata.createEvent();
            objEve.Subject = 'Classroom Training';
            objEve.StartDateTime = system.now();
            objEve.ActivityDateTime = objEve.StartDateTime;
            objEve.Status__c = 'Confirmed';
            objEve.Booking_Type__c = 'Classroom Teaching';
            objEve.Location = objLoc.Name;
            objEve.WhatId = ObjAcc.id;
            objEve.time_Entry__c = true;
            try{
            insert objEve;
            }catch(exception e){
            }    
            Event_Properties__c varEveProp = new Event_Properties__c();
            varEveProp.Duration__c = 40;
            varEveProp.Booking_Type__c = 'Classroom Teaching';
            varEveProp.Status__c = objEve.Status__c;
            insert varEveProp; 
            
           }
        }  
            
        public static testMethod void updateEventWorkOrder_test()
        {
            system.test.starttest();
            Event objEve = SR_testdata.createEvent();
           
            objEve.Subject = 'Classroom Training';
            objEve.StartDateTime = system.now();
            objEve.ActivityDateTime = objEve.StartDateTime;
            //objEve.DurationInMinutes = 60;
            objEve.Status__c = 'Confirmed';
            objEve.Booking_Type__c = 'Classroom Teaching';
            objEve.Location = objLoc.Name;
            objEve.Whatid = objProd.ID;
            objEve.time_Entry__c = true;                        
            try{
            insert objEve;
            }catch(exception e){}                                                  
            objWO.Event_Number__c = objEve.Event_Number__c;
            objWO.Event_ID__c = objEve.id;
            objWO.SVMXC__Order_Status__c = 'Cancelled';
            objwo.Cancellation_Reason__c = 'test';
            insert objWO;
            
            objEve.Status__c = 'Cancelled';
            try{
            update objEve; 
            }catch(exception e){}
            system.Test.stoptest();
        }
   
            
             
       /* 1C Yukti - 31 Mar 15
        public static testMethod void InstallationWorkOrderAssignmentEmail_test()
        {                     
                SR_Class_Event obj  = new SR_Class_Event();
                List<Event> lstEve = new List<Event>(); 
                
                Contact con = new Contact();
                con.FirstName = 'Test';
                con.LastName = 'Test';
                con.email = 'test@test.com';
                con.mailingcountry = 'India';
                insert con;
                
                objWO.RecordTypeID = WOInstallationId;
                objWO.SVMXC__Contact__c = con.id;
                insert objWO;
                
                Event objEve = SR_testdata.createEvent();
                objEve.Status__c = 'Cancelled';
                objEve.Booking_Type__c = 'Classroom Teaching';
                //objEve.Whatid = objWO.ID;
                objEve.OwnerID = userInfo.getUserID();
                insert objEve;

                lstEve.add(objEve);
                obj.InstallationWorkOrderAssignmentEmail(lstEve);
                
                delete objEve;
        } 1C Yukti - 31 Mar 15*/
}
/************************************************************************
    @ Author        : Priti Jaiswal
    @ Date      :     21-April-2014
    @ Description   : This class is being called from trigger "SR_Expertise_BeforeTrigger " to perform various functionalities
                      related to Expertise object..
    @ Last Modified By  :  Priti Jaiswal
    @ Last Modified On  :   06/05/2014
    @ Last Modified Reason  : US4292 - set the Current End Date on the Technician/Equipment record with Expertise's Availability End Date. 
    @ Last Modified By  :  Ritika
    @ Last Modified On  :   06/08/2014
    @ Last Modified Reason  : US4509 - creating case workorders from expertise if out of tolerance is check 
    
*********************************************/
public class SR_Class_Expertise
{
    public void updateFieldsOnExpertise(List<SVMXC__Service_Group_Skills__c> lstOfExpertise, Map<ID,SVMXC__Service_Group_Skills__c> oldmapOfExpertise)
    {
        //priti US4341, to update Distric Manger and Regional Manager from Terriotory at expertise leevel
        Set<Id> setOfServiceTeamIds = new Set<Id>();                                                                    //set of service team Id
        Map<ID, SVMXC__Service_Group__c> mapOfServcTeamIdAndServiceTeamRecord = new Map<Id, SVMXC__Service_Group__c>();
        Set<Id> setOfOwnerIds = new Set<Id>();                                                                          // set of owner ids
        Set<Id> setOfRMAndFOAsIds = new Set<Id>();                                                                      // set of RMA & FOA ids
        Map<Id, Id> mapOfownerIdAndRMId = new Map<Id, Id>();                                                            //map of owner & RMA id
        Map<Id, Id> mapOfownerIdAndFOAId = new Map<Id,Id>();                                                            //map of owner & FOA ids
        Map<Id, String> mapOfRMFOAIdAndEmail = new Map<id, String>();                                                   // map of RMA, FOA id and email
        
        for(SVMXC__Service_Group_Skills__c exp : lstOfExpertise)
        {
            if(exp.SVMXC__Service_Group__c != null && (oldmapOfExpertise == null || exp.SVMXC__Service_Group__c != oldmapOfExpertise.get(exp.id).SVMXC__Service_Group__c || oldmapOfExpertise.get(exp.id).SVMXC__Service_Group__c == null))
            {
                setOfServiceTeamIds.add(exp.SVMXC__Service_Group__c);
            }
        }

        /********************* QUERY ON SERVICE TEAM **************************/
        if(setOfServiceTeamIds.size() > 0)
        {
            mapOfServcTeamIdAndServiceTeamRecord = new Map<Id, SVMXC__Service_Group__c>([select OwnerId, Service_Team__r.FOA__r.email,
                                                                                            Service_Team__r.District_Manager__c, 
                                                                                            Service_Team__r.District_Manager__r.ManagerId, Service_Team__r.District_Manager__r.DelegatedApproverID, 
                                                                                            Service_Team__r.District_Manager__r.Manager.Email //, Service_Team__r.District_Manager__r.DelegatedApprover.Email 
                                                                                         from SVMXC__Service_Group__c where id in : setOfServiceTeamIds]);

            if(mapOfServcTeamIdAndServiceTeamRecord.size() > 0)
            {
                for(SVMXC__Service_Group__c servcTeam : mapOfServcTeamIdAndServiceTeamRecord.values())
                {
                    setOfOwnerIds.add(servcTeam.Service_Team__r.District_Manager__c);
                    if(servcTeam.Service_Team__r.District_Manager__r.ManagerId != null)
                    {
                        //setOfRMAndFOAsIds.add(servcTeam.Service_Team__r.District_Manager__r.ManagerId);
                        mapOfownerIdAndRMId.put(servcTeam.Service_Team__r.District_Manager__c, servcTeam.Service_Team__r.District_Manager__r.ManagerId);
                        
                        mapOfRMFOAIdAndEmail.put(servcTeam.Service_Team__r.District_Manager__r.ManagerId, servcTeam.Service_Team__r.District_Manager__r.Manager.Email );
                    }
                    if(servcTeam.Service_Team__r.District_Manager__r.DelegatedApproverID != null)
                    {
                        //setOfRMAndFOAsIds.add(servcTeam.Service_Team__r.District_Manager__r.DelegatedApproverID);
                        mapOfownerIdAndFOAId.put(servcTeam.Service_Team__r.District_Manager__c, servcTeam.Service_Team__r.District_Manager__r.DelegatedApproverID);
                        
                        //mapOfRMFOAIdAndEmail.put(servcTeam.Service_Team__r.District_Manager__r.DelegatedApproverID, servcTeam.Service_Team__r.District_Manager__r.DelegatedApprover.email);
                    }
                }
            }
        }

        //below code commented for code optimization and moved to the query on service team above.
        /* DE7370:  integrated above
        if(setOfOwnerIds.size() > 0)
        {
            //**************************** QUERY ON USER *****************************************
            for(User usr : [select id, ManagerId, DelegatedApproverID from User where id in :setOfOwnerIds])
            {
                if(usr.ManagerId != null)
                {
                    setOfRMAndFOAsIds.add(usr.ManagerId);
                    mapOfownerIdAndRMId.put(usr.id, usr.ManagerId);
                }
                if(usr.DelegatedApproverID != null)
                {
                    setOfRMAndFOAsIds.add(usr.DelegatedApproverID);
                    mapOfownerIdAndFOAId.put(usr.id, usr.DelegatedApproverID);
                }
            }
            //**************************** QUERY ON USER *****************************************
            if(setOfRMAndFOAsIds.size() > 0)
            {
                for(User usr : [select id, email from User where id in :setOfRMAndFOAsIds])
                {
                    if(usr.id != null && usr.email != null)
                    {
                        mapOfRMFOAIdAndEmail.put(usr.id, usr.email);
                    }
                }
            }
        }*/

        //if(mapOfRMFOAIdAndEmail.size() > 0)
        {
            for(SVMXC__Service_Group_Skills__c exp : lstOfExpertise)
            {
                //updating FOA Email and Regional Manager Email at expertise level from Serviec Team's owner.Dalegated approver.Email and Serviec Team's owner.Manager.Email
                if(exp.SVMXC__Service_Group__c != null && mapOfServcTeamIdAndServiceTeamRecord.containsKey(exp.SVMXC__Service_Group__c) )
                {
                    exp.District_Manager__c = mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).Service_team__r.District_manager__c;

                    exp.FOA_Email__c = mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).Service_team__r.Foa__r.email;
                    
                    if(mapOfownerIdAndRMId.containsKey((mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).Service_team__r.District_manager__c)) && mapOfownerIdAndRMId.get((mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).Service_team__r.District_manager__c)) != null && mapOfRMFOAIdAndEmail.containsKey(mapOfownerIdAndRMId.get((mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).Service_team__r.District_manager__c))))
                    {
                        exp.Regional_Manager_Email__c = mapOfRMFOAIdAndEmail.get(mapOfownerIdAndRMId.get((mapOfServcTeamIdAndServiceTeamRecord.get(exp.SVMXC__Service_Group__c).OwnerId)));
                    }
                }
            }
        }
    }
    //Consolidated method for createCaseWorkOrder (US4509) and updateTechnicianfromExpertise(US4292)
    public void createCaseWOupdateTechnician(List<SVMXC__Service_Group_Skills__c> lstOfExpertise, Map<Id,SVMXC__Service_Group_Skills__c> oldValueMap)
    {
        set<string> setTechnicianEquipmentID = new set<string>(); //set of Technician/Equipment Ids
        set<string> setExpId = new set<string>();  // set of string expertise id
        map<ID,SVMXC__Service_Group_Members__c> mapUpdateEquipment = new map<ID, SVMXC__Service_Group_Members__c>();    //DE3045
        map<string,list<SVMXC__Service_Order_Line__c>> mapTechandlistWD = new map<string,list<SVMXC__Service_Order_Line__c>>();//Contain equipment id and list of work details
        map<string,SVMXC__Service_Group_Skills__c> mapCurrentEXPEquip = new map<string,SVMXC__Service_Group_Skills__c>(); // map of current expertise equipment
        map<id, SVMXC__Service_Group_Members__c> mapPrevExpertiseEquipment = new  map<id, SVMXC__Service_Group_Members__c>();  //map of previous expertise equipment
        Map<Id,Date> mapOfTechIdAndEndDate = new Map<Id, Date>(); // map of technician id & end date
        Map<ID, SVMXC__Service_Group_Members__c> mapOfTechToupdate = new Map<ID, SVMXC__Service_Group_Members__c>(); // list of technicians to update
        list<Case_Work_Order__c> lstInsertCaseWO = new list<Case_Work_Order__c>(); // list of case work order to insert
        List<Case> lstInsertCase = new List<Case>(); // list of case to insert
        map<String,SVMXC__Service_Order_Line__c> wdlMap = new map<String,SVMXC__Service_Order_Line__c>();
       
        // custom labels added
        FINAL String LineTypeMisc = system.label.LineTypeMisc;
        FINAL String RecTypeNameEquip = system.label.RecTypeNameEquip;
        FINAL String AccNameVMS = system.label.Varian_Medical_Systems;
        FINAL String AccTypeInternal = system.label.AccTypeInternal;
        FINAL String CaseRecTypeCA = system.label.Case_Customer_Advocacy_Record_Type;
        FINAL String CaseOrigin = system.label.SR_Case_Origin;
        FINAL String CaseReasonNSubject = system.label.SR_Case_ReasonORSubject;

        ID CARecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CaseRecTypeCA).getRecordTypeId();

        Account acc = new Account(); // account creation
        Country__c cntry = [select id from  Country__c where Name = 'USA' limit 1]; // country creation      
        
        if(cntry != null)
        {
            list<Account> listAccnt = [SELECT id From Account where Name = :AccNameVMS and Account_Type__c = :AccTypeInternal 
                                        and BillingCountry = 'USA' and Country1__c = :cntry.id limit 1]; // list of accounts
            
            if(listAccnt != null && listAccnt.size() > 0)
            {
                acc = listAccnt[0];
            }
        }  
        
        for(SVMXC__Service_Group_Skills__c objExp : lstOfExpertise)
        {
            if(objExp.SVMXC__Availability_End_Date__c != null && objExp.SVMXC__Group_Member__c != null && (oldValueMap == null || objExp.SVMXC__Availability_End_Date__c != oldValueMap.get(objExp.id).SVMXC__Availability_End_Date__c || oldValueMap.get(objExp.id).SVMXC__Availability_End_Date__c == null))
            {
                mapOfTechIdAndEndDate.put(objExp.SVMXC__Group_Member__c, objExp.SVMXC__Availability_End_Date__c);
            }
            if(objExp.Out_of_Tolerance__c == true && objExp.SVMXC__Group_Member__c != null && (oldValueMap == null || oldValueMap.get(objExp.id).Out_of_Tolerance__c == false))
            {
                setExpId.add(objExp.id);
                setTechnicianEquipmentID.add(objExp.SVMXC__Group_Member__c);
                mapCurrentEXPEquip.put(objExp.SVMXC__Group_Member__c, objExp);
            }
        }
        if(mapOfTechIdAndEndDate.size() > 0 || setTechnicianEquipmentID.size() > 0)
        {          
            list<SVMXC__Service_Order_Line__c> listWorkDetail = [select id, SVMXC__Group_Member__c, SVMXC__Service_Order__c, SVMXC__Start_Date_and_Time__c,
                                                SVMXC__Service_Order__r.CreatedDate, SVMXC__Service_Order__r.SVMXC__Closed_On__c, SVMXC__End_Date_and_Time__c,
                                                SVMXC__Group_Member__r.RecordType.Name, SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c,
                                                SVMXC__Service_Order__r.Service_Comment__c, SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c,
                                                SVMXC__Service_Order__r.SVMXC__Group_Member__r.User__c from SVMXC__Service_Order_Line__c where
                                                SVMXC__Group_Member__c in : setTechnicianEquipmentID and SVMXC__Line_Type__c =: LineTypeMisc
                                                and SVMXC__Group_Member__r.RecordType.Name =: RecTypeNameEquip];
            
            for(SVMXC__Service_Order_Line__c wdl : listWorkDetail){
                String key = wdl.SVMXC__Group_Member__c + '' + wdl.SVMXC__Service_Order__c;
                if(!wdlMap.containsKey(key)){
                    wdlMap.put(key,wdl);
                }
            }
            
            system.debug('wdlist size = '+listWorkDetail.size());
            
           mapPrevExpertiseEquipment = new  map<id, SVMXC__Service_Group_Members__c>([select id, Current_End_Date__c, OOC__c, SVMXC__Service_Group__r.Service_Team__r.District_Manager__c,
                                         SVMXC__Service_Group__r.Service_Team__r.FOA__r.Email,
                                         SVMXC__Service_Group__r.Service_Team__r.SC__r.Email,
                                        (Select id, Certificate_Email__c, SVMXC__Availability_Start_Date__c, SVMXC__Availability_End_Date__c, OOT_Measured_After__c,
                                         OOT_Measured_Before__c, OOT_Nominal__c, OOT_Parameter__c, OOT_Tolerance__c 
                                         from SVMXC__Service_Group_Skills__r
                                         where id NOT in :setExpId order by SVMXC__Availability_End_Date__c desc limit 1) //DE7471:  exclude the current ones to find the previous ones and sort by start date since based on start dates 
                                         from SVMXC__Service_Group_Members__c 
                                         where Id in : setTechnicianEquipmentID OR id in :mapOfTechIdAndEndDate.keySet() ]);  
            
            if(mapOfTechIdAndEndDate.size() > 0 && mapPrevExpertiseEquipment.size() > 0)    
            {
                for(SVMXC__Service_Group_Members__c tech : mapPrevExpertiseEquipment.values())
                {
                    //CM : Jan 25th : STRY0015429 : STSK0011173
                    SVMXC__Service_Group_Skills__c exp = tech.SVMXC__Service_Group_Skills__r;
                    if(mapOfTechIdAndEndDate.containsKey(tech.id)) // check added
                    {
                        //CM : Jan 25th : STRY0015429 : STSK0011173
                        if (exp.SVMXC__Availability_End_Date__c >= mapOfTechIdAndEndDate.get(tech.id))
                        {
                            tech.Current_End_Date__c = exp.SVMXC__Availability_End_Date__c;
                        }
                        else
                        {
                            tech.Current_End_Date__c = mapOfTechIdAndEndDate.get(tech.id);
                        }
                        
                         /* DE7330 If-Else Code Block :US5154 Code Update as par Technical Notes 16-Jun-2015
                        {
                            tech.Calibration_Status__c = 'Calibrated';
                        }*/
                        if(tech.Current_End_Date__c ==null)
                        {  
                            tech.Calibration_Status__c = 'Not Required';
                            tech.OOC__c = false; 
                        }
                        if(tech.Current_End_Date__c >= System.Today())
                        {
                          tech.OOC__c=false;
                          tech.Calibration_Status__c    ='Calibrated';
                        }
                        else {
                              tech.OOC__c=true;
                              tech.Calibration_Status__c    ='Out of Calibration';
                        }
                        mapOfTechToupdate.put(tech.id, tech);
                    }
                }
            }   

            // populating map of technician and work detail
            if(wdlMap != null && wdlMap.size() > 0)
            {
                for (SVMXC__Service_Order_Line__c objWD : wdlMap.values())
                {
                    if(objWD.SVMXC__Group_Member__c != null)
                    {
                        if(mapPrevExpertiseEquipment.containsKey(objWD.SVMXC__Group_Member__c) && mapPrevExpertiseEquipment.get(objWD.SVMXC__Group_Member__c).SVMXC__Service_Group_Skills__r != null && mapPrevExpertiseEquipment.get(objWD.SVMXC__Group_Member__c).SVMXC__Service_Group_Skills__r.size() > 0)
                        {
                            SVMXC__Service_Group_Skills__c oldExp = mapPrevExpertiseEquipment.get(objWD.SVMXC__Group_Member__c).SVMXC__Service_Group_Skills__r[0];
                            SVMXC__Service_Group_Skills__c objExp = mapCurrentEXPEquip.get(objWD.SVMXC__Group_Member__c); //DE7309
                           
                            if(objWD.SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c != null &&
                                         (oldExp.SVMXC__Availability_Start_Date__c <= objWD.SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c && objWD.SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c < objExp.SVMXC__Availability_Start_Date__c)  ||               
                              (objWD.SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c != null && oldExp.SVMXC__Availability_Start_Date__c <= objWD.SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c &&  objWD.SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c< objExp.SVMXC__Availability_Start_Date__c) ||
                              (objWD.SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c <= oldExp.SVMXC__Availability_Start_Date__c && (objWD.SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c == null ||(objWD.SVMXC__Service_Order__r.SVMXC__Actual_Restoration__c > objExp.SVMXC__Availability_Start_Date__c)))
                               ) { //DE7400              
                                List<SVMXC__Service_Order_Line__c> lstTempWD = new list<SVMXC__Service_Order_Line__c>(); // list of temporary work detail

                                if(!mapTechandlistWD.containsKey(objWD.SVMXC__Group_Member__c))
                                {
                                    mapTechandlistWD.Put(objWD.SVMXC__Group_Member__c, new list<SVMXC__Service_Order_Line__c>{objWD});
                                }
                                else
                                {
                                    lstTempWD = mapTechandlistWD.get(objWD.SVMXC__Group_Member__c);
                                    lstTempWD.add(objWD);
                                    mapTechandlistWD.put(objWD.SVMXC__Group_Member__c,lstTempWD);                   
                                }
                            }
                        }
                        
                        
                        /*
                        if(mapCurrentEXPEquip.containsKey(objWD.SVMXC__Group_Member__c))
                        {
                            if(objWD.SVMXC__Service_Order__r.SVMXC__Actual_Onsite_Response__c <= mapCurrentEXPEquip.get(objWD.SVMXC__Group_Member__c).SVMXC__Availability_Start_Date__c)
                            {
                                List<SVMXC__Service_Order_Line__c> lstTempWD = new list<SVMXC__Service_Order_Line__c>(); // list of temporary work detail
                                if(!mapTechandlistWD.containsKey(objWD.SVMXC__Group_Member__c))
                                {
                                    mapTechandlistWD.put(objWD.SVMXC__Group_Member__c, new list<SVMXC__Service_Order_Line__c>{objWD});
                                }
                                else
                                {
                                    lstTempWD = mapTechandlistWD.get(objWD.SVMXC__Group_Member__c);
                                    lstTempWD.add(objWD);
                                    mapTechandlistWD.put(objWD.SVMXC__Group_Member__c,lstTempWD);                   
                                }
                            }
                        }
                        
                        */
                        
                        
                        //DE3045
                        if(mapPrevExpertiseEquipment.containsKey(objWD.SVMXC__Group_Member__c))
                        {
                            SVMXC__Service_Group_Members__c varTech = mapPrevExpertiseEquipment.get(objWD.SVMXC__Group_Member__c);
                            
                            if(varTech.Current_End_Date__c != null && varTech.Current_End_Date__c >= System.today())
                            {
                                varTech.OOC__c = false;

                                mapUpdateEquipment.put(varTech.id, varTech);
                            }
                        }
                    }
                }
            }
             
            system.debug('mapTechandlistWD size = '+mapTechandlistWD.size()); 
            
        /*    Account acc = new Account(); // account creation
        //Country__c cntry = [select id from  Country__c where Name = 'USA' limit 1]; // country creation  //DE7370: consolidate queries    

        if (setExpId.size()>0)
        {
            list<Account> listAccnt = [SELECT id From Account where Name = :AccNameVMS and Account_Type__c = :AccTypeInternal 
                                        and BillingCountry = 'USA' and Country1__r.Name = 'USA' limit 1]; // list of accounts

            if(listAccnt != null && listAccnt.size() > 0)
            {
                acc = listAccnt[0];
            }
        }  */
             
            for(SVMXC__Service_Group_Skills__c objExp : lstOfExpertise)
            {
                // creating Case records
                if(objExp.Out_of_Tolerance__c == true && (oldValueMap == null || (oldValueMap != null && oldValueMap.get(objExp.id).Out_of_Tolerance__c == false))) 
                {
                    Case objCase = new Case();//create new case
                    objCase.Test_Equipment__c = objExp.SVMXC__Group_Member__c;
                    objCase.Origin = CaseOrigin;
                    objCase.Reason = CaseReasonNSubject;
                    objCase.Subject = CaseReasonNSubject;   
                    objCase.Local_Subject__c = CaseReasonNSubject;            
                    objCase.RecordTypeId = CARecordType;
                    objCase.Accountid = acc.id;
                    
                    if(objExp.SVMXC__Group_Member__c != null && mapPrevExpertiseEquipment.containsKey(objExp.SVMXC__Group_Member__c) && mapPrevExpertiseEquipment.get(objExp.SVMXC__Group_Member__c).SVMXC__Service_Group_Skills__r != null && mapPrevExpertiseEquipment.get(objExp.SVMXC__Group_Member__c).SVMXC__Service_Group_Skills__r.size() > 0)
                    {
                        SVMXC__Service_Group_Members__c serviceGroupMember = mapPrevExpertiseEquipment.get(objExp.SVMXC__Group_Member__c);
                        SVMXC__Service_Group_Skills__c objSGS = objExp; //serviceGroupMember.SVMXC__Service_Group_Skills__r[0];

                        //DE5591
                        if(serviceGroupMember.SVMXC__Service_Group_Skills__r.size() > 1){
                            SVMXC__Service_Group_Skills__c prevobjSGS = serviceGroupMember.SVMXC__Service_Group_Skills__r[0];
                            objCase.OOT_Certification_Number__c = prevobjSGS.Certificate_Email__c ; 
                        }

                        

                        System.debug('experties record = '+ objSGS);
                        
                        if(objSGS != null)
                        {
                            objCase.Expertise__c = objSGS.id;
                            if(serviceGroupMember.SVMXC__Service_Group__r.Service_Team__r.District_Manager__c != null){
                                objCase.OwnerId = serviceGroupMember.SVMXC__Service_Group__r.Service_Team__r.District_Manager__c;
                            }
                            objCase.OOT_Measured_After__c = objSGS.OOT_Measured_After__c;                                                                                         
                            objCase.OOT_Measured_Before__c = objSGS.OOT_Measured_Before__c;                                                                                      
                            objCase.OOT_Nominal__c = objSGS.OOT_Nominal__c;
                            objCase.OOT_Parameter__c = objSGS.OOT_Parameter__c;
                            objCase.OOT_Tolerance__c = objSGS.OOT_Tolerance__c;
                            objCase.FOA_Email__c = serviceGroupMember.SVMXC__Service_Group__r.Service_Team__r.FOA__r.Email;
                            objCase.SC_Email__c = serviceGroupMember.SVMXC__Service_Group__r.Service_Team__r.SC__r.Email;
                        }              
                        objCase.Test_Equipment__c = mapPrevExpertiseEquipment.get(objExp.SVMXC__Group_Member__c).id;
                    }
                    else    
                    {
                        objExp.addError('Previous Availability not found for this Test Equipment');
                    }
                    lstInsertCase.add(objCase);          
                   // creating case work order 
                    if(objExp.SVMXC__Group_Member__c != null && mapTechandlistWD.containsKey(objExp.SVMXC__Group_Member__c) && mapTechandlistWD.get(objExp.SVMXC__Group_Member__c).size() > 0)
                    {
                        for(SVMXC__Service_Order_Line__c objWrkDet : mapTechandlistWD.get(objExp.SVMXC__Group_Member__c))
                        {
                                        system.debug('objWrkDet inside loop');
                                        Case_Work_Order__c objCaseWO = new Case_Work_Order__c();
                                        objCaseWO.Work_Order__c = objWrkDet.SVMXC__Service_Order__c;
                                        objCaseWO.Closed_Date__c = Date.valueOf(objWrkDet.SVMXC__Service_Order__r.SVMXC__Closed_On__c);
                                        objCaseWO.Case__r = objCase ;
                                        objCaseWO.Test_Equipment__c = objExp.SVMXC__Group_Member__c;
                                        objCaseWO.Service_Comments__c = objWrkDet.SVMXC__Service_Order__r.Service_Comment__c;
                                        //objCaseWO.OwnerID = objWrkDet.SVMXC__Service_Order__r.SVMXC__Group_Member__r.User__c;
                                        lstInsertCaseWO.add(objCaseWO);
                        }                       
                    }
                }
            }
            // updating map of technician
            if(mapOfTechToupdate.size() > 0)
            {
                update mapOfTechToupdate.values();   
            }
            if(mapUpdateEquipment.size() > 0)
            {
                update mapUpdateEquipment.values();
            }
            
            // inserting case & case work order 
            if(lstInsertCase!=null && lstInsertCase.size()>0)
            {
                try
                {
                    insert lstInsertCase;
                    if(lstInsertCaseWO!=null && lstInsertCaseWO.size()>0)
                    {
                        for(Case_Work_Order__c caseWO :lstInsertCaseWO)
                        {
                            if(caseWO.Case__r != null)
                            {
                                caseWO.Case__c = caseWO.Case__r.id;
                            }
                        }                                                                       
                        insert  lstInsertCaseWO;
                    }
                    
                    list<Case> casesToUpdate = new list<Case>();
                    
                    list<Case> caseWithoutCWOList = [select id from case where Number_of_OOT_Case_Work_Orders__c = 0 and Id in : lstInsertCase ];
                    for(Case cs : caseWithoutCWOList){
                        cs.Status = 'Closed'; 
                        cs.Closed_Case_Reason__c = 'Resolved No Further Action';
                        casesToUpdate.add(cs);
                    }
                    
                    list<Case> caseWithCWOList = [select id from case where Number_of_OOT_Case_Work_Orders__c > 0 and Id in : lstInsertCase ];
                    for(Case cs : caseWithCWOList ){
                        cs.OOT_Is_Contains_CWO__c = true; 
                        casesToUpdate.add(cs);
                    }
                    
                    if(!casesToUpdate .isEmpty()){
                        update casesToUpdate ;
                    }
                    
                }
                catch(Exception e)
                {                   
                    for(SVMXC__Service_Group_Skills__c exp : lstOfExpertise)
                    {
                        exp.addError(e.getMessage()); //REFACTOR: Incorrectly bulkified error reporting
                    }
                }
            }
        }
    }
}
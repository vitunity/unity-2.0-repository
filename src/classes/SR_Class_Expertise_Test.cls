@isTest(seeAllData=true)
public class SR_Class_Expertise_Test
{
    public static SVMXC__Service_Group_Members__c tech;
    public static SVMXC__Service_Group_Members__c tech1;
    public static SVMXC__Service_Group_Members__c tech2;
    public static User serviceUser;
    public static SVMXC__Service_Group__c ServiceTeam;
    static
    {
            Test.startTest();
            Profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
            User serviceUser1 = new User(FirstName ='Test3', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                    alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                    LocaleSidKey = 'en_US',UserName='test3@service.com');
            insert serviceUser1;

            User serviceUser2 = new User(FirstName ='Test2', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                    alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                    LocaleSidKey = 'en_US',UserName='test2@service.com');
            insert serviceUser2;

            serviceUser = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                                    alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                                    LocaleSidKey = 'en_US',UserName='test1@service.com', ManagerId = serviceUser1.Id, DelegatedApproverID = serviceUser2.Id);
            insert serviceUser;

            String RecTypeNameEquip = system.label.RecTypeNameEquip;
            Recordtype rec = [Select id, Name from recordtype where sobjecttype = 'SVMXC__Service_Group_Members__c' and Name =: RecTypeNameEquip];

            SVMXC__Service_Group__c ServiceTeam1 = new SVMXC__Service_Group__c();
            ServiceTeam1.SVMXC__Active__c= true;
            ServiceTeam1.ERP_Reference__c='TestExternal';
            ServiceTeam1.Name = 'TestService';
            ServiceTeam1.District_Manager__c = serviceUser.Id;
            Insert ServiceTeam1;

            ServiceTeam = new SVMXC__Service_Group__c();
            ServiceTeam.SVMXC__Active__c= true;
            ServiceTeam.Service_Team__c = ServiceTeam1.Id;
            ServiceTeam.ERP_Reference__c='TestExternal';
            ServiceTeam.Name = 'TestService';
            ServiceTeam.District_Manager__c = serviceUser.Id;
            Insert ServiceTeam;

            tech = new SVMXC__Service_Group_Members__c();
            tech.SVMXC__Service_Group__c = ServiceTeam.id;
            tech.User__c = serviceUser.id ;
            tech.ERP_Reference__c = 'TextExternal';
            tech.Name = 'TestTechnician';
            tech.SVMXC__Average_Drive_Time__c = 2.00;
            tech.SVMXC__Average_Speed__c = 2.00;
            tech.SVMXC__Break_Duration__c = 2.00;
            tech.SVMXC__Break_Type__c = 'Fixed';
            tech.SVMXC__City__c = 'Noida';
            tech.SVMXC__Country__c = 'India'; 
            tech.Current_End_Date__c = system.today();
            tech.Dispatch_Queue__c = 'DISP - AMS';
            //tech.Docusign_Recipient__c = 'DISP - AMS';
            tech.SVMXC__Email__c = 'standarduser@testorg.com';
            //SVMXC__Inventory_Location__c
            tech.SVMXC__State__c = 'UP';
            tech.SVMXC__Street__c = 'abc';
            tech.recordtypeid = rec.id ;   
            tech.SVMXC__Zip__c = '201301';
            tech.SVMXC__Salesforce_User__c = serviceUser.id;    
            tech.SVMXC__Select__c = false;

            Insert tech;

            tech1 = new SVMXC__Service_Group_Members__c();
            tech1.SVMXC__Service_Group__c = ServiceTeam.id;
            tech1.User__c = serviceUser.id ;
            tech1.ERP_Reference__c = 'TextExternal';
            tech1.Name = 'TestTechnician';
            tech1.SVMXC__Average_Drive_Time__c = 2.00;
            tech1.SVMXC__Average_Speed__c = 2.00;
            tech1.SVMXC__Break_Duration__c = 2.00;
            tech1.SVMXC__Break_Type__c = 'Fixed';
            tech1.SVMXC__City__c = 'Noida';
            tech1.SVMXC__Country__c = 'India'; 
            tech1.Current_End_Date__c = system.today() - 3;
            tech1.Dispatch_Queue__c = 'DISP - AMS';
            //tech.Docusign_Recipient__c = 'DISP - AMS';
            tech1.SVMXC__Email__c = 'standarduser@testorg.com';
            //SVMXC__Inventory_Location__c
            tech1.SVMXC__State__c = 'UP';
            tech1.SVMXC__Street__c = 'abc';
            tech1.recordtypeid = rec.id ;   
            tech1.SVMXC__Zip__c = '201301';
            tech1.SVMXC__Salesforce_User__c = serviceUser.id;    
            tech1.SVMXC__Select__c = false;
            tech1.Current_End_Date__c = null;
            Insert tech1;

            tech2 = new SVMXC__Service_Group_Members__c();
            tech2.SVMXC__Service_Group__c = ServiceTeam.id;
            tech2.User__c = serviceUser.id ;
            tech2.ERP_Reference__c = 'TextExternal';
            tech2.Name = 'TestTechnician';
            tech2.SVMXC__Average_Drive_Time__c = 2.00;
            tech2.SVMXC__Average_Speed__c = 2.00;
            tech2.SVMXC__Break_Duration__c = 2.00;
            tech2.SVMXC__Break_Type__c = 'Fixed';
            tech2.SVMXC__City__c = 'Noida';
            tech2.SVMXC__Country__c = 'India'; 
            tech2.Dispatch_Queue__c = 'DISP - AMS';
            //tech.Docusign_Recipient__c = 'DISP - AMS';
            tech2.SVMXC__Email__c = 'standarduser@testorg.com';
            //SVMXC__Inventory_Location__c
            tech2.SVMXC__State__c = 'UP';
            tech2.SVMXC__Street__c = 'abc';
            tech2.recordtypeid = rec.id ;   
            tech2.SVMXC__Zip__c = '201301';
            tech2.SVMXC__Salesforce_User__c = serviceUser.id;    
            tech2.SVMXC__Select__c = false;

            Insert tech2;
            Test.stopTest();
    }

   static testMethod void SR_Class_ExpertiseTest()
    {

            /*
            SVMXC__Service_Group__c ServiceTeam1 = new SVMXC__Service_Group__c();
            ServiceTeam1.SVMXC__Active__c= true;
            ServiceTeam1.ERP_Reference__c='TestExternal';
            ServiceTeam1.Name = 'TestService';
            ServiceTeam1.Service_Team__c = ServiceTeam.Id;
            Insert ServiceTeam1;
            */
            SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
            workOrdObject.Event__c = True;
            workOrdObject.ownerID = serviceUser.id;
            workOrdObject.SVMXC__Order_Status__c = 'open';
            workOrdObject.SVMXC__Preferred_Technician__c = tech.id;
            workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
            workOrdObject.SVMXC__Actual_Onsite_Response__c =  system.today()-10;
            // workOrdObject.SVMXC__Top_Level__c = testcase.SVMXC__Top_Level__c;
            //workOrdObject.SVMXC__Company__c = testcase.Accountid;
            //workOrdObject.SVMXC__Case__c = testcase.id;
            //workOrdObject.recordtypeid = rec.id;
            //workOrdObject.SVMXC__Product__c = testcase.Productid;
            Insert workOrdObject;
            String LineTypeMisc = system.label.LineTypeMisc;
            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
            wd.SVMXC__Group_Member__c = tech.id;
            wd.SVMXC__Line_Type__c = LineTypeMisc;
            wd.SVMXC__Service_Order__c = workOrdObject.id; 
            insert wd;
            system.debug('------------------->'+wd.SVMXC__Group_Member__c+'tech.RecordType.Name'+tech.RecordType.Name);
            String AccTypeInternal = system.label.AccTypeInternal;
            String AccNameVMS = system.label.Varian_Medical_Systems;
            Country__c cntry = [select id from  Country__c where Name = 'USA' limit 1];
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
            Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = AccNameVMS, Account_Type__c = AccTypeInternal, CurrencyIsoCode='USD', Country1__c = cntry.id, Country__c = 'USA', BillingCountry = 'USA',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
            //insert accObj;

            SVMXC__Skill__c skill = new SVMXC__Skill__c();
            insert skill;

            Schema.DescribeSObjectResult sr = Schema.SObjectType.SVMXC__Service_Group_Skills__c; 
            Map<String,Schema.RecordTypeInfo> skillMapByName = sr.getRecordTypeInfosByName();
            SVMXC__Service_Group_Skills__c expertise1 = new SVMXC__Service_Group_Skills__c();
            expertise1.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise1.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise1.SVMXC__Group_Member__c = tech.id;
            expertise1.Out_of_Tolerance__c = false;
            expertise1.Acknowledge_Date__c = system.today()-1;
            expertise1.SVMXC__Availability_Start_Date__c = system.today()-1;
            expertise1.SVMXC__Availability_End_Date__c = system.today();
            expertise1.Acknowledge_Date__c = system.today() + 3;
            //expertise1.Vendor__c = accObj.Id;
            expertise1.Certificate_Email__c = '123';
            insert expertise1;

            SVMXC__Service_Group_Skills__c expertise2 = new SVMXC__Service_Group_Skills__c();
            expertise2.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise2.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise2.SVMXC__Group_Member__c = tech2.id;
            expertise2.Out_of_Tolerance__c = false;
            expertise2.Acknowledge_Date__c = system.today()-1;
            expertise2.SVMXC__Availability_Start_Date__c = system.today()-6;
            expertise2.SVMXC__Availability_End_Date__c = system.today() - 3;
            expertise2.Acknowledge_Date__c = system.today() + 3;
            //expertise2.Vendor__c = accObj.Id;
            expertise2.Certificate_Email__c = '123';
            insert expertise2;

            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.Out_of_Tolerance__c = false;
            expertise.SVMXC__Group_Member__c = tech.id;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-1;
            insert expertise;

            expertise1.Out_of_Tolerance__c = true;
            update expertise1;
    }
    static testMethod void SR_Class_ExpertiseTest1()
    {

            /*
            SVMXC__Service_Group__c ServiceTeam1 = new SVMXC__Service_Group__c();
            ServiceTeam1.SVMXC__Active__c= true;
            ServiceTeam1.ERP_Reference__c='TestExternal';
            ServiceTeam1.Name = 'TestService';
            ServiceTeam1.Service_Team__c = ServiceTeam.Id;
            Insert ServiceTeam1;
            */

            SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
            workOrdObject.Event__c = True;
            workOrdObject.ownerID = serviceUser.id;
            workOrdObject.SVMXC__Order_Status__c = 'open';
            workOrdObject.SVMXC__Preferred_Technician__c = tech1.id;
            workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
            workOrdObject.SVMXC__Actual_Onsite_Response__c =  system.today()-10;
            // workOrdObject.SVMXC__Top_Level__c = testcase.SVMXC__Top_Level__c;
            //workOrdObject.SVMXC__Company__c = testcase.Accountid;
            //workOrdObject.SVMXC__Case__c = testcase.id;
            //workOrdObject.recordtypeid = rec.id;
            //workOrdObject.SVMXC__Product__c = testcase.Productid;
            Insert workOrdObject;
            String LineTypeMisc = system.label.LineTypeMisc;
            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
            wd.SVMXC__Group_Member__c = tech1.id;
            wd.SVMXC__Line_Type__c = LineTypeMisc;
            wd.SVMXC__Service_Order__c = workOrdObject.id; 
            insert wd;
            system.debug('------------------->'+wd.SVMXC__Group_Member__c+'tech.RecordType.Name'+tech.RecordType.Name);
            String AccTypeInternal = system.label.AccTypeInternal;
            String AccNameVMS = system.label.Varian_Medical_Systems;
            Country__c cntry = [select id from  Country__c where Name = 'USA' limit 1];
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
            Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = AccNameVMS, Account_Type__c = AccTypeInternal, CurrencyIsoCode='USD', Country1__c = cntry.id, Country__c = 'USA', BillingCountry = 'USA',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
           // insert accObj;

            SVMXC__Skill__c skill = new SVMXC__Skill__c();
            insert skill;

            Schema.DescribeSObjectResult sr = Schema.SObjectType.SVMXC__Service_Group_Skills__c; 
            Map<String,Schema.RecordTypeInfo> skillMapByName = sr.getRecordTypeInfosByName();
            SVMXC__Service_Group_Skills__c expertise1 = new SVMXC__Service_Group_Skills__c();
            expertise1.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise1.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise1.SVMXC__Group_Member__c = tech1.id;
            expertise1.Out_of_Tolerance__c = false;
            expertise1.Acknowledge_Date__c = system.today()-1;
            expertise1.SVMXC__Availability_Start_Date__c = system.today()-7;
            expertise1.SVMXC__Availability_End_Date__c = system.today() - 3;
            expertise1.Acknowledge_Date__c = system.today() + 3;
            //expertise1.Vendor__c = accObj.Id;
            expertise1.Certificate_Email__c = '123';
            insert expertise1;

            SVMXC__Service_Group_Skills__c expertise2 = new SVMXC__Service_Group_Skills__c();
            expertise2.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise2.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise2.SVMXC__Group_Member__c = tech2.id;
            expertise2.Out_of_Tolerance__c = false;
            expertise2.Acknowledge_Date__c = system.today()-1;
            expertise2.SVMXC__Availability_Start_Date__c = system.today()-6;
            expertise2.SVMXC__Availability_End_Date__c = system.today() - 3;
            expertise2.Acknowledge_Date__c = system.today() + 3;
            //expertise2.Vendor__c = accObj.Id;
            expertise2.Certificate_Email__c = '123';
            insert expertise2;

            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.Out_of_Tolerance__c = false;
            expertise.SVMXC__Group_Member__c = tech1.id;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-1;
            insert expertise;

            expertise1.Out_of_Tolerance__c = true;
            update expertise1;
    }

    static testMethod void SR_Class_ExpertiseTest2()
    {

            SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
            workOrdObject.Event__c = True;
            workOrdObject.ownerID = serviceUser.id;
            workOrdObject.SVMXC__Order_Status__c = 'open';
            workOrdObject.SVMXC__Preferred_Technician__c = tech2.id;
            workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
            workOrdObject.SVMXC__Actual_Onsite_Response__c =  system.today()-10;
            // workOrdObject.SVMXC__Top_Level__c = testcase.SVMXC__Top_Level__c;
            //workOrdObject.SVMXC__Company__c = testcase.Accountid;
            //workOrdObject.SVMXC__Case__c = testcase.id;
            //workOrdObject.recordtypeid = rec.id;
            //workOrdObject.SVMXC__Product__c = testcase.Productid;
            Insert workOrdObject;
            String LineTypeMisc = system.label.LineTypeMisc;
            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
            wd.SVMXC__Group_Member__c = tech.id;
            wd.SVMXC__Line_Type__c = LineTypeMisc;
            wd.SVMXC__Service_Order__c = workOrdObject.id; 
            insert wd;
            system.debug('------------------->'+wd.SVMXC__Group_Member__c+'tech.RecordType.Name'+tech.RecordType.Name);
            String AccTypeInternal = system.label.AccTypeInternal;
            String AccNameVMS = system.label.Varian_Medical_Systems;
            Country__c cntry = [select id from  Country__c where Name = 'USA' limit 1];
            Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
            Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
            Account accObj = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = AccNameVMS, Account_Type__c = AccTypeInternal, CurrencyIsoCode='USD', Country1__c = cntry.id, Country__c = 'USA', BillingCountry = 'USA',OMNI_Postal_Code__c ='21234', Agreement_Type__c ='Terms Addendum',AccountNumber = '12345678', ERP_Timezone__c = 'AUSSA');
            //insert accObj;

            SVMXC__Skill__c skill = new SVMXC__Skill__c();
            insert skill;

            Schema.DescribeSObjectResult sr = Schema.SObjectType.SVMXC__Service_Group_Skills__c; 
            Map<String,Schema.RecordTypeInfo> skillMapByName = sr.getRecordTypeInfosByName();
            SVMXC__Service_Group_Skills__c expertise1 = new SVMXC__Service_Group_Skills__c();
            expertise1.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise1.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise1.SVMXC__Group_Member__c = tech2.id;
            expertise1.Out_of_Tolerance__c = false;
            expertise1.Acknowledge_Date__c = system.today()-1;
            expertise1.SVMXC__Availability_Start_Date__c = system.today()-6;
            expertise1.SVMXC__Availability_End_Date__c = system.today() - 3;
            expertise1.Acknowledge_Date__c = system.today() + 3;
            expertise1.Vendor__c = accObj.Id;
            expertise1.Certificate_Email__c = '123';
            insert expertise1;

            SVMXC__Service_Group_Skills__c expertise2 = new SVMXC__Service_Group_Skills__c();
            expertise2.RecordTypeId = skillMapByName.get('Technician').getRecordTypeId();
            expertise2.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise2.SVMXC__Group_Member__c = tech2.id;
            expertise2.Out_of_Tolerance__c = false;
            expertise2.Acknowledge_Date__c = system.today()-1;
            expertise2.SVMXC__Availability_Start_Date__c = system.today()-6;
            expertise2.SVMXC__Availability_End_Date__c = system.today() - 3;
            expertise2.Acknowledge_Date__c = system.today() + 3;
            expertise2.Vendor__c = accObj.Id;
            expertise2.Certificate_Email__c = '123';
            insert expertise2;

            SVMXC__Service_Group_Skills__c expertise = new SVMXC__Service_Group_Skills__c();
            expertise.SVMXC__Service_Group__c = ServiceTeam.id;
            expertise.Out_of_Tolerance__c = false;
            expertise.SVMXC__Group_Member__c = tech2.id;
            expertise.Acknowledge_Date__c = system.today()-1;
            expertise.SVMXC__Availability_End_Date__c = null;
            expertise.SVMXC__Availability_Start_Date__c = system.today()-1;
            insert expertise;

            expertise1.Out_of_Tolerance__c = true;
            update expertise1;
    }
}
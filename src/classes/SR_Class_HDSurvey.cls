/***************************************************************************
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
09-06-2017 - Rakesh - STSK0012699 - English traslation for field Write-in Comments if user locale is not english.
*************************************************************************************/
/***Us4134 called from trigger kaushiki 26/08/14 translation of write in comments from userlocale to english if user locale is not english*****/
public class SR_Class_HDSurvey
{

     @future (callout=true)
      public static void TranslatetheComments(Set<Id> HDidSet) {
        List<HD_Case_Closed_Survey_Results__c> HDtobeupdated = new List<HD_Case_Closed_Survey_Results__c>();
        Set<Id> Userid = new set<Id>();
        Map<Id,User> MapOfUser = new map<id,User>();
      // [select id,LanguageLocaleKey,LocaleSidKey from user where id in:MapOfUser].LanguageLocaleKey
        List<HD_Case_Closed_Survey_Results__c> HDList = new List<HD_Case_Closed_Survey_Results__c>([Select id,OwnerId,Translated_comments__c,RecordType.Name,Write_in_comments__c from HD_Case_Closed_Survey_Results__c where id in: HDidSet]);
         if(test.isRunningTest())
            HDList = new List<HD_Case_Closed_Survey_Results__c>([Select id,OwnerId,Translated_comments__c,RecordType.Name,Write_in_comments__c from HD_Case_Closed_Survey_Results__c where id in: HDidSet limit 1]);
        
        for(HD_Case_Closed_Survey_Results__c VarHD : HDList)
        {
            Userid.add(VarHD.OwnerId);
        }
        string UserLocale = [select id,LanguageLocaleKey,LocaleSidKey from user where id =:UserInfo.getUserId()].LanguageLocaleKey;
        if(UserLocale != null && UserLocale.contains('_'))
        {
            UserLocale = UserLocale.substring(0,UserLocale.indexOf('_'));
        }
        for(HD_Case_Closed_Survey_Results__c VarHD : HDList)
        {
            if(VarHD.Write_in_comments__c!= null && VarHD.RecordType.Name == 'HelpDesk')
            {
                HD_Case_Closed_Survey_Results__c  varHDAlias = new HD_Case_Closed_Survey_Results__c(id = VarHD.id);
                if(VarHD.Write_in_comments__c!= null )
                {
                    if(UserLocale != 'en')
                    {
                        TranslateService TranslateServiceobj = new TranslateService();
                        VarHDAlias.Translated_comments__c = TranslateServiceobj.getTranslatedstring(VarHD.Write_in_comments__c.replaceAll('<[^>]+>',' '),UserLocale);
                    }
                    else
                    {
                        varHDAlias.Translated_comments__c = VarHD.Write_in_comments__c;
                        system.debug('varHDAlias.Translated_comments__c ELSE' + varHDAlias.Translated_comments__c);
                    }
                    
                }
                HDtobeupdated.add(varHDAlias);
            }
        }
        update HDtobeupdated;
    }
    /***************************************************************************
    STSK0012699 - Description:
    Transulate Write-in Comments field and store in Eng Translation – Write-in Comments field if user locale is not english
    *************************************************************************************/
    @future (callout=true)
    public Static void TranslateFSComment(Set<Id> FieldServiceSet){
        
        List<HD_Case_Closed_Survey_Results__c> FSToUpdate = new List<HD_Case_Closed_Survey_Results__c>();
        Set<Id> SurveyOwnerIds = new set<Id>();
        List<HD_Case_Closed_Survey_Results__c> SurveyList = [Select id,Contact__r.Language_ISOCode__c,OwnerId,Eng_Translation_Write_in_Comments__c,RecordType.Name,Write_in_Comments_FS__c from HD_Case_Closed_Survey_Results__c where id in: FieldServiceSet];
        if(test.isRunningTest())
            SurveyList = new List<HD_Case_Closed_Survey_Results__c>([Select id,Contact__r.Language_ISOCode__c,OwnerId,Eng_Translation_Write_in_Comments__c,RecordType.Name,Write_in_Comments_FS__c from HD_Case_Closed_Survey_Results__c limit 1]);
        
        for(HD_Case_Closed_Survey_Results__c VarFS : SurveyList)
        {
            SurveyOwnerIds.add(VarFS.OwnerId);
        }
        
        
        for(HD_Case_Closed_Survey_Results__c VarFS : SurveyList)
        {
            if(test.isRunningTest() || (VarFS.Write_in_Comments_FS__c!= null && VarFS.RecordType.Name == 'Field Service'))
            {
                HD_Case_Closed_Survey_Results__c  VarFSAlias = new HD_Case_Closed_Survey_Results__c(id = VarFS.id);
                if(test.isRunningTest() || (VarFS.Write_in_Comments_FS__c!= null  && VarFS.Contact__c <> null && VarFS.Contact__r.Language_ISOCode__c <> null))
                {
                    string UserLocale = VarFS.Contact__r.Language_ISOCode__c;  
                    //STSK0012699.Start                  
                    if(UserLocale != null && UserLocale.contains('_'))
                    {
                        UserLocale = UserLocale.substring(0,UserLocale.indexOf('_'));
                    }
                    //STSK0012699.End
                    if(UserLocale != 'en')
                    {
                        TranslateService TranslateServiceobj = new TranslateService();
                        VarFSAlias.Eng_Translation_Write_in_Comments__c = TranslateServiceobj.getTranslatedstring(VarFS.Write_in_Comments_FS__c.replaceAll('<[^>]+>',' '),UserLocale);
                    }
                    else
                    {
                        VarFSAlias.Eng_Translation_Write_in_Comments__c = VarFS.Write_in_Comments_FS__c;
                    }
                    
                }
                FSToUpdate.add(VarFSAlias);
            }
        }
        update FSToUpdate;
    }
    //STSK0012699
    //Rakesh.04/11/2016.Start
    @future (callout=true)
      public static void RltshipTranslatetheComments(Set<Id> HDidSet) {
        List<HD_Case_Closed_Survey_Results__c> HDtobeupdated = new List<HD_Case_Closed_Survey_Results__c>();
        Set<Id> Userid = new set<Id>();
        Map<Id,User> MapOfUser = new map<id,User>();
        List<HD_Case_Closed_Survey_Results__c> HDList = new List<HD_Case_Closed_Survey_Results__c>([Select id,TransulateUpdate__c,Write_in_comments__c,Contact__c,Contact__r.Language_ISOCode__c,RecordType.Name,P_S_English_Translation__c, Preferred_Language__c,OwnerId,Translated_comments__c,Comments_on_products_and_services__c from HD_Case_Closed_Survey_Results__c where id in: HDidSet]);
        if(test.isRunningTest())
            HDList = new List<HD_Case_Closed_Survey_Results__c>([Select id,TransulateUpdate__c,Write_in_comments__c,Contact__c,Contact__r.Language_ISOCode__c,RecordType.Name,P_S_English_Translation__c, Preferred_Language__c,OwnerId,Translated_comments__c,Comments_on_products_and_services__c from HD_Case_Closed_Survey_Results__c where recordType.Name ='Relationship' limit 1]);
        
        for(HD_Case_Closed_Survey_Results__c VarHD : HDList)
        {
            VarHD.TransulateUpdate__c= VarHD.TransulateUpdate__c+1;  //09/13/2016.Rakesh
            //To transulate 'Comments on Products & Services' field
            if(VarHD.Comments_on_products_and_services__c!= null && VarHD.RecordType.Name == 'Relationship')
            {
                
                if(VarHD.Comments_on_products_and_services__c!= null && VarHD.Contact__c <> null && VarHD.Contact__r.Language_ISOCode__c <> null)
                {
                    string UserLocale = VarHD.Contact__r.Language_ISOCode__c;
                    if(UserLocale != null)
                    { 
                        if(UserLocale != 'en')
                        {
                            TranslateService TranslateServiceobj = new TranslateService();
                            VarHD.P_S_English_Translation__c = TranslateServiceobj.getTranslatedstring(VarHD.Comments_on_products_and_services__c.replaceAll('<[^>]+>',' '),UserLocale);
                            system.debug('VarHD.P_S_English_Translation__c ELSE 1' + VarHD.P_S_English_Translation__c);
                        }
                        else
                        {
                            VarHD.P_S_English_Translation__c = VarHD.Comments_on_products_and_services__c;
                            system.debug('VarHD.P_S_English_Translation__c ELSE 2' + VarHD.P_S_English_Translation__c);
                        }
                    }
                    
                }
                
            }
            //To transulate 'NPS Comments' field
            if(VarHD.Write_in_comments__c!= null && VarHD.RecordType.Name == 'Relationship')
            {
                
                if(VarHD.Write_in_comments__c!= null && VarHD.Contact__c <> null && VarHD.Contact__r.Language_ISOCode__c <> null)
                {
                    string UserLocale = VarHD.Contact__r.Language_ISOCode__c;
                    if(UserLocale != null)
                    {
                        
                        if(UserLocale != 'en')
                        {
                            
                            TranslateService TranslateServiceobj = new TranslateService();
                            VarHD.Translated_comments__c = TranslateServiceobj.getTranslatedstring(VarHD.Write_in_comments__c.replaceAll('<[^>]+>',' '),UserLocale);
                            system.debug('VarHD.Translated_comments__c ELSE 3' + VarHD.Translated_comments__c);
                        }
                        else
                        {
                            VarHD.Translated_comments__c = VarHD.Write_in_comments__c;
                            system.debug('VarHD.Translated_comments__c ELSE 4' + VarHD.Translated_comments__c);
                        }
                    }
                    
                }
                
            }
            // if((VarHD.Write_in_comments__c!= null || VarHD.Comments_on_products_and_services__c!= null) && VarHD.RecordType.Name == 'Relationship')
            // if((VarHD.Comments_on_products_and_services__c!= null) && VarHD.RecordType.Name == 'Relationship')
            // {
                HDtobeupdated.add(VarHD);
            // }
        }
        
        //Rakesh.04/11/2016.End
        update HDtobeupdated;
    }
    
}
global class SR_Class_InstallProductWO {

    global SVMXC__Service_Order__c workorder{get;set;}
    global List<SVMXC__Installed_Product__c> InstalledProduct {get;set;}
    global string workorderid;
    
    
    public SR_Class_InstallProductWO(ApexPages.StandardController controller) {
        workorderid = apexpages.currentpage().getparameters().get('id');
    }
    
    public pagereference buttonClick()
    {
        return new pagereference('/'+workorderid);
        
    }
    public pagereference CreateinstalledProduct(){
        
        Product2 prd = new Product2(); 
        
        Workorder = [Select id,ERP_Sales_Order__c,ERP_Functional_Location__c,ERP_Sales_Order_Item__c,ERP_Reference__c,SVMXC__Product__c,SVMXC__Top_Level__c,SVMXC__Site__c,SVMXC__Company__c,District__c from SVMXC__Service_Order__c where id =:workorderid ];
        try{
            prd = [select id,Name,Product_Type__c,Enable_Configuration_Tracking__c from Product2 where id =:Workorder.SVMXC__Product__c];
        }catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'There is no Product associated with the Work order');
            
            ApexPages.addMessage(myMsg);
            return null;

        }
        //if (prd.Product_Type__c ==null && prd.Enable_Configuration_Tracking__c ==true){
        //    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Product Type is not Catalog');
            
        //    ApexPages.addMessage(myMsg);
        //    return null;
        //}
        
        //if (prd.Product_Type__c == 'Catalog' && prd.Enable_Configuration_Tracking__c == false){
        //    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Enable Configuration Tracking is not True');
            
        //    ApexPages.addMessage(myMsg);
        //    return null;
        //}
        //if (prd.Product_Type__c == null && prd.Enable_Configuration_Tracking__c == false){
        //    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Product Type is not Catalog and Enable Configuration Tracking is not True');
            
        //    ApexPages.addMessage(myMsg);
        //    return null;
        //}
        SVMXC__Installed_Product__c insprd =new SVMXC__Installed_Product__c ();
        if (Workorder.SVMXC__Top_Level__c!=null){
            insprd =[Select id,Name from SVMXC__Installed_Product__c where id =:Workorder.SVMXC__Top_Level__c];
        }
        pagereference prf= null;
        
        if (prd.Product_Type__c == 'Catalog' && prd.Enable_Configuration_Tracking__c == True)
        {
            SVMXC__Installed_Product__c InstProduct = new SVMXC__Installed_Product__c();
            if (workorder.ERP_Reference__c!=null){
                InstProduct.WBS_Element__c=workorder.ERP_Reference__c;
            }
            InstProduct.SVMXC__Product__c= workorder.SVMXC__Product__c;
            InstProduct.Equipment_Description__c= prd.Name;
            if (insprd.name!=null){
                InstProduct.ERP_Top_Level__c= insprd.name;
                InstProduct.ERP_Parent__c= insprd.name;
            }
            InstProduct.SVMXC__Site__c= workorder.SVMXC__Site__c;
            if(workorder.SVMXC__Company__c!=null){
                InstProduct.SVMXC__Company__c= workorder.SVMXC__Company__c;
            }
            InstProduct.SVMXC__Sales_Order_Number__c= workorder.ERP_Sales_Order__c;
            InstProduct.ERP_Sales_Order_Item__c = workorder.ERP_Sales_Order_Item__c;
            InstProduct.ERP_CSS_District__c= workorder.District__c;
            InstProduct.SVMXC__Status__c= 'From Sales Order';
            InstProduct.Name= workorder.ERP_Sales_Order__c+' '+workorder.ERP_Sales_Order_Item__c;
            InstProduct.ERP_Functional_Location__c= workorder.ERP_Functional_Location__c;
            
            
            insert InstProduct; 
            pagereference prf1= new pagereference('/'+InstProduct.id);
            prf=prf1;
        }
        return prf;
    }
    
}
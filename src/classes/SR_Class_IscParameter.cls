/********************************************************************************************************************************
      @ Author          : Nikhil Verma
      @ Date            : 19-Oct-2014.
      @ Description     : This class contains methods for ISC Parameters triggers
      @ Last Modified By      :  
      @ Last Modified On      :   
      @ Last Modified Reason  :  
*********************************************************************************************************************************/
public class SR_Class_IscParameter
{
    //before trigger, Code for US5004
    public void updateIscParameterFields(List<ISC_Parameters__c> listIscParameter)
    {
        Map<Id, List<ISC_Parameters__c>> mapISCHistoryId_ISCParaName = new Map<Id, List<ISC_Parameters__c>>();
        Set<string> setOfParaDefName = new Set<string>();
        Map<Id, string> mapISCParaId_ERPpcode = new Map<Id, string>();
        Map<string, Id> mapOfISCParaDefNameAndId = new Map<string, Id>();
        List<ISC_Parameter_Definition__c> listUpdateISCParameterDef = new List<ISC_Parameter_Definition__c>();
        Map<string, Id> mapAPIName_ISCParaDef = new Map<string, Id>();
        //Set<String> setOfParaName = new Set<String>();    //Set<Id> setISCHistory = new Set<Id>();

        for(ISC_Parameters__c varISCpara : listIscParameter)
        {
            if(varISCpara.ISC__c != null && varISCpara.Parameter_Definition__c == null)
            {
                List<ISC_Parameters__c> listTempISCParameterName = new List<ISC_Parameters__c>();

                if(mapISCHistoryId_ISCParaName.containsKey(varISCpara.ISC__c))
                {
                    listTempISCParameterName = mapISCHistoryId_ISCParaName.get(varISCpara.ISC__c);
                }
                listTempISCParameterName.add(varISCpara);
                mapISCHistoryId_ISCParaName.put(varISCpara.ISC__c, listTempISCParameterName);
                System.debug('map of isc history and list of isc parameter ' + mapISCHistoryId_ISCParaName.get(varISCpara.ISC__c));
                //setISCHistory.add(varISCpara.ISC__c); //setOfParaName.add(varISCpara.Name);
            }
        }

        /***************** QUERY ON ISC HISTORY ****************************/
        if(mapISCHistoryId_ISCParaName.size() > 0)
        {
            System.debug('map of isc history list of isc parameter size greater than 0');
            for(ISC_History__c varISCH : [select Id, ERP_Pcode__c from ISC_History__c where Id in :mapISCHistoryId_ISCParaName.keySet()])
            {
                if(mapISCHistoryId_ISCParaName.containsKey(varISCH.Id) && mapISCHistoryId_ISCParaName.get(varISCH.Id) != null && varISCH.ERP_Pcode__c != null)
                {
                    System.debug('query isc parameter from the map');
                    for(ISC_Parameters__c varISCP : mapISCHistoryId_ISCParaName.get(varISCH.Id))
                    {
                        string strKey = varISCH.ERP_Pcode__c + '-' + varISCP.Name;
                        if(strKey != null)
                        {
                            setOfParaDefName.add(strKey);
                            mapISCParaId_ERPpcode.put(varISCP.Id, varISCH.ERP_Pcode__c);
                            System.debug('map of isc parameter and erp pcode' + mapISCParaId_ERPpcode.get(varISCP.Id));
                            System.debug('erp pcode-isc parameter name' + strKey);
                        }
                    }
                }
            }

            /**************** QUERY ON ISC PARAMETER DEFINITION ****************/
            if(setOfParaDefName.size()>0)
            {
                System.debug('setOfParaDefName@@@ ' + setOfParaDefName);
                for(ISC_Parameter_Definition__c iscParaDef : [select Id, API_Name__c from ISC_Parameter_Definition__c where API_Name__c in : setOfParaDefName])
                {
                    if(iscParaDef.API_Name__c != null)
                    {
                        mapOfISCParaDefNameAndId.put(iscParaDef.API_Name__c, iscParaDef.Id);
                        System.debug('mapOfISCParaDefNameAndId @@@ ' + mapOfISCParaDefNameAndId.get(iscParaDef.API_Name__c));
                    }
                }
            }
        }

        for(ISC_Parameters__c objISCpara : listIscParameter)
        {
            if(objISCpara.Parameter_Definition__c == null && mapISCParaId_ERPpcode.containsKey(objISCpara.Id) && mapISCParaId_ERPpcode.get(objISCpara.Id) != null)
            {
                System.debug(' to update isc parameter with isc parameter definition null ');
                string strKey = mapISCParaId_ERPpcode.get(objISCpara.Id) + '-' + objISCpara.Name;

                if(strKey != null && mapOfISCParaDefNameAndId.containsKey(strKey))
                {
                    objISCpara.Parameter_Definition__c = mapOfISCParaDefNameAndId.get(strKey);
                    System.debug('updated isc parameter ' + objISCpara.Parameter_Definition__c);
                }
                else
                {
                    //create new ISC Parameter Definition record
                    ISC_Parameter_Definition__c objISCParaDef = new ISC_Parameter_Definition__c();

                    objISCParaDef.API_Name__c = strKey;
                    objISCParaDef.Name = objISCpara.Name;

                    listUpdateISCParameterDef.add(objISCParaDef);   //DO NOT MOVE TO AFTER TRIGGER
                    System.debug('listUpdateISCParameterDef.size ' + listUpdateISCParameterDef.size());
                }
            }
        }
        
        if(listUpdateISCParameterDef.size() > 0)        //DO NOT MOVE TO AFTER TRIGGER
        {
            System.debug('listUpdateISCParameterDef insert isc parameter definition record ');
            insert listUpdateISCParameterDef;

            for(ISC_Parameter_Definition__c varISCParaDef : listUpdateISCParameterDef)
            {
                if(varISCParaDef.API_Name__c != null)
                {
                    mapAPIName_ISCParaDef.put(varISCParaDef.API_Name__c, varISCParaDef.Id);
                    System.debug('isc parameter definition map of new record ' + varISCParaDef.API_Name__c);
                }
            }
            //Update ISC Parameter
            for(ISC_Parameters__c objISCpara : listIscParameter)
            {
                if(mapISCParaId_ERPpcode.containsKey(objISCpara.Id) && mapISCParaId_ERPpcode.get(objISCpara.Id) != null/* && objISCpara.Parameter_Definition__c == null*/)
                {
                    string strKey = mapISCParaId_ERPpcode.get(objISCpara.Id) + '-' + objISCpara.Name;
                    if(strKey != null && mapAPIName_ISCParaDef.containsKey(strKey) && mapAPIName_ISCParaDef.get(strKey) != null && objISCpara.Parameter_Definition__c == null)
                    {
                        System.debug('update isc para with isc parameter definition ' + mapAPIName_ISCParaDef.get(strKey));
                        objISCpara.Parameter_Definition__c = mapAPIName_ISCParaDef.get(strKey);
                        System.debug('updated isc para def lookup with the new record' + objISCpara.Parameter_Definition__c);
                    }
                }
            }
        }
    }

    public void updateInstProdFrmISCParam(List<ISC_Parameters__c> IscParamTriggList)     //method to update ISC Parameter List from ISC Parameters. added by Nikhil Verma (US5002)
    {
        Set<Id> IscHistIdSet = new Set<Id>();
        Set<Id> instProdIdSet = new Set<Id>();
        Set<Id> prodIdSet = new Set<Id>();
        Set<Id> iscParamIdSet = new Set<Id>();
        Set<SVMXC__Installed_Product__c> instProdSet = new set<SVMXC__Installed_Product__c>();
        set<product2> prodSet = new set<product2>();
        Map<Id, String> iscHist2IscParamMap = new Map<Id, String>();
        Map<Id, String> prod2ProdVersMap = new Map<Id, String>();
        Map<Id, SVMXC__Installed_Product__c> instProdIdMap = new Map<Id, SVMXC__Installed_Product__c>();
        Map<Id, Product2> prodIdMap = new Map<Id, Product2>();

        for(ISC_Parameters__c iscParam : IscParamTriggList)
        {
            iscParamIdSet.add(iscParam.Id);
            if(iscParam.ISC__c != null)
            {
                IscHistIdSet.add(iscParam.ISC__c);
            }
        }
        List<ISC_Parameters__c> iscParamTriggListNew = [SELECT Id, Name, ISC__c, ISC__r.Installed_Product__c, ISC__r.Product__c FROM ISC_Parameters__c WHERE Id IN : iscParamIdSet];
        List<ISC_History__c> IscHistList = [SELECT Id, Name, Installed_Product__c, Product__c FROM ISC_History__c WHERE Id IN : IscHistIdSet];
        for(ISC_History__c iscHist : IscHistList)
        {
            if(iscHist.Installed_Product__c != null)
            {
                instProdIdSet.add(iscHist.Installed_Product__c);
            }
            if(iscHist.Product__c != null)
            {
                prodIdSet.add(iscHist.Product__c);
            }
        }
        List<SVMXC__Installed_Product__c> instProdList = [SELECT Id, Name, ISC_Parameter_List__c FROM SVMXC__Installed_Product__c WHERE Id IN :instProdIdSet];
        List<ISC_Parameters__c> IscParamList = [SELECT Id, Name, Parameter_Value__c, ISC__c FROM ISC_Parameters__c WHERE ISC__c IN :IscHistIdSet];
        List<Product2> prodList = [SELECT Id, Name, Product_Version_List__c FROM Product2 WHERE Id IN : prodIdSet];
        List<Product_Version__c> prodVersionList = [SELECT Id, Name, Product__c, Version__c FROM Product_Version__c WHERE Product__c IN : prodIdSet];
        for(ISC_Parameters__c iscParam : IscParamList)
        {
            if(iscHist2IscParamMap.containsKey(iscParam.ISC__c))
            {
                String temp = iscHist2IscParamMap.get(iscParam.ISC__c);
                temp = temp + '$,$' + iscParam.Name + '#^#' + iscParam.Parameter_Value__c;
                iscHist2IscParamMap.put(iscParam.ISC__c, temp);

            }
            else
            {
                iscHist2IscParamMap.put(iscParam.ISC__c,iscParam.Name + '#^#' + iscParam.Parameter_Value__c);
            }
        }
        system.debug('ISC Parameter Map: '+iscHist2IscParamMap);
        for(SVMXC__Installed_Product__c instProd : instProdList)
        {
            instProdIdMap.put(instProd.Id, instProd);
        }
        system.debug('Installed Product Map: '+instProdIdMap);
        for(Product2 prod : prodList)
        {
            prodIdMap.put(prod.Id, prod);
        }
        system.debug('Product Map: '+prodIdMap);
        for(Product_Version__c prodVers : prodVersionList)
        {
            if(prod2ProdVersMap.containsKey(prodVers.Product__c))
            {
                String temp = prod2ProdVersMap.get(prodVers.Product__c);
                temp = temp + '$,$' + prodVers.Version__c;
                prod2ProdVersMap.put(prodVers.Product__c, temp);
            }
            else
            {
                prod2ProdVersMap.put(prodVers.Product__c, prodVers.Version__c);
            }
        }
        system.debug('Product version Map: '+prod2ProdVersMap);
        instProdList.clear();
        prodList.clear();
        for(ISC_Parameters__c iscParam : iscParamTriggListNew)
        {
            SVMXC__Installed_Product__c instProd = instProdIdMap.get(iscParam.ISC__r.Installed_Product__c);
            system.debug(' IP retrieved from Map'+instProd);
            if(instProd != null)
            {
                instProd.ISC_Parameter_List__c = iscHist2IscParamMap.get(iscParam.ISC__c);
                instProdList.add(instProd);
            }
            Product2 prod = prodIdMap.get(iscParam.ISC__r.Product__c);
            system.debug('Product retrieved from Map'+prod);
            if(prod != null)
            {
                prod.Product_Version_List__c = prod2ProdVersMap.get(iscParam.ISC__r.Product__c);
                prodList.add(prod);
            }
        }
        system.debug('Installed Products to be updated:'+instProdList);
        if(instProdList.size()>0)
        {
            instProdSet.addall(instProdList);
        }
        if(prodList.size()>0)
        {
            prodSet.addall(prodList);
        }
        system.debug('instProdSet---'+instProdSet);
        system.debug('prodSet---'+prodSet);
        instProdList.clear();
        prodList.clear();
        system.debug('instProdList1---'+instProdList);
        system.debug('prodList---'+prodList);
        instProdList.addall(instProdSet);
        prodList.addall(prodSet);
        system.debug('instProdList2---'+instProdList);
        system.debug('Products to be updated:'+prodList);
        if(instProdList.size()>0)
        {
            update instProdList;
        }
        if(prodList.size()>0)
        {
            update prodList;
        }
    }
}
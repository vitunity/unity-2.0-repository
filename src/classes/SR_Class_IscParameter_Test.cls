/***************************************************************************
Author: Chandramouli Vegi
Created Date: 15-Sep-2017
Project/Story/Inc/Task : Keystone Project : Sep 2017 Release
Description: 
This is a test class to take care of the functionality of below triggers:
CustomerAgreements_Aftertrigger
IPComponents_beforetrigger
SR_ISC_Parameters_AfterTrigger

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/
@isTest
public class SR_Class_IscParameter_Test
{
    Public Static testMethod void SR_Class_IscParameterTest()
    {
        List<ISC_Parameters__c> listISCPara = new List<ISC_Parameters__c>();
        List<ISC_Parameters__c> listUpdatePara = new List<ISC_Parameters__c>();

        ISC_History__c varISCHist = new ISC_History__c();
        varISCHist.PCSN__c = 'H12345';
        insert varISCHist;

        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = '123456';
        testAcc.ERP_Site_Partner_Code__c = '123456';
        insert testAcc;

        Customer_Agreements__c ca = new Customer_Agreements__c ();
        ca.affected_account__c = testAcc.id;
        ca.Agreement_Type__c = 'Remote Service';
        ca.Agreement_Response__c = 'Yes';
        insert ca;

        ca.Agreement_Response__c = 'No';
        ca.comment__c = 'test';
        update ca;

        SVMXC__Site__c objLoc = new SVMXC__Site__c();
        objLoc.Name = 'Test Location';
        objLoc.SVMXC__Street__c = 'Test Street';
        objLoc.SVMXC__Country__c = 'United States';
        objLoc.SVMXC__Zip__c = '98765';
        objLoc.ERP_Functional_Location__c = 'Test Location';
        objLoc.Plant__c = 'Test Plant';
        objLoc.ERP_Site_Partner_Code__c = '123456';
        objLoc.SVMXC__Location_Type__c = 'Depot';
        //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
        objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
        //objLoc.ERP_Org__c = objOrg.id;
        objLoc.SVMXC__Stocking_Location__c = true;
        objLoc.SVMXC__Account__c = testAcc.id;
        insert objLoc;

        IP_Components__c ipc= new IP_Components__c();
        ipc.ipc_Computer_Id__c = 'testcomp';
        ipc.ipc_Functional_Location__c = 'Test Location';
        insert ipc;

        for(integer count = 0; count < 6; count++)
        {
            ISC_Parameters__c varISCPara = new ISC_Parameters__c();
            varISCPara.name = 'TEST ISC Parameter ' + count;
            if (count == 2)
            {
                varISCPara.name = 'Varian Function';
            }
            varISCPara.ISC__c = varISCHist.Id;
            if(count == 0)
                varISCPara.Parameter_Value__c = '2.1.21';
            else if(count == 1)
                varISCPara.Parameter_Value__c = '11/11/2011';
            else if(count == 2)
                varISCPara.Parameter_Value__c = '2';
            else if(count == 3)
                varISCPara.Parameter_Value__c = 'E-Arm';
            else if(count == 4)
                varISCPara.Parameter_Value__c = 'Y';
            else if(count == 5)
                varISCPara.Parameter_Value__c = 'N';
            varISCPara.iscp_IP_Components__c = ipc.id;
            listISCPara.add(varISCPara);
        }
        insert listISCPara;
        delete listISCPara;
    }
}
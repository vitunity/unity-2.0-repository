/*************************************************************************\

    @ Author        : mohit Sharma
    @ Date          : 28--feb-2014
    @ Description   : Class for creating a new L2848 record from the Case US3821
    @ Last Modified By  : Nikita Gupta
    @ Last Modified On  :    6/9/2014
    @ Last Modified Reason  :  added null checks and corrected indentation 
****************************************************************************/

public with sharing class SR_Class_L2848
{
    Public   void createL2848FromCase(List<Case> lstCase)
    {    
        set<string> stCaseID = new set<string>();                        //Set for Case Id
        Map<string,L2848__c> mapCaseL2848 = new Map<string,L2848__c>();  // Holding Case-L2848 records
        List<L2848__c> LstL2848 = new list<L2848__c>();                 //List for insert-update L2848 records
        
        for(Case varCase: lstCase)
        {
            //stCaseID.add(varCase.id);
            if(varCase.Is_escalation_to_the_CLT_required__c =='Yes' &&(varCase.Is_This_a_Complaint__c =='Yes'  ||  varCase.Was_anyone_injured__c =='Yes'))
            {
                L2848__c objl2848 = new L2848__c();
                objl2848.Case__c = varCase.id;
                LstL2848.add(objl2848);
            }
            stCaseID.add(varCase.id);
        }
        
       /* if(stCaseID.size() > 0) //added null check, Nikita 6/9/2014
        {
            for(L2848__c varL2848 : [select id,Case__c from L2848__c where Case__c = :stCaseID])
            {
                mapCaseL2848.put(varL2848.Case__c,varL2848);
            }
                
            Map<string,Case> mapCaseWithRelObject = new Map<string,Case>( [SELECT Id,Contact_Phone__c,Description,Account.CSS_District__c,
                                                               
                                                                SVMXC__Top_Level__r.Equipment_Description__c,
                                                                Case_Activity__c,Account.Territory_Team__r.CSS_District_Manager__r.Name,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c ,
                                                                Contact.Name,
                                                                Was_anyone_injured__c,
                                                                Owner.title,
                                                                Contact.Email,
                                                                SVMXC__Top_Level__r.SVMXC__Product__r.Name,

                                                                Original_Owner__c,
                                                                Is_This_a_Complaint__c,
                                                                Account.District_Manager__c,
                                                                Is_escalation_to_the_CLT_required__c ,
                                                                Account.Name ,
                                                                Account.ShippingCity,Account.ShippingCountry,
                                                                Account.ShippingState,
                                                                Account.BillingCity,Account.BillingCountry,
                                                                Account.BillingState,
                                                                Employee_Phone__c,
                                                                Phone_Number__c,
                                                                SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c,
                                                                Account.District__c,
                                                                City2__c,
                                                                Contact.Title,

                                                                SVMXC__Product__r.Name,
                                                                CreatedBy.Phone,
                                                                Device_or_Application_Name2__c ,
                                                                E_mail_Address__c ,PCSN__c,
                                                                PCSN2__c ,
                                                                Service_Pack__c,Service_Pack2__c,
                                                                CaseNumber ,

                                                                State_Province2__c,
                                                                Title_Role__c,
                                                                Version_Mode__c,Version_Model2__c,Site_Country__c 
                                                                FROM Case where id IN :stCaseID]
                                                             );// kaushiki Country__c changed to Site_Country__c

        // SVMXC__Top_Level__r.Product_Version_Build__r.Name, // Wave 1C - Nikita 1/25/2015
        
            if(mapCaseWithRelObject.size() > 0)
            {   
                for(Case varCase : mapCaseWithRelObject.Values())
                {   
                    L2848__c objL2848;
                    if(mapCaseL2848.containskey(varCase.id))
                    {
                        objL2848 = new L2848__c (id=mapCaseL2848.get(varCase.id).id);
                    }
                    else
                    {
                        objL2848 = new L2848__c();

                    }
                    
                    if(varCase.Is_escalation_to_the_CLT_required__c =='Yes' &&(varCase.Is_This_a_Complaint__c =='Yes'  ||  varCase.Was_anyone_injured__c =='Yes'))
                    {
                        if(mapCaseWithRelObject.containsKey(varCase.Id) && mapCaseWithRelObject.get(varCase.Id).Account.ShippingCity != Null && mapCaseWithRelObject.get(varCase.Id).Account.ShippingCountry != Null && mapCaseWithRelObject.get(varCase.Id).Account.ShippingState != Null) //added null check, Nikita 6/9/2014
                        {
                            objL2848.City__c = mapCaseWithRelObject.get(varCase.Id).Account.ShippingCity;
                            objL2848.Country__c = varCase.Account.ShippingCountry;
                            objL2848.State_Province__c = varCase.Account.ShippingState;
                        }
                        else
                        {
                            objL2848.City__c = varCase.Account.BillingCity;//Case__r.City2__c;
                            objL2848.Country__c = varCase.Account.BillingCountry;//Case__r.Country__c;
                            objL2848.State_Province__c = varCase.Account.BillingState;//Case__r.State_Province2__c;                       

                        }
                        system.debug('CaseId>>'+varCase.Id);
                        objL2848.Case__c = varCase.Id;
                        objL2848.Device_or_Application_Name__c = varCase.SVMXC__Top_Level__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
                        objL2848.Device_or_Application_Name2__c = varCase.Device_or_Application_Name2__c ;
                        objL2848.District_or_Area__c = varCase.Account.CSS_District__c; //District__c;
                        objL2848.E_mail_Address__c = varCase.Contact.Email;          //contact.Email;
                        objL2848.Employee_Phone__c = varCase.CreatedBy.Phone;
                        objL2848.Name_of_Customer_Contact__c = varCase.Contact.Name;        //Case__r.Contact.Name;
                        objL2848.PCSN__c = varCase.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c;//Case__r.PCSN__c;
                        objL2848.PCSN2__c = varCase.PCSN2__c ;
                        objL2848.Phone_Number__c = varCase.Contact_Phone__c;//Phone_Number__c;
                        objL2848.Service_Pack__c = varCase.Service_Pack__c;
                        objL2848.Service_Pack2__c = varCase.Service_Pack2__c;
                        objL2848.Service_Request_Notification__c = varCase.CaseNumber ;
                        objL2848.Site_Name__c = varCase.Account.Name;
                        objL2848.Title_Department__c = varCase.owner.Title;
                        objL2848.Title_Role__c = varCase.Contact.Title;
                        if(varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name!=null)
                        {
                            system.debug('varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name' +varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name);
                            objL2848.Varian_District_Manager_for_Site__c = varCase.Account.Territory_Team__r.CSS_District_Manager__r.Name;//Account.District_Manager__c; DE1489 Kaushiki 6/11/14
                            
                            system.debug('objL2848.Varian_District_Manager_for_Site__c' +objL2848.Varian_District_Manager_for_Site__c);
                        }
                        objL2848.Varian_Employee_Receiving_Info__c = varCase.Original_Owner__c;
                        //objL2848.Version_Model__c = varCase.SVMXC__Top_Level__r.Product_Version_Build__r.Name; // Wave 1C - Nikita 1/25/2015
                        objL2848.Version_Model2__c = varCase.Version_Model2__c;
                        objL2848.When_Where_Who_What__c = varCase.Case_Activity__c;
                        objL2848.Complaint_Short_Description__c = varCase.Description;
                        objL2848.Date_of_Event__c = System.Today();
                        objL2848.Date_Reported_to_Varian__c = System.Today();
                        objL2848.Was_anyone_injured__c = varCase.Was_anyone_injured__c;

                        LstL2848.add(objL2848);              
                    }
                }    
            }   
        }       
    */
       
        If(LstL2848.size()>0)      
        {
            insert LstL2848;

            //upsert LstL2848;   
        }  
    }   

}
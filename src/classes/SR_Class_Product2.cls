/*************************************************************************\
      @ Author          : Priti Jaiswal
      @ Date            : 09/22/2014
      @ Description     : This class contains all the logic related to product2
      Change Log:
      Date/Modified By Name/Task or Story or Inc # /Description of Change
      Feb-21-2018 : Ujjal Baruah : STRY0017303 : If Product Name is changed, the change need to be reflected in Counter Details as well.
**************************************************************************/

public Class SR_Class_Product2
{

    //priti, Us4914, 09/22/2014 -  to send email to logistic when PSE Approval Resquired is set to false on Product2       
    
    /************  Uncommented by kaushiki 1A 1C code merge 15/5/15*************/
    public void sendEmailToLogistic(List<Product2> lstOfProd, Map<Id, Product2> oldMap)
    {
        List<Product2> lstOfPrdToProcess = new List<Product2>(); 
        Set<String> setOfEmailAdd = new Set<String>(); 
        Map<Id, String> mapOfProdIDAndPartNumber = new Map<Id, String>();
        for(Product2 prod : lstOfProd )
        {
            if(((oldMap == null) || (oldMap != null && oldMap.get(prod.id).PSE_Approval_Required__c != prod.PSE_Approval_Required__c)) && prod.PSE_Approval_Required__c == false && prod.ProductCode != null)
            {
                mapOfProdIDAndPartNumber.put(prod.id, prod.ProductCode);
                lstOfPrdToProcess.add(prod);
            }
        }
        String[] toaddress = new String[]{};
        for(Logistic_Email__c logEmail : Logistic_Email__c.getall().values())
        {
            toaddress.add(logEmail.Email_Address__c);
        }   
        if(toaddress.size() > 0){
            for(Product2 prod : lstOfPrdToProcess)
            {   
                System.debug('to address-->'+toaddress);
                Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage();  
            
                //set the recipient
                mailHandler.setToAddresses(toaddress);
                //set the subject
                mailHandler.setSubject('PN : ' + mapOfProdIDAndPartNumber.get(prod.id));
                String strBody =  '<html><body>Hi,<br/><br/>Please remove PSE approval required flag for the associated part number.<br/><br/>Thanks & Regards,<br/>Varian Medical System</body></html>';
                mailHandler.setHtmlBody(strBody);

                Messaging.sendEmail(new Messaging.Email[] { mailHandler });  
                System.debug('mail handler-->'+mailHandler);
            }
        }
    }
    // 1C - Sunil - 20-Jan-15 ********/
    //Ritika US4960 to populate the ERP PCode field 
    public void populateFieldsOnProduct(List<Product2> listProduct)
    {
        set<string> setERPPcode = new set<string> ();
          
         List<ERP_Pcode__c> listERP_Pcode = new List<ERP_Pcode__c>();
         map<string,id> mapERPPcodeNameAndERPPcodeId = new  map<string,id> ();
        for(Product2 objProd : listProduct )
        {
           
            if(objProd.SAP_PCode__c!=null)
            {
                setERPPcode.add(objProd.SAP_PCode__c);
                
            }

            //savon.FF US5148 DE6642
            if (objProd.Product_Type__c == 'Part') {
                objProd.SVMXC__Product_Type__c = 'Part';
            }
            
        }
        if(setERPPcode!=null && setERPPcode.size()>0){
            listERP_Pcode = [select id,Name from ERP_Pcode__c where Name in :setERPPcode ];
        
        }
        if(listERP_Pcode!=null && listERP_Pcode.size()>0){
            for(ERP_Pcode__c objERP : listERP_Pcode){
                mapERPPcodeNameAndERPPcodeId .put(objERP.Name,objERP.id);
                
            }
        
        }
        for(Product2 varProd : listProduct )
        {
            if(varProd.SAP_PCode__c!=null &&  (varProd.ERP_Pcode__c == null || (mapERPPcodeNameAndERPPcodeId !=null && mapERPPcodeNameAndERPPcodeId.get(varProd.SAP_PCode__c)!=null)))
            {
                varProd.ERP_Pcode__c = mapERPPcodeNameAndERPPcodeId.get(varProd.SAP_PCode__c);
            }
            
           
            
        }
    
        //Set service max product type field using Product_Type__c
        //defect DE6642 fixed by Amitkumaar Katre
        for(Product2 varProd : listProduct ){
            if(varProd.Product_Type__c != null){
                varProd.SVMXC__Product_Type__c = varProd.Product_Type__c;
            }
        }
    
    }
    
    //This Method is used to Re-compute and Reassign Counter Details
    //8-6-15 - update to method by JW/FF for TA6989 
    public void updateCounterDetailFrmProduct(list<product2> lstProduct, map<id,product2> oldmap,map<id,product2> newmap)
    {
        //String recordtypeid = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Reading').getRecordTypeId() ;
        Map<Id, SVMXC__Counter_Details__c> mapCounterToUpdate = new Map<Id, SVMXC__Counter_Details__c>(); // List to Update Counters
        List<SVMXC__Counter_Details__c> bstQueueCounterList = new List<SVMXC__Counter_Details__c>(); //list of counter details to have owner set to BST queue
        Set<string> stIdsProduct = new Set<string>();  // Set to hold counters Value
        Set<string> stIdsProductUOM = new Set<string>(); //Feb-21-2018 : Ujjal Baruah : STRY0017303 : If Product Name is changed, the change need to be reflected in Counter Details as well.
        final Map<String, String> counterUnitMap = new Map<String, String>{'EA' => 'Count', 'UN' => 'Credits', 'HR' => 'Hours', 'DA' => 'Hours'};
        
        for(Product2 varProduct2: lstProduct )
        {
            /*if(varProduct2.Budget_Hrs__c !=null &&  (oldmap != null && oldmap.get(varProduct2.id).Budget_Hrs__c == null ||oldmap.get(varProduct2.id).Budget_Hrs__c == 0)  && (oldmap != null && oldmap.get(varProduct2.id).Budget_Hrs__c != varProduct2.Budget_Hrs__c))
        {
                stIdsProduct.add(varProduct2.id);
            }*/
            //Feb-21-2018 : Ujjal Baruah : STRY0017303 : If Product Name is changed, the change need to be reflected in Counter Details as well.
            if ( oldMap != null && (varProduct2.Name != oldmap.get(varProduct2.Id).Name && varProduct2.Name != null))
            {
                stIdsProduct.add(varProduct2.id);
                //stIdsProductName.add(varProduct2.id);
            }
            if(oldMap != null && (varProduct2.UOM__c != oldmap.get(varProduct2.Id).UOM__c && varProduct2.UOM__c != null))
           {
              stIdsProduct.add(varProduct2.id);
              stIdsProductUOM.add(varProduct2.id);
           }

        }

        Map<id,SVMXC__Counter_Details__c> map_CounterDetail = new Map<id,SVMXC__Counter_Details__c>([select id,recordtypeid,SVMXC__Coverage_Limit__c,SVMXC__Product__c,
                                                            SVMXC__Counters_Covered__c,SVMXC__Counter_Reading__c,Units__c, SVMXC__Counter_Name__c
                                                                                                    from SVMXC__Counter_Details__c where SVMXC__Product__c IN : stIdsProduct]);//AND Units__c ='Hours']);
                                                            
        if(map_CounterDetail.keyset().size()>0)
        {
            for(SVMXC__Counter_Details__c varCDT: map_CounterDetail.values() )
            {
                /*if(varCDT.recordtypeid == recordtypeid)// Checking Reading Record Type ID
               {
                 varCDT.SVMXC__Counter_Reading__c  = 0;
                }
                else
               {
               varCDT.SVMXC__Counter_Reading__c  = newmap.get(varCDT.SVMXC__Product__c).Budget_Hrs__c;
                }*/
                //Feb-21-2018 : Ujjal Baruah : STRY0017303 : If Product Name is changed, the change need to be reflected in Counter Details as well.
                if (stIdsProductUOM.contains(varCDT.SVMXC__Product__c))
                {
                    Product2 relatedProduct = newmap.get(varCDT.SVMXC__Product__c);
                    Product2 oldProduct = oldMap.get(varCDT.SVMXC__Product__c);
                
                    varCDT.Units__c = counterUnitMap.get(relatedProduct.UOM__c);
                
                    //CM: DE7513 Changed the below logic to make sure when there is change in UOM__c field
                    //if(relatedProduct.UOM__c == 'DA' && String.isNotBlank(oldProduct.UOM__c))
                    if(relatedProduct.UOM__c == 'DA' && relatedProduct.UOM__c != oldProduct.UOM__c)
                    {
                        if(varCDT.SVMXC__Counters_Covered__c != null)
                        {
                            varCDT.SVMXC__Counters_Covered__c *= 8;
                        }
                        
                        if(varCDT.SVMXC__Coverage_Limit__c != null)
                        {
                            varCDT.SVMXC__Coverage_Limit__c *= 8;
                        }
                    }
                    //CM: DE7513 Changed the below logic to make sure when there is change in UOM__c field
                    //else if(oldProduct.UOM__c == 'DA' && String.isNotBlank(relatedProduct.UOM__c))
                    else if(oldProduct.UOM__c == 'DA' && relatedProduct.UOM__c != oldProduct.UOM__c)
                    {
                        if(varCDT.SVMXC__Counters_Covered__c != null)
                        {
                            varCDT.SVMXC__Counters_Covered__c /= 8;
                        }
                        if(varCDT.SVMXC__Coverage_Limit__c != null)
                        {
                            varCDT.SVMXC__Coverage_Limit__c /= 8;
                        }
                    }

                    if(varCDT.SVMXC__Counter_Reading__c != 0)
                    {
                        bstQueueCounterList.add(varCDT);
                    }
                }
                    
                //varCDT.SVMXC__Counters_Covered__c = newmap.get(varCDT.SVMXC__Product__c).Budget_Hrs__c;
                //varCDT.SVMXC__Coverage_Limit__c = newmap.get(varCDT.SVMXC__Product__c).Budget_Hrs__c;
                //Feb-21-2018 : Ujjal Baruah : STRY0017303 : If Product Name is changed, the change need to be reflected in Counter Details as well.
                if(newmap != null && varCDT.SVMXC__Counter_Name__c != newmap.get(varCDT.SVMXC__Product__c).Name) {
                    varCDT.SVMXC__Counter_Name__c = newmap.get(varCDT.SVMXC__Product__c).Name;
                }
                mapCounterToUpdate.put(varCDT.Id, varCDT);    
            }
        }
      

        if(!bstQueueCounterList.isEmpty())
        {
            try{
                Id bstQueueId = [select Id from Group where Name = 'BST' and Type = 'Queue' Limit 1].Id;
                for(SVMXC__Counter_Details__c cd: bstQueueCounterList)
                {
                    cd.OwnerId = bstQueueId;
                    mapCounterToUpdate.put(cd.Id, cd);
                }
            }catch(QueryException qe){
                System.debug('ERROR: No BST queue found');
            }
        }

        if(mapCounterToUpdate.size()>0)
        {
            update mapCounterToUpdate.values();
        }
  }
    
    /**
     * Update ERP_Material_Memo_1__c with first 255 characters from ERP_Material_Memo__c
     */
    public void updateERPMaterialMemo(List<Product2> products){
        
        for(Product2 product : products){
            
            if(!String.isBlank(product.ERP_Material_Memo__c) && product.ERP_Material_Memo__c.length() > 255){
                product.ERP_Material_Memo_1__c = product.ERP_Material_Memo__c.subString(0,255);
            }else{
                product.ERP_Material_Memo_1__c = product.ERP_Material_Memo__c;
            }
        }
    }
}
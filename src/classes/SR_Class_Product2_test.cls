@isTest
private class SR_Class_Product2_test
{
    static map<ID, Product2> oldmap = new map<id, product2>();
    static List<Product2> lstOfProd = new List<Product2>();
    static void initData()
    {
        ERP_Pcode__c  erpPcode = new ERP_Pcode__c ();
        erpPcode.Name = '1234ab';
        insert erpPcode;
        
        Logistic_Email__c logEmail = new Logistic_Email__c ();
        logEmail.Name = 'email1';
        logEmail.Email_Address__c = 'abc@gmail.com';
        insert logEmail;
        
        
        
        Product2 prod = new Product2();
        prod.name = 'testProd';
        prod.PSE_Approval_Required__c = false;
        prod.SAP_PCode__c = '1234ab';
        prod.ProductCode = 'XYZ';
        prod.Budget_Hrs__c = 0;
        insert prod;
        lstOfProd.add(prod);
        oldmap.put(prod.id, prod);

        
        SVMXC__Counter_Details__c countDetail = new SVMXC__Counter_Details__c();    
        countDetail.SVMXC__Product__c  = prod.id;
        countDetail.Units__c = 'Hours';
        insert countDetail;     
        
        prod.Budget_Hrs__c = 4;
        prod.UOM__c = 'UN';
        update lstOfProd;        
        
        
        
    }
    
    static TestMethod void myUnitTest()
    {
        initData();
        SR_Class_Product2 prod2 = new SR_Class_Product2();
        //prod2.sendEmailToLogistic(lstOfProd, null);
        prod2.populateFieldsOnProduct(lstOfProd);
        prod2.updateCounterDetailFrmProduct(lstOfProd, oldmap, null);
    }

}
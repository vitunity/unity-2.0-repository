/****************************
 * Aurthor    :    Priti Jaiswal
 * Description :   This class include all logic related to Product System Subsystem
 * Created Date    :    03/06/2014
 * Last Modified By : Priti Jaiswal
 * Last Modified Reason :  US4257 - Set Require explanantion to true when System or Subsystem Contains 'Other' 
 * Last Modified Date :
****************************/

public class SR_Class_ProductSystemSubsystem
{
    
    public void setFieldsOnProdSysSubSystem(List<Product_System_Subsystem__c> lstOfProdSysSubSys)
    {
    	Set <String> ERP_Pcodes = new Set <String> ();
        for(Product_System_Subsystem__c prodSys : lstOfProdSysSubSys)
        {
            //prodSys.Covered_Part_Code__c = 'SY'+prodSys.ERP_PCode__c + prodSys.ERP_System_Code__c + prodSys.ERP_Subsystem_Code__c; // US3728
            if(prodSys.ERP_PCode__c != null)
            	prodSys.Covered_Part_Code__c = 'SY'+prodSys.ERP_PCode__c.subString(1) + prodSys.ERP_System_Code__c + prodSys.ERP_Subsystem_Code__c; // DE3362
            prodSys.Name = prodSys.ERP_PCode__c +'-'+prodSys.ERP_System_Code__c + '-' +prodSys.ERP_Subsystem_Code__c; //de3686
            String erpSysCode = '';
            String erpSubSysCode  = '';           
            if(prodSys.ERP_System_Code__c != null)
                erpSysCode = prodSys.ERP_System_Code__c.toUpperCase();
            if(prodSys.ERP_Subsystem_Code__c != null)
                erpSubSysCode = prodSys.ERP_Subsystem_Code__c.toUpperCase();
            if(erpSysCode.contains('OTH'))
                prodSys.System_requires_explanation__c = True; 
            if(erpSubSysCode.contains('OTH'))
                prodSys.Subsystem_requires_explanation__c = True;
            if(prodSys.ERP_Pcode__c != null && prodSys.ERP_Pcode_ID__c == null)
                    ERP_Pcodes.add(prodSys.ERP_Pcode__c);
        }
        Map <String,ERP_Pcode__c> mapPcode3ERPpcodes = new Map <String,ERP_Pcode__c>();
        if (ERP_Pcodes.size()>0)
        {
        	for (ERP_Pcode__c ep:[select id, Name from ERP_Pcode__c where Name in :ERP_PCodes])
        	{
        	      mapPcode3ERPpcodes.put(ep.Name,ep);
        	}
        }
        
        for(Product_System_Subsystem__c uprodSys : lstOfProdSysSubSys)
        {
        	if(uprodSys.ERP_Pcode__c != null && mapPcode3ERPpcodes.get(uprodSys.ERP_Pcode__c) != null)
        	{
        	       uprodSys.erp_pcode_id__c=mapPcode3ERPpcodes.get(uprodSys.ERP_Pcode__c).id;
        }
    } 
        
    }
     
}
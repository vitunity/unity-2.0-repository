/***************************************
@author : Bushra
Created On : 20-02-2015
Reason : This class contains data to cover functionality of class SR_Class_ProductSystemSubsystem

***************************************/

@isTest
public class SR_Class_ProductSystemSubsystem_Test{

    public static testMethod void CreateTestData()
    {
        // insert data for product system Subsystem //
        Product_System_Subsystem__c prodSystemSubSystem = new Product_System_Subsystem__c(  ERP_PCode__c='Test', ERP_System_Code__c = 'other', ERP_Subsystem_Code__c = 'other');
        insert prodSystemSubSystem;
        
    
    }

}
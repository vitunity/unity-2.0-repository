/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_Class_SVMXCTimesheetTEST {
  
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static SVMXC_Timesheet__c PreTimeSheet;
    public static List<SVMXC_Timesheet__c> timeSheetList; 
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static Time_Entry_Matrix__c matrix;
    public static Time_Entry_Matrix__c matrix1;
    public static List<Time_Entry_Matrix__c> matrixList;
    public static Organizational_Activities__c OrgActivity1;
    public static Organizational_Activities__c OrgActivity2;
    public static List<Organizational_Activities__c> OrgActivityList;
    public static Document Doc;
    public static Organizational_Calendar__c orgCal;
    public static Organizational_Holiday__c OrgHoli;
  
    static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];

        TCprofile = UnityDataTestClass.TIMECARD_Data(true);

        orgCal = UnityDataTestClass.OrgCalData(true);
        OrgHoli = UnityDataTestClass.OrgHoliday(true, orgCal);

        servTeam = UnityDataTestClass.serviceTeam_Data(true);

        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.Organizational_Calendar__c = orgCal.Id;
        technician.Weekend_Override__c = 'Sunday'; 
        insert technician;

        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];

        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        newAcc = UnityDataTestClass.AccountData(true);
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);

        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        insert NWA;
        
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.name = 'Default';
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        insert cdf; 
        
        WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
        WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        WO.ERP_WBS__c = WBS.Id;
        WO.ERP_Service_Order__c = '123';
        insert WO;

          WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
          
          WD.SVMXC__Consumed_From_Location__c = newLoc.id;
            WD.SVMXC__Activity_Type__c = 'Install';
            WD.Completed__c=false;
            WD.SVMXC__Group_Member__c = technician.id;
            WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
            WD.SVMXC__Work_Description__c = 'test';
            WD.SVMXC__Start_Date_and_Time__c =System.now();
            WD.Actual_Hours__c = 5;
            WD.SVMXC__Service_Order__c = WO.id;
            WD.Sales_Order_Item__c = SOI.id;
            WD.NWA_Description__c = 'nwatest';
          WD.SVMXC__Line_Status__c = 'Submitted';
          insert WD;


        //doc = UnityDataTestClass.Doc_Data();
    }
  
    private static void tmdata(){
        PreTimeSheet = UnitYDataTestClass.TIMESHEET_Data(false, technician);
        PreTimeSheet.Is_TEM__c = true;
        PreTimeSheet.Start_Date__c = system.today().addDays(-7);
        //insert PreTimeSheet;

        TimeSheet = UnitYDataTestClass.TIMESHEET_Data(false, technician);
        TimeSheet.Is_TEM__c = true;
        //insert TimeSheet;

        timeSheetList = new List<SVMXC_Timesheet__c>();
        timeSheetList.add(PreTimeSheet);
        timeSheetList.add(TimeSheet);
        insert timeSheetList;

        timeEntryList = new List<SVMXC_Time_Entry__c>();
        timeEntry = UnitYDataTestClass.TIMEENTRIES_DATA(false, false, TimeSheet, null);
        timeEntry2 = UnitYDataTestClass.TIMEENTRIES_DATA(false, true, TimeSheet, WD);
        //timeEntry2.Next_Eighth_Day__c =
        //timeEntryList.add(timeEntry);
        timeEntryList.add(timeEntry2);
        insert timeEntryList;

        OrgActivity1 = UnityDataTestClass.OrgActivity_Data(false, TCprofile, 'Indirect', 'Training');
        OrgActivity2 = new Organizational_Activities__c();
        OrgActivity2.Indirect_Hours__c = TCprofile.Id;
        OrgActivity2.Type__c = 'Indirect';
        OrgActivity2.Round_the_clock__c = false;
        OrgActivity2.Unpaid_Time__c = true;
        OrgActivity2.Recuperative_Time__c = false;
        OrgActivity2.Name = 'Lunch';

        OrgActivityList = new List<Organizational_Activities__c>();
        OrgActivityList.add(OrgActivity1);
        OrgActivityList.add(OrgActivity2);
        insert OrgActivityList;

        matrix = UnityDataTestClass.TIMEMATRIX_Data(false, 'Indirect', technician, 'Training', OrgActivity1);
        matrix.Status__c = 'Submitted';
        matrix1 = UnityDataTestClass.TIMEMATRIX_Data(false, 'Direct', technician, 'Training', OrgActivity2);
        matrixList = new List<Time_Entry_Matrix__c>();
        matrixList.add(matrix);
        matrixList.add(matrix1);
        insert matrixList;
    }
    static testMethod void myUnitTest() {
        Test.StartTest();
        tmdata();
        List<SVMXC_Timesheet__c> timeSheetListLocal = new List<SVMXC_Timesheet__c>();
        timeSheetListLocal.add(TimeSheet);
        map<Id, SVMXC_Timesheet__c> oldMap = new map<Id, SVMXC_Timesheet__c>(timeSheetListLocal);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.udpateFieldsOnTimesheetInBeforeInsertUpdateTrigger(timeSheetListLocal,oldMap);
        timeSheetcontrol.udpateFieldsOnTimesheetInBeforeTrigger(timeSheetListLocal);
        Test.StopTest();
    }
    
    static testMethod void getWorkItemId_Test() {
        Test.StartTest();
        tmdata();
        List<Id> getWorkItemIdList = new List<Id>();
        for( SVMXC_Time_Entry__c e : timeEntryList) {
        getWorkItemIdList.add(e.Id);
        }
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.getWorkItemId(getWorkItemIdList);
        Test.StopTest();
    }
    
    static testMethod void queryTechnicians_Test() {
        Test.StartTest();
        tmdata();
        Set<Id> techId = new Set<Id>();
        techId.add(technician.Id);//);SVMXC__Service_Group_Members__c technician;
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.queryTechnicians(techId);
        Test.StopTest();
    }
    
    static testMethod void CheckProfileAndCreateDefaultLunchBreaksOnTimeSheetCreationTest() {
        Test.StartTest();
        tmdata();
        TCprofile.Lunch_Time__c = true;
        TCprofile.Lunch_Time_Start__c = DateTime.newInstance(system.today().year(), 1, 31, 11, 0, 00);
        update TCprofile;
        Set<id> technicianIds = new Set<Id>();
        technicianIds.add(technician.Id);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.queryTechnicians(technicianIds);
        SR_Class_SVMXCTimesheet.techniciansMap = new Map<id,SVMXC__Service_Group_Members__c>([Select id,Timecard_Profile__r.Lunch_Time__c, SVMXC__Service_Group__c,
                        Timecard_Profile__c,Timecard_Profile__r.Name,Timecard_Profile__r.Lunch_Time_Start__c,
                        Timecard_Profile__r.Lunch_Time_duration__c,Organizational_Calendar__c,Weekend_Override__c,Name, 
                        SVMXC__Salesforce_User__c, ERP_Timezone__c, ERP_Work_Center__c, SVMXC__Working_Hours__c,
                        Additional_Availability__c, Timecard_Profile__r.X30_min_Travel_Time__c, Timecard_Profile__r.Normal_End_Time__c, 
                        Timecard_Profile__r.Normal_Start_Time__c, Additional_Availability__r.MondayEndTime, 
                        Additional_Availability__r.MondayStartTime,Additional_Availability__r.TuesdayStartTime,
                        Additional_Availability__r.WednesdayStartTime,Additional_Availability__r.ThursdayStartTime,
                        Additional_Availability__r.FridayStartTime, Additional_Availability__r.SaturdayStartTime, 
                        Additional_Availability__r.SundayStartTime, Additional_Availability__r.TuesdayEndTime,
                        Additional_Availability__r.WednesdayEndTime, Additional_Availability__r.ThursdayEndTime, 
                        Additional_Availability__r.FridayEndTime, Additional_Availability__r.SaturdayEndTime, 
                        Additional_Availability__r.SundayEndTime from SVMXC__Service_Group_Members__c where id =: technician.Id]);
        timeSheetcontrol.CheckProfileAndCreateDefaultLunchBreaksOnTimeSheetCreation(timeSheetList);

        Test.StopTest();
    }
    
    static testMethod void updateTimeEntryAndWorkDetail_Open_Test() {
        Test.StartTest();
        tmdata();
        SVMXC_Timesheet__c NewTimeSheet = TIMESHEET_Data(false, technician, 'Open');
        NewTimeSheet.Is_TEM__c = true;
        NewTimeSheet.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet.Status__c = 'Open';

        SVMXC_Timesheet__c NewTimeSheet2 = TIMESHEET_Data(false, technician, 'Open');
        NewTimeSheet2.Is_TEM__c = true;
        NewTimeSheet2.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet2.Status__c = 'Open';

        List<SVMXC_Timesheet__c> TSLIST = new List<SVMXC_Timesheet__c>();
        TSLIST.add(NewTimeSheet);
        //TSLIST.add(NewTimeSheet2);
        Insert TSLIST;

        SVMXC_Time_Entry__c timeEntry3 = UnitYDataTestClass.TIMEENTRIES_DATA(true, true, TSLIST[0], WD);
        system.debug(' ==== timeEntry3 ==== ' + timeEntry3.TimeSheet__c);
        List<SVMXC_Timesheet__c> TS = new List<SVMXC_Timesheet__c>();
        TS = [SELECT Id, Status__c FROM SVMXC_Timesheet__c WHERE Id =: timeEntry3.TimeSheet__c];
        system.debug(' ==== TS ==== ' + TS);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        SR_Class_SVMXCTimesheet.techniciansMap = new Map<id,SVMXC__Service_Group_Members__c>([Select id,Timecard_Profile__r.Lunch_Time__c, SVMXC__Service_Group__c,
                        Timecard_Profile__c,Timecard_Profile__r.Name,Timecard_Profile__r.Lunch_Time_Start__c,
                        Timecard_Profile__r.Lunch_Time_duration__c,Organizational_Calendar__c,Weekend_Override__c,Name, 
                        SVMXC__Salesforce_User__c, ERP_Timezone__c, ERP_Work_Center__c, SVMXC__Working_Hours__c,
                        Additional_Availability__c, Timecard_Profile__r.X30_min_Travel_Time__c, Timecard_Profile__r.Normal_End_Time__c, 
                        Timecard_Profile__r.Normal_Start_Time__c, Additional_Availability__r.MondayEndTime, 
                        Additional_Availability__r.MondayStartTime,Additional_Availability__r.TuesdayStartTime,
                        Additional_Availability__r.WednesdayStartTime,Additional_Availability__r.ThursdayStartTime,
                        Additional_Availability__r.FridayStartTime, Additional_Availability__r.SaturdayStartTime, 
                        Additional_Availability__r.SundayStartTime, Additional_Availability__r.TuesdayEndTime,
                        Additional_Availability__r.WednesdayEndTime, Additional_Availability__r.ThursdayEndTime, 
                        Additional_Availability__r.FridayEndTime, Additional_Availability__r.SaturdayEndTime, 
                        Additional_Availability__r.SundayEndTime from SVMXC__Service_Group_Members__c where id =: technician.Id]);
        Set<Id> setTimeSheetId = new Set<Id>();
        for(SVMXC_Timesheet__c sheet : TSLIST) {
        setTimeSheetId.add(sheet.Id);
        }

        SR_Class_SVMXCTimesheet.telstafterinsert = [select Id,RecordTypeId, Interface_Status__c, Status__c, Timesheet__r.Status__c, Work_Details__c, Work_Details__r.SVMXC__Service_Order__c from
                        SVMXC_Time_Entry__c WHERE Id =: timeEntry3.Id];
        system.debug(' ===== SR_Class_SVMXCTimesheet.telstafterinsert ====== ' + SR_Class_SVMXCTimesheet.telstafterinsert);
        for(SVMXC_Time_Entry__c e : SR_Class_SVMXCTimesheet.telstafterinsert) {
        system.debug(e.Timesheet__r.Status__c);
        }
        timeSheetcontrol.updateTimeEntryAndWorkDetail(setTimeSheetId);
        Test.Stoptest();
    }
    
    public static SVMXC_Timesheet__c TIMESHEET_Data(Boolean isInsert, SVMXC__Service_Group_Members__c tech, String status) {
        SVMXC_Timesheet__c ts = new SVMXC_Timesheet__c();
        ts.Start_Date__c = system.Today();
        ts.End_Date__c = system.today().addDays(1);
        ts.Status__c = status;
        ts.Technician__c = tech.Id;
        ts.TimeSheet_Unique__c = tech.Id + tech.ERP_Work_Center__c + system.today().year()+ system.today().month()+ system.today().day();
        if(isInsert)
        insert ts;
        return ts;
    }
    
    static testMethod void updateTimeEntryAndWorkDetail_Submitted_Test() {
        Test.StartTest();
        tmdata();
        SVMXC_Timesheet__c NewTimeSheet = TIMESHEET_Data(false, technician, 'Submitted');
        NewTimeSheet.Is_TEM__c = true;
        NewTimeSheet.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet.Status__c = 'Submitted';

        SVMXC_Timesheet__c NewTimeSheet2 = TIMESHEET_Data(false, technician, 'Submitted');
        NewTimeSheet2.Is_TEM__c = true;
        NewTimeSheet2.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet2.Status__c = 'Submitted';

        List<SVMXC_Timesheet__c> TSLIST = new List<SVMXC_Timesheet__c>();
        TSLIST.add(NewTimeSheet);
        //TSLIST.add(NewTimeSheet2);
        Insert TSLIST;

        SVMXC_Time_Entry__c timeEntry3 = UnitYDataTestClass.TIMEENTRIES_DATA(true, true, TSLIST[0], WD);
        system.debug(' ==== timeEntry3 ==== ' + timeEntry3.TimeSheet__c);
        List<SVMXC_Timesheet__c> TS = new List<SVMXC_Timesheet__c>();
        TS = [SELECT Id, Status__c FROM SVMXC_Timesheet__c WHERE Id =: timeEntry3.TimeSheet__c];
        system.debug(' ==== TS ==== ' + TS);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        SR_Class_SVMXCTimesheet.techniciansMap = new Map<id,SVMXC__Service_Group_Members__c>([Select id,Timecard_Profile__r.Lunch_Time__c, SVMXC__Service_Group__c,
                        Timecard_Profile__c,Timecard_Profile__r.Name,Timecard_Profile__r.Lunch_Time_Start__c,
                        Timecard_Profile__r.Lunch_Time_duration__c,Organizational_Calendar__c,Weekend_Override__c,Name, 
                        SVMXC__Salesforce_User__c, ERP_Timezone__c, ERP_Work_Center__c, SVMXC__Working_Hours__c,
                        Additional_Availability__c, Timecard_Profile__r.X30_min_Travel_Time__c, Timecard_Profile__r.Normal_End_Time__c, 
                        Timecard_Profile__r.Normal_Start_Time__c, Additional_Availability__r.MondayEndTime, 
                        Additional_Availability__r.MondayStartTime,Additional_Availability__r.TuesdayStartTime,
                        Additional_Availability__r.WednesdayStartTime,Additional_Availability__r.ThursdayStartTime,
                        Additional_Availability__r.FridayStartTime, Additional_Availability__r.SaturdayStartTime, 
                        Additional_Availability__r.SundayStartTime, Additional_Availability__r.TuesdayEndTime,
                        Additional_Availability__r.WednesdayEndTime, Additional_Availability__r.ThursdayEndTime, 
                        Additional_Availability__r.FridayEndTime, Additional_Availability__r.SaturdayEndTime, 
                        Additional_Availability__r.SundayEndTime from SVMXC__Service_Group_Members__c where id =: technician.Id]);
        Set<Id> setTimeSheetId = new Set<Id>();
        for(SVMXC_Timesheet__c sheet : TSLIST) {
        setTimeSheetId.add(sheet.Id);
        }

        SR_Class_SVMXCTimesheet.telstafterinsert = [select Id,RecordTypeId, Interface_Status__c, Status__c, Timesheet__r.Status__c, Work_Details__c, Work_Details__r.SVMXC__Service_Order__c from
                        SVMXC_Time_Entry__c WHERE Id =: timeEntry3.Id];
        system.debug(' ===== SR_Class_SVMXCTimesheet.telstafterinsert ====== ' + SR_Class_SVMXCTimesheet.telstafterinsert);
        for(SVMXC_Time_Entry__c e : SR_Class_SVMXCTimesheet.telstafterinsert) {
        system.debug(e.Timesheet__r.Status__c);
        }
        timeSheetcontrol.updateTimeEntryAndWorkDetail(setTimeSheetId);
        Test.Stoptest();
    }
    
    static testMethod void updateTimeEntryAndWorkDetail_Approved_Test() {
        Test.StartTest();
        tmdata();
        SVMXC_Timesheet__c NewTimeSheet = TIMESHEET_Data(false, technician, 'Approved');
        NewTimeSheet.Is_TEM__c = true;
        NewTimeSheet.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet.Status__c = 'Approved';

        SVMXC_Timesheet__c NewTimeSheet2 = TIMESHEET_Data(false, technician, 'Approved');
        NewTimeSheet2.Is_TEM__c = true;
        NewTimeSheet2.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet2.Status__c = 'Approved';

        List<SVMXC_Timesheet__c> TSLIST = new List<SVMXC_Timesheet__c>();
        TSLIST.add(NewTimeSheet);
        //TSLIST.add(NewTimeSheet2);
        Insert TSLIST;

        SVMXC_Time_Entry__c timeEntry3 = UnitYDataTestClass.TIMEENTRIES_DATA(true, true, TSLIST[0], WD);
        system.debug(' ==== timeEntry3 ==== ' + timeEntry3.TimeSheet__c);
        List<SVMXC_Timesheet__c> TS = new List<SVMXC_Timesheet__c>();
        TS = [SELECT Id, Status__c FROM SVMXC_Timesheet__c WHERE Id =: timeEntry3.TimeSheet__c];
        system.debug(' ==== TS ==== ' + TS);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        SR_Class_SVMXCTimesheet.techniciansMap = new Map<id,SVMXC__Service_Group_Members__c>([Select id,Timecard_Profile__r.Lunch_Time__c, SVMXC__Service_Group__c,
                        Timecard_Profile__c,Timecard_Profile__r.Name,Timecard_Profile__r.Lunch_Time_Start__c,
                        Timecard_Profile__r.Lunch_Time_duration__c,Organizational_Calendar__c,Weekend_Override__c,Name, 
                        SVMXC__Salesforce_User__c, ERP_Timezone__c, ERP_Work_Center__c, SVMXC__Working_Hours__c,
                        Additional_Availability__c, Timecard_Profile__r.X30_min_Travel_Time__c, Timecard_Profile__r.Normal_End_Time__c, 
                        Timecard_Profile__r.Normal_Start_Time__c, Additional_Availability__r.MondayEndTime, 
                        Additional_Availability__r.MondayStartTime,Additional_Availability__r.TuesdayStartTime,
                        Additional_Availability__r.WednesdayStartTime,Additional_Availability__r.ThursdayStartTime,
                        Additional_Availability__r.FridayStartTime, Additional_Availability__r.SaturdayStartTime, 
                        Additional_Availability__r.SundayStartTime, Additional_Availability__r.TuesdayEndTime,
                        Additional_Availability__r.WednesdayEndTime, Additional_Availability__r.ThursdayEndTime, 
                        Additional_Availability__r.FridayEndTime, Additional_Availability__r.SaturdayEndTime, 
                        Additional_Availability__r.SundayEndTime from SVMXC__Service_Group_Members__c where id =: technician.Id]);
        Set<Id> setTimeSheetId = new Set<Id>();
        for(SVMXC_Timesheet__c sheet : TSLIST) {
        setTimeSheetId.add(sheet.Id);
        }

        SR_Class_SVMXCTimesheet.telstafterinsert = [select Id,RecordTypeId, Interface_Status__c, Status__c, Timesheet__r.Status__c, Work_Details__c, Work_Details__r.SVMXC__Service_Order__c from
                        SVMXC_Time_Entry__c WHERE Id =: timeEntry3.Id];
        system.debug(' ===== SR_Class_SVMXCTimesheet.telstafterinsert ====== ' + SR_Class_SVMXCTimesheet.telstafterinsert);
        for(SVMXC_Time_Entry__c e : SR_Class_SVMXCTimesheet.telstafterinsert) {
        system.debug(e.Timesheet__r.Status__c);
        }
        timeSheetcontrol.updateTimeEntryAndWorkDetail(setTimeSheetId);
        Test.Stoptest();
    }
    
    static testMethod void queryTimeEntry_test() {
        Test.StartTest();
        tmdata();
        Set<Id> timeSheetListLocal = new Set<Id>();
        timeSheetListLocal.add(TimeSheet.Id);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.queryTimeEntry(timeSheetListLocal);
        Test.StopTest();
    }
    
    static testMethod void updateTimeEntryWorkDetailWorkOrder_Test() {
        Test.StartTest();
        tmdata();
        timeEntry = UnitYDataTestClass.TIMEENTRIES_DATA(true, false, TimeSheet, null);
        timeEntry2.Next_Eighth_Day__c = timeEntry.Id;
        update timeEntry2;

        Set<Id> timeSheetListLocal = new Set<Id>();
        timeSheetListLocal.add(TimeSheet.Id);
        SR_Class_SVMXCTimesheet timeSheetcontrol = new SR_Class_SVMXCTimesheet();
        timeSheetcontrol.updateTimeEntryWorkDetailWorkOrder(timeSheetListLocal);
        Test.StopTest();
    }
    
    static testMethod void CheckRecordforSubmitted_Test() {
        Test.StartTest();
        tmdata();
        SVMXC_Timesheet__c NewTimeSheet = TIMESHEET_Data(false, technician, 'Submitted');
        NewTimeSheet.Is_TEM__c = true;
        NewTimeSheet.Start_Date__c = system.today().addDays(-14);
        NewTimeSheet.Status__c = 'Submitted';
        insert newTimeSheet;

        SR_Class_SVMXCTimesheet control = new SR_Class_SVMXCTimesheet();
        control.CheckRecordforSubmitted(newTimeSheet.Id);
        Test.StopTest();
    }
}
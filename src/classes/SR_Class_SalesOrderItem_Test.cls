@isTest
private class SR_Class_SalesOrderItem_Test {
  
  public static BusinessHours businesshr;
  public static SVMXC__Service_Group__c servTeam;
  public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static Sales_Order_Item__c PSOI;
    public static List<Sales_Order_Item__c> soiList;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Service_Level__c slaTerm;
    public static SVMXC__Service_Contract__c servcContrct;
    public static SVMXC__Service_Contract_Products__c contractProd;
    public static SVMXC__Counter_Details__c counter;
    public static Counter_Usage__c usage;
    static {
      businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
    
      TCprofile = UnityDataTestClass.TIMECARD_Data(true);
    
      servTeam = UnityDataTestClass.serviceTeam_Data(true);
    
      technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
      technician.Timecard_Profile__c = TCprofile.Id;
      insert technician;
    
      newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
    
      businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
      newAcc = UnityDataTestClass.AccountData(true);
      erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
      newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
      newProd = UnityDataTestClass.product_Data(true);
      SO = UnityDataTestClass.SO_Data(true,newAcc);
      PSOI = UnityDataTestClass.SOI_Data(false, SO);
      SOI = UnityDataTestClass.SOI_Data(false, SO);
      SOI.Parent__c = SOI.Id;
      soiList = new List<Sales_Order_Item__c>();
      soiList.add(SOI);
      soiList.add(PSOI);
      insert soiList;
    
      WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
      NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
      NWA.ERP_Std_Text_Key__c = 'PM00001';
      insert NWA;
    
      WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
      WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
      WO.ERP_WBS__c = WBS.Id;
      WO.ERP_Service_Order__c = '123';
      insert WO;
    
    WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
      
    WD.SVMXC__Consumed_From_Location__c = newLoc.id;
    WD.SVMXC__Activity_Type__c = 'Install';
    WD.Completed__c=false;
    WD.SVMXC__Group_Member__c = technician.id;
    WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
    WD.SVMXC__Work_Description__c = 'test';
    WD.SVMXC__Start_Date_and_Time__c =System.now();
    WD.Actual_Hours__c = 5;
    WD.SVMXC__Service_Order__c = WO.id;
    WD.Sales_Order_Item__c = SOI.id;
    WD.NWA_Description__c = 'nwatest';
    WD.SVMXC__Line_Status__c = 'Submitted';
    insert WD;
    
    }
  
  public static void TESTDATA() {
      slaTerm = UnityDataTestClass.slaterm_Data(true, businessHr);
    servcContrct = UnityDataTestClass.servContract_Data(true, slaTerm, newAcc, servTeam);
    contractProd = UnityDataTestClass.CoveredProd_Data(true, slaTerm, servcContrct);
    counter = UnityDataTestClass.CD_Data(true, contractProd, SOI);
      
    usage = UnityDataTestClass.CounterUSE_Data(true, WD, counter);
  }
  
    static testMethod void updateSalesOrderItem_Test() {
      Test.StartTest();
      
      TESTDATA();
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
      
      SOI.ERP_Item_Profit_Center__c = 'ABC2';
      SOI.Sales_Order_Number__c = 'XYZ123';
      SOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      SOI.ERP_Higher_Level_Item__c = 'ABC321';
      SOI.ERP_Site_Partner_Code__c ='9870';
      SOI.ERP_ship_to_party__c = '098765';
      SOI.Site_Partner__c = newAcc.Id;
      SOI.Site_Partner2__c = newERPPartner2.Id;
      SOI.Product__c = newProd.Id;
      SOI.Status__c = 'Cancelled';
      SOI.ERP_Higher_Level_Item__c = 'AXF123';
      SOI.ERP_Reference__c = 'ABC321';
      SOI.ERP_ship_to_party__c = 'New Zealand';
      SOI.Ship_To_Party__c = null;
      SOI.Quantity__c = 5;
      update SOI; 
      
      Test.StopTest();
  }
  
    static testMethod void createCounterDetails_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        update newProd;
      
        ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
        PSOI.ERP_Item_Profit_Center__c = 'ABC2';
        PSOI.Sales_Order_Number__c = 'XYZ123';
        PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
        PSOI.ERP_Higher_Level_Item__c = 'ABC321';
        PSOI.ERP_Site_Partner_Code__c ='9870';
        PSOI.ERP_ship_to_party__c = '098765';
        PSOI.Site_Partner__c = newAcc.Id;
        PSOI.Site_Partner2__c = newERPPartner2.Id;
        PSOI.Product__c = newProd.Id;
        PSOI.Status__c = 'Cancelled';
        PSOI.ERP_Higher_Level_Item__c = 'AXF123';
        PSOI.ERP_Reference__c = 'ABC321';
        PSOI.ERP_ship_to_party__c = 'New Zealand';
        PSOI.Ship_To_Party__c = null;
        PSOI.Quantity__c = 100;
        PSOI.ERP_Item_Category__c = 'Z014';
        update PSOI;
      
        SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
    static testMethod void createCounterDetails_UOM_DA_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        newProd.UOM__c = 'DA';
        update newProd;
      
        ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
        PSOI.ERP_Item_Profit_Center__c = 'ABC2';
        PSOI.Sales_Order_Number__c = 'XYZ123';
        PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
        PSOI.ERP_Higher_Level_Item__c = 'ABC321';
        PSOI.ERP_Site_Partner_Code__c ='9870';
        PSOI.ERP_ship_to_party__c = '098765';
        PSOI.Site_Partner__c = newAcc.Id;
        PSOI.Site_Partner2__c = newERPPartner2.Id;
        PSOI.Product__c = newProd.Id;
        PSOI.Status__c = 'Cancelled';
        PSOI.ERP_Higher_Level_Item__c = 'AXF123';
        PSOI.ERP_Reference__c = 'ABC321';
       PSOI.ERP_ship_to_party__c = 'New Zealand';
        PSOI.Ship_To_Party__c = null;
        PSOI.Quantity__c = 100;
        PSOI.ERP_Item_Category__c = 'Z014';
        update PSOI;
      
        SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
    static testMethod void createCounterDetails_UOM_EA_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        newProd.UOM__c = 'EA';
        update newProd;
      
        ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
        PSOI.ERP_Item_Profit_Center__c = 'ABC2';
        PSOI.Sales_Order_Number__c = 'XYZ123';
        PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
        PSOI.ERP_Higher_Level_Item__c = 'ABC321';
        PSOI.ERP_Site_Partner_Code__c ='9870';
        PSOI.ERP_ship_to_party__c = '098765';
        PSOI.Site_Partner__c = newAcc.Id;
        PSOI.Site_Partner2__c = newERPPartner2.Id;
        PSOI.Product__c = newProd.Id;
        PSOI.Status__c = 'Cancelled';
        PSOI.ERP_Higher_Level_Item__c = 'AXF123';
        PSOI.ERP_Reference__c = 'ABC321';
       PSOI.ERP_ship_to_party__c = 'New Zealand';
        PSOI.Ship_To_Party__c = null;
        PSOI.Quantity__c = 100;
        PSOI.ERP_Item_Category__c = 'Z014';
        update PSOI;
      
        SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
    static testMethod void createCounterDetails_UOM_UN_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        newProd.UOM__c = 'UN';
        update newProd;
      
        ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
        PSOI.ERP_Item_Profit_Center__c = 'ABC2';
        PSOI.Sales_Order_Number__c = 'XYZ123';
        PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
        PSOI.ERP_Higher_Level_Item__c = 'ABC321';
        PSOI.ERP_Site_Partner_Code__c ='9870';
        PSOI.ERP_ship_to_party__c = '098765';
        PSOI.Site_Partner__c = newAcc.Id;
        PSOI.Site_Partner2__c = newERPPartner2.Id;
        PSOI.Product__c = newProd.Id;
        PSOI.Status__c = 'Cancelled';
        PSOI.ERP_Higher_Level_Item__c = 'AXF123';
        PSOI.ERP_Reference__c = 'ABC321';
        PSOI.ERP_ship_to_party__c = 'New Zealand';
        PSOI.Ship_To_Party__c = null;
        PSOI.Quantity__c = 100;
        PSOI.ERP_Item_Category__c = 'Z014';
        update PSOI;
      
        SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
    static testMethod void createCounterDetails_UOM_HR_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        newProd.UOM__c = 'HR';
        update newProd;
      
        ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
        PSOI.ERP_Item_Profit_Center__c = 'ABC2';
        PSOI.Sales_Order_Number__c = 'XYZ123';
        PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
        PSOI.ERP_Higher_Level_Item__c = 'ABC321';
        PSOI.ERP_Site_Partner_Code__c ='9870';
        PSOI.ERP_ship_to_party__c = '098765';
        PSOI.Site_Partner__c = newAcc.Id;
        PSOI.Site_Partner2__c = newERPPartner2.Id;
        PSOI.Product__c = newProd.Id;
        PSOI.Status__c = 'Cancelled';
        PSOI.ERP_Higher_Level_Item__c = 'AXF123';
        PSOI.ERP_Reference__c = 'ABC321';
        PSOI.ERP_ship_to_party__c = 'New Zealand';
        PSOI.Ship_To_Party__c = null;
        PSOI.Quantity__c = 100;
        PSOI.ERP_Item_Category__c = 'Z014';
        update PSOI;
      
        SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
    static testMethod void createCounterDetails_SOUOM_NULL_Test() {
      Test.StartTest();
        //delete counter;
      
        TESTDATA();
      
        newProd.Material_Group__c = 'H:EDUCTN';
        newProd.UOM__c = null;
        update newProd;
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
      PSOI.ERP_Item_Profit_Center__c = 'ABC2';
      PSOI.Sales_Order_Number__c = 'XYZ123';
      PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      PSOI.ERP_Higher_Level_Item__c = 'ABC321';
      PSOI.ERP_Site_Partner_Code__c ='9870';
      PSOI.ERP_ship_to_party__c = '098765';
      PSOI.Site_Partner__c = newAcc.Id;
      PSOI.Site_Partner2__c = newERPPartner2.Id;
      PSOI.Product__c = newProd.Id;
      PSOI.Status__c = 'Cancelled';
      PSOI.ERP_Higher_Level_Item__c = 'AXF123';
      PSOI.ERP_Reference__c = 'ABC321';
      PSOI.ERP_ship_to_party__c = 'New Zealand';
      PSOI.Ship_To_Party__c = null;
      PSOI.Quantity__c = 100;
      PSOI.ERP_Item_Category__c = 'Z014';
      PSOI.UOM__c = 'DA';
      update PSOI;
      
      SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
      //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
    Test.StopTest();
  }
  
  static testMethod void createCounterDetails_SOUOM_EA_Test() {
    Test.StartTest();
      //delete counter;
      
      TESTDATA();
      
      newProd.Material_Group__c = 'H:EDUCTN';
      newProd.UOM__c = null;
      update newProd;
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
      PSOI.ERP_Item_Profit_Center__c = 'ABC2';
      PSOI.Sales_Order_Number__c = 'XYZ123';
      PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      PSOI.ERP_Higher_Level_Item__c = 'ABC321';
      PSOI.ERP_Site_Partner_Code__c ='9870';
      PSOI.ERP_ship_to_party__c = '098765';
      PSOI.Site_Partner__c = newAcc.Id;
      PSOI.Site_Partner2__c = newERPPartner2.Id;
      PSOI.Product__c = newProd.Id;
      PSOI.Status__c = 'Cancelled';
      PSOI.ERP_Higher_Level_Item__c = 'AXF123';
      PSOI.ERP_Reference__c = 'ABC321';
      PSOI.ERP_ship_to_party__c = 'New Zealand';
      PSOI.Ship_To_Party__c = null;
      PSOI.Quantity__c = 100;
      PSOI.ERP_Item_Category__c = 'Z014';
      PSOI.UOM__c = 'EA';
      update PSOI;
      
      SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
      //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
    Test.StopTest();
  }
  
  static testMethod void createCounterDetails_SOUOM_UN_Test() {
    Test.StartTest();
      //delete counter;
      
      TESTDATA();
      
      newProd.Material_Group__c = 'H:EDUCTN';
      newProd.UOM__c = null;
      update newProd;
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
      PSOI.ERP_Item_Profit_Center__c = 'ABC2';
      PSOI.Sales_Order_Number__c = 'XYZ123';
      PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      PSOI.ERP_Higher_Level_Item__c = 'ABC321';
      PSOI.ERP_Site_Partner_Code__c ='9870';
      PSOI.ERP_ship_to_party__c = '098765';
      PSOI.Site_Partner__c = newAcc.Id;
      PSOI.Site_Partner2__c = newERPPartner2.Id;
      PSOI.Product__c = newProd.Id;
      PSOI.Status__c = 'Cancelled';
      PSOI.ERP_Higher_Level_Item__c = 'AXF123';
      PSOI.ERP_Reference__c = 'ABC321';
      PSOI.ERP_ship_to_party__c = 'New Zealand';
      PSOI.Ship_To_Party__c = null;
      PSOI.Quantity__c = 100;
      PSOI.ERP_Item_Category__c = 'Z014';
      PSOI.UOM__c = 'UN';
      update PSOI;
      
      SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
      //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
    Test.StopTest();
  }
  
  static testMethod void createCounterDetails_SOUOM_HR_Test() {
    Test.StartTest();
    //delete counter;
      
    TESTDATA();
      
    newProd.Material_Group__c = 'H:EDUCTN';
    newProd.UOM__c = null;
    update newProd;
      
    ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
    
    PSOI.ERP_Item_Profit_Center__c = 'ABC2';
    PSOI.Sales_Order_Number__c = 'XYZ123';
    PSOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
    PSOI.ERP_Higher_Level_Item__c = 'ABC321';
    PSOI.ERP_Site_Partner_Code__c ='9870';
    PSOI.ERP_ship_to_party__c = '098765';
    PSOI.Site_Partner__c = newAcc.Id;
    PSOI.Site_Partner2__c = newERPPartner2.Id;
    PSOI.Product__c = newProd.Id;
    PSOI.Status__c = 'Cancelled';
    PSOI.ERP_Higher_Level_Item__c = 'AXF123';
    PSOI.ERP_Reference__c = 'ABC321';
    PSOI.ERP_ship_to_party__c = 'New Zealand';
    PSOI.Ship_To_Party__c = null;
    PSOI.Quantity__c = 100;
    PSOI.ERP_Item_Category__c = 'Z014';
    PSOI.UOM__c = 'HR';
    update PSOI;
      
      SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
    //controller.createCounterDetails(new List<Sales_Order_Item__c>{PSOI});
      Test.StopTest();
    }
  
  static testMethod void UpdateZ001SOItemProductModel_Test() {
      Test.StartTest();
        
      TESTDATA();
  
    SR_Class_SalesOrderItem controller = new SR_Class_SalesOrderItem();
        controller.UpdateZ001SOItemProductModel(soiList);
      
      Test.StopTest();
    }
  
  static testMethod void updateCounterDetails_Test() {
      Test.StartTest();
        
      TESTDATA();
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
      
      SOI.ERP_Item_Profit_Center__c = 'ABC2';
      SOI.Sales_Order_Number__c = 'XYZ123';
      SOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      SOI.ERP_Higher_Level_Item__c = 'ABC321';
      SOI.ERP_Site_Partner_Code__c ='9870';
      SOI.ERP_ship_to_party__c = '098765';
      SOI.Site_Partner__c = newAcc.Id;
      SOI.Site_Partner2__c = newERPPartner2.Id;
      SOI.Product__c = newProd.Id;
      SOI.Status__c = 'Cancelled';
      SOI.ERP_Higher_Level_Item__c = 'AXF123';
      SOI.ERP_Reference__c = 'ABC321';
      SOI.ERP_ship_to_party__c = 'New Zealand';
      SOI.Ship_To_Party__c = null;
      SOI.Quantity__c = 100;
      SOI.ERP_Item_Category__c = 'Z014';
      update SOI;   
        
      Test.StopTest();
    }
  
  static testMethod void cancelCounterDetail_Test() {
      Test.Starttest();
      
      TESTDATA();
      
      ERP_Partner__c newERPPartner2 = UnityDataTestClass.createERPPartner(true, newAcc);
      
      SOI.ERP_Item_Profit_Center__c = 'ABC2';
      SOI.Sales_Order_Number__c = 'XYZ123';
      SOI.ERP_Material_Number__c = SOI.ERP_Material_Number__c + 's';
      SOI.ERP_Higher_Level_Item__c = 'ABC321';
      SOI.ERP_Site_Partner_Code__c ='9870';
      SOI.ERP_ship_to_party__c = '098765';
      SOI.Site_Partner__c = newAcc.Id;
      SOI.Site_Partner2__c = newERPPartner2.Id;
      SOI.Product__c = newProd.Id;
      SOI.Status__c = 'Cancelled';
      SOI.ERP_Higher_Level_Item__c = 'AXF123';
      SOI.ERP_Reference__c = 'ABC321';
      SOI.ERP_ship_to_party__c = 'New Zealand';
      SOI.Ship_To_Party__c = null;
      SOI.Quantity__c = 100;
      SOI.ERP_Item_Category__c = 'Z014';
      SOI.Reject_Reason__c ='Rejected';
      update SOI;   
    
      Test.StopTest();
  }
}
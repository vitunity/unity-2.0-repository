public class SR_Class_ServiceSurvey
{

     @future (callout=true)
      public static void TranslatetheComments(Set<Id> SSidSet)
      {
        List<Service_Survey_Results__c> ServeyTobBeUpdated = new List<Service_Survey_Results__c>();
        Set<Id> Userid = new set<Id>();
        Map<Id,User> MapOfUser = new map<id,User>();
        //[select id,LanguageLocaleKey,LocaleSidKey from user where id in:MapOfUser].LanguageLocaleKey
        List<Service_Survey_Results__c> SSList = new List<Service_Survey_Results__c>([Select id,OwnerId,Translated_comments__c,Write_in_comments__c from Service_Survey_Results__c where id in: SSidSet]);
        system.debug('SSList'+SSList );
        for(Service_Survey_Results__c VarSS : SSList)
        {
            Userid.add(VarSS.OwnerId);
        }
        /*for(User us: [select id,LanguageLocaleKey,LocaleSidKey from user where id =: UserInfo.getUserId()])
        {
            MapOfUser.put(us.Id,us);
        }*/
         string UserLocale = [select id,LanguageLocaleKey,LocaleSidKey from user where id =:UserInfo.getUserId()].LanguageLocaleKey;
        if(UserLocale != null && UserLocale.contains('_'))
        {
            UserLocale = UserLocale.substring(0,UserLocale.indexOf('_'));
        }
        system.debug('UserLocale ' + UserLocale );
        system.debug('TTTTTTT' + SSList);
        for(Service_Survey_Results__c VarSS : SSList)
        {
            if(VarSS.Write_in_comments__c!= null )
            {
                Service_Survey_Results__c  VarSSAlias = new Service_Survey_Results__c(id = VarSS.id);
                system.debug('DDDDD' + (VarSS.Write_in_comments__c!= null));
                if(VarSS.Write_in_comments__c!= null)
                {
                    if(UserLocale != 'en')
                    {
                        TranslateService TranslateServiceobj = new TranslateService();
                        VarSSAlias.Translated_comments__c = TranslateServiceobj.getTranslatedstring(VarSS.Write_in_comments__c.replaceAll('<[^>]+>',' '),UserLocale);
                        system.debug('VarSSAlias.Translated_comments__c IF PART' + VarSSAlias.Translated_comments__c);
                    }
                    else
                    {
                        VarSSAlias.Translated_comments__c = VarSS.Write_in_comments__c;
                        system.debug('VarSSAlias.Translated_comments__c ELSE' + VarSSAlias.Translated_comments__c);
                    }
                    
                }
                ServeyTobBeUpdated .add(VarSSAlias);
            }
        }
        //system.debug('DDDDDDD' + Casetobeupdated[0].Local_Subject__c + 'SSSSSS' + Casetobeupdated[0].Internal_Comments__c);
        update ServeyTobBeUpdated ; 
        
    } 
}
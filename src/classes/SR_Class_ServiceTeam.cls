public class SR_Class_ServiceTeam
{
/***********************************************************************
* Author         : Mohit Sharma
* Functionality  : Method execution from trigger :Service Team
* Description    : Service Team Trigger Execution
*Created         : 16-June-2015
Date/Modified By Name/Task or Story or Inc # /Description of Change
11-14-2017 - Rakesh - STSK0013241 - RD & DM email fields update on Technician  based on District Manager on Service Team.
1/16/2018 - Rakesh - STSK0013691 - Added Equipment RT
************************************************************************/

public static boolean chk_recursive = false;   //Variable to stop  recursion
    
    
/*=======================Method to Create Inventory Location=============================*/ 
    public static void create_InvLocation(list<SVMXC__Service_Group__c> lstSrvcTeam,map<id,SVMXC__Service_Group__c> Oldmap) //Method to Create Inventory Location for Searvice team
    {
        list<SVMXC__Site__c> lstLocation = new list<SVMXC__Site__c>();
        Map<string,string> map_RecDevNameIdMapping =new Map<string,string>();
        List<SVMXC__Service_Group__c> lstSvcTeam_Update=new List<SVMXC__Service_Group__c>(); // List To Update Service Team
        Map<string,recordType> map_rec_Location = new map<string,recordType>([select id , Name,developername from Recordtype where sobjecttype='SVMXC__Site__c']);
        
        for(recordtype rt:map_rec_Location.values())      // Iteration Over Recordtype
        {
          map_RecDevNameIdMapping.put(rt.developername,rt.id); // Adding recordtype developerName-Id mapping
        }
    
        for(SVMXC__Service_Group__c varSrvcTeam : lstSrvcTeam)
        {
            if(varSrvcTeam.SVMXC__Group_Type__c!=null && (varSrvcTeam.SVMXC__Group_Type__c.containsIgnoreCase('Installation') || varSrvcTeam.SVMXC__Group_Type__c =='HW Installation' || varSrvcTeam.SVMXC__Group_Type__c =='Field Service' || varSrvcTeam.SVMXC__Group_Type__c =='Field Service') &&( Oldmap ==null ||( Oldmap !=null && varSrvcTeam.SVMXC__Group_Type__c !=Oldmap.get(varSrvcTeam.id).SVMXC__Group_Type__c) ))
            {
                SVMXC__Site__c varLoc = new SVMXC__Site__c();
                
                varLoc.ERP_CSS_District__c = varSrvcTeam.SVMXC__Group_Code__c;
                varLoc.SVMXC__Stocking_Location__c= true;
                varLoc.SVMXC__Location_Type__c = 'Depot';
                varLoc.recordtypeid= map_RecDevNameIdMapping.get('Inventory_Location');
                varLoc.Active__c = true ;
                varSrvcTeam.Inventory_Location__r = varLoc;
                lstSvcTeam_Update.add(varSrvcTeam);
                lstLocation.add(varLoc);
            }
        }
        
        if(lstLocation.size() > 0 ) // Checking if list  :holding Data to insert 
        {
            insert lstLocation;     // Inserting record
        }
        
        
        if(lstSvcTeam_Update.size() > 0 ) // Checking if lstSvcTeam_Update : holding Data
        {
            for(SVMXC__Service_Group__c varSVCTeam :lstSrvcTeam )
            {
                varSVCTeam.Inventory_Location__c = varSVCTeam.Inventory_Location__r.id;
                
                    
            }
        }
       // chk_recursive = true;  // Sets value to true
       
    }
    
    public static Boolean StopOOCUpdate = false;
    //STSK0012092
    public static void UpdateDM_FOA_SC(List<SVMXC__Service_Group__c> techServiceTeam, Map<Id, SVMXC__Service_Group__c> oldtechServiceTeam, Boolean isUpdate) {
        // making sure below logic work only for Update
        if(!isUpdate)
            return;
        
        Map<String,Schema.RecordTypeInfo> ServiceRecordType = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName();
        String TechServiceEquipRecType = ServiceRecordType.get('Equipment').getRecordTypeId();
        
        Map<String, SVMXC__Service_Group__c> serviceteamId = new Map<String, SVMXC__Service_Group__c>(); // Map of GroupCode -> SVMXC__Service_Group__c
        Map<Id, SVMXC__Service_Group__c> serviceGrpSetId = new Map<Id, SVMXC__Service_Group__c>(); // Map of SVMXC__Service_Group__c id -> SVMXC__Service_Group__c
        
        List<SVMXC__Service_Group_Members__c> updateTech_Equip = new List<SVMXC__Service_Group_Members__c>(); // List of technician to get updated
        List<SVMXC__Service_Group__c> equipServiceTeam = new List<SVMXC__Service_Group__c>();
        system.debug(' ==== techServiceTeam ==== ' + techServiceTeam);
        
        map<Id,string> mapDMEmail = new map<Id,string>();
        map<Id,string> mapRDEmail = new map<Id,string>();
        map<Id,string> mapFOAEmail = new map<Id,string>();
        set<Id> setSGroup = new set<Id>();
        set<Id> setFOAGroup = new set<Id>();
        // Making one more SOQL as related lookup field do not come along trigger
        for(SVMXC__Service_Group__c grp : [ SELECT Id, Name, District_Manager__c,Regional_Manager__c, District_Manager__r.manager.email,District_Manager__r.Email,Regional_Manager__r.Email , FOA__c, FOA__r.Email, SC__c, SC__r.Email, SVMXC__Group_Code__c, RecordTypeId
                                            FROM SVMXC__Service_Group__c WHERE Id IN: techServiceTeam]) {
            system.debug(' ==== grp ==== ' + grp);
            system.debug(' ==== oldtechServiceTeam ==== ' + oldtechServiceTeam);
            system.debug(' ==== oldtechServiceTeam.get(grp.Id).District_Manager__c ==== ' + oldtechServiceTeam.get(grp.Id).District_Manager__c + ' = ' + grp.District_Manager__c);
            system.debug(' ==== oldtechServiceTeam.get(grp.Id).FOA__c ==== ' + oldtechServiceTeam.get(grp.Id).FOA__c + ' = ' + grp.FOA__c);
            system.debug(' ==== oldtechServiceTeam.get(grp.Id).SC__c ==== ' + oldtechServiceTeam.get(grp.Id).SC__c + ' = ' + grp.SC__c);
            system.debug(' ==== oldtechServiceTeam.get(grp.Id).SC__c ==== ' + serviceteamId);
            if((ServiceRecordType.get('Technician').getRecordTypeId() == grp.RecordTypeId) || (ServiceRecordType.get('Equipment').getRecordTypeId() == grp.RecordTypeId)) { //STSK0013691. Added Equipment RT
                // If Dristict Manager changed OR FOA changed OR SC changed
                if( (grp.District_Manager__c != oldtechServiceTeam.get(grp.Id).District_Manager__c) || (grp.FOA__c != oldtechServiceTeam.get(grp.Id).FOA__c) || (grp.Regional_Manager__c != oldtechServiceTeam.get(grp.Id).Regional_Manager__c) ||
                        (grp.SC__c != oldtechServiceTeam.get(grp.Id).SC__c) ) {
                    serviceteamId.put(grp.SVMXC__Group_Code__c + '-E', grp);
                    //serviceGrpSetId.put(grp.Id, grp);
                    if( grp.District_Manager__c != oldtechServiceTeam.get(grp.Id).District_Manager__c || grp.Regional_Manager__c != oldtechServiceTeam.get(grp.Id).Regional_Manager__c){ 
                        setSGroup.add(grp.id);
                        mapDMEmail.put(grp.id,grp.District_Manager__r.email);                           
                        mapRDEmail.put(grp.id,grp.Regional_Manager__r.Email);
                    }
                    if( grp.FOA__c != oldtechServiceTeam.get(grp.Id).FOA__c){ 
                        setFOAGroup.add(grp.id);
                        mapFOAEmail.put(grp.id,grp.FOA__r.Email);
                    }
                    
                }
            }
            system.debug(' ==== oldtechServiceTeam.get(grp.Id).SC__c ==== ' + serviceteamId);
        }
        
        if(!serviceteamId.isEmpty()) {
            
            for(SVMXC__Service_Group__c serviceGrp : [  SELECT Id, Name, SVMXC__Group_Code__c, SC__r.Email, SC__c, FOA__r.Email, FOA__c, 
                                                                District_Manager__r.Email, District_Manager__c, RecordTypeId 
                                                        FROM SVMXC__Service_Group__c WHERE SVMXC__Group_Code__c IN: serviceteamId.keySet()
                                                            AND RecordTypeId =: TechServiceEquipRecType]) {
                serviceGrp.District_Manager__c = serviceteamId.get(serviceGrp.SVMXC__Group_Code__c).District_Manager__c;
                serviceGrp.FOA__c = serviceteamId.get(serviceGrp.SVMXC__Group_Code__c).FOA__c;
                serviceGrp.SC__c = serviceteamId.get(serviceGrp.SVMXC__Group_Code__c).SC__c;
                equipServiceTeam.add(serviceGrp);
                serviceGrpSetId.put(serviceGrp.Id, serviceteamId.get(serviceGrp.SVMXC__Group_Code__c));
            }
            update equipServiceTeam;
            
            system.debug(' == I M HERE == ' + serviceGrpSetId);//system.assert(false);
            for(SVMXC__Service_Group_Members__c members : [ SELECT Id, DM_Email__c, FOA_Email__c, SC_Email__c, SVMXC__Service_Group__c, InventoryLocation__c, SVMXC__Inventory_Location__c 
                                                            FROM SVMXC__Service_Group_Members__c WHERE (SVMXC__Service_Group__c IN: setFOAGroup or SVMXC__Service_Group__c IN: setSGroup) or (SVMXC__Service_Group__c IN: serviceGrpSetId.keySet()
                                                            AND SVMXC__Inventory_Location__c != null)]) {
                
                system.debug(' === SVMXC__Service_Group_Members__c members === 105 > ' + members);
                
                if(serviceGrpSetId.containsKey(members.SVMXC__Service_Group__c) && members.SVMXC__Inventory_Location__c != null){
                    if(members.DM_Email__c != serviceGrpSetId.get(members.SVMXC__Service_Group__c).District_Manager__r.Email
                    || members.FOA_Email__c !=  serviceGrpSetId.get(members.SVMXC__Service_Group__c).FOA__r.Email
                    || members.SC_Email__c != serviceGrpSetId.get(members.SVMXC__Service_Group__c).SC__r.Email) {
                        
                        members.DM_Email__c = serviceGrpSetId.get(members.SVMXC__Service_Group__c).District_Manager__r.Email;
                        members.FOA_Email__c =  serviceGrpSetId.get(members.SVMXC__Service_Group__c).FOA__r.Email;
                        members.SC_Email__c = serviceGrpSetId.get(members.SVMXC__Service_Group__c).SC__r.Email;
                        
                        system.debug(' == I M HERE AGAINa == ' + serviceGrpSetId.get(members.SVMXC__Service_Group__c).District_Manager__r.Email);
                        system.debug(' == I M HERE AGAINb == ' + serviceGrpSetId.get(members.SVMXC__Service_Group__c).FOA__r.Email);
                        system.debug(' == I M HERE AGAINc == ' + serviceGrpSetId.get(members.SVMXC__Service_Group__c).SC__r.Email);
                        
                    }
                }
                //STSK0013241.Start
                if(setSGroup.contains(members.SVMXC__Service_Group__c)){
                    members.DM_Email__c = (mapDMEmail.containsKey(members.SVMXC__Service_Group__c))?mapDMEmail.get(members.SVMXC__Service_Group__c):'';
                    members.RD_Email__c = (mapRDEmail.containsKey(members.SVMXC__Service_Group__c))?mapRDEmail.get(members.SVMXC__Service_Group__c):'';
                }
                //STSK0013241.End
                
                if(setFOAGroup.contains(members.SVMXC__Service_Group__c)){
                    members.FOA_Email__c = (mapFOAEmail.containsKey(members.SVMXC__Service_Group__c))?mapFOAEmail.get(members.SVMXC__Service_Group__c):'';
                }
                
                
                updateTech_Equip.add(members);
            }
            system.debug(' == I M HERE AGAIN == ' + updateTech_Equip);
            //system.assert(false);
        }
        //system.assert(false);
        // Updating Technician/Equipment
        if(!updateTech_Equip.isEmpty()) {
            StopOOCUpdate = true;
            Database.Update( updateTech_Equip);
            system.debug(' == I M HERE AGAIN 2 == ' + updateTech_Equip);
            //system.assert(false);
        }
    }
    
    /**
     *    STSK0012095: STRY0024706 : Service Manager updates from Service Team to Account 
     *    Puneet Mishra, 27 July 2017
     */
    public static void serviceManagerUpdatefromTeamtoAccount(List<SVMXC__Service_Group__c> techServiceTeam, Map<Id, SVMXC__Service_Group__c> oldtechServiceTeam) {
        Map<String,Schema.RecordTypeInfo> ServiceRecordType = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName();
        String TechServiceEquipRecType = ServiceRecordType.get('Technician').getRecordTypeId();
        
        Map<String, SVMXC__Service_Group__c> grpId_ServiceMap = new Map<String, SVMXC__Service_Group__c>();
        Map<String, User> managerMap = new Map<String, User>();
        List<Account> updateAcc = new List<Account>();
        Set<Id> managerSet = new Set<Id>();
        
        for(SVMXC__Service_Group__c grp : techServiceTeam) {
            if(ServiceRecordType.get('Technician').getRecordTypeId() == grp.RecordTypeId) {
                if(grp.District_Manager__c != oldtechServiceTeam.get(grp.Id).District_Manager__c ||
                   grp.Regional_Manager__c != oldtechServiceTeam.get(grp.Id).Regional_Manager__c) {
                    grpId_ServiceMap.put(grp.SVMXC__Group_Code__c, grp);
                    managerSet.add(grp.District_Manager__c);
                }
            }
        }
        
        Map<Id, User> usr_managerMap = new Map<Id, User>();
        for(User u : [SELECT id, ManagerId FROM User WHERE Id IN: managerSet]) {
            usr_managerMap.put(u.Id, u);
        }
        
        if(!grpId_ServiceMap.isEmpty()){
            
            for(Account acc : [SELECT Id, Name, District_Manager__c, CSS_District__c 
                               FROM Account WHERE CSS_District__c in: grpId_ServiceMap.keySet()]) {
                system.debug('=== acc.District_Manager__c = == ' + acc.District_Manager__c);
                String distManagerId = grpId_ServiceMap.get(acc.CSS_District__c).District_Manager__c;
                acc.District_Service_Manager__c = usr_managerMap.get(distManagerId).Id;
                String managerId = grpId_ServiceMap.get(acc.CSS_District__c).District_Manager__c;
                //acc.Regional_Service_Manager__c = usr_managerMap.get(managerId).ManagerId ;
                //Pranjul - STRY0069687 [Service Regional Manager on Account]
                acc.Regional_Service_Manager__c = grpId_ServiceMap.get(acc.CSS_District__c).Regional_Manager__c;
                updateAcc.add(acc);
            }
        }
        system.debug('===== ' + updateAcc);
        if(!updateAcc.isEmpty())
            Database.update(updateAcc);
    
    }
}
/*************************************************************************\
    @ Author        : Vipul Jain
    @ Date      :     20-Aug-2013
    @ Description   : This class is being used to send the FSR pdf to contact or district manager of work order.
    @ Last Modified By  :Vipul Jain   
    @ Last Modified On  :19-sep-2013
    @ Last Modified Reason  : Updated to attach the FSR pdf as attachement under work order on click of "Generate FSR" button. 
****************************************************************************/

global class SR_Class_WorkOrderDetailPDF
{
    global SVMXC__Service_Order__c workorder{get;set;}
    global List<SVMXC__Service_Order_Line__c> Workdetails{get;set;}
    global string Language{get;set;}
    global string workorderid {get;set;}
    public List<selectoption> SelectContactOrDM{get;set;}
    public String OptionSelected{get;set;}
   /* public string getworkorderid(){
        system.debug('getter is working---->');
        return workorderid;
    }
    public void setworkorderid(string workorderid){
        this.workorderid = workorderid;
        system.debug('setter is working---->');
        methodcalledfromcontroller();
        system.debug('in setter value of work details id--->'+workorderid);
    }*/

    global SR_Class_WorkOrderDetailPDF(ApexPages.StandardController controller) {
        workorderid = apexpages.currentpage().getparameters().get('id');
        SelectContactOrDM = new list<selectoption>();
        SelectContactOrDM.add(new selectoption('Contact','Contact'));
        SelectContactOrDM.add(new selectoption('District Manager','District Manager'));
        if(workorderid!=null){
            methodcalledfromcontroller();
        }
    }
   global SR_Class_WorkOrderDetailPDF() {
        workorderid = apexpages.currentpage().getparameters().get('id');
        if(workorderid!=null){
            methodcalledfromcontroller();
        }
    } 
    global void methodcalledfromcontroller(){
        Workdetails = new list<SVMXC__Service_Order_Line__c>();
        Language = 'en_US';
        system.debug('workorderid  is---->'+workorderid);

        workorder = [select Name, Service_Comment__c, SVMXC__Company__r.BillingStreet, SVMXC__Company__r.BillingCity, SVMXC__Company__r.BillingState,
                    SVMXC__Company__r.BillingCountry, SVMXC__Problem_Description__c, CreatedDate, SVMXC__Company__r.Owner.Name, SVMXC__Company__r.Owner.Email,
                    Additional_Email__c, SVMXC__Closed_On__c, SVMXC__Top_Level__r.SVMXC__Product_Name__c, SVMXC__Contact__r.Account.Prefered_Language__c,
                    SVMXC__Top_Level__r.Name, SVMXC__Contact__c, SVMXC__Contact__r.AccountId, SVMXC__Contact__r.Email, SVMXC__Case__r.CaseNumber,
                    SVMXC__Case__r.CreatedDate, SVMXC__Case__r.Reason, Purchase_Order_Number__c, SVMXC__Preferred_Technician__r.Name, SVMXC__Company__r.Name,
                    SVMXC__Component__r.Name, SVMXC__Component__r.SVMXC__Product_Name__c,Heater_Hours__c from SVMXC__Service_Order__c where Id = :workorderid];

        Workdetails  = [select SVMXC__Date_Requested__c, SVMXC__Start_Date_and_Time__c, SVMXC__End_Date_and_Time__c from SVMXC__Service_Order_Line__c
                        where SVMXC__Service_Order__c = :workorderid and SVMXC__Start_Date_and_Time__c != null and SVMXC__End_Date_and_Time__c != null];

        if(workorder.SVMXC__Contact__c!=null && workorder.SVMXC__Contact__r.AccountId != null){
            if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c != null){
                if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c.containsIgnoreCase('French')){
                    Language = 'fr';
                }
                if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c.containsIgnoreCase('Dutch')){
                    Language = 'nl_NL';
                }
                if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c.containsIgnoreCase('Italian')){
                    Language = 'it';
                }
                if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c.containsIgnoreCase('Portuguese')){
                    Language = 'pt_BR';
                }
                if(workorder.SVMXC__Contact__r.Account.Prefered_Language__c.containsIgnoreCase('Spanish')){
                    Language = 'es';
                }
            }
        }
        system.debug('----------->>>>>>language---->'+Language );
    }
    global pagereference generatepdf(){
        try{
            PageReference pdf = Page.SR_VF_WorkOrderDetailPDF;
            pdf.getparameters().put('id',ApexPages.currentPage().getParameters().get('id'));
            Blob body;
            if(Test.IsRunningTest()) // Added by Bushra to improve the test coverage on 13/11/2014
            {
                body = Blob.valueOf('UNIT.TEST');
            }
            else {
                // Take the PDF content
                body = pdf.getContent();
            }
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setContentType('application/pdf');
            attach.setFileName('FSR_Report.pdf');
            attach.setInline(false);
            attach.Body = body;
            string email;
            String[] toAddresses = new String[]{};

            if(OptionSelected =='Contact' && workorder.SVMXC__Contact__r.Email!=null){
                email = workorder.SVMXC__Contact__r.Email;
                toAddresses.add(email);
            }
            if(OptionSelected =='District Manager' &&workorder.SVMXC__Company__r.Owner.Email!=null ){
                email = workorder.SVMXC__Company__r.Owner.Email;
                toAddresses.add(email);
            } 

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setUseSignature(false);
            mail.setSubject('Field Service Report (PDF document attached)');
            system.debug('email additional are-->'+workorder.Additional_Email__c);

            if(workorder.Additional_Email__c!=null){
                string[] additionalemail = workorder.Additional_Email__c.split(',');
                for(string em:additionalemail){
                    toAddresses.add(em);
                }
            }
            system.debug('email------>'+email );
            mail.setToAddresses(toAddresses);
            string closeddate = '';

            if(workorder.SVMXC__Closed_On__c != null){
                closeddate = workorder.SVMXC__Closed_On__c.Year()+'/'+workorder.SVMXC__Closed_On__c.Month()+'/'+workorder.SVMXC__Closed_On__c.Day();
            }
            if(OptionSelected=='Contact'){
                if(workorder.SVMXC__Contact__r.Email != null){
                    mail.setHtmlBody('Dear Customer:<br/><br/>Find attached your copy of the <b> Field Service Report </b>associated to the recent work performed on your Varian Medical System.<br/><br/>System :' + workorder.SVMXC__Top_Level__r.Name + '<br/>Date of Service :'+closeddate+' <br/><br/>If you have any questions or comments about this report or the work performed, please contact your Local Field Service Representative or your Local District Service Manager.<br/><br/>Sincerely,<br/><br/><b><font color="blue">Varian Medical Systems</font></b>');
                }
                if(workorder.SVMXC__Contact__r.Email == null){
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Email address not found on contact.'));
                    return null;
                }
            }
            if(OptionSelected == 'District Manager'){
                if(workorder.SVMXC__Company__r.Owner.Email == null){
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the account on work order first.'));
                    return null;
                }
                if(workorder.SVMXC__Company__r.Owner.Email != null){
                    String mailbody = 'Dear :<b>'+workorder.SVMXC__Company__r.Owner.Name+'</b><br/><br/>Please find attached the Field Service Report You requested associated to the recent work performed.<br/><br/><br/><br/>Work Order :<b>'+workorder.Name+'</b><br/><br/>Ticket Description :<b>'+workorder.SVMXC__Problem_Description__c+'</b><br/><br/>Creation Date:<b>'+workorder.CreatedDate.Day()+'/'+workorder.CreatedDate.Month()+'/'+workorder.CreatedDate.Year()+'</b><br/><br/>Site Name:<b>'+workorder.SVMXC__Company__r.Name+'</b><br/><br/>Site Address:<b>'+workorder.SVMXC__Company__r.BillingStreet+','+workorder.SVMXC__Company__r.BillingCity+','+workorder.SVMXC__Company__r.BillingState+','+workorder.SVMXC__Company__r.BillingCountry+'</b><br/><br/>Site City/Address:<b>'+workorder.SVMXC__Company__r.BillingCity+'</b><br/><br/>Equipment : <b>'+workorder.SVMXC__Top_Level__r.Name+'</b><br/><br/>Notes:[log]:<b>'+workorder.Service_Comment__c+'</b>';
                    mailbody = mailbody.replaceall('null,','');
                    mailbody = mailbody.replaceall('null','');
                    mail.setHtmlBody(mailbody);
                }
            }
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
            //if(workorder.SVMXC__Contact__r.Email!=null)
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail});
            pagereference pg = new pagereference('/'+ApexPages.currentPage().getParameters().get('id'));
            return pg;
        }
        catch(exception e){
            system.debug('Exception occured-->'+e);
            pagereference pg = new pagereference('/'+ApexPages.currentPage().getParameters().get('id'));
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There are invalid email addresses in your additional email list. Please correct and then send the FSR report.'));
            return null;
        }
    }
    
    // Below method used to generate the PDF and attach the pdf under work order (US 1998)
    public pagereference GenerateFSR(){
        try{
            // Creating the instance of vf page which is being used to generate the FSR pdf.
            // Pass the current record id in the pdf to generate the FSR for current record.
            PageReference pdf = Page.SR_VF_WorkOrderDetailPDF;
            pdf.getparameters().put('id',ApexPages.currentPage().getParameters().get('id'));
            Blob body;
            if(Test.IsRunningTest()) // Added by Bushra to improve the test coverage on 13/11/2014
            {
                body = Blob.valueOf('UNIT.TEST');
            }else {
                // Take the PDF content
                body = pdf.getContent();
            }
            // creating the attachement.
            Attachment at = new attachment();
            at.parentid = ApexPages.currentPage().getParameters().get('id');// set the parent id as the current work order.
            at.body=(body);// set the body as FSR pdf
            at.Name = workorder.Name+'_FSR'+'.pdf';
            at.Isprivate=false;
            insert at;// inserting the attachement
            return (new pagereference('/'+ApexPages.currentPage().getParameters().get('id')));// return the user to work order detail page
        }
        // catch the exception.
        catch(exception e){
            system.debug('Error Generated ---->'+e);
            return null;
        }
    }
    // method ends here
}
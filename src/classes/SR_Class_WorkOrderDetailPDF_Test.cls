/**
* Author : Yogesh Dixit
* Date : 17th June 2016
* Test class for SR_Class_WorkOrderDetailPDF
* SR_Class_WorkOrderDetailPDF class is being used on SR_VF_WorkOrderDetailPDF and SR_WorkDetail_PDF_Email VF pages as extension
**/
@isTest
public class SR_Class_WorkOrderDetailPDF_Test
{
	static PageReference pref_WorkOrderDetailPDF, pref_WorkDetail_PDF_Email;
	static Contact objCont;
	static Country__c country;
	static SVMXC__Service_Order__c serviceOrder;
	static Case cs;
	static List<Account> accList;
	static Account account1, account2, account3, account4, account5;

	static
	{
		accList = new List<Account>();

		country = testUtils.createCountry('United States');
        country.Alternate_Name__c = 'US';
        country.Parts_Order_Approval_Level__c  = 10;
        insert country;

		account1 = testUtils.createAccount('Test1', 'AUSSA', country.Name, country.Id);
		account1.Prefered_Language__c = 'French';
        accList.add(account1);

        account2 = testUtils.createAccount('Test2', 'AUSSA', country.Name, country.Id);
		account2.Prefered_Language__c = 'Dutch';
        accList.add(account2);

        account3 = testUtils.createAccount('Test3', 'AUSSA', country.Name, country.Id);
		account3.Prefered_Language__c = 'Italian';
        accList.add(account3);

        account4 = testUtils.createAccount('Test4', 'AUSSA', country.Name, country.Id);
		account4.Prefered_Language__c = 'Spanish';
        accList.add(account4);

        account5 = testUtils.createAccount('Test5', 'AUSSA', country.Name, country.Id);
		account5.Prefered_Language__c = 'Portuguese';
        accList.add(account5);

        insert accList;

        objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = accList[0].Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
        insert objCont;

        cs = testUtils.createCase('Test subject');
        cs.Reason = 'Analysis';
        insert cs;

		serviceOrder = new SVMXC__Service_Order__c(
        SVMXC__Company__c = accList[0].id,
        SVMXC__Case__c = cs.id);
        serviceOrder.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        serviceOrder.Malfunction_Start__c = system.now().addDays(10);
        serviceOrder.SVMXC__Order_Status__c = 'Open';
        serviceOrder.SVMXC__Contact__c  =objCont.id;
        insert serviceOrder;
	}


	/**
	*	Test method to cover GenerateFSR
	**/
    @isTest
    public static void Test_GenerateFSR()
    {

    	pref_WorkOrderDetailPDF = Page.SR_VF_WorkOrderDetailPDF; 
		pref_WorkOrderDetailPDF.getParameters().put('id', serviceOrder.id);
		Test.setCurrentPage(pref_WorkOrderDetailPDF);
        ApexPages.StandardController stdControllerObj = new ApexPages.StandardController(serviceOrder);
        SR_Class_WorkOrderDetailPDF Obj_WorkOrderDetailPDF = new SR_Class_WorkOrderDetailPDF(stdControllerObj);

        Test.startTest();

        Obj_WorkOrderDetailPDF.GenerateFSR();

        Test.stopTest();
    }
    
    /**
    *	Test method to cover generatepdf
    **/
    @isTest
    public static void Test_generatepdf()
    {
    	pref_WorkDetail_PDF_Email = Page.SR_WorkDetail_PDF_Email; //SR_WorkDetail_PDF_Email
		pref_WorkDetail_PDF_Email.getParameters().put('id', serviceOrder.id);
		Test.setCurrentPage(pref_WorkDetail_PDF_Email);
        ApexPages.StandardController stdControllerObj = new ApexPages.StandardController(serviceOrder);
        SR_Class_WorkOrderDetailPDF Obj_WorkDetail_PDF_Email1 = new SR_Class_WorkOrderDetailPDF(stdControllerObj);
        Obj_WorkDetail_PDF_Email1.OptionSelected = 'Contact';

        SR_Class_WorkOrderDetailPDF Obj_WorkDetail_PDF_Email2 = new SR_Class_WorkOrderDetailPDF(stdControllerObj);
        Obj_WorkDetail_PDF_Email2.OptionSelected = 'District Manager';


        Test.startTest();

        Obj_WorkDetail_PDF_Email1.generatepdf();
        Obj_WorkDetail_PDF_Email2.generatepdf();
        Test.stopTest();
    }


    /**
    *	Test method to cover generatepdf (to cover country if else loop)
    **/
    @isTest
    public static void Test_generatepdf_coverCountriesIfElse()
    {
    	pref_WorkDetail_PDF_Email = Page.SR_WorkDetail_PDF_Email; //SR_WorkDetail_PDF_Email
		
        
        List<SR_Class_WorkOrderDetailPDF> listObj = new List<SR_Class_WorkOrderDetailPDF>();

        for(Account eachAcc : accList)
        {
        	serviceOrder.SVMXC__Company__c = eachAcc.Id;
        	update serviceOrder;

        	pref_WorkDetail_PDF_Email.getParameters().put('id', serviceOrder.id);
			Test.setCurrentPage(pref_WorkDetail_PDF_Email);
	        ApexPages.StandardController stdControllerObj = new ApexPages.StandardController(serviceOrder);	
        	SR_Class_WorkOrderDetailPDF Obj_WorkDetail_PDF_Email = new SR_Class_WorkOrderDetailPDF(stdControllerObj);
        	listObj.add(Obj_WorkDetail_PDF_Email);	
        }

        

        Test.startTest();
        for(SR_Class_WorkOrderDetailPDF eachObj : listObj)
        {
        	eachObj.generatepdf();
        }
        Test.stopTest();
    }
}
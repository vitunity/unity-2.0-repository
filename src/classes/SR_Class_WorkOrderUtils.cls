/***************************************************************************
Author: Amitkumar Katre
Created Date: 10-31-2017
Project/Story/Inc/Task : Work order class utils
Description: Work order class utils 
Change Log:
Dec-4-2017 - Chandramouli - STSK0013483 : Added the Name field in the location query
12-Nov-2017 - Nilesh -STSK0013232 - Added new field in SOQL
13-Dec-2017 - Divya Hargunani - STSK0013513 - Added inner query in SOQL
21-Feb-2018 - Divya Hargunani - STSK0013866 - Updated inner query in SOQL 
*************************************************************************************/
public class SR_Class_WorkOrderUtils {
  
    public static String convertMonthNameToNumber(String strMonthName){
        if(strMonthName == 'Jan')
        return '01';
        else if(strMonthName == 'Feb')
        return '02';
        else if(strMonthName == 'Mar')
        return '03';
        else if(strMonthName == 'Apr')
        return '04';
        else if(strMonthName == 'May')
        return '05';
        else if(strMonthName == 'Jun')
        return '06';
        else if(strMonthName == 'Jul')
        return '07';
        else if(strMonthName == 'Aug')
        return '08';
        else if(strMonthName == 'Sep')
        return '09';
        else if(strMonthName == 'Oct')
        return '10';
        else if(strMonthName == 'Nov')
        return '11';
        else if(strMonthName == 'Dec')
        return '12';
        else
        return '01';
    } 

    
    /*Merged from Prod september release*/
    /************************SLA Term  from work Order*****************/
    public static void gtSLATerm(set<Id> stSLATermIds)
    {
        if (!SR_Class_WorkOrder.mapWorkOrder_SlaTerm.keySet().containsAll(stSLATermIds))
        for (SVMXC__Service_Level__c slaterm : [select Id,(select Id,SVMXC__Available_Services__c,SVMXC__Available_Services__r.Name from SVMXC__SLA_Detail__r where SVMXC__Available_Services__c!=null) from SVMXC__Service_Level__c where Id=:stSLATermIds and SVMXC__Active__c = true]) // added active check on 13-02-2015 By Bushra for de3515
        SR_Class_WorkOrder.mapWorkOrder_SlaTerm.put(slaterm.id,slaterm); //just add to the map if new id's
    }
    
    /************************Account  from work Order*****************/
    public static void gtAccount(set<Id> stActIds)
    {
        // STSK0013232 - Added "District_Sales_Manager__c" and "District_Service_Manager__c" fields in below query
        if(!SR_Class_WorkOrder.mapAccount_WorkOrder.keySet().containsAll(stActIds))
        for (Account acct :  [Select ID, Name, Country__c, ERP_Timezone__c, Timezone__r.Name, Timezone__r.Salesforce_timezone__c, PO_Required__c,
        BillingCity, BillingState, BillingCountry,Owner.Email, District_Sales_Manager__c, District_Service_Manager__c,
        (select ID, SVMXC__Account__c, ERP_Functional_Location__c from SVMXC__Sites__r ) 
        from Account where Id IN : stActIds])
        {
            SR_Class_WorkOrder.mapAccount_WorkOrder.put (acct.id,acct);
        }
    }
    
    public static void gtTechEqt(set<Id> stTechEqt)
    {
        if (!SR_Class_WorkOrder.mapTechEquipment.keySet().containsAll(stTechEqt))
        for(SVMXC__Service_Group_Members__c gm : [ SELECT ID, name, RecordTypeID, SVMXC__Email__c,  SVMXC__Working_Hours__c, Technician_Status__c,
        Dispatch_Queue__c, SVMXC__Inventory_Location__c, OOC__c, Current_End_Date__c,
        Country__c, Country__r.Name,
        User__c,User__r.Email, User__r.Badge_Number__c, SVMXC__Salesforce_User__c,SVMXC__Salesforce_User__r.Email, SVMXC__Salesforce_User__r.Badge_Number__c,
        SVMXC__Service_Group__c,SVMXC__Service_Group__r.FOA__c,SVMXC__Service_Group__r.Work_Order_BST_Queue__c, SVMXC__Service_Group__r.District_Manager__c,SVMXC__Service_Group__r.Work_Order_Review_Queue__c,
        SVMXC__Service_Group__r.Review_Required__c,
        (Select Id, RecordTypeId, Plant__c,RecordType.DeveloperName From Locations__r where RecordType.DeveloperName = 'Standard_Location')
        from SVMXC__Service_Group_Members__c where  ID in : stTechEqt])
        
        {
            SR_Class_WorkOrder.mapTechnicianID_UserID.put(gm.id,gm.user__c);  
            SR_Class_WorkOrder.mapTechEquipment.put(gm.id,gm);  
        }
    }
    
    public static void gtServiceTeam(set<id>TeamIds)
    {
        
        if (TeamIds.size()>0) {
            
            if (!SR_Class_WorkOrder.mapServiceTeam.keySet().containsAll(TeamIds))
            for (SVMXC__Service_Group__c st : [select id, Work_Order_BST_Queue__c, FOA__c, Review_Required__c, District_Manager__c, Work_Order_Review_Queue__c from SVMXC__Service_Group__c where id in :TeamIds]) {
                if (st.Work_Order_Review_Queue__c != null) SR_Class_WorkOrder.WorkOrderQueueNames.add(st.Work_Order_Review_Queue__c); //DE7468
                if (st.Work_Order_BST_Queue__c != null) SR_Class_WorkOrder.WorkOrderQueueNames.add(st.Work_Order_BST_Queue__c); //DE7468
                SR_Class_WorkOrder.mapServiceTeam.put(st.id,st);
            }
        }
    }
    
    public static void gtEvents(set<id>woIds,set<id>techIds)
    {
        if(woIds.size() > 0) 
        {
            if (!SR_Class_WorkOrder.WOtoSFEvents.keySet().containsAll(woIds))
            for (SVMXC__SVMX_Event__c svmxEvent : [Select Id, SVMXC__StartDateTime__c, SVMXC__WhatId__c, SVMXC__WhoId__c,
            SVMXC__DurationInMinutes__c, SVMXC__Technician__c, SVMXC__Service_Order__c,
            (Select Id from Events where whatId in: woIds ) 
            from SVMXC__SVMX_Event__c where SVMXC__Service_Order__c in: woIds and SVMXC__Technician__c in :techids and SVMXC__EndDateTime__c > Last_N_Days:30]) // should be kept in sync with mobile client's period of days in the past to hold
            {
                for(Event ev : svmxEvent.Events)
                {
                    if(SR_Class_WorkOrder.WOToSFEvents.get(ev.whatId) == null)
                    {
                        SR_Class_WorkOrder.WOToSFEvents.put(ev.whatId, new List<Event>());
                    }
                    SR_Class_WorkOrder.WOToSFEvents.get(ev.whatId).add(ev);
                }
                SR_Class_WorkOrder.mapAllSVMXEvent.put(svmxEvent.id,svmxEvent); //TODO: publicInsertSVMXEvent need to add to this map after insert so it has an idea
            }//By HARVEY Jan 23
        }

    }
    
    public static Map<string,Integer> getMapMonthNumber(){
        Map<string,Integer> mapMonthNumber = new Map<string,Integer>();
        mapMonthNumber.put('Jan',1);
        mapMonthNumber.put('Feb',2);
        mapMonthNumber.put('Mar',3);
        mapMonthNumber.put('Apr',4);
        mapMonthNumber.put('May',5);
        mapMonthNumber.put('Jun',6);
        mapMonthNumber.put('Jul',7);
        mapMonthNumber.put('Aug',8);
        mapMonthNumber.put('Sep',9);
        mapMonthNumber.put('Oct',10);
        mapMonthNumber.put('Nov',11);
        mapMonthNumber.put('Dec',12);
        return mapMonthNumber;
    }
    
    /************************ERP Location  from work Order*****************/
    public static void gtLocationFromWO(set<Id> locationIds, set<string> ERPFunctionalLocation) // This Method is used to get all the Cases related to WO
    {
        if (!SR_Class_WorkOrder.mapOfLocation.keyset().containsAll(locationIds))
        //Chandra : Dec 4 2017 : STSK0013483 : Added the Name field in the below query
        for (SVMXC__Site__c l :  [Select Id,Name, SVMXC__Country__c, SVMXC__Street__c,SVMXC__Zip__c,SVMXC__State__c, ERP_Functional_Location__c, ERP_Site_Partner_Code__c, SVMXC__Account__r.Prefered_Language__c from SVMXC__Site__c where ERP_Functional_Location__c in: ERPFunctionalLocation or id in: locationIds])
        {
            SR_Class_WorkOrder.mapOfLocation.put(l.id,l);
            SR_Class_WorkOrder.mapWorkOrder_ERPLocation.put(l.ERP_Functional_Location__c,l);
        }   
        
    }
    
    /************************Installed product From work Order*****************/
    //TODO: Don't mix Id's and String:  Pass in two arguments: one for by id and another for by name/reference
    public static void gtInstalledProductFromWO(set<Id> stInstalledProdIds,Set<String>stInstalledProductRefs){
        system.debug(' ==== stInstalledProdIds ==== ' + stInstalledProdIds);
                system.debug(' ==== stInstalledProductRefs ==== ' + stInstalledProductRefs);
        //  system.assertEquals(1,2);      
        if(stInstalledProdIds.size() > 0 || stInstalledProductRefs.size()>0 ){
            if (!SR_Class_WorkOrder.mapWorkOrder_InstalledProduct.keySet().containsAll(stInstalledProdIds) 
                    || !SR_Class_WorkOrder.mapInstalledProductReference2Id.keySet().containsAll(stInstalledProductRefs)){
                for (SVMXC__Installed_Product__c ip : [select Id, Name, ERP_Reference__c, ERP_Sales__c,  Last_Acceptance_date__c,
                SVMXC__Preferred_Technician__c, SVMXC__Preferred_Technician__r.User__c,  SVMXC__Preferred_Technician__r.Dispatch_Queue__c,
                SVMXC__Service_Contract__r.SVMXC__Service_Level__c, PCSN__c,
                SVMXC__Product__r.Includes_Software__c, SVMXC__Product__r.Skills_Required__c, SVMXC__Product__r.SVMXC__Product_Line__c,SVMXC__Product__r.name, SVMXC__Product__r.ERP_Pcode__c,
                SVMXC__Site__c, SVMXC__Site__r.ERP_Org__c,  SVMXC__Site__r.ERP_Org__r.Received_Date_Required__c, SVMXC__Site__r.ERP_Org__r.Part_Order_Value_Limit__c,
                SVMXC__Site__r.ERP_Org__r.Sales_Org__c, SVMXC__Site__r.ERP_Org__r.Approval_time_limit__c, SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c,
                SVMXC__Site__r.plant__c, SVMXC__Company__c, SVMXC__Site__r.Country__c,SVMXC__Site__r.SVMXC__State__c,SVMXC__Site__r.SVMXC__City__c,SVMXC__Site__r.Name,SVMXC__Site__r.SVMXC__Country__c,
                SVMXC__Site__r.ERP_Org__r.Parts_Order_Approval_Level__c, SVMXC__Site__r.Sales_Org__c, 
                Service_Team__c, 
                Service_Team__r.Automatic_Approval_Enabled__c, 
                Service_Team__r.FOA__c, 
                Service_Team__r.District_Manager__c, 
                Service_Team__r.SVMXC__Group_Code__c, SVMXC__Status__c,
                Service_Team__r.Work_Order_Review_Queue__c,
                Service_Team__r.District_Manager__r.Email,
                Service_Team__r.Work_Order_BST_Queue__c, 
                Service_Team__r.Dispatch_Queue__c,
                SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.ERP_Contract_Type__c,
                SVMXC__Top_Level__c, SVMXC__Top_Level__r.SVMXC__Service_Contract__c,SVMXC__Top_Level__r.SVMXC__Service_Contract__r.ERP_Contract_Type__c,
                Service_Team__r.SVMXC__Email__c,
                SVMXC__Site__r.FE_Service_Team__r.SVMXC__Email__c,
                SVMXC__Site__r.FE_Service_Team__r.District_Manager__c,
                SVMXC__Site__r.FE_Service_Team__r.SVMXC__Group_Code__c,
                SVMXC__Site__r.FE_Service_Team__r.District_Manager__r.Email,
                SVMXC__Site__r.FE_Service_Team__r.work_Order_Review_Queue__c,
                SVMXC__Site__r.FE_Service_Team__r.FOA__c,
                SVMXC__Site__r.FE_Service_Team__r.work_Order_BST_Queue__c,
                SVMXC__Site__r.FE_Service_Team__r.Dispatch_Queue__c,
                Service_Team__r.SVMXC__Group_Type__c,SVMXC__Site__r.ERP_Org__r.Default_Depot_Location__c
                from SVMXC__Installed_Product__c 
                where ID in : stInstalledProdIds OR  ERP_Reference__c in :stInstalledProductRefs OR Name in :stInstalledProductRefs])
                {
                    SR_Class_WorkOrder.mapInstalledProductReference2Id.put(ip.name,ip.id);
                    SR_Class_WorkOrder.mapInstalledProductReference2Id.put(ip.ERP_Reference__c,ip.id);
                    SR_Class_WorkOrder.mapServiceTeam.put(ip.Service_Team__c,ip.Service_Team__r);
                    SR_Class_WorkOrder.mapWorkOrder_InstalledProduct.put(ip.id,ip);
                }       
            }
            
        }        
    }
    
    /************************Work Detail based on Work Order*****************/
    // This Method is used to get all the Cases related to WO
    public static void gtWorkDetailFromWOId(set<Id> stWOids,Set<id>stWOtrnIds) {   
        if(stWOids.size() > 0)
        {
            //IMPORTANT: if you update the list of fields for one query you need to do it for both queries (at least for now) //REFACTOR into dynamic query to avoid having to update in two places
            List<SVMXC__Service_Order_Line__c> listChildWorkDetail = new List<SVMXC__Service_Order_Line__c>();
            if (!SR_Class_WorkOrder.mapAllWorkDetails.keySet().containsAll(stWOids)) {
                if (stWOtrnIds.size()> 0)
                //savon.FF 12-23-2015 DE6248 Added Parts_Order_Line__r.SVMXC__Sales_Order_Number__c and Parts_Order_Line__r.ERP_Item_Number__c to query
                //savon.FF 1-7-2016 Added Billing_Review__c to query
                //savon.FF 1-11-2016 DE6960 Added Parts_Order_Line__r.Remaining_Qty__c,Parts_Order_Line__r.Product_Cost__c to query 
                //CM : DFCT0012421 : Modified the Query to add the field SVMXC__Group_Member__r.Technicians_Org__r.PM_No_Cross_Bill__c
                listChildWorkDetail = [select id, Name, SVMXC__Service_Order__c, SVMXC__Line_Type__c, Parts_Order_Line__r.SVMXC__Sales_Order_Number__c, Test_Equipment_Expired_Explanation__c,
                SVMXC__Start_Date_and_Time__c, SVMXC__Is_Billable__c, Parts_Order_Line__r.ERP_Item_Number__c,Part_Disposition__c, SVMXC__Product__c, Equipment_Location__c,
                Covered_or_Not_Covered__c, waived__c, SVMXC__End_Date_and_Time__c, RecordTypeID, Billing_Review__c,
                SVMXC__Group_Member__c,SVMXC__Group_Member__r.RecordTypeID, SVMXC__Group_Member__r.Current_End_Date__c, 
                Order_Type__c, Order_Sub_Type__c, STB_Master__c, Mod_Status__c, Recordtype.Name,Product_Stock__c,
                SVMXC__Serial_Number__c, Work_Order_RecordType__c, SVMXC__Line_Status__c, Part_Returnable__c, Free_Issue__c, Return_All__c,
                Interface_Status__c, SVMXC__Service_Order__r.SVMXC__Order_Status__c, ERP_PCSN__c,
                SVMXC__Actual_Quantity2__c, Related_Activity__c, Billing_Type__c, Billing_Review_Requested__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__c,
                SVMXC__Work_Detail__c, Parts_Order_Line__c, ERP_Serial_Number__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c, 
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__c, SVMXC__Product__r.Returnable__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__c, Removal_Condition__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.Sales_Org__c, 
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Sales_Org__c,
                End_Date_Date__c, SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Top_Level__c,
                Parts_Order_Line__r.ERP_Serial_Number__c, SVMXC__Posted_To_Inventory__c,
                SVMXC__Service_Order__r.ERP_Org_lookup__r.PM_No_Cross_Bill__c,
                SVMXC__Service_Order__r.SVMXC__Group_Member__r.Default_Plant__c,
                SVMXC__Group_Member__r.Technicians_Org__r.PM_No_Cross_Bill__c,
                SVMXC__Service_Order__r.ERP_Org_lookup__r.Sales_Org__c, Review_Category__c,
                SVMXC__Product__r.Enable_Configuration_Tracking__c, Sales_Order_Item__c,  
                Sales_Order_Item__r.Sales_Order__c, Sales_Order_Item__r.Sales_Order__r.Name,
                SVMXC__Consumed_From_Location__c, Related_Activity__r.SVMXC__End_Date_and_Time__c, Parts_Order_Line__r.Remaining_Qty__c, Parts_Order_Line__r.Product_Cost__c, 
                SVMXC__Consumed_From_Location__r.SVMXC__Stocking_Location__c, Completed__c, Test_Equipment_Expired__c,
                Parts_Order_Line__r.SVMXC__Actual_Quantity2__c,SVMXC__Service_Order__r.WD_count__c,SVMXC__Service_Order__r.recordTypeID,
                SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c,
                SVMXC__Service_Order__r.Submitted_WD_Count__c,recordtype.developername, SVMXC__Product__r.SVMXC__Product_Cost__c, 
                (select Counter_Detail__r.Actual__c, Counter_Detail__r.recordTypeId from Counter_Usage__r),
                SVMXC__Service_Order__r.Source_of_Part__c
                from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c IN : stWOtrnIds  and SVMXC__Activity_Type__c != 'Created in error' order by SVMXC__End_Date_and_Time__c desc ];
                else
                listChildWorkDetail = [select id, Name, SVMXC__Service_Order__c, SVMXC__Line_Type__c, Parts_Order_Line__r.SVMXC__Sales_Order_Number__c, Test_Equipment_Expired_Explanation__c,
                SVMXC__Start_Date_and_Time__c, SVMXC__Is_Billable__c, Parts_Order_Line__r.ERP_Item_Number__c,Part_Disposition__c, SVMXC__Product__c, Equipment_Location__c,
                Covered_or_Not_Covered__c, waived__c, SVMXC__End_Date_and_Time__c, RecordTypeID, Billing_Review__c,
                SVMXC__Group_Member__c,SVMXC__Group_Member__r.RecordTypeID, SVMXC__Group_Member__r.Current_End_Date__c, 
                Order_Type__c, Order_Sub_Type__c, STB_Master__c, Mod_Status__c, Recordtype.Name,Product_Stock__c,
                SVMXC__Serial_Number__c, Work_Order_RecordType__c, SVMXC__Line_Status__c, Part_Returnable__c, Free_Issue__c, Return_All__c,
                Interface_Status__c, SVMXC__Service_Order__r.SVMXC__Order_Status__c, ERP_PCSN__c, Source_of_Parts__c,
                SVMXC__Actual_Quantity2__c, Related_Activity__c, Billing_Type__c, Billing_Review_Requested__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__c,
                SVMXC__Work_Detail__c, Parts_Order_Line__c, ERP_Serial_Number__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c, 
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__c, SVMXC__Product__r.Returnable__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__c, Removal_Condition__c,
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.Sales_Org__c, 
                SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Sales_Org__c,
                SVMXC__Group_Member__r.Technicians_Org__r.PM_No_Cross_Bill__c,
                End_Date_Date__c, SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Top_Level__c,
                Parts_Order_Line__r.ERP_Serial_Number__c, SVMXC__Posted_To_Inventory__c,
                SVMXC__Service_Order__r.ERP_Org_lookup__r.PM_No_Cross_Bill__c,
                SVMXC__Service_Order__r.SVMXC__Group_Member__r.Default_Plant__c,
                SVMXC__Service_Order__r.ERP_Org_lookup__r.Sales_Org__c, Review_Category__c,
                SVMXC__Product__r.Enable_Configuration_Tracking__c, Sales_Order_Item__c,  
                Sales_Order_Item__r.Sales_Order__c, Sales_Order_Item__r.Sales_Order__r.Name,
                SVMXC__Consumed_From_Location__c, Related_Activity__r.SVMXC__End_Date_and_Time__c, Parts_Order_Line__r.Remaining_Qty__c, Parts_Order_Line__r.Product_Cost__c, 
                SVMXC__Consumed_From_Location__r.SVMXC__Stocking_Location__c, Completed__c, Test_Equipment_Expired__c,
                Parts_Order_Line__r.SVMXC__Actual_Quantity2__c,SVMXC__Service_Order__r.WD_count__c,SVMXC__Service_Order__r.recordTypeID,
                SVMXC__Service_Order__r.Submitted_WD_Count__c,recordtype.developername, SVMXC__Product__r.SVMXC__Product_Cost__c,
                SVMXC__Service_Order__r.Source_of_Part__c,
                SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c,
                SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__c,
                Batch_Number__c
                from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c IN : stWOids  and SVMXC__Activity_Type__c != 'Created in error' order by SVMXC__End_Date_and_Time__c desc ];   
            }                                   
            if(listChildWorkDetail.size() > 0)
            {
                for(SVMXC__Service_Order_Line__c varWD : listChildWorkDetail)
                {
                    //system.debug(varWD.Billing_Review__c);
                    //system.debug(varWD.Review_Category__c);
                    map<id,SVMXC__Service_Order_Line__c> tempMapWD = new map<id,SVMXC__Service_Order_Line__c>();
                    SR_Class_WorkOrder.mapAllWorkDetails.put(varWD.id,varWD);  // cached values to be updated when updates are made
                    if(SR_Class_WorkOrder.mapChildWorkDetail.containsKey(varWD.SVMXC__Service_Order__c))
                    {
                        tempMapWD = SR_Class_WorkOrder.mapChildWorkDetail.get(varWD.SVMXC__Service_Order__c);
                    }
                    
                    tempmapWD.put(varWD.id,varWD);
                    SR_Class_WorkOrder.mapChildWorkDetail.put(varWD.SVMXC__Service_Order__c, tempMapWD);
                    
                    if(SR_Class_WorkOrder.WOmapClose.containskey(varWD.SVMXC__Service_Order__c))
                    {
                        if(SR_Class_WorkOrder.WOmapClose.get(varWD.SVMXC__Service_Order__c) 
                           && varWD.recordtype.developername == 'UsageConsumption' && varWD.SVMXC__Line_Status__c != 'Closed')
                        {
                            SR_Class_WorkOrder.WOmapClose.put(varWD.SVMXC__Service_Order__c, false);
                        }
                    }else{
                        if(varWD.recordtype.developername == 'UsageConsumption' && varWD.SVMXC__Line_Status__c == 'Closed')
                        {
                            SR_Class_WorkOrder.WOmapClose.put(varWD.SVMXC__Service_Order__c, true);
                        }
                        if(varWD.recordtype.developername == 'UsageConsumption' && varWD.SVMXC__Line_Status__c != 'Closed')
                        {
                            SR_Class_WorkOrder.WOmapClose.put(varWD.SVMXC__Service_Order__c, false);
                        }
                    }
                    if (!SR_Class_WorkOrder.WOmapBonded.containskey(varWD.SVMXC__Service_Order__c))
                    {
                        //DFCT0011612 added by amit.
                        //code update INC4749771 because we removed PS line for field service WO.
                        //STRY0045968 - Parts:  December Release Fix - ZHS Automation for Brazil
                        //removed PS line work detail check
                        if(varWD.SVMXC__Work_Detail__c != null  
                           && varWD.SVMXC__Work_Detail__r.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c
                           && varWD.SVMXC__Line_Type__c == 'Parts' 
                           && varWD.SVMXC__Service_Order__r.Source_of_Part__c != null 
                           && varWD.SVMXC__Service_Order__r.Source_of_Part__c.substring(0,1) == 'V'){
                               SR_Class_WorkOrder.WOmapBonded.put(varWD.SVMXC__Service_Order__c,true);
                        }else if(varWD.SVMXC__Serial_Number__r.SVMXC__Site__r.ERP_Org__r.Bonded_Warehouse_used__c
                           && varWD.SVMXC__Line_Type__c == 'Parts' 
                           && varWD.SVMXC__Service_Order__r.Source_of_Part__c != null 
                           && varWD.SVMXC__Service_Order__r.Source_of_Part__c.substring(0,1) == 'V'){
                               SR_Class_WorkOrder.WOmapBonded.put(varWD.SVMXC__Service_Order__c,true);
                        }
                        
                    }
                }
            }
        }
    }
    
    /************************Time Entry Matrixes ******************************/
    public static void getTimeEntryMatrixes(Set<Id> stWOids){
        if(stWOids.size() > 0){
            List <Time_Entry_Matrix__c> tems = new List <Time_Entry_Matrix__c>();
            if (!SR_Class_WorkOrder.mapChildTimeMatrix.keySet().containsAll(stWOids))
            tems = [SELECT Id,Status__c,Work_Order__c
            FROM Time_Entry_Matrix__c
            WHERE Work_Order__c IN :stWOids limit 20000];

            if(tems.size() > 0)
            {
                for(Time_Entry_Matrix__c tem : tems)
                {
                    list<Time_Entry_Matrix__c> tempList = new List<Time_Entry_Matrix__c>();
                    
                    if(SR_Class_WorkOrder.mapChildTimeMatrix.containsKey(tem.Work_Order__c))
                    {
                        tempList = SR_Class_WorkOrder.mapChildTimeMatrix.get(tem.Work_Order__c);
                    }
                    
                    tempList.add(tem);
                    SR_Class_WorkOrder.mapChildTimeMatrix.put(tem.Work_Order__c, tempList);
                }
            }                                                
        }
    }

    /************************Cases from work Order*****************/
    // This Method is used to get all the Cases related to WO
    //STSK0013513 - Divya Hargunani - Added inner query in SOQL
    public static void gtCaseFromWO(set<ID> stCaseids) {
        if(stCaseids.size() > 0){
            if (!SR_Class_WorkOrder.mapAllCase.keySet().containsAll(stCaseIds))
            SR_Class_WorkOrder.mapAllCase = new Map<Id, Case>([select id, CaseNumber, Malfunction_Start__c, subject, WO_Last_Close_Date__c,
            description, Internal_Comments__c, AccountId, WO_Last_Restoration_Time__c,
            Total_Downtime_Hrs__c, Reason, Assigned_Future_WorkOrder__c, Case_Activity__c, 
            All_WO_closed__c, WO_Last_Acceptance_Date__c, Priority,
            recordTypeID, Not_Varian_s_Fault__c, SVMXC__Billing_Type__c, P_O_Number__c, 
            Case_Preferred_Technician__c,Type, Local_Subject__c, 
            Customer_Preferred_End_Date_time__c,Customer_Preferred_Start_Date_time__c, OwnerId,
            Customer_Malfunction_End_Date_Time_str__c, 
            Status, ProductSystem__r.Service_team__r.District_Manager__r.Id, ProductSystem__c, 
            Last_Machine_Release__c, SVMXC__Top_Level__c, 
            Earliest_Requested_Start__c,CreatedDate, Site_Country_Code__c, 
            Customer_Malfunction_Start_Date_time__c, Customer_Requested_End_Date_Time__c, 
            ProductSystem__r.Service_team__r.District_Manager__r.isActive,
            RecordType.Name,
            // (SELECT Id, Is_Submitted__c FROM L2848__r where Is_Submitted__c != 'Rejected by Regulatory'  AND Is_Submitted__c != 'Accepted & Closed by Regulatory')
            (SELECT Id, Is_Submitted__c FROM L2848__r where Is_Submitted__c = 'Not yet' )
            from Case where id IN : stCaseids]);
        }
    }
    
    
    /*
     * Update malfunction date form work order to workdetail line if malfunction date get changed on work order
     */
    public static void updateMalFunDtToWL(list<SVMXC__Service_Order__c> woList , map<Id, SVMXC__Service_Order__c> woOldMap){
        set<Id> woIds = new set<Id>();
        for(SVMXC__Service_Order__c wo : woList) {
            if(woOldMap.get(wo.Id).Malfunction_Start__c != wo.Malfunction_Start__c) {
                woIds.add(wo.id);
            }     
        }
        if(!woIds.isEmpty()) {
            list<SVMXC__Service_Order_Line__c> wdlList = [select Id, SVMXC__Service_Order__r.Malfunction_Start__c  from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in : woIds];
            for(SVMXC__Service_Order_Line__c wdl : wdlList){
                wdl.Malfunction_Start_Date__c = wdl.SVMXC__Service_Order__r.Malfunction_Start__c;
            }
            if(!wdlList.isEmpty()){
                update wdlList;
            }
        }
    }
    
    /*
     * Update case work order resolution to investigation completed
     */
    public static void updateCaseWorkOrderResolution(list<SVMXC__Service_Order__c> woList , map<Id, SVMXC__Service_Order__c> woOldMap){
        set<Id> woIds = new set<Id>();
        for(SVMXC__Service_Order__c wo : woList) {
            if(woOldMap.get(wo.Id).SVMXC__Order_Status__c != wo.SVMXC__Order_Status__c &&
               wo.SVMXC__Order_Status__c == 'Closed' &&
               wo.SVMXC__Purpose_of_Visit__c == 'Out of Tolerance') {
                woIds.add(wo.id);
            }     
        }
        if(!woIds.isEmpty()) {
            list<Case_Work_Order__c> cwoList = [select Id from Case_Work_Order__c where Follow_Up_Work_Order__c in : woIds];
            for(Case_Work_Order__c cwo : cwoList){
                cwo.Follow_Work_Order_Complete__c = true;
            }
            if(!cwoList.isEmpty()){
                update cwoList;
            }
        }
    }
}
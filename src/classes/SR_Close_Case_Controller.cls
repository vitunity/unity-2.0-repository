/**************************************************************************\
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Nov-27-2017 : Chandra : STSK0012982 : Added new logic for HD Cases where reason is "Installation Assistance"
03-Oct-2017 - Rakesh - STSK0012993 - Added case type, classification 1, classfication 2 picklist fields and made required.
26-Oct-2017 - Rakesh - STSK0012993 - Updated error message on Closed Case Reason field validation. 
22-Dec-2017 - Divya Hargunani - STSK0013527 - Do not allow "case closure code created in error" if there is a case line
08-Jan-2018 - Rakesh - STSK0013571 - Closure is blocked if Escalation Required? = Yes, and Case ECF Count is 0.
12-Jan-2018 - Divya Hargunani - STSK0013616 - STRY0040874 : Create two new article templates (types) - added property displayArticleType
15-Mar-2018 - Rakesh - STRY0053307 - Update case type value from Technical to Hardware / Get current case values for Case type and classification fields.
12-Apr-2018 - Rakesh - STSK0013845 - No validation rules fires when closed case reason is 'Created in Error' 
/**************************************************************************/
public class SR_Close_Case_Controller {
   public Boolean draftarticle;
   public String tabid{get;set;}
   public Case currentcase {get;set;}
   public string draftstr{get;set;}
   public boolean flag{get; set;}
   public Integer caseLinesPresents {get;set;}
   public String displayArticleType{get; set;}
    
    public list<SelectOption> getArticleTypesOptions(){
        map<String,String> aTypes = RecordTypeUtility.getInstance().rtMaps.get('Service_Article__kav');
        list<SelectOption> options = new list<SelectOption>();
        options.add(new SelectOption(aTypes.get('Solution'),'Solution'));
        options.add(new SelectOption(aTypes.get('How_To'),'How To'));
        options.add(new SelectOption(aTypes.get('Q_A'),'Q&A'));
        return options;
    } 

   public SR_Close_Case_Controller(ApexPages.StandardController controller) {
        case cCase = [select Id,OwnerId from Case where Id =: ApexPages.currentPage().getParameters().get('id')];
        List<User> cUsers = [select Case_Group_Assignment__c from User where Id=: cCase.OwnerId];
        currentcase = new case();
        tabid = ApexPages.currentPage().getParameters().get('tabid');
        String caseid = controller.getId();
        currentcase = [Select id,Local_Case_Activity__c,Reason,Closed_Case_Reason__c,Case_Type__c,Classification_1__c,Classification_2__c,Classification_3__c,Status,Internal_Comments__c,
                       (select Id from SVMXC__Case_Lines__r )from Case where id =: caseid];
        caseLinesPresents = currentcase.SVMXC__Case_Lines__r.size();
        //currentcase.Case_Type__c = null;
        //Nov-27-2017 : Chandra : STSK0012982 : Added new logic for HD Cases where reason is "Installation Assistance"
        if(currentcase.Reason == 'Installation Assistance') {
            currentcase.Case_Type__c = 'Hardware';
            currentcase.Classification_1__c = 'N/A';
            currentcase.Classification_2__c = 'N/A';
        }
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'-->'+cUser.Case_Group_Assignment__c ));
        if(cUsers.size() > 0 && cUsers[0].Case_Group_Assignment__c <> null){
            if(cUsers[0].Case_Group_Assignment__c.startswith('CHD'))currentcase.Case_Type__c = 'Clinical';
            if(cUsers[0].Case_Group_Assignment__c.startswith('THD OIS'))currentcase.Case_Type__c = 'Software';
            if(cUsers[0].Case_Group_Assignment__c.startswith('THD DEL'))currentcase.Case_Type__c = 'Hardware';
        }
        draftstr = 'false';
        draftarticle = false;
        ApexPages.currentPage().getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
        ApexPages.currentPage().getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 
    }
    public Boolean getdraftarticle(){
        return draftarticle;
    }
    public void setdraftarticle(boolean str)
    {
      this.draftarticle = str;  
      if(draftarticle)
            draftstr = 'true';
        else
            draftstr = 'false';
      system.debug('AWDAEDAQED' + draftstr);
    }
    public void onCaseTypeChange(){
        if(currentcase.Case_Type__c <> null && currentcase.Case_Type__c == 'Hardware'){
            currentcase.Classification_1__c = 'N/A';
            currentcase.Classification_2__c = 'N/A';
        }
    }
    public boolean isSuccess{get;set;}
    public pagereference yes(){
        isSuccess = false;
        Case updatecase = [SELECT Id,Local_Case_Activity__c, Closed_Case_Reason__c,Reason,Description,Status,Is_escalation_to_the_CLT_required__c, Is_This_a_Complaint__c,Case_Type__c,Classification_1__c,Classification_2__c,Classification_3__c,
                           (SELECT Id, Name, Is_Submit__c FROM L2848__r),
                           (select Id from SVMXC__Case_Lines__r )
                           FROM Case WHERE Id = :currentcase.id]; //Case_ECF_Count__c,Case_ECF_Submitted__c,
        system.debug('Case selected is: '+updatecase);
        flag = true;
        /*Divya Hargunani - commented code for STSK0013616
        if(draftarticle == true && String.isBlank(displayArticleType)){ ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select article type.')); flag = false; }*/
        
        if(currentcase.Closed_Case_Reason__c == 'Created in Error' ){ //STSK0013845 
            flag = true;        
        }else{
            if(currentcase.Local_Case_Activity__c == null && updatecase.Status != 'Closed by Work Order'){  //currentcase.Closed_Case_Reason__c != 'Customer Cancelled'  &&
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a recommendation.');
                ApexPages.addMessage(myMsg);
                flag = false;
            }
             
                if(currentcase.Closed_Case_Reason__c != 'Closed as Duplicate' && currentcase.Case_Type__c == null ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select case type.'));
                    flag = false;
                }
            
            if( currentcase.Closed_Case_Reason__c != 'Closed as Duplicate' && currentcase.Case_Type__c == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select case type.'));
                flag = false;
            }
            if(currentcase.Case_Type__c <> null && currentcase.Closed_Case_Reason__c != 'Closed as Duplicate' && currentcase.Classification_1__c == null ) //&& currentcase.Case_Type__c <> 'Hardware'
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Classification 1.'));
                flag = false;
            }
            if(currentcase.Classification_1__c <> null && currentcase.Closed_Case_Reason__c != 'Closed as Duplicate' && currentcase.Classification_2__c == null ) //&& currentcase.Case_Type__c <> 'Hardware'
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Classification 2.'));
                flag = false;
            }
            if(currentcase.Closed_Case_Reason__c == null )
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Closed Case Reason: You must enter a value');
                ApexPages.addMessage(myMsg);
                flag = false;
            }
            //STSK0013527: Divya : Added condition to check closure reason and case lines
            if(currentcase.Closed_Case_Reason__c == 'Created in Error' && currentcase.SVMXC__Case_Lines__r.size() > 0 )
            {
                flag = false;
                return null;
            }
            if(updatecase.Reason == null || updatecase.Description == null)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Case Reason and Description are mandatory to close the case.');
                ApexPages.addMessage(myMsg);
                flag = false;
            }           
            /*if(currentcase.Local_Case_Activity__c == null && (currentcase.Closed_Case_Reason__c != 'Customer Cancelled' && currentcase.Closed_Case_Reason__c != 'Created in Error' )&&(updatecase.Status != 'Closed by Work Order')){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a recommendation.');
                ApexPages.addMessage(myMsg);
                flag = false;
                }  */
            /*  
            //STSK0013571 - Rakesh - Closure is blocked if Escalation Required? = Yes/Yes from WO, and Case ECF Count/Submitted is 0.       
            if(((updatecase.Status != 'Closed') && (updatecase.Status != 'Closed by Work Order')) && ((updatecase.Is_escalation_to_the_CLT_required__c == 'Yes') || (updatecase.Is_escalation_to_the_CLT_required__c == 'Yes from Work Order')) && ((updatecase.Case_ECF_Count__c== 0) || (updatecase.Case_ECF_Submitted__c== 0))){
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Case is marked as Escalation Required, and no ECF has been created and/or submitted.  Please return to Case and create, or ECF and submit.');
                ApexPages.addMessage(myMsg); flag = false; }
            */  
            // /* STSK0013663 - Commenting unused code  
            if(updatecase.Is_escalation_to_the_CLT_required__c == 'Yes' && updatecase.Is_This_a_Complaint__c == 'Yes' && flag == true)
            {
                if(updatecase.L2848__r.size() == 0)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Case cannot be closed since there are no L2848 forms associated to it');
                    ApexPages.addMessage(myMsg);
                    flag = false;
                }
                 else
                {
                    flag = true;
                    /*for(L2848__c clt : updatecase.L2848__r)   {   if(clt.Is_Submit__c == true)    flag = true;  } 
                    system.debug('Value of flag after all CLT---->> '+flag);
                    if(flag == false) {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The Case cannot be closed since the L2848 form associated to it is not yet submitted');
                        ApexPages.addMessage(myMsg); }*/
                }
               } 
         //*/
        } 
        if(flag == true || test.isRunningTest())
        {
            onCaseTypeChange();
            //case updatecase = new case(id = currentcase.id);
            updatecase.status = 'Closed';
            updatecase.Case_Type__c = currentcase.Case_Type__c;
            updatecase.Classification_1__c = currentcase.Classification_1__c;
            updatecase.Classification_2__c = currentcase.Classification_2__c;
            //updatecase.isclosed = true;
            updatecase.Closed_Case_Reason__c = currentcase.Closed_Case_Reason__c;
            updatecase.Local_Case_Activity__c = currentcase.Local_Case_Activity__c;
            try{
            update updatecase;
            isSuccess = true;
          }
          catch(exception e)
          {
              if(!e.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getmessage()));
              else ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getmessage().substring(e.getmessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
              return null;
          }
            if(draftarticle)draftstr = 'true';
            else draftstr = 'false';
          system.debug('Draft value--' + draftstr);
        }
        return null;//(new pagereference('/apex/SR_Close_case_page?id='+ currentcase.id));
    }
    public pagereference yesForEdu()
    {
        Case updatecase = [SELECT Id,Local_Case_Activity__c, Closed_Case_Reason__c,Reason,Description,Status,Is_escalation_to_the_CLT_required__c, Is_This_a_Complaint__c,Case_Type__c,Classification_1__c,Classification_2__c,Classification_3__c,
                           (SELECT Id, Name, Is_Submit__c FROM L2848__r) FROM Case WHERE Id = :currentcase.id]; //Case_ECF_Count__c,Case_ECF_Submitted__c,
        system.debug('Case selected is: '+updatecase);
        flag = true;

        if(currentcase.Closed_Case_Reason__c == 'Created in Error' && currentcase.SVMXC__Case_Lines__r.size() > 0 )
        {
            flag = false;
            return null;
        }

         //*/
        if(flag == true || test.isRunningTest())
        {
            onCaseTypeChange();
            //case updatecase = new case(id = currentcase.id);
            updatecase.status = 'Closed';
            updatecase.Case_Type__c = currentcase.Case_Type__c;
            updatecase.Closed_Case_Reason__c = currentcase.Closed_Case_Reason__c;
            updatecase.Internal_Comments__c = currentcase.Internal_Comments__c;
            try{
            update updatecase;
            if(draftarticle)draftstr = 'true';
            else draftstr = 'false';
            system.debug('Draft value--' + draftstr);
          }
          catch(exception e)
          {
              flag = false;
              if(!e.getmessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getmessage()));
              else ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, e.getmessage().substring(e.getmessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
              return null;
          }
        }
        return null;//(new pagereference('/apex/SR_Close_case_page?id='+ currentcase.id));
    }
    public pagereference No(){
        return (new pagereference('/'+ currentcase.id));
    } 
}
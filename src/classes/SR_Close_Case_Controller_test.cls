/***************************************************************************\
    @ Author                : Kaushiki Verma
    @ Date                  : 21/11/2014
Change log:
22-Dec-2017 - Divya Hargunani - STSK0013527 - Do not allow "case closure code created in error" if there is a case line - Increased test coverage
****************************************************************************/
@isTest
public class SR_Close_Case_Controller_test 
{
    public static Id caseCA = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CA').getRecordTypeId();
    public static testMethod void SR_Close_Case_Controller_test() 
    {
     //insert Account
    //Account objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA',BillingCity = 'Atlanta', country__c = 'United States');
    //insert objAcc;
    Account objAcc  = new Account(name='testacc',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
    insert objAcc ; 
    
    Contact objContact = new Contact();
    objContact.AccountId = objAcc.id;
    objContact.FirstName = 'Test';
    objContact.LastName = 'Contact';
    objContact.CurrencyIsoCode = 'USD';
    objContact.Email = 'test.tester@testing.com';
    objContact.MailingState = 'CA';
    objContact.Phone= '1235678';
    objContact.MailingCountry = 'USA';
    insert objContact;
     
    Case testcase = SR_testdata.createcase();
    testCase.Priority = 'Medium';
    testCase.ContactID = objContact.id;
    testCase.RecordTypeId = caseCA;
    Insert testcase ;
    
    //testcase.status = 'Closed';
    testcase.Is_This_a_Complaint__c = 'Yes';
    testcase.Reason = 'Installation Assistance';
    testcase.Was_anyone_injured__c = 'No';
    testcase.Is_escalation_to_the_CLT_required__c = 'No';
      //updatecase.isclosed = true;
    testcase.Closed_Case_Reason__c = 'First Call Resolution';
    update testcase;
    
    L2848__c objEC = new L2848__c();
    objEC.Case__c = testcase.id;
    objEC.Is_Submit__c = true; 
    insert objEC;
    
    ApexPages.currentPage().getParameters().put('tabid',testcase.id);
    ApexPages.currentPage().getParameters().put('id',testcase.id);
    ApexPages.StandardController testcontroller = new ApexPages.StandardController(testcase);
    SR_Close_Case_Controller  controller = new SR_Close_Case_Controller(testcontroller);
    controller.getdraftarticle();
    controller.setdraftarticle(true);
    controller.yes();
    controller.No();
    
    Case testcase1 = SR_testdata.createcase();
    testCase1.RecordTypeId = caseCA;
    testCase1.Priority = 'Medium';
    testCase1.ContactID = objContact.id;
    Insert testcase1 ;
    
    //testcase1.status = 'Closed';
    testcase1.Is_This_a_Complaint__c = 'Yes';
    testcase1.Was_anyone_injured__c = 'No';
    testcase1.Is_escalation_to_the_CLT_required__c = 'No';
    testcase1.Closed_Case_Reason__c = 'First Call Resolution';
    update testcase1;    
    
    ApexPages.currentPage().getParameters().put('tabid',testcase1.id);
    ApexPages.StandardController testcontroller1 = new ApexPages.StandardController(testcase1);
    SR_Close_Case_Controller  controller1 = new SR_Close_Case_Controller(testcontroller1);
    controller1.getdraftarticle();
    controller1.setdraftarticle(true);
    controller1.yes();
    controller1.No();
    
    controller1.getArticleTypesOptions();
    controller1.yesForEdu();
    
    }
    
    public static testMethod void SR_Close_Case_Controller_test1() 
    {
    
    //insert Account
     Account objAcc  = new Account(name='testacc',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
    insert objAcc ; 
    
    
    Contact objContact = new Contact();
    objContact.AccountId = objAcc.id;
    objContact.FirstName = 'Test';
    objContact.LastName = 'Contact';
    objContact.CurrencyIsoCode = 'USD';
    objContact.Email = 'test.tester@testing.com';
    objContact.MailingState = 'CA';
    objContact.Phone= '1235678';
    objContact.MailingCountry = 'USA';
    insert objContact;
     
    Case testcase = SR_testdata.createcase();
    testCase.RecordTypeId = caseCA;
    testCase.Priority = 'Medium';
    testCase.ContactID = objContact.id;
    Insert testcase ;
    
    //testcase.status = 'Closed';
    testcase.Is_This_a_Complaint__c = 'Yes';
    testcase.Was_anyone_injured__c = 'No';
    testcase.Is_escalation_to_the_CLT_required__c = 'no';
      //updatecase.isclosed = true;
    testcase.Closed_Case_Reason__c = 'First Call Resolution';
    update testcase;
    
    L2848__c objEC = new L2848__c();
    objEC.Case__c = testcase.id;
    objEC.Is_Submit__c = false; 
    insert objEC;
    
    ApexPages.currentPage().getParameters().put('tabid',testcase.id);
    ApexPages.currentPage().getParameters().put('id',testcase.id);
    ApexPages.StandardController testcontroller = new ApexPages.StandardController(testcase);
    SR_Close_Case_Controller  controller = new SR_Close_Case_Controller(testcontroller);
    controller.getdraftarticle();
    controller.setdraftarticle(false);
    controller.yes();
    controller.No();
    
    Case testcase1 = SR_testdata.createcase();
    testCase1.Priority = 'Medium';
    testCase1.ContactID = objContact.id;
    Insert testcase1 ;
    
    //testcase1.status = 'Closed';
    testcase1.Is_This_a_Complaint__c = 'Yes';
    testcase1.Was_anyone_injured__c = 'No';
    testcase1.Is_escalation_to_the_CLT_required__c = 'no';
    testcase1.Closed_Case_Reason__c = 'First Call Resolution';
    update testcase1;    
    
    ApexPages.currentPage().getParameters().put('tabid',testcase1.id);
    ApexPages.StandardController testcontroller1 = new ApexPages.StandardController(testcase1);
    SR_Close_Case_Controller  controller1 = new SR_Close_Case_Controller(testcontroller1);
    controller1.getdraftarticle();
    controller1.setdraftarticle(true);
    controller1.flag = true;
    controller1.yes();
    controller1.No();
    
    
    }
    
    public static testMethod void SR_Close_Case_Controller_test2() 
    {
    
        //insert Account
         Account objAcc  = new Account(name='testacc',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objAcc ; 
    
        Contact objContact = new Contact();
        objContact.AccountId = objAcc.id;
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        insert objContact;
         
        Case testcase = SR_testdata.createcase();
        testCase.Priority = 'Medium';
        testCase.ContactID = objContact.id;
        testCase.case_type__c = 'Clinical';
        testcase.Classification_1__c = 'Aria';
        testcase.Classification_2__c = 'AURA';
        testCase.RecordTypeId = caseCA;
        Insert testcase ;
        
        
        L2848__c objEC = new L2848__c();
        objEC.Case__c = testcase.id;
        objEC.Is_Submit__c = false; 
        
        insert objEC;
        
           
        
        ApexPages.currentPage().getParameters().put('tabid',testcase.id);
        ApexPages.currentPage().getParameters().put('id',testcase.id);
        ApexPages.StandardController testcontroller2 = new ApexPages.StandardController(testcase);
        SR_Close_Case_Controller  controller2 = new SR_Close_Case_Controller(testcontroller2);
        controller2.getdraftarticle();
        controller2.setdraftarticle(true);
        controller2.flag = true;
        controller2.yes();
        controller2.yes();
        controller2.No();
        
        Case testCase3 = SR_testdata.createcase();
        testCase3.Priority = 'Medium';
        testCase3.ContactID = objContact.id;
        testCase3.case_type__c = 'Hardware';
        testCase3.Classification_1__c = 'Other';
        testCase3.Classification_2__c = 'Other';
        //testCase3.Closed_Case_Reason__c = 'Customer Cancelled';
        testCase3.RecordTypeId = caseCA;
        Insert testCase3 ;
        
        ApexPages.currentPage().getParameters().put('tabid',testCase3.id);
        ApexPages.StandardController testcontroller3 = new ApexPages.StandardController(testCase3);
        SR_Close_Case_Controller  controller3 = new SR_Close_Case_Controller(testcontroller3);
        controller3.getdraftarticle();
        controller3.setdraftarticle(true);
        controller3.flag = true;
        controller3.yes();
        controller3.yes();
        controller3.No();
        controller3.CurrentCase.case_Type__c = 'Hardware';
        controller3.onCaseTypeChange();
    
    }
}
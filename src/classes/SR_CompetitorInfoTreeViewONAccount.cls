/***********************************************************************
* Author        : Rishabh Verma
* Functionality : Tree- Like Data Selection
* Description   :  Tree- Like Data Selection for Competitor Information on Account record                  
* This Apex Class is being referenced in VF Page - SR_CompetitorTreeViewONAccount.
* 
************************************************************************/
public with sharing class SR_CompetitorInfoTreeViewONAccount
{
    
    /* Declaration of Variables */ 
    boolean flag{get;set;}
    set<string> stTempLocationSet=new set<string>();
    public string strNodeId{get;set;}
    public string strAccountId{get;set;}
    Public string strAccountLocationId{get;set;}
    set<string> stChildData=new set<string>();
    private static JSONGenerator gen {get; set;}
    public List<String> LstOfProductNodes{get;set;} 
    set<string> stLocationId=new set<string>();
    public string picklistvalue{get;set;}
    // Getter Setter Method for searching input text in Product Tree on VF page.   
   
    list<Competitor__c> lstCompititorInfo;
    // Getter Setter Method for passing Selected Product from VF page to Apex Class. 
    public String ProductId{get;set;} 
    set<string> stIP=new set<string>(); 
    set<string> competitorRecordTypeSet =new set<string>();
    Map<String,List<Competitor__c>>MapCompetitorAndLstChildren=new Map<String,List<Competitor__c>>();
    List<Competitor__c> MapOfCompetitor;
    List<Competitor__c> LstOfCompetitor;
    Map<string,string> MapIdOfLocation=new Map<string,string>();
    String ipprodtype ;
    PageReference pageRef;
    /* Controller Method Starts here.*/
    public SR_CompetitorInfoTreeViewONAccount(ApexPages.StandardController controller)
    {
        //Id bchyRecType = Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('BCHY').getRecordTypeId();
        //Id ebrtRecType = Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('EBRT').getRecordTypeId();
        //Id swmodelsRecType = Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('SWMD').getRecordTypeId();
        //Id thpyRecType = Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('THPY').getRecordTypeId();
        
        //system.debug('==bchyRecType=='+bchyRecType);
        //system.debug('==ebrtRecType=='+ebrtRecType);
        //system.debug('==swmodelsRecType=='+swmodelsRecType);
        //system.debug('==thpyRecType=='+thpyRecType);
        
        flag = true;
        pageRef = new PageReference('/apex/SR_CompetitorInfoTreeViewONAccount?id='+strAccountId);
        LstOfProductNodes=new List<String>();
        strAccountId=apexpages.currentpage().getparameters().get('id');  // getting Id of Parameter
        
        stLocationId.add(strAccountId);
        if(strAccountId != null)
        {
            MapOfCompetitor = new List<Competitor__c>();
            lstCompititorInfo =new list<Competitor__c>();
            lstCompititorInfo =[select id, Name, Competitor_Account_Name__c, Version__c, Status__c, Vault_identifier__c, No_of_Licences_OIS__c, No_of_Units__c, No_of_Workstations_TPS__c, No_Treatment_Units__c, Afterloader_type__c, Age_In_Years__c, CreatedDate, Channels__c, Compatability__c, Dedicated_or_Shared__c, Energy_Source__c, Model__c, Product_Type__c, Service_Provider__c, Vendor__c from Competitor__c where Competitor_Account_Name__c IN: stLocationId];
            //}
            //lstCompititorInfo =[select id, Name, Competitor_Account_Name__c, Vault_identifier__c, No_of_Licences_OIS__c, No_of_Units__c, No_of_Workstations_TPS__c, No_Treatment_Units__c, Afterloader_type__c, Age_In_Years__c, CreatedDate, Channels__c, Compatability__c, Dedicated_or_Shared__c, Energy_Source__c, Model__c, Product_Type__c, Service_Provider__c, Vendor__c from Competitor__c where Competitor_Account_Name__c IN: stLocationId];
            
            System.debug(' lstCompititorInfo >>>>'+lstCompititorInfo );
            for(Competitor__c varIp:lstCompititorInfo)
            {
                stIP.add(varIp.Product_Type__c);
                system.debug('==varIp.Product_Type__c=='+varIp.Product_Type__c);
                system.debug('==varIp=='+varIp);
                //if(!MapOfCompetitor.containsKey(varIp.Product_Type__c)){
                    
                    MapOfCompetitor.add(varIp);
                //}
            }
            
            system.debug('==MapOfCompetitor=='+MapOfCompetitor);
            //MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,SVMXC__Site__r.name,name,SVMXC__Serial_Lot_Number__c,Product_Version_Build__r.Name,SVMXC__Product__r.name,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Site__c IN:stLocationId]);
                
            
        for(Competitor__c pd:MapOfCompetitor) 
        {
            if(MapCompetitorAndLstChildren.get(pd.Product_Type__c)==null)
            {
                LstOfCompetitor = new List<Competitor__c>();
                LstOfCompetitor.add(pd);
                MapCompetitorAndLstChildren.put(pd.Product_Type__c,LstOfCompetitor);
            }
        }
       
        
        //*****************************************
        for(string varLCId: stIP)
        {  
            LstOfCompetitor = new List<Competitor__c>();
            for(Competitor__c pd:MapOfCompetitor) 
            {
                stTempLocationSet.add(pd.Product_Type__c);
                if(pd.Product_Type__c==varLCId)
                {
                    MapIdOfLocation.put(pd.Product_Type__c,'Location');
                    LstOfCompetitor.add(pd);
                }
            }
            MapCompetitorAndLstChildren.put(varLCId,LstOfCompetitor);
        }
        //*******************************************
        
        
        Map<string,list<Competitor__c>> mapChld=new Map<string,list<Competitor__c>>();
        for(string varStr:stIP)
        {
            LstOfCompetitor=new List<Competitor__c>();
            for(Competitor__c pd:MapOfCompetitor)
            {
                if(varStr == pd.Product_Type__c)
                {
                 LstOfCompetitor.add(pd);
                }
            }
            mapChld.put(varStr,LstOfCompetitor);     // Map holding Product-ProductChild data
        }
        for(string varst:stIP)                 // Used to get all child record id which does not occur as root node
        {
            for(Competitor__c varStr:mapChld.get(varst))
            {
              stChildData.add(varStr.Product_Type__c);                          
            }
        }
        System.debug('------MapCompetitorAndLstChildren------------------>'+MapCompetitorAndLstChildren);
        }
    } 
    
    /* Action method (called from VF:SR_ProductTreeViewONAccount) starts here.
       Below method parses all Parent Products and adds them to JSON String 
       which is rendered on VF as Tree Structure. */
       
    public void GenerateTreeStructureForProducts()
    {
        System.debug('-----stLocation------------->');
        gen = JSON.createGenerator(true);
        HeadertoJSON();
        LstOfProductNodes.add(gen.getAsString()); 
        flag = false;
        // Generating Tree Strucutre for all Parent Products.
        set<string> alreadyExists = new set<string>();
        for(Competitor__c pd: [select Id, Name, Product_Type__c  from  Competitor__c where Product_Type__c IN :stIP])
        {
            if(alreadyExists != null && alreadyExists.contains(pd.Product_Type__c) == false){
                alreadyExists.add(pd.Product_Type__c);
                // Initializing JSON Generator Object.
                gen = JSON.createGenerator(true);
                system.debug('==pd=='+pd);
                WrpProductNode node = CreateTreeStructure(pd.Product_Type__c,null);
                system.debug('--Node->'+Node);

                ConvertNodeToJSON(node);    
                system.debug('>>>>>>>>>>>>>>'+ gen.getAsString());
                LstOfProductNodes.add(gen.getAsString()); 
            }           
        }
    } /* Action method ends here.*/ 
    
    /*******************************************************************/
    public void HeadertoJSON()
    {
        gen.writeStartObject();
        //gen.writeStringField('title', 'Product Type'+'@'+'Product Name'+'@'+' Vendor'+'@'+'');
        //gen.writeStringField('title', 'Product Type'+'@'+'Model'+'@'+'Version'+'@'+' Vendor'+'@'+'Age In Years'+'@'+'CreatedDate'+'@'+'Vault Identifier'+'@'+'Status'+'@'+'');
        gen.writeStringField('title', 'Product Type'+'@'+'Vendor'+'@'+'Product Name'+'@'+' Version'+'@'+'Age In Years'+'@'+'CreatedDate'+'@'+'Vault Identifier'+'@'+'Status'+'@'+'');
        
        
        gen.writeBooleanField('unselectable', false);
        gen.writeStringField('key', '');
        gen.writeBooleanField('expand', false);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        gen.writeEndObject();  
    }
    
    /*******************************************************************/
    /* CreateTreeStructure Method starts here : Below method creates Parent and Child Nodes for each Product 
       and returns instance of WrpProductNode (Wrapper class) */
    public  WrpProductNode CreateTreeStructure(String prd, Competitor__c prd1)
    {            
        WrpProductNode wrpObj=new WrpProductNode();
        wrpObj.prod=prd;   // Assign the site to Loation property Inner Class
        wrpObj.prod1=prd1; // Assign Ip to Inner Class property
        List<WrpProductNode>lstOfChild;
        if(wrpObj.prod !=null && wrpObj.prod1==null) // 
        {
            system.debug('==MapCompetitorAndLstChildren.get(prd)=='+MapCompetitorAndLstChildren.get(prd));
            if(MapCompetitorAndLstChildren.get(prd)!=null)
            {
                system.debug('==MapCompetitorAndLstChildren.get(prd)=='+MapCompetitorAndLstChildren.get(prd));
                wrpObj.hasChildren =true;
                lstOfChild=new List<WrpProductNode >();
                for(Competitor__c pd:MapCompetitorAndLstChildren.get(prd)){
                  WrpProductNode temp=CreateTreeStructure(null,pd);
                    lstOfChild.add(temp);
                }           
            }else
            {
                wrpObj.hasChildren =false;   
            }   
                
            wrpObj.prodChildNode=lstOfChild;
        }
        else
        {
            if(MapCompetitorAndLstChildren.get(prd1.id)!=null)
            {
                wrpObj.hasChildren =true;
                lstOfChild=new List<WrpProductNode >();
                for(Competitor__c pd:MapCompetitorAndLstChildren.get(prd1.Product_Type__c))
                {
                  WrpProductNode temp=CreateTreeStructure(null,pd);
                    lstOfChild.add(temp);
                }           
            }
            else 
            {
                wrpObj.hasChildren =false;   
            }   
                
            wrpObj.prodChildNode=lstOfChild;
        }
        system.debug('------aa-->'+wrpObj.prodChildNode);
        return wrpObj;        
    } /*CreateTreeStrucure Method Ends here.*/
    
    /*ConvertNodeToJSON method starts here : Below method converts 
    instance of each Product Node(Wrapper class) into JSON.*/
    public void ConvertNodeToJSON(WrpProductNode prodNode)
    { 
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce. 
        System.debug('--Id'+prodNode.prod);  
        System.debug('--MapIdOfLocation.keyset--'+MapIdOfLocation.keyset());   
  
        if( MapIdOfLocation.keyset().contains(prodNode.prod) )
        {   
            gen.writeStartObject();
            gen.writeStringField('title', prodNode.Prod+'@'+' '+'@'+' '+'@'+' '+'@'+' '+'@'+' '+'@'+prodNode.Prod);
            gen.writeStringField('key', prodNode.Prod);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
            gen.writeBooleanField('isFolder', false);
            gen.writeBooleanField('icon', false);
        }
        else if(prodNode.Prod1 !=null)
        {
            /* if(count == 0)
            {
                gen.writeStringField('title', 'Installed Product Name'+'@'+'Product Name'+'@'+' Product Version Build'+'@'+'Serial Number'+'@'+'Service Contract '+'@'+' Contract Start Date'+'@'+'Contract End Date'+'@'+'City'+'@'+'Status'+'@'+'Header');
                count = 1;
            }*/
            
            gen.writeStartObject();
            gen.writeStringField('title', prodNode.Prod1.Product_Type__c+'@'+(prodNode.Prod1.Vendor__c==null?' ': prodNode.Prod1.Vendor__c)+'@'
            
            + (prodNode.Prod1.Model__c==null?' ':prodNode.Prod1.Model__c)+'@'
            + (prodNode.Prod1.Version__c==null?'-':prodNode.Prod1.Version__c)+'@'
            + (string.valueof(prodNode.Prod1.Age_In_Years__c)==null?' ':string.valueof(prodNode.Prod1.Age_In_Years__c))+'@'
            + (string.valueof(prodNode.Prod1.CreatedDate) ==null?' ':(string.valueof(prodNode.Prod1.CreatedDate)).substring(0,10))+'@'
            + (string.valueof(prodNode.Prod1.Vault_identifier__c)==null?' ':string.valueof(prodNode.Prod1.Vault_identifier__c))+'@'
            + (prodNode.Prod1.Status__c == null?' ':prodNode.Prod1.Status__c)+'@'+prodNode.Prod1.Id);
            //+ (string.valueof(prodNode.Prod1.Age_In_Years__c)==null?' ':string.valueof(prodNode.Prod1.Age_In_Years__c))+'@'+prodNode.Prod1.Id);
            
            gen.writeStringField('key', prodNode.Prod1.Id);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
            gen.writeBooleanField('isFolder', false);
            gen.writeBooleanField('icon', false);
        }
        // Below condition checks for any child Products available for each Parent product.
        if(prodNode.hasChildren )
        {                
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
            {
                if(!stChildData.contains(temp.Prod1.id))  // cheking for root or leaf node data
                {
                    ConvertNodeToJSONChild(temp);    
                } 
            }               
        }
        gen.writeEndArray();               
        gen.writeEndObject();           
    }/*ConvertNodeToJSON method ends here.*/
    
    public void ConvertNodeToJSONChild(WrpProductNode prodNode)
    { 
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce.      
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod1.Name+'@'+(prodNode.Prod1.Vendor__c==null?' ': prodNode.Prod1.Vendor__c)
        +'@'+ (prodNode.Prod1.Model__c==null?' ':prodNode.Prod1.Model__c)+
        +'@'+(prodNode.Prod1.Version__c==null?'-':prodNode.Prod1.Version__c)+ 
        +'@'+(string.valueof(prodNode.Prod1.Age_In_Years__c)==null?' ':string.valueof(prodNode.Prod1.Age_In_Years__c))+
        +'@'+(string.valueof(prodNode.Prod1.CreatedDate)==null?' ':(String.valueof(prodNode.Prod1.CreatedDate)).substring(0,10))+
        +'@'+ (string.valueof(prodNode.Prod1.Vault_identifier__c)==null?' ':string.valueof(prodNode.Prod1.Vault_identifier__c))+
        +'@'+(prodNode.Prod1.Status__c==null?' ':prodNode.Prod1.Status__c)+'@'+prodNode.Prod1.Id);
        //+'@'+ (string.valueof(prodNode.Prod1.Age_In_Years__c)==null?' ':string.valueof(prodNode.Prod1.Age_In_Years__c))+'@'+prodNode.Prod1.Id);
        
        
        gen.writeStringField('key', prodNode.Prod1.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
        if(prodNode.hasChildren )
        {    
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
        gen.writeEndObject();    
    }
    /*Wrapper class starts here : Below class wraps Products,List of Child Products 
    and boolean variable for childs present for each Parent Product.*/
    public class WrpProductNode
    {        
        public List<WrpProductNode>prodChildNode{get;set;}
        public Boolean hasChildren {get; set;} 
        public String prod{get;set;}    
        public Competitor__c prod1{get;set;}        
    } /*Wrapper Class ends here*/   
    
    //***********************************
}
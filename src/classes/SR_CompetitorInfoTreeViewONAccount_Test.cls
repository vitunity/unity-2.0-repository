@isTest 
Public class SR_CompetitorInfoTreeViewONAccount_Test {
    static testMethod void TestTreeData() 
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt10', Email='standarduser10@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing10', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser10@testorg.com');

        //System.runAs(u) {
            Account a;
            SVMXC__Site__c objloc;
            List<SVMXC__Installed_Product__c> lstInstall=new list<SVMXC__Installed_Product__c>();
            
            
            // Inserting account
            a = new Account(name='test',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'); 
            insert a;  
            system.assertNotEquals(a.Id,null);
            
            //Inserting objLocation
            objloc=new SVMXC__Site__c();
            objloc.SVMXC__Account__c=a.id;
            objloc.Name='testLocation';
            objloc.SVMXC__Street__c='testStreet';
            objloc.SVMXC__Country__c='testCountry';
            objloc.SVMXC__Zip__c='201301';
            insert objloc;  
            
            
            system.assertEquals(objloc.SVMXC__Account__c,a.Id);
            system.assertNotEquals(objloc.Id,null);
            
            
            
            //Inserting  Non-Varian Installed Prods
                    
            Competitor__c compobj = new Competitor__c(
            Product_Type__c = 'Brachytherapy Afterloaders',// Product Type
              Competitor_Account_Name__c = a.Id,  // Account Name
              Vendor__c = 'Bebig',                             // Vendor
              Year_Installed__c = '1980',                      // Year Installed
              Model__c = 'Ir-192 or Co-60',                    // Product Name
              Afterloader_type__c = 'GyneSource',              // Afterloader type
              Vault_identifier__c = '2',                       // Vault identifier
              Channels__c = '20',                              // Channels
              No_Treatment_Units__c = 1,                       // # Treatment Units
              Isotope_Source__c = 'Test',                      // Isotope Source
              Dedicated_or_Shared__c = 'Dedicated',            // Dedicated or Shared
              Status__c = 'Current'                           // Status
            );
            insert compobj;
            
            
            
            Competitor__c compobj1 = new Competitor__c(
              Product_Type__c = 'Brachytherapy Afterloaders',// Product Type
              Competitor_Account_Name__c = a.Id,  // Account Name
              Vendor__c = 'Bebig',                             // Vendor
              Year_Installed__c = '1980',                      // Year Installed
              Model__c = 'Ir-192 or Co-60',                    // Product Name
              Afterloader_type__c = 'MultiSource',              // Afterloader type
              Vault_identifier__c = '3',                       // Vault identifier
              Channels__c = '20',                              // Channels
              No_Treatment_Units__c = 1,                       // # Treatment Units
              Isotope_Source__c = 'Test',                      // Isotope Source
              Dedicated_or_Shared__c = 'Dedicated',            // Dedicated or Shared
              Status__c = 'Current'    
            );
            insert compobj1;
            
            
            
            List<SVMXC__Site__c> lstSite=[select id,SVMXC__Account__c from  SVMXC__Site__c where SVMXC__Account__c =: a.Id ];
            system.assertEquals(lstSite.size(),1);
            
            //List<SVMXC__Installed_Product__c> lstInProd=[select id from  SVMXC__Installed_Product__c where SVMXC__Site__c =: objloc.Id ];
            //system.assertEquals(lstInProd.size(),3);
            
            ApexPages.StandardController sc = new ApexPages.standardController(a);
            Test.setCurrentPage(Page.SR_CompetitorInfoTreeViewONAccount);
            ApexPages.currentPage().getParameters().put('id', a.id);
            //ApexPages.currentPage().getParameters().put('prodtype', 'TESTPROD');
            
            
            SR_CompetitorInfoTreeViewONAccount sic = new SR_CompetitorInfoTreeViewONAccount(sc);
            sic.GenerateTreeStructureForProducts();
       // }
    }
}
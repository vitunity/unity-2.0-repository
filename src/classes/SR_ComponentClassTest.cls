@isTest
public class SR_ComponentClassTest {

    public static testMethod void myController1() {
    
        //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234',Prefered_Language__c ='English',BillingCity = 'Pune');
        insert accnt;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        objContact.AccountId = accnt.Id;
        insert objContact;
        
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
         Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(),contactID = objContact.id, AccountId = accnt.Id,Subject ='test case',Status = 'Closed', Reason ='System Down',Closed_Case_Reason__c ='Resolved No Further Action' );
         insert caseObj ;
    
            SR_ComponentClass compController = new SR_ComponentClass();
            compController.controllerValue = caseObj.id;
            
            compController.SR_ComponentClass();
            compController.getRecCase(); 
    
        
    }
    
     public static testMethod void myController2() {
    Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User u2 = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        System.RunAs(u2){
        //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234',Prefered_Language__c ='English',BillingCity = 'Pune');
        insert accnt;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        objContact.AccountId = accnt.Id;
        insert objContact;
        
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
         Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Local_Case_Activity__c = 'asdfghjkl',contactId = objContact.id, Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), AccountId = accnt.Id,Subject ='test case',Status = 'Closed By Work Order', Reason ='System Down', Closed_Case_Reason__c ='Resolved No Further Action' );
         caseObj.Priority = 'Low';
            insert caseObj ;
    
            SR_ComponentClass compController = new SR_ComponentClass();
            compController.controllerValue = caseObj.id;
            
            compController.SR_ComponentClass();
            compController.getRecCase(); 
    
        }
    }

}
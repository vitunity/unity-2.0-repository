global without sharing class SR_ComponentClassV1 
{
    global Id controllerValue{get;set;}  
    global boolean Service {get;set;} 
    global boolean HelpDesk {get;set;}
    global string DateToDisplay {get;set;} 
    global string DateToDisplayNA {get;set;}
    global string DateForJapan {get;set;}
    
    global string Summary{get;set;}    
    global Case VarCase = new case();
    global CaseComment CaseCommentObj{get;set;}
    global string getCaseURL(){
        return URL.getSalesforceBaseUrl().toExternalForm()+'/'+VarCase.Id;
    }
    global void SR_ComponentClassV1()
    {
        Service = false;
        HelpDesk = false;

    }
    global case getRecCase()
    {   
        
        VarCase = [Select id, AccountId, Account.Country__c, CaseNumber, Reason, Closed_Case_Reason__c, ClosedBy__c, Local_Subject__c, Owner.name, Local_Case_Activity__c, OwnerId, CreatedDate,
                    Case_Activity__c, status, Subject, ProductId, Contact.Name,Contact.FirstName,Contact.LastName, Contact.Email, Created_Date_Time__c, ClosedBy__r.ManagerId, ClosedDate, ClosedBy__r.Name, contactId, 
                    ProductSystem__c, SVMXC__Top_Level__r.SVMXC__Product__r.Name, SVMXC__Top_Level__c,
                    SVMXC__Top_Level__r.name, ProductSystem__r.SVMXC__Product__r.Name, ProductSystem__r.name,SVMXC__Site__r.ERP_Default_Preferred_Language_Code__c 
                    from case where id=:controllerValue];
        CaseCommentObj = [select Id,ParentId,CommentBody from CaseComment where ParentId =: VarCase.Id order by createdDate Desc limit 1];
        if(VarCase.Local_Case_Activity__c!=NULL)
        {
            Summary = VarCase.Local_Case_Activity__c.replaceAll('<[^>]*br>',' ').replaceall('<br>',' ').ReplaceAll('<[^>]*>',' ').ReplaceAll('&#39;"','\'').unescapeHtml4() ;//ReplaceAll('&#39;',''); //.ReplaceAll('<[^>]*>',' '); 
        }
        
        
        if((VarCase.Reason == 'System Down'|| VarCase.Reason =='Education/Training Request' ||VarCase.Reason == 'Analysis/Troubleshoot' || VarCase.Reason == 'Information' || VarCase.Reason == 'Repair' ||VarCase.Reason == 'Invoice & Billing' || VarCase.Reason == 'Parts Order'|| VarCase.Reason == 'Request'|| VarCase.Reason == 'Scheduled Maintenance') && (VarCase.Closed_Case_Reason__c != 'Created in Error' && VarCase.Closed_Case_Reason__c != 'Sent to Other Dept')  && VarCase.Status == 'Closed By Work Order')
        {
            Service= true;
            system.debug('In the service if'+Service);
        }
        
        if((VarCase.Reason == 'System Down'|| VarCase.Reason =='Education/Training Request' ||VarCase.Reason == 'Analysis/Troubleshoot' || VarCase.Reason == 'Information' || VarCase.Reason == 'Repair' ||VarCase.Reason == 'Invoice & Billing' || VarCase.Reason == 'Parts Order'|| VarCase.Reason == 'Request'|| VarCase.Reason == 'Scheduled Maintenance')  && (VarCase.Closed_Case_Reason__c != 'Created in Error' && VarCase.Closed_Case_Reason__c != 'Sent to Other Dept' ) && VarCase.Status == 'Closed')
        {
            HelpDesk = true;
            system.debug('In the helpesk if'+HelpDesk );
        }
        
        DateToDisplayNA = VarCase.Created_Date_Time__c.format('MM/dd/YYYY' ) ;//MM/DD/YYYY 
        System.debug('DateToDisplayNA@@@@' +DateToDisplayNA );
        DateToDisplay = VarCase.Created_Date_Time__c.format('dd/MM/yyyy' ) ;//DD/MM/YYYY 
        
        System.debug('DateToDisplay@@@@' +DateToDisplay );
        
        DateForJapan = VarCase.Created_Date_Time__c.format('yyyy/MM/DD' ) ;
        
        return VarCase ;
    }
}
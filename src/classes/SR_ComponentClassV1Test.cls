@isTest
public class SR_ComponentClassV1Test {

    public static testMethod void myController1() {
    
        //insert Account record
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test Account', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234',Prefered_Language__c ='English');
        insert accnt;
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='USA' ; 
        insert reg ;
        
        Contact objContact = new Contact();
        objContact.AccountId = accnt.Id;
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        insert objContact;
        
        //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
         Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Priority = 'high',Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(),contactID = objContact.id, AccountId = accnt.Id,Subject ='test case',Status = 'Closed', Reason ='System Down',Closed_Case_Reason__c ='Resolved No Further Action' );
         insert caseObj ;
         caseComment cm = new caseComment();
         cm.parentId = caseObj.Id;
         cm.CommentBody = 'test';         
         insert cm;
         
    
            SR_ComponentClassV1 compController = new SR_ComponentClassV1();
            compController.controllerValue = caseObj.id;
            compController.getRecCase(); 
            
            compController.getCaseURL();
            compController.SR_ComponentClassV1();
    
        
    }
}
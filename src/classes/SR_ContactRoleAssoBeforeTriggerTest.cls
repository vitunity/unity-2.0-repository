@isTest(SeeAllData =true)
private class SR_ContactRoleAssoBeforeTriggerTest {

    static testmethod void ContactRoleTest() {
        Schema.DescribeSObjectResult ARs = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AcMapByName = ARs.getRecordTypeInfosByName();    
        Account accnt = new Account(Recordtypeid=AcMapByName.get('Sold To').getRecordTypeId(), name = 'Wipro technologies1',CurrencyIsoCode='USD',BillingCity='New York',BillingCountry='USA', Country__c='USA',BillingStreet='New Jersey',BillingPostalCode='552600',BillingState='LA',VMS_Factory_Tours_Attended__c= 1,OMNI_Postal_Code__c ='21234', At_least_one_RAQA_Contact_Assigned__c= true);
        insert accnt;
        
        contact con = new contact();
        con.FirstName= 'Megha';
        con.lastname= 'Arora';
        con.department='Rad ONC';
        con.MailingCity='New York';
        con.MailingCountry='US';
        con.MailingStreet='New Jersey2,';
        con.MailingPostalCode='552601';
        con.MailingState='CA';
        con.Mailing_Address1__c= 'New Jersey2';
        con.Mailing_Address2__c= '';
        con.Mailing_Address3__c= '';
        con.Accountid= accnt.id;
        con.Functional_Role__c = 'Legal';
        con.Email='test@gmail.com';
        con.Phone = '98998999899';
        con.VMS_Factory_Tour_Attended__c= true;
        con.RAQA_Contact__c= true;
        con.Date_of_Last_Factory_Visit__c= System.today();
        insert con;
        
        Contact_Role_Association__c cra = new Contact_Role_Association__c ();
        cra.account__c = accnt.id;
        cra.Contact__c = con.id;
        insert cra;
 
    }
}
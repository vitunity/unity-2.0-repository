@isTest(SeeAllData = true)

public class SR_CoveredProduct_Test
{
   Static testMethod void SR_CoveredProduct()
   {
        test.startTest();
        
        Account acc = new Account();
        acc.Name = 'testERPAccount';
        acc.AccountNumber = 'sitePartner';
        acc.Country__c = 'India';
        insert acc;
            
        SVMXC__Service_Contract__c servcContrct = new SVMXC__Service_Contract__c();
        servcContrct.Name = 'testContrct';
        servcContrct.SVMXC__End_Date__c = System.today()+7;  
        servcContrct.SVMXC__Start_Date__c = System.today();
        servcContrct.ERP_Sales_Org__c = 'my org';
        insert servcContrct;
        
        SVMXC__Service__c sa = new SVMXC__Service__c();
        sa.SVMXC__Service_Type__c = 'Preventive Maintenance Inspections';
        sa.Frequency__c = 18;
        insert sa;
                        
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.SVMXC__Available_Services__c = sa.id; 
        sld1.Service_Maintenance_Contract__c = servcContrct.id;
        insert sld1;
                   
        SVMXC__Service_Contract__c servcContrct2 = new SVMXC__Service_Contract__c();
        servcContrct2.Name = 'testContrct';
        servcContrct2.Quote_Reference__c = 'quote123';
        servcContrct2.SVMXC__End_Date__c = System.today()+7;  
        servcContrct2.SVMXC__Start_Date__c = System.today();
        servcContrct2.ERP_Contract_Type__c = 'ZH3';
        servcContrct2.SVMXC__Active__c = true;
        servcContrct2.ERP_Sales_Org__c = 'my org';
        servcContrct2.ERP_Sold_To__c = 'bdbdb';
        servcContrct2.SVMXC__Company__c = acc.id;
        servcContrct2.SVMXC__Renewed_From__c = servcContrct.id;
        servcContrct2.ERP_Order_Reason__c = 'Sales call';
        insert servcContrct2;
        
        SVMXC__SLA_Detail__c objDetails = new SVMXC__SLA_Detail__c();
        objDetails.SVMXC__Available_Services__c = sa.id;
        objDetails.Service_Maintenance_Contract__c = servcContrct2.id;
        insert objDetails;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'testSit';
        site.ERP_Functional_Location__c = 'funcLocation';
        insert site;
        
        Product2 pro = new Product2();
        pro.Name = 'testProd';
        pro.ProductCode = 'pro123';
        pro.SVMXC__Product_Line__c = 'Accessory';
        insert pro; 
               
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'test1IP';
        ip1.SVMXC__Product__c = pro.id;
        ip1.SVMXC__Serial_Lot_Number__c = 'sr123';
        insert ip1;   
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.Name = 'test1IP';
        ip2.SVMXC__Product__c = pro.id;
        ip2.SVMXC__Serial_Lot_Number__c = 'sr123';
        ip2.SVMXC__Top_Level__c = ip1.id;
        ip2.SVMXC__Parent__c =  ip1.id;
        insert ip2;

        SVMXC__PM_Plan_Template__c pmPlanTemp = new SVMXC__PM_Plan_Template__c();
        pmPlanTemp.Name = 'pnPlanTemp';
        insert pmPlanTemp;
              
        SVMXC__PM_Applicable_Product__c pmApplcablProd = new SVMXC__PM_Applicable_Product__c();
        pmApplcablProd.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmApplcablProd.PM_Group__c  = 'testGroup';
        pmApplcablProd.SVMXC__Product_Line__c = 'Accessory';
        insert pmApplcablProd;
        
        ERP_Partner_Association__c ea = new ERP_Partner_Association__c();
        ea.ERP_Partner_Number__c = 'sitePartner';
        ea.Partner_Function__c = 'Z1=Site Partner';
        insert ea;   
        
        SVMXC__PM_Plan__c pmPlan = new SVMXC__PM_Plan__c();
        pmplan.name = 'testPlan';
        pmplan.SVMXC__Service_Contract__c = servcContrct.id;  
        pmplan.SVMXC__End_Date__c =  System.today();   
        pmplan.SVMXC__Status__c = 'Active';
        pmplan.Top_Level__c = ip1.id;
        pmplan.PM_Group__c = 'testGroup';
        insert pmPlan;  

        ERP_Org__c eorg = new ERP_Org__c();
        eorg.Sales_Org__c = 'my org';
        eorg.Days_Offset__c = 1234568878.2;
        insert eorg;
        
        SVMXC__PM_Coverage__c pm = new SVMXC__PM_Coverage__c();
        pm.SVMXC__Product_Name__c = ip2.id;
        pm.SVMXC__PM_Frequency__c = 10;
        pm.SVMXC__PM_Plan__c = pmPlan.id;
        insert pm;
        
        List <SVMXC__Service_Contract_Products__c> listvarCP = new List <SVMXC__Service_Contract_Products__c>();
        SVMXC__Service_Contract_Products__c varCP = new SVMXC__Service_Contract_Products__c ();
        varCP.SVMXC__Service_Contract__c = servcContrct2.id;
        varCP.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP.SVMXC__Product__c = pro.id;
        varCP.Product_Code__c = 'pro123';
        varCP.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP.ERP_Site_Partner__c = ea.ERP_Partner_Number__c;
        varCP.SVMXC__Installed_Product__c = ip2.id;
        listvarCP.add(varCP);
        insert listvarCP;
    
        test.stopTest();
         
    }
    Static testMethod void createPMCoverageTest()
    {
        
        Account acc = new Account();
        acc.Name = 'testERPAccount';
        acc.AccountNumber = 'sitePartner';
        acc.Country__c = 'India';
        insert acc;
            
        SVMXC__Service_Contract__c servcContrct = new SVMXC__Service_Contract__c();
        servcContrct.Name = 'testContrct';
        servcContrct.SVMXC__End_Date__c = System.today()+6;  
        servcContrct.SVMXC__Start_Date__c = System.today();
        servcContrct.ERP_Sales_Org__c = 'my org';
        servcContrct.Quote_Reference__c = 'quote123';
        servcContrct.ERP_Contract_Type__c = 'ZH3';
        servcContrct.SVMXC__Active__c = true;
        servcContrct.ERP_Sold_To__c = 'bdbdb';
        servcContrct.SVMXC__Company__c = acc.id;
        insert servcContrct;
        
        SVMXC__Service__c sa = new SVMXC__Service__c();
        sa.SVMXC__Service_Type__c = 'Preventive Maintenance Inspections';
        sa.Frequency__c = 18;
        insert sa;
                        
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.SVMXC__Available_Services__c = sa.id; 
        sld1.Service_Maintenance_Contract__c = servcContrct.id;
        insert sld1;
                   
        SVMXC__Service_Contract__c servcContrct2 = new SVMXC__Service_Contract__c();
        servcContrct2.Name = 'testContrct1';
        servcContrct2.Quote_Reference__c = 'quote123';
        servcContrct2.SVMXC__End_Date__c = System.today()+7;  
        servcContrct2.SVMXC__Start_Date__c = System.today();
        servcContrct2.ERP_Contract_Type__c = 'ZH3';
        servcContrct2.SVMXC__Active__c = true;
        servcContrct2.ERP_Sales_Org__c = 'my org';
        servcContrct2.ERP_Sold_To__c = 'bdbdb';
        servcContrct2.SVMXC__Company__c = acc.id;
        servcContrct2.SVMXC__Renewed_From__c = servcContrct.id;
        insert servcContrct2;
        
        SVMXC__SLA_Detail__c objDetails = new SVMXC__SLA_Detail__c();
        objDetails.SVMXC__Available_Services__c = sa.id;
        objDetails.Service_Maintenance_Contract__c = servcContrct2.id;
        insert objDetails;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'testSit';
        site.ERP_Functional_Location__c = 'funcLocation';
        insert site;
        
        Product2 pro = new Product2();
        pro.Name = 'testProd';
        pro.ProductCode = 'pro123';
        pro.SVMXC__Product_Line__c = 'Accessory';
        insert pro; 
               
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'sr12';
        ip1.SVMXC__Product__c = pro.id;
        ip1.SVMXC__Serial_Lot_Number__c = 'sr12';
        insert ip1;   
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.Name = 'sr123';
        ip2.SVMXC__Product__c = pro.id;
        ip2.SVMXC__Serial_Lot_Number__c = 'sr123';
        ip2.SVMXC__Top_Level__c = ip1.id;
        ip2.SVMXC__Parent__c =  ip1.id;
        insert ip2;

        SVMXC__PM_Plan_Template__c pmPlanTemp = new SVMXC__PM_Plan_Template__c();
        pmPlanTemp.Name = 'pnPlanTemp';
        insert pmPlanTemp;
              
        SVMXC__PM_Applicable_Product__c pmApplcablProd = new SVMXC__PM_Applicable_Product__c();
        pmApplcablProd.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmApplcablProd.PM_Group__c  = 'testGroup';
        pmApplcablProd.SVMXC__Product_Line__c = 'Accessory';
        insert pmApplcablProd;
        
        ERP_Partner_Association__c ea = new ERP_Partner_Association__c();
        ea.ERP_Partner_Number__c = 'sitePartner';
        ea.Partner_Function__c = 'Z1=Site Partner';
        insert ea;   
        
        SVMXC__PM_Plan__c pmPlan = new SVMXC__PM_Plan__c();
        pmplan.name = 'testPlan';
        pmplan.SVMXC__Service_Contract__c = servcContrct.id;  
        pmplan.SVMXC__End_Date__c =  System.today()+7;   
        pmplan.SVMXC__Status__c = 'Active';
        pmplan.Top_Level__c = ip1.id;
        pmplan.SVMXC__Frequency__c = 20;
        pmplan.PM_Group__c = 'testGroup';
        insert pmPlan;  
        
        SVMXC__PM_Plan__c pmPlan1 = new SVMXC__PM_Plan__c();
        pmPlan1.name = 'testPlan1';
        pmPlan1.SVMXC__Service_Contract__c = servcContrct2.id;  
        pmPlan1.SVMXC__End_Date__c =  System.today()+7;   
        pmPlan1.SVMXC__Status__c = 'Active';
        pmPlan1.Top_Level__c = ip1.id;
        pmPlan1.SVMXC__Frequency__c = 20;
        pmPlan1.PM_Group__c = 'testGroup';
        insert pmPlan1;  
        
        SVMXC__Task_Template__c stt = new SVMXC__Task_Template__c();
        insert stt;

        SVMXC__PM_Schedule_Template__c pmSchdlTemp = new SVMXC__PM_Schedule_Template__c();
        pmSchdlTemp.SVMXC__Frequency_Unit__c = 'Months';
        pmSchdlTemp.SVMXC__Recurring__c = true;
        pmSchdlTemp.SVMXC__Schedule_Type__c = 'Time Based';
        pmSchdlTemp.SVMXC__Visit_Description__c = 'testVisit Desc';
        pmSchdlTemp.SVMXC__Work_Order_Purpose__c = stt.id;
        pmSchdlTemp.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmSchdlTemp.SVMXC__Frequency__c = 10;
        insert pmSchdlTemp;
       
        ERP_Org__c eorg = new ERP_Org__c();
        eorg.Sales_Org__c = 'my org';
        eorg.Days_Offset__c = 1234568878.2;
        insert eorg;
        
        SVMXC__PM_Coverage__c pm = new SVMXC__PM_Coverage__c();
        pm.SVMXC__Product_Name__c = ip2.id;
        pm.SVMXC__PM_Frequency__c = 10;
        pm.SVMXC__PM_Plan__c = pmPlan1.id;
        insert pm;
                        
        SVMXC__Service_Contract_Products__c varCP = new SVMXC__Service_Contract_Products__c ();
        varCP.SVMXC__Service_Contract__c = servcContrct2.id;
        varCP.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP.SVMXC__Product__c = pro.id;
        varCP.Product_Code__c = 'pro123';
        varCP.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP.ERP_Site_Partner__c = ea.ERP_Partner_Number__c;
        varCP.SVMXC__Installed_Product__c = ip2.id; 
        varCP.SVMXC__Start_Date__c = system.today();
        varCP.SVMXC__End_Date__c = system.today()+ 7;       
        insert varCP;
        
        test.startTest();
        SVMXC__Service_Contract_Products__c varCP1 = new SVMXC__Service_Contract_Products__c ();
        varCP1.SVMXC__Service_Contract__c = servcContrct2.id;
        varCP1.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP1.SVMXC__Product__c = pro.id;
        varCP1.Product_Code__c = 'pro123';
        varCP1.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP1.SVMXC__Start_Date__c = system.today();
        varCP1.SVMXC__End_Date__c = system.today()+ 7;  
        varCP1.SVMXC__Installed_Product__c = ip2.id;
        insert varCP1;

        test.stopTest();
    }
    Static testMethod void createPMCoverageTest1()
    {
      
        Account acc = new Account();
        acc.Name = 'testERPAccount';
        acc.AccountNumber = 'sitePartner';
        acc.Country__c = 'India';
        insert acc;
        
        ERP_Org__c eorg = new ERP_Org__c();
        eorg.Sales_Org__c = 'my org';
        eorg.Days_Offset__c = 12.20;
        insert eorg;
        
        SVMXC__Service_Contract__c servcContrct = new SVMXC__Service_Contract__c();
        servcContrct.Name = 'testContrct';
        servcContrct.SVMXC__End_Date__c = System.today()+6;  
        servcContrct.SVMXC__Start_Date__c = System.today();
        servcContrct.ERP_Sales_Org__c = 'my org';
        servcContrct.Quote_Reference__c = 'quote123';
        servcContrct.ERP_Contract_Type__c = 'ZH3';
        servcContrct.SVMXC__Active__c = true;
        servcContrct.ERP_Sold_To__c = 'bdbdb';
        servcContrct.SVMXC__Company__c = acc.id;
        insert servcContrct;
        
        SVMXC__Service__c sa = new SVMXC__Service__c();
        sa.SVMXC__Service_Type__c = 'Preventive Maintenance Inspections';
        sa.Frequency__c = 18;
        insert sa;
                        
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.SVMXC__Available_Services__c = sa.id; 
        sld1.Service_Maintenance_Contract__c = servcContrct.id;
        insert sld1;
                   
        SVMXC__Service_Contract__c servcContrct2 = new SVMXC__Service_Contract__c();
        servcContrct2.Name = 'testContrct1';
        servcContrct2.Quote_Reference__c = 'quote123';
        servcContrct2.SVMXC__End_Date__c = System.today()+7;  
        servcContrct2.SVMXC__Start_Date__c = System.today();
        servcContrct2.ERP_Contract_Type__c = 'ZH3';
        servcContrct2.SVMXC__Active__c = true;
        servcContrct2.ERP_Sales_Org__c = 'my org';
        servcContrct2.ERP_Sold_To__c = 'bdbdb';
        servcContrct2.SVMXC__Company__c = acc.id;
        servcContrct2.SVMXC__Renewed_From__c = servcContrct.id;
        insert servcContrct2;
        
        SVMXC__SLA_Detail__c objDetails = new SVMXC__SLA_Detail__c();
        objDetails.SVMXC__Available_Services__c = sa.id;
        objDetails.Service_Maintenance_Contract__c = servcContrct.id;
        insert objDetails;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'testSit';
        site.ERP_Functional_Location__c = 'funcLocation';
        insert site;
        
        Product2 pro = new Product2();
        pro.Name = 'testProd';
        pro.ProductCode = 'pro123';
        pro.SVMXC__Product_Line__c = 'Accessory';
        insert pro; 
               
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'sr12';
        ip1.SVMXC__Product__c = pro.id;
        ip1.SVMXC__Serial_Lot_Number__c = 'sr12';
        insert ip1;           
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.Name = 'sr123';
        ip2.SVMXC__Product__c = pro.id;
        ip2.SVMXC__Serial_Lot_Number__c = 'sr123';
        ip2.SVMXC__Top_Level__c = ip1.id;
        ip2.SVMXC__Parent__c =  ip1.id;
        insert ip2;

        SVMXC__PM_Plan_Template__c pmPlanTemp = new SVMXC__PM_Plan_Template__c();
        pmPlanTemp.Name = 'pnPlanTemp';
        insert pmPlanTemp;
              
        SVMXC__PM_Applicable_Product__c pmApplcablProd = new SVMXC__PM_Applicable_Product__c();
        pmApplcablProd.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmApplcablProd.PM_Group__c  = 'testGroup';
        pmApplcablProd.SVMXC__Product_Line__c = 'Accessory';
        insert pmApplcablProd;
        
        ERP_Partner_Association__c ea = new ERP_Partner_Association__c();
        ea.ERP_Partner_Number__c = 'sitePartner';
        ea.Partner_Function__c = 'Z1=Site Partner';
        insert ea;   
        
        SVMXC__PM_Plan__c pmPlan = new SVMXC__PM_Plan__c();
        pmplan.name = 'testPlan';
        pmplan.SVMXC__Service_Contract__c = servcContrct2.id;  
        pmplan.SVMXC__End_Date__c =  System.today()+7;   
        pmplan.SVMXC__Status__c = 'Active';
        pmplan.Top_Level__c = ip1.id;
        pmplan.SVMXC__Frequency__c = 20;
        pmplan.PM_Group__c = 'testGroup';
        insert pmPlan;  
        
        SVMXC__PM_Plan__c pmPlan1 = new SVMXC__PM_Plan__c();
        pmPlan1.name = 'testPlan1';
        pmPlan1.SVMXC__Service_Contract__c = servcContrct.id;  
        pmPlan1.SVMXC__End_Date__c =  System.today()+7;   
        pmPlan1.SVMXC__Status__c = 'Active';
        pmPlan1.Top_Level__c = ip1.id;
        pmPlan1.SVMXC__Frequency__c = 20;
        pmPlan1.PM_Group__c = 'testGroup';
        insert pmPlan1;  
        
        SVMXC__Task_Template__c stt = new SVMXC__Task_Template__c();
        insert stt;

        SVMXC__PM_Schedule_Template__c pmSchdlTemp = new SVMXC__PM_Schedule_Template__c();
        pmSchdlTemp.SVMXC__Frequency_Unit__c = 'Months';
        pmSchdlTemp.SVMXC__Recurring__c = true;
        pmSchdlTemp.SVMXC__Schedule_Type__c = 'Time Based';
        pmSchdlTemp.SVMXC__Visit_Description__c = 'testVisit Desc';
        pmSchdlTemp.SVMXC__Work_Order_Purpose__c = stt.id;
        pmSchdlTemp.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmSchdlTemp.SVMXC__Frequency__c = 10;
        insert pmSchdlTemp;
        
        SVMXC__PM_Coverage__c pm = new SVMXC__PM_Coverage__c();
        pm.SVMXC__Product_Name__c = ip2.id;
        pm.SVMXC__PM_Frequency__c = 10;
        pm.SVMXC__PM_Plan__c = pmPlan1.id;
        insert pm;

        SVMXC__Service_Contract_Products__c varCP = new SVMXC__Service_Contract_Products__c ();
        varCP.SVMXC__Service_Contract__c = servcContrct.id;
        varCP.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP.SVMXC__Product__c = pro.id;
        varCP.Product_Code__c = 'pro123';
        varCP.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP.ERP_Site_Partner__c = ea.ERP_Partner_Number__c;
        varCP.SVMXC__Installed_Product__c = ip2.id; 
        varCP.SVMXC__Start_Date__c = system.today();
        varCP.SVMXC__End_Date__c = system.today()+7;       
        insert varCP;
        
        test.startTest();
        SVMXC__Service_Contract_Products__c varCP1 = new SVMXC__Service_Contract_Products__c ();
        varCP1.SVMXC__Service_Contract__c = servcContrct.id;
        varCP1.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP1.SVMXC__Product__c = pro.id;
        varCP1.Product_Code__c = 'pro123';
        varCP1.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP1.SVMXC__Start_Date__c = system.today();
        varCP1.SVMXC__End_Date__c = system.today()+7;  
        varCP1.SVMXC__Installed_Product__c = ip2.id;
        insert varCP1;
        varCP1.SVMXC__End_Date__c = system.today()+9; 
        update varCP1;
        test.stopTest();
    }


}
public class SR_CreateArticleFeedback_class {
    Public Service_Article__kav varArticle {get;set;}
    public id ArticleId{get;set;}
    Public string comment{get;set;}
    //Public string varNumber{get;set;}
    public boolean displayPopup{get;set;}   
    //public List<ProcessInstance> objProcess;
    //public list<ProcessInstanceStep> objProInstStep ;
    public Id result;
    public string owner;
    public SR_CreateArticleFeedback_class (){
        displayPopup = false;
        ArticleId = apexpages.currentpage().getparameters().get('id');
        if(ArticleId!=null){
            varArticle = [select id,ArticleNumber, RecordType.Name ,LastModifiedById, PublishStatus,Title,VersionNumber,LastPublishedDate,CreatedDate from Service_Article__kav where id =:ArticleId];
        }
    }
    
    public pagereference doSave ()
    {
        
        displayPopup = true;
        PageReference pagereferencealias ;
        Article_Feedback__c varArticleFeedback = new Article_Feedback__c ();
        if(varArticle.ArticleNumber!=null){
            varArticleFeedback.Article_Type__c = varArticle.RecordType.Name;
            varArticleFeedback.Article_Number__c = varArticle.ArticleNumber;
            varArticleFeedback.View_Article__c = URL.getSalesforceBaseUrl().toExternalForm() + '/' +varArticle.Id;
            varArticleFeedback.Type__c = 'Feedback';
            varArticleFeedback.Status__c = 'New';
            varArticleFeedback.Source__c = 'Internal';
            varArticleFeedback.Article_Title__c = varArticle.Title;
            varArticleFeedback.Article_Version__c = varArticle.VersionNumber;
            varArticleFeedback.Article_Created_Date__c = varArticle.CreatedDate;
            varArticleFeedback.Last_Published_Date__c = varArticle.LastPublishedDate;
            //varArticleFeedback.OwnerId = null;
           
           if(!String.isBlank(varArticle.PublishStatus)){
              
            varArticleFeedback.Last_Published_By__c = varArticle.LastModifiedById;
            }
            /* Commenting this code as we don't need the approver logic now 
            objProcess = new list<ProcessInstance>([SELECT Id,TargetObjectId FROM ProcessInstance WHERE TargetObjectId =:varArticle.id]);
            if(objProcess.size()>0 && objProcess!=null)
            {
                objProInstStep = new list<ProcessInstanceStep>([Select id, ActorId,StepStatus  from ProcessInstanceStep where ProcessInstanceID =:objProcess[0].id ]);
                if(objProInstStep.size()>0 && objProInstStep!=null)
                {
                    for(ProcessInstanceStep p : objProInstStep)
                    {
                        if(p.StepStatus == 'Approved')
                        {
                            owner= p.ActorId;
                        }
                        
                    }
                    if(owner != null)
                    {
                        varArticleFeedback.OwnerId = owner;
                    }
                    else
                        varArticleFeedback.OwnerId = UserInfo.getUserId();
                    
                }
                
                
            }
            else
                varArticleFeedback.OwnerId = UserInfo.getUserId();*/
          }
        varArticleFeedback.Article_Feedback__c = comment;
        Insert varArticleFeedback;
        
        /* commenting this code as we don't need the task creation 
        // US4896 : task created and assigned to approver code : added by Bushra
        result = varArticleFeedback.id;
        System.debug('@@@@@@@@@@@@'+result);
        if(objProcess.size()>0 && objProcess!=null  && owner != null)
        {
            Article_Feedback__c varAF = [Select id,Name from Article_Feedback__c where id=:result];
            Task varTask = new Task();
            
            varTask.OwnerId = owner;
            
            varTask.WhatID= varAF.id;
            varTask.Status = 'Not Started';
            varTask.Priority = 'Normal';
            varTask.Subject = 'Please review the Article Feedback given by Customer'+' : '+varAF.Name;
            insert varTask;
            
        }*/
        // code for task creation ends here
        pagereferencealias = new PageReference('/'+varArticle.Id);
        pagereferencealias.setRedirect(true);
        return null;
        
    }
    public pagereference closePopup()
        
    {     
        displayPopup = false; 
        PageReference pagereferencealias1 ;     
        pagereferencealias1 = new PageReference('/'+varArticle.Id);
        pagereferencealias1.setRedirect(true);
        return pagereferencealias1; 
    }    
    
}
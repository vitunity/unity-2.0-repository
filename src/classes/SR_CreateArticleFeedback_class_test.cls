@isTest
public class SR_CreateArticleFeedback_class_test
{/*
    public static testMethod void ArticleTestmethod()
    {
    Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User u1= [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        Service_Article__kav objArt = new Service_Article__kav (Title = 'abcTest', Summary = 'test data for article feedback class', UrlName = 'abcTest');
        insert objArt;
        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(objArt.id);
        
        Account a = new Account();
        a.Name = 'acc';
        a.Country__C = 'India';
        insert a;
                
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(u1.Id); 
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});// added by Yukti
        
         // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 = 
            new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving request.');
        req2.setAction('Approve');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));
        
        Task tsk =  new Task();
        tsk.CallType = '';
        tsk.WhatId = a.Id;
        insert tsk;
        
        // Submit the request for approval
        Approval.ProcessResult result2 =  Approval.process(req2);
        PageReference myVfPage = Page.SR_CreateArticleFeedback;
        Test.setCurrentPage(myVfPage);
         ApexPages.currentPage().getParameters().put('id',objArt.id);
         SR_CreateArticleFeedback_class testController = new SR_CreateArticleFeedback_class();
         //testController.comment = 'test';
       testController.doSave();
         testController.closePopup();
        
    }
    */
     public static testMethod void ArticleTestmethod2()
    {
        Service_Article__kav objArt = new Service_Article__kav (Title = 'abcTest1', Summary = 'test data for article feedback class', UrlName = 'abcTest1');
        insert objArt;
        PageReference myVfPage = Page.SR_CreateArticleFeedback;
        Test.setCurrentPage(myVfPage);
         ApexPages.currentPage().getParameters().put('id',objArt.id);
         SR_CreateArticleFeedback_class testController = new SR_CreateArticleFeedback_class();
         testController.comment = 'test';
        testController.doSave();
         testController.closePopup();
        
    }
    
}
/*************************************************************************\
    @ Author        : Ritika Kaushik
    @ Date      : 20-Aug-2013
    @ Description   : Class handling inbound email and create article.
    @ Last Modified By  :   Ritika Kaushik
    @ Last Modified On  :    29-Aug-2013
    @ Last Modified Reason  Nikita Gupta
    @ Last Modified On  :    2-April-2014
    @ Last Modified Reason  :  US4295, To set appover1 when creating article from email
****************************************************************************/

global class SR_CreateArticlebyEmail implements Messaging.InboundEmailHandler {
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
 {
     Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
     if(email.subject !='' && email.subject!=null)//check subject should not be blank
      {
          String mailSubject = email.subject; 
          system.debug('mailSubject >>>>>>>>'+mailSubject );
           //If(mailSubject.trim().containsIgnoreCase('XIT'))//commented by Ritika
          // {
            try
            {
                Service_Article__kav objServiceArt = new Service_Article__kav();//create a new service article
                objServiceArt.Title= mailSubject ;
                objServiceArt.ValidationStatus = 'In Reviewed';
                objServiceArt.Approval_Type__c = '1 Approver'; //US4295, Added by Nikita
                objServiceArt.Article_Image__c=email.htmlBody ;//  email.plainTextBody;              
                objServiceArt.UrlName = mailSubject.trim().replace(' ','-').replace(':','-');
                system.debug('Url Name>>>>>'+objServiceArt.UrlName);
                insert objServiceArt; 
                SYSTEM.DEBUG('++++++++++++++++++++++++++'+objServiceArt);                
                SYSTEM.DEBUG('++++++++++++++++++++++++++'+objServiceArt.ArticleNumber);
                
           }
            catch (System.DmlException e)
            {
                System.debug('ERROR: Not able to create article: ' + e);
            }
          // }     
      } 
      return result;
  }
}
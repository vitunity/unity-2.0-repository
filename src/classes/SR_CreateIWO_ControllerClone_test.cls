@isTest(SeeAllData=true)
public class SR_CreateIWO_ControllerClone_test
{
    static SVMXC__Service_Order__c workOrdObject;
    static SVMXC__Service_Group__c ServiceTeam;
    static Id recTech = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    static Id rec = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
    static Id rectypeIdTraining;
    static Id rectypeIdPSProfessional_Services;
    static Id rectypeIdConsulting;
    static SVMXC__Service_Order_Line__c TestWD;
    static Case testcase;
    static Case testcase1;
    static SVMXC__Dispatcher_Access__c TestDA;
    static SVMXC__Service_Group_Members__c tech;
    static SVMXC__Service_Group_Members__c tech1;
    static SVMXC__Service_Group_Members__c tech2;    
    static Product2 testProd;
    static Technician_Assignment__c TestTechAssignment1;
    static Technician_Assignment__c TestTechAssignment;
    static SVMXC__SVMX_Event__c sev;
    static SVMXC__Site__c TestLoc;
    static SVMXC__Installed_Product__c TestIP;
    static Sales_Order_Item__c TestSOI ;
    static ERP_WBS__c TestERPWbs;
    static ERP_NWA__c nwa;
    static ERP_Project__c erpproject;
    static Profile sysadminprofile; 
    public static ID TechRecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();  
    public static ID hdRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();  
    public static profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
    public static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname='Testing',languagelocalekey='en_US', localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles', username='standardtestuse92@testclass.com');
    public static Account objAcc;
    public static Contact objCont;
    public static product2 objProd;
    public static SVMXC__Installed_Product__c objIns; 
    public static Case objCase;
    public static SVMXC__Service_Level__c slaTerm ;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static SVMXC__Service_Group_Members__c ObjTech;
    public static SVMXC__Case_Line__c cl1;
    public static SVMXC__Case_Line__c cl2;
    public static Sales_Order_Item__c soi;
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    public static Id recServiceTeam = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId(); 
    public static List<SVMXC__Service_Group_Members__c> techs = new List<SVMXC__Service_Group_Members__c>();

    public static User systemuser
    {
        set;
        get
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            return thisUser;
        }
    }
    static
    {
        //create country
        Country__c con = new Country__c(Name = 'United States');
        con.SAP_Code__c = 'US';
        insert con;

        objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA',State__C = 'California', BillingCity = 'Santa Clara',country__c = 'United States', BillingState = 'CA');
        insert objAcc;

        //insert Contact Record
        //objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
        //insert objCont;


        ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.ERP_Reference__c='TestExternal';
        ServiceTeam.SVMXC__Group_Code__c ='TestExternal';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.district_manager__c = systemuser.id;
        ServiceTeam.recordtypeId = recServiceTeam;
        Insert ServiceTeam;
        
        TestDA = new SVMXC__Dispatcher_Access__c();
        TestDA.SVMXC__Dispatcher__c = systemuser.id;
        TestDA.SVMXC__Service_Team__c =ServiceTeam.id ;
        insert TestDA;
        
        //List<SVMXC__Service_Group_Members__c> techs = new List<SVMXC__Service_Group_Members__c>();
        tech = new SVMXC__Service_Group_Members__c();
        tech.SVMXC__Service_Group__c = ServiceTeam.id;
        tech.User__c = systemuser.id ;
        tech.ERP_Reference__c = 'TextExternal';
        tech.Name = 'TestTechnician';
        tech.SVMXC__Average_Drive_Time__c = 2.00;
        tech.SVMXC__Average_Speed__c = 2.00;
        tech.SVMXC__Break_Duration__c = 2.00;
        tech.SVMXC__Break_Type__c = 'Fixed';
        tech.SVMXC__City__c = 'Noida';
        tech.SVMXC__Country__c = 'India'; 
        tech.Current_End_Date__c = system.today();
        tech.Dispatch_Queue__c = 'DISP - AMS';
        //tech.Docusign_Recipient__c = 'DISP - AMS';
        tech.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech.SVMXC__State__c = 'UP';
        tech.SVMXC__Street__c = 'abc';
        tech.SVMXC__Zip__c = '201301';
        tech.recordtypeId = recTech;
        tech.SVMXC__Salesforce_User__c = systemuser.id;
        tech.SVMXC__Select__c = false;
        techs.add(tech);
        tech1 = new SVMXC__Service_Group_Members__c();
        tech1.SVMXC__Service_Group__c = ServiceTeam.id;
        tech1.User__c = systemuser.id ;
        tech1.ERP_Reference__c = 'TextExternal';
        tech1.Name = 'TestTechnician';
        tech1.SVMXC__Average_Drive_Time__c = 2.00;
        tech1.SVMXC__Average_Speed__c = 2.00;
        tech1.SVMXC__Break_Duration__c = 2.00;
        tech1.SVMXC__Break_Type__c = 'Fixed';
        tech1.SVMXC__City__c = 'Noida';
        tech1.SVMXC__Country__c = 'India'; 
        tech1.Current_End_Date__c = system.today();
        tech1.Dispatch_Queue__c = 'DISP - AMS';
        //tech1.Docusign_Recipient__c = 'DISP - AMS';
        tech1.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech1.SVMXC__State__c = 'UP';
        tech1.SVMXC__Street__c = 'abc';
        tech1.SVMXC__Zip__c = '201301';
        tech1.recordtypeId = recTech;
        tech1.SVMXC__Salesforce_User__c = systemuser.id;
        tech1.SVMXC__Select__c = false;
        techs.add(tech1);
        tech2 = new SVMXC__Service_Group_Members__c();
        tech2.SVMXC__Service_Group__c = ServiceTeam.id;
        tech2.User__c = systemuser.id ;
        tech2.ERP_Reference__c = 'TextExternal';
        tech2.Name = 'TestTechnician';
        tech2.SVMXC__Average_Drive_Time__c = 2.00;
        tech2.SVMXC__Average_Speed__c = 2.00;
        tech2.SVMXC__Break_Duration__c = 2.00;
        tech2.SVMXC__Break_Type__c = 'Fixed';
        tech2.SVMXC__City__c = 'Noida';
        tech2.SVMXC__Country__c = 'India'; 
        tech2.Current_End_Date__c = system.today();
        tech2.Dispatch_Queue__c = 'DISP - AMS';
        //tech2.Docusign_Recipient__c = 'DISP - AMS';
        tech2.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech2.SVMXC__State__c = 'UP';
        tech2.SVMXC__Street__c = 'abc';
        tech2.SVMXC__Zip__c = '201301';
        tech2.recordtypeId = recTech;
        tech2.SVMXC__Salesforce_User__c = systemuser.id;
        tech2.SVMXC__Select__c = false;        
        techs.add(tech2);
        insert techs;

        testProd = new Product2();
        testProd.Name = 'tstProd';
        testProd.Material_Group__c= 'H:TRNING';
        testProd.Budget_Hrs__c =10.0;
        testProd.Credit_s_Required__c=10;
        testProd.UOM__c='EA';
        testProd.ProductCode ='AABB';
        testProd.Training_Region__c = 'NA';
        insert testProd;      
        
        //Recordtype woRecordTypeIEM  = [select id,developername,name from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Implementation' ];
        Id id = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); 
        
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name = 'sdddsds';
        so.Block__c = '70-Order Cancellation';
        insert so;

        soi = new Sales_Order_Item__c(Status__c = 'Open');
        soi.Sales_Order__c = so.Id;
        soi.Reject_Reason__c = 'Cancelled';
        soi.WBS_Element__c='erpwbstest';
        soi.ERP_Equipment_Number__c = '12345678';
        insert soi;
        
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        TestLoc.SVMXC__Street__c = 'Test Street';
        TestLoc.SVMXC__Country__c = 'United States';
        TestLoc.Sales_Org__c = '0600';
        TestLoc.SVMXC__Zip__c = '98765';
        TestLoc.ERP_Functional_Location__c = 'Test Location';
        TestLoc.Plant__c = 'Test Plant';
        TestLoc.ERP_Site_Partner_Code__c = '123456';
        TestLoc.SVMXC__Location_Type__c = 'Depot';
        TestLoc.country__c = con.id;
        //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
        TestLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
        //objLoc.ERP_Org__c = objOrg.id;
        TestLoc.SVMXC__Stocking_Location__c = true;
        TestLoc.SVMXC__Account__c = objAcc.id;
        Insert TestLoc;

        TestIP = new SVMXC__Installed_Product__c();
        TestIP.Name = 'TestIP';
        TestIP.SVMXC__Site__c = TestLoc.id;
        TestIP.ERP_Functional_Location__c = 'USA';
        TestIP.SVMXC__Serial_Lot_Number__c ='test';
        testIP.ERP_Reference__c = 'H12345';
        testIP.SVMXC__Company__c = objAcc.id;
        testIP.SVMXC__Product__c = testprod.id;
        testIP.SVMXC__Preferred_Technician__c = techs[0].id;
        Insert TestIP;

        testcase = SR_testdata.createcase();
        testcase.Priority = 'Medium';
        testcase.ProductSystem__c =TestIP.id;
        testcase.AccountId = objAcc.id;
        //testcase.ContactId = objCont.id;
        Insert testcase ;
        
        workOrdObject =  SR_testdata.createServiceOrder();
        workOrdObject.SVMXC__Case__c = testcase.id;
        workOrdObject.Event__c = True;
        workOrdObject.SVMXC__Company__c = objAcc.Id;
        workOrdObject.ownerID = systemuser.id;
        workOrdObject.SVMXC__Order_Status__c = 'open';
        workOrdObject.SVMXC__Preferred_Technician__c = techs[0].id;
        workOrdObject.SVMXC__Top_Level__c = TestIP.id;
        workOrdObject.recordtypeid = rec;
        workOrdObject.SVMXC__Product__c = testcase.Productid;
        workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
        workOrdObject.SVMXC__Preferred_End_Time__c = system.now()+1;
        workOrdObject.SVMXC__Preferred_Start_Time__c  = system.now();
        workOrdObject.ProjectManager__c = systemuser.id;
        workOrdObject.Subject__c= 'bbb5';
        workOrdObject.Installation_Manager__c = systemuser.id;
        workOrdObject.SVMXC__Product__c = testProd.id;
        workOrdObject.Internal_Comment__c = 'ABCD';
        workOrdObject.Service_Team__c = ServiceTeam.id;
        workOrdObject.SVMXC__Service_Group__c = ServiceTeam.id;
        workOrdObject.SVMXC__Group_Member__c = tech.id;
        workordObject.Sales_order__c = so.id;
        workordObject.sales_order_Item__C = soi.id;
        workordObject.Material_Description__c = 'H:TRNING';
        workordObject.SVMXC__Street__c = '2006';
        workordObject.SVMXC__City__c = 'Santa Clara';
        workordObject.SVMXC__State__c = 'CA';
        workordObject.SVMXC__Zip__c = '95051';
        workordObject.SVMXC__Country__c = 'USA';
        workordObject.Service_Super_Region__c = 'NorthAmerica';
        workordObject.Region__c = 'USCA';
        Insert workOrdObject;


        TestTechAssignment = new Technician_Assignment__c();
        TestTechAssignment.Work_Order__c = workOrdObject.id;
        TestTechAssignment.Technician_Name__c = techs[1].id;
        Insert TestTechAssignment;
        
        sev  = new SVMXC__SVMX_Event__c();
        sev.SVMXC__Service_Order__c = workOrdObject.Id;
        sev.SVMXC__WhatId__c = workOrdObject.Id;
        sev.SVMXC__Technician__c = techs[0].id;
        sev.SVMXC__StartDateTime__c = System.now();
        sev.SVMXC__EndDateTime__c = System.now().addDays(1);
        //insert sev; 
    }
    
    static testMethod void SR_CreateIWO_CreateInstallationWO1() 
    {
       Test.startTest();
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        Insert TestLoc;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;

        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = 'erpwbstest');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        

        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+1, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl1;

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+4, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl2;

        controller.CreateInstallationWO();
        controller.convertMonthNameToNumber('Jan');
        controller.convertMonthNameToNumber('Feb');
        controller.convertMonthNameToNumber('Mar');
        controller.convertMonthNameToNumber('Apr');
        controller.convertMonthNameToNumber('May');
        controller.convertMonthNameToNumber('Jun');
        controller.convertMonthNameToNumber('Jan');
        controller.convertMonthNameToNumber('Jul');
        controller.convertMonthNameToNumber('Aug');
        controller.convertMonthNameToNumber('Sep');
        controller.convertMonthNameToNumber('Oct');
        controller.convertMonthNameToNumber('Nov');
        controller.convertMonthNameToNumber('Dec');
        controller.convertMonthIntNumberToName(1);
        controller.convertMonthIntNumberToName(2);
        controller.convertMonthIntNumberToName(3);
        controller.convertMonthIntNumberToName(4);
        controller.convertMonthIntNumberToName(5);
        controller.convertMonthIntNumberToName(6);
        controller.convertMonthIntNumberToName(7);
        controller.convertMonthIntNumberToName(8);
        controller.convertMonthIntNumberToName(9);
        controller.convertMonthIntNumberToName(10);
        controller.convertMonthIntNumberToName(11);
        controller.convertMonthIntNumberToName(12);
        //controller.convertMonthIntNumberToName(1);
        Test.stopTest();
    }

    static testMethod void SR_CreateIWO_CreateInstallationWO8() 
    {   
   
        Test.startTest();

        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        system.debug('==erpproject=='+erpproject);

        TestERPWbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = 'erpwbstest');
        insert TestERPWbs;
        system.debug('==erpwbs=='+TestERPWbs);
        nwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = TestERPWbs.id, ERP_Project_Nbr__c='fefew223');
        insert nwa;
        system.debug('==erpnwa=='+nwa);

        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=TestERPWbs.Id, ERP_NWA__c = nwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+1, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl1;
        system.debug('==cl1=='+cl1);
        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=TestERPWbs.Id,  ERP_NWA__c = nwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+4, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl2;
        system.debug('==cl2=='+cl2);


        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        controller.searchString = 'acme';
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);  
        
        controller.ListWrapTechnician.add(tempw);
        controller.ListWrapTechnician.add(tempw1);  
        controller.AddTechnicians();

        //TestLoc = new SVMXC__Site__c();
        //TestLoc.Name = 'LOc1';
        //TestLoc.Sales_Org__c = '0600';
        //Insert TestLoc;
        system.debug('==TestLoc=='+TestLoc);
        string strDateTime = string.valueof(System.now());
      
        controller.constructDateTimeGMT(strDateTime);
        controller.constructDateTimeGMT('10/10/2017 00:00:00 PM');
        controller.constructDateTimeGMT('10/10/2017 12:00:00 AM');
        controller.constructDateTimeGMT('10-10-2017 00:00:00 PM');
        controller.constructDateTimeGMT('10-10-2017 12:00:00 PM');
      
        
        controller.CreateInstallationWO();
    
        Test.stopTest();
        //SVMXC__Service_Order__c iwo = [select id, sales_org__c, erp_org_lookup__c from SVMXC__Service_Order__c where SVMXC__Case__c = :testcase.id  and Event__c = false];
        //system.assertNotEquals(null,iwo.Sales_Org__c);
        //system.assertNotEquals(null,iwo.erp_Org_lookup__c);
    }
    
    static testMethod void SR_CreateIWO_OnLoad() 
    {
        Test.startTest();
        Datetime stdt = system.now().addMinutes(-15);
        Datetime eddt = system.now().addMinutes(-5);
        Datetime stdt1 = system.now();
        Datetime eddt1 = system.now().addMinutes(+5);
        
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl1);

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt1,End_date_time__c = eddt1, SVMXC__Installed_Product__c = TestIP.id, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl2);
        //insert cllst;   
        //objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
        //insert objCont;
        ID Rec_helpdesk = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();  
        SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c(RecordTypeId = Rec_helpdesk,SVMXC__Case__c= testcase.id,SVMXC__Company__c = objAcc.Id, SVMXC__Priority__c = 'Medium', SVMXC__Group_Member__c = tech.Id, event__c = false, Parent_WO__c = workOrdObject.id,Sales_Org__c='0600');
        insert objWO;
        Apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        controller.nonInstallation = false;
    

        
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,false);
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);   
        controller.OnLoad();
        Test.stopTest();
    }  
    
    static testMethod void SR_CreateIWO_SetYes() 
    {
        Test.startTest();
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+1,SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.Id);
        cl1.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        cl1.SVMXC__Product__c = testprod.id;
        //cl1.SVMXC__Installed_Product__c = TestIP.id;
        insert cl1;

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+4, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.Id);
        cl2.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        cl2.SVMXC__Product__c = testprod.id;
        //cl2.SVMXC__Installed_Product__c = TestIP.id;
        insert cl2;
       
        
        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        controller.nonInstallation = true;
        
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,false);
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);
        
       
        controller.SetYes();
        Test.stopTest();
    }
    static testMethod void SR_CreateIWO_SetNo() 
    {
        test.startTest();
        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        controller.nonInstallation = true;
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,false);
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);  
        controller.SetNo();
        test.stopTest();
    }

    static testMethod void SR_CreateIWO_SearchFilter() 
    {
        Test.startTest();
        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        controller.nonInstallation = false; 
        System.runAs (systemuser)
        {
            controller.getServiceTeam();            
        }
        controller.ServiceTeamPicklistVal = '[' + ServiceTeam.id + ']';
        controller.SearchFilter();
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);   
        controller.resetServiceTeam();
        controller.ListWrapTechnician.add(tempw);
        controller.ListWrapTechnician.add(tempw1);  
        controller.AddTechnicians();
        
        controller.CreateInstallationWO();
        Test.stopTest();
    }
    
    static testMethod void SR_CreateIWO_CreateInstallationWO3() 
    {
        
        Id woRecordTypeIEM  = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('IEM').getRecordTypeId();
        workOrdObject.RecordTypeId = woRecordTypeIEM;
        workOrdObject.SVMXC__Top_Level__c = testIp.id;
        update workOrdObject;
        Test.startTest();        
        List<SVMXC__Service_Order__c> orders = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c workOrdObject1 =  SR_testdata.createServiceOrder();
        workOrdObject1.SVMXC__Case__c = testcase.id;
        workOrdObject1.Event__c = True;
        workOrdObject1.SVMXC__Top_Level__c = testip.id;
        workOrdObject1.SVMXC__Company__c = testcase.Accountid;
        workOrdObject1.ownerID = systemuser.id;
        workOrdObject1.SVMXC__Order_Status__c = 'open';
        workOrdObject1.SVMXC__Preferred_Technician__c = tech.id;
        workOrdObject1.recordtypeid = woRecordTypeIEM;
        workOrdObject1.SVMXC__Product__c = testcase.Productid;
        workOrdObject1.SVMXC__Problem_Description__c = 'Test Description';
        workOrdObject1.SVMXC__Preferred_End_Time__c = system.now()+1;
        workOrdObject1.SVMXC__Preferred_Start_Time__c  = system.now();
        workOrdObject1.Subject__c= 'bbb3';
        orders.add(workOrdObject1);
        SVMXC__Service_Order__c workOrdObject2 =  SR_testdata.createServiceOrder();
        workOrdObject2.SVMXC__Case__c = testcase.id;
        workOrdObject2.Event__c = True;
        workOrdObject2.SVMXC__Top_Level__c = testip.Id;
        workOrdObject2.SVMXC__Company__c = testcase.Accountid;
        workOrdObject2.ownerID = systemuser.id;
        workOrdObject2.SVMXC__Order_Status__c = 'open';
        workOrdObject2.SVMXC__Preferred_Technician__c = tech.id;
        workOrdObject2.recordtypeid = woRecordTypeIEM;
        workOrdObject2.SVMXC__Product__c = testcase.Productid;
        workOrdObject2.SVMXC__Problem_Description__c = 'Test Description';
        workOrdObject2.SVMXC__Preferred_End_Time__c = system.now()+1;
        workOrdObject2.SVMXC__Preferred_Start_Time__c  = system.now();
        workOrdObject2.Subject__c= 'bbb4';
        orders.add(workOrdObject2);
        insert orders;
        delete TestTechAssignment;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;
        SR_CreateIWO_ControllerClone.WrapperClass tempw = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true);
        tempw.evnt = sev;
        SR_CreateIWO_ControllerClone.WrapperClass tempw1 = new SR_CreateIWO_ControllerClone.WrapperClass(tech,true,true); 
        tempw1.evnt = sev;  
        controller.ListWrapTechnician.add(tempw);
        controller.ListWrapTechnician.add(tempw1);  
        controller.AddTechnicians();
        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = 'erpwbstest');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        
        Datetime stdt = system.now().addMinutes(-15);
        Datetime eddt = system.now().addMinutes(-5);
        Datetime stdt1 = system.now();
        Datetime eddt1 = system.now().addMinutes(+5);
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id,  ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.Id);
        cllst.add(cl1);

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id,  ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt1,End_date_time__c = eddt1, SVMXC__Installed_Product__c = TestIP.id, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.Id);
        cllst.add(cl2);
        insert cllst;

        controller.CreateInstallationWO();
        system.assertNotEquals(workOrdObject.SVMXC__Service_Group__c,null);
        //system.assertNotEquals(workOrdObject.Installation_Review_Queue__c,null);
        //system.assertNotEquals(workOrdObject.Queue__c,null);
        system.assertNotEquals(workOrdObject.SVMXC__Service_Group__c,null);
        Test.stopTest();
    }
    
    static testMethod void SR_CreateIWO_CreateInstallationWO5() 
    {
       Test.startTest();
        rectypeIdTraining = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Consulting').getRecordTypeId();
        workOrdObject.RecordTypeId = rectypeIdTraining;
        workordObject.ERP_Contract_Nbr__c = '40164742';
        update workOrdObject;
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        Insert TestLoc;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;

        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = 'erpwbstest');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        

        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+1, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl1;

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = system.now(),End_date_time__c = system.now()+4, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        insert cl2;

        controller.CreateInstallationWO();
        controller.convertMonthNameToNumber('Jan');
        controller.convertMonthNameToNumber('Feb');
        controller.convertMonthNameToNumber('Mar');
        controller.convertMonthNameToNumber('Apr');
        controller.convertMonthNameToNumber('May');
        controller.convertMonthNameToNumber('Jun');
        controller.convertMonthNameToNumber('Jan');
        controller.convertMonthNameToNumber('Jul');
        controller.convertMonthNameToNumber('Aug');
        controller.convertMonthNameToNumber('Sep');
        controller.convertMonthNameToNumber('Oct');
        controller.convertMonthNameToNumber('Nov');
        controller.convertMonthNameToNumber('Dec');
        controller.convertMonthIntNumberToName(1);
        controller.convertMonthIntNumberToName(2);
        controller.convertMonthIntNumberToName(3);
        controller.convertMonthIntNumberToName(4);
        controller.convertMonthIntNumberToName(5);
        controller.convertMonthIntNumberToName(6);
        controller.convertMonthIntNumberToName(7);
        controller.convertMonthIntNumberToName(8);
        controller.convertMonthIntNumberToName(9);
        controller.convertMonthIntNumberToName(10);
        controller.convertMonthIntNumberToName(11);
        controller.convertMonthIntNumberToName(12);
        //controller.convertMonthIntNumberToName(1);
        Test.stopTest();
    }
    
    static testMethod void SR_CreateIWO_CreateInstallationWO9() 
    {
        Test.startTest();
        rectypeIdTraining = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Training').getRecordTypeId();
        workOrdObject.RecordTypeId = rectypeIdTraining;

        update workOrdObject;
        
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        Insert TestLoc;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);

        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;

        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = '1234zxcv');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        
        Datetime stdt = system.now().addMinutes(-15);
        Datetime eddt = system.now().addMinutes(-5);
        Datetime stdt1 = system.now();
        Datetime eddt1 = system.now().addMinutes(+5);
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl1);

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id,  ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt1,End_date_time__c = eddt1, SVMXC__Installed_Product__c = TestIP.id, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl2);
        insert cllst;

        controller.CreateInstallationWO();
        Test.stopTest();
    }
    
    
    static testMethod void SR_CreateIWO_CreateInstallationWO10() 
    {
        Test.startTest(); 
        rectypeIdPSProfessional_Services = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Professional Services').getRecordTypeId();
        workOrdObject.RecordTypeId = rectypeIdPSProfessional_Services;
        update workOrdObject;
          
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        Insert TestLoc;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);
        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;

        Datetime stdt = system.now().addMinutes(-15);
        Datetime eddt = system.now().addMinutes(-5);
        Datetime stdt1 = system.now();
        Datetime eddt1 = system.now().addMinutes(+5);
        
        
        
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = '1234zxcv');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        //delete sev;
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl1);

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id,  ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt1,End_date_time__c = eddt1, SVMXC__Installed_Product__c = TestIP.id, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl2);
        insert cllst;

        controller.CreateInstallationWO();
        Test.stopTest();
    }
    
    static testMethod void SR_CreateIWO_CreateInstallationWO11() 
    {
        Test.startTest();
        TestLoc = new SVMXC__Site__c();
        TestLoc.Name = 'LOc1';
        TestLoc.Sales_Org__c = '0600';
        Insert TestLoc;

        apexpages.currentpage().getparameters().put('id',workOrdObject.id);

        SR_CreateIWO_ControllerClone controller = new SR_CreateIWO_ControllerClone();
        controller.WorkOrdId = workOrdObject.id;

        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = '1234zxcv');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        
        Datetime stdt = system.now().addMinutes(-15);
        Datetime eddt = system.now().addMinutes(-5);
        Datetime stdt1 = system.now();
        Datetime eddt1 = system.now().addMinutes(+5);
        list<SVMXC__Case_Line__c> cllst = new List<SVMXC__Case_Line__c>();
        cl1 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt, SVMXC__Installed_Product__c = TestIP.id,  SVMXC__Product__c =  testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl1);

        cl2 = new SVMXC__Case_Line__c(SVMXC__Case__c = testcase.id, ERP_WBS__c=erpwbs.Id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt1,End_date_time__c = eddt1, SVMXC__Installed_Product__c = TestIP.id, SVMXC__Product__c = testProd.id, Sales_Order_Item__c = soi.id,SVMXC__Location__c = TestLoc.id);
        cllst.add(cl2);
        insert cllst;

        controller.CreateInstallationWO();
        Test.stopTest();
    }
}
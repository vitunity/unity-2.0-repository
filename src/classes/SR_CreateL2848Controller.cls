/*************************************************************************\
    @ Author        : Ritika Kaushik
    @ Date      : 05-Sep-2013
    @ Description   : Class for creating a new L2848 record from the Complaint
    @ Last Modified By  :   Ritika Kaushik
    @ Last Modified On  :    06-Sep-2013
    @ Last Modified Reason  :   Adding the code for saving the record
    @ Last Modified By  : Shubham Jaiswal
    @ Last Modified On  :    30-Oct-2014
    @ Last Modified Reason  : DE1489 
    Date/Modified By Name/Task or Story or Inc # /Description of Change 
    1/16/2018 - Rakesh - STSK0013571 - Added few fields on L2848 creation from 'Create Escalated Complaint Form' Button on Case. 
    2/15/2018 - Rakesh - STSK0013902 - Display DM on creation
****************************************************************************/


public with sharing class SR_CreateL2848Controller 
{

    public L2848__c objL2848{ get; set; }
    public String ComplaintId, ComplaintEditId, L2848Id, caseID;
    User cuser;
    Id ComplaintRTId = Schema.SObjectType.L2848__c.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
    

    public SR_CreateL2848Controller(ApexPages.StandardController objComplaint) 
    { 
    cuser = [select Id,Title,Phone,Department from user where Id =: userinfo.getuserid()];
      /* ComplaintId=ApexPages.currentPage().getParameters().get('cid');
        ComplaintEditId=ApexPages.currentPage().getParameters().get('id');
        List<Complaint_Tracking__c> complaint= new list <Complaint_Tracking__c>();
        if(ComplaintId!=null)
        {
        complaint= [SELECT Case__r.Id,Case__r.Contact_Phone__c,Case__r.Description,Case__r.SVMXC__Top_Level__r.Equipment_Description__c,Work_Order__r.SVMXC__Top_Level__r.Equipment_Description__c,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c,Case__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c,Case__r.Case_Activity__c,Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,
        Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c ,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c,
        Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,Was_anyone_injured__c, Comments__c,
        Case__r.Contact.Email,Work_Order__r.SVMXC__Contact__r.Email,Work_Order__r.SVMXC__Company__r.District__c,
        Case__r.owner.Title,Work_Order__r.owner.Title,Case__r.SVMXC__Top_Level__r.SVMXC__Product__r.Name,Case__r.SVMXC__Top_Level__r.Product_Version_Build__r.Name,
        Case__r.Original_Owner__c,Work_Order__r.owner.FirstName,
        Case__r.Account.District_Manager__c,Work_Order__r.SVMXC__Company__r.District_Manager__c,
        Work_Order__r.SVMXC__Company__r.Name,Case__r.Account.Name ,Case__r.Account.ShippingCity,Case__r.Account.ShippingCountry,Case__r.Account.ShippingState,
        Case__r.Account.BillingCity,Case__r.Account.BillingCountry,Case__r.Account.BillingState,
        Case__r.Employee_Phone__c,Case__r.Phone_Number__c,Work_Order__r.SVMXC__Company__r.Phone,Case__r.Contact.Name,
        Work_Order__r.SVMXC__Contact__r.FirstName,Case__r.Account.District__c,Case__c,Work_Order__c,
        Work_Order__r.SVMXC__City__c,Case__r.City2__c,Work_Order__r.SVMXC__Country__c ,Case__r.Contact.Title,
       ,Work_Order__r.SVMXC__Top_Level__r.Name,Case__r.SVMXC__Product__r.Name,Case__r.CreatedBy.Phone,
        Case__r.Device_or_Application_Name2__c , Case__r.E_mail_Address__c ,Case__r.PCSN__c,
        Case__r.PCSN2__c ,Case__r.Service_Pack__c,Case__r.Service_Pack2__c,Work_Order__r.Name,
        Case__r.CaseNumber ,Work_Order__r.SVMXC__State__c, Case__r.State_Province2__c,Case__r.Title_Role__c, 
        Case__r.Version_Mode__c,Case__r.Version_Model2__c   FROM Complaint_Tracking__c where id =:ComplaintId ]; 
        }    
        
        if(ComplaintEditId!=null)
        {
        List<L2848__c> l2848=[Select Complaint__c from L2848__c where id=:ComplaintEditId];
        complaint= [SELECT Case__r.Id,Case__r.Contact_Phone__c,Case__r.Description,Case__r.SVMXC__Top_Level__r.Equipment_Description__c,Work_Order__r.SVMXC__Top_Level__r.Equipment_Description__c,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c,Case__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c,Case__r.Case_Activity__c,Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,
        Case__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c ,Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c,
        Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,Was_anyone_injured__c, Comments__c,
        Case__r.Contact.Email,Work_Order__r.SVMXC__Contact__r.Email,Work_Order__r.SVMXC__Company__r.District__c,
        Case__r.owner.Title,Work_Order__r.owner.Title,Case__r.SVMXC__Top_Level__r.SVMXC__Product__r.Name,Case__r.SVMXC__Top_Level__r.Product_Version_Build__r.Name,
        Case__r.Original_Owner__c,Work_Order__r.owner.FirstName,
        Case__r.Account.District_Manager__c,Work_Order__r.SVMXC__Company__r.District_Manager__c,
        Work_Order__r.SVMXC__Company__r.Name,Case__r.Account.Name ,Case__r.Account.ShippingCity,Case__r.Account.ShippingCountry,Case__r.Account.ShippingState,
        Case__r.Account.BillingCity,Case__r.Account.BillingCountry,Case__r.Account.BillingState,
        Case__r.Employee_Phone__c,Case__r.Phone_Number__c,Work_Order__r.SVMXC__Company__r.Phone,Case__r.Contact.Name,
        Work_Order__r.SVMXC__Contact__r.FirstName,Case__r.Account.District__c,Case__c,Work_Order__c,
        Work_Order__r.SVMXC__City__c,Case__r.City2__c,Work_Order__r.SVMXC__Country__c ,Case__r.Contact.Title,
        Work_Order__r.SVMXC__Top_Level__r.Name,Case__r.SVMXC__Product__r.Name,Case__r.CreatedBy.Phone,
        Case__r.Device_or_Application_Name2__c , Case__r.E_mail_Address__c ,Case__r.PCSN__c,
        Case__r.PCSN2__c ,Case__r.Service_Pack__c,Case__r.Service_Pack2__c,Work_Order__r.Name,
        Case__r.CaseNumber ,Work_Order__r.SVMXC__State__c, Case__r.State_Province2__c,Case__r.Title_Role__c, 
        Case__r.Version_Mode__c,Case__r.Version_Model2__c   FROM Complaint_Tracking__c where id =:l2848[0].Complaint__c ]; 
        }    
        for(Complaint_Tracking__c  listCompTrack : complaint)
        {
            objL2848 = new L2848__c();
            objL2848.recordTypeId = ComplaintId;
            system.debug('Was_anyone_injured__c>>>>>>'+listCompTrack.Was_anyone_injured__c);
            objL2848.Complaint__c= ComplaintId;
            objL2848.Was_anyone_injured__c = listCompTrack.Was_anyone_injured__c;
            objL2848.Complaint_Short_Description__c = listCompTrack.Comments__c;
            if(listCompTrack.Case__c!=null)//check if the complaint is created from the Case then Auto-populate the fields from case
            {                 
                if(listCompTrack.Case__r.Account.ShippingCity!=Null && listCompTrack.Case__r.Account.ShippingCountry!=Null && listCompTrack.Case__r.Account.ShippingState!=Null)
                 {
                objL2848.City__c=listCompTrack.Case__r.Account.ShippingCity;//Case__r.City2__c;
                objL2848.Country__c=listCompTrack.Case__r.Account.ShippingCountry;//Case__r.Country__c;
                objL2848.State_Province__c=listCompTrack.Case__r.Account.ShippingState;//Case__r.State_Province2__c;
                }
                else
                {
                 objL2848.City__c=listCompTrack.Case__r.Account.BillingCity;//Case__r.City2__c;
                 objL2848.Country__c=listCompTrack.Case__r.Account.BillingCountry;//Case__r.Country__c;
                 objL2848.State_Province__c=listCompTrack.Case__r.Account.BillingState;//Case__r.State_Province2__c;
                
                }
                
                objL2848.Case__c=listCompTrack.Case__r.Id;
                objL2848.Device_or_Application_Name__c= listCompTrack.Case__r.SVMXC__Top_Level__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
                objL2848.Device_or_Application_Name2__c= listCompTrack.Case__r.Device_or_Application_Name2__c ;
                objL2848.District_or_Area__c=listCompTrack.Case__r.Account.District__c;
                objL2848.E_mail_Address__c=listCompTrack.Case__r.Contact.Email;//E_mail_Address__c;
                objL2848.Employee_Phone__c=listCompTrack.Case__r.CreatedBy.Phone;
                objL2848.Name_of_Customer_Contact__c=listCompTrack.Case__r.Contact.Name;//Contact.Email;//Case__r.Contact.Name;
                objL2848.PCSN__c=listCompTrack.Case__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c;//Case__r.PCSN__c;
                objL2848.PCSN2__c=listCompTrack.Case__r.PCSN2__c ;
                objL2848.Phone_Number__c=listCompTrack.Case__r.Contact_Phone__c;//Phone_Number__c;
                objL2848.Service_Pack__c=listCompTrack.Case__r.Service_Pack__c;
                objL2848.Service_Pack2__c=listCompTrack.Case__r.Service_Pack2__c;
                objL2848.Service_Request_Notification__c=listCompTrack.Case__r.CaseNumber ;
                objL2848.Site_Name__c=listCompTrack.Case__r.Account.Name;
                
                objL2848.Title_Department__c=listCompTrack.Case__r.owner.Title;
                objL2848.Title_Role__c=listCompTrack.Case__r.Contact.Title; 
                objL2848.Varian_District_Manager_for_Site__c=listCompTrack.Case__r.Account.District_Manager__c;
                objL2848.Varian_Employee_Receiving_Info__c=listCompTrack.Case__r.Original_Owner__c;
                objL2848.Version_Model__c=listCompTrack.Case__r.SVMXC__Top_Level__r.Product_Version_Build__r.Name;
                objL2848.Version_Model2__c=listCompTrack.Case__r.Version_Model2__c;
                objL2848.When_Where_Who_What__c=listCompTrack.Case__r.Case_Activity__c;
                objL2848.Complaint_Short_Description__c=listCompTrack.Case__r.Description;
                        
            }
            else if(listCompTrack.Work_Order__c!=null)//check if the complaint is created from the WO then Auto-populate the fields from WO
            {
                objL2848.City__c=listCompTrack.Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c;//Work_Order__r.SVMXC__City__c;
                objL2848.Country__c=listCompTrack.Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c;//Work_Order__r.SVMXC__Country__c;
                objL2848.Device_or_Application_Name__c= listCompTrack.Work_Order__r.SVMXC__Top_Level__r.Equipment_Description__c;//SVMXC__Top_Level__r.Name ;  
                objL2848.District_or_Area__c=listCompTrack.Work_Order__r.SVMXC__Company__r.District__c ;     
                objL2848.E_mail_Address__c=listCompTrack.Work_Order__r.SVMXC__Contact__r.Email;                              
                objL2848.Service_Request_Notification__c=listCompTrack.Work_Order__r.Name  ;                   
                objL2848.State_Province__c=listCompTrack.Work_Order__r.SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c;//Work_Order__r.SVMXC__State__c;             
                objL2848.Name_of_Customer_Contact__c=listCompTrack.Work_Order__r.SVMXC__Contact__r.FirstName;
                objL2848.Phone_Number__c=listCompTrack.Work_Order__r.SVMXC__Company__r.Phone;
                objL2848.Site_Name__c=listCompTrack.Work_Order__r.SVMXC__Company__r.Name;
                objL2848.Varian_District_Manager_for_Site__c=listCompTrack.Work_Order__r.SVMXC__Company__r.District_Manager__c;
                objL2848.Varian_Employee_Receiving_Info__c=listCompTrack.Work_Order__r.owner.FirstName;
                objL2848.Title_Department__c=listCompTrack.Work_Order__r.owner.Title;
                objL2848.PCSN__c=listCompTrack.Work_Order__r.SVMXC__Top_Level__r.SVMXC__Serial_Lot_Number__c;
            }
        }*/
    
        //ComplaintEditId=ApexPages.currentPage().getParameters().get('id');
        /*if(ComplaintEditId!=null)
        {



            objL2848=[Select Name, Case__c, Work_Order__c, Site_Name__c, District_or_Area__c, City__c, Varian_District_Manager_for_Site__c, 
                    State_Province__c, Name_of_Customer_Contact__c, Title_Role__c, Phone_Number__c, E_mail_Address__c, 
                    Varian_Employee_Receiving_Info__c, Date_of_Event__c, Title_Department__c, Date_Reported_to_Varian__c, Employee_Phone__c,
                    Device_or_Application_Name__c, PCSN__c, If_yes_describe_sequence__c, Version_Model__c, Service_Pack__c, 
                    Device_or_Application_Name2__c, PCSN2__c, Version_Model2__c, Service_Pack2__c, Other_Device_Information__c, 
                    Transponder_Type__c, Lot__c, Frequency_in_kHz__c, Enter_Expiration_Date__c, Enter_Implant_Date__c , Was_anyone_injured__c ,
                    When_Where_Who_What__c, Immediate_correction_performed__c, Can_the_problem_be_reproduced__c , Photos_or_screenshots_collected__c,
                    SmartConnect_available__c, If_Yes_document_Gateway_info__c , Electronic_data_collected_and_posted__c ,
                    Patient_on_the_table_at_time_of_event__c, Retained_Replaced_defective_parts__c , Was_there_a_death__c, Injured_Patient__c,
                    Injured_Patient_ID__c, Age__c,
                    Enter_Age__c ,
                    Sex__c,
                    Complaint_Short_Description__c ,
                    Did_a_Physical_Injury_occur__c ,
                    PHYSICAL_INJURY_Non_Radiation_Event__c ,
                    Select_the_appropriate_injury__c ,
                    Other_Physical_Injury__c ,
                    What_anatomic_site_received_the_injury__c ,
                    Did_a_Radiation_Event_occur__c ,
                    Was_treatment_delivered_to_wrong_patient__c, 
                    If_Yes_provide_description1__c ,
                    Patient_ID1__c ,
                    Patient_ID2__c, 
                    Was_the_wrong_treatment_modality_used__c ,
                    If_Yes_provide_description__c, 
                    Was_treatment_delivered_outside_of_the_p__c, 
                    If_yes_describe_tissue_exposed_amount__c, 
                    Were_any_critical_structures_exposed_to__c, 
                    If_yes_describe_critical_structure_expo__c, 
                    Unit_of_measurement_for_tissue_exposed__c, 
                    Planned_treatment__c, 
                    Actual_Treatment__c, 
                    Anatomic_site__c, 
                    error_exceed_15_of_the_weekly_planned__c, 
                    error_exceed_10_of_total_planned_dose__c, 
                    For_Brachy_enter_unintended_dose_to__c, 
                    critical_structure_exposed__c, 
                    For_Brachy_enter_unintended_dose_V100_cc__c, 
                    For_Brachy_enter_unintended_dose_V200_cc__c, 
                    For_Brachy_enter_Planned_V100_cc__c, 
                    For_Brachy_enter_Actual_V100_cc__c, 
                    Planned_treatment1__c, 
                    Delivered_treatment__c, 
                    Was_treatment_delivered_to_wrongpatient1__c, 
                    Patient_ID1_MOE__c, 
                    Patient_ID2_MOE__c, 
                    Was_the_wrong_treatment_Regimen_used__c, 
                    If_Yes_provide_description2__c,
                    Treatment_delivered_outside_pres__c, 
                    If_Yes_provide_description3__c,
                    Error_exceed_10_of_total_planned_dose_MO__c,
                    If_Yes_provide_description4__c, 
                    Error_exceed_of_total_planned_dose__c,
                    Was_there_an_observable_effect_on_the_pa__c,  
                    injured_person_examined_by_a_Physician__c, 
                    If_yes_name_phone__c, 
                    Physician_intend_to_modify_the_therapy__c, 
                    Does_injury_change_the_prognosis__c, 
                    is_the_injury_considered_a_serious_injur__c
                    
                    from L2848__c where id=:ComplaintEditId];
        } */
         String prefix = Schema.getGlobalDescribe().get('Case').getDescribe().getKeyPrefix();

        String idVal = null;

        Map<String, String> params = ApexPages.currentPage().getParameters();





        for (String key : params.keySet()) 
        {
            if (key.startsWith('CF') && key.endsWith('lkid')) 
            {
                String val = params.get(key);

                //if (val.startsWith(prefix)) 

                idVal = key;

                //break;



            }

        }
        caseID = ApexPages.currentPage().getParameters().get(idVal);
        //l2848ID = ApexPages.currentPage().getParameters().get('id');
        
        Map<string,Case> mapCaseWithRelObject = new Map<string,Case>( [SELECT Id,Contact_Phone__c,Description,Subject,
                                                                SVMXC__Top_Level__r.Service_Team__r.SVMXC__Group_Code__c,SVMXC__Top_Level__r.Service_Team__r.District_Manager__r.Name,
                                                               
                                                                SVMXC__Top_Level__r.Equipment_Description__c,
                                                                Case_Activity__c,Account.Territory_Team__r.District_Manager__r.Name,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__State__c,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__City__c,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.SVMXC__Country__c ,
                                                                Contact.Name,
                                                                Was_anyone_injured__c,
                                                                Owner.title,
                                                                Contact.Email,
                                                                SVMXC__Top_Level__r.SVMXC__Product__r.Name,
                                                                SVMXC__Site__r.Id,
                                                                Original_Owner__c,
                                                                Is_This_a_Complaint__c,
                                                                Account.District_Manager__c,
                                                                Is_escalation_to_the_CLT_required__c ,
                                                                Account.Name ,
                                                                Account.ShippingCity,Account.ShippingCountry,
                                                                Account.ShippingState,
                                                                Account.BillingCity,Account.BillingCountry,
                                                                Account.BillingState,
                                                                Employee_Phone__c,
                                                                Phone_Number__c,
                                                                SVMXC__Top_Level__r.SVMXC__Site__r.Id,
                                                                SVMXC__Top_Level__r.Name,
                                                                Account.District__c,
                                                                City2__c,
                                                                 Contact.Title,
                                                                SVMXC__Product__r.Name,
                                                                CreatedBy.Phone,
                                                                Device_or_Application_Name2__c ,
                                                                E_mail_Address__c ,PCSN__c,
                                                                PCSN2__c ,
                                                                Service_Pack__c,Service_Pack2__c,
                                                                CaseNumber ,
                                                                SVMXC__Top_Level__r.Product_Version_Build__r.Name,
                                                                State_Province2__c,
                                                                Title_Role__c,
                                                                Version_Mode__c,Version_Model2__c, 
                                                                SVMXC__Top_Level__r.Service_Team__r.District_Manager__c
                                                                FROM Case where id = :caseID]
                                                             );


                                                            // SVMXC__Top_Level__r.Product_Version_Build__r.Name, //Wave 1C - Nikita 1/25/2015
        if(mapCaseWithRelObject.size() > 0)
        {   
            for(Case varCase : mapCaseWithRelObject.Values())
            {   
                objL2848 = new L2848__c();
                objL2848.recordTypeId = ComplaintRTId;
                if(mapCaseWithRelObject.containsKey(varCase.Id) && mapCaseWithRelObject.get(varCase.Id).Account.ShippingCity != Null && mapCaseWithRelObject.get(varCase.Id).Account.ShippingCountry != Null && mapCaseWithRelObject.get(varCase.Id).Account.ShippingState != Null) //added null check, Nikita 6/9/2014
                {
                    objL2848.City__c = varCase.Account.ShippingCity;
                    objL2848.Country__c = varCase.Account.ShippingCountry;
                    objL2848.State_Province__c = varCase.Account.ShippingState;
                }
                else
                {
                    objL2848.City__c = varCase.Account.BillingCity;//Case__r.City2__c;
                    objL2848.Country__c = varCase.Account.BillingCountry;//Case__r.Country__c;
                    objL2848.State_Province__c = varCase.Account.BillingState;//Case__r.State_Province2__c;                
                }
                
                system.debug('CaseId>>'+varCase.Id);
                objL2848.Case__c = varCase.Id;
                objL2848.Top_Level_Installed_Product__c = varCase.SVMXC__Top_Level__r.Id; //Product/System
                objL2848.Device_or_Application_Name__c = varCase.SVMXC__Top_Level__r.SVMXC__Product__r.Name; //Case__r.Product.Name;
                objL2848.Device_or_Application_Name2__c = varCase.Device_or_Application_Name2__c ;
                objL2848.District_or_Area__c = varCase.SVMXC__Top_Level__r.Service_Team__r.SVMXC__Group_Code__c;//varCase.Account.CSS_District__c; //District__c;
                objL2848.E_mail_Address__c = varCase.Contact.Email;          //contact.Email;
                //objL2848.Employee_Phone__c = varCase.CreatedBy.Phone;
                objL2848.Name_of_Customer_Contact__c = varCase.Contact.Name;        //Case__r.Contact.Name;
                objL2848.PCSN__c = varCase.SVMXC__Top_Level__r.Name;//Case__r.PCSN__c;
                objL2848.PCSN2__c = varCase.PCSN2__c ;
                objL2848.Phone_Number__c = varCase.Contact_Phone__c;//Phone_Number__c;
                objL2848.Service_Pack__c = varCase.Service_Pack__c;
                objL2848.Service_Pack2__c = varCase.Service_Pack2__c;
                objL2848.Service_Request_Notification__c = varCase.CaseNumber ;
                objL2848.Location__c = varCase.SVMXC__Top_Level__r.SVMXC__Site__r.Id;
                objL2848.Site_Name__c = varCase.Account.Name;
                objL2848.Title_Department__c = cuser.Title+ ' / '+ cuser.Department;
                objL2848.Varian_Employee__c= cuser.Id;
                objL2848.Employee_Phone__c = cuser.Phone;
                objL2848.Title_Role__c = varCase.Contact.Title;
                //objL2848.Varian_District_Manager_for_Site__c = varCase.Account.Territory_Team__r.District_Manager__r.Name;//Account.District_Manager__c; // replaced by below line for DE1489
                objL2848.Varian_District_Manager_for_Site__c = varCase.SVMXC__Top_Level__r.Service_Team__r.District_Manager__r.Name;//Account.District_Manager__c; 
                objL2848.Varian_Employee_Receiving_Info__c = varCase.Original_Owner__c;
                objL2848.Version_Model__c = varCase.SVMXC__Top_Level__r.Product_Version_Build__r.Name; 
                objL2848.Version_Model2__c = varCase.Version_Model2__c;
                objL2848.When_Where_Who_What__c = varCase.Case_Activity__c;
                objL2848.Complaint_Short_Description__c = varCase.Subject;
                objL2848.Facts_Details_of_Complaint__c = varCase.Description;
                objL2848.Date_of_Event__c = System.Today();
                //objL2848.Date_Only_Reported_to_Varian__c = System.Today();
                objL2848.Was_anyone_injured__c = varCase.Was_anyone_injured__c;
                objL2848.District_Manager__c = varCase.SVMXC__Top_Level__r.Service_Team__r.District_Manager__c; //STSK0013902 - Display DM on creation
                //LstL2848.add(objL2848);   
                system.debug('@@@@@@@' + objL2848);
            }
        }
    }
    
/**
    Method to insert the L2848 record
 */
    public pageReference save()
    {
        try
        {
            //objL2848.Note__c = objL2848.Date_Only_Reported_to_Varian__c+'-----'+objL2848.Version_Model__c;
            insert objL2848;
            return new pageReference('/' + objL2848.Id);
        }
        catch(Exception ex)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Insert error, Please contact administrator'));
        }
        return null;
    }
    
    public pageReference cancel()
    {
        return new pageReference('/' + objL2848.Case__c);
    } 


}
@isTest
public class SR_CreateL2848Controller_Test {

    

    public static testMethod void testSR_CreateL2848Controller() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
            Recordtype rec = [Select id from recordtype where developername = 'Site_Partner'];  //Code comment start by mohit 27-feb-2014
            Account testAcc = AccountTestdata.createAccount();
            testAcc.recordtype = rec;
            testAcc.ShippingCity = 'gjguytugjgjyuihkhk';
            testAcc.ShippingCountry = 'hjhkn';
            testAcc.ShippingState = 'test state';           
            insert testAcc;
            // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con; 
            Case testCase = SR_testdata.createCase();
            testCase.Account = testAcc;
            testCase.ContactId = con.id;
            testCase.Is_escalation_to_the_CLT_required__c = 'Yes';
            testCase.Is_This_a_Complaint__c = 'Yes';
            testCase.Was_anyone_injured__c = 'Yes';
            insert testCase;
            
            L2848__c testL2848 = SR_testdata.createL2848();
           // testL2848.Case__c = testCase.id;
            //testL2848.Case__r = testCase;
            testL2848.Is_Submit__c = False;
            //testL2848.Complaint__c = testComplaintTracking.id;
            insert testL2848;
            
            PageReference pageRef = new PageReference('/apex/OnSubmitL2848?id=' + testL2848.Id);
            bLOb body;
 
            
            if (!Test.isRunningTest())
            {
                body = pageRef.getContentAsPDF(); 
            }
            else
            {
                body = Blob.valueof('Some random String');
            } 
            
            Attachment attach=new Attachment();    

            attach.Name='Unit Test Attachment';
    
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    
            attach.body=bodyBlob;
    
            attach.parentId=testL2848.id;
    
            insert attach;

            
            List<Attachment> attachments=[select id, name from Attachment where parent.id=:testL2848.id];
           
            emailmessage em = new emailmessage();
            em.fromaddress = 'test@test.com';
            em.incoming = true;
            //em.HasAttachment= true;
            em.toaddress = 'test1@test.com';
            em.subject = 'Test Email';
            em.textbody = 'testing';
            em.parentid = testCase.id;
            test.starttest();
            insert em;
            
            Contact con1 = new Contact();
            con1.Accountid = testAcc.id;
            con1.FirstName = 'Test';
            con1.LastName = 'Test';
            con1.Email = em.fromaddress;
            con1.MailingCountry = 'India';
            insert con1;
            
            test.stoptest();   
                 
            //ApexPages.currentPage().getParameters().put('cid',testComplaintTracking.id);
            ApexPages.currentPage().getParameters().put('CF00Nc0000000xe33_lkid',testCase.id);
            ApexPages.currentPage().getParameters().put('id',testL2848.id);
            ApexPages.StandardController testcontroller = new ApexPages.StandardController(testL2848);
            SR_CreateL2848Controller controller = new SR_CreateL2848Controller(testcontroller);
            controller.save();
            ApexPages.StandardController testcontroller1 = new ApexPages.StandardController(testL2848);
            SubmitL2848Record controller1 = new SubmitL2848Record(testcontroller1);
            controller1.updateIsSubmit();
            
            
            //controller.cancel(); //Code comment end by mohit 27-feb-2014
            
        }
      }
      
      public static testMethod void testSR_CreateL2848tRUE() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
            Recordtype rec = [Select id from recordtype where developername = 'Site_Partner'];  //Code comment start by mohit 27-feb-2014
            Account testAcc = AccountTestdata.createAccount();
            testAcc.recordtype = rec;
            testAcc.ShippingCity = 'gjguytugjgjyuihkhk';
            testAcc.ShippingCountry = 'hjhkn';
            testAcc.ShippingState = 'test state';           
            insert testAcc;
            Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con; 
            Case testCase = SR_testdata.createCase();
            testCase.Accountid = testAcc.id;
            testCase.contactId = con.id;
            testCase.Is_escalation_to_the_CLT_required__c = 'Yes';
            testCase.Is_This_a_Complaint__c = 'Yes';
            testCase.Was_anyone_injured__c = 'Yes';
            insert testCase;
            
            L2848__c testL2848 = SR_testdata.createL2848();
            //testL2848.Case__c = testCase.id;
            //testL2848.Case__r = testCase;
            testL2848.Is_Submit__c = TRUE;
            //testL2848.Complaint__c = testComplaintTracking.id;
            insert testL2848;
           
            PageReference pageRef = new PageReference('/apex/OnSubmitL2848?id=' + testL2848.Id);
            bLOb body;
            
            
            if (!Test.isRunningTest())
            {
                body = pageRef.getContentAsPDF(); 
            }
            else
            {
                body = Blob.valueof('Some random String');
            } 
            
            Attachment attach=new Attachment();    

            attach.Name='Unit Test Attachment';
    
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    
            attach.body=bodyBlob;
    
            attach.parentId=testL2848.id;
    
            insert attach;
            
            List<Attachment> attachments=[select id, name from Attachment where parent.id=:testL2848.id];

            emailmessage em = new emailmessage();
            em.fromaddress = con.Email;
            em.incoming = true;
            //em.HasAttachment= true;
            em.toaddress = 'test1@test.com';
            em.subject = 'Test Email';
            em.textbody = 'testing';
            em.parentid = testCase.id;
            test.starttest();
            insert em;
             
    
            
            test.stoptest();

            //ApexPages.currentPage().getParameters().put('cid',testComplaintTracking.id);
            ApexPages.currentPage().getParameters().put('CF00Nc0000000xe33_lkid',testCase.id);
            ApexPages.currentPage().getParameters().put('id',testL2848.id);
            ApexPages.StandardController testcontroller = new ApexPages.StandardController(testL2848);
            SR_CreateL2848Controller controller = new SR_CreateL2848Controller(testcontroller);
            controller.save();
            ApexPages.StandardController testcontroller1 = new ApexPages.StandardController(testL2848);
            SubmitL2848Record controller1 = new SubmitL2848Record(testcontroller1);
            controller1.updateIsSubmit();
            ApexPages.StandardController testcontroller11 = new ApexPages.StandardController(testL2848);
            SR_L2848Detail cont2 = new SR_L2848Detail (testcontroller11);
           // cont2.getPrintableView();
            
            //controller.cancel(); //Code comment end by mohit 27-feb-2014
            
        }
      }
}
/***********************************************************************
* Author         : Kaushiki Verma (Wipro)
* Functionality  : Create planning work order for a case if one planning work order already is not present.
* This Apex Class is being referenced in VF Page - SR_CreatePWOPage 
* User Story     : US4311
* Created On     : 26/3/14
*
* Change Log -: 
* 22/06/2018 - STRY0031303 - Nilesh - Subject on PWO is not pulled from case
*
************************************************************************/
public with sharing class SR_CreatePWO {

    public SVMXC__Service_Order__c woObj{get;set;}
    public recordtype woRecordType = new recordtype(); 
    public  recordtype woRecordTypeIEM =new recordtype(); 
    public boolean checkTrue{get;set;}
    Public List<Sales_Order_Item__c> SalesOredrItemList = new List<Sales_Order_Item__c>();
    Public set<Id> SoiSize = new set<Id>();
    Public set<Id> SoiSET = new set<Id>();
    Public set<Id> CaseLineSize = new set<Id>();
    Public list<SVMXC__Case_Line__c> listCaseLine = new list<SVMXC__Case_Line__c>();
    Public list<SVMXC__Case_Line__c> updateListCaseLine = new list<SVMXC__Case_Line__c>();
    Public boolean RedirectToCasePage{get;set;}
    public boolean displayPopup{get;set;}   
    Public SVMXC__Service_Order__c workOrdObject;    
    Public string strCaseId{get;set;}
    Public case objCS{get;set;}
    Public Map<id,Id> MapParentAndSoInstalledProduct = new map<id,Id>();
    Public Map<id,Id> MapParentParentAndSoParentInstalledProduct = new map<id,Id>();
    public Map<Id,SVMXC__Installed_Product__c> MapInstalledProduct = new Map<Id,SVMXC__Installed_Product__c>();
    List<SVMXC__Installed_Product__c> ListIPInsert = new List<SVMXC__Installed_Product__c>();
    //Public set<Id> ipSet = new set<Id>();
    public map<Sales_Order_Item__c,List<SVMXC__Installed_Product__c>> mapsoiid2lstipid = new Map<Sales_Order_Item__c,List<SVMXC__Installed_Product__c>>();
    public List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>() ;
    public List<Sales_Order_Item__c> ListSoiToUpdate = new List<Sales_Order_Item__c>();
    Public boolean WBSmissing{GET;SET;} 
    Public boolean CaseLineDateValuesMissing{GET;SET;}
    Public boolean LocationMissing {GET;SET;} 

    //savon/FF
    public String countryCode;
    
    //**************************Contstructor Start*************************//
    
    public SR_CreatePWO() 
    {
        RedirectToCasePage = false;
        displayPopup = false;
        checkTrue = false;
        CaseLineDateValuesMissing = false;
        LocationMissing = false;
        WBSmissing = false;
        strCaseId = apexpages.currentpage().getparameters().get('id');
          if(strCaseId!=null)
          {
               
               objCS = [select id, 
               Createddate,Service_Type__c,
               CaseNumber,Subject,
               SVMXC__Top_Level__c,
               SVMXC__Top_Level__r.SVMXC__Service_Contract__c,
               SVMXC__Site__c,description,ProductSystem__c,
               Accountid,
               Productid,
               Ownerid,
               Type,
               recordtype.developerName
               From case where Id =:strCaseId ]; // Getting Case Record
           }
           
          woObj= new SVMXC__Service_Order__c ();
          woRecordType  = [select id,developername,name from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Installation' ];
          woRecordTypeIEM  = [select id,developername,name from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Implementation' ];    
           
    }
      
    //**************************Contstructor End*************************//
   
    //*************************** Action Method***************************//
    Public void OnLoad()
    {         

       for(SVMXC__Case_Line__c CaseLine : [select id,SVMXC__Location__r.ERP_Country_Code__c,Start_Date_time__c,SVMXC__Location__c,SVMXC__Case__c,SVMXC__Case__r.AccountID,End_date_time__c,name,Sales_Order_Item__r.Product_Model__c,Sales_Order_Item__r.Location__c,Sales_Order_Item__c,Sales_Order_Item__r.name,Sales_Order_Item__r.Installed_Product__c from SVMXC__Case_Line__c where SVMXC__Case__c =:objCS.id and Sales_Order_Item__c!=null])
       {
       
            countryCode = Caseline.SVMXC__Location__r.ERP_Country_Code__c;
           //updateSSR.add(woObj);
                 
           if(CaseLine.SVMXC__Location__c == null)
           {
                LocationMissing = true; 
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please update the plan and validate for completeness of locational information.');
                //ApexPages.addMessage(myMsg);                                 
           }
           CaseLineSize.add(CaseLine.id);
           If(CaseLine.Sales_Order_Item__c!=null )
           SoiSize.add(CaseLine.Sales_Order_Item__c);
           if(CaseLine.Sales_Order_Item__c!=null && CaseLine.Sales_Order_Item__r.Product_Model__c!=null && CaseLine.Sales_Order_Item__r.Location__c!=null)
           SoiSET.add(CaseLine.Sales_Order_Item__c);
           
           //*****************Give error if any of the case line is not having start or end date******************//
           
           if(CaseLine.Start_Date_time__c == null || CaseLine.End_date_time__c == null)
            {
                CaseLineDateValuesMissing = true; 
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'One of your case lines does not have EPM iStart/iFinish dates'); //8/5/2015 TA6990 - update from ERROR to WARNING
                ApexPages.addMessage(myMsg);                                 
            }
            
            
            
           if(woObj.SVMXC__Preferred_Start_Time__c == null && CaseLine.Start_Date_time__c != null) 
           {
                //woObj.SVMXC__Preferred_Start_Time__c  = dateTime.newInstance(Date.valueof(CaseLine.Start_Date_time__c),Time.newInstance(08, 00, 00, 00)); 
                woObj.SVMXC__Preferred_Start_Time__c  = CaseLine.Start_Date_time__c;            
           }   
           
           if (woObj.SVMXC__Preferred_End_Time__c == null &&  CaseLine.End_date_time__c != null) 
           {
                //woObj.SVMXC__Preferred_End_Time__c = dateTime.newInstance(Date.valueof(CaseLine.End_date_time__c),Time.newInstance(17, 00, 00, 00));
                woObj.SVMXC__Preferred_End_Time__c = CaseLine.End_date_time__c;             
           }                          
       }

       //upsert woObj; commenting cause inserting Field Service WO behind which is not required at all, Ref: INC4203760, DFCT0011560
       
       //****************ERP_WBS Query****************************//
       
       For(ERP_WBS__c ObjERPWBS : [select id,name,Sales_Order_Item__c from ERP_WBS__c where Sales_Order_Item__c in : SoiSize])
       {
            SoiSize.remove(ObjERPWBS.Sales_Order_Item__c);              
       }
       
       //****************Error message when no erp wbs present under SOI ****************************//
       
       if(SoiSize.Size()> 0)
       {
            WBSmissing = true; 
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Case Lines do not have a linkage to a WBS to be able to receive ISTART/IFINISH Dates from EPM'); //8/5/2015 TA6990 - update from ERROR to WARNING
            ApexPages.addMessage(myMsg);  
       }      
        
   } 
    //*************************** Action Method End***************************// 
   
    //*****************Assign To Installation Manager Button functionality*************//    
        
    public pagereference createWO()
    {
        
        IF(CaseLineDateValuesMissing == true)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING,'One of your case lines does not have EPM iStart/iFinish dates'); //8/5/2015 TA6990 - update from ERROR to WARNING
            ApexPages.addMessage(myMsg);
        }
        IF(WBSMissing == true)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Case Lines do not have a linkage to a WBS to be able to receive ISTART/IFINISH Dates from EPM'); //8/5/2015 TA6990 - update from ERROR to WARNING
            ApexPages.addMessage(myMsg);
        }
        if(LocationMissing == true)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Location information is incomplete. Please go back to the case <a href="{!objCS.CaseNumber}"/> and click on update the plan to update the information.');
            ApexPages.addMessage(myMsg);
        }
        If(LocationMissing == false)
        {
            PageReference ret;
            Integer isPwoCaseCount;
            if(objCS!= null)
            {
                isPwoCaseCount  = [select count() from SVMXC__Service_Order__c where SVMXC__Case__c =:objCS.id and Event__c = true ];
                listCaseLine = [select id,name, SVMXC__Location__r.Country__r.Service_Super_Region__c, SVMXC__Location__r.SVMXC__State__c, SVMXC__Location__r.SVMXC__Country__c, 
                                  SVMXC__Location__r.SVMXC__City__c, Sales_Order_Item__c, Sales_Order_Item__r.Sales_Order__c from SVMXC__Case_Line__c where SVMXC__Case__c =:objCS.id];
                system.debug('===== listCaseLine ===== ' + listCaseLine);
            }
            
            if(isPwoCaseCount > 0){
                checkTrue = true;           
            }else{
                checkTrue = false;
            }
            
            
            if(checkTrue == true)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Planning Work order already exist for this case, you cannot create another planning work order.')); 
                return null;
            }
            else
            {    
                 set<Id> userIdSet = new set<Id>();
                 userIdSet.add(woObj.OwnerId);
                 userIdSet.add(objCS.Ownerid);
                 list<User> usrList = new list<User>();
                 usrList = [Select Id, Name, Email from User where Id IN:userIdSet];
                 map<Id,String> usrEmailMap = new map<Id,String>();
                 for(User u :usrList){
                    if(!usrEmailMap.containsKey(u.Id)){
                        usrEmailMap.put(u.Id,u.Email);
                    } 
                 }
                 system.debug('aebug == '+woObj);
                 workOrdObject = new SVMXC__Service_Order__c();//(ownerid=woObj.ownerid);
                 workOrdObject.SVMXC__Case__c = objCS.id;
                 workOrdObject.Event__c = True;
                 workOrdObject.SVMXC__Top_Level__c = objCS.SVMXC__Top_Level__c;
                 workOrdObject.SVMXC__Company__c = objCS.Accountid;
                 workOrdObject.Installation_Type__c = woObj.Installation_Type__c;
                 workOrdObject.OwnerId = woObj.OwnerId; //cdelfattore@forefrontcorp 10/1/2015 (MM/DD/YYYY) - DE5778 work order owner needs to be the selected user from the createPwo page.
                 workOrdObject.Installation_Manager__c = woObj.OwnerId; //cdelfattore@forefrontcorp 10/1/2015 (MM/DD/YYYY) - DE5778 - set the Owner to the Installation Manager (the User chosen on Create PWO process) 
                // 24 Aug 2016 DFCT0011754
                 workOrdObject.ProjectManager__c = objCS.Ownerid;
                 if(usrEmailMap.containsKey(woObj.OwnerId))
                    workOrdObject.Installation_Manager_Email__c = usrEmailMap.get(woObj.OwnerId);
                 if(usrEmailMap.containsKey(objCS.Ownerid))
                    workOrdObject.Project_Manager_Email__c = usrEmailMap.get(objCS.Ownerid);
                
                 //cdelfattore@forefront 10/1/2015
                 //2. The Owner (Install Manager User Record) should flow down to technician (Install Manager Technician Record)
                 try {
                  //workOrdObject.SVMXC__Group_Member__c = [SELECT Id FROM SVMXC__Service_Group_Members__c WHERE User__c = :woObj.OwnerId LIMIT 1].Id; 
                 } catch(QueryException e){}
                 
                 //workOrdObject.SVMXC__Group_Member__c = //set the technician based on the owner of the work order

                 //added savon/FF 8-6 for TA6975
                 if (listCaseLine.size() > 0) {
                   if (listCaseLine[0].SVMXC__Location__r.Country__r.Service_Super_Region__c != null) {
                      workOrdObject.Service_Super_Region__c = listCaseLine[0].SVMXC__Location__r.Country__r.Service_Super_Region__c;              
                   }
                  if (listCaseLine[0].SVMXC__Location__r.SVMXC__City__c != null) {
                      workOrdObject.SVMXC__City__c = listCaseLine[0].SVMXC__Location__r.SVMXC__City__c;              
                   }
                   if (listCaseLine[0].SVMXC__Location__r.SVMXC__State__c != null) {
                      workOrdObject.SVMXC__State__c = listCaseLine[0].SVMXC__Location__r.SVMXC__State__c;              
                   }
                  if (listCaseLine[0].SVMXC__Location__r.SVMXC__Country__c != null) {
                      workOrdObject.SVMXC__Country__c = listCaseLine[0].SVMXC__Location__r.SVMXC__Country__c;              
                   }
                   //DE7214, SO on the Work Order
                   workOrdObject.Sales_Order__c = listCaseLine[0].Sales_Order_Item__r.Sales_Order__c;
                   workOrdObject.Sales_Order_Item__c = listCaseLine[0].Sales_Order_Item__c;
                 }
                
                 workOrdObject.SVMXC__Order_Status__c = 'open';
                 if(objCS.recordtype.developerName == 'IEM')
                 {
                     workOrdObject.recordtypeid = woRecordTypeIEM.id;
                 }
                 else
                 workOrdObject.recordtypeid = woRecordType.id;
                 if(workOrdObject.recordtypeid == woRecordType.id)
                 {
                 workOrdObject.Work_Order_Reason__c = 'New Installation';
                 workOrdObject.SVMXC__Purpose_of_Visit__c = 'New Installation';
                 }
                 workOrdObject.SVMXC__Product__c = objCS.Productid;
                 system.debug('woObj problem descrition == '+woObj.SVMXC__Problem_Description__c);
                 if(objCS.Description!=null)
                 workOrdObject.SVMXC__Problem_Description__c= objCS.Description;
                 
                 /* STRY0031303 - Subject on PWO is not pulled from case */
                 if(objCS.Subject!=null)
                     workOrdObject.Subject__c = objCS.Subject;
                 
                  workOrdObject.Internal_Comment__c = woObj.Internal_Comment__c;
                 
                 if(woObj.SVMXC__Preferred_End_Time__c !=null)
                 workOrdObject.SVMXC__Preferred_End_Time__c = woObj.SVMXC__Preferred_End_Time__c;//datetime.newInstance(woObj.SVMXC__Preferred_End_Time__c ,Time.newInstance(17, 00, 00, 00));
                 if(woObj.SVMXC__Preferred_Start_Time__c !=null) {
                    workOrdObject.SVMXC__Preferred_Start_Time__c  = woObj.SVMXC__Preferred_Start_Time__c;// datetime.newInstance(woObj.SVMXC__Preferred_Start_Time__c  ,Time.newInstance(08, 00, 00, 00));
                    system.debug('pete> workOrdObject.SVMXC__Preferred_Start_Time__c: ' + workOrdObject.SVMXC__Preferred_Start_Time__c);
                 }
                 system.debug('workOrdObject problem descrition == '+workOrdObject.SVMXC__Problem_Description__c);  

                //HL@ff
                //Default service team to the current user's service team, if none is found, leave blank
                try{  
                   woObj.Service_Team__c = [Select Id, District_Manager__c 
                                            From SVMXC__Service_Group__c 
                                            Where District_Manager__c =: woObj.ownerid Limit 1].Id;
                    system.debug('woObj.Service_Team__c===='+woObj.Service_Team__c);
                    workOrdObject.Service_Team__c = woObj.Service_Team__c;
                }catch(QueryException qe){}
                 
                 try{
                    insert workOrdObject;
                 }
                 catch(DMLException e)
                 {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
                    return null;
                 }
                 
                system.debug('===== listCaseLine LINE # 271===== ' + listCaseLine);
                 for(SVMXC__Case_Line__c varCaseLine : listCaseLine )
                 {
                     varCaseLine.Work_Order__c = workOrdObject.id;
                     updateListCaseLine.add(varCaseLine );
                 }
                 
                 if(updateListCaseLine.size()>0)
                 {
                     try
                     { 
                        update updateListCaseLine;
                     }
                     catch(DMLException e)
                     {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
                        return null;                        
                     }                                          
                 }
                 
                 ret = new PageReference ('/'+workOrdObject.id );
                 ret.setRedirect(true);
                 return ret ;
                 system.debug('ret == '+ret);
        
            }         
        }
        return null;
    }
    
     Public pagereference SetOk()
    {
        LocationMissing = false;
        CaseLineDateValuesMissing = false;
        WBSMissing = false;
        Pagereference pg = new pagereference('/'+objcs.id );
        pg.setredirect(true);
        return pg;
    }
    
    //******************** Cancel Method************************//
    
    Public PageReference goCancel()
    {
        displayPopup = false;
        PageReference goBack;
        goBack = new PageReference ('/'+objcs.id );
                 goBack.setRedirect(true);
                 return goBack;
    
    }
}
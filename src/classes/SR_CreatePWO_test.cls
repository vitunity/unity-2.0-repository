@isTest(SeeAllData=true)
public class SR_CreatePWO_test 
{
    public static testMethod void testSR_CreatePWO() 
    {
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        RecordType rec1 = [Select id,Name,Description from Recordtype where developername = 'IEM' and SObjectType=:'Case'];
        recordtype rec = [Select id from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Installation' ];
        recordtype rec2 = [Select id from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Implementation' ];
        
                
        Sales_order__c objSO = SR_testdata.createSalesOrder();
        objSO.Pricing_Date__c = system.today();
        objSO.ERP_Reference__c = 'Sales Order 001';
        insert objSO;
        
        Product2 objProd = SR_testdata.createProduct();
        objProd.Material_Group__c = Label.Product_Material_group;
        objProd.Budget_Hrs__c = 4;
        objProd.UOM__c = 'HR';
        objProd.Product_Type__c = label.SR_Product_Type_Sellable;
        objProd.ProductCode = 'Test Material No 001';
        objProd.ERP_PCode_4__c = 'H012';
        objProd.Is_Model__c = true;
        insert objProd;
        
        SVMXC__Site__c testsite = new SVMXC__Site__c();
        testsite.SVMXC__Street__c = 'test street';
        testsite.Name = 'test site';
        testsite.SVMXC__Country__c = 'USA';
        testsite.SVMXC__Zip__c = '244401';
        insert testsite;

        //INSERT PRODUCT RECORD
        Product2 objProdModel = SR_testdata.createProduct();
        objProdModel.Product_Type__c = 'Model';
        objProdModel.ProductCode = 'H012';
        insert objProdModel;

        //INSERT SOI RECORD 1
        Sales_Order_Item__c objParentSOI = new Sales_Order_Item__c();
        objParentSOI.name = 'test Parent SOI';
        objParentSOI.Quantity__c = 1.0;
        objParentSOI.ERP_Reference__c = '012345';
        objParentSOI.Sales_Order__c = objSO.ID;
        objParentSOI.ERP_Item_Profit_Center__c = 'Test 01';
        objParentSOI.Location__c = testsite.id;
        insert objParentSOI;

        //INSERT IP RECORD
        SVMXC__Installed_Product__c testInstProduct = new SVMXC__Installed_Product__c();
        testInstProduct.Name = 'H6878876';
        testInstProduct.SVMXC__Product__c = objProd.id;
        testInstProduct.SVMXC__Site__c = testsite.id;
        testInstProduct.Sales_OrderItem__c = objParentSOI.id;
        Insert testInstProduct;

        

        

        //INSERT CASE RECORD
        Case testcase = SR_testdata.createcase();
        testcase.Description = 'anc';
        testcase.recordtypeid= rec1.id;
        testcase.Priority = 'Medium';
        Insert testcase ;

        
      

        //INSERT CASE LINE RECORD 1
        SVMXC__Case_Line__c CaseLine = new SVMXC__Case_Line__c();
        CaseLine.SVMXC__Case__c = testcase.id;
        //CaseLine.Start_Date_time__c= system.now();
        CaseLine.End_date_time__c = system.now()+1;
        CaseLine.Sales_Order_Item__c = objParentSOI.id ;
        
        insert CaseLine;

        
        
        //INSERT SERVICE TEAM RECORD
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.ERP_Reference__c='TestExternal';
        ServiceTeam.SVMXC__Group_Code__c='TestExternal';
        ServiceTeam.Name = 'TestService';
        Insert ServiceTeam;

        //INSERT TECHNICIAN RECORD
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.SVMXC__Service_Group__c = ServiceTeam.id;
        tech.User__c = systemuser.id ;
        tech.ERP_Reference__c = 'TextExternal';
        tech.Name = 'TestTechnician';
        tech.SVMXC__Average_Drive_Time__c = 2.00;
        tech.SVMXC__Average_Speed__c = 2.00;
        tech.SVMXC__Break_Duration__c = 2.00;
        tech.SVMXC__Break_Type__c = 'Fixed';
        tech.SVMXC__City__c = 'Noida';
        tech.SVMXC__Country__c = 'India'; 
        tech.Current_End_Date__c = system.today();
        tech.Dispatch_Queue__c = 'DISP - AMS';
        //tech.Docusign_Recipient__c = 'DISP - AMS';
        tech.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech.SVMXC__State__c = 'UP';
        tech.SVMXC__Street__c = 'abc';
        tech.SVMXC__Zip__c = '201301';
        tech.SVMXC__Salesforce_User__c = systemuser.id;
        tech.SVMXC__Select__c = false;
        Insert tech;

        //INSERT WO RECORD 1
        SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
        workOrdObject.SVMXC__Case__c = testcase.id;
        workOrdObject.Event__c = True;
        workOrdObject.SVMXC__Top_Level__c = testcase.ProductSystem__c;
        workOrdObject.SVMXC__Company__c = testcase.Accountid;
        workOrdObject.ownerID = systemuser.id;
        workOrdObject.SVMXC__Order_Status__c = 'open';
        workOrdObject.SVMXC__Preferred_Technician__c = tech.id;
        workOrdObject.recordtypeid = rec.id;
        workOrdObject.SVMXC__Product__c = testcase.Productid;
        workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
        Insert workOrdObject;

        
    
        CaseLine.Work_Order__c = workOrdObject.id;
        Update CaseLine ;
        
        
    
        apexpages.currentpage().getparameters().put('id',testCase.id);
        //apexpages.currentpage().getparameters().put('id',testCase1.id);
        SR_CreatePWO controller = new SR_CreatePWO();
        controller.OnLoad();
        controller.createWO();
        controller.SetOk();
        controller.goCancel();
        
    }
    public static testMethod void testSR_CreatePWO2() 
    {
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        RecordType rec1 = [Select id,Name,Description from Recordtype where developername = 'IEM' and SObjectType=:'Case'];
        recordtype rec = [Select id from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Installation' ];
        recordtype rec2 = [Select id from recordtype where sobjecttype = 'SVMXC__Service_Order__c' and developername = 'Implementation' ];
        
                
        Sales_order__c objSO = SR_testdata.createSalesOrder();
        objSO.Pricing_Date__c = system.today();
        objSO.ERP_Reference__c = 'Sales Order 001';
        insert objSO;
        
        Product2 objProd = SR_testdata.createProduct();
        objProd.Material_Group__c = Label.Product_Material_group;
        objProd.Budget_Hrs__c = 4;
        objProd.UOM__c = 'HR';
        objProd.Product_Type__c = label.SR_Product_Type_Sellable;
        objProd.ProductCode = 'Test Material No 001';
        objProd.ERP_PCode_4__c = 'H012';
        objProd.Is_Model__c = true;
        insert objProd;
        
        SVMXC__Site__c testsite = new SVMXC__Site__c();
        testsite.SVMXC__Street__c = 'test street';
        testsite.Name = 'test site';
        testsite.SVMXC__Country__c = 'USA';
        testsite.SVMXC__Zip__c = '244401';
        insert testsite;

        //INSERT PRODUCT RECORD
        Product2 objProdModel = SR_testdata.createProduct();
        objProdModel.Product_Type__c = 'Model';
        objProdModel.ProductCode = 'H012';
        insert objProdModel;
        Sales_Order_Item__c objParentSOI = new Sales_Order_Item__c();
        objParentSOI.name = 'test Parent SOI';
        objParentSOI.Quantity__c = 1.0;
        objParentSOI.ERP_Reference__c = '012345';
        objParentSOI.Sales_Order__c = objSO.ID;
        objParentSOI.ERP_Item_Profit_Center__c = 'Test 01';
        objParentSOI.Location__c = testsite.id;
        insert objParentSOI;

        //INSERT IP RECORD
        SVMXC__Installed_Product__c testInstProduct = new SVMXC__Installed_Product__c();
        testInstProduct.Name = 'H6878876';
        testInstProduct.SVMXC__Product__c = objProd.id;
        testInstProduct.SVMXC__Site__c = testsite.id;
        testInstProduct.Sales_OrderItem__c = objParentSOI.id;
        Insert testInstProduct;
        Sales_Order_Item__c objSOI =  SR_testdata.createSalesOrderItem();
        objSOI.Sales_Order_Number__c = objSO.ERP_Reference__c;
        objSOI.ERP_Material_Number__c = objProd.ProductCode;
        objSOI.Product__c = objProd.ID;
        objSOI.Parent_Item__c = objParentSOI.ID;
        objSOI.ERP_Product_Model__c = objProdModel.ProductCode;
        objSOI.ERP_Higher_Level_Item__c = objParentSOI.ERP_Reference__c;
        objSOI.ERP_Item_Profit_Center__c = 'Test 02';
        //objSOI.ERP_Reference__c = objWO.ERP_Sales_Order_Item__c;
        //objSOI.ERP_Site_Partner_Code__c = objAcc.AccountNumber;
        objSOI.Sales_Order__c = objSO.ID;
        objSOI.Location__c= testsite.ID;
        objSOI.Product_Model__c = objProdModel.ID;  
        objSOI.ERP_Item_Category__c = 'Z001';
        objSOI.Reject_Reason__c = 'test';
        objSOI.Installed_Product__c = testInstProduct.ID;
        insert objSOI;


        //INSERT ERP WBS RECORD
        ERP_WBS__c ERPwbs = new ERP_WBS__c();
        ERPwbs.Name = 'Test';
        ERPwbs.Sales_Order_Item__c = objSOI.id;
        insert ERPwbs;
        
        Case testcase1 = SR_testdata.createcase();
        testcase1.Description = 'anc';
        testcase1.recordtypeid= rec1.id;
        testcase1.Priority = 'Medium';
        Insert testcase1 ;
        
        //INSERT CASE LINE RECORD 2
        SVMXC__Case_Line__c CaseLine1 = new SVMXC__Case_Line__c();
        CaseLine1.SVMXC__Case__c = testcase1.id;
        CaseLine1.Start_Date_time__c= system.now();
        CaseLine1.End_date_time__c = system.now()+1  ;
        CaseLine1.Sales_Order_Item__c = objSOI.id;
        CaseLine1.SVMXC__Location__c = testsite.id;
        insert CaseLine1;
        
        //INSERT SERVICE TEAM RECORD
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.ERP_Reference__c='TestExternal';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.SVMXC__Group_Code__c='TestExternal';
        Insert ServiceTeam;
    
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.SVMXC__Service_Group__c = ServiceTeam.id;
        tech.User__c = systemuser.id ;
        tech.ERP_Reference__c = 'TextExternal';
        tech.Name = 'TestTechnician';
        tech.SVMXC__Average_Drive_Time__c = 2.00;
        tech.SVMXC__Average_Speed__c = 2.00;
        tech.SVMXC__Break_Duration__c = 2.00;
        tech.SVMXC__Break_Type__c = 'Fixed';
        tech.SVMXC__City__c = 'Noida';
        tech.SVMXC__Country__c = 'India'; 
        tech.Current_End_Date__c = system.today();
        tech.Dispatch_Queue__c = 'DISP - AMS';
       //tech.Docusign_Recipient__c = 'DISP - AMS';
        tech.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech.SVMXC__State__c = 'UP';
        tech.SVMXC__Street__c = 'abc';
        tech.SVMXC__Zip__c = '201301';
        tech.SVMXC__Salesforce_User__c = systemuser.id;
        tech.SVMXC__Select__c = false;
        Insert tech;
    
        //INSERT WO RECORD 2
        SVMXC__Service_Order__c workOrdObject1 =  SR_testdata.createServiceOrder();
        workOrdObject1.SVMXC__Case__c = testcase1 .id;
        workOrdObject1.Event__c = false;
        workOrdObject1.SVMXC__Top_Level__c = testcase1.ProductSystem__c;
        workOrdObject1.SVMXC__Company__c = testcase1.Accountid;
        workOrdObject1.ownerID = systemuser.id;
        workOrdObject1.SVMXC__Order_Status__c = 'open';
        workOrdObject1.SVMXC__Preferred_Technician__c = tech.id;
        workOrdObject1.recordtypeid = rec2.id;
        workOrdObject1.SVMXC__Product__c = testcase1.Productid;
        workOrdObject1.SVMXC__Problem_Description__c = testcase1.Description;
        workOrdObject1.Acceptance_Date__c= system.today();
        workOrdObject1.Date_of_Intervention__c = system.today();
        workOrdObject1.SVMXC__Preferred_End_Time__c = datetime.newInstance(workOrdObject1.Acceptance_Date__c,Time.newInstance(17, 00, 00, 00)); 
        workOrdObject1.SVMXC__Preferred_Start_Time__c  = datetime.newInstance(workOrdObject1.Date_of_Intervention__c,Time.newInstance(08, 00, 00, 00));
        Insert workOrdObject1;
        CaseLine1.Work_Order__c = workOrdObject1.id;
        Update CaseLine1;
        
        apexpages.currentpage().getparameters().put('id',testCase1.id);
        SR_CreatePWO controller = new SR_CreatePWO();
        controller.OnLoad();
        controller.createWO();
        controller.goCancel();
    }
}
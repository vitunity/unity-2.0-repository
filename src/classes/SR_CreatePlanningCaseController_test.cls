@isTest
public class SR_CreatePlanningCaseController_test 
{
    
    static Account objAcc = SR_testdata.creteAccount();
    static ERP_Partner__c objPart =  SR_testdata.createERPPartner();
    static ERP_Partner_Association__c objEPA = new ERP_Partner_Association__c();
    static Product2 objProd = SR_testdata.createProduct();
    static Sales_order__c objSO = SR_testdata.createSalesOrder();
    static ERP_NWA__c objErpNwa ;
    static SVMXC__Installed_Product__c objIP = SR_testdata.createInstalProduct();
    static Sales_Order_Item__c objSOI;
    static Sales_Order_Item__c objParentSOI = new Sales_Order_Item__c();
    static SVMXC__Case_Line__c objCL = SR_testdata.createCaseLine();
    static SVMXC__Site__c objLoc = SR_testdata.createsite();
    static Contact objCont = SR_testdata.createContact();
    static Case objCase = SR_testdata.createCase();
    static User systemuser = new user();
    static Profile sysadminprofile = new profile();
    static Id IEMCaserectypeid = [Select id from Recordtype where sObjectType=:'Case' and Recordtype.DeveloperName=:'IEM'].Id;
    static Id TrainingCaserectypeid = [Select id from Recordtype where sObjectType=:'Case' and Recordtype.DeveloperName=:'Training'].Id;
    static Id InstCaserectypeid = [Select id from Recordtype where sObjectType=:'Case' and Recordtype.DeveloperName=:'Installation'].Id;
    static SVMXC__SLA_Detail__c objSLA;
    static ERP_WBS__c objWBS;
    static SVMXC__Service_Contract__c objSC;
    static SVMXC__Service_Contract_Products__c obSCP;
    static
    {
        
        
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        
        test.startTest();
        
        objAcc.AccountNumber = '123456';
        objAcc.Country__c = 'Austrlia';        
        insert objAcc;
        
        insert objCont;
        objCase.Priority = 'Medium';
        insert objCase;  
      
        objLoc.SVMXC__Account__c = objAcc.id;
        insert objLoc;
        
        objSO.Pricing_Date__c = system.today();
        objSO.ERP_Reference__c = 'Sales Order 001';
        insert objSO;
        
        objIP.SVMXC__Product__c = objProd.ID;
        objIP.SVMXC__Site__c = objLoc.ID;
        objIP.ERP_Reference__c = 'H1234';
        objIP.SVMXC__Serial_Lot_Number__c = 'H6878876';
        objIP.SVMXC__Status__c = 'Ordered';
        insert objIP;
        
        objParentSOI.name = 'test Parent SOI';
            objParentSOI.Quantity__c = 1.0;
            objParentSOI.ERP_Reference__c = '012345';
            objParentSOI.Sales_Order__c = objSO.ID;
            objParentSOI.ERP_Item_Profit_Center__c = 'Test 01';
            objParentSOI.Site_Partner__c = objAcc.id;
            objParentSOI.ERP_Material_Number__c = 'abc667';
            objParentSOI.Case_Line_Status__c = 'Available for Planning';
            objParentSOI.ERP_Item_Category__c = '00';//uyt
            objParentSOI.Installed_Product__c = objIP.id;
            objParentSOI.Location__c = objIP.SVMXC__Site__c;
            objParentSOI.Reject_Reason__c = '';
            objParentSOI.New__C = 'Upgrade';
            objParentSOI.WBS_Element__c = 'ABC-231'; 
            objParentSOI.Case_Line_Status__c = system.label.Case_Line_Status;
        insert objParentSOI;
       
        Product_FRU__c prodFRu = new Product_FRU__c();
        prodFRU.Top_Level__c = objProd.Id;
        prodFRU.Part__c = objProd.Id;
        insert prodFRU;


        objWBS = new ERP_WBS__c();
            objWBS.name = 'ABC-231';
            objWBS.Sales_Order_Item__c = objParentSOI.id;
        insert objWBS;
        
        objErpNwa=new ERP_NWA__c();
            objErpNwa.ERP_Std_Text_Key__c = 'INST001';
            objErpNwa.ERP_Status__c = 'REL';
            objErpNwa.WBS_Element__c=objWBS.id;
        insert objErpNwa;
        
        objSLA = new SVMXC__SLA_Detail__c();
            objSLA.Installed_Product__c = objIP.id;
            objSLA.Status__c = 'Valid';
        insert objSLA;
        
        Id Caselinerectype =[Select id from Recordtype where sObjectType=:'SVMXC__Case_Line__c' and Recordtype.DeveloperName=:'Case_Line'].Id;
        objCL.SVMXC__Case__c= objCase.id;
        objCL.ERP_NWA__c=objErpNwa.id;
        objCL.SLA_Detail__c = objSLA.id;
        objCL.Sales_Order_Item__c = objParentSOI.id;
        objCL.SVMXC__Priority__c = 'Medium';
        objCL.SVMXC__Installed_Product__c = objIP.Id;
        objCL.recordtypeId = Caselinerectype;
        insert objCL;
        test.stopTest();
    }
 
    public static testMethod void accountInst_test1()
    {
        system.runas(systemuser)
        {
            
            objCase.Recordtypeid = TrainingCaserectypeid;  
            update objCase;
              
            objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
            update objErpNwa;
            
            ApexPages.currentPage().getParameters().put('accid',objAcc.id);
            ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
            //ApexPages.currentPage().getParameters().put('training','true');
            ApexPages.currentPage().getParameters().put('caseid',objCase.id);
            
            SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
            //productPicklistVal = 
            //systemPicklistVal = 

            objcls.getsystemValues();
            objcls.getProductOptions();
            objcls.getChngeTypeValues();
            objcls.getCaseLineStatusValues();
            objcls.productPicklistVal.add('abc667');
            objcls.getproductPicklistVal();
            objcls.getcaseLineStatusVal();
            //objcls.setcaseLineStatusVal('');
            objcls.getchngeTyp();
            objcls.getSLAwrapperlist();
            objcls.setSLAwrapperlist();

            //objcls.caseLineStatusVal = 'Available for Planning';
            
            objcls.fnSearch();
            system.debug('objcls.wrapperlist>>>>>'+objcls.wrapperlist);
            //objcls.wrapperlist[0].checked = true;
            objcls.preview();
             
             
            objcls.previewERPNWA();
            objcls.previewSLA();
            objcls.CreateUpdateCase();
            objcls.lstcaseline = new List<SVMXC__Case_Line__c>();
            objcls.UpdateCase();
            objcls.chngeTyp = 'New';
            ApexPages.currentPage().getparameters().put('idCaseline',objCL.id);
            ApexPages.currentPage().getparameters().put('idSalesOrdrItm',objParentSOI.id); 
            ApexPages.currentPage().getparameters().put('idLocation',objLoc.id);
            ApexPages.currentPage().getparameters().put('idInstallProd',objIP.id);
            ApexPages.currentPage().getparameters().put('idTopLevel',objProd.id);
            ApexPages.currentPage().getparameters().put('selectedRowIdx','1');
            ApexPages.currentPage().getparameters().put('idTopLevel',objIP.id);
            ApexPages.currentPage().getParameters().put('soid',objSO.id);
            ApexPages.currentPage().getParameters().put('training','true');
            ApexPages.currentPage().getParameters().put('ipid',objIP.id);
            
            objcls.showPopupModel();
            objcls.UpdateModel();
            objcls.chngeTyp = 'Upgrade';
            objcls.showPopupModel();
            objcls.chngeTyp = 'Retrofit';
            objcls.showPopupModel();
            objcls.closePopupModel();
            

            //SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
            /*objcls.fnSearch();
            objcls.preview();
            objcls.previewERPNWA();
            objcls.previewSLA();*/
            
        }
        
    }    
    
    public static testMethod void testinfFnSearchMethod() {

            objCase.Recordtypeid = TrainingCaserectypeid;  
            update objCase;
              
            objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
            update objErpNwa;
            
            objSC = new SVMXC__Service_Contract__c();
            objSC.SVMXC__Company__c = objAcc.Id;
            insert objSC;

            obSCP  = new SVMXC__Service_Contract_Products__c(); 
            obSCP.SVMXC__Service_Contract__c = objSC.Id;
            insert obSCP; 

            ApexPages.currentPage().getParameters().put('accid',objAcc.id);
            ApexPages.currentPage().getParameters().put('caseid',objCase.id);
            ApexPages.currentPage().getParameters().put('training', null);
            ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
            SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
            objcls.systemPicklistVal = new String[]{'test string'};
            objcls.fnSearch();

            ApexPages.currentPage().getParameters().put('training', 'true');
            objcls.fnSearch();

            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(true,true, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa1 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(false,true, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa2 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(true,false, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa3 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(false,false, objErpNwa);
            SR_CreatePlanningCaseController.WrapperClass wClass1 = new SR_CreatePlanningCaseController.WrapperClass(true, true, objParentSOI, 1);
            wClass1.PSAvailable = true;
            SR_CreatePlanningCaseController.WrapperClass wClass2 = new SR_CreatePlanningCaseController.WrapperClass(false, true, objParentSOI, 2);
            wClass2.PSAvailable = true;
            SR_CreatePlanningCaseController.WrapperClass wClass3 = new SR_CreatePlanningCaseController.WrapperClass(true, false, objParentSOI, 3);
            wClass3.PSAvailable = true;
            SR_CreatePlanningCaseController.WrapperClass wClass4 = new SR_CreatePlanningCaseController.WrapperClass(false, false, objParentSOI, 4);
            wClass4.PSAvailable = true;
            SR_CreatePlanningCaseController.SLAWrapperClass slaWC = new SR_CreatePlanningCaseController.SLAWrapperClass(true, true, objSLA);
            objcls.NWAwrapperlist = new List<SR_CreatePlanningCaseController.ERPNWAWrapperClass>();
            objcls.previewERPNWA();
            objcls.wrapperlist.add(wClass1);
            objcls.wrapperlist.add(wClass2);
            objcls.wrapperlist.add(wClass3);
            objcls.wrapperlist.add(wClass4);
            objcls.NWAwrapperlist.add(wrapNwa);
            objcls.NWAwrapperlist.add(wrapNwa1);
            objcls.NWAwrapperlist.add(wrapNwa2);
            objcls.NWAwrapperlist.add(wrapNwa3);
            objcls.getWrapperColor(objcls.wrapperlist);
            objcls.preview();
            objcls.previewSLA();
            objcls.previewERPNWA();
            objcls.UpdateCase();
    }

    public static testMethod void testinfFnSearchMethod1() {

            objCase.Recordtypeid = TrainingCaserectypeid;  
            update objCase;
              
            objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
            update objErpNwa;
            
            objSC = new SVMXC__Service_Contract__c();
            objSC.SVMXC__Company__c = objAcc.Id;
            insert objSC;

            obSCP  = new SVMXC__Service_Contract_Products__c(); 
            obSCP.SVMXC__Service_Contract__c = objSC.Id;
            insert obSCP; 

            objCL.ERP_NWA__c = null;
            update objCL;
            ApexPages.currentPage().getParameters().put('accid',objAcc.id);
            ApexPages.currentPage().getParameters().put('caseid',objCase.id);
            ApexPages.currentPage().getParameters().put('training', null);
            ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
            SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();

            objcls.getsystemValues();
            objcls.getProductOptions();
            objcls.getChngeTypeValues();
            objcls.getCaseLineStatusValues();
            objcls.productPicklistVal.add('abc667');
            objcls.getproductPicklistVal();
            objcls.getcaseLineStatusVal();
            //objcls.setcaseLineStatusVal('');
            objcls.getchngeTyp();
            objcls.getSLAwrapperlist();

            objcls.systemPicklistVal = new String[]{'test string'};
            objcls.fnSearch();
            objcls.keyPrefixLocation = 'acme';



            ApexPages.currentPage().getParameters().put('training', 'true');
            objcls.fnSearch();

            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(true,true, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa1 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(false,true, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa2 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(true,false, objErpNwa);
            SR_CreatePlanningCaseController.ERPNWAWrapperClass wrapNwa3 = new SR_CreatePlanningCaseController.ERPNWAWrapperClass(false,false, objErpNwa);
            SR_CreatePlanningCaseController.WrapperClass wClass1 = new SR_CreatePlanningCaseController.WrapperClass(true, true, objParentSOI, 1);
            wClass1.ISTART = Date.today();
            wClass1.IFINISH = Date.today();
            SR_CreatePlanningCaseController.WrapperClass wClass2 = new SR_CreatePlanningCaseController.WrapperClass(true, true, objParentSOI, 2);
            SR_CreatePlanningCaseController.WrapperClass wClass3 = new SR_CreatePlanningCaseController.WrapperClass(true, true, objParentSOI, 3);
            SR_CreatePlanningCaseController.WrapperClass wClass4 = new SR_CreatePlanningCaseController.WrapperClass(true, true, objParentSOI, 4);
            objcls.NWAwrapperlist = new List<SR_CreatePlanningCaseController.ERPNWAWrapperClass>();
            objcls.previewERPNWA();
            objcls.preview();
            objcls.previewSLA();
            objcls.UpdateCase();
    }

    public static testMethod void accountInst_test2() {

        objCase.Recordtypeid = TrainingCaserectypeid;  
        update objCase;
              
        objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
        update objErpNwa;
        ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
        ApexPages.currentPage().getParameters().put('accid',objAcc.id);
        //ApexPages.currentPage().getParameters().put('training','true');
        ApexPages.currentPage().getParameters().put('Caseid',objCase.id);
        ApexPages.currentPage().getParameters().put('soid', objSO.id);
        ApexPages.currentPage().getParameters().put('ipid',objIP.id);
        SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
        objcls.chngeTyp = 'New';
            ApexPages.currentPage().getparameters().put('idCaseline',objCL.id);
            ApexPages.currentPage().getparameters().put('idSalesOrdrItm',objParentSOI.id); 
            ApexPages.currentPage().getparameters().put('idLocation',objLoc.id);
            ApexPages.currentPage().getparameters().put('idInstallProd',objIP.id);
            ApexPages.currentPage().getparameters().put('idTopLevel',objProd.id);
            ApexPages.currentPage().getparameters().put('selectedRowIdx','1');
            ApexPages.currentPage().getparameters().put('idTopLevel',objIP.id);
            ApexPages.currentPage().getParameters().put('soid',objSO.id);
            ApexPages.currentPage().getParameters().put('training','true');
            ApexPages.currentPage().getParameters().put('ipid',objIP.id);
        objcls.showPopupModel();
        objcls.UpdateModel();
        objcls.chngeTyp = 'Upgrade';
        objcls.showPopupModel();
        objcls.UpdateUPG();
        objcls.chngeTyp = 'Retrofit';
        objcls.showPopupModel();
        objcls.UpdateRetrofit();
        objcls.closePopupModel();
        


    }

     public static testMethod void accountInst_test3() {

        objCase.Recordtypeid = TrainingCaserectypeid;  
        update objCase;
              
        objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
        update objErpNwa;
        ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
        ApexPages.currentPage().getParameters().put('accid',objAcc.id);
        //ApexPages.currentPage().getParameters().put('training','true');
        ApexPages.currentPage().getParameters().put('Caseid',null);
        ApexPages.currentPage().getParameters().put('soid', objSO.id);
        ApexPages.currentPage().getParameters().put('ipid',objIP.id);
        SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController(); 
        objcls.productPicklistVal = null;
        objcls.getproductPicklistVal();
        objcls.caseLineStatusVal = null;
        objcls.getcaseLineStatusVal();
       // objcls.setproductPicklistVal('test value');
        objcls.getwrapperlist();
        objcls.setwrapperlist();
        objcls.getNWAwrapperlist();
        objcls.setNWAwrapperlist();
        objcls.setcaseLineStatusVal('test val');
        objcls.setchngeTyp('test string');
        List<String> listOfTestStrings = new List<String>{'list', 'of', 'strings'};
        
        objcls.setproductPicklistVal(listOfTestStrings);

    }

    public static testMethod void caseTraining_test()
    {
        
        objCase.Recordtypeid = TrainingCaserectypeid;  
       
          
        objErpNwa.ERP_Std_Text_Key__c = 'TRN0001';        
        update objErpNwa;
        ApexPages.currentPage().getParameters().put('Accountid',objAcc.id);
        ApexPages.currentPage().getParameters().put('caseid',objCase.id);
        ApexPages.currentPage().getParameters().put('training','true');       
        SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
        objcls.fnSearch();
        objcls.preview();
        objcls.previewERPNWA();
        objcls.previewSLA();
        
    }
 
    public static testMethod void caseIEM_test()
    {
        
        objCase.Recordtypeid = IEMCaserectypeid;  
        update objCase;
        
        ApexPages.currentPage().getParameters().put('caseid',objCase.id);
        ApexPages.currentPage().getParameters().put('training','true');       
        SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
        objcls.fnSearch();
        objcls.preview();
        objcls.previewERPNWA();
        objcls.previewSLA();
        
    }
 
    public static testMethod void caseInst_test()
    {
        
        objCase.Recordtypeid = InstCaserectypeid;  
        update objCase;
        
        ApexPages.currentPage().getParameters().put('caseid',objCase.id);           
        SR_CreatePlanningCaseController objcls = new SR_CreatePlanningCaseController();
        objcls.fnSearch();
        objcls.preview();
        objcls.previewERPNWA();
        objcls.previewSLA();
        
    }
}
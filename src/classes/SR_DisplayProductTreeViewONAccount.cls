public class SR_DisplayProductTreeViewONAccount {

     public String picklistvalue{ get; set; }
     public String iframeSource { get; set; }
     String strAccountId=apexpages.currentpage().getparameters().get('id'); 
     
    public SR_DisplayProductTreeViewONAccount(ApexPages.StandardController controller)
    {
        iframeSource = '/apex/SR_ProductTreeViewONAccount?id='+strAccountId+'&prodtype=None';
    }
    
   public PageReference reloadIframe()
    {
        if(picklistvalue != Null)
            iframeSource = '/apex/SR_ProductTreeViewONAccount?id='+strAccountId+'&prodtype='+picklistvalue;
        else
            iframeSource = '/apex/SR_ProductTreeViewONAccount?id='+strAccountId+'&prodtype=None';
            
        return null;
    }
    
     public List<SelectOption> getpicklistoptions()
    {  List<SelectOption> FamilyOptions= new List<SelectOption>();
    Schema.DescribeFieldResult productfieldDesc = Product2.Product_Type__c.getDescribe();
    system.debug('%%%%%%%%'+ productfieldDesc);
    FamilyOptions.add(new selectoption('None','None'));
     for(Schema.Picklistentry picklistvalues: productfieldDesc.getPicklistValues())
     {
       FamilyOptions.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel()));
     }
     system.debug('-------FamilyOptions-----'+FamilyOptions);
       return FamilyOptions;
    }


}
@isTest
public class SR_DisplayProductTreeViewONAccount_test{

    public static testMethod void testSR_DisplayProductTreeViewONAccount(){
        
        //Dummy data crreation 
         Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
            // Account
            Account testaccount = sr_testdata.creteAccount();
            testaccount.country__c = 'India';
            insert testaccount;
            apexpages.currentpage().getparameters().put('id',testaccount.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testaccount);
            SR_DisplayProductTreeViewONAccount testcontroller = new SR_DisplayProductTreeViewONAccount(controller);
            testcontroller.reloadIframe();
            testcontroller.getpicklistoptions();
            testcontroller.picklistvalue = 'Part';
            testcontroller.reloadIframe();
            }
        }
    }
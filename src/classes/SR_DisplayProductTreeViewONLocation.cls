public with sharing class SR_DisplayProductTreeViewONLocation {

   public String picklistvalue{ get; set; }
     public String iframeSource { get; set; }
     String strLocationId=apexpages.currentpage().getparameters().get('id'); 
     String keyPrefix;
     
    public SR_DisplayProductTreeViewONLocation(ApexPages.StandardController controller)
    {
       Schema.DescribeSObjectResult r = SVMXC__Installed_Product__c.sObjectType.getDescribe();
       keyPrefix = r.getKeyPrefix(); // to get the id prefix of installed product
       if(strLocationId.startsWith(keyPrefix))
            iframeSource = '/apex/SR_ProductTreeViewONInstalledProduct?id='+strLocationId+'&prodtype=None';
       else
            iframeSource = '/apex/SR_InstalledProductTreeViewOnLocation?id='+strLocationId+'&prodtype=None';
    }
    
   public PageReference reloadIframe()
    {
       if(strLocationId.startsWith(keyPrefix)){
            if(picklistvalue != Null)
                iframeSource = '/apex/SR_ProductTreeViewONInstalledProduct?id='+strLocationId+'&prodtype='+picklistvalue;
            else
                iframeSource = '/apex/SR_ProductTreeViewONInstalledProduct?id='+strLocationId+'&prodtype=None';
       }else{
            if(picklistvalue != Null)
                iframeSource = '/apex/SR_InstalledProductTreeViewOnLocation?id='+strLocationId+'&prodtype='+picklistvalue;
            else
                iframeSource = '/apex/SR_InstalledProductTreeViewOnLocation?id='+strLocationId+'&prodtype=None';
          }
        return null;
    }
    
     public List<SelectOption> getpicklistoptions()
    {  
        List<SelectOption> FamilyOptions= new List<SelectOption>();
        Schema.DescribeFieldResult productfieldDesc = Product2.Product_Type__c.getDescribe();
        system.debug('%%%%%%%%'+ productfieldDesc);
        FamilyOptions.add(new selectoption('None','None'));
         for(Schema.Picklistentry picklistvalues: productfieldDesc.getPicklistValues())
         {
           FamilyOptions.add(new selectoption(picklistvalues.getvalue(),picklistvalues.getlabel()));
         }
         system.debug('-------FamilyOptions-----'+FamilyOptions);
           return FamilyOptions;
    }


}
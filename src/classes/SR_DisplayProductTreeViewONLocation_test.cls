@isTest
public class SR_DisplayProductTreeViewONLocation_test{

    public static testMethod void testSR_DisplayProductTreeViewONLocation(){
        
        //Dummy data crreation 
         Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        
        system.runas(systemuser){
            // Intalled Product
            SVMXC__Installed_Product__c testinstlldprod = sr_testdata.createInstalProduct();
            insert testinstlldprod;
            apexpages.currentpage().getparameters().put('id',testinstlldprod.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(testinstlldprod);
            SR_DisplayProductTreeViewONLocation testcontroller = new SR_DisplayProductTreeViewONLocation(controller);
            testcontroller.picklistvalue = 'Part';
            testcontroller.reloadIframe();
            testcontroller.getpicklistoptions();
            SVMXC__Site__c testsite = sr_testdata.createsite();
            insert testsite;
            apexpages.currentpage().getparameters().put('id',testsite.id);
            ApexPages.StandardController controller1 = new ApexPages.StandardController(testsite);
            SR_DisplayProductTreeViewONLocation testcontroller1 = new SR_DisplayProductTreeViewONLocation(controller1);
            testcontroller1.picklistvalue = 'Part';
            testcontroller1.reloadIframe();
            }
        }
    }
@isTest(seeAllData=true)
private class SR_ERPCustomerRequestTest {

    static testMethod void testNewSR_ERPCustomerRequest() {
        SR_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        Account sfdcAccount = [Select Id, SFDC_Account_Ref_Number__c 
                                    From Account 
                                    Where Id =:SR_PrepareOrderControllerTest.CombinedQuote.Bigmachines__Account__c];
        
        PageReference erpCustomerRequestPage = Page.SR_CustomerRequestForm;
        erpCustomerRequestPage.getParameters().put('quoteId',SR_PrepareOrderControllerTest.combinedQuote.Id);
        erpCustomerRequestPage.getParameters().put('salesOrg','0601-US - OS - Systems - 0600-US - OS - Service');
        erpCustomerRequestPage.getParameters().put('BP', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('SP', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('PY', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('Z1', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('SH', 'Combined1212');
        Test.setCurrentPage(erpCustomerRequestPage);
        
        ApexPages.StandardController sc  = new ApexPages.StandardController(new ERP_Customer_Request__c());
        
        SR_ERPCustomerRequest erpCustomerRequest = new SR_ERPCustomerRequest(sc);
        erpCustomerRequest.saveRequest();
        
        ERP_Customer_Request__c erpCustomerRec = [Select Id, New_Updated_Bill_To_Name__c,New_Updated_Site_Partner_Name__c,
                                                            Ship_To_Name__c,New_Updated_Payer_Name__c,New_Updated_Sold_To_Name__c,
                                                            SFA_Number_Sold_To__c,SFA_Number_Site_Partner__c
                                                        From  ERP_Customer_Request__c
                                                        Where New_Updated_Bill_To_Name__c = 'Sample Partner'];
        
        System.assertEquals(erpCustomerRec.New_Updated_Bill_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Site_Partner_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.Ship_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Payer_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.New_Updated_Sold_To_Name__c,'Sample Partner');
        System.assertEquals(erpCustomerRec.SFA_Number_Sold_To__c,sfdcAccount.SFDC_Account_Ref_Number__c);
        //System.assertEquals(erpCustomerRec.SFA_Number_Site_Partner__c,sfdcAccount.SFDC_Account_Ref_Number__c);
    }
    
    static testMethod void testEditSR_ERPCustomerRequest() {
        SR_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        ERP_Customer_Request__c erpCustomerRec = new ERP_Customer_Request__c();
        erpCustomerRec.New_Updated_Bill_To_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Site_Partner_Name__c = 'TEST NAME';
        erpCustomerRec.Ship_To_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Payer_Name__c = 'TEST NAME';
        erpCustomerRec.New_Updated_Sold_To_Name__c = 'TEST NAME';
        insert erpCustomerRec;
        
        PageReference erpCustomerRequestPage = Page.SR_CustomerRequestForm;
        erpCustomerRequestPage.getParameters().put('Id',erpCustomerRec.Id);
        Test.setCurrentPage(erpCustomerRequestPage);
        
        ApexPages.StandardController sc  = new ApexPages.StandardController(erpCustomerRec);
        
        SR_ERPCustomerRequest updateCustomer = new SR_ERPCustomerRequest(sc);
        updateCustomer.erpCustomerRequest.Bill_To_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Ship_To_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Site_Partner_Country__c = 'TEST Country';
        updateCustomer.erpCustomerRequest.Sold_To_Country__c = 'TEST Country';
        updateCustomer.saveRequest();
        
        ERP_Customer_Request__c erpCustomer = [Select Id, New_Updated_Bill_To_Name__c,New_Updated_Site_Partner_Name__c,
                                                            Ship_To_Name__c,New_Updated_Payer_Name__c,New_Updated_Sold_To_Name__c,
                                                            Ship_To_Country__c,Site_Partner_Country__c,Sold_To_Country__c,Bill_To_Country__c
                                                        From  ERP_Customer_Request__c
                                                        Where Id =:erpCustomerRec.Id];
        
        System.assertEquals(erpCustomer.New_Updated_Bill_To_Name__c,'TEST NAME');
        System.assertEquals(erpCustomer.New_Updated_Site_Partner_Name__c,'TEST NAME');
        System.assertEquals(erpCustomer.Ship_To_Name__c,'TEST NAME');
        System.assertEquals(erpCustomer.New_Updated_Payer_Name__c,'TEST NAME');
        System.assertEquals(erpCustomer.New_Updated_Sold_To_Name__c,'TEST NAME');
        
        System.assertEquals(erpCustomer.Ship_To_Country__c,'TEST Country');
        System.assertEquals(erpCustomer.Site_Partner_Country__c,'TEST Country');
        System.assertEquals(erpCustomer.Sold_To_Country__c,'TEST Country');
        System.assertEquals(erpCustomer.Bill_To_Country__c,'TEST Country');
        
    }
}
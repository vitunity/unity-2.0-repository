/**************************************
@Author         :   Shradha Jain
@Created Date   :   23/02/2015
**************************************/

@isTest
private class SR_ERPPcode_AfterTrigger_Test{

    static Product2 objProd = SR_testdata.createProduct();
    
    public static testmethod void test() {
        
        test.startTest();
        Id CoverageRecTypID = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId();
        
        ERP_Pcode__c objEPcode = new ERP_Pcode__c();
        objEPcode.Name = 'SAPCode';
        objEPcode.Service_Product_Group__c = 'test Service Product';
        insert objEPcode;
        
        objProd.ERP_Pcode__c = objEPcode.id;
        objProd.SAP_PCode__c = 'SAPCode';
        objProd.Budget_Hrs__c = 0;
        objProd.SVMXC__Product_Line__c = 'test Service Product';
        insert objProd;

        SVMXC__Counter_Details__c varCD = new SVMXC__Counter_Details__c();
        varCD.SVMXC__Coverage_Limit__c = 5;
        varCD.SVMXC__Product__c = objProd.id;
        varCD.SVMXC__Counters_Covered__c = 3;
        varCD.SVMXC__Counter_Reading__c = 3;
        varCD.Units__c ='Hours';
        varCD.recordtypeid = CoverageRecTypID;
        insert varCD;        
        
        objProd.Budget_Hrs__c = 2;
        update objProd;
        
        objEPcode.Service_Product_Group__c = 'test Service Product1';
        update objEPcode;

        
        test.stopTest();
            
    
    }
}
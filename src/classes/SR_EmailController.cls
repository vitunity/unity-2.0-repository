global class SR_EmailController {
    
    public String Subject {get;set;}          
    public String Body { get; set; }
    public String toAddress {get;set;}
    Public String CCAddress {get;set;}
    public Contact toContact {get;set;}
    public String contactsStr {get;set;}
    public List<Attachment> attchmentInsertList {get;set;}
    public BigMachines__Quote__c quoteRec {get;set;}
    public List<Attachment> alist {get;set;}
    public list<AttchmentWrapper> attchmentWrapperList {get;set;}
    public AttchmentWrapper extraAw {get;set;} 
    String quoteId;
    
    public string toAddressTempStr = '' ;
    public string attachemntsTempStr = '' ;
    
    public SR_EmailController() {
        extraAw = new AttchmentWrapper();
        attchmentWrapperList = new list<AttchmentWrapper>();
        quoteId = System.currentPageReference().getParameters().get('id'); 
        quoteRec = new BigMachines__Quote__c();
        quoteRec = [Select Id, Name from BigMachines__Quote__c where id =: quoteId];
        Subject = 'Varian Quote Number : '+quoteRec.Name;
        List<Attachment> attchFroQueryList = [select Id, Name, Body from Attachment where ParentId =: quoteRec.Id AND contenttype in ('application/pdf') ]; 
        for(Attachment att : attchFroQueryList) {
            AttchmentWrapper aw = new AttchmentWrapper();
            aw.a = att;
            aw.check = false;
            attchmentWrapperList.add(aw);
        }
        
    }
    
    public PageReference sendEmailToUser() {     
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        List<string> toaddressList = new List<String>();  
        List<string> CCaddressList = new List<string>();
        
        if(String.isNotBlank(CCAddress)){
            CCaddressList.addAll(CCAddress.split(','));
        }
        
        list<Contact> contacts = [select Name, email from contact where id in : selectedContacts.split(';', -2)] ;
        for(Contact c : contacts){
            toaddressList.add(c.Email);
            toAddressTempStr += c.name+'<'+c.email+'>\n';
        }
        
        system.debug('toaddressList === '+toaddressList);
        mail.setToAddresses(toaddressList);
        if(!CCaddressList.isEmpty())
        mail.setCCaddresses(CCaddressList);
        mail.setSubject(Subject);
        mail.setPlainTextBody(Body);
        mail.setSaveAsActivity(false);
        
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
        for(AttchmentWrapper  aw : attchmentWrapperList) {  
            if(aw.check){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(aw.a.Name);
                efa.setBody(aw.a.Body);
                fileAttachments.add(efa);
                String link = Url.getSalesforceBaseUrl().toexternalform()+'/'+aw.a.Id;
            	attachemntsTempStr += aw.a.Name+'('+link+')\n';
            }
        	
        }
        
        if(extraAw.a != null && extraAw.a.body != null){
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(extraAw.a.Name);
            efa.setBody(extraAw.a.Body);
            fileAttachments.add(efa);
            attachemntsTempStr += extraAw.a.Name+'\n';
        }
        		
        
        mail.setFileAttachments(fileAttachments);
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  		Additional_Quote_History__c aqh = new Additional_Quote_History__c();
        aqh.Date__c = system.now();
        aqh.To_Addresses__c = toAddressTempStr;
        aqh.CC_Addresses__c = CCAddress;
        aqh.Quote__c = quoteId;
        aqh.Attachments__c = attachemntsTempStr;
        insert aqh;
        
        return new PageReference('/'+quoteRec.Id);
       
    }
    
    
    // attchement wrapper for selection
    class AttchmentWrapper{
        public Attachment a {get;set;}
        public boolean check {get;set;}
        public AttchmentWrapper(){
            a = new Attachment();
            check = false;
        }
    }
    
    //Instance fields
    public String searchTerm {get; set;}
    public String selectedContacts {get; set;}
   
    // JS Remoting action called when searching for a account name
    @RemoteAction
    global static List<Contact> searchContact(String searchTerm) {
        System.debug('Contact Name is: '+searchTerm );
        if(searchTerm.contains(';')){
            String [] tempSearchTerm = searchTerm.split(';', -2);
            searchTerm = tempSearchTerm.get(tempSearchTerm.size()-1);
        }
        List<Contact> Contact= Database.query('Select Id, Name, Phone from Contact where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return Contact;
    }
    
    public PageReference cancel(){
        return new PageReference('/'+quoteId);
    }
}
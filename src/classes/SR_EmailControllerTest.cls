@isTest(seeAllData=true)
private class SR_EmailControllerTest {

    static testMethod void testSR_EmailController() {
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        Attachment quoteAttachment = new Attachment();
        quoteAttachment.ParentId = SR_PrepareOrderControllerTest.salesQuote.Id;
        quoteAttachment.Body = Blob.valueOf('Test');
        quoteAttachment.Name = 'TEST.pdf';
        insert quoteAttachment;
        
        PageReference emailPage = Page.SR_EmailPage;
        System.debug('------SR_PrepareOrderControllerTest.salesQuote.Id'+SR_PrepareOrderControllerTest.salesQuote.Id);
        System.debug('-----'+[Select Id From BigMachines__Quote__c where Id=:SR_PrepareOrderControllerTest.salesQuote.Id]);
        Test.setCurrentPage(emailPage);
        ApexPages.currentPage().getParameters().put('id',SR_PrepareOrderControllerTest.salesQuote.Id);
        
        SR_EmailController sendEmail = new SR_EmailController();
        sendEmail.selectedContacts = '';
        sendEmail.toAddress = 'krishna.katve@varian.com,test@test.com';
        sendEmail.CCAddress = 'krishna.katve@varian.com,test@test.com';
        sendEmail.Body = 'TEST';
        sendEmail.sendEmailToUser();
        
    }
}
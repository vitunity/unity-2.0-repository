global without sharing class SR_English_TestSR_Class

{
 
 global Contact VarContact{get;set;}
 global Id contactId{get;set;}
 global Id controllerValue{get;set;}  
 global contact getRecContact()
    {   
      
        VarContact = [Select Id,name,Email,Phone, Firstname, Lastname, accountid, account.billingcity,account.billingstate,account.billingcountry, account.name,Preferred_Language1__c,Local_language_Name__c,
                        Functional_Role__c,Territory__c,Relationship_Survey_Opt_Out__c    from contact where Id =: contactId limit 1 ];
       
return VarContact;
}
}
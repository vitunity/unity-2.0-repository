/*************************************************************************\
      @ Author          : Ritika Kaushik.
      @ Date            : 15-Aug-2014.
      @ Description     : This controller class is created for US4509.This will call from the 'Follo up Work Order' button
                           from Case Work Order detail page.
                         New Work order will be created  which clones the original Work Order of the Case Work Order 
                         and create all Product service work details under this WO and set the default  initial values
                          to the fields: Order Status, Dispatch Response, Technician, Scheduled Date to allow the 
                          Work Order to appear on the Dispatch Console.

      @ Last Modified By      :    
      @ Last Modified On      :    
      @ Last Modified Reason  :            
****************************************************************************/

public class SR_FollowWorkOrder
{
    public SR_FollowWorkOrder(ApexPages.StandardController controller) {}
    public pagereference FollowWOfromCaseWO()
    {
        if(apexpages.currentpage().getparameters().get('id')!=null)
        {
            Id csWOId = apexpages.currentpage().getparameters().get('id');
            List<Case_Work_Order__c> listCaseWO = new List<Case_Work_Order__c>();
            ID prodServicedRecType = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get(Label.SR_ProductServiced).getRecordTypeId(); 
           
            List<SVMXC__Service_Order_Line__c> listWD = new  List<SVMXC__Service_Order_Line__c>();
            List<SVMXC__Service_Order_Line__c> listInsertWD = new  List<SVMXC__Service_Order_Line__c> ();
            listCaseWO = [Select Id, Work_Order__c from Case_Work_Order__c where id=:csWOId limit 1] ;
            ID WOId = listCaseWO[0].Work_Order__c;
            if (WOid == null) {
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'There is no work order to Follow Up on.'));
            	return null;
            }
            string woFields = returnObjectFields('SVMXC__Service_Order__c');
            string wdFields = returnObjectFields('SVMXC__Service_Order_line__c');
            
            /* SVMXC__Service_Order__c objWO = [SELECT Acceptance_Date__c,Accepted_Date_Time__c,Additional_Email__c,Affected_Energies__c,Approval_Comments__c,Assignment_Status__c,Attach_Ids__c,Beam_Hours__c,Billable__c,Case_Assignment_Date__c,Closing_Status__c,Competitive_Equipment_Info__c,Competitive_Noteables__c,Complaint_Link__c,Contact2_Email_del__c,Contact2_Phone_del__c,Contact2__c,Contact_Email__c,Contact_Phone__c,Date_of_Intervention__c,Date_Time_for_30_mins_WO__c,Date_Time_for_WO__c,Delays__c,Depot_Location__c,District_Manager_1__c,District_Manager_Email__c,District__c,Dosimetric_Impact__c,Email_Handler__c,Enddate_formatted__c,ERP_Billable__c,ERP_Check_Messages__c,ERP_Contract_Nbr__c,ERP_Depot__c,ERP_Functional_Location__c,ERP_Line_Nbr__c,ERP_Processing_Error_Messages__c,ERP_Project_Nbr__c,ERP_Reference__c,ERP_Sales_Order_Higher_Item_Nbr__c,ERP_Sales_Order_Item_Nbr__c,ERP_Sales_Order_Item__c,ERP_Sales_Order_Nbr__c,ERP_Sales_Order__c,ERP_Sales_Org__c,ERP_Schedule_Agreement_Nbr__c,ERP_Service_Order__c,ERP_Service_Request_Nbr__c,ERP_Status__c,ERP_Success_Messages__c,ERP_Top_Level__c,ERP_WBS__c,Escalated_to_DM__c,Escalation__c,Estimated_Arrival_Time__c,Event_ID__c,Event_Number__c,Event__c,Exposure_Counter_Hours__c,Follow_Up_comment__c,Follow_up_Noteables__c,Follow_Up_required__c,FSR_Billable_Status__c,Heater_Hours__c,High_Voltage_Hours__c,Id,Installation_Manager__c,Installed_Product_Status__c,Interface_Status__c,Internal_Comment__c,Inventory_Location__c,IsDeleted,Is_escalation_to_the_CLT_required__c,Is_Master_WO__c,Is_This_a_Complaint__c,L2848_Count__c,LastActivityDate,LastReferencedDate,LastViewedDate,Last_Assigned_Date_Time__c,Last_End_Date_Time__c,level001__c,Link_to_PMI_Checklist__c,Link_to_PSE__c,Machine_release_time__c,Malfunction_Start__c,Name,Next_PM_Date_PM_Plan__c,Next_Training_Trip_Reminder__c,Not_Covered__c,Not_Replied__c,No_of_Closed_Work_Details__c,No_of_closed_work_Orders__c,No_of_days_PMI_overdue__c,No_of_Instalation_WO_s__c,No_Test_Equipment_Used__c,Order_Sub_Type__c,Other_Repercussion_Area__c,OwnerId,Parent_WO__c,Parts_Order_Approval_Level__c,Parts_Order_Approval_Request__c,Part_Order_Value_Limit__c,Pcode__c,PMI_Due_Date__c,PMI_Earliest_Start_Date__c,PMI_Type__c,PPL_Check__c,Preferred_Technician_Email__c,Preferred_Technician_Manager__c,Product_Line__c,Product_Noteables__c,Project_Manager__c,Purchase_Order_Number__c,Reason_for_Reassignment__c,Received_Date_Required__c,Received_Date__c,RecordTypeId,Rejected_Date_Time__c,Repercussion_Areas__c,Repercussion_Comment__c,Replied__c,Requested_End__c,Requested_Start__c,Sales_Noteables__c,Sales_Order__c,Sequence__c,Service_Comment__c,Service_Duration_in_minutes__c,Service_Noteables__c,Source_of_Part__c,StartDate_Formatted__c,Subject__c,Submitted_Date_and_Time__c,SVMXC__Acknowledged_By_Technician_Date_Time__c,SVMXC__Actual_Initial_Response__c,SVMXC__Actual_Onsite_Response__c,SVMXC__Actual_Resolution__c,SVMXC__Actual_Restoration__c,SVMXC__Age_Bucket__c,SVMXC__Age__c,SVMXC__Apply_Business_Hours_For_OptiMax__c,SVMXC__Auto_Entitlement_Status__c,SVMXC__BatchUpdate__c,SVMXC__Billing_Type__c,SVMXC__Canceled_Date_Time__c,SVMXC__Case__c,SVMXC__City__c,SVMXC__Clock_Paused_Forever__c,SVMXC__Closed_By__c,SVMXC__Closed_On__c,SVMXC__Company__c,SVMXC__Completed_Date_Time__c,SVMXC__Component__c,SVMXC__Configuration_After__c,SVMXC__Configuration_Before__c,SVMXC__Contact__c,SVMXC__Corrective_Action__c,SVMXC__Country__c,SVMXC__Customer_Down__c,SVMXC__Customer_Failure_Feedback__c,SVMXC__Dispatch_Now__c,SVMXC__Dispatch_Priority__c,SVMXC__Dispatch_Process__c,SVMXC__Drip__c,SVMXC__Driving_Time__c,SVMXC__Entitlement_Notes__c,SVMXC__Entitlement_Type__c,SVMXC__Failed_Assembly__c,SVMXC__Failure_Location__c,SVMXC__Finished_Onsite_Date_Time__c,SVMXC__FirstScheduledDateTime__c,SVMXC__First_Assigned_DateTime__c,SVMXC__First_Queued_DateTime__c,SVMXC__Group_Email__c,SVMXC__How_Fixed__c,SVMXC__Idle_Time__c,SVMXC__Initial_Response_Customer_By__c,SVMXC__Initial_Response_Internal_By__c,SVMXC__Invoice_Created__c,SVMXC__Invoice_Number__c,SVMXC__IsPartnerRecord__c,SVMXC__Is_Entitlement_Performed__c,SVMXC__Is_Exported__c,SVMXC__Is_PM_Work_Order__c,SVMXC__Is_Service_Covered__c,SVMXC__Is_SLA_Calculated__c,SVMXC__Last_Dispatch_Event__c,SVMXC__Latitude__c,SVMXC__Locked_By_DC__c,SVMXC__Longitude__c,SVMXC__Master_Order_Line__c,SVMXC__Member_Email__c,SVMXC__NoOfTimesAssigned__c,SVMXC__NoOfTimesQueued__c,SVMXC__NoOfTimesScheduled__c,SVMXC__Number_Of_Times_Assigned_Bucket__c,SVMXC__Number_Of_Times_Queued_Bucket__c,SVMXC__Number_Of_Times_Scheduled_Bucket__c,SVMXC__Onsite_Response_Customer_By__c,SVMXC__Onsite_Response_Internal_By__c,SVMXC__OptiMax_Error_Email1__c,SVMXC__OptiMax_Error_Email2__c,SVMXC__OptiMax_Error_Occurred__c,SVMXC__OptiMax_Error_Text__c,SVMXC__OptiMax_Last_Run_Time__c,SVMXC__OptiMax_Status__c,SVMXC__Order_Type__c,SVMXC__Partner_Account__c,SVMXC__Partner_Contact__c,SVMXC__Perform_Auto_Entitlement__c,SVMXC__PM_Plan__c,SVMXC__PM_SC__c,SVMXC__PM_Tasks_Created__c,SVMXC__Preferred_Business_Hours__c,SVMXC__Preferred_End_Time__c,SVMXC__Preferred_Resource_Priority__c,SVMXC__Preferred_Start_Time__c,SVMXC__Preferred_Technician__c,SVMXC__Previous_Scheduled_Date_Time__c,SVMXC__Primary_Territory__c,SVMXC__Priority__c,SVMXC__Problem_Description__c,SVMXC__Product__c,SVMXC__Proforma_Invoice_Amount__c,SVMXC__Proforma_Invoice__c,SVMXC__Purpose_of_Visit__c,SVMXC__QTL_Status__c,SVMXC__Qualified_Technicians__c,SVMXC__Rate_Pricing_Rule__c,SVMXC__Rate_Type_Pricing_Rule__c,SVMXC__Resolution_Customer_By__c,SVMXC__Resolution_Internal_By__c,SVMXC__Restoration_Customer_By__c,SVMXC__Restoration_Internal_By__c,SVMXC__Root_Cause__c,SVMXC__Scheduled_Date__c,SVMXC__Scheduling_Change_Token__c,SVMXC__Scheduling_Retry_Count__c,SVMXC__Service_Contract__c,SVMXC__Service_Duration__c,SVMXC__Service_Group__c,SVMXC__Service_Zone_ID__c,SVMXC__Site__c,SVMXC__Skill_Set__c,SVMXC__Skill__c,SVMXC__SLA_Clock_Extension_Minutes__c,SVMXC__SLA_Clock_Paused__c,SVMXC__SLA_Clock_Pause_Days__c,SVMXC__SLA_Clock_Pause_Hours__c,SVMXC__SLA_Clock_Pause_Minutes__c,SVMXC__SLA_Clock_Pause_Reason__c,SVMXC__SLA_Clock_Pause_Restart_Time__c,SVMXC__SLA_Clock_Pause_Time__c,SVMXC__SLA_Terms__c,SVMXC__Special_Instructions__c,SVMXC__Started_Driving_To_Location_Date_Time__c,SVMXC__State__c,SVMXC__Street__c,SVMXC__Symptom__c,SVMXC__TimeToAssign__c,SVMXC__TimeToQueue__c,SVMXC__TimeToSchedule__c,SVMXC__Top_Level__c,SVMXC__Travel_Rate__c,SVMXC__Travel_Unit__c,SVMXC__Violation_Message__c,SVMXC__Violation_Status2__c,SVMXC__Warranty__c,SVMXC__Work_Order_Scheduling_Status__c,SVMXC__Work_Performed__c,SVMXC__Zip__c,Symptom_Error_Code__c,SystemModstamp,Tech_AssignDateTime__c,Tech_DistrictMangerEmail__c,Temp_Last_Assigned_Date_Time__c,Test_Equipment_Expired__c,Test_Equipmt_Expired_Explanation_Missing__c,Timezone_Shift__c,Total_Downtime__c,Training_Lines_Count__c,Training_Noteables__c,Was_anyone_injured__c,Was_the_applicable_test_performed_and_di__c,Working_Hours__c,WorkOrder_From_SFM__c,Work_Around__c,Work_Order_DM_Emergency_Escalation__c,Work_Order_Reason__c,WO_Reason_for_Closure__c FROM SVMXC__Service_Order__c WHERE Id = : WOId ]; */
            
            String soqlWorkOrder = 'SELECT ' +  woFields  + ' FROM  SVMXC__Service_Order__c where id =\''+WOId+'\'';
                   
            list<SVMXC__Service_Order__c> listWO = new list<SVMXC__Service_Order__c>();
            listWO = Database.query(soqlWorkOrder);
            SVMXC__Service_Order__c objWO = listWO[0];
            SVMXC__Service_Order__c newWO = objWO.clone(false,true);     
            newWO.SVMXC__Order_Status__c = 'Open';    
            system.debug('newWO >>>>>'+newWO.id);
            
            /*  listWD = [SELECT Actual_Hours__c,Applicable_Tests_Performed_and_Specs_Met__c,Badge_Number__c,Billing_Reviewed__c,Billing_Type__c,Budgeted_Hours__c,Case_Line__c,Case__c,Cause_Category__c,Cause_Code__c,Cause_Detail__c,Cause__c,Certificate_of_Destruction_Required__c,Certified_that_parts_were_destroyed__c,Cert_of_Destruction_Qty__c,Chargeable_Flag__c,Completed__c,Counter_Used__c,CreatedById,CreatedDate,CurrencyIsoCode,Customer_End_Date_Time__c,Customer_Start_Date_Time__c,Days_of_the_Week__c,Default_Reference__c,Default_Spec__c,Default_test__c,Delays__c,Destruction_Acknowledgement_Msg_shown__c,District_Manager_Aproval_Process__c,Document_Requirement__c,Done_Remotely__c,End_Date_Date__c,ERP_Activity_Description__c,ERP_Activity_Nbr__c,ERP_Activity_Type__c,ERP_Billable__c,ERP_Network_Nbr__c,ERP_NWA__c,ERP_Pcode_2__c,ERP_Pcode__c,ERP_PCSN__c,ERP_Product_Code__c,ERP_Product_Name__c,ERP_Proj_Nbr__c,ERP_Reference__c,ERP_Sales_Order_Item__c,ERP_Sales_Order__c,ERP_Sales_Org__c,ERP_Serial_Number__c,ERP_Service_Order__c,ERP_Status_Code__c,ERP_Status__c,ERP_Std_Text_Key__c,ERP_Subsystem_Code__c,ERP_Subsystem_Name__c,ERP_System_Code__c,ERP_System_Name__c,ERP_WBS_Nbr__c,ERP_WBS__c,Error_Code__c,Expertise_Availability_End_Date__c,Exposure_Counter_Hours__c,Follow_Up_Required__c,Heater_Hours__c,High_Voltage_Hours__c,Holiday_Labor__c,Id,Installed_Product_Header__c,Interface_Status__c,Internal_Comment__c,Inventory_Location_Id__c,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Lost_Hours__c,Lost_Time__c,Malfunction_Start__c,Material_Name__c,Method__c,Miles_Driven__c,Mod_Status__c,Multiples__c,Name,Not_Varian_Fault__c,No_Charge__c,NWA_Description__c,Old_Part_Number__c,Old_Serial_Number__c,Order_Reason__c,Order_Sub_Type__c,Order_Type__c,Other_Subsystem__c,Other_System__c,Override_Change_to_Customer__c,Parent_Template_Work_Detail_Id__c,Parts_Order_Line__c,Part_Disposition__c,Part_Repairable__c,Part_Returnable__c,Pcode__c,Priority__c,Problem_Date__c,ProductId__c,Product_Code__c,Product_Description__c,Product_Id_Filter__c,Product_Part__c,Product_Stock__c,Product_System_Subsystem__c,Product_Version_Build__c,PSE_Interface_Message__c,PSE_Interface_Status__c,Purchase_Order_Number1__c,Related_Activity__c,Remaining_Hours__c,Remotely__c,Return_Reason__c,Revised_Reference__c,Revised_Spec__c,Revised_test__c,Revision_Date__c,RMA_Number__c,SLA_Detail__c,Solution__c,start_time_Text__c,STB_Master__c,STB_Product_Code__c,STB_Product__c,STB_Status__c,Submitted_Date_and_Time__c,Substitute_Material__c,Subsystem_Name__c,Subsystem_requires_explanation__c,Sub_Equipment__c,SVMXC__Activity_Type__c,SVMXC__Actual_Price2__c,SVMXC__Actual_Quantity2__c,SVMXC__Applied_Rate_Type__c,SVMXC__Billable_Line_Price__c,SVMXC__Billable_Quantity__c,SVMXC__Billing_Information__c,SVMXC__Canceled_By__c,SVMXC__Canceled_On__c,SVMXC__Closed_By__c,SVMXC__Closed_On__c,SVMXC__Consumed_From_Location__c,SVMXC__Cost_Category__c,SVMXC__Covered__c,SVMXC__Date_Received__c,SVMXC__Date_Requested__c,SVMXC__Discount__c,SVMXC__End_Date_and_Time__c,SVMXC__Estimated_Price2__c,SVMXC__Estimated_Quantity2__c,SVMXC__Expense_Type__c,SVMXC__From_Location__c,SVMXC__Group_Member__c,SVMXC__Include_in_Quote__c,SVMXC__Is_Billable__c,SVMXC__Line_Status__c,SVMXC__Line_Type__c,SVMXC__Log_Against__c,SVMXC__Posted_To_Inventory__c,SVMXC__Product_Warranty__c,SVMXC__Product__c,SVMXC__Quantity_Shipment_Initiated2__c,SVMXC__Quantity_Shipped2__c,SVMXC__Received_City__c,SVMXC__Received_Country__c,SVMXC__Received_Location__c,SVMXC__Received_Quantity2__c,SVMXC__Received_State__c,SVMXC__Received_Street__c,SVMXC__Received_Zip__c,SVMXC__Reference_Information__c,SVMXC__Requested_City__c,SVMXC__Requested_Country__c,SVMXC__Requested_Location__c,SVMXC__Requested_Quantity2__c,SVMXC__Requested_State__c,SVMXC__Requested_Street__c,SVMXC__Requested_Zip__c,SVMXC__Select__c,SVMXC__Serial_Number__c,SVMXC__Service_Group__c,SVMXC__Service_Order__c,SVMXC__Start_Date_and_Time__c,SVMXC__Total_Estimated_Price__c,SVMXC__Total_Line_Price__c,SVMXC__Use_Price_From_Pricebook__c,SVMXC__Work_Description__c,SVMXC__Work_Detail__c,Symptom_Category__c,Symptom_Code1__c,Symptom_Code__c,Symptom_Detail__c,Symptom_Error__c,Symptom_Summary__c,Symptom__c,SystemModstamp,System_Name__c,System_requires_explanation__c,TestSpecificationsMeet__c,Test_Equipment_Expired_Explanation__c,Test_Equipment_Expired__c,Test_Line_Number__c,Test_performed_per_Documentation__c,Travel_In__c,Version_Trained__c,Waived__c,Week_Number__c,Work_Around__c,Work_Detail_Line__c,Work_Duration__c,Work_Order_RecordType__c,Work_Type__c,RecordTypeID from SVMXC__Service_Order_Line__c where RecordTypeID =: prodServicedRecType and SVMXC__Service_Order__c =: WOId ];*/
         
            String soqlWorkDetails = 'SELECT ' +  wdFields  + ' FROM  SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c =\''+WOId+'\''+ ' And recordTypeID =  \'' +    prodServicedRecType + '\'';
                   
            // list<SVMXC__Service_Order_Line__c> listWD = new list<SVMXC__Service_Order_Line__c>();
            listWD = Database.query(soqlWorkDetails);
           
            if(listWD!=null && listWD.size()>0)
            {
                system.debug('listWD >>>>>'+listWD );
                for(SVMXC__Service_Order_Line__c WD : listWD)
                {
                    SVMXC__Service_Order_Line__c newWD = new SVMXC__Service_Order_Line__c();
                    newWD = WD.clone(false,true);
                    newWD.SVMXC__Service_Order__r = newWO;
                    listInsertWD.add(newWD);
                }
            }
            try
            {
               
               newWO.SVMXC__Purpose_of_Visit__c = 'Out of Tolerance'; 
               insert newWO ;
               
               listCaseWO[0].Follow_Up_Work_Order__c = newWo.Id;
               update listCaseWO;
                
               if(listInsertWD!=null && listInsertWD.size()>0)
               {
               		Boolean FieldService = false;
                    for(SVMXC__Service_Order_Line__c workDet :listInsertWD)
                    {
                        if(workDet.SVMXC__Service_Order__r!=null)
                        {
                            workDet.SVMXC__Service_Order__c= newWO.id;
                            if (WorkDet.Work_Order_RecordType__c == 'Field Service') {
                            	FieldService = true;
                            }
                        }
                    }
                   if (!FieldService)
                   {
                   	       insert  listInsertWD;
                   }
                 
               }
               return new pagereference('/'+newWO.id);
            }
            catch(DMLException e)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getmessage()));
              return null;
           }
        }
        return null;
    }
    public string returnObjectFields(string objName)
    {
        String selects = '';
        
        // Get a map of field name and field token
        
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objName).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null)
        {
            for (Schema.SObjectField ft : fMap.values())
            { // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty())
        {
            for (string s:selectFields)
            {
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}             
        }
        return selects;
    }
}
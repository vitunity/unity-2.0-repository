/***************************************************************************\
    @ Author                : Kaushiki Verma
    @ Date                  : 18/11/2014
    (Covers class - SR_HDSurvey_BeforeTrigger)
 ****************************************************************************/
@isTest 
public class SR_HDSurvey_AfterTrigger_test {

    public static testMethod void SR_HDSurvey_AfterTrigger_test () {
    
    
    /* Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];*/
        // and id =:userInfo.getUserId()
        //User systemuser = [Select id from user where isActive = true and ManagerId != null and Manager.isActive = true and Manager.Managerid != Null and Manager.Manager.isActive = True and usertype = 'standard' limit 1]; 
     User systemuser = [Select id from user where id =: userInfo.getUserId()];
    system.debug('systemuser ......'+systemuser );
    
    recordtype rec = [Select id from recordtype where developername ='Site_Partner'];
    Account testAcc = accounttestdata.createaccount();
    testAcc.recordtype = rec;
    testAcc.Country__c = 'India';
    insert testAcc;
    
    // insert Contact 
    Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678',Preferred_Language1__c = 'English');
    insert con;
     
    Case testCase = SR_testdata.createcase();
    testCase.Account = testAcc;
    testCase.Contactid=con.id;
    testCase.Status = 'Closed';
    testCase.ClosedBy__c = systemuser.Id;
    insert testCase;
     
     
    recordtype rec1 = [Select id from recordtype where Name ='HelpDesk' and sObjectType = 'HD_Case_Closed_Survey_Results__c' limit 1];
    
    HD_Case_Closed_Survey_Results__c HDTest = new HD_Case_Closed_Survey_Results__c();
    HDTest.Account__c = testAcc.id;
    HDTest.Contact__c=con.id;
    HDTest.Case__c = testCase.id;
    HDTest.Write_in_comments__c = 'abc';
    HDTest.Translated_comments__c = 'abc';
    HDTest.Overall_Sat__c = '1';
    HDTest.Follow_Up_Status__c = 'Complete';
    HDTest.Approval_Status__c= 'New';
    HDTest.Comments_on_products_and_services__c = 'abcd';
    HDTest.P_S_English_Translation__c = 'abcd';
    HDTest.RecordTypeId = rec1.Id;
    

    insert HDTest;
    
    
    HD_Case_Closed_Survey_Results__c HDTest1 = new HD_Case_Closed_Survey_Results__c();
    HDTest1.Account__c = testAcc.id;
    HDTest1.Contact__c=con.id;  
    HDTest1.RecordTypeId = rec1.Id;
    HDTest1.Case__c = testCase.id;
    HDTest1.Write_in_comments__c = 'abc';
    HDTest1.Translated_comments__c = 'abc';
    HDTest1.Overall_Sat__c = '1';
    HDTest1.Follow_Up_Status__c = 'Complete';
    HDTest1.Approval_Status__c= 'New';
    HDTest1.Comments_on_products_and_services__c = 'abcd';
    HDTest1.P_S_English_Translation__c = 'abcd';
    

    insert HDTest1;
    
    recordtype rec2 = [Select id from recordtype where Name ='Relationship' and sObjectType = 'HD_Case_Closed_Survey_Results__c' limit 1];
     
    HD_Case_Closed_Survey_Results__c HDTest2 = new HD_Case_Closed_Survey_Results__c();
    HDTest2.Account__c = testAcc.id;
    HDTest2.Contact__c=con.id;
    HDTest2.RecordTypeId = rec1.Id;
    HDTest2.Case__c = testCase.id;
    HDTest2.Write_in_comments__c = 'abc';
    HDTest2.Translated_comments__c = 'abc';
    HDTest2.Overall_Sat__c = '1';
    HDTest2.Follow_Up_Status__c = 'Complete';
    HDTest2.Approval_Status__c= 'New';
    HDTest2.Comments_on_products_and_services__c = 'abcd';
    HDTest2.P_S_English_Translation__c = 'abcd';

    insert HDTest2;
    
    HDTest2.RecordTypeId = rec2.Id;
    HDTest2.Comments_on_products_and_services__c = 'oioioi';
    update HDTest2;
    
   
    
    
    
    
        /*Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(HDTest.id);
        req1.setSubmitterId(systemuser.Id); 
    
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1); 
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
   
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 =  new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejecting request.');
        req2.setAction('Reject');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        HDTest.Approval_Status__c= 'Requires Further Action';
        update HDTest; */
    
    
    
    
    } 
 }
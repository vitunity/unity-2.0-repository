/********************************************************************************************************************************
      @ Author          : Jyoti Gaur
      @ Date            : 12-Nov-2013.
      @ Description     : This Apex Class is for updating product version id on Installed products.
      @ Last Modified By      :  Nikita  
      @ Last Modified On      :    19/12/2013
      @ Last Modified Reason  :     Changed Product Version ID name to Product Version Build on IP for (DE531) 
*********************************************************************************************************************************/

Public class SR_ISC_History_c_class
{
    Map<Id,Product_Version_Build__c> mapofISCandprodversion=new Map<Id,Product_Version_Build__c>();
    set<id> ISc_set=new set<id>();
    set<id> Ip_set=new set<id>();
    set<string> ISC_PCSN=new set<string>();
    set<string> ISC_Version=new set<string>();
    list<SVMXC__Installed_Product__c> iplist=new list<SVMXC__Installed_Product__c>();
    list<Product_Version_Build__c> Prod_ver_list=new list<Product_Version_Build__c>();
    set<id> ipset_update=new set<id>();
    list<SVMXC__Installed_Product__c> iplist_update=new list<SVMXC__Installed_Product__c>();
    map<id,string> mapofidandprodverbuild=new map<id,string>();

    public void ISCVersion(List<ISC_History__c> ISClist)
    {
        for(ISC_History__c iscobj:ISClist)
        {
            ISc_set.add(iscobj.Installed_Product__c);
            ISC_PCSN.add(iscobj.PCSN__c);
            ISC_Version.add(iscobj.Version__c);
        }
        // list of installed products in which PCSN no. and Ip id are matching with ISC records 
        iplist = [select id, SVMXC__Product__c, Name, Product_Version_Build__c from SVMXC__Installed_Product__c where id in :ISc_set
                                                AND Name in : ISC_PCSN];    //DE3364 changed where condition, replaced SVMXC__Serial_Lot_Number__c with Name
        system.debug('iplist######'+iplist);
        if(iplist.size() > 0)
        {
            for(SVMXC__Installed_Product__c ipobj : iplist)
            {
                Ip_set.add(ipobj.SVMXC__Product__c);
            }
        }

        //list of product version build records in which version and product ids matching with ISC version and product id
        Prod_ver_list = [select id, Product__c, Version__c, Product_Version__c from Product_Version_Build__c where Product__c in :Ip_set
                                                                                AND Version__c in : ISC_Version];
        system.debug('Prod_ver_list######'+Prod_ver_list);
        for(Product_Version_Build__c prod:Prod_ver_list)
        {
            mapofidandprodverbuild.put(prod.Product__c,prod.id);
        }
        system.debug('mapofidandprodverbuild######'+mapofidandprodverbuild);

        for(SVMXC__Installed_Product__c ip_obj_id:iplist)
        {
            ip_obj_id.Product_Version_Build__c=mapofidandprodverbuild.get(ip_obj_id.SVMXC__Product__c);
        }
        system.debug('iplist######'+iplist); 
        update iplist;
    }
}
@isTest
private class SR_ISC_History_c_classTest {
    
      static Profile vmsserviceuserprofile;
      static User serviceuser;
      static
      {
        vmsserviceuserprofile = new profile();
        //vmsserviceuserprofile = [Select id from profile where name = 'VMS Unity 1C - Service User'];
         
       // p = [Select id from profile where name = 'System Administrator'];
       //serviceuser= [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
      }
      
      static testMethod void runPositiveTestCases()
      {
        //System.runAs(serviceuser)
        //{       
            List<ISC_History__c> isclist = new List<ISC_History__c>();
            
            //Product record
            Product2 prod = new Product2(name = 'testprod');
            insert prod;
            system.debug('prod@@@@@@'+prod );
            
            //IP record
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(name = '12345',SVMXC__Product__c = prod.id,SVMXC__Serial_Lot_Number__c = '12345' );
            insert ip;
            system.debug('ip @@@@@@'+ip );
            
            //ISC History record
            ISC_History__c isc = new ISC_History__c(Installed_Product__c = ip.id,PCSN__c = '12345',Version__c = '1.00.001');
            insert isc;
            system.debug('isc @@@@@@'+isc );
            
            //Product Version record
            Product_Version__c prodver = new Product_Version__c(Major_Release__c = 1,Minor_Release__c = 0);
            insert prodver;
            system.debug('prodver@@@@@@'+prodver);
            
            //Product Version Build record
            Product_Version_Build__c prodverbuild = new Product_Version_Build__c(name = 'prodverbuildtest',Product__c = prod.id, Build_Number__c = 1,Product_Version__c=prodver.id);
            insert prodverbuild;
            system.debug('prodverbuild@@@@@@'+prodverbuild);
            
            isclist.add(isc);
            
            SR_ISC_History_c_class obj = new SR_ISC_History_c_class();
            obj.ISCVersion(isclist); // calling method
            
            system.debug('ip.Product_Version_Build__c@@@@@@'+ip.Product_Version_Build__c);
            system.debug('prodverbuild.id2@@@@@@'+prodverbuild.id);
            
            ip = [SELECT Product_Version_Build__c FROM  SVMXC__Installed_Product__c WHERE id =:ip.id]; // retrieving the updated ip record
            //System.assertEquals(prodverbuild.id,ip.Product_Version_Build__c); // comparing ip prouct version build lookup to product version build record created
        //}
      }
    }
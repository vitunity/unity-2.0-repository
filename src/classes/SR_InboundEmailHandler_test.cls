@isTest
public class SR_InboundEmailHandler_test
{
    public static testMethod void testSR_InboundEmailHandler1()
    {
        //insert  IP 
            SVMXC__Installed_Product__c objIP1 = new SVMXC__Installed_Product__c(Name='test13', SVMXC__Status__c ='Installed', SVMXC__Serial_Lot_Number__c ='test13',ERP_Product_Code__c ='testxy', ERP_Functional_Location__c = 'lol' );
            insert objIP1;

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
            email.subject = objIP1.Name;
            email.fromAddress = 'someaddress@email.com';
            email.plainTextBody = 'email fine\n2225256325\nTitle';
            email.toAddresses = new List<String>{'fine.owner.new@cpeneac.cl.apex.sandbox.salesforce.com'};
            SR_InboundEmailHandler testRecord = new SR_InboundEmailHandler();
            testRecord.handleInboundEmail(email,env);
    }

    public static testMethod void testSR_InboundEmailHandler2() 
    {
        //insert  IP 
            SVMXC__Installed_Product__c objIP1 = new SVMXC__Installed_Product__c(Name='test13', SVMXC__Status__c ='Installed', SVMXC__Serial_Lot_Number__c ='test13',ERP_Product_Code__c ='testxy', WBS_Element__c = 'x1y1z1' );
            insert objIP1;

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
            email.subject = objIP1.Name;
            email.fromAddress = 'someaddress1@email.com';
            email.plainTextBody = 'email alert\n2225256325\nTitle';
            email.toAddresses = new List<String>{'alert.owner.new@cpeneac.cl.apex.sandbox.salesforce.com'};
            SR_InboundEmailHandler testRecord1 = new SR_InboundEmailHandler();
            testRecord1.handleInboundEmail(email,env);
    }

    public static testMethod void testSR_InboundEmailHandler3() 
    {
        //insert  IP 
            SVMXC__Installed_Product__c objIP1 = new SVMXC__Installed_Product__c(Name='test134', SVMXC__Status__c ='Installed', SVMXC__Serial_Lot_Number__c ='test134',ERP_Product_Code__c ='testxy', ERP_Functional_Location__c = 'lol', WBS_Element__c = 'x1y1z1' );
            insert objIP1;

        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        // setup the data for the email
            email.subject = objIP1.Name;
            email.fromAddress = 'someaddress1@email.com';
            email.plainTextBody = 'email alert\n2225256325\nTitle';
            email.toAddresses = new List<String>{'failure.owner.new@cpeneac.cl.apex.sandbox.salesforce.com'};
            SR_InboundEmailHandler testRecord1 = new SR_InboundEmailHandler();
            testRecord1.handleInboundEmail(email,env);
    }
}
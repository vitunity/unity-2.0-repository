/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 14-Sep-2013
      @ Description           : This Class creates a new case when an User replies to a Closed Case email.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/
global class SR_InboundEmailHandlerforCase implements Messaging.InboundEmailHandler {
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
      {
          Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
         if(email.subject !='' && email.subject!=null)
          {
         String lstSubject = email.subject; 
         system.debug('****'+lstSubject); 
          System.debug('-------------->'+email.toAddresses);
        List<Case> ListofCase=[Select Id,Status,Priority,Reason,Check_on_Case_Creation__c,Origin,Ownerid,AccountId,ContactId,Primary_Contact_Number__c from Case where Email_Handler__c=:lstSubject];
        List<Case> ListofCasestobeInserted = new List<Case>();
        for(Case casealias : ListofCase)
        {
          if(casealias.Status=='Closed' && casealias.Check_on_Case_Creation__c==False)
          {
             List<QueueSobject> queueowner=[Select QueueId from QueueSobject where SobjectType=:'Case' and Queue.DeveloperName=:'US_Email_to_Case_Queue'];
             Case varcase = new Case();
             varcase.ParentId=casealias.Id;
             varcase.Status='New';
             varcase.Origin='Email';
             varcase.Priority=casealias.Priority;
             varcase.Reason=casealias.Reason;
             varcase.AccountId=casealias.AccountId;
             varcase.ContactId=casealias.ContactId;
             varcase.Primary_Contact_Number__c=casealias.Primary_Contact_Number__c;
             varcase.OwnerId=queueowner[0].QueueId ;
             ListofCasestobeInserted.add(varcase);
             ListofCase[0].Check_on_Case_Creation__c=True;
          }  
                
        }
        if(ListofCasestobeInserted.Size()>0)
        {
          insert ListofCasestobeInserted;  
          Update  ListofCase[0];
        }

      }
       return result;
  }
}
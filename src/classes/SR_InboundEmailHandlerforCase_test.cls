@isTest
public class SR_InboundEmailHandlerforCase_test {

    public static testMethod void testSR_InboundEmailHandlerforCase() {
        //Dummy data crreation 
         Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
            
            Group testGroup = new Group(Name ='Queue', DeveloperName='US_Email_to_Case_Queue', Type = 'Queue');
            insert testGroup;

            QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'Case');
            insert testQueue;
            }
            Account testAcc = AccountTestData.createAccount();
            insert testAcc;
            Contact testContact = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
            insert testContact ;
            Case testCase = SR_testdata.createCase();
            testCase.Priority = 'Medium';
            testCase.AccountId = testAcc.id;
            testCase.Status = 'Closed';
            testCase.ContactId = testContact .id; 
            
            testCase.Email_Handler__c = 'test subject';
            insert testCase;
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
           Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
           
           // setup the data for the email
           
          email.subject = testCase.Email_Handler__c;
          email.fromAddress = 'someaddress@email.com';
          SR_InboundEmailHandlerforCase testRecord = new SR_InboundEmailHandlerforCase();
          testRecord.handleInboundEmail(email,env);
       // }
      }
}
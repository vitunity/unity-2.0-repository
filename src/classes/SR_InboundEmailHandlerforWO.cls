/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 13-Sep-2013
      @ Description           : This is a Inbound Email Handler Class for WO which updates the WO on response from Preferred Technician/Technician by email.
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/

global class SR_InboundEmailHandlerforWO  implements Messaging.InboundEmailHandler 
{
   global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
      
     {
         
         Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
       
         if(email.subject !='' && email.subject!=null)
         
         {
            String lstSubject = email.subject;
            Pattern idPattern = Pattern.compile('WO-[0-9]{8}');
            Matcher matcher = idPattern.matcher(lstSubject);
            String WOName;
            if (matcher.find())
            {
                WOName = matcher.group(0);
            }
        
         list<SVMXC__Service_Order__c> Workorder= [select id, name,Email_Handler__c,SVMXC__Order_Status__c,Replied__c  from SVMXC__Service_Order__c where Email_Handler__c=:lstSubject OR Tech_EmailSubject__c=:lstSubject OR Name =:WOName];
         String emailbody=email.plaintextbody;
         emailbody=emailbody.replaceAll('\\s','');
       
         if((emailbody.startsWithIgnoreCase('yes')||emailbody.startsWithIgnoreCase('accept') || emailbody.startsWithIgnoreCase('accepted') || emailbody.startsWithIgnoreCase('y') || emailbody.startsWithIgnoreCase('a')))
         {
           
          
          Workorder[0].Tech_AssignDateTime__c=null;
          Workorder[0].SVMXC__Order_Status__c='Assigned';  
          Workorder[0]. SVMXC__Dispatch_Response__c='Accepted';
          Workorder[0].Replied__c=True;
         }
         else if((emailbody.startsWithIgnoreCase('no')||emailbody.startsWithIgnoreCase('n') ||  emailbody.startsWithIgnoreCase('rejected')|| emailbody.startsWithIgnoreCase('reject')))
         {
           
           Workorder[0].Tech_AssignDateTime__c = null;
           Workorder[0].SVMXC__Order_Status__c = 'Open';
           Workorder[0]. SVMXC__Dispatch_Response__c = 'Rejected';
           Workorder[0].Replied__c=True;
          
         }
          
          update  Workorder[0];
          
         }
         
         return result;
          
      }
       
  }
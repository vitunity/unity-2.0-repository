/***************
Author : Bushra
Date : 16-12-2014
*****************/

@istest(seeAllData = true)
public class SR_InboundEmailHandlerforWO_Test {

    public static testMethod void testSR_InboundEmailHandler1() {
      
        //insert contact
        Contact conObj = new Contact(LastName = 'test contact', FirstName = 'test', Email ='test@wipro.com', phone = '986743212', Contact_Type__c ='Urologist', MailingCountry='Australia');
        insert conObj;
        
         //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), ContactId = conObj.id, Subject ='test case', Status = 'New',Priority = 'Medium');
         insert caseObj ;
         
       
        // insert WorkOrder
        Schema.DescribeSObjectResult wo = Schema.SObjectType.SVMXC__Service_Order__c; 
         Map<String,Schema.RecordTypeInfo> woMapByName = wo.getRecordTypeInfosByName();
         SVMXC__Service_Order__c woObj = new SVMXC__Service_Order__c(Recordtypeid = woMapByName.get('Field Service').getRecordTypeId(), SVMXC__Case__c = caseObj.id, SVMXC__Order_Status__c ='Open', SVMXC__Priority__c='3 - Information',SVMXC__Contact__c = conObj.id, SVMXC__Order_Type__c ='Off-Site Service Clinical', SVMXC__Entitlement_Type__c ='High Utilization', Email_Handler__c ='Test Subject' ); 
         insert woObj;
            
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
           
        // setup the data for the email
           
        email.subject = woObj.Email_Handler__c;
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'yes\n2225256325\nTitle';
        //email.toAddresses =   new List<String>{'fine.owner.new@cpeneac.cl.apex.sandbox.salesforce.com'};
        SR_InboundEmailHandlerforWO testRecord = new SR_InboundEmailHandlerforWO();
        testRecord.handleInboundEmail(email,env);
          
          
          
        }
        
        public static testMethod void testSR_InboundEmailHandler2() 
        {
        
        //insert contact
        Contact conObj = new Contact(LastName = 'test contact', FirstName = 'test', Email ='test@wipro.com', phone = '986743212', Contact_Type__c ='Urologist', MailingCountry='Australia');
        insert conObj;
        
         //insert case
        Schema.DescribeSObjectResult CR = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseMapByName = CR.getRecordTypeInfosByName();
         Case caseObj = new Case(Recordtypeid=caseMapByName.get('HD/DISP').getRecordTypeId(), ContactId = conObj.id, Subject ='test case', Status = 'New', Priority = 'Medium');
         insert caseObj ;
         
       
        // insert WorkOrder
        Schema.DescribeSObjectResult wo = Schema.SObjectType.SVMXC__Service_Order__c; 
         Map<String,Schema.RecordTypeInfo> woMapByName = wo.getRecordTypeInfosByName();
         SVMXC__Service_Order__c woObj = new SVMXC__Service_Order__c(Recordtypeid = woMapByName.get('Field Service').getRecordTypeId(), SVMXC__Case__c = caseObj.id, SVMXC__Order_Status__c ='Open', SVMXC__Priority__c='3 - Information',SVMXC__Contact__c = conObj.id, SVMXC__Order_Type__c ='Off-Site Service Clinical', SVMXC__Entitlement_Type__c ='High Utilization', Email_Handler__c ='Test Subject' ); 
         insert woObj;
            
            
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
           
        // setup the data for the email
           
        email.subject = woObj.Email_Handler__c;
        email.fromAddress = 'someaddress1@email.com';
        email.plainTextBody = 'no \n2225256325\nTitle';
        //email.toAddresses =   new List<String>{'alert.owner.new@cpeneac.cl.apex.sandbox.salesforce.com'};
        SR_InboundEmailHandlerforWO testRecord1 = new SR_InboundEmailHandlerforWO();
        testRecord1.handleInboundEmail(email,env);
          
          
        }
        
        
}
/**
 *This Controller is to Replace Item detail Standard page with VF page. 
 * @ Author: Chandramouli : MCM Project
 **/

public class SR_ItemDetailController {
     
    public Item_Details__c objItemDetail {get; set;}
    public final Id ItemdetailId;
    public Map<id,Blocked_Items__c> blockeditems2update = new map<id,Blocked_Items__c>();
    
    public SR_ItemDetailController(ApexPages.StandardController sc) {
        ItemdetailId = Apexpages.currentPage().getparameters().get('Id');
        objItemDetail = getItemDetail(ItemdetailId);
    }
    
    public Item_Details__c getItemDetail(Id ItemdetailId) {
        return [Select id, Name, Business_Unit__c, Clearance_Family__c, Item_Level__c, Product1__c, Product5__c, Product__c, Product2__c, Product3__c, Product_Code__c, Review_Product__c,Model__c,
        Product_Profile_Name__c, Regulatory_Name__c, Version_No__c from Item_Details__c where id = : ItemdetailId
        ];
    }
    
    public PageReference saveRequest() {
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        if (objItemDetail.Product__c != null || objItemDetail.Product1__c != null || objItemDetail.Product2__c != null || objItemDetail.Product3__c != null || objItemDetail.Product5__c != null)
        {
            if (objItemDetail.Model__c == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please enter Model Part # before adding products to Item Detail.'));
                return null;
            }
        }
        if (String.isBlank(recordId)) {
            insert objItemDetail;
        } else {
            update objItemDetail;
        }
        updateblockeditems(objItemDetail);
        return new PageReference('/' + objItemDetail.Id);
    }
 
    public Boolean getIsCommertializationGroup() {
        
        List < String > allGroupIDs = new List < String > ();
        for (GroupMember gm: [SELECT Id FROM GroupMember WHERE UserOrGroupId = : UserInfo.getUserId() AND group.name = 'VMS-Commercialization Members']) {
            allGroupIDs.add(gm.id);
        }
        System.debug('----'+allGroupIDs+'----size'+allGroupIDs.size());
        if (allGroupIDs.size() > 0) {
            return true;
        } else {
          return false;
        }
    }
    
    public Boolean getIsRegulatoryGroup() {
        
        List < String > allGroupIDs = new List < String > ();
        for (GroupMember gm: [SELECT Id FROM GroupMember WHERE UserOrGroupId = : UserInfo.getUserId() AND group.name = 'VMS-Regulatory Members']) {
            allGroupIDs.add(gm.id);
        }
        System.debug('----'+allGroupIDs+'----size'+allGroupIDs.size());
        if (allGroupIDs.size() > 0) {
            return true;
        } else {
          return false;
        }
    }
    
    public void updateblockeditems(Item_Details__c itm)
    {
        for (Blocked_Items__c bi : [select id from Blocked_Items__c where Item_Part__c = :itm.id])
        {
            blockeditems2update.put(bi.id, bi);
        }
        if (blockeditems2update.size() > 0)
        {
            update blockeditems2update.values();
        }
    }
    
}
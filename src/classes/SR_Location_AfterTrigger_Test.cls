@isTest
public class SR_Location_AfterTrigger_Test
{
    public static testMethod void testmethod1()
    {
        Map<Id, Account> mapAccount = new Map<Id, Account>();

        /******trigger for Account insert************************/
        
            Account testAcc = AccountTestData.createAccount();
            testAcc.AccountNumber = '123456';
            testAcc.CSS_District__c = 'Test';
            testAcc.BillingCountry='USA';
            testAcc.Country__c= 'USA';
            testAcc.BillingState ='CA';
            testAcc.BillingPostalCode = '95051';
            testAcc.ERP_Timezone__c = 'CST';
            testAcc.ERP_Site_Partner_Code__c = '123456';
            insert testAcc;
            System.assertnotequals(null, testAcc.id);
            Country__c con = new Country__c(Name = 'United States');
            con.SAP_Code__c = 'US';
            insert con;
            System.assertnotequals(null, con.id);            
            //insert country record
            Country__c varC = new Country__c();
            varC.name = 'India';
            varC.ISO_Code_3__c = 'China';
            varC.SAP_Code__c = 'USA';
            insert varC;

            //insert Account record
            Account varAcc = AccountTestData.createAccount();
            varAcc.Country__c = 'India';
            varAcc.ERP_Site_Partner_Code__c = '12345';
            System.debug('varAcc.Country__c@@@ ' + varAcc.Country__c);
            insert varAcc;

        /******trigger for Account update************************/

            //insert location record
            SVMXC__Site__c varLoc = SR_testdata.createsite();
            varLoc.SVMXC__Account__c = testAcc.Id;
            varLoc.ERP_CSS_District__c = 'Paharganj';
            varLoc.Name = 'Test Location';
            varLoc.SVMXC__Street__c = 'Test Street';
            varLoc.SVMXC__Country__c = 'United States';
            varLoc.Sales_Org__c = '0600';
            varLoc.SVMXC__Zip__c = '98765';
            varLoc.ERP_Functional_Location__c = 'Test Location';
            varLoc.Plant__c = 'Test Plant';
            varLoc.ERP_Site_Partner_Code__c = '123456';
            varLoc.SVMXC__Location_Type__c = 'Depot';
            varLoc.country__c = con.id;
            //objLoc.Sales_Org__c = objOrg.Sales_Org__c;
            varLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
            //objLoc.ERP_Org__c = objOrg.id;
            varLoc.SVMXC__Stocking_Location__c = true;
            insert varLoc;
                        System.assertnotequals(null, varLoc.id);
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
            ip.SVMXC__Site__c = varLoc.id;
            ip.ERP_CSS_District__c = varLoc.ERP_CSS_District__c;
            insert ip;
            
            varLoc.ERP_CSS_District__c = 'Karol Bagh';
            varloc.SVMXC__Account__c = varAcc.id;
            update varLoc;
      }
}
/***************************************************************************
Author: Divya Hargunani
Created Date: 13-Nov-2017
Project/Story/Inc/Task : INC4638313 - Able to Pick the Functional Location based on Site Location selected on Prepare Order Page
Description: This is a controller class for apex page SR_LookupFunctionalLocation
*************************************************************************************/

public with sharing class SR_LookupFunctionalLocationCtrl {
   
    private static Integer RECORDS_PER_PAGE = 10; 
    
    public SR_LookupFunctionalLocationCtrl(){}
   
    public List<SVMXC__Site__c> getSites(){
        return (List<SVMXC__Site__c>)functionalLocations.getRecords();
    }
     
    @TestVisible
    private String getSiteQuery(String srcText, String sitePartner){
        String query = 'select Id, Name, ERP_Functional_Location__c, ERP_Site_Partner_Code__c, SVMXC__Street__c, SVMXC__City__c, SVMXC__State__c,'
                      +' SVMXC__Zip__c, SVMXC__Country__c from SVMXC__Site__c where ERP_Functional_Location__c != null ';
        if(String.isNotBlank(srcText)){
            query += ' and ( name like \'%'+srcText+'%\' ';
        }
        if(String.isNotBlank(srcText)){
            query += ' or ERP_Functional_Location__c like \'%'+srcText+'%\' '; 
        }
        if(String.isNotBlank(srcText)){
            query += ' or ERP_Site_Partner_Code__c like \'%'+srcText+'%\' ) ';
        }
        if(String.isNotBlank(sitePartner) && sitePartner != 'undefined' ){
            query += ' and ERP_Site_Partner_Code__c = \''+sitePartner+'\' ';
        }
        
        query += 'Limit 1000';
        return query;
        
    }
    
    private ApexPages.StandardSetController functionalLocations {
        get {
            Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
            String searchText = pageParameters.get('searchText');
            String sitePartner = pageParameters.get('sitePartner');
            if((functionalLocations == null || searchText != null)&& String.isNotBlank(sitePartner)){
                functionalLocations = new ApexPages.StandardSetController(
                Database.getQueryLocator(getSiteQuery(searchText,sitePartner)));
                functionalLocations.setPageSize(RECORDS_PER_PAGE);
            }else{
                functionalLocations = new ApexPages.StandardSetController(new List<SVMXC__Site__c>());
            }
            return functionalLocations;
        }
        set;
    }
  
    public Boolean hasNext {
        get {
            return functionalLocations.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return functionalLocations.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return functionalLocations.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    public Integer totalPages {
        get {
            Decimal dTotalPages = functionalLocations.getResultSize()/functionalLocations.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(functionalLocations.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    public void first() {
        functionalLocations.first();
    }

    // returns the last page of records
    public void last() {
        functionalLocations.last();     
    }

    // returns the previous page of records
    public void previous() {
        functionalLocations.previous();
    }

    // returns the next page of records
    public void next() {
       functionalLocations.next();
    }
}
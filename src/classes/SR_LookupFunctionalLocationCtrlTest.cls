/***************************************************************************
Author: Divya Hargunani
Created Date: 13-Nov-2017
Project/Story/Inc/Task : INC4638313 - Able to Pick the Functional Location based on Site Location selected on Prepare Order Page
Description: This is a test class for apex class SR_LookupFunctionalLocationCtrlTest
*************************************************************************************/
@isTest
public class SR_LookupFunctionalLocationCtrlTest {
    
        public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
        public static Account newAcc;
        public static SVMXC__Site__c newLoc;
        
    Static {
        Account newAcc = new Account();
        newAcc.Name = 'My Varian Test Account';
        newAcc.BillingCountry='USA';
        newAcc.Country__c= 'USA';
        newAcc.BillingCity= 'Los Angeles';
        newAcc.BillingStreet ='3333 W 2nd St';
        newAcc.BillingState ='CA';
        newAcc.BillingPostalCode = '90020';
        newAcc.Agreement_Type__c = 'Master Pricing';
        newAcc.OMNI_Address1__c ='3333 W 2nd St';
        newAcc.OMNI_City__c ='Los Angeles';
        newAcc.OMNI_State__c ='CA';
        newAcc.OMNI_Country__c = 'USA';  
        newAcc.OMNI_Postal_Code__c = '90020';
        insert newAcc;
        
        newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        insert newLoc;
     }
    
    static testMethod void funLocLookupTest(){
        Test.startTest();
        SR_LookupFunctionalLocationCtrl  lookupTest = new SR_LookupFunctionalLocationCtrl ();
        lookupTest.getSites();
        String searchText = 'test';
        String site = 'Sample site';
        lookupTest.getSiteQuery(searchText, site);
        
        //To test pagination methods
        boolean nextResult = lookupTest.hasNext;
        boolean pretResult = lookupTest.hasPrevious;
        Integer pageNum = lookupTest.pageNumber;
        lookupTest.first();
        lookupTest.next();
        lookupTest.previous();
        lookupTest.last();
        Test.stopTest();
    }

}
public with sharing class SR_LookupPartners {
    
    //Page parameters
    private String partnerFunction;
    private String soldToNumber;
    private String salesOrg;
    private String paginatorFilter;
    
    private static Integer RECORDS_PER_PAGE = 10; 
    
    public String salesOrgSubString{get;private set;}
    
    public SR_LookupPartners(){
        
        Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
        
        partnerFunction = pageParameters.get('partnerFunction');
        soldToNumber = pageParameters.get('soldToNumber');
        salesOrg = pageParameters.get('salesOrg');
        salesOrgSubString = salesOrg.subString(0,4);
    }
    
    /**
     * Returns list of erp associations related to search text and page parameters
     */
    public List<ERP_Partner_Association__c> getErpPartnerAssociations(){
        return (List<ERP_Partner_Association__c>)erpAssociationPaginator.getRecords();
    }
    
    /**
     * 
     */
    public String getPartnerType(){
        Map<String,String> partnerTypes = new Map<String,String>{'SP' => 'Sold To Parties', 'Z1' => 'Site Partners',
                                                                'SH' => 'Ship To Parties', 'EU' => 'End Users',
                                                                'PY' => 'Payers', 'BP' => 'Bill To Parties'};
        return partnerTypes.get(partnerFunction);
    }
    
    /**
     * Search erp associations based on page parameters and search string
     */
   
    /** reference to the paginating controller and get ERP association paginator*/
    private ApexPages.StandardSetController erpAssociationPaginator {
        get {
            Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
            String searchText = pageParameters.get('searchText');
            
            if(erpAssociationPaginator == null && String.isBlank(searchText)) {
                
                erpAssociationPaginator = getERPAssociationsBYSOQL();
                erpAssociationPaginator.setPageSize(RECORDS_PER_PAGE);
            }else if(paginatorFilter != searchText && pageParameters.containsKey('searchText')){
                
                if(String.isBlank(searchText)){
                    erpAssociationPaginator = getERPAssociationsBYSOQL();
                }else{
                    erpAssociationPaginator = getERPAssociationsBYSOSL(searchText);
                }
                paginatorFilter = searchText;
                erpAssociationPaginator.setPageSize(RECORDS_PER_PAGE);
            }
            return erpAssociationPaginator;
        }
        set;
    }
    
    ///////////// Private Utility Methods //////////////
    /**
     * Return query instead of sosl because we need all erp partner associations 
     * and not used sosl on ERP Partrner Association as sosl will not search formula fields
     * (most of the fields from erp partner are reffered using formula fields in ERP partner association)
     */
    private ApexPages.StandardSetController getERPAssociationsBYSOSL(String searchText){
        
        String searchString = '%'+searchText+'%';
        String pf = partnerFunction+'%';
        
        return new ApexPages.StandardSetController(Database.getQueryLocator([Select Sales_Org__c,ERP_Partner__r.Name, ERP_Partner__r.Street__c,
                                                                                ERP_Partner_Number__c,ERP_Partner__r.City__c,ERP_Partner__r.State_Province_Code__c,
                                                                                ERP_Partner__r.Country_Code__c, ERP_Partner__r.Zipcode_Postal_Code__c 
                                                                            From ERP_Partner_Association__c 
                                                                            where Partner_Function__c LIKE :pf
                                                                            And (ERP_Partner__r.Name LIKE :searchString
                                                                                 OR ERP_Partner_Number__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Street__c LIKE :searchString
                                                                                 OR ERP_Partner__r.City__c LIKE :searchString
                                                                                 OR ERP_Partner__r.State_Province_Code__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Country_Code__c LIKE :searchString
                                                                                 OR ERP_Partner__r.Zipcode_Postal_Code__c LIKE :searchString
                                                                                 OR Sales_Org__c LIKE :searchString) 
                                                                                 Limit 10000]));
        
    }
    
    /**
     * Utility Factory methods for Paginating Controller for all ERP Associations with search text
     */
    private ApexPages.StandardSetController getERPAssociationsBYSOQL(){
        String soqlQuery = 'Select Sales_Org__c,ERP_Partner__r.Name, ERP_Partner__r.Street__c,'
                                +'ERP_Partner_Number__c,Partner_Function__c,'
                                +'ERP_Partner__r.City__c,ERP_Partner__r.State_Province_Code__c,'
                                +'ERP_Partner__r.Country_Code__c, ERP_Partner__r.Zipcode_Postal_Code__c From ERP_Partner_Association__c where ';
        
        soqlQuery += getFilters();  
        soqlQuery += ' Limit 10000';                            
        System.debug('---soqlQuery'+soqlQuery);                         
        return new ApexPages.StandardSetController(Database.getQueryLocator(soqlQuery));
    }
    
    /**
     * Utility methid for getting filters for query
     */
    private String getFilters(){
        
        String filters = '';
        
        if(!String.isBlank(partnerFunction)){
            filters += ' Partner_Function__c LIKE \''+partnerFunction.subString(0,2)+'%\' and ';
        }
        if(!String.isBlank(soldToNumber)){
            filters += ' ERP_Customer_Number__c = \''+soldToNumber+'\' and ';
        }
        if(!String.isBlank(salesOrg)){
            filters += ' Sales_Org__c = \''+salesOrgSubString+'\' and';
        }
        
        return filters.removeEnd('and');
    }
    
    //////////////////////////////////////////////////////////////////////////////
    ////// Pagination  Actions                                               /////
    //////////////////////////////////////////////////////////////////////////////
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return erpAssociationPaginator.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return erpAssociationPaginator.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return erpAssociationPaginator.getPageNumber();
        }
        set;
    }
    
    // returns total # of records pages 
    public Integer totalPages {
        get {
            Decimal dTotalPages = erpAssociationPaginator.getResultSize()/erpAssociationPaginator.getPageSize();
            dTotalPages = Math.floor(dTotalPages) + ((Math.mod(erpAssociationPaginator.getResultSize(), RECORDS_PER_PAGE)>0) ? 1 : 0);
            return Integer.valueOf(dTotalPages);
        }
    }

    // returns the first page of records
    public void first() {
        erpAssociationPaginator.first();
    }

    // returns the last page of records
    public void last() {
        erpAssociationPaginator.last();     
    }

    // returns the previous page of records
    public void previous() {
        erpAssociationPaginator.previous();
    }

    // returns the next page of records
    public void next() {
       erpAssociationPaginator.next();
    }
}
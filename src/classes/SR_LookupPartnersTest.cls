@isTest
private class SR_LookupPartnersTest {

    static testMethod void testLookupController() {
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = 'Sales1212';
        insert erpPartner;
        
        insertERPAssociations('Sales1212', erpPartner.Id, salesAccount.Id);
        
        // test partner function z1
        PageReference lookupPageZ1 = Page.SR_LookupPartners;
        lookupPageZ1.getParameters().put('salesOrg','0600');
        lookupPageZ1.getParameters().put('partnerFunction','Z1');
        lookupPageZ1.getParameters().put('searchText','');
        lookupPageZ1.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageZ1);
        
        SR_LookupPartners lookupControllerZ1 = new SR_LookupPartners();
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
        
        // test partner function BP
        PageReference lookupPageBP = Page.SR_LookupPartners;
        lookupPageBP.getParameters().put('salesOrg','0600');
        lookupPageBP.getParameters().put('partnerFunction','BP');
        lookupPageBP.getParameters().put('searchText','');
        lookupPageBP.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageBP);
        
        SR_LookupPartners lookupControllerBP = new SR_LookupPartners();
        System.assertEquals(10,lookupControllerBP.getErpPartnerAssociations().size());
        
        //test partner function SH
        PageReference lookupPageSH = Page.SR_LookupPartners;
        lookupPageSH.getParameters().put('salesOrg','0600');
        lookupPageSH.getParameters().put('partnerFunction','SH');
        lookupPageSH.getParameters().put('searchText','');
        lookupPageSH.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageSH);
        
        SR_LookupPartners lookupControllerSH = new SR_LookupPartners();
        
        System.assertEquals(10,lookupControllerSH.getErpPartnerAssociations().size());
        
        //Test partner function SP
        PageReference lookupPageSP = Page.SR_LookupPartners;
        lookupPageSP.getParameters().put('salesOrg','0600');
        lookupPageSP.getParameters().put('partnerFunction','SP');
        lookupPageSP.getParameters().put('searchText','');
        lookupPageSP.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageSP);
        
        SR_LookupPartners lookupControllerSP = new SR_LookupPartners();
        
        //Test Pagination actions
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        System.assert(lookupControllerSP.hasNext);
        lookupControllerSP.next();
        System.assertEquals(5,lookupControllerSP.getErpPartnerAssociations().size());
        System.assert(lookupControllerSP.hasPrevious);
        lookupControllerSP.previous();
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        lookupControllerSP.last();
        System.assertEquals(5,lookupControllerSP.getErpPartnerAssociations().size());
        lookupControllerSP.first();
        System.assertEquals(1,lookupControllerSP.pageNumber);
        System.assertEquals(2,lookupControllerSP.totalPages);
        System.assertEquals('Sold To Parties',lookupControllerSP.getPartnerType());
        
        System.assertEquals(10,lookupControllerSP.getErpPartnerAssociations().size());
        
        //Test partner function PY
        PageReference lookupPagePY = Page.SR_LookupPartners;
        lookupPagePY.getParameters().put('salesOrg','0600');
        lookupPagePY.getParameters().put('partnerFunction','PY');
        lookupPagePY.getParameters().put('searchText','');
        lookupPagePY.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPagePY);
        
        SR_LookupPartners lookupControllerPY = new SR_LookupPartners();
        
        System.assertEquals(10,lookupControllerPY.getErpPartnerAssociations().size());
        
        //Test partner function EU
        PageReference lookupPageEU = Page.SR_LookupPartners;
        lookupPageEU.getParameters().put('salesOrg','0600');
        lookupPageEU.getParameters().put('partnerFunction','EU');
        lookupPageEU.getParameters().put('searchText','');
        lookupPageEU.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageEU);
        
        SR_LookupPartners lookupControllerEU = new SR_LookupPartners();
        System.assertEquals(10,lookupControllerEU.getErpPartnerAssociations().size());
    }
    
    static testMethod void testLookUpControllerSearchString(){
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'SalesCustomer';
        salesAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert salesAccount;
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sample Partner';
        erpPartner.Partner_Number__c = 'Sales1212';
        insert erpPartner;
        
        insertERPAssociations('Sales1212', erpPartner.Id, salesAccount.Id);
        
        // test partner function z1
        PageReference lookupPageZ1 = Page.SR_LookupPartners;
        lookupPageZ1.getParameters().put('salesOrg','0600');
        lookupPageZ1.getParameters().put('partnerFunction','Z1');
        lookupPageZ1.getParameters().put('soldToNumber','SalesCustomer');
        
        Test.setCurrentPage(lookupPageZ1);
        
        SR_LookupPartners lookupControllerZ1 = new SR_LookupPartners();
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
        
        lookupPageZ1.getParameters().put('searchText','Sample');
        System.assertEquals(10,lookupControllerZ1.getErpPartnerAssociations().size());
        
    }
    
    private static void insertERPAssociations(String partnerNumber, Id erpPartnerId, Id accountId){
        
        Set<String> partnerFunctions = new Set<String>{'SP=Sold-to party',
                                                        'Z1=Site Partner',
                                                        'BP=Bill-to Party',
                                                        'SH=Ship-to party',
                                                        'PY=Payer',
                                                        'EU=End User'};
        
        List<ERP_Partner_Association__c> partnerAssociations = new List<ERP_Partner_Association__c>();
        
        for(String partnerFunction : partnerFunctions){
            for(Integer counter = 0; counter<15;counter++){
                ERP_Partner_Association__c partnerAssociation = new ERP_Partner_Association__c();
                partnerAssociation.Customer_Account__c = accountId;
                partnerAssociation.Erp_Partner__c = erpPartnerId;
                partnerAssociation.ERP_Customer_Number__c = 'SalesCustomer';
                partnerAssociation.Partner_Function__c = partnerFunction;
                partnerAssociation.ERP_Partner_Number__c = partnerNumber;
                partnerAssociation.Sales_Org__c = '0600';
                partnerAssociations.add(partnerAssociation);
            }
        }
        insert partnerAssociations;
    }
}
public class SR_OOTVFEmailTemplateCompController {
    public String caseId { get;set; }
    public list<Case_Work_Order__c> getCaseWorkOrders(){
        return [select Name, Work_Order__r.Name from Case_Work_Order__c where Case__c =: caseId];
    } 
}
/*
 * Send prebooked and booked order emails
 Test Class - SR_SuccessEmailRestControllerTest
 */
public class SR_OrderEmail implements Queueable, Database.AllowsCallouts{
    
    private Id pobjQuoteId;
    private String emailTemplate;
    
    public SR_OrderEmail(Id pobjQuoteId, String emailTemplate){
        this.pobjQuoteId = pobjQuoteId;
        this.emailTemplate = emailTemplate;
    }
    
    public void execute(QueueableContext context) {
        
        System.debug('-----pobjQuoteId'+pobjQuoteId+'---template'+emailTemplate);
        
        BigMachines__Quote__c pobjQuote = [Select Id,Name,Order_Type__c, Service_Org__c, SAP_Prebooked_Sales__c, Quote_Region__c, BigMachines__Account__r.BillingCountry, 
        SAP_Prebooked_Service__c, SAP_Booked_Service__c, Ship_To_Country__c, SAP_Booked_Sales__c , Fulfillment_Quote__c, Subscription_Status__c, 
        Brachy_Sales__c, Price_Group__c, OwnerId, Submitted_To_SAP_By__c,Project_Manager__c,BigMachines__Account__r.Country__c
        From BigMachines__Quote__c Where id =: pobjQuoteId];
                                        
        Map<String,String> emailmap = new Map<String,String>();
        
        List<EmailTemplate> lstEmailTemplates = [select Id,Name, DeveloperName 
                                                    from EmailTemplate 
                                                    where (Name = 'Quote SAP BookedSales - Success VF' OR 
                                                        Name = 'Quote SAP PreBookedSales - Success VF' OR 
                                                        Name = 'Addendum Quote SAP PreBookedSales - Success VF' OR 
                                                        Name = 'Addendum_Quote SAP PreBookedCombined - Success VF' OR                                                      
                                                        Name = 'Quote SAP PreBookedService - Success VF' OR
                                                        Name = 'Quote SAP PreBookedCombined - Success VF' OR
                                                        Name = 'Quote SAP BookedService - Success VF')];
        
        if(lstEmailTemplates!=null && lstEmailTemplates.size()>0){
            for(emailtemplate e: lstEmailTemplates){
                emailmap.put(e.name,e.id);
            }
        } 
        
        User userRec = [Select Id, Email from User where id = :Userinfo.getUserId() limit 1];
                
        if(emailmap.get(emailTemplate) != null){   
            Blob cntPDF ; // added by Kaushiki
            PageReference orderPg = Page.SR_PrepareOrderPdf;
            orderPg.getParameters().put('id',pobjQuote.id); 
            //if(emailTemplate == 'Quote SAP BookedService - Success VF_ Test')orderPg.getParameters().put('booked','true');          
            
            //added by Kaushiki to cover the code in test class as test method do not support getcontent() call
            if(Test.IsRunningTest()){
                cntPDF=Blob.valueOf('UNIT.TEST');
            }else{
                cntPDF = orderPg.getContent();
            }
            
         
            Blob cntPDF1;//added by kaushiki
            PageReference orderPg1 = Page.SR_PrepareOrderPdf2;
            orderPg1.getParameters().put('id',pobjQuote.id);
            
            //if(emailTemplate == 'Quote SAP BookedService - Success VF_ Test')orderPg1.getParameters().put('booked','true');
            
            //added by Kaushiki to cover the code in test class as test method do not support getcontent() call
            if (Test.IsRunningTest()){
                cntPDF1=Blob.valueOf('UNIT.TEST');
            }else{
                cntPDF1 = orderPg1.getContent();
            }
            
            List<Attachment> insertAttList = new List<Attachment>();
            
            Attachment att = new Attachment();
            att.Name = 'Epot_'+pobjQuote.Name+'.pdf';
            att.parentId = pobjQuote.Id;
            att.Body = cntPDF;
            
            Attachment att1 = new Attachment();
            att1.Name = 'Epot Line Items_'+pobjQuote.Name+'.pdf';
            att1.parentId = pobjQuote.Id;
            att1.Body = cntPDF1;
            
            insertAttList.add(att);
            insertAttList.add(att1);
            
            System.debug('----emailTemplate'+emailTemplate);
            
            if(emailTemplate != 'Quote SAP BookedService - Success VF' && emailTemplate != 'Quote SAP BookedSales - Success VF'){  // && emailTemplate != 'Quote SAP BookedService - Success VF_ Test'
                insert insertAttList;
                
                Set<Id> attchInsertIds = new Set<Id>();
                for(Attachment aRec : insertAttList){
                    attchInsertIds.add(aRec.Id);
                }
            }
            
            
            
            String functionality = '';
        
            if(pobjQuote.Order_Type__c == 'Service' && pobjQuote.SAP_Prebooked_Service__c != null) 
                functionality = 'Prebooked Service';
            
            if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.SAP_Prebooked_Sales__c != null) 
                functionality = 'Prebooked Sales';
            
            if(pobjQuote.Order_Type__c == 'Service' && pobjQuote.SAP_Booked_Service__c != null) 
                functionality = 'Booked Service';
            
            if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.SAP_Booked_Sales__c != null) 
                functionality = 'Booked Sales';
            
            if(pobjQuote.Order_Type__c == 'Sales' && pobjQuote.Fulfillment_Quote__c){
                functionality = 'Fullfilment';
            }
            
            Set<String> emailsSet = new Set<String>();
        
            if(pobjQuote.Order_Type__c == 'Sales' || pobjQuote.Order_Type__c == 'Combined'){
                
                if(pobjQuote.Order_Type__c == 'Combined' && pobjQuote.SAP_Prebooked_Sales__c != null) 
                    functionality = 'Prebooked Sales';      
                
                if(pobjQuote.Order_Type__c == 'Combined' && pobjQuote.SAP_Booked_Sales__c != null) 
                    functionality = 'Booked Sales';
                
                List<Email_Dlists__c> DListRecs = [Select Id,Name,Primary_Dlist__c,secondary_Dlist__c,Admin_Dlist__c,
                                                    Brachy_Dlist__c,Business_Dlist__c,Functionality__c,Region__c,
                                                    Support_Dlist__c,SalesOrg__c,Fulfillment_DList__c
                                                    From Email_Dlists__c 
                                                    Where Functionality__c =:functionality 
                                                    And Region__c =:pobjQuote.Quote_Region__c]; 
                
                system.debug('DListRecs---'+DListRecs);
                for(Email_Dlists__c DListRec : DListRecs){
                    
                    if(!String.isBlank(DListRec.Business_Dlist__c))
                        emailsSet.add(DListRec.Business_Dlist__c.trim());
                    
                    if(!String.isBlank(DListRec.Primary_Dlist__c))
                        emailsSet.add(DListRec.Primary_Dlist__c.trim());
                    
                    if(!String.isBlank(DListRec.Brachy_Dlist__c) && pobjQuote.Brachy_Sales__c)
                        emailsSet.add(DListRec.Brachy_Dlist__c.trim());
                    
                    if(!String.isBlank(DListRec.Support_Dlist__c))
                        emailsSet.add(DListRec.Support_Dlist__c.trim());
                    
                     if(!String.isBlank(DListRec.Admin_Dlist__c))
                        emailsSet.add(DListRec.Admin_Dlist__c.trim());
                    
                    if(!String.isBlank(DListRec.Secondary_Dlist__c))
                        emailsSet.add(DListRec.Secondary_Dlist__c.trim());
                    
                    if(!String.isBlank(DListRec.Fulfillment_DList__c) && pobjQuote.Fulfillment_Quote__c && pobjQuote.Order_Type__c == 'Sales'){
                        emailsSet.add(DListRec.Fulfillment_DList__c.trim());
                    }
                    
                }
            }
            
            if(pobjQuote.Order_Type__c == 'Services' || pobjQuote.Order_Type__c == 'Combined'){
                
                if(pobjQuote.Order_Type__c == 'Combined' && pobjQuote.SAP_Prebooked_Service__c != null) 
                    functionality = 'Prebooked Service';    
                
                if(pobjQuote.Order_Type__c == 'Combined' && pobjQuote.SAP_Booked_Service__c != null) 
                    functionality = 'Booked Service';
                
                List<Email_Dlists__c> DListRecs = [Select Id,
                                                   Name,
                                                   Primary_Dlist__c,
                                                   secondary_Dlist__c,
                                                   Admin_Dlist__c,
                                                   Brachy_Dlist__c,
                                                   Business_Dlist__c,
                                                   Functionality__c,
                                                   Region__c,
                                                   Support_Dlist__c ,
                                                   SalesOrg__c,
                                                   DocType__c
                                                   from Email_Dlists__c where Functionality__c = :functionality AND SalesOrg__c = :pobjQuote.Service_Org__c limit 1]; 
                
                system.debug('DListRecs---'+DListRecs);
                for(Email_Dlists__c DListRec : DListRecs){
                    if(DListRec.DocType__c == 'ZCSS'){
                        if(DListRec.Primary_Dlist__c != null && DListRec.Primary_Dlist__c != '')
                            emailsSet.add(DListRec.Primary_Dlist__c.trim());
                        
                        if(DListRec.Secondary_Dlist__c != null && DListRec.Secondary_Dlist__c != '')
                            emailsSet.add(DListRec.Secondary_Dlist__c.trim());
                    }
                    
                    if(DListRec.DocType__c == 'ZSQT'){
                        if(DListRec.Primary_Dlist__c != null && DListRec.Primary_Dlist__c != '')
                            emailsSet.add(DListRec.Primary_Dlist__c.trim());
                        
                        if(DListRec.Secondary_Dlist__c != null && DListRec.Secondary_Dlist__c != '')
                            emailsSet.add(DListRec.Secondary_Dlist__c.trim());
                    }
                }    
            }
            
            Map<Id,User> users = new Map<Id,User>([Select Id, Email 
                                                    from User 
                                                    where Id =: pobjQuote.ownerId 
                                                        or Id =: pobjQuote.Submitted_To_SAP_By__c]);
            if(users.containsKey(pobjQuote.ownerId) && pobjQuote.BigMachines__Account__r.Country__c != 'China'){
                emailsSet.add(users.get(pobjQuote.ownerId).Email);
            }
            if(users.containsKey(pobjQuote.Submitted_To_SAP_By__c)){
                emailsSet.add(users.get(pobjQuote.Submitted_To_SAP_By__c).Email);
            }
            if(pobjQuote.Project_Manager__c != null && pobjQuote.BigMachines__Account__r.Country__c != 'China')    
            {
               emailsSet.add( pobjQuote.Project_Manager__c);
            }

            // Add to send email to user as per country for acknowledgement
            
            if((emailTemplate == 'Quote SAP BookedSales - Success VF' || emailTemplate == 'Quote SAP BookedService - Success VF' ) && pobjQuote.Subscription_Status__c!='Error')  // || emailTemplate == 'Quote SAP BookedService - Success VF_ Test'
                emailsSet.addAll(getUserEmailByCountry(pobjQuote, 'SUCCESS'));
            else if (pobjQuote.Subscription_Status__c=='Error')
                emailsSet.addAll(getUserEmailByCountry(pobjQuote, 'ERROR'));
            else if (pobjQuote.Subscription_Status__c=='' || pobjQuote.Subscription_Status__c==null)
                emailsSet.addAll(getUserEmailByCountry(pobjQuote, 'SUCCESS'));

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            string [] toaddress = new List<String>();
            //emailsSet.add('matt.carvalho@varian.com');
            //emailsSet.add('David.Biller@varian.com');
            //emailsSet.add('narmada.kunamaneni@varian.com');
            system.debug('emailsSet---'+emailsSet);                  
            for(String s : emailsSet)
                toaddress.add(s.trim());
                
            mail.setToAddresses(toaddress);  
            //mail.setToAddresses(new List<string>{'rakesh.basani2@varian.com','Roop.Kaur@varian.com'}); 
            mail.setTargetObjectId(userRec.Id);
            mail.setWhatId(pobjQuote.Id);
            mail.setTemplateId(emailmap.get(emailTemplate));
            mail.setOrgWideEmailAddressId(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c);
            mail.setSaveAsActivity(false);
                  // Commented by Narmada as we no longer require attachments From Notes and attachments in success emails
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
              
            List<Attachment> aList  = [select Name,BodyLength from Attachment where ParentId = :pobjQuote.Id and Name != :att.Name and Name!= :att1.Name limit 10];
            system.debug('aList---'+aList);
            for(Attachment a : aList){    
                System.debug('----a.name'+a.name+'----'+a.BodyLength);
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                // Updated by Ajinkya to handle attachments getting corrupted when sent via email
                efa.setbody([select body from attachment where id = :a.id].body);
                //efa.setBody(a.Body);
                fileAttachments.add(efa);                   
            }
                
            Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
            efa1.setFileName(att.Name);
            efa1.setBody( cntPDF );
            
            
            Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
            efa2.setFileName(att1.Name);
            efa2.setBody(cntPDF1);
            
            fileAttachments.add(efa1);
            fileAttachments.add(efa2);
                
            if(!fileAttachments.isEmpty()){
                System.debug('-----attachments');
                mail.setFileAttachments(fileAttachments);
            }
            System.debug('----Email'+mail);
            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private Set<String> getUserEmailByCountry(BigMachines__Quote__c objQuote, String category) {
        Set<String> emailsSet = new Set<String>();

        String Country;
        if(objQuote.BigMachines__Account__r.Country__c != null)
            Country = objQuote.BigMachines__Account__r.Country__c;
        else if (objQuote.Ship_To_Country__c != null)
            Country = objQuote.Ship_To_Country__c;
        else
            Country = objQuote.BigMachines__Account__r.BillingCountry;

        if (Country != null)
            Country = String.valueOf(Country).trim();
        Country = '%'+ Country +'%';

        List<Email_Dlists__c> email_dlist_list = [Select Id, Email_List__c From Email_Dlists__c Where Order_Status__c=: category and Country__c LIKE : Country limit 1];
        if(email_dlist_list.size() > 0) {
            for(Email_Dlists__c dlist : email_dlist_list) {
                List<String> e_list = dlist.Email_List__c.split(';');
                for (String u_email : e_list) {
                    system.debug('-----EMAIL-------'+u_email.trim());
                    emailsSet.add(u_email.trim());
                }
            }
        }
        return emailsSet;
    }
}
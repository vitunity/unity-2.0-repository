@isTest
private class SR_PHILogTriggerTestClass {
    static testMethod void validatePHILogTrigger() {
        //create and insert users
        List < user > Users = testUtils.createListofUser(4);
        Profile profile_u = [Select Id From Profile Where Name='VMS Service - User' or  Name='VMS Unity 1C - Service User'][0];
        Profile profile_a = [Select Id From Profile Where Name='VMS Service - Admin' or Name='VMS Unity 1C - Service Admin'][0];
        //Update to Users managers
        Users [0].managerID = Users[1].ID;
        Users [1].managerID = Users[2].ID;
        Users [2].managerID = Users[3].ID;
        Users [0].ProfileId = profile_u.Id;
        Users [1].ProfileID = profile_a.Id;
        update Users;
        
        //Create PHI Log data
        PHI_Log__c b = new PHI_Log__c(OwnerID= Users[0].ID);
        System.debug('Manager before insert is: ' + b.Employee_s_Manager__c + 'Director before insert is: ' + b.Director_Dept_Head__c );
        
        //create PHI Log in system as VMS Service - User
        System.runAs(Users [0]){
            //insert the PHI Log record
            insert b;

            //Retrieve the PHI Log record
            b = [SELECT Employee_s_Manager__c, Director_Dept_Head__c FROM PHI_Log__c WHERE Id =: b.Id];
            System.debug('Manager after insert is: ' + b.Employee_s_Manager__c + 'Director after insert is: ' + b.Director_Dept_Head__c);

            //Test the trigger executed
            //System.assertEquals (Users[0].ManagerID, b.Employee_s_Manager__c);
            //System.assertEquals (Users[1].ManagerID, b.Director_Dept_Head__c);
        }
        //change to run as VMS Service - Admin
        System.runAs(Users [1]){

            //change the owner of the PHI Log record
            b.OwnerID = Users [1].ID;

            //update PHI Log with new owner
            try{
                update b;
            }catch(Exception e){
      }
            

            //Retrieve updated PHI Log record
            b = [SELECT Employee_s_Manager__c, Director_Dept_Head__c FROM PHI_Log__c WHERE Id =: b.Id];
            System.debug('Manager after insert is: ' + b.Employee_s_Manager__c + 'Director after insert is: ' + b.Director_Dept_Head__c);

            //Test the trigger updated properly
            //System.assertEquals (Users [1].ManagerID, b.Employee_s_Manager__c);
            //System.assertEquals (Users [2].ManagerID, b.Director_Dept_Head__c);
        }
    }
}
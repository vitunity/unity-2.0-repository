/*********************************
Last Modified by    :    Nikita Gupta
Last Modified On    :    3/3/2014
Last Modified Reason:    US3606, to fetch Employee's Manager details and his manager's details
Last Modified by    :    Nikita Gupta
Last Modified On    :    5/29/2014
Last Modified Reason:    to add exception handling and to add new owner condition for disposition type 'Regulatory Ownership Assigned and Transferred'
*********************************/
public class SR_PHI_Log 
{
    public PHI_Log__c phi{get;set;}
    public PHI_Log__c philog;
    public boolean destroyed{get;set;}
    public boolean transferred{get;set;}
    public boolean shared{get;set;}
    public boolean assigned{get;set;}
    public boolean Encrypt{get;set;}
    public List<SelectOption> statusOptions {get;set;}
    private ApexPages.StandardController stdController;
    Public String Overrideedit;
    public string Disposition{get;set;}
    public string Encryption{get;set;}
    public Id CaseId{get;set;}
    public PHI_Log__c phirec{get;set;}

    
    public SR_PHI_Log(ApexPages.StandardController controller) 
    { 
        String prefix = Schema.getGlobalDescribe().get('Case').getDescribe().getKeyPrefix();

        String idVal = null;

        Map<String, String> params = ApexPages.currentPage().getParameters();

        for (String key : params.keySet()) {

          if (key.startsWith('CF') && key.endsWith('lkid')) {

            String val = params.get(key);

            //if (val.startsWith(prefix)) {

              idVal = val;

              //break;

            //}

          }

        }
        phirec = new PHI_Log__c();
        destroyed = false;
        transferred = false;
        shared = false;
        assigned = false;
        Encrypt = false;
        CaseId= idVal; //Apexpages.currentPage().getParameters().get(idVal); 
        
        Overrideedit=Apexpages.currentPage().getParameters().get('sfdc.override'); 
        this.philog= (PHI_Log__c )controller.getRecord(); 
        this.stdController = controller;
        if(CaseId!=Null)
        {
            philog.Log_Type__c='Case';
            Case ListofCase=[Select Id,AccountId,ProductSystem__c,SVMXC__Top_Level__c from Case where id=:CaseId];
            philog.Account__c = ListofCase.AccountId;
            philog.Product_ID__c = ListofCase.ProductSystem__c; //DE3449, Updated from SVMXC__Top_Level__c to ProductSystem__c
            philog.Case__c = CaseId;
        }
        if(Overrideedit==Null)
        {
            philog.Case__c=CaseId;
            Case ListofCase=[Select Id,AccountId,ProductSystem__c,SVMXC__Top_Level__c from Case where id=:CaseId];
            philog.account__c=ListofCase.AccountId;
            philog.Product_ID__c=ListofCase.ProductSystem__c; //DE3449, Updated from SVMXC__Top_Level__c to ProductSystem__c
            if(CaseId!=Null)
            {
                List<SVMXC__Site__c> listoflocation =[Select Id from SVMXC__Site__c where SVMXC__Account__c=:listofcase.AccountId];
                for(SVMXC__Site__c location : listoflocation )
                {
                    philog.Location__c=location.Id;
                    break;
                }
            }
            List<User> Useralias=[Select Id, Manager.ID, Manager.Email from User where Id=:Userinfo.getUserId()];
            philog.Employee_s_Manager__c = Useralias[0].Manager.ID; //added by nikita for US3606 to update Manager Name on the record
            philog.Employee_s_Manager_Email_Address__c = Useralias[0].Manager.Email;
              
            Id MAnagerID= [ select id,ManagerId from user where id=: Userinfo.getUserId()][0].Managerid; //Added by Nikita to fetch Manager ID from current User
            List<User> Manageralias=[Select Id, Manager.ID, Manager.Email from User where Id = : MAnagerID]; //to fetch manager record
            if(Manageralias.size() > 0)
            { // added by harshita because of a list out of bound error.
                philog.Director_Dept_Head__c = Manageralias[0].Manager.ID; //added by nikita for US3606 to update Director/Dept Head(Manager's Manager)
                philog.Director_Dept_Head_Email__c = Manageralias[0].Manager.Email; //added by nikita for US3606 to update Director/Dept Head's(Manager's Manager) email address
            }
        }
    }
    
    Public void checkDestroyed()
    {
        //PHI_Log__c newphilog = (PHI_Log__c)stdController();
        //system.debug('#############3333333' + stdController.getrecord());
        Disposition = Apexpages.currentPage().getParameters().get('Disposition');
        if(Disposition =='Destroyed')
        destroyed= true;
        else
        destroyed = false;
        
        if(Disposition =='Transferred - Owner Copy Destroyed/Sent to New Owner')
        transferred = true;
        else
        transferred = false;
        
        if(Disposition =='Shared - Owner Copy Retained')
        shared = true;
        else
        shared = false;
        
        if(Disposition =='Regulatory Ownership Assigned and Transferred')
        assigned = true;
        else
        assigned = false;
        
        system.debug('########' + Disposition);
    }
    
    Public void checkEncrypt()
    {     
        Encryption = Apexpages.currentPage().getParameters().get('Encryption');
        if(Encryption =='No')
            Encrypt= true;
            else
            Encrypt= false;
        system.debug('########encrpt' + Encryption);
    }
    
    public pagereference dosave()
    {
        if(philog.Storage_Location2__c == 'Server (In house lab)' && philog.Server_Location__c == NULL)    //this block is added by Nikhil Verma (14-Feb-2014)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Server location since you have chosen Storage Location as Server (In house lab)'));
            return null;
        }
        //stdController = new ApexPages.StandardController(phirec);
        PageReference ret = stdController.save();
        Id PHIId= stdController.getId();
        if(PHIId != Null)
        {

            PHI_Log__c phialias=[Select Case__c,OwnerId,Date_Obtained__c,Complaint__c,Reference_Number__c,Location__c,Product_ID__c,Purpose_of_PHI__c,Method_of_Receipt__c,Data_Format__c,Encryption_Method__c,Encryption_Details__c,
                                Storage_Location2__c,Data_Security_Details__c,Disposition2__c,Status__c,Date_Destroyed__c,Data_destruction_details__c,Short_Description__c,Notes__c, Account__c from PHI_Log__c where Id =:PHIId]; //Commented for US5161 'New_Owner__c'
            system.debug('------>>>'+phialias);
            if(phialias.Disposition2__c!='Destroyed')
            {
                phialias.Date_Destroyed__c=Null;
                Update phialias;
            }
            
            if(phialias.Disposition2__c == 'Regulatory Ownership Assigned and Transferred' ) //(|| phialias.Disposition2__c=='Shared - Owner Copy Retained' || phialias.Disposition2__c=='Transferred - Owner Copy /Sent to New Owner') Commented for story US4058 ny Nikita on 3/27/14
            {
                PHI_Log__c newphi = new PHI_Log__c();
                newphi.Case__c = phialias.Case__c;
                newphi.Date_Obtained__c = phialias.Date_Obtained__c;
                newphi.OwnerId = phialias.OwnerId;
                newphi.Location__c = phialias.Location__c;
                /*if(phialias.New_Owner__c != null)
                {
                    newphi.OwnerId = phialias.New_Owner__c;
                } 
                else                
                {
                    newphi.OwnerId = phialias.OwnerId;
                }*/ //Commented for US5161
                newphi.OwnerId = phialias.OwnerId;
                newphi.Account__c = phialias.Account__c;
                newphi.Product_ID__c = phialias.Product_ID__c;
                newphi.Purpose_of_PHI__c = phialias.Purpose_of_PHI__c;
                newphi.Method_of_Receipt__c = phialias.Method_of_Receipt__c;
                newphi.Data_Format__c = phialias.Data_Format__c;
                newphi.Encryption_Method__c = phialias.Encryption_Method__c;
                newphi.Encryption_Details__c = phialias.Encryption_Details__c;
                newphi.Storage_Location2__c = phialias.Storage_Location2__c;
                newphi.Data_Security_Details__c = phialias.Data_Security_Details__c;
                //newphi.Disposition2__c = phialias.Disposition2__c;
                newphi.Status__c = 'Open';
                newphi.Date_Destroyed__c = phialias.Date_Destroyed__c;
                newphi.Data_destruction_details__c = phialias.Data_destruction_details__c;
                
                if(phialias.Disposition2__c == 'Shared - Owner Copy Retained')
                {
                    phialias.Status__c='Open';
                }
                else
                {
                    phialias.Status__c='Closed';
                }
                Update phialias;
                try //exception handling (added by Nikita, 5/29/2014)
                {
                    insert newphi;
                }
                catch(Exception e)
                {
                    ApexPages.addMessages(e);
                }
            }
         
            if((phialias.Disposition2__c == 'Shared - Owner Copy Retained' || phialias.Disposition2__c == 'Transferred - Owner Copy Destroyed/Sent to New Owner')) //Commented for US5161, ' && phialias.New_Owner__c != null'
            {
                PHI_Log__c objPhi = new PHI_Log__c();
                objPhi.Case__c = phialias.Case__c;
                objPhi.Account__c = phialias.Account__c;
                objPhi.Product_ID__c = phialias.Product_ID__c;
                // objPhi.OwnerID = phialias.New_Owner__c;  //Commented for US5161
                objPhi.Parent_PHI__c = phialias.ID;
               
               try //exception handling (added by Nikita, 5/29/2014)
                {
                    insert objPhi;
                }
                catch(Exception e)
                {
                    ApexPages.addMessages(e);
                }
            }
            PageReference Page = new PageReference('/'+ phialias.Case__c);
            return Page;//ret;
        }
         
        return null;
    }     
}
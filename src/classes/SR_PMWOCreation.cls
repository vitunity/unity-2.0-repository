global class SR_PMWOCreation
{
    static Id wdprodservId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
    static Id wdusagconsId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
    static Id projManagRecTypeWO = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
            
    WebService static void CreatePMWO(string erpwbsidvar)
    {
        if(erpwbsidvar != null)
        {
            //CM : DFCT0012019 : Modified the query to add new fields.
            List<ERP_WBS__c> erpwbslist = new List<ERP_WBS__c>([SELECT Id, ERP_Reference__c,ERP_PM_Work_Center__c,ERP_Project_Nbr__c,ERP_Sales_Order_Number__c,ERP_Sales_Order_Item_Number__c, Is_CLSD__c, Is_DLFL__c, Is_TECO__c,Sales_Order_Item__c,Sales_Order__c,Org__c,
                                                                  (SELECT id,name FROM Work_Orders__r order by createddate DESC limit 1),Status__c  
                                                               FROM ERP_WBS__c where id =:erpwbsidvar limit 1]);
            ERP_WBS__c ERPWBS = new ERP_WBS__c();
            
            
            if(erpwbslist.size() == 1)
            {
                ERPWBS = erpwbslist[0];
                SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c();
                List<SVMXC__Service_Group_Members__c> techResult;
                map<String,ERP_Org__c> orgNameIdMap = new map<String,ERP_Org__c>(); // CM : DFCT0012421 
                if(ERPWBS.ERP_PM_Work_Center__c  != null)
                {
                    //CM : DFCT0011619: Modified the Query to add Default_Plant__c field. //CM: DFCT0012019 
                    techResult = [SELECT Id,User__c,Default_Plant__c,SVMXC__Service_Group__c From SVMXC__Service_Group_Members__c where ERP_Work_Center__c = :ERPWBS.ERP_PM_Work_Center__c AND User__c != null limit 1]; 
                }

                For (ERP_Org__c erporg : [select Id, Sales_Org__c, PM_No_Cross_Bill__c, Default_Plant__c from ERP_Org__c])
                {
                    orgNameIdMap.put(erporg.Sales_Org__c, erporg);
                }
                List<Case> CaseResult;
                if(ERPWBS.ERP_Project_Nbr__c  != null)
                {
                    CaseResult = [SELECT Id,ProductSystem__c,ProductSystem__r.SVMXC__Sales_Order_Number__c,ProductSystem__r.ERP_Parent__c From Case where ERP_Project_Number__c = : ERPWBS.ERP_Project_Nbr__c  limit 1]; 
                }

                List<Sales_Order__c> soResult;
                if(ERPWBS.ERP_Sales_Order_Number__c  != null)
                {
                    soResult = [SELECT Id From Sales_Order__c where ERP_Reference__c = : ERPWBS.ERP_Sales_Order_Number__c ]; 
                }

                List<Sales_Order_Item__c> soiResult;
                if(ERPWBS.ERP_Sales_Order_Item_Number__c  != null)
                {
                    soiResult = [SELECT Id,Site_Partner__c,Sales_Order__r.Site_Partner__c,Sales_Order__c,Site_Partner2__c From Sales_Order_Item__c where ERP_Reference__c = : ERPWBS.ERP_Sales_Order_Item_Number__c ]; 
                }

                List<SVMXC__Installed_Product__c> ipResult ;
                if(CaseResult.size() > 0 && CaseResult != null && ERPWBS.ERP_Sales_Order_Item_Number__c != null && CaseResult[0].ProductSystem__c != null && CaseResult[0].ProductSystem__r.ERP_Parent__c != null)
                {
                    ipResult = [SELECT Id,ERP_Parent__c,SVMXC__Sales_Order_Number__c  from SVMXC__Installed_Product__c where SVMXC__Sales_Order_Number__c = :ERPWBS.ERP_Sales_Order_Item_Number__c  AND ERP_Parent__c = :CaseResult[0].ProductSystem__r.ERP_Parent__c ]; 
                }

                if( !ERPWBS.Is_DLFL__c && !ERPWBS.Is_CLSD__c && !ERPWBS.Is_TECO__c) 
                {
                    
                    WO.RecordTypeId = projManagRecTypeWO;
                    WO.SVMXC__Purpose_of_Visit__c = 'New Installation'; 
                    WO.ERP_WBS__c = ERPWBS.Id;
                    if(CaseResult != null && !CaseResult.isEmpty())
                    {
                        WO.SVMXC__Case__c = CaseResult[0].Id;
                        WO.SVMXC__Component__c = CaseResult[0].ProductSystem__c;
                    }
                    /*US5223: 
                    if(soResult != null && !soResult.isEmpty())
                    {
                        WO.Sales_Order__c = soResult[0].Id;
                    }
                    */
                    if(soiResult != null && !soiResult.isEmpty())
                    {
                        if (soiResult[0].Site_Partner__c != null)
                        {
                            WO.SVMXC__Company__c = soiResult[0].Site_Partner__c;
                        }
                        else if (soiResult[0].Sales_Order__c != null && soiResult[0].Sales_Order__r.Site_Partner__c != null)
                        {
                            WO.SVMXC__Company__c = soiResult[0].Sales_Order__r.Site_Partner__c;   
                        }
                        WO.ERP_Partner__c = soiResult[0].Site_Partner2__c;
                    }
                    //CM : DFCT0012019  : Added below lines of code.
                    if (ERPWBS.Sales_Order__c != null)
                    {
                        WO.Sales_Order__c = ERPWBS.Sales_Order__c;
                    }
                    if (ERPWBS.Sales_Order_Item__c != null)
                    {
                        WO.Sales_Order_Item__c = ERPWBS.Sales_Order_Item__c;
                    }
                    if (ERPWBS.ERP_Project_Nbr__c != null)
                    {
                        WO.ERP_Project_Nbr__c = ERPWBS.ERP_Project_Nbr__c;
                    }
                    //CM : DFCT0012019 . End of changes
                    if(ipResult != null && !ipResult.isEmpty())
                    {
                        WO.SVMXC__Top_Level__c = ipResult[0].Id;
                        WO.ERP_Sales_Order_Higher_Item_Nbr__c = ipResult[0].ERP_Parent__c;
                        WO.ERP_Sales_Order_Item__c = ipResult[0].SVMXC__Sales_Order_Number__c;
                    }
                    if(techResult != null && !techResult.isEmpty())
                    {
                        WO.SVMXC__Group_Member__c = techResult[0].Id;
                        WO.OwnerId = techResult[0].User__c;
                        WO.Service_Team__c = techResult[0].SVMXC__Service_Group__c; //CM: DFCT0012019 : Fixed the preferred service team issue on create PMWO creation
                        //CM : DFCT0012421 Commented as no longer needed. wo.sales_Org__c = techResult[0].Default_Plant__c; //CM : DFCT0011619: setting the Sales_Org__c to technician Default Point.
                    }   
                    //CM : DFCT0012421 
                    if(ERPWBS.Org__c != null && !String.isBlank(ERPWBS.Org__c))
                    {
                        WO.sales_Org__c = ERPWBS.Org__c;
                        if (orgNameIdMap.ContainsKey(ERPWBS.Org__c))
                        {
                            WO.ERP_Org_Lookup__c = orgNameIdMap.get(ERPWBS.Org__c).id;
                        }
                    }       
                    // DFCT0011577, INC4211283 : W.O.Status should be assigned
                    WO.SVMXC__Order_Status__c = 'Assigned';
                    System.Debug('***CM WO: '+WO);
                    insert WO;
                }

                /* Commented by amit and sudhakar 
                   duplicate product serviced records get created.

                ERPWBS = [SELECT Id, ERP_Reference__c,ERP_PM_Work_Center__c,ERP_Project_Nbr__c,ERP_Sales_Order_Number__c,ERP_Sales_Order_Item_Number__c,(SELECT id,name FROM Work_Orders__r order by createddate DESC limit 1),Status__c  FROM ERP_WBS__c where id =:erpwbsidvar limit 1];
                
                List<SVMXC__Service_Order_Line__c> listInsertProdServ_WD = new List<SVMXC__Service_Order_Line__c>();
                /*jkondrat/forefront: 
                -no idea why we assume there will be only one WD 
                -no idea why we cloning own WD
                *
                if(WO.id != null && ERPWBS.Work_Orders__r.size() == 1)
                {
                    system.debug('$$$$$$$$$wo.id'+WO.id);
                    Map<String, Schema.SObjectField> WDfieldMap = Schema.SObjectType.SVMXC__Service_Order_Line__c.fields.getMap();
                    String wdsoql = 'SELECT ';
                    for(Schema.SObjectField wdfield :WDfieldMap.values())
                    {
                        if(wdsoql == 'SELECT ')
                        wdsoql += String.valueof(wdfield) ;
                        
                        else 
                        wdsoql += ',' + String.valueof(wdfield)  ;
                    }
                    wdsoql += '  FROM SVMXC__Service_Order_Line__c where recordTypeID = : wdprodservId AND SVMXC__Service_Order__c = \''+ ERPWBS.Work_Orders__r[0].id + '\'' ;
                    
                    system.debug('$$$$$$$wosoql'+wdsoql);
                    List<SVMXC__Service_Order_Line__c> wds = (List<SVMXC__Service_Order_Line__c>)Database.query(wdsoql);
                    system.debug('$$$$$$$$$wos'+wds);
                    for(SVMXC__Service_Order_Line__c wdprodserv :wds )
                    {
                        SVMXC__Service_Order_Line__c wdprodservclone = wdprodserv.clone(false,true,false,false);
                        wdprodservclone.SVMXC__Group_Member__c = WO.SVMXC__Group_Member__c;
                        wdprodservclone.SVMXC__Service_Order__c = WO.id;
                        listInsertProdServ_WD.add(wdprodservclone);
                        system.debug('$$$$$$$$$listInsertProdServ_WD'+listInsertProdServ_WD);
                    }
                    //jkondrat: DE5884
                    if(WO.id != null){
                        SVMXC__Service_Order_Line__c laborWD = new SVMXC__Service_Order_Line__c();
                        //List<ERP_NWA__c> laborNWAList = new List<ERP_NWA__c>([select Id from ERP_NWA__c where WBS_Element__c = :erpwbsidvar and ERP_Std_Text_Key__c = 'PM00001' and (NOT (ERP_Status__c includes('CNF') and (NOT ERP_Status__c like '%TECO%') and (NOT ERP_Status__c like '%DLFL%') limit 1]);
                        List<ERP_NWA__c> laborNWAList = new List<ERP_NWA__c>([select Id 
                                                     from ERP_NWA__c 
                                                     where WBS_Element__c = :erpwbsidvar 
                                                     and ERP_Std_Text_Key__c = 'PM00001' 
                                                     and ERP_Status__c excludes('CNF','TECO','DLFL') limit 1]);
                                                     
                        if(laborNWAList != null && laborNWAList.size() == 1){
                            laborWD.ERP_NWA__c = laborNWAList[0].id;
                            laborWD.SVMXC__Service_Order__c = WO.id;
                            laborWD.RecordTypeId = wdprodservId;
                            laborWD.ERP_WBS__c = erpwbsidvar;
                            laborWD.SVMXC__Is_Billable__c = false;  
                            laborWD.Billing_Type__c = 'I – Installation'; //savon.FF 12-2-2015 DE6820 Long Dash
                            
                            listInsertProdServ_WD.add(laborWD);
                        }
                    }
                    
                    if(listInsertProdServ_WD.size() > 0)
                    {
                        //insert listInsertProdServ_WD;
                    }
                }

                */
            }
        }
    } 
    
    private static map<String,SVMXC__Service_Group_Members__c> getTechMap(set<string> workcenterIds){
        map<String,SVMXC__Service_Group_Members__c> technicainMap = new map<String,SVMXC__Service_Group_Members__c>();
        list<SVMXC__Service_Group_Members__c> techResult = [SELECT Id,User__c, ERP_Work_Center__c, SVMXC__Service_Group__c From SVMXC__Service_Group_Members__c 
                                                            where ERP_Work_Center__c in :workcenterIds 
                                                            AND User__c != null ];
        for(SVMXC__Service_Group_Members__c t : techResult){
            technicainMap.put(t.ERP_Work_Center__c, t);
        }                                             
        return technicainMap;       
                                               
    }
    
    private static map<String,Case> getCaseMap(set<string> projectNumIds){
        List<Case> caseResult = [SELECT Id,ProductSystem__c,ProductSystem__r.SVMXC__Sales_Order_Number__c,ProductSystem__r.ERP_Parent__c,ERP_Project_Number__c 
                                 From Case 
                                 where ERP_Project_Number__c in : projectNumIds]; 
        map<String,Case> caseMap = new map<String,Case>();
        for(Case c : caseResult){
            caseMap.put(c.ERP_Project_Number__c,c);
        }
        return caseMap;
    }
    
    private static map<String,Sales_Order__c> getSalesOrdersMap(set<string> soids){
        List<Sales_Order__c> soResult = [SELECT Id, ERP_Reference__c From Sales_Order__c where ERP_Reference__c in : soids]; 
        map<String,Sales_Order__c> salesOrdersMap = new map<String,Sales_Order__c>();
        for(Sales_Order__c so : soResult){
            salesOrdersMap.put(so.ERP_Reference__c,so);
        }
        return salesOrdersMap;
    }
    
    private static map<String,Sales_Order_Item__c> getSalesOrdersLineMap(set<string> solids){
        List<Sales_Order_Item__c> soiResult = [SELECT Id,Site_Partner__c,sales_order__c,sales_order__r.Site_Partner__c,Site_Partner2__r.Account__c, ERP_Reference__c,Site_Partner2__c 
                                               From Sales_Order_Item__c 
                                               where ERP_Reference__c in : solids]; 
                 
        map<String,Sales_Order_Item__c> salesOrdersMap = new map<String,Sales_Order_Item__c>();
        for(Sales_Order_Item__c sol : soiResult){
            salesOrdersMap.put(sol.ERP_Reference__c,sol);
        }
        return salesOrdersMap;
    }
    
    private static map<String,SVMXC__Installed_Product__c> getIpMap(set<string> solids, set<string> erpParentIds){
         List<SVMXC__Installed_Product__c> ipResult = [SELECT Id,ERP_Parent__c,SVMXC__Sales_Order_Number__c  
                                                       from SVMXC__Installed_Product__c 
                                                       where SVMXC__Sales_Order_Number__c in : solids  AND ERP_Parent__c in : erpParentIds]; 
        map<String,SVMXC__Installed_Product__c> ipMap = new map<String,SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c ip : ipResult){
            ipMap.put(ip.SVMXC__Sales_Order_Number__c,ip);
        }
        return ipMap;
    }
    
    public static void CreatePMWO(set<Id> wbsIds){
        //CM : DFCT0012019 : Modified the query to add new fields.
       List<ERP_WBS__c> erpwbslist = new List<ERP_WBS__c>([SELECT Id, ERP_Reference__c,ERP_PM_Work_Center__c,ERP_Project_Nbr__c,Sales_Order_Item__c,Sales_Order__c,
                            ERP_Sales_Order_Number__c,ERP_Sales_Order_Item_Number__c, Is_DLFL__c, Is_TECO__c, Is_CLSD__c,Org__c,
                            (SELECT id,name FROM Work_Orders__r order by createddate DESC limit 1),Status__c  
                            FROM ERP_WBS__c where id in : wbsIds]);
       
        set<string> workcenterIds = new set<string>();
        set<string> projectNumIds = new set<string>();
        set<string> soids = new set<string>();
        set<string> solids = new set<string>();
        list<SVMXC__Service_Order__c> workOrders = new list<SVMXC__Service_Order__c> ();
        map<String,ERP_Org__c> orgNameIdMap = new map<String,ERP_Org__c>(); // CM : DFCT0012421 
        /* CM : not needed : DFCT0011619
        set<String> wbsSet = new set<String>(); // DFCT0011625, 10 Aug 2016,
        map<String,ERP_Org__c> orgNameIdMap = new map<String,ERP_Org__c>(); // DFCT0011625, 10 Aug 2016,
        */
        // CM : DFCT0012421    
        For (ERP_Org__c erporg : [select Id, Sales_Org__c, PM_No_Cross_Bill__c, Default_Plant__c from ERP_Org__c])
        {
            orgNameIdMap.put(erporg.Sales_Org__c, erporg);
        }

        for(ERP_WBS__c wbs : erpwbslist){
            if(wbs.ERP_PM_Work_Center__c  != null){
                workcenterIds.add(wbs.ERP_PM_Work_Center__c);
            }
            if(wbs.ERP_Project_Nbr__c  != null){
                projectNumIds.add(wbs.ERP_Project_Nbr__c);
            }
            if(wbs.ERP_Sales_Order_Number__c  != null){
                soids.add(wbs.ERP_Sales_Order_Number__c);
            }
           
            if(wbs.ERP_Sales_Order_Item_Number__c  != null){
                solids.add(wbs.ERP_Sales_Order_Item_Number__c);
            } 
           
            // CM : DFCT0011619 : not needed : // wbsSet.add(wbs.Org__c); // DFCT0011625, 10 Aug 2016,
        }
       /* CM : not needed : DFCT0011619
        // DFCT0011625, 10 Aug 2016,
        List<ERP_Org__c> orgList = [select Id, Sales_Org__c, PM_No_Cross_Bill__c from ERP_Org__c where Sales_Org__c IN: wbsSet];
        for(ERP_Org__c org : orgList){
            orgNameIdMap.put(org.Sales_Org__c,org);
        }
        // DFCT0011625, 10 Aug 2016,
        */
        map<String,SVMXC__Service_Group_Members__c> technicainMap = getTechMap(workcenterIds);
        map<String,Case> caseMap = getCaseMap(projectNumIds);
        map<String,Sales_Order__c> salesOrdersMap = getSalesOrdersMap(soids);
        map<String,Sales_Order_Item__c> salesOrderLineMap = getSalesOrdersLineMap(solids);
       
        set<string> erpParentIds = new set<string>();
        for(Case cs : caseMap.values()){
           if(cs.ProductSystem__r.ERP_Parent__c != null)
           erpParentIds.add(cs.ProductSystem__r.ERP_Parent__c);
       }
       
       map<String,SVMXC__Installed_Product__c> ipMap = getIpMap(solids, erpParentIds);  
       
       for(ERP_WBS__c wbs : erpwbslist){
           if(wbs.is_DLFL__c || wbs.Is_CLSD__c || wbs.is_TECO__c){
               system.debug('Not aplicable to create service order');
           }else{
               SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c();
               WO.RecordTypeId = projManagRecTypeWO;
               WO.SVMXC__Purpose_of_Visit__c = 'New Installation'; 
               WO.ERP_WBS__c = wbs.Id;
               WO.SVMXC__Order_Status__c = 'Assigned'; //CM : DFCT0011768 : status of workorder to be set to Assigned. //DFCT0012153
               //CM : DFCT0012019 : Added the below lines of code.
               if (wbs.Sales_Order__c != null)
               {
                    WO.Sales_Order__c = wbs.Sales_Order__c;
               }
               if (wbs.Sales_Order_Item__c != null)
               {
                    WO.Sales_Order_Item__c = wbs.Sales_Order_Item__c;
               }
               if (wbs.ERP_Project_Nbr__c != null)
               {
                    WO.ERP_Project_Nbr__c = wbs.ERP_Project_Nbr__c;
               }
               //CM : end of DFCT0012019 
               if(technicainMap.containsKey(wbs.ERP_PM_Work_Center__c)){
                   WO.SVMXC__Group_Member__c = technicainMap.get(wbs.ERP_PM_Work_Center__c).Id;
                   WO.Service_Team__c = technicainMap.get(wbs.ERP_PM_Work_Center__c).SVMXC__Service_Group__c;   // INC4278793
                   WO.SVMXC__Service_Group__c = technicainMap.get(wbs.ERP_PM_Work_Center__c).SVMXC__Service_Group__c;   // INC4278793
                   WO.OwnerId = technicainMap.get(wbs.ERP_PM_Work_Center__c).User__c;
               }
               if(caseMap.containsKey(wbs.ERP_Project_Nbr__c)){
                   WO.SVMXC__Case__c = caseMap.get(wbs.ERP_Project_Nbr__c).Id;
                   WO.SVMXC__Component__c = caseMap.get(wbs.ERP_Project_Nbr__c).ProductSystem__c;
                   if(ipMap.containsKey(wbs.ERP_Sales_Order_Item_Number__c) &&
                      caseMap.get(wbs.ERP_Project_Nbr__c).ProductSystem__r.ERP_Parent__c != null &&
                      ipMap.get(wbs.ERP_Sales_Order_Item_Number__c).ERP_Parent__c == caseMap.get(wbs.ERP_Project_Nbr__c).ProductSystem__r.ERP_Parent__c){
                           WO.SVMXC__Top_Level__c = ipMap.get(wbs.ERP_Sales_Order_Item_Number__c).Id;
                           WO.ERP_Sales_Order_Higher_Item_Nbr__c = ipMap.get(wbs.ERP_Sales_Order_Item_Number__c).ERP_Parent__c;
                           WO.ERP_Sales_Order_Item__c = ipMap.get(wbs.ERP_Sales_Order_Item_Number__c).SVMXC__Sales_Order_Number__c;
                      }
               }
               if(salesOrderLineMap.containsKey(wbs.ERP_Sales_Order_Item_Number__c)){
                   WO.SVMXC__Company__c = salesOrderLineMap.get(wbs.ERP_Sales_Order_Item_Number__c).Site_Partner__c;
                   WO.ERP_Partner__c = salesOrderLineMap.get(wbs.ERP_Sales_Order_Item_Number__c).Site_Partner2__c;
                   if(WO.SVMXC__Company__c == null && salesOrderLineMap.get(wbs.ERP_Sales_Order_Item_Number__c).Sales_Order__c != null && salesOrderLineMap.get(wbs.ERP_Sales_Order_Item_Number__c).Sales_Order__r.Site_Partner__c != null)          {
                            WO.SVMXC__Company__c =salesOrderLineMap.get(wbs.ERP_Sales_Order_Item_Number__c).Sales_Order__r.Site_Partner__c;
                   }
               }
               //CM : DFCT0012421 
                if(wbs.Org__c != null && !String.isBlank(wbs.Org__c))
                {
                    WO.sales_Org__c = wbs.Org__c;
                    if (orgNameIdMap.ContainsKey(wbs.Org__c))
                    {
                        WO.ERP_Org_Lookup__c = orgNameIdMap.get(wbs.Org__c).id;
                    }
                }
               /* CM : DFCT0011619 : not needed 
                // DFCT0011625, 10 Aug 2016, If cross billing on ERP Org is checked then setting Interface Status to 'Do Not Process' 
                if(orgNameIdMap.get(wbs.Org__c).PM_No_Cross_Bill__c)
                    WO.Interface_Status__c = 'Do Not Process'; */
               
               workOrders.add(WO);
           }
       }
       if(!workOrders.isEmpty()){
           insert workOrders;
       }   
            
    } 
}
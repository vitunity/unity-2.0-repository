@isTest
public class SR_PMWOCreation_test
{
	public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    Static testmethod void SR_PMWOCreationTest()
    {

    	User serviceUser = InsertUserData(true);
		Account newAcc = AccountData(true);
		Sales_Order__c so = SO_Data(true, newAcc);
		Sales_Order_Item__c soi = SOI_Data(true, so);
		ERP_Project__c erpProject = ERPProject_Data(true, newAcc);
		ERP_WBS__c erpWBS = ERPWBS_Data(true, erpProject);
		erpWBS.Sales_Order__c = so.Id;
		erpWBS.Sales_Order_Item__c = soi.Id;
		update erpWBS;
		Test.startTest();
        SVMXC__Service_Group__c objServTeam = new SVMXC__Service_Group__c();
        objServTeam.Name = 'ABC';
        objServTeam.SVMXC__Active__c = true;
        objServTeam.Dispatch_Queue__c = 'DISP CH';
        objServTeam.District_Manager__c = userInfo.getUserID();
        
        objServTeam.SVMXC__Group_Code__c = 'ABC';
        insert objServTeam;

        SR_PMWOCreation pmwoClass = new SR_PMWOCreation();
		SR_PMWOCreation.CreatePMWO(erpWBS.id);
		set<ID> erpwbsids = new set<ID>();
		erpwbsids.add(erpWBS.id);
		SR_PMWOCreation.CreatePMWO(erpwbsids);
		Test.stopTest();
    }

    public static Account AccountData(Boolean isInsert) {
		Account newAcc = new Account();
		newAcc.Name ='Test Account';
		newAcc.Account_Type__c = 'Customer';
		newAcc.SAP_Account_Type__c = 'Z001';
		newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
		newAcc.Country__c = 'USA';
		if(isInsert)
			insert newAcc;
		return newAcc;
	}
	
	public static ERP_Project__c ERPProject_Data(Boolean isInsert, Account newAcc) {
		ERP_Project__c erpProject = new ERP_Project__c();
		erpProject.Name = 'M-0319112526';
		erpProject.Sold_To__c  = newAcc.Id;
		erpProject.Start_Date__c  =  system.today();
		erpProject.End_Date__c  =  system.today().addMonths(3);
		erpProject.Description__c  = '11 / 2008 - BETH ISRAEL MEDICAL CENTER.';
		erpProject.Org__c = '0601';
		erpProject.ERP_PM_Work_Center__c = 'H87803';
		if(isInsert)
			insert erpProject;
		return erpProject;
	}
	
	public static ERP_WBS__c ERPWBS_Data(Boolean isInsert, ERP_Project__c erpProject) {
		ERP_WBS__c erpWBS = new ERP_WBS__c();
		erpWBS.name = 'M-0319112526-0001-99';
		erpWBS.ERP_Status__c = 'REL';
		erpWBS.ERP_Network_Nbr__c = '4001734';
		erpWBS.ERP_Reference__c = 'M-0319112526-0001-99';
		erpWBS.ERP_Project_Nbr__c = 'M-0319112526';
		erpWBS.ERP_PM_Work_Center__c = 'H87696';
		erpWBS.ERP_Project__c = erpProject.Id;
		erpWBS.ERP_Sales_Order_Number__c = '1234';
		erpWBS.ERP_Sales_Order_Item_Number__c = '1234';
		if(isInsert)
			insert erpWBS;
		return erpWBS;
	}
    public static Sales_Order__c SO_Data(Boolean isInsert, Account newAcc) {
		Sales_Order__c so = new Sales_Order__c();
		so.name = '319112526';
		so.Site_Partner__c = newAcc.Id;
		so.Sold_To__c = newAcc.Id;
		so.Requested_Delivery_Date__c = system.today().addMonths(-1);
		so.Sales_Group__c = 'N08';
		so.Sales_District__c = 'NA02';
		so.Status__c ='Open';
		so.Sales_Org__c = '0601';
		so.ERP_Reference__c = '1234';
		if(isInsert)
			insert so;
		return so;
	}
	
	public static Sales_Order_Item__c SOI_Data(Boolean isInsert, Sales_Order__c so) {
		Sales_Order_Item__c soi = new Sales_Order_Item__c();
		soi.Name = '000005';
		soi.Sales_Order__c = so.id;
		soi.ERP_Material_Number__c = 'FI_REV_ITEM';
		soi.ERP_Equipment_Number__c = '123';
		soi.ERP_Reference__c = '1234';
		if(isInsert)
			insert soi;
		return soi;
	}

	public static User InsertUserData(Boolean isInsert) {
		profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
		User serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
						alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
						LocaleSidKey = 'en_US',UserName='test@service.com');
		try {
		if(isInsert)
			insert serviceUser;
		} catch(Exception e) {
			system.debug(' == Exception == ' + e.getMessage() + ' = ' + e.getLineNumber());
		}
		return serviceUser;
	}
}
/*************************************************************************\
    @ Author        : Vipul Jain
    @ Date      : 20-Aug-2013
    @ Description   : This class is used to call the appproval process(through apex code) of all open part order associated with the work order 
    @ Last Modified By  :   
    @ Last Modified On  :  
    @ Last Modified Reason  :   
****************************************************************************/
public class SR_PartsOrderApproval_Submit {
    
    public list<SVMXC__RMA_Shipment_Order__c> OpenPartsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
    ApexPages.StandardController control;
    // controller starts here.
    public SR_PartsOrderApproval_Submit(ApexPages.StandardController controller) {
        // querying the open parts associated with current work order which are of type "Shipment".
        OpenPartsOrderList  = [select Id,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Order_Status__c='Open' and SVMXC__Service_Order__c!=null and SVMXC__Service_Order__c=:apexpages.currentpage().getparameters().get('id') and RecordType.DeveloperName='Shipment'];
        control = controller;

    }
    // controller ends here.
    // Below method used to submit all open 
    public pagereference SubmitRecordForApproval(){
        try{
            /* checking the size of list which contains open parts , if no open parts associated with the work order , then
            show an message to user.*/
            if(OpenPartsOrderList.size()==0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Work order has no "open" parts for approval'));
                return null;
            }
            // iterating on list of open parts list and approval process of parts order is being called.
            for(SVMXC__RMA_Shipment_Order__c partsorder :OpenPartsOrderList){
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Parts Order Submitted For Approval');// set the comment of the approval process
                req1.setObjectId(partsorder.id); // set the id of record.
                Approval.ProcessResult result = Approval.process(req1);// submit the record for approval
    
            }
            pagereference pg=control.view();
            return pg;// returning the user to detail page of work order.
        }
        //catch block starts here , to catch any exception occured.
        catch(exception e){
            system.debug('exception occured-->'+e);
            return null;
        }
    }
    

}
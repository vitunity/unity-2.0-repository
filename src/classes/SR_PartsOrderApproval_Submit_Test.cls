/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = true)
private class SR_PartsOrderApproval_Submit_Test {
  
    public Static SVMXC__Service_Order__c WO;
    
    static void setupData(){
        Country__c country = testUtils.createCountry('United States');
        insert country;
        Account account = testUtils.createAccount('Test', 'AUSSA', country.Name, country.Id);
        insert account;
        Case cs = testUtils.createCase('Test subject');
        insert cs;        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c();
        insert serviceOrder;
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(UserInfo.getUserId());
        insert serviceGroup;
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=UserInfo.getUserId(), ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, 
        RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
        SVMXC__Service_Order__c = serviceOrder.Id,SVMXC__Order_Status__c = 'Open',
        SVMXC__Case__c = cs.Id, Cancel__c = false );
        insert po;
       
    }
    static testMethod void SR_PartsOrderApproval_Submit_Test() {
        setupData();
        System.Test.StartTest();
        WO = new SVMXC__Service_Order__c();
        WO = [SELECT Id FROM SVMXC__Service_Order__c LIMIT 1];
      
        ApexPages.StandardController control = new ApexPages.StandardController(WO);
        SR_PartsOrderApproval_Submit controller = new SR_PartsOrderApproval_Submit(control);
        
        PageReference pageRef = Page.SR_PartsOrderApproval_Submit;
        pageRef.getParameters().put('id', String.valueOf(WO.Id));
        Test.setCurrentPage(pageRef);
        System.Test.StopTest();
    }
    
    public static testMethod void SubmitRecordForApproval_NO_ORDER_LIST_Test() {
        setupData();
        System.Test.StartTest();
        WO = new SVMXC__Service_Order__c();
        WO = [SELECT Id FROM SVMXC__Service_Order__c LIMIT 1];
      
        ApexPages.StandardController control = new ApexPages.StandardController(WO);
        SR_PartsOrderApproval_Submit controller = new SR_PartsOrderApproval_Submit(control);
        
        PageReference pageRef = Page.SR_PartsOrderApproval_Submit;
        pageRef.getParameters().put('id', String.valueOf(WO.Id));
        Test.setCurrentPage(pageRef);
        
        PageReference expectedRef = controller.SubmitRecordForApproval();
        system.assertEquals(expectedRef, null);
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        for(ApexPages.Message message : pageMessages) {
        if(message.getSeverity() == ApexPages.Severity.Info) {
          system.assert(true);
        }
      
        if(message.getSummary() == 'Work order has no "open" parts for approval') {
          system.assert(true);
        }
       }
        System.Test.StopTest();
    }
    
    @isTest(SeeAllData = true)
    public static void SubmitRecordForApproval_Test() {
      setupData();
      Test.StartTest();
      
      SVMXC__RMA_Shipment_Order__c pOrder = [SELECT Id, SVMXC__Order_Status__c, RecordTypeId, SVMXC__Case__c, SVMXC__Company__c, SVMXC__Contact__c, 
                             Technician__c, SVMXC__Source_Location__c, SVMXC__Destination_Location__c, SVMXC__Service_Order__c
                           FROM SVMXC__RMA_Shipment_Order__c WHERE SVMXC__Order_Status__c='Open' AND SVMXC__Service_Order__c !=: null 
                               AND RecordType.DeveloperName='Shipment' Limit 1];
      system.debug(' ===== pOrder ===== ' + pOrder);
      
      WO = new SVMXC__Service_Order__c();
      WO = [SELECT Id FROM SVMXC__Service_Order__c WHERE Id =: pOrder.SVMXC__Service_Order__c LIMIT 1];
      
      ApexPages.StandardController control = new ApexPages.StandardController(WO);
        SR_PartsOrderApproval_Submit controller = new SR_PartsOrderApproval_Submit(control);
        
        PageReference pageRef = Page.SR_PartsOrderApproval_Submit;
        pageRef.getParameters().put('id', String.valueOf(WO.Id));
        Test.setCurrentPage(pageRef);
        
        controller.OpenPartsOrderList.add(pOrder);
        PageReference expectedRef = controller.SubmitRecordForApproval();
        Test.StopTest();
    }
}
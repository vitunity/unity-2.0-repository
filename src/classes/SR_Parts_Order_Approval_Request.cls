public class SR_Parts_Order_Approval_Request {
    List<SVMXC__RMA_Shipment_Order__c> PartsOrder{get;set;}

    public SR_Parts_Order_Approval_Request(ApexPages.StandardController controller) {
       PartsOrder = new list<SVMXC__RMA_Shipment_Order__c>();
       PartsOrder = [select Id,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Service_Order__c =:apexpages.currentpage().getparameters().get('id')];
       
    }
    public pagereference SendEmailToUser(){
        SVMXC__Service_Order__c Workorder = new SVMXC__Service_Order__c();
        Workorder = [select Id,Parts_Order_Approval_Request__c from SVMXC__Service_Order__c where Id=:apexpages.currentpage().getparameters().get('id')];
        Workorder.Parts_Order_Approval_Request__c = True;
        update Workorder;
        pagereference pg = new pagereference('/'+Workorder.Id);
        
        return pg;
    }

}
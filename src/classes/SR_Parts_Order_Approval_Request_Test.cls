/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_Parts_Order_Approval_Request_Test {
	
	public Static SVMXC__Service_Order__c WO;
	
	// Test Method for SR_Parts_Order_Approval_Request, SeeAllData true as its is a configuration Data
	@isTest(SeeAllData=true)
    static void SR_Parts_Order_Approval_Request_Test() {
        Test.StartTest();
    	
    	WO = new SVMXC__Service_Order__c();
    	WO = [SELECT Id FROM SVMXC__Service_Order__c LIMIT 1];
    	
    	ApexPages.StandardController control = new ApexPages.StandardController(WO);
        SR_Parts_Order_Approval_Request controller = new SR_Parts_Order_Approval_Request(control);
        
        PageReference pageRef = Page.SR_Parts_Order_Approval_Request;
        pageRef.getParameters().put('id', String.valueOf(WO.Id));
        Test.setCurrentPage(pageRef);
    	
    	Test.StopTest();
    	
    }
    
    // Test Method for SendEmailToUser, SeeAllData true as its is a configuration Data
    @isTest(SeeAllData=true)
    static void SendEmailToUser_Test() {
    	Test.starttest();
    	
    	WO = new SVMXC__Service_Order__c();
    	WO = [SELECT Id FROM SVMXC__Service_Order__c LIMIT 1];
    	
    	PageReference expectedPageRef = new PageReference('/'+WO.Id);
    	
    	ApexPages.StandardController control = new ApexPages.StandardController(WO);
        SR_Parts_Order_Approval_Request controller = new SR_Parts_Order_Approval_Request(control);
        
        PageReference pageRef = Page.SR_Parts_Order_Approval_Request;
        pageRef.getParameters().put('id', String.valueOf(WO.Id));
        Test.setCurrentPage(pageRef);
    	
    	PageReference actualPageRef = controller.SendEmailToUser();
    	
    	system.assertEquals(expectedPageRef.getURL(), actualPageRef.getURL());
    	
    	Test.stoptest();
    }
    
}
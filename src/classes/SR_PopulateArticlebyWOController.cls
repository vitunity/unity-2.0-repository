/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 25-Sep-2013
      @ Description           : This Class is Used to Create an Article from a WO.
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/
public class SR_PopulateArticlebyWOController 
{
 private final SVMXC__Service_Order__c WO;
    public SR_PopulateArticlebyWOController(ApexPages.StandardController controller) 
    {
      this.WO= (SVMXC__Service_Order__c )controller.getRecord();  
    }
    public pagereference GenerateArticle()
    {
       String WOId = ApexPages.currentPage().getParameters().get('id');
       Service_Article__kav article= new Service_Article__kav();
       Boolean ExceptionHandled=false;
       try
       {
       
       if(WOID!=Null)
       {
         SVMXC__Service_Order__c WOalias=[Select Id,Subject__c,Service_Comment__c,SVMXC__Work_Performed__c,SVMXC__Problem_Description__c,SVMXC__Special_Instructions__c,SVMXC__Corrective_Action__c from SVMXC__Service_Order__c where id=:WOId ];
         
         article.Article_Image__c='Work Performed :'+WOalias.SVMXC__Work_Performed__c+'<br></br><br></br>'+'Special Instructions :'+WOalias.SVMXC__Special_Instructions__c+'<br></br><br></br>'+'Corrective Action :'+WOalias.SVMXC__Corrective_Action__c;
         article.Article_Image__c=WOalias.Service_Comment__c+'<br></br>'+WOalias.SVMXC__Problem_Description__c;
         system.debug('########## article.Article_Image__c' + article.Article_Image__c);
         article.Summary=WOalias.Subject__c;
         article.Title=WOalias.Subject__c.trim() + system.today().format().trim();// ading time stamp US4258
         system.debug('########## article.Title' + article.Title);
         article.UrlName=WOalias.Subject__c.trim().replaceall(' ','-') + system.now().format().replaceall('\\s','-').replaceall('/','-').replaceall(':','-').replaceall(' ','-'); // '$$$';//system.today().format();//modified by kaushiki for DE955
         system.debug('##########' + article.UrlName);
         insert article;
         system.debug('##########' + article.id);
         
       }
       
       }
       catch(Exception e)
       {
       
         system.debug('@@@@@@@@' + e.getmessage());
         ExceptionHandled=True;
         ApexPages.Message error = new ApexPages.Message(ApexPages.SEVERITY.INFO,'There is already an Article created by this WO Subject.');
         ApexPages.addMessage(error);
         
       }
       if(ExceptionHandled==False)
       {
       PageReference pagereferencealias= new PageReference('/'+article.Id);
       pagereferencealias.setRedirect(true);
       return pagereferencealias; 
       }
       return null;
    }

}
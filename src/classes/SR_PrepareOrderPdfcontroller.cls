public with sharing class SR_PrepareOrderPdfcontroller
{
    public string salesManager{get;set;} 
    public string imageURL{get;set;}
    public BigMachines__Quote__c objQuote {get;set;}
    public string site {get; set;}
    public string contrAdmin {get; set;}
    Public string salesAdministrator {get; set;}
    public string siteProjManager {get; set;}
    public string soldTo {get; set;}
    public string SitePartner {get; set;}
    public string shipTo {get; set;}
    public string billTo {get; set;}
    public string payer {get; set;} 
    public string salesAdmin {get; set;}
    public string prod {get; set;}
    public string soldToAdd {get; set;}
    public string SitePartnerAdd {get; set;}
    public string shipToAdd {get; set;}
    public string billToAdd {get; set;}
    public string payerAdd {get; set;}
    public string quoteCreator {get; set;}
    public Datetime QuoteDate {get; set;}
    public String Contingencies {get;set;}
    public List<BigMachines__Quote_Product__c> quoteProducts;
    public String soldtoparty{get;set;}
    public String soldToAddrLine1 {get;set;}
    public String soldToAddrLine2 {get;set;}
    public String soldToAddrLine3 {get;set;}
    public String soldToAddrLine4 {get;set;}
    public String soldToAcctName {get;set;}
    
    public String SitePartnerAcctName {get;set;}
    public String SitePartnerAddrLine1 {get;set;}
    public String SitePartnerAddrLine2 {get;set;}
    public String SitePartnerAddrLine3 {get;set;}
    public String SitePartnerAddrLine4 {get;set;}
    
    public String shipToAddrLine1 {get;set;}
    public String shipToAddrLine2 {get;set;}
    public String shipToAddrLine3 {get;set;}
    public String shipToAddrLine4 {get;set;}
    
    public String billToAddLine1 {get;set;}
    public String billToAddLine2 {get;set;}
    public String billToAddLine3 {get;set;}
    public String billToAddLine4 {get;set;}
    
    public String payerAddrLine1 {get;set;}
    public String payerAddrLine2 {get;set;}
    public String payerAddrLine3 {get;set;}
    public String payerAddrLine4 {get;set;}    
    
    public String endUser {get;set;}
    public String endUserAdd {get;set;}
    public String endUserAddLine1 {get;set;}
    public String endUserAddLine2 {get;set;}
    public String endUserAddLine3 {get;set;}
    public String endUserAddLine4 {get;set;}
    
    public SR_PrepareOrderPdfcontroller()
    {
        imageURL = '/servlet/servlet.FileDownload?file=015c0000000Akzb';
        if(Apexpages.currentpage().getparameters().get('id') != null)
        {
        objQuote = [SELECT Id,
                           Name,
                           Subscription_Quote__c,
                           Subscription_Only_Quote__c,
                           Billing_Frequency__c,
                           Offer_in_USD__c,
                           prd_ht3__c,
                           BigMachines__Total_Amount__c,
                           Contract_Start_Date__c,
                           Contract_End_Date__c,
                           Sales_Org__c, 
                           Service_Org__c,
                           Order_Type__c,
                           SAP_Prebooked_Sales__c,
                           SAP_Prebooked_Service__c,
                           No_Contingencies__c,
                           Release_Date__c,
                           Contingency_Release_Date__c,
                           Certificate_of_Need_Replacement__c,
                           Certificate_of_Need_New__c,
                           Board_Approval_Required__c,
                           Plan_to_Financing_or_Lease__c,
                           Export_License_Required__c,
                           Other_Blocks__c,                           
                           Purchase_Order_Number__c, 
                           Projected_Ship_Date__c, 
                           Quote_Type__c, 
                           Requested_Delivery_Date__c, 
                           CreatedById, 
                           Payment_Terms__c, 
                           IncoTerms__c, 
                           prd_ht1__c,
                           prd_hbp__c,
                           Penalty_Terms__c,
                           Customer_Penalty__c,
                           Varian_penalty__c,
                           Actions_Required__c,
                           Reference_Documents__c,
                           Custom_Billing_Milestones__c,
                           Installation_Acceptance_Instructions__c,
                           Configuration_Instructions__c,
                           Shipping_instructions__c, 
                           Reference_Quotation__c,
                           Principal_Contact__c,
                           Principal_Contact__r.Name,
                           Principal_Contact__r.Title,
                           Principal_Contact__r.Phone,
                           Principal_Contact__r.OtherPhone,
                           Purchasing_Contact__c,
                           Purchasing_Contact__r.Name,
                           Purchasing_Contact__r.Title,
                           Purchasing_Contact__r.Phone,
                           Purchasing_Contact__r.OtherPhone,
                           Medical_Contact__C,
                           Medical_Contact__r.Name,
                           Medical_Contact__r.Title,
                           Medical_Contact__r.Phone,
                           Medical_Contact__r.OtherPhone,
                           Physics_Contact__c,
                           Physics_Contact__r.Name,
                           Physics_Contact__r.Title,
                           Physics_Contact__r.Phone,
                           Physics_Contact__r.OtherPhone,
                           Technical_Contact__c,
                           Technical_Contact__r.Name,
                           Technical_Contact__r.Title,
                           Technical_Contact__r.Phone,
                           Technical_Contact__r.OtherPhone,
                           Booking_Message__c,
                           Message_Service__c,
                           SAP_PreBooked_Date_Sales__c,   
                           SAP_PreBooked_Date_Service__c,
                           Down_Payment__c,
                           Pre_Shipment__c,
                           ShipmentValue__c,
                           Delivery__c,
                           AcceptanceValue__c,
                           PostAcceptanceValue__c,
                           Voltage__c,
                           Frequency__c,
                           Original_Order_Value__c,
                           Purchase_Order_Date__c,
                           Purchase_Order_Received_Date__c,
                           Interface_Status__c,
                           Sales_Order__c,
                           Sales_Order__r.Name,
                           Service_Payment_Terms__c,
                           Functional_Location__c,
                           Affiliated_Account__c,
                           BigMachines__Opportunity__c, 
                           BigMachines__Opportunity__r.Conversion_Rate__c,
                           BigMachines__Opportunity__r.NA_Affiliation__c, 
                           BigMachines__Opportunity__r.NA_Pricing_Applied__c,
                           BigMachines__Opportunity__r.Accountid,
                           BigMachines__Opportunity__r.Account.BillingStreet,
                           BigMachines__Opportunity__r.Account.BillingCity,
                           BigMachines__Opportunity__r.Account.BillingState,
                           BigMachines__Opportunity__r.Account.BillingCountry,
                           BigMachines__Opportunity__r.Account.BillingPostalCode,
                           BigMachines__Account__c,
                           Total_Delta__c,
                           Total_Discountable_Threshold__c,Special_Payment_Terms__c
                           FROM BigMachines__Quote__c WHERE Id = : Apexpages.currentpage().getparameters().get('id')];
                                   
        List<Quote_Product_Partner__c> qotProdPartList = [SELECT Id, 
                                                                 Name, 
                                                                 ERP_Partner_Number__c, 
                                                                 ERP_Partner_Function__c, 
                                                                 Quote__c, ERP_Partner__c,
                                                                 ERP_Partner__r.Partner_Name__c,
                                                                 ERP_Partner__r.Partner_Name_Line_1__c, 
                                                                 ERP_Partner__r.Partner_Name_Line_2__c, 
                                                                 ERP_Partner__r.Partner_Name_Line_3__c, 
                                                                 ERP_Partner__r.City__c,
                                                                 ERP_Partner__r.Country_Code__c,
                                                                 ERP_Partner__r.Zipcode_Postal_Code__c,
                                                                 ERP_Partner__r.Street__c,
                                                                 ERP_Contact_Number__c,
                                                                 ERP_Contact_Function__c,
                                                                 ERP_Partner__r.State_Province_Code__c FROM Quote_Product_Partner__c 
                                                                 WHERE Quote__c = :objQuote.Id
                                                                 order by LastModifiedDate];
        
         System.debug('Qute product list :: '+qotProdPartList);       
        if(objQuote.No_Contingencies__c)
        {    
            Contingencies = 'False';
        }
        else
        {
            Contingencies = 'True';
        }
            
        for(Quote_Product_Partner__c qpp : qotProdPartList)
        {
            if(qpp.ERP_Contact_Function__c == 'ZN')
            { 
                List<Contact> conRec = [Select Id, Name from Contact where SAP_Contact_ID__c =: qpp.ERP_Contact_Number__c limit 1];
                
                if(!conRec.isEmpty())
                {    
                    salesManager = conRec[0].Name;
                }
            }
            else if(qpp.ERP_Contact_Function__c == 'ZJ')
            { 
                List<Contact> conRec = [Select Id, Name from Contact where SAP_Contact_ID__c =: qpp.ERP_Contact_Number__c limit 1];
                
                if(!conRec.isEmpty())
                {    
                    salesAdministrator = conRec[0].Name;
                }
            }
            else if(qpp.ERP_Contact_Function__c == 'ZO')
            { 
                List<Contact> conRec = [Select Id, Name from Contact where SAP_Contact_ID__c =: qpp.ERP_Contact_Number__c limit 1];
                
                if(!conRec.isEmpty())
                {    
                    contrAdmin = conRec[0].Name;
                }
            }
            else if(qpp.ERP_Contact_Function__c == 'PM')
            {
                List<Contact> conRec = [Select Id, Name from Contact where SAP_Contact_ID__c =: qpp.ERP_Contact_Number__c limit 1];
                
                if(!conRec.isEmpty())
                {    
                    siteProjManager = conRec[0].Name;
                }
            }
            else if(qpp.ERP_Partner_Function__c == 'SP')
            {
                soldtoparty ='';
                soldToAddrLine1 =''; 
                soldToAddrLine2 ='';
                soldToAddrLine3 ='';
                soldToAddrLine4 ='';
                soldToAcctName = '';
                soldTo = qpp.ERP_Partner_Number__c;
                soldToAdd = '';
                if(qpp.ERP_Partner__r != NULL)
                {   
                    if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        soldToAddrLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        soldToAddrLine2 = soldToAdd + qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        soldToAddrLine3 = soldToAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        soldToAddrLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        soldToAddrLine4 = soldToAddrLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        soldToAddrLine4 = soldToAddrLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
            else if(qpp.ERP_Partner_Function__c == 'SH')
            {
                shipToAddrLine1 = '';
                shipToAddrLine2 = '';
                shipToAddrLine3 = '';
                shipToAddrLine4 = '';
                shipTo = qpp.ERP_Partner_Number__c;
                shipToAdd = '';
                if(qpp.ERP_Partner__r != NULL)
                {   if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        shipToAddrLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_2__c != NULL)
                    {
                        shipToAddrLine2 = shipToAdd + qpp.ERP_Partner__r.Partner_Name_Line_2__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_3__c != NULL)
                    {
                        shipToAddrLine3 = shipToAdd + qpp.ERP_Partner__r.Partner_Name_Line_3__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        shipToAdd = shipToAdd + qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        shipToAdd = shipToAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        shipToAddrLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        shipToAddrLine4 = shipToAddrLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        shipToAddrLine4 = shipToAddrLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
            
            else if(qpp.ERP_Partner_Function__c == 'Z1')
            {
                SitePartnerAddrLine1 = '';
                SitePartnerAddrLine2 = '';
                SitePartnerAddrLine3 = '';
                SitePartnerAddrLine4 = '';
                SitePartner = qpp.ERP_Partner_Number__c;
                SitePartnerAdd = '';
                if(qpp.ERP_Partner__r != NULL)
                {   if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        SitePartnerAddrLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_2__c != NULL)
                    {
                        SitePartnerAddrLine2 = SitePartnerAdd + qpp.ERP_Partner__r.Partner_Name_Line_2__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_3__c != NULL)
                    {
                        SitePartnerAddrLine3 = SitePartnerAdd + qpp.ERP_Partner__r.Partner_Name_Line_3__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        SitePartnerAdd = SitePartnerAdd + qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        SitePartnerAdd = SitePartnerAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        SitePartnerAddrLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        SitePartnerAddrLine4 = SitePartnerAddrLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        SitePartnerAddrLine4 = SitePartnerAddrLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
            
            else if(qpp.ERP_Partner_Function__c == 'BP')
            {
                billToAddLine1 = '';
                billToAddLine2 = '';
                billToAddLine3 = '';
                billToAddLine4 = '';
                billTo = qpp.ERP_Partner_Number__c;
                billToAdd = '';
                if(qpp.ERP_Partner__r != NULL) 
                {   if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        billToAddLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_2__c != NULL)
                    {
                        billToAddLine2 = billToAdd + qpp.ERP_Partner__r.Partner_Name_Line_2__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_3__c != NULL)
                    {
                        billToAddLine3 = billToAdd + qpp.ERP_Partner__r.Partner_Name_Line_3__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        billToAdd = billToAdd + qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        billToAdd = billToAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        billToAddLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        billToAddLine4 = billToAddLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        billToAddLine4 = billToAddLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
            else if(qpp.ERP_Partner_Function__c == 'EU')
            {
                endUserAddLine1 = '';
                endUserAddLine2 = '';
                endUserAddLine3 = '';
                endUserAddLine4 = '';
                endUser = qpp.ERP_Partner_Number__c;
                endUserAdd = '';
                if(qpp.ERP_Partner__r != NULL) 
                {   if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        endUserAddLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_2__c != NULL)
                    {
                        endUserAddLine2 = qpp.ERP_Partner__r.Partner_Name_Line_2__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Partner_Name_Line_3__c != NULL)
                    {
                        endUserAddLine3 = qpp.ERP_Partner__r.Partner_Name_Line_3__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        endUserAdd = qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        endUserAdd = endUserAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        endUserAddLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        endUserAddLine4  = endUserAddLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        endUserAddLine4 = endUserAddLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
            else if(qpp.ERP_Partner_Function__c == 'PY')
            {
                payerAddrLine1 =''; 
                payerAddrLine2 ='';
                payerAddrLine3 ='';
                payerAddrLine4 ='';
                payer = qpp.ERP_Partner_Number__c;
                payerAdd = '';
                if(qpp.ERP_Partner__r != NULL)
                {   
                    if(qpp.ERP_Partner__r.Partner_Name_Line_1__c != NULL)
                    {
                        payerAddrLine1 = qpp.ERP_Partner__r.Partner_Name_Line_1__c + '\n';
                    }
                    if(qpp.ERP_Partner__r.Street__c != NULL)
                    {
                        payerAddrLine2 = payerAdd + qpp.ERP_Partner__r.Street__c;
                    }
                    if(qpp.ERP_Partner__r.City__c!= NULL)
                    {
                        payerAddrLine3 = payerAdd +' '+qpp.ERP_Partner__r.City__c;
                    }
                    if(qpp.ERP_Partner__r.State_Province_Code__c!= NULL)
                    {
                        payerAddrLine4 = qpp.ERP_Partner__r.State_Province_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Country_Code__c != NULL)
                    {
                        payerAddrLine4 = payerAddrLine4 +' '+qpp.ERP_Partner__r.Country_Code__c;
                    }
                    if(qpp.ERP_Partner__r.Zipcode_Postal_Code__c != NULL)
                    {
                        payerAddrLine4 = payerAddrLine4 +' '+qpp.ERP_Partner__r.Zipcode_Postal_Code__c;
                    }
                }
            }
        }
        salesAdmin = [SELECT Id, Name FROM User WHERE Id = :Userinfo.getUserId()].Name;
        List<BigMachines__Quote_Product__c> qotProdList = [SELECT Id, Name,Section_Name__c, Booking_Value_v1__c,
                                                                      BigMachines__Product__c,
                                                                      BigMachines__Total_Price__c,
                                                                      BigMachines__Quote__c, BigMachines__Sales_Price__c,
                                                                      BigMachines__Product__r.Name,BigMachines__Quantity__c
                                                                      FROM BigMachines__Quote_Product__c WHERE BigMachines__Quote__c = :objQuote.Id LIMIT 1];
          
                                                                  
        prod = qotProdList[0].Section_Name__c;//commented for test class kaushiki
        
        quoteCreator = [SELECT Id, Name FROM User WHERE Id = :objQuote.CreatedById].Name;
        QuoteDate = [SELECT id, CreatedDate from BigMachines__Quote__c WHERE Id = : Apexpages.currentpage().getparameters().get('id')].CreatedDate;

        }
    }
     
    public Decimal totalPrice {get;set;}
    
    public List<QuoteProductWrapper> getquoteProducts(){
        List<QuoteProductWrapper> quotProductWrapperList = new List<QuoteProductWrapper>();
        
        if(objQuote != null){     
            quoteProducts = [SELECT Id, Name,
                                    BigMachines__Quantity__c,
                                    BigMachines__Product__c,
                                    Section_Name__c,
                                    Section_Number__c,
                                    BigMachines__Product__r.name, 
                                    BigMachines__Description__c, 
                                    Cost__c, 
                                    Grp__c,
                                    Customer_Price__c, 
                                    Delivery_Date__c, 
                                    BigMachines__Sales_Price__c, 
                                    BigMachines__Quote__c , 
                                    Requested_Delivery_Date__c,
                                    Threshold_Price__c,Booking_Value_v1__c,
                                    Header__c,
                                    Booking_Price_Non_Discountable__c,
                                    BigMachines__Product__r.Booking_Discountable__c,
                                    BigMachines__Product__r.Discountable__c,
                                    BigMachines__Quote__r.Total_Discountable_Threshold__c,
                                    BigMachines__Quote__r.BigMachines__Total_Amount__c,
                                    BigMachines__Total_Price__c
                                    FROM BigMachines__Quote_Product__c WHERE Grp__c != Null AND BigMachines__Quote__c = :objQuote.Id
                                    ORDER BY Grp__c ASC];
           
            totalPrice = 0;
            Map<Decimal,QuoteProductWrapper> sectionQuoteProducts = new Map<Decimal,QuoteProductWrapper>(); 
            for(BigMachines__Quote_Product__c BP : quoteProducts){
                                
                if(BP.BigMachines__Total_Price__c != null)      
                    totalPrice = totalPrice + BP.BigMachines__Total_Price__c;
                
                if(sectionQuoteProducts.containsKey(BP.Section_Number__c)){
                    
                    QuoteProductWrapper QPW = sectionQuoteProducts.get(BP.Section_Number__c);
                    
                    if(BP.Header__c){
                        QPW.prodName = BP.Section_Name__c;
                    }                   
                    QPW.quoteProducts.add(BP);
                }else{
                    
                    QuoteProductWrapper QPW = new QuoteProductWrapper();
                    if(BP.Header__c){
                        QPW.prodName = BP.Section_Name__c;
                    }   
                    QPW.quoteProducts = new List<BigMachines__Quote_Product__c>();
                    QPW.quoteProducts.add(BP);
                    quotProductWrapperList.add(QPW);
                    sectionQuoteProducts.put(BP.Section_Number__c, QPW);
                }
            }
            System.debug('----sectionQuoteProducts'+sectionQuoteProducts);
       }
        
        for(QuoteProductWrapper QP : quotProductWrapperList){
            
            QP.offerPriceTotal = 0;
            QP.bookingValTotal = 0;
            QP.thresholdPriceTotal = 0;
            QP.DifferenceTotal = 0;
            QP.totalCostTotal = 0;
            
            for(BigMachines__Quote_Product__c B : QP.quoteProducts){
                if(B.BigMachines__Sales_Price__c != null){
                    QP.offerPriceTotal = QP.offerPriceTotal + (B.BigMachines__Sales_Price__c.setScale(2) * B.BigMachines__Quantity__c.setScale(0));
                }
                if(B.Booking_Value_v1__c != null){
                    QP.bookingValTotal = QP.bookingValTotal + (B.Booking_Value_v1__c.setScale(2));
                }
                if(B.Threshold_Price__c != null){
                    QP.thresholdPriceTotal = QP.thresholdPriceTotal + B.Threshold_Price__c;
                }
                //if(B.BigMachines__Sales_Price__c != null){QP.DifferenceTotal = QP.DifferenceTotal + B.BigMachines__Sales_Price__c;}
                if(B.Cost__c != null){QP.totalCostTotal = QP.totalCostTotal + B.Cost__c;}
            }
            
           // QP.DifferenceTotal = QP.bookingValTotal - QP.thresholdPriceTotal;
               QP.DifferenceTotal = QP.offerPriceTotal-QP.bookingValTotal ;
        }
        
        return quotProductWrapperList;
    } 
    
    public String getSubscriptionNumbers(){
        String contractNumbers = '';
        if(objQuote!=null){
            for(Subscription__c sfSubscription : [SELECT Id,Name FROM Subscription__c WHERE Quote__c =:objQuote.Id]){
                contractNumbers = contractNumbers+sfSubscription.Name+',';
            }
            contractNumbers = contractNumbers.removeEnd(',');
        }
        return contractNumbers;
    }
    
    public List<SR_PrepareOrderController.SAASProduct> getSubscriptions(){
        if(objQuote!=null){
            return SR_PrepareOrderController.getSubscriptionProductDetails(objQuote.Id);
        }
        return new List<SR_PrepareOrderController.SAASProduct>();
    }
    
    public Map<String,String> getSubscriptionContractNumbers(){
        Map<String,String> contractNumbers = new Map<String,String>();
        if(objQuote!=null){
            for(Subscription__c subscription : [SELECT Name,Product_Type__c FROM Subscription__c WHERE Quote__c =:objQuote.Id]){
                contractNumbers.put(subscription.Product_Type__c, subscription.Name);
            }
        }
        return contractNumbers;
    }
    
    public class QuoteProductWrapper
    {
        public String prodName {get;set;}
        
        public List<BigMachines__Quote_Product__c> quoteProducts {get;set;}
        public Decimal offerPriceTotal {get;set;}
        public Decimal bookingValTotal {get;set;}
        public Decimal thresholdPriceTotal {get;set;}
        public Decimal DifferenceTotal {get;set;}
        public Decimal totalCostTotal {get;set;}
        
    }
    
}
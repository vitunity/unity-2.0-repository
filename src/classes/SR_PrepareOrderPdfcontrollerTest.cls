@isTest(seeAllData=true)
private class SR_PrepareOrderPdfcontrollerTest {

    static testMethod void testSR_PrepareOrderPdfcontroller() {
        SR_PrepareOrderControllerTest.insertERPOrg();
        //SR_PrepareOrderControllerTest.insertRegions();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        Test.startTest();
            SR_PrepareOrderControllerTest.insertContactRoleAssociation();
            SR_PrepareOrderControllerTest.insertLookUpvalues();
            PageReference pageRefSales = Page.SR_PrepareOrder;
            Test.setCurrentPage(pageRefSales);
            ApexPages.currentPage().getParameters().put('id',SR_PrepareOrderControllerTest.salesQuote.Id);
            
            SR_PrepareOrderController sendToERPSales = new SR_PrepareOrderController();
            sendToERPSales.selectedRegion = 'APAC';
            sendToERPSales.selectedSalesOrg = SR_PrepareOrderControllerTest.erpOrg.Id;
            sendToERPSales.soldToNumber = 'Sales11111';
            sendToERPSales.sitePartnerNumber = 'Sales11111';
            sendToERPSales.billToNumber = 'Sales11111';
            sendToERPSales.shipToNumber = 'Sales11111';
            sendToERPSales.payerNumber = 'Sales11111';
            sendToERPSales.endUserNumber = 'Sales11111';
            sendToERPSales.selectedProjectManager = 'erpref123';
            sendToERPSales.selectedOpSpecialist = 'erpref123';
            sendToERPSales.selectedSalesManager = 'erpref123';
            sendToERPSales.selectedContractAdmin = 'erpref123';
            sendToERPSales.objQuote.Distribution_Channel__c = '20 - International';
            sendToERPSales.objQuote.Purchase_Order_Number__c = '121212';
            sendToERPSales.objQuote.Purchase_Order_Date__c = Date.today().addDays(10);
            sendToERPSales.objQuote.Projected_Ship_Date__c = Date.today().addDays(20);
            sendToERPSales.objQuote.Requested_Delivery_Date__c = Date.today().addDays(30);
            sendToERPSales.objQuote.Contract_Start_Date__c = Date.today().addDays(30);
            sendToERPSales.objQuote.Contract_End_Date__c = Date.today().addYears(1);
            sendToERPSales.objQuote.Order_reason__c = 'HS1-ISS contract';
            sendToERPSales.objQuote.Prd_OS__c = 'SalesTESTPRDOS';
            sendToERPSales.objQuote.Payment_Terms__c = 'Net 111 Days';
            sendToERPSales.objQuote.Service_Payment_Terms__c = 'Same as Sales';
            sendToERPSales.objQuote.Customer_Penalty__c = 'CPenalty';
            sendToERPSales.objQuote.Varian_penalty__c = 'VPenalty';
            sendToERPSales.getQuoteProductWrapperList();
            sendToERPSales.submitOrder();
            Test.stopTest();
            PageReference orderPdf = Page.SR_PrepareOrderPdf;
            orderPdf.getParameters().put('id', SR_PrepareOrderControllerTest.salesQuote.Id);
            Test.setCurrentPage(orderPdf);
            
            SR_PrepareOrderPdfcontroller prepareOrderPdf = new SR_PrepareOrderPdfcontroller();
            prepareOrderPdf.getquoteProducts();
       
    }
}
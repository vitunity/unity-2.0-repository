public class SR_PrintServiceContractController {

    public SVMXC__Service_Contract__c objContract{get;set;}
    public string ContractId{get;set;}
    public list<SVMXC__Service_Contract_Products__c> relatedcoveredprod{get;set;}
    public list<SVMXC__Service_Contract_Services__c> relatedincservices{get;set;}
    public list<SVMXC__Service_Contract_Sites__c> relatedcovlocation{get;set;}
    public decimal total{get;set;}
    public decimal total1{get;set;}
    
    public SR_PrintServiceContractController(ApexPages.StandardController controller) 
    {
  
    total=0;
    total1=0;
       ContractId = apexpages.currentpage().getparameters().get('Id');
       objContract =[SELECT SVMXC__company__r.name,SVMXC__company__r.BillingPostalCode,SVMXC__Service_Level__r.Name,Calculated_date__c,Contract_type_description__c,CreatedById,CreatedDate,CurrencyIsoCode,Default_Service_Group__c,ERP_Cancellation_Reason__c,ERP_Collective_No__c,ERP_Contract_Nbr__c,ERP_Contract_Type__c,ERP_Customer_Group_4__c,ERP_Customer_Group_5__c,ERP_Customer_Note__c,ERP_Internal_Note__c,ERP_Location_Name__c,ERP_National_Account__c,ERP_Order_Reason__c,ERP_PO_Date__c,ERP_PO_Number__c,ERP_Prior_Contract__c,ERP_Sales_Org__c,ERP_SLA_Product__c,ERP_Sold_To_Name__c,GMAX_Combined_Supplied__c,GMAX_Labor_Supplied__c,GMAX_Labor__c,GMAX_Parts_Suppiled__c,GMAX_Parts__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Location__c,Name,National_Account__c,Non_Revenue_of_days__c,OwnerId,PO__c,Quote_Reference__c,Renewal_Opportunity__c,Service_District__c,Service_Region__c,Sold_To__c,SVMXC__Activation_Notes__c,SVMXC__Active__c,SVMXC__All_Contacts_Covered__c,SVMXC__All_Products_Covered__c,SVMXC__All_Services_Covered__c,SVMXC__All_Sites_Covered__c,SVMXC__Billing_Schedule__c,SVMXC__Business_Hours__c,SVMXC__Cancelation_Notes__c,SVMXC__Canceled_By__c,SVMXC__Canceled_On__c,SVMXC__Company__c,SVMXC__Contact__c,SVMXC__Contact__r.name,SVMXC__Contact__r.Phone,SVMXC__Contract_Price2__c,SVMXC__Contract_Price__c,SVMXC__Default_Group_Member__c,SVMXC__Default_Parts_Price_Book__c,SVMXC__Default_Service_Group__c,SVMXC__Default_Travel_Price__c,SVMXC__Default_Travel_Unit__c,SVMXC__Discounted_Price2__c,SVMXC__Discounted_Price__c,SVMXC__Discount__c,SVMXC__EndpointURL__c,SVMXC__End_Date__c,SVMXC__Exchange_Type__c,SVMXC__Labor_Rounding_Type__c,SVMXC__Minimum_Labor__c,SVMXC__Minimum_Travel__c,SVMXC__Primary_Technician__c,SVMXC__Renewal_Date__c,SVMXC__Renewal_Notes__c,SVMXC__Renewal_Number__c,SVMXC__Renewed_From__c,SVMXC__Round_Labor_To_Nearest__c,SVMXC__Round_Travel_To_Nearest__c,SVMXC__Sales_Rep__c,SVMXC__Select__c,SVMXC__Service_Contract_Notes__c,SVMXC__Service_Level__c,SVMXC__Service_Plan__c,SVMXC__Service_Pricebook__c,SVMXC__SESSION_ID__c,SVMXC__Start_Date__c,SVMXC__Travel_Rounding_Type__c,SVMXC__Weeks_To_Renewal__c,SVMXC__Zone__c,SystemModstamp,Tech_OpportunityOwner__c,X0_contract__c FROM SVMXC__Service_Contract__c where id =:ContractId]; 
       relatedcoveredprod= new list<SVMXC__Service_Contract_Products__c>([select id,name,SVMXC__Installed_Product__r.name,SVMXC__Product__r.name,SVMXC__Product_Family__c,SVMXC__Product_Line__c,SVMXC__Start_Date__c,SVMXC__End_Date__c,SVMXC__Line_Price__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c=:ContractId  AND Status__c =: true ]); 
    /*for(SVMXC__Service_Contract_Products__c ca:relatedcoveredprod )
    {
    total =  ca.SVMXC__Line_Price__c+TOTAL;
    relatedcoveredprod.add(ca);
    }*/
 relatedincservices= new list<SVMXC__Service_Contract_Services__c>([select id,  Name,SVMXC__Service__c,SVMXC__Included_Units__c,SVMXC__Consumed_Units__c,SVMXC__Parts_Discount_Covered__c,  SVMXC__Labor_Discount_Covered__c,SVMXC__Expense_Discount_Covered__c,SVMXC__Travel_Discount_Covered__c,SVMXC__Is_Billable__c,SVMXC__Line_Price__c from SVMXC__Service_Contract_Services__c where SVMXC__Service_Contract__c=: ContractId]);
 relatedcovlocation= new list<SVMXC__Service_Contract_Sites__c>  ([select id,Name,SVMXC__Site__r.name,SVMXC__Is_Billable__c,SVMXC__Line_Price__c from SVMXC__Service_Contract_Sites__c where SVMXC__Service_Contract__c=:ContractId]); 
    }
    
    
    public pagereference generatePDF()
    {
        PageReference pdf =Page.SR_Print_ServiceContract;
        pdf.getParameters().put('Id',ContractId);
        pdf.setRedirect(true);
        Blob b;
       if(Test.IsRunningTest()) // Added by Harshita to improve the test coverage on 21/02/2014
           {
             b = Blob.valueOf('UNIT.TEST');
           }else
            {
            // Take the PDF content
             b = pdf.getContent();
            
            }
        //Create Document and Insert it
        Document doc = new Document();
        
        doc.Name='Contract.pdf';
        doc.Body=b;
        doc.IsPublic=True;
        doc.FolderId='00lE0000000WoLg';
        insert doc;
        
        PageReference pagereferencealias= new PageReference('/_ui/core/email/author/EmailAuthor?p3_lkid='+objContract.Id+'&p2_lkid='+objContract.SVMXC__Contact__c+'&doc_id='+doc.Id+'&retURL=%2F'+objContract.Id);
        pagereferencealias.setRedirect(true);
        return pagereferencealias;
  
        return null;
    }

}
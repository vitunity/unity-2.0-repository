@isTest
public class SR_PrintServiceContractController_Test {

    static  account accObj =new account();
    public static Id account_RecID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId(); //record type id of Installation WO
    static 
    {
     accObj.recordtypeid =account_RecID;
     accObj.Name='Test Account';
     accObj.CurrencyIsoCode='USD';
     accObj.Country__c = 'India';
     accObj.OMNI_Postal_Code__c ='21234';
     accObj.Agreement_Type__c ='Terms Addendum';
     accObj.AccountNumber = '12345678';
     insert accObj;
    }

    public static testMethod void testSR_PrintServiceContractController() {
    
        test.startTest();
        //Dummy data creation 
       
        Profile pr = [Select id from Profile where name = 'VMS Service - User'];
        User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u;
        system.runAs(u){
        
        
         //insert installed product
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'test1IP';
        ip1.SVMXC__Serial_Lot_Number__c = 'sr123';
        insert ip1;
        
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c=accObj.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
         
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+5, SVMXC__Installed_Product__c = ip1.id);
        insert testServiceContractProduct;
        
        
         apexpages.currentpage().getparameters().put('Id',testServiceContract.id); 
         ApexPages.StandardController testcontroller = new ApexPages.StandardController(testServiceContract);
         SR_PrintServiceContractController controller = new SR_PrintServiceContractController(testcontroller);  
          controller.generatePDF();
          //controller.cancel();   
                
        }
        test.StopTest();      
      }
      
      /********** method covering all the functionality of class Service Contract ***********/
       
        
    public static testMethod void InsertServiceContract()
    {    
         
            test.startTest();
            
           Profile pr = [Select id from Profile where name = 'VMS Service - User'];
        User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u;
        system.runAs(u){
        
            // insert Service Group //
                SVMXC__Service_Group__c objSvcGrp = new SVMXC__Service_Group__c(Name = 'U4G', SVMXC__Group_Code__c = 'U4G', ERP_Reference__c = 'U4G', SVMXC__Description__c = 'U4G');
                insert objSvcGrp;
                
            User userList = [SELECT id, name FROM User WHERE isActive = True and userType !='CsnOnly' and Id != :UserInfo.getUserId() LIMIT 1];        
            
            // create territory Team //
            Territory_Team__c tt = new Territory_Team__c();
            tt.Super_Territory__c = 'Americas';
            tt.Territory__c = 'North America';
            TT.OIS_SW__c = userList.id;
            TT.TPS_Sales__c = userList.id;
            tt.VSS_District_Manager__c = userList.id;
            tt.Zone_VP__c = userList.id;
            tt.HW_Install_District__c = 'hw district';
            tt.SW_Install_District__c = 'sw district';
            tt.Region__c = 'Eastern Zone';
            tt.Sub_Region__c = 'Northern Region';
            tt.District__c = 'Bill Tom';
            tt.City__c = 'Newark';
            tt.Country__c = 'USA';
            tt.ISO_Country__c = 'US';
            tt.State__c = 'DE';
            tt.Zip__c = '17042';
            tt.CSS_District__c = 'U4G';
            tt.District_Manager__c = userList.Id;
            insert tt;
          
            
            // Insert Partner Association //
            ERP_Partner_Association__c  erpPart = new ERP_Partner_Association__c (Customer_Account__c = accObj.id, Partner_Function__c= 'SP=Sold To ',ERP_Partner_Number__c ='abh123');
            insert erpPart;
            
                    
            // insert SLA term //
            SVMXC__Service_Level__c slaTerm = new SVMXC__Service_Level__c(Name = 'test sla Term', SVMXC__Active__c = true);
            insert slaTerm;
            
            // insert Service Plan //
            SVMXC__Service_Plan__c serPlan = new SVMXC__Service_Plan__c(NAme = 'test Plan', SVMXC__SLA_Terms__c = slaTerm.id, Contract_Type__c = 'test data 1223');
            insert serPlan;
            
            
            // insert Prior Service/Maintenance Contract //
            SVMXC__Service_Contract__c testServiceContract1 = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today()-5,SVMXC__End_Date__c = system.today()+1, SVMXC__Company__c=accObj.id, ERP_Sold_To__c = 'ab123', ERP_Order_Reason__c = 'HS1');
            insert testServiceContract1;
            
            testServiceContract1.SVMXC__End_Date__c = System.Today()-1;
            testServiceContract1.SVMXC__Active__c = True;
            Update testServiceContract1;
            
            // Insert Main Service/Maintenance Contract //
            SVMXC__Service_Contract__c testServiceContract2 = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+29, SVMXC__Service_Level__c= slaTerm.id, SVMXC__Company__c=accObj.id, ERP_Sold_To__c = erpPart.ERP_Partner_Number__c, SVMXC__Renewed_From__c = testServiceContract1.id, ERP_Contract_Type__c = serPlan.Contract_Type__c, ERP_Order_Reason__c = 'HS1',Flex_Credit__c = 'PEC', Quote_Reference__c = 'asdfghjk', SVMXC__Default_Service_Group__c =objSvcGrp.id);
            insert testServiceContract2;
            
            UpdateSrvContactFlag ben = new UpdateSrvContactFlag ();
              database.executeBatch(ben); 
        
        }
        test.stopTest();
    }
    public static testMethod void UpdateServiceContract()
    {   
        
        
            
           Profile pr = [Select id from Profile where name = 'VMS Service - User'];
        User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
        insert u;
        system.runAs(u){
           
            
            
            //insert PM plan Templates
            SVMXC__PM_Plan_Template__c pmTemp = new SVMXC__PM_Plan_Template__c(Name = 'service');
            insert pmTemp;
            
        
            
             //insert installed product
            
            SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(Name='H12345', ERP_Top_Level__c = '1234', SVMXC__Status__c ='Installed', SVMXC__Serial_Lot_Number__c ='H12345', SVMXC__Company__c = accObj.id, ERP_Reference__c = 'H12345');
            insert ip1;
            
            
            // insert SLA term
            SVMXC__Service_Level__c slaTerm = new SVMXC__Service_Level__c(Name = 'test sla Term', SVMXC__Active__c = true);
            insert slaTerm;
            
            // Insert Contracts //
            List<SVMXC__Service_Contract__c> listToInsertContract = new list<SVMXC__Service_Contract__c>();
            List<SVMXC__Service_Contract__c> listToUpdateContract = new list<SVMXC__Service_Contract__c>();
            
            //Record 1 : Contract //
            SVMXC__Service_Contract__c testServiceContract1 = new SVMXC__Service_Contract__c(name = 'test service contract1',SVMXC__Start_Date__c = system.today()+1,SVMXC__End_Date__c = system.today()+29,SVMXC__Company__c=accObj.id,   ERP_Order_Reason__c = 'HS1', Quote_Reference__c = 'asdfghjk');
            listToInsertContract.add(testServiceContract1);
            
            //Record 2 : Contract //
            SVMXC__Service_Contract__c testServiceContract2 = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today()+1,SVMXC__End_Date__c = system.today()+29, SVMXC__Service_Level__c= slaTerm.id, SVMXC__Company__c=accObj.id,   ERP_Order_Reason__c = 'HS1', Quote_Reference__c = 'asdfghjk');
            listToInsertContract.add(testServiceContract2);
            insert listToInsertContract ;
            
            // insert SLA Details //
            SVMXC__SLA_Detail__c slaDetail = new SVMXC__SLA_Detail__c(SVMXC__SLA_Terms__c = slaTerm.id, Service_Maintenance_Contract__c = testServiceContract2.id, Status__c = 'Valid' );
            insert slaDetail;
            
            //insert Counter Detail //
            SVMXC__Counter_Details__c  cdt = new SVMXC__Counter_Details__c (SVMXC__Active__c = true, SVMXC__Service_Maintenance_Contract__c =testServiceContract2.id, SVMXC__Counters_Covered__c = 12.00 , ERP_Serial_Number__c = 'H12345', SVMXC__Counter_Name__c = 'adfgh');
            insert cdt;
            cdt.ERP_Serial_Number__c = 'H1234';
            update cdt;
            
            //Insert Covered Products //
            list<SVMXC__Service_Contract_Products__c> listInsertCovProd = new list<SVMXC__Service_Contract_Products__c>();
            
            //Record 1 : Covered Product //
            SVMXC__Service_Contract_Products__c testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract2.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345');
            listInsertCovProd.add(testServiceContractProduct);
            
            //record 2 : covered Product //
            SVMXC__Service_Contract_Products__c testServiceContractProduct1 = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract2.id, SVMXC__Start_Date__c =system.today(), SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345');
            listInsertCovProd.add(testServiceContractProduct1);
            
            insert listInsertCovProd;
            System.debug('covered++++++++++++'+testServiceContractProduct);
            
            test.startTest();
            
            System.debug('covered++++++++++++@@@@@@'+testServiceContractProduct.Status__c);
            
            testServiceContract2.SVMXC__Start_Date__c = system.today();
            testServiceContract2.SVMXC__End_Date__c = system.today()+30;
            
            testServiceContract2.Flex_Credit__c = 'PEC';
            
            listToUpdateContract.add(testServiceContract2);
            testServiceContract1.SVMXC__Start_Date__c = system.today();
            testServiceContract1.SVMXC__End_Date__c = system.today()+30;
            listToUpdateContract.add(testServiceContract1);
            update listToUpdateContract;
            
            
            
            SVMXC__PM_Plan__c pmPlan = new SVMXC__PM_Plan__c(SVMXC__Status__c = 'Active', Name= 'test', SVMXC__Service_Contract__c = testServiceContract2.id , SVMXC__SLA_Terms__c = slaTerm.id, SVMXC__PM_Plan_Template__c= pmTemp.id);
            insert pmPlan;
            
             testServiceContract2.ERP_Order_Reason__c = 'Cancellation';
            update testServiceContract2; 
             test.stopTest();  
            
        }
        
        
     
    }
      
}
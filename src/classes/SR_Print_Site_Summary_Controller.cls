/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 26-Aug-2013
      @ Description           : This Class is Used to Print the Site and the Installed Product details on the Account.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/
public class SR_Print_Site_Summary_Controller 
{
public String Site_Name{get;set;}
public String Site_Street{get;set;}
public String Site_City{get;set;}
public String Site_State{get;set;}
public String Site_Zip{get;set;}
public String Site_Country{get;set;}
public String AccountId{get;set;}
public Integer sizeofListofInstalledProduct{get;set;}
public List<SVMXC__Installed_Product__c> ListofInstalledProduct{get;set;}
    public SR_Print_Site_Summary_Controller(ApexPages.StandardController controller) 
    {
       Site_Name=ApexPages.CurrentPage().getParameters().get('AccountName'); 
       Site_Street=ApexPages.CurrentPage().getParameters().get('Street'); 
       Site_City=ApexPages.CurrentPage().getParameters().get('City'); 
       Site_State=ApexPages.CurrentPage().getParameters().get('State'); 
       Site_Zip=ApexPages.CurrentPage().getParameters().get('Zip'); 
       Site_Country=ApexPages.CurrentPage().getParameters().get('Country'); 
       AccountId=ApexPages.CurrentPage().getParameters().get('AccountId'); 
       ListofInstalledProduct = new list<SVMXC__Installed_Product__c>();
       
       ListofInstalledProduct =[Select SVMXC__Product__r.Name,SVMXC__Serial_Lot_Number__c from SVMXC__Installed_Product__c where SVMXC__Company__c=:AccountId and SVMXC__Company__c!=Null];
       sizeofListofInstalledProduct = ListofInstalledProduct.size();
    }

}
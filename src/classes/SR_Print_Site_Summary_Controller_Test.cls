@isTest
public class SR_Print_Site_Summary_Controller_Test
{
    @isTest
    public static void SR_Print_Site_Summary_Controller_test()
    {
        Id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 
        Account testAcc = accounttestdata.createaccount();
        testAcc.Name= 'TestAccount';
        testAcc.recordtypeId = rec;
        insert testAcc;
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id);
        insert objIP;
        
        System.debug('SR_Print_Site_Summary_Controller_test SJSU ----- '+ testAcc.Id + '    objIPID : ' + objIP.id);

        ApexPages.StandardController controller = new ApexPages.StandardController(testAcc);

        Test.startTest();

            PageReference pageRef = Page.SR_Print_Site_Summary;
            pageRef.getParameters().put('AccountId', testAcc.Id);
            Test.setCurrentPage(pageRef);
            SR_Print_Site_Summary_Controller sController = new SR_Print_Site_Summary_Controller(controller);


        Test.stopTest();

        //System.assertEquals(1, sController.sizeofListofInstalledProduct);
    }
}
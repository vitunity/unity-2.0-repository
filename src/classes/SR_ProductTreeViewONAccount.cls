/***********************************************************************
* Author        : Wipro Technologies
* Functionality : Tree- Like Data Selection
* Description   :  Tree- Like Data Selection for Products on click of
*                  "Search Prouduct" Button available on Case Record.
* This Apex Class is being referenced in VF Page - SR_ProductTreeViewONAccount.
* 
************************************************************************/
public with sharing class SR_ProductTreeViewONAccount
{
    
    /* Declaration of Variables */ 
    boolean flag{get;set;}
    set<string> stTempLocationSet=new set<string>();
    public string strNodeId{get;set;}
    public string strAccountId{get;set;}
    Public string strAccountLocationId{get;set;}
    set<string> stChildData=new set<string>();
    private static JSONGenerator gen {get; set;}
    public List<String> LstOfProductNodes{get;set;} 
    set<string> stLocationId=new set<string>();
    public string picklistvalue{get;set;}
    // Getter Setter Method for searching input text in Product Tree on VF page.   
   
    list<SVMXC__Installed_Product__c> lstInstalledProduct;
    // Getter Setter Method for passing Selected Product from VF page to Apex Class. 
    public String ProductId{get;set;}  
    set<string> stIP=new set<string>();
    Map<id,List<SVMXC__Installed_Product__c>>MapProductAndLstChildren=new Map<Id,List<SVMXC__Installed_Product__c>>();
    Map<Id,SVMXC__Installed_Product__c> MapOfProducts;
    List<SVMXC__Installed_Product__c> LstOfProducts;
    Map<string,string> MapIdOfLocation=new Map<string,string>();
    String ipprodtype ;
    PageReference pageRef;
    /* Controller Method Starts here.*/
    public SR_ProductTreeViewONAccount(ApexPages.StandardController controller)
    {
        flag = true;
        pageRef = new PageReference('/apex/SR_ProductTreeViewONAccount?id='+strAccountId+'&prodtype=None');
        LstOfProductNodes=new List<String>();
        strAccountId=apexpages.currentpage().getparameters().get('id');  // geting Id of Parameter
        ipprodtype = apexpages.currentpage().getParameters().get('prodtype');
        System.debug('1ipprodtype-----'+ipprodtype);
        if(ipprodtype == Null)
            picklistvalue = 'None';
        else
            picklistvalue = ipprodtype;
        
        System.debug('1picklistvalue-----'+picklistvalue);
        list<SVMXC__Site__c> lstSite = new list<SVMXC__Site__c>();
        if(strAccountId != null)
        {
            lstSite=[select id,SVMXC__Account__c from  SVMXC__Site__c where SVMXC__Account__c =:strAccountId ];
        }
        System.debug('lstSite-----'+lstSite);
        for(SVMXC__Site__c varlc: lstSite)
        {
            stLocationId.add(varlc.id);
        }   
        System.debug('stLocationId-----'+stLocationId);
        if(stLocationId != null)
        {
            lstInstalledProduct=new list<SVMXC__Installed_Product__c>();
            System.debug('picklist value >>>>' + picklistvalue );
            if(ipprodtype != 'None' && ipprodtype != null)
            {
                String soql = 'select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c,   SVMXC__Status__c, Product_Version_Build__r.Name, SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where SVMXC__Site__c IN: stLocationId';
                //,Product_Version_Build__r.Name //Wave 1C - Nikita 1/25/2015
                soql = soql + ' and  SVMXC__Product__r.Product_Type__c  =\'' + ipprodtype +'\'';
                System.debug('soql*******'+soql);
                lstInstalledProduct=database.query(soql);
                System.debug(' In if block >>>>' );
            }
            else
            {
                System.debug(' In else block >>>>' );
                lstInstalledProduct=[select id,SVMXC__Site__c,name,SVMXC__Serial_Lot_Number__c, Product_Version_Build__r.Name, SVMXC__Status__c,   SVMXC__Product__r.name,SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where SVMXC__Site__c IN: stLocationId]; //,Product_Version_Build__r.Name //Wave 1C -Nikita 1/25/2015
            }
            System.debug(' lstInstalledProduct >>>>'+lstInstalledProduct );
            for(SVMXC__Installed_Product__c varIp:lstInstalledProduct)
            {
           
             stIP.add(varIp.id);
            }
            if(ipprodtype != 'None' && ipprodtype != null)
            {
                MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,SVMXC__Site__r.name,name,SVMXC__Serial_Lot_Number__c,Product_Version_Build__r.Name,SVMXC__Product__r.name,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Site__c IN:stLocationId AND SVMXC__Product__r.Product_Type__c  =:ipprodtype]);
                //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/25/2015
            }
            else
            {
                MapOfProducts=new map<Id,SVMXC__Installed_Product__c>([select id,SVMXC__Site__c,SVMXC__Site__r.name,name,SVMXC__Serial_Lot_Number__c,Product_Version_Build__r.Name,SVMXC__Product__r.name,SVMXC__Parent__r.SVMXC__Parent__r.name,SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.Name,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Status__c, SVMXC__Country__c, SVMXC__City__c,SVMXC__Parent__r.Name,SVMXC__Service_Contract__r.Name,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Service_Contract__r.SVMXC__Start_Date__c from SVMXC__Installed_Product__c where (SVMXC__Parent__c In: stIP  OR SVMXC__Parent__r.SVMXC__Parent__c In:stIP OR SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c IN:stIP OR id IN :stIP) AND SVMXC__Site__c IN:stLocationId]);
                //,Product_Version_Build__r.Name // Wave 1C - Nikita 1/25/2015
            }
            // Iterating for loop to add list of Child Products in a Map corresponding to Parent Product Id's.   

        for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) 
        {
           if(MapProductAndLstChildren.get(pd.SVMXC__Parent__c)==null)
            {
                LstOfProducts=new List<SVMXC__Installed_Product__c>();
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }
            else
            {               
                LstOfProducts=MapProductAndLstChildren.get(pd.SVMXC__Parent__c);
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Parent__c,LstOfProducts);
            }
        }
       
        /* for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) 
        {
           stTempLocationSet.add(pd.SVMXC__Site__c);
        
           if(MapProductAndLstChildren.get(pd.SVMXC__Site__c)==null)
            {
                LstOfProducts=new List<SVMXC__Installed_Product__c>();
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Site__c,LstOfProducts);
            }else
            {   
                MapIdOfLocation.put(pd.SVMXC__Site__c,'Location');         
                LstOfProducts=MapProductAndLstChildren.get(pd.SVMXC__Site__c);
                LstOfProducts.add(pd);
                MapProductAndLstChildren.put(pd.SVMXC__Site__c,LstOfProducts);
            }
        }*/
        for(string varLCId: stLocationId)
        {  
            LstOfProducts=new List<SVMXC__Installed_Product__c>();
            for(SVMXC__Installed_Product__c pd:MapOfProducts.values()) 
            {
                stTempLocationSet.add(pd.SVMXC__Site__c);
                if(pd.SVMXC__Site__c==varLCId)
                {
                    MapIdOfLocation.put(pd.SVMXC__Site__c,'Location');
                    LstOfProducts.add(pd);
                }
            }
            MapProductAndLstChildren.put(varLCId,LstOfProducts);
        }
        
        Map<string,list<SVMXC__Installed_Product__c>> mapChld=new Map<string,list<SVMXC__Installed_Product__c>>();
        for(string varStr:stIP)
        {
            LstOfProducts=new List<SVMXC__Installed_Product__c>();
            for(SVMXC__Installed_Product__c pd:MapOfProducts.values())
            {
                if(varStr == pd.SVMXC__Parent__c)
                {
                 LstOfProducts.add(pd);
                }
            }
            mapChld.put(varStr,LstOfProducts);     // Map holding Product-ProductChild data
        }
        for(string varst:stIP)                 // Used to get all child record id which does not occur as root node
        {
            for(SVMXC__Installed_Product__c varStr:mapChld.get(varst))
            {
              stChildData.add(varStr.id);                          
            }
        }
        System.debug('------MapProductAndLstChildren------------------>'+MapProductAndLstChildren);
        }
    } 
    
    /* Action method (called from VF:SR_ProductTreeViewONAccount) starts here.
       Below method parses all Parent Products and adds them to JSON String 
       which is rendered on VF as Tree Structure. */
       
    public void GenerateTreeStructureForProducts()
    {
        System.debug('-----stLocation------------->'+stTempLocationSet);
        gen = JSON.createGenerator(true);
        HeadertoJSON();
        LstOfProductNodes.add(gen.getAsString()); 
        flag = false;
        // Generating Tree Strucutre for all Parent Products.
        
        for(SVMXC__Site__c pd: [select id,name ,SVMXC__Account__c from  SVMXC__Site__c where SVMXC__Account__c =:strAccountId  and Id IN:stTempLocationSet])
        {
            // Initializing JSON Generator Object.
            gen = JSON.createGenerator(true);
            
            /* Calls CreateTreeStructure method to retrieve Instance of Wrapper Class 
                   which contains Parent and Child Nodes for each Product. */
                     // Calling ConvertNodeToJSON Method. 
            /* if( !stChildData.contains(pd.id))
            {
                WrpProductNode node = CreateTreeStructure(pd);
            
                ConvertNodeToJSON(node);
                LstOfProductNodes.add(gen.getAsString());
         
                // Creating List of All Product Nodes by adding instance of JSONGenerator as String.
            }*/// by mohit
            WrpProductNode node = CreateTreeStructure(pd,null);
            system.debug('--Node->'+Node);

            ConvertNodeToJSON(node);    
            system.debug('>>>>>>>>>>>>>>'+ gen.getAsString());
            LstOfProductNodes.add(gen.getAsString());    
        }
    } /* Action method ends here.*/ 
    
    /*******************************************************************/
    public void HeadertoJSON()
    {
        gen.writeStartObject();
        gen.writeStringField('title', 'Installed Product Name'+'@'+'Product Name'+'@'+' Product Version Build'+'@'+'Serial Number'+'@'+'Service Contract '+'@'+' Contract Start Date'+'@'+'Contract End Date'+'@'+'City'+'@'+'Status'+'@'+'');
        gen.writeBooleanField('unselectable', false);
        gen.writeStringField('key', '');
        gen.writeBooleanField('expand', false);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        gen.writeEndObject();  
    }
    
    /*******************************************************************/
    /* CreateTreeStructure Method starts here : Below method creates Parent and Child Nodes for each Product 
       and returns instance of WrpProductNode (Wrapper class) */
    public  WrpProductNode CreateTreeStructure(SVMXC__Site__c prd,SVMXC__Installed_Product__c prd1)
    {            
        WrpProductNode wrpObj=new WrpProductNode();
        wrpObj.prod=prd;   // Assign the site to Loation property Inner Class
        wrpObj.prod1=prd1; // Assign Ip to Inner Class property
        List<WrpProductNode>lstOfChild;
        if(wrpObj.prod !=null && wrpObj.prod1==null) // 
        {
            if(MapProductAndLstChildren.get(prd.id)!=null)
            {
                wrpObj.hasChildren =true;
                lstOfChild=new List<WrpProductNode >();
                for(SVMXC__Installed_Product__c pd:MapProductAndLstChildren.get(prd.id)){
                  WrpProductNode temp=CreateTreeStructure(null,pd);
                    lstOfChild.add(temp);
                }           
            }else
            {
                wrpObj.hasChildren =false;   
            }   
                
            wrpObj.prodChildNode=lstOfChild;
        }
        else
        {
            if(MapProductAndLstChildren.get(prd1.id)!=null)
            {
                wrpObj.hasChildren =true;
                lstOfChild=new List<WrpProductNode >();
                for(SVMXC__Installed_Product__c pd:MapProductAndLstChildren.get(prd1.id))
                {
                  WrpProductNode temp=CreateTreeStructure(null,pd);
                    lstOfChild.add(temp);
                }           
            }
            else
            {
                wrpObj.hasChildren =false;   
            }   
                
            wrpObj.prodChildNode=lstOfChild;
        }
        system.debug('------aa-->'+wrpObj.prodChildNode);
        return wrpObj;        
    } /*CreateTreeStrucure Method Ends here.*/
    
    /*ConvertNodeToJSON method starts here : Below method converts 
    instance of each Product Node(Wrapper class) into JSON.*/
    public void ConvertNodeToJSON(WrpProductNode prodNode)
    { 
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce. 
        System.debug('--Id'+prodNode.prod.id);  
        System.debug('--MapIdOfLocation.keyset--'+MapIdOfLocation.keyset());   
  
        if( MapIdOfLocation.keyset().contains(prodNode.prod.id) )
        {   
            gen.writeStartObject();
            gen.writeStringField('title', prodNode.Prod.Name+'@'+' '+'@'+' '+'@'+' '+'@'+' '+'@'+' '+'@'+prodNode.Prod.Id);
            gen.writeStringField('key', prodNode.Prod.Id);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
            gen.writeBooleanField('isFolder', false);
            gen.writeBooleanField('icon', false);
        }
        else if(prodNode.Prod1 !=null)
        {
            /* if(count == 0)
            {
                gen.writeStringField('title', 'Installed Product Name'+'@'+'Product Name'+'@'+' Product Version Build'+'@'+'Serial Number'+'@'+'Service Contract '+'@'+' Contract Start Date'+'@'+'Contract End Date'+'@'+'City'+'@'+'Status'+'@'+'Header');
                count = 1;
            }*/
            
            gen.writeStartObject();
            gen.writeStringField('title', prodNode.Prod1.Name+'@'+(prodNode.Prod1.SVMXC__Product__r.name==null?' ': prodNode.Prod1.SVMXC__Product__r.name)+'@'
            /* Wave 1C - Nikita 1/25/2015
            + (prodNode.Prod1.Product_Version_Build__r.Name==null?' ':prodNode.Prod1.Product_Version_Build__r.Name)+
            Wave 1C - Nikita 1/25/2015 '@'*/
            + (prodNode.Prod1.Product_Version_Build__r.Name==null?' ':prodNode.Prod1.Product_Version_Build__r.Name)+'@' // DFCT0011784 : 22 Aug 2016
            +(prodNode.Prod1.SVMXC__Serial_Lot_Number__c==null?'':prodNode.Prod1.SVMXC__Serial_Lot_Number__c)+'@'+(prodNode.Prod1.SVMXC__Service_Contract__r.Name==null?'':prodNode.Prod1.SVMXC__Service_Contract__r.Name)+'@'+(string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__Start_Date__c)==null?'':string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__Start_Date__c))+'@'+(string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__End_Date__c)==null?'':string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__End_Date__c))+'@'+(prodNode.Prod1.SVMXC__City__c==null?'':prodNode.Prod1.SVMXC__City__c)+'@'+(prodNode.Prod1.SVMXC__Status__c==null?' ':prodNode.Prod1.SVMXC__Status__c)+'@'+prodNode.Prod1.id);
            gen.writeStringField('key', prodNode.Prod1.Id);
            gen.writeBooleanField('unselectable', false);
            gen.writeBooleanField('expand', true);
            gen.writeBooleanField('isFolder', false);
            gen.writeBooleanField('icon', false);
        }
        // Below condition checks for any child Products available for each Parent product.
        if(prodNode.hasChildren )
        {                
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
            {
                if(!stChildData.contains(temp.Prod1.id))  // cheking for root or leaf node data
                {
                    ConvertNodeToJSONChild(temp);    
                } 
            }               
        }
        gen.writeEndArray();               
        gen.writeEndObject();           
    }/*ConvertNodeToJSON method ends here.*/
    
    public void ConvertNodeToJSONChild(WrpProductNode prodNode)
    { 
        // Creating Product Nodes in JSON format which are also attributes of DynaTree JQuery used in VisualForce.      
        gen.writeStartObject();
        gen.writeStringField('title', prodNode.Prod1.Name+'@'+(prodNode.Prod1.SVMXC__Product__r.name==null?' ': prodNode.Prod1.SVMXC__Product__r.name)
        /* Wave 1C - Nikita 1/25/2015
        +'@'+(prodNode.Prod1.Product_Version_Build__r.Name==null?' ':prodNode.Prod1.Product_Version_Build__r.Name)
        Wave 1C - Nikita 1/25/2015*/
        +'@'+(prodNode.Prod1.Product_Version_Build__r.Name==null?' ':prodNode.Prod1.Product_Version_Build__r.Name)+ // DFCT0011784 : 22 Aug 2016
        +'@'+(prodNode.Prod1.SVMXC__Serial_Lot_Number__c==null?'':prodNode.Prod1.SVMXC__Serial_Lot_Number__c)+'@'+(prodNode.Prod1.SVMXC__Service_Contract__r.Name==null?'':prodNode.Prod1.SVMXC__Service_Contract__r.Name)+'@'+(string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__Start_Date__c)==null?'':string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__Start_Date__c))+'@'+(string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__End_Date__c)==null?'':string.valueof(prodNode.Prod1.SVMXC__Service_Contract__r.SVMXC__End_Date__c))+'@'+(prodNode.Prod1.SVMXC__City__c==null?'':prodNode.Prod1.SVMXC__City__c)+'@'+(prodNode.Prod1.SVMXC__Status__c==null?' ':prodNode.Prod1.SVMXC__Status__c)+'@'+prodNode.Prod1.id);
        gen.writeStringField('key', prodNode.Prod1.Id);
        gen.writeBooleanField('unselectable', false);
        gen.writeBooleanField('expand', true);
        gen.writeBooleanField('isFolder', false);
        gen.writeBooleanField('icon', false);
        
        // Below condition checks for any child Products available for each Parent product.
        if(prodNode.hasChildren )
        {    
            // Below line disables Radio buttons for all Parent Products.
            gen.writeBooleanField('hideCheckbox',true);
            gen.writeFieldName('children');
            gen.writeStartArray();   
                    
            // Iterating each child product and adding further child products to Tree nodes by calling ConvertNodeToJSON method recursively.
            for(WrpProductNode temp:prodNode.prodChildNode)
               ConvertNodeToJSONChild(temp);                                    
            gen.writeEndArray();           
        }
        gen.writeEndObject();    
    }
    /*Wrapper class starts here : Below class wraps Products,List of Child Products 
    and boolean variable for childs present for each Parent Product.*/
    public class WrpProductNode
    {        
        public List<WrpProductNode>prodChildNode{get;set;}
        public Boolean hasChildren {get; set;} 
        public SVMXC__Site__c prod{get;set;}    
        public SVMXC__Installed_Product__c prod1{get;set;}        
    } /*Wrapper Class ends here*/   
    
    //***********************************
}
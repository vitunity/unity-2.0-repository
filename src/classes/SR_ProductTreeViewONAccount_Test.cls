@isTest 
Public class SR_ProductTreeViewONAccount_Test {
    static testMethod void TestTreeData() 
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt10', Email='standarduser10@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing10', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser10@testorg.com');

        //System.runAs(u) {
            Account a;
            SVMXC__Site__c objloc;
            List<SVMXC__Installed_Product__c> lstInstall=new list<SVMXC__Installed_Product__c>();
            
            
            // Inserting account
            a = new Account(name='test',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'); 
            insert a;  
            system.assertNotEquals(a.Id,null);
            
            //Inserting objLocation
            objloc=new SVMXC__Site__c();
            objloc.SVMXC__Account__c=a.id;
            objloc.Name='testLocation';
            objloc.SVMXC__Street__c='testStreet';
            objloc.SVMXC__Country__c='testCountry';
            objloc.SVMXC__Zip__c='201301';
            insert objloc;  
            
            
            system.assertEquals(objloc.SVMXC__Account__c,a.Id);
            system.assertNotEquals(objloc.Id,null);
            
            // Inserting Product
            product2 ObjPro=new product2();
            ObjPro.name='testProd';
            ObjPro.Product_Type__c='TESTPROD';          
            insert ObjPro;
            system.assertEquals(ObjPro.Product_Type__c,'TESTPROD');
            system.assertNotEquals(ObjPro.Id,null);
            
            //Inserting  installed prods
                    
            SVMXC__Installed_Product__c objIns1=new SVMXC__Installed_Product__c();
            objIns1.SVMXC__Site__c=objloc.id;
            objIns1.SVMXC__Product__c=ObjPro.id;
            objIns1.SVMXC__Status__c='testInstalled';
            objIns1.SVMXC__Serial_Lot_Number__c='0010';
            objIns1.ERP_Reference__c = 'H1244';
            insert objIns1;
            
            SVMXC__Installed_Product__c objIns2=new SVMXC__Installed_Product__c();
            objIns2.SVMXC__Site__c=objloc.id;
            objIns2.SVMXC__Product__c=ObjPro.id;
            objIns2.SVMXC__Status__c='testInstalled';
            objIns2.SVMXC__Serial_Lot_Number__c='0010';
            objIns2.SVMXC__Parent__c=objIns1.id;
            lstInstall.add(objIns2);
            
            SVMXC__Installed_Product__c objIns3=new SVMXC__Installed_Product__c();
            objIns3.SVMXC__Site__c=objloc.id;
            objIns3.SVMXC__Product__c=ObjPro.id;
            objIns3.SVMXC__Status__c='testInstalled';
            objIns3.SVMXC__Serial_Lot_Number__c='0010';
            lstInstall.add(objIns3);

            Insert lstInstall;          
            
            
            List<SVMXC__Site__c> lstSite=[select id,SVMXC__Account__c from  SVMXC__Site__c where SVMXC__Account__c =: a.Id ];
            system.assertEquals(lstSite.size(),1);
            
            List<SVMXC__Installed_Product__c> lstInProd=[select id from  SVMXC__Installed_Product__c where SVMXC__Site__c =: objloc.Id ];
            system.assertEquals(lstInProd.size(),3);
            
            ApexPages.StandardController sc = new ApexPages.standardController(a);
            Test.setCurrentPage(Page.SR_ProductTreeViewONAccount);
            ApexPages.currentPage().getParameters().put('id', a.id);
            ApexPages.currentPage().getParameters().put('prodtype', 'TESTPROD');
            
            
            SR_ProductTreeViewONAccount sic = new SR_ProductTreeViewONAccount(sc);
            sic.GenerateTreeStructureForProducts();
       // }
    }
}
public class SR_Project_Time_Sheet
{
    public List<SVMXC__Service_Order__c> Lstworkorder{get;set;}
    public list<string> str{get;set;}
    public decimal totaltravelintime{get;set;}
    public decimal totaltravelouttime{get;set;}
    public date da{get;set;}
    public List<AggregateResult> Maximum = new list<AggregateResult>();
    public List<AggregateResult> SecondMaximum = new list<AggregateResult>();
    public string workdetailfields= '';
    public List<SVMXC__Service_Order_Line__c> templateworkdetail{get;set;}
    public map<id,SVMXC__Service_Order_Line__c> mapofWorkDetailIdandInfo;
    public map<String, Decimal> mapofActualHrs {get;set;}
    public map<string,SVMXC__Service_Order_Line__c> mapofstringanddetail = new map<string,SVMXC__Service_Order_Line__c>();
    public list<SVMXC_Time_Entry__c>  TravelTimeList{get;set;}
    public list<SVMXC_Time_Entry__c>  MealTimeList{get;set;}
    public list<SVMXC__Service_Order_Line__c> LstTravelInWd{get;set;}
    public list<SVMXC__Service_Order_Line__c> LstTravelOutWd{get;set;}
    public list<SVMXC__Service_Order_Line__c> LstTravelIn{get;set;}
    public list<SVMXC__Service_Order_Line__c> LstTravelOut{get;set;}
    public list<SVMXC_Time_Entry__c> MealBreakEntry{get;set;}
    public list<SVMXC_Time_Entry__c> MealBreak{get;set;}
    public map<string,List<SVMXC__Service_Order_Line__c>> mapOfTemplateAndWorkdetail = new map<string,list<SVMXC__Service_Order_Line__c>>();
    public string test{get;set;}
    set<string> stWO = new set<string>(); // Aded By Mohit
    List<Technician_Assignment__c> lstTechAssignment; // Added By Mohit

    public map<string,List<SVMXC__Service_Order_Line__c>> getmapOfTemplateAndWorkdetail()
    {
        return mapOfTemplateAndWorkdetail;
    }
    public void setmapOfTemplateAndWorkdetail(map<string,List<SVMXC__Service_Order_Line__c>> xc)
    {
        this.mapOfTemplateAndWorkdetail = xc;
        system.debug('----settter called--->'+mapOfTemplateAndWorkdetail);
    }

    public SR_Project_Time_Sheet()
    {
        lstTechAssignment=New List<Technician_Assignment__c>();  // Added  By Mohit
        LstTravelInWd = new list<SVMXC__Service_Order_Line__c>();
        totaltravelintime = 0;
        totaltravelouttime = 0;
        LstTravelOutWd = new list<SVMXC__Service_Order_Line__c>();
        LstTravelIn = new list<SVMXC__Service_Order_Line__c>();
        LstTravelOut = new list<SVMXC__Service_Order_Line__c>();
        Lstworkorder= new list<SVMXC__Service_Order__c>();
        TravelTimeList = new list<SVMXC_Time_Entry__c>();
        MealTimeList = new list<SVMXC_Time_Entry__c>();
        MealBreakEntry = new list<SVMXC_Time_Entry__c>();
        MealBreak = new list<SVMXC_Time_Entry__c>();
        templateworkdetail = new list<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> Lstwd;
        list<SVMXC__Service_Order__c> LsttempWO = new list<SVMXC__Service_Order__c> ();

        Id RecordTypeId = schema.SobjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();

        List<SVMXC__Service_Group_Members__c> technician = [select id,name from SVMXC__Service_Group_Members__c where User__c=:userinfo.getuserid() OR SVMXC__Salesforce_User__c=:userinfo.getuserid()];

        if(technician.size()>0)
        {
            da = (date.valueof(system.now()).tostartofweek())-1;
            Schema.DescribeSObjectResult workdetailobject = SVMXC__Service_Order_Line__c.sObjectType.getDescribe();

            Map<String,Schema.SObjectField> workdetailobjectfieldmap = workdetailobject.fields.getMap();

            for(Schema.SObjectField sobj:workdetailobjectfieldmap.values())
            {           
                Schema.DescribeFieldResult fieldresult = sobj.getDescribe();
                if(fieldresult.isCreateable())
                {
                    if(workdetailfields=='')
                    {
                        workdetailfields = fieldresult.getname();
                    }
                    else
                    {
                        workdetailfields =workdetailfields +',' +fieldresult.getname();
                    }   
                }
            }

            lstTechAssignment=[select id,Work_Order__c,Technician_Name__c,Technician_Name__r.SVMXC__Salesforce_User__c,Technician_Name__r.User__c from Technician_Assignment__c where Technician_Name__r.User__c =: userinfo.getuserid() OR Technician_Name__r.SVMXC__Salesforce_User__c =: userinfo.getuserid()];

            for(Technician_Assignment__c varTecAss: lstTechAssignment)
            {
                if(varTecAss.Work_Order__c!=null)
                {
                    stWO.Add(varTecAss.Work_Order__c);
                }
            }
            System.debug('-----stWO-->'+stWO);

            for(integer y=0;y<21;y++){
                SVMXC_Time_Entry__c xv = new SVMXC_Time_Entry__c();    
                MealTimeList.add(xv);
            }
            str = new list<string>{'x'};

            if(stWO.size() > 0)
            {
                LsttempWO = [select Id, SVMXC__Group_Member__c, Name, SVMXC__Site__r.Name,SVMXC__Component__r.Name,(select Id,SVMXC__Consumed_From_Location__c,
                                Completed__c, SVMXC__Group_Member__c,  ERP_Reference__c, SVMXC__Service_Order__c, Budgeted_Hours__c, //Purchase_Order_Number1__c, DE7294
                                Name, Actual_Hours__c, SVMXC__Work_Description__c from SVMXC__Service_Order_Line__r where recordtype.Name = 'Template'
                                AND Completed__c = false AND SVMXC__Line_Type__c ='Labor' order by SVMXC__Service_Order__c)
                                from SVMXC__Service_Order__c where  ID in :stWO order by Name];
            }

            for(SVMXC__Service_Order__c Vartemp: LsttempWO)
            {
                if(Vartemp.SVMXC__Service_Order_Line__r.size()>0)
                {
                    Lstworkorder.add(Vartemp);
                }
            }

            mapofActualHrs = new map<String, Decimal>();

            for(SVMXC__Service_Order_Line__c WDActlHrs : [select Id, ERP_Reference__c, Actual_Hours__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in :Lstworkorder])           
            {
                if(mapofActualHrs.get(WDActlHrs.ERP_Reference__c) == null)
                {               
                    mapofActualHrs.put(WDActlHrs.ERP_Reference__c, WDActlHrs.Actual_Hours__c);
                }
                else
                {
                    decimal ActHr = mapofActualHrs.get(WDActlHrs.ERP_Reference__c) + WDActlHrs.Actual_Hours__c;
                    mapofActualHrs.put(WDActlHrs.ERP_Reference__c, ActHr);
                }   
                system.debug(' ------- work detail Achual Hrs -------- ' + mapofActualHrs.get(WDActlHrs.ERP_Reference__c));                 
            }

            // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
            List<SVMXC__Service_Order__c> Maximum  = [select Total_Budget_Hours__c, id from SVMXC__Service_Order__c where
                                (SVMXC__Group_Member__r.SVMXC__Salesforce_User__c =:UserInfo.getUserId() OR SVMXC__Group_Member__r.SVMXC__Salesforce_User__c
                                =:UserInfo.getUserId()) AND ID in (select SVMXC__Service_Order__c from SVMXC__Service_Order_Line__c where
                                recordtype.Name='Template' and Completed__c = false ) order by Total_Budget_Hours__c Desc ];

            system.debug('find maximum--->'+Maximum  );

            Id MaxId;

            if(Maximum.size()>0)
            {
                MaxId = string.valueof(Maximum[0].id);
                system.debug('current start week date-->'+date.newInstance(da.year(), da.month(),da.day()));
                da = date.newInstance(da.year(), da.month(),da.day());
                date nextdate = da+7;

                // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
                LstTravelInWd  = [select Id,SVMXC__Start_Date_and_Time__c,SVMXC__End_Date_and_Time__c,Completed__c,SVMXC__Group_Member__c,
                                        SVMXC__Service_Order__c,Actual_Hours__c,Budgeted_Hours__c,Name,SVMXC__Work_Description__c //Purchase_Order_Number1__c, DE7294
                                        from SVMXC__Service_Order_Line__c where recordtype.Name='Usage/Consumption' and
                                        (SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId() OR
                                        SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId()) and
                                        SVMXC__Line_Type__c ='Travel' AND Travel_Direction__c = 'In' and (SVMXC__Start_Date_and_Time__c>=:(da-1) AND
                                        SVMXC__Start_Date_and_Time__c <=:(nextdate)) order by SVMXC__Start_Date_and_Time__c, SVMXC__Service_Order__c ,Id];
                system.debug('list of travel in data query-->'+LstTravelInWd  );
                system.debug('size of travel in list-->'+LstTravelInWd.size());

                // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
                LstTravelOutWd = [select Id,SVMXC__Start_Date_and_Time__c,SVMXC__End_Date_and_Time__c,Completed__c,SVMXC__Group_Member__c,
                                        SVMXC__Service_Order__c,Actual_Hours__c,Budgeted_Hours__c,Name,SVMXC__Work_Description__c //Purchase_Order_Number1__c, DE7294
                                        from SVMXC__Service_Order_Line__c where recordtype.Name='Usage/Consumption' and
                                        (SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId() OR
                                        SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId()) and
                                        SVMXC__Line_Type__c='Travel' AND Travel_Direction__c = 'Out' and (SVMXC__Start_Date_and_Time__c>=:(da-1) AND
                                        SVMXC__Start_Date_and_Time__c <=:(nextdate)) order by SVMXC__Start_Date_and_Time__c, SVMXC__Service_Order__c ,Id];

                MealBreakEntry = [select Id, Meal_Break_One_Two_Three__c, Start_Time__c, TotalTime__c, Start_Date_Time__c, End_Date_Time__c from
                                    SVMXC_Time_Entry__c where (Activity__c = 'Meal Break') and Technician__r.SVMXC__Salesforce_User__c =:UserInfo.getUserId()
                                    AND (Start_Date_Time__c>=:(da-1) and Start_Date_Time__c <=:(nextdate)) order by Start_Date_Time__c ];

                for(integer y=0; y<21; y++)
                {
                    boolean find = false;
                    for(SVMXC_Time_Entry__c timeent:MealBreakEntry)
                    {
                        if(timeent.Start_Date_Time__c.day()==(da+y).day())
                        {
                            MealBreak.add(timeent);
                            find = true;
                            break;
                        }
                    }
                    if(find == false)
                    {
                        MealBreak.add(new SVMXC_Time_Entry__c(Technician__c=technician[0].id,Activity__c='Meal Break'));
                    }
                }

                // new meal break 
                integer dx;
                for(integer x=0;x<21;x++)
                {
                    string typeofmeal='';

                    if(x==0 || x==7 || x==14){
                        dx = ((date.valueof(system.now()).tostartofweek())-1).day();
                    }
                    if(x<=0 && x>=6){
                        typeofmeal = 'Break 1';
                    }
                    if(x<=7 && x>=13){
                        typeofmeal = 'Break 2';
                    }
                    if(x<=14 && x>=20){
                        typeofmeal = 'Break 3';
                    }
                    boolean find = false;
                    system.debug('Meal Break entry--->'+MealBreakEntry);
                    for(SVMXC_Time_Entry__c timeent:MealBreakEntry)
                    {
                        if(x!=0)
                        {
                            if(timeent.Start_Date_Time__c.day()== dx+(math.mod(x,7)) && timeent.Meal_Break_One_Two_Three__c==typeofmeal)
                            {
                                MealBreak.add(timeent);
                                find = true;
                                break;
                            }
                        }
                    }
                    if(find == false)
                    {
                        MealBreak.add(new SVMXC_Time_Entry__c(Technician__c=technician[0].id,Activity__c='Meal Break',Meal_Break_One_Two_Three__c=typeofmeal) );
                    }
                }
                for(integer i=0;i<7;i++)
                {
                    boolean find = false;
                    for(SVMXC__Service_Order_Line__c ser:LstTravelInWd)
                    {
                        system.debug('Travel In is---->'+ser);
                        if(ser.SVMXC__Start_Date_and_Time__c.day()==(da+i).day())
                        {
                            LstTravelIn.add(ser);
                            find = true;
                            totaltravelintime = totaltravelintime + ser.Actual_Hours__c;
                            break;
                        }
                    }
                    if(find == false)
                    {
                        LstTravelIn.add(new SVMXC__Service_Order_Line__c(Actual_Hours__c=0.00,RecordTypeid=RecordTypeId ,SVMXC__Line_Type__c='Travel',Travel_Direction__c = 'In', SVMXC__Service_Order__c= MaxId,SVMXC__Group_Member__c=technician[0].id)); //,Purchase_Order_Number1__c='2345' DE7294
                    }
                }
                for(integer i=0;i<7;i++)
                {
                    boolean find = false;
                    for(SVMXC__Service_Order_Line__c ser:LstTravelOutWd ){
                        if(ser.SVMXC__Start_Date_and_Time__c.day()==(da+i).day()){
                            LstTravelOut.add(ser);
                            find = true;
                            totaltravelouttime = totaltravelouttime+ser.Actual_Hours__c;
                            break;
                        }
                    }
                    if(find == false)
                    {
                        LstTravelout.add(new SVMXC__Service_Order_Line__c(Actual_Hours__c=0.00,RecordTypeid=RecordTypeId ,SVMXC__Line_Type__c='Travel',Travel_Direction__c = 'Out', SVMXC__Service_Order__c=MaxId,SVMXC__Group_Member__c=technician[0].id )); //,Purchase_Order_Number1__c='2345' DE7294
                    }
                }

                // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
                templateworkdetail = [select Id,SVMXC__Consumed_From_Location__c,Completed__c,SVMXC__Group_Member__c,SVMXC__Service_Order__c,Actual_Hours__c,Budgeted_Hours__c,Name,SVMXC__Work_Description__c from SVMXC__Service_Order_Line__c where recordtype.Name='Template' and (SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId() OR SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getUserId()) and Completed__c=False and SVMXC__Line_Type__c='Labor' order by SVMXC__Service_Order__c ,Id]; //Purchase_Order_Number1__c, DE7294

                // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
                mapofWorkDetailIdandInfo = new map<id,SVMXC__Service_Order_Line__c>([select Id,SVMXC__Consumed_From_Location__c,Completed__c,Actual_Hours__c,SVMXC__Group_Member__c,Name,SVMXC__Service_Order__c,SVMXC__Work_Description__c,SVMXC__Start_Date_and_Time__c,SVMXC__End_Date_and_Time__c,Parent_Template_Work_Detail_Id__c from SVMXC__Service_Order_Line__c where recordtype.Name='Usage/Consumption' and Parent_Template_Work_Detail_Id__c!=null and SVMXC__Start_Date_and_Time__c!=null and SVMXC__End_Date_and_Time__c!=null and (SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:userinfo.getuserid() OR SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:userinfo.getuserid()) and SVMXC__Line_Type__c='Labor' order by Parent_Template_Work_Detail_Id__c ,SVMXC__Start_Date_and_Time__c]); // Purchase_Order_Number1__c, DE7294

                for(SVMXC__Service_Order_Line__c wd:mapofWorkDetailIdandInfo.values())
                {
                    system.debug('putting in map--->'+wd.id);
                    mapofstringanddetail.put(wd.Parent_Template_Work_Detail_Id__c+string.valueof(wd.SVMXC__Start_Date_and_Time__c.day()),wd);
                }
                integer d = (da.day());
                system.debug('map of string and detail'+mapofstringanddetail);

                for(SVMXC__Service_Order_Line__c wdet:templateworkdetail)
                {
                    system.debug('yes coming inside-->'+wdet);
                    date xcomp = da;
                    integer x = d;

                    for(integer i=0;i<7;i++)
                    {
                        SVMXC__Service_Order_Line__c assoc;
                        system.debug('-----find in map--->'+wdet.id+string.valueof(xcomp.day()));
                        system.debug('test the map value-->'+mapofstringanddetail.get('a1mL0000000Da33IAC2'));

                        if(mapofstringanddetail.get(wdet.id+string.valueof(xcomp.day()))!=null)
                        {
                            assoc = mapofstringanddetail.get(wdet.id+string.valueof(xcomp.day()));
                            system.debug('Id is coming ???--'+assoc.id);
                        }
                        else
                        {
                            assoc = new SVMXC__Service_Order_Line__c();
                            assoc.Parent_Template_Work_Detail_Id__c =  wdet.id; 
                            assoc.SVMXC__Service_Order__c = wdet.SVMXC__Service_Order__c;
                            assoc.Actual_Hours__c = 0;  
                           // assoc.Purchase_Order_Number1__c = wdet.Purchase_Order_Number1__c;
                            assoc.SVMXC__Work_Description__c = wdet.SVMXC__Work_Description__c;
                            assoc.recordtypeid= RecordTypeId ;
                            assoc.SVMXC__Group_Member__c = wdet.SVMXC__Group_Member__c;
                            assoc.SVMXC__Line_Type__c='Labor';
                            assoc.Budgeted_Hours__c = wdet.Budgeted_Hours__c;                          
                            assoc.SVMXC__Consumed_From_Location__c = wdet.SVMXC__Consumed_From_Location__c;
                            system.debug('what is the value of group member-->'+assoc.SVMXC__Group_Member__c);
                        }
                        if(mapOfTemplateAndWorkdetail.get(wdet.id)==null)
                        {
                            Lstwd = new list<SVMXC__Service_Order_Line__c>();
                            Lstwd.add(assoc);
                        }
                        else
                        {
                            Lstwd  = mapOfTemplateAndWorkdetail.get(wdet.id);
                            Lstwd.add(assoc);
                        }
                        system.debug('yes create value in map--->'+assoc);
                        mapOfTemplateAndWorkdetail.put(wdet.id,Lstwd);
                       xcomp = xcomp +1;// new line
                    }
                }
            }
        }
    }

    public pagereference savetimesheet()
    {
        system.debug('--now map--->>>>>'+mapOfTemplateAndWorkdetail);
        //list<Work_Lines__c> lstWorkLine=new list<Work_Lines__c>();  // Code Added By Mohit
        List<SVMXC__Service_Order_Line__c> LstToUpsert = new list<SVMXC__Service_Order_Line__c>();
        map<integer, decimal> mapofdayandhour = new map<integer, decimal>();
        List<SVMXC__Service_Order_Line__c> TravelInToUpsert = new list<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> TraveloutToUpsert = new list<SVMXC__Service_Order_Line__c>();
        list<SVMXC_Time_Entry__c> MealBraekToUpsert = new list<SVMXC_Time_Entry__c>();
        map<string,decimal> mapOfTemplateandTotalHours = new map<string,decimal>();
     //   list<Work_Lines__c> lstWL=new list<Work_Lines__c>();

        for(integer i=0;i<LstTravelIn.size(); i++)
        {
            if(LstTravelIn[i].Actual_Hours__c>0)
            {
                integer minut = integer.valueof(8*60); 
                date dToday  = (da+i);
                Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day()).addminutes(minut);
                LstTravelIn[i].SVMXC__Start_Date_and_Time__c = dt;
                LstTravelIn[i].SVMXC__End_Date_and_Time__c = LstTravelIn[i].SVMXC__Start_Date_and_Time__c.addminutes(integer.valueof(LstTravelIn[i].Actual_Hours__c*60));
                mapofdayandhour.put(i,LstTravelIn[i].Actual_Hours__c+(8));
                TravelInToUpsert.add(LstTravelIn[i]);
            }
        }

        for(SVMXC__Service_Order__c Lst:Lstworkorder)
        {
            for(SVMXC__Service_Order_Line__c ser:Lst.SVMXC__Service_Order_Line__r)
            {
                integer i = 0;
                for(SVMXC__Service_Order_Line__c wde:mapOfTemplateAndWorkdetail.get(ser.id))
                {
                    if(mapofdayandhour.get(i)!=null)
                    {
                        integer minut = integer.valueof(mapofdayandhour.get(i)*60); 
                       // Work_Lines__c wl=new Work_Lines__c();
                        date dToday  = (da+i);
                        system.debug('---- now date is--->'+dToday);
                        Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day()).addminutes(minut);
                        //wl.Start_Time__c = dt;
                       // wl.Work_Detail__c=wde.id;
                        system.debug('----- now what is the date and hours-->'+wde.SVMXC__Start_Date_and_Time__c+'what are hours--'+minut);
                        mapofdayandhour.put(i,mapofdayandhour.get(i)+wde.Actual_Hours__c);
                       // lstWL.add(wl);
                    }
                }
            }
        }
       // Insert lstWL;

        //pagereference pg = page.SR_Project_Time_Sheet;
       // pg.setredirect(true);
        return null; //pg
    }
}
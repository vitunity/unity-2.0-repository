/****************************************************************
    Change Log: 
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    5-Sept-2017 - Amitkumar - STSK0012570 : Coding Task - STB Status Trigger Update
    
    @ Created By            : Parul Gupta
    @ Created Date          : 11/7/2014
    @ Created Description   : US4657, Create WO and WD records.
    @ Last Modified By      : Parul Gupta
    @ Last Modified Date    : 17/7/2014
    @ Last Modified Reason  : Update US4657.
    @ Last Modified By      : Parul Gupta
    @ Last Modified Date    : 22/7/2014
    @ Last Modified Reason  : US4657, Update Location from IP.
    @ Last Modified By      : Nikita Gupta
    @ Last Modified Date    : 25/7/2014
    @ Last Modified Reason  : US4583, Update Location from IP on both insert and update.
    @ Last Modified By      : Parul Gupta
    @ Last Modified Date    : 09/08/2014
    @ Last Modified Reason  : US4657, update Prod Serviced Work Detail.
*****************************************************************/

public class SR_STB_Status
{
    /****************** RECORD TYPE ID'S ************************************************/
    public static Id fieldServiceRecTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
    public static Id prodServiceRecTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
    public static Id caseHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();

    public static Set<ID> setInstProd = new Set<ID>();
    public static Map<Id, SVMXC__Installed_Product__c> mapIDInstProd = new Map<Id, SVMXC__Installed_Product__c>();  //map of IP from STB Status to be used in before and after trigger

    // Method added by Parul, 22/07/2014, US4560, before trigger on STB Status
    public void updateFieldsOnSTBStatus(List<STB_Status__c> listSTBStatus, Map<ID, STB_Status__c> mapOldSTBStatus) 
    {
        for(STB_Status__c varSTB : listSTBStatus)
        {
            if((mapOldSTBStatus == null && varSTB.Installed_Product__c != null) || (mapOldSTBStatus != null && varSTB.Installed_Product__c != mapOldSTBStatus.get(varSTB.ID).Installed_Product__c))
            {
                setInstProd.add(varSTB.Installed_Product__c);       
            }
        }

        /********************* QUERY ON INSTALLED PRODUCT ***********************************************/
        if(setInstProd.size() > 0)
        {

            //savon.FF 11-30-2015 US4335 DE6775 fields added to query to set to work order below
            for(SVMXC__Installed_Product__c varIP : [select Id, SVMXC__Site__c, SVMXC__Site__r.SVMXC__Account__c, SVMXC__Preferred_Technician__c,
                                                            SVMXC__Preferred_Technician__r.User__c, SVMXC__Preferred_Technician__r.SVMXC__Service_Group__c,SVMXC__Preferred_Technician__r.SVMXC__Working_Hours__c,
                                                            SVMXC__Site__r.SVMXC__City__c, SVMXC__Service_Contract__c, SVMXC__Site__r.SVMXC__Country__c,
                                                            SVMXC__Site__r.Country__r.SAP_Code__c, SVMXC__Service_Contract__r.SVMXC__Service_Level__c,
                                                            SVMXC__Site__r.SVMXC__State__c, SVMXC__Site__r.SVMXC__Street__c, SVMXC__Site__r.SVMXC__Zip__c,
                                                            First_Acceptance_Date__c from SVMXC__Installed_Product__c where ID in :setInstProd])
            {
                mapIDInstProd.put(varIP.Id, varIP);
            }

            for(STB_Status__c varSTBStatus : listSTBStatus)
            {
                if(mapIDInstProd.containsKey(varSTBStatus.Installed_Product__c))
                {
                    varSTBStatus.Location__c = mapIDInstProd.get(varSTBStatus.Installed_Product__c).SVMXC__Site__c;
                    system.debug('@@@@@@@!!!!!!! ' + mapIDInstProd.get(varSTBStatus.Installed_Product__c).SVMXC__Site__c + ' ' + varSTBStatus.Location__c);
                }
            }
        }
    }

    // Method added by Parul, 11/7/2014, US4560, after trigger on STB Status
  /*DE7593: Commented out for future release
    public void createWorkOrderWorkDetail(List<STB_Status__c> listSTBStatus) 
    {
        System.debug('Inside method createWorkOrderWorkDetail');
        set<ID> setSTBMasterID = new Set<ID>();
        Set<Id> setWorkOrderComp = new Set<Id>();
        set<DateTime> setDueDate = new Set<DateTime>();
        set<ID> setWorkOrderId = new set<ID>(); //Set to store created WO ID
        Map<ID, STB_Master__c> mapSTBMaster = new Map<ID, STB_Master__c>();
        Map<ID, List<SVMXC__Service_Order__c>> mapWOInstProdToWorkOrder = new Map<ID, List<SVMXC__Service_Order__c>>();
        map<id, SVMXC__Service_Order__c> mapUpdateCaseOnWO = new map<id, SVMXC__Service_Order__c>();
        Map<ID ,List<SVMXC__Service_Order_Line__c>> mapIP_ProdServWD = new Map<ID ,List<SVMXC__Service_Order_Line__c>>();
        list<Case> listCaseInsert = new list<Case>();
        List<SVMXC__Service_Order__c> listUpdateWO = new List<SVMXC__Service_Order__c>(); 
        map<String, Case> mapCaseProd = new map<String, Case>();

        List<SVMXC__Service_Order__c> listInsertWO = new List<SVMXC__Service_Order__c>();        
        List<SVMXC__Service_Order_Line__c> listUpdatePSWorkDet = new List<SVMXC__Service_Order_Line__c> ();
        List<SVMXC__SVMX_Event__c> listInsertEvent = new List<SVMXC__SVMX_Event__c>();

        for(STB_Status__c varSTB : listSTBStatus)
        {
            if(varSTB.STB_Master__c != null && varSTB.Status__c.startsWithIgnoreCase('P'))
            {
                setSTBMasterID.add(varSTB.STB_Master__c);
                system.debug('setSTBMasterID $$$##@@@' + setSTBMasterID);
            }
        }

        /********************** QUERY ON PRODUCTS STB MASTER *************************************
        if(setSTBMasterID.size() > 0)
        {
            mapSTBMaster = new map<ID, STB_Master__c>([Select ID, Due_Date__c, Can_Do_Completely_Remotely__c, Can_Do_Partially_Remotely__c from STB_Master__c where ID in :setSTBMasterID AND Due_Date__c > :system.today()]);
            System.debug('mapSTBMaster -->>' + mapSTBMaster);

            if(mapSTBMaster.size() > 0)
            {
                for(STB_Master__c varMaster : mapSTBMaster.values())
                {
                    setDueDate.add(varMaster.Due_Date__c);
                    system.debug('setDueDate!@#$%^%!@#$%' + setDueDate);
                }
            }
        }

        /********************** QUERY ON WORK ORDER *************************************
        if(mapSTBMaster.size() > 0 && setDueDate.size() > 0 && setInstProd.size() > 0)
        {
            System.debug('STB Master MAP, IP && Due Date not null');

            List<SVMXC__Service_Order__c> listWorkOrder = [select ID, SVMXC__Order_Status__c, SVMXC__Component__c, SVMXC__Purpose_of_Visit__c, RecordTypeID,
                                    SVMXC__Preferred_End_Time__c, SVMXC__Dispatch_Status__c, (select STB_Master__c from SVMXC__Service_Order_Line__r
                                    where STB_Master__c in :setSTBMasterID) from SVMXC__Service_Order__c where SVMXC__Order_Status__c = :'Open'
                                    AND SVMXC__Component__c in :setInstProd AND (SVMXC__Purpose_of_Visit__c = :'Stb'
                                    OR SVMXC__Purpose_of_Visit__c = :'stb' OR SVMXC__Purpose_of_Visit__c = :'STB') AND RecordTypeID = :fieldServiceRecTypeId];
            if(listWorkOrder.size() > 0)
            {
                for(SVMXC__Service_Order__c varWO : listWorkOrder)
                {
                    if(varWO.SVMXC__Component__c != null)
                    {
                        System.debug('Component not null');
                        list<SVMXC__Service_Order__c> listTempWO = new list<SVMXC__Service_Order__c>();

                        if(mapWOInstProdToWorkOrder.containsKey(varWO.SVMXC__Component__c))
                        {
                            listTempWO = mapWOInstProdToWorkOrder.get(varWO.SVMXC__Component__c);
                        }
                        listTempWO.add(varWO);
                        mapWOInstProdToWorkOrder.put(varWO.SVMXC__Component__c, listTempWO);
                        System.debug('mapWOInstProdToWorkOrder -->>' + mapWOInstProdToWorkOrder);
                    }
                }
            }
        }

        for(STB_Status__c varSTB : listSTBStatus)
        {
            if(varSTB.STB_Master__c != null && varSTB.Status__c.startsWithIgnoreCase('P'))
            {
                boolean endDateAndDueDate = false;
                if(mapWOInstProdToWorkOrder.containsKey(varSTB.Installed_Product__c))
                {
                    for(SVMXC__Service_Order__c varWO : mapWOInstProdToWorkOrder.get(varSTB.Installed_Product__c))
                    {
                        if(mapSTBMaster.containsKey(varSTB.STB_Master__c) && mapIDInstProd.containsKey(varSTB.Installed_Product__c) && mapIDInstProd.get(varSTB.Installed_Product__c).First_Acceptance_Date__c != null && mapIDInstProd.get(varSTB.Installed_Product__c).First_Acceptance_Date__c < mapSTBMaster.get(varSTB.STB_Master__c).Due_Date__c)
                        {
                            System.debug('IP First acceptance date < due date');
                            endDateAndDueDate = true;
                        }
                    }
                }
                if(endDateAndDueDate == false && mapSTBMaster.containsKey(varSTB.STB_Master__c) && mapIDInstProd.containsKey(varSTB.Installed_Product__c) && mapIDInstProd.get(varSTB.Installed_Product__c).SVMXC__Preferred_Technician__c != null && mapIDInstProd.get(varSTB.Installed_Product__c).SVMXC__Preferred_Technician__r.SVMXC__Working_Hours__c != null)
                {
                    System.debug('endDateAndDueDate -->>' + endDateAndDueDate);
                    SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c();

                    if(mapIDInstProd.containsKey(varSTB.Installed_Product__c))
                    {
                        setWorkOrderComp.add(varSTB.Installed_Product__c);

                        SVMXC__Installed_Product__c objIP = mapIDInstProd.get(varSTB.Installed_Product__c);
                        
                        objWO.SVMXC__Site__c  = objIP.SVMXC__Site__c;
                        objWO.SVMXC__Company__c = objIP.SVMXC__Site__r.SVMXC__Account__c;
                        objWO.SVMXC__Preferred_Technician__c = objIP.SVMXC__Preferred_Technician__c;
                        objWO.SVMXC__Group_Member__c = objIP.SVMXC__Preferred_Technician__c;
                        objWO.SVMXC__Service_Group__c = objIP.SVMXC__Preferred_Technician__r.SVMXC__Service_Group__c;
                        objWO.SVMXC__Purpose_of_Visit__c = 'Stb';
                        objWO.RecordTypeID = fieldServiceRecTypeId;
                        objWO.SVMXC__Component__c = varSTB.Installed_Product__c;
                        objWO.SVMXC__Dispatch_Response__c = 'Assigned';
                        objWO.SVMXC__Priority__c = 'Medium'; //DE3319, Nikita, updated SVMXC__Priority__c = '2 - Non Emergency' to SVMXC__Priority__c = 'Medium'
                        objWO.STB_Status__c = varSTB.Id; //cdelfattore@forefront 10/16/2015 (mm/dd/yyyy)
                        objWO.Malfunction_Start__c = system.now();
                        objWO.SVMXC__Preferred_Start_Time__c = BusinessHours.nextStartDate(objIP.SVMXC__Preferred_Technician__r.SVMXC__Working_Hours__c,mapSTBMaster.get(varSTB.STB_Master__c).Due_Date__c); //DE7250

                        objWO.SVMXC__Preferred_End_Time__c   = objWO.SVMXC__Preferred_Start_Time__c.addHours(4);  // DE7240

                        //savon.FF 11-30-2015 US4335 DE6775 field additions
                        objWO.SVMXC__City__c = objIP.SVMXC__Site__r.SVMXC__City__c;
                        objWO.Is_escalation_to_the_CLT_required__c = 'No';
                        objWO.Is_This_a_Complaint__c = 'No';
                        objWO.Was_anyone_injured__c = 'No';
                        objWO.SVMXC__Is_PM_Work_Order__c = false;
                        objWO.SVMXC__Is_Service_Covered__c = true;
                        objWO.Purchase_Order_Number__c = 'None Required';
                        objWO.SVMXC__Service_Contract__c = objIP.SVMXC__Service_Contract__c;
                        objWO.Service_Country__c = objIP.SVMXC__Site__r.SVMXC__Country__c;
                        objWO.Service_Country_Code__c = objIP.SVMXC__Site__r.Country__r.SAP_Code__c;
                        objWO.SVMXC__SLA_Terms__c = objIP.SVMXC__Service_Contract__r.SVMXC__Service_Level__c;
                        objWO.SVMXC__State__c = objIP.SVMXC__Site__r.SVMXC__State__c;
                        objWO.SVMXC__Street__c = objIP.SVMXC__Site__r.SVMXC__Street__c;
                        objWO.SVMXC__Top_Level__c = objIP.Id;
                        objWO.SVMXC__Zip__c = objIP.SVMXC__Site__r.SVMXC__Zip__c;
                        objWO.SVMXC__Order_Status__c = 'Open';
                        objWO.SVMXC__Problem_Description__c = varSTB.Name;
                        objWO.Subject__c = varSTB.Name;
             
                        
                        
                        //DE6926
                        objWO.Customer_Malfunction_Start_Date_Time__c = objWO.Malfunction_Start__c.day() + '-' + convertMonthIntNumberToName(objWO.Malfunction_Start__c.month()) + '-' + objWO.Malfunction_Start__c.year() +' '+ + (objWO.Malfunction_Start__c.hour()>12? objWO.Malfunction_Start__c.hour()-12 : objWO.Malfunction_Start__c.hour())+':'+ objWO.Malfunction_Start__c.minute() +' '+ (objWO.Malfunction_Start__c.hour()>12? 'PM' : 'AM');
                        objWO.Customer_Requested_Start_Date_Time__c = objWO.SVMXC__Preferred_Start_Time__c.day() + '-' + convertMonthIntNumberToName(objWO.SVMXC__Preferred_Start_Time__c.month()) + '-' + objWO.SVMXC__Preferred_Start_Time__c.year() +' '+ + (objWO.SVMXC__Preferred_Start_Time__c.hour()>12? objWO.SVMXC__Preferred_Start_Time__c.hour()-12 : objWO.SVMXC__Preferred_Start_Time__c.hour())+':'+ objWO.SVMXC__Preferred_Start_Time__c.minute() +' '+ (objWO.SVMXC__Preferred_Start_Time__c.hour()>12? 'PM' : 'AM');
                        objWO.Customer_Requested_End_Date_Time__c = objWO.SVMXC__Preferred_End_Time__c.day() + '-' + convertMonthIntNumberToName(objWO.SVMXC__Preferred_End_Time__c.month()) + '-' + objWO.SVMXC__Preferred_End_Time__c.year() +' '+ + (objWO.SVMXC__Preferred_End_Time__c.hour()>12? objWO.SVMXC__Preferred_End_Time__c.hour()-12 : objWO.SVMXC__Preferred_End_Time__c.hour())+':'+ objWO.SVMXC__Preferred_End_Time__c.minute() +' '+ (objWO.SVMXC__Preferred_End_Time__c.hour()>12? 'PM' : 'AM');
                        
                        objWO.ERP_Requested_End_Date_Time__c = constructDateTimeGMT(objWO.Customer_Requested_End_Date_Time__c);
                        objWO.ERP_Malfunction_Start_Date_Time__c = constructDateTimeGMT(objWO.Customer_Malfunction_Start_Date_Time__c);
                        objWO.ERP_Requested_Start_Date_Time__c = constructDateTimeGMT(objWO.Customer_Requested_Start_Date_Time__c);
                        //DE6926
                         
                        listInsertWO.add(objWO);
                    }
                }
            }
        }

        if(listInsertWO.size() > 0)
        {
            system.debug('listInsertWO&*()&^%%%&^&' + listInsertWO);
            insert listInsertWO;

            for(SVMXC__Service_Order__c varWO : listInsertWO)
            {
                setWorkOrderId.add(varWO.ID);   //Set to store created WO ID

                SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c(ID = varWO.ID);
                //De1952 Starts here (1)
                Case objCase = new Case();
                objCase.RecordtypeId= caseHD;
                objCase.Reason = varWO.SVMXC__Purpose_of_Visit__c;
                objCase.AccountID = varWO.SVMXC__Company__c;
                System.debug('top@@'+varWO.SVMXC__Component__c);
                objCase.ProductSystem__c = varWO.SVMXC__Component__c; //DE3449, Updated SVMXC__Top_Level__c to ProductSystem__c
                //objCase.SVMXC__Service_Order__r = varWO;
                System.debug(objCase.ProductSystem__c);  //DE3449, Updated SVMXC__Top_Level__c to ProductSystem__c
                objCase.ContactID = varWO.SVMXC__Contact__c;
                
                objCase.Priority = varWO.SVMXC__Priority__c;
                objCase.Type = 'Request';
                objCase.Requested_Start_Date_Time__c = varWO.SVMXC__Preferred_Start_Time__c; //DE2600
                objCase.Requested_End_Date_Time__c = varWO.SVMXC__Preferred_End_Time__c; //DE2600
                objCase.SVMXC__Service_Contract__c = varWO.SVMXC__Service_Contract__c;
                objCase.Not_Varian_s_Fault__c = false; //objCase.SVMXC__Billing_Type__c = 'O - Other'; //objCase. = varWO.
                
                //savon.FF 11-30-2015 US4335 DE6775 field additions
                objCase.City2__c = varWO.SVMXC__City__c;
                objCase.Is_escalation_to_the_CLT_required__c = 'No';
                objCase.Is_This_a_Complaint__c = 'No';
                objCase.Was_anyone_injured__c = 'No';
                objCase.SVMXC__Is_PM_Case__c = false;
                objCase.SVMXC__Is_Service_Covered__c = true;
                objCase.SVMXC__Preferred_End_Time__c = varWO.SVMXC__Preferred_End_Time__c;
                objCase.SVMXC__Preferred_Start_Time__c = varWO.SVMXC__Preferred_Start_Time__c; // DE7240
                objCase.SVMXC__Site__c = varWO.SVMXC__Site__c;
                objCase.SVMXC__SLA_Terms__c = varWO.SVMXC__SLA_Terms__c;
                objCase.State_Province2__c = varWO.SVMXC__State__c;
                objCase.Local_Subject__c = varWO.Subject__c;
                objCase.Description = varWO.Subject__c;
                objCase.SVMXC__Top_Level__c = varWO.SVMXC__Top_Level__c;
                objCase.Zip_Postal_Code__c = varWO.SVMXC__Zip__c;
                objCase.Status = 'New';

                objWO.SVMXC__Case__r = objCase;

                mapUpdateCaseOnWO.put(objWO.ID, objWO);
                
                listCaseInsert.add(objCase);
                // De1952 ends here 

                //create event record
                SVMXC__SVMX_Event__c objEvent = new SVMXC__SVMX_Event__c();
                if(varWO.SVMXC__Component__c != null && mapIDInstProd.containsKey(varWO.SVMXC__Component__c))
                {
                    SVMXC__Installed_Product__c objIP = mapIDInstProd.get(varWO.SVMXC__Component__c);
                    
                    objEvent.SVMXC__Technician__c = objIP.SVMXC__Preferred_Technician__c;
                    objEvent.SVMXC__WhoId__c = objIP.SVMXC__Preferred_Technician__r.User__c;
                    objEvent.SVMXC__WhatId__c = varWO.Id;
                    objEvent.SVMXC__Service_Order__c = varWO.Id;
                    objEvent.Name = 'Mandatory STB';
                    objEvent.Booking_Type__c = 'STB';
                    objEvent.SVMXC__StartDateTime__c = varWO.SVMXC__Preferred_Start_Time__c;
                    objEvent.SVMXC__EndDateTime__c = varWO.SVMXC__Preferred_End_Time__c;
                    if(varWO.SVMXC__Preferred_End_Time__c != null && varWO.SVMXC__Preferred_Start_Time__c != null)
                    {
                        objEvent.SVMXC__DurationInMinutes__c = ((varWO.SVMXC__Preferred_Start_Time__c.date().daysBetween(varWO.SVMXC__Preferred_End_Time__c.date())*24*60) + ((((varWO.SVMXC__Preferred_End_Time__c.time()).hour()) - ((varWO.SVMXC__Preferred_Start_Time__c.time()).hour()))*60) + ((((varWO.SVMXC__Preferred_End_Time__c.time()).minute()) - ((varWO.SVMXC__Preferred_Start_Time__c.time()).minute()))) ); //DE7240: + (((varWO.SVMXC__Preferred_End_Time__c.time()).second()) - ((varWO.SVMXC__Preferred_Start_Time__c.time()).second()))/60)
                    }

                    listInsertEvent.add(objEvent);
                }
            }
            if(listInsertEvent.size() > 0)
            {
                // using true because I am assuming you do not actually want a partially successful save.  use false if you do want some to succeed and others to fail.
                Database.SaveResult[] results = Database.insert(listInsertEvent, true);
                List<String> errorMessages = new List<String>();
                Integer count=0;
                for (Database.SaveResult sr : results) 
                {
                    if (!sr.isSuccess()) 
                    {
                        for (Database.Error e : sr.getErrors()) 
                        {
                            errorMessages.add(e.getMessage());
                            listInsertWO[count].addError(e.getMessage());
                        }
                    }
                    count++;
                }

                
            }
            //De1952 Starts here (2)
            if(listCaseInsert.size()>0)
            {
                insert listCaseInsert;
                if(mapUpdateCaseOnWO.size() > 0)
                {
                    for(SVMXC__Service_Order__c varWO : mapUpdateCaseOnWO.values())
                    {
                        if(varWO.SVMXC__Case__r != null)
                            varWO.SVMXC__Case__c = varWO.SVMXC__Case__r.ID;
                    }
                    
                    update mapUpdateCaseOnWO.values();
                }
            }
            // De1952 ends here
        }
        //De1952 Starts here (3)
         //for(SVMXC__Service_Order__c varWO : listInsertWO)
        {
            SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c(id=varWO.id);
            if(varWO.SVMXC__Top_Level__c!= null && mapCaseProd.containsKey(varWO.SVMXC__Top_Level__c))
            {
                System.debug('Name'+objWO.Name);
                system.debug('++'+varWO.SVMXC__Top_Level__c);
                objWO.SVMXC__Case__c = mapCaseProd.get(varWO.SVMXC__Top_Level__c).id;
                listUpdateWO.add(objWO);
            }
        }
        if(listUpdateWO.size()>0)
        {
            update listUpdateWO;
        }//
        // De1952 ends here

        // Update Products Serviced WD records
        if(setWorkOrderId.size() > 0 && setWorkOrderComp.size() > 0)
        {
            /********************** QUERY ON PRODUCTS SERVICED WORK DETAIL *************************************
            System.debug('setWorkOrderId and setWorkOrderComp -->>' + setWorkOrderId + '--' + setWorkOrderComp);
            List<SVMXC__Service_Order_Line__c> listProdServWD = [select ID, SVMXC__Serial_Number__c, SVMXC__Service_Order__c, SVMXC__Start_Date_and_Time__c,
                                                    SVMXC__End_Date_and_Time__c, SVMXC__Service_Order__r.SVMXC__Preferred_Start_Time__c, SVMXC__Service_Order__r.SVMXC__Preferred_End_Time__c from SVMXC__Service_Order_Line__c where RecordTypeID = : prodServiceRecTypeId AND SVMXC__Service_Order__c in : setWorkOrderId AND SVMXC__Serial_Number__c in : setWorkOrderComp];
            if(listProdServWD.size() > 0)
            {
                System.debug('listProdServWD -->>' + listProdServWD);
                for(SVMXC__Service_Order_Line__c varWD : listProdServWD)
                {
                    List<SVMXC__Service_Order_Line__c> tempListWD = new List<SVMXC__Service_Order_Line__c>();
                    if(mapIP_ProdServWD.containsKey(varWD.SVMXC__Serial_Number__c))
                    {
                        tempListWD = mapIP_ProdServWD.get(varWD.SVMXC__Serial_Number__c);
                    }
                    tempListWD.add(varWD); //mapWOIdProdServWD.put(varWD.SVMXC__Service_Order__c, tempListWD);
                    mapIP_ProdServWD.put(varWD.SVMXC__Serial_Number__c, tempListWD);
                }
            }
        }
        // update PS WD
        set<string> stDupeCheckWO=new set<string>();   // Added  to avoid list duplicate Exception Dated: 22-5-2015 :ms
        for(STB_Status__c varSTB : listSTBStatus)
        {
            if(mapIP_ProdServWD.containsKey(varSTB.Installed_Product__c))
            {
                for(SVMXC__Service_Order_Line__c objWD : mapIP_ProdServWD.get(varSTB.Installed_Product__c))
                {
                    objWD.STB_Master__c = varSTB.STB_Master__c;
                    //objWD.SVMXC__Start_Date_and_Time__c = objWD.SVMXC__Service_Order__r.SVMXC__Preferred_Start_Time__c;
                    //objWD.SVMXC__End_Date_and_Time__c = objWD.SVMXC__Service_Order__r.SVMXC__Preferred_End_Time__c;

                    if(mapSTBMaster.containsKey(varSTB.STB_Master__c) && (mapSTBMaster.get(varSTB.STB_Master__c).Can_Do_Completely_Remotely__c == true || mapSTBMaster.get(varSTB.STB_Master__c).Can_Do_Partially_Remotely__c == true))
                    {
                        objWD.Done_Remotely__c = true;
                    }
                    if(!stDupeCheckWO.contains(objWD.id)) // Added if Block to avoid list duplicate Exception Dated: 22-5-2015:ms
                    {
                        listUpdatePSWorkDet.add(objWD);
                        stDupeCheckWO.Add(objWD.id); // Added  to avoid list duplicate Exception Dated: 22-5-2015:ms
                    }
                    System.debug('listUpdatePSWorkDet@@@ ' + listUpdatePSWorkDet );
                }
            }
        }
        if(listUpdatePSWorkDet.size() > 0)
        {
            update listUpdatePSWorkDet;
        }
    } */

    //cdelfattore@forefront US5150 - TA7642
    //I want: the system to cancel system generated STB (Service Technical Bulletin) Work orders when the status on the STB Status Record becomes
    //'Completed, Verified, Admin Closure, or Superseded' on the PSE website.  This applies only where Work Orders do not have Work Detail Lines already open against them.
    //non static in order to reference recordTypeIds defined at the top of the class
   /*DE7593: Commented out for future release
    public void cancelSTBWorkOrder(List<STB_Status__c> listSTBStatus){
        Set<Id> STBStatusIdSet = new Set<Id>();
        for(STB_Status__c stbStat : listSTBStatus){
            //STB Status is set to ‘C-Complete’  or   ‘V-Verified’   or   ‘S-Superseded’   or   ‘X-Admin Closed’   or    ‘R-Refused by Customer’
            if(stbStat.Status__c.startsWithIgnoreCase('C-Completed') || stbStat.Status__c.startsWithIgnoreCase('V-Verified') || stbStat.Status__c.startsWithIgnoreCase('S-Superseded') || stbStat.Status__c.startsWithIgnoreCase('X-Admin') || stbStat.Status__c.startsWithIgnoreCase('R-Refused')){
                STBStatusIdSet.add(stbStat.Id);
            }
        }

        //query for the work orders and the work details
        //Work Order Status is not equal to Submitted, Approved, Closed, or Cancelled
        //There are NO usage consumption work details on the WO.  In case there are Usage consumption -> check the Cancel checkbox on that Work Orders but don't change the Order Status
        List<SVMXC__Service_Order__c> workOrderToUpdate = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c wo : [SELECT Id, SVMXC__Order_Status__c, (SELECT Id FROM SVMXC__Service_Order_Line__r WHERE RecordType.Name = 'Usage/Consumption') FROM SVMXC__Service_Order__c WHERE STB_Status__c IN :STBStatusIdSet]){
            //TA7642 There are NO usage consumption work details on the WO.  In case there are Usage consumption à check the Cancel checkbox on that Work Orders but don't change the Order Status . 
            //if(wo.SVMXC__Order_Status__c != 'Submitted' && wo.SVMXC__Order_Status__c != 'Approved' && wo.SVMXC__Order_Status__c != 'Closed' && wo.SVMXC__Order_Status__c != 'Cancelled' && wo.SVMXC__Order_Status__c != 'D-Product permanently scrapped (obsolete)' && wo.SVMXC__Service_Order_Line__r.isEmpty()){
                wo.SVMXC__Order_Status__c = 'Cancelled';
                workOrderToUpdate.add(wo);
            }//
            //TA7643 There are usage consumption work details on the WO.  In case there are Usage consumption à check the Cancel checkbox on that Work Orders but don't change the Order Status
            //else/ if(wo.SVMXC__Order_Status__c != 'Submitted' && wo.SVMXC__Order_Status__c != 'Approved' && wo.SVMXC__Order_Status__c != 'Closed' && wo.SVMXC__Order_Status__c != 'Cancelled' && wo.SVMXC__Order_Status__c != 'D-Product permanently scrapped (obsolete)' && !wo.SVMXC__Service_Order_Line__r.isEmpty()){
                wo.Cancel__c = true;
                workOrderToUpdate.add(wo);
            }
        }

        update workOrderToUpdate;
    }*/

    //savon.FF 11-30-2015 US4335 DE6775
    //created to set the WO fields on a WO that was created in the createWorkOrderWorkDetail method above from the Case created in same method 
    /* DE6926: Incorporated into the creation of the Work Order above
    public static void updateWOFieldsFromCase(List<SVMXC__Service_Order__c> workOrderList, Set<Id> caseIds) {
        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id, Customer_Malfunction_Start_Date_time_Str__c, Customer_Requested_End_Date_Time__c, 
                                                    Customer_Requested_Start_Date_Time__c, Customer_Preferred_End_Date_time__c, Customer_Preferred_Start_Date_time__c,
                                                    Malfunction_Start__c FROM Case WHERE Id IN :caseIds]);



        for (SVMXC__Service_Order__c varWO : workOrderList) {

            varWO.Customer_Malfunction_Start_Date_Time__c = cases.get(varWO.SVMXC__Case__c).Customer_Malfunction_Start_Date_time_Str__c;
            varWO.Customer_Requested_End_Date_Time__c = cases.get(varWO.SVMXC__Case__c).Customer_Requested_End_Date_Time__c;
            varWO.Customer_Requested_Start_Date_Time__c = cases.get(varWO.SVMXC__Case__c).Customer_Requested_Start_Date_Time__c;
            varWO.ERP_Requested_End_Date_Time__c = cases.get(varWO.SVMXC__Case__c).Customer_Preferred_End_Date_time__c;
            varWO.ERP_Requested_Start_Date_Time__c = cases.get(varWO.SVMXC__Case__c).Customer_Preferred_Start_Date_time__c;
            varWO.SVMXC__Preferred_Start_Time__c = system.now(); //reset preferred start time to now (set to now above, but was before malfunction start date which is error)
            varWO.Malfunction_Start__c = cases.get(varWO.SVMXC__Case__c).Malfunction_Start__c;

        }
    }*/
    
    /**
        Create GMT instance from datetime string, when set on field, should show teh right date/time when user timezone is GMT, user has to change time zone to GMT to see right values
    */
    public DateTime constructDateTimeGMT(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = new List<string>();
        if(dateTimeSplitList.size()>=3)
        {
            if(dateTimeSplitList[0].contains('/'))
            dateValueSplit = dateTimeSplitList[0].split('/') ;
            if(dateTimeSplitList[0].contains('-'))
            if(dateTimeSplitList[0] !=null )
            {
                dateValueSplit = dateTimeSplitList[0].split('-') ;
            }
            List<String> timeSplit = dateTimeSplitList[1].split(':');
            String strAMPM = dateTimeSplitList[2];
            DateTime constructedDateTime;
            if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),00, Integer.valueof(timeSplit[1]), 00);
            }
            else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
            }
            else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
            }
            
            return constructedDateTime;
        }
        else
        {
            return null;
        }
    }
    
    @TestVisible private String convertMonthIntNumberToName(Integer iMonthNumber)
    {
        if(iMonthNumber == 1)
            return 'Jan';
        else if(iMonthNumber == 2)
            return 'Feb';
        else if(iMonthNumber == 3)
            return 'Mar';
        else if(iMonthNumber == 4)
            return 'Apr';
        else if(iMonthNumber == 5)
            return 'May';
        else if(iMonthNumber == 6)
            return 'Jun';
        else if(iMonthNumber == 7)
            return 'Jul';
        else if(iMonthNumber == 8)
            return 'Aug';
        else if(iMonthNumber == 9)
            return 'Sep';
        else if(iMonthNumber == 10)
            return 'Oct';
        else if(iMonthNumber == 11)
            return 'Nov';
        else if(iMonthNumber == 12)
            return 'Dec';
        else
            return 'Jan';
    }
    
    @TestVisible private String convertMonthNameToNumber(String strMonthName)
    {
        if(strMonthName == 'Jan')
            return '01';
        else if(strMonthName == 'Feb')
            return '02';
        else if(strMonthName == 'Mar')
            return '03';
        else if(strMonthName == 'Apr')
            return '04';
        else if(strMonthName == 'May')
            return '05';
        else if(strMonthName == 'Jun')
            return '06';
        else if(strMonthName == 'Jul')
            return '07';
        else if(strMonthName == 'Aug')
            return '08';
        else if(strMonthName == 'Sep')
            return '09';
        else if(strMonthName == 'Oct')
            return '10';
        else if(strMonthName == 'Nov')
            return '11';
        else if(strMonthName == 'Dec')
            return '12';
        else
            return '01';
    }
    
    /*
     * STRY0013023 : Offline: DE8164:  Map STB Org to STB Status - Required for MFL Use without Sync
     * STSK0012570 : Coding Task - STB Status Trigger Update
     */
    public void updateSTBJONumber(List<STB_Status__c> stbStatusList, map<Id,STB_Status__c> stbStatusMap){
        
        set<id> locationIds = new set<id> ();
        set<id> stbMasterids = new set<id> ();
        for(STB_Status__c stbStatusObj : stbStatusList){
            if(stbStatusObj.Location__c != null 
                && stbStatusObj.STB_Master__c != null
                && (stbStatusMap == null
                    || stbStatusMap.get(stbStatusObj.id).Location__c != stbStatusObj.Location__c 
                    || stbStatusMap.get(stbStatusObj.id).STB_Master__c != stbStatusObj.STB_Master__c
                    || stbStatusObj.STB_Org_JO__c == null
                    ) 
            ){
                locationIds.add(stbStatusObj.Location__c);
                stbMasterids.add(stbStatusObj.STB_Master__c);
                
            }
        }
        
        if(!locationIds.isEmpty()){
            set<String> stbJOOrg = new set<String>();
            map<Id,STB_Master__c> stbMap = new map<Id,STB_Master__c>([select Name, Job_Number__c from STB_Master__c where Id in :stbMasterids]);
            map<Id,SVMXC__Site__c> locationMap = new map<Id,SVMXC__Site__c>([select Sales_Org__c from SVMXC__Site__c where Id in : locationIds]);
            
            for(STB_Status__c stbStatusObj : stbStatusList){
                if(locationMap.containsKey(stbStatusObj.Location__c) && stbMap.containsKey(stbStatusObj.STB_Master__c)){
                    stbJOOrg.add(stbMap.get(stbStatusObj.STB_Master__c).Name +' '+locationMap.get(stbStatusObj.Location__c).Sales_Org__c);
                }
            }
            
            if(!stbJOOrg.isEmpty()){
                
                map<String,STB_Org__c> stbOrgMap = new map<String,STB_Org__c>();
                for(STB_Org__c stbOrgObj : [select Name, Job_Number__c from STB_Org__c where name in : stbJOOrg]){
                    stbOrgMap.put(stbOrgObj.name,stbOrgObj);
                }
                
                for(STB_Status__c stbStatusObj : stbStatusList){
                    if(locationMap.containsKey(stbStatusObj.Location__c) 
                       && stbMap.containsKey(stbStatusObj.STB_Master__c)
                       && locationMap.get(stbStatusObj.Location__c).Sales_Org__c != '0600'){
                        String key = stbMap.get(stbStatusObj.STB_Master__c).Name +' '+locationMap.get(stbStatusObj.Location__c).Sales_Org__c;
                        if(stbOrgMap.containsKey(key)){
                            stbStatusObj.STB_Org_JO__c = stbOrgMap.get(key).Job_Number__c;
                        }
                    }else{
                        if(stbMap.containsKey(stbStatusObj.STB_Master__c)){
                            stbStatusObj.STB_Org_JO__c = stbMap.get(stbStatusObj.STB_Master__c).Job_Number__c;
                        }
                    }
                }
            }
        }
    }
    
}
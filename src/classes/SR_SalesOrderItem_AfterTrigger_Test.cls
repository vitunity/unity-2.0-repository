/**
 * Test Class Dedicated to SR_SalesOrderItem_AfterTrigger Trigger
 */
@isTest
private class SR_SalesOrderItem_AfterTrigger_Test {
  
  public static BusinessHours businesshr;
  public static Account newAcc;
  public static Product2 newProd;
  public static Product2 prodModel;
  public static List<Product2> prod_List;
  public static Product_FRU__c ProdAssociation;
  public static Sales_Order__c SO;
  public static Sales_Order_Item__c parent_SOI;
  public static Sales_Order_Item__c SOI;
  public static List<Sales_Order_Item__c> SOI_List;
  public static Case newCase;
  public static SVMXC__Case_Line__c caseLine;
  public static SVMXC__Service_Group__c servTeam;
  public static SVMXC__Service_Group_Members__c technician;
  public static SVMXC__Site__c newLoc;
  public static SVMXC__Service_Order__c WO;
  
  static {
    businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1]; 
    
    newAcc = UnityDataTestClass.AccountData(true);
    newProd = UnityDataTestClass.product_Data(false); 
    
    prodModel = new Product2();
        prodModel.Name = 'Rapid Arc for Eclipse MODEL';
        prodModel.ProductCode = 'TEST_VC_RAE_MODEL';
        prodModel.BigMachines__Part_Number__c = 'TEST_VC_RAE' + Math.random();
        prodModel.Product_Type__c = 'Model';
        prodModel.IsActive = true;
        prodModel.Product_Source__c = 'SAP';
        prodModel.ERP_Plant__c = '0601';
        prodModel.ERP_Sales_Org__c = '0601';
        prodModel.UOM__c = 'EA';
    
    prod_List = new List<Product2>();
    prod_List.add(newProd);
    prod_List.add(prodModel);
    insert prod_List;
    
    ProdAssociation = new Product_FRU__c();
    ProdAssociation.Top_Level__c = newProd.Id;
    ProdAssociation.Part__c = prodModel.Id;
    insert ProdAssociation;
    
    SO = UnityDataTestClass.SO_Data(true,newAcc);
    Parent_SOI = UnityDataTestClass.SOI_Data(true,SO);
    SOI = UnityDataTestClass.SOI_Data(false, SO);
    SOI.Parent_Item__c = Parent_SOI.Id;
    SOI.Product__c = newProd.Id;
    SOI.Product_Model__c = prodModel.Id;
    SOI.ERP_Item_Category__c = 'Z003';
    insert SOI;
    
    servTeam = UnityDataTestClass.serviceTeam_Data(true);
    technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
    newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
  }
  
    static testMethod void SOI_Location_Update() {
        Test.StartTest();
        
        SOI.Location__c = newLoc.Id;
        SOI.ERP_Reference__c = 'H621598-VE';
        update SOI;
      
      Test.StopTest();
    }
    
    static testMethod void SOI_Rejection_Update() {
      Test.StartTest();
      
      newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
    
    caseLine = UnityDataTestClass.CaseLine_Data(false, newCase, newProd);
    caseLine.Sales_Order_Item__c = SOI.Id;
    insert caseLine;
      
      WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
      WO.Event__c = false;
      insert WO;
      
      SOI.Reject_Reason__c = 'V2-Clerical Cancellation';
      update SOI;
    
      Test.StopTest();
    }
    
    
}
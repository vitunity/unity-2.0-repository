/**
 * Test Class Dedicated to SR_SalesOrder_AfterTrigger Trigger
 * 
 */
@isTest
private class SR_SalesOrder_AfterTrigger_Test {
  
  public static Account newAcc;
  public static Sales_Order__c SO;
  public static Sales_Order_Item__c SOI;
  
  static {
    newAcc = UnityDataTestClass.AccountData(true);
    SO = UnityDataTestClass.SO_Data(true,newAcc);
    SOI = UnityDataTestClass.SOI_Data(true, SO);
  }
  
    static testMethod void SO_Cancellation_Update_Test() {
        Test.StartTest();
        
        SO.Block__c = '70-Order Cancellation';
        update SO;
        
      Test.StopTest();
    }
}
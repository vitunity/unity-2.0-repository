global class SR_SendCustomerRequestForm {
    
    webservice static void sendEmail(ID recId){
        User userRec = new User();
        ERP_Customer_Request__c  erpCustReq = [Select Sales_Org__c from ERP_Customer_Request__c where id =: recId];
        
        Blob cntPDF ; 
        PageReference orderPg = Page.SR_CustomerRequestFormPdf;
        orderPg.getParameters().put('Id',recId); 
        
        if(!Test.isRunningTest()){
            cntPDF = orderPg.getContent();
        }else{
            cntPDF = Blob.valueOf('TEST');
        }
                
        Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
        efa1.setFileName('CustomerRequestForm.pdf');
        efa1.setBody( cntPDF );
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] toaddress = new List<String>(); 
         
        String functionality = 'customer request';
        
        List<String> salesOrgs = new List<String>();
        if(!String.isBlank(erpCustReq.Sales_Org__c)){
            salesOrgs = erpCustReq.Sales_Org__c.split(';');
        }
        
        List<Email_Dlists__c> emailDlist = [Select Id, Primary_Dlist__c from Email_Dlists__c where Functionality__c =: functionality and SalesOrg__c IN :salesOrgs];
        userRec = [Select Id, Email from User where id = :Userinfo.getUserId() limit 1];
        Set<String> uniqueEmailSet = new Set<String>();
        uniqueEmailSet.add(userRec.Email);
        for(Email_Dlists__c E : emailDlist){
            uniqueEmailSet.add(E.Primary_Dlist__c);
        } 
         
        for(String S : uniqueEmailSet){
            toaddress.add(S);
        }
        List<Messaging.EmailFileAttachment> fileAttachments = new List<Messaging.EmailFileAttachment>{efa1};
        List<Attachment> aList  = [select Name, Body from Attachment where ParentId = :recId limit 10];
        for(Attachment a : aList){    
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(a.Name);
            efa.setBody(a.Body);
            fileAttachments.add(efa);                   
        }
        
        if(!toaddress.isEmpty()){
            email.setSubject('Customer Request Form') ;
            email.setToAddresses(toaddress);
            email.setHtmlBody('<html>' + '<br/>'+ 'Attached is the customer request form') ;
            email.setFileAttachments(fileAttachments); 
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}) ;
        }
    }
}
@isTest(seeAllData=true)
private class SR_SendCustomerRequestFormTest {

    static testMethod void testSR_SendCustomerRequestForm() {
        SR_SuccessEmailRestControllerTest.insertDlists();
        SR_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        Account sfdcAccount = [Select Id, SFDC_Account_Ref_Number__c 
									From Account 
									Where Id =:SR_PrepareOrderControllerTest.CombinedQuote.Bigmachines__Account__c];
        
        PageReference erpCustomerRequestPage = Page.SR_CustomerRequestForm;
        erpCustomerRequestPage.getParameters().put('quoteId',SR_PrepareOrderControllerTest.combinedQuote.Id);
        erpCustomerRequestPage.getParameters().put('salesOrg','0601-US - OS - Systems - 0600-US - OS - Service');
        erpCustomerRequestPage.getParameters().put('BP', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('SP', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('PY', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('Z1', 'Combined1212');
        erpCustomerRequestPage.getParameters().put('SH', 'Combined1212');
        Test.setCurrentPage(erpCustomerRequestPage);
        
        ApexPages.StandardController sc  = new ApexPages.StandardController(new ERP_Customer_Request__c());
        
        SR_ERPCustomerRequest erpCustomerRequest = new SR_ERPCustomerRequest(sc);
        erpCustomerRequest.saveRequest();
        
        ERP_Customer_Request__c erpCustomerRec = [Select Id, New_Updated_Bill_To_Name__c,New_Updated_Site_Partner_Name__c,
        													Ship_To_Name__c,New_Updated_Payer_Name__c,New_Updated_Sold_To_Name__c,
        													SFA_Number_Sold_To__c,SFA_Number_Site_Partner__c
        												From  ERP_Customer_Request__c
        												Where New_Updated_Bill_To_Name__c = 'Sample Partner'];
        
        Attachment customerReqAttachment = new Attachment();
        customerReqAttachment.Name = 'TEST.PDF';
        customerReqAttachment.Body = Blob.valueOf('Test');
        customerReqAttachment.ParentId = erpCustomerRec.Id;
        insert customerReqAttachment;
        
        SR_SendCustomerRequestForm.sendEmail(erpCustomerRec.Id);
    }
}
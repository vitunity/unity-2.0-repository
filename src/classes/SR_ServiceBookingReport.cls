/**
 * Controller for Quote service booking pdf
 */
public with sharing class SR_ServiceBookingReport {
  
  /**
   * Customer and order details details from Quote
   */
  public BigMachines__Quote__c quoteRec{get;set;}
  
  /**
   * Total of all sla parts price in a section
   */
  public Map<Decimal,Map<String,Decimal>> sectionTotals{get;set;}
  
  /**
   * Total of all sla parts price in al sections(Quotation totals)
   */
  public Map<String,Decimal> quotationTotals{get;set;}
  
  /**
   * Entitlements grouped by products
   */
  public Map<Decimal,Map<Decimal,List<Quote_Product_Pricing__c>>> productEntitlementsMap{get;set;}
  
  /**
   * sub-products each instanlled or non-installed service product
   */
  public Map<Decimal,List<Quote_Product_Pricing__c>> subProductsMap{get;set;}
  
  /**
   * There can be multiple products in each section which are represented by SLA parts
   */
  public Map<Decimal,Quote_Product_Pricing__c> slaPartsMap{get;set;}

  public Map<Decimal, Boolean> subProductsKeyMap{get;set;}
  public Map<Decimal, Boolean> slaPartsKeyMap{get;set;}
  public Map<Decimal, Boolean> sectionTotalsKeyMap{get;set;}
  
  public SR_ServiceBookingReport(){
    quoteRec = [SELECT Id, Name, Owner.Name,CurrencyIsoCode,BigMachines__Account__r.Name,BigMachines__Account__r.BillingCity,
          BigMachines__Account__r.BillingState,BigMachines__Opportunity__r.CurrencyISOCode
          FROM BigMachines__Quote__c 
          WHERE id =: ApexPages.currentPage().getParameters().get('id')];
  }
  
  
  
  public void getServiceBookingReportData(){
    List<Quote_Product_Pricing__c> quotePricings = [SELECT Id,Doc_Number__c,Entitle_price_in_USD__c,Install_Base__c,SLA_Part__c,Quote_Product__r.Parent_Doc_Number__c,
                            Part_Number__c,Description__c,Quantity__c,Quote_Product__r.BigMachines__Product__r.Name,Total_Price__c,
                            Quote_Product__r.BigMachines__Description__c,Quote_Product__r.BigMachines__Quantity__c,Quote_Product__r.Name,
                            Quote_Product__r.Standard_Price__c,Quote_Product__r.BigMachines__Sales_Price__c,Quote_Product__r.Year1__c,
                            Quote_Product__r.Year2__c,Quote_Product__r.Year3__c,Quote_Product__r.Year4__c,Quote_Product__r.Year5__c,
                            Quote_Product__r.Year6__c,Quote_Product__r.Year7__c,Quote_Product__r.Year8__c,Quote_Product__r.Year9__c,
                            Quote_Product__r.Year10__c,Year1__c,Year2__c,Year3__c,Year4__c,Year5__c,Year6__c,Year7__c,Year8__c,Year9__c,Year10__c,
                            Coverage_Start_Date__c,Coverage_End_Date__c,Quote_Product__c
                            FROM Quote_Product_Pricing__c
                            WHERE BMI_Quote__c =:quoteRec.Id];
    
    List<Quote_Product_Pricing__c> productEntitlements = new List<Quote_Product_Pricing__c>();
    List<Quote_Product_Pricing__c> subProducts = new List<Quote_Product_Pricing__c>();     
    List<Quote_Product_Pricing__c> slaParts = new List<Quote_Product_Pricing__c>();                  
    
    for(Quote_Product_Pricing__c quotePricing : quotePricings){
      if(!quotePricing.SLA_Part__c && String.isBlank(quotePricing.Install_Base__c)){
        productEntitlements.add(quotePricing);
      }else if(!String.isBlank(quotePricing.Install_Base__c)){
        subProducts.add(quotePricing);
      }else{
        slaParts.add(quotePricing);
      }
    }
    
    populateEntitlementsMap(productEntitlements);  
    populateSubProductsMap(subProducts);
    populateSLAPartsMap(slaParts);
    populateSectionTotals(slaParts);
    
    //Fix for Map key [] not found in map. d represent parent doc number Ajinkya INC4581606
    /*for(Decimal d : productEntitlementsMap.keySet()) {
        system.debug(' == KEY == ' + d);
        if(!subProductsMap.containsKey(d)) {
            system.debug(' == KEY == ' + d);
            productEntitlementsMap.remove(d);
            //subProductsMap.put(d, null);
        }
    }*/
    

    // Nilesh - Fix for Map key [] not found for sectionTotals, slaPartsKeyMap and subProductsKeyMap
    subProductsKeyMap = new Map<Decimal, Boolean>();
    slaPartsKeyMap    = new Map<Decimal, Boolean>();
    sectionTotalsKeyMap = new Map<Decimal, Boolean>();

    for(Decimal parentDocNumber : productEntitlementsMap.keySet()) {
        Map<Decimal,List<Quote_Product_Pricing__c>> productPricingMap = productEntitlementsMap.get(parentDocNumber);
    
        // Check for parentDocNumber key exist in sectionTotals
        if(!sectionTotals.containsKey(parentDocNumber))
            sectionTotalsKeyMap.put(parentDocNumber, false);
        else
            sectionTotalsKeyMap.put(parentDocNumber, true);

        for(Decimal docNumber : productPricingMap.keySet()) {
            // Check for docNumber key exist in slaPartsMap
            if(!slaPartsMap.containsKey(docNumber))
                slaPartsKeyMap.put(docNumber, false);
            else
                slaPartsKeyMap.put(docNumber, true);

            // Check for docNumber key exist in subProductsMap
            if(!subProductsMap.containsKey(parentDocNumber))
                subProductsKeyMap.put(parentDocNumber, false);
            else
                subProductsKeyMap.put(parentDocNumber, true);
        }
    }
  }
  
  /**
   * Returns products and its entitlements
   * Groups product by Parent doc number and doc number.
   * Every parent doc number grouping can contains mulitple SLA parts and each SLA part is related to multiple entitlements
   */
  private void populateEntitlementsMap(List<Quote_Product_Pricing__c> entitlements){
    
    productEntitlementsMap = new Map<Decimal,Map<Decimal,List<Quote_Product_Pricing__c>>>();
    
    for(Quote_Product_Pricing__c quotePricing : entitlements){
      
      Decimal parentDocNumber = quotePricing.Quote_Product__r.Parent_Doc_Number__c;
      Decimal docNumber = quotePricing.Doc_Number__c;
      
      if(productEntitlementsMap.containsKey(parentDocNumber)){
        Map<Decimal,List<Quote_Product_Pricing__c>> productPricings = productEntitlementsMap.get(parentDocNumber);
        if(productPricings.containsKey(docNumber)){
          productPricings.get(docNumber).add(quotePricing);
        }else{
          productPricings.put(docNumber, new List<Quote_Product_Pricing__c>{quotePricing});
        }
      }else{
        productEntitlementsMap.put(
                      parentDocNumber,
                      new Map<Decimal,List<Quote_Product_Pricing__c>>{docNumber => new List<Quote_Product_Pricing__c>{quotePricing}}
                    );
      }
    }
  }
  
  /**
   * Returns sub-products each installed or non-installed service product
   */
  private void populateSubProductsMap(List<Quote_Product_Pricing__c> subProducts){
    subProductsMap = new Map<Decimal,List<Quote_Product_Pricing__c>>();
    
    for(Quote_Product_Pricing__c quotePricing : subProducts){
      
      Decimal parentDocNumber = quotePricing.Quote_Product__r.Parent_Doc_Number__c;
      if(subProductsMap.containsKey(parentDocNumber)){
        subProductsMap.get(parentDocNumber).add(quotePricing);
      }else{
        subProductsMap.put(parentDocNumber, new List<Quote_Product_Pricing__c>{quotePricing});
      }
    }
  }
  
  /**
   * populate sla parts map
   */
  private void populateSLAPartsMap(List<Quote_Product_Pricing__c> slaParts){
    
    slaPartsMap = new Map<Decimal,Quote_Product_Pricing__c>();
    
    for(Quote_Product_Pricing__c slaPart : slaParts){
      slaPartsMap.put(slaPart.Doc_Number__c,slaPart);
    }
  }
  
  /**
   * Section Total is to total of all sla parts belong to perticular section and Quotation totals
   */
  private void populateSectionTotals(List<Quote_Product_Pricing__c> slaParts){
    sectionTotals = new Map<Decimal,Map<String,Decimal>>();
    quotationTotals = new Map<String,Decimal>{
      'offerLocalTotal' => 0.0,
      'offerUSDTotal' => 0.0,
      'year1Total' => 0.0,
      'year2Total' => 0.0,
      'year3Total' => 0.0,
      'year4Total' => 0.0,
      'year5Total' => 0.0,
      'year6Total' => 0.0,
      'year7Total' => 0.0,
      'year8Total' => 0.0,
      'year9Total' => 0.0,
      'year10Total' => 0.0 
    };
    
    Map<String,Decimal> conversionRates = getConversionRates();
    
    for(Quote_Product_Pricing__c slaPart : slaParts){
      
      Decimal parentDocNumber = slaPart.Quote_Product__r.Parent_Doc_Number__c;      
      if(sectionTotals.containsKey(parentDocNumber)){
        Map<String,Decimal> sectionTotalsMap = sectionTotals.get(parentDocNumber);
        preparePricingTotalsMap(sectionTotalsMap, slaPart, conversionRates);
        sectionTotals.put(parentDocNumber, sectionTotalsMap);
      }else{
        
        Map<String,Decimal> pricingTotals = new Map<String,Decimal>{
          'offerLocalTotal' => slaPart.Quote_Product__r.BigMachines__Sales_Price__c,
          'offerUSDTotal' => (slaPart.Quote_Product__r.BigMachines__Sales_Price__c * conversionRates.get(quoteRec.CurrencyISOCode)),
          'year1Total' => slaPart.Quote_Product__r.Year1__c,
          'year2Total' => slaPart.Quote_Product__r.Year2__c,
          'year3Total' => slaPart.Quote_Product__r.Year3__c,
          'year4Total' => slaPart.Quote_Product__r.Year4__c,
          'year5Total' => slaPart.Quote_Product__r.Year5__c,
          'year6Total' => slaPart.Quote_Product__r.Year6__c,
          'year7Total' => slaPart.Quote_Product__r.Year7__c,
          'year8Total' => slaPart.Quote_Product__r.Year8__c,
          'year9Total' => slaPart.Quote_Product__r.Year9__c,
          'year10Total' => slaPart.Quote_Product__r.Year10__c 
        };
        
        sectionTotals.put(parentDocNumber,pricingTotals);
      }
      preparePricingTotalsMap(quotationTotals, slaPart, conversionRates);
    }
  }
  
  private void preparePricingTotalsMap(Map<String,Decimal> totalsMap, Quote_Product_Pricing__c slaPart, Map<String,Decimal> conversionRates){
    
    Decimal offerLocalTotal = totalsMap.get('offerLocalTotal') + slaPart.Quote_Product__r.BigMachines__Sales_Price__c;
    Decimal offerUSDTotal = totalsMap.get('offerUSDTotal') + (slaPart.Quote_Product__r.BigMachines__Sales_Price__c * conversionRates.get(quoteRec.CurrencyISOCode));
    System.debug('----totalsMap'+totalsMap.get('year1Total')+'---'+slaPart.Quote_Product__c);
    System.debug('----Quote_Product__c'+(slaPart.Quote_Product__r.Year1__c==null?0.0:slaPart.Quote_Product__r.Year1__c)+'---'+slaPart.Quote_Product__r.Year1__c);
    Decimal year1Total = totalsMap.get('year1Total') + (slaPart.Quote_Product__r.Year1__c==null?0.0:slaPart.Quote_Product__r.Year1__c);
    Decimal year2Total = totalsMap.get('year2Total') + (slaPart.Quote_Product__r.Year2__c==null?0.0:slaPart.Quote_Product__r.Year2__c);
    Decimal year3Total = totalsMap.get('year3Total') + (slaPart.Quote_Product__r.Year3__c==null?0.0:slaPart.Quote_Product__r.Year3__c);
    Decimal year4Total = totalsMap.get('year4Total') + (slaPart.Quote_Product__r.Year4__c==null?0.0:slaPart.Quote_Product__r.Year4__c);
    Decimal year5Total = totalsMap.get('year5Total') + (slaPart.Quote_Product__r.Year5__c==null?0.0:slaPart.Quote_Product__r.Year5__c);
    Decimal year6Total = totalsMap.get('year6Total') + (slaPart.Quote_Product__r.Year6__c==null?0.0:slaPart.Quote_Product__r.Year6__c);
    Decimal year7Total = totalsMap.get('year7Total') + (slaPart.Quote_Product__r.Year7__c==null?0.0:slaPart.Quote_Product__r.Year7__c);
    Decimal year8Total = totalsMap.get('year8Total') + (slaPart.Quote_Product__r.Year8__c==null?0.0:slaPart.Quote_Product__r.Year8__c);
    Decimal year9Total = totalsMap.get('year9Total') + (slaPart.Quote_Product__r.Year9__c==null?0.0:slaPart.Quote_Product__r.Year9__c);
    Decimal year10Total = totalsMap.get('year10Total') + (slaPart.Quote_Product__r.Year10__c==null?0.0:slaPart.Quote_Product__r.Year10__c);
    
    totalsMap.put('offerLocalTotal',offerLocalTotal);
    totalsMap.put('offerUSDTotal',offerLocalTotal);
    totalsMap.put('year1Total',year1Total);
    totalsMap.put('year2Total',year2Total);
    totalsMap.put('year3Total',year3Total);
    totalsMap.put('year4Total',year4Total);
    totalsMap.put('year5Total',year5Total);
    totalsMap.put('year6Total',year6Total);
    totalsMap.put('year7Total',year7Total);
    totalsMap.put('year8Total',year8Total);
    totalsMap.put('year9Total',year9Total);
    totalsMap.put('year10Total',year10Total);
  }
  
  /** 
   * Returns conversion rate map which contains conversion rate for each currency against USD
   */
  public Map<String,Decimal> getConversionRates(){
    Map<String,Decimal> conversionRates = new Map<String,Decimal>();
    
    List<CurrencyType> currencies = [SELECT Id, IsoCode, ConversionRate, DecimalPlaces, IsActive, IsCorporate
                    FROM CurrencyType]; 
                      
    for(CurrencyType sfCurrency : currencies){
      conversionRates.put(sfCurrency.ISOCode,sfCurrency.ConversionRate);
    }
    return conversionRates;
  }
}
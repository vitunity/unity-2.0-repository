@isTest(seeAllData=true)
private class SR_ServiceBookingReportTest {

    static testMethod void testServiceBookingReport() {
        Sr_PrepareOrderControllerTest.setUpServiceTestdata();
        List<BigMachines__Quote_Product__c> quoteProducts = [SELECT Id,Doc_Number__c 
                                                                FROM BigMachines__Quote_Product__c
                                                                WHERE BigMachines__Quote__c =: SR_PrepareOrderControllerTest.serviceQuote.Id];
        
        List<Quote_Product_Pricing__c> quoteProductPricings = new List<Quote_Product_Pricing__c>();
        
        Integer counter = 0;
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            if(counter == 0){
                Quote_Product_Pricing__c quoteProductPricing = new Quote_Product_Pricing__c();
                quoteProductPricing.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing.Name= 'TEST';
                quoteProductPricing.Doc_Number__c = 2;
                quoteProductPricing.Install_Base__c = 'TEST123';
                quoteProductPricing.Entitle_price_in_USD__c = 123;
                quoteProductPricing.Year1__c = 123;
                quoteProductPricings.add(quoteProductPricing);
                Quote_Product_Pricing__c quoteProductPricing1 = new Quote_Product_Pricing__c();
                quoteProductPricing1.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing1.Name= 'TEST';
                quoteProductPricing1.Doc_Number__c = 2;
                quoteProductPricing1.SLA_Part__c = true;
                quoteProductPricings.add(quoteProductPricing1);
                Quote_Product_Pricing__c quoteProductPricing2 = new Quote_Product_Pricing__c();
                quoteProductPricing2.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing2.Name= 'TEST';
                quoteProductPricing2.Doc_Number__c = 2;
                quoteProductPricing2.Entitle_price_in_USD__c = 123;
                quoteProductPricing2.Year1__c = 123;
                quoteProductPricings.add(quoteProductPricing2);
            }else if(counter == 1){
                Quote_Product_Pricing__c quoteProductPricing1 = new Quote_Product_Pricing__c();
                quoteProductPricing1.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing1.Name= 'TEST';
                quoteProductPricing1.Doc_Number__c = 3;
                quoteProductPricing1.SLA_Part__c = true;
                quoteProductPricing1.Entitle_price_in_USD__c = 123;
                quoteProductPricing1.Year1__c = 123;
                Quote_Product_Pricing__c quoteProductPricing2 = new Quote_Product_Pricing__c();
                quoteProductPricing2.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing2.Name= 'TEST';
                quoteProductPricing2.Doc_Number__c = 3;
                quoteProductPricing2.Entitle_price_in_USD__c = 123;
                quoteProductPricing2.Year1__c = 123;
                Quote_Product_Pricing__c quoteProductPricing3 = new Quote_Product_Pricing__c();
                quoteProductPricing3.BMI_Quote__c = SR_PrepareOrderControllerTest.serviceQuote.Id;
                quoteProductPricing3.Name= 'TEST';
                quoteProductPricing3.Doc_Number__c = 3;
                quoteProductPricing3.Entitle_price_in_USD__c = 456;
                quoteProductPricing3.Year1__c = 123;
                quoteProductPricings.add(quoteProductPricing1);
                quoteProductPricings.add(quoteProductPricing2);
                quoteProductPricings.add(quoteProductPricing3);
            }
            counter++;
        }
        
        insert quoteProductPricings;
        
        PageReference bookingReport = Page.SR_BookingReportXls;
        Test.setCurrentPage(bookingReport);
        ApexPages.currentPage().getParameters().put('id',Sr_PrepareOrderControllerTest.serviceQuote.Id);
        
        SR_ServiceBookingReport serviceReportController = new SR_ServiceBookingReport();
        serviceReportController.getServiceBookingReportData();
        
        System.assertEquals(2,serviceReportController.slaPartsMap.size());
        System.assertEquals(1,serviceReportController.subProductsMap.size());
        System.assertEquals(1,serviceReportController.subProductsMap.get(1).size());
        //System.assertEquals(2,serviceReportController.productEntitlementsMap.size());
    }
}
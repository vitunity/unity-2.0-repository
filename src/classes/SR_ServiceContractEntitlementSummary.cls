/***********************************************
    Author      :   Nikita Gupta
    Createn On  :   5/28/2014
    Created for :   US3298, to display Service/Maintenance Contract record details as PDF
***********************************************/


public class SR_ServiceContractEntitlementSummary 
{
    Public List<SVMXC__Service_Contract_Products__c> listOfCoveredProduct{get;set;}
    Public List<SVMXC__Service_Contract_Services__c> listOfIncludedServices{get;set;}
    Public List<SVMXC__Service_Contract_Sites__c> listOfCoveredLocations{get;set;}
    Public List<SVMXC__PM_Offering__c> listOfPmOfferings{get;set;}
    Public Id serviceContractId;
    Public double sumOfISLinePrice {get; set;}
    Public double sumOfPMLinePrice {get; set;}
   
     
    public SR_ServiceContractEntitlementSummary(ApexPages.StandardController controller) 
    {
    try
    {
        sumOfISLinePrice = 0;
        sumOfPMLinePrice = 0;
        serviceContractId = ApexPages.currentPage().getParameters().get('id');
        listOfCoveredProduct = new list<SVMXC__Service_Contract_Products__c>([SELECT Name,SVMXC__End_Date__c,SVMXC__Installed_Product__c, SVMXC__Installed_Product__r.name,SVMXC__Line_Price__c,SVMXC__Product_Family__c,SVMXC__Product_Line__c,SVMXC__Product__c, SVMXC__Product__r.name, SVMXC__Start_Date__c FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__c = :serviceContractId]);
        listOfIncludedServices = new list<SVMXC__Service_Contract_Services__c>([SELECT Name,SVMXC__Consumed_Units__c,SVMXC__Expense_Discount_Covered__c,SVMXC__Included_Units__c,SVMXC__Is_Billable__c,SVMXC__Labor_Discount_Covered__c,SVMXC__Line_Price__c,SVMXC__Parts_Discount_Covered__c,SVMXC__Service_Type__c,SVMXC__Travel_Discount_Covered__c FROM SVMXC__Service_Contract_Services__c WHERE SVMXC__Service_Contract__c = :serviceContractId]);
        listOfCoveredLocations = new list<SVMXC__Service_Contract_Sites__c>([SELECT Name,SVMXC__Site__r.Name FROM SVMXC__Service_Contract_Sites__c WHERE SVMXC__Service_Contract__c = :serviceContractId]);
        listOfPmOfferings = new list<SVMXC__PM_Offering__c>([SELECT Name,SVMXC__Is_Billable__c,SVMXC__Line_Price__c,SVMXC__PM_Plan_Template__c,SVMXC__Service_Plan__c FROM SVMXC__PM_Offering__c WHERE SVMXC__Service_Contract__c = :serviceContractId]);
       
        for(SVMXC__Service_Contract_Services__c varLinePrice : listOfIncludedServices )
        {
            sumOfISLinePrice = sumOfISLinePrice + varLinePrice.SVMXC__Line_Price__c;
        }
        
         for(SVMXC__PM_Offering__c varPMoffer : listOfPmOfferings )
        {
            sumOfPMLinePrice = sumOfPMLinePrice + varPMoffer .SVMXC__Line_Price__c;
        }
    }
    
    catch(Exception e)
    {
        ApexPages.addMessages(e);
        
    }
    
    }

}
/*************************************************************************\
      @ Author                : Chiranjeeb Dhar
      @ Date                  : 25-Nov-2013
      @ Description           : This Class creates a new case when an Email is sent from Smart Connect.
      @ Last Modified By      :     
      @ Last Modified On      :     
      @ Last Modified Reason  :     
****************************************************************************/
global class SR_SmartAlertCaseCreation implements Messaging.InboundEmailHandler 
{
    // The default from email address is storied in Custom label SR_SmartConnectCaseCreationFromAddress
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        try{            
            Boolean newFormat = false;
            Boolean offline = false;
            Integer priorityNum = 0;
            String SNStr = '';
            String SNContactId = '';
            if(email.subject.contains('P=') && email.subject.contains('SN='))
            {
                Integer pIndex = email.subject.indexOf('P=') + 2;
                priorityNum = Integer.valueOf(email.subject.substring(pIndex, pIndex+1));
                Integer sIndex = email.subject.indexOf('SN=') + 3;
                SNStr = email.subject.substring(sIndex, sIndex+7);
                String soqlStr = 'select Id, FirstName from Contact where FirstName LIKE \'%' + SNStr + '%\'';
                for(sObject s : Database.query(soqlStr))
                {
                    Contact con = (Contact)s;
                    SNContactId = con.Id;
                }
                newFormat = true;
            }
            if(email.subject.contains('offline'))
            {
                offline = true;
            }
            if(email.subject.contains('SmartConnect AutoNotification') || newFormat)
            {
                
                Id CARecordtype = [Select id from recordtype where sObjecttype = 'Case' and developername = 'CA_Customer_Advocacy'].id;
                String lstSubjectForReplace = email.subject;
                String lstSubject = lstSubjectForReplace.unescapeXml();//Kaushiki 27/3/15;
                System.debug('lstSubject +++'+lstSubject );
                List<String> splitString = new List<String>();
                splitString =lstSubject.split(' ');
                System.debug('splitString +++'+splitString );
                String WOId;
                
                List<SVMXC__Installed_Product__c> ListofInstalledProduct;
                    
                string emailsubject = (email.subject).unescapeXml();
                string splitStr;
                emailsubject = emailsubject.ToUpperCase();
                if(emailsubject.contains('SN='))splitStr = 'SN=';
                if(emailsubject.contains('SN ='))splitStr = 'SN =';
                if(splitStr <> null){
                    string s = emailsubject.substring(emailsubject.indexOf(splitStr)+splitStr.length());
                    boolean isfound = false;
                    string proId = '';
                    for(integer i=1;i<s.length();i++){
                        string schar = s.substring(i-1,i);
                        if(schar == ' ' && isfound){
                            break;
                        }else{
                            if(schar != ' ') {
                            isfound = true;
                            proId += schar;
                            }
                        }
                       
                    }
                    proId = proId.trim();
                    if(proId.contains(','))proId = proId.split(',')[0];
                    if(proId.contains(';'))proId = proId.split(';')[0];
                    ListofInstalledProduct=[Select Id,SVMXC__Contact__c, SVMXC__Contact__r.Phone, SVMXC__Company__c,SVMXC__Company__r.RecordType.DeveloperName,SVMXC__Preferred_Technician__c from SVMXC__Installed_Product__c where name=:proId];
                }
                
                RecordType [] recType  = [ Select Id, Name from RecordType where SobjectType = 'Case'  and  Name ='CA'];            
                String lstDescription = email.plainTextBody; 
                Case casealias = new Case();
                casealias.recordtypeId = CARecordtype;
                casealias.Origin = 'Machine';
                casealias.local_Subject__c = lstSubject;
             
                if(ListofInstalledProduct <> null){
                    for(SVMXC__Installed_Product__c IP : ListofInstalledProduct )
                    {
                        if(IP.SVMXC__Company__r.RecordType.DeveloperName=='Site_Partner')
                        {
                            system.debug('asdf1'+IP);
                            casealias.AccountId=IP.SVMXC__Company__c;
                            casealias.ContactId = IP.SVMXC__Contact__c; // fixed for custom validation
                        }
                        casealias.ProductSystem__c=IP.Id; //DE3449
                    } 
                }           
                if(String.isNotBlank(SNContactId))
                {
                    casealias.ContactId = SNContactId;
                }
                
                if(recType.size() > 0)
                {
                    casealias.RecordTypeId = recType[0].Id;
                }
                casealias.Local_Description__c = lstDescription.unescapeXml();
                casealias.Priority='Emergency';
                if(priorityNum >= 1 && priorityNum <= 6)
                {
                    casealias.Priority = 'Emergency';
                }
                if(priorityNum > 6)
                {
                    casealias.Priority = 'Low';
                }
                if(offline)
                {
                    casealias.Priority = 'Low';
                }
                casealias.Is_This_a_Complaint__c='Yes';
                casealias.Is_escalation_to_the_CLT_required__c = 'No';
                casealias.Was_anyone_injured__c = 'No';        
                casealias.Smart_Connect_Case__c=True;
                
                // For assignment rule execution
                AssignmentRule AR = new AssignmentRule();
                AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];              
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.assignmentRuleId= AR.id;
                casealias.setoptions(dmo);            
                Insert casealias;
            }
        }
        catch(Exception e){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {system.Label.SmartAlertExceptionEmail};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Smart Alert Failure for : '+ email.subject);
            mail.setHTMLBody('failure reason : '+e.getMessage() +'.<br/>From Address : '+email.fromAddress+'<br/>Email body : '+email.plainTextBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        return result;
    }
}
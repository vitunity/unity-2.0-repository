@isTest
public class SR_SmartAlertCaseCreation_test 
{

    public static testMethod void testSR_SmartAlertCaseCreation() {
        //Dummy data crreation 
      Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
       RecordType rt = [SELECT id,developername FROM recordtype where developername='Site_Partner' and SobjectType = 'Account'];

            // insertAccount
        Account testAcc= new Account(recordTypeid = rt.id, ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
 
            insert testAcc;
            system.debug('testAcc-----'+testAcc);
            Contact testcontact = SR_testdata.createContact();
            testcontact.AccountId = testAcc.id;
            testcontact.FirstName = 'TestAprRel1FN';
            testcontact.LastName = 'TestAprRel1';
            testcontact.Email = 'Test@1234APR.com';
            testcontact.MailingCity='New York';
            testcontact.MailingCountry='US';
            testcontact.MailingPostalCode='552601';
            testcontact.MailingState='CA'; 
            testcontact.Phone = '123567856';             
            insert testcontact;

            SVMXC__Installed_Product__c testInstallPrd = new SVMXC__Installed_Product__c ();
            testInstallPrd.Name = 'H11111';
            testInstallPrd.SVMXC__Company__c = testAcc.id;
            testInstallPrd.SVMXC__Contact__c = testcontact.id;
            //testInstallPrd.SVMXC__Company__r.RecordType.DeveloperName = rt.developername;
            insert testInstallPrd;
            system.debug('23456'+ testInstallPrd.SVMXC__Company__r.RecordType.DeveloperName);
            // for testing email inbound handerler
           
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();  
             string strValue = string.valueOf(testInstallPrd.Name);         
             email.subject =  '5555Re: 123Re: P=1, SN=H349999 NDS-SVC-xxxx - SmartConnect AutoNotification';
             email.fromAddress = 'SmartConnect@varian.com';
              email.plainTextBody = 'asdfghjkl';
              try{
            SR_SmartAlertCaseCreation testEmailServices = new SR_SmartAlertCaseCreation();
            testEmailServices.handleInboundEmail(email,env); 
                }catch(exception e)
                {}          
        }
      }
}
/**************************************************************************\
@Last Modified Date- NA
@Last Modified By - NA 
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
04-Oct-2017 - Rakesh - STSK0012631 - Reason for Billing Review field required when Billing Review Requested is True.(Line 394 - 402 )

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
4 - Apr-2018 - Nilesh Gorle - STSK0014169 - Check end date crossed another day using start date and time spent in hrs
20 - Nov-2017 - Chandra Mouli - STSK0012982 :Assigning Helpdesk case hours to project (APAC priority list #31)
5 - OCT-2017 - Puneet Mishra - STSK0012982- Assigning Helpdesk case hours to project

/**************************************************************************/
public with sharing class SR_Start_End_CaseLine_Extension
{
    public static final string strBillingType_Contract = 'C – Contract';    //added by Parul
    public SVMXC__Case_Line__c newcaseline {get;set;}
    public BillingTypeOptions billingType {get;set;}
    public string hoursspent{get;set;}
    public string minutesspent{get;set;}
    public Case casealias{get;set;}
    public String erpcontracttype{get;set;}
    public String erpprojectnbr{get;set;}
    public String varInstalledProduct{get;set;}
    public boolean errorflag{get;set;}
    public boolean IsClosed{get;set;}
    public id profileid;
    public string profileName;
    public Case parentcase;
    public ERP_NWA__c erpnwa;
    Id caseid;

    /* STSK0014169 - Flag to check end date goes into another day. if true then show msg */
    public boolean showMsg{get;set;}
    public boolean isExecute{get;set;}
    
    public String installProdId{get;set;}// STSK0012982 : Store Case Installed Product Id
    public Boolean disableProjectId {get;set;}// STSK0012982 : Flag to disable Lookup field
    private List<String> invalidStatus = new List<String>{'Error', 'Cancelled', 'Closed'}; // STSK0012982 : Invalid status for WO
    
    /***************************************************************************
    Description: 
    STSK0012982 : This method create Case Line Billing Type based on Case Billing Type value
    *************************************************************************************/
    public class BillingTypeOptions {
        
        public List<SelectOption> billType { get; set; }
        
        public BillingTypeOptions(Case parentcase) {
            Map<String, List<String>> billingoptionsMap = this.getCaseLineOptions();
            this.billType = new List<SelectOption>();
            if(billingoptionsMap.containsKey(parentcase.SVMXC__Billing_Type__c) ){
                //&& (parentcase.SVMXC__Billing_Type__c != '' || String.isBlank(parentcase.SVMXC__Billing_Type__c)) ) {
                for(String options : billingoptionsMap.get(parentcase.SVMXC__Billing_Type__c)) {
                    this.billType.add(new SelectOption(options, options));
                }
            } else {
                for(String options : billingoptionsMap.get('Others')) {
                    this.billType = new List<SelectOption>();
                    this.billType.add(new SelectOption(options, options));
                }
            }
        }
        
        /***************************************************************************
            Description: 
            STSK0012982 : method fetch Billling options from custom setting and split using delimiter ';'
        *************************************************************************************/
        
        public Map<String, List<String>> getCaseLineOptions() {
            List<String> billingOptions = new List<String>();
            Map<String, List<String>> billingoptionsMap = new Map<String, List<String>>();
            
            for(Billing_Types__c billing : Billing_Types__c.getall().values()) {
                if(!billingoptionsMap.containsKey(billing.Name)) {
                    billingoptions = billing.Caseline_Billing_Types__c.split(';');
                    billingoptionsMap.put(billing.Name, billingoptions);
                } 
            }
            return billingoptionsMap;
        }
        
    }
    // public static Id recTypeTimeEntry = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Time Entry').getRecordTypeId(); 
    
    public static Id recTypeTimeEntry = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Case_Line__c').get('Time_Entry');
    public static Id recTypeHelpdesk = RecordTypeUtility.getInstance().rtMaps.get('Case').get('Helpdesk');
    
    public SR_Start_End_CaseLine_Extension(ApexPages.StandardController stdController)
    {
        errorflag = false;
        IsClosed = false;
        showMsg = false;
        isExecute = true;
        newcaseline = new SVMXC__Case_Line__c();
        casealias = new Case();
        newcaseline.Start_Date_time__c = system.now();
        newcaseline.Case_Line_Owner__c = userinfo.getuserid();
        //caseid = stdController.getId();
        caseid = ApexPages.currentPage().getParameters().get('id');
        if(caseid  != Null)
        {
            newcaseline.SVMXC__Case__c = caseid ;
            
            parentcase = [SELECT id,SVMXC__Top_Level__c,Customer_Malfunction_Start_Date_time_Str__c,SVMXC__Top_Level__r.Name, 
            Priority,Status, SVMXC__Top_Level__r.PCSN__c, SVMXC__Service_Contract__r.ERP_Contract_Type__c,SVMXC__SLA_Terms__r.Name,
            Not_Varian_s_Fault__c, SVMXC__Billing_Type__c, ProductSystem__c,ProductSystem__r.Name, ProductSystem__r.PCSN__c,P_O_Number__c, 
            Malfunction_Start__c, ProductSystem__r.SVMXC__Status__c, RecordTypeID From Case where id =: caseid]; 
            system.debug('SVMXC__Billing_Type__c--->'+ parentcase.SVMXC__Billing_Type__c);
            
            billingType = new BillingTypeOptions(parentcase);
            newcaseline.SVMXC__Installed_Product__c = parentcase.ProductSystem__c; //DE3449
            newcaseline.Billing_Type__c = 
            varInstalledProduct = parentcase.ProductSystem__r.Name; //DE3449
            installProdId = parentcase.ProductSystem__c; // STSK0012982 : assignging Case Install Product Id
            erpcontracttype = parentcase.SVMXC__SLA_Terms__r.Name;
            casealias.P_O_Number__c = parentcase.P_O_Number__c; //DE1687
            profileId=userinfo.getProfileId();
            profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            if((parentcase.status == 'Closed' || parentcase.status == 'Closed by Work Order') && 
                    ( profileName =='VMS Unity 1C - Service User' || profileName == 'VMS Unity 1C - Service Admin' ||
                    profileName == 'VMS Service - User' || profileName == 'VMS Service - Admin' ) )
            {
                IsClosed = true;
            }
        }

        /*if(newcaseline.Billing_Type__c == null) //de4332
        {
            if(parentcase != null && parentcase.SVMXC__Service_Contract__r.ERP_Contract_Type__c == 'ZH3')
               newcaseline.Billing_Type__c = 'W - Warranty';
            else
               newcaseline.Billing_Type__c = 'C - Contract';
        }*/

        if(parentcase.SVMXC__Billing_Type__c != null && parentcase.SVMXC__Billing_Type__c != '') {
            newcaseline.Billing_Type__c = parentcase.SVMXC__Billing_Type__c; //DE2961, Nikita 2/3/2015  
        } else {
            newcaseline.Billing_Type__c = strBillingType_Contract; //changed from picklist value 'C - Contract' to public static final variable, Parul, 4/16/15
        }
        
        // STSK0012982 : Disbale/Enable lookup field if Billing type is Installation on page load
        if(parentcase.SVMXC__Billing_Type__c == Label.I_Installation)
            disableProjectId = false ;
        else 
            disableProjectId = true;
        
    }
    
    
    /***************************************************************************
    Description: 
    STSK0012982 : Disbale/Enable lookup field if Billing type on page changes
    *************************************************************************************/
    
    public PageReference resetdisableProjectId() {
        
        if(newcaseline.Billing_Type__c == Label.I_Installation) {
            disableProjectId = false ;
        } else {
            disableProjectId = true ;
            newcaseline.ERP_Project__c= null;
            //caseLinebillingType = '';
        }
        return null;
    }

    public List<SelectOption> getpicklistoptionshours()
    { 
        List<SelectOption> options = new List<SelectOption>();
        Integer hours = 1;
        options.add(new selectoption('0','0'));
        While(hours < 24)
        {
            options.add(new selectoption(String.valueof(hours),String.valueof(hours)));
            hours++;
        }
        //hoursspent = '0';
        return options;
    }
    public List<SelectOption> getpicklistoptionsminutes()
    { 
        List<SelectOption> options = new List<SelectOption>();
        Decimal minutes = 0.00;
        While(minutes <= 0.55)
        {
           options.add(new selectoption(String.valueof(minutes) ,String.valueof(minutes) ));
           minutes = minutes + 0.05;
        }
        return options;
    }
   
    /** 1C Sunil - 23 Jan 15  
    public void installationcheck()
    {
        List<SVMXC__Service_Order_Line__c> wd = [SELECT ERP_NWA__c,SVMXC__Service_Order__r.ERP_Project_Nbr__c From SVMXC__Service_Order_Line__c 
                                             WHERE  SVMXC__Serial_Number__c = :parentcase.ProductSystem__c 
                                             AND  SVMXC__Service_Order__r.recordtype.Name = 'Installation' 
                                             AND  SVMXC__Service_Order__r.SVMXC__Order_Status__c = 'Open' AND ERP_NWA__c != Null ];  
        if(wd != Null && wd.size() > 0)
        { 
            if(wd[0].SVMXC__Service_Order__r.ERP_Project_Nbr__c != null)
                erpprojectnbr = wd[0].SVMXC__Service_Order__r.ERP_Project_Nbr__c;
            else
                erpprojectnbr = 'NA';
        }
        else
        { 
            erpprojectnbr = 'NA';
        }
    }
    1C Sunil - 23 Jan 15  **/

    /* // By Rakesh, 12/30/2015. START
    public string isAlreadyBooked(Datetime sdate,Datetime eDate, SVMXC__Case_Line__c insertcaseline){
        
        string msg;
        List<SVMXC__Case_Line__c> lstExistingCaseLine = [select Id,Name,Case_Line_Owner__r.Name,Start_Date_time__c,End_Date_time__c from SVMXC__Case_Line__c 
        where  Case_Line_Owner__c =: insertcaseline.Case_Line_Owner__c  order by Start_Date_time__c];
        
        if(lstExistingCaseLine <> null){
            for(SVMXC__Case_Line__c cl : lstExistingCaseLine){
            
            String Startformatted = (cl.Start_Date_time__c).format('yyyy MMM dd - HH:mm:ss');
            String Endformatted = (cl.End_Date_time__c).format('yyyy MMM dd - HH:mm:ss');
            
            
                if(sDate >= cl.Start_Date_time__c && sDate <= cl.End_Date_time__c)
                {
                  //  msg = 'Work Detail: '+cl.Name+' already assigned to  '+cl.Case_Line_Owner__r.Name +'-'+Startformatted+' from :'+cl.Start_Date_time__c +' to '+ cl.End_Date_time__c;
                      msg = 'Case Line: '+cl.Name+' already assigned to '   +cl.Case_Line_Owner__r.Name +' from : '+ Startformatted +' to ' + Endformatted;
                    break;
                } //-'+formatted+'
                else if(eDate >= cl.Start_Date_time__c && eDate <= cl.End_Date_time__c)
                {
                  //  msg = 'Work Detail: '+cl.Name+' already assigned to '+Startformatted +'-'+cl.Case_Line_Owner__r.Name+' from :'+cl.Start_Date_time__c +' to '+ cl.End_Date_time__c;
                      msg = 'Case Line: '+cl.Name+' already assigned to '+cl.Start_Date_time__c +' - ' +cl.Case_Line_Owner__r.Name +' from :'+ Startformatted +' to ' + Endformatted;
                    break;
                }
            }
        }
        
        return msg;
    }
    // By Rakesh, 12/30/2015. END  */
    
    
    /*
    //DFCT0012244.start
    public String convertMonthNameToNumber(String strMonthName)
    {
        if(strMonthName == 'Jan')  return '01';
        if(strMonthName == 'Feb') return '02';
        if(strMonthName == 'Mar') return '03';
        if(strMonthName == 'Apr') return '04';
        if(strMonthName == 'May') return '05';
        if(strMonthName == 'Jun') return '06';
        if(strMonthName == 'Jul') return '07';
        if(strMonthName == 'Aug') return '08';
        if(strMonthName == 'Sep') return '09';
        if(strMonthName == 'Oct') return '10';
        if(strMonthName == 'Nov') return '11';
        if(strMonthName == 'Dec') return '12';
        return '01';
        
    }
    public DateTime constructDateTime(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = dateTimeSplitList[0].split('-') ;
        List<String> timeSplit = dateTimeSplitList[1].split(':');
        String strAMPM = dateTimeSplitList[2];
        DateTime constructedDateTime;
        if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
        {
            constructedDateTime = dateTime.NewInstanceGMT(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),00, Integer.valueof(timeSplit[1]), 00);
        }
        else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
        {
            constructedDateTime = dateTime.NewInstanceGMT(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
        }
        else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
        {
            constructedDateTime = dateTime.NewInstanceGMT(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
        }
        
        return constructedDateTime;
    }
    
    /* STSK0014169 - Set isExecute and showMsg to False and call add method */
    public pagereference execute() {
        isExecute = false;
        showMsg = false;
        pagereference pg = Add();
        isExecute = true;
        return pg;
    }
    
    //DFCT0012244.end */
    public pagereference Add() {
        system.debug(' === ' + newcaseline.Billing_Type__c);
        system.debug(' === ' + newcaseline.ERP_Project__c);

        //STSK0012982 :Assigning Helpdesk case hours to project (APAC priority list #31)
        if(parentcase.status == 'Closed') {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Your case is closed. You cannot submit any additional case lines. Please contact CRM help for further assistance.'));
            return null;   
        }
        if(newcaseline.Billing_Type__c.contains(Label.I_Installation) && newcaseline.ERP_NWA__c == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,Label.NetworkActivityRequired));
            return null;
        }
        /* CM : this is no longer needed.
        List<SVMXC__Service_Order__c> pwo = new List<SVMXC__Service_Order__c>();
        pwo = [SELECT Id, SVMXC__Case__c, Event__c, SVMXC__Order_Status__c FROM SVMXC__Service_Order__c WHERE Event__c =: true
                AND SVMXC__Case__c =: newcaseline.SVMXC__Case__c AND SVMXC__Order_Status__c IN: invalidStatus];
        if(!pwo.isEmpty() && newcaseline.Billing_Type__c.contains(Label.I_Installation)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'No Valid PWO Exists'));
            return null;
        }
        */
        if(newcaseline.ERP_NWA__c != null)
        {
            erpnwa = [SELECT Id, Name, Description__c, WBS_Element__c,WBS_Element__r.Sales_Order__c,WBS_Element__r.Sales_Order__r.Name,WBS_Element__r.Sales_Order_Item__c,WBS_Element__r.Sales_Order_Item__r.Name,ERP_Project__c, ERP_Std_Text_Key__c, ERP_Status__c, ERP_Estimated_Work__c from ERP_NWA__c where id = :newcaseline.ERP_NWA__c];
        }
        
        if (erpnwa != null && !erpnwa.ERP_Std_Text_Key__c.containsIgnoreCase('inst') )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Please select a valid INST NWA.'));
            return null;
        }
        //CM : DFCT0011764 : Aug 23 : Recalculated the start date to exclude seconds as this causing issue in overlap code
        Datetime FinalstartDate;
        Integer yr;
        Integer month;
        Integer day;
        Integer hr;
        Integer min;
        yr = newcaseline.Start_Date_time__c.year();
        month = newcaseline.Start_Date_time__c.month();
        day = newcaseline.Start_Date_time__c.day();
        hr = newcaseline.Start_Date_time__c.hour();
        min = newcaseline.Start_Date_time__c.minute();
        FinalstartDate = Datetime.newInstance(yr,month,day,hr,min,0);

        System.Debug('***CM finalstartDate : '+FinalstartDate);
        newcaseline.Start_Date_time__c = FinalstartDate;
        //CM : DFCT0011764 : Aug 23 : End
        SVMXC__Case_Line__c insertcaseline = new SVMXC__Case_Line__c();
        insertcaseline.SVMXC__Line_Status__c = 'Open';
        insertcaseline.RecordTypeId = recTypeTimeEntry; // DE3639
        insertcaseline.SVMXC__Installed_Product__c =  newcaseline.SVMXC__Installed_Product__c;
        //insertcaseline.SVMXC__Description__c = newcaseline.SVMXC__Description__c;
        insertcaseline.ERP_Service_order_nbr__c = newcaseline.ERP_Service_order_nbr__c;
        insertcaseline.Billing_Type__c = newcaseline.Billing_Type__c;
        insertcaseline.Start_Date_time__c = newcaseline.Start_Date_time__c;
        insertcaseline.Case_Line_Owner__c = newcaseline.Case_Line_Owner__c;
        insertcaseline.PO_Number__c = casealias.P_O_Number__c; //DE1687
        insertcaseline.Override_Change_to_Customer__c= newcaseline.Override_Change_to_Customer__c; //DE1687
        insertcaseline.ERP_NWA__c = newcaseline.ERP_NWA__c; // STSK0012982 : mapping ERP Project# to CaseLine
        if (erpnwa != null)
        {
            insertcaseline.ERP_Project__c = erpnwa.ERP_Project__c;
            insertcaseline.ERP_WBS__c = erpnwa.WBS_Element__c;
            insertcaseline.Sales_Order_Item__c = erpnwa.WBS_Element__r.Sales_Order_Item__c;
        }
        //insertcaseline.ERP_Project__c = newcaseline.ERP_Project__c; // STSK0012982 : mapping ERP Project# to CaseLine
        String TechRecTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Group_Members__c').get('Technician');
        List<SVMXC__Service_Group_Members__c> techrelatedtouser = new list<SVMXC__Service_Group_Members__c>();
        techrelatedtouser = [Select id from SVMXC__Service_Group_Members__c where User__c =: newcaseline.Case_Line_Owner__c AND RecordTypeId =: TechRecTypeId ];
        
        if(!TEST.isRunningTest() && parentcase.ProductSystem__r.SVMXC__Status__c <> 'Installed' && parentcase.ProductSystem__r.SVMXC__Status__c <> 'Installation' && parentcase.RecordTypeId == recTypeHelpdesk )
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Installed Product Is Not Eligible For Use.'));
            return null;
        }

        if(techrelatedtouser == null || techrelatedtouser.size() == 0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Selected Employee does not have any techinician associated'));
            return null;
        }
        system.debug('@@@@@@insertcaseline.Start_Date_time__c'+insertcaseline.Start_Date_time__c);
        //CM : DFCT0011764 : Aug 22 : modifed the logic to calculate the end time to fix overlap issue
        Decimal decmins;
        Decimal baseval;
        Integer finalmins;
        Integer hrs;
        Datetime hrsbeforemins;
        if(hoursspent == '0')
        {
            decmins = Decimal.valueof(minutesspent);
            baseval = (decmins * 100);
            finalmins = baseval.intValue();
            //insertcaseline.Hours__c = (Integer.valueof(minutesspent);
            system.debug('@@@@@@ mins'+finalmins);
            insertcaseline.End_date_time__c = insertcaseline.Start_Date_time__c.addMinutes(finalmins);
            system.debug('@@@@@@insertcaseline.End_date_time__c'+insertcaseline.End_date_time__c);        
        }
        else
        {
            System.Debug('***CM minutesspent: '+minutesspent);
            hrs = Integer.valueof(hoursspent);
            hrsbeforemins = insertcaseline.Start_Date_time__c.addHours(hrs);
            System.debug('***CM hrsbeforemins : '+hrsbeforemins);
            if (minutesspent != '0.00'){
                decmins = Decimal.valueof(minutesspent);
                baseval = (decmins * 100);
                finalmins = baseval.intValue();
                //insertcaseline.Hours__c = (Integer.valueof(minutesspent);
                system.debug('@@@@@@ mins'+finalmins);
                insertcaseline.End_date_time__c = hrsbeforemins.addMinutes(finalmins);
            }
            else
            {
                insertcaseline.End_date_time__c = hrsbeforemins;
            }
            system.debug('@@@@@@insertcaseline.End_date_time__c'+insertcaseline.End_date_time__c);      
            //insertcaseline.Hours__c = Integer.valueof(hoursspent) + ((Integer.valueof(minutesspent) )*100)/60;
            //system.debug('@@@@@@Decimal.valueof(hoursspent) + ((Decimal.valueof(minutesspent) )*100)/60'+(Decimal.valueof(hoursspent) + ((Decimal.valueof(minutesspent) )*100)/60));
            //system.debug('@@@@@@insertcaseline.Hours__c'+insertcaseline.Hours__c);
        }

        /* Start - STSK0014169 - Flag to check end date goes into another day. if true then show msg */
        System.debug('=====isExecute======='+isExecute);
        System.debug('=====Start date======='+insertcaseline.Start_Date_time__c);
        System.debug('=====End date======='+insertcaseline.End_date_time__c);

        TimeZone tz = UserInfo.getTimeZone();
        string sDateTimeStr = insertcaseline.Start_Date_time__c.format('yyyy-MM-dd HH:mm:ss',  tz.toString());
        System.debug('=====Local Start date======='+sDateTimeStr );
        string eDateTimeStr = insertcaseline.End_date_time__c.format('yyyy-MM-dd HH:mm:ss',  tz.toString());
        System.debug('=====Local End date======='+eDateTimeStr );
        Datetime sDateTime = DateTime.valueOf(sDateTimeStr);
        Datetime eDateTime = DateTime.valueOf(eDateTimeStr);
        Date sDate = sDateTime.date();
        Date eDate = eDateTime.date();
        System.debug('=====Date Difference======='+(sDate<eDate));
        boolean isSameDay = sDateTime.isSameDay(eDateTime);
        System.debug('=====Same Day======='+isSameDay);
        if(isExecute && (!isSameDay || (sDate<eDate))) {
            showMsg = true;
            return null;
        }
        /* End - STSK0014169 - Flag to check end date goes into another day. if true then show msg */

        //insertcaseline.End_date_time__c = insertcaseline.Start_Date_time__c + (insertcaseline.Hours__c)/24;
        //system.debug('@@@@@@insertcaseline.Start_Date_time__c + (insertcaseline.Hours__c)/24'+(insertcaseline.Start_Date_time__c + (insertcaseline.Hours__c)/24));
        //system.debug('@@@@@@(insertcaseline.Hours__c)/24'+(insertcaseline.Hours__c)/24);
        //system.debug('@@@@@@insertcaseline.End_date_time__c'+insertcaseline.End_date_time__c);
        //CM : DFCT0011764 : End of changes
        insertcaseline.SVMXC__Case__c = caseid;
        /*if(insertcaseline.Billing_Type__c != 'I - Installation' && insertcaseline.Billing_Type__c != 'O - Other' && insertcaseline.Billing_Type__c != 'C - Contract')
        {
            errorflag = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select Billing Type.'));
            return null;
        }*/

          /*  //DFCT0012244.start:datetime compare
        datetime CLdt = dateTime.NewInstanceGMT(insertcaseline.Start_Date_time__c.year(),insertcaseline.Start_Date_time__c.month(),insertcaseline.Start_Date_time__c.day(),insertcaseline.Start_Date_time__c.hour(),insertcaseline.Start_Date_time__c.minute(),insertcaseline.Start_Date_time__c.second());
        datetime CMdt = constructDateTime(parentcase.Customer_Malfunction_Start_Date_time_Str__c);
        
        if( CLdt < CMdt) //DE3683 
        {
           
                errorflag = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Start Date Time cannot be less than Malfunction Start Time.'));
                return null;
            
        }
        //DFCT0012244.end  */
        
        //STSK0012631 - Start     
        insertcaseline.Billing_Review_Requested__c = newcaseline.Billing_Review_Requested__c;
        insertcaseline.Reason_for_Billing_Review__c = newcaseline.Reason_for_Billing_Review__c;         
        if(newcaseline.Billing_Review_Requested__c && newcaseline.Reason_for_Billing_Review__c == null){      
            errorflag = true;       
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Reason for Billing Review is required.'));       
            return null;        
        }       
        //STSK0012631 - End.
        
        //DFCT0012244.start
        if(insertcaseline.Start_Date_time__c < parentcase.Malfunction_Start__c) //DE3683 
        {
            errorflag = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Start Date Time cannot be less than Malfunction Start Time.'));
            return null;
        }
        //DFCT0012244.end
        if(insertcaseline.Billing_Type__c != null && insertcaseline.Billing_Type__c.substring(0,1) == 'I') // DE4371 (substring fix)
        {
            /** 1C Sunil - 23 Jan 15  
            List<SVMXC__Service_Order_Line__c> wd = [SELECT ERP_NWA__c,SVMXC__Service_Order__r.ERP_Project_Nbr__c From SVMXC__Service_Order_Line__c 
                                             WHERE  SVMXC__Serial_Number__c = :parentcase.ProductSystem__c 
                                             AND  SVMXC__Service_Order__r.recordtype.Name = 'Installation' 
                                             AND  SVMXC__Service_Order__r.SVMXC__Order_Status__c = 'Open' AND ERP_NWA__c != Null ];  
            if(wd != Null && wd.size() > 0)
            { 
                insertcaseline.ERP_NWA__c = wd[0].ERP_NWA__c;
            }
            1C Sunil - 23 Jan 15  **/
        }
        
        else if(insertcaseline.Billing_Type__c != null && insertcaseline.Billing_Type__c.substring(0,1) == 'O') // DE4371 (substring fix)
        {
            if(insertcaseline.ERP_Service_order_nbr__c == Null)
            {
                errorflag = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'ERP Service Order Nbr is required.'));
                return null;
            }
        }
        else if(insertcaseline.Billing_Type__c != null && insertcaseline.Billing_Type__c.substring(0,1) == 'P') // DE4371 (substring fix)
        {
            if(insertcaseline.PO_Number__c == Null)
            {
                errorflag = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'PO Number is required.'));
                return null;
            }
        }
        
        else if(parentcase.Priority == 'None')  { errorflag = true;     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Priority on Case cannot be None, Please change the Priority'));     return null;  }
        
        /* // By Rakesh, 12/29/2015. START
        string isAlreadyBook = isAlreadyBooked(insertcaseline.Start_Date_time__c,insertcaseline.End_Date_time__c,insertcaseline);
        if( isAlreadyBook <> null){
            errorflag = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,isAlreadyBook));
            return null;  
        }
        // By Rakesh, 12/29/2015. END  */
        try
        {
            system.debug('@@@@@@insertcaseline'+insertcaseline);
            if(insertcaseline.Billing_Type__c == 'I – Installation'){
                insertcaseline.Case_Line_WO_Type__c = 'Installation';
            }else{
                insertcaseline.Case_Line_WO_Type__c = 'Helpdesk';
            }
            insert insertcaseline;
            system.debug('@@@@@@insertcaseline'+insertcaseline);
            errorflag = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Case Line Created Successfully.'));
            //hoursspent = '0';
            //minutesspent = '0.00';
            //newcaseline.Start_Date_time__c = system.now();
            // newcaseline.Case_Line_Owner__c = userinfo.getuserid();
            // newcaseline.ERP_Service_order_nbr__c = null;
            // PageReference refresh = new PageReference('/apex/SR_Start_End_CaseLine');
            //refresh.setRedirect(true);
            // return refresh;  
            return null; 
        }
        catch(DmlException e) 
        {
            //de3367 
            String errMsg = e.getMessage(); 
            if(errMsg.Contains('Please enter Time Spent(in hours)  to save the Case Line.'))  {errorflag = false;  }
            else if(errMsg.Contains('Priority cannot be None')) { errorflag = false;   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Priority on Case cannot be None, Please change the Priority')); }
            else if(errMsg.Contains('Contact Phone on Case cannot be blank')) { errorflag = false;ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Contact Phone on Case cannot be blank.'));  }
           // else if(errMsg.Contains('First exception on row 0; first error: FIELD_CUS')) {errorflag = false;ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,'Time overlap in case line'));}
            else if(errMsg.Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {errorflag = false;}
            else  {  errorflag = true;     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The following exception has occurred: ' + errMsg+'-'+e.getLineNumber()+'-'+e.getTypeName()+'-'+e.getCause()));     }
            
            return null;
        }
    }   
}
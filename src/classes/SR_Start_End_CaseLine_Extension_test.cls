@isTest//(SeeAllData=true)
public class SR_Start_End_CaseLine_Extension_test
{

    public static Id recTypeIDTechnician = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
    public static id recHD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
    //static profile pr = [Select id from Profile where name = 'VMS Service - User'];
    //public static User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
    public static ERP_Timezone__c etz ;
    public static Account acc ;
    public static SVMXC__Site__c varLoc;
    public static Contact con ;
    public static SVMXC__Service_Group__c objServTeam;
    public static SVMXC__Service_Group_Members__c techEqipt,techEqipt2 ;
    public static SVMXC__Installed_Product__c objIP, objIP2 ;
    public static Case testcase, testcase2;
    
    static
    {
        
        etz = new ERP_Timezone__c(Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)', name='Aussa');
        insert etz;
        
        // insertAccount
        acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        acc.BillingCity = 'Anytown';
        acc.State_Province_Is_Not_Applicable__c=true;
        insert acc;
        
        //insert location
        varLoc = new SVMXC__Site__c(SVMXC__Account__c = acc.id, Plant__c = 'testPlant', SVMXC__Service_Engineer__c = userInfo.getUserID());
        insert varLoc;

        // insert Contact  
        con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1235678');
        insert con; 
        
        //insert Service Team
        objServTeam = new SVMXC__Service_Group__c(Name = 'test team' , District_Manager__c = userinfo.getUserID());
        insert objServTeam;
        
        //insert technician
        techEqipt = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
        Name = 'Test Technician', User__c = userInfo.getUserId(),ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt;
        
        //insert technician1
        techEqipt2 = new SVMXC__Service_Group_Members__c(RecordTypeId = recTypeIDTechnician,
        Name = 'Test Technician2', User__c = userinfo.getUserID(), ERP_Timezone__c = 'Aussa',SVMXC__Service_Group__c = objServTeam.Id);
        insert techEqipt2;
        System.debug('techEqipt2----->'+techEqipt2);
        
        // insert parent IP var loc
        objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Site__c =varLoc.id, SVMXC__Preferred_Technician__c = techEqipt.id,
        Service_Team__c =objServTeam.Id );
        insert objIP;
        
         // insert parent IP var loc
        objIP2 = new SVMXC__Installed_Product__c(Name='H134072', SVMXC__Status__c ='Installed', SVMXC__Preferred_Technician__c = techEqipt2.id,
        Service_Team__c =objServTeam.Id );
        insert objIP2;
            
        // insert Case
        testcase = new case(recordtypeId= recHD, AccountId =acc.id, Priority='Medium', SVMXC__Top_Level__c = objIP.id,contactId = con.id, SVMXC__Billing_Type__c = 'P - Paid Service');
        insert testcase;
        // insert Case2
        testcase2 = new case(recordtypeId= recHD, AccountId =acc.id, Priority='Medium', SVMXC__Top_Level__c = objIP2.id, contactId = con.id);
        insert testcase2;
        
        Billing_Types__c bt = new Billing_Types__c(name='Others', Caseline_Billing_Types__c ='W – Warranty;P – Paid Service;I – Installation;N – Waived;X – Cross;C – Contract;O - Other');
        insert bt;

    }
    public static testMethod void testSR_Start_End_CaseLine_Extension() // covering functionality when Selected Employee does not have any techinician associated
    { 
        user u = new user();
        u.id = userinfo.getUserID();
        system.runas(u)
        {
            PageReference myVfPage = Page.SR_Start_End_CaseLine;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id',testcase2.id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testcase2);
            SR_Start_End_CaseLine_Extension controller = new SR_Start_End_CaseLine_Extension(stdController);
                
                controller.getpicklistoptionshours();
                controller.getpicklistoptionsminutes();
                controller.minutesspent = '0.15';
                controller.hoursspent = '1';
                controller.Add();
                //controller.newcaseline = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl', SVMXC__Case__c = testcase.id, Case_Line_Owner__c = systemuser.id, Start_Date_time__c = system.now());
                //controller.newcaseline.Billing_Type__c = 'O - Other';          
                //controller.newcaseline();
        }
        
    }
    
    public static testMethod void testSR_Start_End_CaseLine_ExtensionWithTech()
    { 
       
            
            PageReference myVfPage = Page.SR_Start_End_CaseLine;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id',testcase.id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testcase);
            SR_Start_End_CaseLine_Extension controller = new SR_Start_End_CaseLine_Extension(stdController);
                
                test.startTest();
                controller.getpicklistoptionshours();
                controller.getpicklistoptionsminutes();
                controller.minutesspent = '0.15';
                controller.hoursspent = '1';
                controller.Add();
                test.stopTest();

    }
    
    public static testMethod void testSR_Start_End_CaseLine_ExtensionWithOther() // billing type as o other
    { 
       
            
            PageReference myVfPage = Page.SR_Start_End_CaseLine;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id',testcase.id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testcase);
            SR_Start_End_CaseLine_Extension controller = new SR_Start_End_CaseLine_Extension(stdController);
                
                test.startTest();
                controller.getpicklistoptionshours();
                controller.getpicklistoptionsminutes();
                controller.minutesspent = '0.15';
                controller.hoursspent = '0';
                controller.newcaseline.Billing_Type__c = 'O - Other';
                controller.Add();
                test.stopTest();
                //controller.newcaseline = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl', SVMXC__Case__c = testcase.id, Case_Line_Owner__c = systemuser.id, Start_Date_time__c = system.now());
                //controller.newcaseline.Billing_Type__c = 'O - Other';          
                //controller.newcaseline();

    }
    public static testMethod void testSR_Start_End_CaseLine_ExtensionInsert() // insert case line
    { 
       
            
        PageReference myVfPage = Page.SR_Start_End_CaseLine;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id',testcase.id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testcase);
            SR_Start_End_CaseLine_Extension controller = new SR_Start_End_CaseLine_Extension(stdController);

            test.startTest();
            controller.getpicklistoptionshours();
            controller.getpicklistoptionsminutes();
            controller.minutesspent = '0.15';
            controller.hoursspent = '1';
            controller.newcaseline.Billing_Type__c = 'C – Contract';
            controller.newcaseline.Start_Date_time__c = system.now().addDays(10);
            controller.Add();
            test.stopTest();
            //controller.newcaseline = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl', SVMXC__Case__c = testcase.id, Case_Line_Owner__c = systemuser.id, Start_Date_time__c = system.now());
            //controller.newcaseline.Billing_Type__c = 'O - Other';          
            //controller.newcaseline();

    }
    
    public static testMethod void testSR_Start_End_CaseLine_Default() // insert case line
    {      
        PageReference myVfPage = Page.SR_Start_End_CaseLine;
            Test.setCurrentPage(myVfPage);

			
			
            ApexPages.currentPage().getParameters().put('id',testcase.id);
            ApexPages.StandardController stdController = new ApexPages.StandardController(testcase);
            SR_Start_End_CaseLine_Extension controller = new SR_Start_End_CaseLine_Extension(stdController);
			controller.isExecute = true;
        	controller.showMsg = false;
            test.startTest();
            controller.getpicklistoptionshours();
            controller.getpicklistoptionsminutes();
            controller.minutesspent = '0.15';
            controller.hoursspent = '1';
            controller.newcaseline.Billing_Type__c = 'C – Contract';
            controller.newcaseline.Start_Date_time__c = system.now().addDays(10);
            controller.Add();
        	controller.execute();
        
        	controller.newcaseline.Start_Date_time__c = system.now();
        	controller.hoursspent = '23';
        	controller.Add();
            test.stopTest();
            //controller.newcaseline = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl', SVMXC__Case__c = testcase.id, Case_Line_Owner__c = systemuser.id, Start_Date_time__c = system.now());
            //controller.newcaseline.Billing_Type__c = 'O - Other';          
            //controller.newcaseline();

    }
}
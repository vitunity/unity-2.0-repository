public class SR_SuccessEmailController {
     @future(callout=true)
    public static void sendVF(String pobjQuoteId, String templateName, String userSessionId)
    {
        //Replace below URL with your Salesforce instance host
        String salesforceBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('Salesforce Base URL :: '+salesforceBaseURL );
        String addr = salesforceBaseURL+'/services/apexrest/sendPDFEmail';
        HttpRequest req = new HttpRequest();
        req.setEndpoint( addr );
        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + userSessionId);
        req.setHeader('Content-Type','application/json');
        req.setTimeout(120000);
        Map<String,String> postBody = new Map<String,String>();
        //postBody.put('EmailIdCSV',EmailIdCSV);
        postBody.put('pobjQuoteId',pobjQuoteId);
        postBody.put('emailTemplate',templateName);
	
        String reqBody = JSON.serialize(postBody);

        req.setBody(reqBody);
        Http http = new Http();
        if(!Test.isRunningTest()){
        	HttpResponse response = http.send(req);
        }
    }

}
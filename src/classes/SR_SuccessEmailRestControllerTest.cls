@isTest(seeAllData=true)
public class SR_SuccessEmailRestControllerTest {
    
    static testMethod void testSalesOrderEmails() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Booking_Message__c = 'SUCCESS';
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.SAP_Prebooked_Sales__c = 'TESTNUM';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.SAP_Booked_Sales__c = 'TEST';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }
    
    static testMethod void testServiceOrderEmails() {
        Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
        insertDlists();
        SR_PrepareOrderControllerTest.setUpServiceTestdata();
        
        BigMachines__Quote__c serviceQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c,Service_Org__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.serviceQuote.Id];
        System.debug('----Service_Org__c'+serviceQuote.Service_Org__c);
         Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            serviceQuote.Message_Service__c = 'SUCCESS';
            serviceQuote.Price_Group__c = 'Z1';
            serviceQuote.SAP_Prebooked_Service__c = 'TESTNUM';
            serviceQuote.Interface_Status__c = 'Processed';
            serviceQuote.SAP_Booked_Service__c = 'TEST';
            serviceQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            serviceQuote.Quote_Region__c = 'NA';
            serviceQuote.Order_Type__c = 'Services';
            update serviceQuote;
            
            SR_SuccessEmailRestController.sendEmail(serviceQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(serviceQuote,'TEST');
            ackEmail.SendErrorEmail(serviceQuote, 'Quote SAP PreBookedService - Error VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(serviceQuote.Id, 'Quote SAP PreBookedService - Success VF');
            System.enqueueJob(orderEmail);
        Test.stopTest();
    }
    
    static testMethod void testCombinedOrderEmails() {
        Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
        insertDlists();
        SR_PrepareOrderControllerTest.setUpCombinedTestdata();
        
        BigMachines__Quote__c combinedQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.combinedQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            combinedQuote.Booking_Message__c = 'SUCCESS';
            combinedQuote.Price_Group__c = 'Z1';
            combinedQuote.SAP_Prebooked_Sales__c = 'TESTNUM';
            combinedQuote.Message_Service__c = 'SUCCESS';
            combinedQuote.SAP_Prebooked_Service__c = 'TESTNUM';
            combinedQuote.Interface_Status__c = 'Processed';
            combinedQuote.SAP_Booked_Sales__c = 'TEST';
            combinedQuote.SAP_Booked_Service__c = 'Test';
            combinedQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            combinedQuote.Quote_Region__c = 'NA';
            combinedQuote.Order_Type__c = 'Combined';
            update combinedQuote;
            
            SR_SuccessEmailRestController.sendEmail(combinedQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(combinedQuote,'TEST');
            ackEmail.SendErrorEmail(combinedQuote, 'Quote SAP PreBookedCombined - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(combinedQuote.Id, 'Quote SAP PreBookedCombined - Success VF');
            System.enqueueJob(orderEmail);
        Test.stopTest();
    }

    static testMethod void testSalesOrderEmails1() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Booking_Message__c = 'SUCCESS';
            salesQuote.Order_Type__c = 'Sales';
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.SAP_Booked_Sales__c = 'TEST';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }

    static testMethod void testSalesOrderEmails2() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Message_Service__c = 'SUCCESS';
            salesQuote.Order_Type__c = 'Services';
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.SAP_Prebooked_Service__c = 'TEST';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }
    
    

    static testMethod void testSalesOrderEmails3() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Booking_Message__c = 'Test123';
            salesQuote.SAP_Prebooked_Sales__c = 'TESTNUM';
            salesQuote.Message_Service__c = 'Test';
            salesQuote.Order_Type__c = 'Combined';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.SAP_Prebooked_Service__c = 'TEST';
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }
    
    static testMethod void testSalesOrderEmails4() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,
                                                Message_Service__c,SAP_Prebooked_Service__c, SAP_Booked_Sales__c, 
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.SAP_Booked_Sales__c = 'Test';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.Booking_Message__c = 'Test123';
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }
    
    static testMethod void testSalesOrderEmails6() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,  
                                                Message_Service__c,SAP_Prebooked_Service__c, SAP_Booked_Sales__c, SAP_Booked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Booking_Message__c = 'test';
            salesQuote.Order_Type__c = 'Sales';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.SAP_Prebooked_Sales__c = null;
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }   
    
    static testMethod void testSalesOrderEmails7() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c, 
                                                Message_Service__c,SAP_Prebooked_Service__c, SAP_Booked_Sales__c, SAP_Booked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Message_Service__c = 'test';
            salesQuote.Order_Type__c = 'Services';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.Message_Service__c = 'test';
            salesQuote.SAP_Prebooked_Service__c = null;
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }   

    static testMethod void testSalesOrderEmails8() {
        
        
        insertDlists();
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        
        BigMachines__Quote__c salesQuote = [Select Id,Booking_Message__c,SAP_Prebooked_Sales__c,  
                                                Message_Service__c,SAP_Prebooked_Service__c, SAP_Booked_Sales__c, SAP_Booked_Service__c,
                                                Interface_Status__c,Order_Type__c
                                            From BigMachines__Quote__c
                                            Where Id =: SR_PrepareOrderControllerTest.salesQuote.Id];
        
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SR_SuccessEmailRestControllerMock());
            salesQuote.Booking_Message__c = 'test';
            salesQuote.Message_Service__c = 'test';
            salesQuote.Order_Type__c = 'Combined';
            salesQuote.Interface_Status__c = 'Processed';
            salesQuote.Booking_Message__c = 'test';
            salesQuote.Message_Service__c = 'test';
            salesQuote.SAP_Prebooked_Sales__c = null;
            salesQuote.Price_Group__c = 'Z1';
            salesQuote.Submitted_To_SAP_BY__c = UserInfo.getUserId();
            salesQuote.Quote_Region__c = 'NA';
            update salesQuote;
            SR_SuccessEmailRestController.sendEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
        
            AcknowledgementEmailController ackEmail = new AcknowledgementEmailController(); 
            ackEmail.SendAcknowledgementEmail(salesQuote,'TEST');
            ackEmail.SendErrorEmail(salesQuote, 'Quote SAP PreBookedSales - ERROR VF');
            
            SR_OrderEmail orderEmail = new SR_OrderEmail(salesQuote.Id, 'Quote SAP PreBookedSales - Success VF');
            System.enqueueJob(orderEmail);
            
        Test.stopTest();
    }

    /**
     * This method used to test SendCustomerRequestForm as well
     */
    public static void insertDlists(){
        
        Email_Dlists__c emailDlist1 = TestUtils.getEmailDlist();
        emailDlist1.Functionality__c = 'Prebooked Sales';
        emailDlist1.Region__c = 'NA';
        emailDlist1.DocType__c = 'ZCSS';
        emailDlist1.Brachy_Dlist__c = null;
        emailDlist1.SalesOrg__c = '0601';
        emailDlist1.Support_Dlist__c = 'k@k.com';
        
        Email_Dlists__c emailDlist2 = TestUtils.getEmailDlist();
        emailDlist2.Functionality__c = 'Prebooked Service';
        emailDlist2.Region__c = 'NA';
        emailDlist2.DocType__c = 'ZSQT';
        emailDlist2.SalesOrg__c = '0600';
        emailDlist2.Brachy_Dlist__c = null;
        
        Email_Dlists__c emailDlist3 = TestUtils.getEmailDlist();
        emailDlist3.Functionality__c = 'Booked Service';
        emailDlist3.Region__c = 'NA';
        emailDlist3.DocType__c = 'ZCSS';
        emailDlist3.SalesOrg__c = '0600';
        emailDlist3.Brachy_Dlist__c = null;
        
        Email_Dlists__c emailDlist4 = TestUtils.getEmailDlist();
        emailDlist4.Functionality__c = 'Booked Sales';
        emailDlist4.Region__c = 'NA';
        emailDlist4.DocType__c = 'ZSQT';
        emailDlist4.SalesOrg__c = '0601';
        emailDlist4.Brachy_Dlist__c = null;
        
        Email_Dlists__c emailDlist5 = TestUtils.getEmailDlist();
        emailDlist5.Functionality__c = 'customer request';
        emailDlist5.Region__c = 'NA';
        emailDlist5.DocType__c = 'ZSQT';
        emailDlist5.Primary_DList__c = 'Rupal.Shah@varian.com';
        
        insert new List<Email_Dlists__c>{emailDlist1,emailDlist2,emailDlist3,emailDlist4,emailDlist5};
        
        
    }
}
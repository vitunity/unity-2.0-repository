/***************************************************************************\
    @ Author                : Kaushiki Verma
    @ Date                  : 18/11/2014
    Last Modified By            : Bushra
    
****************************************************************************/
@isTest 
public class SR_SurveyResults_AfterTrigger_test{

   public static testMethod void SRServiceSurveyAfterTrigger_test () {
    
    
    Profile p = new profile();
    p = [Select id from profile where name = 'System Administrator'];
    //User systemuser1 = [Select id from user where isActive = true and id !=:userInfo.getUserId() and ManagerId != null and Manager.isActive = true limit 1]; 
    User systemuser1 = [Select id from user where id =: userInfo.getUserId()];
    //insert systemuser1;
    
    recordtype rec = [Select id from recordtype where developername ='Site_Partner'];
    Account testAcc = accounttestdata.createaccount();
    testAcc.recordtype = rec;
    insert testAcc;
      
    Contact objContact = new Contact();
    objContact.AccountId = testAcc.Id;
    objContact.FirstName = 'Test';
    objContact.LastName = 'Contact';
    objContact.CurrencyIsoCode = 'USD';
    objContact.Email = 'test.tester@testing.com';
    objContact.MailingState = 'CA';
    objContact.Phone= '1235678';
    objContact.MailingCountry = 'USA';
    insert objContact;
      
    Case testCase = SR_testdata.createcase();
    testCase.Account = testAcc;
    testCase.ContactID = objContact.ID;
    insert testCase;
     
     
    Service_Survey_Results__c ServiceSurveyTest = new Service_Survey_Results__c();
    ServiceSurveyTest.Account__c = testAcc.id;
    ServiceSurveyTest.Case__c = testCase.id;
    ServiceSurveyTest.Write_in_comments__c = 'abc';
    ServiceSurveyTest.Translated_comments__c = 'abc';
    ServiceSurveyTest.Overall_Sat__c = 'Dissatisfied';
    ServiceSurveyTest.Follow_Up_Status__c= 'Complete';
    ServiceSurveyTest.Approval_Status__c = 'New';
     
    insert ServiceSurveyTest;
 
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(ServiceSurveyTest.id);
        req1.setSubmitterId(systemuser1.Id); 
    
        //Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
   
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
         ServiceSurveyTest.Approval_Status__c = 'Pending Approval';
    
        update ServiceSurveyTest;
    
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest req2 =   new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejecting request.');
        req2.setAction('Reject');
        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
   
    

    
    ServiceSurveyTest.Approval_Status__c = 'Requires Further Action';
    update ServiceSurveyTest;

   // ServiceSurveyTest.OwnerId = systemuser1.id;
    //HDTest.Rejection_Reason__c = req.Comments ;
    
   // update ServiceSurveyTest;
    }
    
    
    }
@isTest(SeeAllData=true)
Public class SR_TE_ExtractControllerTest
{
  public static testMethod void testM1()
    {
      Datetime d1 = system.now();
      SVMXC_Time_Entry__c te1 = new SVMXC_Time_Entry__c();
      te1.TEExtractDt__c = Date.valueOf(d1.addDays(-10));
      SVMXC_Time_Entry__c te2 = new SVMXC_Time_Entry__c();
      te2.TEExtractDt__c = Date.valueOf(d1);
    SR_TimeEntry_Extract_Controller tec = new SR_TimeEntry_Extract_Controller();
    tec.startdatefrmte = te1;
    tec.enddatefromte = te2;
    List<string> statlist = new List<string>();
    string status = 'Submitted';
    statlist.add(status);
    List<string> countlist = new List<string>();
    string countr = 'United States';
    //string countr1 = 'United Kingdom';
    //string countr2 = 'CHINA';
    countlist.add(countr);
    //countlist.add(countr1);
    //countlist.add(countr2);
    tec.strStatus = statlist;
    tec.strCountry = countlist;
    tec.incldtename = true;
    tec.searchTE();
    //tec.getTimeEntries();
    tec.preparefile();
    //tec.extractpg();
  }
  public static testMethod void testM2()
    {
      Datetime d1 = system.now();
      SVMXC_Time_Entry__c te1 = new SVMXC_Time_Entry__c();
      te1.TEExtractDt__c = Date.valueOf(d1.addDays(-90));
      SVMXC_Time_Entry__c te2 = new SVMXC_Time_Entry__c();
      te2.TEExtractDt__c = Date.valueOf(d1);
    SR_TimeEntry_Extract_Controller tec = new SR_TimeEntry_Extract_Controller();
    tec.startdatefrmte = te1;
    tec.enddatefromte = te2;
    List<string> statlist = new List<string>();
    string status = 'Submitted';
    statlist.add(status);
    List<string> countlist = new List<string>();
    string countr = 'Germany';
    string countr1 = 'DE';
    countlist.add(countr);
    countlist.add(countr1);
    tec.strStatus = statlist;
    tec.strCountry = countlist;
    tec.userstdate = Date.valueOf(d1.addDays(-30));
    tec.usereddate = Date.valueOf(d1);
    tec.incldtename = true;
    tec.searchTE();
    tec.getTimeEntries();
    tec.preparefile();
    tec.extractpg1();
  }
  public static testMethod void testM3()
    {
      Datetime d1 = system.now();
      SVMXC_Time_Entry__c te1 = new SVMXC_Time_Entry__c();
      te1.TEExtractDt__c = Date.valueOf(d1.addDays(-90));
      SVMXC_Time_Entry__c te2 = new SVMXC_Time_Entry__c();
      te2.TEExtractDt__c = Date.valueOf(d1.addDays(-30));
    SR_TimeEntry_Extract_Controller tec = new SR_TimeEntry_Extract_Controller();
    tec.startdatefrmte = te1;
    tec.enddatefromte = te2;
    List<string> statlist = new List<string>();
    string status = 'Submitted';
    statlist.add(status);
    List<string> countlist = new List<string>();
    //string countr = 'United States';
    string countr1 = 'United Kingdom';
    //string countr2 = 'CHINA';
    //countlist.add(countr);
    countlist.add(countr1);
    //countlist.add(countr2);
    tec.strStatus = statlist;
    tec.strCountry = countlist;
    //tec.userstdate = Date.valueOf(d1.addDays(-90));
    //tec.usereddate = Date.valueOf(d1.addDays(-30));
    tec.incldtename = false;
    tec.searchTE();
    //tec.getTimeEntries();
    tec.preparefile();
    tec.extractpg();
  }  
}
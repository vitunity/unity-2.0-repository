@isTest(seeAllData=true)
public class SR_Task_AfterInsertUpdate_Trigger_test
{
    public static testMethod void SR_Task_AfterInsertUpdate_Trigger_test(){
        Country__c con = new Country__c();
        con.Name = 'India';
        insert con;
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.ERP_Timezone__c = 'AUSSA';
        acc.country__c = 'India';
        acc.Country1__c = con.id;
        acc.BillingCity = 'Test';
        insert acc;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test';
        objContact.FirstName = 'Test';
        objContact.Email= 'test@abc.com';  
        objContact.Contact_Type__c = 'Therapist';
        objContact.Phone = '9999999999'; 
        objContact.MailingCountry = 'India';
        objContact.AccountId = acc.id;
        insert objContact;
        
        //insert objCase
        Case cs = new Case();
        // cs.OwnerID = testGroup.ID;
        // cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        cs.subject = 'Test';
        cs.ContactId = objContact.id;
        cs.Priority = 'Medium';
        // cs.Status='Closed (Close case)';
        // cs.origin = 'Phone';
        cs.AccountId = acc.id;
        insert cs;
        
        //insert Task
        Task task1 = new Task();
        task1.CallType = 'Inbound';
        task1.WhatId= cs.id;
        task1.CallDurationInSeconds = 360;
        task1.Subject= 'Other';
        insert task1;
        cs.Status='Closed (Close case)'; 
        update cs;
        Task task2 = new Task();
        task2.CallType = 'Internal';
        //task2.WhatId= cs.id;
        task2.CallDurationInSeconds = 360;
        task2.Subject= 'Other';
        test.startTest();
        insert task2;
        test.stopTest();
    }
    
    public static testMethod void taskPlannigTest(){
        Country__c con = new Country__c();
        con.Name = 'India';
        insert con;
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.ERP_Timezone__c = 'AUSSA';
        acc.country__c = 'India';
        acc.Country1__c = con.id;
        acc.BillingCity = 'Test';
        insert acc;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test';
        objContact.FirstName = 'Test';
        objContact.Email= 'test@abc.com';  
        objContact.Contact_Type__c = 'Therapist';
        objContact.Phone = '9999999999'; 
        objContact.MailingCountry = 'India';
        objContact.AccountId = acc.id;
        insert objContact;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.Account__c = acc.Id;
        opp.AccountId = acc.Id;
        opp.CloseDate = system.today().addDays(30);
        opp.StageName = '0 - NEW LEAD';
        insert opp;
        
        Task task1 = new Task();
        task1.WhatId= opp.id;
        task1.RecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('Task').get('PlanningRequest');
        insert task1;
    }
    public static testMethod void caseTestTask(){
        Country__c con = new Country__c();
        con.Name = 'India';
        insert con;
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.ERP_Timezone__c = 'AUSSA';
        acc.country__c = 'India';
        acc.Country1__c = con.id;
        acc.BillingCity = 'Test';
        insert acc;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test';
        objContact.FirstName = 'Test';
        objContact.Email= 'test@abc.com';  
        objContact.Contact_Type__c = 'Therapist';
        objContact.Phone = '9999999999'; 
        objContact.MailingCountry = 'India';
        objContact.AccountId = acc.id;
        insert objContact;
        
        //insert objCase
        Case cs = new Case();
        cs.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        cs.subject = 'Test';
        cs.ContactId = objContact.id;
        cs.Priority = 'Medium';
        cs.origin = 'Phone';
        cs.AccountId = acc.id;
        cs.Status = 'Closed (Close case)';
        cs.Closed_Case_Reason__c = 'Test';
        insert cs;
        
        //insert Task
        Task task1 = new Task();
        task1.CallType = 'Inbound';
        task1.WhatId= cs.id;
        task1.CallDurationInSeconds = 360;
        task1.Subject= 'Other';
        insert task1;

    }
}
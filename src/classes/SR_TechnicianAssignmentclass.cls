/*******************************************************************************************************************************
@Author   :       Jyoti Gaur
@Date      :       15/10/2013
@Description   :  This apex class is for multiple technician assignements for Work Orders and for inserting servicemax event record as well as technician assignement record.
@Last Modified by: Priti Jaiswal
@Last Modified reason: To update Fields on Event
@Last Modified Date : 28-Feb-2014
@Last Modified by: Shubham Jaiswal
@Last Modified reason: US4486: to replace SVMXC__Salesforce_User__c field with User__c
@Last Modified Date : 14-May-2014
@Last Modified by: Nikita Gupta
@Last Modified reason: add null checks before all sets, lists and maps.
@Last Modified Date : 6/132014
@Last Modified by   :   Nikita Gupta
@Last Modified on   :   7/8/2014
@Last Modified reason:  merged before trigger code in class
*******************************************************************************************************************/

public class SR_TechnicianAssignmentclass
{
    //DE3644, method to insert Technician Assignment records on insert of ServiceMax event
    public void insertTechnicianAssignment(List<SVMXC__SVMX_Event__c> eventlist)
    {
        List<Technician_Assignment__c> lstTechnicianAssignment = new List<Technician_Assignment__c>();

        for(SVMXC__SVMX_Event__c varEve : eventlist)
        {
            Technician_Assignment__c objTA = new Technician_Assignment__c();

            objTA.Scheduled_End_Date__c = varEve.SVMXC__EndDateTime__c;
            objTA.Scheduled_Start_Date__c = varEve.SVMXC__StartDateTime__c;
            objTA.Debrief_Status__c = 'Assigned';
            objTA.Event_Id__c = varEve.id;
            objTA.Technician_Name__c = varEve.SVMXC__Technician__c;

            if(varEve.SVMXC__Service_Order__c != null)
            {
                objTA.Work_Order__c = varEve.SVMXC__Service_Order__c;
            }
            system.debug('objTA.Technician_Name__c.....>>> ' + objTA.Technician_Name__c);

            lstTechnicianAssignment.add(objTA);
        }
        if(lstTechnicianAssignment.size()>0)
        {
            insert lstTechnicianAssignment;
        }
    }

    //DE3644, method to update Technician Assignment records on update of ServiceMax event
    public void updateTechnicianAssign(List<SVMXC__SVMX_Event__c> listEvent , Map<id, SVMXC__SVMX_Event__c> mapOldEvent)
    {
        Set<Id> setEventId = new Set<Id>();
        Map<string, Technician_Assignment__c> mapTechnicianAssignment = new Map<string, Technician_Assignment__c>();
        Map<Id, Technician_Assignment__c> mapUpdateTechnicianAssignment = new Map<Id, Technician_Assignment__c>();

        for(SVMXC__SVMX_Event__c varEve : listEvent)
        {
            if(varEve.SVMXC__Service_Order__c != null && (varEve.SVMXC__StartDateTime__c != mapOldEvent.get(varEve.id).SVMXC__StartDateTime__c || varEve.SVMXC__EndDateTime__c != mapOldEvent.get(varEve.id).SVMXC__EndDateTime__c || (varEve.SVMXC__Technician__c != null && varEve.SVMXC__Technician__c != mapOldEvent.get(varEve.id).SVMXC__Technician__c)))
            {
                setEventId.add(varEve.id); 
            }
        }
        if(setEventId.size() > 0)
        {
            List<Technician_Assignment__c> listTempTechAssign = [select id, Event_Id__c, Scheduled_End_Date__c, Scheduled_Start_Date__c,
                                                                Technician_Name__c from Technician_Assignment__c where Event_Id__c = :setEventId];
            if(listTempTechAssign.size() > 0)
            {
                for(Technician_Assignment__c verTA : listTempTechAssign)
                {
                    if(verTA.Event_Id__c != null)
                    {
                        mapTechnicianAssignment.put(verTA.Event_Id__c, verTA);
                    }
                }
            }
        }
        for(SVMXC__SVMX_Event__c varEve : listEvent)
        {
            if(mapTechnicianAssignment.containsKey(varEve.Id) && mapTechnicianAssignment.get(varEve.Id) != null)
            {
                Technician_Assignment__c objTA = mapTechnicianAssignment.get(varEve.Id);

                if(varEve.SVMXC__StartDateTime__c != null && varEve.SVMXC__StartDateTime__c != mapOldEvent.get(varEve.id).SVMXC__StartDateTime__c)
                {
                    objTA.Scheduled_Start_Date__c = varEve.SVMXC__StartDateTime__c;  
                }
                if(varEve.SVMXC__EndDateTime__c != null && varEve.SVMXC__EndDateTime__c != mapOldEvent.get(varEve.id).SVMXC__EndDateTime__c)
                {
                    objTA.Scheduled_End_Date__c = varEve.SVMXC__EndDateTime__c;
                }
                if(varEve.SVMXC__Technician__c != null && varEve.SVMXC__Technician__c != mapOldEvent.get(varEve.id).SVMXC__Technician__c)
                {
                    objTA.Technician_Name__c = varEve.SVMXC__Technician__c;
                }
                mapUpdateTechnicianAssignment.put(objTA.Id, objTA);
            }
        }
        if(mapUpdateTechnicianAssignment.size() > 0)
        {
            update mapUpdateTechnicianAssignment.values();
        }
    }

    //DE3644, method to delete Technician Assignment records on delete of ServiceMax event
    public void deleteTechnicianAssignment(Map<Id, SVMXC__SVMX_Event__c> mapOldSvcmxEvent)
    {
        Set<Id> setSvcmxEventId = new Set<Id>();
        List<Technician_Assignment__c> listDeleteTechnicianAssignment = new List<Technician_Assignment__c>();

        for(SVMXC__SVMX_Event__c varEvent : mapOldSvcmxEvent.values()) 
        {
            setSvcmxEventId.add(varEvent.Id);
        }
        if(setSvcmxEventId.size() > 0)
        {
            listDeleteTechnicianAssignment = [select Id, Event_Id__c from Technician_Assignment__c where Event_Id__c in :setSvcmxEventId];

            if(listDeleteTechnicianAssignment.size() > 0)
            {
                system.debug('listDeleteTechnicianAssignment value is === ' + listDeleteTechnicianAssignment);
                delete listDeleteTechnicianAssignment;
            }
        }
    }
}
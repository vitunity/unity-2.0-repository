@isTest(SeeAllData=true)
public class SR_TechnicianAssignmentclass_Test
{
    public static testMethod void SR_TechnicianAssignmentclassTest()
    {
        //Dummy data crreation 
        /*Profile sysadminprofile = [Select id from profile where name = 'System Administrator'];

        User varSysADMN = [Select id from user where profileId = :sysadminprofile.id AND isActive = true AND id != :userInfo.getUserId() limit 1]; */

        //insert Account record
        Account varAcc = AccountTestData.createAccount();
        insert varAcc;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        insert objContact;
        
        Case testcase = sr_testdata.createcase();
        testcase.Priority = 'Medium';
        testcase.ContactID = objContact.id;
        insert testcase;
        //insert service team
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.Name = 'Test Service';
        Insert ServiceTeam;
        
        //insert technician record user
        SVMXC__Service_Group_Members__c varTech = SR_testdata.createTech();
        varTech.SVMXC__Service_Group__c  = ServiceTeam.Id;
        varTech.Name = 'Test Tech';
        varTech.User__c = userInfo.getUserid();
        varTech.SVMXC__Salesforce_User__c = userInfo.getUserid();
        varTech.SVMXC__Street__c = 'Test Street';
        varTech.SVMXC__City__c = 'Test City';
        varTech.SVMXC__State__c = 'Test State';
        varTech.SVMXC__Zip__c = '302015';
        varTech.SVMXC__Country__c = 'India';
        varTech.SVMXC__Email__c = 'standarduser@testorg.com';
        insert varTech;
        
        //insert technician record with salesforce user
        SVMXC__Service_Group_Members__c varTech_2 = SR_testdata.createTech();
        varTech_2.SVMXC__Service_Group__c  = ServiceTeam.Id;
        varTech_2.Name = 'Test Tech';
        varTech_2.SVMXC__Salesforce_User__c = userInfo.getUserid();
        varTech_2.SVMXC__Street__c = 'Test Street';
        varTech_2.SVMXC__City__c = 'Test City';
        varTech_2.SVMXC__State__c = 'Test State';
        varTech_2.SVMXC__Zip__c = '302015';
        varTech_2.SVMXC__Country__c = 'India';
        varTech_2.SVMXC__Email__c = 'standarduser@testorg.com';
        insert varTech_2;
      
        /***************For IF Condition**********************/
       
        //insert WO record 1
        SVMXC__Service_Order__c varWO = SR_testdata.createServiceOrder();
        varWO.SVMXC__Group_Member__c = varTech.Id;
        varWO.SVMXC__Company__c = varAcc.Id;
        varWO.SVMXC__Order_Status__c = 'Open';
        varWO.SVMXC__Preferred_Technician__c = varTech.Id;
        varWO.SVMXC__Case__c =testcase.id;
        varWO.SVMXC__Priority__c = 'High';
        insert varWO;
        
        SVMXC__Service_Order__c varWO2 = SR_testdata.createServiceOrder();
        varWO2.SVMXC__Group_Member__c = varTech.Id;
        varWO2.SVMXC__Company__c = varAcc.Id;
        varWO2.SVMXC__Order_Status__c = 'Open';
        varWO2.SVMXC__Preferred_Technician__c = varTech.Id;
        varWO2.SVMXC__Case__c =testcase.id;
        varWO2.Parent_WO__r = varWO;
        varWO2.SVMXC__Priority__c = 'Emergency';
        insert varWO2;
        


        //insert technician assignment record
        Technician_Assignment__c varTechAsn = new Technician_Assignment__c();
        varTechAsn.Work_Order__c = varWO.Id;
        varTechAsn.Technician_Name__c = varTech.Id;
        insert varTechAsn;
        
        //insert location record
        SVMXC__Site__c varLoc = SR_testdata.createsite();
        varLoc.SVMXC__Account__c = varAcc.Id;
        varLoc.Location_Capacity__c = 2;
        insert varLoc;

        //insert event properties record
        Event_Properties__c varEveProp = new Event_Properties__c();
        varEveProp.Booking_Type__c = 'TEST';
        varEveProp.Status__c = 'Open';
        insert varEveProp;
        
        //insert event record 1
        SVMXC__SVMX_Event__c varEve = new SVMXC__SVMX_Event__c ();
        varEve.SVMXC__StartDateTime__c= System.today();
        varEve.SVMXC__Technician__c = varTech_2.id;
        varEve.SVMXC__EndDateTime__c = system.now();
        varEve.SVMXC__DurationInMinutes__c = 60;
        varEve.SVMXC__Service_Order__c = varWO2.id;
        varEve.SVMXC__WhatId__c = varWO.Id;
        varEve.SVMXC__Location__c = 'Test Site';
        varEve.Booking_Type__c = 'TEST';
        varEve.Status__c = 'Open';
        //system.debug('!!@@## varEve' + varEve.DurationInMinutes);
        insert varEve;
       
        //update ServiceMax Event
        varEve.SVMXC__EndDateTime__c = system.now().addHours(1);
        varEve.SVMXC__StartDateTime__c= System.today();
        varEve.SVMXC__Technician__c = varTech_2.id;
        update varEve;
        
        SVMXC__SVMX_Event__c varEve1 = new SVMXC__SVMX_Event__c ();
        varEve1.SVMXC__StartDateTime__c= System.today();
        varEve1.SVMXC__Technician__c = varTech_2.id;
        varEve1.SVMXC__EndDateTime__c = system.now();
        varEve1.SVMXC__DurationInMinutes__c = 60;
        varEve1.SVMXC__Service_Order__c = varWO2.id;
        varEve1.SVMXC__WhatId__c = varWO.Id;
        varEve1.SVMXC__Location__c = 'Test Site';
        varEve1.Booking_Type__c = 'TEST';
        varEve1.Status__c = 'Open';
        insert varEve1;
        varEve1.Status__c = 'Cancelled';
         update varEve1;
       
        

        /***************For ELSE Condition**********************/
        
        //insert WO record 2 for else
        SVMXC__Service_Order__c varWO_2 = SR_testdata.createServiceOrder();
        varWO_2.SVMXC__Group_Member__c = varTech.Id;
        varWO_2.SVMXC__Company__c = varAcc.Id;
        varWO_2.SVMXC__Order_Status__c = 'Open';
        varWO_2.SVMXC__Preferred_Technician__c = varTech_2.Id;
        varWO_2.SVMXC__Case__c =testcase.id;
        varWO_2.SVMXC__Priority__c = 'Emergency';

        insert varWO_2;
        
        SVMXC__Service_Order__c varWO_22 = SR_testdata.createServiceOrder();
        varWO_22.SVMXC__Group_Member__c = varTech.Id;
        varWO_22.SVMXC__Company__c = varAcc.Id;
        varWO_22.SVMXC__Order_Status__c = 'Open';
        varWO_22.SVMXC__Preferred_Technician__c = varTech_2.Id;
        varWO_22.SVMXC__Case__c =testcase.id;
        varWO_22.Parent_WO__c = varWO_2.ID;
        varWO_22.SVMXC__Priority__c = 'Emergency';
        insert varWO_22;

        //insert event record 2
        Event varEve_2 = new Event();
        varEve_2.Subject = 'Test Event';
        //varEve_2.ownerid = varSysADMN.Id;
        varEve_2.whatid = varWO_2.Id;
        varEve_2.StartDateTime = system.now().addHours(2);
        varEve_2.EndDateTime = system.now().addHours(3);
        varEve_2.DurationInMinutes = 60;
        //varEve_2.Booking_Type__c = 'TEST';
        //varEve_2.Status__c = 'Open';
        //varEve_2.Location = 'test site';
        //insert varEve_2;
        
        //Delete ServiceMax Event
        varEve.SVMXC__EndDateTime__c = system.now().addHours(1);
        varEve.SVMXC__StartDateTime__c= System.today();
        varEve.SVMXC__Technician__c = varTech_2.id;
        delete varEve;
        
    }
}
/*************************************************************************\
Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************
27-Oct-2017 Nick Sauer    STSK0013212     1)  Add CreatedBy for use on email
****************************************************************************/

Public class SR_TechnicianResponseController {
    Public id workOrderId{get;set;}
    Public SVMXC__Service_Order__c objwo{get;set;}
    Public Map<String,String> TechandResponsemap{get;set;} //= new Map<String,string>(); // map of technician, respose
    Public list<wrapperClass> wrapperList;//{get;set;}
    Public String techresponsetemp;
    Public String techniciantemp;
    Public list<SVMXC__Service_Order__History> historywo{get;set;}
           
    
    Public SVMXC__Service_Order__c getVarWO(){
        objwo = new SVMXC__Service_Order__c();
        objwo = [
            select id , 
            Name , 
            SVMXC__Preferred_Technician__r.Name , 
            SVMXC__Case__r.SVMXC__Billing_Type__c ,
            SVMXC__Group_Member__r.Name , 
            SVMXC__Dispatch_Response__c , 
            SVMXC__Priority__c , 
            SVMXC__Service_Contract__c , 
            SVMXC__Site__r.Name , 
            SVMXC__City__c , 
            SVMXC__Contact__r.Name , 
            Contact_Phone__c , 
            SVMXC__Product__c , 
            SVMXC__Problem_Description__c , 
            SVMXC__Corrective_Action__c , 
            District_Manager_1__r.Name ,
            SVMXC__Top_Level__r.Name ,
            Dispatcher_Status__c ,
            SVMXC__Service_Group__r.SVMXC__Group_Code__c ,
            Service_Team__r.SVMXC__Group_Code__c ,
            Escalated_by_Dispatch__c,
            CreatedBy.Name,
            CreatedBy.UserRole.Name
            from SVMXC__Service_Order__c where id =: workOrderId
        ];
                
        return objwo;
    }
    Public void setwrapperList(list<wrapperClass> templist)
    {
        this.wrapperList = templist;
    }
    Public list<wrapperClass> getwrapperList(){
        schema.DescribeSObjectResult result = SVMXC__Service_Group_Members__c.sObjectType.getDescribe();
        String keyPrefix = result.getKeyPrefix();
        system.debug('keyPrefix'+keyPrefix+result);
        list <SVMXC__Service_Order__c> lastTechnician = [select id,SVMXC__Group_Member__r.Name from SVMXC__Service_Order__c where id =: workOrderId limit 1];
        wrapperList = new list<wrapperClass>();
        historywo = new List<SVMXC__Service_Order__History>();
        TechandResponsemap = new Map<String,string>(); 
        System.debug('workOrderId ^^^'+workOrderId );
        for(SVMXC__Service_Order__History objHistory : [select id, CreatedDate,Field,IsDeleted,NewValue,OldValue,ParentId from SVMXC__Service_Order__History where ParentId =: workOrderid order By CreatedDate]){
      historywo.add(objHistory); 
            if(objHistory.Field == 'SVMXC__Dispatch_Response__c' && objHistory.NewValue!= null && objHistory.OldValue != null)techresponsetemp = String.ValueOf(objHistory.OldValue);
            else if(objHistory.Field == 'SVMXC__Group_Member__c' && objHistory.Oldvalue != null && !String.ValueOf(objHistory.Oldvalue).contains(keyPrefix))TechandResponsemap.put(String.ValueOf(objHistory.OldValue),techresponsetemp);
      else if(objHistory.Field == 'SVMXC__Group_Member__c' && objHistory.NewValue != null && !String.ValueOf(objHistory.NewValue).contains(keyPrefix))techniciantemp = String.ValueOf(objHistory.NewValue);
        }
        if(!TechandResponsemap.keyset().contains(techniciantemp))
        {
            if(!String.ISBLANK(techniciantemp))TechandResponsemap.put(techniciantemp,techresponsetemp);
            else if(lastTechnician.size()>0 && lastTechnician[0].SVMXC__Group_Member__r != null) TechandResponsemap.put(lastTechnician[0].SVMXC__Group_Member__r.Name,techresponsetemp);
        }
        
        for(String str : TechandResponsemap.keySet()){
            wrapperClass objWrapper = new wrapperClass();
            objWrapper.technicianName = str;
            objWrapper.technicianResponse = TechandResponsemap.get(str);
            wrapperList.add(objWrapper);
        }
        return wrapperList; 
    }
    
    Public class wrapperClass{
        Public string technicianName{get;set;}
        Public String technicianResponse{get;set;}
    }
}
@isTest(seeALLData=true)
public class SR_TechnicianResponseController_Test {
      
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;

    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    
    Static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        newAcc = UnityDataTestClass.AccountData(true);
        
        WO = new SVMXC__Service_Order__c();
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Reviewed';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Interface_Status__c = 'Processed';
        WO.SVMXC__Dispatch_Response__c = 'Accepted';
        insert WO;
        
      
    }
   
    private static testMethod void UnitTest() {
        Test.StartTest();
            
            SR_TechnicianResponseController controller = new SR_TechnicianResponseController();
            controller.workOrderId = wo.id;
            controller.setwrapperList(controller.getwrapperList());
            controller.getVarWO();
            SR_TechnicianResponseController.wrapperClass wr = new SR_TechnicianResponseController.wrapperClass();
            wr.technicianName = 'test';
            wr.technicianResponse = 'Test';
        Test.stopTest();
    }
}
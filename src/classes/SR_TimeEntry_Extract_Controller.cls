public class SR_TimeEntry_Extract_Controller
{
    public String startdate{
        get;
        set;
    }
    public String enddate{
        get;
        set;
    }
    public boolean incldtename {get;set;}
    public List<String> strCountry{
        get;
        set;
    }
    public List<String> strCountryupd{
        get;
        set;
    }
    public List<String> strStatus{
        get;
        set;
    }
    public List<String> strStatusupd{
        get;
        set;
    }
    public String filename{
        get;
        set;
    }
    public boolean toggleResult{
        get;
        set;
    }
    public boolean toggleResultGE{
        get;
        set;
    }
    public SVMXC_Time_Entry__c startdatefrmte{
        get;
        set;
    }
    public SVMXC_Time_Entry__c enddatefromte{
        get;
        set;
    }
    public List<SVMXC_Time_Entry__c> timeentrs {get;set;}
    Public set<String> cntryfinal;
    public list<wrpTEextract> timeentries1 {get; private set;}
    public List<string> getData {get; private set;}
    public string getData1{get; private set;}
    public set<string> extracttxt {get; private set;}
    public static Date stdatefrquery;
    public static Date eddatefrquery;
    public Date userstdate;
    public Date usereddate;
    public static Date usereddatefinal;
    public static Date usereddatefinal1;
    public Date convsttimeui;
    public Date convedtimeui;
    public Datetime convsttimefi;
    public Datetime convedtimefi;
    public String extract{get; set;}
    public String fmtstdate;
    public String fmtstdate1;
    public String fmteddate;
    public String fmteddate1;
    public String queryforui;
    public String queryforfile;
    public set<String> exclddistricts;
    public ApexPages.StandardSetController paginationSetController{get;set;}

    public SR_TimeEntry_Extract_Controller()
    {
        queryforui = '';
        queryforfile = '';
        startdatefrmte = new SVMXC_Time_Entry__c();
        toggleResult = false;
        startdatefrmte.TEExtractDt__c = date.today();
        enddatefromte = new SVMXC_Time_Entry__c();
        enddatefromte.TEExtractDt__c = date.today();      
        exclddistricts = new set<String>();
        map<string,Time_Entry_Extract_districts__c> mapexcldist = Time_Entry_Extract_districts__c.getAll();
        for (String s : mapexcldist.keyset())
        {
           exclddistricts.add(mapexcldist.get(s).value__c); 
        }
    }
    public pageReference cancel()
    {
        toggleResult = false;
        toggleResultGE = false;
        paginationSetController = null;
        return new pageReference('/apex/SR_TimeEntry_Extract');
    }
    public pageReference extractpg()
    {
        preparefile();
        PageReference extractpage = new PageReference('/apex/TextExtract_Page');        
        extractpage.setRedirect(false);
        return extractpage;
    }
    public pageReference extractpg1()
    {
        preparefile();
        PageReference extractpage = new PageReference('/apex/TextExtract_pagecsv');        
        extractpage.setRedirect(false);
        return extractpage;
    }
    /*
    public ApexPages.StandardSetController con
    {
        get{
            strCountryupd = new List<String>();
            strStatusupd = new List<String>();
            System.debug('***CM strCountry: '+strCountry);
            if (con == null)
            {
                if (strStatus.size() > 0){
                    for (String stats : strStatus)
                    {
                        if (stats == 'any'){
                            strStatusupd.add('');
                            strStatusupd.add('Open');
                            strStatusupd.add('Incomplete');
                            strStatusupd.add('Submitted');
                            strStatusupd.add('Approved');
                        }
                        else
                        {
                            strStatusupd.add(stats);
                        }
                    }
                }
                if (strCountry.size() > 0 ){
                    for (String cntry : strCountry)
                    {
                        if (cntry == 'United States')
                        {
                            strCountryupd.add(cntry);
                            strCountryupd.add('US');
                        }
                        if (cntry == 'United Kingdom')
                        {
                            strCountryupd.add(cntry);
                            strCountryupd.add('UK');
                            strCountryupd.add('Ireland');
                            strCountryupd.add('IE');
                            strCountryupd.add('GB');
                        }
                        if (cntry == 'Germany')
                        {
                            strCountryupd.add(cntry);
                            strCountryupd.add('DE');
                        }
                        if (cntry == 'China')
                        {
                            strCountryupd.add(cntry);
                            strCountryupd.add('CN');
                        }
                        if (cntry == 'Canada')
                        {
                            strCountryupd.add(cntry);
                            strCountryupd.add('CA');
                        }
                    }
                }
                cntryfinal = new set<String>();
                if(strCountryupd.size() > 0)
                {
                    for (string s : strCountryupd)
                    {
                        cntryfinal.add(s);
                    }
                }
                System.debug('***CM strCountryupd: '+strCountryupd);
                userstdate = startdatefrmte.TEExtractDt__c;
                usereddate = enddatefromte.TEExtractDt__c;
                usereddatefinal = usereddate.addDays(1);
                System.Debug('***CM userstdate : '+userstdate);
                System.Debug('***CM usereddate : '+usereddate);
                stdatefrquery = userstdate.addDays(-1);
                eddatefrquery = usereddate.addDays(1);
                System.Debug('***CM stdatefrquery : '+stdatefrquery);
                System.Debug('***CM eddatefrquery : '+eddatefrquery);
                if (cntryfinal.size() > 0 && cntryfinal.Contains('Germany') && cntryfinal.Contains('DE')){
                    queryforui = 'Select Timesheet__c, Timesheet__r.Name, Timesheet__r.Organization__c,Timesheet__r.Organization__r.Name,Timesheet__r.District__r.Name,Work_Center__c,Technician__r.Name,Record_Type_Name__c,Timesheet__r.Status__c,Payroll_End_Date__c,Start_Time__c,End_Time__c,TotalTime__c,Equipment__c,city__c,Work_Order_Number__c,timesheet__r.Dosimeter_Reading__c,name,WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Payroll_Start_Date__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, Type__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Country__c in :strCountryupd and Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and End_Date_Time__c < :eddatefrquery and Technician__r.Group_Type__c != \'Project Management\' and Technician__r.Group_Type__c != \'SW Project Management\' order by Start_Date_Time__c limit 2000';
                }
                else if(cntryfinal.size() > 0)
                {
                    queryforui = 'Select WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Payroll_Start_Date__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, Type__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Country__c in :strCountryupd and Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and End_Date_Time__c < :eddatefrquery and Technician__r.Group_Type__c != \'Project Management\' and Technician__r.Group_Type__c != \'Training\' and Technician__r.Group_Type__c != \'SW Project Management\' order by Start_Date_Time__c limit 2000';
                }
                System.debug('***CM queryforui: '+queryforui);
                con = new ApexPages.StandardSetController(Database.getQueryLocator(queryforui));
                con.setPagesize(100);
            }
            return con;
        }
        set;
    }
    */
    public pageReference searchTE()
    {
        this.startdate = startdate;
        this.enddate = enddate;
        strCountryupd = new List<String>();
        strStatusupd = new List<String>();
        System.debug('***CM in search');
        if (strCountry.size() == 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Country.'); 
            ApexPages.addMessage(myMsg);
            return null;
        }
        if (strStatus.size() == 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Status.'); 
            ApexPages.addMessage(myMsg);
            return null;
        }
        if (strStatus.size() > 0){
            for (String stats : strStatus)
            {
                if (stats == 'any'){
                    strStatusupd.add('');
                    strStatusupd.add('Open');
                    strStatusupd.add('Incomplete');
                    strStatusupd.add('Submitted');
                    strStatusupd.add('Approved');
                }
                else
                {
                    strStatusupd.add(stats);
                }
            }
        }
        if (strCountry.size() > 0 ){
            for (String cntry : strCountry)
            {
                if (cntry == 'United States')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('US');
                }
                if (cntry == 'United Kingdom')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('UK');
                    strCountryupd.add('Ireland');
                    strCountryupd.add('IE');
                    strCountryupd.add('GB');
                }
                if (cntry == 'Germany')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('DE');
                }
                if (cntry == 'China')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('CN');
                }
                if (cntry == 'Canada')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('CA');
                }
            }
        }
        cntryfinal = new set<String>();
        if(strCountryupd.size() > 0)
        {
            for (string s : strCountryupd)
            {
                cntryfinal.add(s);
            }
        }
        System.Debug('****CM cntryfinal: '+cntryfinal);
        if (cntryfinal.size() > 0)
        {
            System.debug('***inside: '+cntryfinal);
            if (cntryfinal.contains('United Kingdom'))
            {
                if (cntryfinal.size() > 5)
                {
                    toggleResult = false;
                    toggleResultGE = false;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Selection, You can select only United States and Canada.'); 
                    ApexPages.addMessage(myMsg);
                    return null;
                }
            } 
            else 
            {
                if (cntryfinal.size() > 2)
                {
                    if (cntryfinal.contains('United States') && cntryfinal.contains('Canada') && cntryfinal.contains('CA') && cntryfinal.contains('US') && cntryfinal.size() == 4)
                    {
                        system.debug('***Valid Scenario');
                    }
                    else
                    {
                        toggleResult = false;
                        toggleResultGE = false;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Selection, You can select only United States and Canada.'); 
                        ApexPages.addMessage(myMsg);
                        return null;
                    }
                }
            }
        }
        System.debug('***CM strCountryupd: '+strCountryupd);
        userstdate = startdatefrmte.TEExtractDt__c;
        usereddate = enddatefromte.TEExtractDt__c;
        usereddatefinal = usereddate.addDays(1);
        System.Debug('***CM userstdate : '+userstdate);
        System.Debug('***CM usereddate : '+usereddate);
        stdatefrquery = userstdate.addDays(1);
        eddatefrquery = usereddate.addDays(1);
        System.Debug('***CM stdatefrquery : '+stdatefrquery);
        System.Debug('***CM eddatefrquery : '+eddatefrquery);
        timeentrs = new List<SVMXC_Time_Entry__c>();
        if (cntryfinal.size() > 0 && cntryfinal.Contains('Germany') && cntryfinal.Contains('DE'))
        {
            timeentrs = [Select Timesheet__c, Timesheet__r.Name, Timesheet__r.Organization__c,Timesheet__r.Organization__r.Name,Timesheet__r.District__r.Name,Work_Center__c,Technician__r.Name,Record_Type_Name__c,Timesheet__r.Status__c,Payroll_End_Date__c,Start_Time__c,End_Time__c,TotalTime__c,Equipment__c,city__c,Work_Order_Number__c,timesheet__r.Dosimeter_Reading__c,name,WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Payroll_Start_Date__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, Type__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Timesheet__r.Is_TEM__c = false and Timesheet__r.District__r.Name not in :exclddistricts and Country__c in :strCountryupd and Timesheet__r.Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and Start_Date_Time__c < :eddatefrquery order by Start_Date_Time__c limit 1000];
        }
        else if(cntryfinal.size() > 0)
        {
            timeentrs = [Select WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Payroll_Start_Date__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, Type__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Timesheet__r.Is_TEM__c = false and Country__c in :strCountryupd and Timesheet__r.Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and Start_Date_Time__c < :eddatefrquery and Technician__r.Group_Type__c != 'Training' order by Start_Date_Time__c limit 1000];
        }
        getTimeentriesForPagination(timeentrs);
        getTimeEntries();

        System.debug('***CM timeentries size: '+timeentries1.size());
        if (paginationSetController != null){
            if (cntryfinal.size() > 0 && cntryfinal.Contains('Germany') && cntryfinal.Contains('DE'))
            {
                toggleResult=false;
                toggleResultGE=true;  
            }
            else if (cntryfinal.size() > 0)
            {
                toggleResultGE=false;
                toggleResult=true;     
            }
        }
        return null;
    }
    public void getTimeentriesForPagination(List<SVMXC_Time_Entry__c> timeentrys)
    {
        paginationSetController = new ApexPages.StandardSetController(timeentrys);
        paginationSetController.setPageSize(100);
    }

    public String preparefile(){
        if (startdatefrmte.TEExtractDt__c != null && enddatefromte.TEExtractDt__c != null)
        {
            userstdate = startdatefrmte.TEExtractDt__c;
            usereddate = enddatefromte.TEExtractDt__c;
            usereddatefinal = usereddate.addDays(1);
            stdatefrquery = userstdate.addDays(-2);
            eddatefrquery = usereddate.addDays(2);
        }
        strCountryupd = new List<String>();
        strStatusupd = new List<String>();
        if (strStatus.size() > 0){
            for (String stats : strStatus)
            {
                if (stats == 'any'){
                    strStatusupd.add('');
                    strStatusupd.add('Open');
                    strStatusupd.add('Incomplete');
                    strStatusupd.add('Submitted');
                    strStatusupd.add('Approved');
                }
                else
                {
                    strStatusupd.add(stats);
                }
            }
        }
        if (strCountry.size() > 0 ){
            for (String cntry : strCountry)
            {
                if (cntry == 'United States')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('US');
                }
                if (cntry == 'United Kingdom')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('UK');
                    strCountryupd.add('Ireland');
                    strCountryupd.add('IE');
                    strCountryupd.add('GB');
                }
                if (cntry == 'Germany')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('DE');
                }
                if (cntry == 'China')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('CN');
                }
                if (cntry == 'Canada')
                {
                    strCountryupd.add(cntry);
                    strCountryupd.add('CA');
                }
            }
        }
        System.debug('***CM strCountryupd: '+strCountryupd);
        //extracttxt = new set<String>();
        extract='';
        getData = new List<String>();
        queryforfile = '';
        if (cntryfinal.size() > 0 && cntryfinal.Contains('Germany') && cntryfinal.Contains('DE'))
        {
            queryforfile = 'Select Timesheet__c, Timesheet__r.Name, Timesheet__r.Organization__c,Timesheet__r.Organization__r.Name,Timesheet__r.District__r.Name,Work_Center__c,Labor_Time__c,Technician__r.Name,Record_Type_Name__c,Timesheet__r.Status__c,Payroll_End_Date__c,Start_Time__c,End_Time__c,TotalTime__c,Equipment__c,city__c,Work_Order_Number__c,timesheet__r.Dosimeter_Reading__c,name,WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Payroll_Start_Date__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, Type__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Timesheet__r.Is_TEM__c = false and Timesheet__r.District__r.Name not in :exclddistricts and Country__c in :strCountryupd and Timesheet__r.Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and Start_Date_Time__c < :eddatefrquery order by Start_Date_Time__c';
        }
        else if(cntryfinal.size() > 0)
        {
            queryforfile = 'Select Name,WCExtract__c,Start_Date_Time__c,End_Date_Time__c,Country__c,Technician__r.User__r.TimeZoneSidKey, Paystdtext__c, StTimeExtract__c, EdTimeExtract__c, LTExtract__c,WOExtract__c, TETYPE__c, Activity_Description__c FROM SVMXC_Time_Entry__c where Timesheet__r.Is_TEM__c = false and Country__c in :strCountryupd and Timesheet__r.Status__c in :strStatusupd and Start_Date_Time__c >= :stdatefrquery and Start_Date_Time__c < :eddatefrquery and Technician__r.Group_Type__c != \'Training\' order by Start_Date_Time__c ';
        }
        string befheader = '';
        String headkey = '';
        For (SVMXC_Time_Entry__c timeentry : Database.Query(queryforfile))
        {
            convsttimefi = null;
            convedtimefi = null;
            if (timeentry.Start_Date_Time__c != null && timeentry.Technician__c != null && timeentry.Technician__r.User__r != null && timeentry.Technician__r.User__r.TimeZoneSidKey != null)
            {
                fmtstdate1 = timeentry.Start_Date_Time__c.format('yyyy-MM-dd HH:mm:ss',timeentry.Technician__r.User__r.TimeZoneSidKey);
                Integer UTZYear = Integer.valueOf(fmtstdate1.substring(0, 4));
                Integer UTZMonth = Integer.valueOf(fmtstdate1.substring(5, 7));
                Integer UTZDay = Integer.valueOf(fmtstdate1.substring(8, 10));
                convsttimefi = Date.valueOf(fmtstdate1.substring(0,10));
            }
            if (timeentry.End_Date_Time__c != null && timeentry.Technician__c != null && timeentry.Technician__r.User__r != null  && timeentry.Technician__r.User__r.TimeZoneSidKey != null)
            {
                fmteddate1 = timeentry.End_Date_Time__c.format('yyyy-MM-dd HH:mm:ss',timeentry.Technician__r.User__r.TimeZoneSidKey);
                Integer UTZYear1 = Integer.valueOf(fmteddate1.substring(0, 4));
                Integer UTZMonth1 = Integer.valueOf(fmteddate1.substring(5, 7));
                Integer UTZDay1 = Integer.valueOf(fmteddate1.substring(8, 10));
                convedtimefi = Date.valueOf(fmteddate1.substring(0,10));
            }
            if (convsttimefi != null && convsttimefi >= userstdate &&  convsttimefi < usereddatefinal)
            {
                if (timeentry.Country__c != 'Germany' && timeentry.Country__c != 'DE')
                {
                    String strEndtime = '';
                    String strLabortime = '';
                    if (timeentry.EdTimeExtract__c == '235900')
                    {
                        strEndtime = '000000';
                        String telabtime = timeentry.LTExtract__c;
                        String hr = telabtime.substring(0, 2);
                        String min = telabtime.substring(2,4);
                        Time labtime = Time.newInstance(Integer.valueOf(hr), Integer.valueOf(min),0,0);
                        labtime = labtime.addMinutes(1);
                        strLabortime = ((String.valueOf(labtime)).substring(0,5)).replace(':','');
                    }
                    else
                    {
                        strEndtime = timeentry.EdTimeExtract__c;
                        strLabortime = timeentry.LTExtract__c;
                    }
                    if(timeentry.Country__c == 'UK' || timeentry.Country__c == 'United Kingdom' || timeentry.Country__c == 'GB' || timeentry.Country__c == 'IE' || timeentry.Country__c == 'CHINA' || timeentry.Country__c == 'CN' || timeentry.Country__c == 'Ireland')
                    {
                        String UKWC = '';
                        
                        if (!String.isBlank(timeentry.WCExtract__c))
                        {
                            ukwc = timeentry.WCExtract__c;
                        }
                        if (timeentry.Activity_Description__c != null && ( !timeentry.Activity_Description__c.contains('MEAL') && !timeentry.Activity_Description__c.contains('Lunch') ) )
                        {
                            String key = '';
                            if (timeentry.Country__c == 'UK' || timeentry.Country__c == 'United Kingdom' || timeentry.Country__c == 'GB' || timeentry.Country__c == 'IE' || timeentry.Country__c == 'Ireland'){
                                if (!String.isBlank(ukwc))
                                {
                                    ukwc = ukwc.replace('GB5','095');
                                    ukwc = ukwc.replace('GB6','096');
                                    ukwc = ukwc.replace('GB0','010');
                                }
                            }
                            if (incldtename)
                            {
                                key = timeentry.Name+ukwc+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';
                            }
                            else
                            {
                                key = ukwc+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';    
                            }
                            //extracttxt.add(key);
                            extract=extract+key+'\n';
                            //getData.add(key);
                            System.Debug('***CM key: '+key);
                        }
                        else if(timeentry.Activity_Description__c == null)
                        {
                            String key = '';
                            if (timeentry.Country__c == 'UK' || timeentry.Country__c == 'United Kingdom' || timeentry.Country__c == 'GB' || timeentry.Country__c == 'IE' || timeentry.Country__c == 'Ireland')
                            {
                                if (!String.isBlank(ukwc))
                                {
                                    ukwc = ukwc.replace('GB5','095');
                                    ukwc = ukwc.replace('GB6','096');
                                    ukwc = ukwc.replace('GB0','010');
                                }
                            }    
                                if (incldtename)
                                {
                                    key = timeentry.Name+ukwc+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';
                                }
                                else
                                {
                                    key = ukwc+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';    
                                }
                                //extracttxt.add(key);
                                extract=extract+key+'\n';
                                //getData.add(key);
                                System.Debug('***CM key: '+key);
                        }
                    }
                    else
                    {
                        String key = '';
                        if (incldtename)
                        {
                            key = timeentry.Name+timeentry.WCExtract__c+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';
                        }
                        else
                        {
                            key = timeentry.WCExtract__c+timeentry.Paystdtext__c+timeentry.StTimeExtract__c+strEndtime+strLabortime+timeentry.WOExtract__c+timeentry.TETYPE__c+timeentry.Activity_Description__c+'\r';    
                        }
                        //extracttxt.add(key);
                        extract=extract+key+'\n';
                        //getData.add(key);
                        System.Debug('***CM key: '+key);
                    }
                }
                else if(timeentry.Country__c == 'Germany' || timeentry.Country__c == 'DE')
                {
                    String Org = '';
                    string paystdate ='';
                    string payeddate ='';
                    Date paystdatedt;
                    Date payeddatedt;
                    string finalpaystdate ='';
                    string finalpayeddate ='';
                    String Labrtimefinal = '';
                    decimal labrtimedec = 0.00;
                    if (timeentry.Timesheet__r.Organization__c != null){
                        org = String.valueOf(timeentry.Timesheet__r.Organization__r.Name);
                    }
                    else
                        org = '';
                    if (timeentry.Payroll_Start_Date__c != null){
                        String paystday = timeentry.Payroll_Start_Date__c.substring(6,8);
                        String paystmnt = timeentry.Payroll_Start_Date__c.substring(4,6);
                        String paystyr = timeentry.Payroll_Start_Date__c.substring(0,4);
                        paystdate = paystday + '.' + paystmnt + '.' + paystyr;
                        paystdatedt = Date.Parse(paystdate);
                        finalpaystdate = String.valueOf(paystdatedt);
                    }
                    else 
                        paystdate = '';
                    if (timeentry.Payroll_End_Date__c != null){
                        String payedday = timeentry.Payroll_End_Date__c.substring(6,8);
                        String payedmnt = timeentry.Payroll_End_Date__c.substring(4,6);
                        String payedyr = timeentry.Payroll_End_Date__c.substring(0,4);
                        payeddate = payedday + '.' + payedmnt + '.' + payedyr;
                        payeddatedt = Date.Parse(payeddate);
                        finalpayeddate = String.valueOf(payeddatedt);
                    }
                    else 
                        payeddate = '';

                    if (timeentry.TotalTime__c != null){
                        String labrtime = String.valueOf(timeentry.TotalTime__c);
                        Labrtimefinal = labrtime.replace('.',',');
                        labrtimedec = decimal.valueOf(labrtime);
                    }
                    else
                    {
                        Labrtimefinal = '';
                    }
                    Decimal finallabtime = 0.00;
                    finallabtime = timeentry.TotalTime__c;
                    String str = String.valueOf(finallabtime);

                    str = str.replace('.',',');
                    headkey = 'ORGANIZATION'+'\t'+'STATUS'+'\t'+'DISTRICT'+'\t'+'BADGE NUMBER'+'\t'+'TECHNICIAN'+'\t'+'ACTIVITY DESCRIPTION'+'\t'+'RECORD TYPE'+'\t'+'START DATE'+'\t'+'START TIME'+'\t'+'END DATE'+'\t'+'END TIME'+'\t'+'LABOR TIME'+'\t'+'EQUIPMENT'+'\t'+'CITY'+'\t'+'WORK ORDER NUMBER'+'\t'+'WEEKLY DOSIMETER READING'+'\t'+'TIMESHEET NAME'+'\t'+'TIMEENTRY NAME'+'\n';
                    String key = org+'\t'+timeentry.Timesheet__r.Status__c+'\t'+timeentry.Timesheet__r.District__r.Name+'\t'+timeentry.Work_Center__c+'\t'+timeentry.Technician__r.Name+'\t'+timeentry.Activity_Description__c+'\t'+timeentry.Record_Type_Name__c+'\t'+finalpaystdate+'\t'+timeentry.Start_Time__c+'\t'+finalpayeddate+'\t'+timeentry.End_Time__c+'\t'+str+'\t'+timeentry.Equipment__c+'\t'+timeentry.City__c+'\t'+timeentry.Work_Order_Number__c+'\t'+timeentry.Timesheet__r.Dosimeter_Reading__c+'\t'+timeentry.timesheet__r.Name+'\t'+timeentry.Name+'\r';
                    key = key.replaceAll('null',' ');
                    //extracttxt.add(key);
                    befheader=befheader+key+'\n';
                    //getData.add(key);
                    System.Debug('***CM key: '+key);
                }
            }     
        }
        //extract = '<table>'+extract+'</table>';
        //return extracttxt;
        if (!String.isBlank(befheader))
        {
            extract = headkey + befheader;
        }
        return extract;
    }
    // returns a list of wrapper objects for the sObjects in the current page set
    public List<wrpTEextract> getTimeEntries() {
        timeentries1 = new List<wrpTEextract>();
        for (SVMXC_Time_Entry__c te : (List<SVMXC_Time_Entry__c>)paginationSetController.getRecords())
        {
            timeentries1.add(new wrpTEextract(te));
        }
        return timeentries1;
    }

    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return paginationSetController.getHasNext();
        }
        set;
    }
    

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return paginationSetController.getHasPrevious();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         paginationSetController.first();
     }

     // returns the last page of records
     public void last() {
        paginationSetController.last();
     }

     // returns the previous page of records
     public void previous() {
        paginationSetController.previous();
     }

     // returns the next page of records
     public void next() {
        paginationSetController.next();
     }    
}
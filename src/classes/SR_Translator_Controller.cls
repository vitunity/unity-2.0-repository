public class SR_Translator_Controller 
{
  
Public String status='Online';
Public String status1='Published';
Public String lang='en_US';
Public String Articletype='Service_Article__kav';
 
public String ArticleId{get;set;}
 
 
Public List<List<SObject>> searchList{get;set;}
Public List<KnowledgeArticleVersion> listArticles {get;set;}
public String ArticleValidationStatus{get; set;}
public String Type{get; set;}
 public string Translatedresponse{get;set;} 
 public string TranslatedSearchtext{get;set;} 
  
 
 
Public Service_Article__kav servicearticle{get;set;}
Public List<wrapperclass> wrapperlist= new list<wrapperclass>();
 
public List<CaseArticle> ArticleCaselist = new List<CaseArticle>();
 
 public id CaseId;
 
public String keyPrefix{get;set;}
public String Searchtext{get;set;}
public String TranslationSearchtext{get;set;}
 
 
Map<wrapperchild,List<SelectOption>> MapOfParntChld; // Data category
public List<wrapperchild> Mapkeyval; //Data category

public List<wrapperchild> getMapkeyval()
{
    Mapkeyval = new List<wrapperchild>();
    mapofchild();
    for(wrapperchild child : MapOfParntChld.keyset())
     {
        wrapperchild tempvalue = new wrapperchild();
        tempvalue = child;
        tempvalue.optionslist = new List<SelectOption>();
        tempvalue.optionslist.add(new selectOption('---None---','---None---'));
        tempvalue.optionslist.addall(MapOfParntChld.get(child));
        tempvalue.optionslist.sort();
        Mapkeyval.add(tempvalue);
     }
    system.debug('@@@@@@@@@@' + Mapkeyval);
    system.debug('MAP----->' + MapOfParntChld);
    return Mapkeyval;
}

public SR_Translator_Controller()
{

}

public list<wrapperclass> getwrapperlist()
    {
        return wrapperlist;
    }
    public list<wrapperclass> setwrapperlist()
    {
        return wrapperlist;
    }

     
    
    // Code used for showing data categories as filter
    public void mapofchild(){
    
    MapOfParntChld = new Map<wrapperchild,List<SelectOption>>();
    List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
   // Map<wrapperchild,List<wrapperchild>> 
    Map<wrapperchild,List<wrapperchild>> MapOfParntChldwrap = new Map<wrapperchild,List<wrapperchild>>();

    List<String> objType = new List<String>();
           
             objType.add('KnowledgeArticleVersion');
             //Describe Call
             List<DescribeDataCategoryGroupResult> describeCategoryResult = Schema.describeDataCategoryGroups(objType);
    system.debug('#####' + describeCategoryResult);
    List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
    //Looping throught the first describe result to create
    //the list of pairs for the second describe call
    for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
        DataCategoryGroupSobjectTypePair p =
        new DataCategoryGroupSobjectTypePair();
        p.setSobject(singleResult.getSobject());
        p.setDataCategoryGroupName(singleResult.getName());
        pairs.add(p);
    }
    //describeDataCategoryGroupStructures()
    describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);
    //DCSG  = describeCategoryStructureResult ;
    system.debug('$$$$$$$' + describeCategoryStructureResult);
    for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult ){
        wrapperchild ParentRec = new wrapperchild();
        //Get name of the associated Sobject
        singleResult.getSobject();
        //Get the name of the data category group
        ParentRec.CategoryName = singleResult.getName() + '__c'; // adding __c as this will be the value of the selected picklist value
        //Get the name of the data category group
        ParentRec.CategoryLabel = singleResult.getLabel();
        //Get the description of the data category group
        singleResult.getDescription();
        //Get the top level categories
        DataCategory [] toplevelCategories = singleResult.getTopCategories();
        //Recursively get all the categories
        List<DataCategory> allCategories = getAllCategories(toplevelCategories);
        system.debug('ALLCHILDREN----->' + allCategories);
        List<wrapperchild> Childlist = new List<wrapperchild>();
        for(DataCategory category : allCategories) {
            //Get the list of sub categories in the category
            DataCategory [] childCategories = category.getChildCategories();
            system.debug('CHILDCATEGORIES---->' + childCategories);
            for(DataCategory child : childCategories){
                 wrapperchild childrec1 = new wrapperchild();
                 childrec1.CategoryName = child.getName() + '__c';
                 childrec1.CategoryLabel = child.getLabel();
                 Childlist.add(childrec1);
            }
        }
        MapOfParntChldwrap.put(ParentRec,Childlist);
    }
    //Mapkeyval.addall(MapOfParntChldwrap.keyset());
    system.debug('CCCCCCCCCCC' + MapOfParntChldwrap);
  for(wrapperchild p : MapOfParntChldwrap.keyset()){
    List<SelectOption> options = new List<SelectOption>();
    for(wrapperchild c: MapOfParntChldwrap.get(p)){
        options.add(new SelectOption(c.CategoryName,c.CategoryLabel));
       }
    MapOfParntChld.put(p,options);
  }
}

//Wrapper class for Creating data categories parent child list
public class wrapperchild{
     public String CategoryName{get;set;}
     public String CategoryLabel{get;set;}
     public String selectedvalue{get;set;}
     public List<SelectOption> optionslist{get;set;}
     public void wrapperchild(){
        optionslist = new List<SelectOption>();
     }
}
// method for getting all the child categories 
private static DataCategory[] getAllCategories(DataCategory [] categories){
      if(categories.isEmpty()){
         return new DataCategory[]{};
      } else {
         DataCategory [] categoriesClone = categories.clone();
         DataCategory category = categoriesClone[0];
         DataCategory[] allCategories = new DataCategory[]{category};
         categoriesClone.remove(0);
         categoriesClone.addAll(category.getChildCategories());
         allCategories.addAll(getAllCategories(categoriesClone));
         return allCategories;
      }
   }
    //End here data category code
      public Pagereference SearchArticle()
       {      
         
        String SubjectName;
        System.debug(' Language of user ::--:: ' + UserInfo.getUserId());
        string UserLocale = [select id,LanguageLocaleKey,LocaleSidKey from user where id =:UserInfo.getUserId()].LanguageLocaleKey;
        System.debug(' Language of user ::--:: ' + UserLocale);
        if(UserLocale.contains('en_')){
           UserLocale = 'en';
           Translatedresponse = TranslationSearchtext;
           SubjectName = TranslationSearchtext;
           System.debug('Inside English');
        }
        else
        {
            UserLocale=UserLocale.substring(0,2);
        
        
            System.debug(' Language of user ::--::2222 ' + UserLocale);
            
            //string  sourceTargetLanguage =UserLocale + '&#124;' + 'en' ;
             
            // system.debug('sourceTargetLanguage-======'+sourceTargetLanguage );
            system.debug('TranslationSearchtext-======'+TranslationSearchtext );
            if(TranslationSearchtext!=null)
            {
                TranslateService TranslateServiceobj = new TranslateService();
                system.debug('class-======'+TranslationSearchtext );
                Translatedresponse = TranslateServiceobj.getTranslatedstring(TranslationSearchtext,UserLocale); 
            }
        }
        system.debug('Translatedresponse-======'+Translatedresponse );
        searchList = new List<List<SObject>>();
        system.debug('DDDDDDDD' + Mapkeyval);      
        listArticles = new List<KnowledgeArticleVersion>();
        
        if(TranslationSearchtext!=null){
          SubjectName = Translatedresponse;//Apexpages.currentPage().getParameters().get('SubjectName');
        }
        else{
          SubjectName = Searchtext;
        }
        system.debug('SubjectName-======'+SubjectName );
        String Validaton = ArticleValidationStatus;//Apexpages.currentPage().getParameters().get('validation'); 
        String type = Type;//Apexpages.currentPage().getParameters().get('type'); 
        system.debug('Validaton---->'+ArticleValidationStatus);
        //system.debug('Type---->'+Type);  
         //SubjectName = 'test'; 
         if(SubjectName == null)
                SubjectName = Apexpages.currentPage().getParameters().get('SubjectName');
                        
        if(SubjectName.length()>2)
        {  
         SubjectName = String.escapeSingleQuotes(SubjectName);
         SubjectName.replaceall('\\s','or'); //for searching based on single words by harshita US4260
         //String searchquery = 'FIND {"'+SubjectName+'"} IN ALL FIELDS RETURNING Service_Article__kav(ID,KnowledgeArticleId,ArticleNumber,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang)';
         String searchquery = 'FIND {'+SubjectName+'} IN ALL FIELDS RETURNING KnowledgeArticleVersion(ID,KnowledgeArticleId,LastPublishedDate,ArticleType,FirstPublishedDate,ArticleNumber,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang)';
         if(SubjectName.length()>2 && Validaton!=Null && Validaton!='--None--')
         {
         //searchquery = 'FIND {"'+SubjectName+'"} IN ALL FIELDS RETURNING Service_Article__kav(ID,KnowledgeArticleId,ArticleNumber,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang and ValidationStatus=:Validaton)';
         searchquery = 'FIND {'+SubjectName+'} IN ALL FIELDS RETURNING KnowledgeArticleVersion(ID,KnowledgeArticleId,LastPublishedDate,ArticleNumber,ArticleType,FirstPublishedDate,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang and ValidationStatus=:Validaton)'; 
         }
         
          if(SubjectName.length()>2 && type!=Null && type!='--None--')
         {
          //searchquery = 'FIND {"'+SubjectName+'"} IN ALL FIELDS RETURNING Service_Article__kav(ID,KnowledgeArticleId,ArticleNumber,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang and Approval_Type__c=:type)';
          searchquery = 'FIND {'+SubjectName+'} IN ALL FIELDS RETURNING KnowledgeArticleVersion(ID,KnowledgeArticleId,LastPublishedDate,ArticleNumber,ArticleType,FirstPublishedDate,Title,Summary,UrlName where PublishStatus=:status AND Language=:type)';
         }
         
         if(SubjectName.length()>2 && type!=Null && type!='--None--' && Validaton!=Null && Validaton!='--None--')
         {
          //searchquery = 'FIND {"'+SubjectName+'"} IN ALL FIELDS RETURNING Service_Article__kav(ID,KnowledgeArticleId,ArticleNumber,Title,Summary,UrlName where PublishStatus=:status AND Language=:lang  and ValidationStatus=:Validaton and Approval_Type__c=:type)';
          searchquery = 'FIND {'+SubjectName+'} IN ALL FIELDS RETURNING KnowledgeArticleVersion(ID,KnowledgeArticleId,LastPublishedDate,ArticleNumber,ArticleType,FirstPublishedDate,Title,Summary,UrlName where PublishStatus=:status AND Language=:type  and ValidationStatus=:Validaton)';
         }
         
         String datacategory = '@';
         
         for(wrapperchild child : Mapkeyval){
            system.debug('DDDDDDDDDDDDD'+(child.selectedvalue != null) + 'BBBBBBB'+(child.selectedvalue != '---None---') + 'VVVVVV'+ (datacategory != '@' )+ 'BBBBB'+child.selectedvalue + 'FFFF'+  datacategory);
            if(child.selectedvalue != '---None---' && child.selectedvalue != null){
             if(datacategory != '@')
              datacategory = datacategory + ' and ' + child.CategoryName + ' AT ' + child.selectedvalue ;
             else
               datacategory = child.CategoryName + ' AT ' + child.selectedvalue ;
            }
         }
         
         if(datacategory != '@'){
           searchquery = searchquery + ' WITH DATA CATEGORY '+ datacategory;
         }
          system.debug('%%%%%%%'+searchquery );
        searchList = search.query(searchquery);
        //system.debug('%%%%%%%'+searchList)
        listArticles  = ((List<KnowledgeArticleVersion>)searchList[0]);
        }
        
        system.debug('#####'+listArticles);
        wrapperlist.clear();
        for(KnowledgeArticleVersion articlealias : listArticles)
        {
          wrapperclass w1= new wrapperclass(articlealias,false);
          wrapperlist.add(w1);  
        }
        
        
        
        return null;
       }
       
        public List<SelectOption> getItems() 
        {
      
          List<SelectOption> options = new List<SelectOption>();
              
             options.add(new SelectOption('--None--','--None--'));
              options.add(new SelectOption('Approved','Approved'));
              options.add(new SelectOption('Rejected','Rejected'));
              options.add(new SelectOption('In Reviewed','In Reviewed'));
              return options;
        }
   
   public List<SelectOption> gettypeItems() 
    {
      
      List<SelectOption> typeoptions = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = KnowledgeArticleVersion.Language.getDescribe();
                   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    typeoptions.add(new SelectOption('--None--','--None--'));   
                   for( Schema.PicklistEntry f : ple)
                   {
                      typeoptions.add(new SelectOption(f.getValue(),f.getLabel()));
                   } 
         
          return typeoptions;
    }
       
       
         
     
       
       public class wrapperclass
      {
        public boolean check{get;set;}
        public  KnowledgeArticleVersion servicearticle{get;set;}
        
        public wrapperclass(KnowledgeArticleVersion service, boolean f)
        {
            servicearticle=service;
            check=f;
            
        }
      } 
     
}
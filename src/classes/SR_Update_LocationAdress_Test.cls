@isTest(SeeAllData=true)
public class SR_Update_LocationAdress_Test
{
    public static Id recServTeam_Equip = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();

    public static testMethod void SR_Update_LocationAdressTest()
    {
        //insert Account record 1
        Account varAcc = AccountTestData.createAccount();
        varAcc.AccountNumber = '12345A';
        varAcc.Functional_Location__c = 'FUNC123';
        insert varAcc;

        //insert ERP Partner record
        ERP_Partner__c varPartner = new ERP_Partner__c();
        varPartner.name = 'Test Partner';
        varPartner.Partner_Number__c = 'HH1122';
        insert varPartner;

        //insert ERP Partner Association record
        ERP_Partner_Association__c varAssociation = new ERP_Partner_Association__c();
        varAssociation.name = 'Test Association';
        varAssociation.ERP_Partner__c = varPartner.Id;
        varAssociation.Partner_Function__c = 'Z1=Site Partner';
        varAssociation.Customer_Account__c = varAcc.id;
        insert varAssociation;

        //insert ERP Org record
        ERP_Org__c varOrg = new ERP_Org__c();
        varOrg.name = 'Test Org';
        varOrg.Sales_Org__c = 'ABC123';
        insert varOrg;

        //insert country record
        Country__c varCountry = new Country__c();
        varCountry.name = 'India';
        varCountry.SAP_Code__c = 'A1B2C3';
        varCountry.ISO_Code_3__c = 'ABCTEST';
        insert varCountry;

        //insert Service Team record
        SVMXC__Service_Group__c ServiceTeam = SR_testdata.createServiceTeam();
        ServiceTeam.RecordTypeId = recServTeam_Equip;
        insert ServiceTeam;

        //insert technician record
        SVMXC__Service_Group_Members__c varTech = SR_testdata.createTech();
        varTech.SVMXC__Service_Group__c  = ServiceTeam.Id;
        //varTech.User__c = systemuser.Id;
        varTech.User__c = userInfo.getUserId();
        varTech.SVMXC__Street__c = 'Test Street';
        varTech.SVMXC__City__c = 'Test City';
        varTech.SVMXC__State__c = 'Test State';
        varTech.SVMXC__Zip__c = '302015';
        varTech.SVMXC__Country__c = 'India';
        varTech.SVMXC__Email__c = 'standarduser@testorg.com';
        varTech.ERP_Work_Center__c = 'H123H';
        insert varTech;

        //insert location record
        SVMXC__Site__c varLoc = SR_testdata.createsite();
        varLoc.SVMXC__Account__c = varAcc.Id;
        varLoc.Country__c = varCountry.Id;
        varLoc.ERP_Country_Code__c = 'A1B2C3';
        varLoc.ERP_CSS_District__c = 'abc';
        varLoc.Sales_Org__c = 'ABC123';
        varLoc.SVMXC__Location_Type__c = 'Field';
        varLoc.ERP_Site_Partner_Code__c = varAssociation.ID;
        varLoc.Work_Center__c = 'H123H';
        varLoc.ERP_Functional_Location__c = 'FUNC123';
        insert varLoc;

        //insert Product record
        Product2 varProd = new Product2();
        varProd.Name = 'Test Product';
        insert varProd;

        //insert installed product record
        SVMXC__Installed_Product__c varIP = new SVMXC__Installed_Product__c();
        varIP.Name = 'H0000';
        varIP.SVMXC__Product__c = varProd.Id;
        varIP.ERP_Functional_Location__c = 'FUNC123';
        varIP.SVMXC__Preferred_Technician__c = varTech.Id;
        insert varIP;

        //update 1 location record
        varLoc.ERP_CSS_District__c = 'xyz';
        varLoc.ERP_Site_Partner_Code__c = 'HH1122';
        varLoc.SVMXC__Country__c = 'India';
        update varLoc;

        //update 2 location record
        varLoc.ERP_Site_Partner_Code__c = '12345A';
        varLoc.SVMXC__Country__c = 'A1B2C3';
        update varLoc;

        //update 3 location record
        varLoc.SVMXC__Country__c = 'ABCTEST';
        update varLoc;

        //update 4 location record
        varLoc.SVMXC__Country__c = 'ABC123';
        varLoc.SVMXC__Account__c = null;
        varLoc.ERP_Site_Partner_Code__c = null;
        varLoc.ERP_Sold_To_Code__c = null;
        update varLoc;
    }

    public static testMethod void updateAssociatedAccountTest()
    {
        //insert Account record
        Account varAcc = AccountTestData.createAccount();
        varAcc.Has_Location__c = true;  
        varAcc.Site = 'varLoc';       
        varAcc.AccountNumber = '12345A';
        insert varAcc;

        //insert Account record without location
        Account varAcc2 = AccountTestData.createAccount();
        varAcc2.Has_Location__c = true;
        varAcc2.AccountNumber = '12345B';
        insert varAcc2;

        //insert location record
        SVMXC__Site__c varLoc = SR_testdata.createsite();
        varLoc.SVMXC__Account__c = varAcc.Id;
        varLoc.ERP_Country_Code__c = 'A1B2C3';
        varLoc.ERP_CSS_District__c = 'abc';
        varLoc.ERP_Sold_To_Code__c = '12345A';
        varLoc.Sales_Org__c = 'ABC123';
        varLoc.SVMXC__Location_Type__c = 'Field';
        varLoc.Work_Center__c = 'H123H';
        insert varLoc;

        //insert location record B
        SVMXC__Site__c varLoc1 = SR_testdata.createsite();
        varLoc1.SVMXC__Account__c = varAcc2.Id;
        varLoc1.ERP_Country_Code__c = 'A1B2C3';
        varLoc1.ERP_CSS_District__c = 'abc';
        varLoc1.Sales_Org__c = 'ABC123';
        varLoc1.SVMXC__Location_Type__c = 'Field';
        varLoc1.Work_Center__c = 'H123H';
        varLoc1.ERP_Sold_To_Code__c = '12345B';
        insert varLoc1;

        //update location record
        varLoc.ERP_Site_Partner_Code__c = '12345B';
        varLoc.SVMXC__Country__c = 'A1B2C3';
        update varLoc;

        //delete location record
        delete varLoc;
    }
}
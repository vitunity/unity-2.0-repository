public class SR_WorkOrder_Customer_Values
{
    public String Customer_date{get;set;}
    public String Customer_date_end{get;set;}
    public String Customer_req_strt_date{get;set;}
    public String Customer_req_end_date{get;set;}
    public SVMXC__Service_Order__c current_workOrder{get;set;}
    public String AMorPM{get;set;}
    public Integer hour_time{get;set;}
    public String hour_timeOfMal{get;set;}
    public String min_time{get;set;}
    public String time_AmPM{get;set;}
    public String hour_timeOfMalStart{get;set;}
    public String min_timeMalSart{get;set;}
    public String time_AmPMMalStart{get;set;}
    public String hour_timeOfReqStart{get;set;}
    public String min_timeReqSart{get;set;}
    public String time_AmPMReqStart{get;set;}
    public String hour_timeOfReqEnd{get;set;}
    public String min_timeReqEnd{get;set;}
    public String time_AmPMReqEnd{get;set;}
    public String errorflag{get;set;}
    public String tabid{get;set;}
    public boolean ShowError{get;set;}
    public boolean IsClosed{get;set;}
    public ID profileid;
    public string profileName;
    Map<Id, string> mapTech_ERPTimezone = new Map<Id, string>();    //DE2249
    Map<string, ERP_Timezone__c> mapERPTimeZone = new Map<string, ERP_Timezone__c>();   //DE2249
    string dtformat;
   
    
    public SR_WorkOrder_Customer_Values(ApexPages.StandardController controller) 
    {    ShowError = false;
         IsClosed = false;   
        //Customer_date = 'this is the date';
        current_workOrder = new SVMXC__Service_Order__c();
        List<String> Split_Malfunction_Start = new list<String>();
        List<string> DateValues = new List<String>();
        List<String> Min1 = new List<String>();
        List<String> Split_Requested_Start = new List<String>();
        List<string> DateVal = new List<String>();
        List<String> Min = new list<String>();
        List<String> Split_Requested_End = new List<String>();
        List<string> DateVa = new List<String>();
        List<String> Min2 = new List<String>();
        errorflag = ApexPages.currentPage().getParameters().get('PrimarytabId');
        tabid = ApexPages.currentPage().getParameters().get('tabid');
        try
        {
            String workOrderId = ApexPages.currentpage().getparameters().get('id');
            current_workOrder = [Select id, Notification_Interface_Status__c, SVMXC__Case__c, SVMXC__Case__r.CaseNumber, SVMXC__Case__r.AccountId, SVMXC__Case__r.Account.ERP_Timezone__c, Acceptance_Date1__c, Date_of_Intervention1__c, Next_Training_Trip_Reminder1__c,Acceptance_Date__c, Date_of_Intervention__c, Next_Training_Trip_Reminder__c, ERP_Requested_Start_Date_Time__c, Customer_Requested_Start_Date_Time__c, Customer_Requested_End_Date_Time__c, Customer_Malfunction_Start_Date_Time__c, SVMXC__Preferred_End_Time__c, SVMXC__Preferred_Start_Time__c, Malfunction_Start__c,SVMXC__Company__r.country__c from SVMXC__Service_Order__c where id =: workOrderId];
            if(current_workOrder.SVMXC__Case__c != null && current_workOrder.SVMXC__Case__r.Account != null)
            {
                    mapTech_ERPTimezone.put(current_workOrder.SVMXC__Case__r.AccountId, current_workOrder.SVMXC__Case__r.Account.ERP_Timezone__c);
            }
            Map<string, CountryDateFormat__c> Dateformatsetting = CountryDateFormat__c.getAll();
            dtformat = Dateformatsetting.get('Default').Date_Format__c;
            if(current_workOrder.SVMXC__Company__r.country__c <> null){
                if(Dateformatsetting.containsKey(current_workOrder.SVMXC__Company__r.country__c )){
                    dtformat = Dateformatsetting.get(current_workOrder.SVMXC__Company__r.country__c).Date_Format__c;
                }
            }
            String customerTimeZone = '';
            if(mapTech_ERPTimezone.size() > 0)
            {
                for(ERP_Timezone__c varTZ : [select Id, Name, Salesforce_timezone__c from ERP_Timezone__c where Name in : mapTech_ERPTimezone.values()])
                {
                    customerTimeZone = varTZ.Salesforce_timezone__c.substring(varTZ.Salesforce_timezone__c.indexof('(') + 1, varTZ.Salesforce_timezone__c.indexof(')'));
                }         
            }
/************************************************For Hiding Modify Date/Time button for closed cases for Service Users*********************************************************/
/************************************************For Malfunction Start*********************************************************/     
           
            if(current_workOrder.Acceptance_Date1__c != null)
            {   
                String adateStr = current_workOrder.Acceptance_Date1__c.format('MM/dd/yyyy',customerTimeZone);
                String [] adateStrArray = adateStr.split('/',-2);
                //current_workOrder.Acceptance_Date__c = date.parse((current_workOrder.Acceptance_Date1__c).format('MM/dd/yyyy',customerTimeZone));
                //current_workOrder.Acceptance_Date__c = date.newinstance(current_workOrder.Acceptance_Date1__c.year(), current_workOrder.Acceptance_Date1__c.month(), current_workOrder.Acceptance_Date1__c.day());
                current_workOrder.Acceptance_Date__c = Date.valueOf(adateStrArray[2]+'-'+adateStrArray[0]+'-'+adateStrArray[1]); 
                
                string AcceptanceTime =  (current_workOrder.Acceptance_Date1__c).format('hh:mm a',customerTimeZone);
                if(AcceptanceTime <> null)
                {    
                    if(AcceptanceTime.split(' ').size() > 1){
                        String AcceptanceHHMM = AcceptanceTime.split(' ')[0];
                        time_AmPMMalStart = AcceptanceTime.split(' ')[1];
                        if(AcceptanceHHMM.split(':').size() > 0){
                            hour_timeOfMalStart = AcceptanceHHMM.split(':')[0];
                            min_timeMalSart = AcceptanceHHMM.split(':')[1];                         
                        }
                    }                    
                }
             }

/***********************************************For Requested Start**********************************************************/
            
            if(current_workOrder.Date_of_Intervention1__c != null)
            {    
                String dofdateStr = current_workOrder.Date_of_Intervention1__c.format('MM/dd/yyyy',customerTimeZone);
                String [] dofdateStrArray = dofdateStr.split('/',-2);
                current_workOrder.Date_of_Intervention__c = Date.valueOf(dofdateStrArray[2]+'-'+dofdateStrArray[0]+'-'+dofdateStrArray[1]); 
                //current_workOrder.Date_of_Intervention__c = date.parse((current_workOrder.Date_of_Intervention1__c).format('MM/dd/yyyy',customerTimeZone));
                //current_workOrder.Date_of_Intervention__c = date.newinstance(current_workOrder.Date_of_Intervention1__c.year(), current_workOrder.Date_of_Intervention1__c.month(), current_workOrder.Date_of_Intervention1__c.day());
                string DateIntervention =  (current_workOrder.Date_of_Intervention1__c).format('hh:mm a',customerTimeZone);
                if(DateIntervention <> null)
                {    
                    if(DateIntervention.split(' ').size() > 1){
                        String AcceptanceHHMM = DateIntervention.split(' ')[0];
                        time_AmPMReqStart = DateIntervention.split(' ')[1];
                        if(AcceptanceHHMM.split(':').size() > 0){
                            hour_timeOfReqStart = AcceptanceHHMM.split(':')[0];
                            min_timeReqSart = AcceptanceHHMM.split(':')[1];                         
                        }
                    }                    
                }
            }
            
/************************************************For Requested End*********************************************************/
            if(current_workOrder.Next_Training_Trip_Reminder1__c != null)
            {   
                
                String nofdateStr = current_workOrder.Next_Training_Trip_Reminder1__c.format('MM/dd/yyyy',customerTimeZone);
                String [] nofdateStrArray = nofdateStr.split('/',-2);
                current_workOrder.Next_Training_Trip_Reminder__c = Date.valueOf(nofdateStrArray[2]+'-'+nofdateStrArray[0]+'-'+nofdateStrArray[1]);
                //current_workOrder.Next_Training_Trip_Reminder__c = date.parse((current_workOrder.Next_Training_Trip_Reminder1__c).format('MM/dd/yyyy',customerTimeZone));
                //current_workOrder.Next_Training_Trip_Reminder__c = date.newinstance(current_workOrder.Next_Training_Trip_Reminder1__c.year(), current_workOrder.Next_Training_Trip_Reminder1__c.month(), current_workOrder.Next_Training_Trip_Reminder1__c.day());
                string NextTrainingTripReminder =  (current_workOrder.Next_Training_Trip_Reminder1__c).format('hh:mm a',customerTimeZone);
                if(NextTrainingTripReminder <> null)
                {    
                    if(NextTrainingTripReminder.split(' ').size() > 1){
                        String AcceptanceHHMM = NextTrainingTripReminder.split(' ')[0];
                        time_AmPMReqEnd = NextTrainingTripReminder.split(' ')[1];
                        if(AcceptanceHHMM.split(':').size() > 0){
                            hour_timeOfReqEnd = AcceptanceHHMM.split(':')[0];
                            min_timeReqEnd = AcceptanceHHMM.split(':')[1];                          
                        }
                    }                    
                }
            }
            
        }catch(Exception e)
        {
            ShowError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()));
            //return null;
        }
    }
    
/*********************************************For Hour picklist************************************************************/
            public List<SelectOption> getHour() {
                List<SelectOption> options3 = new List<SelectOption>();
                options3.add(new SelectOption('hour','hour'));
                options3.add(new SelectOption('01','01'));
                options3.add(new SelectOption('02','02'));
                options3.add(new SelectOption('03','03'));
                options3.add(new SelectOption('04','04'));
                options3.add(new SelectOption('05','05'));
                options3.add(new SelectOption('06','06'));
                options3.add(new SelectOption('07','07'));
                options3.add(new SelectOption('08','08'));
                options3.add(new SelectOption('09','09'));
                options3.add(new SelectOption('10','10'));
                options3.add(new SelectOption('11','11'));
                options3.add(new SelectOption('12','12'));
                return options3;
            }
            
/*********************************************For Minutes picklist************************************************************/            
            
            public List<SelectOption> getMin() {
                List<SelectOption> options4 = new List<SelectOption>();
                options4.add(new SelectOption('min','min'));
                options4.add(new SelectOption('00','00'));
                options4.add(new SelectOption('01','01'));
                options4.add(new SelectOption('02','02'));
                options4.add(new SelectOption('03','03'));
                options4.add(new SelectOption('04','04'));
                options4.add(new SelectOption('05','05'));
                options4.add(new SelectOption('06','06'));
                options4.add(new SelectOption('07','07'));
                options4.add(new SelectOption('08','08'));
                options4.add(new SelectOption('09','09'));
                options4.add(new SelectOption('10','10'));
                options4.add(new SelectOption('11','11'));
                options4.add(new SelectOption('12','12'));
                options4.add(new SelectOption('13','13'));
                options4.add(new SelectOption('14','14'));
                options4.add(new SelectOption('15','15'));
                options4.add(new SelectOption('16','16'));
                options4.add(new SelectOption('17','17'));
                options4.add(new SelectOption('18','18'));
                options4.add(new SelectOption('19','19'));
                options4.add(new SelectOption('20','20'));
                options4.add(new SelectOption('21','21'));
                options4.add(new SelectOption('22','22'));
                options4.add(new SelectOption('23','23'));
                options4.add(new SelectOption('24','24'));
                options4.add(new SelectOption('25','25'));
                options4.add(new SelectOption('26','26'));
                options4.add(new SelectOption('27','27'));
                options4.add(new SelectOption('28','28'));
                options4.add(new SelectOption('29','29'));
                options4.add(new SelectOption('30','30'));
                options4.add(new SelectOption('31','31'));
                options4.add(new SelectOption('32','32'));
                options4.add(new SelectOption('33','33'));
                options4.add(new SelectOption('34','34'));
                options4.add(new SelectOption('35','35'));
                options4.add(new SelectOption('36','36'));
                options4.add(new SelectOption('37','37'));
                options4.add(new SelectOption('38','38'));
                options4.add(new SelectOption('39','39'));
                options4.add(new SelectOption('40','40'));
                options4.add(new SelectOption('41','41'));
                options4.add(new SelectOption('42','42'));
                options4.add(new SelectOption('43','43'));
                options4.add(new SelectOption('44','44'));
                options4.add(new SelectOption('45','45'));
                options4.add(new SelectOption('46','46'));
                options4.add(new SelectOption('47','47'));
                options4.add(new SelectOption('48','48'));
                options4.add(new SelectOption('49','49'));
                options4.add(new SelectOption('50','50'));
                options4.add(new SelectOption('51','51'));
                options4.add(new SelectOption('52','52'));
                options4.add(new SelectOption('53','53'));
                options4.add(new SelectOption('54','54'));
                options4.add(new SelectOption('55','55'));
                options4.add(new SelectOption('56','56'));
                options4.add(new SelectOption('57','57'));
                options4.add(new SelectOption('58','58'));
                options4.add(new SelectOption('59','59'));
                return options4;
            }
            
/*********************************************For AM And PM picklist************************************************************/             
            
            public List<SelectOption> getAmPM() {
                List<SelectOption> options5 = new List<SelectOption>();
                options5.add(new SelectOption('AM','AM'));
                options5.add(new SelectOption('PM','PM'));
                return options5;
            }

        
   
/*********************************************For Save Method********************************************************************/    
      public pagereference saveDates()
      {     
        try
        {

            String customerTimeZone = '';
            if(mapTech_ERPTimezone.size() > 0)
            {
                for(ERP_Timezone__c varTZ : [select Id, Name, Salesforce_timezone__c from ERP_Timezone__c where Name in : mapTech_ERPTimezone.values()])
                {
                    customerTimeZone = varTZ.Salesforce_timezone__c.substring(varTZ.Salesforce_timezone__c.indexof('(') + 1, varTZ.Salesforce_timezone__c.indexof(')'));
                }         
            }
            System.TimeZone userTimeZone = UserInfo.getTimeZone(); 
            ShowError = false;
            IF(hour_timeOfMalStart == 'hour' || min_timeMalSart == 'min' || hour_timeOfReqStart == 'hour' || min_timeReqSart == 'min' || hour_timeOfReqEnd =='hour' || min_timeReqEnd == 'min')
            {   ShowError = true;
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Please enter the hour and minute values');
                apexPages.addMessage(msg);
                return null; 
            }
            SVMXC__Service_Order__c updateWorkOrder = new SVMXC__Service_Order__c(id = current_workOrder.id);
/*********************************************For Malfunction Start********************************************************************/            
            IF(current_workOrder.Acceptance_Date__c!=null && hour_timeOfMalStart!=null && min_timeMalSart !=null && time_AmPMMalStart!=null)
            {   
                string str_cusMalfunSDT = current_workOrder.Acceptance_Date__c.day() + '-' + convertMonthIntNumberToName(current_workOrder.Acceptance_Date__c.month()) + '-' + current_workOrder.Acceptance_Date__c.year() +' '+  hour_timeOfMalStart+':'+ min_timeMalSart +' '+ time_AmPMMalStart;
                Datetime dt_cusMalfunSDT = constructDateTime(str_cusMalfunSDT);
                Datetime dt_cusMalfunSDTGMT = constructDateTimeGMT(str_cusMalfunSDT);
                updateWorkOrder.Customer_Malfunction_Start_Date_Time__c =  dt_cusMalfunSDT.format(dtformat);
                if(String.isNotBlank(customerTimeZone))
                {
                    updateWorkOrder.Malfunction_Start__c = gettimezonecorrection(constructDateTime(str_cusMalfunSDT), customerTimeZone, userTimeZone);
                }
                updateWorkOrder.Acceptance_Date1__c = updateWorkOrder.Malfunction_Start__c;
                updateWorkOrder.ERP_Malfunction_Start_Date_Time__c = dt_cusMalfunSDTGMT;  
            }
/*********************************************For Requested Start********************************************************************/            

            IF(current_workOrder.Date_of_Intervention__c!=null && hour_timeOfReqStart!=null && min_timeReqSart !=null && time_AmPMReqStart!=null)
            {
                string str_cusReqSDT = current_workOrder.Date_of_Intervention__c.day() + '-' + convertMonthIntNumberToName(current_workOrder.Date_of_Intervention__c.month()) + '-' + current_workOrder.Date_of_Intervention__c.year() +' '+hour_timeOfReqStart +':'+ min_timeReqSart+' '+ time_AmPMReqStart;
                Datetime dt_cusReqSDT = constructDateTime(str_cusReqSDT);
                Datetime dt_cusReqSDTGMT = constructDateTimeGMT(str_cusReqSDT);
                updateWorkOrder.Customer_Requested_Start_Date_Time__c =  dt_cusReqSDT.format(dtformat);
                if(String.isNotBlank(customerTimeZone))
                {
                    updateWorkOrder.SVMXC__Preferred_Start_Time__c = gettimezonecorrection(constructDateTime(str_cusReqSDT), customerTimeZone, userTimeZone);
                }
                updateWorkOrder.Date_of_Intervention1__c =  updateWorkOrder.SVMXC__Preferred_Start_Time__c;               
                updateWorkOrder.ERP_Requested_Start_Date_Time__c = dt_cusReqSDTGMT;
            }
/*********************************************For Requested End********************************************************************/            

            IF(current_workOrder.Next_Training_Trip_Reminder__c!=null && hour_timeOfReqEnd!=null && min_timeReqEnd !=null && time_AmPMReqEnd!=null)
            {
                string str_cusReqEDT = current_workOrder.Next_Training_Trip_Reminder__c.day() + '-' + convertMonthIntNumberToName(current_workOrder.Next_Training_Trip_Reminder__c.month()) + '-' + current_workOrder.Next_Training_Trip_Reminder__c.year() +' '+ hour_timeOfReqEnd +':'+ min_timeReqEnd +' '+ time_AmPMReqEnd;              
                Datetime dt_cusReqEDT = constructDateTime(str_cusReqEDT);
                Datetime dt_cusReqEDTGMT = constructDateTimeGMT(str_cusReqEDT);
                updateWorkOrder.Customer_Requested_End_Date_Time__c =  dt_cusReqEDT.format(dtformat);
                            
                if(String.isNotBlank(customerTimeZone))
                {
                    updateWorkOrder.SVMXC__Preferred_End_Time__c = gettimezonecorrection(constructDateTime(str_cusReqEDT), customerTimeZone, userTimeZone);
                } 

                updateWorkOrder.Next_Training_Trip_Reminder1__c = updateWorkOrder.SVMXC__Preferred_End_Time__c;                
                updateWorkOrder.ERP_Requested_End_Date_Time__c = dt_cusReqEDTGMT;          
            }

            updateWorkOrder.Notification_Interface_Status__c = 'Process';
            update updateWorkOrder;
        }
        catch(Exception e)
        {
            String errMsg = e.getMessage();
            ShowError = false;   
            if(errMsg.Contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
            {               
                string customValid_begin = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
                integer intIndexOfErr = errMsg.indexOf(customValid_begin) + 35;
                string customValid_end = errMsg.substring(intIndexOfErr);
                integer intIndexOfErrEnd;
                if(customValid_end.contains(':'))
                    intIndexOfErrEnd = customValid_end.indexOf(':');
                
                ShowError = true;
                if(intIndexOfErrEnd != null)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg.substring(intIndexOfErr, (intIndexOfErr +intIndexOfErrEnd))));
                else
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, errMsg.substring(intIndexOfErr)));
            }
            else
            {
                ShowError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'The following exception has occurred: ' + errMsg));
            }
            return null;
        }
        
        return null;
        
    }
   
    public DateTime gettimezonecorrection(DateTime dt,String CustomerTZ, system.Timezone UserTZ  )
    {
        string  customerDateTimeString = dt.format('yyyy-MM-dd HH:mm:ss');
        DateTime customerDateTime = DateTime.valueof(customerDateTimeString);
        string customerTimeZoneSidId = CustomerTZ;
        System.TimeZone customerTimeZone = System.TimeZone.getTimeZone(customerTimeZoneSidId);
        integer offsetToCustomersTimeZone = customerTimeZone.getOffset(customerDateTime);
        System.TimeZone tz = UserTZ;
        integer offsetToUserTimeZone = tz.getOffset(customerDateTime);
        integer correction = offsetToUserTimeZone - offsetToCustomersTimeZone;
        DateTime correctedDateTime = customerDateTime.addMinutes(correction / (1000 * 60));
        return correctedDateTime;
    }
    /**
        create date time instance from datetime string in user timezone
    */
    public DateTime constructDateTime(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = new List<string>();
        if(dateTimeSplitList.size()>=3)
        {
            if(dateTimeSplitList[0].contains('/'))
            dateValueSplit = dateTimeSplitList[0].split('/') ;
            if(dateTimeSplitList[0].contains('-'))
            if(dateTimeSplitList[0] !=null )
            {
                dateValueSplit = dateTimeSplitList[0].split('-') ;
            }
            List<String> timeSplit = dateTimeSplitList[1].split(':');
            String strAMPM = dateTimeSplitList[2];
            DateTime constructedDateTime;
            if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
            {
                constructedDateTime = dateTime.newInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),00, Integer.valueof(timeSplit[1]), 00);
            }
            else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
            {
                constructedDateTime = dateTime.newInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
            }
            else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
            {
                constructedDateTime = dateTime.newInstance(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
            }
            
            return constructedDateTime;
        }
        else
        {
            return null;
        }
    }
    
    /**
        Create GMT instance from datetime string, when set on field, should show teh right date/time when user timezone is GMT, user has to change time zone to GMT to see right values
    */
    public DateTime constructDateTimeGMT(String strDateTime)
    {
        List<String> dateTimeSplitList =  strDateTime.split(' ');
        List<string> dateValueSplit = new List<string>();
        if(dateTimeSplitList.size()>=3)
        {
            if(dateTimeSplitList[0].contains('/'))
            dateValueSplit = dateTimeSplitList[0].split('/') ;
            if(dateTimeSplitList[0].contains('-'))
            if(dateTimeSplitList[0] !=null )
            {
                dateValueSplit = dateTimeSplitList[0].split('-') ;
            }
            List<String> timeSplit = dateTimeSplitList[1].split(':');
            String strAMPM = dateTimeSplitList[2];
            DateTime constructedDateTime;
            if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),00, Integer.valueof(timeSplit[1]), 00);
            }
            else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
            }
            else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
            }
            
            return constructedDateTime;
        }
        else
        {
            return null;
        }
    }   

    @TestVisible private String convertMonthNameToNumber(String strMonthName)
    {
        if(strMonthName == 'Jan')
            return '01';
        else if(strMonthName == 'Feb')
            return '02';
        else if(strMonthName == 'Mar')
            return '03';
        else if(strMonthName == 'Apr')
            return '04';
        else if(strMonthName == 'May')
            return '05';
        else if(strMonthName == 'Jun')
            return '06';
        else if(strMonthName == 'Jul')
            return '07';
        else if(strMonthName == 'Aug')
            return '08';
        else if(strMonthName == 'Sep')
            return '09';
        else if(strMonthName == 'Oct')
            return '10';
        else if(strMonthName == 'Nov')
            return '11';
        else if(strMonthName == 'Dec')
            return '12';
        else
            return '01';
    }
    
    @TestVisible private String convertMonthIntNumberToName(Integer iMonthNumber)
    {
        if(iMonthNumber == 1)
            return 'Jan';
        else if(iMonthNumber == 2)
            return 'Feb';
        else if(iMonthNumber == 3)
            return 'Mar';
        else if(iMonthNumber == 4)
            return 'Apr';
        else if(iMonthNumber == 5)
            return 'May';
        else if(iMonthNumber == 6)
            return 'Jun';
        else if(iMonthNumber == 7)
            return 'Jul';
        else if(iMonthNumber == 8)
            return 'Aug';
        else if(iMonthNumber == 9)
            return 'Sep';
        else if(iMonthNumber == 10)
            return 'Oct';
        else if(iMonthNumber == 11)
            return 'Nov';
        else if(iMonthNumber == 12)
            return 'Dec';
        else
            return 'Jan';
    }
}
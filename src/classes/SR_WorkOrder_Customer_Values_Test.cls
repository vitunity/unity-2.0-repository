@isTest
public class SR_WorkOrder_Customer_Values_Test
{
    public Static Profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
    public static Id fieldServcRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();//record type id of Field Service WO
    public static Id instlRecTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); //record type id of Installation WO
    public static Id UsageConRecTypID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();                                
    static SVMXC__Service_Order_Line__c varWD = new SVMXC__Service_Order_Line__c();
    static SVMXC__Service_Order_Line__c varWD1 = new SVMXC__Service_Order_Line__c();
    static SVMXC__RMA_Shipment_Order__c testPartsOrder = new SVMXC__RMA_Shipment_Order__c();
    static SVMXC__Service_Order__c objWO1 = new SVMXC__Service_Order__c();
    static SVMXC__RMA_Shipment_Line__c varPOL = new SVMXC__RMA_Shipment_Line__c();
    static SVMXC__Service_Order__c objWO2 = new SVMXC__Service_Order__c();
    static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    static Case objCase = SR_testdata.createCase();
    static Case objCase1 = SR_testdata.createCase();
    static Contact objCont = SR_testdata.createContact();
    static Account objAcc = SR_testdata.creteAccount();
    static Product2 objProd = SR_testdata.createProduct();
    static SVMXC__Service_Contract__c objSMC = new SVMXC__Service_Contract__c();
    static SVMXC__Site__c objLoc = new SVMXC__Site__c();
    static SVMXC__Service_Level__c objSLATerm = new SVMXC__Service_Level__c();
    static SVMXC__Service_Group__c objServTeam = new SVMXC__Service_Group__c();
    static SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c();
    static Depot_Sourcing_Rules__c objDSR = new Depot_Sourcing_Rules__c();
    static Country__c objCounty = new Country__c();
    static ERP_Org__c objOrg = new ERP_Org__c();
    static DateTime d = system.Now();
    // public static List<User> Usr = [SELECT Phone, Id, email FROM User WHERE Id = : UserInfo.getUserId() limit 1];
    public static List<BusinessHours> listBusinessHrs = [Select ID from BusinessHours limit 1];

    static testMethod void SR_WorkOrder_Customer_Values_Test()
    {
        
        //Rakesh add Custom setting
        List<CountryDateFormat__c> lstCustSett = new  List<CountryDateFormat__c>();
        lstCustSett.add(new CountryDateFormat__c(Name = 'Australia',Date_Format__c='MM/dd/yyyy hh:mm a'));
        lstCustSett.add(new CountryDateFormat__c(Name = 'Default',Date_Format__c='dd/MMM/yyyy hh:mm a'));
        insert lstCustSett;
        
        //
        
        ERP_Timezone__c erpTZ = new ERP_Timezone__c();
        erpTZ.Name = 'AUSSA';
        erpTZ.Salesforce_timezone__c = 'Central Summer Time (South Australia)';
        insert erpTZ;
        
        objAcc.AccountNumber = '123456';
        objAcc.country__c = 'Australia';
        objAcc.ERP_Timezone__c = 'AUSSA';
        insert objAcc;

        objOrg.Name = 'Test Org';
        objOrg.Sales_Org__c = 'Test Sales Org';
        objOrg.Approval_time_limit__c = 2;
        objOrg.Part_Order_Value_Limit__c = 12;
        objOrg.Received_Date_Required__c = true;
        objOrg.Bonded_Warehouse_used__c = true;
        objOrg.Parts_Order_Approval_Level__c = 12.8;
        insert objOrg;
        
        objLoc.Name = 'Test Location';
        objLoc.SVMXC__Street__c = 'Test Street';
        objLoc.SVMXC__Country__c = 'United States';
        objLoc.SVMXC__Zip__c = '98765';
        objLoc.ERP_Functional_Location__c = 'Test Location';
        objLoc.Plant__c = 'Test Plant';
        objLoc.ERP_Site_Partner_Code__c = '123456';
        objLoc.SVMXC__Location_Type__c = 'Depot';
        objLoc.Sales_Org__c = objOrg.Sales_Org__c;
        objLoc.SVMXC__Service_Engineer__c = userInfo.getUserID();
        objLoc.ERP_Org__c = objOrg.id;
        objLoc.SVMXC__Stocking_Location__c = true;
        insert objLoc;
        
        Depot_Sourcing_Rules__c varDepo = new Depot_Sourcing_Rules__c();
        varDepo.Country__c = objCounty.id;
        varDepo.Depot_Priority__c = 2;
        varDepo.Location__c = objLoc.ID;
        varDepo.Machine_Down__c = true;
        insert varDepo;
        
        objDSR.Name = 'Test DSR';
        objDSR.Country__c = objCounty.ID;
        objDSR.Country_Code__c = 'US';
        objDSR.Depot_Priority__c = 1;
        objDSR.Location__c = objLoc.ID;
       
        objCont.Email = 'abc@xyz.com';
        objCont.mailingcountry = 'India';
        insert objCont;
        
        objCase.AccountID = objAcc.ID;
        objCase.ContactID = objCont.ID;
        objCase.Reason = 'System Down';
        objCase.Priority = 'Medium';
        objCase.Type ='Problem';
        objCase.Reason = 'Source Exchange';
        //objCase.Customer_Preferred_End_Date_time__c = d.addMinutes(4);
        // objCase.Customer_Requested_End_Date_Time__c= '12-6-2015 11:59 PM';
        //insert objCase;  
        
        objProd.Name = 'Test Product';
        objProd.SVMXC__Product_Line__c = 'Accessory';
        objProd.Skills_Required__c = 'BEAM' ;
        objProd.Returnable__c = 'Scrap Locally';
        //objProd.Includes_Software__c = true;
        /*objProd.Material_Group__c = Label.Product_Material_group;
        objProd.Budget_Hrs__c = 4;
        objProd.UOM__c = 'HR';
        objProd.Product_Type__c = label.SR_Product_Type_Sellable;
        objProd.ProductCode = 'Test Material No 001';
        objProd.ERP_PCode_4__c = 'H012';
        objProd.Is_Model__c = true; */
        insert objProd;

        objSMC.Name = 'Test Contract';
        objSMC.SVMXC__Company__c = objAcc.ID;
        objSMC.SVMXC__Start_Date__c = system.today();
        objSMC.SVMXC__End_Date__c = system.today() + 30;
        insert objSMC;
        
        objSLATerm.Name = 'Test SLA Term';
        objSLATerm.SVMXC__Restoration_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Onsite_Response_Tracked_On__c = 'WorkOrder';
        objSLATerm.SVMXC__Resolution_Tracked_On__c = 'WorkOrder';
        insert objSLATerm;
        
        objServTeam.Name = 'ABC';
        objServTeam.SVMXC__Active__c = true;
        objServTeam.Dispatch_Queue__c = 'DISP CH';
        objServTeam.District_Manager__c = userInfo.getUserID();
        
        objServTeam.SVMXC__Group_Code__c = 'ABC';
        insert objServTeam;
        
        objTechEquip.Name = 'Test Technician';
        objTechEquip.SVMXC__Active__c = true;
        objTechEquip.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        objTechEquip.SVMXC__Enable_Scheduling__c = true;
        objTechEquip.SVMXC__Phone__c = '987654321';
        objTechEquip.SVMXC__Email__c = 'abc@xyz.com';
        objTechEquip.SVMXC__Service_Group__c = objServTeam.ID;
        objTechEquip.SVMXC__Street__c = 'Test Street';
        objTechEquip.SVMXC__Zip__c = '98765';
        objTechEquip.SVMXC__Country__c = 'United States';
        objTechEquip.OOC__c = false;
        objTechEquip.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
        objTechEquip.User__c = userInfo.getUserID();
        objTechEquip.Dispatch_Queue__c = 'DISP CH';
        insert objTechEquip;
        
        objTopLevel.Name = 'H23456';
        objTopLevel.SVMXC__Serial_Lot_Number__c = 'H23456';
        objTopLevel.SVMXC__Product__c = objProd.ID;
        objTopLevel.SVMXC__Site__c = objLoc.ID;
        objTopLevel.ERP_Reference__c = 'H23456';
        //objTopLevel.Service_Team__c = objServTeam.Id;
        objTopLevel.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objTopLevel.ERP_CSS_District__c = 'ABC';
        objTopLevel.ERP_Sales__c = 'Test ERP Sales';
        insert objTopLevel;
        
        objComponent.Name = 'H12345';
        objComponent.SVMXC__Serial_Lot_Number__c = 'H12345';
        objComponent.SVMXC__Parent__c = objTopLevel.ID;
        objComponent.SVMXC__Product__c = objProd.ID;
        objComponent.SVMXC__Preferred_Technician__c = objTechEquip.ID;
        objComponent.ERP_Sales__c = 'Test ERP Sales';
        objComponent.ERP_Reference__c = 'H12345';
        objComponent.SVMXC__Top_Level__c = objTopLevel.id;
        //objComponent.Service_Team__c = objServTeam.Id;
        objComponent.ERP_CSS_District__c = 'ABC';
        objComponent.SVMXC__Site__c = objLoc.id;
        objComponent.SVMXC__Status__c = 'Shipped';
        objComponent.SVMXC__Company__c = objAcc.id;
        insert objComponent;
        
        objCase1.AccountID = objAcc.ID;
        objCase1.ContactID = objCont.ID;
        objCase1.Reason = 'System Down';
        objCase1.Priority = 'Medium';
        objCase1.Status = 'New';
        objCase1.SVMXC__Top_Level__c = objComponent.id;
        objCase1.Type ='Problem';
        objCase1.Reason = 'System Down';
        objCase1.Description = 'asdfghjkzxcvbnm';
        // objCase1.Assigned_Future_WorkOrder__c = objCase.id;
        insert objCase1;
         
        SVMXC__SVMX_Event__c objEvent = new SVMXC__SVMX_Event__c(SVMXC__StartDateTime__c = System.now().addMinutes(-20), SVMXC__EndDateTime__c = System.now().addHours(5), SVMXC__Technician__c = objTechEquip.ID);
        //insert objEvent;    
        
        objWO1.recordtypeID = fieldServcRecTypeID;
        objWO1.Machine_release_time__c = system.now();
        objWO1.SVMXC__Order_Status__c = 'Assigned';
        //objWO1.Follow_Up_required__c = false;
        objWO1.SVMXC__Case__c = objCase1.Id;
        objWO1.Direct_Assignment__c = true ;
        objWO1.SVMXC__Group_Member__c = objTechEquip.ID;
        objWO1.SVMXC__Preferred_End_Time__c = System.now().addMinutes(240);
        objWO1.SVMXC__Top_Level__c = objTopLevel.id;
        objWO1.Subject__c = 'qwertyui';
        objWO1.SVMXC__Preferred_Start_Time__c = System.now();
        objWO1.SVMXC__Purpose_of_Visit__c = 'Repair';
        objWO1.ERP_Depot__c = objLoc.Plant__c;
        
        objWO1.ERP_Functional_Location__c = objLoc.ERP_Functional_Location__c;
        //objWO1.Is_Master_WO__c = true; //US5236 decommission
        objWO1.Customer_Malfunction_Start_Date_Time__c = '07-Jul-2015 01:02 PM';
        objWO1.Customer_Requested_Start_Date_Time__c = '07-Jul-2015 01:02 PM';        
        objWO1.Customer_Requested_End_Date_Time__c = '07-Jul-2015 11:59 PM';    

        //Rakesh
        objWO1.Acceptance_Date1__c = System.now();
        objWO1.Date_of_Intervention1__c = System.now();        
        objWO1.Next_Training_Trip_Reminder1__c = System.now();
        //
        
        insert objWO1;
        //}   
        test.startTest();
        //insert objWO1;  
        
        PageReference myVfPage1 = Page.SR_WorkOrder_Edit_Values_PopUp_Page;
        Test.setCurrentPage(myVfPage1);     
        ApexPages.currentPage().getParameters().Put('id', objWO1.id);
        System.debug('***: ' + objWO1.id);
        ApexPages.StandardController con1 = new ApexPages.StandardController(objWO1);
        SR_WorkOrder_Customer_Values controller1 = new SR_WorkOrder_Customer_Values(con1);
        controller1.convertMonthNameToNumber('Jan');        
        controller1.convertMonthNameToNumber('Feb');
        controller1.convertMonthNameToNumber('Mar');
        controller1.convertMonthNameToNumber('Apr');
        controller1.convertMonthNameToNumber('May');   
        controller1.convertMonthNameToNumber('Jun');        
        controller1.convertMonthNameToNumber('Jul');
        controller1.convertMonthNameToNumber('Aug');
        controller1.convertMonthNameToNumber('Sep');
        controller1.convertMonthNameToNumber('Oct');  
        controller1.convertMonthNameToNumber('Nov');
        controller1.convertMonthNameToNumber('Dec');                       
        controller1.convertMonthIntNumberToName(1);    
        controller1.convertMonthIntNumberToName(2);  
        controller1.convertMonthIntNumberToName(3);  
        controller1.convertMonthIntNumberToName(4);  
        controller1.convertMonthIntNumberToName(5);  
        controller1.convertMonthIntNumberToName(6);            
        controller1.convertMonthIntNumberToName(7);    
        controller1.convertMonthIntNumberToName(8);  
        controller1.convertMonthIntNumberToName(9);  
        controller1.convertMonthIntNumberToName(10);  
        controller1.convertMonthIntNumberToName(11);  
        controller1.convertMonthIntNumberToName(12);                    
        controller1.getHour();
        controller1.getMin();  
        controller1.getAmPM();
        controller1.saveDates();
        objWO1.Customer_Malfunction_Start_Date_Time__c = '07-Jul-2015 01:02 PM';
        objWO1.Customer_Requested_Start_Date_Time__c = '09-Jul-2015 01:02 PM';        
        objWO1.Customer_Requested_End_Date_Time__c = '07-Jul-2015 11:59 PM';   
        update objWO1;
        ApexPages.StandardController con2 = new ApexPages.StandardController(objWO1);
        SR_WorkOrder_Customer_Values controller2 = new SR_WorkOrder_Customer_Values(con2);           
        controller2.saveDates();   
        test.stopTest();                 
    }
}
public class SR_case_feed_component 
{
    public case caserec{get;set;}
    public string keyPrefix{get;set;}
    public string keyPrefixCont{get;set;}
    public List<Case> Opencases{get;set;}
    public String CaseSubjct{get;set;}
    public String caseId{get;set;}
    public String convarId{get;set;}
    public String ACCvarId{get;set;}
    public string InstalledProductId{get;set;}
    string caseRecordTypeHD;
    Id Caserecrdtypid;
    Public contact conVar {get;set;}
    Public SVMXC__Installed_Product__c InstalledVar {get;set;}
    public String Casenmbr{get;set;}
    public String caserecid{get;set;}
    public Boolean updatetohppn{get;set;}
    public Boolean Inserttohppn{get;set;}
    public SR_case_feed_component(ApexPages.StandardController controller) 
    {
        caserec = new Case();
        Opencases = new List<Case>();
        Schema.DescribeSObjectResult r = SVMXC__Installed_Product__c.sObjectType.getDescribe();
        keyPrefix = r.getKeyPrefix();
        Schema.DescribeSObjectResult rcont = Contact.sObjectType.getDescribe();
        keyPrefixCont = rcont.getKeyPrefix();
        caseRecordTypeHD = system.label.Case_rectyp_H_D; 
        Caserecrdtypid = Schema.SObjectType.Case.getRecordTypeInfosByName().get(caseRecordTypeHD).getRecordTypeId();
        caserecid = ApexPages.CurrentPage().getparameters().get('id');
        if(caserecid != null)
        {
            system.debug('In the caserec if part');
            caserec = [Select id,AccountId,ContactId,Subject,SVMXC__Top_Level__c, ProductSystem__c from case where id =: caserecid limit 1000];
            CaseSubjct = caserec.Subject;
        }
        
        if(ApexPages.CurrentPage().getparameters().get('ConId')!=null)
        {
            system.debug('In the contact if part');
            convarId = ApexPages.CurrentPage().getparameters().get('ConId');
            system.debug('convarId----->'+convarId);
            conVar = [select id,name,AccountId from contact where id!=null and id =:convarId limit 1000];
            caserec.contactId = convarId;
            system.debug('caserec.contactId----->'+caserec.contactId);
            caserec.AccountId = conVar.AccountId;
            system.debug('caserec.AccountId----->'+caserec.AccountId);
        }
        if(ApexPages.CurrentPage().getparameters().get('Accid')!=null)
        {
            system.debug('In the Account if part');
            ACCvarId = ApexPages.CurrentPage().getparameters().get('Accid');
            caserec.AccountId = ACCvarId;
        }
        if(ApexPages.CurrentPage().getparameters().get('InstalledProdId')!=null)
        {
            InstalledProductId = ApexPages.CurrentPage().getparameters().get('InstalledProdId');
            system.debug('In the Installed Prod if part');
            InstalledVar = [select id,name,SVMXC__Company__c from SVMXC__Installed_Product__c where id!=null and id =:InstalledProductId limit 1000];
            caserec.ProductSystem__c = InstalledProductId; //DE3449
            caserec.AccountId = InstalledVar.SVMXC__Company__c;            
        }
        updatetohppn = true;
        Inserttohppn = true;
        Search();
    }
    // Clear functionality
    public void ClearSearch()
    {
        CaseSubjct = '';
        caserec = new case();
        Opencases = new List<Case>();
    }
    public pagereference CreateCase()
    {
        // logic for assignment rules
        //Fetching the assignment rules on case
        /*AssignmentRule AR = new AssignmentRule();
        AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;*/
        
        //subject mandatory check 
        if(CaseSubjct == null)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Subject : You must enter a value'));
            return null;
        }
        set<id> contactid = new set<id>();
        set<id> Installedprod = new set<id>();
        case newcase = new case();
        //newCase.setOptions(dmlOpts);
         
         // Code for account validation
        if(caserec.AccountId != null)
        {
            newcase.AccountId = caserec.AccountId;
            Account acc = [Select id,Name,(Select id from Contacts limit 1000),(Select id from SVMXC__R00N70000001hzZ0EAI__r) from Account where id=: caserec.AccountId limit 1000];
            for(Contact con : acc.Contacts)
                contactid.add(con.id);
            for(SVMXC__Installed_Product__c ip : acc.SVMXC__R00N70000001hzZ0EAI__r)
                Installedprod.add(ip.id);
            if(!contactid.contains(caserec.ContactId) && caserec.ContactId != null)
            {//&& caserec.ContactId == null && oldcase.ContactId != null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The Contact Linked to the case is not from the Account Selected.'));
                Inserttohppn = false;
            }
            if(!Installedprod.contains(caserec.ProductSystem__c) && caserec.ProductSystem__c != null)// && caserec.ProductSystem__c == null && oldcase.ProductSystem__c != null) //DE3449
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The Installed Product Linked to the case is not from the Account Selected.'));
                Inserttohppn = false;
            }
        }
        newcase.ContactId = caserec.ContactId;
     
        newcase.Local_Subject__c = CaseSubjct;
        newcase.ProductSystem__c = caserec.ProductSystem__c; //DE3449
        if(Inserttohppn)
        {
            Database.SaveResult result = Database.insert(newcase);
            if (result.isSuccess()) 
            {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted account. Account ID: ' + result.getId());
            
                caseId = result.getId();
                Casenmbr = [Select CaseNumber from case where id =: caseId].CaseNumber;
                system.debug('DDDDD' + Casenmbr);
            }
        }
             
        return null;
    }
    
    //method for update
    public void updatecasemethod()
    {
        case oldcase = [Select ContactId,SVMXC__Top_Level__c, ProductSystem__c from case where id =: caserecid limit 1000];
        set<id> contactid = new set<id>();
        set<id> Installedprod = new set<id>();
        // logic for assignment rules
             //Fetching the assignment rules on case
            /*AssignmentRule AR = new AssignmentRule();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            //Creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;*/
            
        case updatecase = new case(id = caserecid);
        //updatecase.setOptions(dmlOpts);
        updatetohppn = true;
        if(caserec.AccountId != null)
        {
            updatecase.AccountId = caserec.AccountId;
            Account acc = [Select id,Name,(Select id from Contacts limit 1000),(Select id from SVMXC__R00N70000001hzZ0EAI__r) from Account where id=: caserec.AccountId limit 1000];
            for(Contact con : acc.Contacts)
                contactid.add(con.id);
            for(SVMXC__Installed_Product__c ip : acc.SVMXC__R00N70000001hzZ0EAI__r)
                Installedprod.add(ip.id);
            if(!contactid.contains(caserec.ContactId) && caserec.ContactId != null)
            {//&& caserec.ContactId == null && oldcase.ContactId != null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The Contact Linked to the case is not from the Account Selected.'));
                updatetohppn = false;
            }
            if(!Installedprod.contains(caserec.ProductSystem__c) && caserec.ProductSystem__c != null)// && caserec.ProductSystem__c == null && oldcase.ProductSystem__c != null) //DE3449
            {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The Installed Product Linked to the case is not from the Account Selected.'));
            updatetohppn = false;
            }
        }
        if(caserec.ContactId != null)
            updatecase.ContactId = caserec.ContactId;
        if(CaseSubjct != null)
            updatecase.Subject = CaseSubjct;
            updatecase.Local_Subject__c = CaseSubjct;
        if(caserec.ProductSystem__c != null)
            updatecase.ProductSystem__c = caserec.ProductSystem__c;
        if(updatetohppn){
            update updatecase;
        }
        system.debug('UPDATE CASE FOR--->' + updatecase);
    }
    
    public void Search()
    {
        Opencases = new List<Case>();
        List<String> closedstatus = new List<String>();
        closedstatus.add(label.Case_Status_Closed);
        closedstatus.add(label.Case_status_closed_WO);
        String SearchQury = 'Select id,Subject,AccountId,type,Status,createddate,ContactId,SVMXC__Top_Level__c,ProductSystem__c,ClosedDate,CaseNumber,recordtypeId from Case';
        String whr = ' RecordTypeId =: Caserecrdtypid ';
       /* if(CaseSubjct != null && CaseSubjct != '')
        {
           // whr = whr + 'Subject Like \'%'+ CaseSubjct + '%\' ';
        }*/
        Id accid = (id)caserec.AccountId;
        if(accid != null )
        {
            if(whr != '' && whr != null)
            {
                whr = whr + ' and AccountId =: accid';
            }else{
                whr = whr + ' AccountId =: accid ';
            }
        }
       /* if(caserec.ContactId != null)
        {
            if(whr != null && whr != '')
            {
                //whr = whr + ' and ContactId = \'' + caserec.ContactId + '\'';
            }else{
              //  whr = whr + 'ContactId = \'' + caserec.ContactId + '\'';
            }
        }
        if(caserec.SVMXC__Top_Level__c != null)
        {
            if(whr != null && whr != '')
            {
               // whr = whr + ' and SVMXC__Top_Level__c = \''+ caserec.SVMXC__Top_Level__c + '\'';
            }else{
               // whr = whr + 'SVMXC__Top_Level__c =\''+ caserec.SVMXC__Top_Level__c + '\'';
            }
        } */
        if(whr != null && whr != '')
        {
            SearchQury = SearchQury + ' where ' + whr;
        }
        system.debug('Search Query-->' + SearchQury);
        
            SearchQury = SearchQury + ' Limit 1000';
        if(accid != null){
            for(Sobject sObj : Database.query(SearchQury))
            {
                Opencases.add((Case) sObj);
            }
        }
    }
    
    //Method for account search
    public void SearchAccount()
    {
        String objecttype = ApexPages.CurrentPage().getParameters().get('Object');
        if(objecttype == 'Contact')
        {
            List<Contact> Acc = new List<Contact>([Select id, AccountId from contact where id =: caserec.ContactId]);
            if(Acc != null && Acc.size() > 0)
                caserec.AccountId = Acc[0].AccountId;
        }
        else if(objecttype == 'Product')
        {
            List<SVMXC__Installed_Product__c> Acc = new List<SVMXC__Installed_Product__c>([Select id, SVMXC__Company__c from SVMXC__Installed_Product__c where id =: caserec.ProductSystem__c]); //DE3449
            if(Acc != null && Acc.size() > 0)
                caserec.AccountId = Acc[0].SVMXC__Company__c;
        }
        system.debug('caserec.ContactId---' + caserec.ContactId);
    }
}
/**
  * @author pszagdaj/Forefront
  * @version 1.0
  * @description SR_classPartsOder test class
  *
  * PURPOSE
  *
  *    Test class for SR_classPartsOder class
  *
  * CHANGE LOG
  *
  *    v1.0; 2015/10/13; pszagdaj/Forefront; Initial Build
  *    06-Jul-2018:  STSK0015140:  Nick Sauer - Updated for dedupe rules
  *
  **/
@isTest(seeAllData=true)
private class SR_classPartsOder_Test {
    
    
 
    static testMethod void shouldSetCancelStatusWhenMarkedCancel() {
        //given
        Country__c country = testUtils.createCountry('United States');
        insert country;
        Account account = testUtils.createAccount('Test1', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';    
        insert account;
        Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        insert contact;
        Case cs = testUtils.createCase('Test subject');
        insert cs;
        
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Cancel__c = false);
        insert partsOrderNotMarked;
        partsOrderNotMarked = [SELECT Cancel__c, SVMXC__Order_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partsOrderNotMarked.Id];
        system.assertNotEquals('Canceled', partsOrderNotMarked.SVMXC__Order_Status__c);
        
        //when
        test.startTest();
        SVMXC__RMA_Shipment_Order__c partsOrderMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Cancel__c = true);
        partsOrderNotMarked.Cancel__c = true;
        upsert new List<SVMXC__RMA_Shipment_Order__c> {partsOrderNotMarked, partsOrderMarked};
        test.stopTest();
        
        //then
        Map<Id,SVMXC__RMA_Shipment_Order__c> partsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([
            SELECT SVMXC__Order_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN (:partsOrderNotMarked.Id, :partsOrderMarked.Id)
        ]);
        system.assertEquals('Canceled', partsOrderMap.get(partsOrderNotMarked.Id).SVMXC__Order_Status__c);
        system.assertEquals('Canceled', partsOrderMap.get(partsOrderMarked.Id).SVMXC__Order_Status__c);
    }
    
    static testMethod void shouldUpdateServiceTeamCoordinatorFields() {
        //given
        Country__c country = testUtils.createCountry('United States');
        insert country;
        Account account = testUtils.createAccount('TestAccount1', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        insert contact;
        Case cs = testUtils.createCase('Test subject');
        insert cs;
        User srvcTeamUsr = testUtils.createUser();
        SVMXC__Service_Group__c srvcTeam = testUtils.createServiceroup(srvcTeamUsr.Id);
        insert srvcTeam;
        
        SVMXC__RMA_Shipment_Order__c partsOrderNoST = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id);
        SVMXC__RMA_Shipment_Order__c partsOrderST = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Service_Team__c = srvcTeam.Id);
        insert new List<SVMXC__RMA_Shipment_Order__c> {partsOrderNoST, partsOrderST};
        
        Map<Id,SVMXC__RMA_Shipment_Order__c> partsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([
            SELECT Service_Coordinator__c, Service_Coordinator_Email__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN (:partsOrderNoST.Id, :partsOrderST.Id)
        ]); 
        system.assertEquals(null,partsOrderMap.get(partsOrderNoST.Id).Service_Coordinator__c,'Initial value of Coordinator field for no Service team should be null');
        system.assertEquals(null,partsOrderMap.get(partsOrderNoST.Id).Service_Coordinator_Email__c,'Initial value of Coordinator Email field for no Service team should be null');
        system.assertNotEquals(null,partsOrderMap.get(partsOrderST.Id).Service_Coordinator__c,'Initial value of Coordinator field for no Service team should not be null');
        system.assertNotEquals(null,partsOrderMap.get(partsOrderST.Id).Service_Coordinator_Email__c,'Initial value of Coordinator Email field for no Service team should not be null');
        
        //when
        test.startTest();
        partsOrderNoST.Service_Team__c = srvcTeam.Id;
        partsOrderST.Service_Team__c = null;
        update new List<SVMXC__RMA_Shipment_Order__c> {partsOrderNoST, partsOrderST};
        test.stopTest();
        
        //then
        partsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([
            SELECT Service_Coordinator__c, Service_Coordinator_Email__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN (:partsOrderNoST.Id, :partsOrderST.Id)
        ]); 
        system.assertNotEquals(null,partsOrderMap.get(partsOrderNoST.Id).Service_Coordinator__c,'Final value of Coordinator field for no Service team should not be null');
        system.assertNotEquals(null,partsOrderMap.get(partsOrderNoST.Id).Service_Coordinator_Email__c,'Final value of Coordinator Email field for no Service team should not be null');
        system.assertEquals(null,partsOrderMap.get(partsOrderST.Id).Service_Coordinator__c,'Final value of Coordinator field for no Service team should be null');
        system.assertEquals(null,partsOrderMap.get(partsOrderST.Id).Service_Coordinator_Email__c,'Final value of Coordinator Email field for no Service team should be null');
        
    }
    
    static testMethod void shouldDoSomething() {
        //given
        Country__c country = testUtils.createCountry('United States');
        insert country;
        Account account = testUtils.createAccount('Test3', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        insert contact;
        Case cs = testUtils.createCase('Test subject');
        insert cs;
        
        User usr = testUtils.createUser();
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c();
        insert serviceOrder;
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(usr.Id);
        insert serviceGroup;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=usr.Id, ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Cancel__c = false );
        insert partsOrderNotMarked;
        partsOrderNotMarked = [SELECT Cancel__c, SVMXC__Order_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partsOrderNotMarked.Id];
        system.assertNotEquals('Canceled', partsOrderNotMarked.SVMXC__Order_Status__c);
        
        //when
        test.startTest();
        SVMXC__RMA_Shipment_Order__c partsOrderMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Cancel__c = true, SVMXC__Service_Order__c=serviceOrder.Id,Technician__c = technician.Id );
        partsOrderNotMarked.Cancel__c = true;
        System.debug('partsOrderMarked '+partsOrderMarked);
        upsert new List<SVMXC__RMA_Shipment_Order__c> {partsOrderNotMarked, partsOrderMarked};
        test.stopTest();
        
        //then
        Map<Id,SVMXC__RMA_Shipment_Order__c> partsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([
            SELECT SVMXC__Order_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN (:partsOrderNotMarked.Id, :partsOrderMarked.Id)
        ]);
        system.assertEquals('Canceled', partsOrderMap.get(partsOrderNotMarked.Id).SVMXC__Order_Status__c);
        system.assertEquals('Canceled', partsOrderMap.get(partsOrderMarked.Id).SVMXC__Order_Status__c);
    }
    

    static testMethod void succesStatusOnUpdateTEST() {
        //given
        Country__c country = testUtils.createCountry('United States');
        insert country;
        
        Country__c country2 = testUtils.createCountry('India');
        insert country2;
        
        Country_Country_Rule__c cr = new Country_Country_Rule__c();
        cr.To_Country__c = country.id;
        cr.From_Country__c = country2.id;
        cr.Commerical_Invoice_Required__c = true;
        cr.Export_License_Required__c = true;
        insert cr;
        
        Account account = testUtils.createAccount('Test4', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        //Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        //insert contact;
        Case cs = testUtils.createCase('Test subject');
        insert cs;
        
        User usr = testUtils.createUser();
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c();
        insert serviceOrder;
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(usr.Id);
        insert serviceGroup;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=usr.Id, ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Case__c = cs.Id, Cancel__c = false );
        partsOrderNotMarked.To_Country__c = country.Id;
        partsOrderNotMarked.From_Country__c = country2.Id;
        insert partsOrderNotMarked;
        
        //when
        test.startTest();
        partsOrderNotMarked.SVMXC__Sales_Order_Number__c='111111';
        partsOrderNotMarked.Interface_Error_Message__c = null;
        partsOrderNotMarked.SVMXC__Order_Status__c = 'Approved';
        upsert partsOrderNotMarked;
        test.stopTest();
        
        //then
        partsOrderNotMarked = [SELECT Cancel__c, SVMXC__Order_Status__c, ERP_Result_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partsOrderNotMarked.Id];
        //system.assertEquals('SUCCESS', partsOrderNotMarked.ERP_Result_Status__c);
    } 

    static testMethod void failureStatusOnUpdateTEST() {
        //given
        Country__c country = testUtils.createCountry('United States');
        insert country;
        Account account = testUtils.createAccount('Test5', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        insert contact;
        Case cs = testUtils.createCase('Test subject');
        insert cs;
        
        User usr = testUtils.createUser();
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c();
        insert serviceOrder;
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(usr.Id);
        insert serviceGroup;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=usr.Id, ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(SVMXC__Company__c = account.id, SVMXC__Contact__c = contact.Id, SVMXC__Case__c = cs.Id, Cancel__c = false );
        insert partsOrderNotMarked;
        
        //when
        test.startTest();
        partsOrderNotMarked.SVMXC__Sales_Order_Number__c=null;
        partsOrderNotMarked.Interface_Error_Message__c = 'ERROR_TEST';
        upsert partsOrderNotMarked;
        test.stopTest();
        
        //then
        partsOrderNotMarked = [SELECT Cancel__c, SVMXC__Order_Status__c, ERP_Result_Status__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id = :partsOrderNotMarked.Id];
        system.assertEquals('FAILURE', partsOrderNotMarked.ERP_Result_Status__c);
    }   
    
    
    static testMethod void updateFieldTEST() {
        //given
        Country__c country = testUtils.createCountry('United States');
        country.Alternate_Name__c = 'US';
        insert country;
        
        Country__c country2 = testUtils.createCountry('India');
        country.Alternate_Name__c = 'IN';
        insert country2;
        
        Country_Country_Rule__c cr = new Country_Country_Rule__c();
        cr.To_Country__c = country.id;
        cr.From_Country__c = country2.id;
        cr.Commerical_Invoice_Required__c = true;
        cr.Export_License_Required__c = true;
        insert cr;
        
        Product2 product = new Product2(Name='Connect Utilities', SVMXC__Product_Cost__c  = 20);
        product.PSE_Approval_Queue__c = 'PSE Approval';
        insert product;
        
        Account account = testUtils.createAccount('Test6', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        insert contact;
        Case cs = testUtils.createCase('Test subject');
        cs.Priority = 'Low';
        insert cs;
        
        User usr = testUtils.createUser();
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(usr.Id);
        serviceGroup.Work_Order_BST_Queue__c = 'BST';
        insert serviceGroup;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=usr.Id, ERP_Work_Center__c='Test ERP Work Center', SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        ERP_Org__c eorg = new ERP_Org__c();
        eorg.name = 'testorg';
        eorg.Sales_Org__c = 'testorg';
        eorg.Bonded_Warehouse_used__c = true;
        insert eorg; 
        
        SVMXC__Site__c location = new SVMXC__Site__c(Sales_Org__c = 'testorg', 
        SVMXC__Service_Engineer__c = userInfo.getUserId(), 
        SVMXC__Location_Type__c = 'Field', 
        Plant__c = 'dfgh', 
        SVMXC__Account__c = account.id,
        ERP_Org__c = eorg.Id
        );
        
        location.Plant__c = 'testorg';
        location.Facility_Code__c = 'facilitycode1234';
        location.Country__c =country.id;
        insert location;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Name = '21ST CENTURY ONCOLOGY LLC...';
        erp.State_Province_Code__c = 'TET';
        erp.Country_Code__c = 'dd';
        insert erp; 
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c(
        SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Company__c = account.id,
        SVMXC__Case__c = cs.id,ERP_Service_Order__c = '12345678');
        serviceOrder.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        serviceOrder.Malfunction_Start__c = system.now().addDays(10);
        serviceOrder.SVMXC__Order_Status__c = 'Open';
        serviceOrder.Depot_Location__c = location.Id;
        serviceOrder.Inventory_Location__c = location.Id;
        serviceOrder.Equipment_Location__c = location.Id;    
        serviceOrder.Service_Team__c  =  serviceGroup.id; 
        System.Test.startTest();
        insert serviceOrder;
               
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(
        SVMXC__Company__c = account.id, 
        SVMXC__Contact__c = contact.Id, 
        SVMXC__Case__c = cs.Id, 
        Cancel__c = false );
        partsOrderNotMarked.To_Country__c = country.Id;
        partsOrderNotMarked.From_Country__c = country2.Id;
        partsOrderNotMarked.SVMXC__Destination_Location__c = location.id;
        partsOrderNotMarked.SVMXC__Source_Location__c = location.id;
        partsOrderNotMarked.SVMXC__Source_Country__c = 'India';
        partsOrderNotMarked.SVMXC__Destination_Country__c  ='United States';
        partsOrderNotMarked.ERP_Sold_To__c = account.id;
        partsOrderNotMarked.ERP_Ship_To__c = erp.id;
        partsOrderNotMarked.ERP_Ship_To_Code__c = 'TEST'; 
        partsOrderNotMarked.One_time__c = true;
        partsOrderNotMarked.New_Work_Order__c = serviceOrder.Id;
        partsOrderNotMarked.SVMXC__Service_Order__c = serviceOrder.Id;
        partsOrderNotMarked.ERP_Result_Status__c = 'failure';
        partsOrderNotMarked.Transfer_to_FE__c = technician.Id;
        insert partsOrderNotMarked;
        
        
        
        SVMXC__RMA_Shipment_Line__c poline = new SVMXC__RMA_Shipment_Line__c(
            RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
            SVMXC__RMA_Shipment_Order__c = partsOrderNotMarked.Id,
            SVMXC__Actual_Quantity2__c = 5,
            SVMXC__Expected_Quantity2__c = 5,
            SVMXC__Fulfillment_Qty__c =  5,
            Remaining_Qty__c = 5,
            SVMXC__Line_Status__c = 'Completed',
            SVMXC__Line_Type__c = 'Shipment',
            Consumed__c = false,
            Returned__c = false,
            SVMXC__Ship_Location__c = location.Id,
            ERP_Serial_Number__c = '8521469',
            SVMXC__Product__c = product.Id,
            SVMXC__Service_Order__c = serviceOrder.Id
        );   
        
        
        
        insert poline;
        
        //when
        
        partsOrderNotMarked.SVMXC__Sales_Order_Number__c='111111';
        partsOrderNotMarked.Interface_Error_Message__c = null;
        partsOrderNotMarked.SVMXC__Order_Status__c = 'Submitted';
        partsOrderNotMarked.ERP_Ship_To_Code__c = 'TEST2';
        partsOrderNotMarked.SVMXC__Source_Country__c = 'India 4';
        partsOrderNotMarked.SVMXC__Destination_Country__c  ='United States 4';
        partsOrderNotMarked.Interface_Status__c = 'Error';
        upsert partsOrderNotMarked;
        
        partsOrderNotMarked.SVMXC__Order_Status__c = 'Approved';
        partsOrderNotMarked.Interface_Status__c = 'Error';
        upsert partsOrderNotMarked;
        
        System.Test.stopTest();
        
        
    } 
    
    static testMethod void updatePartsOrderTest() {
        //given
        Country__c country = testUtils.createCountry('United States');
        country.Alternate_Name__c = 'US';
        insert country;
        
        Country__c country2 = testUtils.createCountry('India');
        country.Alternate_Name__c = 'IN';
        insert country2;
        
        Country_Country_Rule__c cr = new Country_Country_Rule__c();
        cr.To_Country__c = country.id;
        cr.From_Country__c = country2.id;
        cr.Commerical_Invoice_Required__c = true;
        cr.Export_License_Required__c = true;
        insert cr;
        
        Product2 product = new Product2(Name='Connect Utilities', SVMXC__Product_Cost__c  = 20);
        product.PSE_Approval_Queue__c = 'PSE Approval';
        insert product;
        
        Account account = testUtils.createAccount('Test7', 'AUSSA', country.Name, country.Id);
        account.BillingCity = 'Milpitas';
        account.BillingState = 'CA';
        insert account;
        //Contact contact = testUtils.createContact(account.Id, 'FirstName', 'LastName', 'a@b.c');
        //insert contact;
        Case cs = testUtils.createCase('Test subject');
        cs.Priority = 'Low';
        insert cs;
        
        User usr = testUtils.createUser();
        
        SVMXC__Service_Group__c serviceGroup = testUtils.createServiceroup(usr.Id);
        serviceGroup.Work_Order_BST_Queue__c = 'BST';
        insert serviceGroup;
        
        SVMXC__Service_Group__c serviceGroup2 = testUtils.createServiceroup(usr.Id);
        serviceGroup2.Work_Order_BST_Queue__c = 'BST';
        insert serviceGroup2;
        
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(User__c=usr.Id, ERP_Work_Center__c='Test ERP Work Center', 
                                                                                         SVMXC__Service_Group__c=serviceGroup.Id);
        insert technician;
        
        ERP_Org__c eorg = new ERP_Org__c();
        eorg.name = 'testorg';
        eorg.Sales_Org__c = 'testorg';
        eorg.Bonded_Warehouse_used__c = true;
        insert eorg; 
        
        SVMXC__Site__c location = new SVMXC__Site__c(Sales_Org__c = 'testorg', 
        SVMXC__Service_Engineer__c = userInfo.getUserId(), 
        SVMXC__Location_Type__c = 'Field', 
        Plant__c = 'dfgh', 
        SVMXC__Account__c = account.id,
        ERP_Org__c = eorg.Id
        );
        
        location.Plant__c = 'testorg';
        location.Facility_Code__c = 'facilitycode1234';
        location.Country__c =country.id;
        insert location;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Name = '21ST CENTURY ONCOLOGY LLC...';
        erp.State_Province_Code__c = 'TET';
        erp.Country_Code__c = 'dd';
        insert erp; 
        
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c(
        SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Company__c = account.id,
        SVMXC__Case__c = cs.id,ERP_Service_Order__c = '12345678');
        serviceOrder.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        serviceOrder.Malfunction_Start__c = system.now().addDays(10);
        serviceOrder.SVMXC__Order_Status__c = 'Open';
        serviceOrder.Depot_Location__c = location.Id;
        serviceOrder.Inventory_Location__c = location.Id;
        serviceOrder.Equipment_Location__c = location.Id;    
        serviceOrder.Service_Team__c  =  serviceGroup.id; 
        
        insert serviceOrder;
        System.Test.startTest();  
        
        SVMXC__RMA_Shipment_Order__c partsOrderNotMarked = new SVMXC__RMA_Shipment_Order__c(
        SVMXC__Company__c = account.id, 
        //SVMXC__Contact__c = contact.Id, 
        SVMXC__Case__c = cs.Id, 
        Cancel__c = false );
        partsOrderNotMarked.To_Country__c = country.Id;
        partsOrderNotMarked.From_Country__c = country2.Id;
        partsOrderNotMarked.SVMXC__Destination_Location__c = location.id;
        partsOrderNotMarked.SVMXC__Source_Location__c = location.id;
        partsOrderNotMarked.SVMXC__Source_Country__c = 'India';
        partsOrderNotMarked.SVMXC__Destination_Country__c  ='United States';
        partsOrderNotMarked.ERP_Sold_To__c = account.id;
        partsOrderNotMarked.ERP_Ship_To__c = erp.id;
        partsOrderNotMarked.ERP_Ship_To_Code__c = 'TEST'; 
        partsOrderNotMarked.One_time__c = true;
        partsOrderNotMarked.New_Work_Order__c = serviceOrder.Id;
        partsOrderNotMarked.SVMXC__Service_Order__c = serviceOrder.Id;
        partsOrderNotMarked.Transfer_to_FE__c = technician.Id;
        insert partsOrderNotMarked;
        
        
        
        SVMXC__RMA_Shipment_Line__c poline = new SVMXC__RMA_Shipment_Line__c(
            RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
            SVMXC__RMA_Shipment_Order__c = partsOrderNotMarked.Id,
            SVMXC__Actual_Quantity2__c = 5,
            SVMXC__Expected_Quantity2__c = 5,
            SVMXC__Fulfillment_Qty__c =  5,
            Remaining_Qty__c = 5,
            SVMXC__Line_Status__c = 'Open',
            SVMXC__Line_Type__c = 'Shipment',
            Consumed__c = false,
            Returned__c = false,
            SVMXC__Ship_Location__c = location.Id,
            ERP_Serial_Number__c = '8521469',
            SVMXC__Product__c = product.Id,
            SVMXC__Service_Order__c = serviceOrder.Id
        );  
        
        SVMXC__RMA_Shipment_Line__c poline2 = new SVMXC__RMA_Shipment_Line__c(
            RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
            SVMXC__RMA_Shipment_Order__c = partsOrderNotMarked.Id,
            SVMXC__Actual_Quantity2__c = 5,
            SVMXC__Expected_Quantity2__c = 5,
            SVMXC__Fulfillment_Qty__c =  5,
            Remaining_Qty__c = 5,
            SVMXC__Line_Status__c = 'Open',
            SVMXC__Line_Type__c = 'Shipment',
            Consumed__c = false,
            Returned__c = false,
            SVMXC__Ship_Location__c = location.Id,
            ERP_Serial_Number__c = '8521469',
            SVMXC__Product__c = product.Id,
            SVMXC__Service_Order__c = serviceOrder.Id
        );   
                              
        insert new list<SVMXC__RMA_Shipment_Line__c> {poline,poline2};

        partsOrderNotMarked.Submitted_Line_Count__c = 1;
        partsOrderNotMarked.SVMXC__Order_Status__c = 'Submitted';
        partsOrderNotMarked.Interface_Status__c = 'Processed';
        partsOrderNotMarked.Service_Team__c = serviceGroup2.id;
        update partsOrderNotMarked;
        
        partsOrderNotMarked.SVMXC__Service_Order__r = serviceOrder;
        partsOrderNotMarked.ERP_Result_Status__c = 'failure'; 
        
        
        poline.SVMXC__Line_Status__c = 'Open';
        update poline;
        
        SR_classPartsOder.setOwnerToQueueIfStatusIsFailure(new list<SVMXC__RMA_Shipment_Order__c> {partsOrderNotMarked});
        partsOrderNotMarked.Cancel__c = true;
        SR_classPartsOder.updateStatusWhenCancelFlagged(new list<SVMXC__RMA_Shipment_Order__c> {partsOrderNotMarked});
        
        SR_classPartsOder obj = new SR_classPartsOder();
        obj.updatePOLFromPOAfterTrigger(new list<SVMXC__RMA_Shipment_Order__c> {partsOrderNotMarked}, null);
        
        
        
        System.Test.stopTest();
      
    }   
}
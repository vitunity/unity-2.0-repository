/*************************************************************************\
      @ Author                : Kaushiki Verma(WIPRO Technologies)
      @ Date                  : 05/02/2014
      @ Description           : 1. To set SalesOrderLookup on Quotes matching Quote Name with Ext_Quote_Number on SO. This class is called from trigger
      @ Last Modified By      : Priti Jaiswal
      @ Last Modified On      : 19-02-2014
      @ Last Modified Reason  : US4030 - To update Sold To and Quote on Sales Order
/**************************************************************************/
public class SR_classSalesOrder{
    public static List<SVMXC__Installed_Product__c> Iplist = new List<SVMXC__Installed_Product__c>(); // Related time entries new values
    
    public void setSalesOrderOnQuote(List<Sales_Order__c> salesOrderList){
        
        Map<String,id> mapOfSalesQuote = new Map<String,id>();
        Map<String,String> mapOfSalesOrderName = new Map<String,String>();
        Set<string> setQuoteNumber = new set<String>();
        
        Set<Id> SetofSalesOrderId = new Set<Id>();
        List<SVMXC__Installed_Product__c> ListOfIPUpdate = new List<SVMXC__Installed_Product__c>();
        Map<Id,Sales_Order_Item__c> MapofIdtoSOI = new Map<Id,Sales_Order_Item__c>();
        Set<Sales_Order_Item__c> SetofSOI = new Set<Sales_Order_Item__c>();
        
        for(Sales_Order__c so: salesOrderList){
           
            if(so.Ext_Quote_Number__c != null){
                System.debug('-----so.Ext_Quote_Number__c'+so.Ext_Quote_Number__c);
                setQuoteNumber.add(so.Ext_Quote_Number__c);//set of quote number
                mapOfSalesQuote.put(so.Ext_Quote_Number__c,so.id);//map of quote name matching with Ext_Quote_Number__c and its relates quote
                mapOfSalesOrderName.put(so.Ext_Quote_Number__c,so.Name);
            }
            SetofSalesOrderId.add(SO.Id);
        }
        if(mapOfSalesQuote.size()>0 && setQuoteNumber.size() > 0){
            List<BigMachines__Quote__c> bmQuoteList = new List<BigMachines__Quote__c>();
            for(BigMachines__Quote__c bmQuote:[SELECT id,name FROM BigMachines__Quote__c WHERE name IN :setQuoteNumber ]){
                bmQuote.Sales_Order__c = mapOfSalesQuote.get(bmQuote.name);//quote sales order will be set,where Ext_Quote_Number__c mathes with quote name
                bmQuote.SAP_Booked_Sales__c = mapOfSalesOrderName.get(bmQuote.name);
                System.debug('-----bmQuote.SAP_Booked_Sales__c'+bmQuote.SAP_Booked_Sales__c);
                bmQuoteList.add(bmQuote);   
            }
            if(bmQuoteList.size()>0){
                System.debug('-----bmQuoteList'+bmQuoteList);
                update bmQuoteList;
            }
        }
        
         
    }
    
    //priti, US4030, 19/02/2014 - To update Sold To and Quote on Sales Order
    public void updateSO(List<Sales_Order__c> lstOfSOToUpdate, Map<Id,Sales_Order__c> mapOldSalesOrder){
        Set<String> setOfERPSoldTo = new Set<String>();
        Set<String> setOfEXtQuoteNo = new Set<String>();
        Map<String, Id> mapOfAccNoAndId = new Map<String, Id>();
        Map<String, Id> mapOfBMQuoteNameAndId = new Map<String, Id>();
        Set<String> setOfERPSitePartner = new Set<String>();
        Map<String, Id> mapOfPatnerNoAndERPPartnerId = new Map<String, Id>();
        Map<string, Id> mapSitePartner_Location = new Map<string, Id>();    //DE3202

        for(Sales_Order__c so : lstOfSOToUpdate){
            if(so.ERP_Sold_To__c != null && (mapOldSalesOrder == null || mapOldSalesOrder.get(so.Id).ERP_Sold_To__c == null || mapOldSalesOrder.get(so.Id).ERP_Sold_To__c != so.ERP_Sold_To__c)){
                setOfERPSoldTo.add(so.ERP_Sold_To__c);
            }
            if(so.Ext_Quote_Number__c != null && (mapOldSalesOrder == null || mapOldSalesOrder.get(so.Id).Ext_Quote_Number__c == null || mapOldSalesOrder.get(so.Id).Ext_Quote_Number__c != so.Ext_Quote_Number__c)){
                setOfEXtQuoteNo.add(so.Ext_Quote_Number__c);
            }
            if(so.ERP_Site_Partner__c != null && (mapOldSalesOrder == null || mapOldSalesOrder.get(so.Id).ERP_Site_Partner__c == null || mapOldSalesOrder.get(so.Id).ERP_Site_Partner__c != so.ERP_Site_Partner__c)){
                setOfERPSitePartner.add(so.ERP_Site_Partner__c); 
                System.debug('setOfERPSitePartner value === ' + setOfERPSitePartner);
            } 
        }

        //creating map of Account number and and Account id //if condition changed for DE3202
        //|| setOfERPSitePartner != null)
        if(setOfERPSoldTo != null  && setOfERPSoldTo.size() > 0 ){
            List<Account> lstOfAccount = [select id, AccountNumber from Account where AccountNumber in : setOfERPSoldTo OR AccountNumber in :setOfERPSitePartner];
            if(lstOfAccount.size()>0){
                
                for(Account acc : lstOfAccount){
                    if(acc.AccountNumber != null){
                        mapOfAccNoAndId.put(acc.AccountNumber, acc.id);
                    }
                }
            }
        }
        
        
        if(setOfERPSitePartner != null && setOfERPSitePartner.size() > 0){
            
            List<ERP_Partner__c> lstOfErpPartner = [select id, Partner_Number__c from ERP_Partner__c where Partner_Number__c in :setOfERPSitePartner];
            if(lstOfErpPartner.size()>0){
                
                for(ERP_Partner__c erppartner : lstOfErpPartner){
                    if(erppartner.Partner_Number__c != null){
                        mapOfPatnerNoAndERPPartnerId.put(erppartner.Partner_Number__c ,erppartner.id);
                    }
                }
            }
        }
        //code for DE3202
        if(setOfERPSitePartner.size() > 0){
            
            for(SVMXC__Site__c varLoc : [select Id, ERP_Site_Partner_Code__c, SVMXC__Account__c from SVMXC__Site__c
                                                            where ERP_Site_Partner_Code__c in :setOfERPSitePartner]){
                mapSitePartner_Location.put(varLoc.ERP_Site_Partner_Code__c, varLoc.SVMXC__Account__c);
                System.debug('mapSitePartner_Location value === ' + mapSitePartner_Location.get(varLoc.ERP_Site_Partner_Code__c));
            }
        }
        //}
        
        //creating map of Quote Name and and Quote id
        if(setOfEXtQuoteNo != null && setOfEXtQuoteNo.size() > 0){
            List<BigMachines__Quote__c> lstOfBMQuote = [select id, Name from BigMachines__Quote__c where Name in : setOfEXtQuoteNo];

            if(lstOfBMQuote.size()>0){
                for(BigMachines__Quote__c quote : lstOfBMQuote){
                    mapOfBMQuoteNameAndId.put(quote.Name, quote.id);
                }
            }
        }

        for(Sales_Order__c so : lstOfSOToUpdate){
           //update Sold To of Sales Order
            if(so.ERP_Sold_To__c != null && mapOfAccNoAndId.get(so.ERP_Sold_To__c) != Null){
                so.Sold_To__c = mapOfAccNoAndId.get(so.ERP_Sold_To__c);
            }
            //update Site Partner of Sales Order    //below code changed for DE3202
            if(so.ERP_Site_Partner__c != null && mapSitePartner_Location.containsKey(so.ERP_Site_Partner__c)){
                so.Site_Partner__c = mapSitePartner_Location.get(so.ERP_Site_Partner__c);
                System.debug('mapSitePartner_Location value === ' + so.Site_Partner__c + ' == ' + mapSitePartner_Location.get(so.ERP_Site_Partner__c));
                //so.Site_Partner__c = mapOfAccNoAndId.get(so.ERP_Site_Partner__c);
            }else{
                so.Site_Partner__c = so.Sold_To__c;
            }

            //update Quote of Sales Order   
            if(so.Ext_Quote_Number__c != null && mapOfBMQuoteNameAndId != null && mapOfBMQuoteNameAndId.get(so.Ext_Quote_Number__c) != Null){
                so.Quote__c = mapOfBMQuoteNameAndId.get(so.Ext_Quote_Number__c);   
            }

            if(so.ERP_Site_Partner__c != null && mapOfPatnerNoAndERPPartnerId != null && mapOfPatnerNoAndERPPartnerId.get(so.ERP_Site_Partner__c) != Null){
                so.ERP_Partner__c= mapOfPatnerNoAndERPPartnerId.get(so.ERP_Site_Partner__c);
            }
        }   
    }
}
/*************************************************************************\    
@ Author                : Kaushiki Verma    
@ Date                  : 19/02/14   
@ Description           : This class is being called from triggers related to ERP Project object..    
@ Last Modified By      :   
@ Last Modified On      :    
@ Last Modified Reason  :   
****************************************************************************/
public class SR_class_ERPProject{

//method start kaushiki 19/02/14 US4163..
public static void populateFieldsOnErpProject(list<ERP_Project__c> erpProjList)
{
set<string> erpSitePartAndSoldTo= new set<string>();
map<string,id> accMap = new map<string,id>();
map<string,id> accMap2 = new map<string,id>();
map<string,id> salesOrderMap = new map<string,id>();
map<string,id> pmWorkOrderMap = new map<string,id>();
set<string> erpSoldToCodeList = new set<string>();
set<string> erpSalesOrderNumber = new set<string>();
set<string> erpPmWorkCenter = new set<string>();

    for(ERP_Project__c erp:erpProjList)
    {
    if(erp.ERP_Site_Partner_Code__c!=null)
        {
            erpSitePartAndSoldTo .add(erp.ERP_Site_Partner_Code__c);
        }
    if(erp.ERP_Sold_To_Code__c!=null)
        {
            erpSoldToCodeList.add(erp.ERP_Sold_To_Code__c);
        }
    if(erp.ERP_Sales_Order_Number__c!=null) 
        {
            erpSalesOrderNumber.add(erp.ERP_Sales_Order_Number__c);
        }
    if(erp.ERP_PM_Work_Center__c!=null) 
        {
            erpPmWorkCenter.add(erp.ERP_PM_Work_Center__c);
        }
    }
    
    for(Account acc: [select id,AccountNumber from account where AccountNumber in : erpSitePartAndSoldTo and recordtype.developername =:'Site_Partner' ])        
    {    
            accMap.put(acc.accountNumber,acc.id);
            system.debug('accmap is>>' +accMap);
            
    }
    for(Account acc1: [select id,AccountNumber from account where AccountNumber in : erpSoldToCodeList and recordtype.developername =:'Sold_to'  ])
    {
        accMap2.put(acc1.accountNumber,acc1.id);
        system.debug('accmap2 is>>' +accMap2);

    }
    for(Sales_Order__c so: [select id,ERP_Reference__c from Sales_Order__c where ERP_Reference__c in : erpSalesOrderNumber])
    {
        salesOrderMap.put(so.ERP_Reference__c,so.id);
    }
    for(SVMXC__Service_Group_Members__c tech: [select id,ERP_Work_Center__c from SVMXC__Service_Group_Members__c where ERP_Work_Center__c in : erpPmWorkCenter])
    {
        pmWorkOrderMap.put(tech.ERP_Work_Center__c,tech.id);
    }
    
    for(ERP_Project__c erp1:erpProjList)
    {
        if(accMap.size()>0 && accMap!=null)
        {
            erp1.Site_Partner__c = accMap.get(erp1.ERP_Site_Partner_Code__c);
            system.debug('site partner get is>>' +accMap.get(erp1.ERP_Site_Partner_Code__c));
    

        }
        if(accMap2.size()>0 && accMap2!=null)
        {
            erp1.Sold_To__c = accMap2.get(erp1.ERP_Sold_To_Code__c);
            system.debug('sold to get is>>' +accMap2.get(erp1.ERP_Sold_To_Code__c));

        }
        if(salesOrderMap.size()>0 && salesOrderMap!=null)
        {
            erp1.Sales_Order__c = salesOrderMap.get(erp1.ERP_Sales_Order_Number__c);
        }
        if(pmWorkOrderMap.size()>0 && pmWorkOrderMap!=null)
        {
            erp1.Primary_PM__c= pmWorkOrderMap.get(erp1.ERP_PM_Work_Center__c);
        }
        // erp1.ERP_Status__c = erp1.Status__c; //Copy from the interface field to the multi-select that the code uses //DE7931
    
    }
}
//method end kaushiki....
}
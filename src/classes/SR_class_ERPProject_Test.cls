/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_class_ERPProject_Test {

	public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();

    static testMethod void myUnitTest() {
    	Account sitePartner = UnityDataTestClass.AccountData(false);
    	sitePartner.RecordTypeId = accRecordType.get('Site Partner').getRecordTypeId();
    	sitePartner.AccountNumber = '123';
    	insert sitePartner;
    	
    	Account soldTo = UnityDataTestClass.AccountData(false);
    	soldTo.AccountNumber = '456';
    	insert soldTo;
    	
    	Sales_Order__c so = UnityDataTestClass.SO_Data(false, sitePartner);
    	so.ERP_Reference__c = '345';
    	insert so;
    	
    	BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
    	SVMXC__Service_Group__c serviceTeam = UnityDataTestClass.serviceTeam_Data(true);
    	SVMXC__Service_Group_Members__c grpMem = UnityDataTestClass.Techinician_Data(false, serviceTeam, businesshr);
    	grpMem.ERP_Work_Center__c = 'US123';
    	insert grpMem;
    	 
    	
        ERP_Project__c erpProj = UnityDataTestClass.ERPPROJECT_Data(false);
        erpProj.ERP_Site_Partner_Code__c = '123';
        erpProj.ERP_Sold_To_Code__c = '456';
        erpProj.ERP_Sales_Order_Number__c = '345';
        erpProj.ERP_PM_Work_Center__c = 'US123';
        insert erpProj;
    	
    	list<ERP_Project__c> erpProjList = new list<ERP_Project__c>();
    	erpProjList.add(erpProj);
    	
    	SR_class_ERPProject.populateFieldsOnErpProject(erpProjList);
    	
    }
}
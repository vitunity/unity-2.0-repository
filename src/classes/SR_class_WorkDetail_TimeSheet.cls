public class SR_class_WorkDetail_TimeSheet {
    public List<SVMXC__Service_Order_Line__c> TechnicianWDLst = new list<SVMXC__Service_Order_Line__c>();
    public string workdetailfields = '';
    // Initilaze the fields
    public  string sunday{get;set;}
    public  string Monday{get;set;}
    public  string Tuesday{get;set;}
    public  string Wednesday{get;set;}
    public  string Thursday{get;set;}
    public  string Friday{get;set;}
    public  string Saturday{get;set;}
    public boolean wdfound{get;set;}
    public List<DaysOftheweek> LstDay = new List<DaysOftheweek>();
    public List<DaysOftheweek> getLstDay(){
        return LstDay;
    }
    public void setLstDay(List<DaysOftheweek> Ls){
        this.LstDay =Ls ;
    }
    public map<string,List<SVMXC__Service_Order_Line__c>>mapofworkdetaildescandworkdet = new map<string,List<SVMXC__Service_Order_Line__c>>();
    public map<string,List<SVMXC__Service_Order_Line__c>> getmapofworkdetaildescandworkdet(){
        return mapofworkdetaildescandworkdet;
    }
    public void setmapofworkdetaildescandworkdet(map<string,List<SVMXC__Service_Order_Line__c>> mapofworkdetail){
        this.mapofworkdetaildescandworkdet = mapofworkdetail;
    }
    // Final Map of map
    public map<string,List<SVMXC__Service_Order_Line__c>>Finalmapofworkdetaildescandworkdet = new map<string,List<SVMXC__Service_Order_Line__c>>();
    public map<string,List<SVMXC__Service_Order_Line__c>> getFinalmapofworkdetaildescandworkdet (){
        return Finalmapofworkdetaildescandworkdet ;
    }
    public void setFinalmapofworkdetaildescandworkdet (map<string,List<SVMXC__Service_Order_Line__c>> mapofworkdetail){
        this.Finalmapofworkdetaildescandworkdet  = mapofworkdetail;
    }

    public SR_class_WorkDetail_TimeSheet(ApexPages.StandardController controller) {
        DaysOftheweek WeekDetail = new DaysOftheweek();
        sunday = date.valueof(system.now()).tostartofweek().format()+'         Sunday';
        date temp = date.valueof(system.now()).tostartofweek();
        Monday= date.valueof(temp+1).format()+'   Monday';
        Tuesday= date.valueof(temp +2).format()+'                      Tuesday';
        Wednesday = date.valueof(temp +3).format()+'                Wednesday';
        Thursday = date.valueof(temp +4).format()+'                 Thursday';
        Friday = date.valueof(temp +5).format()+'                    Friday';
        Saturday= date.valueof(temp +6).format()+'                   Saturday';
        LstDay.add(WeekDetail);
        Schema.DescribeSObjectResult workdetailobject = SVMXC__Service_Order_Line__c.sObjectType.getDescribe();
        Map<String,Schema.SObjectField> workdetailobjectfieldmap = workdetailobject.fields.getMap();
        for(Schema.SObjectField sobj:workdetailobjectfieldmap.values()){ 
            Schema.DescribeFieldResult fieldresult = sobj.getDescribe();
            if(fieldresult.isCreateable()){
                if(workdetailfields==''){
                    workdetailfields = fieldresult.getname();
                }
                else{
                    workdetailfields =workdetailfields +',' +fieldresult.getname();
                }   
            }
        }
        string UserId =UserInfo.getuserid();
        //string Query = 'Select Id,'+workdetailfields+ ' from SVMXC__Service_Order_Line__c where SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserId and SVMXC__Start_Date_and_Time__c!=null order by SVMXC__Start_Date_and_Time__c desc' ;
        // above line replaced for US4486 by Shubham Jaiswal on 14/5/2014
        string Query = 'Select Id,'+workdetailfields+ ' from SVMXC__Service_Order_Line__c where (SVMXC__Group_Member__r.User__c=:UserId OR SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserId) and SVMXC__Start_Date_and_Time__c!=null order by SVMXC__Start_Date_and_Time__c desc' ;
        system.debug('Query is--->'+Query );
        TechnicianWDLst = database.query(Query);
        system.debug('Work detaill List Is----->'+TechnicianWDLst);
        //TechnicianWDLst = [select Id,Exposure_Counter_Hours__c,SVMXC__Work_Description__c,SVMXC__Service_Order__c,SVMXC__Start_Date_and_Time__c,SVMXC__End_Date_and_Time__c from SVMXC__Service_Order_Line__c where SVMXC__Group_Member__r.SVMXC__Salesforce_User__c=:UserInfo.getuserid() order by SVMXC__Start_Date_and_Time__c ];
        
    }
    public pagereference Creatingworkorderlist(){
        try{
        for(SVMXC__Service_Order_Line__c wd:TechnicianWDLst){
            //system.debug(date.valueof(wd.SVMXC__Start_Date_and_Time__c).toStartOfWeek() );
            List<SVMXC__Service_Order_Line__c> Lstwd = new list<SVMXC__Service_Order_Line__c>();
            system.debug('First Point--->'+wd.SVMXC__Start_Date_and_Time__c);
            system.debug('second point--->'+date.valueof(wd.SVMXC__Start_Date_and_Time__c).tostartofweek().format());
            system.debug('Third Point--->'+Date.today().tostartofweek().format());
            if(wd.SVMXC__Start_Date_and_Time__c!=null && date.valueof(wd.SVMXC__Start_Date_and_Time__c).tostartofweek().format()== Date.today().tostartofweek().format()){
                system.debug('coming Inside-->');
                if(mapofworkdetaildescandworkdet.get(wd.SVMXC__Work_Description__c+wd.SVMXC__Service_Order__c)!=null){
                    Lstwd = mapofworkdetaildescandworkdet.get(wd.SVMXC__Work_Description__c+wd.SVMXC__Service_Order__c);
                    Lstwd.add(wd);  
                }
                else{
                    Lstwd.add(wd);
                }
            }
            system.debug('Lst wd is---->'+Lstwd);
                       
            if(wd.SVMXC__Work_Description__c!=null && wd.SVMXC__Start_Date_and_Time__c!=null && date.valueof(wd.SVMXC__Start_Date_and_Time__c).tostartofweek().format()== Date.today().tostartofweek().format()){
                mapofworkdetaildescandworkdet.put(wd.SVMXC__Work_Description__c+wd.SVMXC__Service_Order__c,Lstwd);
            }  
        }
        
        for(string str:mapofworkdetaildescandworkdet.keyset()){
            List<SVMXC__Service_Order_Line__c> TempList = new List<SVMXC__Service_Order_Line__c>();
            integer y=0;
            for(SVMXC__Service_Order_Line__c wod:mapofworkdetaildescandworkdet.get(str)){
                integer x = DayOfweek(wod.SVMXC__Start_Date_and_Time__c);
                system.debug('Id of record---->'+wod.Id);
                system.debug('coming in which order---->'+wod.SVMXC__Start_Date_and_Time__c);
                
                system.debug('List size---<'+TempList.size());
                system.debug('Value of x-->'+x);
                if(TempList.size()>=x){
                    if(x!=0){
                        TempList[x] = wod;
                    }
                    if(x==0){
                        TempList[x] = wod;
                    }
                    continue;
                }
                Integer z= y;
                for(integer i=z;i<x;i++){
                    SVMXC__Service_Order_Line__c temp = new SVMXC__Service_Order_Line__c();
                    TempList.add(temp);
                    y=y+1;
                }
                TempList.add(wod);
                y++;
                
            }
            for(integer r=y;r<=6;r++){
                SVMXC__Service_Order_Line__c tempnew = new SVMXC__Service_Order_Line__c();
                TempList.add(tempnew);
            }
            Finalmapofworkdetaildescandworkdet.put(str,TempList);
        }
        
        system.debug('Final Map is--->'+Finalmapofworkdetaildescandworkdet);
        if(Finalmapofworkdetaildescandworkdet.size()>0){
            wdfound = True;
        }
        return null;
        }
        catch(exception e){
            system.debug('exception occured-->'+e);
            return null;
        }
        
    }
    
    public pagereference SubMitTimesheet(){
        try{
        List<SVMXC__Service_Order_Line__c> WdToBeInserted = new list<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> WdToBeUpdated = new list<SVMXC__Service_Order_Line__c>();
        system.debug('--map detailare-->'+Finalmapofworkdetaildescandworkdet);
        for(string str:Finalmapofworkdetaildescandworkdet.keyset()){
            List<SVMXC__Service_Order_Line__c> TempList = new list<SVMXC__Service_Order_Line__c>();
            TempList = Finalmapofworkdetaildescandworkdet.get(str);
            system.debug('Temp List details---->'+TempList);
            system.debug('Temp List Id---->'+TempList[0].Id);
            integer Idfound;
            for(integer Lst=0;Lst<TempList.size();Lst++){
                if(TempList[Lst].Id!=null){
                    Idfound = Lst;
                    break;
                }
            }
            for(integer i=0;i<7;i++){
                system.debug('---helloo--final list-->'+TempList[i]);
                if(TempList[i].Id==null){
                    SVMXC__Service_Order_Line__c newone = new SVMXC__Service_Order_Line__c();
                    system.debug('helloo-------->'+TempList[i]);
                    if(TempList[i].Exposure_Counter_Hours__c!=null){
                        Integer hours = integer.valueof(TempList[i].Exposure_Counter_Hours__c);
                        newone = TempList[Idfound].clone(false,true);
                        newone.Exposure_Counter_Hours__c = TempList[i].Exposure_Counter_Hours__c;
                        newone.SVMXC__Start_Date_and_Time__c = date.today().tostartofweek()+i;
                        newone.SVMXC__End_Date_and_Time__c = newone.SVMXC__Start_Date_and_Time__c.addhours(integer.valueof(newone.Exposure_Counter_Hours__c));
                        system.debug('cloned list is---->'+newone);
                        system.debug('Starts Time is---->'+newone.SVMXC__Start_Date_and_Time__c);
                        system.debug('end Time is---->'+newone.SVMXC__End_Date_and_Time__c);
                        system.debug('Hours is------->'+newone.Exposure_Counter_Hours__c);
                        WdToBeInserted.add(newone);
                    }
                    
                    
                }
                if(TempList[i].Id!=null){
                    if(TempList[i].Exposure_Counter_Hours__c!=null){
                        TempList[i].SVMXC__End_Date_and_Time__c = TempList[i].SVMXC__Start_Date_and_Time__c.addhours(integer.valueof(TempList[i].Exposure_Counter_Hours__c));
                        WdToBeUpdated.add(TempList[i]);
                    }
                }
            }
        }
        for(integer i=0;i<WdToBeInserted.size();i++){
            system.debug('Starts Time BEFORE QUERY is---->'+WdToBeInserted[i].SVMXC__Start_Date_and_Time__c);
            system.debug('end Time is---->'+WdToBeInserted[i].SVMXC__End_Date_and_Time__c);
            system.debug('Hours is------->'+WdToBeInserted[i].Exposure_Counter_Hours__c);
        }
        insert WdToBeInserted;
        for(integer i=0;i<WdToBeInserted.size();i++){
            system.debug('Starts Time AFTER QUERY is---->'+WdToBeInserted[i].SVMXC__Start_Date_and_Time__c);
            system.debug('end Time is---->'+WdToBeInserted[i].SVMXC__End_Date_and_Time__c);
            system.debug('Hours is------->'+WdToBeInserted[i].Exposure_Counter_Hours__c);
        }
        update WdToBeUpdated;
        pagereference pg = new pagereference('/home/home.jsp');
        return pg;
        }
        catch(exception e){
            system.debug('exception occured---->'+e);
            return null;
        }
    }
    
    public integer DayOfweek(DateTime dt){
        string DayOfweek = dt.format('EEEE');
        system.debug('Date passed is---->'+dt);
        system.debug('---Day Of week---->'+DayOfweek);
        if(DayOfweek =='Sunday'){
            return 0;
        }
        if(DayOfweek =='Monday'){
            return 1;
        }
        if(DayOfweek =='Tuesday'){
            return 2;
        }
        if(DayOfweek =='Wednesday'){
            return 3;
        }
        if(DayOfweek =='Thursday'){
            return 4;
        }
        if(DayOfweek =='Friday'){
            return 5;
        }
        if(DayOfweek =='Saturday'){
            return 6;
        }
        return 10;
    }
    
    public class DaysOftheweek{
        public  string wd;
        public  string sunday;
        public  string Monday;
        public  string Tuesday;
        public  string Wednesday;
        public  string Thursday;
        public  string Friday;
        public  string Saturday;
        
    }

}
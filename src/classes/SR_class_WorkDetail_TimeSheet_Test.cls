/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SR_class_WorkDetail_TimeSheet_Test {
	
	public Static Account newAcc;
	public Static Contact newCont;
	public Static Case newCase;
	public Static SVMXC__Case_Line__c newCaseLine;
	public Static Product2 newProd;
	public Static Sales_Order__c SO;
	public Static Sales_Order_Item__c SOI;
	public static SVMXC__Service_Order__c WO;
	public static SVMXC__Service_Order_Line__c WD;
	public static SVMXC__Service_Order_Line__c WD1;
	public Static List<SVMXC__Service_Order_Line__c> WDList;
	public Static SVMXC__Service_Group__c servTeam;
	public Static SVMXC__Service_Group_Members__c technician;
	public Static SVMXC__Site__c newLoc;
	public Static BusinessHours businesshr;
	public Static ERP_WBS__c WBS;
	public Static ERP_NWA__c NWA;
	public Static ERP_Project__c erpProj;
	
	static {
		newAcc = UnityDataTestClass.AccountData(true);
		newProd = UnityDataTestClass.product_Data(false);
		SO = UnityDataTestClass.SO_Data(true,newAcc);
		SOI = UnityDataTestClass.SOI_Data(true, SO);
		
		businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
		servTeam = UnityDataTestClass.serviceTeam_Data(true);
		technician = UnityDataTestClass.Techinician_Data(true, servTeam, businesshr);
		
		newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
		
		erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
		
		WBS = UnityDataTestClass.ERPWBS_DATA(false, erpProj, technician);
		WBS.Sales_Order_Item__c = SOI.Id;
		WBS.Sales_Order__c = SO.Id;
		WBS.Acceptance_Date__c = system.today().addDays(-2);
		WBS.ERP_status__c = 'REL';
		insert WBS;
		
		NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
		NWA.ERP_Status__c = 'SETC;REL';
		NWA.ERP_Std_Text_Key__c = 'INST001';
		insert NWA;
		newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
		WO = UnityDataTestClass.WO_Data(true, newAcc, newCase, newProd, SOI, technician);
    	WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
		WD1 = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
		WD1.Exposure_Counter_Hours__c = 2;
		WDList = new List<SVMXC__Service_Order_Line__c>();
		WDList.add(WD);
		WDList.add(WD1);
		insert WDList;
	}

    static testMethod void LastDay_TestMethod() {
    	Test.StartTest();
    		Test.setCurrentPage(Page.SR_WorkDetail_TimeSheet);
         
        	SR_class_WorkDetail_TimeSheet timesheet = new SR_class_WorkDetail_TimeSheet(new ApexPages.StandardController(WD));
        	SR_class_WorkDetail_TimeSheet.DaysOftheweek days_Week = new SR_class_WorkDetail_TimeSheet.DaysOftheweek();
    		List<SR_class_WorkDetail_TimeSheet.DaysOftheweek> daysOfTheWeekList = new List<SR_class_WorkDetail_TimeSheet.DaysOftheweek>();
    		timesheet.setLstDay(daysOfTheWeekList);
    		timesheet.getLstDay();
    		
    		Map<String, List<SVMXC__Service_Order_Line__c>> mapofworkdetaildescandworkdet_Test = new Map<String, List<SVMXC__Service_Order_Line__c>>();
    		mapofworkdetaildescandworkdet_Test.put(WD.SVMXC__Work_Description__c + WD.SVMXC__Service_Order__c, WDList);
    		
    		timesheet.mapofworkdetaildescandworkdet.put(WD.Id, WDList);
    		timesheet.getmapofworkdetaildescandworkdet();
    		timesheet.setmapofworkdetaildescandworkdet(mapofworkdetaildescandworkdet_Test);
    		timesheet.setFinalmapofworkdetaildescandworkdet(mapofworkdetaildescandworkdet_Test);
    		timesheet.getFinalmapofworkdetaildescandworkdet();
    	Test.StopTest();
    }
    
    static testMethod void Creatingworkorderlist_Test() {
    	Test.StartTest();
    		Test.setCurrentPage(Page.SR_WorkDetail_TimeSheet);
         
        	SR_class_WorkDetail_TimeSheet timesheet = new SR_class_WorkDetail_TimeSheet(new ApexPages.StandardController(WD));
        	
        	Map<String, List<SVMXC__Service_Order_Line__c>> mapofworkdetaildescandworkdet_Test = new Map<String, List<SVMXC__Service_Order_Line__c>>();
    		for(SVMXC__Service_Order_Line__c WDS : WDList) 
    			mapofworkdetaildescandworkdet_Test.put(WDS.SVMXC__Work_Description__c + WDS.SVMXC__Service_Order__c, WDList);
    		
    		timesheet.mapofworkdetaildescandworkdet.put(WD.Id, WDList);
        	timesheet.Creatingworkorderlist();
    	Test.StopTest();
    }
    
    // Test Method for DayOfweek    
    static testMethod void DayOfweek_test() {
    	Test.StartTest();
    		Test.setCurrentPage(Page.SR_WorkDetail_TimeSheet);
         	SR_class_WorkDetail_TimeSheet timesheet = new SR_class_WorkDetail_TimeSheet(new ApexPages.StandardController(WD));
        	
	    	dateTime todaysDate = system.today();
	    	for(Integer i = 0; i < 7; i ++) {
	    		todaysDate = system.Today().addDays(i);
	    		timesheet.DayofWeek(todaysDate);
	    	}
    	Test.StopTest();
    }
    
    // Test Method for SubMitTimesheet Exception
    static testMethod void SubMitTimesheet_Exception_test() {
    	Test.StartTest();
    		Test.setCurrentPage(Page.SR_WorkDetail_TimeSheet);
         	SR_class_WorkDetail_TimeSheet timesheet = new SR_class_WorkDetail_TimeSheet(new ApexPages.StandardController(WD));
    		Map<String, List<SVMXC__Service_Order_Line__c>> mapofworkdetaildescandworkdet_Test = new Map<String, List<SVMXC__Service_Order_Line__c>>();
    		mapofworkdetaildescandworkdet_Test.put(WD.SVMXC__Work_Description__c + WD.SVMXC__Service_Order__c, WDList);
    		
    		SVMXC__Service_Order_Line__c WD2 = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
			WD2.Exposure_Counter_Hours__c = 2;
			WDList.add(WD2);
    		
    		for(SVMXC__Service_Order_Line__c WDS : WDList) 
    			mapofworkdetaildescandworkdet_Test.put(WDS.SVMXC__Work_Description__c + WDS.SVMXC__Service_Order__c, WDList);
    			
    		timesheet.setFinalmapofworkdetaildescandworkdet(mapofworkdetaildescandworkdet_Test);
    		
    		timesheet.SubMitTimesheet();
    	Test.StopTest();
    }
    
    // Test Method for SubMitTimesheet
    static testMethod void SubMitTimesheet_test() {
    	Test.StartTest();
    		Test.setCurrentPage(Page.SR_WorkDetail_TimeSheet);
         	SR_class_WorkDetail_TimeSheet timesheet = new SR_class_WorkDetail_TimeSheet(new ApexPages.StandardController(WD));
    		Map<String, List<SVMXC__Service_Order_Line__c>> mapofworkdetaildescandworkdet_Test = new Map<String, List<SVMXC__Service_Order_Line__c>>();
    		mapofworkdetaildescandworkdet_Test.put(WD.SVMXC__Work_Description__c + WD.SVMXC__Service_Order__c, WDList);
    		
    		SVMXC__Service_Order_Line__c WD2 = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
			WD2.Exposure_Counter_Hours__c = 2;
			WDList.add(WD2);
			
			for(Integer i = 0 ; i < 5; i ++)
				WDList.add(WD2);
    		
    		for(SVMXC__Service_Order_Line__c WDS : WDList) 
    			mapofworkdetaildescandworkdet_Test.put(WDS.SVMXC__Work_Description__c + WDS.SVMXC__Service_Order__c, WDList);
    			
    		timesheet.setFinalmapofworkdetaildescandworkdet(mapofworkdetaildescandworkdet_Test);
    		
    		timesheet.SubMitTimesheet();
    	Test.StopTest();
    }
}
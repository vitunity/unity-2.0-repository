public class SRaddCaseTeamMember
{

    public SRaddCaseTeamMember(ApexPages.StandardController controller) {}

       
    public pagereference caseTeamMember()
    {
        Id csId = apexpages.currentpage().getparameters().get('id');
        try{
            
            CaseTeamRole csRle = [SELECT Id FROM CaseTeamRole limit 1];
            
            CaseTeamMember csMem = new CaseTeamMember();
            
            csMem.MemberId = userInfo.getUserId();
            csMem.ParentId = csId;
            csMem.TeamRoleId = csRle.Id;
            if(([select count() from CaseTeamMember where MemberId = :userInfo.getUserId() and ParentId = :csId]) == 0)
            Insert csMem;
        }
        catch(Exception e)
        {}
        
        return new pagereference('/'+csId);
    }



}
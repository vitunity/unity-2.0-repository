@isTest
public class SRaddCaseTeamMember_test {

    public static testMethod void testSRaddCaseTeamMember() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
        recordtype rec = [Select id from recordtype where developername = 'Site_Partner' and SobjectType = 'Account'];
        Account testacc = new account(name = 'testaccount',recordtypeid = rec.id, country__c = 'India');
        insert testacc;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        insert objContact;
        CaseTeamRole testcaseteamrole = new CaseTeamRole(Name = 'testteamrole',AccessLevel = 'Edit');
        insert testcaseteamrole;
        Case testCase = new Case(Account = testacc, ContactID = objContact.id);
        insert testCase;
        CaseTeamMember testCaseTeamMembr = new CaseTeamMember(MemberId = userInfo.getUserId(),ParentId = testCase.id,TeamRoleId = testcaseteamrole.id);
        insert testCaseTeamMembr;
        apexpages.currentpage().getparameters().put('id',testCase.id);
        SRaddCaseTeamMember controller = new SRaddCaseTeamMember(new ApexPages.StandardController(testCase));
        controller.caseTeamMember();
        }
        
        }
        
      public static testMethod void myTestController2()
        {
       Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 
        system.runas(systemuser){
        recordtype rec = [Select id from recordtype where developername = 'Site_Partner' and SobjectType = 'Account'];
        Account testacc = new account(name = 'testaccount',recordtypeid = rec.id, country__c = 'India');
        insert testacc;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Contact';
        objContact.CurrencyIsoCode = 'USD';
        objContact.Email = 'test.tester@testing.com';
        objContact.MailingState = 'CA';
        objContact.Phone= '1235678';
        objContact.MailingCountry = 'USA';
        insert objContact;
        CaseTeamRole testcaseteamrole = new CaseTeamRole(Name = 'testteamrole',AccessLevel = 'Edit');
        insert testcaseteamrole;
        Case testCase = new Case(Account = testacc, ContactID = objContact.id);
        insert testCase;
        //apexpages.currentpage().getparameters().put('id',testCase.id);
        SRaddCaseTeamMember controller = new SRaddCaseTeamMember(new ApexPages.StandardController(testCase));
        controller.caseTeamMember();
        }
      }
}
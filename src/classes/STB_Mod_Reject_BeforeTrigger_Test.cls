/**
 
 */
@isTest
private class STB_Mod_Reject_BeforeTrigger_Test {

    public static ID TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();  
    public static ID hdRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();  
    public static ID FieldSeRecordTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();  
    public static ID UsageConRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
    public static ID ProdServRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();  
    //public static Profile pr = [Select id from Profile where name = 'VMS Service - User'];    
    //public static User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
    
    public static Account objAcc;
    public static Country__c con;
    public static Contact objCont;
    public static SVMXC__Installed_Product__c objIns;
    public static ERP_Timezone__c  objERP;
    public static Product2 objProd;
    public static SVMXC__Site__c varSite;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static ERP_Org__c varErp ;
    public static SVMXC__Service_Group_Members__c ObjTech;
    public static Case ObjCase;
    public static SVMXC__Service_Order__c objWO;
    public static SVMXC__Service_Level__c objSLATerm;
    public static SVMXC__Service_Order_Line__c objWD;
    public static SVMXC__Service_Contract__c varSC;
    public static SVMXC__Service_Offerings__c objSO;
    public static SVMXC__Service_Plan__c objSp;
    
    public static CountryDateFormat__c dateFormat; 
    Static {
        dateFormat = new CountryDateFormat__c(Name='Default',Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat;  
    
    //insert Country record
    con = new Country__c(Name = 'United States');
    insert con;
    
    //insert ERP Timezone Record
    objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
    insert objERP;
    
    //insert account Record
    objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id);
    objAcc.BillingCity = 'Milpitas';
    insert objAcc;
   
    //insert Contact Record
    objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
    insert objCont;
    
    //insert Product Record
    objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
    insert objProd;
    
    //insert location Record
     varSite = new SVMXC__Site__c(Sales_Org__c = '0600', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = objAcc.id);
     insert varSite;
     
     //insert sla term
     objSLATerm = new SVMXC__Service_Level__c(Name = 'Test SLA Term', SVMXC__Active__c = true, SVMXC__Restoration_Tracked_On__c = 'WorkOrder');
     insert objSLATerm;
     
    objSp = new SVMXC__Service_Plan__c(Name = 'test', SVMXC__Active__c = true, SVMXC__SLA_Terms__c = objSLATerm.id);
     insert objSp;
     //insert Service contract
    varSC = new SVMXC__Service_Contract__c(SVMXC__Company__c = objAcc.id, ERP_Contract_Type__c = 'ZH3', SVMXC__Service_Level__c = objSLATerm.id);
    insert varSC;
    
    objSO= new SVMXC__Service_Offerings__c(SLA_Term__c = objSLATerm.id, SVMXC__Labor_Discount_Covered__c = 100.00, SVMXC__Service_Plan__c = objSp.id);
    insert objSO;
     //insert Service Team Record
     ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(), SVMXC__Group_Code__c = 'ABC');
    insert ObjSvcTeam;
    
    //insert ERP org Record
    varErp = new ERP_Org__c(Name = '0600', Return_All__c = false, Sales_Org__c = '0600');
    insert varErp;
    
    //insert technician Record
    ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true, SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com', SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId(),ERP_Work_Center__c='H9876545321');
    insert ObjTech; 
    
    //insert Insalled Product Record
    objIns = new SVMXC__Installed_Product__c(SVMXC__Service_Contract__c = varSC.id, SVMXC__Site__c = varSite.ID, Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H1234', SVMXC__Company__c = objAcc.id, Name = 'H1234', SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id);
    insert objIns;
    
    ObjCase = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, SVMXC__Top_Level__c = objIns.id, Priority = 'Medium');
    insert objCase;
    }
    
    Static Profile serviceUserProfile;
    Static User serviceUser;
    
    public static User InsertUserData(Boolean isInsert) 
    {
        serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test@service.com');
        try {
        if(isInsert)
            insert serviceUser;
        } catch(Exception e) {
            system.debug(' == Exception == ' + e.getMessage() + ' = ' + e.getLineNumber());
        }
        return serviceUser;
    }
    
    public static testMethod void testUsageConsum()
    {
        test.StartTest();
        list<SVMXC__Service_Order_Line__c> listWorkDetail1 = new list<SVMXC__Service_Order_Line__c>();
        Map<ID, SVMXC__Service_Order_Line__c> mapOldWorkDetail1= new Map<ID, SVMXC__Service_Order_Line__c>();

        STB_Master__c stb = new STB_Master__c();
        stb.name = 'STB-XX-9999';
        stb.Job_Number__c = '1234';
        stb.Product_Codes_Affected__c = '19,29';
        stb.Type__c ='1';
        stb.is_Open__c = True;
        insert stb;
        system.assertNotEquals(null,stb.id);
                
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Case__c=objCase.id, SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium', SVMXC__Top_Level__c = objIns.id,SVMXC__Group_Member__c=ObjTech.id);
        insert objWO;
        SVMXC__Service_Order_Line__c objps = [select id, name, recordtypeId, SVMXC__Serial_Number__c, STB_Master__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c = :objWO.id];
        System.debug('objps+======='+objps);
        
        objWD = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c= objWo.id, SVMXC__Work_Detail__c = objps.id, RecordTypeId = UsageConRecordTypeID, SVMXC__Group_Member__c = ObjTech.id, STB_Master__c = stb.Id,   SVMXC__Start_Date_and_Time__c =  System.now(),
        SVMXC__End_Date_and_Time__c = System.now().addMinutes(30), SVMXC__Line_Type__c = 'Travel', SVMXC__Serial_Number__c = objIns.id, Covered_or_Not_Covered__c = 'Covered',
        Mod_Status__c='V - Verified',Method__c='In Person', SVMXC__Line_Status__c = 'Approved', Interface_Status__c = 'Processed');
        insert objWD;
        
        objWD = [select id, name, STB_Master__c,SVMXC__Line_Status__c, Interface_Status__c, Mod_status__c, Method__c, 
                        SVMXC__Serial_Number__c, SVMXC__Work_Detail__c, SVMXC__Group_Member__c
                 from SVMXC__Service_Order_Line__c where id = :objWD.id];
        system.assertNotEquals(null,objWD.STB_master__c);
        
        // Creating STB Mod Data
        STB_mod__c stbMod = new STB_mod__c();
        stbMod.STB_Master__c = stb.Id;
        stbMod.Status__c = 'Submitted';
        stbMod.Work_Details__c = objWD.Id;
        stbMod.Installed_Product__c = objins.id;
        insert stbMod;
        /*
        List<STB_Mod__c> modcards = [select id, name, stb_master__c, Case_Number__c,Badge_Number__c, Completion_Date__c,Done_Remotely__c,Installed_Product__c,Mod_Status__c,PCSN__c,Rep_Name__c,Status__c,STB_Mod_Name__c,work_Order_Number__c 
            from STB_Mod__c where work_Details__c = :objWd.id];
        
        system.assertNotEquals(0,modcards.size());
        system.assertEquals(stb.id,modcards[0].stb_master__c);
        system.assertNotEquals(null,modcards[0].case_number__c);
        system.assertNotEquals(null,modcards[0].Badge_Number__c);
        system.assertNotEquals(null,modcards[0].Completion_Date__c);
        system.assertEquals(false,modcards[0].Done_Remotely__c);
        system.assertEquals(objins.id,modcards[0].installed_product__c);
        system.assertEquals(objins.ERP_Reference__c,modcards[0].PCSN__c);
        system.assertEquals('V - Verified',modcards[0].Mod_Status__c);
        system.assertNotEquals(null,modcards[0].STB_Mod_Name__c);
        system.assertNotEquals(null,modcards[0].work_Order_Number__c);
        */
        //List<Optional_STB__c> optSTBs = [select id from Optional_STB__c where STB_Master__c = :Stb.id];
        //system.assertNotEquals(0,optSTBs.size());
        
        
        
        //stb.is_Open__c = false;
        //update stb;
        
       
        test.stopTest();
        
        
        
        
        SVMXC__Service_Order_Line__c objPTWDUC= new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = objWO.id, recordtypeId = UsageConRecordTypeID,SVMXC__Group_Member__c = ObjTech.id, SVMXC__Start_Date_and_Time__c =  System.now(),
        SVMXC__End_Date_and_Time__c = System.now().addHours(25), SVMXC__Line_Type__c = 'Parts', Old_Part_Number__c = objProd.id, SVMXC__Work_Detail__c = objps.id, Related_Activity__c = objWD.id, Covered_or_Not_Covered__c = 'Not Covered', SVMXC__Serial_Number__c = objIns.id);
        insert objPTWDUC;
        //listWorkDetail1.add(objWDUC);
        STB_mod__c mod = [Select ID, LastModifiedDate, Installed_Product__c, STB_Master__c, Work_Details__c from STB_Mod__c where
                                        Installed_Product__c =: objins.id and STB_Master__c =: stb.id and Work_Details__c =: objWD.id Limit 1];
        STB_Reject__c rej = new STB_Reject__c();
        rej.PCSN__c = objIns.ERP_Reference__c;
        rej.STB_Name__c = 'STB-XX-9999';
        rej.Work_Detail_Name__c = objWD.name;
        rej.Rev_Date__c = mod.LastModifiedDate.date();
        insert rej;
        
        rej= [select id, STB_Mod__c, Installed_Product__c, STB_Master__c, Work_Detail__c from STB_Reject__c where id = :rej.id];
        
        //system.assertEquals(objins.id,rej.installed_product__c);
        //system.assertEquals(objWD.id,rej.Work_Detail__c);
        //system.assertEquals(stb.id,rej.STB_Master__c);
        //system.assertNotEquals(null,rej.STB_Mod__c);
        //SRClassWorkOrderDetail objWDC =new SRClassWorkOrderDetail();
         //objWDC.updateFieldsOnWorkDetail(listWorkDetail1,null );
       // System.debug('-------->objWDUC'+objWDUC);
        
    }
}
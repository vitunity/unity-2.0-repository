/**
 *@description Creates subscription installation plan
 *@author Krishna Katve
 */
public with sharing class SaasInstallation{
    
    
    static List<Product2> fullScaleList = new List<Product2>();
    /**
     * Creates records of Installation case, Case lines and Installed product for new subscription
     */
    public static void createInstallationPlan(List<Subscription__c> subscriptions, Map<Id, Subscription__c> oldSubscriptions){
        Set<Id> quoteIds = new Set<Id>();
        Set<String> functionalLocations = new Set<String>();
        List<Subscription__c> subscriptionsToProcess = new List<Subscription__c>();
        Map<String,Id> primaryContacts = new Map<String,Id>();
        Map<String,String> poNumbers = new Map<String, String>();
        Map<String,Id> erpPcodeIds = new Map<String,Id>();
        Map<String,String> parentPCSN = new Map<String,String>();
        Set<String> equipmentCodes = new Set<String>();
        Set<String> projectManagerNumbers = new Set<String>();
        system.debug('subscriptions=='+subscriptions);
        for(Subscription__c saasSubscription : subscriptions){
           
            //if(!String.isBlank(saasSubscription.Functional_Location__c) && !String.isBlank(saasSubscription.PCSN__c) && saasSubscription.ERP_Project__c && 
            //((oldSubscriptions!=null && (saasSubscription.Functional_Location__c!=oldSubscriptions.get(saasSubscription.Id).Functional_Location__c || oldSubscriptions.get(saasSubscription.Id).ERP_Project__c == false)) || oldSubscriptions==null)){
           //Changed by Harshal for STRY0055313 
           system.debug('check condition=='+(!String.isBlank(saasSubscription.PCSN__c)&&((oldSubscriptions!=null && oldSubscriptions.get(saasSubscription.Id).PCSN__c!=saasSubscription.PCSN__c) || oldSubscriptions==null)));
           if(!String.isBlank(saasSubscription.PCSN__c)&&((oldSubscriptions!=null && oldSubscriptions.get(saasSubscription.Id).PCSN__c!=saasSubscription.PCSN__c) || oldSubscriptions==null)){
                
                subscriptionsToProcess.add(saasSubscription);
                quoteIds.add(saasSubscription.Quote__c);
                
                if(String.isNotBlank(saasSubscription.Functional_Location__c)){
                    functionalLocations.add(saasSubscription.Functional_Location__c);
                }
                system.debug('saasSubscription pcsn==='+(saasSubscription.PCSN__c.split('-')[1]));
                equipmentCodes.add(saasSubscription.PCSN__c.split('-')[1]);
                system.debug('equipmentCodes=='+equipmentCodes);
                projectManagerNumbers.add(saasSubscription.Site_Project_Manager__c);
            }
        }
        system.debug('equipmentCodes 11=='+equipmentCodes);
        //List to insert installedProductsinstallationCases,caseLines related to subscriptions
        List<SVMXC__Installed_Product__c> installedProducts = new List<SVMXC__Installed_Product__c>();
        List<Case> installationCases = new List<Case>();
        List<SVMXC__Case_Line__c> caseLines = new List<SVMXC__Case_Line__c>();
        List<Subscription_Product__c> subscriptionProducts = new List<Subscription_Product__c>();
        
        Map<String,Id> locationIds = getLocationRecords(functionalLocations);
        system.debug('^^locationIds=='+locationIds);
        
        Map<String,Id> subscriptionProductIds = getHeaderProductsDetails(quoteIds, primaryContacts, poNumbers, erpPcodeIds, parentPCSN);
        Map<Id,Id> erpNWAIds = getSubscriptionNWAMap(subscriptionsToProcess);
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('INST').getRecordTypeId();
        Id caseLineRecordTypeId = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Case Line').getRecordTypeId();
        Map<String, Id> parentInstalledProducts = getParentInstalledProducts(parentPCSN);
        Map<String, Product2> saasModels = getSubscriptionModels(equipmentCodes);
        system.debug('saasModels =='+saasModels);
        Map<String, User> projectManagers = getProjectManagers(projectManagerNumbers);
        
        //Create installation case/installed product/case line for every subscription
        for(Subscription__c saasSubscription : subscriptionsToProcess){
            Id locationId = (locationIds!=null && locationIds.size()>0)?locationIds.get(saasSubscription.Functional_Location__c):null;
            Id productId = subscriptionProductIds.get(saasSubscription.Name);
            String parentPCSNNumber = parentPCSN.get(saasSubscription.Name);
            Id nwaId = erpNWAIds.get(saasSubscription.Id);
            system.debug('saasSubscription.PCSN__c==='+saasSubscription.PCSN__c);
            String productCode = saasSubscription.PCSN__c.split('-')[1];
            
            SVMXC__Installed_Product__c installedProduct = getInstalledProduct(saasSubscription, locationId, saasModels.get(productCode), parentInstalledProducts.get(saasSubscription.Name));
            Id saasProductId;
            
            if((saasModels!=null && saasModels.size()>0) && (productCode!=null && productCode.length()>0)){
                system.debug('=saasModels.chk=='+saasModels.get(productCode).Id);
                saasProductId=saasModels.get(productCode).Id;
            }
            Case installationCase = getInstallationCase(saasSubscription, locationId, caseRecordTypeId,(primaryContacts!=null? primaryContacts.get(saasSubscription.Name):null), installedProduct,saasProductId);
            
            if(projectManagers.containsKey(saasSubscription.Site_Project_Manager__c)){ 
                installationCase.OwnerId = projectManagers.get(saasSubscription.Site_Project_Manager__c).Id;
            }
            
            installedProducts.add(installedProduct);
            installationCases.add(installationCase);
            caseLines.add(
                getCaseLine(installationCase,saasProductId, installedProduct, locationId, poNumbers.get(saasSubscription.Name), saasSubscription.Number_Of_Licences__c, caseLineRecordTypeId, nwaId)
            );
            subscriptionProducts.add(getSubscriptionProduct(installedProduct, locationId, saasSubscription));
        }
        
        System.debug('----installedProducts'+installedProducts);
        if(!installedProducts.isEmpty()) insert installedProducts;
        
        for(Case installationCase : installationCases){
            installationCase.ProductSystem__c = installationCase.ProductSystem__r.Id;
        }
        System.debug('----installationCases'+installationCases);
        if(!installationCases.isEmpty()) insert installationCases;
        
        for(SVMXC__Case_Line__c caseLine : caseLines){
            caseLine.SVMXC__Case__c = caseLine.SVMXC__Case__r.Id;
            caseLine.SVMXC__Installed_Product__c = caseLine.SVMXC__Installed_Product__r.Id;
            System.debug('----caseLine.SVMXC__Installed_Product__c'+caseLine.SVMXC__Installed_Product__c);
        }
        System.debug('----caseLine'+caseLines);
        if(!caseLines.isEmpty()) insert caseLines;
        
        for(Subscription_Product__c subProduct : subscriptionProducts){
            subProduct.Installed_Product__c = subProduct.Installed_Product__r.Id;
        }
        
        if(!subscriptionProducts.isEmpty()) insert subscriptionProducts;
    }
    
    /**
     * Get location records related to subscriptions. Used for creating installed products
     */
    private static Map<String, Id> getLocationRecords(Set<String> functionalLocations){
        Map<String,Id> locationIds = new Map<String, Id>();
        for(SVMXC__Site__c location : [SELECT Id,ERP_Functional_Location__c FROM SVMXC__Site__c WHERE ERP_Functional_Location__c IN:functionalLocations]){
            locationIds.put(location.ERP_Functional_Location__c, location.Id);
        }
        return locationIds;
    }
    
    /**
     * Create installed product instance using subscription
     */
   /* private static SVMXC__Installed_Product__c getInstalledProduct(Subscription__c saasSubscription, Id locationId, Product2 product, Id parentId){
        
        Product2 localProd = product;
        System.debug('localProd+++++'+localProd);
        
        if((saasSubscription.Product_Type__c.equalsIgnoreCase('FS') && localProd.productCode.substring(0,3) == 'HMS'))
        {
            List<Product2> tempList = new List<Product2>();
            System.debug('fullScaleList+++++++'+fullScaleList);
            localProd = fullScaleList [0];
            System.debug('localProd+++++++'+localProd);
        }
        System.debug('Outside if');
        System.debug('localProd.Id+++++'+localProd.Id);
        return new SVMXC__Installed_Product__c(
            Name = saasSubscription.Name,
            //STSK0011740 - Start -- Changed Status to 'Ordered' From 'Pending Installation',
            SVMXC__Status__c = 'Ordered',
            Interface_Status__c = 'Process',
            Billing_Type__c = 'P – Paid Service',
            ERP_Functional_Location__c = saasSubscription.Functional_Location__c,
            SVMXC__Company__c = saasSubscription.Account__c,
            SVMXC__Site__c = locationId,
            SVMXC__Product__c = localProd.Id,
            ERP_Pcodes__c = localProd.ERP_PCode__c,
            SVMXC__Serial_Lot_Number__c = saasSubscription.PCSN__c.remove(localProd.ERP_PCode__r.Name).remove('-'+localProd.Service_Product_Group__c).trim(),
            ERP_EQUIPMENT__c = saasSubscription.PCSN__c,
            Subscription__c = saasSubscription.Id,
            SVMXC__Parent__c = parentId,
            Interface_Change_Type__c = localProd.Assign_PCSN_Type__c
        );
    }*/
    
    private static SVMXC__Installed_Product__c getInstalledProduct(Subscription__c saasSubscription, Id locationId, Product2 product, Id parentId){
        
        Product2 localProd = product;
        
        if((saasSubscription !=null && localProd!=null)&&(saasSubscription.Product_Type__c.equalsIgnoreCase('FS') && localProd.productCode.substring(0,3) == 'HMS'))
        {
            List<Product2> tempList = new List<Product2>();
            System.debug('fullScaleList+++++++'+fullScaleList);
            localProd = fullScaleList [0];
            System.debug('localProd+++++++'+localProd);
        }
        System.debug('Outside if');
        //System.debug('localProd.Id+++++'+localProd.Id);
        
        SVMXC__Installed_Product__c objInstalledProduct=new SVMXC__Installed_Product__c();
        objInstalledProduct.Name = saasSubscription.Name;
        //STSK0011740 - Start -- Changed Status to 'Ordered' From 'Pending Installation',
        objInstalledProduct.SVMXC__Status__c = 'Ordered';
        objInstalledProduct.Interface_Status__c = 'Process';
        objInstalledProduct.Billing_Type__c = 'P – Paid Service';
        objInstalledProduct.ERP_Functional_Location__c = saasSubscription.Functional_Location__c;
        objInstalledProduct.SVMXC__Company__c = saasSubscription.Account__c;
        objInstalledProduct.SVMXC__Site__c = locationId;
        objInstalledProduct.ERP_EQUIPMENT__c = saasSubscription.PCSN__c;
        objInstalledProduct.Subscription__c = saasSubscription.Id;
        objInstalledProduct.SVMXC__Parent__c = parentId;
        
        if(localProd!=null){
            objInstalledProduct.Interface_Change_Type__c = localProd.Assign_PCSN_Type__c;
            objInstalledProduct.SVMXC__Product__c = localProd.Id;
            objInstalledProduct.ERP_Pcodes__c = localProd.ERP_PCode__c;
            objInstalledProduct.SVMXC__Serial_Lot_Number__c = saasSubscription.PCSN__c.remove(localProd.ERP_PCode__r.Name).remove('-'+localProd.Service_Product_Group__c).trim();
        }
        else{
            objInstalledProduct.Interface_Message__c ='Missing ERP Pcode';
        }
        return objInstalledProduct;
    }
    
    
    /**
     * Create installation case for Saas subscription
     */
    private static Case getInstallationCase(Subscription__c saasSubscription, Id locationId, Id recordTypeId, Id primaryContactId, SVMXC__Installed_Product__c insProd, Id saasProductId){
        Case objCase=new Case();
            
            objCase.AccountId = saasSubscription.Account__c;
            objCase.SVMXC__Site__c = locationId;
            objCase.RecordTypeId = recordTypeId;
            objCase.Subject = 'Subscription Installation';
            objCase.ERP_Project_Number__c = saasSubscription.ERP_Project_Number__c;
            objCase.Origin = 'Sales Order/Entitlement';
            objCase.ContactId = primaryContactId;
            objCase.ProductSystem__r = insProd;
            if(saasProductId!=null)
            objCase.ProductId = saasProductId;
            objCase.Subscription__c = saasSubscription.Id;
        
         return objCase;      
    }
    
    /**
     * Create case line for Saas subscription
     */
    private static SVMXC__Case_Line__c getCaseLine(Case instCase, Id productId, SVMXC__Installed_Product__c insProd, Id locationId, String poNumber, Decimal quantity, Id recTypeId, Id nwaId){
        
        SVMXC__Case_Line__c objCaseLine= new SVMXC__Case_Line__c ();
            if(instCase!=null)
            objCaseLine.SVMXC__Case__r = instCase;
            if(productId!=null)
            objCaseLine.SVMXC__Product__c = productId;
             if(insProd!=null)
            objCaseLine.SVMXC__Installed_Product__r = insProd;
             if(locationId!=null)
            objCaseLine.SVMXC__Location__c = locationId;
             if(poNumber!=null)
            objCaseLine.PO_Number__c = poNumber;
             if(quantity!=null)
            objCaseLine.SVMXC__Quantity__c = quantity;
            objCaseLine.RecordTypeId = recTypeId;
            objCaseLine.SVMXC__Line_Status__c = 'Open';
             if(nwaId!=null)
            objCaseLine.ERP_NWA__c = nwaId;
        
        return objCaseLine;
    }
    
    /**
     * Get parent installed product ids to link saas installed product to header
     */
    private static Map<String, Id> getParentInstalledProducts(Map<String,String> parentPCSN){
        Map<String, Id> parentInstalledProducts = new Map<String, Id>();
        for(SVMXC__Installed_Product__c parentIP : [
            SELECT ID, ERP_Reference__c, Name
            FROM SVMXC__Installed_Product__c
            WHERE NAME IN :parentPCSN.values()
        ]){
            parentInstalledProducts.put(parentIP.Name, parentIP.Id);
        }
        return parentInstalledProducts;
    }
    
    /**
     * Create instance of subscriptiot using installed product and location.
     */
    private static Subscription_Product__c getSubscriptionProduct(SVMXC__Installed_Product__c installedProduct, Id locationId, Subscription__c saasSubscription){
        return new Subscription_Product__c(
            ERP_Equipment_Nbr__c = installedProduct.ERP_EQUIPMENT__c,
            ERP_Partner_Number__c = saasSubscription.ERP_Site_Partner__c,
            Installed_Product__r = installedProduct,
            Subscription__c = saasSubscription.Id,
            Location__c = locationId
        );
    }
    
    /**
     * Get header products related to each subscription to create installed product
     */
    private static Map<String,Id> getHeaderProductsDetails(Set<Id> quoteIds, Map<String, Id> primaryContacts, 
    Map<String,String> poNumbers, Map<String,Id> erpPcodeIds, Map<String,String> parentPCSN){
        List<BigMachines__Quote_Product__c> quoteProducts = [
            SELECT Id,Name,BigMachines__Product__c,Subscription_Product_Type__c,BigMachines__Quote__r.Principal_Contact__c,
            BigMachines__Quote__r.Purchase_Order_Number__c,SAP_Contract_Number__c,BigMachines__Product__r.ERP_Pcode__c,Parent_PCSN__c
            FROM BigMachines__Quote_Product__c
            WHERE Subscription_Product_Type__c!=''
            AND BigMachines__Quote__c IN: quoteIds
            AND Header__c = true
        ];
        
        Map<String,Id> subscriptionProductIds = new Map<String,Id>();
        
        for(BigMachines__Quote_Product__c quoteProduct : quoteProducts){
            subscriptionProductIds.put(quoteProduct.SAP_Contract_Number__c, quoteProduct.BigMachines__Product__c);
            primaryContacts.put(quoteProduct.SAP_Contract_Number__c, quoteProduct.BigMachines__Quote__r.Principal_Contact__c);
            poNumbers.put(quoteProduct.SAP_Contract_Number__c, quoteProduct.BigMachines__Quote__r.Purchase_Order_Number__c);
            erpPcodeIds.put(quoteProduct.SAP_Contract_Number__c, quoteProduct.BigMachines__Product__r.ERP_Pcode__c);
            parentPCSN.put(quoteProduct.SAP_Contract_Number__c, quoteProduct.Parent_PCSN__c);
        }
        return subscriptionProductIds;
    }
    
    /**
     * Get project managers to assign installation cases
     */
    private static Map<String, User> getProjectManagers(Set<String> pmERPNumbers){
        Map<String,String> emailWithERPNumbers = new Map<String,String>();
        Map<String, User> projectManagers = new Map<String,User>();
        
        for(Contact internalContact :[
            SELECT Id, Email, ERP_Reference__c 
            FROM Contact
            WHERE ERP_Reference__c IN:pmERPNumbers
            AND ERP_Reference__c!=null
        ]){
            emailWithERPNumbers.put(internalContact.Email, internalContact.ERP_Reference__c);
        }
        
        for(User pmUser : [SELECT Id, Email FROM USER WHERE Email IN:emailWithERPNumbers.keySet()]){
            if(emailWithERPNumbers.containsKey(pmUser.Email)){
                projectManagers.put(emailWithERPNumbers.get(pmUser.Email), pmUser);
            }
        }
        return projectManagers;
    }
    /**
     * Get subscription models
     */
    private static Map<String,Product2> getSubscriptionModels(Set<String> saasProductCodes){
        Map<String,Product2> saasModels = new Map<String,Product2>();
        system.debug('saasProductCodes=='+saasProductCodes);
        List<Product2> saasServiceProducts = [
            SELECT Id, ProductCode, ERP_Pcode__c,ERP_PCode__r.Name, Service_Product_Group__c,Assign_PCSN_Type__c
            FROM Product2
            WHERE Service_Product_Group__c IN:saasProductCodes
        ];
        system.debug('==saasServiceProducts =='+saasServiceProducts);
        
        for(Product2 product : saasServiceProducts){
            saasModels.put(product.Service_Product_Group__c,product);
            
            if(product.productCode == 'HMSC')
                fullScaleList.add(product);
        }
        system.debug('==saasModels=='+saasModels);
        return saasModels;
    }
    
    /**
     * ERP NWA ids to update on case lines
     */
    private static Map<Id,Id> getSubscriptionNWAMap(List<Subscription__c> subscriptions){
        System.debug(LoggingLevel.INFO+'-----subscriptions'+subscriptions);
        
        Map<Id,Id> erpNWAIds = new Map<Id,Id>();
        for(ERP_NWA__c erpnwa :[
            SELECT Id, Name,ERP_Project__r.Subscription__c
            FROM ERP_NWA__c
            WHERE ERP_Project__r.Subscription__c IN :subscriptions
            AND ERP_Std_Text_Key__c = 'INST001'
        ]){
            System.debug(LoggingLevel.INFO+'-----erpnwa'+erpnwa);
            erpNWAIds.put(erpnwa.ERP_Project__r.Subscription__c, erpnwa.Id);                                    
        }
        return erpNWAIds;
    } 
    
    /**
     * This method creates product service records for SAAS IWO
     */
    public static String createWorkDetailRecords(
        List<SVMXC__Service_Order_Line__c> InsertObjWorkDetailList, 
        List<SVMXC__Service_Order__c> ListInstallationWOInserted,
        SVMXC__Service_Order__c wo,
        SVMXC__Case_Line__c objCaseLine,
        Id workDetrectypeId,
        Map<id,SVMXC__Service_Order__c> mapInstallationWOUpdate,
        Map <Id,SVMXC__Installed_Product__c> mapIP
    ){
        set<Id> projectManagerIdSet = new set<Id>();
        map<Id,Id> ManagerServiceTeamMap = new map<Id,Id>();
        list<SVMXC__Service_Group_Members__c> technicianServiceTeamList = new list<SVMXC__Service_Group_Members__c>();
        for(SVMXC__Service_Order__c iwos : ListInstallationWOInserted){
            if(iwos.ProjectManager__c != null)
                projectManagerIdSet.add(iwos.ProjectManager__c);
        }
        technicianServiceTeamList = [Select Id, User__c, SVMXC__Service_Group__c
                                    From SVMXC__Service_Group_Members__c 
                                    Where SVMXC__Salesforce_User__c IN:  projectManagerIdSet];
        for(SVMXC__Service_Group_Members__c st :technicianServiceTeamList ){
            ManagerServiceTeamMap.put(st.User__c,st.SVMXC__Service_Group__c);
        }
        
        String message = '';
        for(SVMXC__Service_Order__c iwo : ListInstallationWOInserted){
            if (objCaseLine.SVMXC__Location__c == null) {
                message += 'Location is missing on '+objCaseLine.Sales_Order_Item__r.ERP_Reference__c+';';
            }
            if (objCaseLine.SVMXC__Product__c == null) {
                message += 'Product is missing on '+objCaseLine.Sales_Order_Item__r.ERP_Reference__c+';';
            }
            if (objCaseLine.SVMXC__Installed_Product__c == null) {
                message += 'Installed Product is missing on '+objCaseLine.Sales_Order_Item__r.ERP_Reference__c+';';
            }
            SVMXC__Service_Order_Line__c objWorkDetailTemp = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = iwo.id,Case_Line__c = objCaseLine.id);  
            objWorkDetailTemp.Billing_Type__c = 'I – Installation';
            objWorkDetailTemp.SVMXC__Group_Member__c = iwo.SVMXC__Group_Member__c;                                              
            objWorkDetailTemp.SVMXC__Line_Type__c = 'Miscelleaneous';
            objWorkDetailTemp.SVMXC__End_Date_and_Time__c = objCaseLine.End_date_time__c;
            objWorkDetailTemp.SVMXC__Serial_Number__c = objCaseLine.SVMXC__Installed_Product__c;
            objWorkDetailTemp.SVMXC__Line_Status__c = objCaseLine.SVMXC__Line_Status__c;
            objWorkDetailTemp.SVMXC__From_Location__c = objCaseLine.SVMXC__Location__c;
            objWorkDetailTemp.SVMXC__Work_Description__c = objCaseLine.SVMXC__Description__c;
            objWorkDetailTemp.SVMXC__Product__c = objCaseLine.SVMXC__Product__c;
            objWorkDetailTemp.SVMXC__Start_Date_and_Time__c = objCaseLine.Start_Date_time__c;
            objWorkDetailTemp.recordtypeid = workDetrectypeId;
            objWorkDetailTemp.Malfunction_Start_Date__c =iwo.Created_Date_Time__c;
            objWorkDetailTemp.ERP_PCSN__c = objCaseLine.SVMXC__Installed_Product__r.PCSN__c;
            objWorkDetailTemp.Sub_Equipment__c = objCaseLine.Sales_Order_Item__r.Installed_Product__c;
            objWorkDetailTemp.ERP_NWA__c = objCaseLine.ERP_NWA__C;
            objWorkDetailTemp.ERP_WBS_Nbr__c = objCaseLine.ERP_NWA__r.ERP_WBS_Nbr__c;
            objWorkDetailTemp.ERP_WBS__c = objCaseLine.ERP_NWA__r.WBS_Element__c;

            if(objCaseLine.ERP_NWA__r.ERP_Project_Nbr__c != null){                                         
                iwo.ERP_Project_Nbr__c = objCaseLine.ERP_NWA__r.ERP_Project_Nbr__c;
                mapInstallationWOUpdate.put(iwo.id,iwo);
            }
            
            system.debug('+++iwo.ProjectManager__c+++'+iwo.ProjectManager__c);
            system.debug('+++technicianServiceTeamList  +++'+ technicianServiceTeamList );
            if(objCaseLine.SVMXC__Installed_Product__c != null){ //DE1500
                if (objCaseLine.SVMXC__Installed_Product__r.SVMXC__Top_Level__c == null){
                    iwo.SVMXC__Top_Level__c = objCaseLine.SVMXC__Installed_Product__c; 
                    if(ManagerServiceTeamMap.containsKey(iwo.ProjectManager__c))
                    iwo.Service_Team__c = ManagerServiceTeamMap.get(iwo.ProjectManager__c);
                }
                else{
                    iwo.SVMXC__Top_Level__c = objCaseLine.SVMXC__Installed_Product__r.SVMXC__Top_Level__c;
                }
                iwo.SVMXC__Site__c = objCaseLine.SVMXC__Location__c;
                iwo.Sales_Org__c = objCaseLine.SVMXC__Case__r.Subscription__r.ERP_Sales_Org__c;
                objWorkDetailTemp.Top_Level_PCSN__c = objCaseLine.SVMXC__Installed_Product__r.Top_Level_PCSN__c;
                mapInstallationWOUpdate.put(iwo.id,iwo);
            }
            InsertObjWorkDetailList.add(objWorkDetailTemp);
        }
        return message;
    }
}
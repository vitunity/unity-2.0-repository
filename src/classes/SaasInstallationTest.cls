@isTest(seeAllData=true)
public class SaasInstallationTest {
    
    private static Product2 productModel;
    private static Contact con;
    private static SVMXC__Installed_Product__c TestIP;
    private static SVMXC__Site__c location;
    private static Case testCase;
    
    static testMethod void myUnitTest() {
        
        setUpSubscriptionTestData();
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425',BillingCity='LA',BillingState='CA',State_Province_Is_Not_Applicable__c=true);
        insert acc;
        
        SVMXC__Site__c location=new SVMXC__Site__c(name='SPIRE SPECIALIST CARE CENTRE',SVMXC__Account__c=acc.id,ERP_Functional_Location__c='H-CHELMSFORD -GB-EU-001');
        insert location;
        List<Subscription__c> subscriptions = new List<Subscription__c>();
        Test.startTest();
            Subscription__c subscription1 = new Subscription__c();
            subscription1.Name = 'TEST1234';
            //subscription1.Account__c = subscriptionAccount.Id;
            subscription1.Ext_Quote_Number__c = 'TEST-QUOTE1';
            subscription1.ERP_Project__c = true;
            subscription1.Number_Of_Licences__c = 5;
            subscription1.ERP_Site_Partner__c = 'Sales11111';
            subscription1.Functional_Location__c = 'H-CHELMSFORD -GB-EU-001';
            subscription1.Installation_Status__c = 'Pending';
            subscription1.Licence_Status__c = 'Not Started';
            subscription1.Site_Project_Manager__c = '1234567';
            subscription1.Sales_Operations_Specialist__c = '1234567';
            subscription1.Sales_Manager__c = '1234567';
            subscription1.Contract_Administrator__c = '1234567';
            subscription1.Product_Type__c = 'AN';           
            //subscription1.PCSN__c = 'HAN0263-ARIANOT';
            subscription1.PCSN__c = 'HAN0100-ARIANOT';
            subscriptions.add(subscription1);
            /*
            Subscription__c subscription2 = new Subscription__c();
            subscription2.Name = 'TEST789';
            subscription2.PCSN__c = 'TEST123-ARIANOT';
            subscription2.Functional_Location__c = 'TEST1234';
            subscription2.Number_Of_Licences__c = 3;
            subscription2.Ext_Quote_Number__c = 'TEST-QUOTE2';
            subscription2.ERP_Site_Partner__c = 'Sales11111';
            //subscription2.Account__c = subscriptionAccount.Id;
            subscription2.Installation_Status__c = 'Pending';
            subscription2.Licence_Status__c = 'Not Started';
            subscription2.Product_Type__c = 'AN';
            subscription2.ERP_Project__c = false;
      */
           
        
            //subscriptions.add(subscription2);
            insert subscriptions;
            
            //SaasInstallation.createInstallationPlan(subscriptions,null);
            //TEST Subscription Product Trigger and validate create installation plan result
            List<SVMXC__Installed_Product__c> installedProducts = [
                SELECT Id, Subscription__c, ERP_Reference__c, ERP_EQUIPMENT__c
                FROM SVMXC__Installed_Product__c
                WHERE Subscription__c IN:subscriptions
            ];
            
            List<Subscription_Product__c> subscriptionProducts = new List<Subscription_Product__c>();
            for(SVMXC__Installed_Product__c installedProduct : installedProducts){
                subscriptionProducts.add(
                    new Subscription_Product__c(
                        ERP_Equipment_Nbr__c = installedProduct.ERP_EQUIPMENT__c,
                        Subscription__c = installedProduct.Subscription__c,
                        ERP_Partner_Number__c = 'TESTSITE123'
                    )
                );
            }
            
            insert subscriptionProducts;
        Test.stopTest();
    }
    
    static testMethod void testCreateSubscriptionIWO() {
        
        System.debug('----TEST QUERIES INITIAL 1'+Limits.getQueries());
        
        con = [SELECT Id, AccountId FROM Contact WHERE Account.AccountNumber = 'ZEMI' LIMIT 1]; 
        
       // ERP_Pcode__c pCode = [SELECT Id FROM ERP_Pcode__c WHERE Name = 'HAN' LIMIT 1];
       
        ERP_Pcode__c  pCode = new ERP_Pcode__c ();
        pCode.Name = 'HAN';
        insert pCode;
        System.debug('----ERP_Pcode__c pCode'+pCode);
            
        //Product_System_Subsystem__c pSystem = [SELECT Id FROM Product_System_Subsystem__c WHERE ERP_Pcode_ID__c =:pCode.Id LIMIT 1];
        
        Product_System_Subsystem__c pSystem =  new Product_System_Subsystem__c();
        pSystem.name = 'Test';
        insert pSystem;
        System.debug('----Product_System_Subsystem__c pSystem'+pSystem); 
        
        productModel = [SELECT Id FROM Product2 WHERE ProductCode = 'HANA' LIMIT 1];
        
        System.debug('----TEST QUERIES INITIAL 2'+Limits.getQueries());
        
        List<SVMXC__Service_Group_Members__c> techs = insertTechnicians();
        
        List<Subscription__c> subscriptions = new List<Subscription__c>();
        Subscription__c subscription1 = new Subscription__c();
        subscription1.Name = 'TEST1234';
        subscription1.Ext_Quote_Number__c = 'TEST-QUOTE1';
        subscription1.ERP_Project__c = false;
        subscription1.Number_Of_Licences__c = 5;
        //subscription1.ERP_Site_Partner__c = 'Sales11111';
        subscription1.Functional_Location__c = '';
        subscription1.Installation_Status__c = 'Pending';
        subscription1.Licence_Status__c = 'Not Started';
        subscription1.Site_Project_Manager__c = '1234567';
        subscription1.Sales_Operations_Specialist__c = '1234567';
        subscription1.Sales_Manager__c = '1234567';
        subscription1.Contract_Administrator__c = '1234567';
        subscription1.Product_Type__c = 'AN';
        subscription1.PCSN__c = 'HAN0100-ARIANOT';
        insert subscriptions;
        System.debug('----TEST QUERIES2'+Limits.getQueries());
        
        location = new SVMXC__Site__c(
            Sales_Org__c = 'testorg', 
            SVMXC__Service_Engineer__c = userInfo.getUserId(), 
            ERP_Functional_Location__c = 'TEST1234', 
            SVMXC__Location_Type__c = 'Field', 
            Plant__c = 'dfgh', 
            ERP_Site_Partner_Code__c = 'TESTSITE123',
            SVMXC__Account__c = con.AccountId
        );
        
        location.Plant__c = 'testorg';
        location.Facility_Code__c = 'facilitycode1234';
        location.Country__c = [SELECT Id FROM Country__c WHERE Name = 'USA' LIMIT 1].Id;
        insert location;
        
        TestIP = new SVMXC__Installed_Product__c();
        TestIP.Name = 'HAN0999-ARIANOT';
        TestIP.SVMXC__Site__c = location.id;
        TestIP.ERP_Functional_Location__c = 'USA';
        TestIP.SVMXC__Serial_Lot_Number__c ='test';
        testIP.ERP_Reference__c = 'H12345';
        testIP.SVMXC__Company__c = con.AccountId;
        testIP.SVMXC__Product__c = productModel.id;
        testIP.SVMXC__Preferred_Technician__c = techs[0].id;
        insert TestIP;
        
        SR_Class_case.CheckRecusrsiveCase = true;
        SR_Class_case.CheckRecusrsiveCaseafter = true;
        testCase = SR_testdata.createcase();
        testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('INST').getRecordTypeId();
        testCase.Priority = 'Medium';
        testCase.ProductSystem__c = TestIP.Id;
        testCase.AccountId = con.AccountId;
        testCase.Subscription__c = subscription1.Id;
        testCase.ProductId = productModel.Id;
        insert testCase;
        System.debug('----TEST QUERIES BEFORE TEST START'+Limits.getQueries());
        SVMXC__Service_Order__c serviceOrder = insertSAASIWOTestData(techs);
        System.debug('----TEST QUERIES3'+Limits.getQueries());
        Test.startTest();               
            
            System.debug('----TEST QUERIES4'+Limits.getQueries());
            
            Pagereference iwoPage = Page.CreateIWOPage;
                
            apexpages.currentpage().getparameters().put('id',serviceOrder.id);
            SR_CreateIWO_Controller controller = new SR_CreateIWO_Controller();
            System.debug('----TEST QUERIES5'+Limits.getQueries());
            controller.WorkOrdId = serviceOrder.id;
            controller.searchString = 'acme';
            SR_CreateIWO_Controller.WrapperClass tempw = new SR_CreateIWO_Controller.WrapperClass(techs[0],true,true);
            tempw.evnt.SVMXC__StartDateTime__c = DateTime.now();
            tempw.evnt.SVMXC__EndDateTime__c = DateTime.now().addHours(1);
            //SR_CreateIWO_Controller.WrapperClass tempw1 = new SR_CreateIWO_Controller.WrapperClass(techs[0],true,true);  
            controller.ListWrapTechnician.add(tempw);
            //controller.ListWrapTechnician.add(tempw1);  
            controller.AddTechnicians();   
    
            controller.CreateInstallationWO();
        Test.stopTest();
        System.debug('----TEST QUERIES6'+Limits.getQueries());
    }
    /**
     * Utility method to create test subscription quote
     */
    public static void setUpSubscriptionTestData(){
        
        con = [SELECT Id, AccountId FROM Contact WHERE Account.AccountNumber = 'ZEMI' LIMIT 1];
        Contact_Role_Association__c contactRole = new Contact_Role_Association__c();
        contactRole.Role__c = 'PM-Project Mgr';
        contactRole.Contact__c = con.Id;
        contactRole.Account__c = con.AccountId;
        insert contactRole;
        System.debug('----AFTER CONTACT ROLE'+Limits.getQueries());
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = con.AccountId;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
         // ERP_Pcode__c pCode = [SELECT Id FROM ERP_Pcode__c WHERE Name = 'HAN' LIMIT 1];
       
        ERP_Pcode__c  pCode = new ERP_Pcode__c ();
        pCode.Name = 'HAN';
        insert pCode;
        System.debug('----ERP_Pcode__c pCode'+pCode);
            
        //Product_System_Subsystem__c pSystem = [SELECT Id FROM Product_System_Subsystem__c WHERE ERP_Pcode_ID__c =:pCode.Id LIMIT 1];
        
        Product_System_Subsystem__c pSystem =  new Product_System_Subsystem__c();
        pSystem.name = 'Test';
        insert pSystem;
        System.debug('----Product_System_Subsystem__c pSystem'+pSystem); 
        
        Product2 subscriptionProduct = [SELECT Id FROM Product2 WHERE Subscription_Product_Type__c = 'Subscription' LIMIT 1];
        
        
        Product2 installationProduct = [SELECT Id FROM Product2 WHERE Subscription_Product_Type__c = 'Installation' LIMIT 1];
        
        
        productModel = [SELECT Id FROM Product2 WHERE ProductCode = 'HANA' LIMIT 1];
        
        RecursiveQuoteTriggerController.isSuccessEmailSent = true;
        
        BigMachines__Quote__c subscriptionQuoteNew = TestUtils.getQuote();
        subscriptionQuoteNew.Name = 'TEST-QUOTE1';
        subscriptionQuoteNew.BigMachines__Account__c = con.AccountId;
        subscriptionQuoteNew.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteNew.National_Distributor__c = '121212';
        subscriptionQuoteNew.Order_Type__c = 'sales';
        subscriptionQuoteNew.Price_Group__c = 'Z2';
        subscriptionQuoteNew.Quote_Region__c = 'NA';
        
        BigMachines__Quote__c subscriptionQuoteRenewal = TestUtils.getQuote();
        subscriptionQuoteRenewal.Name = 'TEST-QUOTE2';
        subscriptionQuoteRenewal.BigMachines__Account__c = con.AccountId;
        subscriptionQuoteRenewal.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteRenewal.National_Distributor__c = '121212';
        subscriptionQuoteRenewal.Order_Type__c = 'sales';
        subscriptionQuoteRenewal.Price_Group__c = 'Z2';
        subscriptionQuoteRenewal.Quote_Region__c = 'NA';
        
        insert new List<BigMachines__Quote__c>{subscriptionQuoteNew, subscriptionQuoteRenewal};
        
        AutoCreateBillingPlan.STOP_BILLING_PLAN_CRERATION = true;
        
        insert new List<BigMachines__Quote_Product__c>{
            getQuoteProduct(subscriptionQuoteNew.Id,subscriptionProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteNew.Id,installationProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteRenewal.Id,subscriptionProduct.Id, '3', 'Annually','Renewal')
        };
        
        /*Country__c country = testUtils.createCountry('United States');
        country.Alternate_Name__c = 'US';
        country.Parts_Order_Approval_Level__c  = 10;
        insert country;*/
    }
    
    private static BigMachines__Quote_Product__c getQuoteProduct(Id quoteId,Id subscriptionProductId, String numberOfYears, String billingFrequency, String salesType){
        BigMachines__Quote_Product__c quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.BigMachines__Quote__c = quoteId;
        quoteProduct.BigMachines__Product__c = subscriptionProductId;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = 'Sales';
        quoteProduct.Standard_Price__c = 40000;
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.Line_Number__c= 12;
        quoteProduct.EPOT_Section_Id__c = 'TEST';
        quoteProduct.Subscription_Start_Date__c = Date.today();
        quoteProduct.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
        quoteProduct.Number_Of_Years__c = numberOfYears;
        quoteProduct.Subscription_End_Date__c = Date.today().addYears(Integer.valueOf(numberOfYears));
        quoteProduct.Billing_Frequency__c = billingFrequency;
        quoteProduct.Subscription_Sales_Type__c = salesType;
        quoteProduct.Installation_Price__c = 0.0;
        quoteProduct.Subscription_Unit_Price__c = 2900;
        if(salesType != 'Renewal'){
            quoteProduct.SAP_Contract_Number__c = 'TEST1234';
        }
        return quoteProduct;
    }
    
    private static SVMXC__Service_Order__c insertSAASIWOTestData(List<SVMXC__Service_Group_Members__c> technicians){
        
        Id iwoRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); 
        
        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = '1234zxcv');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        
        Datetime stdt = System.now().addHours(1);
        Datetime eddt = System.now().addHours(2);
        
        SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
        workOrdObject.SVMXC__Case__c = testCase.Id;
        workOrdObject.Event__c = true;
        workOrdObject.SVMXC__Company__c = TestIP.SVMXC__Company__c;
        workOrdObject.ownerID = UserInfo.getUserId();
        workOrdObject.SVMXC__Order_Status__c = 'open';
        workOrdObject.SVMXC__Top_Level__c = TestIP.Id;
        workOrdObject.RecordTypeId = iwoRecordTypeId;
        workOrdObject.SVMXC__Product__c = TestIP.SVMXC__Product__c;
        workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
        workOrdObject.SVMXC__Preferred_End_Time__c = system.now()+1;
        workOrdObject.SVMXC__Preferred_Start_Time__c  = system.now();
        workOrdObject.Subject__c= 'bbb5';
        workOrdObject.Installation_Manager__c = UserInfo.getUserId();
        workOrdObject.Internal_Comment__c = 'ABCD';
        workOrdObject.Service_Team__c = technicians[0].SVMXC__Service_Group__c;
        workOrdObject.SVMXC__Service_Group__c = technicians[0].SVMXC__Service_Group__c;
        workOrdObject.SVMXC__Group_Member__c = technicians[0].id;
        workOrdObject.SVMXC__Preferred_Technician__c = technicians[0].id;
        insert workOrdObject;
        
        SVMXC__Case_Line__c cl1 = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl',SVMXC__Case__c = testCase.id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt ,SVMXC__Location__c = location.id);
        cl1.recordTypeId = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Case Line').getRecordTypeId();
        cl1.SVMXC__Product__c = productModel.Id;
        cl1.SVMXC__Installed_Product__c = TestIP.Id;
        insert cl1;
        return workOrdObject;
    }
    
    private static List<SVMXC__Service_Group_Members__c> insertTechnicians(){
        
        Id userId = UserInfo.getUserId();
        
        RecordType recTech = [Select id,Name,Description from Recordtype where developername = 'Technician' and SObjectType=:'SVMXC__Service_Group_Members__c'];
        Recordtype recServiceTeam = [Select id,Name,Description from Recordtype where developername = 'Technician' and SObjectType=:'SVMXC__Service_Group__c'];
        
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c();
        serviceTeam.SVMXC__Active__c= true;
        serviceTeam.ERP_Reference__c='TestExternal';
        serviceTeam.SVMXC__Group_Code__c  = 'TestExternal';
        serviceTeam.Name = 'TestService';
        serviceTeam.recordtypeId = recServiceTeam.id;
        insert serviceTeam;
        
        SVMXC__Dispatcher_Access__c testDA = new SVMXC__Dispatcher_Access__c();
        testDA.SVMXC__Dispatcher__c =  userId;
        testDA.SVMXC__Service_Team__c = ServiceTeam.id ;
        insert testDA;
        
        List<SVMXC__Service_Group_Members__c> techs = new List<SVMXC__Service_Group_Members__c>();
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.SVMXC__Service_Group__c = serviceTeam.id;
        tech.User__c = userId;
        tech.ERP_Reference__c = 'TextExternal';
        tech.Name = 'TestTechnician';
        tech.SVMXC__Average_Drive_Time__c = 2.00;
        tech.SVMXC__Average_Speed__c = 2.00;
        tech.SVMXC__Break_Duration__c = 2.00;
        tech.SVMXC__Break_Type__c = 'Fixed';
        tech.SVMXC__City__c = 'Noida';
        tech.SVMXC__Country__c = 'India'; 
        tech.Current_End_Date__c = system.today();
        tech.Dispatch_Queue__c = 'DISP - AMS';
        //tech.Docusign_Recipient__c = 'DISP - AMS';
        tech.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech.SVMXC__State__c = 'UP';
        tech.SVMXC__Street__c = 'abc';
        tech.SVMXC__Zip__c = '201301';
        tech.recordtypeId = recTech.id;
        tech.SVMXC__Salesforce_User__c = userId;
        tech.SVMXC__Select__c = false;
        techs.add(tech);
        
        SVMXC__Service_Group_Members__c tech1 = new SVMXC__Service_Group_Members__c();
        tech1.SVMXC__Service_Group__c = serviceTeam.id;
        tech1.User__c = userId;
        tech1.ERP_Reference__c = 'TextExternal';
        tech1.Name = 'TestTechnician';
        tech1.SVMXC__Average_Drive_Time__c = 2.00;
        tech1.SVMXC__Average_Speed__c = 2.00;
        tech1.SVMXC__Break_Duration__c = 2.00;
        tech1.SVMXC__Break_Type__c = 'Fixed';
        tech1.SVMXC__City__c = 'Noida';
        tech1.SVMXC__Country__c = 'India'; 
        tech1.Current_End_Date__c = system.today();
        tech1.Dispatch_Queue__c = 'DISP - AMS';
        //tech1.Docusign_Recipient__c = 'DISP - AMS';
        tech1.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech1.SVMXC__State__c = 'UP';
        tech1.SVMXC__Street__c = 'abc';
        tech1.SVMXC__Zip__c = '201301';
        tech1.recordtypeId = recTech.id;
        tech1.SVMXC__Salesforce_User__c = userId;
        tech1.SVMXC__Select__c = false;
        techs.add(tech1);
        
        SVMXC__Service_Group_Members__c tech2 = new SVMXC__Service_Group_Members__c();
        tech2.SVMXC__Service_Group__c = serviceTeam.id;
        tech2.User__c = userId;
        tech2.ERP_Reference__c = 'TextExternal';
        tech2.Name = 'TestTechnician';
        tech2.SVMXC__Average_Drive_Time__c = 2.00;
        tech2.SVMXC__Average_Speed__c = 2.00;
        tech2.SVMXC__Break_Duration__c = 2.00;
        tech2.SVMXC__Break_Type__c = 'Fixed';
        tech2.SVMXC__City__c = 'Noida';
        tech2.SVMXC__Country__c = 'India'; 
        tech2.Current_End_Date__c = system.today();
        tech2.Dispatch_Queue__c = 'DISP - AMS';
        //tech2.Docusign_Recipient__c = 'DISP - AMS';
        tech2.SVMXC__Email__c = 'standarduser@testorg.com';
        //SVMXC__Inventory_Location__c
        tech2.SVMXC__State__c = 'UP';
        tech2.SVMXC__Street__c = 'abc';
        tech2.SVMXC__Zip__c = '201301';
        tech2.recordtypeId = recTech.id;
        tech2.SVMXC__Salesforce_User__c =  userId;
        tech2.SVMXC__Select__c = false;        
        techs.add(tech2);
        insert techs;
        return techs;
    }
    
    
   @isTest private static void testcreateWorkDetailRecords()
    {
        System.test.startTest();
        //test Data Generation -- Start
        List<SVMXC__Service_Group_Members__c> techs = insertTechnicians();
        Product2 productModel = [SELECT Id FROM Product2 WHERE ProductCode = 'HANA' LIMIT 1];
        Contact con = [SELECT Id, AccountId FROM Contact WHERE Account.AccountNumber = 'ZEMI' LIMIT 1];
        Contact_Role_Association__c contactRole = new Contact_Role_Association__c();
        contactRole.Role__c = 'PM-Project Mgr';
        contactRole.Contact__c = con.Id;
        contactRole.Account__c = con.AccountId;
        insert contactRole;
        System.debug('----AFTER CONTACT ROLE'+Limits.getQueries());
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = con.AccountId;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        Id iwoRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId(); 
        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = '1234zxcv');
        insert erpwbs;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Std_Text_Key__c='INST001', WBS_Element__c = erpwbs.id, ERP_Project_Nbr__c='fefew223');
        insert erpnwa;
        
        Datetime stdt = System.now().addHours(1);
        Datetime eddt = System.now().addHours(2);
        
          SVMXC__Site__c   location = new SVMXC__Site__c(
            Sales_Org__c = 'testorg', 
            SVMXC__Service_Engineer__c = userInfo.getUserId(), 
            ERP_Functional_Location__c = 'TEST1234', 
            SVMXC__Location_Type__c = 'Field', 
            Plant__c = 'dfgh', 
            ERP_Site_Partner_Code__c = 'TESTSITE123',
            SVMXC__Account__c = con.AccountId
        );
        
        location.Plant__c = 'testorg';
        location.Facility_Code__c = 'facilitycode1234';
        location.Country__c = [SELECT Id FROM Country__c WHERE Name = 'USA' LIMIT 1].Id;
        insert location;
        
       SVMXC__Installed_Product__c  TestIP = new SVMXC__Installed_Product__c();
        TestIP.Name = 'HAN0999-ARIANOT';
        TestIP.SVMXC__Site__c = location.id;
        TestIP.ERP_Functional_Location__c = 'USA';
        TestIP.SVMXC__Serial_Lot_Number__c ='test';
        testIP.ERP_Reference__c = 'H12345';
        testIP.SVMXC__Company__c = con.AccountId;
        testIP.SVMXC__Product__c = productModel.id;
        testIP.SVMXC__Preferred_Technician__c = techs[0].id;
        insert TestIP;
        
        Case testCase = SR_testdata.createcase();
        testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('INST').getRecordTypeId();
        testCase.Priority = 'Medium';
        testCase.ProductSystem__c = TestIP.Id;
        testCase.AccountId = con.AccountId;
        testCase.ProductId = productModel.Id;
        insert testCase;
        
        SVMXC__Service_Order__c workOrdObject =  SR_testdata.createServiceOrder();
        workOrdObject.SVMXC__Case__c = testCase.Id;
        workOrdObject.Event__c = true;
        workOrdObject.SVMXC__Company__c = TestIP.SVMXC__Company__c;
        workOrdObject.ownerID = UserInfo.getUserId();
        workOrdObject.SVMXC__Order_Status__c = 'open';
        workOrdObject.SVMXC__Top_Level__c = TestIP.Id;
        workOrdObject.RecordTypeId = iwoRecordTypeId;
        workOrdObject.SVMXC__Product__c = TestIP.SVMXC__Product__c;
        workOrdObject.SVMXC__Problem_Description__c = 'Test Description';
        workOrdObject.SVMXC__Preferred_End_Time__c = system.now()+1;
        workOrdObject.SVMXC__Preferred_Start_Time__c  = system.now();
        workOrdObject.Subject__c= 'bbb5';
        workOrdObject.Installation_Manager__c = UserInfo.getUserId();
        workOrdObject.Internal_Comment__c = 'ABCD';
        workOrdObject.Service_Team__c = techs[0].SVMXC__Service_Group__c;
        workOrdObject.SVMXC__Service_Group__c = techs[0].SVMXC__Service_Group__c;
        workOrdObject.SVMXC__Group_Member__c = techs[0].id;
        workOrdObject.SVMXC__Preferred_Technician__c = techs[0].id;
        insert workOrdObject;
        
        SVMXC__Case_Line__c cl1 = new SVMXC__Case_Line__c(ERP_Service_order_nbr__c = 'sfl',SVMXC__Case__c = testCase.id, ERP_NWA__c = erpnwa.id, Work_Order__c = workOrdObject.id, Start_Date_time__c = stdt,End_date_time__c = eddt ,SVMXC__Location__c = location.id);
        cl1.recordTypeId = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Case Line').getRecordTypeId();
        cl1.SVMXC__Product__c = productModel.Id;
        cl1.SVMXC__Installed_Product__c = TestIP.Id;
        insert cl1;
              
        List<SVMXC__Service_Order_Line__c> InsertObjWorkDetailList=new  List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order__c> ListInstallationWOInserted=new  List<SVMXC__Service_Order__c>();
        ListInstallationWOInserted.add(workOrdObject);
                
        Map<id,SVMXC__Service_Order__c> mapInstallationWOUpdate=new Map<id,SVMXC__Service_Order__c>();
        mapInstallationWOUpdate.put(workOrdObject.id, workOrdObject);
       
        Map <Id,SVMXC__Installed_Product__c> mapIP=new  Map <Id,SVMXC__Installed_Product__c>();
        mapIP.put(TestIP.id,TestIP);
        
        try{
        SaasInstallation.createWorkDetailRecords(InsertObjWorkDetailList,ListInstallationWOInserted,workOrdObject,cl1,iwoRecordTypeId,mapInstallationWOUpdate,mapIP);
        }
        Catch(Exception e)
        {
            
        }
        System.test.stopTest();
    }
}
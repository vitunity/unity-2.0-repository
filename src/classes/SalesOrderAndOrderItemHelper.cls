/**
  * @author jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Holds helper methods for Sales Order and Order Items in triggers
  * 
  * TEST CLASS 
  * 
  *    SalesOrderAndOrderItemHelper_TEST
  *  
  * ENTRY POINTS 
  *  
  *    Called from SR_SalesOrderItem_AfterTrigger and SR_SalesOrder_AfterTrigger  
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  jwagner/Forefront; Initial Build
  * 
  **/
public with sharing class SalesOrderAndOrderItemHelper {

  //savon/FF 9-2-2015 DE5674
  //@params: Set of Sales Order Item ID's (if the ERP Reject Reason is updated to 'V2-Clerical Cancellation')
  /*
  Function: 
    Change the statuses of all the Caselines that have a reference to these sales order items to 'Cancelled'
    Change the statuses of all related WO's to 'Cancelled'
    Change the statuses of related Salesforce Events and ServiceMax Events to 'Cancelled'
  */ 


  public static void cancelRelatedItems(Set<Id> salesOrderLineItemIds) {
    
    Id prodServicedId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Products Serviced').getRecordTypeId();   
    List<SVMXC__Case_Line__c> caseLines = [SELECT Id, SVMXC__Line_Status__c, Work_Order__c FROM SVMXC__Case_Line__c WHERE Sales_Order_Item__c IN :salesOrderLineItemIds];
    
    Set<Id> workOrderIds = new Set<Id>();

    for (SVMXC__Case_Line__c cl : caseLines) {
      cl.SVMXC__Line_Status__c = 'Canceled';
      workOrderIds.add(cl.Work_Order__c);
    }

    if (!caseLines.isEmpty()) {
      update caseLines;
    }

    /*List<SVMXC__Service_Order__c> workOrders = [SELECT Id, SVMXC__Order_Status__c, (SELECT Id FROM Case_Lines__r WHERE SVMXC__Line_Status__c != 'Canceled') FROM SVMXC__Service_Order__c WHERE Id IN :workOrderIds OR Parent_WO__c IN :workOrderIds];
    List<SVMXC__Service_Order__c> workOrdersToUpdate = new List<SVMXC__Service_Order__c>();

    for (SVMXC__Service_Order__c wo : workOrders) {
        if (wo.Case_Lines__r.isEmpty()) {
        wo.SVMXC__Order_Status__c = 'Cancelled';
        wo.Cancel__c = true; //cdelfattore@forefront 10/14/2014 (mm/dd/yyyy) - US5150
            workOrdersToUpdate.add(wo);
            if (!workOrderIds.contains(wo.Id)) {
              workOrderIds.add(wo.Id);
          }
        } else {
            workOrderIds.remove(wo.Id);
        }
    } 

    if (!workOrdersToUpdate.isEmpty()) {
      update workOrdersToUpdate;
    }

    List<SVMXC__SVMX_Event__c> svmxEvents = new List<SVMXC__SVMX_Event__c>();
    List<Event> salesforceEvents = new List<Event>();
      
    if (!workOrderIds.isEmpty()) {
      svmxEvents = [SELECT Id, Status__c FROM SVMXC__SVMX_Event__c WHERE SVMXC__Service_Order__c IN :workOrderIds];
        salesforceEvents = [SELECT Id, Status__c FROM Event WHERE whatid IN :workOrderIds];
    }  

    for (SVMXC__SVMX_Event__c se : svmxEvents) {
      se.Status__c = 'Cancelled';
    }

    if (!svmxEvents.isEmpty()) {
      update svmxEvents;
    } 

    for (Event e : salesforceEvents) {
      e.Status__c = 'Cancelled';
    }

    if (!salesforceEvents.isEmpty()) {
      update salesforceEvents;
    }*/

    //DK 12 8 15 DE6875
    //query for work detail's that are recordtype 'Product Serviced' 
    //&& Sales Order Item references the Sales Order Item with the 'V2-Clerical Cancellation' 
    System.Debug('***CM salesOrderLineItemIds: '+salesOrderLineItemIds);
    System.Debug('***CM prodServicedId : '+prodServicedId);
    List<SVMXC__Service_Order_Line__c> cancelledWDs = [SELECT id, RecordTypeId, Sales_Order_Item__c, Cancel__c FROM SVMXC__Service_Order_Line__c WHERE Sales_Order_Item__c IN: salesOrderLineItemIds AND RecordTypeId =: prodServicedId];

    //set that work detail's Cancel to 'true'.
    if(!cancelledWDs.isEmpty())
    {
      for(SVMXC__Service_Order_Line__c wds: cancelledWDs)
      {
        wds.cancel__c = True;
      }
    }
    update cancelledWDs;
  }
    
  //savon/FF 10-13-2015 DE6287 US5121
  //@params: MAP with the cancelled SOI name and the affected IWO list
  /*
  Function: 
    Send an email to the appropriate FOA that the SOI has been cancelled even though IWO's exist  
  */
  public static void sendEmailToFOA(Map<String, List<SVMXC__Service_Order__c>> soiToIWOMap) {

     String host = System.URL.getSalesforceBaseUrl().getHost();
     
      for (String s : soiToIWOMap.keySet()) {
     
        String subject = 'Sales Order Item ' + s + ' has been cancelled with IWOs existing ';
      String email = '';
        String body = 'Sales Order Item ' + s + ' has been cancelled, please review the following Work Orders: </br>';
         
      List<SVMXC__Service_Order__c> values = soiTOIWOMap.get(s);
        for (SVMXC__Service_Order__c wo : values) {
             if (wo.Service_Super_Region__c != null) {
                 if (!subject.contains('APAC') || !subject.contains('AMS') || !subject.contains('EMEA')) {
                     if (wo.Service_Super_Region__c == 'APAC') {
                        subject += 'APAC';
                        //email = 'jschmitt@forefrontcorp.com';
                     	email = System.Label.APAC;
                     } else if (wo.Service_Super_Region__c == 'AMS') {
                        subject += 'AMS';
                     	//email = 'jschmitt@forefrontcorp.com';
                     	email = System.Label.AMS;
                     } else {
                     	subject += 'EMEA';
                        //email = 'jschmitt@forefrontcorp.com';
                     	email = System.Label.EMEA;
                     }                     
                 }
             } 
            if (wo.Event__c == false) {
              body += 'Work Order ' + wo.Name +'. To view the Work Order <a href=https://' + host +  '/'+wo.Id+'>click here.</a></br>';  
            }
        }
         
        List<String> toAddresses = new List<String>();
        toAddresses.add(email);
          
          if (email != '') {
             Messaging.SingleEmailMessage sendMessage = SendEmailHelperClass.sendEmail(subject, body, toAddresses, new List<String>(), new List<String>(), true); 
          } 
 
     }  
  }      

  //JW/FF 6-22-15 Var-36 US5121 TA6797
  //@params: One or the other: set of sales order line item ids OR set of sales order Ids (other should be null)
  //function: When there is no Counter Detail:
  //          1. Cancel all product service work detail lines (update status to cancelled)
  //          2. check if there are any usage work detail that haven't been processed to SAP (erp status == null or failed). 
  //             if yes, send end the Work Order's Service Team's FOA, SC, and District Manager an email to the Work Order (not each work detail line cancelled).
  public static void cancelledSalesOrderOrLine(Set<Id> salesOrderLineItemIds, Set<Id> salesOrderIds){  

    Id prodServicedId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Products Serviced').getRecordTypeId(); 
    Id usageConsumptionId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Usage/Consumption').getRecordTypeId();

    List<SVMXC__Service_Order_Line__c> cancelledWDList;

    Set<Id> woIdSet = new Set<Id>();

    //query for all sales orders or lines that are in the passed in set of ids
    //remove any ids that have related counter details
    //query for work details related to the sales order or lines left in the id set
    if(salesOrderLineItemIds != null && !salesOrderLineItemIds.isEmpty())
    {
      List<Sales_Order_Item__c> cancelledSOIList = [Select Id, (Select Id From Counter_Details__r) From Sales_Order_Item__c Where Reject_Reason__c = 'V2-Clerical Cancellation' And Id IN: salesOrderLineItemIds];

      for(Sales_Order_Item__c soi: cancelledSOIList)
      {
        if(soi.Counter_Details__r.isEmpty()) //!soi.Counter_Details__r.isEmpty() - changed CM: DE7905
        {
          salesOrderLineItemIds.remove(soi.Id);
        }
      }
    //savon.FF 10-13-15 Removed RecordTypeId =: prodServicedId Or From Where clause per DE6306 requirements change US5121
      cancelledWDList = [Select Id, RecordTypeId, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Group_Member__c
                        From SVMXC__Service_Order_Line__c 
                        Where SVMXC__Service_Order__r.SVMXC__Order_Status__c != 'Closed'  // CM : DE7905 : Removed RecordTypeId =: usageConsumptionId And 
                              And Sales_Order_Item__r.Reject_Reason__c = 'V2-Clerical Cancellation' 
                              And Sales_Order_Item__c IN: salesOrderLineItemIds];
    }
    else if(salesOrderIds != null && !salesOrderIds.isEmpty())
    {
      List<Sales_Order__c> cancelledSOList = [Select Id, (Select Id From Counter_Details__r) From Sales_Order__c Where Block__c = '70-Order Cancellation' And Id IN: salesOrderIds];

      for(Sales_Order__c so: cancelledSOList)
      {
        if(so.Counter_Details__r.isEmpty()) //!so.Counter_Details__r.isEmpty() - Changed CM : DE7905
        {
          salesOrderIds.remove(so.Id);
        }
      }
    //savon.FF 10-13-15 Removed RecordTypeId =: prodServicedId Or From Where clause per DE6306 requirements change US5121
      cancelledWDList = [Select Id, RecordTypeId, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Group_Member__c
                        From SVMXC__Service_Order_Line__c 
                        Where SVMXC__Service_Order__r.SVMXC__Order_Status__c != 'Closed' // CM : DE7905 : Removed RecordTypeId =: usageConsumptionId And 
                              And Sales_Order_Item__r.Sales_Order__r.Block__c = '70-Order Cancellation'
                              And Sales_Order_Item__r.Sales_Order__c IN: salesOrderIds];
    }

    //cancel all related products serviced work details
    //send email to FOA, SC, and Distrcit manager if any unprocessed usage consumption lines exist
    if(cancelledWDList != null && !cancelledWDList.isEmpty())
    {
      for(SVMXC__Service_Order_Line__c wd: cancelledWDList)
      {
        if(wd.RecordTypeId == prodServicedId)
        {
          wd.Cancel__c = true;
        }
        if(wd.RecordTypeId == usageConsumptionId)
        {
          if(!woIdSet.contains(wd.SVMXC__Service_Order__c) && wd.SVMXC__Service_Order__r.SVMXC__Group_Member__c != null)
          {
            woIdSet.add(wd.SVMXC__Service_Order__c);
          }
        }
      }

      if(!woIdSet.isEmpty())
      {
         List<Messaging.SingleEmailMessage> mailList = generateEmailsForWO(woIdSet);

        if(!mailList.isEmpty())
        {
          Messaging.sendEmail(mailList, false);
        }
      }
    }
  }

  //JW/FF 6-23-15 Var-36 US5121 TA6797
  //@params: list of work order ids that should have notification emails sent
  //Fucntion: refer to #2 of cancelledSalesOrderOrLine method function section
  @TestVisible
  private static List<Messaging.SingleEmailMessage> generateEmailsForWO(Set<Id> woIdSet){  

    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();

    List<SVMXC__Service_Order__c> woList = [Select  Id, Name, SVMXC__Group_Member__r.SVMXC__Service_Group__r.FOA__r.Email, 
                                                    SVMXC__Group_Member__r.SVMXC__Service_Group__r.District_Manager__r.Email, 
                                                    SVMXC__Group_Member__r.SVMXC__Service_Group__r.SC__r.Email,
                                                    SVMXC__Company__r.Country1__r.Service_Super_Region__c
                                            From    SVMXC__Service_Order__c
                                            Where   Id IN: woIdSet];
                                            
    Map<String, SuperRegionToPMAssignmentEmail__c> superRegionEmailMap = SuperRegionToPMAssignmentEmail__c.getAll();

    for(SVMXC__Service_Order__c wo: woList)
    {
      Set<String> toAddressSet = new Set<String>();
      List<String> ccAddressSet = new List<String>();

      if(ObjectHelperClass.isNotBlank(String.valueOf(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.FOA__r.Email)))
      {
        toAddressSet.add(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.FOA__r.Email);
      }
      if(ObjectHelperClass.isNotBlank(String.valueOf(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.District_Manager__r.Email)))
      {
        toAddressSet.add(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.District_Manager__r.Email);
      }
      if(ObjectHelperClass.isNotBlank(String.valueOf(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.SC__r.Email)))
      {
        toAddressSet.add(wo.SVMXC__Group_Member__r.SVMXC__Service_Group__r.SC__r.Email);
      }
      if(wo.SVMXC__Company__r.Country1__r.Service_Super_Region__c != null && superRegionEmailMap.containsKey(wo.SVMXC__Company__r.Country1__r.Service_Super_Region__c))
      {
        ccAddressSet.add(superRegionEmailMap.get(wo.SVMXC__Company__r.Country1__r.Service_Super_Region__c).PM_Assignment_Email__c);
      }

      List<String> toAddresses = new List<String>();
      toAddresses.addAll(toAddressSet);

      String subject = 'Work Order ' + wo.Name + ' : Usage Consumption detail lines have not been processed';
      String host = System.URL.getSalesforceBaseUrl().getHost();
      String body = 'Work Order <b> ' + wo.Name +' </b>has Usage Consumption lines that still require processing.<p>'+ 'To view the Work Order <a href=https://' + host +  '/'+wo.Id+'>click here.</a>';

      mailList.add(SendEmailHelperClass.sendEmail(subject, body, toAddresses, ccAddressSet, new List<String>(), false));

    }

    return mailList;
  }

    /**
    * DE6204
    * Deletes related Installed Products when ERP Reject Reason on Sales Order Item has been changed to 'V2-Clerical Cancellation'
    * and Installed Products Status equals 'Ordered'. Trigger helper method.
    * @param  soItemOldMap Map<Id,Sales_Order_Item__c> map of Sales Order Items with old values
    * @param  soItemNewMap Map<Id,Sales_Order_Item__c> map of Sales Order Items with new values
    */
    public static void deleteOrderedInstalledProducts(Map<Id,Sales_Order_Item__c> soItemOldMap, Map<Id,Sales_Order_Item__c> soItemNewMap) {
        if (soItemOldMap != null && soItemNewMap != null) {
            Set<Id> changedSoItems = new Set<Id>();
            for (Id soiId : soItemNewMap.keySet()) {
                Sales_Order_Item__c oldValue = soItemOldMap.get(soiId);
                Sales_Order_Item__c newValue = soItemNewMap.get(soiId);
                if (oldValue.Reject_Reason__c != newValue.Reject_Reason__c && 'V2-Clerical Cancellation'.equalsIgnoreCase(newValue.Reject_Reason__c)) {
                    changedSoItems.add(soiId);
                }
            }
            
            if (!changedSoItems.isEmpty()) {
                List<SVMXC__Installed_Product__c> instProdToDelete = [SELECT Id FROM SVMXC__Installed_Product__c WHERE Sales_OrderItem__c = :changedSoItems AND SVMXC__Status__c = 'Ordered'];
                
                delete instProdToDelete;
            }
        }
    }
}
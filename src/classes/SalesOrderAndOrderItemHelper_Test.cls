@isTest
private class SalesOrderAndOrderItemHelper_Test {
    
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static SVMXC__Case_Line__c caseLine;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static Time_Entry_Matrix__c matrix;
    public static Time_Entry_Matrix__c matrix1;
    public static List<Time_Entry_Matrix__c> matrixList;
    public static Organizational_Activities__c OrgActivity1;
    public static Organizational_Activities__c OrgActivity2;
    public static List<Organizational_Activities__c> OrgActivityList;
    
    static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        
        newAcc = UnityDataTestClass.AccountData(true);
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        
        newProd = UnityDataTestClass.product_Data(true);
        
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        caseLine = UnityDataTestClass.CaseLine_Data(false, newCase, newProd);
        caseLine.Sales_Order_Item__c = SOI.Id;
        insert caseLine;
        
        WO = UnityDataTestClass.WO_Data(true, newAcc, newCase, newProd, SOI, technician);
        
        WD = UnityDataTestClass.WD_Data(true, WO, technician, newLoc, SOI, NWA);
        
    }
    
    public static testmethod void cancelRelatedItems_test(){
        Test.StartTest();
        
        SalesOrderAndOrderItemHelper.cancelRelatedItems(new set<Id> {SOI.id});
        
        Test.Stoptest();
    } 
    
    public static testMethod void cancelledSalesOrderOrLine_Test() {
        Test.StartTest();
        
        SOI.Reject_Reason__c = 'V2-Clerical Cancellation';
        update SOI;
        
        
        SalesOrderAndOrderItemHelper.cancelledSalesOrderOrLine(new set<Id> {SOI.id}, new set<Id> {SO.id});
        Test.StopTest();
    }
    
    public static testMethod void cancelledSalesOrderOrLine_Test2() {
        Test.StartTest();
        
        SO.Block__c = '70-Order Cancellation';
        update SO;
        
        WD = [SELECT Id, RecordTypeId, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Group_Member__c,
                    SVMXC__Service_Order__r.SVMXC__Order_Status__c, Sales_Order_Item__r.Sales_Order__r.Block__c,
                    Sales_Order_Item__r.Sales_Order__c
              FROM SVMXC__Service_Order_Line__c WHERE Id =: WD.Id];
        
        SalesOrderAndOrderItemHelper.cancelledSalesOrderOrLine(new set<Id>(), new set<Id> {SO.id});
        Test.StopTest();
    }
    
    public static testMethod void sendEmailToFOA_REGION_APAC_Test() {
        Test.StartTest();
        
        SuperRegionToPMAssignmentEmail__c region = new SuperRegionToPMAssignmentEmail__c();
        region.Name = 'APAC';
        region.PM_Assignment_Email__c = 'Test@Email.com';
        region.SuperRegion__c = 'APAC';
        insert region;
        
        WO.Event__c = false;
        WO.Service_Super_Region__c = 'APAC';
        update WO;
        
        List<SVMXC__Service_Order__c> WOLIST = new List<SVMXC__Service_Order__c>();
        WOLIST.add(WO); 
        Map<String, List<SVMXC__Service_Order__c>> soiToIWOMap = new Map<String, List<SVMXC__Service_Order__c>>();
        soiToIWOMap.put(WO.Id, WOLIST);
        
        SalesOrderAndOrderItemHelper.sendEmailToFOA(soiToIWOMap);
        
        Test.StopTest();
    }
    
    public static testMethod void sendEmailToFOA_REGION_AMS_Test() {
        Test.StartTest();
        
        SuperRegionToPMAssignmentEmail__c region = new SuperRegionToPMAssignmentEmail__c();
        region.Name = 'AMS';
        region.PM_Assignment_Email__c = 'Test@Email.com';
        region.SuperRegion__c = 'AMS';
        insert region;
        
        WO.Event__c = false;
        WO.Service_Super_Region__c = 'AMS';
        update WO;
        
        List<SVMXC__Service_Order__c> WOLIST = new List<SVMXC__Service_Order__c>();
        WOLIST.add(WO); 
        Map<String, List<SVMXC__Service_Order__c>> soiToIWOMap = new Map<String, List<SVMXC__Service_Order__c>>();
        soiToIWOMap.put(WO.Id, WOLIST);
        
        SalesOrderAndOrderItemHelper.sendEmailToFOA(soiToIWOMap);
        
        Test.StopTest();
    }
    
    public static testMethod void sendEmailToFOA_REGION_EMEA_Test() {
        Test.StartTest();
        
        SuperRegionToPMAssignmentEmail__c region = new SuperRegionToPMAssignmentEmail__c();
        region.Name = 'EMEA';
        region.PM_Assignment_Email__c = 'Test@Email.com';
        region.SuperRegion__c = 'EMEA';
        insert region;
        
        WO.Event__c = false;
        WO.Service_Super_Region__c = 'EMEA';
        update WO;
        
        List<SVMXC__Service_Order__c> WOLIST = new List<SVMXC__Service_Order__c>();
        WOLIST.add(WO); 
        Map<String, List<SVMXC__Service_Order__c>> soiToIWOMap = new Map<String, List<SVMXC__Service_Order__c>>();
        soiToIWOMap.put(WO.Id, WOLIST);
        
        SalesOrderAndOrderItemHelper.sendEmailToFOA(soiToIWOMap);
        
        Test.StopTest();
    }
    
    public static testMethod void generateEmailsForWO_Test() {
        Test.StartTest();
        
        Set<Id> woId = new Set<Id>();
        woId.add(WO.Id);
        
        SalesOrderAndOrderItemHelper.generateEmailsForWO(woId);
        
        Test.StopTest();
    }

}
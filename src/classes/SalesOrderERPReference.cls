public class SalesOrderERPReference{
    public static void updateSalesOrderNumber(List<Part_Delivery__c> lstPartDelivery,map<Id,Part_Delivery__c> oldmap){
        map<Id,string> mapSalesOrderNumberLine = new map<Id,string>();
        for(Part_Delivery__c part : lstPartDelivery){
            if(oldmap == null || (oldmap <> null && (oldmap.get(part.Id).ERP_Reference__c <> part.ERP_Reference__c || oldmap.get(part.Id).ERP_Delivery_Type__c <> part.ERP_Delivery_Type__c || oldmap.get(part.Id).ERP_Item_Category__c <> part.ERP_Item_Category__c)))
            {
                if(part.Parts_Order_Line__c <> null && part.ERP_Reference__c <> null && part.ERP_Reference__c.contains('#') && part.ERP_Delivery_Type__c == 'ZHCT' && part.ERP_Item_Category__c == 'ZKBI'){
                    mapSalesOrderNumberLine.put(part.Parts_Order_Line__c,(part.ERP_Reference__c).split('#')[0]);
                }
            }
        }
        if(mapSalesOrderNumberLine.size()>0){
            List<SVMXC__RMA_Shipment_Line__c> lstLine = new list<SVMXC__RMA_Shipment_Line__c>();
            map<Id,string> mapSalesOrderNumberOrder = new map<Id,string>();
            for(SVMXC__RMA_Shipment_Line__c line: [select id,SVMXC__RMA_Shipment_Order__c,SVMXC__Sales_Order_Number__c from SVMXC__RMA_Shipment_Line__c where id =: mapSalesOrderNumberLine.keyset()]){
                line.SVMXC__Sales_Order_Number__c = mapSalesOrderNumberLine.get(line.id);
                mapSalesOrderNumberOrder.put(line.SVMXC__RMA_Shipment_Order__c,line.SVMXC__Sales_Order_Number__c);
                
                lstLine.add(line);
            }
            update lstLine;

            List<SVMXC__RMA_Shipment_Order__c> lstOrder = new list<SVMXC__RMA_Shipment_Order__c>();
            for(SVMXC__RMA_Shipment_Order__c order: [select id,SVMXC__Sales_Order_Number__c from SVMXC__RMA_Shipment_Order__c where id =: mapSalesOrderNumberOrder.keyset() and SVMXC__Order_Type__c = 'Replacement']){
                order.SVMXC__Sales_Order_Number__c = mapSalesOrderNumberOrder.get(order.id);
                lstOrder.add(order);
            }
            update lstOrder;
        }
    }
}
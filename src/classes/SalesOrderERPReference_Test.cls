@isTest
public class SalesOrderERPReference_Test
{
    @isTest(seeAllData=true)
    static void SalesOrderERPReference() 
    {
        Profile p = [Select id from profile where name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='test18th_1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test18th_1@testorg.com');  
        insert u;
        
        
        Country__c con = new Country__c(Name = 'United States');
        insert con;
    
        Account acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id); 
        insert acc;
        
        Product2 product = new Product2(Name='Connect Utilities', ProductCode = '1234567892');
        insert product;
        
        SVMXC__Site__c location = new SVMXC__Site__c(Sales_Org__c = 'testorg', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = acc.id, SVMXC__Stocking_Location__c = true);
        location.Plant__c = 'testorg';
        Location.Facility_Code__c = 'facilitycode1234';
        insert location;
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test',SVMXC__Product__c=product.id,ERP_Reference__c = '123456',ERP_Sales_Order_Item__c = '9876153',SVMXC__Site__c = location.Id);
        insert ip;
        
        Case casealias = new Case(Subject = 'testsubject');
        insert casealias;
        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(SVMXC__Site__c = location.id,SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id,ERP_Service_Order__c = '12345678');
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        insert wo;
        
        SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Consumed_From_Location__c = location.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Part_Disposition__c = 'Installing';
        wd.Completed__c=false;
        wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.Actual_Hours__c = 5;
        wd.SVMXC__Service_Order__c = wo.id;
        wd.NWA_Description__c = 'nwatest';
        wd.Removal_Condition__c = 'bad';
        wd.Part_Disposition__c = 'Removed';
        wd.SVMXC__Serial_Number__c = ip.id;
        wd.SVMXC__Start_Date_and_Time__c = System.now();
        wd.SVMXC__End_Date_and_Time__c = System.now().adddays(7);
        insert wd;
    
        
        test.startTest();
            SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(
            RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId(),
            SVMXC__Company__c = acc.id, 
             
            SVMXC__Source_Location__c = location.Id,
            SVMXC__Destination_Location__c = location.Id,
            Inventory_Type__c = 'Customer',
            SVMXC__Service_Order__c = wo.Id);   
            
            insert po;
            
            
            SVMXC__RMA_Shipment_Line__c pol = new SVMXC__RMA_Shipment_Line__c(
                RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
                SVMXC__RMA_Shipment_Order__c = po.Id,
                SVMXC__Actual_Quantity2__c = 23,
                SVMXC__Serial_Number__c = ip.Id,
                SVMXC__Expected_Quantity2__c = 5,
                Interface_Status__c = 'Do Not Process',
                SVMXC__Line_Status__c = 'Complete',
                SVMXC__Line_Type__c = 'Inbound (RMA)',
                Consumed__c = false,
                Returned__c = false,
                SVMXC__Ship_Location__c = location.Id,
                SVMXC__Service_Order_Line__c = wd.Id,
                ERP_Serial_Number__c = '8521469',
                SVMXC__Product__c = product.Id
            );
            
            insert pol;
            
            Sales_Order_Item__c soi = new Sales_Order_Item__c(Installed_Product__c = ip.Id, Location__c = location.Id);
            Sales_Order_Item__c soi2 = new Sales_Order_Item__c(Parent_Item__c = soi.Id, ERP_Reference__c = '9876153');
            insert new list<Sales_Order_Item__c> {soi2,soi};
        
            SVMXC__RMA_Shipment_Line__c polFromQuery = [Select Id, Name from SVMXC__RMA_Shipment_Line__c where Id =: pol.Id];
            
            
            Part_Delivery__c pd = new Part_Delivery__c(
            Name = 'TestPD',
            ERP_Parts_Line_Number__c = polFromQuery.Name,
            Parts_Order_Line__c = pol.Id,
            Tracking_Number__c = '12345678',
            ERP_Material_Nbr__c = '1234567892',
            ERP_Reference__c = '9876153#9876153',
            Part__c = product.Id,
            ERP_Qty__c=10,
            ERP_Equipment_Nbr__c = ip.Id,
            Sales_Order_Item__c = soi2.Id,
            Installed_Product__c = ip.Id,
            ERP_Sales_Order_Item__c = '9876153',
            ERP_Serial_Number__c = '8521469',
            ERP_Movement_Type__c = 'test',
            Carrier_name__c = 'dcfdf',
            ERP_Delivery_Type__c = 'ZHCT',
            ERP_Item_Category__c = 'ZKBI'
           
            
            );
            insert pd;

            
        test.stopTest();
    }
    
}
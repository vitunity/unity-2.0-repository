public with sharing class SalesOrderItemDeliveryCtrl {

	private static final String WORKORDER_OBJ = 'SVMXC__Service_Order__c';
	private static final String WORKORDERDETAIL_OBJ = 'SVMXC__Service_Order_Line__c';

	@AuraEnabled 
	public static List<SOI_Delivery__c> getSoiDeliveries(String recId) {
		List<SOI_Delivery__c> listSoiDelReturn;
		String sObjName = Id.valueOf(recId).getSObjectType().getDescribe().getName();
		
		//check if the recId is WorkOrder or WorkDetail
		//Call Workorder method if Workorder
		//Call WkDetail method if WkDetail

		System.debug('#### debug recId = ' + recId);
		System.debug('#### debug sObjName = ' + sObjName);

		if(sObjName == WORKORDER_OBJ) {
			listSoiDelReturn = workOrderContext(recId);
		} /*else if(sObjName == WORKORDERDETAIL_OBJ) {
			listSoiDelReturn = workOrderDetailContext(recId);
		}*/



		return listSoiDelReturn;
	}   


	private static List<SOI_Delivery__c> workOrderContext(String wrkOrdId) {

		System.debug('#### debug in Workord ');

		List<SOI_Delivery__c> listSoiDelReturnLocal1 = new List<SOI_Delivery__c>();

		Set<String> setSoIds = new Set<String>();
		List<String> listSoItemIds = new List<String>();

        /* Adjusted to Work Order as this is not used on WDL
		For(SVMXC__Service_Order_Line__c wrkDt : [SELECT SVMXC__Service_Order__r.Sales_Order__c, Sales_Order_Item__c
													FROM SVMXC__Service_Order_Line__c 
													WHERE SVMXC__Service_Order__c = :wrkOrdId])
		*/
        For (SVMXC__Service_Order__c wrkOrder : [SELECT SVMXC__Service_Order__c.Sales_Order__c
                                                 FROM SVMXC__Service_Order__c
                                                 WHERE Id = :wrkOrdId])
		{
			setSoIds.add(wrkOrder.Sales_Order__c);
            
            //Adjsuted to Work Order as this is not used on WDL.
            //setSoIds.add(wrkDt.SVMXC__Service_Order__r.Sales_Order__c);
			//listSoItemIds.add(wrkdt.Sales_Order_Item__c);
		}

		System.debug('#### debug in Workord setSoIds = ' + setSoIds);
		System.debug('#### debug in Workord listSoItemIds = ' + listSoItemIds);

		if(setSoIds.size() > 0 /*&& listSoItemIds.size() > 0*/) {
			listSoiDelReturnLocal1 = [SELECT Id, Name, ERP_Delivery_Item__c, ERP_Delivery_Date__c, ERP_Delivery_DOC__c, 
										Material__r.Name, Material__r.ProductCode, ERP_Sales_Order__c, ERP_Sales_Order_Higher_Level_Item__c,
										Sales_Order__c, Sales_Order_Item__c,
										ERP_As_Built__c, ERP_Production_Order_Nbr__c, ERP_Serial_Nbr__c, Status__c,
										ERP_Delivery_Qty__c, ERP_Material__c FROM SOI_Delivery__c
										WHERE Sales_Order__c IN :setSoIds 
											  /*AND Sales_Order_Higher_Level_Item__c IN :listSoItemIds*/];
		}
		System.debug('#### debug in Workord listSoiDelReturnLocal1 = ' + listSoiDelReturnLocal1);

		return listSoiDelReturnLocal1;
	}

    /*Not using Work Detail
	private static List<SOI_Delivery__c> workOrderDetailContext(String wrkOrdDtId) {

		System.debug('#### debug in Workord Detail ');

		List<SOI_Delivery__c> listSoiDelReturnLocal2 = new List<SOI_Delivery__c>();

		List<SVMXC__Service_Order_Line__c> listWrkDet = [SELECT SVMXC__Service_Order__r.Sales_Order__c, Sales_Order_Item__c
															FROM SVMXC__Service_Order_Line__c 
															WHERE Id = :wrkOrdDtId];

		System.debug('#### debug in Workord Detail listWrkDet = ' + listWrkDet);
													
															
		if(listWrkDet.size() >0) {
											
			listSoiDelReturnLocal2 = [SELECT ERP_Delivery_Item__c, ERP_Delivery_Date__c, ERP_Delivery_DOC__c, 
										Material__r.Name, ERP_Sales_Order__c, ERP_Sales_Order_Higher_Level_Item__c,
										Sales_Order__c, Sales_Order_Item__c,
										ERP_As_Built__c, ERP_Production_Order_Nbr__c, ERP_Serial_Nbr__c, Status__c,
										ERP_Delivery_Qty__c, ERP_Material__c FROM SOI_Delivery__c
										WHERE Sales_Order__c = :listWrkDet[0].SVMXC__Service_Order__r.Sales_Order__c 
											  AND Sales_Order_Higher_Level_Item__c = :listWrkDet[0].Sales_Order_Item__c];
		}

		System.debug('#### debug in Workord Detail listSoiDelReturnLocal2 = ' + listSoiDelReturnLocal2);


		return listSoiDelReturnLocal2;
	}
	*/
}
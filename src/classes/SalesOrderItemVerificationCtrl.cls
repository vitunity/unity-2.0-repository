public without sharing class SalesOrderItemVerificationCtrl {

    private static final String WORKORDER_OBJ = 'SVMXC__Service_Order__c';

    @AuraEnabled 
    public static List<SOI_Delivery_Verification__c> getSoiVerifications(String recId) {
        List<SOI_Delivery_Verification__c> listSoiDelReturn;
        String sObjName = Id.valueOf(recId).getSObjectType().getDescribe().getName();
        
        //check if the recId is WorkOrder or WorkDetail
        //Call Workorder method if Workorder
        //Call WkDetail method if WkDetail

        System.debug('#### debug recId = ' + recId);
        System.debug('#### debug sObjName = ' + sObjName);

        if(sObjName == WORKORDER_OBJ) {
            listSoiDelReturn = workOrderContext(recId);
        } 

        return listSoiDelReturn;
    }   

    private static List<SOI_Delivery_Verification__c> workOrderContext(String wrkOrdId) {

        System.debug('#### debug in Workord ');

        List<SOI_Delivery_Verification__c> listSoiDelReturnLocal1 = new List<SOI_Delivery_Verification__c>();

        Set<String> setSoIds = new Set<String>();
        List<String> listSoItemIds = new List<String>();
        

        /* Adjusted to Work Order as this is not used on WDL
        For(SVMXC__Service_Order_Line__c wrkDt : [SELECT SVMXC__Service_Order__r.Sales_Order__c, Sales_Order_Item__c
                                                    FROM SVMXC__Service_Order_Line__c 
                                                    WHERE SVMXC__Service_Order__c = :wrkOrdId])
        */
        
        For (SVMXC__Service_Order__c wrkOrder : [SELECT Id, Sales_Order__c
                                                 FROM SVMXC__Service_Order__c
                                                 WHERE Id = :wrkOrdId])
        {
            setSoIds.add(wrkOrder.Sales_Order__c);
            //listSoItemIds.add(wrkOrder.Sales_Order_Item__c);
            
            //Adjsuted to Work Order as this is not used on WDL.
            //setSoIds.add(wrkDt.SVMXC__Service_Order__r.Sales_Order__c);
            //listSoItemIds.add(wrkdt.Sales_Order_Item__c);
        }

        System.debug('#### debug in Workord setSoIds = ' + setSoIds);
        System.debug('#### debug in Workord listSoItemIds = ' + listSoItemIds);

        if(setSoIds.size() > 0 /*&& listSoItemIds.size() > 0*/) {
            listSoiDelReturnLocal1 = [SELECT Name, Confirm__c, ERP_As_Built__c, ERP_Material__c, 
                                        Material__r.Name, Material__r.ProductCode, ERP_Production_Order_Nbr__c, ERP_Serial_Nbr__c,
                                        Sales_Order__c, Sales_Order_Item__c, Sales_Order_Higher_Level_Item__c,
                                        Sales_Order__r.Name, Sales_Order_Item__r.Name, Sales_Order_Higher_Level_Item__r.Name
                                        FROM SOI_Delivery_Verification__c
                                        WHERE Sales_Order__c IN :setSoIds AND Confirm__c = false
                                              /*AND Sales_Order_Higher_Level_Item__c IN :listSoItemIds*/];
        }
        System.debug('#### debug in Workord listSoiDelReturnLocal1 = ' + listSoiDelReturnLocal1);

        return listSoiDelReturnLocal1;
    }

    @AuraEnabled 
    public static void submitSoiVerifications(Id wrkOrdId, List<SOI_Delivery_Verification__c> listDelVerRecs) {
        System.debug('workorder: '+wrkOrdId);
        System.debug('listDelVerRecs = ' + listDelVerRecs);
        SVMXC__Service_Order__c wrkOrderObj = [SELECT Id, SVMXC__Top_Level__r.Id 
                                               FROM SVMXC__Service_Order__c 
                                               WHERE Id = :wrkOrdId];
        list<SOI_Delivery_Verification__c> listDelVersUpd = new list<SOI_Delivery_Verification__c>();
        //List<SOI_Installation_Event__e> listInstallEvents = new List<SOI_Installation_Event__e>();
        //Updated from String soiIds to String soiIds = '' to start string built from blank
        //String soiIds = '';

        if(listDelVerRecs.size() > 0) {
            for(SOI_Delivery_Verification__c delVer : listDelVerRecs) {
                delVer.Work_Order__c = wrkOrderObj.Id;
                delVer.Installed_Product__c = wrkOrderObj.SVMXC__Top_Level__r.Id;
                delVer.Interface_Status__c = 'Process';
                system.debug('delVer: '+delVer);
                if(delVer.Confirm__c == true) {
                    //add this delivery verification to update list if confirmed by installer
                    listDelVersUpd.add(delVer);
                    /* commenting out as Platform Events no longer used
                    if(soiIds != '') {
                        soiIds += ',' + delVer.Id;
                    } else {
                        soiIds += delVer.Id;
                    }                
                    listInstallEvents.add(new SOI_Installation_Event__e(SOI_Verification_SFDC_Id__c = delVer.Id));
                }*/
                }
            }
            //update delivery verification record so they don't get sent multiple times. 
            update(listDelVersUpd);
            }
            //listInstallEvents.add(new SOI_Installation_Event__e(SOI_Verification_SFDC_Id__c = soiIds));
            //system.debug('list of Install Events: '+listInstallEvents);

            /*if(listInstallEvents.size() > 0) {
            // Call method to publish events
                List<Database.SaveResult> results = EventBus.publish(listInstallEvents);

                // Inspect publishing result for each event
                for (Database.SaveResult sr : results) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully published event.');
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Error returned: ' +
                                        err.getStatusCode() +
                                        ' - ' +
                                        err.getMessage());
                        }
                    }       
                }
            }  */     
    }
}
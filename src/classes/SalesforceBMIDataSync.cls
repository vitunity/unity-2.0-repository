/**
 * @description this class syncs Salesforce table with bmi data table
 * @author krishna katve
 */
global with sharing class SalesforceBMIDataSync implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
  
  private String query;
  private String objectName;
  private BigMachinesTransactions bmiTxn;
  private String sessionId;
  private String tableName;
  private String partNumber;
  private String fieldName;
  
  /**
   * Get bmi session id and Delate data table in BMI
   */
  public SalesforceBMIDataSync(String query, String objectName, String tableName, String fieldName, String partNumber){
    this.query = query;
    this.objectName = objectName;
    this.tableName = tableName;
    this.partNumber = partNumber;
    this.fieldName = fieldName;
    bmiTxn = new BigMachinesTransactions();
  }
  
    global Database.QueryLocator start(Database.BatchableContext BC){
    
    sessionId = bmiTxn.getSessionId();
    bmiTxn.sendDeleteTableRequest(tableName, fieldName, sessionId, partNumber);
    
        String query = query;
        return Database.getQueryLocator(query);
    }
    
    /**
     * Push data chunk to bigmachines
     */
    global void execute(Database.BatchableContext BC, List<sObject> sfRecords){
    bmiTxn.sendAddDatatableRequest(objectName, sessionId, sfRecords);
  }
    
    /**
     * Deploy data table in BMI
     */
  global void finish(Database.BatchableContext BC){
    bmiTxn.sendDeployTableRequest(tableName, sessionId);
  }
}
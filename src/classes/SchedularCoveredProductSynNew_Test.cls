@isTest
private class SchedularCoveredProductSynNew_Test
{   
    static testmethod void SchedularCoveredProductSynNew()
    {
        

        Test.StartTest();
        
        // insertAccount
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
        
        Regulatory_Country__c reg = new Regulatory_Country__c();
        reg.Name='US' ; 
        insert reg ;
        
        // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //insert Opportunity
        Opportunity opp = new  Opportunity(Name = 'testOPP', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '30%',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        insert opp;
               
        
        //insert Service/Maintenance Contract
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(Renewal_Opportunity__c = opp.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
        // insert parent IP var loc
        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H12345',SVMXC__Serial_Lot_Number__c ='H12345',SVMXC__Status__c ='Installed',ERP_Reference__c = 'H12345');
        insert objIP;
        
        //Insert Covered Products
        SVMXC__Service_Contract_Products__c testServiceContractProduct1 = new SVMXC__Service_Contract_Products__c(SVMXC__Installed_Product__c  = objIP.Id, SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today()-5, SVMXC__End_Date__c = system.today()+1, Serial_Number_PCSN__c = 'H12345');
        insert testServiceContractProduct1;
        
        SVMXC__Service_Contract_Products__c testServiceContractProduct2 = new SVMXC__Service_Contract_Products__c(SVMXC__Installed_Product__c  = objIP.Id, SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today()-5, SVMXC__End_Date__c = system.today()+1, Serial_Number_PCSN__c = 'H12345');
        insert testServiceContractProduct2;
                
                
        objIP.SVMXC__Service_Contract_Line__c = testServiceContractProduct1.Id;
        update objIP ;
        
        
        SVMXC__Installed_Product__c objTest = [select Id,SVMXC__Service_Contract_Line__c ,Service_Contract_Line_IsActive__c from SVMXC__Installed_Product__c where SVMXC__Service_Contract_Line__c =: testServiceContractProduct1.Id];
        //system.assertEquals(objTest.SVMXC__Service_Contract_Line__c,testServiceContractProduct1.Id);
        //system.assertEquals(objTest.Service_Contract_Line_IsActive__c,true);
        
            
        String sch = '0 0 23 * * ?';
        SchedularCoveredProductSyncNew sh1 = new SchedularCoveredProductSyncNew();            
        system.schedule('Covered Product Sync New', sch, sh1);  

             
        Test.stopTest(); 
      
        
    }  
}
global class SchedularCoveredProductSync implements Schedulable{    
    
    global void execute(SchedulableContext sc) {
      string query = 'select Id from SVMXC__Service_Contract_Products__c where status__c = false and SVMXC__End_Date__c = yesterday';
            
      BatchCoveredProductSync  b;
      if(test.isRunningTest())
      {
           b = new BatchCoveredProductSync('select Id from SVMXC__Service_Contract_Products__c limit  1'); 
      }
      else
      {
            b = new BatchCoveredProductSync(query);   
      }      
      database.executebatch(b,200);       
    }
      
}
global class SchedularCoveredProductSyncNew implements Schedulable{    
    
    global void execute(SchedulableContext sc) {
      string query = 'select Id,SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where status__c = true and SVMXC__Start_Date__c = Today';
            
      BatchCoveredProductSyncNew  b;
      if(test.isRunningTest())
      {
           b = new BatchCoveredProductSyncNew('select Id,SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where status__c = true limit  1'); 
      }
      else
      {
            b = new BatchCoveredProductSyncNew(query);   
      }      
      database.executebatch(b,200);       
    }
      
}
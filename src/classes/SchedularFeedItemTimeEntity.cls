global class SchedularFeedItemTimeEntity implements Schedulable{    
    
    global void execute(SchedulableContext sc) {
      //string query = 'select Id,(select Id from Time_Entries__r limit 100),(select Id from ERP_Partner_Association__r limit 100),(select Id from SVMXC__RMA_Shipment_Order__r limit 100) from Account';
      string query = 'select Id from Account';
      
      BatchFeedItemTimeEntity  b;
      if(test.isRunningTest())
      {
           b = new BatchFeedItemTimeEntity(query +' limit  100'); 
      }
      else
      {
            b = new BatchFeedItemTimeEntity(query);   
      }      
      database.executebatch(b,50);       
    }
      
}
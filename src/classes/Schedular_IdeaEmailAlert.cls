global class Schedular_IdeaEmailAlert implements Schedulable{
    global void execute(SchedulableContext SC){
        string s = 'select Id, Categories, createdDate from Idea where Status = \'New\''; //and createdDate = THIS_WEEK
        if(test.isRunningTest()){
            s = 'select Id, Categories, createdDate from Idea limit 1';
        }
        Batch_IdeaEmailAlert IE = new Batch_IdeaEmailAlert(s);
        Database.executeBatch(IE, 250);  
    }  
}
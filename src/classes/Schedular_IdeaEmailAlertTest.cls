@isTest(SeeAllData=true)
private class Schedular_IdeaEmailAlertTest{
    
    testmethod static void UnitTest() {
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        
        Idea obj = new Idea();
        obj.title='test Idea';
        obj.body = 'test';
        obj.CommunityId = lstc[0].Id;
        insert obj;

        
        IdeaComment com = new IdeaComment();
        com.IdeaId = obj.Id;
        com.CommentBody = 'Test';
        insert com;
        
        Test.StartTest();
            Schedular_IdeaEmailAlert sh1 = new Schedular_IdeaEmailAlert();
            String sch = '0 0 23 * * ?'; 
            system.schedule('TEST Idea Email Alert', sch, sh1); 
        Test.stopTest(); 
    }

}
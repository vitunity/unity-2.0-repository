/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 08-Aug-2017
    @ Description   : STSK0012079 - MyVarian User with Eclipse access should have Model Analytics access.
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    14-Nov-2017 - Rakesh - STSK0013441 - Updated Batch size.
****************************************************************************/
global class ScheduleBatch_Model_Analytic_Member implements Schedulable{
  global void execute(SchedulableContext SC) {
    Batch_Model_Analytic_Member b = new Batch_Model_Analytic_Member();
    Database.executeBatch(b, 10);  
  }
}
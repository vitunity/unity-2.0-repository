/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 18-Jan-2018
    @ Description   : STSK0013441 - Sync deactivated users in Salesforce and Okta.
    Date/Modified By Name/Task or Story or Inc # /Description of Change   
****************************************************************************/
global class ScheduleBatch_OktaUserDeactivation implements Schedulable{
  global void execute(SchedulableContext SC) {
    Batch_OktaUserDeactivation b = new Batch_OktaUserDeactivation();
    Database.executeBatch(b, 10);  
  }
}
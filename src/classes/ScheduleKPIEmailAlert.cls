global class ScheduleKPIEmailAlert implements Schedulable{
  global void execute(SchedulableContext SC) {
    Batch_KPIEmailAlert b = new Batch_KPIEmailAlert();
    Database.executeBatch(b, 100);  
  }
}
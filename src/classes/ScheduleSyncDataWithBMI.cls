global class ScheduleSyncDataWithBMI implements Schedulable{
	global void execute(SchedulableContext SC) {
		SyncDataWithBMI pushData = new SyncDataWithBMI();
		Database.executeBatch(pushData, 200);  
   	}
}
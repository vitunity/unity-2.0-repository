global class ScheduledLocalContractReminderEmail implements Schedulable {
   global void execute(SchedulableContext sc) {
      Batch_LocalItemContractEmail b = new  Batch_LocalItemContractEmail(); 
      database.executebatch(b);
   }
}
@isTest
private class ScheduledLocalContractReminderEmailTest
{

    static testmethod void schedulerTest() 
    {
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        Account acc = new Account();
        acc.Country__c='INDIA';
        acc.BillingCity='PUNE';
        acc.State_Province_Is_Not_Applicable__c=true;
        acc.Name='test';
        insert acc;
        
        Local_Item_Contract__c item = new Local_Item_Contract__c();
        item.Account_Name__c=acc.id;
        item.Contract_End_Date__c = system.today().addDays(15);
        insert item;
        
        Test.startTest();

            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ScheduledLocalContractReminderEmail());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
        // Add assert here to validate result
    }
}
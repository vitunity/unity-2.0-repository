global class Script_SetClearanceToExistingQuote implements Database.Batchable<SObject> {
    global string query;
    global Map<String, Boolean> countryMap;
    global List<Country__c> countrylist;
    global List<BigMachines__Quote__c> updateQuoteList;

    global void getCountryMap() {
        countryMap = new Map<String, Boolean>();
        countrylist = [select Id, Name, Clearance__c from Country__c];
        for (Country__c con: countrylist) {
            if (con!=null && con.Clearance__c!=null && con.Name!=null) {
                countryMap.put(con.Name, con.Clearance__c);
            }
        }
    }

    global Script_SetClearanceToExistingQuote() {
        query = 'select Id, Name, Clearance__c, Ship_To_Country__c from BigMachines__Quote__c';
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List<BigMachines__Quote__c> toUpdate) {
        getCountryMap();
        updateQuoteList = new List<BigMachines__Quote__c>();
        for (BigMachines__Quote__c quote:toUpdate) {
            if(quote != null && countryMap.get(quote.Ship_To_Country__c) != null) {
                quote.Clearance__c =  countryMap.get(quote.Ship_To_Country__c);
                updateQuoteList.add(quote);
            }
        }
        if (!updateQuoteList.isEmpty()) {
            update updateQuoteList;
        }
    }

    global void finish(Database.BatchableContext BC) {}
}
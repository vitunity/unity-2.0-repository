@isTest(seeAllData=False)
public class Script_SetClearanceToExistingQuoteTest {
    static testMethod void test_Clearance() {
        Test.startTest();
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = '121212';
        insert acct;
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        /*BigMachines__Configuration_Record__c site_record = new BigMachines__Configuration_Record__c();
        site_record.BigMachines__bm_site__c = 'Test';
        site_record.BigMachines__process_id__c = '12345';
        site_record.BigMachines__action_id_copy__c = '12345';
        site_record.BigMachines__action_id_open__c = '12345';
        site_record.BigMachines__document_id__c = '12345';
        site_record.BigMachines__version_id__c = '12345';
        site_record.BigMachines__process__c = 'Test123';
        site_record.BigMachines__Is_Active__c = true;
        insert site_record;*/
        
        BigMachines__Quote__c quote = TestUtils.getQuote();
        quote.BigMachines__Account__c = acct.Id;
        quote.BigMachines__Opportunity__c = opp.Id;
        quote.National_Distributor__c = '121212'; 
        quote.SAP_Booked_Service__c = 'Booked';
        quote.Clearance__c =  true;
        quote.Ship_To_Country__c = 'Algeria';
        //quote.BigMachines__Site__c = site_record.Id;
        insert quote;
        
        Script_SetClearanceToExistingQuote sctq = new Script_SetClearanceToExistingQuote();
        Database.BatchableContext bc;

        sctq.query = 'select Id, Name, Clearance__c, Ship_To_Country__c from BigMachines__Quote__c';
        List<BigMachines__Quote__c> st = database.query(sctq.query);
        sctq.start(BC);
        sctq.execute(BC, st);
        sctq.finish(BC);

        Test.stopTest();
    }
}
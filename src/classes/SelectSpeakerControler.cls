public with sharing class SelectSpeakerControler {

 
  
    public SelectOption[] selectedSpeakers { get; set; }
    public SelectOption[]  allSpeakers { get; set; } 
    public String message { get; set; }
    
    public SelectSpeakerControler() 
    {
    //CH.New.Start
      //  system.debug('xxxEID: '+ApexPages.currentPage().getParameters().get('eid'));
       // VU_Event_Schedule__c ves = [Select ID, VU_Events__c from VU_Event_Schedule__c where ID=:ApexPages.currentPage().getParameters().get('eid')];
    //CH.New.End        
       List<VU_Speakers__c> Speakers = [SELECT Name,LastName__c,Id FROM VU_Speakers__c ORDER BY LastName__c];    
     //  List<VU_Speaker_Event_Map__c> Speakers = [SELECT Id, VU_Speakers__r.Name FROM VU_Speaker_Event_Map__c]; 
        allSpeakers = new List<SelectOption>();
        
        selectedSpeakers = new List<SelectOption>();
      for ( VU_Speakers__c c : Speakers ) 
      //CH.Old.Start
      
        //for ( VU_Speaker_Event_Map__c c : Speakers ) 
        {
            allSpeakers.add(new SelectOption(c.id ,c.LastName__c+', '+ c.Name));
        }
      //CH.Old.End        
      //CH.New.Start        
      /*List<VU_Speakers__c> lstSpk = [Select ID, Name from VU_Speakers__c where VU_Events__c=:ves.VU_Events__c];
      for ( VU_Speakers__c c1 : lstSpk) 
        {
            allSpeakers.add(new SelectOption(c1.id , c1.Name));
        }*/
      //CH.New.End
    }

    public PageReference save() 
    {
        List<VU_Speaker_Event_Map__c> lstVUspeaker = new List<VU_Speaker_Event_Map__c>();
        message = 'Selected Speakers: ';
        Boolean first = true;
           system.debug('SSSS'+selectedSpeakers );
        for ( SelectOption so : selectedSpeakers ) 
        {
            if (!first) 
            {
                message += ', ';
            }
            message += so.getLabel() + ' (' + so.getValue() + ')';
            first = false;
            VU_Speaker_Event_Map__c v1 = new VU_Speaker_Event_Map__c(Event_Webinar__c=ApexPages.currentPage().getParameters().get('eid'), VU_Speakers__c =so.getValue());
           // v1.id=id.valueOf(so.getValue());
            lstVUspeaker.add(v1);
         
        }
       if(lstVUspeaker.size()>0)
        {
            system.debug('XXXX'+lstVUspeaker);
           insert lstVUspeaker;
        }
        system.debug(ApexPages.currentPage().getParameters().get('eid'));
        PageReference pr = new PageReference('/'+ApexPages.currentPage().getParameters().get('eid'));
        return pr; 
    }
}
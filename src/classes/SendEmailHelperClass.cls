/**
  * @author jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Holds modularized method for sending an email message.
  * 
  * TEST CLASS 
  * 
  *    SendEmailHelperClass_TEST
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  jwagner/Forefront; Initial Build
  * 
  **/
public with sharing class SendEmailHelperClass {
    
  public static Messaging.SingleEmailMessage sendEmail(String subject, String body, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, Boolean sendNow)
  {
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

      mail.setToAddresses(toAddresses);
      mail.setCcAddresses(ccAddresses);
      mail.setBccAddresses(bccAddresses);
      mail.setSenderDisplayName('Salesforce Support');
      mail.setSubject(subject);
      mail.setUseSignature(false);
      mail.setHtmlBody(body);

      if(sendNow)
      {
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      }

      return mail;
  }
}
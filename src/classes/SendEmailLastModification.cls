/**************************************************************************\
    @ Author        : Rakesh Basani
    @ Date      : 08-Aug-2017
    @ Description   : STSK0012077 - Update last modification data/time when emails are sent in/out of the case
****************************************************************************/
global class SendEmailLastModification implements Schedulable {
    public set<Id> setCaseIds;
    global SendEmailLastModification(set<Id> ss){
        setCaseIds = ss;
    }
    global void execute(SchedulableContext sc) {           
        List<Case> lstCase = [select id,LastEmailSent__c from Case where Id IN: setCaseIds];
        for(Case c : lstCase){
            c.LastEmailSent__c = system.now();
        }
        database.update(lstCase,false);  
    }     
}
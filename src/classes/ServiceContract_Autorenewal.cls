/*
@ Author          : Shital Bhujbal
@ Date            : 20-Aug-2018
@ Description     : This class is written to create shell Opportunity for the service contracts which are eligible for 1 year auto renewal.
*/
Public class ServiceContract_Autorenewal{
    
    List<Opportunity> lstOpportunity = new List<Opportunity>(); 
    List<BigMachines__Quote__c> lstQuote = new List<BigMachines__Quote__c>();
    Map<Id,String> contractOptyMap = new Map<Id,String>();
    Map<String,Opportunity> quoteOptyMap = new Map<String,Opportunity>();
    Set<Id> contractIds = new Set<Id>();
    Opportunity commonOppty;
    SVMXC__Service_Contract__c commonContract;
    public static boolean isExecuting = false;
    List<String> softwarePCSNs = new List<String>{'HIT','HML','H48','H62','H6T'};
    String tempMasterEquipment;
    
    public void createOpptyAndQuote(list<SVMXC__Service_Contract__c> contractList){   
        if(ServiceContract_Autorenewal.isExecuting){
            return;
        }
        
        ServiceContract_Autorenewal.isExecuting = true; 
        
        for(SVMXC__Service_Contract__c objContract : contractList){
            if(objContract.ERP_Header_Contract_Description__c!='' && objContract.ERP_Header_Contract_Description__c.startsWith('AR-')){
                
                Opportunity opp = new Opportunity();
                opp.AccountId = objContract.SVMXC__Company__c;  
                opp.Name = '1 year Auto-Renewal Opportunity# '+objContract.name;
                //opp.ownerid = objContract.ownerid ;
                opp.Service_Maintenance_Contract__c = objContract.Id;
                opp.StageName = '7 - CLOSED WON';
                opp.Account_Sector__c = 'Private';
                opp.CurrencyIsoCode = 'USD';
                opp.Type = 'Service Contract -New';
                opp.Existing_Equipment_Replacement__c = 'No';
                opp.Payer_Same_as_Customer__c = 'No';
                opp.Description = 'Auto renewal Opportunity created for service contract';
                opp.Primary_Contact_Name__c = objContract.SVMXC__Contact__c;
                opp.CloseDate = objContract.SVMXC__End_Date__c;
                opp.Deliver_to_Country__c = objContract.SVMXC__Company__r.Country__c;
                opp.X1_Year_Auto_Renewal_Contract_Oppty__c = true;
                
                if(objContract.Master_Equipment__c!=NULL && objContract.Master_Equipment__c!=''){
                    
                	tempMasterEquipment = objContract.Master_Equipment__c.substring(0,3);
                	if(softwarePCSNs.contains(tempMasterEquipment) && objContract.SVMXC__Company__r.Software_Sales_Manager__c!=NULL){
                    	opp.ownerid = objContract.SVMXC__Company__r.Software_Sales_Manager__c;
                	}else if(objContract.SVMXC__Company__r.Service_Sales_Manager__c!=NULL){
                    	opp.ownerid = objContract.SVMXC__Company__r.Service_Sales_Manager__c;
                	}else{
                    	opp.ownerid = objContract.ownerid;
                	}
            	}
                //opp.Amount__c = objContract.SVMXC__Contract_Price2__c; - Amount field is read only field
                contractOptyMap.put(objContract.Id,opp.Name);
                lstOpportunity.add(opp);
            }
        }
        if(lstOpportunity.size()>0){
            insert lstOpportunity;
        } 
        
        // 1 Year Auto renewal - Create 1 year Autorenewal Opportunity field on Service contract object and update Service contract with this field in ‘ServiceContract_Autorenewal’ class.
        List<SVMXC__Service_Contract__c> lstSvcContract=new List<SVMXC__Service_Contract__c>();
        
        for(Opportunity opty:lstOpportunity){
			if(contractList!=null && contractList.size()>0){
                for(SVMXC__Service_Contract__c sc:contractList){
                        
                    if(contractOptyMap.containsKey(sc.Id) && contractOptyMap.get(sc.Id)!=null && contractOptyMap.get(sc.Id)== opty.Name){
                        SVMXC__Service_Contract__c tempContract = new SVMXC__Service_Contract__c(Id=sc.Id);
                        tempContract.X1_year_Autorenewal_Opportunity__c = opty.Id;
                        lstSvcContract.add(tempContract);
                    }
                }
            }
        }
        if(lstSvcContract!=null && lstSvcContract.size()>0){
            update lstSvcContract;
        }
    }
}
@isTest
private class ServiceContract_AutorenewalTest {
     static SVMXC__Service_Contract__c testServiceContract ;
     static {
        
        // insertAccount
        Account acc = new Account(ERP_Timezone__c='Aussa', Name = 'TestAprRel',OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425',BillingCity='LA',BillingState='CA',State_Province_Is_Not_Applicable__c=true);
        insert acc;
        
        // insert Contact  
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = acc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //insert Opportunity
        Opportunity opp = new  Opportunity(Name = 'testOPP', AccountId = acc.id, Primary_Contact_Name__c = con.id, StageName = '30%',
        type = 'Service Contract - Renewal', Expected_Close_Date__c = System.today(), CloseDate =System.today());
        insert opp;
               
        //insert Oracle Quote
        /*BigMachines__Quote__c  salesQuote= TestUtils.getQuote();
        salesQuote.BigMachines__Account__c = acc.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        salesQuote.Price_Group__c = 'Z1';
        salesQuote.Service_Contract_Auto_Renewal__c='Auto-Renewal';
        insert salesQuote;*/
        
        //insert Service/Maintenance Contract
        testServiceContract = new SVMXC__Service_Contract__c(ERP_Header_Contract_Description__c = 'AR-abcd', Renewal_Opportunity__c = opp.id, name = 'test service contract',SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+30, SVMXC__Company__c=acc.id, ERP_Sold_To__c = '12345678');
        insert testServiceContract;
        
    }
    
     static testmethod void test() {     
        Test.startTest();
        testServiceContract.X1_year_Auto_renewal_contract__c=true;
        testServiceContract.OwnerId=UserInfo.getUserId();
        update testServiceContract;
        List<SVMXC__Service_Contract__c> lstSvcContract=new List<SVMXC__Service_Contract__c>();
        lstSvcContract.add(testServiceContract);
        ServiceContract_Autorenewal autoRenewal=new ServiceContract_Autorenewal();
        autoRenewal.createOpptyAndQuote(lstSvcContract);
        Test.stopTest();

    }
}
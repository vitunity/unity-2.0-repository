global class ServiceOrderLineWrapper implements Comparable {

    public SVMXC__Service_Order_Line__c sol;
    
    // Constructor
    public ServiceOrderLineWrapper(SVMXC__Service_Order_Line__c wd) {
        sol = wd;
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        ServiceOrderLineWrapper compareToSOL = (ServiceOrderLineWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (sol.SVMXC__Start_Date_And_time__c > compareToSOL.sol.SVMXC__Start_Date_And_time__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (sol.SVMXC__Start_Date_And_time__c < compareToSOL.sol.SVMXC__Start_Date_And_time__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}
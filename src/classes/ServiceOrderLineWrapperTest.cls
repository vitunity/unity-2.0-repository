/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest 
private class ServiceOrderLineWrapperTest {
    static testmethod void testSort() {
        // Add the Work Detail wrapper objects to a list.
        ServiceOrderLineWrapper[] wdList = new List<ServiceOrderLineWrapper>();
        Date adate = Date.today();
        wdList.add( new ServiceOrderLineWrapper(new SVMXC__Service_Order_Line__c(
            SVMXC__Start_Date_and_Time__c=adate.addDays(10))));
        wdList.add( new ServiceOrderLineWrapper(new SVMXC__Service_Order_Line__c(
            SVMXC__Start_Date_and_Time__c=adate.addDays(-1))));
        wdList.add( new ServiceOrderLineWrapper(new SVMXC__Service_Order_Line__c(
            SVMXC__Start_Date_and_Time__c=adate)));
        
        // Sort the wrapper objects using the implementation of the 
        // compareTo method.
        wdList.sort();
        
        // Verify the sort order
        System.assertEquals(adate.addDays(-1), wdList[0].sol.SVMXC__Start_Date_and_Time__c);
       
        System.assertEquals(adate, wdList[1].sol.SVMXC__Start_Date_and_Time__c);
        
        System.assertEquals(adate.addDays(10), wdList[2].sol.SVMXC__Start_Date_and_Time__c);
        
        
        // Write the sorted list contents to the debug log.
        System.debug(wdList);
    }
}
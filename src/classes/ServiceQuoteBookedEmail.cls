global class ServiceQuoteBookedEmail implements Database.Batchable<sObject>, Schedulable  {
    
    public Integer minutesOffset;
    
    public static void scheduleOneTime(){
        // Cron EXP for hourly schedule 
        String CRON_EXP = '0 0 * * * ?'; 
        ServiceQuoteBookedEmail sch = new ServiceQuoteBookedEmail(); 
        system.schedule('Hourly ServiceQuoteBookedEmail Batch Schedule job', CRON_EXP, sch);
    }
    
    global void execute(SchedulableContext SC) {
        ServiceQuoteBookedEmail obj = new ServiceQuoteBookedEmail();
        Database.executeBatch(obj, 1); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(minutesOffset == null){
            minutesOffset = 50;
        }
        Integer minutesOffsetLocal = minutesOffset;
        DateTime localDateStart= System.now().addMinutes(-(minutesOffsetLocal));
        DateTime localDateEnd = localDateStart.addHours(-2);
        return Database.getQueryLocator(
            'SELECT ID FROM BigMachines__Quote__c ' + 
            'Where SAP_Booked_Service_Start_Update__c <=:  localDateStart '+
            'and SAP_Booked_Service_Start_Update__c >= :localDateEnd ' +
            'and SAP_Booked_Service_Email__c = false'
        );
        /*
        DateTime dt = DateTime.newInstance(2018,2,25);
        return Database.getQueryLocator(
            'SELECT ID FROM BigMachines__Quote__c ' + 
            'Where CreatedDate >=:dt and SAP_Booked_Service_Start_Update__c != null'+
            'and SAP_Booked_Service_Email__c = false'
        );*/
        
    }

    global void execute(Database.BatchableContext bc, List<sObject> records){
        list<BigMachines__Quote__c> listOfQuotes = (list<BigMachines__Quote__c>)records;
        for(BigMachines__Quote__c bq: listOfQuotes){
            bq.SAP_Booked_Service_Email__c = true;
        }
        update listOfQuotes;
    }    

    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    

}
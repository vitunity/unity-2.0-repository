/**
 * Test class for Batch ServiceQuoteBookedEmai 
 */
@isTest(seealldata=false)
public class ServiceQuoteBookedEmailTest {
    
    @isTest(seealldata=true)
    public static void UnityTest1(){
        list<BigMachines__Quote__c> quoteList = [select Id from BigMachines__Quote__c order by createddate  limit 1];
        for(BigMachines__Quote__c q: quoteList){
            q.SAP_Booked_Service_Start_Update__c = System.now().addHours(-1);
        }                   
        update quoteList;
        System.Test.startTest();
        ServiceQuoteBookedEmail obj = new ServiceQuoteBookedEmail();
        Database.executeBatch(obj, 1);     
        System.Test.stopTest();
        }
     
    public static testMethod void UnityTest4(){
        System.Test.startTest();
        try{ServiceQuoteBookedEmail.scheduleOneTime();}catch(Exception e){}        
        System.Test.stopTest(); 
       
    }

}
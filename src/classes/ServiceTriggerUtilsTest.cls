@isTest(SeeAllData = false)
public class ServiceTriggerUtilsTest
{
    
    public static ID TechRecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();  
    public static ID hdRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();  
    public static profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
    public static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname='Testing',languagelocalekey='en_US', localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles', username='standardtestuse92@testclass.com');
    public static Account objAcc;
    public static Contact objCont;
    public static product2 objProd;
    public static SVMXC__Installed_Product__c objIns; 
    public static Case objCase;
    public static SVMXC__Service_Level__c slaTerm ;
    public static SVMXC__Service_Contract__c testServiceContract;
    public static SVMXC__Service_Contract_Products__c testServiceContractProduct;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static SVMXC__Service_Group_Members__c ObjTech;
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    Static{
        
        //insert Account
        objAcc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States');
        insert objAcc;
       
        //insert Contact Record
        objCont = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = objAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '55667688');
        insert objCont;
        
         //insert Product Record
        objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
        insert objProd;
        
        //insert Service Team Record
         ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(), SVMXC__Group_Code__c = 'ABC');
        insert ObjSvcTeam;
        
        //insert technician Record
        ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true, SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com', SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId());
        insert ObjTech; 
          
          //insert Insalled Product Record
        objIns = new SVMXC__Installed_Product__c( Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H12345', SVMXC__Company__c = objAcc.id, Name = 'H12345', SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id);
        insert objIns;
         
         // insert SLA term
        slaTerm = new SVMXC__Service_Level__c(Name = 'test sla Term', SVMXC__Active__c = true);
        insert slaTerm;
        
        //insert service contract
        testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',SVMXC__Start_Date__c = system.today()-1,SVMXC__End_Date__c = system.today()+29, SVMXC__Service_Level__c= slaTerm.id, SVMXC__Company__c = objAcc.id, ERP_Order_Reason__c = 'HS1', Quote_Reference__c = 'asdfghjk');
        insert testServiceContract;
        
        //insert Covered Product
        testServiceContractProduct = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c =testServiceContract.id, SVMXC__Start_Date__c =system.today()-1, SVMXC__End_Date__c = system.today()+29, Serial_Number_PCSN__c = 'H12345', SVMXC__Installed_Product__c = objIns.id);
        insert testServiceContractProduct;
        
        //insert Case
        ObjCase = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id, SVMXC__Top_Level__c = objIns.id, Priority = 'Medium');
        insert objCase;
    
    
    }

    public static SVMXC__Case_Line__c CaseLine_Data(Boolean isInsert, Case newCase, product2 newProd) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase.id;
        caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd.Id;
        caseLine.Start_Date_time__c = system.today().addDays(-2);
        caseLine.End_date_time__c = system.today().addDays(+2);
        //caseLine.SVMXC__Installed_Product__c = ;
        if(isInsert)
            insert caseLine;
        return caseLine;
    }

    public static testMethod void testController()
    { 
        test.startTest();
        SVMXC__Case_Line__c cl1 = CaseLine_Data(false, objcase, objProd);
        cl1.Start_Date_time__c = system.now();
        cl1.End_date_time__c = system.now().addHours(1);
        cl1.RecordTypeId = RecordTypeHelperClass.CASELINE_RECORDTYPES.get('Time Entry').getRecordTypeId();

            /*
            SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;*/
        insert cl1;
        cl1.End_date_time__c = system.now().addMinutes(130);
       /*
        SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;*/
        update cl1;

        set<id> caseId= new Set<id>();
        set<String> caseStr= new Set<String>();
        caseId.add(objcase.id);
        caseStr.add(objcase.id);
        ServiceTriggerUtil serv = new ServiceTriggerUtil();
        ServiceTriggerUtil.gtCaseMap(caseStr);
        
        set<SVMXC__Case_Line__c> caseLines = new set<SVMXC__Case_Line__c>();
        caseLines.add(cl1);
        set<id> ipId = new Set<id>();
        ipId.add(objIns.id);
        map<id, Case> mapCa = new map<id, Case> ();
        mapCa.put(objCase.id, objCase);
        
        serv.MethodScCaseLineTrigger(caseLines, ipId, caseId, mapCa);
 
        delete cl1;
        test.stopTest();
    }
    public static testMethod void testController_csline()
    {   ID Rec_helpdesk = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();  
        ID Rec_ProdService = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();  
        Case ObjCS =new Case(ProductSystem__c =objIns.id,RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id,Priority = 'Medium');
        Insert ObjCS;
        SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c(RecordTypeId = Rec_helpdesk,SVMXC__Case__c= ObjCS.id,SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium');
            insert objWO;
            /*SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;*/
            SVMXC__Service_Order_Line__c   objWD = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c=objWO.id,recordtypeid=Rec_ProdService);
           insert objWD;
        SVMXC__Case_Line__c objCaseLine = new SVMXC__Case_Line__c(SVMXC__Case__c = ObjCS.id, Start_Date_time__c = system.now(),End_date_time__c = system.now().addHours(1), SVMXC__Installed_Product__c = objIns.id);
        test.startTest();
            /*SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;*/
        insert objCaseLine;

        objCaseLine = new SVMXC__Case_Line__c(id = objCaseLine.id, End_date_time__c = system.now().addMinutes(130));
        SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;
            
           update objCaseLine;
        
        
        
        set<string> caseId= new Set<string>();
        caseId.add(ObjCS.id);
        //ServiceTriggerUtil serv = new ServiceTriggerUtil();
        ServiceTriggerUtil.gtCaseMap(caseId);
        /*
        set<SVMXC__Case_Line__c> = new set<SVMXC__Case_Line__c>();
        set<id> ipId= new Set<id>();
        map<id, Case> mapCa = new map<id, Case> ();
        
         MethodScCaseLineTrigger(set<SVMXC__Case_Line__c> set_CaseLine,set<id> setIPId,set<id> setCaseId, map<id, Case> mapcase)//
        MethodScCaseLineTrigger
        */
        test.stopTest();
    }
    public static testMethod void testController_csline1()
    {   ID Rec_helpdesk = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Helpdesk').getRecordTypeId();  
        ID Rec_ProdService = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();  
        Case ObjCS =new Case(ProductSystem__c =objIns.id,RecordTypeId = hdRecordTypeID, AccountId = objAcc.id, ContactId = objCont.id,Priority = 'Medium');
        Insert ObjCS;
        //SVMXC__Service_Order__c objWO = new SVMXC__Service_Order__c(RecordTypeId = Rec_helpdesk,SVMXC__Case__c= ObjCS.id,SVMXC__Company__c = objAcc.id, SVMXC__Contact__c = objCont.id, SVMXC__Priority__c = 'Medium');
           // insert objWO;
            SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;
            
        SVMXC__Case_Line__c objCaseLine = new SVMXC__Case_Line__c(SVMXC__Case__c = ObjCS.id, Start_Date_time__c = system.now(),End_date_time__c = system.now().addHours(1), SVMXC__Installed_Product__c = objIns.id);
        test.startTest();
            SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;
        insert objCaseLine;

        objCaseLine = new SVMXC__Case_Line__c(id = objCaseLine.id, End_date_time__c = system.now().addMinutes(130));
        objCaseLine.RecordTypeId = RecordTypeHelperClass.CASELINE_RECORDTYPES.get('Time Entry').getRecordTypeId();
        /*SR_Class_WorkOrder.blnRec = true;
            SR_Class_WorkOrder.recursiveCount = 3;
            SR_Class_WorkOrder.CheckRecusrsive = true;
            SR_Class_WorkOrder.CheckRecusrsiveafter = SR_Class_WorkOrder.CheckRecusrsiveafter + 1; //savon.FF 12-30-2015 Changed to Recursive Variables per DE6164
            SR_Class_WorkOrder.UPDATE_WORK_DETAIL_FROM_WORK_ORDER = true;
            SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER = SR_Class_WorkOrder.UPDATE_WORK_ORDER_FROM_WORK_ORDER + 1; //savon.FF 12-30-2015 Changed to Recursive Variable per DE6164
            SRClassWorkOrderDetail.recursiveCount = 3;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDet = true;
            SRClassWorkOrderDetail.CheckRecusrsiveWorkDetAfter = true;*/
            
           update objCaseLine;
        
        
        
        set<string> caseId= new Set<string>();
        caseId.add(ObjCS.id);
        Map<id,case> mapnewCase=new Map<id,case>();
        
        Map<id,case> mapOldCase=new Map<id,case>();
        Case cs=new Case(id = ObjCS.id , status = 'Closed');
        mapOldCase.put(ObjCS.id,ObjCS);
        mapnewCase.put(ObjCS.id,cs);
        //ServiceTriggerUtil serv = new ServiceTriggerUtil();
        ServiceTriggerUtil.gtCaseMap(caseId);
        ServiceTriggerUtil.UpdateWorkOrderStatus(mapnewCase,mapOldCase);
        ServiceTriggerUtil sTrigger = new ServiceTriggerUtil();
        List<SVMXC__Case_Line__c> lstCaseLine = new List<SVMXC__Case_Line__c>();
        lstCaseLine.add(objCaseLine);
        sTrigger.PrePoulateFldsOnCaseLine(lstCaseLine, mapnewCase);
        /*
        set<SVMXC__Case_Line__c> = new set<SVMXC__Case_Line__c>();
        set<id> ipId= new Set<id>();
        map<id, Case> mapCa = new map<id, Case> ();
        
         MethodScCaseLineTrigger(set<SVMXC__Case_Line__c> set_CaseLine,set<id> setIPId,set<id> setCaseId, map<id, Case> mapcase)//
        MethodScCaseLineTrigger
        */
        test.stopTest();
    }
}
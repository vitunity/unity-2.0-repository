global class SimulationFailureBatchForERPDates  implements Database.Batchable<sObject> {
    
    private string q ;
    private set<string> ids;
    
    global SimulationFailureBatchForERPDates(string q){
        this.q = q;
    }
    
    global SimulationFailureBatchForERPDates(set<string> ids, String q){
        this.ids = ids;
        this.q = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
       set<string> temp_ids;
       temp_ids = ids;
       if(ids != null){
           return Database.getQueryLocator(q+ ' where id in : temp_ids ');
       }else{
           return Database.getQueryLocator(q);
       }
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        list<SVMXC__Service_Order__c> woList = (list<SVMXC__Service_Order__c>)scope;
        system.debug('size wolist  ====='+woList.size());
        for(SVMXC__Service_Order__c wo : woList){
            String t = wo.SVMXC__Company__r.Timezone__r.Salesforce_timezone__c;
            if(wo.Machine_release_time__c != null){
                String enddate = wo.Machine_release_time__c.format('YYYY-MM-dd HH:mm:00',t.substring(t.indexof('(')+ 1,t.indexof(')')));
                DateTime erp_enddate = DateTime.valueOf(enddate) ;
                wo.ERP_Malfunction_End_Date_Time__c = erp_enddate;
            }
            
            if(wo.RecordType.DeveloperName == 'Helpdesk'){
                wo.ERP_Malfunction_Start_Date_Time__c = wo.ERP_Requested_Start_Date_Time__c;
            }
            
            if(wo.RecordType.DeveloperName == 'Field_Service'){
                String startdate = wo.Malfunction_Start__c.format('YYYY-MM-dd HH:mm:00',t.substring(t.indexof('(')+ 1,t.indexof(')')));
                DateTime erp_startdate= DateTime.valueOf(startdate) ;
                wo.ERP_Malfunction_Start_Date_Time__c = erp_startdate;  
            }

            if(wo.SVMXC__Order_Status__c == 'Approved' && wo.ERP_Status__c == 'Simulation Failure'){
                wo.ERP_Service_Request_Nbr__c = null;
                wo.ERP_Status__c = null;
                wo.Interface_Status__c = null;
                if(wo.WO_Interface_Simulation_Counter__c  == null)
                    wo.WO_Interface_Simulation_Counter__c = 0;
                wo.WO_Interface_Simulation_Counter__c = wo.WO_Interface_Simulation_Counter__c + 1;
            }
        }
        if(!woList.isEmpty()){
            update woList;
        }
    }

   global void finish(Database.BatchableContext BC){
       
   }
}
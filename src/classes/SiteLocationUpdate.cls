// Test Class - SiteLocationUpdateTest
public class SiteLocationUpdate{
    public static void UpdateSite(List<Case> lstCase, map<Id,case> oldmap){
        set<Id> setIP = new set<Id>();
        Id HDRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        list<case> filteredCase = new list<case>();
        for(Case c : lstCase){
            if(c.RecordTypeId == HDRT ){
                if(c.ProductSystem__c == null){
                    c.SVMXC__Site__c = null;
                }else if(c.ProductSystem__c <> null && (oldmap == null || (oldmap <> null && (c.ProductSystem__c <> oldmap.get(c.Id).ProductSystem__c || c.RecordTypeId <> oldmap.get(c.Id).RecordTypeId)))){
                    setIP.add(c.ProductSystem__c);filteredCase.add(c);
                }
            }
        }
        if(setIP.size()>0){
            map<Id,SVMXC__Installed_Product__c> mapIP = new map<Id,SVMXC__Installed_Product__c>([Select id, SVMXC__Site__c from SVMXC__Installed_Product__c where Id IN: setIP]);
            for(case c: filteredCase){
                if(mapIP.containsKey(c.ProductSystem__c)){
                    c.SVMXC__Site__c = mapIP.get(c.ProductSystem__c).SVMXC__Site__c ;
                }
            }
        }
    }
}
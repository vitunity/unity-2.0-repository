@isTest
public class SiteLocationUpdateTest{
    public static testMethod void TestLocation() {
        
        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        acc.BillingCity ='Pune';
        acc.BillingCountry='Test';
        acc.BillingState='Washington';
        acc.Account_Type__c = 'Customer';
        acc.BillingStreet='xyx';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'test';
        cont.lastname= 'test2';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test2@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        Id HDRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        case cs = new case();
        cs.recordTypeId = HDRT;
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;
    }
}
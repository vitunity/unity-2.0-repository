/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 03-Apr-2013
    @ Description   :  An apex page controller that exposes the site login functionality.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }
    
    global SiteLoginController () {}
    
    @IsTest(SeeAllData=true) global static void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}
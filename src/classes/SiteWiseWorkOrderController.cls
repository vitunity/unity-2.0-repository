/*************************************************************************\
@ Author        : Deepak Sharma
@ Date          :  31-Aug-2017
@ Description   : Classs defined to handle logic to display workorders of given site
*   User has option to select work orders , attachment and hence email the selected one

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************
03-Jan-2018 Nick Sauer        STSK0013526    1) Checkbox to turn auto-CC on/off
14-Nov-2017 Nick Sauer        STSK0013367    1) Added Contact Role Association
23-Oct-2017 Nick Sauer        STSK0013191    1) Add current User email to CC on email generation
23-Oct-2017 Deepak Sharma     STSK0013183    1) Regulatory:  Add Varian Signature to Work Order and Location Email Pages
22-Oct-2017 Deepak Sharma     STSK0013180    1) Allow Query where Site ID is blank
18-Oct-2017 Nick Sauer        STSK0013166    1) Add Service_Team__r.SVMXC__Group_Code__c to query output for VF page
                                             2) Add Service_Team__r.SVMXC__Group_Code__c to query input for VF page
                                             3) Adjust input rules to require 1 selection other than Date range - prevent view state limit
20-Sep-2017 Deepak Sharma     STSK0012811   1)  Add "Create Combined Local PDF" to merge and pop up save dialog for desktop save
											2)  Adjust manual address entry to semicolon delimited instead of comma delimited (to match outlook format).
****************************************************************************/

public with sharing class SiteWiseWorkOrderController {
    
    //Logical Parameters
    public  String siteID{get;set;}
    public  String siteName{get;set;}
    public  SVMXC__Service_Order__c searchFromDate{get;set;}
    public  SVMXC__Service_Order__c searchToDate{get;set;}
    //Objects List
    public List<SVMXC__Service_Order__c> listWorkOrder{get;set;}
    public List<WorkOrderWrapper> listWorkOrderWrapper{get;set;}
    public List<Contact> listContact{get;set;}
    public List<ContactWrapper> listContactWrapper{get;set;}
    public List<AttachmentWrapper> listAttachmentWrapper{get;set;}
    public List<ContactRoleWrapper> listContactRoleWrapper{get;set;}
    @TestVisible List<SVMXC__Service_Order__c> listWorkOrderTemp{get;set;}
    public static List<Map<String, List<Attachment>>> listMapAtchtWrap;
    public  Map<String, List<Attachment>> mapAtchWrap =new  Map<String, List<Attachment>>();
    public   List<SelectOption> selOptERP{get;set;}
    public Boolean emailCC{get;set;}
    //Search Parameter
    public  String searchByTopLevel{get;set;}
    public  String searchByCompLevel{get;set;}
    public  String searchByCaseNo{get;set;}
    public  String searchByTech{get;set;}
    public  String searchByERP{get;set;}
    public  String searchByTeam{get;set;}
    
    
    // query    
    @TestVisible private String queryWorkOrder{get;set;}
    
    //Email Parameters
    public String addMailRecepients{get;set;}
    public String emailSub{get;set;}
    public String emailBody{get;set;}
    //STSK0013191:  Derive current User Email for mailCC
    public String[] ccAddresses = new String[]{ UserInfo.getUserEmail()};
    @Testvisible private List<String> sendTo = new List<String>();
    @Testvisible private String prameters = '';
    //Div Section display Flags
    public  Boolean  showWorkOrderSection{get;set;}
    public  Boolean  showAttachmentSection{get;set;}
    public  Boolean  showContactAndMailSection{get;set;}
    
    //Constants
    Integer woDisplayLimit=50;
    Double  fileAttachLimit=25000000.00;
    String blankLiteral='Blank';
    
    
    //No Argument Constructor 
    public SiteWiseWorkOrderController()
    {
        try
        {
            erpStatusPickList();
            Date defaultFromDate=Date.today().tostartofmonth();
            Date defaultToDate=Date.today().tostartofmonth();
            searchFromDate=new SVMXC__Service_Order__c();
            searchToDate=new SVMXC__Service_Order__c();
            searchFromDate.SVMXC__Scheduled_Date_Time__c=DateTime.newInstance(defaultFromDate.year(),defaultFromDate.month(),defaultFromDate.day(),0,0,0);
            searchToDate.SVMXC__Scheduled_Date_Time__c=DateTime.newInstance(defaultToDate.year(),defaultToDate.month(),defaultToDate.day()+30,0,0,0);
            siteID = ApexPages.currentPage().getParameters().get('id');
            if(String.isBlank(siteID)){
                throw new SiteWiseWorkOrderException('The URL doesn\'t has required parameter [id]');
            }else
            {
                List<SVMXC__Site__c> siteList=[select id, name from SVMXC__Site__c where id=:siteID limit 1];
                if(!siteList.isEmpty())
                {
                    siteName=siteList[0].name;
                }
                else
                {
                    siteName='No Site Name';
                }
                queryBuilder();
                loadWorkOrders(); 
                fetchAttachmentInAdvance();
            }
        }
        catch(Exception exp)
        {
            showWorkOrderSection=false;
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(errMsg);  
        }
    }
    /***
* @Desc: Load work orders on Search Criteria
* @Invoke Method 
*     searchCriteriaValidation();
queryBuilderForSearchCriteria();
loadWorkOrders(); 
* @param : None
* @return :void
**/
    public void loadWorkOrdersOnSearch()
    {
        try{
            searchCriteriaValidation();
            queryBuilderForSearchCriteria();
            loadWorkOrders(); 
            fetchAttachmentInAdvance();
        }
        catch(Exception exp)
        {
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(errMsg);
        }
        
    }
    
    
    /***
* @Desc: To pick the list of ERP Status and set into SelectOption List
* @param : None
* @return :void
**/
    
    private  void erpStatusPickList()
    {      
        selOptERP=new List<SelectOption>();
        selOptERP.add(new SelectOption(blankLiteral,''));
        Schema.DescribeFieldResult fieldResult =  SVMXC__Service_Order__c.ERP_Priority__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            selOptERP.add(new SelectOption(f.getLabel(), f.getValue()));
        } 
    }
    
    
    /***
* @Desc: query Builder for Class Constructor Load work orders on Search Criteria
* @param : None
* @return :void
**/
    
    @TestVisible private  void queryBuilder()
    {
        String queryOnLoad='select Id, name, Service_Team__r.SVMXC__Group_Code__c, SVMXC__Scheduled_Date_Time__c, SVMXC__Top_Level__r.name,'+
            ' SVMXC__Purpose_of_Visit__c,SVMXC__Site__r.SVMXC__Account__r.Name, Machine_release_time__c, SVMXC__Group_Member__r.name, '+
            ' SVMXC__Order_Status__c , recordType.name'+
            ' from SVMXC__Service_Order__c'+
            ' where  SVMXC__Site__c='+'\''+siteID+'\''+' And  recordType.name !=\'Helpdesk\'';
        queryWorkOrder=queryOnLoad;
    }
    
    /***
* @Desc: Validator for search Input
* @param : None
* @return :void
**/
    
    @TestVisible private void searchCriteriaValidation()
    {
        if((String.isBlank(siteName) && String.isBlank(searchByTopLevel) && String.isBlank(searchByCompLevel) && String.isBlank(searchByCaseNo) && String.isBlank(searchByTech) && (String.isBlank(searchByERP) || searchByERP.equalsIgnoreCase(blankLiteral)) && (String.isBlank(searchByTeam)))   
           || (( searchFromDate.SVMXC__Scheduled_Date_Time__c==null)||(searchToDate.SVMXC__Scheduled_Date_Time__c==null) ))
        {  showWorkOrderSection=false;
         throw new SiteWiseWorkOrderException('[Minimum of 1 selection criteria should be entered in addition to Scheduled Date Range Start and End]'); 
        }       
    }
    
    /***
* @Desc: query builder for search criteria
* @param : None
* @return :void
* @todo: query update to include creation date
**/
    @TestVisible private  void queryBuilderForSearchCriteria()
    {
        String queryOnSearch='select Id, name, Service_Team__r.SVMXC__Group_Code__c, SVMXC__Scheduled_Date_Time__c, SVMXC__Top_Level__r.name,'+
            ' SVMXC__Purpose_of_Visit__c,SVMXC__Site__r.SVMXC__Account__r.Name, Machine_release_time__c, SVMXC__Group_Member__r.name, '+
            ' SVMXC__Order_Status__c , recordType.name'+
            ' from SVMXC__Service_Order__c'+
            ' where  recordType.name !=\'Helpdesk\'';
        if(String.isNotEmpty(siteName))
        {
            String getSiteID='select id, name from SVMXC__Site__c where name like\''+siteName+'%\'Limit 1';
            List<SVMXC__Site__c> siteList=Database.query(getSiteID);
            if(!siteList.isEmpty())
            {
                siteID=siteList[0].ID;
                siteName=siteList[0].name;
                queryOnSearch= queryOnSearch + ' AND SVMXC__Site__c= \''+siteID+'\'';
            }
            else
            {
                showWorkOrderSection=false;
                showAttachmentSection=false;
                showContactAndMailSection=false;
                throw new SiteWiseWorkOrderException('There is no site ID exist with given name. Please refine your search');
            }
        }
        
        if(String.isNotEmpty(searchByTopLevel))
        { 
            queryOnSearch= queryOnSearch + ' AND SVMXC__Top_Level__r.name like \'%'+searchByTopLevel+'%\'';
        }
        if(String.isNotEmpty(searchByCompLevel))
        {
            queryOnSearch= queryOnSearch + ' AND SVMXC__Component__r.name like \'%'+searchByCompLevel+'%\'';
        }
        if(String.isNotEmpty(searchByCaseNo))
        {
            queryOnSearch= queryOnSearch + ' AND SVMXC__Case__r.CaseNumber like \'%'+searchByCaseNo+'%\'';
        }
        if(String.isNotEmpty(searchByTech))
        {
            queryOnSearch= queryOnSearch + ' AND SVMXC__Group_Member__r.name like \'%'+searchByTech+'%\'';
        }
        if(String.isNotEmpty(searchByTeam))
        {
            queryOnSearch= queryOnSearch + ' AND Service_Team__r.SVMXC__Group_Code__c like \'%'+searchByTeam+'%\'';
        }
        if(String.isNotEmpty(searchByERP) && !searchByERP.equalsIgnoreCase(blankLiteral))
        {
            queryOnSearch= queryOnSearch + ' AND ERP_Priority__c = \''+searchByERP+'\'';
        }
        if( (searchFromDate.SVMXC__Scheduled_Date_Time__c !=null) && (searchToDate.SVMXC__Scheduled_Date_Time__c!=null))
        {
            queryOnSearch= queryOnSearch + ' AND SVMXC__Scheduled_Date_Time__c > ' +searchFromDate.SVMXC__Scheduled_Date_Time__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + ' AND SVMXC__Scheduled_Date_Time__c  < ' +searchToDate.SVMXC__Scheduled_Date_Time__c.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') ;
        } 
        queryWorkOrder=queryOnSearch;
        
    }
    
    /***
* @Desc: Loads workorder on constructor
* Set the visibilty of VF Page Section Accordingly
* @param : None
* @return :void
**/    
    @TestVisible private void loadWorkOrders()
    {
        
        listWorkOrderTemp=Database.query(queryWorkOrder);
        if(listWorkOrderTemp==null || listWorkOrderTemp.isEmpty())
        {
            showWorkOrderSection=false;
            showAttachmentSection=false;
            showContactAndMailSection=false;
            throw new SiteWiseWorkOrderException('No Work order found for Search Criteria. Please refine your Search');
        }
        else
        {
            if(listWorkOrderTemp.size()>woDisplayLimit)
            {
                showWorkOrderSection=false;
                showAttachmentSection=false;
                showContactAndMailSection=false;
                throw new SiteWiseWorkOrderException('Search Limit of 50 Work Orders exceeded.  Please refine search and try again');
            }
            else{ 
                showWorkOrderSection=true;
                listWorkOrderWrapper=new List<WorkOrderWrapper>();
                for(SVMXC__Service_Order__c tWorkOrder: listWorkOrderTemp)
                {
                    WorkOrderWrapper workOrderWrap=new WorkOrderWrapper(false,tWorkOrder,0);
                    listWorkOrderWrapper.add(workOrderWrap);
                    
                }
            }
        }
    }    
    
    
    /***
* @Desc: Fetch Attachment of Workorders before being displayed.
* Required to have the attachment count.
* It Saves another data call to have the count
* @param : None
* @return :void
**/    
    @TestVisible  private void fetchAttachmentInAdvance()
    {
        List<Attachment>  listAtch;
        for(WorkOrderWrapper tWorkOrderWrapper: listWorkOrderWrapper)
        {
            listAtch=[select Id, Name,LastModifiedBy.Name, LastModifiedDate, bodyLength from Attachment where parentId = : tWorkOrderWrapper.objWO.id];
            if(!listAtch.isEmpty())
            {
                mapAtchWrap.put(tWorkOrderWrapper.objWO.name,listAtch);
                tWorkOrderWrapper.countAttach=listAtch.size();
            }
            else
            {
                mapAtchWrap.put(tWorkOrderWrapper.objWO.name,null);
                tWorkOrderWrapper.countAttach=0;
            }
        }
    }    
    
    
    /***
* @Desc: Coded to handle the Attachment and Contacts related to selected work order
* Invokes  showAttachmentOFWO();
showContactsOFSite();
* @param : None
* @return :void
**/   
    public void showAtchmntANDContactOfWorkOrder()
    {
        try{
            showAttachmentOFWO();
            showContactsOFSite();
        }
        catch(Exception exp)
        {
            showAttachmentSection=false;
            showContactAndMailSection=false;
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(errMsg); 
        }
        
    }
    /***
* @Desc: Coded to handle the Attachment display
* Set the VF section visibilty
* @param : None
* @return :void
**/    
    @TestVisible    private void showAttachmentOFWO()
    {
        listAttachmentWrapper=new List<AttachmentWrapper>();
        Integer countSelected=0;
        for(WorkOrderWrapper wrapWorkOrder: listWorkOrderWrapper)
        {
            if(wrapWorkOrder.isSelected)
            {
                countSelected++;
                List<Attachment> listTempAttach=(List<Attachment>)mapAtchWrap.get(wrapWorkOrder.objWO.name);
                if(listTempAttach!=null && !listTempAttach.isEmpty())
                {
                    for(Attachment atcTemp:listTempAttach)
                    {
                        AttachmentWrapper wrapAttach=new AttachmentWrapper(false,atcTemp,wrapWorkOrder.objWO.name);
                        listAttachmentWrapper.add(wrapAttach);
                        
                    }
                }
            }
        }
        if(countSelected==0)
        {
            Throw new SiteWiseWorkOrderException('Please select atleast one work order to proceed');
        }
        if(listAttachmentWrapper.isEmpty())
        {
            Throw new SiteWiseWorkOrderException('No Attachments for selected Work Orders');
            
        }
        showAttachmentSection=true;
        showContactAndMailSection=true;
        
    }    
    
    /***
* @Desc: Coded to handle the Contact display
* Set the VF section visibilty
* @param : None
* @return :void
**/  
    public void showContactsOFSite()
    {
        listContactWrapper=new List<ContactWrapper>();
        listContactRoleWrapper=new List<ContactRoleWrapper>();
        list<SVMXC__Site__c>  siteList=[select name, SVMXC__Account__c from SVMXC__Site__c where id=:siteID];
        if(siteList.isEmpty()){
            throw new SiteWiseWorkOrderException('Data Mismatch::: SiteList should have data on input id ::'+siteID);
        }else{
            SVMXC__Site__c site  = siteList[0];
            ID idAccount=site.SVMXC__Account__c;
            for(Contact objCon : [select Id, Name,Functional_Role__c,Email from Contact where AccountId != null and Active_Contact__c = : idAccount and Email != null order by name]){
                listContactWrapper.add(new ContactWrapper(false,objCon));
            }
            //STSK0013367:  Build Contact Role List for Account
            for(Contact_Role_Association__c objCra : [select Id,Contact__r.Name,Contact_Email__c,Functional_Role__c from Contact_Role_Association__c where Account__c != null and Account__c = : idAccount and Contact_Email__c != null order by Contact__r.Name]){
            	listContactRoleWrapper.add(new ContactRoleWrapper(false,objCra));
        	}
            //STSK0013367:  Adjust empty check to be for both Contact and Contact Role
            if(listContactWrapper.isEmpty() && listContactRoleWrapper.isEmpty()){
                throw new SiteWiseWorkOrderException('No Contacts Found for Site  ::'+siteID);
            }
        }
        
    }
    
    /***
* @Desc: Coded to handle the attachment merge and mail it
* invokes
*       validateInput();
mergeAndMail();
* @param : None
* @return :void
**/  
    public PageReference mergeAtchmentsAndMail()
    {
        try{
            sendTo = new List<String>();
            prameters = '';
            validateInput();
            mergeAndMail();
            return null;
        }
        catch(Exception exp)
        {
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
        return new PageReference('/'+siteID);  
    }
    
    /***
* @Desc: Coded to handle the mailing the selected attachments
* invokes
*       validateInput();
mail();
* @param : None
* @return :void
**/ 
    
    public PageReference attachmentsMail()
    {
        try{
            sendTo = new List<String>();
            prameters = '';
            validateInput();
            mail();
            return null;
        }
        catch(Exception exp)
        {
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
        return new PageReference('/'+siteID);   
    }
    
    /***
* @Desc: Coded to handle the validation input for email options mailing the selected attachments
* @param : None
* @return :void
* 
**/ 
    
    @TestVisible    private void validateInput(){
        
        for(AttachmentWrapper awt : listAttachmentWrapper){
            if(awt.isSelected){
                prameters = prameters+awt.objAtch.Id + '|';
            }
        }
        if(String.isBlank(prameters)){
            throw new SiteWiseWorkOrderException('Please select attachments');
        }
        for(ContactWrapper cwt : listContactWrapper){
            if(cwt.isSelected){
                sendTo.add(cwt.objCnt.Email);
            }
        }
        //STSK0013367:  Add Contact Role Association selections to sendTo list.
        for(ContactRoleWrapper cra : listContactRoleWrapper){
            if(cra.isSelected){
                sendTo.add(cra.objContRole.Contact_Email__c);
            }
        }
        if(String.isNotBlank(addMailRecepients))
        {
            //STSK0012811 -Start
            List<String> tempStr=addMailRecepients.split('[;,]');
            //STSK0012811 --End
            for(String str:tempStr)
            {
                sendTo.add(str);
            }
        }
        if(sendTo.isEmpty()){
            throw new SiteWiseWorkOrderException('Please select at least one Contact or enter at least one Additional Email');
        }
        if(String.isBlank(emailSub)){
            throw new SiteWiseWorkOrderException('Please enter email Subject');
        }
        if(String.isBlank(emailBody)){
            throw new SiteWiseWorkOrderException('Please enter email body');
        }
        // Start ---STSK0013183
        else
        {
            appendSignature();
        }   
        
        
    }// End ---STSK0013183
    
    /***
* @Desc: Coded to append User Signature in the Email
@Task: STSK0013183
* @param : None
* @return :void
* 
**/ 
    
    @TestVisible  private void appendSignature()
    {
        
        User userInfoDB=[Select Title,Address From User where id =:UserInfo.getUserId() LIMIT 1];
        emailBody=emailBody+'<br>'+UserInfo.getName()+'<br>' ;
        if(String.isNotBlank( userInfoDB.Title))
        {
            emailBody=emailBody+userInfoDB.Title+'<br>';
        }
        if(String.isNotBlank(userInfo.getOrganizationName()))
            
        {
            emailBody=emailBody+userInfo.getOrganizationName()+'<br>';
        }
        if(userInfoDB.Address!=null)
        {
            emailBody=emailBody+userInfoDB.Address.getCity()+', '+userInfoDB.Address.getState()+', '+userInfoDB.Address.getCountry()+'<br>';
        }
        emailBody=emailBody+UserInfo.getUserEmail()+'<br>';
        emailBody=emailBody+'<a href="https://www.varian.com">varian.com</a><br>';
        Document doc=[select id from document where name ='Email Varian Logo' Limit 1];
        String imgServerPath='/servlet/servlet.ImageServer?id='+doc.ID+'&oid='+UserInfo.getOrganizationId();
        String imgURL='https://'+ApexPages.currentPage().getHeaders().get('Host').replace('.visual.', '.content.')+imgServerPath;
        emailBody=emailBody+'<img src="'+imgURL+'" /><br>';       
        emailBody=emailBody+'This message may contain information that is confidential or otherwise protected from disclosure. If you are not the intended recipient, you are hereby notified that any use, disclosure, dissemination, distribution, or copying of this message, or any attachments, is strictly prohibited. If you have received this message in error, please advise the sender by reply e-mail, and delete the message and any attachments.';       
        
    }
    /***
* @Desc: Coded to handle the merging of selected items and mail it as single 
* attachment 
* @param : None
* @return :void
*
**/ 
    @TestVisible   private void mergeAndMail(){
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(sendTo);
        //STSK0013191-- Start
        //STSK0013526 Start
        if(emailCC == TRUE){
            mail.setCcAddresses(ccAddresses);
        }
        //STSK0013526 End
        //STSK0013191 --End
        mail.setSenderDisplayName(UserInfo.getUserName());
        mail.setSubject(emailSub);
        mail.setHtmlBody(emailBody);
        List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
        HttpRequest mergeRequest = new HttpRequest();
        String endpointURL = Label.Heroku_App_URL;
        prameters = prameters.substring(0,prameters.length()-1);
        prameters = prameters+'.pdf';
        prameters = EncodingUtil.urlEncode(prameters, 'UTF-8');
        endpointURL = endpointURL+prameters;
        mergeRequest.setEndpoint(endpointURL);
        mergeRequest.setMethod('GET');
        http h = new Http();
        HttpResponse mergeResponse;
        if(!Test.isRunningTest()) {
            mergeResponse = h.send(mergeRequest);
        }
        else {
            mergeResponse = new HttpResponse();
            mergeResponse.setStatusCode(200); 
            mergeResponse.setBody('{"result":"test"}');
        }
        
        Blob data = Blob.valueof(mergeResponse.getBody());
        if(data.size()>fileAttachLimit)
        {
            throw new SiteWiseWorkOrderException('Email Attachment size exceeds limit of 25MB.  Please reduce number of attachments and try again.' );
        }
        Messaging.Emailfileattachment eAttachment = new Messaging.Emailfileattachment();
        String reportName='Merged Attachments_'+System.Today()+'.pdf';
        eAttachment.setFileName( reportName); 
        eAttachment.setBody(mergeResponse.getBodyAsBlob());
        efaList.add(eAttachment);
        mail.setFileAttachments(efaList );
        mails.add(mail);
        Messaging.sendEmail(mails);
        Task activityHistory = new Task( WhatId= siteID , Subject = mail.subject, Description ='Mail Body::  '+emailBody +'\n Mail Recipients ::Attachment mailed to  '+sendTo, ActivityDate = System.Today(), Status='Completed');
        Database.insert(activityHistory);
        Attachment atchMerged=new Attachment(ParentId=activityHistory.id, Name=reportName, body=mergeResponse.getBodyAsBlob());
        Database.insert(atchMerged);
        ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.Info,'Combined Attachments mailed Successfully to ::'+sendTo);
        ApexPages.addMessage(successMsg);
    }
    
    /***
* @Desc: Coded to handle the attachments mailing 
* @param : None
* @return :void
* @TODO: Need to move to Client side. Discussion Level
**/ 
    
    @TestVisible private void mail(){
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(sendTo);
        //STSK0013191-- Start
        //STSK0013526 Start
        if(emailCC == TRUE){
             mail.setCcAddresses(ccAddresses);
        }
        //STSK0013526 End
        //STSK0013191 --End
        mail.setSenderDisplayName(UserInfo.getUserName());
        mail.setSubject(emailSub );
        mail.setHtmlBody(emailBody);
        List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
        Double mailSizeLimit=0;
        list<Attachment> taskAttachment=new List<Attachment>();
        for(AttachmentWrapper awt : listAttachmentWrapper){
            if(awt.isSelected){ 
                mailSizeLimit=mailSizeLimit+awt.objAtch.BodyLength;
                if(mailSizeLimit>fileAttachLimit)
                {
                    throw new SiteWiseWorkOrderException('Email Attachment size exceeds limit of 25MB.  Please reduce number of attachments and try again.' );
                }
                
                list<Attachment> aaObjList = [select Name, body from Attachment where id =:awt.objAtch.Id limit 1];
                if(!aaObjList.isEmpty()){
                    Messaging.Emailfileattachment eAttachment = new Messaging.Emailfileattachment();
                    eAttachment.setFileName( aaObjList[0].name);
                    eAttachment.setBody(aaObjList[0].body);
                    efaList.add(eAttachment);
                    taskAttachment.add(aaObjList[0]);
                    
                }
            }
        }
        mail.setFileAttachments(efaList );
        mails.add(mail);
        Task activityHistory = new Task( WhatId= siteID , Subject = mail.subject, Description ='Mail Body::  '+emailBody +'\n Mail Recipients ::Reports mailed to  '+sendTo, ActivityDate = System.Today(), Status='Completed');
        Database.insert(activityHistory);
        //STSK0012811 -Start
        List<Attachment> listToDML=new List<Attachment>();
        for(Attachment atch :taskAttachment)
        {
            Attachment atchForTask=new Attachment(ParentId=activityHistory.id,Name=atch.Name, body=atch.Body);
            listToDML.add(atchForTask);
        }
        insert listToDML;
        
        //STSK0012811 -End
        Messaging.sendEmail(mails);
        ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.Info,'Attachments mailed Successfully to ::'+sendTo);
        ApexPages.addMessage(successMsg);
        
    }
    
    
    /***
* @Desc: Coded to merge the attachments and save under Activity History
* @param : None
* @return :pageReference
* @task :STSK0012811
*
**/ 
    
    public pageReference mergeAndDownloadAttachments()
    {
        String prameters='';
        Attachment atchMerged;
        try
        {
            for(AttachmentWrapper awt : listAttachmentWrapper){
                if(awt.isSelected){
                    prameters = prameters+awt.objAtch.Id + '|';
                }
            }
            if(String.isBlank(prameters)){
                throw new SiteWiseWorkOrderException('Please select attachments');
            }
            HttpRequest mergeRequest = new HttpRequest();
            String endpointURL = Label.Heroku_App_URL;
            prameters = prameters.substring(0,prameters.length()-1);
            prameters = prameters+'.pdf';
            prameters = EncodingUtil.urlEncode(prameters, 'UTF-8');
            endpointURL = endpointURL+prameters;
            mergeRequest.setEndpoint(endpointURL);
            mergeRequest.setMethod('GET');
            http h = new Http();
            HttpResponse mergeResponse;
            if(!Test.isRunningTest()) {
                mergeResponse = h.send(mergeRequest);
            }
            else {
                mergeResponse = new HttpResponse();
                mergeResponse.setStatusCode(200); 
                mergeResponse.setBody('{"result":"test"}');
            }
            Blob data = Blob.valueof(mergeResponse.getBody());
            if(data.size()>fileAttachLimit)
            {
                throw new SiteWiseWorkOrderException('Attachment size exceeds limit of 25MB.  Please reduce number of attachments and try again.' );
            }
            
            atchMerged=new Attachment(ParentId=siteID, Name='Merged Attachments_'+System.Today()+'.pdf', body=mergeResponse.getBodyAsBlob());
            Database.insert(atchMerged);
            return new PageReference('/servlet/servlet.FileDownload?file='+atchMerged.id); 
        }
        catch(Exception exp)
        {
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
        
        
    }
    
    /***
* @Desc: Back Button click Logic
* @param : None
* @return :pageReference
*
**/ 
    
    public PageReference back(){
        
        return new PageReference('/'+siteID);
    }
    
    /****************************************WRAPPER CLASSES************************************************************************/
    
    
    
    @TestVisible public class ContactWrapper
    {
        public Contact objCnt{get;set;}
        public Boolean isSelected{get;set;}
        public ContactWrapper()
        {
            //Do Nothing
        }
        
        public ContactWrapper(Boolean flagP, Contact cntP)
        {
            isSelected=flagP;
            objCnt=cntP;
        }
        
    }
    
    //STSK0013367:  Create Contact Role Wrapper class
    @TestVisible public class ContactRoleWrapper
    {
        public Contact_Role_Association__c objContRole{get;set;}
        public Boolean isSelected{get;set;}
        public ContactRoleWrapper()
        {
            //Do Nothing
        }
        
        public ContactRoleWrapper(Boolean flagP, Contact_Role_Association__c contRoleP)
        {
            isSelected=flagP;
            objContRole=contRoleP;
        }
        
    }
    
    @TestVisible public class WorkOrderWrapper
    {
        public SVMXC__Service_Order__c objWO{get;set;}
        public Boolean isSelected{get;set;}
        public Integer countAttach{get;set;}
        public WorkOrderWrapper()
        {
            //Do Nothing  
        }
        public WorkOrderWrapper(Boolean flag,SVMXC__Service_Order__c svmxObj , Integer count)
        {
            isSelected=flag;
            objWO=svmxObj;
            countAttach=count;
        }
        
    }
    
    public class AttachmentWrapper{
        public Attachment objAtch{get;set;}
        public Boolean isSelected{get;set;}
        public String woNo{get;set;}
        public AttachmentWrapper()
        {
            //Do Nothing
        }
        public AttachmentWrapper(Boolean flag, Attachment atch, String name)
        {
            isSelected=false;
            objAtch=atch;
            woNo=name;
        }
        
        
    }    
    
    public class SiteWiseWorkOrderException extends Exception{}
    
    
}
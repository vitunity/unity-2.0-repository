@isTest
public with sharing  class SiteWiseWorkOrderTest {
    
    public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent1 = new SVMXC__Installed_Product__c();
    public static Product2 objProd;
    public static List<Attachment> listAttach=new List<Attachment>();
    public static List<SVMXC__Service_Order__c> listWorkOrder=new List<SVMXC__Service_Order__c>();
    public static List<Contact> listContact=new List<Contact>();
    public static List<Contact_Role_Association__c> listContactRole=new List<Contact_Role_Association__c>();
    
    
    
    
    Static {
        
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        Profile profile = [Select id from Profile where name = 'VMS Service - User'];
        
        Account newAcc = new Account();
        newAcc.Name = 'My Varian Test Account';
        newAcc.BillingCountry='USA';
        newAcc.Country__c= 'USA';
        newAcc.BillingCity= 'Los Angeles';
        newAcc.BillingStreet ='3333 W 2nd St';
        newAcc.BillingState ='CA';
        newAcc.BillingPostalCode = '90020';
        newAcc.Agreement_Type__c = 'Master Pricing';
        newAcc.OMNI_Address1__c ='3333 W 2nd St';
        newAcc.OMNI_City__c ='Los Angeles';
        newAcc.OMNI_State__c ='CA';
        newAcc.OMNI_Country__c = 'USA';  
        newAcc.OMNI_Postal_Code__c = '90020';
        insert newAcc;
        
        
        newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        insert newLoc;
        
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
        
        List<ERP_NWA__c> NWALIst = new List<ERP_NWA__c>();
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        nwa.ERP_Status__c = 'CLSD';
        //insert NWA;
        
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        cdf.Name ='Default';
        insert cdf;
        
        WO = new SVMXC__Service_Order__c();
        WO.SVMXC__Case__c = newCase.id;
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Open';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.SVMXC__Group_Member__c=technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO.SVMXC__Product__c = newProd.ID;
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Subject__c= 'bbb';
        WO.SVMXC__Site__c=newLoc.id;
        WO.Sales_Order_Item__c = SOI.id;
        insert WO;
        listWorkOrder.add(WO);
        
       
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment1'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=WO.id;
        insert attach;
        listAttach.add(attach);
        
        attach=new Attachment(); 
        attach.Name='Unit Test Attachment2'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob1; 
        attach.parentId=WO.id;
        insert attach;
        listAttach.add(attach);
        
        Contact newCont = new Contact();
        newCont.FirstName = 'Test2';
        newCont.LastName = 'Contact2';
        newCont.Email = 'Test2@email.com';
        newCont.AccountId = newAcc.Id;
        insert newCont;
        listContact.add(newCont);
        
        //Add Contact Role Association
        Contact_Role_Association__c newContactRole = new Contact_Role_Association__c();
        newContactRole.Contact__c = newCont.Id;
        newContactRole.Account__c = newAcc.Id;
        insert newContactRole;
        listContactRole.add(newContactRole);
            
    }
    
    @isTest public static  void withoutPageReference(){
        Test.StartTest();
        try{
            SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        }
        catch(Exception e)
            
        {
            System.assertEquals('The URL doesn\'t has required parameter [id]', e.getMessage());
        }
        
        Test.StopTest();
    }
    
    @isTest public static  void invalidSiteID() {  
        
        Test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id', '111111111111111111');
        SiteWiseWorkOrderController testClass;
        try{
            testClass = new SiteWiseWorkOrderController(); 
        }
        catch(Exception e)
        {
            System.assertEquals('No Site Name',testClass.siteName);
            System.assertEquals(false,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
        }
        Test.stopTest();
    }     
    
    
    @isTest public static void testSearchCriteriaValidation()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.siteName=null;
        testClass.searchByTopLevel=null;
        testClass.searchByCompLevel=null;
        testClass.searchByCaseNo=null;
        testClass.searchByTech=null;
        testClass.searchByERP=null;
        testClass.searchFromDate.SVMXC__Scheduled_Date_Time__c=null;
        testclass.searchToDate.SVMXC__Scheduled_Date_Time__c=null;
        testClass.showWorkOrderSection=false;
        testClass.showAttachmentSection=false;
        testClass.showContactAndMailSection=false;
        try{
            testClass.searchCriteriaValidation();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('[Minimum of 1 selection criteria should be entered '))  ;   
            System.assertEquals(false,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
        }
        
        Test.StopTest();
            
    }  
    
    @isTest public static void testQueryBuilderForSearchCriteria()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.siteName=newLoc.Name;
        testClass.searchByTopLevel='TopLevel';
        testClass.searchByCompLevel='CompLevel';
        testClass.searchByCaseNo='CaseNo';
        testClass.searchByTech='Deepak';
        testClass.searchByERP='ERP Selc';
        testClass.searchFromDate.SVMXC__Scheduled_Date_Time__c=DateTime.newInstance(2017,11,13,0,0,0);
        testclass.searchToDate.SVMXC__Scheduled_Date_Time__c=DateTime.newInstance(2017,11,13,0,0,0);
        try{
            testClass.queryBuilderForSearchCriteria();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND SVMXC__Site__c= '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND SVMXC__Top_Level__r.name like '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND SVMXC__Component__r.name like '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND SVMXC__Case__r.CaseNumber like '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND SVMXC__Group_Member__r.name like '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains('AND ERP_Priority__c '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains(' AND SVMXC__Scheduled_Date_Time__c > '))  ;   
            System.assertEquals(true,testClass.queryWorkOrder.Contains(' AND SVMXC__Scheduled_Date_Time__c  <'))  ; 
            System.assertEquals(false,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
        }
        
        Test.StopTest();
            
    }  
    
    
    
    @isTest public static void testLoadWorkOrders()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        
        try{
            testClass.queryBuilder();
            testClass.loadWorkOrders();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,testClass.listWorkOrderWrapper.size()>1);
            System.assertEquals(true,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
        }
        
        System.Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id','InvaidID');
        testClass=new SiteWiseWorkOrderController();
        testClass.showWorkOrderSection=false;
        testClass.showAttachmentSection=false;
        testClass.showContactAndMailSection=false;
        
        try{
            testClass.queryBuilder();
            testClass.loadWorkOrders();
            
        }
        catch(Exception e)
        {
            System.assertEquals(false,e.getMessage().Contains('No Work order found for Search Criteria. Please refine your Search'))  ;   
            System.assertEquals(null,testClass.listWorkOrderWrapper);
            System.assertEquals(false,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
        }
        
        Test.StopTest();
           
    }  
    
    
    
    @isTest public static void testFetchAttachmentInAdvance()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.queryWorkOrder='select Id, name, SVMXC__Previous_Scheduled_Date_Time__c, SVMXC__Top_Level__r.name,'+
            ' SVMXC__Purpose_of_Visit__c,SVMXC__Site__r.SVMXC__Account__r.Name, Machine_release_time__c, SVMXC__Group_Member__r.name, '+
            ' SVMXC__Order_Status__c , recordType.name'+
            ' from SVMXC__Service_Order__c'+
            ' where  SVMXC__Site__c= \''+newLoc.id+'\'';
        
        try{
            
            testClass.loadWorkOrders();
            testClass.FetchAttachmentInAdvance();
            
        }
        catch(Exception e)
        {
            System.assertEquals(1,testClass.listWorkOrderTemp.size())  ; 
            System.assertEquals(1,testClass.listWorkOrderWrapper.size())  ; 
            System.assertEquals(1,testClass.mapAtchWrap.size());
            System.assertEquals(true,testClass.showWorkOrderSection);
            System.assertEquals(false,testClass.showAttachmentSection);
            System.assertEquals(false,testClass.showContactAndMailSection);
            
        }
        
        Test.StopTest();
    } 
    
    @isTest public static void testshowAttachmentOFWO()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        SiteWiseWorkOrderController.WorkOrderWrapper  wrap=new SiteWiseWorkOrderController.WorkOrderWrapper(false,WO,0) ;
        List<SiteWiseWorkOrderController.WorkOrderWrapper> wrapList=new List<SiteWiseWorkOrderController.WorkOrderWrapper>();
        wrapList.add(wrap);
        testClass.listWorkOrderWrapper=wrapList;
        try{
            testClass.showAttachmentOFWO();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select atleast one work order to proceed'))  ;   
            
        }
        
        testClass=new SiteWiseWorkOrderController();
        wrap=new SiteWiseWorkOrderController.WorkOrderWrapper(true,WO,0) ;
        wrapList=new List<SiteWiseWorkOrderController.WorkOrderWrapper>();
        wrapList.add(wrap);
        testClass.listWorkOrderWrapper=wrapList;
        try{
            
            testClass.showAttachmentOFWO();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('No Attachments for selected Work Orders'))  ;   
            
        }
        
        
        Test.StopTest();
    } 
    
    @isTest public static void testShowContactsOFSite()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id','invalidID');
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        try{
            testClass.showContactsOFSite();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Data Mismatch::: SiteList should have data on input id'))  ;   
            
        }
        
        System.currentPagereference().getParameters().put('id',newLoc.id);
        testClass=new SiteWiseWorkOrderController();
        try{
            testClass.showContactsOFSite();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,testClass.listContactWrapper.size()>0)  ;   
            
        }
        
        
        test.stopTest();
    }   
    
    @isTest public static void testValidateInput()     
    {
        Test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        testClass.listContactRoleWrapper=new List<SiteWiseWorkOrderController.ContactRoleWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp=new SiteWiseWorkOrderController.AttachmentWrapper();
            temp.isSelected=true;
            temp.objAtch=wrap;
            temp.wono='Name';
            testClass.listAttachmentWrapper.add(temp);
        }
        for(Contact cnt:listContact)
        {
            SiteWiseWorkOrderController.ContactWrapper temp=new SiteWiseWorkOrderController.ContactWrapper();
            temp.isSelected=true;
            temp.objCnt=cnt;
            testClass.listContactWrapper.add(temp);
        }
        //add contact role association to check
        for(Contact_Role_Association__c cra:listContactRole)
        {
            SiteWiseWorkOrderController.ContactRoleWrapper temp=new SiteWiseWorkOrderController.ContactRoleWrapper();
            temp.isSelected=true;
            temp.objContRole=cra;
            testClass.listContactRoleWrapper.add(temp);
        }
        testClass.addMailRecepients='Hello1@gmail.com';
        testClass.emailBody='EmailBody';
        testClass.emailSub='Email SUb';
        try{
            testClass.validateInput();
        }       catch(Exception e)
        {
            
            System.assertEquals(true,testClass.sendTo!=null);
            System.assertEquals(true,testClass.sendTo.size()>0);
            
        }
        
        test.stopTest();
    }   
    
    
    @isTest public static void testValidateInputFail1()    
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        testClass.listContactRoleWrapper=new List<SiteWiseWorkOrderController.ContactRoleWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp1=new SiteWiseWorkOrderController.AttachmentWrapper(false,wrap,'name');
            testClass.listAttachmentWrapper.add(temp1);
        }
        try{
            testClass.ValidateInput();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'))  ;  
            System.assertEquals(true, testClass.prameters=='');
            
            
        }
        Test.stopTest();
    }
    
    
    
    @isTest public static void testValidateInputFail2()     
    {
        Test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        testClass.listContactRoleWrapper=new List<SiteWiseWorkOrderController.ContactRoleWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp=new SiteWiseWorkOrderController.AttachmentWrapper();
            temp.isSelected=true;
            temp.objAtch=wrap;
            temp.wono='Name';
            testClass.listAttachmentWrapper.add(temp);
        }
        
        testClass.addMailRecepients='';
        try{
            testClass.validateInput();
        }
        
        catch(Exception e)
        {
            
            System.assertEquals(true, testClass.listContactWrapper.size()==0);
            System.assertEquals(true, testClass.listContactRoleWrapper.size()==0);
            System.assertEquals(false, String.isBlank(testClass.prameters));
            System.assertEquals(true,e.getMessage().Contains('Please select at least one Contact'));
            
        }
        test.stopTest();
    }
    @isTest public static void testValidateInputFail3()     
    {
        Test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        testClass.listContactRoleWrapper=new List<SiteWiseWorkOrderController.ContactRoleWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp=new SiteWiseWorkOrderController.AttachmentWrapper();
            temp.isSelected=true;
            temp.objAtch=wrap;
            temp.wono='Name';
            testClass.listAttachmentWrapper.add(temp);
        }
        for(Contact cnt:listContact)
        {
            SiteWiseWorkOrderController.ContactWrapper temp=new SiteWiseWorkOrderController.ContactWrapper();
            temp.isSelected=true;
            temp.objCnt=cnt;
            testClass.listContactWrapper.add(temp);
        }
        //add contact role association to check
        for(Contact_Role_Association__c cra:listContactRole)
        {
            SiteWiseWorkOrderController.ContactRoleWrapper temp=new SiteWiseWorkOrderController.ContactRoleWrapper();
            temp.isSelected=true;
            temp.objContRole=cra;
            testClass.listContactRoleWrapper.add(temp);
        }
        
        testClass.addMailRecepients='Hello@gmail.com';
        testClass.emailSub='';
        testClass.emailBody='';
        try{
            testClass.validateInput();
        }
        
        
        catch(Exception e)
        {
            
            System.assertEquals(true,String.isBlank(testClass.emailSub));
            System.assertEquals(true,e.getMessage().Contains('Please enter email Subject'));
            
        }
        test.stopTest();
    }
    
    
    @isTest public static void testValidateInputFail4()     
    {
        Test.starttest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        testClass.listContactRoleWrapper=new List<SiteWiseWorkOrderController.ContactRoleWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp=new SiteWiseWorkOrderController.AttachmentWrapper();
            temp.isSelected=true;
            temp.objAtch=wrap;
            temp.wono='Name';
            testClass.listAttachmentWrapper.add(temp);
        }
        for(Contact cnt:listContact)
        {
            SiteWiseWorkOrderController.ContactWrapper temp=new SiteWiseWorkOrderController.ContactWrapper();
            temp.isSelected=true;
            temp.objCnt=cnt;
            testClass.listContactWrapper.add(temp);
        }
        //add contact role association to check
        for(Contact_Role_Association__c cra:listContactRole)
        {
            SiteWiseWorkOrderController.ContactRoleWrapper temp=new SiteWiseWorkOrderController.ContactRoleWrapper();
            temp.isSelected=true;
            temp.objContRole=cra;
            testClass.listContactRoleWrapper.add(temp);
        }
            
        testClass.addMailRecepients='Mail@mail.com';
        testClass.emailSub='Email SUbject';
        testClass.emailBody='';
        try{
            testClass.validateInput();
        }catch(Exception e)
        {
            
            System.assertEquals(true,String.isBlank(testClass.emailBody));            
            System.assertEquals(true,e.getMessage().Contains('Please enter email body'));
            
            
        }
        Test.stopTest();
    }
    
    @isTest public static void testAppendSignature()
   {
       Test.StartTest();
       Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
       System.currentPagereference().getParameters().put('id',newLoc.id);
       SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
       testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        testClass.listContactWrapper=new List<SiteWiseWorkOrderController.ContactWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp=new SiteWiseWorkOrderController.AttachmentWrapper();
            temp.isSelected=true;
            temp.objAtch=wrap;
            temp.wono='Name';
            testClass.listAttachmentWrapper.add(temp);
        }
        for(Contact cnt:listContact)
        {
            SiteWiseWorkOrderController.ContactWrapper temp=new SiteWiseWorkOrderController.ContactWrapper();
            temp.isSelected=true;
            temp.objCnt=cnt;
            testClass.listContactWrapper.add(temp);
        }
        testClass.addMailRecepients='Hello1@gmail.com';
        testClass.emailBody='EmailBody';
        testClass.emailSub='Email SUb';
       try{    
       testClass.appendSignature();
       System.assert(true, testClass.emailBody.contains('<a href="https://www.varian.com">varian.com</a><br>'));
       System.assert(true, testClass.emailBody.contains('/servlet/servlet.ImageServer?id'));
       System.assert(true, testClass.emailBody.contains('<img src="'));
       System.assert(true, testClass.emailBody.contains('This message may contain information that'));
       }
           catch(Exception e)
           {
               
           }
       Test.stopTest();
           
   }
    
    @isTest public static void  testmergeAtchmentsAndMail()
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp1=new SiteWiseWorkOrderController.AttachmentWrapper(false,wrap,'name');
            testClass.listAttachmentWrapper.add(temp1);
        }
        try{
            testClass.mergeAtchmentsAndMail();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'))  ;   
            
            
        }
        
        test.stopTest();
        
    }
    
    @isTest public static void  testattachmentsMail()
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp1=new SiteWiseWorkOrderController.AttachmentWrapper(false,wrap,'name');
            testClass.listAttachmentWrapper.add(temp1);
        }
        try{
            testClass.attachmentsMail();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'))  ;   
            
            
        }
        
        test.stopTest();
        
    }
    
    
    @isTest public static  void testMail() {  
        
        Test.starttest();
        System.ApexPages.currentPage().getParameters().put('id', newLoc.Id);
        SiteWiseWorkOrderController testClass = new SiteWiseWorkOrderController(); 
        testClass.sendTo.add('testmail1@gmail.com');
        testClass.emailBody='Email Body';
        testClass.emailSub='Email Subject';
        List<SiteWiseWorkOrderController.AttachmentWrapper> tempList=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        for(integer i=0; i<listAttach.size();i++)
        {
            SiteWiseWorkOrderController.AttachmentWrapper wrap=new SiteWiseWorkOrderController.AttachmentWrapper(true,listAttach[i],'Name');
            tempList.add(wrap);
        }
        testClass.listAttachmentWrapper=tempList;
        testClass.mail(); 
        Test.stopTest();
    }
    
    
    @isTest public static  void testMergeAndMail() {  
        
        Test.starttest();
        System.ApexPages.currentPage().getParameters().put('id', newLoc.Id);
        SiteWiseWorkOrderController testClass = new SiteWiseWorkOrderController(); 
        testClass.sendTo.add('testmail1@gmail.com');
        testClass.emailBody='Email Body';
        testClass.emailSub='Email Subject';
        List<SiteWiseWorkOrderController.AttachmentWrapper> tempList=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        String temParameters;
        for(integer i=0; i<listAttach.size();i++)
        {
            SiteWiseWorkOrderController.AttachmentWrapper wrap=new SiteWiseWorkOrderController.AttachmentWrapper(true,listAttach[i],'Name');
            temParameters = temParameters+wrap.objAtch.Id + '|'; 
            tempList.add(wrap);
        }
        testClass.listAttachmentWrapper=tempList;
        testClass.prameters=temParameters;
        testClass.mergeAndMail(); 
        Test.stopTest();
    }
    
    @isTest public static void  testMergeAndDownloadAttachments()
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.listAttachmentWrapper=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        for(Attachment wrap: listAttach)
        {
            SiteWiseWorkOrderController.AttachmentWrapper temp1=new SiteWiseWorkOrderController.AttachmentWrapper(false,wrap,'name');
            testClass.listAttachmentWrapper.add(temp1);
        }
        try{
            testClass.mergeAndDownloadAttachments();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'))  ;   
            
            
        }
        
        test.stopTest();
        
    }
    
    @isTest public static void  testMergeAndDownloadAttachmentsPass()
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        List<SiteWiseWorkOrderController.AttachmentWrapper> tempList=new List<SiteWiseWorkOrderController.AttachmentWrapper>();
        String temParameters;
        for(integer i=0; i<listAttach.size();i++)
        {
            SiteWiseWorkOrderController.AttachmentWrapper wrap=new SiteWiseWorkOrderController.AttachmentWrapper(true,listAttach[i],'Name');
            temParameters = temParameters+wrap.objAtch.Id + '|'; 
            tempList.add(wrap);
        }
        testClass.listAttachmentWrapper=tempList;
        testClass.prameters=temParameters;
        testClass.mergeAndDownloadAttachments(); 
        test.stopTest();
        
    }
    
    
    @isTest public static void backWithPGRef()
    {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.SiteWiseWorkOrder')); 
        System.currentPagereference().getParameters().put('id',newLoc.id);
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.back();
        Test.StopTest();   
    }
    @isTest public static void backWithoutPGRef()
    {
        Test.StartTest();
        SiteWiseWorkOrderController testClass=new SiteWiseWorkOrderController();
        testClass.back();
        Test.StopTest();   
    }
    
}
/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 04-Apr-2013
    @ Description   : An Apex Class for displaying banners based on logged in User profile.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class SlideController {

    public list<banner> bannerList{get; set;}
    public Event_Webinar__c featWebinars{get;set;}      
    public String dspMsg{get;set;}
    public Boolean isGuest{get;set;}
    public String PONrecieve{get;set;}
    public String show{get;set;}
    public Boolean btnVsblity {get;set;}
    public String Complaint_Id{get;set;}
    public String logchk{get;set;}
    public String logchk1{get;set;}
    public String Firstlogin{get;set;}
    public String UserloginStatus{get;set;}
     public String Usrname{get;set;} 
     
    //For Password
      public String shows{get;set;}
    public String Newpass{get;set;}
    public String ConfirmPass{get;set;}
    public String CurrentPass{get;set;}
    public String CustRecovryQ {get; set;}
    public String CustRecoveryAns {get; set;}
    public string SapAcctName{get;set;}
    public user Un{get;set;}
    List<Id> accntId = new List<Id>();
    Id loginacc;
    List<Regulatory_Country__c> chkRegion= new List<Regulatory_Country__c>();
    Boolean ismadalcontact;
    
    public string selectedDocument{get;set;}
    public List<String> picklistvalue{get;set;}
    //public List<ContentVersion> lstContentVersion{get;set;}
    Map<Id, String> contentWorkSpace = new Map<Id, String>();
    
    // Method for recovery question list
    
      Public List<SelectOption> getrecoveryqusoptions()
    {
               list<SelectOption> recoveryqusoptions = new list<SelectOption>();
          // Get the object type of the SObject.
          Schema.sObjectType objType = Contact.getSObjectType(); 
          // Describe the SObject using its object type.
          Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values = fieldMap.get('Recovery_Question__c').getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          { 
             recoveryqusoptions.add(new SelectOption(a.getLabel(), a.getValue())); 
          }
          return recoveryqusoptions;
               
    } 
    
    // logout method
    
    public pagereference logoutmethod()
    {
    
     Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
     sessionmap = usersessionids__c.getall();
     HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        String strToken = label.oktatoken;
        String authorizationHeader = 'SSWS ' + strToken;
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');
        String endpointgroup = Label.oktaendpointsessionkill + sessionmap.get(userinfo.getuserid()).session_id__c;
        reqgroup.setEndPoint(endpointgroup);
        reqgroup.setMethod('DELETE');
        try{
        if(!Test.IsRunningTest()){
        resgroup = httpgroup.send(reqgroup);
        }
        }catch(System.CalloutException e){
          system.debug(resgroup.toString());
        } 
return new pagereference('/secur/logout.jsp'); 
    }
    
    
    
    public SlideController(StLoginCntlr controller) 
    {       
            ismadalcontact = false;
            isGuest =SlideController.logInUser();
            dspMsg = System.currentPageReference().getParameters().get('msage');
            logchk = System.currentPageReference().getParameters().get('dspMsg');
            logchk1 = System.currentPageReference().getParameters().get('dspMsg1');
            if(!isGuest)
            {
                //ismadalcontact =[Select Id, Contact.Is_MDADL__c From User where id = : Userinfo.getUserId() limit 1].Contact.Is_MDADL__c;         
                 String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
                if(conCntry != null){
                  chkRegion = ([Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry limit 1]);   
                }            
            }
            
            shows='none';
            List<User> UserCont = [select id,alias,usertype,ContactId from User where id = : Userinfo.getUserId() limit 1];
               if(UserCont.size() > 0)
               {  
          
          if(UserCont[0].ContactId == null)
          {
           if(UserCont[0].usertype != label.guestuserlicense){
              Usrname=UserCont[0].alias;
              shows='block';
              isGuest=false;
              }
          }
           } 
            fetchEvents();
            fetchBanners();
            fetchWebinars();
            
                                 
           
    }
    /**** Page Action Method for the MADAL Contact and for reset password for the user login in for the first time****/
    public pagereference MADALcontact()
    {
      ismadalcontact =[Select Id, Contact.Is_MDADL__c From User where id = : Userinfo.getUserId() limit 1].Contact.Is_MDADL__c;
      Boolean firsttimelog = false;
      if([Select LastLoginDate from user where id=:Userinfo.getUserId() limit 1].LastLoginDate == null)
      {
        firsttimelog = true;
      }
     /* if(firsttimelog)
       { 
         return new pagereference('/apex/CpMyAccount?Request=Reset');
        } else{*/ 
           if(ismadalcontact && [Select Contact.PasswordReset__c from user where id=:Userinfo.getUserId() limit 1].Contact.PasswordReset__c == true)
            {
             return new pagereference('/apex/cpMDADLLungPhantom'); 
            }
            else if([Select Id, Contact.Functional_Role__c from user where id=:Userinfo.getUserId() limit 1].Contact.Functional_Role__c != null && [Select Id, Contact.Functional_Role__c from user where id=:Userinfo.getUserId() limit 1].Contact.Functional_Role__c.startsWith('Account Payable'))
            {
             return new pagereference('/apex/CpCustomerBilling'); 
            }
            else 
            {
              return null;
            }
           // }            
    }
    
    
    public static Boolean logInUser(){
      List<User> UserCont = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];
      if(UserCont.size() > 0){  
          //if(UserInfo.getUserType() == 'CspLitePortal' || UserInfo.getUserType() == 'PowerCustomerSuccess'){
          if(UserCont[0].ContactId != null){
                  return false;
          }else{
                  return true;
          }
      
      }
      return false;
   }
   
   //class to store banner source url
    public class banner{
        public String imgSrc{get;set;}
        public BannerRepository__c  bannerStr{get;set;}
    
    }
    
    //class to store content and library
    public class content{
        public String library{get;set;}
        public ContentVersion cont{get;set;}
        public String selected {get; set;} 
        public String Institutionaffctd {get;set;}
    }
   
     //constructor
     public SlideController (){  
           
            isGuest =SlideController.logInUser();
            List <User> UsrLoginDetail=new List <User>();
            Date LogDetail;
           
            UsrLoginDetail=[SELECT Contact.ShowAccPopUp__c,Contact.DateOfAccMovePopUp__c from User where Id=:userinfo.getUserId() and Contact.ShowAccPopUp__c=false and contact.passwordreset__c=True];
                   if(UsrLoginDetail.size()>0) 
                   {
                   UserloginStatus='true';

                   }  
                          
            
            selectedDocument = 'Safety Notifications';
            Un = [Select u.Contact.RAQA_Contact__c, u.Contact.AccountID, ContactId,contact.firstname,contact.lastname,contact.MailingCity,
                   Contact.mailingState, Contact.Phone,contact.account.Legal_Name__c,
                   contact.MailingPostalcode,Contact.MailingStreet From User u where id =: userinfo.getUserId()];
            //btnVsblity = Un.Contact.RAQA_Contact__c;
             SapAcctName=Un.contact.account.Legal_Name__c;
                
                IF(SapAcctName!=null)
              {
               List<String> SapaccFirst=new  List<String>();
              SapaccFirst=SapAcctName.split('\r\n');
              if(SapaccFirst.size()>0)
              {
              SapAcctName=SapaccFirst[0];
              }
             
             }
            
            loginacc = Un.Contact.AccountID;
            for(Contact_Role_Association__c CR: [Select Account__c from Contact_Role_Association__c where Contact__c =: Un.ContactId and Role__c = 'RAQA'])
             accntId.add(CR.Account__c);
             if(accntId == null)
              btnVsblity = false;
             else
              btnVsblity = true;
            //accntId = Un.Contact.AccountID;
            if(!isGuest)
            {
                String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
                if(conCntry != null){
                  chkRegion = ([Select Portal_Regions__c, Name From Regulatory_Country__c where Name = :conCntry limit 1]);   
                }            
            }
            System.debug('Aebug  ; userType = ' + btnVsblity);
            System.debug('Aebug  ; isGuest = '+isGuest); 
            shows='none';
            List<User> UserCont = [select id,alias,usertype,ContactId from User where id = : Userinfo.getUserId() limit 1];
               if(UserCont.size() > 0)
               {  
          
          if(UserCont[0].ContactId == null)
          {
           if(UserCont[0].usertype != label.guestuserlicense){
              Usrname=UserCont[0].alias;
              shows='block';
              isGuest=false;
              }
          }
          
          ELSE
          {
            if([Select contact.PasswordReset__c from user where id = :Userinfo.getUserId() limit 1].contact.PasswordReset__c == false)
           {
                Firstlogin = 'true';
                
                system.debug('----FirstLogin-----' + Firstlogin);
             
           }
           } 
           }  
          
              
              fetchBanners();  
              fetchWebinars();  
              fetchEvents();
              fetchContentWorkspace();
               
            
     }
     public pagereference cancelresetpassword()
     {
       User userupdate = [Select ContactId,contact.passwordreset__c from user where id =:Userinfo.getUserId() limit 1];
       Contact con = new contact(id = userupdate.ContactId);
       con.PasswordReset__c = true;
       //update con;
       //return null;
       return new pagereference(Label.cp_site);//'https://sfdm-myvarian.cs7.force.com/');
     }
      public pagereference UpdateFlagFields1()
     {
       User userupdate = [Select ContactId,contact.passwordreset__c from user where id =:Userinfo.getUserId() limit 1];
       Contact con = new contact(id = userupdate.ContactId);
       con.ShowAccPopUp__c=True;
       con.DateOfAccMovePopUp__c=System.Today();
       update con;
       return null;
     }
      public pagereference UpdateFlagFields()
     {
       User userupdate = [Select ContactId,contact.passwordreset__c from user where id =:Userinfo.getUserId() limit 1];
       Contact con = new contact(id = userupdate.ContactId);
       con.ShowAccPopUp__c=True;
      // con.DateOfAccMovePopUp__c=System.Today();
       update con;
       return null;
     
     }
     // Method for reseting the password and recoveryquestion and answer
     public pagereference resetpassword()
     {
       String Recoveryquestion = ApexPages.currentPage().getParameters().get('RecoveryQuestionparam');
       String RecoveryAnswer = ApexPages.currentPage().getParameters().get('RecoveryAnswerparam');
       String NewPassword = ApexPages.currentPage().getParameters().get('NewPasswordparam');
       RecoveryAnswer = RecoveryAnswer.deleteWhitespace().toLowerCase();
       system.debug('---RecoveryQuestion' +'----' + CustRecovryQ + Recoveryquestion +'----Recovery Answer'+RecoveryAnswer + 'Password---'+NewPassword);
       User usercontact = [Select ContactId,Contact.Email,Contact.FirstName,Contact.LastName,Contact.Phone,Contact.OktaLogin__c,Contact.OktaId__c from user where id=: Userinfo.getUserId() limit 1];
       //http Request creation and capturing response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        
        //Construct Authorization and Content header
        String strToken = label.oktatoken;//'000nWLdNsk1RgZpTDbwPG_gO-vu9dWekpb59Qub8kK';//label.oktatoken;//'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //EndPoint Creation
        String EndPoint = label.oktachangepasswrdendpoint + usercontact.Contact.OktaId__c;
        req.setendpoint(EndPoint);
        req.setMethod('PUT');
         String body ='{"profile": {"firstName": "'+ usercontact.Contact.FirstName +'","lastName": "'+usercontact.Contact.LastName +'","email": "'+ usercontact.Contact.Email + '","login": "' + usercontact.Contact.OktaLogin__c + '"},"credentials": {"password" : { "value": "' + NewPassword + '" },"recovery_question": {"question": "'+ Recoveryquestion +'","answer": "'+ RecoveryAnswer + '"}}}';
         req.setBody(body);
         try {
           //Send endpoint to OKTA
           system.debug('aebug ; ' +req.getBody());
           If(!Test.IsRunningTest()){
           res = http.send(req);
           }ELSE
           {
           fakeresponse.fakeresponsemethod('activation');
           }
           if(res.getbody().contains('errorSummary'))
           {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Password provided doesnot match.');
            ApexPages.addMessage(myMsg);
            return null;
           }
           system.debug('aebug ; ' +res.getBody());
         }catch(System.CalloutException e) {
            System.debug(res.toString());
        }
        // Updating Contact Record
        Contact updatecon = new Contact(id = usercontact.ContactId);
        updatecon.Recovery_Question__c = Recoveryquestion;
        updatecon.Recovery_Answer__c = RecoveryAnswer;
        updatecon.PasswordReset__c = true;
        updatecon.activation_url__c = '';
        updatecon.PasswordresetDate__c = system.today();// to capture the last date when the password was reseted
        try{
        update updatecon;
        }  catch(Exception e)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Please contact support for the above mentioned error');
            ApexPages.addMessage(myMsg);
            return null;
        }
        pagereference p = new pagereference('/apex/CpHomePage');
        p.setredirect(true);
        return p;//new pagereference('/apex/CpHomePage');
     }
     
     
     private void fetchContentWorkspace(){
          /*  list<ContentWorkspaceDoc> contentWork = [Select c.ContentWorkspace.Name, c.ContentWorkspaceId, c.ContentDocumentId 
                                               From ContentWorkspaceDoc c];

            if(contentWork.size() > 0){
                for(ContentWorkspaceDoc cd : contentWork){
                    contentWorkSpace.put(cd.ContentDocumentId,cd.ContentWorkspace.Name);
                    System.debug('--->Contentid'+contentWorkSpace);
                }
            }*/
     }
     
     //fetch banners
     private void fetchBanners(){
        bannerList = new list<banner>();
        List<banner> tempbannerlist = new list<banner>();// list for keeping the anonymous banners
        system.debug('UID = '+UserInfo.getUserType()+', UName  ='+UserInfo.getUserName());
        system.debug('IsGuest = '+isGuest);
        Set<String> ProductGroupSet = CpProductPermissions.fetchProductGroup();
        // For internal user
        if(shows == 'block')
        {
          
        Schema.Describefieldresult fieldresult = Product2.Product_Group__c.getDescribe();
        List<Schema.Picklistentry> Pickvalue = fieldresult.getPicklistvalues();
        for(Schema.Picklistentry p : Pickvalue){
           ProductGroupSet.add(p.getvalue());
            //optionsList.add(new selectOption(p.getvalue(), p.getvalue()));
          }
        
        }
      // ends for internal user
      
        //check if portal user
        if(!isGuest){
                
                for(BannerRepository__c bn : [select id,Image__c, Link__c, Weight__c,CreatedDate,
                                                                                End_Date__c,Start_Date__c,Product_Affiliation__c, Region__c
                                                                   from BannerRepository__c where End_Date__c >= Today and Start_Date__c <=Today and recordtype.name = 'Banner'
                                                                   order by weight__c desc, Start_Date__c asc]){ //, Type__c desc]){
                            
                            System.debug('Aebug g : bn.Product_Affiliation__c = '+bn.Product_Affiliation__c);
                            if(bn.Image__c == null || (bn.Image__c != null && !bn.Image__c.contains('src'))){continue;}
                            if(bn.Product_Affiliation__c !=null && bn.Product_Affiliation__c != 'All'){
                                    Boolean prodExist = false;
                                    for(String pg : ProductGroupSet){
                                        if(bn.Product_Affiliation__c != null && bn.Product_Affiliation__c.contains(pg)){
                                                prodExist = true;
                                                break;
                                        }
                                    }
                                    
                                    if(!prodExist){continue;}
                            }
                            //Added by harshita
                             String S1 = bn.Image__c;
                             String S2;
                                System.debug(S1.substring(S1.indexof('src')+5));
         
                                List<String> ss = S1.substring(S1.indexof('src')+5).split('"');
         
                                if(ss.size()>0){
                                       system.debug(ss[0]);
                                       S2=ss[0];
                                }       
                            
                            
                            //End of code
                            /*
                            System.debug('aebug : bn.Image__c ='+bn.Image__c );
                            //String S2 = bn.Image__c.substring(bn.Image__c.indexof('src')+5,bn.Image__c.indexof('alt')-2); 
                            String S2 = bn.Image__c.substring(bn.Image__c.indexof('src')+5,bn.Image__c.indexof('src')+94); 
                            System.debug('aebug : S2 ='+S2 );
                            */
                                                       
                            system.debug('aebug : bn.Region__c == '+bn.Region__c);
                            if(bn.Region__c != null && (chkRegion != null || shows == 'block')){
                              if(shows != 'block'){
                                if(bn.Region__c.contains('All') || bn.Region__c.contains(chkRegion[0].Portal_Regions__c)){
                                    banner obj = new banner();
                                    obj.imgSrc = S2;
                                    obj.bannerStr = bn;
                                    if(bn.Product_Affiliation__c == null)
                                       tempbannerlist.add(obj);
                                    else
                                       bannerList.add(obj);  //personalized banner                              
                                }
                                }else {
                                 // for internal users
                                //if(bn.Region__c.contains('All') || bn.Region__c.contains(chkRegion[0].Portal_Regions__c)){
                                    banner obj = new banner();
                                    obj.imgSrc = S2;
                                    obj.bannerStr = bn;
                                    if(bn.Product_Affiliation__c == null)
                                       tempbannerlist.add(obj);
                                    else
                                       bannerList.add(obj);  //personalized banner                              
                                 // }
                                }
                            }
            
                 }
                 bannerList.addall(tempbannerlist);// for showing personilized banner first and then anonymous
                system.debug('aebug : bannerlist == '+bannerList);
                
        }else{
                 for(BannerRepository__c bn : [select id,Image__c, Link__c, Weight__c,CreatedDate,End_Date__c,Start_Date__c, Region__c
                       from BannerRepository__c where End_Date__c >= Today and Start_Date__c <=Today and recordtype.name = 'banner' and Product_Affiliation__c = null order by weight__c desc, Start_Date__c asc]){
                            
                            if(bn.Image__c == null || (bn.Image__c != null && !bn.Image__c.contains('src'))){continue;}
                            System.debug('aebug : bn.Image__c ='+bn.Image__c );
                            //String S2 = bn.Image__c.substring(bn.Image__c.indexof('src')+5,bn.Image__c.indexof('alt')-2);
                            String S2 = '';
                            System.debug('aebug ; bn.Image__c == '+bn.Image__c.containsIgnoreCase('https'));
                            //Added by harshita
                             String S1 = bn.Image__c;
                             //String S2;
                                System.debug(S1.substring(S1.indexof('src')+5));
         
                                List<String> ss = S1.substring(S1.indexof('src')+5).split('"');
         
                                if(ss.size()>0){
                                       system.debug(ss[0]);
                                       S2=ss[0];
                                }       
                            //End of code
                            /*if(bn.Image__c.containsIgnoreCase('https')){
                                S2 = bn.Image__c.substring(bn.Image__c.indexof('src')+5,bn.Image__c.indexof('src')+126);
                            }else{
                                S2 = bn.Image__c.substring(bn.Image__c.indexof('src')+5,bn.Image__c.indexof('src')+94);
                            }
                            */
                            System.debug('aebug : S2 ='+S2 );
                            
                            
                                banner obj = new banner();
                                obj.imgSrc = S2;
                                obj.bannerStr = bn;
                                bannerList.add(obj);
                            
                 }
                
        }
       
       
     
        
     }
     
     //fetch webinars
     private void fetchWebinars(){
            
            /*
            set<Id> conDocID = new set<Id>();
            for(ContentWorkspaceDoc conWorDoc :[Select c.ContentWorkspace.Name, c.ContentWorkspaceId, c.ContentDocumentId 
                                               From ContentWorkspaceDoc c where ContentWorkspace.Name = 'Webinars']){
            
                    conDocID.add(conWorDoc.ContentDocumentId);                                  
            }
            
            if(conDocID.size() > 0){
                featWebinars = [Select c.Webinar_Date__c, c.Speaker__c, c.IsLatest, c.Institution__c,
                                                c.Id, c.Description, c.ContentDocument.Id, c.ContentDocumentId 
                                                From ContentVersion c where c.ContentDocumentId in : conDocID and 
                                                c.IsLatest = true and
                                                c.Webinar_Date__c > TODAY limit 1];
                                                
                 System.debug('aebug : featWebinars ='+featWebinars );
            }
            */
            //RecordType rc = [Select id, Name from RecordType where name='Webinars' limit 1];
            if(isguest){
            for(Event_Webinar__c fw : [Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,e.Restrict_access_based_on_product_profile__c,
                                        Speaker__c , Title__c, CreatedDate, Time__c, URL__c
                                            From Event_Webinar__c e 
                                            where recordtype.name = 'Webinars' 
                                            and Featured__c = true and private__c = false and e.Restrict_access_based_on_product_profile__c = false and feathured_date__c != null/*and From_Date__c >= today */
                                            order by feathured_date__c desc limit 1]){
                                
                if(fw!= null){
                           featWebinars = fw;
                                                 
                         
                }
            }
        }else{
           for(Event_Webinar__c fw : [Select e.Location__c, e.Institution__c, e.From_Date__c, e.Description__c,e.Restrict_access_based_on_product_profile__c,
                                        Speaker__c , Title__c, CreatedDate, Time__c, URL__c
                                            From Event_Webinar__c e 
                                            where recordtype.name = 'Webinars' 
                                            and Featured__c = true and private__c = false and feathured_date__c!= null/*and From_Date__c >= today */
                                            order by feathured_date__c desc limit 1]){
                                
                if(fw!= null){
                           featWebinars = fw;
                                                 
                         
                }
            }
        }
     }
    
    public list<Event_Webinar__c> upcmgEvents{get;set;}  
    public list<Event_Webinar__c> recentEvents{get;set;}  
    public boolean recentEvntBln0{get;set;}
    public boolean recentEvntBln1{get;set;}  
    public boolean upcmgEvntBln0{get;set;}  
    public boolean upcmgEvntBln1{get;set;}
  public string upcmgEvntStr0{get;set;}
  public string upcmgEvntStr1{get;set;}
    
    private void fetchEvents()
    {
        upcmgEvents = new list<Event_Webinar__c>();
        recentEvents = new list<Event_Webinar__c>();
        
        upcmgEvents = [Select Id,URL__c, e.Location__c, e.Institution__c, e.From_Date__c, Title__c, To_Date__c,CME_Event__c,
                URL_Path_Settings__c, Needs_MyVarian_registration__c,Registration_is_closed_for_event__c From Event_Webinar__c e 
                        where recordtype.name = 'Events' and To_Date__c >= today and Private__c = false order by To_Date__c asc limit 2];
        recentEvents = [Select e.Location__c,URL__c, e.Institution__c, e.From_Date__c, Title__c, To_Date__c,CME_Event__c, 
                URL_Path_Settings__c From Event_Webinar__c e 
                        where recordtype.name = 'Events' and To_Date__c < today and Private__c = false order by To_Date__c desc limit 2];
                        
        if(recentEvents.size() == 1)
        {
            fillBlnkEvents(recentEvents);
            recentEvntBln1 = true;
        }
        else if(recentEvents.size() == 0)
        {
            fillBlnkEvents(recentEvents);
            fillBlnkEvents(recentEvents);
            recentEvntBln0 = true;
            recentEvntBln1 = true;
        }
        if(upcmgEvents.size() == 1)
        {
            fillBlnkEvents(upcmgEvents);
            upcmgEvntBln1 = true;
        }
        else if(upcmgEvents.size() == 0)
        {
            fillBlnkEvents(upcmgEvents);
            fillBlnkEvents(upcmgEvents);
            upcmgEvntBln0 = true;
            upcmgEvntBln1 = true;
        }
    else if(upcmgEvents.size() == 2)
    {
      upcmgEvntStr0 = upcmgEvents[0].Id;
      upcmgEvntStr1 = upcmgEvents[1].Id;
    }
    
    System.debug('aebug : upcmgEvents = '+upcmgEvents);
    System.debug('aebug : recentEvents = '+recentEvents);
    }
    public void fillBlnkEvents(list<Event_Webinar__c> cmnEvents)
    {
        Event_Webinar__c rcntEvnt = new Event_Webinar__c();
        rcntEvnt.Title__c = 'No Event';
        rcntEvnt.Location__c = 'NY';
        rcntEvnt.To_Date__c = system.today();
        rcntEvnt.From_Date__c = system.today();
        rcntEvnt.Needs_MyVarian_registration__c = false;
        cmnEvents.add(rcntEvnt);
    }
  /******************** Function for registering PON **********************/

 public void RecievePON(){
   //Commenting this method as pnl is not going live with portal
    /* List<PON__c> ListofPON = new List<PON__c>();
     String pcsnlist = System.currentPageReference().getParameters().get('pcsnurl');
     Id Complaint = (Id)System.currentPageReference().getParameters().get('complainturl');
     Map<String,PON__c> PONmap = new map<String,PON__c>();
     if(pcsnlist!= null && Complaint!= null){
        for(PON__c p : [Select id,PON_Received__c,PCSN__c from PON__c where Consignee__r.Complaint_Number__c =: Complaint]){
            PONmap.put(p.PCSN__c,p);
        }
        system.debug('pcsnlist'+pcsnlist);
        if(pcsnlist.contains(',')){
          for(String s: pcsnlist.split(',')){
               PON__c pn = new PON__c();
               pn.id = PONmap.get(s).id;
               pn.PON_Received__c = true;
               pn.Mode_of_Acknowledgement__c = 'E-Mail';
               pn.Acknowledgement_Date__c = system.today();
               //pn.Mode_of_Acceptance__c = 'Electronic';
               ListofPON.add(pn);
          }
        }
       else{
           PON__c pn = new PON__c();
           pn.id = PONmap.get(pcsnlist).id;
           pn.PON_Received__c = true;
           pn.Mode_of_Acknowledgement__c = 'E-Mail';
           pn.Acknowledgement_Date__c = system.today();
           //pn.Mode_of_Acceptance__c = 'Electronic';
           ListofPON.add(pn);   
       }
          system.debug('************' + ListofPON);
          update ListofPON;
          PONrecieve = 'block';
        }else{
            PONrecieve = 'none';
        }*/
    } 
    
    
    public PageReference aknowledgePON()
    {
    //Commenting this method as pnl is not going live with portal
      /*  system.debug(' ----------  Aknowledge ---------- ' + accntId);
        PON__c[] ponRcveUpt = new PON__c[]{};
        for(PON__c ponRcve : [Select p.PON_Received__c, p.Id, Acknowledgement_Date__c from PON__c p where Consignee__r.Complaint_Number__c = :Complaint_Id and Customer_Name__c in: accntId])
        {
            ponRcve.PON_Received__c = true;
            ponRcve.Acknowledgement_Date__c = system.today();
            ponRcveUpt.add(ponRcve);
        }
        update ponRcveUpt;
        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Account does not Exist '  + Complaint_Id);
        //ApexPages.addMessage(myMsg); */
        return null;
    }
    
    
    public List<selectOption> getDocumentOptionsList(){
        List<selectOption> optionsList= new List<selectOption>();
        String str='<Any>';
        
        optionsList.add(new selectOption(str,str));
        Schema.Describefieldresult fieldresult = ContentVersion.Document_Type__c.getDescribe();
        List<Schema.Picklistentry> Pickvalue = fieldresult.getPicklistvalues();
        integer i=0;
        //Pickvalue.sort();
        for(Schema.Picklistentry p : Pickvalue){
            i++;
            optionsList.add(new selectOption(p.getvalue(), p.getvalue()));
            

        }
        if(Pickvalue.size()>0 && (selectedDocument == '' || selectedDocument == null) ){
         selectedDocument=Pickvalue[Pickvalue.size()-1].getvalue();   
         }
         optionsList.sort();
        return optionsList;
    }
     
     //system.debug('++++selectedDocument++++'+selectedDocument);
     public List<content> getlstCntntVersion()
     {
        getDocumentOptionsList();
        List<ContentVersion> lstContentVersion=new List<ContentVersion>();
        system.debug('++++selectedDocument++++'+selectedDocument);
        if(selectedDocument == '<Any>'){
            lstContentVersion=[Select id,Mutli_Languages__c,Title,Document_Type__c,Date__c,ContentDocumentId, Complaint__c
                            from ContentVersion where RecordType.DeveloperName = 'Product_Document' and Parent_Documentation__c = null AND IsLatest=True
                            Order by Date__c desc limit 10];
             
             }else{
                 lstContentVersion=[Select id,Mutli_Languages__c,Title,Document_Type__c,Date__c,ContentDocumentId, Complaint__c
                                    from ContentVersion 
                                    where Document_Type__c=:selectedDocument and RecordType.DeveloperName = 'Product_Document' 
                                    and Parent_Documentation__c = null AND IsLatest=True
                                    Order by Date__c desc limit 10];    
             }
             // logic added to reduce the loading time on 30th jan
              Set<id> setofdocids = new Set<id>();
             for(contentversion cv: lstContentVersion){
               setofdocids.add(cv.ContentDocumentId);
             }
             for(ContentWorkspaceDoc cwd : [Select ContentWorkspace.Name, ContentWorkspaceId, ContentDocumentId 
                                               From ContentWorkspaceDoc where ContentDocumentId in: setofdocids])
            {
                String libraryname;
                if(contentWorkSpace.containskey(cwd.ContentDocumentId))
                {
                 libraryname = contentWorkSpace.get(cwd.ContentDocumentId);
                 libraryname = libraryname + ', ' + cwd.ContentWorkspace.Name;
                 contentWorkSpace.put(cwd.ContentDocumentId,libraryname);
                }else{
                 libraryname = cwd.ContentWorkspace.Name;
                 contentWorkSpace.put(cwd.ContentDocumentId,libraryname);
                }
                
                
             }
             
            PON__c[] ponRcve = [Select p.PON_Received__c, p.Id, p.Consignee__r.Complaint_Number__c, p.Consignee__c From PON__c p where Consignee__c != null and Customer_Name__c in: accntId];
             
            Map<Id, Boolean> mapCompPon = new Map<Id, Boolean>();    
            System.debug('PonRcve'+ponRcve.size());       
            for(PON__c p : ponRcve)
            {
                if(mapCompPon.containsKey(p.Consignee__r.Complaint_Number__c))
                {
                  if(!p.PON_Received__c)
                   mapCompPon.put(p.Consignee__r.Complaint_Number__c, p.PON_Received__c);
                }else{
                    mapCompPon.put(p.Consignee__r.Complaint_Number__c, p.PON_Received__c);
                    }
            }
            // Logic for institution affected column in ui
            Set<Id> Institutionaffected = new Set<Id>();
            for(Consignee__c con: [Select name,Complaint_Number__c from consignee__c where Customer_Name__c =: loginacc])
              {
                Institutionaffected.add(con.Complaint_Number__c);
              }
            
         system.debug('++++lstContentVersion++++'+lstContentVersion); 
         system.debug('++++contentWorkSpace++++'+contentWorkSpace);
         List<content> newlist = new List<content>();
         for(ContentVersion cv : lstContentVersion){
            //for(ContentWorkspaceDoc lib : contentWorkSpace){
                if(contentWorkSpace.containskey(cv.ContentDocumentId)){
                //if(cv.ContentDocumentId == lib.ContentDocumentId){
                    content obj = new content();
                    obj.library = contentWorkSpace.get(cv.ContentDocumentId);
                    System.debug('In Library-->'+ contentWorkSpace.get(cv.ContentDocumentId));
                    obj.cont = cv;
                    if(mapCompPon.keySet().contains(cv.Complaint__c))
                        obj.selected = string.valueOf(mapCompPon.get(cv.Complaint__c));
                    else
                        obj.selected = 'blank';
                 if(cv.Complaint__c != null)
                  {
                    if(Institutionaffected.contains(cv.Complaint__c))
                      obj.Institutionaffctd = 'Yes';
                    else
                      obj.Institutionaffctd = 'No';
                  }else               
                      obj.Institutionaffctd = '';
                    
                    newlist.add(obj);
                }
                    
            //}
         }
         system.debug('++++newlist++++'+newlist);
         
         //return lstContentVersion;
         return newlist;
     }
     
}
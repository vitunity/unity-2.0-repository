/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 13-June-2013
    @ Description   :  Test class for SlideController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest (SeeAllData = false)

Public class SlideControllerTest{

    
    //Test Method for SlideController class 
    
    static testmethod void testSlideController(){ 
        
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate'); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='Acuity';
            
            insert prod;
            
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;
            
            Contact_Role_Association__c cr = new Contact_Role_Association__c();
            cr.Account__c= a.Id;
            cr.Contact__c = u.Contactid;
            cr.Role__c = 'RAQA';
        
           insert cr;
           
             
        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         Contact con =[Select Id, Name , AccountId from Contact where Id=:usr.ContactId];
         
         Account a = [Select Id, Name from Account where Id=:con.AccountId ];
         usersessionids__c useess=new usersessionids__c();
         useess.name=usr.id;
         insert useess;
         RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'Banner' AND sobjecttype='BannerRepository__c'];
            BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
            insert bn;
            
          
         System.runAs (usr){
           

        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef);
        Test.StartTest();
      
        try{        
        
         List<Complaint__c > cmpt = new List<Complaint__c>();
        Complaint__c complaint = new Complaint__c();
        complaint.Name = '12345';
        complaint.PNL_Subject__c ='Test Complaint';
        cmpt.add(complaint);
        
        Complaint__c complaint1 = new Complaint__c();
        complaint1.Name = '12345';
        complaint1.PNL_Subject__c ='Test Complaints';
        cmpt.add(complaint1);
        
           insert cmpt;
       
       Complaint__c complaint2 = new Complaint__c();
        complaint2.Name = '12345';
        complaint2.PNL_Subject__c ='Test Complaints';
         insert complaint2;
         
         
         Consignee__c consignee = new Consignee__c();
        consignee.Complaint_Number__c = complaint.id;
        consignee.PCSN__c ='Test';
        insert consignee;
        
        
        PON__c pon = new PON__c();
        pon.PON_Received__c = True;
        pon.PCSN__c = 'Test PON';
        //pon.Complaint__c= complaint.id;
        pon.Customer_Name__c = con.AccountId;
        pon.Acknowledgement_Date__c= Date.Today();
        insert pon;     
        
       
         }
         catch(exception e){
         
         Boolean expectedExceptionThrown =  e.getMessage().contains('Dupliacte PNL Reference ID not allowed') ? true : false;
         }
        
        
        
       
        SlideController myhome = new SlideController();
        SlideController.content cnt = new SlideController.content();
        StLoginCntlr controller = new StLoginCntlr();
        SlideController ext = new SlideController(controller);
        myhome.aknowledgePON();
       // myhome.MADALContact();
        ext.getlstCntntVersion();
        ext.getrecoveryqusoptions();
        ext.cancelresetpassword();
        //Map<String,usersessionids__c> sessionmap = new Map<String,usersessionids__c>();
        //usersessionids__c usrid=[select name,session_id__c from usersessionids__c];
        
       // sessionmap.put('name',usrid.session_id__c );
        ext.logoutmethod();

        myhome.RecievePON();
        
        Test.StopTest();
      
        List<selectOption> soption = myhome.getDocumentOptionsList();
        }
        
     }
     
     //Test Method for SlideController class when running in the context of logged in user
     
     static testmethod void testSlideController1(){ 
        Test.StartTest();
        
        User u = [select Id, Name from User Where Id=:Userinfo.getUserId()];
        system.runAs(u){
        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        
        SlideController myhome = new SlideController();
        SlideController.content cnt = new SlideController.content();
        StLoginCntlr controller = new StLoginCntlr();
        SlideController ext = new SlideController(controller);
        myhome.RecievePON();
        myhome.aknowledgePON();
         myhome.MADALContact();
        ext.getlstCntntVersion();
        ext.getrecoveryqusoptions();
        ext.cancelresetpassword();
        // myhome.resetpassword();
        }
        Test.StopTest();
     }
     
     //Test Method for SlideController class for fillBlnkEvents Method
     
     static testmethod void testSlideController2(){ 
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){


        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        
        List<Event_Webinar__c> Event = new List<Event_Webinar__c>();
        Event.add(new Event_Webinar__c(Title__c = 'Test Event',
        Location__c = 'NY',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False));
        Event.add(new Event_Webinar__c(Title__c = 'Test Event1',
        Location__c = 'Us',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False));
        /*Event.add(new Event_Webinar__c(Title__c = 'Test Event2',
        Location__c = 'India',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False));*/
        SlideController myhome = new SlideController();
        myhome.fillBlnkEvents(Event);
       
        
        }
        Test.StopTest();
     }
     
     //Test Method for SlideController class when prdnameurl doesn't contain ','
     
  static testmethod void testSlideController3(){ 
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User usr;
        Account a;  
            
        System.runAs ( thisUser ) {        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;              
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate' ); 
            insert con;
            usr = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/);             
            insert usr; // Inserting Portal User
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;
            
            Contact_Role_Association__c cra=new Contact_Role_Association__c();
            cra.Account__c=a.id;
            cra.Contact__c=usr.ContactId;
            cra.Role__c = 'RAQA';
            insert cra;

        }
         
         //user usr = [Select Id, ContactId, AccountId,Name from User where email=:'standarduser@testorg.com' ];
         
         //Contact con =[Select Id, Name , AccountId from Contact where Id=:usr.ContactId];
         
         //Account a = [Select Id, Name from Account where Id=:con.AccountId ];
         Test.StartTest();
         System.runAs (usr){


        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        
        Complaint__c complaint = new Complaint__c();
        complaint.Name = '12345';
        complaint.PNL_Subject__c ='Test Complaint';
        
        insert complaint;
        
        Consignee__c consignee = new Consignee__c();
        consignee.Complaint_Number__c = complaint.id;
        consignee.PCSN__c ='Test';
        insert consignee;
        
        PON__c pon = new PON__c();
        pon.PON_Received__c = True;
        pon.PCSN__c = 'Test PON';
        //pon.Complaint__c= consignee.Complaint_Number__c;
        pon.Customer_Name__c = usr.AccountId;
        pon.Consignee__c = consignee.Id;
        pon.Acknowledgement_Date__c= Date.Today();
        insert pon;
        
        
        RecordType RType = [select Id , Name , DeveloperName FROM RecordType WHERE Name ='Webinars' and sobjecttype='Event_Webinar__c'];
        Event_Webinar__c Webinar = new Event_Webinar__c();
        Webinar.RecordTypeId = RType.Id; 
        Webinar.Title__c = 'Test Webinar';
        Webinar.Product_Affiliation__c = '4DITC';
        Webinar.Location__c = 'NY';
        Webinar.To_Date__c = system.today();
        Webinar.From_Date__c = system.today();
        Webinar.Needs_MyVarian_registration__c=False;
        Webinar.Featured__c=true;
        Webinar.private__c=false;
        Webinar.Restrict_access_based_on_product_profile__c=false;
        Webinar.feathured_date__c=system.today();
        insert Webinar;

        
        RecordType rt=[select id, name from RecordType where name='Banner' and sobjecttype='BannerRepository__c'];
        List<BannerRepository__c> lstBannerRepo = new List<BannerRepository__c>();
        BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rt.id,Weight__c=20,Region__c='EMEA');
        lstBannerRepo.add(bn);
        
        BannerRepository__c bn1=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rt.id,Weight__c=20,Region__c='EMEA',Image__c='testimage');
        lstBannerRepo.add(bn1);
        BannerRepository__c bn2=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rt.id,Weight__c=20,Region__c='EMEA',Image__c='testimage', Product_Affiliation__c = 'All');
        lstBannerRepo.add(bn2);
        insert lstBannerRepo;
        ApexPages.currentPage().getParameters().put('complainturl',complaint.id);
        ApexPages.currentPage().getParameters().put('pcsnurl','Test PON');
        ApexPages.currentPage().getParameters().put('prdnameurl','Test PON');
        ApexPages.currentPage().getParameters().put('RecoveryAnswerparam','Test rec');
       
        SlideController myhome = new SlideController();
        StLoginCntlr controller = new StLoginCntlr();
        SlideController ext = new SlideController(controller);
        ext.complaint_id=complaint.id;
       // ext.getlstCntntVersion();
        ext.getrecoveryqusoptions();
        ext.cancelresetpassword();
        ext.resetpassword();
        myhome.RecievePON();
        // myhome.MADALContact();
        }
        Test.StopTest();
     }
     
     //Test Method for SlideController class when prdnameurl contains ','
     
     static testmethod void testSlideController4(){ 
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx' ,Country__c='Test' ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='USA',MailingState='UP' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;
            
            Contact_Role_Association__c cra=new Contact_Role_Association__c();
            cra.Account__c=a.id;
            cra.Contact__c=u.ContactId;
            cra.Role__c = 'RAQA';
            insert cra;

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         Contact con =[Select Id, Name , AccountId from Contact where Id=:usr.ContactId];
         
         Account a = [Select Id, Name from Account where Id=:con.AccountId ];
         
         System.runAs (usr){


        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        
        Complaint__c complaint = new Complaint__c();
        complaint.Name = '12345';
        complaint.PNL_Subject__c ='Test Complaint';
        
        insert complaint;
        
        Consignee__c consignee = new Consignee__c();
        consignee.Complaint_Number__c = complaint.id;
        consignee.PCSN__c ='Test';
        insert consignee;
        
        
        
        PON__c pon = new PON__c();
        pon.PON_Received__c = True;
        pon.PCSN__c = 'Test PON';
        //pon.Complaint__c= consignee.Complaint_Number__c;
        pon.Customer_Name__c = con.AccountId;
        pon.Consignee__c = consignee.Id;
        pon.Acknowledgement_Date__c= Date.Today();
        insert pon;
        
        PON__c pon1 = new PON__c();
        pon1.PON_Received__c = True;
        pon1.PCSN__c = 'Test PON1';
        //pon1.Complaint__c= consignee.Complaint_Number__c;
        pon1.Customer_Name__c = con.AccountId;
        pon1.Consignee__c = consignee.Id;
        pon1.Acknowledgement_Date__c= Date.Today();
        insert pon1;
        
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('BannerRepository__c') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rTId = RecordTypeInfo.get('Banner').getRecordTypeId();

      //RecordType rt=[select id, name from RecordType where name='Banner' and sobjecttype='BannerRepository__c'];
        BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rTId,Weight__c=20,Region__c='N.America', Product_Affiliation__c='Argus',image__c='testimage for testing src data is to be present here');
        insert bn;
        
        /*RecordType rt=[select id, name from RecordType where name='Banner' and sobjecttype='BannerRepository__c'];
        BannerRepository__c bn1=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rt.id,Weight__c=20,Region__c='EMEA',Image__c='testimage');
        insert bn1;
        
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('ContentVersion') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id rTId = RecordTypeInfo.get('Product Document').getRecordTypeId();
        
        //RecordType rt1=[select id, name from RecordType where DeveloperName = 'Product_Document' and sobjecttype='ContentVersion'];
        ContentVersion  contVer=new ContentVersion(RecordTypeId=rTId,Title='test',ContentURL='http://www.google.com/');
        insert contVer;*/
        
        ApexPages.currentPage().getParameters().put('complainturl',complaint.id);
        ApexPages.currentPage().getParameters().put('pcsnurl','Test PON,Test PON1');
        ApexPages.currentPage().getParameters().put('RecoveryAnswerparam','Test rec');
 
        /*SlideController myhome = new SlideController();
        StLoginCntlr controller = new StLoginCntlr();
        SlideController ext = new SlideController(controller);
        ext.selectedDocument='<ANY>';
        ext.getlstCntntVersion();
        ext.getrecoveryqusoptions();
        ext.cancelresetpassword();
        ext.resetpassword();
        myhome.RecievePON();
         myhome.MADALContact(); */
        }
        // for internal user
        SlideController myhome = new SlideController();
        StLoginCntlr controller = new StLoginCntlr();
        SlideController ext = new SlideController(controller);
        Test.StopTest();
     }
    
    static testmethod void testSlideController5(){ 
        Test.StartTest();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx' ,Country__c='Test' ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test',MailingState='UP' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;
            
            Contact_Role_Association__c cra=new Contact_Role_Association__c();
            cra.Account__c=a.id;
            cra.Contact__c=u.ContactId;
            cra.Role__c = 'RAQA';
            insert cra;

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
         
         Contact con =[Select Id, Name , AccountId from Contact where Id=:usr.ContactId];
         
         Account a = [Select Id, Name from Account where Id=:con.AccountId ];
         
         System.runAs (usr){


        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        Complaint__c complaint = new Complaint__c();
        complaint.Name = '12345';
        complaint.PNL_Subject__c ='Test Complaint';
        insert complaint;
        
        Consignee__c consignee = new Consignee__c();
        consignee.Complaint_Number__c = complaint.id;
        consignee.PCSN__c ='Test';
        insert consignee;
        
        RecordType rt=[select id, name from RecordType where name='Banner' and sobjecttype='BannerRepository__c'];
        
        BannerRepository__c bn=new BannerRepository__c(End_Date__c =system.today(),Start_Date__c=system.today(),RecordTypeId=rt.Id,Weight__c=20,Region__c='EMEA', Product_Affiliation__c='All',image__c='testimage for testing');
        insert bn;
        }
        
        SlideController myhome = new SlideController();
        Test.StopTest();
    }
    
    static testmethod void testSlideController6()
    {
        RecordType rt=[select id, name from RecordType where name='Banner' and sobjecttype='BannerRepository__c'];
        BannerRepository__c bn=new BannerRepository__c(Image__c = 'this image is src for testing', End_Date__c =system.today(),Start_Date__c=system.today(),recordtypeid=rt.id,Weight__c=20,Region__c='EMEA');
        insert bn;
        
        rt=[select id, name from RecordType where name='Webinars' and sobjecttype='Event_Webinar__c'];
        Event_Webinar__c webinr = new Event_Webinar__c(Title__c = 'Test Webinar', recordtypeid = rt.Id, Location__c = 'Us', Featured__c = true, private__c = false, Restrict_access_based_on_product_profile__c = false, feathured_date__c = system.today());
        insert webinr;
        
        Profile prof = [select id from profile where name = 'VMS MyVarian - Guest User'];
        User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser16@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = prof.Id, timezonesidkey='America/Los_Angeles', username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
        
        System.runAs(u)
        {
            SlideController sldcntr = new SlideController();
        }
    }
    
    static testmethod void testSlideControllerEvent1(){ 
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a; 
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate' ); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduserak@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Regulatory_Country__c regcount=new Regulatory_Country__c();
            regcount.Name = 'Test';
            regcount.RA_Region__c ='NA';
            regcount.Portal_Regions__c ='N.America,Test';
            insert regcount;
            

        }
         
         user usr = [Select Id, ContactId, Name from User where email=:'standarduserak@testorg.com' ];
         
         System.runAs (usr){


        PageReference pageref = Page.cphomepage;
        Test.setCurrentPage(pageRef); 
        
        List<Event_Webinar__c> Event = new List<Event_Webinar__c>();
       Event.add(new Event_Webinar__c(Title__c = 'Test Event',
        Location__c = 'NY',
        To_Date__c = system.today(),
        From_Date__c = system.today()-2,
        Private__c = false
        ));
       /* Event.add(new Event_Webinar__c(Title__c = 'Test Event1',
        Location__c = 'Us',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False));*/
        /*Event.add(new Event_Webinar__c(Title__c = 'Test Event2',
        Location__c = 'India',
        To_Date__c = system.today(),
        From_Date__c = system.today(),
        Needs_MyVarian_registration__c=False));*/
        SlideController myhomes = new SlideController();
        SlideController.banner inst1 = new SlideController.banner();
       
        
        }
        Test.StopTest();
     }
}
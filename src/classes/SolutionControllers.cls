/*@RestResource(urlMapping='/SolutionList/*')
   global class SolutionControllers 
   {
   @HttpGet
   global static List<VU_Solutions__c> getsSolution() 
   {
   List<VU_Solutions__c> widgets1 ;
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   res.addHeader('Access-Control-Allow-Origin', '*');
   res.addHeader('Content-Type', 'application/json');
   res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
   String SolutionId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
     widgets1 = [SELECT Id, Name,Solution_Details__c,Playlist_Id__c,Languages__c, (SELECT Id FROM Attachments) FROM VU_Solutions__c where Languages__c='English(en)' OR Languages__c=''];
}
else
{
   widgets1 = [SELECT Id, Name,Solution_Details__c,Playlist_Id__c,Languages__c, (SELECT Id FROM Attachments) FROM VU_Solutions__c where Languages__c=:Language];
 }
   return widgets1;
    }
    /*global static Solution doGet() 
    {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String SolutionId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    Solution result1 = [SELECT Id, SolutionName,SolutionNote, (SELECT Id FROM Attachments) FROM Solution];
    return result1;
    }
    
    
}*/

@RestResource(urlMapping='/SolutionList/*')
   global class SolutionControllers 
   {
   @HttpGet
   global static List<VU_Solutions__c> getsSolution() 
   {
   List<VU_Solutions__c> widgets1 ;
   RestRequest req = RestContext.request;
   RestResponse res = RestContext.response;
   res.addHeader('Access-Control-Allow-Origin', '*');
   res.addHeader('Content-Type', 'application/json');
   res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
   String SolutionId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   String Language= RestContext.request.params.get('Language');
    if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(ja)' && Language !='Portuguese(pt-BR)' ){
     widgets1 = [SELECT Id, Name,Solution_Details__c,Playlist_Id__c,Languages__c, (SELECT Id FROM Attachments) FROM VU_Solutions__c where Languages__c='English(en)' OR Languages__c=''];
}
else
{
   widgets1 = [SELECT Id, Name,Solution_Details__c,Playlist_Id__c,Languages__c, (SELECT Id FROM Attachments) FROM VU_Solutions__c where Languages__c=:Language];
 }
   return widgets1;
    }
    /*global static Solution doGet() 
    {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String SolutionId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    Solution result1 = [SELECT Id, SolutionName,SolutionNote, (SELECT Id FROM Attachments) FROM Solution];
    return result1;
    }*/
    
    
}
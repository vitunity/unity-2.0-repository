/*
 * @author - Amitkumar Katre
 * This will be used to set consumed from locationa and source of parts
 * INC4295524  DFCT0011762  Amitkumar Katre  Parts Work Details: Source of Parts and Consumed from Location Incorrect
 * 26-Jul-2018 - Nick Sauer - STSK0015356:  Update SVMXC__Actual_Price2__c to Local_Purchase_Price__c to avoid SVMX Trigger
 */
public class SourceofPartsWorkDetailHandler {
    
    private static map<Id, SVMXC__Service_Order__c> woOrderMap ;
    
    /*
     * To set consumed from location 
     * To set sourc eof parts
     */
    public static void execute(list<SVMXC__Service_Order_line__c> workDetailList, 
                               map<Id, SVMXC__Service_Order_line__c> oldMap){
        set<id> woIds = new set<id>();
        for(SVMXC__Service_Order_line__c wdl : workDetailList){
            if(wdl.SVMXC__Line_Status__c == 'Open'){ 
                if(wdl.SVMXC__Line_Type__c == 'Parts'  
                       && (oldMap == null 
                           || wdl.Parts_Order_Line__c != oldMap.get(wdl.Id).Parts_Order_Line__c
                           || wdl.Product_Stock__c != oldMap.get(wdl.Id).Product_Stock__c 
                           || wdl.Local_Purchase_Price__c != oldMap.get(wdl.Id).Local_Purchase_Price__c
                           || wdl.SVMXC__Consumed_From_Location__c != oldMap.get(wdl.Id).SVMXC__Consumed_From_Location__c
                           
                       )){
                          woIds.add(wdl.SVMXC__Service_Order__c);     
                }
                if(wdl.SVMXC__Line_Type__c == 'Parts'){
                    // This conditions added because mfl syc isssue
                    if((wdl.Parts_Order_Line__c != null 
                        || wdl.Product_Stock__c != null)
                        && wdl.Source_of_Parts__c != 'V'
                        ){
                            woIds.add(wdl.SVMXC__Service_Order__c);
                    }else if(wdl.Parts_Order_Line__c == null 
                        && wdl.Product_Stock__c == null 
                        && (wdl.Local_Purchase_Price__c  == null || wdl.Local_Purchase_Price__c == 0) 
                        && wdl.Source_of_Parts__c != 'C') {
                        woIds.add(wdl.SVMXC__Service_Order__c);
                    }else if(wdl.Parts_Order_Line__c == null 
                        && wdl.Product_Stock__c == null 
                        &&(wdl.Local_Purchase_Price__c  != null && wdl.Local_Purchase_Price__c > 0)
                        && wdl.Source_of_Parts__c != 'L'
                        && (wdl.Part_Disposition__c != 'Removing' || wdl.Part_Disposition__c != 'Removed') ) {
                        woIds.add(wdl.SVMXC__Service_Order__c);
                    }else if(wdl.Parts_Order_Line__c == null 
                        && wdl.Product_Stock__c == null 
                        &&(wdl.Local_Purchase_Price__c  != null && wdl.Local_Purchase_Price__c > 0)
                        && wdl.Source_of_Parts__c != 'C'
                        && (wdl.Part_Disposition__c == 'Removing' || wdl.Part_Disposition__c == 'Removed') ) {
                        woIds.add(wdl.SVMXC__Service_Order__c);
                    }
                }
            }
        }
        if(!woIds.isEmpty()){
            woOrderMap = new map<Id, SVMXC__Service_Order__c>([select SVMXC__Site__c, Sales_Org__c,
                                                               Inventory_Location__c
                                                               from SVMXC__Service_Order__c 
                                                               where id in : woIds]);
            for(SVMXC__Service_Order_line__c wdl : workDetailList){
                SVMXC__Service_Order__c wo = woOrderMap.get(wdl.SVMXC__Service_Order__c);
                if(wo != null && wdl.SVMXC__Line_Type__c == 'Parts' && wdl.SVMXC__Line_Status__c == 'Open'){
                    if((wdl.Parts_Order_Line__c != null || wdl.Product_Stock__c != null)
                        && (wdl.Part_Disposition__c != 'Removing' && wdl.Part_Disposition__c != 'Removed' )){
                        //set FE locsation
                        wdl.SVMXC__Consumed_From_Location__c = wo.Inventory_Location__c;
                        //set source of parts V
                        wdl.Source_of_Parts__c = 'V';                   
                        if(wo.Sales_Org__c == '4400'){
                            wdl.Interface_Status__c = 'Do not process';
                        }else{
                            wdl.Interface_Status__c = '';
                        }
                    }else if(wdl.Parts_Order_Line__c == null && wdl.Product_Stock__c == null 
                        && ( wdl.Local_Purchase_Price__c  == null || wdl.Local_Purchase_Price__c == 0) ) {
                        //set Customer  Location 
                        wdl.SVMXC__Consumed_From_Location__c = wo.SVMXC__Site__c;
                        //set source if part is C
                        wdl.Source_of_Parts__c = 'C';
                        wdl.Interface_Status__c = 'Do not process';
                    }else if(wdl.Parts_Order_Line__c == null && wdl.Product_Stock__c == null 
                        && (wdl.Local_Purchase_Price__c  != null && wdl.Local_Purchase_Price__c > 0) 
                        && (wdl.Part_Disposition__c != 'Removing' && wdl.Part_Disposition__c != 'Removed' )) {
                        //set locally purchased
                        wdl.SVMXC__Consumed_From_Location__c = Label.Locally_Purchased_Location;
                        //set source is L.
                        wdl.Source_of_Parts__c = 'L';
                        if(wo.Sales_Org__c == '4400'){
                            wdl.Interface_Status__c = 'Do not process';
                        }else{
                            wdl.Interface_Status__c = '';
                        }
                    }else if(wdl.Parts_Order_Line__c == null && wdl.Product_Stock__c == null 
                        && (wdl.Local_Purchase_Price__c  != null && wdl.Local_Purchase_Price__c > 0) 
                        && (wdl.Part_Disposition__c == 'Removing' || wdl.Part_Disposition__c == 'Removed' )) {
                        wdl.SVMXC__Consumed_From_Location__c = wo.SVMXC__Site__c;
                        //set source if part is C
                        wdl.Source_of_Parts__c = 'C';
                        wdl.Interface_Status__c = 'Do not process';
                    }      
                }   
            }   
        }
    }
}
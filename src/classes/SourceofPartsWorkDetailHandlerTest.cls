@isTest
public class SourceofPartsWorkDetailHandlerTest
{
    public static ID TechRecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();  
    public static ID hdRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();  
    public static ID FieldSeRecordTypeID = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();  
    public static ID UsageConRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
    public static ID ProdServRecordTypeID = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();  
    public static Profile pr = [Select id from Profile where name = 'VMS Unity 1C - Service User'];    
    public static User u = new user(alias = 'standt', email='standardtestuse92@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pr.Id, timezonesidkey='America/Los_Angeles',username='standardtestuse92@testclass.com');
    public static Account objAcc;
    public static Country__c con;
    public static SVMXC__Installed_Product__c objIns;
    public static SVMXC__Installed_Product__c objIns1;
    public static ERP_Timezone__c  objERP;
    public static Product2 objProd;
    public static SVMXC__Site__c varSite;
    public static SVMXC__Service_Group__c ObjSvcTeam;
    public static ERP_Org__c varErp ;
    public static SVMXC__Service_Group_Members__c ObjTech;
    public static SVMXC__Service_Group_Members__c ObjTech1;
    public static Case ObjCase;
    public static SVMXC__Service_Order__c objWO;
    public static SVMXC__Service_Level__c objSLATerm;
    public static SVMXC__Service_Order_Line__c objWD;
    public static SVMXC__Service_Contract__c varSC;
    public static SVMXC__Service_Contract_Products__c objSCP;
    public static SVMXC__Service_Offerings__c objSO;
    public static SVMXC__Service_Plan__c objSp;
    public static Product_System_Subsystem__c pSystem;
    public static STB_Master__c stb;
    public static CountryDateFormat__c dateFormat;
    public static ERP_NWA__c erpnwa;
    public static Sales_Order_Item__c soi;
    public static ERP_WBS__c wbs;
    
    static
    {

        //insert Country record
        con = new Country__c(Name = 'United States');
        insert con;

        //insert ERP Timezone Record
        objERP = new ERP_Timezone__c(Name = 'AUSSA', Salesforce_timezone__c = 'Central Summer Time (Australia/Adelaide)');
        insert objERP;

        //insert account Record
        objAcc = new Account(Name = 'TestWDL', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id, BillingCountry = 'United States', BillingCity = 'Milpitas', State_Province_Is_Not_Applicable__c = true);
        insert objAcc;

        //insert Product Record
        objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
        insert objProd;

        //insert location Record
        varSite = new SVMXC__Site__c(Sales_Org__c = '0600', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = objAcc.id);
        insert varSite;

        //insert sla term
        objSLATerm = new SVMXC__Service_Level__c(Name = 'Test SLA Term', SVMXC__Active__c = true, SVMXC__Restoration_Tracked_On__c = 'WorkOrder');
        insert objSLATerm;

        objSp = new SVMXC__Service_Plan__c(Name = 'test', SVMXC__Active__c = true, SVMXC__SLA_Terms__c = objSLATerm.id);
        insert objSp;
        //insert Service contract
        varSC = new SVMXC__Service_Contract__c(SVMXC__Company__c = objAcc.id, ERP_Contract_Type__c = 'ZH3', SVMXC__Service_Level__c = objSLATerm.id);
        insert varSC;

        objSO= new SVMXC__Service_Offerings__c(SLA_Term__c = objSLATerm.id, SVMXC__Labor_Discount_Covered__c = 100.00, SVMXC__Service_Plan__c = objSp.id);
        insert objSO;
        //insert Service Team Record
        ObjSvcTeam = new SVMXC__Service_Group__c(Name = 'Test Team', SVMXC__Active__c = true, District_Manager__c = UserInfo.getUserId(), SVMXC__Group_Code__c = 'ABC');
        insert ObjSvcTeam;

        //insert ERP org Record
        varErp = new ERP_Org__c(Name = '0600', Return_All__c = false, Sales_Org__c = '0600');
        insert varErp;

        //insert technician Record
        ObjTech = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true, SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com', SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId(),ERP_Work_Center__c='H9876545321');
        ObjTech1 = new SVMXC__Service_Group_Members__c(RecordTypeId = TechRecordTypeID, Name = 'Test Technician', SVMXC__Active__c = true, SVMXC__Enable_Scheduling__c = true,SVMXC__Phone__c = '987654321', SVMXC__Email__c = 'abc@xyz.com', SVMXC__Service_Group__c = ObjSvcTeam.ID, User__c = UserInfo.getUserId(),ERP_Work_Center__c='H9876545321');

        insert new list<SVMXC__Service_Group_Members__c>{ObjTech,ObjTech1};

        objIns = new SVMXC__Installed_Product__c(SVMXC__Service_Contract__c = varSC.id, SVMXC__Site__c = varSite.ID, Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H1234', SVMXC__Company__c = objAcc.id, Name = 'H1234', SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id, WBS_Element__c = 'W1234');

        objIns1 = new SVMXC__Installed_Product__c(SVMXC__Service_Contract__c = varSC.id, SVMXC__Site__c = varSite.ID, Service_Team__c = ObjSvcTeam.id, ERP_Reference__c = 'H1234', SVMXC__Company__c = objAcc.id, Name = 'H1234', SVMXC__Preferred_Technician__c = ObjTech.id, SVMXC__Product__c = objProd.id, WBS_Element__c = 'W1234');

        insert new list<SVMXC__Installed_Product__c>{objIns,objIns1};
        objSCP = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = varSC.id, SVMXC__End_Date__c = system.today().addDays(+3),
                                                 SVMXC__Installed_Product__c = objIns.Id, SVMXC__Start_Date__c = system.today().addDays(-2));
        insert objSCP;

        ObjCase = new Case(RecordTypeId = hdRecordTypeID, AccountId = objAcc.id,SVMXC__Top_Level__c = objIns.id, Priority = 'Medium');
        insert objCase;
    }
    
    public static void unitTestData3(){
        
        pSystem = new Product_System_Subsystem__c();
        pSystem.Name = 'Test';
        insert pSystem;
        
        objWO = new SVMXC__Service_Order__c(RecordTypeId = FieldSeRecordTypeID, SVMXC__Case__c=objCase.id, SVMXC__Order_Status__c = 'Assigned',
                                            SVMXC__Company__c = objAcc.id, SVMXC__Priority__c = 'Medium', 
                                            SVMXC__Top_Level__c = objIns.id,SVMXC__Group_Member__c=ObjTech.id, Direct_Assignment__c = True);
        insert objWO;
    }
    
    @isTest(seealldata=true)
    public static void unitTest3(){

        System.Test.StartTest();
        unitTestData3();
        SVMXC__RMA_Shipment_Order__c objPO = new SVMXC__RMA_Shipment_Order__c(
            SVMXC__Company__c = objAcc.id, 
            SVMXC__Case__c = ObjCase.Id, 
            To_Country__c = con.Id,
            From_Country__c = con.Id,
            SVMXC__Destination_Location__c = varSite.id,
            SVMXC__Source_Location__c = varSite.id,
            SVMXC__Source_Country__c = 'India',
            SVMXC__Destination_Country__c  ='United States',
            ERP_Sold_To__c = objAcc.id,
            ERP_Ship_To_Code__c = 'TEST',
            One_time__c = true,
            SVMXC__Service_Order__c = objWO.Id,
            ERP_Result_Status__c = 'success',
            Transfer_to_FE__c = ObjTech.Id);
        insert objPO;
                
                
        SVMXC__RMA_Shipment_Line__c objPOL = new SVMXC__RMA_Shipment_Line__c(
            RecordTypeId = Schema.Sobjecttype.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId(),
            SVMXC__RMA_Shipment_Order__c = objPO.Id,
            SVMXC__Actual_Quantity2__c = 5,
            SVMXC__Expected_Quantity2__c = 5,
            SVMXC__Fulfillment_Qty__c =  5,
            Remaining_Qty__c = 5,
            SVMXC__Line_Status__c = 'Completed',
            SVMXC__Line_Type__c = 'Shipment',
            SVMXC__Ship_Location__c = varSite.Id,
            SVMXC__Product__c = objProd.Id,
            SVMXC__Service_Order__c = objWO.Id
            );  
        
        insert objPOL;

       
        SVMXC__Service_Order_Line__c objWD2 = new SVMXC__Service_Order_Line__c(SVMXC__Group_Member__c = ObjTech.id, 
                                              Old_Part_Number__c = objProd.id,  RecordTypeId = UsageConRecordTypeID,
                                              SVMXC__Line_Type__c = 'Parts', 
                                              SVMXC__Serial_Number__c = objIns.id,
                                              SVMXC__Product__c = objProd.Id,
                                              SVMXC__Service_Order__c = objWO.Id,
                                              part_disposition__c = 'Installing',
                                              no_install_required__c = true,
                                              SVMXC__Line_Status__c = 'Open');
        
        SVMXC__Service_Order_Line__c objWD1 = new SVMXC__Service_Order_Line__c(SVMXC__Group_Member__c = ObjTech.id, 
                                              Old_Part_Number__c = objProd.id,  RecordTypeId = UsageConRecordTypeID,
                                              SVMXC__Line_Type__c = 'Parts', 
                                              SVMXC__Serial_Number__c = objIns.id,
                                              SVMXC__Product__c = objProd.Id,
                                              SVMXC__Service_Order__c = objWO.Id,
                                              part_disposition__c = 'Installing',
                                              Local_Purchase_Price__c = 10,
                                              no_install_required__c = true, 
                                              Parts_Order_Line__c = objPOL.Id,
                                              SVMXC__Line_Status__c = 'Open');
        
        SVMXC__Service_Order_Line__c objWD3 = new SVMXC__Service_Order_Line__c(SVMXC__Group_Member__c = ObjTech.id, 
                                              Old_Part_Number__c = objProd.id,  RecordTypeId = UsageConRecordTypeID,
                                              SVMXC__Line_Type__c = 'Parts', 
                                              SVMXC__Serial_Number__c = objIns.id,
                                              SVMXC__Product__c = objProd.Id,
                                              SVMXC__Service_Order__c = objWO.Id,
                                              part_disposition__c = 'Installing',
                                              Local_Purchase_Price__c = 90,
                                              no_install_required__c = true,
                                              SVMXC__Line_Status__c = 'Open');
        
        SVMXC__Service_Order_Line__c objWD4 = new SVMXC__Service_Order_Line__c(
            								  SVMXC__Group_Member__c = ObjTech.id, 
                                              /*Old_Part_Number__c = objProd.id, */ 
            								  RecordTypeId = UsageConRecordTypeID,
                                              SVMXC__Line_Type__c = 'Parts', 
                                              SVMXC__Serial_Number__c = objIns.id,
                                              SVMXC__Product__c = objProd.Id,
                                              SVMXC__Service_Order__c = objWO.Id,
                                              part_disposition__c = 'Removing',
                                              Local_Purchase_Price__c = 90,
                                              no_install_required__c = true,
                                              SVMXC__Line_Status__c = 'Open');
        
        insert new list<SVMXC__Service_Order_Line__c>{objWD1, objWD3,objWD2,objWD4};
        System.Test.StopTest();
        objWD3.Parts_Order_Line__c = objPOL.Id;
        update objWD3;
        
    }
}
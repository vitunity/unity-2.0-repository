public with sharing class SpeakerControler {
    public SelectOption[] selectedSpeakers { get; set; }
    public SelectOption[]  allSpeakers { get; set; } 
    public String message { get; set; }
    
    public SpeakerControler() 
    {
    //CH.New.Start
        system.debug('xxxEID: '+ApexPages.currentPage().getParameters().get('eid'));
        VU_Event_Schedule__c ves = [Select ID,Event_Webinar__c from VU_Event_Schedule__c where ID=:ApexPages.currentPage().getParameters().get('eid')];
    //CH.New.End        
     //  List<VU_Speakers__c> Speakers = [SELECT Name, Id FROM VU_Speakers__c];    
       List<VU_SpeakerSchedule_Map__c> Speakers = [SELECT Id, VU_Speakers__r.Name FROM VU_SpeakerSchedule_Map__c ]; 
        allSpeakers = new List<SelectOption>();
        
        selectedSpeakers = new List<SelectOption>();
      //  for ( VU_Speakers__c c : Speakers ) 
      //CH.Old.Start
      /*
        for ( VU_SpeakerSchedule_Map__c c : Speakers ) 
        {
            allSpeakers.add(new SelectOption(c.id , c.VU_Speakers__r.Name));
        }*/
      //CH.Old.End        
      //CH.New.Start        
      List<VU_Speaker_Event_Map__c> lstSpk = [Select ID,Event_Webinar__c,VU_Speakers__r.LastName__c, VU_Speakers__r.Name from VU_Speaker_Event_Map__c where Event_Webinar__c=:ves.Event_Webinar__c ORDER BY VU_Speakers__r.LastName__c];
      for ( VU_Speaker_Event_Map__c c1 : lstSpk) 
        {
            allSpeakers.add(new SelectOption(c1.VU_Speakers__c, c1.VU_Speakers__r.LastName__c+' , '+c1.VU_Speakers__r.Name ));
        }
      //CH.New.End
    }

    public PageReference save() 
    {
        List<VU_SpeakerSchedule_Map__c> lstVUspeaker = new List<VU_SpeakerSchedule_Map__c>();
        message = 'Selected Speakers: ';
        Boolean first = true;
           system.debug('SSSS'+selectedSpeakers );
        for ( SelectOption so : selectedSpeakers ) 
        {
            if (!first) 
            {
                message += ', ';
            }
            message += so.getLabel() + ' (' + so.getValue() + ')';
            first = false;
            VU_SpeakerSchedule_Map__c v1 = new VU_SpeakerSchedule_Map__c(VU_Event_Schedule__c=ApexPages.currentPage().getParameters().get('eid'), VU_Speakers__c =so.getValue());
           // v1.id=id.valueOf(so.getValue());
            lstVUspeaker.add(v1);
         
        }
       if(lstVUspeaker.size()>0)
        {
            system.debug('XXXX'+lstVUspeaker);
           insert lstVUspeaker;
        }
        system.debug(ApexPages.currentPage().getParameters().get('eid'));
        PageReference pr = new PageReference('/'+ApexPages.currentPage().getParameters().get('eid'));
        return pr; 
    }
}
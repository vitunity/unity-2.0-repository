@isTest
public class SpeakerControlerTest {
    static testMethod void unitTest1()
    {
        Event_Webinar__c eve = new Event_Webinar__c();
        insert eve;
        VU_Speakers__c vuSpeaker = new VU_Speakers__c(name='FirstName',LastName__c='LastName');
        insert vuSpeaker;
        VU_Speaker_Event_Map__c vuSpeakerEvent = new VU_Speaker_Event_Map__c();
        vuSpeakerEvent.Event_Webinar__c=eve.id;
        vuSpeakerEvent.VU_Speakers__c=vuSpeaker.id;
        insert vuSpeakerEvent;       
        VU_Event_Schedule__c vuEvent = new VU_Event_Schedule__c();
        vuEvent.Name='Test';
        vuEvent.Date__c=system.today();
        vuEvent.Start_Time__c=dateTime.now();
        vuEvent.End_Time__c=DateTime.now();
        vuEvent.Event_Webinar__c=eve.id;
        insert vuEvent;
        
        VU_SpeakerSchedule_Map__c V= new VU_SpeakerSchedule_Map__c();
        v.VU_Event_Schedule__c= vuEvent.id;
        v.VU_Speakers__c= vuSpeaker.id;
        insert v;
        //v= [select Id,Name from VU_SpeakerSchedule_Map__c where Id=:v.id ];
        
        PageReference myPage = Page.SelectSpeaker;
        Test.setCurrentPageReference(myPage);
        ApexPages.currentPage().getParameters().put('eid',vuEvent.Id);
        SpeakerControler speaker = new SpeakerControler ();
        List<SelectOption> opt = speaker.selectedSpeakers;
        speaker.save();
    }
}
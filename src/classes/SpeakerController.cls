/*@RestResource(urlMapping='/VUSpeaker/*')
    global class SpeakerController 
    {
    @HttpGet
    global static List<VU_SpeakerSchedule_Map__c > getBlob() {
    List<VU_SpeakerSchedule_Map__c > a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('SpeakerId') ;
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
          a = [SELECT Id,Name,VU_Event_Schedule__c,VU_Event_Schedule__r.Event_Webinar__c,VU_Speakers__c,VU_Event_Schedule__r.Date__c,VU_Event_Schedule__r.Moderator_Email__c,VU_Event_Schedule__r.Moderator_Phone__c,VU_Event_Schedule__r.End_Time__c, VU_Event_Schedule__r.Location__c, VU_Event_Schedule__r.Name, VU_Event_Schedule__r.Description__c, VU_Event_Schedule__r.Start_Time__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.Language__c, VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c,VU_Speakers__r.Description__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Map__c WHERE VU_Speakers__c=:Id AND (VU_Event_Schedule__r.Language__c='English(en)' OR VU_Event_Schedule__r.Language__c='' OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes ('English(en)'))];
}
else
{
    a = [SELECT Id,Name,VU_Event_Schedule__c,VU_Event_Schedule__r.Event_Webinar__c,VU_Speakers__c,VU_Event_Schedule__r.Date__c,VU_Event_Schedule__r.End_Time__c, VU_Event_Schedule__r.Moderator_Email__c,VU_Event_Schedule__r.Moderator_Phone__c,VU_Event_Schedule__r.Location__c, VU_Event_Schedule__r.Name, VU_Event_Schedule__r.Description__c, VU_Event_Schedule__r.Start_Time__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.Language__c, VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c,VU_Speakers__r.Description__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Map__c WHERE VU_Speakers__c=:Id AND (VU_Event_Schedule__r.Language__c=:Language OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes (:Language))];
 }
    return a;
    } 
    }*/
    
    
    @RestResource(urlMapping='/VUSpeaker/*')
    global class SpeakerController 
    {
    @HttpGet
    global static List<VU_SpeakerSchedule_Map__c > getBlob() {
    List<VU_SpeakerSchedule_Map__c > a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('SpeakerId') ;
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='Chinese(zh)'){
          a = [SELECT Id,Name,VU_Event_Schedule__c,VU_Event_Schedule__r.Event_Webinar__c,VU_Speakers__c,VU_Event_Schedule__r.Date__c,VU_Event_Schedule__r.Moderator_Email__c,VU_Event_Schedule__r.Moderator_Phone__c,VU_Event_Schedule__r.End_Time__c, VU_Event_Schedule__r.Location__c, VU_Event_Schedule__r.Name, VU_Event_Schedule__r.Description__c, VU_Event_Schedule__r.Start_Time__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.Language__c, VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c,VU_Speakers__r.Description__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Map__c WHERE VU_Speakers__c=:Id AND (VU_Event_Schedule__r.Language__c='English(en)' OR VU_Event_Schedule__r.Language__c='' OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes ('English(en)'))];
}
else
{
    a = [SELECT Id,Name,VU_Event_Schedule__c,VU_Event_Schedule__r.Event_Webinar__c,VU_Speakers__c,VU_Event_Schedule__r.Date__c,VU_Event_Schedule__r.End_Time__c, VU_Event_Schedule__r.Moderator_Email__c,VU_Event_Schedule__r.Moderator_Phone__c,VU_Event_Schedule__r.Location__c, VU_Event_Schedule__r.Name, VU_Event_Schedule__r.Description__c, VU_Event_Schedule__r.Start_Time__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.Language__c, VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c,VU_Speakers__r.Description__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Map__c WHERE VU_Speakers__c=:Id AND (VU_Event_Schedule__r.Language__c=:Language OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes (:Language))];
 }
    return a;
    } 
    }
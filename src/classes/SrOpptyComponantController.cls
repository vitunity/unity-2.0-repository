public class SrOpptyComponantController {
	public Id OpptyId{get;set;} 
	public list<SVMXC__Service_Contract_Products__c> lstInstalledProduct {get;set;}
	//public list<SVMXC__Service_Contract__c> objServiceContract {get;set;}
	
	public SVMXC__Service_Contract__c getOppty() {
		SVMXC__Service_Contract__c  objServiceContract = [SELECT Id,  Location__r.name,Renewal_Opportunity__r.Name,Renewal_Opportunity__r.Closedate  ,Name,SVMXC__End_Date__c,
			SVMXC__Company__r.name ,    Renewal_Opportunity__r.id,  Renewal_Opportunity__r.CreatedDate FROM SVMXC__Service_Contract__c
            Where Renewal_Opportunity__c=:OpptyId limit 1]; 
        	//system.debug('----In Componant--'+objServiceContract );
    	return objServiceContract  ;
 	}
 	
    public list<string> getInstalledProduct() {
    	List<string> lstIPName=new list<string>();
    	lstInstalledProduct=[select id, Name,SVMXC__Service_Contract__c,SVMXC__Installed_Product__r.Name,SVMXC__Service_Contract__r.Renewal_Opportunity__r.id from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__r.Renewal_Opportunity__r.id =:OpptyId LIMIT 1000];
     	if(lstInstalledProduct.size()>0) {
			for(SVMXC__Service_Contract_Products__c varIP: lstInstalledProduct)
	    	{
	    		lstIPName.add(varIP.SVMXC__Installed_Product__r.Name);
	    	}
    	}
    	return lstIPName;
    }
}
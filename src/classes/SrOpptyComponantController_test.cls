@isTest
public class SrOpptyComponantController_test {

    public static testMethod void testSrOpptyComponantController() {
        //Dummy data crreation 
        Profile p = new profile();
        p = [Select id from profile where name = 'System Administrator'];
        User systemuser = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1]; 

        system.runas(systemuser){
        Account testAcc = AccountTestData.createAccount();
        insert testAcc;
        Opportunity testOpportunity = new opportunity(AccountId = testAcc.Id,name = 'testOpp', StageName='Prospect',CloseDate = system.today(), Deliver_to_Country__c = 'USA');
        insert testOpportunity;
        SVMXC__Service_Contract__c testServiceContract = new SVMXC__Service_Contract__c(name = 'test service contract',Renewal_Opportunity__c = testOpportunity.id,Renewal_Opportunity__r = testOpportunity,SVMXC__Start_Date__c = system.today(),SVMXC__End_Date__c = system.today()+5,SVMXC__Company__c = testAcc.id);
        insert testServiceContract;
        SVMXC__Service_Contract_Products__c testServiceContractProducts = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__r = testServiceContract,SVMXC__Service_Contract__c = testServiceContract.id);
        insert testServiceContractProducts;
        SrOpptyComponantController testcontroller = new SrOpptyComponantController();
        testcontroller.OpptyId = testOpportunity.id;
        testcontroller.getOppty();
        testcontroller.getInstalledProduct();
        /*SVMXC__Installed_Product__c testInstalledProduct = new SVMXC__Installed_Product__c(name = 'H156778');
        insert testInstalledProduct;
        SRCheckContractOnInstalledProduct controller = new SRCheckContractOnInstalledProduct(new ApexPages.StandardController(testInstalledProduct));*/
       
        }
      }
}
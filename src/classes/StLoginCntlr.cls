/**************************************************************************\
    @ Author        : Balraj Singh
    @ Date      : 03-Apr-2013
    @ Description   : An Apex class for Site Login.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
global class StLoginCntlr {

    public boolean chkUser {get; set;}
    public boolean conLungPhantom {get; set;}
    public boolean IsProductTrueBeam{get; set;}
    //public Contact c{get;set;}
    public Boolean usrCont;  
    public Boolean IsMarketYourCenter{get;set;} 
    public Boolean isModelAnalyticsAccess{get;set;}  //STSK0012079.Rakesh
    List<User> portalUser;
    List<MarketingKitRole__c> markkit; 
    public String cookieTokenUrl{get;set;}
    public String exactURL{get;set;}
    public Boolean ready{get; set;}
    public Boolean corrlog{get; set;}
    public Boolean corrlog1{get; set;}
    public Boolean IsPassExist{get; set;}
    public Boolean IsFakeResponse{get; set;}
    public Boolean AccntStatus{get;set;}
    public Boolean AccntStatus1{get;set;}
    public Boolean isinternaluser{get;set;}
    public Boolean isKPIAccess{get;set;}
    public Boolean isPartneruser{get;set;}
    public Boolean isPartnerChanneluser{get;set;}
    public String UserNameText;
    public String Createsupportcase{get;set;}
    public String  ProductImprovementRequest{get;set;}
    set<String> countryset = new set<String>();
    public Boolean isOCSUGC{get;set;}
    public Boolean renderPageMessage{get;set;}
    public Boolean IsPTUser{get; set;}
    
    /**
     *  @author         :   Puneet Mishra
     *  @description    :   Logic for Varian Marketplace, Logic will enable/disable Varian Marketplace web link on MyVarian home page
     */
    public Boolean getEnableVMarketPlace() {
        Map<String, vMarket_LiveControls__c> controlvMarketMap = new Map<String, vMarket_LiveControls__c>();
        // If custom setting has data, populate map else return false
        if(!vMarket_LiveControls__c.getAll().isEmpty()) {
            for(vMarket_LiveControls__c control : vMarket_LiveControls__c.getAll().values())
                controlvMarketMap.put(control.Country__c, control);
        } else {
            return false;
        }
        
        User loggedIn = new User();
        loggedIn = [SELECT Id, ContactId FROM USER WHERE Id =: userInfo.getUserId()];
        if(loggedIn.ContactId != null ){
            Contact cont = new Contact();
            cont =[SELECT Id, Account.Country__c, AccountId FROM Contact WHERE Id =: loggedIn.ContactId];
            if(controlvMarketMap.containsKey(cont.Account.Country__c) && controlvMarketMap.get(cont.Account.Country__c).vMarket_MyVarianWeblink__c) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isSanctionedCountries{get;set;}
    //Added Installed Product Check - API Key App       
    public Boolean eligibleCustomerFlag{get;set;}       
    public Boolean eligibleInternalUserFlag{get;set;}
    
    public static list < OCSUGC_Approved_Countries__c > getAllCountriesContact() {
        return OCSUGC_Approved_Countries__c.getall().values();
    }

    public static list < MarketingYourCenterCountries__c > getAllMrkUrCntrAccessCountries() {
        return MarketingYourCenterCountries__c.getall().values();
    }

    public void testLearningcenter() {
        system.debug('@@in test');
        User Un = [Select u.Contact.RAQA_Contact__c, u.Contact.Is_MDADL__c, u.Contact.Is_Lung_Phantom_Contact__c, u.Contact.AccountID, u.Contact.Distributor_Partner__c, u.Contact.Account.country1__r.Name, ContactId, u.LMS_Status__c From User u where id = : userinfo.getUserId()];
        if (Un.LMS_Status__c == 'Request') {
            String errorMessgage = System.Label.MV_LMS_AccPendingMsg;
            renderPageMessage = true;
            ApexPages.Message msgBlock = new ApexPages.Message(ApexPages.Severity.Info, errorMessgage);
            ApexPages.addMessage(msgBlock);
        }
    }
    
    global StLoginCntlr() {
        isModelAnalyticsAccess  = false; //STSK0012079.Rakesh
        isKPIAccess = false;
        renderPageMessage = false;
        corrlog = false;
        IsMarketYourCenter = false;
        usrCont = false;
        AccntStatus = false;
        AccntStatus1 = false;
        conLungPhantom = false;
        isinternaluser = false;
        isPartneruser = false;
        chkUser = false;
        IsPTUser = false;
        exactURL = ApexPages.currentPage().getURL();
        system.debug('############# URL= ' + exactURL);
        ProductImprovementRequest = 'notpir';
        Createsupportcase = 'notcreatespport';
                            
        //Validating - Installed Product - API Key App      
        checkInstalledProductEligibilty();
        
        String conCntry = [Select Id, Contact.MailingCountry From User where id = : Userinfo.getUserId() limit 1].Contact.MailingCountry;
        if (conCntry != null) {
            /*list<OCSUGC_Approved_Countries__c> AllCount = getAllCountriesContact();  
            for(OCSUGC_Approved_Countries__c Countries: AllCount)  
            { 
             if(Countries.name==conCntry)
             { 
              isOCSUGC=TRUE;
             }
            } */
            list < MarketingYourCenterCountries__c > AllCountryCount = getAllMrkUrCntrAccessCountries();
            for (MarketingYourCenterCountries__c MrktCountries: AllCountryCount) {
                if (MrktCountries.Name == conCntry) {
                    IsMarketYourCenter = true;
                }
            }
        }
        /*PD- This piece of code need to be merged with other public group query. this is done just fro POC....*/
        List < String > Idsp = New List < String > ();
        List < GroupMember > grpptmember = new List < GroupMember > ();
        grpptmember = [Select GroupId, UserOrGroupId, Group.Name from GroupMember where (Group.Name = 'VMS PT - Installation' or Group.Name ='PT - Installation Ext')  and UserOrGroupId = : UserInfo.getuserId()];
        for (GroupMember gm: grpptmember ) {
            Idsp.add(gm.UserOrGroupId);
        }
        
        if (Idsp.size() > 0) {
            IsPTUser = true;
        }

        User Un = [Select u.is_Model_Analytic_Member__c, u.KPI_App_Access__c,u.Contact.RAQA_Contact__c, u.Contact.Is_MDADL__c, u.Contact.Is_Lung_Phantom_Contact__c, u.Contact.AccountID, u.Contact.Distributor_Partner__c, u.Contact.Account.country1__r.Name, ContactId, u.LMS_Status__c From User u where id = : userinfo.getUserId()];

        if (Un.KPI_App_Access__c == true) {
            isKPIAccess = true;
        }
        //STSK0012079.Rakesh.Start
        if(un.is_Model_Analytic_Member__c){
            isModelAnalyticsAccess =true;
        }
        //STSK0012079.Rakesh.Start
        if (Un.contactid == null) {
            isinternaluser = true;
        }
        
        if (un.Contact.Distributor_Partner__c){
            chkUser = true;
        }
        
        if (Un.LMS_Status__c == 'Active') {
            AccntStatus = true;
        }
        
        if (Un.LMS_Status__c == 'Request') {
            AccntStatus1 = true;

        }

        isSanctionedCountries = false;
        if(Un.ContactId <> null && un.Contact.AccountId <> null && Un.Contact.Account.country1__c <> null){
            Sanctioned_Countries__c csett = Sanctioned_Countries__c.getValues(un.Contact.Account.country1__r.Name);
            if(csett <> null){
                isSanctionedCountries = true;
            }
        }

        //countryset.addall(OCSUGC_Approved_Countries__c.getall().values());
        for (Support_Case_Approved_Countries__c countries: Support_Case_Approved_Countries__c.getall().values()) {
            countryset.add(countries.name);
        }
        if (countryset.contains(Un.Contact.Account.country1__r.Name) && chkUser == false)

        {
            chkUser = true;

        }
        List < String > Ids = New List < String > ();
        List < GroupMember > grplmember = new List < GroupMember > ();
        grplmember = [Select GroupId, UserOrGroupId, Group.Name from GroupMember where Group.Name = : Label.VMS_MyVarian_Partners and UserOrGroupId = : UserInfo.getuserId()];
        for (GroupMember gm: grplmember) {
            Ids.add(gm.UserOrGroupId);
        }
        SYSTEM.DEBUG('^^^^' + Ids);
        if (Ids.size() > 0) {
            isPartneruser = true;
        }
        List < String > IdPartnerChannel = New List < String > ();
        List < GroupMember > grplmembers = new List < GroupMember > ();
        grplmembers = [Select GroupId, UserOrGroupId, Group.Name from GroupMember where Group.Name = : Label.VMS_MyVarian_Partner_Channel and UserOrGroupId = : UserInfo.getuserId()];
        for (GroupMember gm: grplmembers) {
            IdPartnerChannel.add(gm.UserOrGroupId);
        }
        SYSTEM.DEBUG('^^^^IdPartnerChannel' + IdPartnerChannel);
        if (IdPartnerChannel.size() > 0) {
            isPartnerChanneluser = true;
        }
        /*    Map<id,List<Contact_Role_Association__c>> RAQAContact = new map<id,List<Contact_Role_Association__c>>();
              List<Contact_Role_Association__c> contactlist = new List<Contact_Role_Association__c>();
         for(Contact_Role_Association__c c:[Select Contact__c,Account__c from Contact_Role_Association__c where Role__c = 'RAQA' and Contact__c =: Un.ContactId ])      
           {
           if(!RAQAContact.containsKey(c.Account__c))  
                  {       
                    contactlist.add(c);      
                    RAQAContact.put(c.Account__c,contactlist);     
                  }        
                  else      
                   {        
                     contactlist= RAQAContact.get(c.Account__c);    
                     contactlist.add(c);     
                     RAQAContact.put(c.Account__c,contactlist);   
                   }
                System.debug('Test'+RAQAContact);
           } 
           if(RAQAContact.containsKey(Un.Contact.AccountID))       
             {
               chkUser=true;
             }     */




        for (Contact_Role_Association__c CRA: [Select Account__c, Account__r.Country1__r.Name from Contact_Role_Association__c where Contact__c = : Un.ContactId and Account__c != NULL]) {
            if (countryset.contains(CRA.Account__r.Country1__r.Name) && chkUser == false) {

                chkUser = true;
            }


        }

        conLungPhantom = Un.Contact.Is_Lung_Phantom_Contact__c;

        system.debug('Un == ' + Un);
        portalUser = [Select u.Id, u.ContactId, u.AccountId From User u where id = : UserInfo.getUserId() limit 1];
        system.debug('-------------->' + portalUser[0].AccountId);
        if (portalUser.size() > 0) {
            String ProdPartNum = system.label.PartNumber;
            markkit = [select id, name, Product__r.Model_Part_Number__c from MarketingKitRole__c where Product__r.Model_Part_Number__c = : ProdPartNum and Account__r.id = : portalUser[0].AccountId limit 1];
            if (markkit.size() > 0) {
                IsProductTrueBeam = true;
            } else {
                IsProductTrueBeam = false;
            }

        }
    }

    global transient String strMsg = '';
    global String username {get;set;}
    global String password {get;set;}
    global String RelayState {get;set;}
    global String pgName {get;set;}

    global String propMessage {
        get {
            return strMsg;
        }
        set {
            strMsg = value;
        }
    }
    
    List < user > u = new List < user > ();
    
    global PageReference login() {

        system.debug('Username' + username);

        u = [Select u.Contact.RAQA_Contact__c, u.Contact.Is_MDADL__c, u.Contact.Is_Lung_Phantom_Contact__c, u.Contact.AccountID, ContactId
            From User u where Username = : username limit 1
        ];

        if (u.size() > 0) {
            conLungPhantom = u[0].Contact.Is_Lung_Phantom_Contact__c;
            usrCont = u[0].Contact.Is_MDADL__c;
        }
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        string prdId = System.currentPageReference().getParameters().get('prdnameurl');
        System.debug('prdId' + prdId);
        system.debug('############# startUrl= ' + startUrl);
        //String startUrl = System.currentPageReference().getParameters().get('chkURL');
        //system.debug('usrCont == '+usrCont);
        system.debug('############# URL1==== ' + exactURL);


        if (usrCont) {
            startUrl = '/apex/cpMDADLLungPhantum';
        } else if ((startUrl == '' || startUrl == null) && !exactURL.contains('rapidplandemo')) {
            startUrl = '/apex/CpHomePage';

            system.debug('############# URL1= ' + exactURL);
            system.debug(' -------------- -check ');
        } else if ((startUrl == '' || startUrl == null) && exactURL.contains('rapidplandemo')) {
            startUrl = '/apex/rapidplandemo';
            system.debug('############# URL2= ' + exactURL);
        } else if (prdId != null) {
            system.debug('-------- --------- ' + prdId);
            startUrl = '/apex/CpEventPresList?Id=' + prdId;
            system.debug('############# URL3= ' + exactURL);
        }
        system.debug('------------ ---------------- ----------- ' + username + '--------- pwd ----- ' + password);
        PageReference pr = Site.login(username, password, startUrl);

        if (ApexPages.hasMessages()) {
            ApexPages.Message[] apm = ApexPages.getMessages();
            for (ApexPages.Message am: apm) {
                if (strMsg != null && strMsg != '')
                    strMsg = strMsg + '  ' + am.getSummary();
                else
                    strMsg = am.getSummary();
            }
            system.debug('+++++' + strMsg);
        } else {
            strMsg = '';
        }


        username = '';
        UserNameText = '';
        password = '';
        system.debug(' ----------- Prrr --------- ' + pr);
        return pr;
    }
    
    public PageReference RedirectExternal() {
            String ProfileName = [Select Id, Profile.name From User where id = : Userinfo.getUserId() limit 1].Profile.name;
            if (ProfileName == 'VMS MyVarian - Customer User') {
                pagereference p = new pagereference('/apex/CpHomePage');
                p.setredirect(true);
                return p;
            } else {
                return null;
            }
            
        }
    global pagereference RedirectLeaningCenter(){
        try{
            User usr = [Select Id, LMS_Last_Access_Date__c From User where id = : Userinfo.getUserId() limit 1];
            usr.LMS_Last_Access_Date__c = system.now();
            update usr;
            pagereference p = new pagereference(Label.CpTrainingSABAurl);
            p.setredirect(true);
            return p;
        }catch(Exception e){
            return null;
        }       
    }

    global pagereference RedirectLeaningCenter2(){
        try{
            User usr = [Select Id, LMS_Last_Access_Date__c From User where id = : Userinfo.getUserId() limit 1];
            usr.LMS_Last_Access_Date__c = system.now();
            update usr;
            pagereference p = new pagereference('http://www.variantraining.com/');
            p.setredirect(true);
            return p;
        }catch(Exception e){
            return null;
        }       
    }
    

    public pagereference gotoCourseListing(){
        if(UserInfo.getUserType() <> 'Standard'){
            try{
            User usr = [Select Id, Contact.Territory__c From User where id = : Userinfo.getUserId() limit 1];       
            Cookie UTerritory = new Cookie('UTerritory', usr.Contact.Territory__c,null,-1,false);
            ApexPages.currentPage().setCookies(new Cookie[]{UTerritory});
            }catch(Exception e){system.debug('Enable to set Cookies:'+e.getMessage());}
        }
        pagereference pg = new pagereference('/apex/CpTrainingandCourse');
        pg.setredirect(true);
        return pg;
    }
        //Okta login method 
    public PageReference LogintoOkta() {
        String fullRecordURL = URL.getCurrentRequestUrl().toExternalForm();
        System.debug('fullRecordURL --->' + fullRecordURL);
        List < String > SplitRecordURL = new List < String > ();
        List < String > SplitRecordURL2 = new List < String > ();



        // Following condition will be working for Event/Presentation/Webinar/Training Course/Product Doc detail Pages that contain id and ref url or in case of access denied page.
        if (fullRecordURL.contains('&refURL') || fullRecordURL.contains('?id')) {
            String Values = System.currentPageReference().getParameters().get('id');
            String fullRecordStarturl = System.currentPageReference().getParameters().get('startURL');
            
            if((Values !=null) || (fullRecordStarturl!=null)) //Kalyan Persist startURL into the relaystate if available
            {
                if (fullRecordStarturl != null) {
                    System.debug('testurl --->' + fullRecordStarturl);
                    RelayState = '&RelayState=' + Label.cp_site + fullRecordStarturl;
                } else {
                    SplitRecordURL = fullRecordURL.split('&refURL');
                    System.debug('SplitRecordURL0-->' + SplitRecordURL[0]);
                    //SplitRecordURL2=SplitRecordURL[1].split('.com');
                    RelayState = '&RelayState=' + SplitRecordURL[0];
                    System.debug('RelayState0---->' + RelayState);
                }
            } else {
                SplitRecordURL = fullRecordURL.split('&refURL');
                System.debug('SplitRecordURL0-->' + SplitRecordURL[1]);
                SplitRecordURL2 = SplitRecordURL[1].split('.com');
                RelayState = '&RelayState=' + Label.cp_site + SplitRecordURL2[1];
                System.debug('RelayState0---->' + RelayState);
            }

        }

        // For Batch Notification File Download  
        if (fullRecordURL.contains('servlet'))  {    SplitRecordURL = fullRecordURL.split('refURL');    System.debug('SplitRecordURL-->' + SplitRecordURL[1]);     SplitRecordURL2 = SplitRecordURL[1].split('.com');     RelayState = '&RelayState=' + Label.cp_site + SplitRecordURL2[1];    System.debug('RelayState---->' + RelayState); }


        //System.debug('fullRecordURL --->'+EncodingUtil.urlDecode(RelayState,'UTF-8'));
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();HttpResponse res = new HttpResponse();Http http = new Http();
        //Construct Authorization and Content header
        //Blob headerValue = Blob.valueOf(username+':'+password);
        String strToken = Label.oktatoken; //'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
        String authorizationHeader = 'SSWS ' + strToken;   req.setHeader('Authorization', authorizationHeader); req.setHeader('Content-Type', 'application/json');req.setHeader('Accept', 'application/json');
        //req.setHeader('username',username);
        //req.setHeader('password',password);

        //   String body = '{ "username": "'+username+'", "password": "'+password+'", "RelayState": "'+EncodingUtil.urlDecode(RelayState,'UTF-8')+'"}';
        String usrnm = ApexPages.currentPage().getParameters().get('user');    String passw = ApexPages.currentPage().getParameters().get('pass'); system.debug('Username--> ' + usrnm + ' password --> ' + passw);  String body = '{ "username": "' + usrnm + '", "password": "' + passw + '"}';req.setBody(body);
        /*
        req.setBody('Content-Type=' + EncodingUtil.urlEncode('application/json', 'UTF-8') +
                        '&charset=' + EncodingUtil.urlEncode('UTF-8', 'UTF-8') +
                        '&Accept=' + EncodingUtil.urlEncode('application/json', 'UTF-8') +
                        '&grant_type=' + EncodingUtil.urlEncode('password', 'UTF-8') +              
                            '&username=' + EncodingUtil.urlEncode(username, 'UTF-8') + 
                             '&password=' + EncodingUtil.urlEncode(password, 'UTF-8')                  
                        );
        */
        //Construct Endpoint



        String endpoint = label.oktaendpoint; //'https://varian.okta.com/api/v1/sessions?additionalFields=cookieTokenUrl';
        // endpoint=endpoint+ EncodingUtil.urlDecode(RelayState,'UTF-8');
        System.debug('EndPoint --->' + endpoint);
        //Set Method and Endpoint and Body
        req.setMethod('POST');   req.setEndpoint(endpoint);

        //String cookieTokenUrl = null; 
        String UserId;  String SessionId;
        try {
            //Send endpoint to OKTA
            if (!Test.isRunningTest()) {
                res = http.send(req);

            } else {
                res = fakeresponse.fakeresponseloginmethod(IsFakeResponse);
            }
            system.debug('aebug ; ' + res.getBody());
            JSONParser parser = JSON.createParser(res.getBody());
             /* STSK0012350, Puneet Mishra : this Logic fetching wrong sessionId and is not correctly written
            Depricated CODE
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if (fieldName == 'cookieToken') {
                        cookieTokenUrl = parser.getText();
                    }
                    if (fieldName == 'userId') {
                        UserId = parser.getText();
                    }
                    if (fieldName == 'id') {
                        SessionId = parser.getText();
                    }

                }
            }*/
            MyVarianJSONParser pars = MyVarianJSONParser.parse(res.getBody());
            SessionId = pars.id;
            UserId = pars.userId;
            cookieTokenUrl = pars.cookieToken;
            // STSK0012350 : parser Logic ends here
            String OktaStatus;
            IF(cookieTokenUrl == NULL) {      String Userid1;
                //Construct HTTP request and response
                HttpRequest req1 = new HttpRequest();    HttpResponse res1 = new HttpResponse();     Http http1 = new Http();
                //Construct Authorization and Content header
                //Blob headerValue = Blob.valueOf(username+':'+password);
                String strToken1 = Label.oktatoken; //'00jv-Dft-6mY5T5zjNCueoSHY4AbKAWYe_-eK7_x9q';
                String authorizationHeader1 = 'SSWS ' + strToken1;req1.setHeader('Authorization', authorizationHeader1);      req1.setHeader('Content-Type', 'application/json');    req1.setHeader('Accept', 'application/json');

                //   String endpoint1 = 'https://varian.okta.com/api/v1/users/'+Cont.OktaId__c;
                String endpoint1 = 'https://varian.okta.com/api/v1/users/' + usrnm; //username;



                //Set Method and Endpoint and Body
                req1.setMethod('GET');      req1.setEndpoint(endpoint1);     if (!Test.isRunningTest()) {   res1 = http.send(req1);    }       ELSE {      res1 = fakeresponse.fakeresponsemethod('getuseremail');     }   system.debug('aebug ; ' + res1.getBody());     JSONParser parser1 = JSON.createParser(res1.getBody());                while (parser1.nextToken() != null) {  if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME)) { String fieldName1 = parser1.getText();     parser1.nextToken();   if (fieldName1 == 'id') {   Userid1 = parser1.getText();    }  if (fieldName1 == 'status') {   OktaStatus = parser1.getText();    System.debug('Check Lock-->' + OktaStatus);   } }
                }
                if (!(String.ISBlank(Userid1)) && OktaStatus != 'LOCKED_OUT' && OktaStatus != 'DEPROVISIONED') // != null
                {
                    system.debug('@@@@@@@' + Userid1);
                    IsPassExist = true;
                    System.debug('password is incorrect----->' + OktaStatus);
                } else {
                    IsPassExist = false;
                    System.debug('user not exist----->');

                }

            }
            if (cookieTokenUrl != null) {
                ready = true;
                System.debug('cookieTokenUrl = ' + cookieTokenUrl);
            } else { if (IsPassExist != null && IsPassExist == true) {    System.debug('Work--->');    IsPassExist = true;  } else if (OktaStatus == 'LOCKED_OUT') {    System.debug('DEPROVISIONED--->');  corrlog1 = true;   } else  corrlog = true;   }

        } catch (System.CalloutException e) {
            System.debug(res.toString());
        }

        //Set the cookie token

        //Cookie myCookies=new Cookie('mycookie',cookieTokenUrl,null,-1,false);
        //ApexPages.currentPage().setCookies(new Cookie[]{myCookies});
        //PageReference pr = new PageReference('/apex/CpSSOChild?token='+cookieTokenUrl);
        //pr.setCookies(new Cookie[]{myCookies});        
        //pr.setRedirect(true);

        //System.debug('aebug +pr.getCookies = '+pr.getCookies());        
        //System.debug('aebug +pr = '+pr); 
        //return pr;
        /*if(!ready)
         {
          return new pagereference('http://sfpq-myvarian.cs7.force.com?msage=true'); 
         }
         else */
        //return null;
        if (cookieTokenUrl != null) {
            User u = new user();
            u = [Select id from user where Contact.OktaId__c = : UserId and isactive = true Limit 1];
            Map < String, usersessionids__c > sessionmap = new map < String, usersessionids__c > ();
            // Changes for STSK0012350, Puneet Mishra
            //sessionmap = usersessionids__c.getall();
            //usersessionids__c usersessionrecord = new usersessionids__c(name = u.id, session_id__c = SessionId);
            /*if (!sessionmap.containskey(u.id)) {
                insert usersessionrecord;
            } else {
                usersessionrecord.Id = sessionmap.get(u.id).id;
                update usersessionrecord;
            }*/
            usersessionids__c usersession;
            List<usersessionids__c> usersessionrecord = new List<usersessionids__c>();
            usersessionrecord = [SELECT Id, Name, session_Id__c, LastModifiedDate FROM usersessionids__c WHERE Name =: u.id ORDER By LastModifiedDate];
            List<usersessionids__c> recToDelete = new List<usersessionids__c>();
            // If multiple usersession rec's exists then delete 1
            if(usersessionrecord.size() > 1) {
                for(Integer i = 1; i < usersessionrecord.size(); i++) {
                    recToDelete.add(usersessionrecord[i]);
                }
                delete recToDelete;
            }
            // user session rec exists then update it with updated sessionId else insert
            if(usersessionids__c.getInstance(u.id) != null) {
                usersessionrecord[0].session_id__c = SessionId;
                update usersessionrecord[0];
            } else {
                usersession = new usersessionids__c(name = u.id, session_id__c = SessionId);
                insert usersession;
            }
            // Changes for STSK0012350 ends here
            
            if (RelayState != null) {  pagereference page = new pagereference(Label.oktacploginurl + cookieTokenUrl + RelayState);  page.setredirect(true);  return page;
            } else {
                pagereference page;
                system.debug('Create support case-->' + Createsupportcase);
                Createsupportcase = ApexPages.currentpage().getparameters().get('Createsupportcase');
                ProductImprovementRequest = ApexPages.currentpage().getparameters().get('ProductImprovementRequest');
                system.debug('Create support case-->' + Createsupportcase);
                if (Createsupportcase == 'createspport'){
                    page = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/CpCasePage');
                } else if (ProductImprovementRequest == 'pir') {
                    //page = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/CpProdImprove?Request=ProductImprovement');
                    page = new pagereference(Label.oktacploginurl + cookieTokenUrl + '&RelayState=' + Label.cp_site + '/apex/Submit_Product_Idea_v1');
                }
                else{
                   page = new pagereference(Label.oktacploginurl + cookieTokenUrl);
                    }
                    
                page.setredirect(true);
                return page;
                }
                } else {
                system.debug('@@@@@@' + corrlog);
                return null;
        }
    }
    
    //method for Validating - Installed Product - API Key App       
    private void checkInstalledProductEligibilty(){             
        apiKeyMgmtHomeController apiKeyObj = new apiKeyMgmtHomeController();        
        eligibleCustomerFlag = apiKeyObj.validateContactInstalledProduct();     
        eligibleInternalUserFlag = apiKeyObj.validateMyVarianInternalUser();                
    }
}
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 13-June-2013
    @ Description   :  Test class for StLoginCntlr class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@isTest(SeeAllData = true)
Public class StLoginCntlrTest{

    //Test Method for StLoginCntlr class when running as user whose Is_MDADL__c is true
    
    static testmethod void testStLoginController(){ 
        insertSAPSettings();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
           
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState'); 
            insert con;
            
            u = new User(alias = 'standt', LMS_Status__c = 'Request', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Contact_Role_Association__c cr = new Contact_Role_Association__c();
            cr.Account__c= a.Id;
            cr.Contact__c = u.Contactid;
            cr.Role__c = 'RAQA';
            
           insert cr;
       
        }
        
        Test.StartTest();
         user usr = [Select Id, ContactId, contact.Is_MDADL__c,contact.Date_of_Factory_Tour_Visit__c, UserName from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){

   
            PageReference pageref = Page.cpmyvarianhomepage;
            Test.setCurrentPage(pageRef);
            usr.contact.Is_MDADL__c = True;
            usr.contact.Date_of_Factory_Tour_Visit__c = System.Today();
            Update usr.contact;
            StLoginCntlr Login = new StLoginCntlr();
            Login.username = usr.Username;
            Login.login();
            Login.testLearningcenter();
            StLoginCntlr.getAllCountriesContact();
        }
         
        Test.StopTest();
     
    }
    
    static testmethod void testStLoginCntlr(){ 
        insertSAPSettings();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
           
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState'); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Contact_Role_Association__c cr = new Contact_Role_Association__c();
            cr.Account__c= a.Id;
            cr.Contact__c = u.Contactid;
            cr.Role__c = 'RAQA';
            
           insert cr;
       
        }
        
        Test.StartTest();
         user usr = [Select Id, ContactId, contact.Is_MDADL__c,contact.Date_of_Factory_Tour_Visit__c, UserName from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){

   
        PageReference pageref = Page.cpmyvarianhomepage;
        Test.setCurrentPage(pageRef);
        usr.contact.Is_MDADL__c = True;
        usr.contact.Date_of_Factory_Tour_Visit__c = System.Today();
        Update usr.contact;
        StLoginCntlr Login = new StLoginCntlr();
        Login.username = usr.Username;
        Login.login();
    }
         
        Test.StopTest();
     
    }
    
    //Test Method for StLoginCntlr class with startUrl and prdnameurl values
    
    Public static testmethod void testStLoginCntlr1(){ 
        insertSAPSettings();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
        }
        
        Test.StartTest();
         user usr = [Select Id, ContactId, UserName from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){

        
        Event_Webinar__c event = new Event_Webinar__c();
        event.Title__c = 'Test Event2';
        event.Location__c = 'India';
        event.To_Date__c = system.today()-1;
        event.From_Date__c = system.today()-2;
        event.Needs_MyVarian_registration__c=False;
        Insert event;
        
        
        PageReference pageref = Page.cpmyvarianhomepage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('startUrl','Test');
        ApexPages.currentPage().getParameters().put('prdnameurl',event.Id);
        StLoginCntlr Loginn = new StLoginCntlr();
        Loginn.username = usr.Username;
        Loginn.login();

        
    }
         
        Test.StopTest();
     
    }
    
    
    //Test Method for StLoginCntlr class when running as customer portal user
    
     static testmethod void testStLoginCntlr3(){ 
        insertSAPSettings();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,OktaId__c='00ub0oNGTSWTBKOLGLNR',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User

       
        }
        
        Test.StartTest();
         
         user usr = [Select Id, ContactId, UserName from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){

        PageReference pageref = Page.cpmyvarianhomepage;
        Test.setCurrentPage(pageRef);
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        StLoginCntlr Login = new StLoginCntlr();
        Login.username = usr.Username;
        Login.login();
        Login.IsFakeResponse=True;      
        Login.LogintoOkta();
        
        StLoginCntlr Login1 = new StLoginCntlr();
        Login1.username = usr.Username;
        Login1.login();
        Login1.IsFakeResponse=FALSE;
        
        Login1.LogintoOkta();
        
    }
         
        Test.StopTest();
     
    }

    //Test Method for StLoginCntlr class with Role__c as RAQA
    
    static testmethod void testStLoginCntlr4(){ 
        insertSAPSettings();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u;
            Account a;  
            
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState',OktaId__c= '00ub0oNGTSWTBKOLGLNR' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User

           List<Contact_Role_Association__c> cra = new List<Contact_Role_Association__c>();
           
             Contact_Role_Association__c cr1 = new Contact_Role_Association__c();
                cr1.Account__c= a.Id;
                cr1.Contact__c = u.Contactid;
                cr1.Role__c = 'RAQA';
                cra.add(cr1);
                         
           
           Contact_Role_Association__c cr = new Contact_Role_Association__c();
                cr.Account__c= a.Id;
                cr.Contact__c = u.Contactid;
                cr.Role__c = 'RAQA';
                cra.add(cr);
               
           
           map<Id,List<Contact_Role_Association__c>> controle = new map<Id,List<Contact_Role_Association__c>>();
              controle.put(cr1.id,cra);
           
       
        }
        
        Test.StartTest();
         
         user usr = [Select Id, ContactId, contact.Is_MDADL__c,contact.Date_of_Factory_Tour_Visit__c, UserName from User where email=:'standarduser@testorg.com' ];
         
         System.runAs (usr){

   
        PageReference pageref = Page.cpmyvarianhomepage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('name','https://sfsd-varian.cs8.force.com/apex/CpEventPresList?id=a0OL00000017sHvMAI&refURL=http%3A%2F%2Fsfsd-varian.cs8.force.com%2Fapex%2FCpEventPresList');
      
        ApexPages.currentPage().getParameters().put('&refURL','https://sfsd-varian.cs8.force.com/apex/CpEventPresList?id=a0OL00000017sHvMAI&refURL=http%3A%2F%2Fsfsd-varian.cs8.force.com%2Fapex%2FCpEventPresList');
        ApexPages.currentPage().getParameters().put('id','https://sfsd-varian.cs8.force.com/apex/CpEventPresList?id=a0OL00000017sHvMAI&refURL=http%3A%2F%2Fsfsd-varian.cs8.force.com%2Fapex%2FCpEventPresList');
        ApexPages.currentPage().getParameters().put('startURL','https://sfsd-varian.cs8.force.com/apex/CpEventPresList?id=a0OL00000017sHvMAI&refURL=http%3A%2F%2Fsfsd-varian.cs8.force.com%2Fapex%2FCpEventPresList');
        usr.contact.Is_MDADL__c = True;
        usr.contact.Date_of_Factory_Tour_Visit__c = System.Today();
        Update usr.contact;
        StLoginCntlr Login = new StLoginCntlr();
        Login.username = usr.Username;
        Login.login();
        Login.IsFakeResponse=True;
        
        Login.LogintoOkta();
        StLoginCntlr Login1 = new StLoginCntlr();
        Login1.username = usr.Username;
        Login1.IsFakeResponse=FALSE;
       
        Login1.cookieTokenUrl = null;
        Login1.LogintoOkta();
        
        
        //rakesh
        Login1.RedirectLeaningCenter2();
        Login1.RedirectLeaningCenter();
        Login1.RedirectExternal();
        Login1.testLearningcenter();
        Login1.pgName = 'test';
        Login1.propMessage = 'test';
        
    }
         
         
        Test.StopTest();
     
    }
    
    private static void insertSAPSettings(){
        //Proxy PHP URL
        PROXY_PHP_URL_DEV2__c proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
        proxyPHPURL.name='Proxy PHP URL';
        proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
        //SAP Endopoint URL
        SAP_ENDPOINT_URL_DEV2__c sapURL = new SAP_ENDPOINT_URL_DEV2__c();
        sapURL.name='SAP Endpoint URL';
        sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
        //SAP Log In Auth Token
        SAP_Login_Authorization__c sapLogInAuthUserName = new SAP_Login_Authorization__c();
        sapLogInAuthUserName.name='Username';
        sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
        SAP_Login_Authorization__c sapLogInAuthPwd = new SAP_Login_Authorization__c();
        sapLogInAuthPwd.name='Password';           
        sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
        INVOKE_SAP_FM_SETTINGS__c csObj = new INVOKE_SAP_FM_SETTINGS__c();
        csObj.name = 'THIRD PARTY SOFTWARE';
        csObj.FM_Call__c = 'ZSAAS_PROVIDE_3RDPARTYSOFT';
        try{
            insert proxyPHPURL;
            insert sapURL;
            insert sapLogInAuthUserName;
            insert sapLogInAuthPwd;        
            insert csObj ;
        }catch(Exception e){
            
        }         
    }
    
    /**
     *  @author         :           Puneet Mishra
     *  @sdescription   :           Test Method for getEnableVMarketPlace, wriiten for Varian MarketPlace Web Link Enabaling and Disabling 
     */
    private static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    private static testMethod void getEnableVMarketPlaceTest() {
        vMarket_LiveControls__c control = new vMarket_LiveControls__c(Name = 'MyVarianWebLink', Country__c = 'USA', vMarket_MyVarianWeblink__c = true);
            //insert control;
            
        Profile portal1 = [SELECT Id, UserType FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' Limit 1][0];
        
        Account acc = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'); 
        insert acc;
        
        Contact cont = new Contact(FirstName = 'Test', LastName = 'Contact', Email = 'Test@email.com', AccountId = acc.Id);
        insert cont;
        
        Test.StartTest();
            UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
            User customer1 = new User( Email = 'testCustomer@bechtel.com', Username = generateUserName('Customer', 'vMarket', 70), LastName = 'test',
                                        FirstName = 'Tester', Alias = 'test', IsActive = true, ProfileId = portal1.Id, LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US', 
                                        TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', ContactId = cont.Id, LMS_Status__c = 'Pending', vMarketTermsOfUse__c = true
                                        , vMarket_User_Role__c = 'Customer');
            insert customer1;
            system.runas(customer1) {
                StLoginCntlr contl = new StLoginCntlr();
                Boolean res = contl.getEnableVMarketPlace();
                //system.assertEquals(res, true);
            }
        Test.Stoptest();
    }
    public static String generateUserName(String prefix,String orgName,Integer size) {
        String userName;
        String orgId = UserInfo.getOrganizationId();
        // Org id - 18 , 1 for @ , 4 for .com
        Integer randomStringGeneratorLen = size - orgId.length() - prefix.length() - orgName.length() - 4 - 1;
        Blob blobKey = crypto.generateAesKey(256);
        String randomString = EncodingUtil.convertToHex(blobKey).substring(0,randomStringGeneratorLen);
        return prefix + randomString + orgId + '@' + orgName + '.com';
    }
    
}
/**
  * @author savon/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Created to set the Work Order field on Stock History Record per DE7019
  * 
  * TEST CLASS 
  * 
  *    NTest Class not created yet
  *    For Naming use the class StockHistoryClass_TEST
  *  
  * ENTRY POINTS 
  *  
  *    Called from StockHistoryBeforeTrigger 
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; 1-11-2016;  savon/Forefront; Initial Build
  * 
  **/
public class StockHistoryClass {

  public static void setStockHistoryWorkOrder (Set<Id> stockHistoryIds, Map<Id, SVMXC__Stock_History__c> triggerNewMap) {

    List <SVMXC__Stock_History__c> updateStockHistories = new List<SVMXC__Stock_History__c>();

    for (SVMXC__Stock_History__c sh : [SELECT Id, SVMXC__RMA_Line__r.SVMXC__Service_Order__c, SVMXC__Shipment_Line__r.SVMXC__Service_Order__c, SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c 
      FROM SVMXC__Stock_History__c WHERE Id IN : stockHistoryIds]) 
    {
      SVMXC__Stock_History__c tempSH = new SVMXC__Stock_History__c(Id = sh.Id);

      if (sh.SVMXC__RMA_Line__r.SVMXC__Service_Order__c != null) 
      {
        tempSH.SVMXC__Service_Order__c = sh.SVMXC__RMA_Line__r.SVMXC__Service_Order__c; 
        updateStockHistories.add(tempSH);
      } 
      else if (sh.SVMXC__Shipment_Line__r.SVMXC__Service_Order__c != null) 
      {
        tempSH.SVMXC__Service_Order__c = sh.SVMXC__Shipment_Line__r.SVMXC__Service_Order__c;
        updateStockHistories.add(tempSH);
      } 
      else if (sh.SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c != null) 
      {
        tempSH.SVMXC__Service_Order__c = sh.SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c;
        updateStockHistories.add(tempSH);
      }
    }

    if (!updateStockHistories.isEmpty()) {
      update updateStockHistories;
    }
  }

  public static void updateQtyChangeOnProductStock(list<SVMXC__Stock_History__c> shList){
      map<Id,SVMXC__Product_Stock__c> psMap = new map<Id,SVMXC__Product_Stock__c>();
      for(SVMXC__Stock_History__c sh : shList){
        if(sh.SVMXC__Product_Stock__c != null && sh.SVMXC__Quantity_after_change2__c != null){
          if(psMap.containsKey(sh.SVMXC__Product_Stock__c)){
            psMap.get(sh.SVMXC__Product_Stock__c).SVMXC__Quantity2__c = sh.SVMXC__Quantity_after_change2__c;
          }else{
            SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
            ps.Id = sh.SVMXC__Product_Stock__c;
            
          //DFCT0012188.Start
            //ps.SVMXC__Quantity2__c = sh.SVMXC__Quantity_after_change2__c;            
            if(sh.SVMXC__Quantity_after_change2__c > 0){
                ps.SVMXC__Quantity2__c = sh.SVMXC__Quantity_after_change2__c;
            }else{
                ps.SVMXC__Quantity2__c = 0;
            }  
          //DFCT0012188.End
            
            psMap.put(sh.SVMXC__Product_Stock__c,ps);
          }
        }
      }

      if(!psMap.isEmpty()){
        update psMap.values();
      }
  }

}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class StockHistoryClass_Test {

	public static Product2 newProd;
	public static SVMXC__Service_Group__c servTeam;
	public static SVMXC__Site__c newLoc;
	public static SVMXC__Stock_History__c stockHistory;
	public static SVMXC__Product_Stock__c prodStock;
	
	static {
		newProd = UnityDataTestClass.product_Data(true); 
		servTeam = UnityDataTestClass.serviceTeam_Data(true);
		newLoc = [	SELECT Id, ERP_CSS_District__c 
					FROM SVMXC__Site__c 
					WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1 ];
		prodStock = UnityDataTestClass.productStock_Data(true, newProd, newLoc);
		stockHistory = UnityDataTestClass.stockHistory_Data(false, newProd, newLoc, prodStock);
	}
	
	static testMethod void updateQtyChangeOnProductStock_QuantityAfterChange_NOTNULL_TEST() {
    	stockHistory.SVMXC__Quantity_after_change2__c = 6;
    	insert stockHistory;
    	
   		list<SVMXC__Stock_History__c> shList = new list<SVMXC__Stock_History__c>();
   		shList.add(stockHistory);
   		
   		StockHistoryClass.updateQtyChangeOnProductStock(shList);	
	}
	
	static testMethod void updateQtyChangeOnProductStock_QuantityAfterChange_NULL_TEST() {
    	insert stockHistory;
    	
   		list<SVMXC__Stock_History__c> shList = new list<SVMXC__Stock_History__c>();
   		shList.add(stockHistory);
   		
   		StockHistoryClass.updateQtyChangeOnProductStock(shList);	
	}
	
	@isTest(SeeAllData=true)
	static void setStockHistoryWorkOrder_Test() {
		Map<Id,SVMXC__Stock_History__c> shMap = new Map<Id,SVMXC__Stock_History__c>();
		Set<Id> stockHistoryId = new Set<Id>();
		for(SVMXC__Stock_History__c sh : [	SELECT Id, SVMXC__RMA_Line__r.SVMXC__Service_Order__c, SVMXC__Shipment_Line__r.SVMXC__Service_Order__c, 
						SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c 
      			FROM SVMXC__Stock_History__c
      			WHERE SVMXC__RMA_Line__r.SVMXC__Service_Order__c != null LIMIT 1]) {
      		stockHistoryId.add(sh.Id);	
      		shMap.put(sh.Id, sh);
      	}
		StockHistoryClass.setStockHistoryWorkOrder(stockHistoryId, shMap);	
	}
	
	@isTest(SeeAllData=true)
	static void setStockHistoryWorkOrder_Test2() {
		Map<Id,SVMXC__Stock_History__c> shMap = new Map<Id,SVMXC__Stock_History__c>();
		Set<Id> stockHistoryId = new Set<Id>();
		for(SVMXC__Stock_History__c sh : [	SELECT Id, SVMXC__RMA_Line__r.SVMXC__Service_Order__c, SVMXC__Shipment_Line__r.SVMXC__Service_Order__c, 
						SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c 
      			FROM SVMXC__Stock_History__c
      			WHERE SVMXC__Shipment_Line__r.SVMXC__Service_Order__c != null LIMIT 1]) {
      		stockHistoryId.add(sh.Id);	
      		shMap.put(sh.Id, sh);
      	}
		StockHistoryClass.setStockHistoryWorkOrder(stockHistoryId, shMap);	
	}
	
	@isTest(SeeAllData=true)
	static void setStockHistoryWorkOrder_Test3() {
		Map<Id,SVMXC__Stock_History__c> shMap = new Map<Id,SVMXC__Stock_History__c>();
		Set<Id> stockHistoryId = new Set<Id>();
		for(SVMXC__Stock_History__c sh : [	SELECT Id, SVMXC__RMA_Line__r.SVMXC__Service_Order__c, SVMXC__Shipment_Line__r.SVMXC__Service_Order__c, 
						SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c 
      			FROM SVMXC__Stock_History__c
      			WHERE SVMXC__Service_Order_Line__r.SVMXC__Service_Order__c != null LIMIT 1]) {
      		stockHistoryId.add(sh.Id);	
      		shMap.put(sh.Id, sh);
      	}
		StockHistoryClass.setStockHistoryWorkOrder(stockHistoryId, shMap);	
	}
}
/***************************************************************************
Author: Ritika Kaushik
Created Date: 06-Aug-2013
Description: 
This class will be called from Submit button on the L2848 Record

Change Log:
26-Oct-2015 -  Steve Avon / Forefront - Removed try-catch blocks with no exception action
22-Sep-2017 - Divya Hargunani - STSK0012960\STSK0013567 - changed button logic to change the status of the document  to "Submitted for Review"
25-Sep-2017 - Divya Hargunani - STSK0012958 - modified logic to Email template so that it sends a link to the corresponding  L2848
                                                and no longer sends the attachment
25-Sep-2017 - Amitkumar Katre - STSK0012838- replaced label L2848 with Escalated Complaint Form (ECF)
14-Feb-2018 - Ujjal Baruah    - Added two fields to get updated by the script (Submit Date and Submitted By for tracking purpose which will be used in Report)
*************************************************************************************/

public class SubmitL2848Record
{ 
    public List<L2848__c> objL2848 = new List<L2848__c>();
    public id idL2848;
    public string L2848RecId;
    public SubmitL2848Record (ApexPages.StandardController controller) 
    {      
        idL2848 = apexpages.currentpage().getparameters().get('id');
        objL2848 =[select id,name,City__c,Country__c,Name_of_Customer_Contact__c,Title_Role__c,Phone_Number__c,Date_of_Event__c,Date_only_Reported_to_Varian__c,Employee_Phone__c,Version_Model__c, Top_Level_Installed_Product__c, Location__c, Device_or_Application_Name__c, Case__c,Owner.Name,Case__r.CaseNumber,Is_Submit__c, Alert_If_No_Action__c from L2848__c where id =:idL2848];  //savon.FF 10-26-15 US5252 DE6473 added Alert If No Action to query
    }

    
    //Method to Submit the Record and check whether the record has already been submitted .
    //STSK0012960\STSK0013567: changed the value of  status of the document  to "Submitted for Review"      
    public pagereference updateIsSubmit()
    {
        
        if(objL2848.size() > 0)
        {
            if(objL2848[0].Is_Submit__c == False)
            {
                if(objL2848[0].Location__c == null || objL2848[0].Top_Level_Installed_Product__c == null || objL2848[0].Device_or_Application_Name__c == null)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.info, 'The Site name, Product ID and Device or Application Name are required, please fill them out and resubmit the form.');            
                    ApexPages.addMessage(myMsg);
                }
                else if(objL2848[0].Alert_If_No_Action__c == null)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.info, 'Cannot submit record if Alert If No Action field not populated.');            
                    ApexPages.addMessage(myMsg);
                } 
                else if(objL2848[0].City__c == null || objL2848[0].Country__c == null || objL2848[0].Name_of_Customer_Contact__c == null || objL2848[0].Title_Role__c == null || objL2848[0].Phone_Number__c == null || objL2848[0].Date_of_Event__c == null || objL2848[0].Date_only_Reported_to_Varian__c == null || objL2848[0].Employee_Phone__c == null || objL2848[0].Device_or_Application_Name__c == null || objL2848[0].Version_Model__c == null)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.info, 'Please fill in the required fields before submitting the Escalated Complaint Form (ECF).');            
                    ApexPages.addMessage(myMsg);
                }
                else
                {
                    objL2848[0].Is_Submit__c = True;
                    //STSK0012960\STSK0013567: changed the value of  status of the document  to "Submitted for Review"
                    objL2848[0].Is_Submitted__c = 'Submitted for Review';
                    objL2848[0].Submit_Date__c = System.Today();             //Added by ubaruah on 02/14/2018
                    objL2848[0].Submitted_By__c = UserInfo.getUserId();      //Added by ubaruah on 02/14/2018                    
                    Database.SaveResult result = Database.update(objL2848[0]);
                    if(result.isSuccess()){
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.info, 'The record has been submitted.');            
                        ApexPages.addMessage(myMsg);
                        SendL2848EmailInFuture(idL2848);  
                   }
                   else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.error, 'Error occured during this operation and email to customer was not sent.Please contact Support Team.');            
                        ApexPages.addMessage(myMsg);
                   }
                }
            }
            else if(objL2848[0].Is_Submit__c )
            { 
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.info, 'The record has already been submitted.');            
                ApexPages.addMessage(myMsg); 
                   
            }
        }
        
        return Null;
    }
    
    //@future(callout=true) //CM : Aug 17 2017 : Commented future annotation for INC 4686619.
    //STSK0012958: Removed the code for adding email attachment and added the code to send out an link to record
    public static void SendL2848EmailInFuture(id idL2848)
    {

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            List<L2848__c> objL2848 = new List<L2848__c>();
            objL2848 =[select id,name,Case__c,Owner.Name,Case__r.CaseNumber,Is_Submit__c from L2848__c where id =:idL2848];
            String Recordlink = URL.getSalesforceBaseUrl().toExternalForm()+'/'+objL2848[0].Id;
            
            //STSK0012958 : Updated the body of email and link to record Id
            String strPlainHtmltext='Dear Regulatory Team,'+'<br><br>'+'Please accept this complaint for ECT review. Please note case '+objL2848[0].Case__r.CaseNumber+' for your reference. All case and work order information is accessible from there.';
            strPlainHtmltext=strPlainHtmltext+'<br><br> To access the Complaint form for input into EtQ system please click on the link below.';
            strPlainHtmltext=strPlainHtmltext+'<br>'+Recordlink;
            strPlainHtmltext=strPlainHtmltext+'<br><br>'+'Thank You!';
            strPlainHtmltext=strPlainHtmltext+'<br>'+objL2848[0].owner.name;
            
            string strEmailSubject = label.SR_L2848_OnSubmitSubject + ' ' + objL2848[0].Name;
            email.setSubject(strEmailSubject); 
            email.setToAddresses( new string[]{Label.SR_RegulatoryEmailAddress,UserInfo.getUserEmail()} );
            email.setHtmlBody(strPlainHtmltext );
            email.usesignature=false;
            Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            String strEventDesc=email.getHtmlBody();
            
            Task e= new Task();
            e.Subject= strEmailSubject;
            e.Ownerid = UserInfo.getUserId();
            e.Status = 'Completed';
            e.Priority = 'Normal';
            e.Description = strEventDesc;
            e.WhatId=objL2848[0].id;
            //insert e;
            Database.SaveResult resultInsert = Database.insert(e);
            if(resultInsert.isSuccess()){
                objL2848[0].ECF_Activity_Created__c = true;
                update objL2848[0];
            }
            
    }
}
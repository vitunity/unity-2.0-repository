/***************************************************************************
Author: Yogesh Dixit
Created Date: 30-Aug-2016
Description: 
This is test class for SubmitL2848Record Apex class

Change Log:
26-Sep-2017 - Divya Hargunani - STSK0012958- Commented the code snippet related to attachment and Added few more methods to increase the test coverage
*************************************************************************************/

@isTest
private class SubmitL2848Record_Test {
    
    /*
    STSK0012958 : commenting this helper method as attachment is no longer supported
    
    public static Attachment Attachment_Data(Boolean isInsert, L2848__c newL2848) {
        Attachment attach = new Attachment();
        attach.Name = 'L2848.pdf';
        attach.Body = Blob.valueOf('UNIT.TEST');
        attach.ParentId = newL2848.Id;
        if(isInsert)
            insert attach;
        return attach;
    }
    */
    
    //Test method to check for the scenario when site name, product and application name are null
    static TestMethod void updateIsSubmit_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        List<L2848__c> L2848 = new List<L2848__c>{UnityDataTestClass.L2848_TestData(true, newCase, serviceUser)};
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
        
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        
        l2848Control.updateIsSubmit();
                
        Test.StopTest();
    }
    
    
    
    //Test method to check for the scenario when Alert no ction field is null
    static TestMethod void updateIsSubmit_blank_alert_no_action_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true,newAcc);
        SVMXC__Service_Group__c newServiceTeam = UnityDataTestClass.serviceTeam_Data(true);
        ERP_Pcode__c newERPcode = UnityDataTestClass.ERPPCODE_Data(true);
        SVMXC__Installed_Product__c newIP = UnityDataTestClass.InstalledProduct_Data(true,newProd,newAcc,newLoc,newServiceTeam,newERPcode);
        L2848__c newL2848 = UnityDataTestClass.L2848_TestData(false, newCase, serviceUser);
        newL2848.Location__c = newLoc.Id;
        newL2848.Top_Level_Installed_Product__c = newIP.Id;
        newL2848.Device_or_Application_Name__c = 'test device';
        newL2848.Alert_If_No_Action__c = null;
        update newL2848;
        List<L2848__c> L2848 = new List<L2848__c>{newL2848};
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
        
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        l2848Control.updateIsSubmit();
        
        Test.StopTest();
    }
    
    //Test method to check for the scenario when city is null
    static TestMethod void updateIsSubmit_blank_city_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true,newAcc);
        SVMXC__Service_Group__c newServiceTeam = UnityDataTestClass.serviceTeam_Data(true);
        ERP_Pcode__c newERPcode = UnityDataTestClass.ERPPCODE_Data(true);
        SVMXC__Installed_Product__c newIP = UnityDataTestClass.InstalledProduct_Data(true,newProd,newAcc,newLoc,newServiceTeam,newERPcode);
        L2848__c newL2848 = UnityDataTestClass.L2848_TestData(false, newCase, serviceUser);
        newL2848.Location__c = newLoc.Id;
        newL2848.Top_Level_Installed_Product__c = newIP.Id;
        newL2848.Device_or_Application_Name__c = 'test device';
        newL2848.City__c = null;
        
        update newL2848;
        List<L2848__c> L2848 = new List<L2848__c>{newL2848};
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
         
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        
        l2848Control.updateIsSubmit();
        l2848List[0].City__c = 'Test';
        l2848Control.updateIsSubmit();
        Test.StopTest();
    }
    
    //Test method to check else part when isSubmit field is set to false
    static TestMethod void updateIsSubmit_ELSE_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true,newAcc);
        SVMXC__Service_Group__c newServiceTeam = UnityDataTestClass.serviceTeam_Data(true);
        ERP_Pcode__c newERPcode = UnityDataTestClass.ERPPCODE_Data(true);
        SVMXC__Installed_Product__c newIP = UnityDataTestClass.InstalledProduct_Data(true,newProd,newAcc,newLoc,newServiceTeam,newERPcode);
        L2848__c newL2848 = UnityDataTestClass.L2848_TestData(false, newCase, serviceUser);
        newL2848.Location__c = newLoc.Id;
        newL2848.Top_Level_Installed_Product__c = newIP.Id;
        newL2848.Device_or_Application_Name__c = 'test device';
        
        update newL2848;
        List<L2848__c> L2848 = new List<L2848__c>{newL2848};
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
        
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        
        l2848Control.updateIsSubmit();
        
        Test.StopTest();
    }
    
    //Test method to check for the scenario when isSubmit field is set to true
    static TestMethod void updateIsSubmit_ISSUBMIT_TRUE_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        L2848__c newL2848 = UnityDataTestClass.L2848_TestData(false, newCase, serviceUser);
        newL2848.Is_Submit__c = true;
        update newL2848;
        List<L2848__c> L2848 = new List<L2848__c>{newL2848};
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
        
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        
        l2848Control.updateIsSubmit();
        
        Test.StopTest();
    }
    
    //Test method to send email to regulatory team
    static TestMethod void SendL2848EmailInFuture_Test() {
        User serviceUser = UnityDataTestClass.InsertUserData(true);
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Case newCase = UnityDataTestClass.Case_Data(true, newAcc, newProd);
        L2848__c newL2848 = UnityDataTestClass.L2848_TestData(false, newCase, serviceUser);
        newL2848.Is_Submit__c = true;
        update newL2848;
        List<L2848__c> L2848 = new List<L2848__c>{newL2848};
        
        //Attachment attach = Attachment_Data(true, newL2848);
        
        Test.StartTest();
        
        PageReference pageRef = Page.OnSubmitL2848;
        pageRef.getParameters().put('id', L2848[0].Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(L2848[0]);
        SubmitL2848Record l2848Control = new SubmitL2848Record(sc);
        
        List<L2848__c> l2848List = new List<L2848__c>();
        l2848List = l2848Control.objL2848;
        
        SubmitL2848Record.SendL2848EmailInFuture(newL2848.Id);
    }
    
}
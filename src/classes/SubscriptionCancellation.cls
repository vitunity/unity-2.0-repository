/**
 * @description Triggered from Cancel Subscription button on Subscription detail page
 * Cancels subscription in salesforce and SAP
 */
global class SubscriptionCancellation {

    webservice static void cancelSubscription(String subscriptionId){
        Subscription__c subscription  = [
            SELECT Id,Status__c, Product_Type__c
            FROM Subscription__c
            WHERE Id =: subscriptionId
        ];
        
        List<Subscription_Line_Item__c> subscriptionLines = new List<Subscription_Line_Item__c>();
        for(Subscription_Line_Item__c subscriptionLine:[
            SELECT Id,Blocked__c,Subscription__c,Due_Date__c
            FROM Subscription_Line_Item__c
            WHERE Subscription__c =:subscriptionId
        ]){
            System.debug('subscriptionLine.Due_Date__c'+subscriptionLine.Due_Date__c);
            if(Date.today() <= subscriptionLine.Due_Date__c){
                subscriptionLine.Blocked__c = true;
                subscriptionLines.add(subscriptionLine);
            }
        }
        System.debug('--subscriptionLines'+subscriptionLines.size());
        System.debug('--subscriptionLines'+subscriptionLines.size());
        subscription.Status__c = 'Pending cancellation';
        subscription.Cancellation_Date__c = Date.today();
        update subscription;
        update subscriptionLines;
    }
}
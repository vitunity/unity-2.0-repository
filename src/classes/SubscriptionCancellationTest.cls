@isTest(seeAllData=true)
private class SubscriptionCancellationTest {
    
    private static BigMachines__Quote__c bmQuote1;
    private static BigMachines__Quote_Product__c quoteProduct;
    
    static testMethod void testCancelSubscription() {
        setUpSubscriptionData();
        
        
        Test.startTest();
        
            Subscription__c subscription1 = new Subscription__c();
            subscription1.Name = 'TEST1234';
            subscription1.Ext_Quote_Number__c = 'QUOTE-TEST1';
            subscription1.ERP_Site_Partner__c = 'Sales11111';
            subscription1.Status__c = 'Processed';
            subscription1.Product_Type__c = 'AN';
            insert subscription1;
        
            List<Subscription_Line_Item__c> subscriptionLines = new List<Subscription_Line_Item__c>();
            for(Integer counter=-1; counter<4; counter++){
                Subscription_Line_Item__c subscriptionLine = new Subscription_Line_Item__c();
                subscriptionLine.Quote_Product__c = quoteProduct.Id;
                subscriptionLine.Due_Date__c = Date.today().addMonths(counter);
                subscriptionLine.Subscription__c = subscription1.Id;
                subscriptionLines.add(subscriptionLine);
            }
            
            insert subscriptionLines;
            
            SubscriptionCancellation.cancelSubscription(subscription1.Id);
            
            Subscription__c cancelledSubscription = [SELECT Id,Status__c FROM Subscription__c WHERE Id=:subscription1.Id];
            System.assertEquals('Pending cancellation', cancelledSubscription.Status__c);
            
            List<Subscription_Line_Item__c> updatedSubscriptionLines = [
                SELECT Id,Blocked__c 
                FROM Subscription_Line_Item__c
                WHERE Blocked__c = true
                AND Quote_Product__c =:quoteProduct.Id
            ];
            
            System.assertEquals(4,updatedSubscriptionLines.size());
        Test.stopTest();
    }
    
    /**
     * Set up test data can be used for all the quotes.
     */
    static void setUpSubscriptionData() {
        
        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Region__c = 'NA';
        country.Pricing_Group__c = 'Z1';
        insert country;
        
        Account acct = TestUtils.getAccount();
        acct.Name = 'SUBSCRIPTION CANCELLATION';
        acct.AccountNumber = '121212';
        acct.Country1__c = country.Id;
        insert acct;
        
        Contact con = TestUtils.getContact();
        con.Email = 'test@test.com';
        con.AccountId = acct.Id;
        insert con;
        
        Opportunity opp1 = TestUtils.getOpportunity();
        opp1.AccountId = acct.Id;
        opp1.Primary_Contact_Name__c = con.Id;
        insert opp1;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 salesProduct = new Product2();
        salesProduct.Name = 'Sample Sales Product';
        salesProduct.IsActive = true;
        salesProduct.ProductCode = 'Test Sales Code';
        insert salesProduct;     
        
        PricebookEntry pbEntrySales = new PricebookEntry();
        pbEntrySales.Product2Id = salesProduct.Id;
        pbEntrySales.Pricebook2Id = standardPricebookId;
        pbEntrySales.UnitPrice = 111;
        pbEntrySales.CurrencyISOCode = 'USD';
        pbEntrySales.IsActive = true;
        insert pbEntrySales;
        
        bmQuote1 = TestUtils.getQuote();
        bmQuote1.Name = 'QUOTE-TEST1';
        bmQuote1.BigMachines__Account__c = acct.Id;
        bmQuote1.BigMachines__Opportunity__c = opp1.Id;
        bmQuote1.BigMachines__Status__c = 'Approved';
        bmQuote1.Order_Type__c = 'Sales';
        bmQuote1.Payment_Terms__c = 'Letter of Credit(Brachy only-Handle this)';
        bmQuote1.BigMachines__Is_Primary__c = true;
        insert bmQuote1;
        
        quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.BigMachines__Quote__c = bmQuote1.Id;
        quoteProduct.BigMachines__Product__c = salesProduct.Id;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = 'Sales';
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.Line_Number__c= 12;
        quoteProduct.Grp__c = 1.01;
        insert quoteProduct;
    }
}
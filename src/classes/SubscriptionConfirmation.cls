global class SubscriptionConfirmation {
    @future(callout=true)
    public static void sendEmail(Id subscriptionId, String templateName) {
        Subscription__c sfSubscription = [
            SELECT Id, Quote__c, Quote__r.Quote_Region__c, Name, Quote__r.OwnerId, Quote__r.Submitted_To_SAP_By__c, Quote__r.Name,Quote__r.Project_Manager__c
            FROM Subscription__c
            WHERE Id=:subscriptionId
        ];
        
        List<EmailTemplate> subscriptionTemplate = [SELECT Id,Name, DeveloperName FROM EmailTemplate WHERE DeveloperName =:templateName];
        
        Map<Id,User> users = new Map<Id,User>([
            SELECT Id, Email FROM User 
            WHERE Id =: sfSubscription.Quote__r.OwnerId 
            OR Id =: sfSubscription.Quote__r.Submitted_To_SAP_By__c
        ]);
            
        List<Attachment> attachments = getAttachments(sfSubscription);
        Set<String> emailsSet = new Set<String>();
        
        
        if(users.containsKey(sfSubscription.Quote__r.OwnerId)){
            emailsSet.add(users.get(sfSubscription.Quote__r.OwnerId).Email);
        }
        if(users.containsKey(sfSubscription.Quote__r.Submitted_To_SAP_By__c)){
            emailsSet.add(users.get(sfSubscription.Quote__r.Submitted_To_SAP_By__c).Email);
        }
        if(!String.isBlank(sfSubscription.Quote__r.Project_Manager__c)){
            emailsSet.add(sfSubscription.Quote__r.Project_Manager__c);
        }
        
        emailsSet.addAll(getEmailList(sfSubscription.Quote__r.Quote_Region__c));
        system.debug('emailsSet---'+emailsSet);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string [] toaddress = new List<String>();
        system.debug('emailsSet---'+emailsSet);                  
        for(String s : emailsSet)
            toaddress.add(s.trim());
                
        mail.setToAddresses(toaddress);  
        mail.setTargetObjectId(sfSubscription.Quote__r.OwnerId);
        mail.setWhatId(sfSubscription.Id);
        mail.setTemplateId(subscriptionTemplate[0].Id);
        //if(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c!= NULL)        
        mail.setOrgWideEmailAddressId(OrgWideNoReplyId__c.getValues('NoReplyId').OrgWideId__c);
        mail.setSaveAsActivity(false);
                  
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            
        for(Attachment att:attachments){
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(att.Name);
            efa.setBody(att.Body);
            fileAttachments.add(efa);
        }
                
        if(!fileAttachments.isEmpty())     
            mail.setFileAttachments(fileAttachments);
        
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    //Get Epot and Epot line items attachments for email
    private static List<Attachment> getAttachments(Subscription__c sfSubscription){
        Blob cntPDF ; 
        PageReference orderPg = Page.SR_PrepareOrderPdf;
        orderPg.getParameters().put('id',sfSubscription.Quote__c);  
        if(Test.IsRunningTest()){
            cntPDF=Blob.valueOf('UNIT.TEST');
        }else{
            cntPDF = orderPg.getContent();
        }
        
        Blob cntPDF1;//added by kaushiki
        PageReference orderPg1 = Page.SR_PrepareOrderPdf2;
        orderPg1.getParameters().put('id',sfSubscription.Quote__c);
        if (Test.IsRunningTest()){
            cntPDF1=Blob.valueOf('UNIT.TEST');
        }else{
            cntPDF1 = orderPg1.getContent();
        }
        
        Attachment att = new Attachment();
        att.Name = 'Epot_'+sfSubscription.Quote__r.Name+'.pdf';
        att.parentId = sfSubscription.Quote__c;
        att.Body = cntPDF;
        
        Attachment att1 = new Attachment();
        att1.Name = 'Epot Line Items_'+sfSubscription.Quote__r.Name+'.pdf';
        att1.parentId = sfSubscription.Quote__c;
        att1.Body = cntPDF1;
        return new List<Attachment>{att, att1};
    }
    
    public static Set<String> getEmailList(String region){
        List<Email_Dlists__c> DListRecs = [SELECT Id,Name,Primary_Dlist__c,secondary_Dlist__c,Admin_Dlist__c,
                                           Brachy_Dlist__c,Business_Dlist__c,Functionality__c,Region__c,
                                           Support_Dlist__c,SalesOrg__c,Fulfillment_DList__c
                                           FROM Email_Dlists__c 
                                           WHERE Functionality__c = 'Booked Sales'
                                           AND Region__c =:region]; 
        Set<String> emailsSet = new Set<String>();
        for(Email_Dlists__c DListRec : DListRecs){
        
            if(!String.isBlank(DListRec.Business_Dlist__c))
                emailsSet.add(DListRec.Business_Dlist__c.trim());
            
            if(!String.isBlank(DListRec.Primary_Dlist__c))
                emailsSet.add(DListRec.Primary_Dlist__c.trim());
    
            if(!String.isBlank(DListRec.Support_Dlist__c))
                emailsSet.add(DListRec.Support_Dlist__c.trim());
                
            if(!String.isBlank(DListRec.Admin_Dlist__c))
                emailsSet.add(DListRec.Admin_Dlist__c.trim());
                
            if(!String.isBlank(DListRec.Secondary_Dlist__c))
                emailsSet.add(DListRec.Secondary_Dlist__c.trim());
        }   
        return emailsSet;
    }
}
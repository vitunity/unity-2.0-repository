@isTest
public class SubscriptionConfirmationTest{
    
    private static BigMachines__Quote__c subscriptionQuote;
    
    static testMethod void testSubscriptionConfirmation() {
        
        setupSubscriptionData();
        Subscription__c sfSubscription = [SELECT Id,Name FROM Subscription__c WHERE Quote__c =:subscriptionQuote.Id];
        SubscriptionConfirmation.sendEmail(sfSubscription.Id, 'Quote_SAP_BookedSubscription_Success_VF');
    }
    
    
    private static void setupSubscriptionData(){
        
        OrgWideNoReplyId__c orgId = new OrgWideNoReplyId__c();
        orgId.Name = 'NoReplyId';
        orgId.OrgWideId__c = [select id, Address, DisplayName from OrgWideEmailAddress][0].Id;
        insert orgId;
        
        Email_Dlists__c dList = testUtils.getEmailDlist();
        dList.Region__c = 'NA';
        dList.Functionality__c = 'Booked Sales';
        dList.Primary_Dlist__c = 'Test@test.com';
        insert dList;
        
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'Sales1212';
        insert salesAccount;
        
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        subscriptionQuote = TestUtils.getQuote();
        subscriptionQuote.Name = 'TEST Subscription QUOTE';
        subscriptionQuote.BigMachines__Account__c = salesAccount.Id;
        subscriptionQuote.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuote.National_Distributor__c = '121212';
        subscriptionQuote.Order_Type__c = 'sales';
        subscriptionQuote.Price_Group__c = 'Z2';
        subscriptionQuote.Submitted_To_SAP_By__c = UserInfo.getUserId();
        subscriptionQuote.Quote_Region__c = 'NA';
        insert subscriptionQuote;
        
        Subscription__c subscription = new Subscription__c();
        subscription.Name = 'TESTSUBSCRIPTION';
        subscription.Ext_Quote_Number__c = subscriptionQuote.Name;
        subscription.Number_Of_Licences__c = 4;
        insert subscription;
    }
}
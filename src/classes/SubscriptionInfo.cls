/**
 * This clas provides web service to get subscription details in qumulate portal
 */
global class SubscriptionInfo {
  
  /**
   * Qumulate calls this method every 6 hours to get any new subscription and updates to existing subscription records
   */
  webservice static List<SubscriptionInfo.QumulateSubscription> getSubscriptionByDate(DateTime lastSyncDate, String productType){
    System.debug('--lastSyncDate'+lastSyncDate+'--productType'+productType);
    List<Subscription__c> qumulateSubscriptions = [
      SELECT Id,Name,Start_Date__c,End_Date__c,Qumulate_Group_Id__c,Status__c,
      Number_Of_Licences__c,Cancellation_Date__c,CreatedDate,LastModifiedDate
      FROM Subscription__c 
      WHERE LastModifiedDate >:lastSyncDate 
      AND Product_Type__c =: productType
    ];    
    System.debug('Subs'+qumulateSubscriptions);
    return getSubscriptions(qumulateSubscriptions);
  }
  
  /**
   * Qumulate calls this method to get subscription details by qumulate group id
   * Once group id can have only one active subscription
   */
  webservice static List<SubscriptionInfo.QumulateSubscription> getActiveSubscription(String qumulateGroupId){
    List<Subscription__c> quSubscription = [
      SELECT Id,Name,Start_Date__c,End_Date__c,Status__c,Number_Of_Licences__c,
      Qumulate_Group_Id__c, Cancellation_Date__c,CreatedDate,LastModifiedDate
      FROM Subscription__c 
      WHERE Quote__r.eCommerce_Group_Id__c=:qumulateGroupId
    ];
    
    return getSubscriptions(quSubscription);
  }
  
    webservice static CustomerInfo getCustomerDetails(String oktaId){
        List<Contact> contacts = [
            SELECT Id, AccountId, OktaId__c, Account.Id, Account.Name, Ownerid, Account.Ext_Cust_Id__c, 
            Account.SFDC_Account_Ref_Number__c,Email,MailingCity,MailingCountry,MailingState,MailingStreet,
            MailingPostalCode,FirstName,LastName,Phone 
            From Contact 
            WHERE OktaId__c =:oktaId
        ];
        if(!contacts.isEmpty()){
            return new CustomerInfo(
                contacts[0].account.Name,
                contacts[0].Account.Ext_Cust_Id__c,
                contacts[0].FirstName,
                contacts[0].LastName,
                contacts[0].Email,
                contacts[0].Phone,
                contacts[0].MailingStreet,
                contacts[0].MailingCity,
                contacts[0].MailingState,
                contacts[0].MailingPostalCode,
                contacts[0].MailingCountry
            );
        }else{
            return null;
        }
    }
    
    /**
     * @description get installbase for given okta id
     */
    webservice static List<SubscriptionInfo.InstalledProduct> getInstalledProducts(String oktaId){
        List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE OktaId__c=:oktaId];
        List<SubscriptionInfo.InstalledProduct> customerInstallBase = new List<SubscriptionInfo.InstalledProduct>();
        if(!contacts.isEmpty()){
            List<SVMXC__Installed_Product__c> installedProducts = [
                SELECT Id,ERP_Reference__c,SVMXC__Product__r.Name,SVMXC__Service_Contract_Start_Date__c, 
                SVMXC__Date_Installed__c,ERP_Functional_Location__c,First_Acceptance_Date__c,Name
                FROM SVMXC__Installed_Product__c
                WHERE SVMXC__Company__c =:contacts[0].AccountId
            ];
            for(SVMXC__Installed_Product__c installedProduct:installedProducts){
                customerInstallBase.add(
                    new SubscriptionInfo.InstalledProduct(
                        installedProduct.ERP_Reference__c,
                        installedProduct.SVMXC__Product__r.Name,
                        installedProduct.ERP_Functional_Location__c,
                        installedProduct.SVMXC__Date_Installed__c,
                        installedProduct.SVMXC__Service_Contract_Start_Date__c,
                        installedProduct.First_Acceptance_Date__c,
                        installedProduct.Name
                    )
                );
            }
        }
        return customerInstallBase;
    }
    
  /**
   * Utility method to create list of QumulateSubscription instances from list of subscription object 
   */
  private static List<SubscriptionInfo.QumulateSubscription> getSubscriptions(List<Subscription__c> quSubscriptions){
    List<SubscriptionInfo.QumulateSubscription> subscriptions = new List<SubscriptionInfo.QumulateSubscription>();
    for(Subscription__c quSubscription : quSubscriptions){
      subscriptions.add(
        new SubscriptionInfo.QumulateSubscription(
          quSubscription.Name,
          quSubscription.Start_Date__c,
          quSubscription.End_Date__c,
          quSubscription.Qumulate_Group_Id__c,
          quSubscription.Status__c=='Processed'?'Active':quSubscription.Status__c,
          Integer.valueOf(quSubscription.Number_Of_Licences__c),
          quSubscription.Cancellation_Date__c,
          quSubscription.CreatedDate,
          quSubscription.LastModifiedDate
        )
      );
    }
    return subscriptions;
  }
  
  /**
   * Custom Qumulate subscription structure. Created to reduce WSDL size
   */ 
  global class QumulateSubscription{
    webservice String subscriptionNumber;
    webservice Date startDate;
    webservice Date endDate;
    webservice String qumulateGroupId;
    webservice String status;
    webservice Integer numberOfLicenses;
    webservice Date cancellationDate;
    webservice String responseStatus;
    webservice DateTime createdDateTime;
    webservice DateTime lastModifiedDateTime;
    
    public QumulateSubscription(String subscriptionNumber,Date startDate, Date endDate, String qumulateGroupId, String status, 
    Integer numberOfLicenses, Date cancellationDate, DateTime createdDateTime, DateTime lastModifiedDateTime){
      this.subscriptionNumber = subscriptionNumber;
      this.startDate = startDate;
      this.endDate = endDate;
      this.qumulateGroupId = qumulateGroupId;
      this.status = status;
      this.numberOfLicenses = numberOfLicenses;
      this.cancellationDate = cancellationDate;
      this.createdDateTime = createdDateTime;
      this.lastModifiedDateTime = lastModifiedDateTime;
    }
  }
  
  /**
   * Get Customer details using okta id
   */
    global class CustomerInfo{
        webservice String customerName;
        webservice String customerNumber;
        webservice String firstName;
        webservice String lastName;
        webservice String email;
        webservice String phone;
        webservice String address;
        webservice String city;
        webservice String state;
        webservice String postalCode;
        webservice String country;
        
        public CustomerInfo(String customerName, String customerNumber, String firstName, String lastName, String email, String phone, String address, 
            String city, String state, String postalCode, String country){
            this.customerName = customerName;
            this.customerNumber = customerNumber;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phone = phone;
            this.address = address;
            this.city = city;
            this.state = state;
            this.postalCode = postalCode;
            this.country = country;
        }
    }
    
    /**
     * Get Customer details using okta id
     */
    global class InstalledProduct{
        webservice String serialNumber;
        webservice String productName;
        webservice String erpFunctionalLocation;
        webservice Date dateInstalled;
        webservice Date contractStartDate;
        webservice Date firstAcceptanceDate;
        webservice String installedProductId;
        
        public InstalledProduct(String serialNumber, String productName, String erpFunctionalLocation, Date dateInstalled, Date contractStartDate, Date firstAcceptanceDate, String installedProductId){
            this.serialNumber = serialNumber;
            this.productName = productName;
            this.erpFunctionalLocation = erpFunctionalLocation;
            this.dateInstalled = dateInstalled;
            this.contractStartDate = contractStartDate;
            this.firstAcceptanceDate = firstAcceptanceDate;
            this.installedProductId = installedProductId;
        }
    }
}
@isTest
private class SubscriptionInfoTest {

    static testMethod void testSubscriptionInfo() {
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        setupSubscriptionData();
      
        System.assertEquals(2, SubscriptionInfo.getSubscriptionByDate(DateTime.now().addHours(-1), 'QU').size());
      
        List<SubscriptionInfo.QumulateSubscription> subscriptions = SubscriptionInfo.getActiveSubscription('TESTGROUPID2');
      
        SubscriptionInfo.getCustomerDetails('TESTID');
        SubscriptionInfo.getInstalledProducts('TESTID');
      
      
        System.assertEquals('TESTSUB2', subscriptions[0].subscriptionNumber);
        System.assertEquals(1, subscriptions.size());
    }
    
    static void setupSubscriptionData() {
        
        OrgWideNoReplyId__c orgId = new OrgWideNoReplyId__c();
        orgId.Name = 'NoReplyId';
        orgId.OrgWideId__c = [select id, Address, DisplayName from OrgWideEmailAddress][0].Id;
        insert orgId;
        
        Account salesAccount = TestUtils.getAccount();
        salesAccount.AccountNumber = 'Sales1212';
        insert salesAccount;
        
        Product2 objProd = new Product2(Name = 'Test Pro', SVMXC__Product_Cost__c = 125.10, ProductCode = 'H09', IsActive = true);   
        insert objProd;
        
        SVMXC__Installed_Product__c objIns = new SVMXC__Installed_Product__c(
            ERP_Reference__c = 'H12345', 
            SVMXC__Company__c = salesAccount.id, Name = 'H12345', 
            SVMXC__Product__c = objProd.id,
            SVMXC__Status__c = 'Installed'
        );
        insert objIns;
        
        Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        con.OktaId__c = 'TESTID';
        insert con;
       
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        BigMachines__Quote__c salesQuote = TestUtils.getQuote();
        salesQuote.Name = 'TEST SALES QUOTE';
        salesQuote.BigMachines__Account__c = salesAccount.Id;
        salesQuote.BigMachines__Opportunity__c = opp.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.eCommerce_Group_Id__c = 'TESTGROUPID1';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Price_Group__c = 'Z2';
        
        BigMachines__Quote__c subscriptionQuote = TestUtils.getQuote();
        subscriptionQuote.Name = 'TEST Subscription QUOTE';
        subscriptionQuote.BigMachines__Account__c = salesAccount.Id;
        subscriptionQuote.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuote.National_Distributor__c = '121212';
        subscriptionQuote.Order_Type__c = 'sales';
        subscriptionQuote.Price_Group__c = 'Z2';
        subscriptionQuote.eCommerce_Group_Id__c = 'TESTGROUPID2';
        insert new List<BigMachines__Quote__c>{salesQuote, subscriptionQuote};
        
        Subscription__c subscription1 = new Subscription__c();
        subscription1.Ext_Quote_number__c = 'TEST SALES QUOTE';
        subscription1.Name = 'TESTSUB1';
        subscription1.Status__c = 'Processed';
        subscription1.Ext_Quote_Number__c = 'TEST SALES QUOTE';
        subscription1.Product_Type__c = 'QU';
        
        Subscription__c subscription2 = new Subscription__c();
        subscription2.Ext_Quote_number__c = 'TEST Subscription QUOTE';
        subscription2.Name = 'TESTSUB2';
        subscription2.Status__c = 'Closed';
        subscription2.Ext_Quote_Number__c = 'TEST Subscription QUOTE';
        subscription2.Product_Type__c = 'QU';
        insert new List<Subscription__c>{subscription1,subscription2};
        System.debug('---Lastmodified'+[SELECT LastModifiedDate FROM Subscription__c][0].LastModifiedDate);
    }
}
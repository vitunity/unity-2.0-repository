/**
 * Search pending and completed installatiopn for subscription products
 */
public without sharing class SubscriptionInstallationController {
    
    public Map<Id,Subscription__c> subscriptions{get;set;}
    public Map<Id,String> attachments{get;set;}
    public Map<Id,String> subscriptionTypes{get;set;}
    
    private Map<String,List<SelectOption>> contactRolesMap;
    private Map<String,Contact_Role_Association__c> contactRoleAssociationMap;
    
    //Contact Role literals
    private final String SITE_PROJECT_MANAGER = 'PM-Project Mgr';
    private final String SALES_OPERATIONS_SPECIALIST = 'ZJ-FOA';
    private final String SALES_SERVICES_MANAGER = 'ZN-Sales Mgr';
    private final String CONTRACT_ADMINISTRATOR = 'ZO-Contract Admin';
    
    public SubscriptionInstallationController(){
        contactRolesMap = new Map<String,List<SelectOption>>();
        initializeContactRoles();
    }
    
    /**
     * Search subscription products based on customerNumber/customerName/quoteNumber/installationStatus
     */
    public void searchSubscriptions(){
        String soql = 'SELECT Id,Name,ERP_Site_Partner__c,Site_Partner__r.Name,PCSN__c,Licence_Status__c,'
        +'Installation_Status__c,Installation_date__c,Quote__r.BigMachines__Account__r.AccountNumber,'
        +'Quote__r.BigMachines__Account__c,Quote__r.BigMachines__Account__r.Name,MAC_Address__c,'
        +'Quote__r.Name,Functional_Location__c,Status__c '
        +' FROM Subscription__c WHERE Name != null AND Product_Type__c = \'AN\'';
        
        soql += addFilters();
        System.debug('---soqlreturn'+soql);
        soql += ' ORDER BY CreatedDate DESC LIMIT 999';
        subscriptions = new Map<Id,Subscription__c>((List<Subscription__c>) Database.query(soql));
        
        //populate attachment ids for subscription for download
        if(!subscriptions.isEmpty()){
            attachments = getAttachments(subscriptions.keySet());
        }
        
        //update type of contract(New,Add-On,Renewal)
        populateSubscriptionTypes();
        
        System.debug('-----subscriptionTypes'+subscriptionTypes);
        if(subscriptions.isEmpty()){
            subscriptions = null;
        }
    }
    
    private String  addFilters(){
        Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
        String customerNumber = pageParameters.get('customerNumber');
        String customerName = pageParameters.get('customerName');
        String quoteNumber = pageParameters.get('quoteNumber');
        String installationStatus = pageParameters.get('installationStatus');
        String licenceStatus = pageParameters.get('licenceStatus');
        String contractnumber = pageParameters.get('contractNumber');
        String siteProjectManager = pageParameters.get('siteProjectManager');
        String soSpecialist = pageParameters.get('soSpecialist');
        String contractAdmin = pageParameters.get('contractAdmin');
        String salesManager = pageParameters.get('salesManager');
        
        System.debug('--contractnumber'+contractnumber+'--siteProjectManager'+siteProjectManager+'--soSpecialist'+soSpecialist+'--contractAdmin'+contractAdmin+'--salesManager'+salesManager);
        String soql = '';
        if(!String.isBlank(customerNumber)){
            soql += ' and Quote__r.BigMachines__Account__r.AccountNumber LIKE \''+String.escapeSingleQuotes(customerNumber)+'%\'';
        }
        
        if(!String.isBlank(customerName)){
            soql += ' and Quote__r.BigMachines__Account__r.Name LIKE \''+String.escapeSingleQuotes(customerName)+'%\'';
        }
        
        if(!String.isBlank(quoteNumber)){
            soql += ' and Quote__r.Name LIKE \''+String.escapeSingleQuotes(quoteNumber)+'%\'';
        }
        
        if(!String.isBlank(installationStatus)){
            soql += ' and Installation_Status__c LIKE \''+String.escapeSingleQuotes(installationStatus)+'%\'';
        }
        
        if(!String.isBlank(licenceStatus)){
            soql += ' and Licence_Status__c LIKE \''+String.escapeSingleQuotes(licenceStatus)+'%\'';
        }
        
        if(!String.isBlank(contractnumber)){
            soql += ' and Name LIKE \''+String.escapeSingleQuotes(contractnumber)+'%\'';
        }
        
        if(!String.isBlank(siteProjectManager)){
            soql += ' and Site_Project_manager__c LIKE \''+String.escapeSingleQuotes(siteProjectManager)+'%\'';
        }
        
        if(!String.isBlank(soSpecialist)){
            soql += ' and Sales_Operations_Specialist__c LIKE \''+String.escapeSingleQuotes(soSpecialist)+'%\'';
        }
        
        if(!String.isBlank(contractAdmin)){
            soql += ' and Contract_Administrator__c LIKE \''+String.escapeSingleQuotes(contractAdmin)+'%\'';
        }
        
        if(!String.isBlank(salesManager)){
            soql += ' and Sales_Manager__c LIKE \''+String.escapeSingleQuotes(salesManager)+'%\'';
        }
        
        return soql;
    }
    
    /**
     * Default subscriptions created in last 30 days on Installation page
     */
    public void getSubscriptions(){
        subscriptions = new Map<Id,Subscription__c>([
            SELECT Id,Name,ERP_Site_Partner__c,Site_Partner__r.Name,PCSN__c,Licence_Status__c,Functional_Location__c,
            Installation_Status__c,Installation_date__c,Quote__r.BigMachines__Account__r.AccountNumber,
            Quote__r.BigMachines__Account__c,Quote__r.BigMachines__Account__r.Name,MAC_Address__c,Quote__r.Name,Status__c
            FROM Subscription__c 
            WHERE CreatedDate = LAST_N_DAYS:30
            AND Product_Type__c = 'AN'
            ORDER BY CreatedDate DESC
        ]);
        
        if(!subscriptions.isEmpty()){
            attachments = getAttachments(subscriptions.keySet());
        }
        
        //update type of contract(New,Add-On,Renewal)
        populateSubscriptionTypes();
        
        if(subscriptions.isEmpty()){
            subscriptions = null;
        }
    }
    
    /*
     * Generate subscription id and its related licence attachment id map
     */
    private Map<Id,String> getAttachments(Set<Id> subscriptionIds){
        List<Attachment> attachments = [SELECT Id,ParentId FROM Attachment WHERE ParentId =: subscriptionIds];
        Map<Id,String> licenceAttachmentIds = new Map<Id,String>();
        for(Attachment licence : attachments){
            licenceAttachmentIds.put(licence.ParentId,licence.Id);
        }
        System.debug('----licenceAttachmentIds1'+licenceAttachmentIds);
        for(Id subId : subscriptionIds){
            if(!licenceAttachmentIds.containsKey(subId)){
                licenceAttachmentIds.put(subId,subId);
            }
        }
        
        System.debug('----licenceAttachmentIds2'+licenceAttachmentIds);
        return licenceAttachmentIds;
    }
    
    /**
     * Licence status values for vf Select options
     */
    public List<SelectOption> getLicenceStatuses(){
        return getPicklistValues(Subscription__c.Licence_Status__c.getDescribe());
    }
    
    /**
     * Installation status values for vf Select options
     */
    public List<SelectOption> getInstallationStatuses(){
        return getPicklistValues(Subscription__c.Installation_Status__c.getDescribe());
    }
    
    private List<SelectOption> getPicklistValues(Schema.DescribeFieldResult dfr){
        
        List<Schema.PicklistEntry> picklistEntries = dfr.getPicklistValues();
        
        // Add picklist values to a SelectOption list
        List<SelectOption> piclistValues = new List<SelectOption>();
        piclistValues.add(new SelectOption('', '--Select '+dfr.getLabel()+'--'));
        for (Schema.PicklistEntry ple : picklistEntries){
            piclistValues.add(new SelectOption(ple.getValue(), ple.getValue()));
        }
        return piclistValues;
    }
    
    /**
     * Update subscription products
     */
    public void updateSubscriptions(){
        update subscriptions.values();
    }
    
    
    /**
     * Mark subscription as ready for new licence generation and delete existing licences
     */
    public void updateSubscription(){
        
        Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
        String subscriptionId = pageParameters.get('subscriptionId');
        String macAddress = pageParameters.get('macAddress');
        
        List<Attachment> licences = [SELECT Id FROM Attachment WHERE ParentId =:subscriptionId];
        
        if(!licences.isEmpty()) delete licences;
        
        update new Subscription__c(Id = subscriptionId, MAC_Address__c = macAddress, Licence_Status__c = 'Processing');
    }
    
    private void populateSubscriptionTypes(){
        subscriptionTypes = new Map<Id,String>();
        
        List<Subscription_Line_Item__c> billingItems = [
            SELECT Id, Billing_Type__c,Subscription__c
            FROM Subscription_Line_Item__c
            WHERE Subscription__c IN:subscriptions.keySet()
        ];
        
        for(Subscription_Line_Item__c billingItem : billingItems){
            if(billingItem.Billing_Type__c == 'Add-On'){
        subscriptionTypes.put(billingItem.Subscription__c, 'Add-On');
      }else if(billingItem.Billing_Type__c == 'New'){
        if(!subscriptionTypes.containsKey(billingItem.Subscription__c)){
          subscriptionTypes.put(billingItem.Subscription__c, 'New');
        }
      }else{
        if(!subscriptionTypes.containsKey(billingItem.Subscription__c)){
          subscriptionTypes.put(billingItem.Subscription__c, 'Renewal');
        }
      }
        }
    }
    
    /**
     * Intialize contact roles map with role and list of contacts
     * Site Project Manager,Contract Administrator,Sales Operations Specialist,Sales/Services Manager Select Options values
     */
    @testVisible private void initializeContactRoles(){
        contactRoleAssociationMap = new Map<String,Contact_Role_Association__c>();
        
        List<string> strZemiAccNum = new List<String>{'ZEMIPACRIM','ZEMIEU','ZEMI'};
                
        List<Contact_Role_Association__c> contactRoles= [Select Role__c,Contact__r.Name,Contact__r.Accountid,Contact__c,
                                                          Contact__r.Email,Contact__r.ERP_Reference__c,Contact__r.Account.AccountNumber
                                                          From Contact_Role_Association__c 
                                                          Where Contact__r.Accountid != null 
                                                          AND Contact__r.Account.AccountNumber IN :strZemiAccNum 
                                                          AND Contact__r.ERP_Reference__c != null
                                                          order by Contact__r.Name   
                                                          limit 9999];
        
        //Initialize contact roles map
        contactRolesMap = new Map<String,List<SelectOption>>{
                                                                SITE_PROJECT_MANAGER => new List<SelectOption>{new SelectOption('','--Select Site Project Manager--')},
                                                                SALES_OPERATIONS_SPECIALIST => new List<SelectOption>{new SelectOption('','--Select Sales Operations Specialist--')},
                                                                SALES_SERVICES_MANAGER => new List<SelectOption>{new SelectOption('','--Select Sales Manager--')},
                                                                CONTRACT_ADMINISTRATOR => new List<SelectOption>{new SelectOption('','--Select Contract Administrator--')}
                                                            };
        
        for(Contact_Role_Association__c contactRole : contactRoles){
            
            if(contactRolesMap.containskey(contactRole.Role__c)){
                contactRolesMap.get(contactRole.Role__c).add(
                                                                new SelectOption(contactRole.Contact__r.ERP_Reference__c, contactRole.Contact__r.Name)
                                                            );
            }
            contactRoleAssociationMap.put(contactRole.Contact__r.ERP_Reference__c,contactRole);
            
        }                                                         
           
    }
    
    /**
     * Contact Roles fields in header section of prepare order form
     */
    public List<SelectOption> getSiteProjectManagers(){
        return contactRolesmap.get(SITE_PROJECT_MANAGER);
    }
    
    public List<SelectOption> getSalesOperationSpecialists(){
        return contactRolesmap.get(SALES_OPERATIONS_SPECIALIST);
    }
    
    public List<SelectOption> getSalesManagers(){
        return contactRolesmap.get(SALES_SERVICES_MANAGER);
    }
    
    public List<SelectOption> getContractAdministrators(){
        return contactRolesmap.get(CONTRACT_ADMINISTRATOR);
    }
}
@isTest
private class SubscriptionInstallationControllerTest {

    static testMethod void testSearchSubscriptions() {
        
        Test.startTest();   
            SR_PrepareOrderControllerTest.insertContactRoleAssociation();
            
            PageReference installationPage = Page.SubscriptionInstallation;
            Test.setCurrentPage(installationPage);    
            
                
            SubscriptionInstallationController subscriptionInstallation = new SubscriptionInstallationController();
            subscriptionInstallation.getLicenceStatuses();
            subscriptionInstallation.getInstallationStatuses();
            
            subscriptionInstallation.getSubscriptions();
            
            System.assertEquals(2, subscriptionInstallation.subscriptions.size());
            List<Subscription__c> subscriptions = subscriptionInstallation.subscriptions.values();
            subscriptionInstallation.updateSubscriptions();
            
            Attachment att = new Attachment();
            att.Name = 'TESTING';
            att.Body = Blob.valueOf('TESTING');
            att.parentId = subscriptions[0].Id;
            insert att;
            
            installationPage.getParameters().put('subscriptionId',att.parentId);
            installationPage.getParameters().put('macAddress','TESTMAC');
            
            subscriptionInstallation.updateSubscription();
            
            installationPage.getParameters().put('customerNumber','SAAS1212');
            installationPage.getParameters().put('customerName','Test Account');
            installationPage.getParameters().put('quoteNumber','TEST-QUOTE1');
            installationPage.getParameters().put('installationStatus','Pending');
            installationPage.getParameters().put('licenceStatus','Processing');
            installationPage.getParameters().put('contractNumber','TEST');
            installationPage.getParameters().put('siteProjectManager','1234567');
            installationPage.getParameters().put('soSpecialist','1234567');
            installationPage.getParameters().put('contractAdmin','1234567');
            installationPage.getParameters().put('salesManager','1234567');
            subscriptionInstallation.searchSubscriptions();
            
            System.debug([
                SELECT Id, Site_Project_Manager__c, Sales_Manager__c, Contract_Administrator__c,
                Sales_Operations_Specialist__c, Account__r.Name, Quote__r.Name, Name, Licence_Status__c,
                Installation_Status__c
                FROM Subscription__c
            ]);
            
            System.assertEquals(1, subscriptionInstallation.subscriptions.size());
        Test.stopTest();    
    }
    
    @testSetup private static void setUpSubscriptionTestData(){
        
        OrgWideNoReplyId__c orgId = new OrgWideNoReplyId__c();
        orgId.Name = 'NoReplyId';
        orgId.OrgWideId__c = [select id, Address, DisplayName from OrgWideEmailAddress][0].Id;
        insert orgId;
        
        Regulatory_Country__c regulatory = new Regulatory_Country__c();
        regulatory.Name = 'USA';
        insert regulatory;
        
        Account subscriptionAccount = TestUtils.getAccount();
        subscriptionAccount.AccountNumber = 'SAAS1212';
        subscriptionAccount.RecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        insert subscriptionAccount;
        
        Contact con = TestUtils.getContact();
        con.AccountId = subscriptionAccount.Id;
        con.ERP_Reference__c = '1234567';
        insert con;
        
        Set<String> roles = new Set<String>{'PM-Project Mgr', 'ZJ-FOA', 'ZN-Sales Mgr', 'ZO-Contract Admin'};
        
        List<Contact_Role_Association__c> contactRoles = new List<Contact_Role_Association__c>();
        
        for(String role : roles){
            Contact_Role_Association__c contactRole = new Contact_Role_Association__c();
            contactRole.Role__c = role;
            contactRole.Contact__c = con.Id;
            contactRole.Account__c = subscriptionAccount.Id;
            contactRoles.add(contactRole);
        }
        insert contactRoles;
        
        Contact_Role_Association__c contactRole = new Contact_Role_Association__c();
        contactRole.Role__c = 'PM-Project Mgr';
        contactRole.Contact__c = con.Id;
        contactRole.Account__c = con.AccountId;
        insert contactRole;
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = con.AccountId;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        ERP_Pcode__c pCode = new ERP_Pcode__c();
        pCode.Name = 'HAN';
        pCode.ERP_Pcode_2__c = 'AN';
        pCode.Status__c = 'Reserved';
        pCode.Service_Product_Group__c = 'ARIANOT';
        pCode.Serial_Number_Length__c = 4;
        pCode.ERP_Equipment_Template__c = 'HANXXXX-ARIANOT';
        insert pCode;
        
        Product_System_Subsystem__c pSystem = new Product_System_Subsystem__c();
        pSystem.Name = 'HAN-HAN-HAN';
        pSystem.ERP_PCode__c = 'HAN';
        pSystem.ERP_System_Code__c = 'HAN';
        pSystem.ERP_Subsystem_Code__c = 'HAN';
        pSystem.ERP_Pcode_ID__c = pCode.Id;
        insert pSystem;
        
        Product2 subscriptionProduct = new Product2();
        subscriptionProduct.Name = 'Sample Sales Product';
        subscriptionProduct.IsActive = true;
        subscriptionProduct.ProductCode = 'Test Code123';
        subscriptionProduct.Family = 'Subscription';
        subscriptionProduct.Subscription_Product_Type__c = 'Subscription';
        subscriptionProduct.Subscription_Product_Code__c = 'AN';
        
        Product2 installationProduct = new Product2();
        installationProduct.Name = 'Sample Sales Product';
        installationProduct.IsActive = true;
        installationProduct.ProductCode = 'Test Code123';
        installationProduct.Family = 'Subscription';
        installationProduct.Subscription_Product_Type__c = 'Installation';
        installationProduct.Subscription_Product_Code__c = 'AN';
        
        Product2 productModel = new Product2();
        productModel.Name = 'Sample Model';
        productModel.IsActive = true;
        productModel.ProductCode = 'HANA';
        productModel.Family = 'Subscription';
        productModel.ERP_PCode__c = pCode.Id;
        productModel.Assign_PCSN_Type__c = 'Link';
        productModel.ERP_PCode_2__c = 'AN';
        productModel.SAP_PCode__c = 'HAN';
        productModel.ERP_PCode_4__c = 'HANA';
        productModel.Subscription_Product_Code__c = 'AN';
        
        installationProduct.Subscription_Product_Type__c = 'Installation';
        insert new List<Product2>{subscriptionProduct,installationProduct,productModel};
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = subscriptionProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 2100;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        //insert pbEntry;
        PricebookEntry pbEntry1 = new PricebookEntry();
        pbEntry1.Product2Id = installationProduct.Id;
        pbEntry1.Pricebook2Id = standardPricebookId;
        pbEntry1.UnitPrice = 3500;
        pbEntry1.CurrencyISOCode = 'USD';
        pbEntry1.IsActive = true;
        insert new List<PricebookEntry>{pbEntry, pbEntry1};
        
        BigMachines__Quote__c subscriptionQuoteNew = TestUtils.getQuote();
        subscriptionQuoteNew.Name = 'TEST-QUOTE1';
        subscriptionQuoteNew.BigMachines__Account__c = con.AccountId;
        subscriptionQuoteNew.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteNew.National_Distributor__c = '121212';
        subscriptionQuoteNew.Order_Type__c = 'sales';
        subscriptionQuoteNew.Price_Group__c = 'Z2';
        
        BigMachines__Quote__c subscriptionQuoteRenewal = TestUtils.getQuote();
        subscriptionQuoteRenewal.Name = 'TEST-QUOTE2';
        subscriptionQuoteRenewal.BigMachines__Account__c = con.AccountId;
        subscriptionQuoteRenewal.BigMachines__Opportunity__c = opp.Id;
        subscriptionQuoteRenewal.National_Distributor__c = '121212';
        subscriptionQuoteRenewal.Order_Type__c = 'sales';
        subscriptionQuoteRenewal.Price_Group__c = 'Z2';
        
        insert new List<BigMachines__Quote__c>{subscriptionQuoteNew, subscriptionQuoteRenewal};
        
        AutoCreateBillingPlan.STOP_BILLING_PLAN_CRERATION = true;
        
        insert new List<BigMachines__Quote_Product__c>{
            getQuoteProduct(subscriptionQuoteNew.Id,subscriptionProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteNew.Id,installationProduct.Id, '3', 'Annually','New'), 
            getQuoteProduct(subscriptionQuoteRenewal.Id,subscriptionProduct.Id, '3', 'Annually','Renewal')
        };
        
        insertSubscriptions();
    }
    
    private static BigMachines__Quote_Product__c getQuoteProduct(Id quoteId,Id subscriptionProductId, String numberOfYears, String billingFrequency, String salesType){
        BigMachines__Quote_Product__c quoteProduct = new BigMachines__Quote_Product__c();
        quoteProduct.BigMachines__Quote__c = quoteId;
        quoteProduct.BigMachines__Product__c = subscriptionProductId;
        quoteProduct.Header__c = true;
        quoteProduct.Product_Type__c = 'Sales';
        quoteProduct.Standard_Price__c = 40000;
        quoteProduct.BigMachines__Sales_Price__c = 40000;
        quoteProduct.BigMachines__Quantity__c = 1;
        quoteProduct.Line_Number__c= 12;
        quoteProduct.EPOT_Section_Id__c = 'TEST';
        quoteProduct.Subscription_Start_Date__c = Date.today();
        quoteProduct.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
        quoteProduct.Number_Of_Years__c = numberOfYears;
        quoteProduct.Subscription_End_Date__c = Date.today().addYears(Integer.valueOf(numberOfYears));
        quoteProduct.Billing_Frequency__c = billingFrequency;
        quoteProduct.Subscription_Sales_Type__c = salesType;
        quoteProduct.Installation_Price__c = 0.0;
        quoteProduct.Subscription_Unit_Price__c = 2900;
        if(salesType != 'Renewal'){
            quoteProduct.SAP_Contract_Number__c = 'TEST1234';
        }
        return quoteProduct;
    }
    
    private static void insertSubscriptions(){
        Subscription__c subscription1 = new Subscription__c();
        subscription1.Name = 'TEST1234';
        //subscription1.Account__c = subscriptionAccount.Id;
        subscription1.Ext_Quote_Number__c = 'TEST-QUOTE1';
        subscription1.ERP_Project__c = false;
        subscription1.Number_Of_Licences__c = 5;
        subscription1.ERP_Site_Partner__c = 'Sales11111';
        subscription1.Functional_Location__c = 'TEST1234';
        subscription1.Installation_Status__c = 'Pending';
        subscription1.Licence_Status__c = 'Processing';
        subscription1.Site_Project_Manager__c = '1234567';
        subscription1.Sales_Operations_Specialist__c = '1234567';
        subscription1.Sales_Manager__c = '1234567';
        subscription1.Contract_Administrator__c = '1234567';
        subscription1.Product_Type__c = 'AN';
        //subscription1.PCSN__c = 'HAN0263-ARIANOT';
        subscription1.PCSN__c = 'HAN0100-ARIANOT';
        
        Subscription__c subscription2 = new Subscription__c();
        subscription2.Name = 'TEST789';
        subscription2.PCSN__c = 'TEST123-ARIANOT';
        subscription2.Functional_Location__c = 'TEST1234';
        subscription2.Number_Of_Licences__c = 3;
        subscription2.Ext_Quote_Number__c = 'TEST-QUOTE2';
        subscription2.ERP_Site_Partner__c = 'Sales11111';
        //subscription2.Account__c = subscriptionAccount.Id;
        subscription2.Installation_Status__c = 'Pending';
        subscription2.Licence_Status__c = 'Processing';
        subscription2.Product_Type__c = 'AN';
        subscription2.ERP_Project__c = false;
        
        insert new List<Subscription__c>{subscription1, subscription2};
    }
}
/**
 * @author Krishna Katve
 * @description This class handles trigger function for subscription product object
 * ** TEST COVERED BY SAASInstallationTest
 */
public with sharing class SubscriptionProductTriggerHandler {

    /**
     * This method updates subscription product fields before insert or update of subscription product
     */
    public static void updateSubscriptionProductFields(List<Subscription_product__c> subscriptionProducts){
        
        Set<String> equipmentNumbers = new Set<String>();
        Set<String> erpPartnerNumbers = new Set<String>();
        List<Subscription_Product__c> productsToBeUpdated = new List<Subscription_Product__c>();
        
        for(Subscription_Product__c subscriptionProduct: subscriptionProducts){
            System.debug('----subscriptionProduct.ERP_Equipment_Nbr__c'+subscriptionProduct.ERP_Equipment_Nbr__c);
            System.debug('----subscriptionProduct.Installed_Product__c'+subscriptionProduct.Installed_Product__c);
            if(
                (!String.isBlank(subscriptionProduct.ERP_Equipment_Nbr__c) && subscriptionProduct.Installed_Product__c==null) ||
                (!String.isBlank(subscriptionProduct.ERP_Partner_Number__c) && subscriptionProduct.Location__c==null)
            ){
                if(!String.isBlank(subscriptionProduct.ERP_Equipment_Nbr__c)){
                    equipmentNumbers.add(subscriptionProduct.ERP_Equipment_Nbr__c);
                }
                if(!String.isBlank(subscriptionProduct.ERP_Partner_Number__c)){
                    erpPartnerNumbers.add(subscriptionProduct.ERP_Partner_Number__c);
                }
                productsToBeUpdated.add(subscriptionProduct);
            }
        }   
        
        Map<String, SVMXC__Installed_Product__c> installedProducts = getInstalledProductsMap(equipmentNumbers);
        Map<String, SVMXC__Site__c> locations = getLocationMap(equipmentNumbers);
        
        for(Subscription_Product__c subscriptionProduct: productsToBeUpdated){
            if(installedProducts.containsKey(subscriptionProduct.ERP_Equipment_Nbr__c)){
                subscriptionProduct.Installed_Product__c = installedProducts.get(subscriptionProduct.ERP_Equipment_Nbr__c).Id;
            }
            if(locations.containsKey(subscriptionProduct.ERP_Partner_Number__c)){
                subscriptionProduct.Location__c = locations.get(subscriptionProduct.ERP_Partner_Number__c).Id;
            }
        }           
    }
    
    /*
     * Utility method which queries installed products and retuen map of erp reference and ip
     */
    private static Map<String, SVMXC__Installed_Product__c> getInstalledProductsMap(Set<String> equipmentNumbers){
        
        Map<String, SVMXC__Installed_Product__c> installedProducts = new Map<String, SVMXC__Installed_Product__c>();
        
        for(SVMXC__Installed_Product__c installedProduct : [
            SELECT Id, ERP_Reference__c, ERP_EQUIPMENT__c FROM SVMXC__Installed_Product__c
            WHERE ERP_EQUIPMENT__c IN :equipmentNumbers
        ]){
            installedProducts.put(installedProduct.ERP_EQUIPMENT__c, installedProduct);
        }
        return installedProducts;
    }
    
    /*
     * Utility method which queries location records and return map of erp partner number and location
     */
    private static Map<String, SVMXC__Site__c> getLocationMap(Set<String> partnerNumbers){
        
        Map<String, SVMXC__Site__c> locations = new Map<String, SVMXC__Site__c>();
        
        for(SVMXC__Site__c location : [
            SELECT Id, ERP_Site_Partner_Code__c FROM SVMXC__Site__c
            WHERE ERP_Site_Partner_Code__c IN :partnerNumbers
        ]){
            locations.put(location.ERP_Site_Partner_Code__c, location);
        }
        return locations;
    }
}
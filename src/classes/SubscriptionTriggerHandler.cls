/**
 * All subscription trigger functions
 */
public with sharing class SubscriptionTriggerHandler {
    
    /**
     * Before insert update the subscription fields
     * Fields updated : Quote__c, Primary_Contact_Email__c
     */
    public static void updateSubscriptionFields(){
        
        List<Subscription__c> subscriptions = [
            SELECT Id,Quote__r.Principal_Contact__r.Email
            FROM Subscription__c
            WHERE Id IN:trigger.new
        ];
        
        Set<String> quotesNames = new Set<String>();
        Set<String> sitePartnerNumbers = new Set<String>();
        Set<String> productTypes = new Set<String>();
        
        for(Subscription__c subscription : (List<Subscription__c>)trigger.new){
            if(String.isBlank(subscription.ERP_Contract_Number__c)){
                subscription.ERP_Contract_Number__c = subscription.Name;
            }
            if(subscription.Name.isNumeric()){
                subscription.Name = String.valueOf(Integer.valueOf(subscription.Name));
            }
            if(!String.isBlank(subscription.Ext_Quote_Number__c)){
                quotesNames.add(subscription.Ext_Quote_Number__c);
            }
            if(!String.isBlank(subscription.ERP_Site_Partner__c)){
                sitePartnerNumbers.add(subscription.ERP_Site_Partner__c);
            }
            if(!String.isBlank(subscription.Product_Type__c)){
                productTypes.add(subscription.Product_Type__c);
            }
        }
        
        Map<String,BigMachines__Quote__c> quotes = getQuotes(quotesNames);
        Map<String,ERP_Partner__c> sitePartners = getSitePartners(sitePartnerNumbers);
        Map<String,Map<String,String>> contactRoles = getQuoteContactRoles(quotesNames);
        Map<String,String> subscriptionModels = getSubscriptionModels(productTypes);
        Map<String,String> billingFrequencies = getSubscriptionBillingFrequencies(quotesNames);
        
        for(Subscription__c subscription : (List<Subscription__c>)trigger.new){
            if(quotes.containsKey(subscription.Ext_Quote_Number__c)){
                BigMachines__Quote__c bmQuote = quotes.get(subscription.Ext_Quote_Number__c);
                subscription.Quote__c = bmQuote.Id;
                subscription.Primary_Contact_Email__c = quotes.get(subscription.Ext_Quote_Number__c).Principal_Contact__r.Email;
                subscription.Account__c = bmQuote.BigMachines__Account__c;
                subscription.Billing_Frequency__c = billingFrequencies.get(subscription.Product_Type__c + subscription.Quote__c);
                updateContactRolesOnSubscription(subscription, contactRoles);
            }
            if(sitePartners.containsKey(subscription.ERP_Site_Partner__c)){
                subscription.Site_Partner__c = sitePartners.get(subscription.ERP_Site_Partner__c).Id;
            }
            if(!String.isBlank(subscription.Product_Type__c) && subscriptionModels.containsKey(subscription.Product_Type__c)){
                subscription.Model_Name__c = subscriptionModels.get(subscription.Product_Type__c);
            }
        }
    }
    
    private static Map<String, String> getSubscriptionBillingFrequencies(Set<String> quoteNumbers){
        Map<String,String> billingFreqencies = new Map<String,String>();
        for(BigMachines__Quote_Product__c quoteProduct : [
            SELECT Id, Billing_Frequency__c, Subscription_Product_Type__c,BigMachines__Quote__c
            FROM BigMachines__Quote_Product__c
            WHERE BigMachines__Quote__r.Name IN:quoteNumbers
            AND Subscription_Product_Type__c!=null
        ]){
            billingFreqencies.put(quoteProduct.Subscription_Product_Type__c + quoteProduct.BigMachines__Quote__c, quoteProduct.Billing_Frequency__c);
        }
        return billingFreqencies;
    }
    
    /**
     * get contact roles(quote product partner) related to subscription quote.
     */
    private static Map<String,Map<String,String>> getQuoteContactRoles(Set<String> quotesNames){
        Map<String,Map<String,String>> contactRoles = new Map<String,Map<String,String>>();
        List<Quote_Product_Partner__c> quoteProductPartners = [
            SELECT Id, ERP_Contact_Function__c, ERP_Contact_Number__c,Quote__r.Name
            FROM Quote_Product_Partner__c
            WHERE Quote__r.Name IN:quotesNames 
            AND ERP_Contact_Function__c != null
        ];
        for(Quote_Product_Partner__c quotePartner : quoteProductPartners){
            if(contactRoles.containsKey(quotePartner.Quote__r.Name)){
                contactRoles.get(quotePartner.Quote__r.Name).put(quotePartner.ERP_Contact_Function__c,quotePartner.ERP_Contact_Number__c);
            }else{
                contactRoles.put(quotePartner.Quote__r.Name, new Map<String,String>{quotePartner.ERP_Contact_Function__c => quotePartner.ERP_Contact_Number__c});
            }
        }
        return contactRoles;
    }
    
    /**
     * Update contact role numbers on given subscription using related quote's quote product partners
     */
    private static void updateContactRolesOnSubscription(Subscription__c subscription, Map<String,Map<String,String>> contactRoles){
        if(contactRoles.containsKey(subscription.Ext_Quote_Number__c)){
            if(contactRoles.get(subscription.Ext_Quote_Number__c).containsKey('PM')){
                subscription.Site_Project_Manager__c = contactRoles.get(subscription.Ext_Quote_Number__c).get('PM');
            }
            if(contactRoles.get(subscription.Ext_Quote_Number__c).containsKey('ZJ')){
                subscription.Sales_Operations_Specialist__c = contactRoles.get(subscription.Ext_Quote_Number__c).get('ZJ');
            }
            if(contactRoles.get(subscription.Ext_Quote_Number__c).containsKey('ZN')){
                subscription.Sales_Manager__c = contactRoles.get(subscription.Ext_Quote_Number__c).get('ZN');
            }
            if(contactRoles.get(subscription.Ext_Quote_Number__c).containsKey('ZO')){
                subscription.Contract_Administrator__c = contactRoles.get(subscription.Ext_Quote_Number__c).get('ZO');
            }
        }
    }
    
    /**
     * Get quote records using EXT_Quote_Number on subscription for populating quote lookup on subscription
     */
    private static Map<String,BigMachines__Quote__c> getQuotes(Set<String> quoteNames){
        Map<String,BigMachines__Quote__c> quotes = new Map<String,BigMachines__Quote__c>();
        
        for(BigMachines__Quote__c bmQuote : [
            SELECT Id,Name,Principal_Contact__r.Email,BigMachines__Account__c
            FROM BigMachines__Quote__c
            WHERE Name IN :quoteNames
        ]){
            quotes.put(bmQuote.Name,bmQuote);
        }
        return quotes;
    }
    
    /**
     * Get ERP Partner (Site partner) record using site partner number for populating Site Partner lookup on Subscription
     */
    private static Map<String,ERP_Partner__c> getSitePartners(Set<String> sitePartnerNumbers){
        Map<String,ERP_Partner__c> sitePartners = new Map<String,ERP_Partner__c>();
        
        if(!sitePartnerNumbers.isEmpty()){
            for(ERP_Partner__c erpPartner : [
                SELECT Id,Partner_Number__c
                FROM ERP_Partner__c
                WHERE Partner_Number__c IN :sitePartnerNumbers
            ]){
                sitePartners.put(erpPartner.Partner_Number__c,erpPartner);
            }
        }
        return sitePartners;
    }
    
    /**
     * After subscription record gets created in salesforce related Billing Plan to Subscription record
     */
    public static void updateSubscriptionLineItems(){
        Map<String,Id> subscriptionIds = new  Map<String,Id>();
        Map<Id, Id> subscriptionModules = new Map<Id, Id>();
        Set<Id> quoteIds = new Set<Id>();
        for(Subscription__c subscription : (List<Subscription__c>)trigger.new){
            subscriptionIds.put(subscription.Quote__c+'-'+subscription.Product_Type__c, subscription.Id);
            quoteIds.add(subscription.Quote__c);
        }
        System.debug('----subscriptionIds'+subscriptionIds);
        List<Subscription_Line_Item__c> subscriptionLines = [
            SELECT Id, Subscription__c, Quote_Product__r.SAP_Contract_Number__c,Quote_Product__r.BigMachines__Quote__c,Quote_Product__c,
            Quote_Product__r.Subscription_Product_Type__c
            FROM Subscription_Line_Item__c
            WHERE Quote_Product__r.BigMachines__Quote__c IN:quoteIds
            AND Subscription__c = null
        ];
        
        System.debug('----subscriptionLines'+subscriptionLines+'---'+subscriptionLines.size());
        
        for(Subscription_Line_Item__c billingtItem: subscriptionLines){
          String key = billingtItem.Quote_Product__r.BigMachines__Quote__c+'-'+billingtItem.Quote_Product__r.Subscription_Product_Type__c;
          System.debug('----key'+key);
            if(subscriptionIds.containsKey(key)){
                billingtItem.Subscription__c = subscriptionIds.get(key);
                subscriptionModules.put(billingtItem.Quote_Product__c, subscriptionIds.get(key));
            }
        }
        List<BigMachines__Quote_Product__c> quoteProducts = new List<BigMachines__Quote_Product__c>();
        for(Id qpId : subscriptionModules.keySet()){
            quoteProducts.add(new BigMachines__Quote_Product__c(Id=qpId, Subscription__c = subscriptionModules.get(qpId)));
        }
        if(!subscriptionLines.isEmpty()) update subscriptionLines;
        if(!quoteProducts.isEmpty()) update quoteProducts;
    }
    
    private static Map<String,String> getSubscriptionModels(Set<String> productTypes){
        Map<String,String> subscriptionModels = new Map<String,String>();
        List<Product2> products = [
            SELECT Id,Name,Subscription_Product_Code__c
            FROM Product2
            WHERE Product_Type__c = 'Model'
            AND Subscription_Product_Code__c IN:productTypes
        ];
        
        for(Product2 product : products){
            subscriptionModels.put(product.Subscription_Product_Code__c, product.Name);
        }
        return subscriptionModels;
    }
    
    /**
     * Update quote fields after subscription contract is created in salesforce
     */
    public static void updateQuoteFields(List<Subscription__c> subscriptions){
        Set<String> quoteNames = new Set<String>();
        Map<String,String> quoteSubscriptions = new Map<String,String>();
       
        for(Subscription__c  subscriptionContract : subscriptions){
            if(subscriptionContract.EXT_Quote_Number__c != null){
                quoteNames.add(subscriptionContract.EXT_Quote_Number__c);
                if(quoteSubscriptions.containsKey(subscriptionContract.EXT_Quote_Number__c)){
                    String subscriptionContractName = quoteSubscriptions.get(subscriptionContract.EXT_Quote_Number__c) + ', '+subscriptionContract.Name;
                    quoteSubscriptions.put(subscriptionContract.EXT_Quote_Number__c,subscriptionContractName);
                }else{
                    quoteSubscriptions.put(subscriptionContract.EXT_Quote_Number__c,subscriptionContract.Name);
                }
            }
        }
        
        List<BigMachines__Quote__c> quoteList = [Select Id,Name,SAP_Booked_Subscription__c, Subscription_Status__c  From BigMachines__Quote__c Where Name IN :quoteNames];
        
        for(BigMachines__Quote__c quote : quoteList){
            String bookedsubscriptionContract = quote.SAP_Booked_Subscription__c+','; 
            
            quote.SAP_Booked_Subscription__c = !String.isBlank(quote.SAP_Booked_Subscription__c)
                                            ?(quote.SAP_Booked_Subscription__c+','+quoteSubscriptions.get(quote.Name))
                                            :quoteSubscriptions.get(quote.Name);
            quote.Subscription_Status__c = 'Success';
        }
        if(!quoteList.isEmpty()){
            update quoteList;
        }
    }
    
    /**
     * STSK0012375 - SaaS - Update Installed Product Billing Type Based on both Service Contract and Subscription Status
     */
    public static void updateInstProdForBillingType(List<Subscription__c> subscriptions) {
        system.debug('---------updateInstProdForBillingType-----------------');
        Map<Id, Boolean> subscriptionMap = new Map<Id, Boolean>();
        for (Subscription__c subscription: subscriptions) {
            subscriptionMap.put(subscription.Id, subscription.Active__c);
        }

        if(!subscriptionMap.isEmpty()) {
            List<SVMXC__Installed_Product__c> ipList = [select Id, Subscription__c, Subscription_IsActive__c, Billing_Type__c from SVMXC__Installed_Product__c where Subscription__c in : subscriptionMap.keySet()];

            for (SVMXC__Installed_Product__c ip: ipList) {
                if(subscriptionMap.get(ip.Subscription__c)) {
                    ip.Billing_Type__c = 'C – Contract';
                } else {
                    ip.Billing_Type__c = 'P – Paid Service';
                }
            }

            system.debug('----------ipList---'+ipList);
            if (!ipList.isEmpty()) {
                update ipList;
                system.debug('--------Updated ipList-----------'+ipList);
            }
        }
    }
     /**
     * STSK0012375 - SaaS - Update Installed Product Billing Type Based on both Service Contract and Subscription Status
* STSK0011740 - Installed Product status Update for the SaaS product
1. Installed: When Subscription Active is true 
2. Inactive: When Subscription Active is false and Installed Product Status == "Installed" 
*/
    
    public static void updateInstProdFields(List<Subscription__c> subscriptions) {
        system.debug('---------updateInstProdStatus-----------------');
        Map<Id, Subscription__c > subscriptionMap = new Map<Id, Subscription__c>();
        for (Subscription__c subscription: subscriptions) {
            subscriptionMap.put(subscription.Id, subscription);
        }
        if(!subscriptionMap.isEmpty()) {
            List<SVMXC__Installed_Product__c> ipList = [select Id, Subscription__c, Subscription_IsActive__c, SVMXC__Status__c, Billing_Type__c from SVMXC__Installed_Product__c where Subscription__c in : subscriptionMap.keySet()];
            for (SVMXC__Installed_Product__c ip: ipList) {
                if(subscriptionMap.get(ip.Subscription__c).Active__c) {
                    ip.SVMXC__Status__c='Installed';
                    ip.Billing_Type__c = 'C – Contract';
                }
                else
                {
                    ip.Billing_Type__c = 'P – Paid Service'; 
                    if(ip.SVMXC__Status__c=='Installed')
                    {
                        ip.SVMXC__Status__c='Inactive'; 
                    }
                }
            }
            if (!ipList.isEmpty()) {
                update ipList;
                system.debug('-----Updated ipList-----------'+ipList);
            }
        }
    }
   
    
}
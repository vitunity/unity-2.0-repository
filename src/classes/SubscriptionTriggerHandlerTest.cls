@isTest(seeAllData=true)
private class SubscriptionTriggerHandlerTest {

    static testMethod void updateSubscriptionFirldsTest() {
        
        //SR_PrepareOrderControllerTest.setUpSubscriptionTestdata();
        Account salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SUBSCRIPTION ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        insert salesAccount;
        
		Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
		
        BigMachines__Quote__c  salesQuote = TestUtils.getQuote();
        salesQuote.Name = 'TEST SALES QUOTE';
        salesQuote.BigMachines__Account__c = salesAccount.Id;
        salesQuote.BigMachines__Opportunity__c = opp.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Principal_Contact__c = con.Id;
        salesQuote.Price_Group__c = 'Z2';
        insert salesQuote;
        
        Test.startTest();
          SR_PrepareOrderControllerTest.insertContactRoleAssociation();
          
          List<Contact_Role_Association__c> contactRoles = [
            SELECT Id,Contact__r.ERP_reference__c,Role__c
            FROM Contact_Role_Association__c
            WHERE Contact__r.ERP_reference__c = 'erpref123'
          ];
          List<Quote_Product_Partner__c> quotePartners = new List<Quote_Product_Partner__c>();
          for(Contact_Role_Association__c contactRole : contactRoles){
            Quote_Product_Partner__c quotePartner = new Quote_Product_Partner__c();
            quotePartner.Quote__c = salesQuote.Id;
            quotePartner.ERP_Contact_Function__c = contactRole.Role__c.subString(0,2);
            quotePartner.ERP_Contact_Number__c = contactRole.Contact__r.ERP_reference__c;
            quotePartners.add(quotePartner);
          }
          
          insert quotePartners;
          
          List<BigMachines__Quote__c> quotes = [
            SELECT Id 
            FROM BigMachines__Quote__c
            WHERE Name ='TEST SALES QUOTE'
            OR Name = 'TEST Subscription QUOTE'
          ];
          
          Subscription__c subscription1 = new Subscription__c();
          subscription1.Name = 'TEST1234';
          subscription1.Ext_Quote_Number__c = 'TEST SALES QUOTE';
          subscription1.ERP_Site_Partner__c = 'Sales11111';
          
          Subscription__c subscription2 = new Subscription__c();
          subscription2.Name = 'TEST12343';
          subscription2.Ext_Quote_Number__c = 'TEST Subscription QUOTE';
          
          Subscription__c subscription3 = new Subscription__c();
          subscription3.Name = 'TEST12344';
          subscription3.Ext_Quote_Number__c = '';
          
          Subscription__c subscription4 = new Subscription__c();
          subscription4.Name = 'TEST12345';
          subscription4.Ext_Quote_Number__c = 'QUOTE-TEST16767';
          
          List<Subscription__c> subscrictions = new List<Subscription__c>{subscription1,subscription2,subscription3,subscription4};
          insert subscrictions;
          
          List<Subscription__c> updatedSubscriptions = [
              SELECT Id,Quote__c,Primary_Contact_Email__c,Site_Partner__r.Partner_NUmber__c,Site_Partner__c,
              Sales_Manager__c,Sales_Operations_Specialist__c,Site_Project_Manager__c,Contract_Administrator__c
              FROM Subscription__c
              WHERE Id IN :subscrictions
          ];
          
          System.assertEquals(quotes[0].Id,updatedSubscriptions[0].Quote__c);
          //System.assertEquals(null,updatedSubscriptions[2].Quote__c);
          //System.assertEquals(null,updatedSubscriptions[3].Quote__c);
          
          /*System.assertEquals('krishna.katve@varian.com',updatedSubscriptions[0].Primary_Contact_Email__c);
          System.assertEquals('krishna.katve@varian.com',updatedSubscriptions[1].Primary_Contact_Email__c);
          System.assertEquals(null,updatedSubscriptions[2].Primary_Contact_Email__c);
          System.assertEquals(null,updatedSubscriptions[3].Primary_Contact_Email__c);
          
          System.assertEquals('Sales11111', updatedSubscriptions[0].Site_Partner__r.Partner_NUmber__c);
          System.assertEquals(null, updatedSubscriptions[1].Site_Partner__c);
          
          //Validate partners on subscription
          System.assertEquals(updatedSubscriptions[0].Sales_Manager__c,'erpref123');
          System.assertEquals(updatedSubscriptions[0].Sales_Operations_Specialist__c,'erpref123');
          System.assertEquals(updatedSubscriptions[0].Site_Project_Manager__c,'erpref123');
          System.assertEquals(updatedSubscriptions[0].Contract_Administrator__c,'erpref123');*/
    Test.stopTest();
    }
    

    static testMethod void updateInstPrdtBillingtypeOnSubscTest() {
        //SR_PrepareOrderControllerTest.setUpSubscriptionTestdata();
        Account salesAccount = TestUtils.getAccount();
        salesAccount.Name = 'SUBSCRIPTION ACCOUNT PREPARE ORDER';
        salesAccount.AccountNumber = 'Sales1212';
        salesAccount.Country__c = 'Japan';
        insert salesAccount;
        
		Contact con = TestUtils.getContact();
        con.AccountId = salesAccount.Id;
        insert con;
        
        System.debug('---getQueries()3'+Limits.getQueries());
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = salesAccount.Id;
        opp.Primary_Contact_Name__c = con.Id;
        insert opp;
		
        BigMachines__Quote__c  salesQuote = TestUtils.getQuote();
        salesQuote.Name = 'TEST SALES QUOTE';
        salesQuote.BigMachines__Account__c = salesAccount.Id;
        salesQuote.BigMachines__Opportunity__c = opp.Id;
        salesQuote.National_Distributor__c = '121212';
        salesQuote.Order_Type__c = 'sales';
        salesQuote.Principal_Contact__c = con.Id;
        salesQuote.Price_Group__c = 'Z2';
        insert salesQuote;
        
        Subscription__c subscription = new Subscription__c();
        subscription.Name = 'TESTSUBSCRIPTION';
        subscription.Ext_Quote_Number__c = salesQuote.Name;
        subscription.Number_Of_Licences__c = 4;
        insert subscription;
        
        Test.startTest();    
	
        Account newAcc = UnityDataTestClass.AccountData(true);
        SVMXC__Service_Group__c servTeam = UnityDataTestClass.serviceTeam_Data(true);
        BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        Timecard_Profile__c TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        SVMXC__Service_Group_Members__c technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        
        Product2 newProd = UnityDataTestClass.product_Data(true);
        
        // When Service Group inserted Location also get inserted
        SVMXC__Site__c newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        ERP_Pcode__c erpPcode = UnityDataTestClass.ERPPCODE_Data(true);

        Subscription__c subscription1 = new Subscription__c();
        subscription1.Name = 'TEST1234';
        subscription1.Ext_Quote_Number__c = 'TEST SALES QUOTE';
        subscription1.ERP_Site_Partner__c = 'Sales11111';
        subscription1.Status__c = 'Active';//Processed
        insert subscription1;
        
        subscription1 = [
              SELECT Id,Quote__c,Primary_Contact_Email__c,Site_Partner__r.Partner_NUmber__c,Site_Partner__c,
              Sales_Manager__c,Sales_Operations_Specialist__c,Site_Project_Manager__c,Contract_Administrator__c,Active__c
              FROM Subscription__c
              WHERE Name = 'TEST1234'
              ];

        SVMXC__Installed_Product__c installProd = new SVMXC__Installed_Product__c();
        installProd.Name = 'H621598';
        installProd.SVMXC__Status__c = 'Installed';
        installProd.ERP_Reference__c = 'H621598-VE';
        installProd.SVMXC__Product__c = newProd.Id;
        installProd.ERP_Pcodes__c = erpPcode.Id;
        installProd.SVMXC__Company__c = newAcc.Id;
        installProd.SVMXC__Site__c = newLoc.Id;
        installProd.ERP_Functional_Location__c = 'H-COLUMBUS -OH-US-005';
        installProd.Billing_Type__c = 'P – Paid Service';
        installProd.Service_Team__c = servTeam.Id;
        installProd.ERP_CSS_District__c = 'U9J';
        installProd.ERP_Work_Center__c = 'H87696';
        installProd.ERP_Product_Code__c = 'H62A';
        installProd.Subscription__c = subscription1.Id;
        insert installProd;

        System.assertEquals(installProd.Billing_Type__c, 'P – Paid Service');
        List<Subscription__c> subLst=new List<Subscription__c>{subscription1};
		SubscriptionTriggerHandler.updateInstProdForBillingType(subLst);
        SubscriptionTriggerHandler.updateInstProdFields(subLst);
        /*SVMXC__Service_Level__c slaterm = UnityDataTestClass.slaterm_Data(true, businesshr);
        SVMXC__Service_Contract__c servContract = UnityDataTestClass.servContract_Data(true, slaterm, newAcc, servTeam);
        installProd.SVMXC__Service_Contract__c = servContract.Id;
        update installProd;
        subscription1.Status__c = 'Active';
        update subscription1;
        System.assertEquals(installProd.Billing_Type__c, 'C – Contract');*/
        
        Test.stopTest();
      
      }
}
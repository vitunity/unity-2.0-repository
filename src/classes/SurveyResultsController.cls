public class SurveyResultsController{


    public class WrapperContact{
        public Contact con{get;set;}
        public boolean isChecked{get;set;}
        public WrapperContact(Contact con){
            this.isChecked = true;
            this.con= con;
             
        }
    }
    
    public Contact FilterContact{get;set;}
    
    
    public SurveyResultsController(){
        FilterContact = new Contact();       
        ListSearchContact = new List<WrapperContact>();        
    }
    public pagereference sendEmail(){
    
        try{            
            List<EmailTemplate> lstTemplate = [select Id from EmailTemplate where Name = 'HD Survey Template On Case Closure_TestSR' limit 1];  
            if(lstTemplate.size()>0 ){
                List<Messaging.SingleEmailMessage> lstEmails = new List<Messaging.SingleEmailMessage>();
                for(WrapperContact c : ListSearchContact){
                    if(c.isChecked){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setTargetObjectId(c.con.id);
                        mail.setWhatId(c.con.AccountId);                        
                        mail.setTemplateId(lstTemplate[0].Id);
                        mail.setSaveAsActivity(true); 
                        lstEmails.add(mail);                        
                    }
                }
                if(lstEmails <> null && lstEmails.size()>0){
                
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,lstEmails.size()+'-'+lstEmails));
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(lstEmails);
                    if (results[0].success) {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Email Sent Successfully'));
                    } else {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'The email failed to send: '+ results[0].errors[0].message));
                    }
                    
        
                }else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Please Select any one of Contact'));
                }
                
            }
            else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.info,'Email Template Contact: Follow Up (SAMPLE) not found'));
            }
        }
        catch(Exception e){       
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'Error : '+e.getMessage()+'::'+ e.getLineNumber()));
        }
        return null;
    }
    public List<WrapperContact> ListSearchContact{get;set;}
    public string sTerritory{get;set;}
    public List<selectOption> getAllTerritory(){
        List<selectOption> options = new List<selectOption>();
        sTerritory = 'US';
        options.add(new selectOption('APAC','APAC')); 
        options.add(new selectOption('EMEA','EMEA'));
        options.add(new selectOption('North America','North America'));
        options.add(new selectOption('Latin America','Latin America'));
        return options;
    }
    public pagereference findContact(){
        ListSearchContact = new list<WrapperContact>();
        
        if(FilterContact.Survey_Quarter__c == null || sTerritory == null )
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error,'Please select required values'));
        }
        
        else{
          for(Contact c: [select Id,Name,Territory__c,Survey_Quarter__c,Email,AccountId,Preferred_Language1__c,Relationship_Survey_Opt_Out__c    from Contact where Survey_Quarter__c =: FilterContact.Survey_Quarter__c and Territory__c =: sTerritory and Relationship_Survey_Opt_Out__c = false ]){
          //for(Contact c: [select Id,Name,Territory__c,Survey_Quarter__c,Email,AccountId,Preferred_Language1__c   from Contact where Survey_Quarter__c =: FilterContact.Survey_Quarter__c]){
                ListSearchContact.add(new WrapperContact(c));
            }
            
            if(ListSearchContact.size() == 0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info,'Records not found'));
            }
        }
        
        
        return null;
    }
    
    
    
}
@isTest
public class SurveyResultsController_Test{

   public static testMethod void SurveyResultsControllerTest () {
    
    
    Profile p = new profile();
    p = [Select id from profile where name = 'System Administrator'];
    User systemuser1 = [Select id from user where isActive = true and id !=:userInfo.getUserId() and ManagerId != null and Manager.isActive = true limit 1]; 
  
    recordtype rec = [Select id from recordtype where developername ='Site_Partner'];
    Account testAcc = accounttestdata.createaccount();
    testAcc.recordtype = rec;
    testAcc.Account_Status__c = 'Available for Use';
    testAcc.Territories1__c = 'North America';
    insert testAcc;
      
    Contact objContact = new Contact();
    objContact.AccountId = testAcc.Id;
    objContact.FirstName = 'Test';
    objContact.Relationship_Survey_Opt_Out__c = false;
    objContact.LastName = 'ContactTest';
    objContact.Survey_Quarter__c = 'FYQ1';
    objContact.CurrencyIsoCode = 'USD';
    objContact.Email = 'test.testerR@testing.com';
    objContact.MailingState = 'CA';
    objContact.Phone= '1235678';
    objContact.MailingCountry = 'USA';
    insert objContact;
    
    system.assertEquals(testAcc.Territories1__c,'North America');
    system.assertEquals(objContact.AccountId,testAcc.Id);
    //system.assertEquals(objContact.Territory__c,'North America');
    
    SurveyResultsController con = new SurveyResultsController();
    con.getAllTerritory();
    con.FilterContact.Survey_Quarter__c = 'FYQ1';
    con.sTerritory = 'North America';
    con.findContact();
    List<SurveyResultsController.WrapperContact> lsts = new List<SurveyResultsController.WrapperContact>();
    lsts.add(new SurveyResultsController.WrapperContact(objContact));
    con.ListSearchContact = lsts;
    con.sendEmail();
    
    
    
    }
}
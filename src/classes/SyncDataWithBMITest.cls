@isTest
private class SyncDataWithBMITest {
	
	@isTest static void testGetSessionId() {

		String mockResponse = getMockSessionIdResponse();

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new SyncDataWithBMISessionIdMock(200,'OK',mockResponse,new Map<String,String>()));
			
			SyncDataWithBMI syncData = new SyncDataWithBMI();
			syncData.getBMISessionID();
			System.assertEquals('TESTSessionID', SyncDataWithBMI.sID);
		Test.stopTest();
	}
	
	@isTest static void testSyncDataWith() {
		String mockResponse = getMockSessionIdResponse();
		
		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new SyncDataWithBMISessionIdMock(200,'OK',mockResponse,new Map<String,String>()));
			SyncDataWithBMI syncData = new SyncDataWithBMI();
			Database.executeBatch(syncData,200);
		Test.stopTest();
	}

	@testSetup private static void insertProductRegions(){
		
		BigMachines__c bmi = new BigMachines__c();
		bmi.Name = 'BMILogin';
		bmi.User_Name__c = 'Test';
		bmi.Password__c = 'TestPassword';
		bmi.Endpoint__c = 'https://devvarian.bigmachines.com/v1_0/receiver';
		bmi.SLSecurity__c = 'https://devvarian.bigmachines.com/bmfsweb/devvarian/schema/v1_0/security/';
		bmi.SLDataTables__c = 'https://devvarian.bigmachines.com/bmfsweb/devvarian/schema/v1_0/datatables/';
		insert bmi;
		
		List<Product_Region__c> productRegionList = new List<Product_Region__c>();
		Product2 varianProduct = new Product2();
		varianProduct.Name = 'Sample Product';
		insert varianProduct;
		for(Integer counter = 1; counter <= 200; counter++){
			Product_Region__c productRegion = new Product_Region__c();
			productRegion.Name = 'Test Region'+counter;
			productRegion.Type__c = 'Sample Type';
			productRegion.Region__C = 'Australia';
			productRegion.Product__C = varianProduct.Id;
			productRegionList.add(productRegion); 
		}
		insert productRegionList;
	}

	private static string getMockSessionIdResponse(){
		return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
					'<soapenv:Body>'+
                      	'<bm:loginResponse xmlns:bm="urn:soap.bigmachines.com">'+
                         	'<bm:status>'+
                            	'<bm:success>true</bm:success>'+
                            	'<bm:message>Successfully processed API for devvarian at Thu Apr 09 05:06:02 EDT 2015</bm:message>'+
                         	'</bm:status>'+
                         	'<bm:userInfo>'+
                            	'<bm:sessionId>TESTSessionID</bm:sessionId>'+
                         	'</bm:userInfo>'+
                      	'</bm:loginResponse>'+
                   	'</soapenv:Body>'+
				'</soapenv:Envelope>';
	}
	
}
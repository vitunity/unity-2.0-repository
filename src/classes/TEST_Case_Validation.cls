@isTest(SeeAllData = true)
public class TEST_Case_Validation
{
    
        public static Profile p = [Select id from profile where name = 'System Administrator'];
        public static User systemuser = [Select id from user where profileId = :p.id and isActive = true and id!= :userInfo.getUserId() AND UserRoleId != null limit 1];
        
        public static Account varAcc = AccountTestdata.createAccount();
        public static Contact varCont = new Contact();
        public static SVMXC__Installed_Product__c varIP_TL = SR_testdata.createInstalProduct();
        public static Id recCase_HD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
        
    public static testMethod void Test_Validation() 
    {
        insert varIP_TL;
        case caseobj = new case();
        caseobj.AccountId = varAcc.Id;
        caseobj.ContactId = varCont.id;                                       
        caseobj.ERP_Top_Level__c  = 'H11111';
        caseobj.recordtypeID = recCase_HD; 
        caseobj.Priority = 'Medium';
        caseobj.Reason = 'Pre - Sales Activity';
        caseobj.Priority = '1 - Emergency';
        //caseobj.Test_Equipment__c = varEquip.id;
        caseobj.Malfunction_Start__c = system.now();
        caseobj.Requested_Start_Date_Time__c = system.now() + 5 ; 
        caseobj.Requested_End_Date_Time__c = system.now() + 10 ; 
        caseobj.SVMXC__Actual_Restoration__c = system.now()+1;
        caseobj.Total_Downtime_Hrs__c  = 6;
        caseobj.ProductSystem__c  = varIP_TL.ID;
        caseobj.SVMXC__Top_Level__c = varIP_TL.ID;
        caseobj.Status = 'Open';
        caseobj.Subject = 'Out of Tolerance';
        caseobj.Local_Internal_Notes__c = 'Test Local Notes 3';
        caseobj.Local_Case_Activity__c = 'Test Local Activity';
        caseobj.Local_Subject__c = 'Test insert';
        caseobj.OwnerId = systemuser.Id;
        caseobj.Primary_Contact_Number__c = 'Mobile';
        caseobj.Local_Description__c = 'Test Local Desc';
        caseobj.Priority = 'Emergency';
        caseobj.Is_escalation_to_the_CLT_required__c = 'No';
        insert caseobj;
    }
}
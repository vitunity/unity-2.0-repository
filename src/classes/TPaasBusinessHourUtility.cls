public class TPaasBusinessHourUtility{
    public  string businesshourname = 'VMS Standard Hours from 9am to 5pm (America/Los Angeles)';
    map<Integer,MBusinessHour> mapBusinesshour;
    map<string,Integer> maydayNumber;
    
    public TPaasBusinessHourUtility(){
        List<BusinessHours> lstbh = [select id,name,MondayStartTime, MondayEndTime,TuesdayStartTime, TuesdayEndTime,WednesdayStartTime, WednesdayEndTime,ThursdayStartTime, ThursdayEndTime,FridayStartTime, FridayEndTime,SaturdayStartTime, SaturdayEndTime,SundayStartTime, SundayEndTime from BusinessHours where Name=: businesshourname];
        
        maydayNumber = new map<string,Integer>(); 
        mapBusinesshour = new map<Integer,MBusinessHour>();
        
        
        maydayNumber.put('Monday',1);maydayNumber.put('Tuesday',2);maydayNumber.put('Wednesday',3);maydayNumber.put('Thursday',4);maydayNumber.put('Friday',5);maydayNumber.put('Saturday',6);maydayNumber.put('Sunday',7);
        
        List<MBusinessHour> lstBusinessHours = new List<MBusinessHour>();
        for(BusinessHours bh: lstbh){
            if(bh.MondayStartTime <> null && bh.MondayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(1,bh.MondayStartTime,bh.MondayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.TuesdayStartTime <> null && bh.TuesdayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(2,bh.TuesdayStartTime,bh.TuesdayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.WednesdayStartTime <> null && bh.WednesdayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(3,bh.WednesdayStartTime,bh.WednesdayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.thursdayStartTime <> null && bh.thursdayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(4,bh.thursdayStartTime,bh.thursdayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.fridayStartTime <> null && bh.fridayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(5,bh.fridayStartTime,bh.fridayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.saturdayStartTime <> null && bh.saturdayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(6,bh.saturdayStartTime,bh.saturdayEndTime);lstBusinessHours.add(mh);
            }
            if(bh.sundayStartTime <> null && bh.sundayEndTime <> null){
                MBusinessHour mh = new MBusinessHour(7,bh.sundayStartTime,bh.sundayEndTime);lstBusinessHours.add(mh);
            }
            
        }
        
        for(MBusinessHour m: lstBusinessHours){
            mapBusinesshour.put(m.index,m);
        }
    }
    
    public class MBusinessHour{
        public Integer index;
        public time startdate;
        public time enddate;
        public MBusinessHour(Integer index, time startdate, time enddate){
            this.index = index;
            this.startdate = startdate;
            this.enddate = enddate;
        }
    }
    
    public datetime getDatetimeinPST(datetime dt){
        
        string dtstr = dt.format('yyyy/MM/dd/HH/mm/ss','PST');
        datetime st = Datetime.newInstance(Integer.valueOf(dtstr.split('/')[0]),Integer.valueOf(dtstr.split('/')[1]),Integer.valueOf(dtstr.split('/')[2]),Integer.valueOf(dtstr.split('/')[3]),Integer.valueOf(dtstr.split('/')[4]),Integer.valueOf(dtstr.split('/')[5]));
        return st;
    }
    public Integer getDayNumber(datetime dt){ 
        string st = dt.format('EEEE');
        return (maydayNumber.containsKey(st))?maydayNumber.get(st):0;
    }
    public boolean isDateTimeExistBeforeINBH(Datetime dt){ 
        Integer dayno = getDayNumber(dt);
        MBusinessHour bh = mapBusinesshour.get(dayno);
        if(bh <> null && bh.startdate > dt.time())return true;
        return false;
    }
    public boolean isDateTimeExistINBH(Datetime dt){ 
        Integer dayno = getDayNumber(dt);
        MBusinessHour bh = mapBusinesshour.get(dayno);
        if(bh <> null && bh.enddate >= dt.time())return true;
        return false;
    }
    public datetime getNextWorkingDateTime(Datetime dt){ 
        Integer dayno = getDayNumber(dt);        
        
        if(dayno < 5){
            dayno++;dt =dt.addDays(1);
        }else if(dayno == 5){
            dayno =1;dt =dt.addDays(3);
        }
        else if(dayno == 6){
            dayno=1;dt =dt.addDays(2);
        }
        else if(dayno == 7){
            dayno=1;dt =dt.addDays(1);
        }
        return datetime.newInstance(dt.date(),mapBusinesshour.get(dayno).enddate);
    }
    
    public  Datetime getNextPlanDueDate(Datetime planduedate, Integer noofdays){
        
        Datetime PSTDT = getDatetimeinPST(planduedate);          
    
        if(isDateTimeExistBeforeINBH(PSTDT)){
            PSTDT = getNextWorkingDateTime(PSTDT);
        }
        else if(isDateTimeExistINBH(PSTDT)){
            PSTDT = getNextWorkingDateTime(PSTDT);
        }
        else{            
            PSTDT = getNextWorkingDateTime(getNextWorkingDateTime(PSTDT));
        }
        
        for(Integer i=2;i<=noofdays;i++){
            PSTDT = getNextWorkingDateTime(PSTDT);
        }
        return convertBetweenTimezones(PSTDT);
    }
    
    private static DateTime convertBetweenTimezones (DateTime timestamp)
    {
        TimeZone originTimezone = Timezone.GetTimezone ('America/Los_Angeles');//PST Timezone
        TimeZone destTimezone = getUserTimezone (userinfo.getUserId());
        Integer offsetMillis = destTimezone.getOffset (timestamp) - originTimezone.getOffset (timestamp);
        return timestamp.AddSeconds (offsetMillis / 1000);
    }

    private static TimeZone getUserTimezone (Id userId)
    {
        User u = [SELECT TimeZoneSidKey FROM User WHERE Id =:userId];
        return Timezone.GetTimezone (u.TimeZoneSidKey);
    }
    
}
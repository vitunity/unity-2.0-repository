public class TPaasServiceResponses{
    public static string getGroupHistory(){
        string res = '{';
        res += '"ok": true,';
        res += '"latest": "1358547726.000003",';
        res += '"messages": [';
        res += '{';
        res +=  '"type": "message",';
        res +=  '"ts": "1358546515.000008",';
        res +=  '"user": "U2147483896",';
        res +=  '"text": "Hello"';
        res += '},';
        res += '{';
        res +=  '"type": "message",';
        res +=  '"ts": "1358546515.000007",';
        res +=  '"user": "U2147483896",';
        res +=  '"text": "World",';
        res +=  '"is_starred": true';
        res += '},';
        res += '{';
        res +=  '"type": "something_else",';
        res +=  '"ts": "1358546515.000007"';
        res += '}';
        res += '],';
        res += '"has_more": false';
        res += '}';
        return res;
    }
    
    public static string getUserList(){
        string res = '{';
        res +=  '"ok": true,';
        res +=  '"members": [';
        res +=   '{';
        res += '"id": "W012A3CDE",';
        res += '"team_id": "T012AB3C4",';
        res += '"name": "spengler",';
        res += '"deleted": false,';
        res += '"color": "9f69e7",';
        res += '"real_name": "spengler",';
        res += '"tz": "America/Los_Angeles",';
        res += '"tz_label": "Pacific Daylight Time",';
        res += '"tz_offset": -25200,';
        res += '"profile": {';
        res += '"avatar_hash": "ge3b51ca72de",';
        res += '"status_text": "Print is dead",';
        res += '"status_emoji": ":books:",';
        res += '"real_name": "Egon Spengler",';
        res += '"display_name": "spengler",';
        res += '"real_name_normalized": "Egon Spengler",';
        res += '"display_name_normalized": "spengler",';
        res += '"email": "spengler@ghostbusters.example.com",';
        res += '"image_24": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"image_32": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"image_48": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"image_72": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"image_192": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"image_512": "https://.../avatar/e3b51ca72dee4ef87916ae2b9240df50.jpg",';
        res += '"team": "T012AB3C4"';
        res += '},';
        res += '"is_admin": true,';
        res += '"is_owner": false,';
        res += '"is_primary_owner": false,';
        res += '"is_restricted": false,';
        res += '"is_ultra_restricted": false,';
        res += '"is_bot": false,';
        res += '"updated": 1502138686,';
        res += '"is_app_user": false,';
        res += '"has_2fa": false';
        res +=   '},';
        res +=   '{';
        res += '"id": "W07QCRPA4",';
        res += '"team_id": "T0G9PQBBK",';
        res += '"name": "glinda",';
        res += '"deleted": false,';
        res += '"color": "9f69e7",';
        res += '"real_name": "Glinda Southgood",';
        res += '"tz": "America/Los_Angeles",';
        res += '"tz_label": "Pacific Daylight Time",';
        res += '"tz_offset": -25200,';
        res += '"profile": {';
        res += '"avatar_hash": "8fbdd10b41c6",';
        res += '"image_24": "https://a.slack-edge.com...png",';
        res += '"image_32": "https://a.slack-edge.com...png",';
        res += '"image_48": "https://a.slack-edge.com...png",';
        res += '"image_72": "https://a.slack-edge.com...png",';
        res += '"image_192": "https://a.slack-edge.com...png",';
        res += '"image_512": "https://a.slack-edge.com...png",';
        res += '"image_1024": "https://a.slack-edge.com...png",';
        res += '"image_original": "https://a.slack-edge.com...png",';
        res += '"first_name": "Glinda",';
        res += '"last_name": "Southgood",';
        res += '"title": "Glinda the Good",';
        res += '"phone": "",';
        res += '"skype": "",';
        res += '"real_name": "Glinda Southgood",';
        res += '"real_name_normalized": "Glinda Southgood",';
        res += '"display_name": "Glinda the Fairly Good",';
        res += '"display_name_normalized": "Glinda the Fairly Good",';
        res += '"email": "glenda@south.oz.coven"';
        res += '},';
        res += '"is_admin": true,';
        res += '"is_owner": false,';
        res += '"is_primary_owner": false,';
        res += '"is_restricted": false,';
        res += '"is_ultra_restricted": false,';
        res += '"is_bot": false,';
        res += '"updated": 1480527098,';
        res += '"has_2fa": false';
        res +=   '}';
        res +=  '],';
        res +=  '"cache_ts": 1498777272,';
        res +=  '"response_metadata": {';
        res +=   '"next_cursor": "dXNlcjpVMEc5V0ZYTlo="';
        res +=  '}';
        res +=  '}';
        
        return res;
    }
    
     public class GroupCreateResponse{
        public boolean ok;
        public groupInformation SL_group;
    }

    public class groupInformation{
        public string id;
        public string name;
        public boolean is_group;
    }
    public static string getCreateGroupResponse(){
        string res = '{';
        res += '"ok": true,';
        res += '"group": {';
        res += '"id": "G024BE91L",';
        res += '"name": "secretplans",';
        res += '"is_group": true,';
        res += '"created": 1360782804,';
        res += '"creator": "U024BE7LH",';
        res += '"is_archived": false,';
        res += '"is_open": true,';
        res += '"last_read": "0000000000.000000",';
        res += '"latest": null,';
        res += '"unread_count": 0,';
        res += '"unread_count_display": 0,';
        res += '"members": [';
        res += '"U024BE7LH"';
        res += '],';
        res += '"topic": {';
        res += '"value": "Secret plans on hold",';
        res += '"creator": "U024BE7LV",';
        res += '"last_set": 1369677212';
        res += '},';
        res += '"purpose": {';
        res += '"value": "Discuss secret plans that no-one else should know",';
        res += '"creator": "U024BE7LH",';
        res += '"last_set": 1360782804';
        res += '}';
        res += '}';
        res += '}';
        return res;
    }
    
}
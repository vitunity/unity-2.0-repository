public class TPaasSlackHandler{
    public static void AfterInsertTPaas(List<TPaaS_Package_Detail__c> NewTPassList){  
        for(TPaaS_Package_Detail__c t :NewTPassList){
            CreateSlackGroup(t.Plan_Identifier__c);            
        } 
    }
    public static void AfterApprovedTPaas(List<TPaaS_Package_Detail__c> NewTPassList,map<Id,TPaaS_Package_Detail__c> oldmap){  
        set<string> setIdentifier = new set<string>();
        
        for(TPaaS_Package_Detail__c t :NewTPassList){
            if(t.Plan_Stage__c == 'Completed' && t.Plan_Stage__c <> oldmap.get(t.id).Plan_Stage__c)
            {setIdentifier.add(t.Plan_Identifier__c);}
        } 
        if(setIdentifier.size()>0){
             ArchieveGroupChannel(setIdentifier); 
        }
        
        for(TPaaS_Package_Detail__c t :NewTPassList){
            if(t.Assigned_Dosiometrist__c <> null && t.Assigned_Dosiometrist__c <> oldmap.get(t.id).Assigned_Dosiometrist__c)
            {onDosiometristChange(t.channel_id__c,t.Assigned_Dosiometrist__c, oldmap.get(t.id).Assigned_Dosiometrist__c);}
        } 
    }
    

    
    @future(callout=true)
    public static void CreateSlackGroup(string packetid){

        String SlackA;
        String SlackB;
        String authtkn=label.Tpaas_Bearer_token;
        string groupname = packetid.toLowerCase();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint('https://slack.com/api/groups.create');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+authtkn);
        req.setHeader('MethodRequested', 'groups.create');
        req.setBody('{"name":"'+groupname+'"}');
                
        string sBody;
            if(!test.isRunningTest()){          
                res = http.send(req);
                sBody = res.getBody();
            }else{
                sBody = TPaasServiceResponses.getCreateGroupResponse();
            }
        
        if (test.isRunningTest() || (res <> null && res.getStatusCode() == 200)) {
            system.debug('RR:Res'+res+'::'+res.getBody());
            sBody = sBody.replace('"group"','"SL_group"');
            GroupCreateResponse GroupRes = (GroupCreateResponse)JSON.deserialize(sBody,GroupCreateResponse.class); 
            String GroupId = GroupRes.SL_group.id;
            
            AddUsersIntoGroup(GroupId,packetid);
            
            if(GroupId <> null){
                TPaaS_Package_Detail__c tpaasObj = [select id,channel_id__c from TPaaS_Package_Detail__c where Plan_Identifier__c=:packetid] ;
                tpaasObj.channel_id__c=GroupId;
                update tpaasObj;
            }
        }
    }
    
    public static void UpdateSFDCUserANDContact(string packetid){
        map<string,string> SlackUserInfo = getSlackUserInfo();
        
        system.debug('RR:SlackUserInfo'+SlackUserInfo);
        List<User> lstUsers = new List<User>();
        List<Contact> lstContacts = new List<Contact>();
        TPaaS_Package_Detail__c tpaasObj = [select id,OwnerId,Assigned_Dosiometrist__c,PrescribedClinician__c from TPaaS_Package_Detail__c where Plan_Identifier__c=:packetid] ;
        for(User u: [select Id,Name,Email,SlackID__c from User where Id =: tpaasObj.OwnerId or Id =: tpaasObj.Assigned_Dosiometrist__c ]){
            string EmailStr = u.Email;
            system.debug('RR:EmailStr'+u.Id+'::'+u.Name+'::'+EmailStr);
            string EmailId = (EmailStr <> null)?(EmailStr.substring(0,(EmailStr).indexOf('@'))):null; 
            if(EmailId <> null && SlackUserInfo.containsKey(EmailId) && SlackUserInfo.get(EmailId) <> u.SlackID__c){
                u.SlackID__c = SlackUserInfo.get(EmailId);
                lstUsers.add(u);
            }
        }
        
        for(Contact u: [select Id,Name,Email,SlackID__c from Contact where Id =: tpaasObj.PrescribedClinician__c]){
            string EmailStr = u.Email;
            system.debug('RR:EmailStr'+u.Id+'::'+u.Name+'::'+EmailStr);
            string EmailId = (EmailStr <> null)?((EmailStr).substring(0,(EmailStr).indexOf('@'))):null; 
            if(EmailId <> null && SlackUserInfo.containsKey(EmailId) && SlackUserInfo.get(EmailId) <> u.SlackID__c){
                u.SlackID__c = SlackUserInfo.get(EmailId);
                lstContacts.add(u);
            }
        }    
        
        update lstUsers;
        update lstContacts;
    }
    public static void AddUsersIntoGroup(string sChannelId, string packetid){

        UpdateSFDCUserANDContact(packetid);
        set<string> setSlackIds = new set<string>();
        string slack1,slack2,slack3;
        TPaaS_Package_Detail__c tpaasObj = [select id,OwnerId,Assigned_Dosiometrist__r.SlackID__c,PrescribedClinician__r.SlackID__c from TPaaS_Package_Detail__c where Plan_Identifier__c=:packetid] ;
        slack1 = [select id,SlackID__c from user where id =: tpaasObj.OwnerId].SlackID__c;
        slack2 = (tpaasObj.Assigned_Dosiometrist__c <> null && tpaasObj.Assigned_Dosiometrist__r.SlackID__c <> null)?tpaasObj.Assigned_Dosiometrist__r.SlackID__c:null;
        slack3 = (tpaasObj.PrescribedClinician__c <> null && tpaasObj.PrescribedClinician__r.SlackID__c <> null)?tpaasObj.PrescribedClinician__r.SlackID__c:null;
        
        if(slack1 <> null)setSlackIds.add(slack1);
        if(slack2 <> null)setSlackIds.add(slack2);
        if(slack3 <> null)setSlackIds.add(slack3);
        AddUsersIntoPrivateChannel('invite',sChannelId,setSlackIds);
    }
    @future(callout=true)
    public static void onDosiometristChange(string sChannelId, Id dosiometristId,  Id olddosiometristId){
        map<string,string> SlackUserInfo = getSlackUserInfo();
        List<USer> lstUsers = new List<User>();
        for(User u: [select Id,Name,Email,SlackID__c from User where Id =: dosiometristId or Id=: olddosiometristId]){
            string EmailStr = u.Email;
            system.debug('RR:EmailStr'+u.Id+'::'+u.Name+'::'+EmailStr);
            string EmailId = (EmailStr <> null)?(EmailStr.substring(0,(EmailStr).indexOf('@'))):null; 
            if(EmailId <> null && SlackUserInfo.containsKey(EmailId) && SlackUserInfo.get(EmailId) <> u.SlackID__c){
                u.SlackID__c = SlackUserInfo.get(EmailId);
                lstUsers.add(u);
            }
        }
        if(lstUsers.size()>0){ update lstUsers;   }
        for(user u: [select Id,SlackID__c from User where Id =: dosiometristId and SlackID__c <> null]){
            AddUsersIntoPrivateChannel('invite',sChannelId,new set<string>{u.SlackID__c});
        }
        for(user u: [select Id,SlackID__c from User where Id =: olddosiometristId and SlackID__c <> null]){
            AddUsersIntoPrivateChannel('kick',sChannelId,new set<string>{u.SlackID__c});
        }
        
        
    }
    public static void AddUsersIntoPrivateChannel(string method, string sChannelId, set<string> setSlackIds ){
        
        string slackusers;
        for(string s: setSlackIds){
            if(s <> null && method == 'invite'){  if(slackusers == null) slackusers = '\''+s+'\'';  else slackusers += ',\''+s+'\'';       }
            if(s <> null && method == 'kick'){  if(slackusers == null) slackusers = s;  else slackusers += ','+s;   }
        }
        
        system.debug('RR:slackusers'+method+'::'+setSlackIds+'::'+slackusers);
        
        String authtkn=label.Tpaas_Bearer_token;
        if(slackusers <> null && authtkn <> null && sChannelId <> null){    
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint('https://slack.com/api/conversations.'+method);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+authtkn);
            if(method == 'invite')req.setHeader('MethodRequested', 'conversations.'+method);
            if(method == 'invite')req.setBody('{"channel":"'+sChannelId+'","users":"'+slackusers+'"}');
            else req.setBody('{"channel":"'+sChannelId+'","user":"'+slackusers+'"}');
            req.setTimeout(10000);
            String jasonstr1;
            try {  if(!test.isRunningTest()){  res = http.send(req); }
                system.debug('Users Added/Remove into channel:'+res.getBody());
            }
            catch(System.CalloutException e) {
                System.debug('Callout error: ' + e);
            }
        }
    }
    
    @future(callout=true)
    public static void ArchieveGroupChannel(set<string> packetids){
 
        for(TPaaS_Package_Detail__c tpaasObj : [select id,channel_id__c from TPaaS_Package_Detail__c where Plan_Identifier__c IN:packetids]){
            if(tpaasObj.channel_id__c <> null){
                try{
                    String authtkn=label.Tpaas_Bearer_token;
                    Http http = new Http();
                    HttpRequest req1 = new HttpRequest();
                    HttpResponse response= new HttpResponse();
                    req1.setHeader('Content-Type', 'application/json');
                    req1.setEndpoint('https://slack.com/api/groups.archive');
                    req1.setHeader('Authorization','Bearer '+authtkn);
                    req1.setMethod('POST');
                    req1.setBody('{"channel":"'+tpaasObj.channel_id__c+'"}');
                    
                    if(!test.isRunningTest()){          
                        response = http.send(req1);
                    }
                    System.debug('ArchieveGroupChannel:'+response.getBody());
                }
                catch(System.CalloutException e) {
                    System.debug('ArchieveGroupChannel: Callout Error: ' + e);
                }
            }
        }
    }  
    public static map<string,string> getSlackUserInfo(){
        map<string,string> slackinfo = new map<string,string>();
        try{
            String authtkn=label.Tpaas_Bearer_token;
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint('https://slack.com/api/users.list');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+authtkn);
            req.setTimeout(20000); 

            string sBody;
            if(!test.isRunningTest()){          
                res = http.send(req);
                sBody = res.getBody();
            }else{
                sBody = TPaasServiceResponses.getUserList();
            }
            System.debug('getSlackUserInfo RES:'+sBody);
            if (test.isRunningTest() || (res <> null && res.getStatusCode() == 200)) {
                SlackUsersInfo SlackinfoObj = (SlackUsersInfo)JSON.deserialize(sBody,SlackUsersInfo.class);
                if(SlackinfoObj <> null){
                    for(SlackMembers s: SlackinfoObj.members){
                        slackinfo.put(s.name,s.id);
                    }                   
                }
            }
        }
        catch(System.CalloutException e) {
            System.debug('getSlackUserInfo Callout error: ' + e);
        }
        return slackinfo;
    }

    public class SlackUsersInfo{
        public boolean ok;
        public List<SlackMembers> members;  
    }
    public class SlackMembers{
        public string id;
        public string name;
        public string team_id;
        public string real_name;
    }

    @future(callout=true)
    public static void AddTpaasCommentsinSlack(set<Id> commentIds){        
        for(TPassPackage_Comment__c comment : [select Id,Body__c,ChannelID__c,UserName__c from TPassPackage_Comment__c where id IN: commentIds]){        
            if(comment.ChannelID__c <> null){
                AddTpaasCommentsinSlack(comment.ChannelID__c, comment.Body__c, comment.UserName__c);
            }   
        }       
    }
    
    public static void AddTpaasStatusChangeNotification(List<TPaaS_Package_Detail__c> newlist, map<Id,TPaaS_Package_Detail__c> oldmap){
        
        
        set<Id> setTpaasIds = new set<Id>();
        if(oldmap <> null){
            for(TPaaS_Package_Detail__c t: newlist){
                if(t.Plan_Stage__c == 'Structures Ready for Review' && t.Plan_Stage__c <> oldmap.get(t.id).Plan_Stage__c){
                    setTpaasIds.add(t.Id);
                }
            }
        }
        if(setTpaasIds.size()>0){
            AddTpaasStatusChangeNotification(setTpaasIds);
        }
    }
    
    @future(callout=true)
    public static void AddTpaasStatusChangeNotification(set<Id> setTpaasIds){

        for(TPaaS_Package_Detail__c t: [select Id,Name,Plan_Identifier__c,Patient__c,Assigned_Dosiometrist__r.FirstName,channel_id__c from TPaaS_Package_Detail__c where id IN: setTpaasIds]){
            if(t.channel_id__c <> null && t.Assigned_Dosiometrist__r.FirstName <> null){
                //string commentBody = 'TPaas ('+t.Plan_Identifier__c+') Plan Stage Changed on '+system.now();
                string commentBody = 'The structures related to '+t.Patient__c+' are now available for review in Eclipse.';
                AddTpaasCommentsinSlack(t.channel_id__c, commentBody, t.Assigned_Dosiometrist__r.FirstName);
            }
        }
                
    }
    public static void AddTpaasCommentsinSlack(string ChannelID, string commentBody, string username){  
    
        try{                
            String authtkn=label.Tpaas_Bearer_token;
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            req.setEndpoint('https://slack.com/api/chat.postMessage');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Authorization','Bearer '+authtkn);
            req.setHeader('MethodRequested', 'chat.postMessage');
            req.setBody('{"channel":"'+ChannelID+'","text":"'+commentBody+'","username":"'+username+'","as_user":"false"}');
            req.setTimeout(10000); 

            if(!test.isRunningTest()){          
                res = http.send(req);
            }
            System.debug('AddTpaasCommentsinSlack RES:'+res.getBody());                         
        }
        catch(System.CalloutException e) {
            System.debug('AddTpaasCommentsinSlack Callout error: ' + e);
        }
        
    }
    public static void getCommentFromSlack(string packageId){
        List<User> Name= new List<User>(); 
        TPaaS_Package_Detail__c TPaasObj = [SELECT Id,channel_id__c from TPaaS_Package_Detail__c where Id =: PackageId];
    
        List<TPassPackage_Comment__c> prevTimeStamp = new List<TPassPackage_Comment__c>([select TimeStamp__c from TPassPackage_Comment__c where TimeStamp__c!=null and TPaaS_Package_Detail__c=:PackageId ORDER BY TimeStamp__c Desc limit 1]);
        Integer LastTimeStamp=0;
        if(prevTimeStamp.size() > 0)       LastTimeStamp =Integer.ValueOf(prevTimeStamp[0].get('TimeStamp__c'));
        
        string token = label.Tpaas_Bearer_token;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://slack.com/api/groups.history?channel='+TPaasObj.channel_id__c+'&token='+token+'&pretty=1');
        request.setMethod('GET');
        
        HttpResponse response;
        string sBody;
        if(!test.isRunningTest()){
            response = http.send(request);
            sBody = response.getBody(); 
        }       
        else{
            sBody= TPaasServiceResponses.getGroupHistory();
        }
        if (test.isRunningTest() || (response <> null && response.getStatusCode() == 200)) {
            
            
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(sBody);
            List<Object> messages = (List<Object>) results.get('messages');
            integer i =0;
            if(messages <> null){
                List<TPassPackage_Comment__c> lstTPassPackageComment = new List<TPassPackage_Comment__c>();
                for (Object message: messages) {
    
                    Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(Json.serialize(message));
                    string UserSlackId = string.valueof(m.get('user'));
    
                    Object bot = m.get('bot_id');
    
                    Object CommentBodySlack;
                    System.debug('sharvya'+'<@'+UserSlackId+'>'+' has joined the group');
                    if(m.get('text') !='<@'+UserSlackId+'>'+' has joined the group' && m.get('bot_id')==null){
                        CommentBodySlack= m.get('text');
                    }
    
                    string timestampslack =(String.valueOf(m.get('ts'))).substring(0,11);
                    Integer timeStamp=Integer.valueOf(timestampslack);    
                    
                    if(CommentBodySlack <> null && timeStamp > LastTimeStamp){
                        TPassPackage_Comment__c oTPassPackageComment = new TPassPackage_Comment__c();
                        oTPassPackageComment.TimeStamp__c=timestampslack;
                        oTPassPackageComment.TPaaS_Package_Detail__c = TPaasObj.Id;
                        oTPassPackageComment.Body__c= string.valueof(CommentBodySlack); 
                        oTPassPackageComment.Slack_Username__c=UserSlackId;  
                        List<User> userlist =[select id,Name from User where SlackID__c=:UserSlackId ];
                        
                        if(userlist.size()>0){
                            String TxtName=String.ValueOf(userlist[0].Id);
                            oTPassPackageComment.Submitted_By__c=TxtName;
                            oTPassPackageComment.Outbound__c=true;
                            lstTPassPackageComment.add(oTPassPackageComment);
                        }
                    }
                }
            
                if(lstTPassPackageComment.size()>0)
                insert lstTPassPackageComment;
            }
        }
    }

    public class GroupCreateResponse{
        public boolean ok;
        public groupInformation SL_group;
    }
    public class groupInformation{
        public string id;
        public string name;
        public boolean is_group;
    }
    public static string get2Digit(Integer s){
        return ((s<=99)? ((s<=9)? '0'+s:''+s): string.valueOf(s).substring(2));
    }
    public static string getCreatedTime(Datetime cd){
        return get2Digit(cd.Year())+get2Digit(cd.Month())+get2Digit(cd.day())+get2Digit(cd.hour())+get2Digit(cd.minute())+get2Digit(cd.second());
    }
    public static void setSequenceNumberBefore(List<TPaaS_Package_Detail__c> lstTpass ,map<Id, TPaaS_Package_Detail__c> oldmap){
        set<Id> ParentIds = new set<Id>();        
        for(TPaaS_Package_Detail__c  t: lstTpass ){
            if(oldmap == null || (oldmap <> null && t.parent__c <> oldmap.get(t.Id).parent__c)){
                ParentIds.add(t.parent__c);
            }
        }
        if(ParentIds.size() > 0 ){
            map<Id,TPaaS_Package_Detail__c> mapParent = new map<Id,TPaaS_Package_Detail__c>([select Id,Sequence__c from TPaaS_Package_Detail__c where Id IN: ParentIds]);
            for(TPaaS_Package_Detail__c  t: lstTpass ){
                if(oldmap == null || (oldmap <> null && t.parent__c <> oldmap.get(t.Id).parent__c)){
                    string pid = (oldmap == null)?getCreatedTime(system.now()):getCreatedTime(t.createddate);
                    t.Sequence__c = (t.parent__c <> null && mapParent.containsKey(t.parent__c))?mapParent.get(t.parent__c).Sequence__c+''+pid:pid;             
                }
            }
        }        
    }
}
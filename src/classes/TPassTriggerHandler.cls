public with sharing class TPassTriggerHandler{
    List<Id> id= new List<Id>();
   
    public void AfterInsert(List<TPaaS_Package_Detail__c> NewTPassList,Map<Id, TPaaS_Package_Detail__c> TPassNewMap ){  
        for(TPaaS_Package_Detail__c tp:NewTPassList){
            createchannel(tp.Plan_Identifier__c);
            
        } 
    }
    
    public void AfterUpdate(List<TPaaS_Package_Detail__c> NewTPassList,Map<Id, TPaaS_Package_Detail__c> oldMap){
        for(TPaaS_Package_Detail__c oTPaaSPackageDetail:NewTPassList){
            if(oTPaaSPackageDetail.Plan_Stage__c =='Approved'){
                id.add(oTPaaSPackageDetail.id);
                ArchieveChannel(oTPaaSPackageDetail.Plan_Stage__c,oTPaaSPackageDetail.channel_id__c,oTPaaSPackageDetail.id);
                system.debug('alert calling archieve');
                
                
            }
            
        }
        
    }
    
    @future(callout=true)
    public static void createchannel(string packetid){
        String SlackA;
        String SlackB;
         String authtkn=label.Tpaas_Bearer_token;
        String endpoint=label.Tpaas_channel_creation;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+authtkn);
        req.setHeader('MethodRequested', 'groups.create');
        string strbody='{"name":"'+packetid+'"}';
        req.setBody(strbody);
        System.debug(req.getbody());
        string jasonstr1;
        try {
            if(!Test.isRunningTest()){
                res = http.send(req);
                jasonstr1 = res.getBody();
                System.debug(res.getBody());
            }
            else{
                jasonstr1 = '{"ok":true,"group":{"id":"GC4U42RU5","name":"oid-00000315","is_group":true,"created":1533720895,"creator":"UAFLSSBME","is_archived":false,"name_normalized":"oid-00000315","is_mpim":false,"is_open":true,"last_read":"0000000000.000000","latest":null,"unread_count":0,"unread_count_display":0,"members":["UAFLSSBME"],"topic":{"value":"","creator":"","last_set":0},"purpose":{"value":"","creator":"","last_set":0},"priority":0},"warning":"missing_charset","response_metadata":{"warnings":["missing_charset"]}}';
            }
            System.debug(req.getBody());
            System.debug(jasonstr1);
            String cid;
            if(jasonstr1!=null && jasonstr1.length()>35){
                cid= jasonstr1.substring(26,35);
            }
            System.debug('check-1'+cid);
            List<TPaaS_Package_Detail__c> l = [select id,Assigned_Dosiometrist__c,PrescribedClinician__c,Plan_Identifier__c,channel_id__c from TPaaS_Package_Detail__c where Plan_Identifier__c=:packetid] ;
            
            String AssgndDos;
            String Prescclin; 
            AssgndDos=String.valueOf(l[0].get('Assigned_Dosiometrist__c'));
            Prescclin=String.valueOf(l[0].get('PrescribedClinician__c'));
            system.debug('@@@assgn'+AssgndDos+'!!!!pre'+Prescclin);
            List<User> userA= new List<User>();
            List<Contact> userPc= new List<Contact>();
            userA = [Select id,SlackID__c from User where Id=:AssgndDos];
            userPc=[Select id,SlackID__c from Contact where Id=:Prescclin];
            if(!userA.isEmpty()){
                SlackA=String.ValueOf(userA[0].get('SlackID__c'));
            }
            if(!userPc.isEmpty()){
                SlackB=String.ValueOf(userPc[0].get('SlackID__c'));
            }
            system.debug('cid@@@---- ' +cid);
            system.debug('@@slackA@@'+SlackA+'@@@SlackB'+SlackB);
            if(cid!=null && SlackB!=null && SlackA!=null )
            { 
                
                AddUser(cid,SlackA,SlackB);                  
                List<TPaaS_Package_Detail__c> lpp = [select id,Assigned_Dosiometrist__c,PrescribedClinician__c,Plan_Identifier__c,channel_id__c from TPaaS_Package_Detail__c where Plan_Identifier__c=:packetid] ;
                
                for(TPaaS_Package_Detail__c lp:lpp){
                    lp.channel_id__c=cid;
                    
                    update lp;
                    system.debug('alert'+lp);
                    
                }
                
            } 
            
            
        }
        catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug(res.toString());
        }
        
        
    }
    
    
    public static void AddUser(string sChannelId,String slack1,String slack2){
        String authtkn=label.Tpaas_Bearer_token;
        String endpoint=label.Tpaas_invite_users_to_chat;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+authtkn);
        req.setHeader('MethodRequested', 'conversations.invite');
        string strbody='{"channel":"'+sChannelId+'","users":"'+slack1+','+slack2+'"}';
        req.setBody(strbody);
        req.setTimeout(10000);
        System.debug(req.getbody());
        String jasonstr1;
        try {
            if(!Test.isRunningTest()){
                res = http.send(req);
                jasonstr1 = res.getBody();
                System.debug(res.getBody());
            }
            else{
                jasonstr1 = '{"ok":true,"channel":{"id":"GC4U42RU5","name":"oid-00000315","is_channel":false,"is_group":true,"is_im":false,"created":1533720895,"is_archived":false,"is_general":false,"unlinked":0,"name_normalized":"oid-00000315","is_shared":false,"creator":"UAFLSSBME","is_ext_shared":false,"is_org_shared":false,"shared_team_ids":["TAG565Y5A"],"pending_shared":[],"is_pending_ext_shared":false,"is_member":true,"is_private":true,"is_mpim":false,"last_read":"0000000000.000000","is_open":true,"topic":{"value":"","creator":"","last_set":0},"purpose":{"value":"","creator":"","last_set":0}},"warning":"missing_charset","response_metadata":{"warnings":["missing_charset"]}}';
            }
            System.debug(req.getBody());
            System.debug(jasonstr1+'jason');
            
        }
        catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug(res.toString());
        }
    }
    
    @future(callout=true)
    public static void ArchieveChannel(string status,String channelId,String id){
        String authtkn=label.Tpaas_Bearer_token;
        String endpoint=label.Tpaas_Archieve_channel;
        list<TPassPackage_Comment__c> lstTPassPackageComment = new list<TPassPackage_Comment__c>();
        System.debug('channelid'+channelId);
        system.debug('status'+status);
        system.debug('id'+id);
        for(TPaaS_Package_Detail__c oTPaaSPackageDetail:[select id,channel_id__c,Plan_Stage__c  from TPaaS_Package_Detail__c where Id=:id]){
            system.debug('query result'+oTPaaSPackageDetail);
            Http http = new Http();
            HttpRequest req1 = new HttpRequest();
            HttpResponse response= new HttpResponse();
            req1.setHeader('Content-Type', 'application/json');
            req1.setEndpoint(endpoint);
            req1.setHeader('Authorization','Bearer '+authtkn);
            req1.setMethod('POST');
            String body='{"channel":"'+channelId+'"}';
            
            req1.setBody(body);
            
            System.debug(req1.getbody());
            try {
                string jasonstr1;
                if(!Test.isRunningTest()){
                    response = http.send(req1);
                    jasonstr1 = response.getBody();
                    System.debug(response.getBody());
                    
                }
                else{
                    jasonstr1 = '{"ok":true,"warning":"missing_charset","response_metadata":{"warnings":["missing_charset"]}}';
                }
                System.debug(req1.getBody());
                
                System.debug(jasonstr1);
                
            }
            catch(System.CalloutException e) {
                System.debug('Callout error: ' + e);
                System.debug(response.toString());
            }
            
            
        }
        
    }
    
    
    public static string get2Digit(Integer s){
        return ((s<=99)? ((s<=9)? '0'+s:''+s): string.valueOf(s).substring(2));
    }
    public static string getCreatedTime(Datetime cd){
        return get2Digit(cd.Year())+get2Digit(cd.Month())+get2Digit(cd.day())+get2Digit(cd.hour())+get2Digit(cd.minute())+get2Digit(cd.second());
    }
    public static void setSequenceNumberBefore(List<TPaaS_Package_Detail__c> lstTpass ,map<Id, TPaaS_Package_Detail__c> oldmap){
        
        
        set<Id> ParentIds = new set<Id>();        
        for(TPaaS_Package_Detail__c  t: lstTpass ){
            if(oldmap == null || (oldmap <> null && t.parent__c <> oldmap.get(t.Id).parent__c)){
                ParentIds.add(t.parent__c);
            }
        }
        
        if(ParentIds.size() > 0 ){
            map<Id,TPaaS_Package_Detail__c> mapParent = new map<Id,TPaaS_Package_Detail__c>([select Id,Sequence__c from TPaaS_Package_Detail__c where Id IN: ParentIds]);
            for(TPaaS_Package_Detail__c  t: lstTpass ){
                if(oldmap == null || (oldmap <> null && t.parent__c <> oldmap.get(t.Id).parent__c)){
                    string pid = (oldmap == null)?getCreatedTime(system.now()):getCreatedTime(t.createddate);
                    t.Sequence__c = (t.parent__c <> null && mapParent.containsKey(t.parent__c))?mapParent.get(t.parent__c).Sequence__c+''+pid:pid;             
                }
            }
        }
        
    }
    
}
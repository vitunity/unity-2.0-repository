@isTest
public class TPassTriggerTest {
    @isTest public static void TestClassTPassTrigger(){
       
        account oAcc = new account();
        oAcc.Name = 'testName';
        oAcc.CurrencyIsoCode = 'USD';
        oAcc.Country__c = 'India';
        oAcc.BillingCity = 'test city';
        oAcc.State__c = 'Rajasthan';
        oAcc.State_Province_Is_Not_Applicable__c = true;
        insert oAcc;
        
        contact oCon = new contact();
        oCon.AccountId = oAcc.Id;
        oCon.FirstName = 'testFName';
        oCon.LastName = 'testLName';
        oCon.Functional_Role__c = 'Nurse';
        oCon.SlackID__c= 'UBDNJHMT3';
        oCon.Account_Admin__c = true;
        oCon.Email = 'testEmail@gmail.com';
        insert oCon;
        
        user oUser = new user();
        oUser.LastName = 'TestLName';
        oUser.Alias = 'Alias';
        oUser.Email = 'tpaass@tpaaas.com';
        oUser.CommunityNickname = 'testNick';
        oUser.Username = 'tpaastest12@testtpaas.com';
        oUser.TimeZoneSidKey = 'America/Los_Angeles';
        oUser.LocaleSidKey = 'en_US';
        oUser.EmailEncodingKey = 'UTF-8';
        oUser.ProfileId = [SELECT Id FROM Profile WHERE Name = 'VMS System Admin'].Id;
        oUser.LanguageLocaleKey = 'en_US';
       oUser.SlackID__c= 'UBDNJHMT3';
        Insert oUser;
         oUser.SlackID__c= 'UBDNJHMT3';
        test.startTest();
        
        update oUser;
        
        TPaaS_Package_Detail__c oTPD = new TPaaS_Package_Detail__c();
        oTPD.Name = 'aAkZ0000000CfBK';
        oTPD.Assigned_Dosiometrist__c = oUser.Id;
        oTPD.PrescribedClinician__c = oCon.Id;
        oTPD.Plan_Due_Date__c = system.today();
        insert oTPD;
       test.stopTest();
        
        oTPD.Plan_Stage__c = 'Approved';
        update oTPD;
    }
}
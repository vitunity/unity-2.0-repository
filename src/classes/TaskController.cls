public with sharing class TaskController{

    Task task = new Task();
    
    public TaskController(ApexPages.StandardController controller) {
      this.task = (Task)controller.getRecord();
      string strWhatId = Apexpages.currentPage().getParameters().get('oppIdd');
      string strOwnerId = Apexpages.currentPage().getParameters().get('ownerId');
      Task.WhatId = strWhatId ;
      Task.OwnerId = strOwnerId ;
        
    }

}
@isTest(SeeAllData=false)
private class TestBatchAddUserToSelectedPG{
   static testMethod void TestBatchAddUserToSelectedPG(){
        Test.StartTest();
        Group g = new Group();
        g.Name = 'TestAprRel';
        insert g;
        
        Product_Roles__c prObj = new Product_Roles__c();
        prObj.Name = 'TestAprRel';
        prObj.public_Group__c = g.Id;
        insert prObj;
        user userList = [select id from user where userRoleId != null limit 1];
        Account acc = new Account();
        acc.Name = 'TestAprRel';
        acc.OMNI_Address1__c = 'address test1';
        acc.OMNI_City__c = 'fremont';
        acc.Country__c = 'USA';
        acc.OMNI_Postal_Code__c = '93425';
        acc.OwnerID = userList.id;
        
        for(Product_Roles__c pr : [select Id,Product_Name__c,name from Product_Roles__c limit 3]){
            if(acc.Selected_Groups__c == null){            
                acc.Selected_Groups__c = pr.Name;
                
            }else{
                acc.Selected_Groups__c = '; '+acc.Selected_Groups__c+  pr.Name;
            }
        }
        
        insert acc; 
        System.debug('TestGroup'+acc.Selected_Groups__c);
        /*Account acc = new Account();
        List<Account> accList = [Select Id,
                      Selected_Groups__c,
                      Name,
                      OMNI_Address1__c,
                      OMNI_City__c,
                      Country__c,
                      OMNI_Postal_Code__c,
                      OwnerID from Account where RecordType.Name = 'Site Partner' limit 100];
        for(Account ac : accList){
            if(ac.Selected_Groups__c != null ){
                acc = ac;
                break;
            }
        }
        List<Contact> con = new List<Contact>();
        con = [select Id,
                      LastName,
                      Email,
                      AccountId,
                      MailingCountry,
                      Medical_School_Country_Others__c,
                      Medical_School_Others__c,
                      Medical_School_State_Others__c
                      from Contact where Id =: acc.Id limit 1];
                      
        */ 
        Contact con = new Contact();
        con.FirstName = 'TestAprRel1FN';
        con.LastName = 'TestAprRel1';
        con.Email = 'Test@1234APR.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Kar';
        insert con; 
        
        //if(con != null){
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.RunAs(thisUser) {
                Profile p = [select id from profile where Name =: Label.VMS_Customer_Portal_User limit 1];
                //UserRole roleId = [select id, name from UserRole where Name =: Label.VMS_Customer_Portal_User limit 1];
                User u = new User();
                //u.UserRoleId = roleId.id;
                u.LastName = con.LastName;//con.get(0).LastName;
                u.Email = con.Email;//con.get(0).Email;
                u.UserName = con.Email;//con.get(0).Email;
                u.contactId = con.Id; //con.get(0).Id;
                u.alias = 'TstApr';
                u.LocaleSidKey = 'en_US';
                u.LanguageLocaleKey = 'en_US';
                u.TimeZoneSidKey = 'America/Los_Angeles';
                u.emailencodingkey='UTF-8';
                u.profileId = p.Id;
                insert u;
                set<Id> userIdSet = new set<Id>();
                userIdSet.add(u.Id);
                
                
                BatchAddUserToSelectedPG bsg = new BatchAddUserToSelectedPG(userIdSet,acc.Id);
                
                database.executeBatch(bsg,1);
               Test.StopTest();
            }
            
        //}    
   }
   
   /*static testMethod void TestRegularGroupListMethod1(){
        
        Group g = new Group();
        g.Name = 'test';
        insert g;
        
        /*Product_Roles__c prObj = new Product_Roles__c();
        prObj.Name = 'Test';
        prObj.public_Group__c = g.Id;
        insert prObj;*/
     /*   user userList = [select id from user where userRoleId != null limit 1];
        Account acc = new Account();
        acc.Name = 'Test';
        acc.OMNI_Address1__c = 'address test1';
        acc.OMNI_City__c = 'fermont';
        acc.Country__c = 'USA';
        acc.OMNI_Postal_Code__c = '93425';
        acc.OwnerID = userList.id;
        
        for(Product_Roles__c pr : [select Id,Product_Name__c,Public_Group__c from Product_Roles__c limit 3]){
            if(acc.Selected_Groups__c == null){            
                acc.Selected_Groups__c = pr.Public_Group__c;
            }else{
                acc.Selected_Groups__c = '; '+acc.Selected_Groups__c+  pr.Public_Group__c;
            }
        }
        
        insert acc;
       // Account acc = [select id from account where OwnerId =: userList.Id];
        
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@123.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Guj';
        insert con;
        
        Profile p = [select id from profile where Name =: Label.VMS_Customer_Portal_User limit 1];
        UserRole roleId = [select id, name from UserRole limit 1];
        User u = new User();
        u.UserRoleId = roleId.id;
        u.LastName = con.LastName;
        u.Email = con.Email;
        u.UserName = con.Email;
        u.contactId = con.Id;
        u.alias = 'test123';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.emailencodingkey='UTF-8';
        u.profileId = p.Id;
        insert u;
        
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        RegularGroupListController trglc = new RegularGroupListController(controller);
        system.debug(''+trglc.leftOptionsHidden);
        system.debug(''+trglc.rightOptionsHidden);
        trglc.save();
   }*/

}
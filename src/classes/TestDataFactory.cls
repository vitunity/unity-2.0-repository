/***********************************************************************
* Author         : Varian
* Description	 : TestDataFactory class for creating test data
* User Story     : 
* Created On     : 5/27/16
************************************************************************/
@isTest
public with sharing class TestDataFactory {
	
	public static List<Account> createAccounts() {
		List<Account> accounts = Test.loadData(Account.sObjectType, 'Test_AccountData');
		return accounts;
	}
	
	public static List<FieldTrackingConfig__c> createFieldTrackingConfig() {
		List<FieldTrackingConfig__c> configs = Test.loadData(FieldTrackingConfig__c.sObjectType, 'Test_FieldTrackingConfigData');
		return configs; 	
	}
	
	public static List<Field_Audit_Trail__c> createFieldAuditTrail() {
		List<Field_Audit_Trail__c> audits = Test.loadData(Field_Audit_Trail__c.sObjectType, 'Test_FieldAuditTrailData');
		return audits; 	
	}
	
	public static List<sObject> createObjects(Schema.SObjectType sObjTyp, String staticFielName) {
		List<sObject> objects = Test.loadData(sObjTyp, staticFielName);
		return objects;
	}
	
	public static List<Invoice__c> createInvoices(List<Account> accounts, List<Contact> contacts) {
		List<Invoice__c> invoices = new List<Invoice__c>();
		for (Integer i=0; i<contacts.size(); i++) {
			Invoice__c inv = new Invoice__c(Amount__c = 1000, Invoice_Date__c = System.Today() - 15, Invoice_Due_Date__c = System.Today() - 6, 
				Billing_Contact__c = contacts[i].Id, Sold_To__c = contacts[i].AccountId, First_Remainder_Sent__c =false);
			invoices.add(inv);
		}
		return invoices;
	}
	
	public static User createUser(String profName) {
		String profileName = '%'+profName+'%';
		List<User> users = new List<User>();
		users = [Select Id, Name From User where Email = 'bruce.wayne@wayneenterprises.com'];
		User sysAdmin;
		if (users != null && users.size() > 0) {
			sysAdmin = 	users[0];	
		} else {
			Profile adminProfile = [select Id from Profile where Name LIKE :profileName Limit 1];
			sysAdmin = new User( FirstName = 'Bruce1', LastName = 'Wayne1', Alias = 'Batman1', email = 'bruce.wayne@wayneenterprises.com', languagelocalekey = 'en_US', Title = 'Engineer', Phone='4081234567',
			localesidkey = 'en_US', emailencodingkey = 'UTF-8', timezonesidkey = 'America/Los_Angeles', username = 'bruce_wayn1@wayneenterprises.com',ProfileId = adminProfile.Id, isActive = true);
		}
		return sysAdmin;
	}
	
	public static user getUser(String email) {
		List<User> users = new List<User>();
		users = [Select Id, Name From User where Email =:email];
		return users != null && users.size() > 0 ? users[0] : null;
	}
	
}
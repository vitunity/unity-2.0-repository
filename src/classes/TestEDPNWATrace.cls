@isTest(seeAllData = true)
public class TestEDPNWATrace
{
    static testMethod void testNWATrace()
    {
        
        ERP_Project__c erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        ERP_WBS__c erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id);
        insert erpwbs;
        ERP_WBS__c erpwbs2 = new ERP_WBS__c(name = 'erpwbstest2',ERP_Project__c = erpproject.id);
        insert erpwbs2;
        
        ERP_NWA__c erpnwa = new ERP_NWA__c(WBS_Element__c = erpwbs.Id, ERP_Status__c = 'REL;SETC');
        insert erpnwa;
        
        erpnwa.WBS_Element__c = erpwbs2.Id;
        update erpnwa;
        
        scheduler_ERPNWAAlert schObj = new scheduler_ERPNWAAlert();
        test.StartTest();
            String crone = '0  00 1 3 * ?';
            system.schedule('Test', crone, schObj);
        test.StopTest();
        
        
    }

}
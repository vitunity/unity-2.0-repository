@isTest
private class TestRegularGroupListController{
    
    static testMethod void TestRegularGroupListMethod(){
        
        Group g = new Group();
        g.Name = 'test';
        insert g;
        
        Product_Roles__c prObj = new Product_Roles__c();
        prObj.Name = 'Test';
        prObj.public_Group__c = g.Id;
        insert prObj;
        user userList = [select id from user where userRoleId != null limit 1];
        Account acc = new Account();
        acc.Name = 'Test';
        acc.OMNI_Address1__c = 'address test1';
        acc.OMNI_City__c = 'fermont';
        acc.Country__c = 'USA';
        acc.OMNI_Postal_Code__c = '93425';
        acc.OwnerID = userList.id;
        
        for(Product_Roles__c pr : [select Id,Product_Name__c,Public_Group__c from Product_Roles__c limit 3]){
            if(acc.Selected_Groups__c == null){            
                acc.Selected_Groups__c = pr.Public_Group__c;
            }else{
                acc.Selected_Groups__c = '; '+acc.Selected_Groups__c+  pr.Public_Group__c;
            }
        }
        
        insert acc;
       /* Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@123.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Guj';
        insert con;
        
        Profile p = [select id from profile where Name =: Label.VMS_Customer_Portal_User limit 1];
        UserRole roleId = [select id, name from UserRole limit 1];
        User u = new User();
        u.UserRoleId = roleId.id;
        u.LastName = con.LastName;
        u.Email = con.Email;
        u.UserName = con.Email;
        u.contactId = con.Id;
        u.alias = 'test123';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.emailencodingkey='UTF-8';
        u.profileId = p.Id;
        insert u;*/
        
        /*user u = new user();
        set<Id> conIdset = new set<Id>();
         List<User> ContactPortalUserList = new List<User>();
        ContactPortalUserList = [select id, name , ContactId from User where Profile.Name =: Label.VMS_Customer_Portal_User and isActive != false limit 5]; 
        
        for(User u : ContactPortalUserList){
            conIdset.add(u.ContactId);
        }
        List<Contact> conList = new List<Contact>();
        conList = [select Id,Name,AccountId from Contact where Id in : conIdset];
        for(Contact c : conList){
            c.AccountId = acc.Id;
        }
        update conList;
        */
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        RegularGroupListController trglc = new RegularGroupListController(controller);
        trglc.unselectclick();
        trglc.selectclick();
        trglc.save();
        trglc.cancel();
        trglc.updateAccountFields();
        
        AssignUserToGroupController temp = new AssignUserToGroupController();
        temp.changeAccount();
        temp.MultipicklistGroupData();
        temp.contactRoleAssoc.Account__c = acc.Id;
        temp.MultipicklistGroupData();
   }
   
   /*static testMethod void TestRegularGroupListMethod1(){
        
        Group g = new Group();
        g.Name = 'test';
        insert g;
        
        /*Product_Roles__c prObj = new Product_Roles__c();
        prObj.Name = 'Test';
        prObj.public_Group__c = g.Id;
        insert prObj;*/
     /*   user userList = [select id from user where userRoleId != null limit 1];
        Account acc = new Account();
        acc.Name = 'Test';
        acc.OMNI_Address1__c = 'address test1';
        acc.OMNI_City__c = 'fermont';
        acc.Country__c = 'USA';
        acc.OMNI_Postal_Code__c = '93425';
        acc.OwnerID = userList.id;
        
        for(Product_Roles__c pr : [select Id,Product_Name__c,Public_Group__c from Product_Roles__c limit 3]){
            if(acc.Selected_Groups__c == null){            
                acc.Selected_Groups__c = pr.Public_Group__c;
            }else{
                acc.Selected_Groups__c = '; '+acc.Selected_Groups__c+  pr.Public_Group__c;
            }
        }
        
        insert acc;
       // Account acc = [select id from account where OwnerId =: userList.Id];
        
        Contact con = new Contact();
        con.LastName = 'Test';
        con.Email = 'Test@123.com';
        con.AccountId = acc.Id;
        con.Mailingcountry = 'India';
        con.Medical_School_Country_Others__c = 'Ind';
        con.Medical_School_Others__c = 'Ind';
        con.Medical_School_State_Others__c = 'Guj';
        insert con;
        
        Profile p = [select id from profile where Name =: Label.VMS_Customer_Portal_User limit 1];
        UserRole roleId = [select id, name from UserRole limit 1];
        User u = new User();
        u.UserRoleId = roleId.id;
        u.LastName = con.LastName;
        u.Email = con.Email;
        u.UserName = con.Email;
        u.contactId = con.Id;
        u.alias = 'test123';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.emailencodingkey='UTF-8';
        u.profileId = p.Id;
        insert u;
        
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        RegularGroupListController trglc = new RegularGroupListController(controller);
        system.debug(''+trglc.leftOptionsHidden);
        system.debug(''+trglc.rightOptionsHidden);
        trglc.save();
   }*/
   
    static testMethod void testMultipicklistGroupData(){
        
        Account partnerAccount = new Account(Name = 'Test Partner Acccount',Country__c='Cuba', BillingCity='BuenosAries');
        insert partnerAccount;
        
        Contact partnerContact = new Contact(FirstName = 'Test Contact',MailingCountry = 'Canada',MailingState = 'state',LastName = 'Test Contact', AccountId = partnerAccount.Id,Email = 'testemail@testemail.com');
        insert partnerContact;
        
        //Create user for the contact
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Partner Community Login User' Limit 1];
        User user1 = new User(
            Username = 'test12345test1cUnique'+math.random()+'@test.com',
            ContactId = partnerContact.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345test1cUnique@testemail.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'test last name',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1); 
        system.debug('****partnerAccount.Id***'+partnerAccount.Id);
        Account acc = [Select Id,IsPartner from Account where Id =: partnerAccount.Id];
        
        system.assert(acc.IsPartner==true);
        
        Contact_Role_Association__c cra = new Contact_Role_Association__c();
        cra.Account__c = partnerAccount.Id;
        
        AssignUserToGroupController controller = new AssignUserToGroupController();
        controller.contactRoleAssoc = cra;
        controller.MultipicklistGroupData();
    }
}
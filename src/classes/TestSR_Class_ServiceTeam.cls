@isTest
public class TestSR_Class_ServiceTeam
{
  public static Map<String,Schema.RecordTypeInfo> serviceTeamRecordType = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName();
    static testMethod void testSR_Class_ServiceTeamMethod()
    {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
        SVMXC__Service_Group__c serviceTeam1 = UnityDataTestClass.serviceTeam_Data(true);
        SVMXC__Service_Group__c serviceTeam2 = UnityDataTestClass.serviceTeam_Data(true);
        List<SVMXC__Service_Group__c> groups = new List<SVMXC__Service_Group__c>();
        groups.add(serviceTeam1);
        groups.add(serviceTeam2);
        Map<id,SVMXC__Service_Group__c> Oldmap = new Map<id,SVMXC__Service_Group__c>();
        Oldmap.put(serviceTeam1.Id, serviceTeam1);
        Oldmap.put(serviceTeam2.Id, serviceTeam2);
        SR_Class_ServiceTeam.create_InvLocation(groups, Oldmap);
    }
	static testMethod void testSRtriggerAfterTest()
    {
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        SVMXC__Territory__c tObj = new SVMXC__Territory__c(name= 'test');
	    tObj.SVMXC__Territory_Code__c = 'US8';
        insert tObj;
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
        SVMXC__Service_Group__c serviceTeam1 = UnityDataTestClass.serviceTeam_Data(false);
        serviceTeam1.SVMXC__Group_Code__c = 'US8';
        serviceTeam1.Equipment_Required__c = false;
        insert serviceTeam1;
        serviceTeam1.Dispatch_Queue__c = 'CSA NA';
        serviceTeam1.SVMXC__Group_Code__c = 'US9';
        serviceTeam1.Equipment_Required__c = true;
        update serviceTeam1;
    }
    
  	static testMethod void UpdateDM_FOA_SC_Test()
    {  
      Profile pr = [Select id from Profile where name = 'VMS BST - Member'];
      User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',
                      languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', 
                      username = 'standardtestuse92@testclass.com');
      insert u;
        Account newAcc = UnityDataTestClass.AccountData(true);
        Product2 newProd = UnityDataTestClass.product_Data(true);
        SVMXC__Site__c newLoc = UnityDataTestClass.Location_Data(true, newAcc);
        SVMXC__Service_Group__c serviceTeam1 = UnityDataTestClass.serviceTeam_Data(false);
        serviceTeam1.FOA__c = null;
        serviceTeam1.District_Manager__c = null;
        
        SVMXC__Service_Group__c serviceTeam2 = UnityDataTestClass.serviceTeam_Data(false);
        serviceTeam2.SVMXC__Group_Code__c = serviceTeam1.SVMXC__Group_Code__c+'-E';
        serviceTeam2.RecordTypeId = serviceTeamRecordType.get('Equipment').getRecordTypeId();
        serviceTeam2.District_Manager__c = null;
        serviceTeam2.FOA__c = null;
        serviceTeam2.SC__c = null;
        
        List<SVMXC__Service_Group__c> groups = new List<SVMXC__Service_Group__c>();
        groups.add(serviceTeam1);
        groups.add(serviceTeam2);
        insert groups;
        
        Map<id,SVMXC__Service_Group__c> Oldmap = new Map<id,SVMXC__Service_Group__c>();
        Oldmap.put(serviceTeam1.Id, serviceTeam1);
        Oldmap.put(serviceTeam2.Id, serviceTeam2);
                
        Test.StartTest();
          List<BusinessHours> listBusinessHrs = [Select ID from BusinessHours limit 1];
          SVMXC__Service_Group_Members__c objTechEquip = new SVMXC__Service_Group_Members__c();
      objTechEquip.Name = 'Test Technician';
          objTechEquip.SVMXC__Active__c = true;
          objTechEquip.RecordTypeID = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Equipment').getRecordTypeId();
          objTechEquip.SVMXC__Enable_Scheduling__c = true;
          objTechEquip.SVMXC__Phone__c = '987654321';
          objTechEquip.SVMXC__Email__c = 'abc@xyz.com';
          objTechEquip.SVMXC__Service_Group__c = serviceTeam1.ID;
          objTechEquip.SVMXC__Street__c = 'Test Street';
          objTechEquip.SVMXC__Zip__c = '98765';
          objTechEquip.SVMXC__Country__c = 'United States';
          objTechEquip.OOC__c = false;
          objTechEquip.SVMXC__Working_Hours__c = listBusinessHrs[0].ID;
          objTechEquip.User__c = userInfo.getUserID();
          objTechEquip.Dispatch_Queue__c = 'DISP CH';
          insert objTechEquip;
          
          serviceTeam1.FOA__c = u.Id;
          update serviceTeam1;
          
          serviceTeam2.District_Manager__c = u.Id;
          //update serviceTeam2;
          
          //SR_Class_ServiceTeam.UpdateDM_FOA_SC(new List<SVMXC__Service_Group__c>{serviceTeam1}, new Map<Id, SVMXC__Service_Group__c>{serviceTeam1.Id => serviceTeam1}, true);
        Test.StopTest();
    }

}
@isTest(SeeAllData=true)
public class TestSR_CustHistoryRelatedListComp
{
    static testMethod void testSR_CustHistoryRelatedListCompMethod()
    {
        List<ContactHistory> contactHistory = [Select ContactId, CreatedDate, CreatedById, Field, NewValue, OldValue from ContactHistory limit 100];
        if(contactHistory.size() > 0)
        {
            List<Contact> contacts = [Select Id from Contact where Id =: contactHistory[0].ContactId];
            SR_CustHistoryRelatedListComp_Controller sHistory = new SR_CustHistoryRelatedListComp_Controller();
            sHistory.myObject = contacts[0];
            //sHistory.getObjectHistory();
            sHistory.updatedRecordLimit = 1;
            sHistory.recordLimit = 1;
            sHistory.refreshObjectHistory();
        }
        
        List<ContactHistory> contactHistory1 = [Select ContactId, CreatedDate, CreatedById, Field, NewValue, OldValue from ContactHistory where Field = 'created' limit 100];
        if(contactHistory1.size() > 0)
        {
            List<Contact> contacts1 = [Select Id from Contact where Id =: contactHistory1[0].ContactId];
            SR_CustHistoryRelatedListComp_Controller sHistory = new SR_CustHistoryRelatedListComp_Controller();
            sHistory.myObject = contacts1[0];
            //sHistory.getObjectHistory();
            sHistory.updatedRecordLimit = 1;
            sHistory.recordLimit = 1;
            sHistory.refreshObjectHistory();
        }
        
        
        List<OpportunityFieldHistory> oppoHistory = [Select OpportunityId, CreatedDate, CreatedById, Field, NewValue, OldValue from OpportunityFieldHistory limit 100];
        if(oppoHistory.size() > 0)
        {
            List<Opportunity> oppos = [Select Id from Opportunity where Id =: oppoHistory[0].OpportunityId];
            SR_CustHistoryRelatedListComp_Controller sHistory = new SR_CustHistoryRelatedListComp_Controller();
            sHistory.myObject = oppos[0];
            //sHistory.getObjectHistory();
            sHistory.updatedRecordLimit = 1;
            sHistory.recordLimit = 1;
            sHistory.refreshObjectHistory();
        }
    }
}
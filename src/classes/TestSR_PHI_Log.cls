@isTest
public class TestSR_PHI_Log
{
    static testMethod void testSR_PHI_Log()
    {
        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;
      
        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = cs.Accountid;  // Added by Mohit 3-Mar-14 Defect
        phi.Product_ID__c = cs.ProductSystem__c;   // Added by Mohit 3-Mar-14 Defect //DE3449 SVMXC__Top_Level__c to ProductSystem__c
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Regulatory Ownership Assigned and Transferred';

        ApexPages.StandardController controller = new ApexPages.StandardController(phi);
        Apexpages.currentpage().getparameters().put('CFlkid',cs.id);
        Apexpages.currentpage().getparameters().put('Disposition', 'Transferred - Owner Copy Destroyed/Sent to New Owner');
        Apexpages.currentpage().getparameters().put('Encryption', 'Yes');
        SR_PHI_Log shi = new SR_PHI_Log(controller);
        shi.checkDestroyed();
        shi.checkEncrypt();
        shi.dosave();
    }    
    
    static testMethod void testSR_PHI_Log1()
    {
        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;
      
        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = cs.Accountid;  // Added by Mohit 3-Mar-14 Defect
        phi.Product_ID__c = cs.ProductSystem__c;   // Added by Mohit 3-Mar-14 Defect //DE3449 SVMXC__Top_Level__c to ProductSystem__c
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Shared - Owner Copy Retained';
        ApexPages.StandardController controller = new ApexPages.StandardController(phi);
        Apexpages.currentpage().getparameters().put('CFlkid',cs.id);
        Apexpages.currentpage().getparameters().put('Disposition', 'Shared - Owner Copy Retained');
        Apexpages.currentpage().getparameters().put('Encryption', 'No');
        SR_PHI_Log shi = new SR_PHI_Log(controller);
        shi.checkDestroyed();
        shi.checkEncrypt();
        shi.dosave();
    } 
    static testMethod void testSR_PHI_Log2()
    {
        Account acc=new Account();
        acc.name='Test Account';
        acc.ERP_Timezone__c='Aussa';
        acc.country__c = 'India';
        insert acc;
        
        contact cont = new contact();
        cont.FirstName= 'Megha';
        cont.lastname= 'Arora';
        cont.Accountid= acc.id;
        cont.department='Rad ONC';
        cont.MailingCity='New York';
        cont.MailingCountry='US';
        cont.MailingStreet='New Jersey2,';
        cont.MailingPostalCode='552601';
        cont.MailingState='CA';
        cont.Mailing_Address1__c= 'New Jersey2';
        cont.Mailing_Address2__c= '';
        cont.Mailing_Address3__c= '';
        cont.Phone= '5675687';
        cont.Email='test@gmail.com';
        insert cont;
        
        SVMXC__Installed_Product__c testinstallprd = SR_testdata.createInstalProduct();
        testinstallprd.SVMXC__Company__c = acc.id;
        insert testinstallprd;
                
        case cs = new case();
        cs.subject='This is a test case';
        cs.Accountid=acc.id;
        cs.Contactid=cont.id;
        cs.priority = 'low';
        cs.ProductSystem__c = testinstallprd.id;
        insert cs;
      
        PHI_Log__c phi = new PHI_Log__c();
        phi.Case__c= cs.Id;
        phi.Log_Type__c='Case';
        phi.Account__c = cs.Accountid;  // Added by Mohit 3-Mar-14 Defect
        phi.Product_ID__c = cs.ProductSystem__c;   // Added by Mohit 3-Mar-14 Defect //DE3449 SVMXC__Top_Level__c to ProductSystem__c
        phi.Date_Obtained__c = system.today();
        phi.Disposition2__c = 'Shared - Owner Copy Retained';
        phi.Storage_Location2__c = 'Server (In house lab)'; 
        phi.Server_Location__c = NULL;
        ApexPages.StandardController controller = new ApexPages.StandardController(phi);
        Apexpages.currentpage().getparameters().put('CFlkid',cs.id);
        Apexpages.currentpage().getparameters().put('Disposition', 'Shared - Owner Copy Retained');
        Apexpages.currentpage().getparameters().put('Encryption', 'No');
        SR_PHI_Log shi = new SR_PHI_Log(controller);
        shi.phi = phi;
        shi.statusOptions = null;
        shi.checkDestroyed();
        shi.checkEncrypt();
        shi.dosave();
    } 
}
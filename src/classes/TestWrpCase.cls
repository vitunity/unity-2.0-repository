@isTest
public class TestWrpCase
{
    static testMethod void testWrpCaseMethod()
    {
        String CaseRecType = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 
        String recTypeHD = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 
        
        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = '1232';
        testAcc.CSS_District__c = 'Test';
        insert testAcc;
        Contact con = new Contact(FirstName = 'TestAprRel1FN', LastName = 'TestAprRel1', Email = 'Test@1234APR.com', AccountId = testAcc.Id, MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '1234567');
        insert con;
            
        //Create Location
        SVMXC__Site__c loc = new SVMXC__Site__c();
        loc.SVMXC__Account__c = testAcc.Id;
        loc.Name = 'Test Location';
        insert loc;
        
        case cs = new case();
        cs.subject = 'test';
        cs.RecordTypeId = recTypeHD;
        cs.AccountId = testAcc.id;
        cs.SVMXC__Site__c = loc.id;
        cs.ContactId = con.id;
        insert cs;
        
        wrpCase wc = new wrpCase(cs, true);
    }
}
@isTest
public class TestWrpCaseLine
{
    static testMethod void TestWrpCaseLineMethod()
    {
        Case casealias = new Case(Subject = 'testsubject');
        casealias.Priority = 'High';
        insert casealias;
    
        Sales_Order_Item__c soi = new Sales_Order_Item__c(Status__c = 'Open');
        insert soi;
        
        SVMXC__Case_Line__c caseLine = SR_testdata.createCaseLine();
        caseLine.Sales_Order_Item__c = soi.Id;
        caseLine.SVMXC__Case__c = casealias.Id;
        caseLine.SVMXC__Priority__c = 'Medium';
        insert caseLine;
        wrpCaseLine wc = new wrpCaseLine(caseLine, true);
    }
}
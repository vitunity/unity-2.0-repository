@isTest
public class TestWrpCounterDetails
{
    static testMethod void testWrpCounterDetailsMethod()
    {

        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = 'AA787799';
        testAcc.CSS_District__c = 'Test';
        insert testAcc;
    
        SVMXC__Site__c loc = new SVMXC__Site__c();
        loc.SVMXC__Account__c = testAcc.Id;
        loc.Name = 'Test Location';
        insert loc;
        
        SVMXC__Service_Pricebook__c objSP = new SVMXC__Service_Pricebook__c();
        objSP.SVMXC__Active__c = true;
        objSP.Name ='price1';
        insert objSP;     
    
        SVMXC__Service_Plan__c objPlan= new SVMXC__Service_Plan__c();
        objPlan.SVMXC__Service_Pricebook__c = objSP.id;
        insert objPlan;

        list<SVMXC__Service_Level__c> lstSLaTerm = new list<SVMXC__Service_Level__c>();
        SVMXC__Service_Level__c objserviceLevel = new SVMXC__Service_Level__c();
        objserviceLevel.Name= 'TestSlaTerm001';
        lstSLaTerm.add(objserviceLevel);

        SVMXC__Service_Level__c objserviceLevel1 = new SVMXC__Service_Level__c();
        objserviceLevel1.Name = 'TestSlaTerm002';
        lstSLaTerm.add(objserviceLevel1);
        Insert lstSLaTerm;

        //Create Service Contract  
        SVMXC__Service_Contract__c testServiceContract = Sr_testdata.createServiceContract();
        testServiceContract.SVMXC__Service_Level__c = lstSLaTerm[0].id;
        testServiceContract.SVMXC__Company__c = testAcc.Id;
        testServiceContract.Location__c =loc.id;
        testServiceContract.SVMXC__Service_Plan__c  = objPlan.id;
        insert testServiceContract;
    
        List<SVMXC__Service_Contract_Products__c> lstOfCovrdProd= new List<SVMXC__Service_Contract_Products__c>();

        SVMXC__Service_Contract_Products__c covrProd1 = new SVMXC__Service_Contract_Products__c();
        covrProd1.SVMXC__Service_Contract__c = testServiceContract.id ;
        covrProd1.ERP_Site_Partner__c = 'sitePartner';
        covrProd1.SVMXC__Product_Line__c = 'ARIA';
        covrProd1.Serial_Number_PCSN__c = 'sr123';
        covrProd1.ERP_Functional_Location__c = 'funcLocation';
        covrProd1.Product_Code__c = 'pro123';
        covrProd1.Cancelation_Reason__c = 'cancel';
        //covrProd1.SVMXC__SLA_Terms__c = sla.id;
        lstOfCovrdProd.add(covrProd1);
        insert lstOfCovrdProd;  
        
        SVMXC__Counter_Details__c scd = new SVMXC__Counter_Details__c();
        scd.SVMXC__Active__c = true;
        scd.SVMXC__Covered_Products__c = covrProd1.id;
        insert scd;
        
        SVMXC__Counter_Details__c counterDetail = null;
        Boolean selected = true;
        Date strAcceptancedate = Date.today();
        String strNWAName = 'acme';
        String strEstimatedWork = 'acme';
        Boolean disabled = true;

        wrpCounterDetails wrp1 = new wrpCounterDetails(counterDetail, selected, strAcceptancedate, strNWAName, strEstimatedWork);
        wrpCounterDetails wrp2 = new wrpCounterDetails(scd, selected, strAcceptancedate, strNWAName, strEstimatedWork, disabled);
        
        wrp1.compareTo(wrp1);
        wrp2.compareTo(wrp2);
    }
}
@isTest
public class TestWrpProdService
{
    static testMethod void TestWrpProdServiceMethod()
    {
        Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = 'AA787799';
        testAcc.CSS_District__c = 'Test';
        insert testAcc;
    
        SVMXC__Service_Pricebook__c objSP = new SVMXC__Service_Pricebook__c();
        objSP.SVMXC__Active__c = true;
        objSP.Name ='price1';
        insert objSP;      

        SVMXC__Service_Plan__c objPlan= new SVMXC__Service_Plan__c();
        objPlan.SVMXC__Service_Pricebook__c = objSP.id;
        insert objPlan;
    
        SVMXC__Site__c loc = new SVMXC__Site__c();
        loc.SVMXC__Account__c = testAcc.Id;
        loc.Name = 'Test Location';
        insert loc;
    
        Product2 objProd2=new Product2();
        objProd2.Name = 'technical Test Product';
        objProd2.Material_Group__c = 'H:TRNING';
        objProd2.Budget_Hrs__c = 10.0;
        objProd2.Credit_s_Required__c = 10;
        objProd2.UOM__c = 'EA';
        insert objProd2;

        SVMXC__Service__c objAvailableSErvice = new SVMXC__Service__c();
        objAvailableSErvice.Name = 'TestOnsiteService';
        objAvailableSErvice.Product__c = objProd2.id;
        insert objAvailableSErvice;
        
        list<SVMXC__Service_Level__c> lstSLaTerm = new list<SVMXC__Service_Level__c>();
        SVMXC__Service_Level__c objserviceLevel = new SVMXC__Service_Level__c();
        objserviceLevel.Name= 'TestSlaTerm001';
        lstSLaTerm.add(objserviceLevel);

        SVMXC__Service_Level__c objserviceLevel1 = new SVMXC__Service_Level__c();
        objserviceLevel1.Name = 'TestSlaTerm002';
        lstSLaTerm.add(objserviceLevel1);
        Insert lstSLaTerm;

        SVMXC__Service_Contract__c testServiceContract = Sr_testdata.createServiceContract();
        testServiceContract.SVMXC__Service_Level__c = lstSLaTerm[0].id;
        testServiceContract.SVMXC__Company__c = testAcc.Id;
        testServiceContract.Location__c =loc.id;
        testServiceContract.SVMXC__Service_Plan__c  = objPlan.id;
        insert testServiceContract;
        
        SVMXC__SLA_Detail__c objSlaDEtail = new SVMXC__SLA_Detail__c();
        objSlaDEtail.SVMXC__Available_Services__c = objAvailableSErvice.id;
        objSlaDEtail.Service_Maintenance_Contract__c= testServiceContract.id;
        objSlaDEtail.SVMXC__SLA_Terms__c = lstSLaTerm[0].id;
        
        SVMXC__Installed_Product__c testInstallProduct = SR_testdata.createInstalProduct();
        testInstallProduct.ERP_Functional_Location__c = 'USA';
        testInstallProduct.SVMXC__Serial_Lot_Number__c ='test';
        insert testInstallProduct;

        SVMXC__Service_Contract_Products__c testContractProduct = new SVMXC__Service_Contract_Products__c();
        testContractProduct.SVMXC__Installed_Product__c = testInstallProduct.id;
        testContractProduct.SVMXC__Service_Contract__c = testServiceContract.id;
        testContractProduct.SVMXC__SLA_Terms__c =  lstSLaTerm[1].id;
        testContractProduct.Cancelation_Reason__c = null;
        testContractProduct.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(10);
        testContractProduct.Serial_Number_PCSN__c = 'test';
        insert testContractProduct;
        wrpProdService wps = null;
        List<wrpProdService> wpsList = new List<wrpProdService>();
        wrpProdService wd = new wrpProdService('acme', wpsList, objSlaDEtail, true, true, testServiceContract, testContractProduct, 'acme');
    }
}
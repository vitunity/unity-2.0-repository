@isTest(seealldata=true)
Public class Test_Batch_Model_Analytic_Member
{
    
    public static testmethod void TestModel()   
    {
        
        Account a = new Account(name='testacctest',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert a;      
        Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];        
        
        Contact con=new Contact(Lastname='rbtest2',FirstName='rbSingh2',Email='rb.test@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwerb', MailingState='teststaterb', Phone = '1244452222');
        insert con;
        
        User u = new User(alias = 'standt', email='standardtestuse92@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', 
        languagelocalekey='en_US', is_Model_Analytic_Member__c = true,
        localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
        username='standardtestuse92@testclass.com');
        insert u;
        
        
        Group egroup = new Group(name='Eclipse', developername='Eclipsetest123');
        insert egroup;
        
        Test.startTest();
            ScheduleBatch_Model_Analytic_Member sh1 = new ScheduleBatch_Model_Analytic_Member();
            String sch = '0 0 23 * * ?'; 
            system.schedule('TEST Model_Analytic_Member', sch, sh1); 
        Test.stopTest();
    }
    
}
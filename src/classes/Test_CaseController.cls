@isTest
Public class Test_CaseController
{
     static testmethod void CaseController() 
     {           
        
        Account a = new Account(name='test2',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',Country__c='Test',Legal_Name__c ='Testing'); 
        insert a;
        Case tcase = new case(Subject = 'Test Case',AccountId =a.id, Priority='Medium');
        insert tcase;
        ApexPages.currentPage().getParameters().PUT('RECVRYA','TESTREQANS'); 
        ApexPages.StandardController sc = new ApexPages.StandardController(tcase);      
        CaseController contr = new CaseController(sc);
        
        contr.getContactId();
        contr.getAccountId();
        contr.getLanguageLocaleKey();
    }

   
}
@isTest(SeeAllData = true)

public class Test_CoveredProductDeleteTrigger
{
   Static testMethod void CoveredProductDeleteTrigger()
   {
        test.startTest();
        
        Account acc = new Account();
        acc.Name = 'testERPAccount';
        acc.Account_Type__c= 'Customer';
        acc.AccountNumber = 'sitePartner';
        acc.Country__c = 'India';
        insert acc;
            
        SVMXC__Service_Contract__c servcContrct = new SVMXC__Service_Contract__c();
        servcContrct.Name = 'testContrct';
        servcContrct.SVMXC__End_Date__c = System.today()+7;  
        servcContrct.SVMXC__Start_Date__c = System.today();
        servcContrct.ERP_Sales_Org__c = 'my org';
        insert servcContrct;
        
        SVMXC__Service__c sa = new SVMXC__Service__c();
        sa.SVMXC__Service_Type__c = 'Preventive Maintenance Inspections';
        sa.Frequency__c = 18;
        insert sa;
                        
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.SVMXC__Available_Services__c = sa.id; 
        sld1.Service_Maintenance_Contract__c = servcContrct.id;
        insert sld1;
                   
        SVMXC__Service_Contract__c servcContrct2 = new SVMXC__Service_Contract__c();
        servcContrct2.Name = 'testContrct';
        servcContrct2.Quote_Reference__c = 'quote123';
        servcContrct2.SVMXC__End_Date__c = System.today()+7;  
        servcContrct2.SVMXC__Start_Date__c = System.today();
        servcContrct2.ERP_Contract_Type__c = 'ZH3';
        servcContrct2.SVMXC__Active__c = true;
        servcContrct2.ERP_Sales_Org__c = 'my org';
        servcContrct2.ERP_Sold_To__c = 'bdbdb';
        servcContrct2.SVMXC__Company__c = acc.id;
        servcContrct2.SVMXC__Renewed_From__c = servcContrct.id;
        servcContrct2.ERP_Order_Reason__c = 'Sales call';
        insert servcContrct2;
        
        SVMXC__SLA_Detail__c objDetails = new SVMXC__SLA_Detail__c();
        objDetails.SVMXC__Available_Services__c = sa.id;
        objDetails.Service_Maintenance_Contract__c = servcContrct2.id;
        insert objDetails;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'testSit';
        site.ERP_Functional_Location__c = 'funcLocation';
        insert site;
        
        Product2 pro = new Product2();
        pro.Name = 'testProd';
        pro.ProductCode = 'pro123';
        pro.SVMXC__Product_Line__c = 'Accessory';
        insert pro; 
               
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.Name = 'test1IP';
        ip1.SVMXC__Product__c = pro.id;
        ip1.ERP_Reference__c= 'sr123';
        insert ip1;   
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.Name = 'test1IP';
        ip2.SVMXC__Product__c = pro.id;
        ip2.ERP_Reference__c= 'sr1238';
        ip2.SVMXC__Top_Level__c = ip1.id;
        ip2.SVMXC__Parent__c =  ip1.id;
        insert ip2;

        SVMXC__PM_Plan_Template__c pmPlanTemp = new SVMXC__PM_Plan_Template__c();
        pmPlanTemp.Name = 'pnPlanTemp';
        insert pmPlanTemp;
              
        SVMXC__PM_Applicable_Product__c pmApplcablProd = new SVMXC__PM_Applicable_Product__c();
        pmApplcablProd.SVMXC__PM_Plan_Template__c = pmPlanTemp.id;
        pmApplcablProd.PM_Group__c  = 'testGroup';
        pmApplcablProd.SVMXC__Product_Line__c = 'Accessory';
        insert pmApplcablProd;
        
        ERP_Partner_Association__c ea = new ERP_Partner_Association__c();
        ea.ERP_Partner_Number__c = 'sitePartner';
        ea.Partner_Function__c = 'Z1=Site Partner';
        insert ea;   
        
        SVMXC__PM_Plan__c pmPlan = new SVMXC__PM_Plan__c();
        pmplan.name = 'testPlan';
        pmplan.SVMXC__Service_Contract__c = servcContrct.id;  
        pmplan.SVMXC__End_Date__c =  System.today();   
        pmplan.SVMXC__Status__c = 'Active';
        pmplan.Top_Level__c = ip1.id;
        pmplan.PM_Group__c = 'testGroup';
        insert pmPlan;  

        ERP_Org__c eorg = new ERP_Org__c();
        eorg.Sales_Org__c = 'my org';
        eorg.Days_Offset__c = 1234568878.2;
        insert eorg;
        
        SVMXC__PM_Coverage__c pm = new SVMXC__PM_Coverage__c();
        pm.SVMXC__Product_Name__c = ip2.id;
        pm.SVMXC__PM_Frequency__c = 10;
        pm.SVMXC__PM_Plan__c = pmPlan.id;
        insert pm;
        
        List <SVMXC__Service_Contract_Products__c> listvarCP = new List <SVMXC__Service_Contract_Products__c>();
        SVMXC__Service_Contract_Products__c varCP = new SVMXC__Service_Contract_Products__c ();
        varCP.SVMXC__Service_Contract__c = servcContrct2.id;
        varCP.ERP_Functional_Location__c = site.ERP_Functional_Location__c;
        varCP.SVMXC__Product__c = pro.id;
        varCP.Product_Code__c = 'pro123';
        varCP.Serial_Number_PCSN__c = ip2.SVMXC__Serial_Lot_Number__c;
        varCP.ERP_Site_Partner__c = ea.ERP_Partner_Number__c;
        varCP.SVMXC__Installed_Product__c = ip2.id;
        listvarCP.add(varCP);
        insert listvarCP;
    
        SVMXC__Installed_Product__c insobj = [select Id,SVMXC__Service_Contract_Line__c from SVMXC__Installed_Product__c where id=: ip1.id ];
        insobj.SVMXC__Service_Contract_Line__c = listvarCP[0].id;
        update insobj;
        
        delete listvarCP;
        
        test.stopTest();
         
    }

}
@isTest
public class Test_MVTPaasPackage{
    public static testmethod void MVTPaasTest(){

        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
         

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];
        Account act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert act; 
        
        Contact con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@2010RR.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
            MailingState='CA', Phone = '12452234',
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        insert con;
        
        
        TPaaS_Package_Detail__c oTPD = new TPaaS_Package_Detail__c();
        oTPD.Assigned_Dosiometrist__c = userinfo.getUserId();
        oTPD.PrescribedClinician__c = Con.Id;
        oTPD.Plan_Due_Date__c = system.today();
        insert oTPD;
        
        User varianUsr;
        varianUsr  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
        lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
        username='test_user@testclass.com', isActive = true, ContactId = con.Id, vMarket_User_Role__c='Reviewer');
        insert varianUsr;

        //Test.setMock(HttpCalloutMock.class, new TPaasCommentMock());
        string dt = datetime.now().format('MM/dd/yyyy');
        
        System.runAs(varianUsr) {
        MVTPassPackageController.getTPassList('undefined','undefined','test','test',1,10,1,dt ,dt,'undefined');
        MVTPassPackageController.getTPassDetails(string.valueOf(oTPD.id));
        MVTPassPackageController.saveComment(string.valueOf(oTPD.id),'Test Comment');
        //MVTPassPackageController.getTPassComments(string.valueOf(oTPD.id));
        
        MVTPassPackageController.getPriorities();
        MVTPassPackageController.getPlanStages();
        MVTPassPackageController.getDosiometristInfo();
        MVTPassPackageController.getDosiometrists();
        MVTPassPackageController.savePriority(string.valueOf(oTPD.id),oTPD.Priority__c,oTPD.Plan_Stage__c,oTPD.PlanState__c,oTPD.Assigned_Dosiometrist__c);
        MVTPassPackageController.savePlanStage(string.valueOf(oTPD.id),oTPD.Plan_Stage__c);
        
        MVTPassPackageController.SaveNewTPassDetail(JSON.serialize(MVTPassPackageController.getNewTPass(string.valueOf(oTPD.id))),oTPD.Priority__c,oTPD.OAR_Metric_Template__c,string.valueOf(act.Id));
        }
        
    }
    
    public static testmethod void TreatmentPlanningExcelTest(){
        string Document_TypeSearch = ApexPages.currentPage().getParameters().get('st');
        string Priority = ApexPages.currentPage().getParameters().get('Priority');
        string PlanStage = ApexPages.currentPage().getParameters().get('PlanStage');
        string frmDate = ApexPages.currentPage().getParameters().get('fdate');
        string toDate = ApexPages.currentPage().getParameters().get('tdate');
        string dt = datetime.now().format('MM/dd/yyyy');
        
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
         

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];
        Account act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert act; 
        
        Contact con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@2010RR.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
            MailingState='CA', Phone = '12452234',Dosiometrist__c=true,
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        insert con;
        
               
        TPaaS_Package_Detail__c oTPD = new TPaaS_Package_Detail__c();
        oTPD.Assigned_Dosiometrist__c = userinfo.getUserId();
        oTPD.PrescribedClinician__c = Con.Id;
        oTPD.Plan_Due_Date__c = system.today();
        insert oTPD;
        
        User varianUsr;
        varianUsr  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
        lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
        username='test_user@testclass.com', isActive = true, ContactId = con.Id, vMarket_User_Role__c='Reviewer');
        insert varianUsr;
        
        System.runAs(varianUsr) {
            PageReference pageRef = Page.TreatmentPlanningExcel;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('st', '');
            pageRef.getParameters().put('Priority', 'All');
            pageRef.getParameters().put('PlanStage', 'All');
            pageRef.getParameters().put('fdate', dt);
            pageRef.getParameters().put('tdate','');
            TreatmentPlanningExcel t = new TreatmentPlanningExcel();
            t.getTpassDetails();
        }
        
        
    }
}
/*
 * Author Rishabh
 * Moving business logic to handler class from trigger
 */
public class TimeEntryHandler{
    
    
    public static list<SVMXC_Time_Entry__c> splitEntries = new  list<SVMXC_Time_Entry__c>();
    
    
    // set indirect hourse org activity
    public static void setOrgActivity(List<SVMXC_Time_Entry__c> timeEntryList){
        set<id> orgActivityIdSet = new set<id> ();
        list<Organizational_Activities__c> orgActivityList = new list<Organizational_Activities__c>();
        map<Id,String> orgActivityMap = new map<Id,String>();
        for(SVMXC_Time_Entry__c t : timeEntryList){
            if(t.Organizational_Activity__c != null){
                orgActivityIdSet.add(t.Organizational_Activity__c);
            }
        }
        if(!orgActivityIdSet.isEmpty()){
            orgActivityList = [select Id, Name 
                        from Organizational_Activities__c
                        where Id IN:orgActivityIdSet];
            for(Organizational_Activities__c oa :orgActivityList){
                orgActivityMap.put(oa.Id,oa.Name);
            }
            for(SVMXC_Time_Entry__c t : timeEntryList){
                if(orgActivityMap.containsKey(t.Organizational_Activity__c) && t.Record_Type_Name__c == 'Indirect Hours'){
                    t.Activity__c = orgActivityMap.get(t.Organizational_Activity__c);
                }
            }
        }
    }
    
    //set start and end times
    public static void setTimesFormats(List<SVMXC_Time_Entry__c> timeEntryList, map<Id, SVMXC_Time_Entry__c> oldmap){
        //populate start and end time formatted.DE8250 - Based on technican time zone.
        set<id> technicianId = new set<id> ();
        for(SVMXC_Time_Entry__c t : timeEntryList){
            if(t.Technician__c != null
               && t.End_Date_Time__c != null
               && t.Start_Date_Time__c != null
               && (
               oldmap == null 
                || oldmap.get(t.Id).Technician__c != t.Technician__c
                || oldmap.get(t.Id).End_Date_Time__c != t.End_Date_Time__c
                || oldmap.get(t.Id).Start_Date_Time__c != t.Start_Date_Time__c)){
                technicianId.add(t.Technician__c);
            }
        }
        if(!technicianId.isEmpty()){
            map<id,SVMXC__Service_Group_Members__c> techMap = new map<id,SVMXC__Service_Group_Members__c>([select User__r.TimeZoneSidKey 
                                            from SVMXC__Service_Group_Members__c where id in :technicianId] );
            string minutes; 
            string hours;
            integer intHours;
            string pm = 'PM';
            string endDate;
            string startDate;
            for(SVMXC_Time_Entry__c entry : timeEntryList){
                if(entry.Technician__c != null && techMap.containsKey(entry.Technician__c)){
                    String timmezone = techMap.get(entry.Technician__c).User__r.TimeZoneSidKey;
                    system.debug('timmezone=='+timmezone);
                    if(timmezone!= null){
                        if(entry.End_Date_Time__c != null){
                            entry.End_Time__c = entry.End_Date_Time__c.format('HH:mm', timmezone);
                            entry.End_Date_New__c = entry.End_Date_Time__c.format('yyyy/MM/dd HH:mm:ss',timmezone).substring(0,11);
                        }   
                        if(entry.Start_Date_Time__c != null){
                            entry.Start_Time__c = entry.Start_Date_Time__c.format('HH:mm', timmezone);
                            entry.Start_Date_New__c = entry.Start_Date_Time__c.format('yyyy/MM/dd HH:mm:ss',timmezone).substring(0,11);
                        }
                        
                    }
                }
            }
        }
    }

    /*
     * Update public holiday Entry if direct get created
     */
    public static void updatePublicTimeEntry(List<SVMXC_Time_Entry__c> timeEntryList, map<Id, SVMXC_Time_Entry__c> oldmap){
        string directTMERecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC_Time_Entry__c').get('Direct_Hours'); 
        set<Id> timesheetIds = new set<Id> ();
        map<Id,SVMXC_Timesheet__c> timesheetMap;
        set<id> technicianIdsSet = new set<id> ();
        
        
        for(SVMXC_Time_Entry__c entryObj : timeEntryList){
            technicianIdsSet.add(entryObj.Technician__c);
            if(directTMERecordTypeId == entryObj.RecordTypeId
             && entryObj.Start_Date_Time__c != null
             && entryObj.End_Date_Time__c != null
             &&(oldmap == null || oldmap.get(entryObj.Id).Start_Date_Time__c != entryObj.Start_Date_Time__c
             || oldmap.get(entryObj.Id).End_Date_Time__c != entryObj.End_Date_Time__c)){
                 timesheetIds.add(entryObj.Timesheet__c);
             }
        }
        if(!timesheetIds.isEmpty()){
            
            
            
            list<SVMXC_Time_Entry__c> publicTimeEntriesToUpdate = new list<SVMXC_Time_Entry__c>();
            timesheetMap = new  map<Id,SVMXC_Timesheet__c>([select Id, Technician__c,
                                                            (select Id,Start_Date_Time__c, End_Date_Time__c from Time_Entries__r where Activity_Description__c = 'Public Holiday') 
                                                            from SVMXC_Timesheet__c
                                                            where Id in : timesheetIds]);
            
            
            
            for(SVMXC_Time_Entry__c entryObj : timeEntryList){
                system.debug('==In==');
                if(directTMERecordTypeId == entryObj.RecordTypeId
                 && entryObj.Start_Date_Time__c != null
                 && entryObj.End_Date_Time__c != null
                 &&(oldmap == null || oldmap.get(entryObj.Id).Start_Date_Time__c != entryObj.Start_Date_Time__c
                 || oldmap.get(entryObj.Id).End_Date_Time__c != entryObj.End_Date_Time__c)){
                     
                     if(timesheetMap.containskey(entryObj.Timesheet__c)){
                         for(SVMXC_Time_Entry__c publicEntryObj : timesheetMap.get(entryObj.Timesheet__c).Time_Entries__r){
                             if(entryObj.Start_Date_Time__c.date().isSameDay(publicEntryObj.Start_Date_Time__c.date())){
                                
                                long startTime = publicEntryObj.Start_Date_Time__c.getTime();
                                long endTime =  publicEntryObj.End_Date_Time__c.getTime();
                                long  milliSeconds = endTime - startTime;
                                system.debug('==milliSeconds=='+milliSeconds);
                                long seconds = milliSeconds/1000;
                                Integer minutes = Integer.valueOf(seconds/60);
                                system.debug('==minutes=='+minutes);
                                
                                publicEntryObj.Start_Date_Time__c = entryObj.End_Date_Time__c;
                                publicEntryObj.End_Date_Time__c = entryObj.End_Date_Time__c.addMinutes(minutes);
                                
                                
                                publicTimeEntriesToUpdate.add(publicEntryObj);
                             }
                         }
                         
                     }
                     
                 }
            } 
            
            if(!publicTimeEntriesToUpdate.isEmpty()){
                update publicTimeEntriesToUpdate;
            }
        }
    }
}
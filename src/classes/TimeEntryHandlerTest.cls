/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TimeEntryHandlerTest {
      public static Timecard_Profile__c profile;
      public static List<SVMXC_Time_Entry__c> lstTimeEntry;
      public static List<SVMXC__Service_Order_Line__c> detailList;
      public static Map<Id, SVMXC__Service_Order_Line__c> serviceMap;
      public static SVMXC_Time_Entry__c TE;
      public static SVMXC_Time_Entry__c TE1;
      public static SVMXC_Time_Entry__c TE2;
      public static SVMXC_Time_Entry__c TE3;
      public static BusinessHours bhs;
      public static SVMXC_Timesheet__c TS;
      public static SVMXC__Service_Order_Line__c wd;
      public static SVMXC__Service_Order_Line__c wd1;
      public static SVMXC__Service_Order_Line__c wd2;
      public static SVMXC__Service_Order_Line__c wd3;    
      public static SVMXC__Service_Order_Line__c wd4;
      public static SVMXC__Service_Order_Line__c wd5;
      public static SVMXC__Service_Order_Line__c wd6;
      public static SVMXC__Service_Order_Line__c wd7;   
      public static SVMXC__Service_Order_Line__c wd8;              
      public static SVMXC__Service_Group_Members__c techn;
      public static Date myDate1 = Date.Today();
      public static SVMXC__Service_Order__c wo;
      public static List<SVMXC_Time_Entry__c> tes = new List<SVMXC_Time_Entry__c>();  
      public static  Map<Id, SVMXC_Time_Entry__c> oldMapTE = new Map<Id, SVMXC_Time_Entry__c>();  
      public static  Map<Id, SVMXC_Time_Entry__c> newMapTE = new Map<Id, SVMXC_Time_Entry__c>();
      public static CountryDateFormat__c dateFormat; 
      Static {
        dateFormat = new CountryDateFormat__c(Name='Default',Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat; 
            User usr = CreateUser();
            profile = creteTimeCardProfile();
            List<BusinessHours> listBusinessHours = [Select id, MondayStartTime, MondayEndTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime, ThursdayStartTime, ThursdayEndTime, FridayStartTime, FridayEndTime, SaturdayStartTime, SaturdayEndTime, SundayStartTime, SundayEndTime from BusinessHours where MondayStartTime != null AND TuesdayStartTime != null AND WednesdayStartTime != null AND ThursdayStartTime != null AND FridayStartTime != null AND SaturdayStartTime != null AND SundayStartTime != null limit 1];
            if(listBusinessHours.size() > 0){ bhs = listBusinessHours[0];}
            techn = creteTechnician(usr);
            TS = creteTimeSheet(usr);
            wo = CreateWorkOrder(); 
      }
    
      private static void otherDataSetup(){
            List<SVMXC__Service_Order_Line__c> wds = new List<SVMXC__Service_Order_Line__c>();
            wd = CreateWorkDetail(wo, 'Usage/Consumption', 'Labor', myDate1, 2,3);
            wd.SVMXC__Group_Member__c = techn.id;
            wd.OJT__c = true;
            //wd.SVMXC__End_Date_and_Time__c = DateTime.newInstance(2015, 12, 12, 0, 0, 0); 
            //wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addHours(10);
            //wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addHours(2);
            wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addHours(2);
            wds.add(wd);
            wd1 = CreateWorkDetailOldMap(wo, 'Usage/Consumption', 'Labor', myDate1, 2,3);
            wd1.SVMXC__Group_Member__c = techn.id;
            wd1.OJT__c = false;
            //wd1.SVMXC__End_Date_and_Time__c = DateTime.newInstance(2015, 12, 13, 0, 0, 0); 
            //wd1.SVMXC__End_Date_and_Time__c = wd1.SVMXC__Start_Date_and_Time__c.addHours(10);
            wd1.SVMXC__Start_Date_and_Time__c = wd1.SVMXC__Start_Date_and_Time__c.addHours(3);
            wd1.SVMXC__End_Date_and_Time__c = wd1.SVMXC__Start_Date_and_Time__c.addHours(2);
            wds.add(wd1);
            wd2 = CreateWorkDetail(wo, 'Usage/Consumption', 'Labor', myDate1, 2,3);
            wd2.SVMXC__Group_Member__c = techn.id;
            wd2.SVMXC__Line_Type__c = 'Travel';
            //wd2.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(2015, 12, 14, 0, 0, 0);
            //wd2.SVMXC__End_Date_and_Time__c = wd2.SVMXC__Start_Date_and_Time__c.addHours(10);
            wd2.SVMXC__Start_Date_and_Time__c = wd2.SVMXC__Start_Date_and_Time__c.addHours(6);
            wd2.SVMXC__End_Date_and_Time__c = wd2.SVMXC__Start_Date_and_Time__c.addHours(2);
            wd2.OJT__c = false;
            wds.add(wd2);
            insert  wds;
            
            Organizational_Activities__c orgAct = creteOrgAct('Meal Time', 'Meal', True, False, False, False);
            Organizational_Activities__c orgAct1 = creteOrgAct('Travel to work', 'Indirect', True, False, False, True);
            TE = creteTimeEntry('Indirect hours', wd, orgAct, myDate1+4 ,1,2, techn);
            TE1 = creteTimeEntry('Indirect hours', wd1, orgAct1, myDate1+5 ,3,4, techn);
            TE.Start_Date_Time__c = TE.Start_Date_Time__c.addHours(6);
            TE.End_Date_Time__c = TE.End_Date_Time__c.addHours(6);
            tes.add(TE);
            TE1.Start_Date_Time__c = TE1.Start_Date_Time__c.addHours(8);
            TE1.End_Date_Time__c = TE1.End_Date_Time__c.addHours(8);
            TE1.Interface_Status__c = 'Processed';
            TE2 = creteRawTimeEntry('Direct Hours', wd1, orgAct, myDate1+6 ,1,2);
            TE2.Technician__c = techn.id;
            TE2.Create_Event__c = true;
            tes.add(TE2);
            TE3 = creteRawTimeEntry('Direct Hours', wd, orgAct1, myDate1+7 ,3,4);  
            TE3.Technician__c = techn.id; 
            TE3.Create_Event__c = true;
            TE3.Status__c = 'Submitted';
            TE3.Interface_Status__c = 'Process';
            tes.add(TE3);
            upsert tes;
            lstTimeEntry = new List<SVMXC_Time_Entry__c>();
            lstTimeEntry.add(TE2);
            lstTimeEntry.add(TE3);            
            oldMapTE.put(TE.id, TE1);
            
            newMapTE.put(TE.id, TE);
            detailList = new List<SVMXC__Service_Order_Line__c>();
            detailList.add(wd);
            serviceMap = new Map<Id, SVMXC__Service_Order_Line__c>();
            serviceMap.put(wd.id, wd1);
      }
        
      
    
     testmethod static void unitTest1() {
         Organizational_Activities__c newOrg = creteOrgAct('Travel to work', 'Indirect', True, False, False, True);
         System.Test.StartTest();
            List<SVMXC__Service_Order_Line__c> wds = new List<SVMXC__Service_Order_Line__c>();
         
            for(Integer i = 0; i<10; i++){
            
            dateTime dt = system.now().addDays(i);
            wd = CreateWorkDetail(wo, 'Usage/Consumption', 'Travel', myDate1, 2,3);
            wd.SVMXC__Group_Member__c = techn.id;
            wd.OJT__c = true;
            wd.SVMXC__Start_Date_and_Time__c = dt.addMinutes(20);
            wd.SVMXC__End_Date_and_Time__c = dt.addMinutes(30);
            wds.add(wd);
          
            wd2 = CreateWorkDetail(wo, 'Usage/Consumption', 'Labor', myDate1, 2,3);
            wd2.SVMXC__Group_Member__c = techn.id;
            wd2.SVMXC__Line_Type__c = 'Travel';
            wd2.SVMXC__Start_Date_and_Time__c = dt.addMinutes(60);
            wd2.SVMXC__End_Date_and_Time__c = dt.addMinutes(70);
            wd2.OJT__c = false;
            wds.add(wd2);
           
            }   
            insert  wds;
            System.Test.StopTest();
      }
        
     testmethod static void testsetTimesFormats()
      {
            System.Test.StartTest();
            otherDataSetup();
            TimeEntryHandler teHandler = new TimeEntryHandler();
            List<SVMXC_Time_Entry__c> tes2 = new List<SVMXC_Time_Entry__c>();
            tes2.add(TE2);
            Map<Id, SVMXC_Time_Entry__c> mapOldTimeEntry = new Map<Id, SVMXC_Time_Entry__c>();
            mapOldTimeEntry.put(TE2.id, TE3);
            TimeEntryHandler.setTimesFormats(tes2, mapOldTimeEntry);
            System.Test.StopTest();
      }
      
      public static User CreateUser() 
      {
        profile p = new profile();
        p = [Select id from profile where name = 'VMS System Admin'];
        User testUsr = [Select id from user where profileId= :p.id and isActive = true and id!=:userInfo.getUserId() limit 1];
        testUsr.FirstName = 'testAm';
        testUsr.LastName = 'UserAm';
        testUsr.Alias = 'tstUsrAM';
        testUsr.CommunityNickname= 'tUsr';
        testUsr.Email = 'tstUsraM@abc.com';
        testUsr.Username = 'tstUsraM@abc.com';
        testUsr.TimeZoneSidKey = 'America/Los_Angeles';
        testUsr.LocaleSidKey = 'en_US';
        testUsr.EmailEncodingKey = 'UTF-8';
        testUsr.ProfileId = p.Id;
        testUsr.LanguageLocaleKey  = 'en_US';
        return testUsr;
      }
      public static Timecard_Profile__c creteTimeCardProfile()
        {
            Timecard_Profile__c testTP = new Timecard_Profile__c();
            testTP.Name = 'Test Profile';
            DateTime LunchStdt = DateTime.newInstance(2014, 11, 3, 1, 0, 0); 
            DateTime NrmlStdt = DateTime.newInstance(2014, 11, 20, 5, 0, 0); 
            DateTime NrmlEddt = DateTime.newInstance(2014, 11, 20, 9, 0, 0); 
            testTP.Lunch_Time_Start__c = LunchStdt;
            testTP.Normal_Start_Time__c = NrmlStdt ;
            testTP.Normal_End_Time__c = NrmlEddt;
            testTP.Hide_Holiday_Cash_hours__c = True;
            testTP.Lunch_Time_duration__c = '60';
            testTP.X30_min_Travel_Time__c = True;
            testTP.Track_Meal_Time__c = True;
            testTP.Track_Dosimetric_Readings__c = True;
            testTP.Stopping_Time_Card_Completion__c = 'Cancelled';
            testTP.Fetch_Direct_Hours__c = True;
            testTP.No_of_weeks_in_advance_timecard_created__c = '15';
            testTP.No_of_weeks_in_arrears_timecard_created__c = '15';
            testTP.First_Day_of_Week__c = 'SATURDAY';
            testTP.Uneven_hours__c = True;
            testTP.Max_Hours_Day_1__c = '8.0';
            testTP.Max_Hours_Day_2__c = '8.0';
            testTP.Max_Hours_Day_3__c = '8.0';
            testTP.Max_Hours_Day_4__c = '8.0';
            testTP.Max_Hours_Day_5__c = '8.0';
            testTP.Max_Hours_Day_6__c = '8.0';
            testTP.Max_Hours_Day_7__c = '8.0';
            testTP.Lunch_Time__c  = true;
            insert testTP;
            return testTP;
        }
        
        public static SVMXC__Service_Group_Members__c creteTechnician(User usr)
        {
            techn = new SVMXC__Service_Group_Members__c();
            techn.User__c = usr.Id;
            techn.Name = usr.FirstName;
            techn.Timecard_Profile__c = profile.Id;
            // Create Service Team
            SVMXC__Service_Group__c grp = new SVMXC__Service_Group__c();
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtByName =  rtMapByName.get('Technician');
            grp.RecordTypeId = rtByName.getRecordTypeId();
            grp.SVMXC__Active__c = TRUE;
            grp.SVMXC__Group_Code__c = 'TestGroup';
            grp.Work_Order_Type__c = 'Professional Services';
            grp.Name = 'TestGroup';
            grp.SVMXC__Street__c = 'Test Street';
            Insert grp;
            techn.SVMXC__Service_Group__c = grp.Id;
            if(bhs.id != null)
            {
                  techn.Additional_Availability__c = bhs.id;
            }

            Insert techn;
            return techn;
            
        } 
        
         public static SVMXC_Timesheet__c creteTimeSheet(User usr)
        {
            SVMXC_Timesheet__c testTS = new SVMXC_Timesheet__c();
            testTS.Start_Date__c = Date.newInstance(2016, 1, 1);
            testTS.End_Date__c = Date.Today().ToStartOfWeek()+ 1;
            testTS.OwnerID = usr.Id;
            testTS.Technician__c = techn.Id;
            insert testTS;
            return testTS;
        }
        
        public static SVMXC__Service_Order__c CreateWorkOrder() 
        {
            Country__c varCntry = new Country__c();
            varCntry.Name = 'India';
            varCntry.Case_queues__c = 'Queue 1';
            insert varCntry;

            Id recSitePartnerAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
            Account varAcc = AccountTestdata.createAccount();
            varAcc.recordTypeId = recSitePartnerAccount;
            varAcc.ERP_Timezone__c = 'testERP Timezone';
            varAcc.Country1__c = varCntry.Id;
            varAcc.AccountNumber = 'ERP_Site_Partner_Code';
            insert varAcc;
            
            Id recCase_HD = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId();
            Profile p = [Select id from profile where name = 'System Administrator'];
            User systemuser = [Select id from user where profileId = :p.id and isActive = true and id != :userInfo.getUserId() AND UserRoleId != null limit 1]; 
            SVMXC__Service_Order__c wotemp = new SVMXC__Service_Order__c();

            Case cs = new case();
            cs.OwnerId = systemuser.Id;                                      //Owner Id
            cs.RecordTypeId = recCase_HD;                                    //Record Type
            cs.AccountiD = varAcc.id;                                        //Account
            //cs.ProductSystem__c = varIP_1.id;                                //IP
            cs.Status = 'Assigned';                                            //Status
            cs.Local_description__c  ='Local_description';                   //Local Description
            cs.Priority = 'Medium';                                          //Priority
            cs.Local_Subject__c = 'Out of Tolerance';                        //Local subject
            cs.ERP_Reference__c = 'HERP_Reference';                          //ERP Reference
            cs.Is_This_a_Complaint__c = 'Yes';                               //Complaint
            cs.Was_anyone_injured__c = 'Yes';                                //Complaint
            cs.ERP_Functional_Location__c = 'EFL';                           //ERP functional Location
            cs.ERP_Top_Level__c = 'H11111';                                  //ERP Top-Level
            cs.Reason = 'System down';                                       //Reason
            cs.Origin = 'Email';                                             //Origin
            //cs.Case_Preferred_Technician__c = varTech.Id;                    //Preferred Tech
            cs.SVMXC__Actual_Onsite_Response__c = system.now();
            cs.Initial_Schedule_Date_Time__c = system.now();
            cs.SVMXC__Scheduled_Date__c = system.today();
            cs.Local_Case_Activity__c  ='Local_Case_Activity';
            cs.Local_Internal_Notes__c   ='Local_Internal_Notes';
            cs.Downtime_start__c  = system.now();
            cs.Downtime_End__c = system.now() + 1;
            cs.ERP_Site_Partner_Code__c  = 'ERP_Site_Partner_Code';
            cs.ERP_Contact_Code__c = 'Contact_Code';
            cs.Malfunction_Start__c =  system.now();
            cs.Requested_Start_Date_Time__c = system.now() + 5 ; 
            cs.Requested_End_Date_Time__c = system.now() + 10 ; 
            cs.Customer_Malfunction_Start_Date_time__c = system.today();
            cs.Customer_Preferred_Start_Date_time__c = system.today();
            cs.SVMXC__Preferred_Start_Time__c = null;
            cs.Is_escalation_to_the_CLT_required__c = 'Yes from Work Order';
            cs.Primary_Contact_Number__c = 'Phone';
            Insert cs;

            
            String recordTypeName = 'Field Service';
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtInfo = rtMapByName.get(recordTypeName);
            id recordTypeId = rtInfo.getRecordTypeId();
            wotemp.RecordTypeId = recordTypeId;
            wotemp.SVMXC__Case__c=cs.id;
            wotemp.SVMXC__Order_Status__c = 'Open';
            wotemp.SVMXC__Priority__c = 'Medium'; //Updated from 'wo.SVMXC__Priority__c = '5 - PMI'' to wo.SVMXC__Priority__c = 'Medium' //Nikita DE3319 2/2/2015
            wotemp.SVMXC__Order_Type__c = 'Availability Services';
            wotemp.Service_Country__c = 'USA';
            insert wotemp;
            return wotemp;
        }
        
         public static SVMXC__Service_Order_Line__c CreateWorkDetail(SVMXC__Service_Order__c workOrder, String rtName, String strLineType, Date myDate, Integer intFromHour, Integer intToHour) 
        { // Usage/Consumption
            String recordTypeWDTravelName = 'Usage/Consumption';
            Id UsageConRecType = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get(recordTypeWDTravelName).getRecordTypeId();
            SVMXC__Service_Order_Line__c orderLine1 = new SVMXC__Service_Order_Line__c(SVMXC__Line_Status__c = 'Completed', Error_Message__c = 'Error Message test field', SVMXC__Service_Order__c = workOrder.Id );
            orderLine1.RECORDTYPEID =UsageConRecType;
            orderLine1.SVMXC__Line_Type__c = 'Travel';
            orderLine1.SVMXC__Group_Member__c = techn.id;
            orderLine1.OJT__c = false;
            orderLine1.SVMXC__Activity_Type__c = 'Project Management';
            orderLine1.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(2015, 12, 14, 9, 0, 0);
            return orderLine1;
        }

        public static SVMXC__Service_Order_Line__c CreateWorkDetailOldMap(SVMXC__Service_Order__c workOrder, String rtName, String strLineType, Date myDate, Integer intFromHour, Integer intToHour) 
        { // Usage/Consumption
            String recordTypeWDTravelName = 'Usage/Consumption';
            Id UsageConRecType = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get(recordTypeWDTravelName).getRecordTypeId();
            SVMXC__Service_Order_Line__c orderLine2 = new SVMXC__Service_Order_Line__c(SVMXC__Line_Status__c = 'Completed', Error_Message__c = 'Error Message test field', SVMXC__Service_Order__c = workOrder.Id );
            orderLine2.RECORDTYPEID =UsageConRecType;
            orderLine2.SVMXC__Line_Type__c = 'Labor';
            orderLine2.SVMXC__Group_Member__c = techn.id;
            orderLine2.OJT__c = true;
            orderLine2.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(2016, 1, 1, 9, 0, 0); 
            orderLine2.SVMXC__End_Date_and_Time__c   = DateTime.newInstance(2016, 1, 1, 10, 0, 0); 
            return orderLine2;
        }
        
        public static SVMXC__Service_Order_Line__c CreateWorkDetailStratAndEnd(SVMXC__Service_Order__c workOrder, String rtName, String strLineType, Date StartDate, Date EndDate, Integer intFromHour, Integer intToHour) 
        { // Usage/Consumption
            /*
            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
            wd.SVMXC__Service_Order__c = workOrder.Id;
            wd.SVMXC__Line_Type__c = strLineType;
            wd.SVMXC__Start_Date_and_Time__c = DateTime.newInstance(StartDate.Year(), StartDate.Month(), StartDate.Day(), intFromHour, 0, 0); 
            wd.SVMXC__End_Date_and_Time__c = DateTime.newInstance(EndDate.Year(), EndDate.Month(), EndDate.Day(), intToHour, 0, 0);
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtByName =  rtMapByName.get(rtName);
            wd.RecordTypeId = rtByName.getRecordTypeId();
            insert wd;
            return wd;
            */
            wd = new SVMXC__Service_Order_Line__c(SVMXC__Line_Status__c = 'Completed', Error_Message__c = 'Error Message test field', SVMXC__Service_Order__c = workOrder.Id );
            insert wd;
            return wd;
        }
        
         public static  Organizational_Activities__c creteOrgAct(String strName, String strType, Boolean isRecup, Boolean isHCH, Boolean isRTC, Boolean isUnpaid) 
        {
            Organizational_Activities__c oa = new Organizational_Activities__c();
            oa.Name = strName;
            oa.Holiday_Cash__c = isHCH;
            oa.Recuperative_Time__c = isRecup;
            oa.Round_the_clock__c = isRTC;
            oa.Type__c = strType;
            oa.Unpaid_Time__c = isUnpaid;
            oa.Indirect_Hours__c = profile.Id;
            insert oa;
            return oa;
        }
        
        public static SVMXC_Time_Entry__c creteTimeEntry(String RecType, SVMXC__Service_Order_Line__c workDetail, Organizational_Activities__c orgAct, Date myDate, Integer intFromHour, Integer intToHour, SVMXC__Service_Group_Members__c tech)
        {
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.SVMXC_Time_Entry__c.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtByName =  rtMapByName.get(RecType);
            SVMXC_Time_Entry__c testTE = new SVMXC_Time_Entry__c();
            testTE.Technician__c = tech.Id;
            testTE.Start_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(),intFromHour, 0, 0); 
            testTE.End_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(), intToHour, 0, 0);
            testTE.Timesheet__c = TS.Id;
            testTE.RecordTypeId = rtByName.getRecordTypeId();
            if(RecType == 'Indirect hours') 
            {
                testTE.Organizational_Activity__c = orgAct.Id; 
            } else if(RecType == 'Direct Hours') 
            {
                // Add Work Detail here
                testTE.Work_Details__c = workDetail.Id;
            }
            insert testTE;
            return testTE;
        }
        
         public static SVMXC_Time_Entry__c creteRawTimeEntry(String RecType, SVMXC__Service_Order_Line__c workDetail, Organizational_Activities__c orgAct, Date myDate, Integer intFromHour, Integer intToHour)
        {
            Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.SVMXC_Time_Entry__c.getRecordTypeInfosByName();
            Schema.RecordTypeInfo rtByName =  rtMapByName.get(RecType);
            SVMXC_Time_Entry__c testTE = new SVMXC_Time_Entry__c();
            testTE.Start_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(),intFromHour, 0, 0); 
            testTE.End_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(), intToHour, 0, 0);
            testTE.Timesheet__c = TS.Id;
            
            testTE.RecordTypeId = rtByName.getRecordTypeId();
            if(RecType == 'Indirect hours') 
            {
                testTE.Organizational_Activity__c = orgAct.Id; 
            } else if(RecType == 'Direct Hours') 
            {
                // Add Work Detail here
                testTE.Work_Details__c = workDetail.Id;
            }
            return testTE;
        }
}
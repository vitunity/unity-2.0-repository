/***************************************************************************
Author:
Created Date: 
Project/Story/Inc/Task : 
Description: 

Change Log
  Date/Modified By Name/Task or story or INC #/ Description of Change
  4 sept 2017-Puneet Mishra - STSK0011545 - Add Notes field to Direct & Indirect entries on TEM, Populating Comments field on TEM records
                                            Added Comments(comments__c ) in SOQL made in GetMatrixList method
*************************************************************************************/

public with sharing class Time_Entry_Matrix_ControllerUtils {
    
    private static Id prodservid = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
    
    /* Get work orders */
    public static map<id,SVMXC__Service_Order__c> GetWorkOrdes(list<Time_Entry_Matrix__c> tmList){
        set<Id> woIds = new set<id>();
        for(Time_Entry_Matrix__c tm: tmList){
            woIds.add(tm.Work_Order__c);
        }
        //CM : STSK0011624 : April Release : Added Site (SVMXC__Site__c) field in the Query.
         //14 Nov 2017 - Puneet Mishra - STSK0013197, added SVMXC__Company__r.Account_Status__c in query
        return new map<id,SVMXC__Service_Order__c>([SELECT id,SVMXC__Order_Status__c,Submited_Line_Count__c,SVMXC__Site__c,SVMXC__Site__r.Name,
              Name,SVMXC__Company__c,SVMXC__Company__r.Name,ERP_Service_Order__c,recordtype.name,SVMXC__Group_Member__c, SVMXC__Group_Member__r.User__c,
              SVMXC__Company__r.Account_Status__c, 
             (SELECT id,Work_Order_RecordType__c,NWA_Description__c,Billing_Type__c,SVMXC__Line_Status__c, 
             ERP_NWA__c,ERP_NWA__r.ERP_Status__c, ERP_NWA__r.Name 
             from SVMXC__Service_Order_Line__r 
             where recordtypeid =: prodservid limit 1),
             ownerid,ERP_WBS__c,ERP_WBS__r.Name 
         from SVMXC__Service_Order__c 
         where id = :woIds]);
    }
    
    /* Get GetMatrixList */
    public static list<Time_Entry_Matrix__c> GetMatrixList(Id userid, Date da){
            // 21 Nov 2016, Puneet, DFCT0011988, Added Week_Number__c field
            // 3 Sept 2017, Puneet, STSK0011545, Added comments__c  in SOQL
            list<Time_Entry_Matrix__c> l = 
             [Select id,Period_Start_Date__c,Day_1_Hours__c,Day_2_Hours__c,Day_3_Hours__c,
            Day_4_Hours__c,Day_5_Hours__c,Day_6_Hours__c,
            Day_7_Hours__c,Activity_Type__c,Sequence__c,
            Work_Order__c,Work_Order__r.Name,IsSWO__c,Complete__c,Status__c, 
            Organizational_Activity__c,recordtypeid, Week_Number__c,Day_1_Start_Date_Time__c, Day_2_Start_Date_Time__c, Day_3_Start_Date_Time__c,
            Day_4_Start_Date_Time__c,Day_5_Start_Date_Time__c,Day_6_Start_Date_Time__c,Day_7_Start_Date_Time__c, comments__c  
            from Time_Entry_Matrix__c 
            where ownerid=:userid 
            AND Period_Start_Date__c =:da  
            ORDER BY Sequence__c];
            system.debug(da + ' === l === ' + l);
            //system.assert(false);
            return l;
    }
}
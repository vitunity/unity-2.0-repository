/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.

  Change Log
  Date/Modified By Name/Task or story or INC #/ Description of Change
  11 sept 2017-Puneet Mishra - STSK0011545 - Add Notes field to Direct & Indirect entries on TEM, Populating Comments field on TEM records


 */
 
 
@isTest
private class Time_Entry_Matrix_ControllerUtils_Test {
    
    public static BusinessHours businesshr;
    public static Time_Entry_Matrix__c matrix;
    public static Time_Entry_Matrix__c matrix1;
    public static List<Time_Entry_Matrix__c> matrixList;
    public static Organizational_Activities__c OrgActivity1;
    public static Organizational_Activities__c OrgActivity2;
    public static List<Organizational_Activities__c> OrgActivityList;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c tech;
    public static Timecard_Profile__c TCprofile;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Site__c newLoc;
    
    
    static {
        /*** Creating CountryDateFormat__c Data
            Description: creating CountryDateFormat__c data as it was missing and throwing null point exception
        **/
        List<CountryDateFormat__c> dataList = new List<CountryDateFormat__c>();
        
        CountryDateFormat__c data = new CountryDateFormat__c();
        data.Name = 'Default';
        data.Date_Format__c = 'dd-MM-YYYY kk:mm';
        
        CountryDateFormat__c data1 = new CountryDateFormat__c();
        data1.Name = 'USA';
        data1.Date_Format__c = 'MM/dd/YYYY kk:mm';
        
        dataList.add(data);
        dataList.add(data1);
        
        insert data;
        
        
        
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        TCprofile = UnityDataTestClass.TIMECARD_Data(true);
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        tech = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        newAcc = UnityDataTestClass.AccountData(true);
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, tech);
        NWA = UnityDataTestClass.NWA_Data(true, newProd, WBS, erpProj);
        
        WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, tech);
        WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        insert WO;
        
        WD = UnityDataTestClass.WD_Data(false, WO, tech, newLoc, SOI, NWA);
        WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD.SVMXC__Line_Status__c = 'Submitted';
        insert WD;
        
        OrgActivity1 = UnityDataTestClass.OrgActivity_Data(false, TCprofile, 'Indirect', 'Training');
        OrgActivity2 = UnityDataTestClass.OrgActivity_Data(false, TCprofile, 'Direct', 'Training');
        OrgActivityList = new List<Organizational_Activities__c>();
        OrgActivityList.add(OrgActivity1);
        OrgActivityList.add(OrgActivity2);
        insert OrgActivityList;
                
        matrix = UnityDataTestClass.TIMEMATRIX_Data(false, 'Indirect', tech, 'Training', OrgActivity1);
        matrix.Status__c = 'Submitted';
        matrix1 = UnityDataTestClass.TIMEMATRIX_Data(false, 'Direct', tech, 'Training', OrgActivity2);
        matrixList = new List<Time_Entry_Matrix__c>();
        matrixList.add(matrix);
        matrixList.add(matrix1);
        insert matrixList;
        
         
    }
    static testMethod void myUnitTest() {
        
        Time_Entry_Matrix_ControllerUtils.GetWorkOrdes(matrixList);
        Time_Entry_Matrix_ControllerUtils.GetMatrixList(userInfo.getUserId(), system.today());
    }
}
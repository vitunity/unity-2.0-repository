/**
  * @author jwagner/Forefront
  * @version 1.0
  * @description
  *
  * PURPOSE
  * 
  *    Used to hold all helper methods needed by Timesheet triggers
  * 
  * TEST CLASS 
  * 
  *    Name of the test class
  *    For Naming use the class Timesheet_TriggerHandler_TEST
  *  
  * ENTRY POINTS 
  *  
  *    Called from Timesheet triggers
  * 
  * CHANGE LOG 
  * 
  *    [Version; Date; Author; Description]
  *    v1.0; Date;  jwagner/Forefront; Initial Build
  * 
  **/
//public with sharing class Timesheet_TriggerHandler {
public class Timesheet_TriggerHandler {
    public static Boolean IsRejectingTimesheet = false; //JW 7-8-2015 VAR-108 US5108, used to not call ServiceMaxTimesheetUtils.removeTimesheetEntriesFromWorkDetails() when deleting work details in this instance because time entries are locked
    public Static Boolean stopUtilMethod = false;// Duplicate Id error as ServiceMaxTimeSheetUtils method 'CreatNewTimesheetAsPerWithTimeEntries' inserting TimeSheet
    //JW 6-23-2015 VAR-57 US5169 TA6836 AND VAR-108 US5108
    //@params: list of rejected timesheets
    //function: When the timecard is rejected:
    //          (Training) Timesheet.Status = 'Incomplete' should be changed by the existing Salesforce Approval process , set the Time Entry's related to that Time Sheet(for that week) Status to Open, then set the U/C Work Detail's Line Status back from Submitted to Open, then set all of the WorkOrder's order status associated with these Work Detail's from 'Submitted or Reviewed' to 'Assigned'.
    //          Also if there are Time Entry Matrix's associated with the work details changed above, set the Status back to Open.
    //          (Project management) Delete Timesheet, Delete Time Entries, Delete UC Work Details, Update Work Order status to assigned, Update Time Entry Matrices statuses to open
    //@future (callout = false)
    public static void updateRejectedTSRelatedRecs(Set<Id> tsIdList, Set<Id> teNoUpdate)
    {
        Id ucRecordTypeId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Usage/Consumption').getRecordTypeId();
        System.debug(ucRecordTypeId);
        Id trainingRecordTypeId = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Training').getRecordTypeId();
        System.debug(trainingRecordTypeId);
        Id pmRecordTypeId = RecordTypeHelperClass.WORK_ORDER_RECORDTYPES.get('Project Management').getRecordTypeId();
        System.debug(pmRecordTypeId);
        Map<String, Schema.RecordTypeInfo> TIME_ENTRY_RECORDTYPES = Schema.SObjectType.Time_Entry_Matrix__c.getRecordTypeInfosByName();
        Id TE_RecordTypeId = TIME_ENTRY_RECORDTYPES.get('IND').getRecordTypeId();
        
        //DE7244, fetching Record Type Ids for Work Detail and Time Entry
        Id wdRecordTypeId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Usage/Consumption').getRecordTypeId();
        Id teRecordTypeId = RecordTypeHelperClass.TIME_ENTRY_RECORDTYPES.get('Direct Hours').getRecordTypeId();
        //DE7244, list of Time Entries to be deleted
        Map<Id, SVMXC_Time_Entry__c> timeEmtriesToDelete = new Map<Id, SVMXC_Time_Entry__c>();
        Set<Id> workDetailsId = new Set<Id>();        
        List<SVMXC__Service_Order_Line__c> workDetailsToBeDeleted = new List<SVMXC__Service_Order_Line__c>();

        Map<Id, Sobject> sobjectUpdateMap = new Map<Id, Sobject>();
        Map<Id, Sobject> sobjectToDeleteMap = new Map<Id, Sobject>();

        Set<Id> ucWDIdSet = new Set<Id>();

        System.debug('tsIdList: ' + tsIdList);
        //DE7722: StartDate , EndDate for Updating Indirect Entry Status to Open
        Date startDate;
        Date endDate;
        List<SVMXC_Timesheet__c> tsList = [Select Id, Status__c, Is_TEM__c, Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c, 
                                                  Start_Date__c,End_Date__c,
                                                  (Select Id, Status__c, Work_Details__c, Work_Details__r.SVMXC__Line_Status__c, 
                                                          Work_Details__r.RecordTypeId, Work_Details__r.SVMXC__Service_Order__c,
                                                          Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c, 
                                                          Work_Details__r.SVMXC__Service_Order__r.RecordTypeId,
                                                          RecordTypeId 
                                                  From Time_Entries__r) 
                                           From SVMXC_Timesheet__c Where Id IN: tsIdList];
        if(!tsList.isEmpty()) {
            startDate = tsList[0].Start_Date__c;
            startDate = Date.newInstance(startDate.year(),startDate.month(),startDate.day());
            endDate = tsList[0].End_Date__c;
            endDate = Date.newInstance(endDate.year(),endDate.month(),endDate.day());
        }
        //DE7244, to update Work Order associated with Direct Time Entries
        Set<Id> woId = new Set<Id>();
        List<SVMXC__Service_Order__c> woListToUpdate = new List<SVMXC__Service_Order__c>();
        
        System.debug('tsList: ' + tsList);

        if(!tsList.isEmpty())
        {
            for(SVMXC_Timesheet__c ts: tsList)
            {
                system.debug('pete>> ts.Time_Entries__r: ' + ts.Time_Entries__r);
                for(SVMXC_Time_Entry__c te: ts.Time_Entries__r)
                {
                    // DE7244, adding Work Ids, if record type is Project Management
                    // If record Type Id for Time Entry is 'Direct Hours', Work Detail = ' Product Serviced', Work Order = 'Project Management'
                    if(te.RecordTypeId == teRecordTypeId && te.Work_Details__r.RecordTypeId == wdRecordTypeId 
                        && te.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId == pmRecordTypeId && te.Work_Details__c != null){
                      woId.add(te.Work_Details__r.SVMXC__Service_Order__c);
                    }
                   
                    If(!sobjectUpdateMap.containsKey(te.Id) && !teNoUpdate.contains(te.Id)) //pszagdaj/FF, 1/22/2016, DE6812 - teNoUpdate expression added
                    { 
                        system.debug('pete>> 1');
                      sobjectUpdateMap.put(te.Id, ObjectHelperClass.updateObject(te, 'Status__c', 'Incomplete', false));
                      sobjectUpdateMap.put(te.Id, ObjectHelperClass.updateObject(te, 'Bypass_Validation__c', true, false));
                    }
                    system.debug(' ======== ' + te.Work_Details__c);//.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId);
                    
                    if(ts.Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c != null && ts.Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c.contains('Training')) //only training timesheets, JW/FF 7-8-15 VAR-57
                      {
                      system.debug('pete>> training');
                      System.debug(sobjectUpdateMap);
                      System.debug(te.Work_Details__c);
                      System.debug(te.Work_Details__r.RecordTypeId);
                      System.debug(ucRecordTypeId);
                      system.debug(te.Work_Details__r.SVMXC__Line_Status__c);
                      if(te.Work_Details__c != null && !sobjectUpdateMap.containsKey(te.Work_Details__c) && te.Work_Details__r.RecordTypeId == ucRecordTypeId && te.Work_Details__r.SVMXC__Line_Status__c == 'Submitted')
                      {
                          SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c(Id = te.Work_Details__c);
                          system.debug('pete>> 2');
                          sobjectUpdateMap.put(wd.Id, ObjectHelperClass.updateObject(wd, 'SVMXC__Line_Status__c', 'Open', false));
                          sobjectUpdateMap.put(wd.Id, ObjectHelperClass.updateObject(wd, 'Interface_Status__c', (String) null, false)); //DE7871
                          sobjectUpdateMap.put(wd.Id, ObjectHelperClass.updateObject(wd, 'ERP_Status__c', (String) null, false)); //DE7871
                          
                          ucWDIdSet.add(wd.Id);
                          System.debug(wd.Id);
                          if(!sobjectUpdateMap.containsKey(te.Work_Details__r.SVMXC__Service_Order__c) 
                            //&& te.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId == trainingRecordTypeId  //only training work orders, VAR-57
                            && (te.Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c == 'Submitted' || te.Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c == 'Reviewed'))
                          {
                              SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Id = te.Work_Details__r.SVMXC__Service_Order__c);
                              system.debug('pete>> 3');
                              sobjectUpdateMap.put(wo.Id, ObjectHelperClass.updateObject(wo, 'SVMXC__Order_Status__c', 'Assigned', false));
                              sobjectUpdateMap.put(wo.Id, ObjectHelperClass.updateObject(wo, 'Interface_Status__c', (String) null, false)); //DE7871
                              sobjectUpdateMap.put(wo.Id, ObjectHelperClass.updateObject(wo, 'ERP_Status__c', (String) null, false));//DE7871
                          }  
                      }
                    }
                    
                    
                    else if(ts.Is_TEM__c == true) //only project management timesheets
                    
                    { 
                      system.debug('te.Id=='+te.Id);
                        
                      
                      //DE7244, adding the time entries to be deleted associated with Time Sheet only if Record type is Project Management
                      //DE7244, On Rejection Work Detail should also get deleted
                      //if(te.Work_Details__r.SVMXC__Service_Order__r.RecordTypeId == pmRecordTypeId) {
                        timeEmtriesToDelete.put(te.Id, te);
                        workDetailsId.add(te.Work_Details__c);
                      //}

                      system.debug('==workDetailsId'+workDetailsId);

                      if(te.Work_Details__c != null && !sobjectToDeleteMap.containsKey(te.Work_Details__c) && te.Work_Details__r.RecordTypeId == ucRecordTypeId)
                      {
                          SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c(Id = te.Work_Details__c);
                          
                          ucWDIdSet.add(wd.Id);
                            
                      }
                    }
                    
                }
            }
            
            system.debug('pete>> ucWDIdSet-v');
            
            System.debug(ucWDIdSet);
            if(!ucWDIdSet.isEmpty())
            {
              List<Time_Entry_Matrix__c> teMatrixList = [Select Id From Time_Entry_Matrix__c
                                                        Where Day_1_Work_Detail__c IN: ucWDIdSet Or Day_2_Work_Detail__c IN: ucWDIdSet
                                                        Or Day_3_Work_Detail__c IN: ucWDIdSet Or Day_4_Work_Detail__c IN: ucWDIdSet
                                                        Or Day_5_Work_Detail__c IN: ucWDIdSet Or Day_6_Work_Detail__c IN: ucWDIdSet
                                                        Or Day_7_Work_Detail__c IN: ucWDIdSet];

                system.debug('pete>> teMatrixList: ' + teMatrixList);
              for(Time_Entry_Matrix__c teMatrix: teMatrixList)
              {
                  system.debug('pete>> 5');
                sobjectUpdateMap.put(teMatrix.Id, ObjectHelperClass.updateObject(teMatrix, 'Status__c', 'Open', false));
              }
            }
            //DE7722: Updating Indirect Entry Status to Open
            List<Time_Entry_Matrix__c> inDirectTEMatrixList = [ SELECT Id, Period_Start_Date__c, Period_End_Date__c, RecordTypeId 
                                                                FROM Time_Entry_Matrix__c 
                                                                WHERE Period_Start_Date__c =: startDate AND
                                                                      Period_End_Date__c =: endDate AND RecordTypeId =: TE_RecordTypeId];
            
            system.debug(' ==inDirectTEMatrixList== ' + inDirectTEMatrixList + startDate + endDate + TE_RecordTypeId);
            for(Time_Entry_Matrix__c teMatrix : inDirectTEMatrixList) {
                sobjectUpdateMap.put(teMatrix.Id, ObjectHelperClass.updateObject(teMatrix, 'Status__c', 'Open', false));
            }
            
            System.debug('sobjectUpdateMap: ' + sobjectUpdateMap);
            System.debug('sobjectToDeleteMap: ' + sobjectToDeleteMap);
            
            if(!sobjectUpdateMap.isEmpty())
            {
                try{
                  system.debug('pete>> sobjectUpdateMap: '+ sobjectUpdateMap);
                  List <sobject> lso = sobjectUpdateMap.values();
                  lso.sort(); //DE7433 //Cannot have more than 10 chunks in a single operation
                  system.debug('pete>> after update' + lso);
                  //system.assertEquals(new List <sobject>(), lso);
                  update lso; 
                  system.debug('sobjectToDeleteMap ' + sobjectToDeleteMap);
                  
                  if(!timeEmtriesToDelete.isEmpty()) {
                    delete timeEmtriesToDelete.values();
                  }

                  // DE7244, Updating Work Order Status to Submitted
                  system.debug(' ==== woId ==== ' + woId);
                  if(!woId.isEmpty()) {
                    for(SVMXC__Service_Order__c wo : [SELECT Id, SVMXC__Order_Status__c 
                                                      FROM SVMXC__Service_Order__c 
                                                      WHERE Id IN: woId]) {
                      wo.SVMXC__Order_Status__c = 'Assigned';
                      wo.Interface_Status__c = null; //DE7871
                      wo.ERP_Status__c = null; //DE7871
                      woListToUpdate.add(wo);
                    }
                    update woListToUpdate;
                  }

                  if(!workDetailsId.isEmpty()) {
                    for(SVMXC__Service_Order_Line__c workDetail : [SELECT Id, Name 
                                                                  FROM SVMXC__Service_Order_Line__c 
                                                                  WHERE Id IN: workDetailsId]) {
                      workDetailsToBeDeleted.add(workDetail);
                      system.debug('==workDetailsId'+workDetailsId);
                    }
                    if(!workDetailsToBeDeleted.isEmpty())
                      delete workDetailsToBeDeleted;
                  }

                  if(!sobjectToDeleteMap.isEmpty())
                  {
                    Timesheet_TriggerHandler.IsRejectingTimesheet = true;
                    system.debug('pete>> sobjectToDeleteMap: '+ sobjectToDeleteMap);
                    lso = sobjectToDeleteMap.values();
                    lso.sort(); //DE7433 //Cannot have more than 10 chunks in a single operation
                    delete lso;
                    system.debug('pete>> after delete');
                  }
                  
                  //SendEmailHelperClass.sendEmail('Process complete', 'Process complete.', new List<String>{UserInfo.getUserEmail()}, new List<String>(), new List<String>(), true);
                }
                catch(DmlException de)    
                {
                  System.debug('Error with the update' + de);
                  SendEmailHelperClass.sendEmail('Process failure', 'Process failed. Please inform your administrator\n\nStack trace: ' + de.getStackTraceString(), new List<String>{UserInfo.getUserEmail()}, new List<String>(), new List<String>(), true);
                }

                Timesheet_TriggerHandler.IsRejectingTimesheet = false;        
            }
        }
    }

    @TestVisible
    private static void deleteTimeSheet(Set<Id> deleteTSList) {
      if(deleteTSList.isEmpty())
        return;
      else {
        List<SVMXC_Timesheet__c> tsList = [SELECT Id, Is_TEM__c FROM SVMXC_Timesheet__c WHERE Id IN: deleteTSList];
        delete tsList;
      }
    }

    //@future(callout=false)
    public static void updateApprovedTSRelatedRecs(Set<Id> timesheets){
      List<SVMXC_Timesheet__c> timesheetlist = [Select Id, Is_TEM__c, Status__c,Technician__c, Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c, (Select Id, Status__c,  Work_Details__c, Work_Details__r.SVMXC__Line_Status__c, Work_Details__r.RecordTypeId, 
            Work_Details__r.SVMXC__Is_Billable__c, Work_Details__r.Related_Activity__c, Work_Details__r.RecordType.name From Time_Entries__r) 
            From SVMXC_Timesheet__c Where Id IN: timesheets];

      Set<Id> teIDs = new Set<Id>();
      List<SObject> updateObjects = new List<SObject>();
      Map<Id, Sobject> sobjectUpdateMap = new Map<Id, Sobject>();
      for(SVMXC_Timesheet__c ts: timesheetlist){
        for(SVMXC_Time_Entry__c te: ts.Time_Entries__r){  
          
          
          If(!sobjectUpdateMap.containsKey(te.id))
          {
            SVMXC_Time_Entry__c entry = new SVMXC_Time_Entry__c(id = te.id);
            sobjectUpdateMap.put(entry.Id, ObjectHelperClass.updateObject(entry, 'Status__c', 'Approved', false));
            sobjectUpdateMap.put(entry.Id, ObjectHelperClass.updateObject(entry, 'Bypass_Validation__c', True, false));
            sobjectUpdateMap.put(entry.Id, ObjectHelperClass.updateObject(entry, 'Interface_Status__c', 'Process', false)); //pszagdaj/FF, 1/26/2016, DE7125
          }
          
          
          SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c(id = te.Work_Details__c);
          
          if(ts.Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c != null && ts.Technician__r.SVMXC__Service_Group__r.SVMXC__Group_Type__c.contains('Training')){

            //DK 9/23/2015 Changed Condition to reflect that there each usage consumption line for training should always have a PS line which is what a Related Activity__c is representing.
            if ((!sobjectUpdateMap.containsKey(te.Work_Details__c)) && 
              te.Work_Details__r.recordtype.name == 'Usage/Consumption' && 
              te.Work_Details__r.SVMXC__Line_Status__c != 'Approved' && 
              te.Work_Details__r.SVMXC__Line_Status__c != 'Closed'){

             // sobjectUpdateMap.put(wd.Id, ObjectHelperClass.updateObject(wd, 'SVMXC__Line_Status__c', 'Approved', false)); //REFACTOR: This is already done in SR_Class_WorkOrder.cls
             // sobjectUpdateMap.put(wd.Id, ObjectHelperClass.updateObject(wd, 'Interface_Status__c', 'Process', false)); //REFACTOR: This is already done in SR_Class_WorkOrder.cls
            }
          }
        }

        System.debug(Logginglevel.Error,sobjectUpdateMap);
        //send email to current user with processing result
        if(!sobjectUpdateMap.isEmpty()) {
          try{
            update sobjectUpdateMap.values();
            //SendEmailHelperClass.sendEmail('Process complete', 'Process complete.', new List<String>{UserInfo.getUserEmail()}, new List<String>(), new List<String>(), true);
          }
          catch(DmlException de) {
            SendEmailHelperClass.sendEmail('Process failure', 'Process failed. Please inform your administrator\n\nStack trace: ' + de.getStackTraceString(), new List<String>{UserInfo.getUserEmail()}, new List<String>(), new List<String>(),true);
            SR_TimeCard_Controler.updateSuccess = 'false';
          }        
        }
      }
    }    
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Timesheet_TriggerHandler_Test {
  
  public static BusinessHours businesshr;
  public static SVMXC__Service_Group__c servTeam;
  public static SVMXC__Service_Group_Members__c technician;
  public static SVMXC_Timesheet__c TimeSheet;
  public static Timecard_Profile__c TCprofile;
  public static SVMXC_Time_Entry__c timeEntry;
  public static SVMXC_Time_Entry__c timeEntry2;
  public static List<SVMXC_Time_Entry__c> timeEntryList;
  public static Account newAcc;
  public static Contact newCont;
  public static ERP_Project__c erpProj;
  public static Case newCase;
  public static Product2 newProd;
  public static Sales_Order__c SO;
  public static Sales_Order_Item__c SOI;
  public static SVMXC__Service_Order__c WO;
  public static SVMXC__Service_Order_Line__c WD;
  public static SVMXC__Site__c newLoc;
  public static ERP_WBS__c WBS;
  public static ERP_NWA__c NWA;
  
  static {
    businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
    
    TCprofile = UnityDataTestClass.TIMECARD_Data(true);
    
    servTeam = UnityDataTestClass.serviceTeam_Data(true);
    
    technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
    technician.Timecard_Profile__c = TCprofile.Id;
    insert technician;
    
    newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
    
    businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
    newAcc = UnityDataTestClass.AccountData(true);
    erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
    newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
    newProd = UnityDataTestClass.product_Data(true);
    SO = UnityDataTestClass.SO_Data(true,newAcc);
    SOI = UnityDataTestClass.SOI_Data(true, SO);
    
    WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
    NWA = UnityDataTestClass.NWA_Data(true, newProd, WBS, erpProj);
    
    
  }
  
  public static void otherData(){
  
      WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
        WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        insert WO;
        
          WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
          
          WD.SVMXC__Consumed_From_Location__c = newLoc.id;
            WD.SVMXC__Activity_Type__c = 'Install';
            WD.Completed__c=false;
            WD.SVMXC__Group_Member__c = technician.id;
            WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
            WD.SVMXC__Work_Description__c = 'test';
            WD.SVMXC__Start_Date_and_Time__c =System.now();
            WD.Actual_Hours__c = 5;
            WD.SVMXC__Service_Order__c = WO.id;
            WD.Sales_Order_Item__c = SOI.id;
            WD.NWA_Description__c = 'nwatest';
          WD.SVMXC__Line_Status__c = 'Submitted';
          insert WD;
          
          
        TimeSheet = UnitYDataTestClass.TIMESHEET_Data(false, technician);
        TimeSheet.Is_TEM__c = true;
        insert TimeSheet;
        
        timeEntryList = new List<SVMXC_Time_Entry__c>();
        timeEntry = UnitYDataTestClass.TIMEENTRIES_DATA(false, false, TimeSheet, null);
        timeEntry2 = UnitYDataTestClass.TIMEENTRIES_DATA(false, true, TimeSheet, WD);
        //timeEntryList.add(timeEntry);
        timeEntryList.add(timeEntry2);
        insert timeEntryList;
  }
  
  
  // Service Team Group Type is Training
  static testMethod void updateRejectedTSRelatedRecs_TEAM_TYpe_Training_Test() {
    Test.StartTest();
    otherData();
    servTeam.SVMXC__Group_Type__c = 'Training';
    update servTeam;
    
    Timesheet_TriggerHandler.updateRejectedTSRelatedRecs(new Set<Id>{TimeSheet.Id}, new Set<Id>{timeEntry.Id});
    Test.StopTest();    
  }
  
  // Service Team Group Type is not Training
  static testMethod void updateRejectedTSRelatedRecs_Test() {
    Test.StartTest();
    otherData();
    Timesheet_TriggerHandler.updateRejectedTSRelatedRecs(new Set<Id>{TimeSheet.Id}, new Set<Id>{timeEntry.Id});
    Test.StopTest();    
  }
  
  // Service Team Group Type is Training
  static testMethod void updateApprovedTSRelatedRecs_TEAM_TYpe_Training_Test() {
    Test.StartTest();
    otherData();
    servTeam.SVMXC__Group_Type__c = 'Training';
    update servTeam;
    
    Timesheet_TriggerHandler.updateApprovedTSRelatedRecs(new Set<Id>{TimeSheet.Id});
    Test.StopTest();
  
  } 
  
  // Service Team Group Type is not Training
  static testMethod void updateApprovedTSRelatedRecs_Test() {
    Test.StartTest();
    otherData();
    Timesheet_TriggerHandler.updateApprovedTSRelatedRecs(new Set<Id>{TimeSheet.Id});
    Test.StopTest();
  } 
  
  static testMethod void deleteTimeSheet_EmptyId_Test() {
      Boolean isRejecting = Timesheet_TriggerHandler.IsRejectingTimesheet;
      Boolean stopUtil = Timesheet_triggerHandler.stopUtilMethod;
        Timesheet_TriggerHandler.deleteTimeSheet(new Set<Id>());
    }
    
    static testMethod void deleteTimeSheet_Test() {
      Test.StartTest();
      otherData();
      Boolean isRejecting = Timesheet_TriggerHandler.IsRejectingTimesheet;
      Boolean stopUtil = Timesheet_triggerHandler.stopUtilMethod;
      Timesheet_TriggerHandler.deleteTimeSheet(new Set<Id>{TimeSheet.Id});
      Test.StopTest();
    }
}
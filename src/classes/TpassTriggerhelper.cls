public class TpassTriggerhelper {
    public void AfterInsert(List<TPassPackage_Comment__c> NewTPassList,Map<Id, TPassPackage_Comment__c> TPassNewMap ){ 
        
        for(TPassPackage_Comment__c tp:NewTPassList){
            if(tp.Outbound__c==false){
                AddComments(tp.Body__c,tp.ChannelID__c,tp.UserName__c);
            }
        }
        
        for(TPassPackage_Comment__c tp:NewTPassList){}
        System.debug('alert Reached');    
    }
    
    @future(callout=true)
    public static void AddComments(String comments,String channel,String user){
        String authtkn=label.Tpaas_Bearer_token;
        String endpoint=label.Tpaas_getting_message;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(endpoint);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+authtkn);
        req.setHeader('MethodRequested', 'chat.postMessage');
        
        string strbody='{"channel":"'+channel+'","text":"'+comments+'","username":"'+user+'"}';
        req.setBody(strbody);
        req.setTimeout(10000);
        System.debug(req.getbody());
        String jasonstr1;
        try {
            if(!Test.isRunningTest()){
            res = http.send(req);
            }
            System.debug(req.getBody());
            jasonstr1 = res.getBody();
            System.debug(res.getBody());
            System.debug(jasonstr1+'jason');
            
        }
        catch(System.CalloutException e) {
            System.debug('Callout error: ' + e);
            System.debug(res.toString());
        }
        
        
    }
}
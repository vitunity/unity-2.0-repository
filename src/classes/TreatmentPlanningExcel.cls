public class TreatmentPlanningExcel{
     private static datetime getDateFromString(string dtstr){
        datetime dt;
        if(dtstr != null && dtstr != '' && dtstr != 'undefined'){
            String[] dateparse = dtstr.split(' ');
            String[] edDate = dateparse[0].split('/');
            dt = Datetime.newInstance(integer.valueOf(edDate[2]), integer.valueOf(edDate[0]), integer.valueOf(edDate[1]),integer.valueOf(edDate[3]),integer.valueOf(edDate[4]),integer.valueOf(edDate[5]));
        }
        return dt;
    }    
    public class WTPaas{
        public TPaaS_Package_Detail__c Tpass{get;private set;}
        public boolean isExpire{get;private set;}
        
        public string PlanRequestedDate{get;private set;}
        public string PlanCompletionDate{get;private set;}
        public string PlanDueDate{get;private set;}

        
        public WTPaas( TPaaS_Package_Detail__c p){
            this.Tpass = p;
            this.PlanRequestedDate = (p.Plan_Requested_Date__c<>null)?(p.Plan_Requested_Date__c).format('MMM d,yyyy kk:mm'):'';
            this.PlanCompletionDate = (p.Plan_Completion_Date__c<>null)?(p.Plan_Completion_Date__c).format('MMM d,yyyy kk:mm'):'';
            this.PlanDueDate = (p.Plan_Due_Date__c<>null)?(p.Plan_Due_Date__c).format('MMM d,yyyy kk:mm'):'';

            
            this.isExpire = false;
            if( Tpass.Plan_Due_Date__c <> null && Tpass.Plan_Completion_Date__c <> null && Tpass.Plan_Due_Date__c < Tpass.Plan_Completion_Date__c)  {
                isExpire = true;
            }       
        }
    }
    public list<WTPaas> getTpassDetails(){
        
        User cUser = [select id,AccountId,Dosiometrist__c from User where Id=: userinfo.getUserId()];
        
        string Document_TypeSearch = ApexPages.currentPage().getParameters().get('st');
        string Priority = ApexPages.currentPage().getParameters().get('Priority');
        string PlanStage = ApexPages.currentPage().getParameters().get('PlanStage');
        string frmDate = ApexPages.currentPage().getParameters().get('fdate');
        string toDate = ApexPages.currentPage().getParameters().get('tdate');
        string ViewMyOrders = ApexPages.currentPage().getParameters().get('ViewMyOrders');
        string TPaasStatusLink = ApexPages.currentPage().getParameters().get('TPaasStatusLink');
        string sortField ='PSequence__c';
        string sortDir ='desc';  
        
        datetime StartDate,EndDate;
        
        if(frmDate != null && frmDate != '' && frmDate != 'undefined'){
            StartDate = getDateFromString(frmDate+'/00/00/01');
        }
        if(toDate != null && toDate != '' && toDate != 'undefined'){
            EndDate = getDateFromString(toDate+'/23/59/59');
        }   

        set<string> setPlanStage = new set<string>();
        if(TPaasStatusLink == 'All'){
            setPlanStage.add('Requested');
            setPlanStage.add('Assigned');
            setPlanStage.add('In Progress');
            setPlanStage.add('Structures Ready for Review');
            setPlanStage.add('Plan Ready for Review');
            setPlanStage.add('Completed');
            setPlanStage.add('Replan Requested');
        }else if(TPaasStatusLink == 'Open'){
            setPlanStage.add('Requested');
            setPlanStage.add('Assigned');
            setPlanStage.add('In Progress');
            setPlanStage.add('Structures Ready for Review');
            setPlanStage.add('Plan Ready for Review');
        }else if(TPaasStatusLink == 'Closed'){
            setPlanStage.add('Completed');
            setPlanStage.add('Replan Requested');
        }

        system.debug(Document_TypeSearch+':LLL:'+Priority+':LLL:'+PlanStage);
        
        String Query = 'select Id,Name,of_business_days__c,parent__r.Plan_Identifier__c,Patient__c,Client_Name__c,Plan_Requested_Date__c ,Plan_Stage__c ,channel_id__c,PrescribedClinician__r.Name,Beam_Data__c,Site_Name__r.name,PrescribedClinician__c,PlanState__c,Machine_ID__c,Prescribing_Clinicians_Email__c,Plan_Due_Date__c,Priority__c,SiteName__c,Plan_Identifier__c,Status__c,Assigned_Dosiometrist__c,Assigned_Dosiometrist__r.name,Plan_Completion_Date__c from TPaaS_Package_Detail__c where id <> null  and Plan_Stage__c IN: setPlanStage ';
        string AccountId = cUser.AccountId;
        string cUserId = cUser.id;
        string wherecondition;
        if(!cUser.Dosiometrist__c){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' ( Site_Name__c =: AccountId or ownerId =: cUserId ) ';
        }
        else if(ViewMyOrders <> null && ViewMyOrders <> '' && ViewMyOrders <> 'undefined'){
            if(ViewMyOrders == 'true'){
                if(wherecondition==null)wherecondition = ' and ';
                if(AccountId == null)wherecondition += ' ( Assigned_Dosiometrist__c =: cUserId ) ';//Internal User
                if(AccountId <> null)wherecondition += ' ( Site_Name__c =: AccountId or ownerId =: cUserId ) ';//External User
            }
            
        }
        if(StartDate <> null && EndDate <> null){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Plan_Due_Date__c >=: StartDate and Plan_Due_Date__c <=: EndDate';
        }
        
        if(Document_TypeSearch  <> null && Document_TypeSearch  <> '' && Document_TypeSearch <> 'null' && Document_TypeSearch <> 'undefined' ){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Site_Name__r.name like \'%'+Document_TypeSearch+'%\'';
        }
        if(Priority <> '--Any--'){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Priority__c =: Priority ';
        }
        if(PlanStage <> '--Any--'){
            if(wherecondition==null)wherecondition = ' and ';else wherecondition += ' and ';
            wherecondition += ' Plan_Stage__c  =: PlanStage ';
        }
        
        
        
        if(wherecondition != null){Query+=wherecondition;}
        string orderby = ' order by ' + sortField + ' '+ sortDir;  
        
        system.debug(Query+':LLL:'+wherecondition+':LLL:'+Query+orderby );
        List<WTPaas> lstW = new List<WTPaas>();
        for(TPaaS_Package_Detail__c t : DataBase.Query(Query+orderby )){
            WTPaas w = new WTPaas(t);
            lstW.add(w);
        }
        return lstW;
        
        
    }
}
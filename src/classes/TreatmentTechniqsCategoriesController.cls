/*@RestResource(urlMapping='/TreatmentTechniqsCategories/*')
    global class TreatmentTechniqsCategoriesController 
    {
    @HttpGet
    global static List<RecordType> getCategory() 
    {
       List<RecordType> widgets1;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
      widgets1 = [SELECT Id,Name,Description  FROM RecordType WHERE SobjectType = 'VU_Treatment_Technique__c' AND (Description='English(en)' OR Description='')];
}
else
{
 widgets1 = [SELECT Id,Name,Description  FROM RecordType WHERE SobjectType = 'VU_Treatment_Technique__c' AND Description=:Language];
 }
    return widgets1;
    }
    }*/
    
    
    @RestResource(urlMapping='/TreatmentTechniqsCategories/*')
    global class TreatmentTechniqsCategoriesController 
    {
    @HttpGet
    global static List<RecordType> getCategory() 
    {
       List<RecordType> widgets1;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(ja)' && Language !='Portuguese(pt-BR)' ){
      widgets1 = [SELECT Id,Name,Description  FROM RecordType WHERE SobjectType = 'VU_Treatment_Technique__c' AND (Description='English(en)' OR Description='')];
}
else
{
 widgets1 = [SELECT Id,Name,Description  FROM RecordType WHERE SobjectType = 'VU_Treatment_Technique__c' AND Description=:Language];
 }
    return widgets1;
    }
    }
public virtual class TriggerDispatcher {
	private static Integer MAX_RECURSSION = 3;
	private static String TRIG_RUN_EXCEP = 'Dispatch run time exception occured';
	private static Map<String, TriggerConfig> triggerControl = new Map<String, TriggerConfig>();
		
	public static void execute(TriggerConfig newConfig) {
		if (validateRun(newConfig)) throw new TriggerDisptachException(TRIG_RUN_EXCEP); 
		TriggerDispatcherI handler = (TriggerDispatcherI) newConfig.classT.newInstance();
		if (trigger.isBefore && trigger.isInsert && newConfig.events.contains(TriggerContext.BEFORE_INSERT)) handler.beforeInsert(trigger.new);
		if (trigger.isAfter && trigger.isInsert && newConfig.events.contains(TriggerContext.AFTER_INSERT)) handler.afterInsert(trigger.new, trigger.newMap);
		if (trigger.isBefore && trigger.isUpdate && newConfig.events.contains(TriggerContext.BEFORE_UPDATE)) handler.beforeUpdate(trigger.old, trigger.oldMap, trigger.new, trigger.newMap);
		if (trigger.isAfter && trigger.isUpdate && newConfig.events.contains(TriggerContext.AFTER_UPDATE)) handler.afterUpdate(trigger.old, trigger.oldMap, trigger.new, trigger.newMap);
	}
	
	public static void disableTrigger(TriggerConfig ctrl) {
		triggerControl.put(ctrl.classT.getName(), ctrl);	
	}
	
	private static boolean validateRun(TriggerConfig config) {
		boolean disable = false;
		if(triggerControl.get(config.classT.getName()) != null && triggerControl.get(config.classT.getName()).disable) disable = true;
		if(Trigger.isExecuting && config.currentLoop >= TriggerDispatcher.MAX_RECURSSION) disable = true;
		config.currentLoop++;
		return disable;	
	}
	
	public virtual class TriggerDispatcherBaseImpl implements TriggerDispatcherI {
		public virtual void beforeInsert(List<sObject> newValues) { }
		
		public virtual void afterInsert(List<sObject> newValues, Map<Id, sObject> newValuesMap) { }
		
		public virtual void beforeUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, List<sObject> newValues, Map<Id, sObject> newValuesMap) { }
		
		public virtual void afterUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, List<sObject> newValues, Map<Id, sObject> newValuesMap) { }		
	}
	
	public class TriggerConfig {
		public System.Type classT;
		public Set<TriggerContext> events;
		public boolean disable;
		public boolean allowRecursion;
		public Integer currentLoop; 
		
		public TriggerConfig(System.Type t, Set<TriggerContext> evt, boolean disab, boolean recur) {
			this.classT = t;
			this.events = evt;
			this.currentLoop = 0;
			this.disable = disab;
			this.allowRecursion = recur;
		}
	}
	
  	public enum TriggerContext {
    	BEFORE_INSERT, BEFORE_UPDATE, BEFORE_DELETE,
    	AFTER_INSERT, AFTER_UPDATE, AFTER_DELETE,
    	AFTER_UNDELETE
  	}
  	
  	class TriggerDisptachException extends System.Exception { }
	    
}
public interface TriggerDispatcherI {
	void beforeInsert(List<sObject> newValues);
	void afterInsert(List<sObject> newValues, Map<Id, sObject> newValuesMap);
	void beforeUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, 
		List<sObject> newValues, Map<Id, sObject> newValuesMap);
	void afterUpdate(List<sObject> oldValues, Map<Id, sObject> oldValuesMap, 
		List<sObject> newValues, Map<Id, sObject> newValuesMap);
}
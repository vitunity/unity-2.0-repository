/**
****************************************************
* @Class :TripReportAttachmentController
* @ Wrapper Class(es) :ManagerUserWrapper
OtherUserWrapper
AttachmentWrapper
* @Author: Deepak Sharma
* @Desc : Coded to handle the logic for sending
*         Trip Reports to Managers and other users
* @Task : Validation needs to be moved to Client side
* @CreationDate :31-07-2017
*****************HISTORY*****************************
* 
*/
public class TripReportAttachmentController {
    
    @TestVisible private String workOrderId;
    @TestVisible private list<AttachmentWrapper> attachmentWrapperList;
    private list<Attachment> attachmentList;
    public SVMXC__Service_Order__c woObj {get;set;}
    @TestVisible private List<String> sendTo = new List<String>();
    private String prameters = '';
    public List<OtherUserWrapper> otherWrapperList{get;set;}
    public List<ManagerUserWrapper> manageWrapperList{get;set;}
    private String appManager='Application Manager';
    private String salesManager='Regional Sales Manager';
    private String serviceManager='Regional Service Manager';
    private String technician='Technician';
    private String emailInfoMsg='[Email Id does not exist for user ]';
    @TestVisible transient private Messaging.SingleEmailMessage mail;
    @TestVisible transient private List<Messaging.SingleEmailMessage> mails;
       
    //Constructor
    public TripReportAttachmentController()
    {
        try{
            woObj = new SVMXC__Service_Order__c();
            attachmentList = new list<Attachment> ();
            attachmentWrapperList = new list<AttachmentWrapper> ();
            workOrderId = ApexPages.currentPage().getParameters().get('woId');
            if(String.isBlank(workOrderId)){
                throw new TripReportException('Please pass work order id as parameter');
            }else{
                list<SVMXC__Service_Order__c> woList =[select Name,  Case_Number__c, SVMXC__Company__c, SVMXC__Case__r.CaseNumber, 
                                                       Contact_Email__c,  SVMXC__Group_Member__r.User__r.email, 
                                                       
                                                       SVMXC__Group_Member__r.Technician_Manager__c,SVMXC__Contact__r.Name,SVMXC__Company__r.Name,
                                                       District_Manager_1__c , SVMXC__Company__r.Regional_Sales_Manager__r.email,
                                                       SVMXC__Company__r.Regional_Service_Manager__r.email from SVMXC__Service_Order__c where Id =: workOrderId];
                if(woList.isEmpty()){
                    throw new TripReportException('Work order not found for id :'+workOrderId);
                }else{
                    woObj = woList[0];
                    initOtherUsers();
                    initManagerUsers();
                    }
               }
            
        }
        catch(Exception exp)
        {
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(errMsg);
        }
    }
    
/**@author: Deepak
* @Method desc: Defined to initiate Manager(Internal User)
* Three Managers -Application Manager
* District Sales Manager and District Service Manager
* are initialized
*/
    
 @TestVisible   private void initManagerUsers()
    {
        manageWrapperList=new List<ManagerUserWrapper>();
        String appEmail;
        String saleEmail;
        String servEmail;
        String techEmail;
        if(String.isNotBlank(woObj.SVMXC__Group_Member__r.Technician_Manager__c))
        {
            List<String> strList=woObj.SVMXC__Group_Member__r.Technician_Manager__c.split(' ');
            User appUser=[select Id ,Name, Email from user where firstname=: strList[0] and lastname=:strList[1] Limit 1];
            if(String.isNotBlank(appUser.Email))
            {
                appEmail=appUser.Email;
            }
            
        }
        if(String.isNotBlank(woObj.SVMXC__Company__r.Regional_Sales_Manager__r.email)) 
        {
            saleEmail=woObj.SVMXC__Company__r.Regional_Sales_Manager__r.email;
        }
        else
        {
            saleEmail=emailInfoMsg; 
        }
        if(String.isNotBlank(woObj.SVMXC__Company__r.Regional_Service_Manager__r.email))
           {
               servEmail=woObj.SVMXC__Company__r.Regional_Service_Manager__r.email ;
           }
        else
           {
               servEmail=emailInfoMsg;
           }
           
           if(String.isNotBlank(woObj.SVMXC__Group_Member__r.User__r.email))
           {
               techEmail=woObj.SVMXC__Group_Member__r.User__r.email;
           }
           else
           {
               techEmail=emailInfoMsg;
           }
           ManagerUserWrapper user1= new ManagerUserWrapper(appManager,appEmail,false);
           ManagerUserWrapper user2= new ManagerUserWrapper(salesManager,saleEmail,false);
           ManagerUserWrapper user3= new ManagerUserWrapper(serviceManager,servEmail,false);
           ManagerUserWrapper user4= new ManagerUserWrapper(technician,techEmail,false);
           manageWrapperList.add(user1);
           manageWrapperList.add(user2);
           manageWrapperList.add(user3);
           manageWrapperList.add(user4);
           }
           
           
 /**@author: Deepak
* @Method desc: Defined to initiate Other User(Internal User)
* Three Users are initialized
*/
           private void initOtherUsers()
           {
               otherWrapperList=new List<OtherUserWrapper>();
               for(Integer i=0; i<3; i++)
               {
                   OtherUserWrapper user1= new OtherUserWrapper(new SVMXC__Service_Order__c(),false);  
                   otherWrapperList.add(user1);
              }
     
           }
/**@author: Amit
* @Referenced From -WorkOrderAttachmentCOntroller
* @Method Desc: Defined to initialize Attachments related to the given work order
* @return :list<AttachmentWrapper>
* 
*/
       
           public list<AttachmentWrapper> getAttachmentWrapperList(){
               for(Attachment attObj : [select Id, Name,LastModifiedBy.Name, LastModifiedDate from Attachment where parentId =: workOrderId]){
                   AttachmentWrapper awObj = new AttachmentWrapper();
                   awObj.attachmentObj = attObj;
                   attachmentWrapperList.add(awObj);
               }
               if(attachmentWrapperList.isEmpty()){
                   ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.Error,'No Attachments are Currently Available on Work Order.  Please add prior to use of Email functionality');
                   ApexPages.addMessage(errMsg);
               }
               return attachmentWrapperList;
           }
           
           
/**@author: Deepak
@Method Desc: Action Controller method on Email Report Button
Encapsulates logic to send reports to intended recepients
@return : PageReference -returns to landing page
* 
*/
           public PageReference singleAttachment()
           {
               try{
                   sendTo = new List<String>();
                   serverValidation();
                   prameters = '';
                   mail = new Messaging.SingleEmailMessage();
                   mails = new List<Messaging.SingleEmailMessage>();
                   emailParametersSetup();
                   if(mails.isEmpty())
                   {
                       throw new TripReportException('No Email content is created.Check the parameters value');
                   }
                   else
                   {
                       Messaging.sendEmail(mails);
                       ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Info,'Mail Sent successfully to ['+ sendTo + ']  !!!');
                       ApexPages.addMessage(myMsg1);
                       return null;
                   }
               }catch(Exception e){
                   ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                   ApexPages.addMessage(myMsg1);
                   return null;
               }
               return new PageReference('/'+workOrderId);
               
           }
           
           
           /**@author: Deepak
@Method Desc: Action Controller method on Back Button
@return : PageReference -returns to launching page
* 
*/
           public PageReference back(){
               
               return new PageReference('/'+workOrderId);
           }
           
           /**@author: Deepak
@Method Desc: Does the input validations.Need to be moved
to VF Page. Pending Work Item
@return : PageReference -returns to launching page
* 
*/  
           @TestVisible private void serverValidation(){
               Boolean noManagerSel=false;
               Integer countMng=0;
               Integer countOth=0;
               for(AttachmentWrapper awt : attachmentWrapperList){
                   if(awt.selectObj){
                       prameters = prameters+awt.attachmentObj.Id + '|';
                   }
               }
               if(String.isBlank(prameters)){
                   throw new TripReportException('Please select attachments');
               }
               
               for(ManagerUserWrapper mW: manageWrapperList )
               {
                   if(mW.isSelected)
                   {
                       countMng++;
                       if(String.isNotBlank(mW.email) && !(mW.email.equals(emailInfoMsg )))
                       {
                           sendTo.add(mW.email);
                           
                       }
                   }
               }
               
               for(OtherUserWrapper oW:otherWrapperList )
               {
                   if(oW.isSelected)
                   {
                       countOth++;
                      if(oW.workOrder.District_Manager_1__c !=null)
                      {
                       User tempUser= [select id,email from user where id=:oW.workOrder.District_Manager_1__c Limit 1];
                      if(String.isNotEmpty(tempUser.Email)){
                      sendTo.add(tempUser.Email);
                      }
                       
                       
                   }
               }
               if(countMng==0 && countOth==0)
               {
                   throw new TripReportException('Please select atleast one [Manager] or [Other User]');            
               }
               if(sendTo.isEmpty())
               {
                   throw new TripReportException('The Mail Id does not exist in Database for selected User'); 
               }
               
           }
           }           
    /**@author: Deepak
@Method Desc: Helper method of singleAttachment
Set the email input values
@return : void
* 
*/
           @TestVisible private void emailParametersSetup()
           {
               List<Messaging.Emailfileattachment> efaList;
               Integer selectCount=0;
               try{
                   if(sendTo==null || sendTo.isEmpty())
                   {
                       throw new TripReportException('No email Addresses found.SendTo List is empty.'); 
                   }
                   else
                   {
                       mail.setToAddresses(sendTo);
                       mail.setSenderDisplayName(UserInfo.getUserName());
                       mail.setSubject('Trip Report' );
                       mail.setHtmlBody('Please find Trip Report Attached');
                       efaList = new List<Messaging.Emailfileattachment>();
                       for(AttachmentWrapper awt : attachmentWrapperList){
                           
                           if(awt.selectObj){ 
                               list<Attachment> aaObjList = [select Name, body from Attachment where id =:awt.attachmentObj.Id limit 1];
                               if(!aaObjList.isEmpty()){
                                   Messaging.Emailfileattachment eAttachment = new Messaging.Emailfileattachment();
                                   eAttachment.setFileName( aaObjList[0].name);
                                   eAttachment.setBody(aaObjList[0].body);
                                   efaList.add(eAttachment);
                                   selectCount++;
                               }
                               
                           }
                           
                       }
                   }
                   if(selectCount==0)
                   {
                       throw new TripReportException('Attachment not selected in wrapper list or not available in database'); 
                   }
                   mail.setFileAttachments(efaList );
                   mails.add(mail);
               }
               catch(Exception exp)
               {
                   throw new TripReportException('Email Message is not set as expected.'); 
               }
               
           }       
           
           
           
           /******************Wrapper Classes Section ********************/ 
           
           
           //Wrapper Class for Attachment Selection
           public class AttachmentWrapper{
               public Attachment attachmentObj {get;set;} 
               public boolean selectObj {get;set;}
           }
           //Wrapper Class for Manager User
           public class ManagerUserWrapper{
               public Boolean isSelected{get;set;}
               public String name{get;set;}
               public String email{get;set;}
               public ManagerUserWrapper(String nameTemp, String emailT,Boolean flag)
               {
                   isSelected=flag;
                   name=nameTemp;
                   email=emailT;
                   
               }
           }
           
           
           //Wrapper Class for other users
           public class OtherUserWrapper{
               public Boolean isSelected{get;set;}
               public SVMXC__Service_Order__c workOrder{get;set;}
               public OtherUserWrapper(SVMXC__Service_Order__c temp,Boolean flag )
               {
                   isSelected=flag;
                   workOrder=temp;
               }
               
           }
           
           //Exception class extends Exception
           public class TripReportException extends Exception{}   
           
           }
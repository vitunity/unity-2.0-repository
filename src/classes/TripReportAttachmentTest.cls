@isTest
public class TripReportAttachmentTest {
    
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent1 = new SVMXC__Installed_Product__c();
    public static Product2 objProd;
    public static List<TripReportAttachmentController.OtherUserWrapper> othListE=new List<TripReportAttachmentController.OtherUserWrapper>();
    public static List<TripReportAttachmentController.OtherUserWrapper> othListNE=new List<TripReportAttachmentController.OtherUserWrapper>();
    public static List<TripReportAttachmentController.OtherUserWrapper> othListNET=new List<TripReportAttachmentController.OtherUserWrapper>();
    
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListE=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListNE=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListNET=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<Attachment> listAttach=new List<Attachment>();
    
    Static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        
        //Using Existing Profile
        Profile profile = [Select id from Profile where name = 'VMS Service - User'];
        
        
        //User -District Sales Manager
        User salesManager = new user();
        salesManager.alias = 'SalesMng';
        salesManager.email='salesMng@testorg.com';
        salesManager.emailencodingkey='UTF-8';
        salesManager.lastname='Testing';
        salesManager.languagelocalekey='en_US';
        salesManager.localesidkey='en_US'; 
        salesManager.profileid = profile.Id;
        salesManager.timezonesidkey='America/Los_Angeles';
        salesManager.username='deepaksharma7862@testclass.com';
        // insert salesManager;
        
        //User -District Service Manager
        
        User serviceManager = new user();
        serviceManager.alias = 'ServMng';
        serviceManager.email='servMng@testorg.com';
        serviceManager.emailencodingkey='UTF-8';
        serviceManager.lastname='Testing';
        serviceManager.languagelocalekey='en_US';
        serviceManager.localesidkey='en_US'; 
        serviceManager.profileid = profile.Id;
        serviceManager.timezonesidkey='America/Los_Angeles';
        serviceManager.username='deepaksharma786@testclass.com';
        //insert serviceManager;
        
        
        //newAcc = UnityDataTestClass.AccountData(true);
        //
        Account newAcc = new Account();
        newAcc.Name = 'My Varian Test Account';
        newAcc.BillingCountry='USA';
        newAcc.Country__c= 'USA';
        newAcc.BillingCity= 'Los Angeles';
        newAcc.BillingStreet ='3333 W 2nd St';
        newAcc.BillingState ='CA';
        newAcc.BillingPostalCode = '90020';
        newAcc.Agreement_Type__c = 'Master Pricing';
        newAcc.OMNI_Address1__c ='3333 W 2nd St';
        newAcc.OMNI_City__c ='Los Angeles';
        newAcc.OMNI_State__c ='CA';
        newAcc.OMNI_Country__c = 'USA';  
        newAcc.OMNI_Postal_Code__c = '90020';
        newAcc.District_Sales_Manager__c=salesManager.id;
        newAcc.District_Service_Manager__c=serviceManager.id;
        insert newAcc;
        
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
        
        List<ERP_NWA__c> NWALIst = new List<ERP_NWA__c>();
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        nwa.ERP_Status__c = 'CLSD';
        insert NWA;
        
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        cdf.Name ='Default';
        insert cdf;
        
        WO = new SVMXC__Service_Order__c();
        //WO.SVMXC__Case__c = newCase.id;
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Open';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.SVMXC__Group_Member__c=technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO.SVMXC__Product__c = newProd.ID;
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Subject__c= 'bbb';
        WO.Sales_Order_Item__c = SOI.id;
        insert WO;
        
        //Insert Attachment
        
        Attachment objAtt = new Attachment();
        objAtt.Name = 'Test';
        objAtt.body = Blob.valueof('string');
        objAtt.ParentId = WO.Id;
        insert objAtt;
        
        
        listAttach.add(objAtt);
        
        
        //Other User Equal List
        othListE=new List<TripReportAttachmentController.OtherUserWrapper>();
        for(Integer i=0; i<3; i++)
        {
            TripReportAttachmentController.OtherUserWrapper user1= new TripReportAttachmentController.OtherUserWrapper(new SVMXC__Service_Order__c(),false);  
            othListE.add(user1);
        }
        //Other User Unequal List --Size
        for(Integer i=0; i<2; i++)
        {
            TripReportAttachmentController.OtherUserWrapper user1= new TripReportAttachmentController.OtherUserWrapper(new SVMXC__Service_Order__c(),false);  
            othListNE.add(user1);
        }
        
        //Other User Unequal List --type
        for(Integer i=0; i<3; i++)
        {
            TripReportAttachmentController.OtherUserWrapper user1= new TripReportAttachmentController.OtherUserWrapper(new SVMXC__Service_Order__c(),true);  
            othListNET.add(user1);
        }
        
        //Managers Equal List
        TripReportAttachmentController.ManagerUserWrapper user1= new TripReportAttachmentController.ManagerUserWrapper('Application Manager','dummymail.1@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user2= new TripReportAttachmentController.ManagerUserWrapper('Regional Sales Manager','dummymail.2@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user3= new TripReportAttachmentController.ManagerUserWrapper('Regional Service Manager','dummymail.3@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user4= new TripReportAttachmentController.ManagerUserWrapper('Technician','dummymail.4@test.com',false);
        mngListE.add(user1);
        mngListE.add(user2);
        mngListE.add(user3);
        mngListE.add(user4);
        
        //Managers UnEqual List --Size
        TripReportAttachmentController.ManagerUserWrapper user5= new TripReportAttachmentController.ManagerUserWrapper('Application Manager','dummymail.1@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user6= new TripReportAttachmentController.ManagerUserWrapper('Regional Sales Manager','dummymail.2@test.com',false);
        mngListNE.add(user5);
        mngListNE.add(user6);
        
        //Manager Unequal List-Type
        TripReportAttachmentController.ManagerUserWrapper user7= new TripReportAttachmentController.ManagerUserWrapper('AM','dummymail.1@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user8= new TripReportAttachmentController.ManagerUserWrapper('RSM','dummymail.2@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user9= new TripReportAttachmentController.ManagerUserWrapper('RSM2','dummymail.3@test.com',false);
        TripReportAttachmentController.ManagerUserWrapper user10= new TripReportAttachmentController.ManagerUserWrapper('T1','dummymail.4@test.com',false);
        mngListNET.add(user7);
        mngListNET.add(user8);
        mngListNET.add(user9);
        mngListNET.add(user10);
        
        
        
        
    }
    
    
    @isTest public static  void withPageReference(){
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testController=new TripReportAttachmentController();
        System.Test.StopTest();
    }
    
    @isTest public static  void withoutPageReference(){
        System.Test.StartTest();
        try{
            TripReportAttachmentController testController=new TripReportAttachmentController();
        }
        catch(Exception e)
            
        {
            System.assertEquals('Please pass work order id as parameter', e.getMessage());
        }
        
        System.Test.StopTest();
    }
    
    
    @isTest public static  void withInvalidWO(){
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId','111111111111111111');
        try{
            TripReportAttachmentController testController=new TripReportAttachmentController();
        }
        catch(Exception e)
        {
            System.assertEquals(true, e.getMessage().Contains('Work order not found for id :'+'111111111111111111'));
        }
        
        System.Test.StopTest();
    }
    
    @isTest public static void othrListEqual()
    {
        
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        System.assertEquals(othListE.size(), testClass.otherWrapperList.size());
        for(integer i=0; i<othListE.size(); i++)
        {
            System.assertEquals(othListE[i].workOrder.getSObjectType(), testClass.otherWrapperList[i].workOrder.getSObjectType());
            System.assertEquals(othListE[i].isSelected, testClass.otherWrapperList[i].isSelected);
        }
        System.Test.StopTest();
    }
    
    @isTest public static void mngListEqual()
    {
        
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        System.assertEquals(mngListE.size(), testClass.manageWrapperList.size());
        for(integer i=0; i<mngListE.size(); i++)
        {
            System.assertEquals(mngListE[i].name, testClass.manageWrapperList[i].name);
            System.assertEquals(mngListE[i].isSelected, testClass.manageWrapperList[i].isSelected);
        }
        System.Test.StopTest();
    }
    
    
    
    
    @isTest public static void mngrListNotEqualSize()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        System.assertNotEquals(mngListNE.size(), testClass.manageWrapperList.size());
        System.Test.StopTest();
        
    }
    
    @isTest public static void mngrListNotEqualType()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(integer i=0; i<mngListNET.size(); i++)
        {
            System.assertNotEquals(mngListNET[i].name, testClass.manageWrapperList[i].name);
            
        } 
        System.Test.StopTest();
    } 
    
    @isTest public static void testAttachment()
    {
        
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        System.assertEquals(listAttach.size(), testClass.getAttachmentWrapperList().size());
        System.Test.StopTest();
        
        
    }
    
    
    @isTest public static void testValidation()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        try
        {testClass.serverValidation();}
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'));
        }
        
        
        
        System.Test.StopTest();   
    }
    
    
    
    @isTest public static void testNoAttachmentFound()    
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        testClass.workOrderId='invalid23456789012';
        try{
            
            List<TripReportAttachmentController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('No Attachments are Currently Available on Work Order.  Please add prior to use of Email functionality'))  ;   
        }
        
        System.Test.StopTest();
        
        
    }  
    
    
    @isTest public static void attachmentSelected()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        try{
            testClass.serverValidation();
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select atleast one [Manager] or [Other User]'))  ;       
        }
        System.Test.StopTest();
        
        
    }
    
    @isTest public static void mangerSelected()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        for(TripReportAttachmentController.ManagerUserWrapper mng1:testClass.manageWrapperList)
        {
            mng1.isSelected=true;
            testClass.sendTo.add(mng1.email);
        }
        testClass.serverValidation();
        System.Test.StopTest();
        
    }
    
    
    @isTest public static void othrSelected()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        for(TripReportAttachmentController.OtherUserWrapper oth1 :testClass.otherWrapperList)
        {
            oth1.isSelected=true;
            testClass.sendTo.add('testmail'+1+'@test.com');
        }
        testClass.serverValidation();
        System.Test.StopTest();
        
        
    }
    
    @isTest public static void sendToisBlank()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        
        for(TripReportAttachmentController.ManagerUserWrapper mng1:testClass.manageWrapperList)
        {
            mng1.isSelected=true;
            mng1.email=null;
        }
        
        
        try{ 
            testClass.serverValidation();
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('The Mail Id does not exist in Database for selected User'));       
        }
        System.Test.StopTest(); 
        
        
        
        
    }
    
    
    
    public static void sendToisNotBlank()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        
        for(TripReportAttachmentController.ManagerUserWrapper mng1:testClass.manageWrapperList)
        {
            mng1.isSelected=true;
            testClass.sendTo.add(mng1.email);
        }
        
        try{
            
            testClass.serverValidation();
            
        }
        catch(Exception e)
        {
            System.assertEquals(true,testClass.sendTo.size()>0)  ;
        }
        System.Test.StopTest();
        
        
    }
    
    
    
    
    @isTest static public void testemailParametersSetupPass( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass =new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        
        testClass.sendTo.add('testmail@test.com');
        testClass.mail = new Messaging.SingleEmailMessage();
        testClass.mails = new List<Messaging.SingleEmailMessage>();
        
        testClass.emailParametersSetup();
        
        
        System.Test.StopTest();
    }       
    
    
    
    @isTest static public void testemailParametersFail1( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        //No Attachment Wrapper List set
        //No SendToSet
        //No email set
        testClass.sendTo=null;
        try{
            testClass.emailParametersSetup();
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Email Message is not set as expected.'));       
        }
        
        System.Test.StopTest();
    } 
    
    
    @isTest static public void testemailParametersSetupFail2( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        //No Attachment Selected
        testClass.sendTo.add('testmail@test.com');
        testClass.mail = new Messaging.SingleEmailMessage();
        testClass.mails = new List<Messaging.SingleEmailMessage>();
        testClass.attachmentWrapperList=null;
        testClass.sendTo.add('testmail@test.com');
        try{
            testClass.emailParametersSetup();
        }
        catch(Exception e)
            
        {
            System.assertNotEquals(true,e.getMessage().Contains('Attachment not selected in wrapper list or not available in database'));       
        }
        
        System.Test.StopTest();
    }       
    
    
    @isTest static public void testemailParametersSetupFail3( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        
        //Attachment Selected
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        testClass.sendTo.add('testmail@test.com');
        testClass.mail=null;
        
        
        //sendTo not set
        try{
            testClass.emailParametersSetup();
        }
        catch(Exception e)
            
        {
            System.assertEquals(true,e.getMessage().Contains('Email Message is not set as expected.'));       
        }
        
        System.Test.StopTest();
    }       
    
    
    
    
    @isTest static public void testSendEmailPass( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        testClass.sendTo.add('testmail@test.com');
        testClass.mail = new Messaging.SingleEmailMessage();
        testClass.mails = new List<Messaging.SingleEmailMessage>();
        
        testClass.emailParametersSetup();
        
        testClass.singleAttachment();
        
        
        System.Test.StopTest();
    }       
    @isTest static public void testSendEmailFail( )
    {
        System.Test.StartTest(); 
        Test.setCurrentPageReference(new PageReference('Page.TripReportAttachmentVF')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        for(TripReportAttachmentController.AttachmentWrapper atch :testClass.getAttachmentWrapperList())
        {
            atch.selectObj=true;
        }
        testClass.sendTo.add('testmail@test.com');
        testClass.mail = new Messaging.SingleEmailMessage();
        testClass.mails = new List<Messaging.SingleEmailMessage>();
        
        testClass.emailParametersSetup();
        testClass.mails=null; 
        testClass.singleAttachment();
        for(ApexPages.Message msg :  ApexPages.getMessages()) {
            System.assertNotEquals(true,msg.getSummary().contains('Mail Sent successfully to ['));       }
        
        
        System.Test.StopTest();
    }       
    
    
    @isTest public static  void testAllinOne() {  
        
        Test.starttest();
        
        //No Work Order Id Exception
        TripReportAttachmentController testClass = new TripReportAttachmentController(); 
        
        //No Work Order Found Exception
        PageReference pageRef = Page.TripReportAttachmentVF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('woId', '11');
        testClass = new TripReportAttachmentController(); 
        
        //No Attachement Exception        
        ApexPages.currentPage().getParameters().put('woId', WO.Id);
        testClass = new TripReportAttachmentController(); 
        testClass.getAttachmentWrapperList();
        
        testClass = new TripReportAttachmentController(); 
        
        //Sending email without attachments to throw exception
        testClass.singleAttachment(); 
        
        //Getting attachments and marking selected
        for(TripReportAttachmentController.AttachmentWrapper attachWrapper: testClass.getAttachmentWrapperList()) {
            attachWrapper.selectObj = true; 
        }
        
        //Sending email without contacts to throw exception
        testClass.singleAttachment(); 
        
        //Getting Contacts and marking selected
        for(TripReportAttachmentController.ManagerUserWrapper mng1:testClass.manageWrapperList)
        {
            mng1.isSelected=true;
            testClass.sendTo.add(mng1.email);
        }
        
        testClass.singleAttachment();
        
        testClass.back();
        Test.stoptest();
    }
    
    @isTest public static void backWithPGRef()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TestTripAttachmentVF345')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        testClass.back();
        System.Test.StopTest();   
    }
    @isTest public static void backWithoutPGRef()
    {
        System.Test.StartTest();
        TripReportAttachmentController testClass=new TripReportAttachmentController();
        testClass.back();
        System.Test.StopTest();   
    }
    
    
    
    
    
}
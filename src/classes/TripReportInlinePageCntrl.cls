/*
 * Author: Amitkumar Katre
 * Created Date: 15-May-2017
 * Project/Story/Inc/Task :STRY0031778
 * Description: This will be used to send trip report form trip report page, Trip report page controller
 *              STSK0012928 and STRY0031778
 Date/Modified By Name/Task or Story or Inc # /Description of Change
 May-21-2018  Chandramouli STSK0014844 Added new fields to the work order object for trip report.
 Feb-20-2018   Chandramouli STSK0013929 Added new fields to the Work Order Object for Trip Report. 
*/
public class TripReportInlinePageCntrl {
    
    @TestVisible private String workOrderId;
    public SVMXC__Service_Order__c woObj {get;set;}
    public SVMXC__Site__c site {get;set;}
    
    @TestVisible private List<String> sendTo = new List<String>();
    private String prameters = '';
    public List<OtherUserWrapper> otherWrapperList{get;set;}
    public List<ManagerUserWrapper> manageWrapperList{get;set;}
    private String appManager='Training Manager';
    private String salesManager='District Sales Manager';
    private String serviceManager='District Service Manager';
    private String technician='Technician';
    private String softwareManager = 'Solftware Sales Manager';
    private String emailInfoMsg='[Email Id does not exist for user ]';
    
    
    public PageReference onChangeLocation(){
        system.debug('Test==='+woObj.SVMXC__Site__c);
        if(woObj.SVMXC__Site__c != null)
        site = [select Id,Name, SVMXC__Street__c, SVMXC__City__c, SVMXC__State__c,SVMXC__Zip__c
                                    from  SVMXC__Site__c where id =:  woObj.SVMXC__Site__c];
         return null;
    }
    
    public TripReportInlinePageCntrl(){
        try{
            woObj = new SVMXC__Service_Order__c();
            workOrderId = ApexPages.currentPage().getParameters().get('woId');
            if(String.isBlank(workOrderId)){
                throw new TripReportException('Please pass work order id as parameter');
            }else{
                woObj  =[select Name,  Additional_Email_1__c,Case_Number__c, SVMXC__Company__c, SVMXC__Case__r.CaseNumber,Sales_Order__c,Sales_Order__r.Name, 
                         SVMXC__Group_Member__r.Name ,Contact_Email__c,Subject__c,Material_Description__c,SVMXC__Country__c,
                         SVMXC__Preferred_Start_Time__c,SVMXC__Preferred_End_Time__c,  SVMXC__Group_Member__r.User__r.email, 
                         Additional_Email_2__c,SVMXC__Site__r.Name,SVMXC__Group_Member__r.Technician_Manager__c,SVMXC__Contact__r.Name,
                         SVMXC__Company__r.Name, SVMXC__Site__r.SVMXC__Street__c, SVMXC__Site__r.SVMXC__City__c, SVMXC__Site__r.SVMXC__State__c, 
                         SVMXC__Site__r.SVMXC__Zip__c, SVMXC__Company__r.District_Sales_Manager__r.email,
                         SVMXC__Company__r.District_Service_Manager__r.email,
                         SVMXC__Company__r.Software_Sales_Manager__r.email,//Additional_Email_4__c,//Additional_Email_3__c Rakesh
                         District_Manager_1__c ,Training_Noteables__c,Product_Noteables__c,Service_Noteables__c,Competitive_Noteables__c,
                         Sales_Noteables__c,Follow_up_Noteables__c,SVMXC__Company__r.BillingStreet,
                         SVMXC__Company__r.BillingCity,SVMXC__Company__r.BillingState,SVMXC__Company__r.BillingPostalCode,
                         SVMXC__Company__r.Regional_Sales_Manager__r.email,Aiport_Hotel__c,Travel_Information__c,
                 SVMXC__Company__r.Regional_Service_Manager__r.email,SVMXC__Company__r.Training_Manager__r.email from SVMXC__Service_Order__c 
                 where Id =: workOrderId];
            }
            initManagerUsers();
            onChangeLocation();
        }
        catch(Exception exp){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,exp.getMessage());
            ApexPages.addMessage(errMsg);
        }
    }
    
     public PageReference cancel(){
         return new PageReference('/'+workOrderId);
     }
     
    public PageReference save(){
        try{        
            String woName = woObj.Name;
            serverValidation(); 
            woObj.Trip_Report_Sent__c = true;
            woObj.Trip_Report_Sent_Date__c = system.now();
            Database.update(woObj);
            woObj  =[select Name,  Additional_Email_1__c,Case_Number__c, SVMXC__Company__c, SVMXC__Case__r.CaseNumber,  SVMXC__Group_Member__r.Name ,Sales_Order__c,Sales_Order__r.Name,
                     Contact_Email__c,Subject__c,Material_Description__c,SVMXC__Country__c,SVMXC__Preferred_Start_Time__c,SVMXC__Preferred_End_Time__c,  SVMXC__Group_Member__r.User__r.email, Additional_Email_2__c,SVMXC__Site__r.Name,
                     SVMXC__Group_Member__r.Technician_Manager__c,SVMXC__Contact__r.Name,SVMXC__Company__r.Name,SVMXC__Site__r.SVMXC__Street__c,
                     //Additional_Email_4__c,// Additional_Email_3__c Rakesh
                     SVMXC__Site__r.SVMXC__City__c,SVMXC__Site__r.SVMXC__State__c,SVMXC__Site__r.SVMXC__Zip__c,
                     District_Manager_1__c ,Training_Noteables__c,Product_Noteables__c,Service_Noteables__c,Competitive_Noteables__c,Sales_Noteables__c,Follow_up_Noteables__c,SVMXC__Company__r.BillingStreet,SVMXC__Company__r.BillingCity,SVMXC__Company__r.BillingState,SVMXC__Company__r.BillingPostalCode, SVMXC__Company__r.Regional_Sales_Manager__r.email,
                     SVMXC__Company__r.Regional_Service_Manager__r.email,Aiport_Hotel__c,Travel_Information__c,
                     SVMXC__Company__r.Training_Manager__r.email
                     from SVMXC__Service_Order__c 
                     where Id =: workOrderId];
           
            woObj.SVMXC__Site__r = site;
            PageReference tripReportPdf = new  PageReference('/apex/TripReportPdf');
            tripReportPdf.setRedirect(false);
            Blob tripReportPdfBlob;
            if(!Test.isRunningTest()){
                tripReportPdfBlob = tripReportPdf.getContentAsPDF();
            }else{
                tripReportPdfBlob = Blob.valueOf('Test Data');
            }
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            Messaging.EmailFileAttachment efa1 = new Messaging.EmailFileAttachment();
            efa1.setFileName('Trip_Report_Output_'+woObj.Name+'_'+system.now()+'.pdf');
            efa1.setBody(tripReportPdfBlob);
            email.setSubject(woObj.Name+' - '+woObj.SVMXC__Company__r.Name);
            sendTo.add(woObj.SVMXC__Group_Member__r.User__r.email);
            email.setToAddresses( sendTo  );
            email.setPlainTextBody('Please find attached Trip report \n Thanks');
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa1});
            
            if(!Test.isRunningTest()){
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            Attachment att = new Attachment();
            att.name = 'Trip_Report_Output_'+woName+'_'+system.now()+'.pdf';
            att.body = tripReportPdfBlob;
            att.ParentId = woObj.Id;
            insert att;
            return new PageReference('/'+woOBj.id);
        }catch(Exception e){
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
    }
    
  
    
    public class ManagerUserWrapper{
        public Boolean isSelected{get;set;}
        public String name{get;set;}
        public String email{get;set;}
        public ManagerUserWrapper(String nameTemp, String emailT,Boolean flag){
            isSelected=flag;
            name=nameTemp;
            email=emailT;
        }
    }
           
    public class OtherUserWrapper{
        public Boolean isSelected{get;set;}
        public SVMXC__Service_Order__c workOrder{get;set;}
        public OtherUserWrapper(SVMXC__Service_Order__c temp,Boolean flag ){
            isSelected=flag;
            workOrder=temp;
        }
        
    }
    
    /*
    * @Method desc: Defined to initiate Manager(Internal User)
    * Three Managers -Application Manager
    * District Sales Manager and District Service Manager
    * are initialized
    */
    @TestVisible   private void initManagerUsers() {
        manageWrapperList=new List<ManagerUserWrapper>();
        String appEmail;
        String saleEmail;
        String servEmail;
        String techEmail;
        String softEmail;//Rakesh
        /*
        if(String.isNotBlank(woObj.SVMXC__Group_Member__r.Technician_Manager__c))
        {
            List<String> strList=woObj.SVMXC__Group_Member__r.Technician_Manager__c.split(' ');
            User appUser=[select Id ,Name, Email from user where firstname=: strList[0] and lastname=:strList[1] Limit 1];
            if(String.isNotBlank(appUser.Email))
            {
                appEmail=appUser.Email;
            }
            
        }*/
        
        if(String.isNotBlank(woObj.SVMXC__Company__r.Training_Manager__r.email)) 
        {
            appEmail = woObj.SVMXC__Company__r.Training_Manager__r.email;
        }
        
        if(String.isNotBlank(woObj.SVMXC__Company__r.District_Sales_Manager__r.email)) 
        {
            saleEmail=woObj.SVMXC__Company__r.District_Sales_Manager__r.email;
        }
        else
        {
            saleEmail=emailInfoMsg; 
        }
        /*if(String.isNotBlank(woObj.SVMXC__Company__r.Regional_Service_Manager__r.email))
           {
               servEmail=woObj.SVMXC__Company__r.Regional_Service_Manager__r.email ;
           }*/
           if(String.isNotBlank(woObj.SVMXC__Company__r.District_Service_Manager__r.email))
           {
               servEmail=woObj.SVMXC__Company__r.District_Service_Manager__r.email ;
           }
        else
           {
               servEmail=emailInfoMsg;
           }
           
           if(String.isNotBlank(woObj.SVMXC__Group_Member__r.User__r.email))
           {
               techEmail=woObj.SVMXC__Group_Member__r.User__r.email;
           }
           else
           {
               techEmail=emailInfoMsg;
           }
           
            //Rakesh
            if(String.isNotBlank(woObj.SVMXC__Company__r.Software_Sales_Manager__r.email))
                softEmail=woObj.SVMXC__Company__r.Software_Sales_Manager__r.email ;         
            else
                softEmail=emailInfoMsg;         
            //End
           
           ManagerUserWrapper user1= new ManagerUserWrapper(appManager,appEmail,true);
           ManagerUserWrapper user2= new ManagerUserWrapper(salesManager,saleEmail,true);
           ManagerUserWrapper user3= new ManagerUserWrapper(serviceManager,servEmail,true);
           ManagerUserWrapper user4= new ManagerUserWrapper(softwareManager,softEmail,true);
           //ManagerUserWrapper user4= new ManagerUserWrapper(technician,techEmail,false);
           manageWrapperList.add(user1);
           manageWrapperList.add(user2);
           manageWrapperList.add(user3);
           manageWrapperList.add(user4);//Rakesh
           //manageWrapperList.add(user4);
     }
           
           
   /*
    * @Method desc: Defined to initiate Other User(Internal User)
    * Three Users are initialized
    */
    @testvisible private void initOtherUsers(){
        otherWrapperList = new List<OtherUserWrapper>();
        for(Integer i=0; i<3; i++){
            OtherUserWrapper user1= new OtherUserWrapper(new SVMXC__Service_Order__c(),false);  
            otherWrapperList.add(user1);
        }
        
    }
    
    //Exception class extends Exception
    public class TripReportException extends Exception{}   
    
    
    /**@author: Deepak
        @Method Desc: Does the input validations.Need to be moved
        to VF Page. Pending Work Item
        @return : PageReference -returns to launching page
    * 
    */  
    @TestVisible private void serverValidation(){
        Boolean noManagerSel=false;
        Integer countMng=0;
        Integer countOth=0;
        for(ManagerUserWrapper mW: manageWrapperList )
        {
            if(mW.isSelected)
            {
                countMng++;
                if(String.isNotBlank(mW.email) && !(mW.email.equals(emailInfoMsg )))
                {
                    sendTo.add(mW.email);
                    
                }
            }
        }
        
        if(woObj.Additional_Email_1__c !=null) {
            countOth++;
            User tempUser= [select id,email from user where id=:woObj.Additional_Email_1__c Limit 1];
            if(String.isNotEmpty(tempUser.Email)){
                sendTo.add(tempUser.Email);
            }
        }
        if(woObj.Additional_Email_2__c !=null) {
            countOth++;
            User tempUser= [select id,email from user where id=:woObj.Additional_Email_2__c  Limit 1];
            if(String.isNotEmpty(tempUser.Email)){
                sendTo.add(tempUser.Email);
            }
        }
        /*
        if(woObj.Additional_Email_3__c !=null) {
            countOth++;
            User tempUser= [select id,email from user where id=:woObj.Additional_Email_3__c Limit 1];
            if(String.isNotEmpty(tempUser.Email)){
                sendTo.add(tempUser.Email);
            }
        }
        */
        if(countMng==0 && countOth==0)
        {
            throw new TripReportException('Please select atleast one [Manager] or [Additional User]');            
        }
        if(sendTo.isEmpty())
        {
            throw new TripReportException('The Mail Id does not exist in Database for selected User'); 
        }
            
    }
}
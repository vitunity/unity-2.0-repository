@IsTest
public class TripReportInlinePageCntrlTest {

    Static testmethod void testmethod1() {
        Account accnt = new Account();
        accnt.Name = 'Wipro technologies';
        accnt.BillingCity = 'New York';
        accnt.country__c = 'USA';
        accnt.OMNI_Postal_Code__c = '940022';
        accnt.BillingCountry = 'USA';
        accnt.BillingStreet = 'New Jersey';
        accnt.BillingPostalCode = '552600';
        accnt.BillingState = 'LA';
        insert accnt;
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        cdf.Name = 'Default';
        insert cdf;
        BusinessHours businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        Product2 newProd = UnityDataTestClass.product_Data(true);
        Sales_Order__c SO = UnityDataTestClass.SO_Data(true, accnt);
        Sales_Order_Item__c SOI = UnityDataTestClass.SOI_Data(true, SO);
        SOI.WBS_Element__c = 'M-0320947099'; 
        update SOI;
        ERP_Project__c erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        Case newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        SVMXC__Service_Group__c servTeam = UnityDataTestClass.serviceTeam_Data(false);
        servTeam.Work_Order_Review_Queue__c = 'Customer Service';
        servTeam.District_Manager__c = userInfo.getUserId();
        insert servTeam;
        Timecard_Profile__c TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        Organizational_Calendar__c orgCal = UnityDataTestClass.OrgCalData(true);
        User u = [select manager.FirstName from user where manager.FirstName != null limit 1][0];
        SVMXC__Service_Group_Members__c technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.Organizational_Calendar__c = orgCal.Id;
        technician.User__c = u.id; 
        insert technician;

        SVMXC__Service_Order__c WO = UnityDataTestClass.WO_Data(true, accnt, newCase, newProd, SOI, technician);
        wo.SVMXC__Group_Member__c = technician.id;
        wo.Additional_Email_1__c = userInfo.getUserId();
        wo.Additional_Email_2__c = userInfo.getUserId();
        wo.Additional_Email_3__c = userInfo.getUserId();
        update wo;

        //System.assertequals([Select id,SVMXC__Group_Member__r.Technician_Manager__c from SVMXC__Service_Order__c where id=:wo.id][0].SVMXC__Group_Member__r.Technician_Manager__c,null);
        PageReference pageRef = Page.TripReportInlinePage;
        Test.setCurrentPage(pageRef);
        //System.assertNOTequals(WO.id,null);
        ApexPages.currentPage().getParameters().put('woId', WO.Id);
        //System.assertequals(ApexPages.currentPage().getParameters().get('id'),'12');
        TripReportInlinePageCntrl tc = new TripReportInlinePageCntrl();

        tc.initManagerUsers();
        tc.save();

        tc.cancel();

        TripReportInlinePageCntrl.OtherUserWrapper tcv = new TripReportInlinePageCntrl.OtherUserWrapper(wo, false);

        tc.onChangeLocation();
        tc.initOtherUsers();
        //tc.serverValidation();
    }


}
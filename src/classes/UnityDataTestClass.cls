/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class UnityDataTestClass {
    
    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> serviceTeamRecordType = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> woRecordType = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> TechnicianRecordType = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> wdRecordType = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> stockHistoryRecordType = Schema.SObjectType.SVMXC__Stock_History__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> partsOrderRecordType = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> partsOrderLineRecordType = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> timeEntriesRecordType = Schema.SObjectType.SVMXC_Time_Entry__c.getRecordTypeInfosByName();
    
    
    public static String textAreaMsg =  'This message may contain information that is confidential or otherwise protected' +
                                        'from disclosure. If you are not the intended recipient, you are hereby notified' +
                                        'that any use, disclosure, dissemination, distribution, or copying of this' +
                                        'message, or any attachments, is strictly prohibited. If you have received this' +
                                        'message in error, please advise the sender by reply e-mail, and delete the' +
                                        'message and any attachments. Thank you.';
    
    public static User InsertUserData(Boolean isInsert) {
        list<profile> serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Service - User' or Name =: 'VMS Unity 1C - Service User'];
        User serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile[0].Id, Email = 'test1c@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1c@service.com');
        if(isInsert)
            insert serviceUser;
        
        return serviceUser;
    }
    
    public static Account AccountData(Boolean isInsert) {
        Account newAcc = new Account();
        newAcc.Name ='Test AccountSFQA';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.Billingcity = 'Milpitas';
        newAcc.State__C = 'California';        
        newAcc.Country__c = 'USA';
        newAcc.BillingState = 'CA';    /* added by Keerat Kaur for STRY0051307 */
        if(isInsert)
            insert newAcc;
        return newAcc;
    }
    
    // Create Contact Data
    public static Contact contact_data(Boolean isInsert, Account newAcc) {
        Contact newCont = new Contact();
        newCont.FirstName = 'Test';
        newCont.LastName = 'Contact';
        newCont.Email = 'Test@email.com';
        newCont.AccountId = newAcc.Id;
        if(isInsert)
            insert newCont;
        return newCont;
    }
    
    // Create Product
    public static Product2 product_Data(Boolean isInsert) {
        Product2 newProd = new Product2();
        newProd.Name = 'Rapid Arc for Eclipse';
        newProd.ProductCode = 'TEST_VC_RAE';
        newProd.BigMachines__Part_Number__c = 'TEST_VC_RAE' + Math.random();
        newProd.Product_Type__c = 'Part';
        newProd.IsActive = true;
        newProd.Product_Source__c = 'SAP';
        newProd.ERP_Plant__c = '0600';
        newProd.ERP_Sales_Org__c = '0600';
        newProd.UOM__c = 'EA';
        if(isInsert)
            insert newProd;
        return newProd;
    }
    
    // Create CASE
    public static Case Case_Data(Boolean isInsert, Account newAcc, Product2 newProd) {
        Case newCase = new Case();
        newCase.AccountId = newAcc.Id;
        newCase.Status = 'New';
        newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId();
        newCase.Product__c = newProd.Id; 
        if(isInsert)
            insert newCase;
        return newCase;
    }
    
    // Creating Case Line Data
    public static SVMXC__Case_Line__c CaseLine_Data(Boolean isInsert, Case newCase, product2 newProd) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase.id;
        caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd.Id;
        caseLine.Start_Date_time__c = system.today().addDays(-2);
        caseLine.End_date_time__c = system.today().addDays(+2);
        //caseLine.SVMXC__Installed_Product__c = ;
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    
    public static L2848__c L2848_TestData(Boolean isInsert, Case newCase, User serviceUser) {
        L2848__c L2848 = new L2848__c();
        L2848.City__c = 'BONITA SPRINGS';
        L2848.State_Province__c = 'FL';
        L2848.Country__c = 'USA';
        L2848.Name_of_Customer_Contact__c = 'Neha Arora';
        L2848.Title_Role__c = 'Test';
        L2848.Phone_Number__c = '123456789';
        L2848.E_mail_Address__c = 'test@emailt.com';
        L2848.Service_Request_Notification__c = newCase.CaseNumber;
        L2848.Case__c = newCase.Id;
        L2848.Employee_Phone__c = '123456';
        L2848.Date_of_Event__c = system.today().addDays(-2);
        L2848.Date_Only_Reported_to_Varian__c = system.today();
        L2848.Device_or_Application_Name__c = 'Test Application';
        L2848.Version_Model__c = '5';
        L2848.Was_anyone_injured__c = 'Yes';
        L2848.Is_Submit__c = false;
        L2848.Alert_If_No_Action__c = serviceUser.Id;
        //if(isInsert)
            insert L2848;
        return L2848;
    }
    
    // Creating Location Record
    public static SVMXC__Site__c Location_Data(Boolean isInsert, Account newAcc) {
        SVMXC__Site__c newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        if(isInsert)
            insert newLoc;
        return newLoc;
    }
    
    // Creating Service Team record
    public static SVMXC__Service_Group__c serviceTeam_Data(Boolean isInsert) {
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c();
        serviceTeam.Name = '[U9J] GAMMAMED';
        serviceTeam.RecordTypeId = serviceTeamRecordType.get('Technician').getRecordTypeId();
        serviceTeam.SVMXC__Group_Type__c = 'Field Service';
        serviceTeam.SVMXC__Active__c = true;
        serviceTeam.SVMXC__Group_Code__c = 'GRP07';
        if(isInsert)
            insert serviceTeam;
        return serviceTeam;
    }
    
    // Creating ERP_Pcode__c record
    public static ERP_Pcode__c ERPPCODE_Data(Boolean isInsert) {
        ERP_Pcode__c erpPcode = new ERP_Pcode__c();
        erpPcode.Name = 'H19';
        erpPcode.ERP_Pcode_2__c = '19';
        erpPcode.Description__c = 'TrueBeam';
        erpPcode.IsActive__c = true;
        erpPcode.Service_Product_Group__c = 'LINAC';
        if(isInsert)
            insert erpPcode;
        return erpPcode;
    }
    
    // Creating Installed Product Data
    public static SVMXC__Installed_Product__c InstalledProduct_Data(Boolean isInsert, Product2 newProd, Account newAcc, SVMXC__Site__c newLoc,
                                                                    SVMXC__Service_Group__c serviceTeam, ERP_Pcode__c erpPcode) {
        SVMXC__Installed_Product__c installProd = new SVMXC__Installed_Product__c();
        installProd.Name = 'H621598';
        installProd.SVMXC__Status__c = 'Installed';
        installProd.ERP_Reference__c = 'H621598-VE';
        installProd.SVMXC__Product__c = newProd.Id;
        installProd.ERP_Pcodes__c = erpPcode.Id;
        installProd.SVMXC__Company__c = newAcc.Id;
        installProd.SVMXC__Site__c = newLoc.Id;
        installProd.ERP_Functional_Location__c = 'H-COLUMBUS -OH-US-005';
        installProd.Billing_Type__c = 'P – Paid Service';
        installProd.Service_Team__c = serviceTeam.Id;
        installProd.ERP_CSS_District__c = 'U9J';
        installProd.ERP_Work_Center__c = 'H87696';
        installProd.ERP_Product_Code__c = 'H62A';
        if(isInsert)
            insert installProd;
        return installProd;
    }
    
    // Creating Sales Order Data
    public static Sales_Order__c SO_Data(Boolean isInsert, Account newAcc) {
        Sales_Order__c so = new Sales_Order__c();
        so.name = '319112526';
        so.Site_Partner__c = newAcc.Id;
        so.Sold_To__c = newAcc.Id;
        so.Requested_Delivery_Date__c = system.today().addMonths(-1);
        so.Sales_Group__c = 'N08';
        so.Sales_District__c = 'NA02';
        so.Status__c ='Open';
        so.Sales_Org__c = '0601';
        if(isInsert)
            insert so;
        return so;
    }
    
    // Creating Sales Order Item Data
    public static Sales_Order_Item__c SOI_Data(Boolean isInsert, Sales_Order__c so) {
        Sales_Order_Item__c soi = new Sales_Order_Item__c();
        soi.Name = '000005';
        soi.Sales_Order__c = so.id;
        soi.ERP_Material_Number__c = 'FI_REV_ITEM';
        soi.ERP_Equipment_Number__c = '123';
        if(isInsert)
            insert soi;
        return soi;
    }
    
    // Creating ERP Partner Data
    public static ERP_Partner__c createERPPartner(Boolean isInsert, Account newAcc) {
        ERP_Partner__c newERPPartner = new ERP_Partner__c();
        newERPPartner.Name = 'testPartner';
        newERPPartner.Partner_Number__c = '098765';
        if(newAcc != null)
            newERPPartner.Account__c = newAcc.Id;
        if(isInsert)
            insert newERPPartner;
        return newERPPartner;
    }
    
    // Creating Work Order Data
    public static SVMXC__Service_Order__c WO_Data(Boolean isInsert, Account newAcc, Case newCase, Product2 newProd, Sales_Order_Item__c newSOI,
                                                    SVMXC__Service_Group_Members__c technician) {
        SVMXC__Service_Order__c newWO = new SVMXC__Service_Order__c();
        newWO.SVMXC__Case__c = newCase.id;
        newWO.Event__c = True;
        newWO.SVMXC__Company__c = newAcc.ID;
        newWO.SVMXC__Order_Status__c = 'Open';
        newWO.SVMXC__Preferred_Technician__c = technician.id;
        newWO.recordtypeid = woRecordType.get('Installation').getRecordTypeId();
        newWO.SVMXC__Product__c = newProd.ID;
        newWO.SVMXC__Problem_Description__c = 'Test Description';
        newWO.SVMXC__Preferred_End_Time__c = system.now()+3;
        newWO.SVMXC__Preferred_Start_Time__c  = system.now();
        newWO.Subject__c= 'bbb';
        newWO.Sales_Order_Item__c = newSOI.id;
        if(isInsert)
            insert newWO;
        return newWO;
    }
    
    // Creating Work Detail Data
    public static SVMXC__Service_Order_Line__c WD_Data(Boolean isInsert, SVMXC__Service_Order__c newWO, SVMXC__Service_Group_Members__c technician, 
                                                        SVMXC__Site__c newLoc, Sales_Order_Item__c newSOI, ERP_NWA__c erpnwa) {
        SVMXC__Service_Order_Line__c newWD = new SVMXC__Service_Order_Line__c();
        newWD.SVMXC__Consumed_From_Location__c = newLoc.id;
        newWD.SVMXC__Activity_Type__c = 'Install';
        newWD.Completed__c=false;
        newWD.SVMXC__Group_Member__c = technician.id;
        newWD.RecordTypeId = wdRecordType.get('Products Serviced').getRecordTypeId();
        newWD.SVMXC__Work_Description__c = 'test';
        newWD.SVMXC__Start_Date_and_Time__c =System.now();
        newWD.Actual_Hours__c = 5;
        newWD.SVMXC__Service_Order__c = newWO.id;
        newWD.Sales_Order_Item__c = newSOI.id;
        newWD.NWA_Description__c = 'nwatest';
        if(erpnwa != null)
            newWD.ERP_NWA__c = erpnwa.id;
        if(isInsert)
            insert newWD;
        return newWD;
    }
    
    // Creating Counter Usage
    public static Counter_Usage__c CounterUSE_Data(Boolean IsInsert, SVMXC__Service_Order_Line__c WD, SVMXC__Counter_Details__c counter) {
      Counter_Usage__c usage = new Counter_Usage__c();
      usage.Work_Detail__c = WD.Id;
      usage.Value__c = 1;
      usage.Counter_Detail__c = counter.Id;
      if(IsInsert)
        insert usage;
      return usage;
    }
    
    // Creating technician Data
    public static SVMXC__Service_Group_Members__c Techinician_Data(Boolean isInsert, SVMXC__Service_Group__c serviceTeam, BusinessHours businesshr) {
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.Name = 'Test Technician';
        tech.SVMXC__Active__c = true;
        tech.RecordTypeID = TechnicianRecordType.get('Technician').getRecordTypeId();
        tech.SVMXC__Enable_Scheduling__c = true;
        tech.SVMXC__Phone__c = '987654321';
        tech.SVMXC__Email__c = 'abc@xyz.com';
        tech.SVMXC__Service_Group__c = serviceTeam.ID;
        tech.SVMXC__Street__c = 'Test Street';
        tech.SVMXC__Zip__c = '98765';
        tech.SVMXC__Country__c = 'United States';
        tech.OOC__c = false;
        tech.SVMXC__Working_Hours__c = businesshr.Id;
        tech.User__c = userInfo.getUserID();
        tech.SVMXC__Salesforce_User__c = userInfo.getUserId();
        tech.Dispatch_Queue__c = 'DISP CH';
        if(isInsert)
            insert tech; 
        return tech;
    }
    
    //Creating Counter Details Data
    public static SVMXC__Counter_Details__c CD_Data(Boolean isInsert, SVMXC__Service_Contract_Products__c covrProd, Sales_Order_Item__c newSOI) {
        SVMXC__Counter_Details__c newCD = new SVMXC__Counter_Details__c();
        newCD.SVMXC__Active__c = true;
        newCD.SVMXC__Covered_Products__c = covrProd.id;
        newCD.Sales_Order_Item__c = newSOI.id;
        newCD.SVMXC__Counters_Covered__c = 20;
        if(isInsert)
            insert newCD;
        return newCD;
    }
    
    // Creating SLA Term data
    public static SVMXC__Service_Level__c slaterm_Data(Boolean isInsert, BusinessHours businessHr) {
        SVMXC__Service_Level__c slaTerm = new SVMXC__Service_Level__c();
        slaTerm.Name = 'NA_Essential_HW#40210172#001310';
        slaTerm.SVMXC__Business_Hours__c = businessHr.Id;
        slaTerm.SVMXC__Description__c = 'test Description';
        slaTerm.SVMXC__Initial_Response_Tracked_On__c = 'Case';
        slaTerm.SVMXC__Active__c = true;
        slaTerm.SVMXC__Effective_Date__c = system.today().addMonths(1);
        slaTerm.SVMXC__Onsite_Response_Tracked_On__c = 'Work Order';
        slaTerm.SVMXC__Resolution_Tracked_On__c = 'Case';
        slaTerm.ERP_Reference__c = '';
        slaterm.ERP_Contract_Hours__c = 'LABOR_08-17';
        if(isInsert)
            insert slaTerm;
        return slaTerm;
    }
    
    // Creating SLA Details Data
    public static SVMXC__SLA_Detail__c sladetail_Data(Boolean isInsert, SVMXC__Service_Level__c slaTerm, SVMXC__Service_Contract__c servContract) {
        SVMXC__SLA_Detail__c slaDetail = new SVMXC__SLA_Detail__c();
        slaDetail.Service_Maintenance_Contract__c = servContract.Id;
        slaDetail.SVMXC__SLA_Terms__c = slaTerm.Id;
        slaDetail.Status__c = 'Valid';
        slaDetail.SVMXC__Business_Hours_Source__c = 'Contact';
        sladetail.SVMXC__Included_Or_Not__c = true;
        slaDetail.SVMXC__Commitment_Type__c = 'InitialResponse';
        slaDetail.ERP_Reference__c = '40210172#001010#';
        if(isInsert)
            insert slaDetail;
        return slaDetail;
    }
    
    // Creating Service Contract Data
    public static SVMXC__Service_Contract__c servContract_Data(Boolean isInsert, SVMXC__Service_Level__c slaTerm, Account newAcc, SVMXC__Service_Group__c serviceTeam) {
        SVMXC__Service_Contract__c servContract = new SVMXC__Service_Contract__c();
        servContract.Name = ' Test Contract';
        servContract.SVMXC__Service_Level__c = slaTerm.Id;
        servContract.SVMXC__Company__c = newAcc.Id;
        servContract.SVMXC__Default_Service_Group__c = serviceTeam.Id; 
        servContract.SVMXC__Active__c = true;
        servContract.SVMXC__Start_Date__c = system.today();
        servContract.SVMXC__End_Date__c = system.today().addYears(1);
        servContract.ERP_Header_Contract_Description__c = 'SLA001001012 4/5';
        servContract.ERP_PO_Number__c = 'NONE PROVIDED';
        servContract.EXT_Quote_Number__c = 'CXM20130917-002B';
        servContract.ERP_Contract_Header__c = UnityDataTestClass.textAreaMsg;
        servContract.ERP_Internal_Note__c = UnityDataTestClass.textAreaMsg;
        servContract.ERP_Sales_Org__c = '0600';
        servContract.ERP_Order_Reason__c = 'HS4 EWO Contract';
        servContract.ERP_Contract_Type__c = 'ZH1';
        servContract.Non_entitlement_flag__c = 'Y';
        
        if(isInsert)
            insert servContract;
        return servContract;
    }
    
    // Creating ERP Project Data
    public static ERP_Project__c ERPPROJECT_Data(Boolean isInsert) {
        ERP_Project__c erpProject = new ERP_Project__c();
        erpProject.Name = 'M-0320947099';
        erpProject.Description__c = 'Test Description';
        erpProject.Start_Date__c = system.today().addDays(-10);
        erpProject.End_Date__c = system.today().addMonths(2);
        //erpProject.ERP_Status__c = 'SETC;REL'; //DE7931
        erpProject.Status__c = 'SETC;REL';
        erpProject.Org__c = '0601';
        
        if(isInsert)
            insert erpProject;
        return erpProject;
    }
    
    // Creating ERP WBS Data
    public static ERP_WBS__c ERPWBS_DATA(Boolean isInsert, ERP_Project__c erpProject, SVMXC__Service_Group_Members__c technician) {
        ERP_WBS__c erpWBS = new ERP_WBS__c();
        erpWBS.Name = 'M-0320947099-9001-13';
        erpWBS.Description__c = 'Test Description';
        erpWBS.ERP_Project__c = erpProject.Id;
        erpWBS.Project_Manager__c = technician.Id;
        erpWBS.ERP_Project_Nbr__c = 'M-0320947099';
        //erpWBS.ERP_Status__c = 'SETC;REL'; //DE7931
        erpWBS.Status__c = 'SETC;REL';
        erpWBS.ERP_PM_Work_Center__c = 'H957353';
        erpWBS.ERP_Profit_Center__c = '10130150';
        if(isInsert)
            insert erpWBS;
        return erpWBS;  
    }
    // Creating ERP NWA
    public static ERP_NWA__c NWA_Data(Boolean isInsert, Product2 newProd, ERP_WBS__c wbs, ERP_Project__c erpProject) {
        ERP_NWA__c nwa = new ERP_NWA__c();
        nwa.Name = '4203980-0230';
        nwa.Description__c = 'Test Description';
        nwa.Product__c = newProd.Id;
        nwa.ERP_Project__c = erpProject.Id;
        nwa.WBS_Element__c = wbs.Id;
        nwa.ERP_Project_Nbr__c = 'M-0320947099';
        nwa.ERP_WBS_Nbr__c = 'M-0320947099-9001-13';
        nwa.ERP_Std_Text_Key__c = 'TRN0005';
        //nwa.Status__c = 'CNF'; //DE7931
        nwa.ERP_Status__c = 'CNF';
        if(isInsert)
            insert nwa;
        return nwa;
    }
    
    // Creating Covered Product Data
    public static SVMXC__Service_Contract_Products__c CoveredProd_Data(Boolean isInsert, SVMXC__Service_Level__c slaTerm, SVMXC__Service_Contract__c servcContrct) {
        SVMXC__Service_Contract_Products__c covrProd = new SVMXC__Service_Contract_Products__c();
        covrProd.ERP_Site_Partner__c = 'sitePartner';
        covrProd.SVMXC__Service_Contract__c = servcContrct.id ;
        covrProd.SVMXC__Product_Line__c = 'ARIA';
        covrProd.Serial_Number_PCSN__c = 'sr123';
        covrProd.ERP_Functional_Location__c = 'funcLocation';
        covrProd.Product_Code__c = 'pro123';
        covrProd.Cancelation_Reason__c = 'cancel';
        covrProd.SVMXC__SLA_Terms__c = slaTerm.id;
        if(isInsert)
            insert covrProd;
        return covrProd;
    }
    
    // Creating Product FRU data
    public static Product_FRU__c Product_FRU_TestData (Boolean isInsert, Product2 newProd, Product2 part) {
        Product_FRU__c prodFRu = new Product_FRU__c();
        prodFRU.Top_Level__c = newProd.Id;
        prodFRU.Part__c = part.Id;
        if(isInsert)
            insert prodFRU;
        return prodFRU;
    }
    
    // Creating Product Stock Data
    public static SVMXC__Product_Stock__c productStock_Data(Boolean isInsert, Product2 newProd, SVMXC__Site__c newLoc) {
        SVMXC__Product_Stock__c prodStock = new SVMXC__Product_Stock__c();
        prodStock.SVMXC__Status__c = 'Available';
        prodStock.SVMXC__Quantity2__c = 10.00;
        prodStock.SVMXC__Product__c = newProd.Id;
        prodStock.SVMXC__Location__c = newLoc.Id;
        if(isInsert)
            insert prodStock;
        return prodStock;
    }
    
    // Parts Order Data SVMXC__RMA_Shipment_Order__c
    public static SVMXC__RMA_Shipment_Order__c partsOrder_Data(Boolean isInsert, Case newCase, Account newAcc, Contact newContact, 
                                                                SVMXC__Service_Group_Members__c technician, SVMXC__Site__c newLoc, SVMXC__Site__c otherLoc) {
        SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
        partsOrder.RecordTypeId = partsOrderRecordType.get('Shipment').getRecordTypeId();
        partsOrder.SVMXC__Order_Status__c = 'Approved';
        //if(newCase!=null)
        //partsOrder.SVMXC__Case__c = newCase.Id;
        partsOrder.SVMXC__Company__c = newAcc.Id;
        partsOrder.SVMXC__Contact__c = newContact.Id;
        //partsOrder.Technician__c = technician.Id;
        //partsOrder.SVMXC__Source_Location__c = newLoc.Id;
        //partsOrder.SVMXC__Destination_Location__c = otherLoc.Id;
        partsOrder.SVMXC__Expected_Delivery_Date__c = system.today().addDays(3);
        partsOrder.SVMXC__Order_Type__c = 'Shipment';
        system.debug(' ==== ' + partsOrder);
        if(isInsert)
            insert partsOrder;
        return partsOrder;
    }
    
    // Creating Part order Line  Data
    public static SVMXC__RMA_Shipment_Line__c partOrderLine_Data(Boolean isInsert, Product2 newProd, SVMXC__RMA_Shipment_Order__c partsOrder ) {
        SVMXC__RMA_Shipment_Line__c porderLine = new SVMXC__RMA_Shipment_Line__c();
        porderLine.RecordTypeId = partsOrderLineRecordType.get('Shipment').getRecordTypeId();
        porderLine.SVMXC__Line_Status__c = 'Completed';
        porderLine.SVMXC__Expected_Quantity2__c = 7.00;
        porderLine.SVMXC__Line_Type__c = 'Outbound (Shipment)';
        porderLine.Interface_Status__c = 'Processed';
        porderLine.SVMXC__RMA_Shipment_Order__c = partsOrder.Id;
        porderLine.Consumed__c = false;
        if(isInsert)
            insert porderLine;
        return porderLine;
    }
    
    // Insert Stock History Data
    public static SVMXC__Stock_History__c stockHistory_Data(Boolean isInsert, Product2 newProd, SVMXC__Site__c newLoc, SVMXC__Product_Stock__c prodStock) {
        SVMXC__Stock_History__c stockHistory = new SVMXC__Stock_History__c();
        stockHistory.SVMXC__Product__c = newProd.Id;
        stockHistory.SVMXC__Location__c = newLoc.Id;
        stockHistory.SVMXC__Product_Stock__c = prodStock.Id;
        stockHistory.SVMXC__Status__c = 'In Transit';
        if(isInsert)
            insert stockHistory;
        return stockHistory;
    }
    
    // Insert SVMXC__Product_Serial__c
    public static SVMXC__Product_Serial__c produSerial_Data(Boolean isInsert, SVMXC__Product_Stock__c prodStock) {
        SVMXC__Product_Serial__c serial = new SVMXC__Product_Serial__c();
        serial.Name = 'Test Serial';
        serial.SVMXC__Product_Stock__c = prodStock.Id;
        serial.SVMXC__Active__c = true;
        if(isInsert)
            insert serial;
        return serial;
    }
    
    // Insert Product Association
    public static Product_FRU__c ProductAssociation_Data(Boolean isInsert, Product2 newProd) {
        Product_FRU__c newProdAsso = new Product_FRU__c();
        //newProdAsso.Name = 'PRDASSC-000000000';
        newProdAsso.Top_Level__c = newProd.Id;
        newProdAsso.Part__c = newProd.Id;
        if(isInsert)
            insert newProdAsso;
        return newProdAsso;
    }
    
    // Insert Time Sheets Records
    public static SVMXC_Timesheet__c TIMESHEET_Data(Boolean isInsert, SVMXC__Service_Group_Members__c tech) {
      SVMXC_Timesheet__c timeSheet = new SVMXC_Timesheet__c();
      timeSheet.Start_Date__c = system.Today();
      timeSheet.End_Date__c = system.today().addDays(1);
      timeSheet.Status__c = 'Open';
      timeSheet.Technician__c = tech.Id;
      timeSheet.TimeSheet_Unique__c = tech.Id + tech.ERP_Work_Center__c + system.today().year()+ system.today().month()+ system.today().day();
      if(isInsert)
        insert timeSheet;
      return timeSheet;
    }
    
    // Insert Time Entries Record
    public static SVMXC_Time_Entry__c TIMEENTRIES_DATA(Boolean isInsert, Boolean Direct, SVMXC_Timesheet__c timeSheet,
                                SVMXC__Service_Order_Line__c WD) {
      SVMXC_Time_Entry__c timeEntries = new SVMXC_Time_Entry__c();
      timeEntries.Timesheet__c = timeSheet.Id;
      timeEntries.Status__c = 'Submitted';
      timeEntries.Technician__c = timeSheet.Technician__c;
      timeEntries.Activity__c = 'Work Detail-Labor';
      
      if(Direct)
        timeEntries.RecordTypeId = timeEntriesRecordType.get('Direct Hours').getRecordTypeId();
      else
        timeEntries.RecordTypeId = timeEntriesRecordType.get('Indirect hours').getRecordTypeId();
      
      if(Direct)
        timeEntries.Work_Details__c = WD.Id;
      
      Integer Yr = System.Today().Year();
      Integer Mon = System.Today().Month();
      Integer Dat = System.Today().Day();
      timeEntries.Start_Date_Time__c = DateTime.newInstance(Yr, Mon, Dat, 7, 0, 00);
      timeEntries.End_Date_Time__c = DateTime.newInstance(Yr, Mon, Dat, 11, 0, 00);
      
      if(isInsert)
        insert timeEntries;
      return timeEntries;
    }
    
    // Time Card Profile Record
    public static Timecard_Profile__c TIMECARD_Data(Boolean isInsert) {
      Timecard_Profile__c profile = new Timecard_Profile__c();
      profile.Normal_Start_Time__c = DateTime.newInstance(system.today().year(), 1, 31, 7, 0, 00);
      profile.Normal_End_Time__c = DateTime.newInstance(system.today().year(), 1, 31, 14, 0, 00);
      profile.Max_Hours_Day_1__c = '0';
      profile.Max_Hours_Day_1__c = '0';
      profile.Max_Hours_Day_1__c = '8';
      profile.Max_Hours_Day_1__c = '8';
      profile.Max_Hours_Day_1__c = '8';
      profile.Max_Hours_Day_1__c = '8';
      profile.Max_Hours_Day_1__c = '8';
      profile.Max_Hours_per_Week__c = '40';
      profile.First_Day_of_Week__c = 'Saturday';
      profile.Lunch_Time_Start__c = DateTime.newInstance(system.today().year(), 1, 31, 10, 0, 00);
      profile.Lunch_Time_duration__c = '60';
      If(isInsert)
        insert profile;
      return profile;
    }
    
    
     // Time Card Profile Record
    public static Timecard_Profile__c TIMECARD_Data1(Boolean isInsert) {
      Timecard_Profile__c profile1 = new Timecard_Profile__c();
      profile1.Normal_Start_Time__c = DateTime.newInstance(system.today().year(), 1, 31, 7, 0, 00);
      profile1.Normal_End_Time__c = DateTime.newInstance(system.today().year(), 1, 31, 14, 0, 00);
      profile1.Max_Hours_Day_1__c = '0';
      profile1.Max_Hours_Day_1__c = '0';
      profile1.Max_Hours_Day_1__c = '8';
      profile1.Max_Hours_Day_1__c = '8';
      profile1.Max_Hours_Day_1__c = '8';
      profile1.Max_Hours_Day_1__c = '8';
      profile1.Max_Hours_Day_1__c = '8';
      profile1.Max_Hours_per_Week__c = '40';
      profile1.First_Day_of_Week__c = 'Sunday';
      profile1.Lunch_Time_Start__c = DateTime.newInstance(system.today().year(), 1, 31, 10, 0, 00);
      profile1.Lunch_Time_duration__c = '60';
      If(isInsert)
        insert profile1;
      return profile1;
    }
    
    // Time_Entry_Matrix__c
    public static Time_Entry_Matrix__c TIMEMATRIX_Data(Boolean IsInsert, String type, SVMXC__Service_Group_Members__c tech,
                              String activityType, Organizational_Activities__c orgActivity) {
      Time_Entry_Matrix__c matrix = new Time_Entry_Matrix__c();
      matrix.Type__c = type;
      Integer Yr = system.today().Year();
      Integer Mon = system.today().Month();
      Integer Dat = system.today().Day();
      matrix.Day_1_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
      matrix.Day_2_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+1, 11, 0, 0);
      matrix.Day_3_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+2, 11, 0, 0);
      matrix.Day_4_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+3, 11, 0, 0);
      matrix.Day_5_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+4, 11, 0, 0);
      matrix.Day_6_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+5, 11, 0, 0);
      matrix.Day_7_Start_Date_Time__c = Datetime.newInstance(Yr, Mon, Dat+6, 11, 0, 0);
      matrix.Day_1_Hours__c = 4.0;
      matrix.Day_2_Hours__c = 4.0;
      matrix.Day_3_Hours__c = 4.0;
      matrix.Day_4_Hours__c = 4.0;
      matrix.Day_5_Hours__c = 4.0;
      matrix.Day_6_Hours__c = 4.0;
      matrix.Day_7_Hours__c = 4.0;
      matrix.Technician__c = tech.Id;
      matrix.Activity_Type__c = activityType;
      matrix.Organizational_Activity__c = orgActivity.Id;
      matrix.Period_Start_Date__c = system.today();
      matrix.Period_End_Date__c = system.today().addDays(7);
      //matrix.Technician__r.User__c = UserInfo.getUserId();
      if(IsInsert)
        insert matrix;
      return matrix;
    }
    
    // Organization Profile
    public static Organizational_Activities__c OrgActivity_Data(Boolean isInsert, Timecard_Profile__c card, String type, String actName) {
      Organizational_Activities__c act = new Organizational_Activities__c();
      act.Indirect_Hours__c = card.Id;
      act.Type__c = type;
      act.Round_the_clock__c = true;
      act.Name = actName;
      if(isInsert)
        insert act;
      return act;
    }
    
    // Test Record for Document
    public static Document Doc_Data() {
      Document document = new Document();
    document.Body = Blob.valueOf('Some Text');
    document.ContentType = 'application/pdf';
    document.DeveloperName = 'TEST_tag';
    document.IsPublic = true;
    document.Name = 'My Document TEST';
    document.FolderId = [select id from folder where Type = 'Document' Limit 1].id;
    insert document;
    return document;
    }
    
    public static Organizational_Calendar__c OrgCalData(Boolean isInsert) {
      Organizational_Calendar__c orgCal = new Organizational_Calendar__c();
      orgCal.Calendar_Description__c = 'Austria';
      orgCal.Calendar_Year__c = String.valueOf(system.today().Year());
      orgCal.First_Day_of_the_Calendar_Year__c = Date.newInstance(2016, 31, 1);
      orgCal.Expire_Day_of_the_Calendar_Year__c = Date.newInstance(2016, 31, 12);
      if(isInsert)
        insert orgCal;
      return orgCal;
    }
    
    public static Organizational_Holiday__c OrgHoliday(Boolean isInsert, Organizational_Calendar__c cal) {
      Organizational_Holiday__c hol = new Organizational_Holiday__c();
      hol.Holiday_Date__c = system.today().addDays(20);
      hol.Organizational_Calendar__c = cal.Id;
      if(isInsert)
        insert hol;
      return hol;
    }
    
    public static Organizational_Activities__c OrgActivity_Data(Boolean isINsert, Timecard_Profile__c TC) {
      Organizational_Activities__c OrgA = new Organizational_Activities__c();
      OrgA.Name = 'Test Lunch';
      OrgA.Type__c = 'Indirect';
      OrgA.Round_the_clock__c = false;
      OrgA.Unpaid_Time__c = true;
      OrgA.Recuperative_Time__c = false;
      if(isInsert)
        insert OrgA;
      return OrgA;
    }
    
    public static Sales_Order_Partner__c SOPartner_Data(Boolean isInsert, Sales_Order__c SO, Sales_Order_Item__c SOI, Contact cont, ERP_Partner__c erpPartner) {
      Sales_Order_Partner__c partner = new Sales_Order_Partner__c();
      partner.Sales_Order_Number__c = SO.Name;
      partner.Sales_Order_Item_Number__c = SOI.Name;
      partner.Role__c = 'PM-Project Manager';
      partner.Contact__c = cont.Id;
      partner.ERP_Reference__c = SO.Name + '-' + 'PM' + cont.ERP_Reference__c;
      partner.ERP_Contact_Number__c = cont.ERP_Reference__c;
      partner.ERP_Partner_Code__c = erpPartner.Name;
      partner.Name = SO.Name + '-' + 'PM' + cont.ERP_Reference__c;
      if(isInsert)
        insert partner;
      return partner;
    }
}
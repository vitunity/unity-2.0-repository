/**
 * @description This class used to send real time updates Blocked items and market clearance objects to BMI data tables
 * @author Chandramouli/krishna katve
 */
public class UpdateBMIDataTable implements Queueable, Database.AllowsCallouts {

    private String objectName;
    private String tableName;
    private String fieldValue;
    private String fieldName;
    private BigMachinesTransactions bmiTxn;
    private List<sObject> sfRecords;
    
    public UpdateBMIDataTable(String objectName, String tableName, String fieldName, String fieldValue, List<sObject> sfRecords){
        system.debug('***cm inside UpdateBMIDataTable');
        this.objectName = objectName;
        this.tableName = tableName;
        this.fieldValue = fieldValue;
        this.fieldName = fieldName;
        this.sfRecords = sfRecords;
        system.debug('***before');
        bmiTxn = new BigMachinesTransactions();
        system.debug('***after');
    }
    
    public void execute(QueueableContext context) {
        String sessionId = bmiTxn.getSessionId();
        
        System.debug('--sfRecords'+sfRecords);
        
        if(!sfRecords.isEmpty()){
            bmiTxn.sendDeleteTableRequest(tableName, fieldName, sessionId, fieldValue);  
            bmiTxn.sendAddDatatableRequest(objectName, sessionId, sfRecords);
            bmiTxn.sendDeployTableRequest(tableName, sessionId);
        }else{
            if(fieldName == 'Country'){
                bmiTxn.sendDeleteTableRequest(tableName, fieldName, sessionId, fieldValue);  
                bmiTxn.sendDeployTableRequest(tableName, sessionId);
            }
        }
    }
}
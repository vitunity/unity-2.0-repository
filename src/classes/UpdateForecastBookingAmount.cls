global class UpdateForecastBookingAmount implements Database.Batchable<sObject>, Queueable{
    
    String query; 
    public UpdateForecastBookingAmount(String query){
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        System.debug(LoggingLevel.INFO,'-----'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<BigMachines__Quote_Product__c> qps = (List<BigMachines__Quote_Product__c>) scope;
        update qps;
    }

    global void finish(Database.BatchableContext BC){
    }
    
    public void execute(QueueableContext context) {
        Database.executeBatch(new UpdateForecastBookingAmount(query),500);
    }
}
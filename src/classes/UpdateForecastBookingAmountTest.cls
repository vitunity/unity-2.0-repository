@isTest(seeAllData=true)
private class UpdateForecastBookingAmountTest {
    static testMethod void testUpdateForecastBookingValue() {
        
        //Setup test data
        Account acct = TestUtils.getAccount();
        acct.Name = 'UPDATE BOOKING AMOUNT TEST';
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        acct.Super_Territory1__c = 'Americas';
        acct.Territories1__c = 'North America';
        insert acct;
     
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;
        
        
        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = acct.Id;
        opp.StageName = '0 - NEW LEAD';
        opp.Primary_Contact_Name__c = con.Id;
        opp.MGR_Forecast_Percentage__c = '';
        opp.Unified_Funding_Status__c = '20%';
        opp.Unified_Probability__c = '20%';
        opp.Net_Booking_Value__c = 200;
        opp.CloseDate = System.today();
        opp.Opportunity_Type__c = 'Sales';
        insert opp;
        
        
        BigMachines__Quote__c bq = new BigMachines__Quote__c();
        bq.Name = '2016-15108'+opp.Id;
        bq.BigMachines__Account__c = acct.Id;
        bq.BigMachines__Opportunity__c = opp.Id;
        bq.BigMachines__Is_Primary__c = true;
        bq.Order_Type__c = 'Sales';
        insert bq;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Product2 varianProduct = new Product2();
        varianProduct.Name = 'Sample Sales Product';
        varianProduct.IsActive = true;
        varianProduct.ProductCode = 'Test Code123';
        
        Product2 varianProduct1 = new Product2();
        varianProduct1.Name = 'Sample Sales Product1';
        varianProduct1.IsActive = true;
        varianProduct1.ProductCode = 'Test Code123';
        varianProduct1.Relevant_Non_Relevant__c = 'Not Relevant';
        insert new List<Product2>{varianProduct,varianProduct1};
        System.debug('---getQueries()5'+Limits.getQueries());
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = varianProduct.Id;
        pbEntry.Pricebook2Id = standardPricebookId;
        pbEntry.UnitPrice = 111;
        pbEntry.CurrencyISOCode = 'USD';
        pbEntry.IsActive = true;
        insert pbEntry;
        
         BigMachines__Quote_Product__c bqp = new BigMachines__Quote_Product__c(
            BigMachines__Quote__c = bq.Id,
            BigMachines__Product__c = varianProduct.Id,
            Header__c = true,
            Product_Type__c = 'Sales',
            BigMachines__Sales_Price__c = 40000,
            BigMachines__Quantity__c = 1,
            Line_Number__c= 12,
            EPOT_Section_Id__c = 'TEST',
            Net_Booking_Val__c = 1000,
            Booking_Value_V1__c = 2000,
            QP_Product_Family__c = 'System Solutions',
            QP_Product_Line__c = 'Advanced Treatment Delivery',
            QP_Product_Model__c = 'TrueBeam',
            Product_Line_Key__c = 'System SolutionsAdvanced Treatment Delivery'
        );
        
        BigMachines__Quote_Product__c bqp1 = new BigMachines__Quote_Product__c(
            BigMachines__Quote__c = bq.Id,
            BigMachines__Product__c = varianProduct1.Id,
            Header__c = true,
            Product_Type__c = 'Sales',
            BigMachines__Sales_Price__c = 40000,
            BigMachines__Quantity__c = 1,
            Line_Number__c= 12,
            EPOT_Section_Id__c = 'TEST',
            Net_Booking_Val__c = 1000,
            Booking_Value_V1__c = 2000,
            QP_Product_Family__c = 'System Solutions',
            QP_Product_Line__c = 'Advanced Treatment Delivery',
            QP_Product_Model__c = 'TrueBeam',
            Product_Line_Key__c = 'System SolutionsAdvanced Treatment Delivery'
        );
        insert new List<BigMachines__Quote_Product__c>{bqp,bqp1};
        
        Test.startTest();
            String query = 'SELECT Id,BigMachines__Product__r.Relevant_Non_Relevant__c,Net_Booking_Val__c,'
                          +' Booking_Value_V1__c,Forecast_Booking_Value__c'
                          +' FROM BigMachines__Quote_Product__c WHERE QP_Product_Family__c !=\'\' '
                          +' AND Product_Line_Key__c !=\'\' AND QP_Product_Model__c !=\'\' limit 1';
            UpdateForecastBookingAmount updateFCValue = new UpdateForecastBookingAmount(query );
            Database.executeBatch(updateFCValue);
        Test.stopTest();
        System.assertEquals(1000, [Select Id, Forecast_Booking_Value__c FROM BigMachines__Quote_Product__c WHERE Id=:bqp.Id][0].Forecast_Booking_Value__c);
        System.assertEquals(2000, [Select Id, Forecast_Booking_Value__c FROM BigMachines__Quote_Product__c WHERE Id=:bqp1.Id][0].Forecast_Booking_Value__c);
    }
}
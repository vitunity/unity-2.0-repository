global class UpdateSrvContactFlag implements  Database.Batchable<sObject>,Database.Stateful,Schedulable{
         
    global void execute(SchedulableContext sc)
    {
        UpdateSrvContactFlag b = new UpdateSrvContactFlag();
        database.executebatch(b);
    }    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    { 
        String query='';
        Date Dt1 = Date.Today();
        Date Dt2 = Date.Today()-1;
        system.debug('Dt1---'+Dt1);
        system.debug('Dt2---'+Dt2);
        //CM: Modified the below to fix defect DFCT0011595
        query = 'Select id, SVMXC__Start_Date__c, SVMXC__End_Date__c, Cancelled_At_Start__c, ERP_Order_Reason__c, SVMXC__Active__c from SVMXC__Service_Contract__c';
        query = query +' Where (SVMXC__End_Date__c < :dt1 and  SVMXC__Active__c = True) ';
        query = query +' OR (SVMXC__Start_Date__c  <= :dt1 and SVMXC__End_Date__c >= :dt1 and SVMXC__Active__c = false)';
        if(Test.isRunningTest())
        {
            query = query + ' LIMIT 100';
        }
            
        system.debug('Query---'+query);
        
        return Database.getQueryLocator(query) ;  
    }
    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Contract__c> SrvCon)
    {
        System.debug('SrvCon---'+SrvCon);
        system.debug('Srvcon.size()---'+Srvcon.size());
        List<SVMXC__Service_Contract__c> ContToUpdate = new List<SVMXC__Service_Contract__c>();
        for(SVMXC__Service_Contract__c SVMC : SrvCon)
        {
            //CM: Modified the below to fix defect DFCT0011595
            if(SVMC.SVMXC__Start_Date__c != Null && SVMC.SVMXC__End_Date__c != Null && SVMC.SVMXC__Start_Date__c <= Date.Today() && SVMC.SVMXC__End_Date__c >= Date.Today() && SVMC.Cancelled_At_Start__c != True && SVMC.ERP_Order_Reason__c != 'Cancellation' && SVMC.SVMXC__Active__c != True)
            {
                SVMC.SVMXC__Active__c = True;
                ContToUpdate.add(SVMC);
            }
            //CM: Modified the below to fix defect DFCT0011595
            if(SVMC.SVMXC__End_Date__c != Null && SVMC.SVMXC__End_Date__c < Date.Today() && SVMC.SVMXC__Active__c != False)
            {
                SVMC.SVMXC__Active__c = False;
                ContToUpdate.add(SVMC);
            }
        }
        
        if(ContToUpdate.size() >0)
        {
            Update ContToUpdate;
        }
    
    }
   
    global void finish(Database.BatchableContext BC)
    {
    
    }
}
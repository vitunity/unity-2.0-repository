@isTest(seeAllData=true)
public class UpdateSrvContactFlagTest
{

    static testmethod void testUpdateSrvContactFlagBatch()
    {    

        Test.startTest();
        UpdateSrvContactFlag batchJob = new UpdateSrvContactFlag();
        ID batchProcessId = Database.executeBatch(batchJob, 200);

        //String jobid = System.schedule('ScheduleApexClassTest',CRON_EXP, new UpdateSrvContactFlag());

        Test.stopTest();
    }
}
global class UploadToBoxAccessHelper_WS{
    webservice static string checkPermission(string sLogid){
        String sResult = '0';

        /*List<PHI_Log__c> philstc = Ltng_BoxAccess.fetchPHILogRecord(sLogid);
        if(!philstc.isEmpty() && philstc[0].Case_Folder_Id__c==null && philstc[0].Folder_Id__c==null) {
            return sResult;
        }*/

        Boolean isValidUser = Ltng_BoxAccess.checkPermission(sLogid);
        if(isValidUser) {
            sResult = '1';
        }

        system.debug('sResult:'+sResult);
        return sResult;
    }
}
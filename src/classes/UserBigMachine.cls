public class UserBigMachine{

    public static void AddRemoveUserToGroup(map<Id,User> mapNew, map<Id,User> mapOld, string chatterGroup,string ProfileName){
        
        //Add user in group
        if(Check_UserBigMachine.RunOnce()){
        set<Id> setUsers = new set<Id>();
        Profile CProfile = [select Id from Profile where Name =: ProfileName limit 1];
        for(User u: mapNew.values()){
            if(((u.BigMachines__Provisioned__c == true && mapOld == null) ||
               (u.BigMachines__Provisioned__c == true && mapOld <> null && u.BigMachines__Provisioned__c <> mapOld.get(u.Id).BigMachines__Provisioned__c)) ||
               (u.profileid == CProfile.Id && mapOld <> null && u.profileid <> mapOld.get(u.Id).profileid))
            
            {
                setUsers.add(u.Id);
            }
        }
        
        if(setUsers.size()>0){
            AddUserToGroup(setUsers, chatterGroup);
        }
        
        //remove member from group
        set<Id> setRUsers = new set<Id>();
        for(User u: mapNew.values()){
            if((u.BigMachines__Provisioned__c == false && mapOld <> null && u.BigMachines__Provisioned__c <> mapOld.get(u.Id).BigMachines__Provisioned__c) ||
               (u.profileid <> CProfile.Id && mapOld <> null && CProfile.Id == mapOld.get(u.Id).profileid)){
                setRUsers.add(u.Id);
            }
        }
        
        if(setRUsers.size()>0){
            RemoveUserToGroup(setRUsers, chatterGroup);
        }
        
      }
    }
    @future
    public static void AddUserToGroup(Set<ID> Users, string chatterGroup) {
        try {
            List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
            Id cgID = [ Select Id FROM CollaborationGroup WHERE Name =: chatterGroup LIMIT 1 ].ID;
            for(ID uId : Users){                          
               cgm.add(new CollaborationGroupMember (CollaborationGroupId = cgID, MemberId = uId));   
            } 
            insert cgm;
        }
        catch (Exception ex) {
           // string [] toaddress= new String[]{'rakesh.basani@varian.com'};
          //  CommonUtilityClass.sendmail(toaddress,'Add Member Failure','Error : '+ex);
        }    
    }
    @future
    public static void RemoveUserToGroup(Set<ID> Users, string chatterGroup) {
        try {
            List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
            Id cgID = [ Select Id FROM CollaborationGroup WHERE Name =: chatterGroup LIMIT 1 ].ID;
            List<CollaborationGroupMember> LstmId = [select Id from CollaborationGroupMember where CollaborationGroupId =: cgID and  MemberId IN: Users];
            if(LstmId <> null && LstmId.size()>0)  
                delete LstmId;
        }
        catch (Exception ex) {
         //   string [] toaddress= new String[]{'rakesh.basani@varian.com'};
         //   CommonUtilityClass.sendmail(toaddress,'Add Member Failure','Error : '+ex);
        }    
    }  
}
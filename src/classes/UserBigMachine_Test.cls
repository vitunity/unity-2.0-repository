@isTest
Class UserBigMachine_Test
{

    private static testmethod void AddTest()
    {
      Profile p = [select id from profile where name='VMS Marketing - User'];
      Profile p2 = [select id from profile where name='System Administrator'];


      
      User u = new User(FirstName = 'test12',alias = 'stasndt',BigMachines__Provisioned__c = false, Subscribed_Products__c=true,email='standardusser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p2.Id, timezonesidkey='America/Los_Angeles', username='standar_eduser@test.com');
      
        insert u;
      
      u.ProfileId= p.Id;
      update u;
      
    }
     private static testmethod void RemoveTest()
    {
      Profile p = [select id from profile where name='VMS Marketing - User'];
      Profile p2 = [select id from profile where name='System Administrator'];


      
      User u = new User(FirstName = 'test12',alias = 'stasndt',BigMachines__Provisioned__c = false, Subscribed_Products__c=true,email='standardusser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', username='standar_eduser@test.com');
      
        insert u;
      
      u.ProfileId= p2.Id;
      update u;
      
    }
    
  
}
@RestResource(urlMapping='/UserDetails/*')
    global class UserDetails
    {
    @HttpGet
    global static List<User> getUserDetails() 
    {
     system.debug('startingupdate user method');
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;

    res.addHeader('Access-Control-Allow-Origin','*');

    res.addHeader('Content-Type', 'application/json');

     res.addHeader('Accept', 'application/json');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
      res.addHeader('Access-Control-Allow-Headers','GET, POST, PUT, DELETE');
     res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');

    String Id= RestContext.request.params.get('UserId') ;
    List<User> Users= [SELECT Id,Name,Email,Username,CommunityNickname FROM User where Id=:Id];
    return Users;
    }
    @HttpPost
    global static string updateUser(List<InputData> request) {
 RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;

    res.addHeader('Access-Control-Allow-Origin','*');

    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Credentials','true');
res.addHeader('Access-Control-Allow-Headers','x-requested-with, Authorization');
     res.addHeader('Accept', 'application/json');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
      res.addHeader('Access-Control-Allow-Headers','GET, POST, PUT, DELETE');
     res.addHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,POST,PUT,DELETE');
       try {
       system.debug('startingupdate user method'+request);
     if(!(request.Size() > 0))
     throw new CustomException('Input should not be empty'); 
     for(InputData data : request) {
     User UserDetail= [SELECT Id,FirstName,LastName,Email,Username FROM User where Id=:data.userId];
    UserDetail.FirstName=data.FirstName;
    UserDetail.LastName=data.LastName;    
    Update UserDetail;
        // return   UserDetail;
    }
          return 'User details Updated';
    }
    catch(Exception e) {
           return null ;//e.getMessage();
        }
        }
    global Class InputData{
    webService String userId { get; set; } 
    webService String FirstName { get; set; } 
    webService String LastName { get; set; }
    }
     public Class CustomException extends Exception {}  
    }
@RestResource(urlMapping='/UserDetailsUpdate/*')
    global class UserDetailsUpdate
    {
    @HttpGet
    global static List<User> getUserDetails() 
    {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
   /* res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
   res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH');*/
   res.addHeader('Access-Control-Allow-Origin','*');
    res.addHeader('Cache-Control','no-cache');
    res.addHeader('Access-Control-Max-Age','1728000');
     res.addHeader('Accept', '*');
     res.addHeader('Content-Type','application/json');
      res.addHeader('Accept','application/json');
     res.addHeader('Access-Control-Allow-Methods','GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
     String ID = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    //String Id= RestContext.request.params.get('UserId') ;
    List<User> Users= [SELECT Id,Name,Username,CommunityNickname FROM User];
    return Users;
    }
    @HttpPost
    global static String updateUser(List<InputData> request) {
       try {
     if(!(request.Size() > 0))
     throw new CustomException('Input should not be empty'); 
     for(InputData data : request) {
     User UserDetail= [SELECT Id,FirstName,LastName,Username FROM User where Id=:data.userId];
    UserDetail.FirstName=data.FirstName;
    UserDetail.LastName=data.LastName;    
    Update UserDetail;
    }
    return 'User details Updated';
    }
    catch(Exception e) {
           return e.getMessage();
        }
        }
    global Class InputData{
    webService String userId { get; set; } 
    webService String FirstName { get; set; } 
    webService String LastName { get; set; }
    }
     public Class CustomException extends Exception {}  
    }
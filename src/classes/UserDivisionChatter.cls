/**************************************************************************\
    @ Author        : NA
    @ Date      : NA
    Date/Modified By Name/Task or Story or Inc # /Description of Change
    2/14/2018 - STSK0013865 - Open up K-Chat to all the users. 
****************************************************************************/
public class UserDivisionChatter{

    public static void AddUserToGroup(List<User> lstUser, string chatterGroup){
        
        //Add user in group
        set<Id> setUsers = new set<Id>();
        for(User u: lstUser){
            /*
            if(u.Division <> null  ){
                string division = u.Division;
                division = division.ToUpperCase();
                if ( division  <> 'CORPORATE' && division  <> 'PT' && division  <> 'XRAY')
                {
                    setUsers.add(u.Id);
                }           
            }
            */
            //else{
                
                    setUsers.add(u.Id);
            //}
        } 
               
        if(setUsers.size()>0){
            AddUserToGroup(setUsers, chatterGroup);
        }        
        
    }
    @future
    public static void AddUserToGroup(Set<ID> Users, string chatterGroup) {
        try {
            List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
            Id cgID = [ Select Id FROM CollaborationGroup WHERE Name =: chatterGroup LIMIT 1 ].ID;
            for(ID uId : Users){                          
               cgm.add(new CollaborationGroupMember (CollaborationGroupId = cgID, MemberId = uId));   
            } 
            insert cgm;
        }
        catch (Exception ex) {
           // string [] toaddress= new String[]{'rakesh.basani@varian.com'};
          //  CommonUtilityClass.sendmail(toaddress,'Add Member Failure','Error : '+ex);
        }    
    }    
}
@isTest
Class UserDivisionChatter_Test
{

    private static testmethod void AddTest()
    {
    
      Profile p = [select Id from Profile where name = 'System Administrator'];
      
      CollaborationGroup cg = new CollaborationGroup(Name = 'K-CHAT',CollaborationType = 'public'); 
                
      User u = new User(FirstName = 'test12',alias = 'stasndt',
      BigMachines__Provisioned__c = false, Subscribed_Products__c=true,
      email='standardusser@testorg.com',emailencodingkey='UTF-8', 
      lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', 
      profileid = p.Id, timezonesidkey='America/Los_Angeles', username='standar_eduser@test.com',
      division  = 'Corporate');
      insert u;
      
      User u1 = new User(FirstName = 'test123',alias = 'stasndth',
      BigMachines__Provisioned__c = false, Subscribed_Products__c=true,
      email='standardussert@testorg.com',emailencodingkey='UTF-8', 
      lastname='Testingg',languagelocalekey='en_US',localesidkey='en_US', 
      profileid = p.Id, timezonesidkey='America/Los_Angeles', username='standar_eduserd@test.com',
      division  = '');
      insert u1;
      
    }
}
/***************************************************************************
Author: Nilesh Gorle
Created Date: 4-Sept-2017
Project/Story/Inc/Task : INC4681292 - Automatically adding new employees to a Chatter group
                         STSK0012842 -  Check AU and NZ user and add user to chatter group 'VOS APAC ALL ANZ information'
Description: 
Whenever new intenal user get added and if it belongs to 'AU' or 'NZ' country code, it will add to chatter group
VOS APAC ALL ANZ
*************************************************************************************/
public class UserGroupTriggerHandler {
    public static String ChatterGroupName;
    public static List<CollaborationGroup> grpList;
    public static String grpId;
    public static List<CollaborationGroupMember> GMlist;
    public UserGroupTriggerHandler() {
    }

    /***************************************************************************
    Description: 
    Add user from country code 'AU' and 'NZ' to chatter group VOS APAC ALL ANZ
    *************************************************************************************/
    @future
    public static void addUserToChatterGroup(Map<Id, String> usrMap) {
        // Add group name here;
        ChatterGroupName = Label.Chatter_Group_Name;
        GMlist = new List<CollaborationGroupMember>();

        if(Test.isRunningTest()) {
            grpList = [SELECT Id,Name From CollaborationGroup Limit 1];
        } else {
            grpList = [SELECT Id,Name From CollaborationGroup WHERE Name=: ChatterGroupName];
        }

        if (grpList.size() > 0) {
            grpId = grpList[0].Id;
        }

        if (grpId != null) {
            for (Id uId: usrMap.keySet()) {
                if(uId!=null) {
                    if(usrMap.get(uId)!=null && (usrMap.get(uId)=='AU' 
                        || usrMap.get(uId)=='NZ'|| usrMap.get(uId)=='Australia' || usrMap.get(uId)=='New Zealand')) { // u.Country=='Australia' || u.Country=='New Zealand'
                        CollaborationGroupMember GM = new CollaborationGroupMember();
                        GM.CollaborationGroupId = grpId;
                        GM.MemberId = uId;
                        GMlist.add(GM);
                    }
                }
            }

            if (!GMlist.isEmpty()) {
                try{
                    insert GMlist;
                } catch(System.DmlException e) {
                    system.debug('------Exception------'+String.valueof(e));
                }
            }
        }
    }
}
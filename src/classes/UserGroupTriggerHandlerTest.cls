@isTest
public class UserGroupTriggerHandlerTest {
    
    public static Account acc;
    public static Contact con;
    public static User usr;
    public static Profile portalProf;
    
    static testMethod void testAddUserToChatterGroup() {
        test.startTest();
        CollaborationGroup myGroup = new CollaborationGroup();
        myGroup.Name='Test1';
        myGroup.CollaborationType='Public';
        insert myGroup;
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User newusr = new User(LastName = 'Test',FirstName='User',Alias = 'test',Email = 'test@unity.com',
                            Username = 'test.user@unity.com',ProfileId = prof.id,
                            TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US', Country='AU');
        insert newusr;
        User newusr1 = new User(LastName = 'Test1',FirstName='User1',Alias = 'test',Email = 'test1@unity.com',
                            Username = 'test.user1@unity.com',ProfileId = prof.id,
                            TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US', Country='NZ');
        insert newusr1;

        Map<Id, String> userMap = new Map<Id, String>();
        userMap.put(newusr.Id, newusr.Country);
        userMap.put(newusr1.Id, newusr1.Country); 
        UserGroupTriggerHandler.addUserToChatterGroup(userMap);
        test.stopTest();
    }
    
    static testMethod void testUpdateRegionalSalesMgr() {
        test.startTest();
        CollaborationGroup myGroup = new CollaborationGroup();
        myGroup.Name='Test1';
        myGroup.CollaborationType='Public';
        insert myGroup;
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        
        User mgUser = new User(LastName = 'Manager',FirstName='Test',Alias = 'test',Email = 'testmanager@unity.com',
                            Username = 'test.manager@unity.com',ProfileId = prof.id,
                            TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US', Country='AU');
        insert mgUser;
        
        User newusr = new User(LastName = 'Test',FirstName='User',Alias = 'test',Email = 'test@unity.com',
                            Username = 'test.user@unity.com',ProfileId = prof.id,
                            TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US', Country='AU');
        insert newusr;
        User newusr1 = new User(LastName = 'Test1',FirstName='User1',Alias = 'test',Email = 'test1@unity.com',
                            Username = 'test.user1@unity.com',ProfileId = prof.id,
                            TimeZoneSidKey = 'GMT',LanguageLocaleKey = 'en_US',EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US', Country='NZ');
        insert newusr1;

        Map<ID,Territory_Team__c> newTerrMap = new Map<ID,Territory_Team__c>();
        List<User> userList = [SELECT id, name FROM User WHERE isActive = True and userType !='CsnOnly' and Id != :UserInfo.getUserId() LIMIT 10];        
        //create 2 territory Team records in different geographies for better code ceoverage
        Territory_Team__c tt = new Territory_Team__c();
        tt.Super_Territory__c = 'Americas';
        tt.Territory__c = 'North America';
        TT.OIS_SW__c = userList[0].id;
        TT.TPS_Sales__c = userList[0].id;
        tt.VSS_District_Manager__c = userList[0].id;
        tt.Zone_VP__c = userList[0].id;
        tt.HW_Install_District__c = 'hw district';
        tt.SW_Install_District__c = 'sw district';
        tt.Network_Specialist_Sales_Admin__c = userList[0].id;
        tt.VSS_Sales_Administrator__c = userList[0].id;
        tt.Brachy_Sales_Manager__c = userList[0].id;
        tt.Brachy_Sales_Administrator__c = userList[0].id;
        tt.Contract_HW__c = userList[0].id;
        tt.CUSTOMER_SUPPORT_MANAGER__c = userList[0].id;
        tt.FSR_Specialist__c = userList[0].id;
        tt.HW_Sys_Install_Mgr__c = userList[0].id;
        tt.SW_Install_Mgr__c = userList[0].id;
        tt.Training_Manager__c = userList[0].id;
        tt.SW_Upgrades__c = userList[0].id;
        tt.Region__c = 'Eastern Zone';
        tt.Sub_Region__c = 'Northern Region';
        tt.District__c = 'Bill Tom';
        tt.City__c = 'Newark';
        tt.Country__c = 'USA';
        tt.ISO_Country__c = 'US';
        tt.State__c = 'DE';
        tt.Zip__c = '17042';
        tt.CSS_District__c = 'U3H';
        tt.District_Manager__c = userList[0].Id;
        tt.District_Sales_Administrator__c = userList[1].Id;
        tt.Regional_Director__c = userList[2].Id;
        tt.Software_Strategic_VP__c = userList[3].Id;
        tt.Network_Specialist__c = userList[4].Id;
        tt.VPT_Regional_Sales_Director__c = userList[5].id;
        tt.VPT_Regional_Svc_Manager__c = userList[6].id;
        tt.VPT_Sales_Manager__c = userList[7].id;
        tt.VPT_Sr_Dir_Svc__c = userList[8].id;
        tt.VPT_Svc_Program_Manager__c = userList[9].id;
        tt.VPT_VP_Sales__c = userList[1].id;
    	tt.Software_Sales_Manager__c = userList[3].id;
        insert tt;
        tt.CSS_District__c = 'U2H';
        update tt;
        newTerrMap.put(tt.Id,tt);
        
        Account acc = new Account();
        acc.name = 'terr Acc';
        acc.BillingCity = 'Pune';
        acc.Ownerid = userInfo.getUserID();
        acc.Territory_Team__c = tt.id;
        acc.country__c ='India';
        acc.OwnerId  = newusr1.Id;
        acc.Exclude_from_Team_Owner_rules__c = false;
        acc.State_Province_Is_Not_Applicable__c=true;
        insert acc;

        newusr1.ManagerId = mgUser.Id;
        update newusr1;
        
        test.stopTest();
    }
    
    public static testMethod void testUpdateContactRecord() {
        test.startTest();
        portalProf = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' LIMIT 1];

        acc = new Account(Name = 'TestAprRel', State_Province_Is_Not_Applicable__c=true, OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;

		MarketingKitRole__c mkt = new MarketingKitRole__c();
        Product2 prod1 = new Product2();
        prod1.Name = 'Acuity';
        prod1.Product_Group__c ='ARIA';
        prod1.ProductCode = '12345';
        insert prod1;
        
        MarketingKitRole__c mktrole = new MarketingKitRole__c ();
        mktrole.Account__c = acc.Id;
        mktrole.Product__c = prod1.Id;
		Insert mktrole;
		
        con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = 'test@varian.com';
        con.AccountId = acc.Id;
        insert con;

        usr = new User();
        usr.FirstName = 'Bruce';
        usr.LastName = 'Wayne';
        usr.Alias = 'Batman';
        usr.email = 'bruce.wayne1c@zynga.com';
        usr.languagelocalekey = 'en_US';
        usr.localesidkey = 'en_US';
        usr.emailencodingkey = 'UTF-8';
        usr.timezonesidkey = 'America/Chicago';
        usr.username = 'bruce.wayne1c@wayneenterprises.com';
        usr.ProfileId = portalProf.Id;
        usr.isActive = true;
        usr.ContactId = con.Id;
        insert usr;
        List<User> usrList = new List<User>{usr};

        test.stopTest();
    }
  
  	public static testMethod void testUpdateUserRecord() {
        test.startTest();
        portalProf = [SELECT Id FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' LIMIT 1];

        acc = new Account(Name = 'TestAprRel', State_Province_Is_Not_Applicable__c=true, OMNI_Address1__c = 'address test1', OMNI_City__c = 'fremont', Country__c = 'USA', OMNI_Postal_Code__c = '93425');
        insert acc;
		
		MarketingKitRole__c mkt = new MarketingKitRole__c();
        Product2 prod1 = new Product2();
        prod1.Name = 'Acuity';
        prod1.Product_Group__c ='ARIA';
        prod1.ProductCode = '12345';
        insert prod1;
        
        MarketingKitRole__c mktrole = new MarketingKitRole__c ();
        mktrole.Account__c = acc.Id;
        mktrole.Product__c = prod1.Id;
		Insert mktrole;

        con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Test';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = 'test@varian.com';
        con.AccountId = acc.Id;
        insert con;

        usr = new User();
        usr.FirstName = 'Bruce';
        usr.LastName = 'Wayne';
        usr.Alias = 'Batman';
        usr.email = 'bruce.wayne1c@zynga.com';
        usr.languagelocalekey = 'en_US';
        usr.localesidkey = 'en_US';
        usr.emailencodingkey = 'UTF-8';
        usr.timezonesidkey = 'America/Chicago';
        usr.username = 'bruce.wayne1c@wayneenterprises.com';
        usr.ProfileId = portalProf.Id;
        usr.isActive = true;
        usr.ContactId = con.Id;
        insert usr;
        List<User> usrList = new List<User>{usr};

        CpOktaGroupIds__c okgrp = new CpOktaGroupIds__c();
        okgrp.Name = 'test okta group';
        okgrp.AppId__c = 'test';
        okgrp.Id__c = 'test Id';
        insert okgrp;

        test.stopTest();
    }
}
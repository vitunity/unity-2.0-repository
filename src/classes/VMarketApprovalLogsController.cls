public class VMarketApprovalLogsController
{
public vmarket_app__C app{get;set;}
public String appId{get;set;}
public List< VMarket_Approval_Log__c> logslist{get;set;}

public VMarketApprovalLogsController()
{
        appId  = ApexPages.CurrentPage().getparameters().get('appId');
        if(String.isNotBlank(appId))
        {
        app = [Select id,name, ApprovalStatus__c from vmarket_app__C where id=:appid][0];
        logslist = [Select id,name, Action__c, Information__c, Status__c, User_Type__c, vMarket_App__c,CreatedBy.Name,CreatedDate from VMarket_Approval_Log__c where vMarket_App__c=:appid Order By CreatedDate Desc];
        
        }
}

}
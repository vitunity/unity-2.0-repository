public class VMarketKeyGenerator
{


public String generateKey(vMarket_Developer_Stripe_Info__c dsi,boolean expired,boolean validation)
{

DateTime dt = System.Now();
VMarket_Secret_Key__C secret = VMarket_Secret_Key__C.getValues('secret');

String encryptedkey = '';

if(String.IsNotBlank(dsi.Stripe_Users_Name__c))
{

Blob data = Blob.valueOf('');


   if(validation) 
    {
        if(dsi.Initial_TimeStamp__c==null || dsi.Expiry_TimeStamp__c==null) return null;
        data = Blob.valueOf(String.valueof(dsi.Initial_TimeStamp__c).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dsi.Expiry_TimeStamp__c).reverse());
        //System.debug('**'+String.valueof(dsi.Initial_TimeStamp__c).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dsi.Expiry_TimeStamp__c).reverse());
    }
    else
    {     
     if(expired) data = Blob.valueOf(String.valueof(dt).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse());
     else data = Blob.valueOf(String.valueof(dt).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse());
     //System.debug('**'+String.valueof(dt).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse());
    }
    
    
  
    Blob exampleIv = Blob.valueOf(secret.key__C.reverse());
    Blob key = Blob.valueOf(secret.secret__c.reverse()); //Crypto.generateAesKey(128);
    System.debug('**key'+EncodingUtil.base64Encode(key));
    Blob encrypted = Crypto.encrypt('AES128', key, exampleIv, data);
    System.debug('**encryption'+EncodingUtil.base64Encode(encrypted));
    Blob hash = Crypto.generateDigest('MD5', encrypted);  
    System.debug('**hash'+EncodingUtil.base64Encode(hash));
    
    
    if(!validation)
    {
        if(expired)
        {
            dsi.Initial_TimeStamp__c = dt;
            dsi.Expiry_TimeStamp__c = dt.addDays(100);
        }
        else
        {
            dsi.Initial_TimeStamp__c = dt;
            if(dsi.Expiry_TimeStamp__c == null) dsi.Expiry_TimeStamp__c = dt.addDays(100);
        }
    //update dsi;    
    }
    
encryptedkey = EncodingUtil.base64Encode(hash);


return encryptedkey.reverse();
}
return null;
/*
Blob exampleIv = Blob.valueOf(secret.key__C.reverse());
Blob key = Crypto.generateAesKey(128);
Blob data = Blob.valueOf(String.valueof(dt).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse());
Blob encrypted = Crypto.encrypt('AES128', key, exampleIv, data);
Blob hash = Crypto.generateDigest('MD5', encrypted);

encryptedkey = EncodingUtil.base64Encode(hash);


return encryptedkey.reverse();

*/

/*Blob encrypted1 = Crypto.encrypt('AES128', key, exampleIv, data);

System.debug('**enc1'+EncodingUtil.base64Encode(encrypted));
System.debug('**enc2'+EncodingUtil.base64Encode(encrypted1));

Blob decrypted = Crypto.decrypt('AES128', key, exampleIv, encrypted);
String decryptedString = decrypted.toString();
System.assertEquals(String.valueof(dt).reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse(), decryptedString);


Blob targetBlob = Blob.valueOf('ExampleMD5String');
Blob hash1 = Crypto.generateDigest('MD5', targetBlob);
Blob hash2 = Crypto.generateDigest('MD5', targetBlob);
System.debug('**hash1'+EncodingUtil.base64Encode(encrypted));
System.debug('**hash2'+EncodingUtil.base64Encode(encrypted));

*/
/*if(String.IsNotBlank(dsi.Stripe_Users_Name__c))
{
   if(validation) 
    {
        if(dsi.Initial_TimeStamp__c==null || dsi.Expiry_TimeStamp__c==null) return null;
        encryptedkey = String.valueof(dsi.Initial_TimeStamp__c).reverse()+secret.key__C.reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dsi.Expiry_TimeStamp__c).reverse();
    }
    else encryptedkey = String.valueof(dt).reverse()+secret.key__C.reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dt.addDays(100)).reverse();
    
    //encryptedkey = EncodingUtil.base64Encode(Blob.valueof(encryptedkey));
    
    Blob key = Crypto.generateAesKey(128);
    //encryptedkey = EncodingUtil.base64Encode(Crypto.generateDigest('MD5',Blob.valueOf(encryptedkey)));
    //System.debug('@@@@@'+String.valueOf(Crypto.decryptWithManagedIV('AES128', key, data)));
   Blob data = Blob.valueOf(encryptedkey);
   encryptedkey = String.valueOf(Crypto.encryptWithManagedIV('AES128', key, data));
    
    
    //encryptedkey = EncodingUtil.base64Encode(Blob.valueof(encryptedkey.reverse()));
    //encryptedkey = EncodingUtil.convertToHex(Blob.valueof(encryptedkey.reverse()));
    
    if(!validation)
    {
        if(expired)
        {
            dsi.Initial_TimeStamp__c = dt;
            dsi.Expiry_TimeStamp__c = dt.addDays(100);
        }
        else
        {
            dsi.Initial_TimeStamp__c = dt;
            if(dsi.Expiry_TimeStamp__c == null) dsi.Expiry_TimeStamp__c = dt.addDays(100);
        }
        
        //update dsi;
    }
    
    System.debug('***'+EncodingUtil.base64Encode(Blob.valueof(encryptedkey.reverse())));
    return EncodingUtil.base64Encode(Blob.valueof(encryptedkey.reverse()));
}*/

}

public boolean validateKey(String key,vMarket_Developer_Stripe_Info__c dsi)
{
   String secretkey = generateKey(dsi,false,true);
    /*Blob key1 = Crypto.generateAesKey(128);    
    String secretkey = String.valueOf(Crypto.decryptWithManagedIV('AES128', key1,EncodingUtil.base64decode(key.reverse())));//generateKey(dsi,false,true);

    VMarket_Secret_Key__C secret = VMarket_Secret_Key__C.getValues('secret');
     String data = String.valueof(dsi.Initial_TimeStamp__c).reverse()+secret.key__C.reverse()+dsi.Stripe_Users_Name__c.reverse()+String.valueof(dsi.Expiry_TimeStamp__c).reverse();
    System.debug('**data'+data+'--'+secretkey);
     //Blob data = Blob.valueOf(encryptedkey);
     //encryptedkey = String.valueOf(Crypto.encryptWithManagedIV('AES128', key, data));
    //encryptedkey = EncodingUtil.base64Encode(Crypto.generateDigest('MD5',Blob.valueOf(encryptedkey)));*/
    


if(String.isblank(secretkey)) return false;
else return secretkey.equals(key);

}

}
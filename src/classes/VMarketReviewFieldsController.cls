/* Class Created by Abhishek K as Part of Custom App Review Process VMT-21 */
public class VMarketReviewFieldsController
{

public List<String> techfields {get;set;}
public List<SelectOption> valitems {get;set;}
public String selectedCountry {get;set;}
public List<String> regfields {get;set;}
public List<String> legalfields {get;set;}
public VMarketCountry__c con{get;set;}

public VMarketReviewFieldsController()
{
         valitems= new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = vMarket_App__c.Info_Access__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry pickListVal : ple)
        {
            valitems.add(new SelectOption(pickListVal.getLabel(),pickListVal.getLabel()));
        }
    
    selectedCountry = ApexPages.CurrentPage().getparameters().get('country');

    con = [Select VMarket_Regulatory_Visible_Fields__c, VMarket_Legal_Visible_Fields__c ,VMarket_Technical_Visible_Fields__c from VMarketCountry__c where code__C=:selectedCountry][0];
    
    techfields = con.VMarket_Technical_Visible_Fields__c.split(';');
    regfields = con.VMarket_Regulatory_Visible_Fields__c.split(';');
    legalfields = con.VMarket_Legal_Visible_Fields__c.split(';');
}

 public Boolean getIsApprovalAdmin() {
    List<VMarket_Reviewers__c> adminslist = [Select id,type__c,active__c,user__C from VMarket_Reviewers__c where active__c=true and user__C=:UserInfo.getUserId()];
    if(adminslist!=null && adminslist.size()>0 && adminslist[0].type__C.containsignorecase('Admin')) return true;
    else return false;
    }
/* Method Added by Abhishek K to update Visible Fields for Review Process */
    public PageReference change()
    {
    System.debug('----'+techfields);
    System.debug('----'+regfields);
    System.debug('----'+legalfields);
    
    String techfieldsUpdated = '';
    String regfieldsUpdated = '';
    String legalfieldsUpdated = '';
    
    for(String s : techfields)
    {
    techfieldsUpdated += s+';';
    }
    for(String s : regfields)
    {
    regfieldsUpdated += s+';';
    }
    for(String s : legalfields)
    {
    legalfieldsUpdated += s+';';
    }
    con.VMarket_Technical_Visible_Fields__c = techfieldsUpdated;
    con.VMarket_Regulatory_Visible_Fields__c = regfieldsUpdated;
    con.VMarket_Legal_Visible_Fields__c = legalfieldsUpdated;
    
    update con;
    
    return new PageReference('/apex/VMarketReviewFieldsCOnfiguration?country='+selectedCountry);
    }



}
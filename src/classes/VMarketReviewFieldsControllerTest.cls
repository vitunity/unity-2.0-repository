@isTest
public class VMarketReviewFieldsControllerTest {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app, app2;
    
    static vMarket_Listing__c listing;
    static VMarket_Reviewers__c reviewer;
    static VMarket_Approval_Log__c logs;
    
    
    static Account account;
    static Contact contact1;
    static Profile admin,portal;
    static User developer1, adminUsr1;
    static List<Id> reviewerIds;
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', true, admin.Id, null, 'Admin1');
        reviewerIds = new List<Id>();
        reviewerIds.add(userInfo.getUserId());
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        
        contact1 = vMarketDataUtility_Test.contact_data(account, true);
        
        developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact1.Id, 'Developer1');
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer1.Id);
        app.Info_Access__c='Logo; Banner';
        app.ApprovalStatus__c = '';
        insert app;
        
         VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US');
         us.VMarket_Regulatory_Visible_Fields__c='Logo; Banner';
         us.VMarket_Legal_Visible_Fields__c ='Logo; Banner';
         us.VMarket_Technical_Visible_Fields__c='Logo; Banner';
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK');
        insert uk;
        
        
         vMarketConfiguration config = new vMarketConfiguration ();
         List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
     System.debug('{{}}'+config.SUBMITTED_STATUS);
// VMarket_Approval_Status__c=config.SUBMITTED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
     countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));
     
     insert countyappsList;
        //listing = vMarketDataUtility_Test.createListingData(app, true);
        
        reviewer= new VMarket_Reviewers__c (countries__c='NL; SV; US',type__c='technical reviewer; regulatory reviewer; Admin; Privacy reviewer; Trade Reviewer',active__c=true,user__C=UserInfo.getUserId());
        insert reviewer;
        
        logs= new VMarket_Approval_Log__c(action__c='Email Sent',Information__c='Dear Test',status__c='In Progress',vMarket_App__c=app.id,User_Type__c='Admin');
        insert logs;
        
    }
    
    public static Approval.ProcessResult[] createApprovalRequests(String appId) {
        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        for (Id reviewerId : reviewerIds) {
            // Create an approval request for the account
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('ApprovalUID');
            req1.setObjectId(appId);
            // Submit on behalf of a specific submitter
            req1.setNextApproverIds(new List<Id>{reviewerId});
            // Submit the record to specific process and skip the criteria evaluation
            req1.setProcessDefinitionNameOrId('vMarketTestApproval');
            req1.setSkipEntryCriteria(true);
            requests.add(req1);
        }
        
        // Submit the approval request for the account
        Approval.ProcessResult[] result;// = Approval.process(requests);
        return result;
    }
    
      public static testMethod void VMarketReviewFieldsControllerTestMethod() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.VMarketReviewFieldsConfiguration')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('country', 'US');
        VMarketReviewFieldsController fieldReview = new VMarketReviewFieldsController();
        fieldReview.getIsApprovalAdmin();
        fieldReview.change();
          
        Test.stopTest();
    }
    
     public static testMethod void VMarketApprovalLogsControllerTestMethod() {
        Test.startTest();
        
        //Setting the appId param
        Test.setCurrentPageReference(new PageReference('Page.VMarketApprovalLogs')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        VMarketApprovalLogsController log= new VMarketApprovalLogsController();
          
        Test.stopTest();
    }
    
}
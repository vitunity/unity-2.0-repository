public class VMarketReviewerController
{

public String type{get;set;}
public boolean techreviewer{get;set;}
public boolean regreviewer{get;set;}

public VMarketReviewerController()
{
techreviewer = false;
regreviewer = false;
type  = ApexPages.CurrentPage().getparameters().get('type');
if(type.containsignorecase('tech')) techreviewer = true;
if(type.containsignorecase('reg')) regreviewer = true;
}
 
 public List<vMarket_App__c> getPendingAppApprovals()
    {
       if(techreviewer) return [Select Id, Technical_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Tech_Review__c ='In Progress' and Technical_Reviewer__c=:UserInfo.getUserId()];
       else return [Select Id, Regulatory_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Regulatory_Review__c ='In Progress' and Regulatory_Reviewer__c =:UserInfo.getUserId()];
    }
    
    public List<vMarket_App__c> getUnderReviewApps()
    {
        if(techreviewer) return [Select Id,Technical_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Tech_Review__c ='In Progress'];
       else return [Select Id,Regulatory_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Regulatory_Review__c ='In Progress'];
    }
    
    public List<vMarket_App__c> getInfoRequestedApps()
    {
        if(techreviewer) return [Select Id,Technical_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Tech_Review__c ='Info Requested'];
       else return [Select Id,Regulatory_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Regulatory_Review__c ='Info Requested'];
    }
    
    public List<vMarket_App__c> getPublishedApps()
    {
        if(techreviewer) return [Select Id,Technical_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Tech_Review__c ='No Objection'];
       else return [Select Id,Regulatory_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Regulatory_Review__c ='No Objection'];
    }
    
    public List<vMarket_App__c> getRejectedApps()
    {
        if(techreviewer) return [Select Id,Technical_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Tech_Review__c ='Rejected'];
       else return [Select Id,Regulatory_Reviewer__r.name, Date_of_Submission__c , Date_of_Info_requested__c ,Date_of_Approval_reuqest__c, Date_of_Approve_Reject__c ,Approval_Information_Required__c ,Owner.FirstName,Owner.Email, Name, ApprovalStatus__c,Regulatory_Review__c,VMarket_Regulatory_Feedback__c,Tech_Review__c,VMarket_Technical_Feedback__c, CreatedDate, Owner.Name from vMarket_App__c where Regulatory_Review__c ='Rejected'];
    }
    
    public Boolean getIsApprovalAdmin() 
    {
    if(techreviewer)
    {
        List<VMarket_Reviewers__c> reviewersList = [Select id,type__c,active__c,user__C from VMarket_Reviewers__c where active__c=true and type__C='technical reviewer' and user__C=:UserInfo.getUserId()];
        if(reviewersList!=null && reviewersList.size()>0) return true;
        else return false;
    }
    else
    {
        List<VMarket_Reviewers__c> reviewersList = [Select id,type__c,active__c,user__C from VMarket_Reviewers__c where active__c=true and type__C='regulatory reviewer' and user__C=:UserInfo.getUserId()];
        if(reviewersList!=null && reviewersList.size()>0) return true;
        else return false;
    }
    
    }


}
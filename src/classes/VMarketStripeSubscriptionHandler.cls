/* Class Created by Abhishek K as Part of Subscription VMT-26 for getting Webhooks Requests */
@RestResource(urlMapping='/VMarketSubscriptionApI') 
global with sharing class VMarketStripeSubscriptionHandler { 
   
    global VMarketStripeSubscriptionHandler(){
    system.debug('++++'+RestContext.request);} 
    
     @HttpGet
    global static String doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('*******'+req);

        return 'All Good';
    }
    
     @HttpPost
    global static String doPost() {
    RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        System.debug('*******headers'+req.headers);
        
        Map<String,String> reqmap = req.headers;
        for(String s :reqmap.keyset())
        {
        System.debug('-----'+s+'----'+reqmap.get(s));
        }
        
        // Find Subscription Based on the Id
        
        // Find Order Based on the Subscription
        
        // Create a Child Record based on the Data
        
        
        
        if(!reqmap.keyset().contains('Stripe-Signature')) return 'You are Not Authroized';

        Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(req.requestBody.toString());
        
        Map<String, Object> cObjMap1 = (Map<String, Object>) cObjMap.get('data');
        
        
        
        Map<String, Object> cObjMap2 = (Map<String, Object>) cObjMap1.get('object');
        String status = String.valueof(cObjMap2.get('paid'));
        List<Object> objlist = (List<Object>)( ((Map<String, Object>)cObjMap2.get('lines')).get('data'));
        String subscriptionId = String.valueof( ((Map<String, Object>)objlist.get(0)).get('id') );//(cObjMap2.get('id'));
        String customerId = String.valueof(cObjMap2.get('customer'));       
        String chargeId = String.valueof(cObjMap2.get('charge'));
        String invId = String.valueof(cObjMap2.get('id'));
        
        System.debug('*****&&&'+subscriptionId);
        // Find Subscription Based on the Id
        
        String webStatus = '';
        if(status.equals('true')) webStatus = 'Success';
        else webStatus = 'Failed';
        
        VMarket_WebHooks_Order__c weborder = new VMarket_WebHooks_Order__c(VMarket_Customer_Id__c =customerId, VMarket_Invoice_Id__c=invId,VMarket_Charge_Id__c=chargeId, VMarket_Subscription_Id__c =subscriptionId, VMarket_Full_Response__c =req.requestBody.toString(), VMarket_Processed__c=false, VMarket_Received_Date__c=System.Now(), VMarket_Subscription__c=true, VMarket_Status__c=webStatus);        
        insert weborder;
        
        /*List<VMarket_Subscriptions__c> subscriptions = [select id, VMarket_Stripe_Subscription_Id__c, VMarket_Stripe_Detail__c from VMarket_Subscriptions__c where VMarket_Stripe_Subscription_Id__c=:subscriptionId];
        
        VMarket_Subscriptions__c subscriptionItem = new VMarket_Subscriptions__c();
        
        if(subscriptions.size()>0) subscriptionItem = subscriptions[0];
        
        System.debug(subscriptionId+'**sub'+subscriptionItem);
        
        */
        
        // Find Order Based on the Subscription
        //vMarketOrderItem__c order = [Select id, VMarket_Subscriptions__c from vMarketOrderItem__c where VMarket_Subscriptions__c=:subscriptionItem.id];
        
        
        // Create a Child Record based on the Data
        
       /* 
        VMarket_Subscription_Payments__c payment = new VMarket_Subscription_Payments__c(VMarket_Invoice_Date__c=date.today(), vMarket_Order_Item__c =order.id,VMarket_Stripe_Customer_Id__c=customerId, VMarket_Stripe_Invoice_Id__c=invId, VMarket_Stripe_Payment_Id__c=chargeId, VMarket_Stripe_Payment_Status__c='Paid', VMarket_Stripe_Subscription_Id__c=subscriptionId);
        if(!status.equals('true')) payment.VMarket_Stripe_Payment_Status__c = 'Failed';
        insert payment;
        */
        
        
        //System.debug('&&&'+orderItems.size());
        /*if( order!=null )
        {
        if(status.equals('true')) order.Status__c = 'Success';
        else 
        {
        order.Status__c = 'PaymentFail';
        order.VMarket_ACH_Failure_Reason__c = req.requestBody.toString();
        }
        
        update order;
         }*/
         
        //List< vMarketOrderItem__c > orderItems = [Select id, Status__c, Charge_Id__c from vMarketOrderItem__c where Charge_Id__c=:chargeid and Status__c='Pending']; 
        return 'All Good';
    }
    
  
}
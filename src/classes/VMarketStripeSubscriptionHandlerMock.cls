@isTest
global with sharing class VMarketStripeSubscriptionHandlerMock implements HttpCalloutMock {
    
    /*public String resBody = '{Body={'+
              '"access_token": "sk_test_9YAb7QbJ2WrzFKYDhgjANxPf",'+
              '"livemode": false,'+
              '"refresh_token": "rt_B3LP2z6a3Rx4nwi6Efj9SKVb7MPx7rHdn5BECaehQfnMA2FX",'+
              '"token_type": "bearer",'+
              '"stripe_publishable_key": "pk_test_CKZzSGQyLGYwGdUoYwiUUT2E",'+
              '"stripe_user_id": "acct_1AhEhRAKEJ4l2FTF",'+
              '"scope": "read_write"'+
            '}, Code=200}';*/
    
      public static String resBody = '{"created": 1326853478,'+
    '"livemode": false,'+
    '"id": "evt_00000000000000",'+
    '"type": "invoice.payment_succeeded",'+
    '"object": "event",'+
    '"request": null,'+
    '"pending_webhooks": 1,'+
    '"api_version": "2017-08-15",'+
    '"data": {'+
    '"object": {'+
    '"id": "in_00000000000000",'+
    '"object": "invoice",'+
    '"amount_due": 50000,'+
    '"amount_paid": 0,'+
    '"amount_remaining": 50000,'+
    '"application_fee": null,'+
    '"attempt_count": 0,'+
    '"attempted": true,'+
    '"billing": "charge_automatically",'+
    '"charge": "_00000000000000",'+
    '"closed": true,'+
    '"currency": "usd",'+
    '"customer": "cus_00000000000000",'+
    '"date": 1525803162,'+
    '"description": null,'+
    '"discount": null,'+
    '"due_date": null,'+
    '"ending_balance": 0,'+
    '"forgiven": false,'+
    '"lines": {'+
    '"data": ['+
    '{'+
    '"id": "sub_CpEvYGt2vY6obW",'+
    '"object": "line_item",'+
    '"amount": 50000,'+
    '"currency": "usd",'+
    '"description": null,'+
    '"discountable": true,'+
    '"livemode": false,'+
    '"metadata": {'+
    '},'+
    '"period": {'+
    '"end": 1588962118,'+
    '"start": 1557339718'+
    '},'+
    '"plan": {'+
    '"id": "a5S0x0000008ivYEAQyearly",'+
    '"object": "plan",'+
    '"aggregate_usage": null,'+
    '"amount": 50000,'+
    '"billing_scheme": "per_unit",'+
    '"created": 1525802974,'+
    '"currency": "usd",'+
    '"interval": "month",'+
    '"interval_count": 12,'+
    '"livemode": false,'+
    '"metadata": {'+
    '},'+
    '"name": "a5S0x0000008ivYEAQyearly",'+
    '"nickname": null,'+
    '"product": "prod_CpEj80ohXANllu",'+
    '"statement_descriptor": null,'+
    '"tiers": null,'+
    '"tiers_mode": null,'+
    '"transform_usage": null,'+
    '"trial_period_days": null,'+
    '"usage_type": "licensed"'+
    '},'+
    '"proration": false,'+
    '"quantity": 1,'+
    '"subscription": null,'+
    '"subscription_item": "si_CpEvLsk5KyCnSG",'+
    '"type": "subscription"'+
    '}'+
    '],'+
    '"has_more": false,'+
    '"object": "list",'+
    '"url": "/v1/invoices/in_1CPaIoL6mRt8fBaVt9HXDlNF/lines"'+
    '},'+
    '"livemode": false,'+
    '"metadata": {'+
    '},'+
    '"next_payment_attempt": null,'+
    '"number": "7DE26F4-0004",'+
    '"paid": true,'+
    '"period_end": 1525803162,'+
    '"period_start": 1525803162,'+
    '"receipt_number": null,'+
    '"starting_balance": 0,'+
    '"statement_descriptor": null,'+
    '"subscription": "sub_00000000000000",'+
    '"subtotal": 50000,'+
    '"tax": null,'+
    '"tax_percent": null,'+
    '"total": 50000,'+
    '"webhooks_delivered_at": 1525803163'+
    '}'+
    '}}';
    
  // Implementing interface method
    global HttpResponse respond(HTTPRequest req) {
      // creating a fake response
      Httpresponse res = new Httpresponse();
      res.setBody(resBody);
      res.setStatusCode(200);
      return res;
    }
    
    global HttpRequest request() {
      // creating a fake response
      HttpRequest req = new HttpRequest();
      req.setBody(resBody);
      req.setHeader('Content-Type', 'application/json');
      return req;
    }
  
}
public class VMarket_Google_API
{

public Map<String,String>analyticsMap{get;set;}

public String fetchAnalytics(String access_token,String viewId,List<String> dimensionsList,List<String> metricsList,Integer maxresults,Date startdate,Date enddate)
{
        Map<String, String> requestBody = new Map<String, String>();
        String dimensions = '';
        for(String s:dimensionsList)
        {
        dimensions += s+',';
        }
        dimensions = dimensions.substring(0,dimensions.length()-1);
        String metrics = '';
        for(String s: metricsList)
        {
        metrics += s+',';
        }
        metrics = metrics.substring(0,metrics.length()-1);
        requestBody.put('ids', viewId); 
        requestBody.put('dimensions', dimensions);
        requestBody.put('start-date', String.valueof(startdate));
        requestBody.put('end-date',String.valueof(enddate));
        requestBody.put('metrics',metrics);
        requestBody.put('max-results',String.valueof(maxresults));
        
        String requestBodyURL = '?';
        for(String key : requestBody.keySet()) {
            requestBodyURL += key+ '=' + blankValue(requestBody.get(key),'')+ '&';
        }
        requestBodyURL = requestBodyURL.substring(0,requestBodyURL.length()-1);
        
        
        String URL = 'https://www.googleapis.com/analytics/v3/data/ga'+requestBodyURL;
        system.debug(' == URL == ' + URL);              
        HttpRequest request = new HttpRequest();
        request.setEndpoint(URL);
        request.setMethod('GET');
        String authorizationHeader = 'Bearer '+access_token;//ya29.GlyjBOoXLNwhW5FnbhMNwfH2Ke9PKgaDbFEQw91LD12Fxy0mwU1kbTVdUgDY0bDHhXoofMLgiBa-W1g7D6i6uoECTTWxHKimn2PEUWhW3gWD9TFQzw2GSEhSpmUDOw';
        request.setHeader('Authorization', authorizationHeader);
        system.debug(' == Request == ' + request);        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            return null;
        }
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        //system.debug(' == Response == ' + response);
        if(statusCode==200) 
        {
        system.debug(' == ResponseBody == ' + ResponseBody);
        return ResponseBody;
        }
        else return null;
        
        return null;
        }
        
        public String fetchAccessToken()
        {
        Map<String, String> requestBody = new Map<String, String>();        
        
        
        requestBody.put('client_secret','1eRT3UlQilG1rlI1eOen0Jd4');
        requestBody.put('grant_type','refresh_token');
        requestBody.put('refresh_token', '1/D2X2_uWf_HZDIuBs2H3NUEzt2wkmpwJo3aamS2SYNwGCcG-xsLLUOWIrcFTIFgKE'); 
        requestBody.put('client_id', '1004050598833-8t84gvh7oo4qjus2oscj7007bead1kvc.apps.googleusercontent.com');
        String requestBodyURL = '';//'?';
        for(String key : requestBody.keySet()) {
            requestBodyURL += EncodingUtil.urlEncode(key, 'UTF-8')+ '=' + EncodingUtil.urlEncode(blankValue(requestBody.get(key),''),'UTF-8')+ '&';
        }
        requestBodyURL = requestBodyURL.substring(0,requestBodyURL.length()-1);
        
        
        String URL = 'https://www.googleapis.com/oauth2/v4/token';//+requestBodyURL;
        //system.debug(' == URL == ' + URL);              
        HttpRequest request = new HttpRequest();
        request.setEndpoint(URL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        //request.setHeader('Content-Length', '100');
        request.setBody(requestBodyURL);
        //request.setTimeout(40000);
        //system.debug(' == Request == ' + requestBodyURL);        
        Http con = new Http();
        HttpResponse response = new HttpResponse();
        try {
            response = con.send(request);
        } catch(Exception e){
            
        }
        String responseBody = response.getBody();
        Integer statusCode = response.getStatusCode();
        /*system.debug(' == Response == ' + response);
        system.debug(' == ResponseBody == ' + ResponseBody);
        system.debug(' == statusCode == ' + statusCode);
        */
        if(statusCode==200 && String.isNotBlank(responseBody))
        {
        if(responseBody.contains('access_token'))
        {//System.debug('Contains');
           JSONParser parser = JSON.createParser(responseBody);
            while (parser.nextToken() != null) {//System.debug('token'+parser.nextToken());
                if (parser.getCurrentToken()== JSONToken.FIELD_NAME && parser.getText() == 'access_token') {    
                    parser.nextToken();                                 
//                    System.debug('Access Token--'+parser.getText());
                    return parser.getText();
                    //break;
                }
        }
        }
        else{
        return null;
        }
        }
        else{
        return null;
        }
        
        return null;
        }
        
        
        
   private static String blankValue(String s1, String s2) {
        if(s1 == '' || s1 == null)
            return s2;
        return s1;    
    }
    
    public void VMarket_Google_API()
    {
    analyticsMap = new Map<String,String>();
    }
    
    
    public void refresh()
    {
    analyticsMap = new Map<String,String>();
    String accesstoken = fetchAccessToken();
    System.debug(accesstoken);
    if(String.isNotBlank(accesstoken))
    {
    String analyticsData = fetchAnalytics(accesstoken,'ga:157329577',new List<String>{'ga:pagePath'},new List<String>{'ga:pageviews'},100,Date.today()-100,Date.today());
    if(analyticsData.contains('rows'))
        {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(analyticsData);
        List<Object> results = (List<Object>)result.get('rows');
        for(Object s : results)
        {
        List<Object> str = (List<Object>)s;
        analyticsMap.put(String.valueof(str[0]),String.valueof(str[1]));
        //System.debug(str[0]+'-'+str[1]);
        }
        
        }
    }
    
    }
    
}
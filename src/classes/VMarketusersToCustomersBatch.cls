public class VMarketusersToCustomersBatch implements Database.Batchable<sObject>, Database.Stateful {

    Public boolean checkRegion;
    Public boolean sendMail;
    public Set<String> regionsAndCountries;
    public List<User> usersToUpdated;

    public VMarketusersToCustomersBatch(boolean isRegion,List<String> allowedRegions,boolean mail)
    {
    checkRegion = isRegion;
    sendMail = mail;
    regionsAndCountries = new Set<String>();
    usersToUpdated = new List<User>();
    regionsAndCountries.addAll(allowedRegions);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) 
    {
    return Database.getQueryLocator('select account.Territories1__c, account.Country__c from Contact where Id in (select ContactId from user where IsActive=true and ContactId!=null and (vMarket_User_Role__c=null or vMarket_User_Role__c=\'\') )');
    }

    public void execute(Database.BatchableContext bc, List<Contact> records)
    {
    for(User u:[Select Id,Contact.Account.Country__c,Contact.Account.Territories1__c, vMarket_User_Role__c from User where ContactId IN :records])
        {
        System.debug('$$$$'+u.Contact.Account);
         System.debug('$$$$'+regionsAndCountries.contains(u.Contact.Account.Country__c));
        if((!checkRegion && regionsAndCountries.contains(u.Contact.Account.Country__c))||(checkRegion && regionsAndCountries.contains(u.Contact.Account.Territories1__c)) )
            {
            //u.vMarket_User_Role__c = Label.vMarket_Customer;
            usersToUpdated.add(u);
            }
        }    
    }    

    public void finish(Database.BatchableContext bc)
    {
    System.debug('@@@@Finish'+usersToUpdated);
        if(sendMail)
        {
            String body='Follwing are the Users which do not have VMarket Role and belong to Region '+regionsAndCountries;
            
            for(User u:usersToUpdated)
            {
                body += '<br>'+u.Id+'--'+u.Contact.Account.Country__c+'--'+u.Contact.Account.Territories1__c+'<br>';
            }
            
            vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>(),'Customers with No VMarket Role',body,'Abhishek');
        }
    }    

}
@isTest
public with sharing class VMarketusersToCustomersBatchTest {
    
    static Profile admin,portal;   
      static User customer1, customer2, developer1, ADMIN1;
      static Account account;
      static Contact contact;
      
      static {
          admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        account = vMarketDataUtility_Test.createAccount('test account', true);
        contact = vMarketDataUtility_Test.contact_data(account, true);
        contact.oktaid__c ='abhishekkolipe67936';
        update contact;
        customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact.Id, 'Customer1');
      }
      
    
    private static testMethod void VMarketusersToCustomersBatchTest() 
    {
      test.StartTest();
      customer1.vMarket_User_Role__c = null;
      insert customer1;
      VMarketusersToCustomersBatch vcb = new VMarketusersToCustomersBatch(false,new List<String>{'USA'},true);
      database.executebatch(vcb);
      test.Stoptest();
    }
    
    }
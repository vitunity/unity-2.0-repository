/*************************************************************************\
    @ Author        : Amit Kumar
    @ Date      : 17-June-2013
    @ Description   :  Test class for VR_CP_Registration class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(seeAllData = true)

Public class VR_CP_RegistrationTest
{
    //Test Method for VR_CP_Registration class sendNotification Method
    //Commented for May release
   /* public static testmethod void TestVR_CP_Registration1()   
    {  
        
        Test.StartTest();
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        {     
        
        User u;
        Account a;      
        Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
        
        VR_CP_Registration Reg1=new VR_CP_Registration();
        List<SelectOption> sopt = Reg1.getoptions();
          
        Reg1.login();
          
        Recordtype rt=[Select id,developername from Recordtype where developername='Site_Partner' and SobjectType='Account'];
      
        Account objAcc = new Account(name='test3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Canada',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST',RecordTypeId=rt.id); 
        insert objAcc ; 
        
        a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='12235',BillingCity='California',BillingCountry='Canada',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST',RecordTypeId=rt.id, Ext_Cust_Id__c = 'abc123test' ); 
        insert a;   
        
        Reg1.AcctId =a.id;
        Reg1.CustEmail='Test@test.com';

        
        Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email=Reg1.CustEmail, AccountId=a.Id,MailingCountry ='Canada',Account_admin__C = True, MailingState='testState'); 
               insert con;
               
        u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'); 

        insert u; // Inserting Portal User
       
 


        VR_CP_Registration Regs=new VR_CP_Registration();
            Regs.CustLname='test1';
            Regs.CustEmail='test@test.com';
            Regs.AcctName='test2';
            Regs.AcctZip='12235';
            //Regs.ResearchCheck=True;
            Regs.AcctCountry='Brazil';
            Regs.CustFname='testing';
            Regs.SendNotification('yes');
            
            Regs.AcctInfo();
            System.currentPageReference().getParameters().put('Request','Reqnewpass');
            System.currentPageReference().getParameters().put('Answerparam','testanswer');
                System.currentPageReference().getParameters().put('Emailidparam',Con.Email);
                System.currentPageReference().getParameters().put('PasswordParam','Wipro@123');
            VR_CP_Registration Regs1=new VR_CP_Registration();
            Regs1.forcaptcha='none';
            Regs1.CustLname='test1';
            Regs1.CustEmail='test@test.com';
            Regs1.AcctName='test';
            Regs1.AcctZip='232323';
            Regs1.AcctId = 'abc123test';
            Regs1.AcctInfo();
            //Regs1.ResearchCheck=True;
            Regs1.AcctCountry='Brazil';
            Regs1.CustFname='testing';
            Regs1.SendNotification('yes');
            Regs1.verify();
            Regs1.extractrecq();
            Regs.rerenderfunction();
         

            }
            Test.StopTest();    
        } */
        
        
        //Test Method for VR_CP_Registration class SendNewpassword Method
        
        public static testmethod void TestVR_CP_Registration2()   
        {  
            VR_CP_Registration Reg=new VR_CP_Registration();


            
            User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
            System.runAs ( thisUser )
            {     
            
                Test.StartTest();
                User u;
                Account a;      
                Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                
                Regulatory_Country__c reg2 = new Regulatory_Country__c();
                reg2.Name='test' ; 
                insert reg2 ;
                
                
                 Account objAcc = new Account(name='test3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12235',BillingCity='California',BillingCountry='TEST',BillingState='Washington',BillingStreet='xyx',Country__c= 'test'); 
                insert objAcc ; 
                
                a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc .id,Ship_To__c=objAcc .id,DBA__C='4665',Sold_to__c=objAcc .id,End_User__c=objAcc .id,Bill_To__c=objAcc .id,BillingPostalCode ='12235',BillingCity='California',BillingCountry='test',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST' ); 
                insert a;   
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='standarduser@testorg.com', AccountId=a.Id,MailingCountry ='test',Account_admin__C = True,MailingState='testState' ); 
                insert con;  
               
                Reg.Email='Test@test.com';
                Reg.username='standarduser@testorg.com';
                u = new User(); 
                u.alias = 'standt';
                u.email=Reg.Email;
                u.emailencodingkey='UTF-8'; 
                u.lastname='Testing';
                u.languagelocalekey='en_US';
                u.localesidkey='en_US';
                u.profileid = p.Id; 
                u.timezonesidkey='America/Los_Angeles'; 
                u.Contactid=con.id;  
                u.username='test_user@testorg.com';
                insert u;
                
                Reg.SendNewpassword();

                VR_CP_Registration Regs=new VR_CP_Registration();
                Regs.input='135';
                Regs.Char1='1';
                Regs.Char3='3';
                Regs.Char5='5';
                Regs.verify();  
                //Rakesh.5/16/2016.Start
                Regs.getoptions();
                Regs.Resetpass();
                Regs.getrecoveryqusoptions();
                Regs.Login();
                Regs.IsFakeResponse=True;
                regs.LogintoOkta();
                Regs.reset();
                //Rakesh.5/16/2016.End

                Test.StopTest();    
        }

}
        
        
        //Test Method for VR_CP_Registration class AcctInfo Method
        
        public static testmethod void TestVR_CP_Registration4()   
        {  
                User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
                System.runAs ( thisUser )
                {     
                Test.StartTest();
                
                
                 Account objAcc = new Account(name='test3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Canada',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST'); 
                insert objAcc ; 
               
                Account  act  = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc .id,Ship_To__c=objAcc .id,DBA__C='4665',Sold_to__c=objAcc .id,End_User__c=objAcc .id,Bill_To__c=objAcc .id,BillingPostalCode ='12235',BillingCity='California',BillingCountry='Canada',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST', Ext_Cust_Id__c = 'testAcct123'); 
                insert act  ;   
                
                Contact objCon=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=act.Id,MailingCountry ='Canada',Account_admin__C = true, MailingState='testState'); 
                insert objCon;
                  System.currentPageReference().getParameters().put('Request','Reqnewpass');
                 System.currentPageReference().getParameters().put('Answerparam','testanswer');
                System.currentPageReference().getParameters().put('Emailidparam',objCon.Email);
                System.currentPageReference().getParameters().put('PasswordParam','Wipro@123');
                VR_CP_Registration Regs=new VR_CP_Registration();
                
                
                Regs.CustEmail='test123@test.com';
                Regs.AcctName='test1'; 
                Regs.CustLname='test1';
                Regs.AcctZip='1223';
                //Regs.ResearchCheck=True;
                Regs.CustFname='testing';
                System.assertequals( Regs.CustLname,'test1');
                System.assertequals( Regs.CustEmail,'test123@test.com');
                Regs.AcctId = 'testAcct123';
                Regs.AcctInfo();
                Regs.SendNotification('yes');
                Regs.rerenderfunction();
                Regs.CheckLMSTabVisibility();
                
                
                 Regs.input='135';
                 Regs.Char1='1';
                 Regs.Char3='3';
                 Regs.Char5='5';
                 Regs.verify();
                 Regs.extractrecq();
                Test.StopTest();    
        }} 

        
        
        //Test Method for VR_CP_Registration class AcctInfo Method
        //Commented for May release
        /*public static testmethod void TestVR_CP_Registration5() 
        
        {  
            
        Test.StartTest();
        
                ApexPages.currentPage().getParameters().put('Request' , 'Reqnewpass');
                VR_CP_Registration Reg=new VR_CP_Registration();
                ApexPages.currentPage().getParameters().put('Request' , 'Login');
               
                VR_CP_Registration Reg1=new VR_CP_Registration();
                 ApexPages.currentPage().getParameters().put('Request' , 'Resetpass');
                VR_CP_Registration Regs=new VR_CP_Registration();
                
                Regs.CustLname='test1';
                Regs.CustEmail='test@test.com';
                Regs.AcctZip='1223';
                Regs.AcctName='test1'; 
                System.assertequals( Regs.CustLname,'test1');
                System.assertequals( Regs.CustEmail,'test@test.com');
                   Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                 Account objAcc = new Account(name='test3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12235',BillingCity='California',BillingCountry='TEST',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST'); 
                insert objAcc ; 
                
                Account  act  = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc .id,Ship_To__c=objAcc .id,DBA__C='4665',Sold_to__c=objAcc .id,End_User__c=objAcc .id,Bill_To__c=objAcc .id,BillingPostalCode ='12235',BillingCity='California',BillingCountry='TEST',BillingState='Washington',BillingStreet='xyx',Country__c= 'TEST' ); 
                insert act  ;   
                
                Contact objCon=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=act.Id,MailingCountry ='test',Account_admin__C = true,MailingState='testState',OktaId__c='00ub0oNGTSWTBKOLGLNR'); 
                insert objCon;*/
                //User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US',profileid = p.Id,timezonesidkey='America/Los_Angeles', Contactid=objCon.id, username='test__user1@testorg.com'/*,UserRoleid=r.id*/); 
            
                //insert u;       
                /*
                Regs.AcctInfo(); 
                Test.setCurrentPageReference(new PageReference('Page.VR_Registration_PG')); 
                System.currentPageReference().getParameters().put('recaptcha_challenge_field', 'testing');
                System.currentPageReference().getParameters().put('Answerparam','testanswer');
                System.currentPageReference().getParameters().put('Emailidparam',objCon.Email);
                System.currentPageReference().getParameters().put('PasswordParam','Wipro@123');
                System.currentPageReference().getParameters().put('Consentoryes','yes');
                
                
                Regs.randomNumber();
                Regs.input='123456';
                Regs.getChar1();
                Regs.getChar2();
                Regs.getChar3();
                Regs.getChar4();
                Regs.getChar5();
                Regs.getChar6();
                Regs.verify();
                
                Regs.login();
                Regs.extractrecq();
                Regs.Resetpass();
                
                Regs.reset();
                Regulatory_Country__c regcount=new Regulatory_Country__c();
                regcount.Name='India';
                regcount.SupportRegion__c='EMEA';
                Insert regcount;
                System.assertequals(regcount.Name,'India');
                System.assertequals( regcount.SupportRegion__c,'EMEA');
                Regs.getoptions();
                Regs.getrecoveryqusoptions();
               // Regs.LogintoOkta();
                Regs.IsFakeResponse=True;     
                Regs.LogintoOkta();
                Regs.IsFakeResponse=False;
                Regs.LogintoOkta();
              
               
                Test.StopTest();  
                
        }*/
        
       //Test Method for VR_CP_Registration class AcctInfo Method
       
        public static testmethod void TestVR_CP_Registrations() 
        
        {  
        Test.StartTest();
        
                VR_CP_Registration Regs=new VR_CP_Registration();
                Regs.AcctInfo(); 
                Test.StopTest();  
                
        }
        public static testmethod void TestVR_CP_Registrations6() 
        
        {  
        Test.StartTest();
        
                VR_CP_Registration Regs=new VR_CP_Registration();
            Regs.CustLname='test1';
            Regs.CustEmail='test@test.com';
            Regs.AcctName='test';
            Regs.AcctZip='232323';
              Regs.AcctInfo(); 
              
                Test.StopTest();  
                
        }  
         public static testmethod void TestVR_CP_Registrations7() 
        
        {  
        User thisUser = [ select Id from User where Id =:UserInfo.getUserId() ];
        System.runAs ( thisUser )
        { 
        Test.StartTest();
             
               VR_CP_Registration Regss=new VR_CP_Registration();
               List<SelectOption> Selopt = Regss.getRoles();
               List<SelectOption> Seloptt = Regss.getPreferredLanguage();
               List<SelectOption> Selo = Regss.getSalutation();
               List<SelectOption> Selop = Regss.getSpecialty();
               VR_CP_Registration Regs=new VR_CP_Registration();
                
                Regs.CustLname='test1';
                Regs.CustEmail='test@test.com';
                Regs.AcctZip='1223';
                Regs.AcctName='test1'; 
                System.assertequals( Regs.CustLname,'test1');
                System.assertequals( Regs.CustEmail,'test@test.com');
                Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
                
                Regulatory_Country__c reg1 = new Regulatory_Country__c();
                reg1.Name='test' ; 
                insert reg1 ;
                
                 Account objAcc = new Account(name='test3',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='12235',BillingCity='California',BillingCountry='test',BillingState='Washington',BillingStreet='xyx',Country__c= 'test'); 
                insert objAcc ; 
                
                Account  act  = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc .id,Ship_To__c=objAcc .id,DBA__C='4665',Sold_to__c=objAcc .id,End_User__c=objAcc .id,Bill_To__c=objAcc .id,BillingPostalCode ='12235',BillingCity='California',BillingCountry='test',BillingState='Washington',BillingStreet='xyx',Country__c= 'test' ); 
                insert act  ;   
                
                Contact objCon=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=act.Id,MailingCountry ='test',Account_admin__C = true,MailingState='testState',Email_Opt_in__c=true,How_Opted_In__c='testhow',Opt_In_Date__c=system.today()); 
                insert objCon;
                 Test.setCurrentPageReference(new PageReference('Page.VR_Registration_PG')); 
                System.currentPageReference().getParameters().put('recaptcha_challenge_field', 'testing');
                System.currentPageReference().getParameters().put('Answerparam','testanswer');
                System.currentPageReference().getParameters().put('Emailidparam',objCon.Email);
                System.currentPageReference().getParameters().put('PasswordParam','Wipro@123'); 
                   
               User u = new User(alias = 'standt', Subscribed_Products__c=true,email=objCon.Email,emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=objCon.id, username='test_user@testorg.com',isActive=True); 

                 insert u; // Inserting Portal User
                  Regs.AcctInfo(); 
                Regs.randomNumber();
                Regs.input='123456';
                Regs.getChar1();
                Regs.getChar2();
                Regs.getChar3();
                Regs.getChar4();
                Regs.getChar5();
                Regs.getChar6();
                 Regs.verify();
                 Regs.extractrecq();
                 Regs.CustEmail = objCon.Email;
                 System.RunAs(u)
                 {
                    Regs.checkUserExistsWithTheGivenEmail();
                    //pagereference pref = Regs.SendNotification('yes');
                 }
                Test.StopTest();  
               } 
        }  
     
        public static testmethod void TestVR_CP_Registrations8() 
        {  /*
                Test.StartTest();
                Account account;
                User managerUsr,contributorUsr,memberUsr,usr4,usr5;
                User adminUsr1,adminUsr2;
                Profile admin,portal;
                List<User> lstUserInsert = new List<User>();
                Contact contact1,contact2,contact3,contact4,contact5;
                admin = OCSUGC_TestUtility.getAdminProfile();
                portal = OCSUGC_TestUtility.getPortalProfile(); 
               lstUserInsert = new List<User>();   
               lstUserInsert.add( adminUsr1 = OCSUGC_TestUtility.createUser(false, admin.Id, '1'));
               lstUserInsert.add( adminUsr2 = OCSUGC_TestUtility.createUser(false, admin.Id, '2'));
               insert lstUserInsert;     
               account = OCSUGC_TestUtility.createAccount('test account', true);
               List<Contact> lstContactInsert = new List<Contact>();
               lstContactInsert.add(contact1 = OCSUGC_TestUtility.createContact(account.Id, false));
               lstContactInsert.add(contact2 = OCSUGC_TestUtility.createContact(account.Id, false));
               lstContactInsert.add(contact3 = OCSUGC_TestUtility.createContact(account.Id, false));
               lstContactInsert.add(contact4 = OCSUGC_TestUtility.createContact(account.Id, false));
               lstContactInsert.add(contact5 = OCSUGC_TestUtility.createContact(account.Id, false));
               insert lstContactInsert;
               System.runAs(adminUsr1) {  
                  lstUserInsert = new List<User>();             
                  managerUsr = OCSUGC_TestUtility.createUser(false, portal.Id, contact1.Id, '13',Label.OCSUGC_Varian_Employee_Community_Member);               
                  insert lstUserInsert;
                  EmailTemplate email = OCSUGC_TestUtility.createEmailTemplate('New_Account_Registration', 'New_Account_Registration', 'test', 'test body', 'test html value', true);
               } 
                PageReference pg = Page.VR_Registration_PG;
                pg.getParameters().put('regsrc','OCSUGCRequest');
                pg.getParameters().put('Emailidparam',contact1.Email);
                pg.getParameters().put('Lastnameparam','test last name');
                pg.getParameters().put('Answerparam','puneet test answer');
                pg.getParameters().put('PasswordParam','appirio@123');
                Test.setCurrentPage(pg);                  
                VR_CP_Registration Regs=new VR_CP_Registration();
                Regs.regsrc='OCSUGCRequest';
                Regs.getRoles();
                Regs.getSpecialty();
                Regs.Email = contact1.Email;
                Regs.CustEmail=managerUsr.Email; 
                Regs.checkUserExistsWithTheGivenEmail();
                Regs.SendNotification('test');
                Regs.getoptions();
                Regs.extractrecq();
                Test.StopTest();  
            */    
            EmailTemplate email = OCSUGC_TestUtility.createEmailTemplate('New_Account_Registration', 'New_Account_Registration', 'test', 'test body', 'test html value', true);
        }   

        
     }
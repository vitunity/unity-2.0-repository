/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 04-Apr-2013
    @ Description   : An Apex controller to display summary of a selected TrueBeam record.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class VR_CP_TBRecPage {

 public List<String> Picklistvalue {get; set;}
 public boolean acknowledge{get; set;}
 public boolean termsAccepted{get; set;}
 List<Contact> cont = new List<Contact>();
 //Public Transient List<Attachment > attachment {get;set;}
 public List<ContentVersion> attachment {get;set;}
 public String TBtype{get;set;}
 public List<Developer_Mode__c> lDevMode{get;set;}
  public boolean termsAcceptedLegal{get; set;}
 public boolean IsHeaderShow{get; set;}
 public string Shows{get;set;}
 public String Usrname{get;set;}
//Constructor

    public VR_CP_TBRecPage(){           
        //acknowledge = false;
        //Id tbId = Apexpages.currentPage().getParameters().get('Id');
        User un = [Select contactid,alias from user where id =: userinfo.getuserId() limit 1];
        shows = 'none';
        if(un.contactid == null)
        {
         shows = 'block';
         Usrname = un.alias;
        }
        TBtype = Apexpages.currentPage().getParameters().get('TBtype');
        if(TBtype=='Legal Agreement')
        {
         lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,(Select Title, Description, LastModifiedDate From Content__r) From Developer_Mode__c d where Type__c =: TBtype];
        System.debug('test----->');
        termsAcceptedLegal=false;
       
        }
        else
       {
        
        lDevMode = new List<Developer_Mode__c>();
        
        if(TBtype != null){
      
        termsAcceptedLegal=true;          
            attachment = new list<ContentVersion>();
            /*
            for(Developer_Mode__c dm : [Select d.Type__c, (Select Id, ParentId, Name, OwnerId, BodyLength, LastModifiedDate,
                        Description From Attachments) From Developer_Mode__c d where Type__c =: TBtype]){*/
            lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,(Select Title, Description, LastModifiedDate From Content__r) From Developer_Mode__c d where Type__c =: TBtype];
                        
            for(Developer_Mode__c dm : lDevMode)
            {             
                attachment.addAll(dm.Content__r);      
            }
            if (attachment.size() >0){
            
            }
        }else{ 
        termsAcceptedLegal=true;
            getPicklistvalue();
            List<User> userList = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];
            system.debug('%%%%%%%%%%%%%%%%%%'+userList);
            if(userList.size() > 0){
                cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
                system.debug('-------------->'+cont);
            }
            
            if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c){
                termsAccepted = true;
               
            }else{
                termsAccepted = false;
            }
        }
     }
     }
 
   public void getPicklistvalue(){
       Picklistvalue = new List<String>();
       Schema.Describefieldresult fieldresult = Developer_Mode__c.Type__c.getDescribe();
       List<Schema.Picklistentry> Pickvalue = fieldresult.getPicklistvalues();
       for(Schema.Picklistentry p : Pickvalue){
        Picklistvalue.add(p.getvalue());
       }
       //return Picklistvalue;
      }
      
      
      //register terms and conditions
      public pagereference registerAgreement(){
      pagereference pref;
         if(cont.size() > 0)
            cont[0].TrueBeam_Accepted__c = true;
         try{
                system.debug('inside tryyyy---------');
                update cont;
                pref=new pagereference('/apex/cptruebeam');
                system.debug('inside tryyyy-&&&&&&&&&&&-');
                pref.setredirect(true);
            }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
            }
         return pref;
        }
        
        /*
        public pagereference tb_dev_pg_display(){
        pagereference pref;
        pref=new pagereference('/apex/tbdev_pg');
        pref.setredirect(true);
        return pref;
        }
        */
        
       
    
     
 }
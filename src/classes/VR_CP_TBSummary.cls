/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 04-Apr-2013
    @ Description   : An Apex controller to display summary of a selected TrueBeam record.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class VR_CP_TBSummary{
 public map<String, List<Developer_Mode__c>> map_Title_Mc{get;set;}
 public Map<ID, List<ContentVersion>> map_Mc_ContentVer{get;set;}
 public List<String> Picklistvalue {get; set;}
 public boolean acknowledge{get; set;}
 public boolean termsAccepted{get; set;}
 List<Contact> cont = new List<Contact>();
 //Public Transient List<Attachment > attachment {get;set;}
 public List<ContentVersion> attachment {get;set;}
 public String TBtype{get;set;}
 public String RecID{get;set;}
 public List<Developer_Mode__c> lDevMode{get;set;}
 public String shows{get;set;}
 public String Usrname{get;set;}
 

//Constructor

    public VR_CP_TBSummary(){           
        //acknowledge = false;
        //Id tbId = Apexpages.currentPage().getParameters().get('Id');
        TBtype = Apexpages.currentPage().getParameters().get('TBtype');
        RecID = Apexpages.currentPage().getParameters().get('recid');
        
        lDevMode = new List<Developer_Mode__c>();
        shows = 'none';
        User un = [Select alias,ContactId from user where id =: UserInfo.getUserId() limit 1];
        if(un.contactId == null)
         {
             shows = 'block';
             Usrname = un.alias;
         }
        if(TBtype != null){          
           
            set<string> stContentdocid=new set<string>();
            map_Title_Mc = new Map<String, List<Developer_Mode__c>>();
            List<Developer_Mode__c> lMonteCarlo = [Select d.Type__c,Title__c,CreatedBy.name,Description__c,LastModifiedDate From Developer_Mode__c d where id=: recid and Type__c =: TBtype];
        for(Developer_Mode__c c : lMonteCarlo)
        {
            if(map_Title_Mc.containsKey(c.Title__c))
            {
                List<Developer_Mode__c> lMc = map_Title_Mc.get(c.Title__c);
                lMc.add(c);
                map_Title_Mc.remove(c.Title__c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
            else
            {
                List<Developer_Mode__c> lMc = new List<Developer_Mode__c>();
                lMc.add(c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
        
        }
            lDevMode  = [Select d.Type__c,Title__c,CreatedBy.name,Description__c,LastModifiedDate,(Select Title,ContentDocumentId,contentSize, Description, LastModifiedDate From Content__r order by VersionNumber Desc) From Developer_Mode__c d where id=: recid and Type__c =: TBtype];
             map_Mc_ContentVer = new Map<ID, List<ContentVersion>>();
           List<ContentVersion> lContentVersion = [SELECT ContentDocumentId,Description,ContentSize,CreatedDate,Id,Developer_Mode__c,Title,VersionNumber FROM ContentVersion where Developer_Mode__c != null  order by VersionNumber Desc];

       
        for(ContentVersion  c : lContentVersion)
        {
            
            If(!stContentdocid.contains(c.ContentDocumentId))
            {
                if(!map_Mc_ContentVer.containsKey(c.Developer_Mode__c))
                {
             
                    List<ContentVersion> lMc = new List<ContentVersion>();
                    lMc.add(c);
                    map_Mc_ContentVer.put(c.Developer_Mode__c, lMc);
                    System.debug('LMC1-->'+lMc);
                }
                else
                {  
                List<ContentVersion> lMc = map_Mc_ContentVer.get(c.Developer_Mode__c);
                lMc.add(c);
                map_Mc_ContentVer.remove(c.Developer_Mode__c);
                map_Mc_ContentVer.put(c.Developer_Mode__c, lMc);
                System.debug('LMC-->'+lMc);
                    
                }
                
                stContentdocid.add(c.ContentDocumentId);
            }
            
            
            
        
        }
        
        Set <String> sTitleKeys = new Set<String>();
        sTitleKeys = map_Title_Mc.keySet();
        
        for(String s : sTitleKeys)
        {
             attachment = new list<ContentVersion>();
           // List<ContentVersion> lContent = new List<ContentVersion>();
            List<Developer_Mode__c> lMonte = map_Title_Mc.get(s);
            if(lMonte != null)
            {
                for(Developer_Mode__c mc : lMonte)
                {
                    List<ContentVersion> lc = new List<Contentversion>();
                    lc = map_Mc_ContentVer.get(mc.id);
                    system.debug('Valid--->'+lc );
                    if(lc != null)
                        attachment.addAll(lc);
                }  
            }
        }
      }
    
    } 
 }
/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 19-June-2013
    @ Description   :  Test class for  VR_CP_TBSummary class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest(SeeAllData = true) 
    Public class VR_CP_TBSummarytest{
    
    //Test method for  VR_CP_TBSummary class
     
    static testmethod void testVR_CP_TBSummary()
        {
            Test.StartTest();
            
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
            
                Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
                
                User u;
                Account a;   
                 
                a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
                insert a;  
                
                Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test',MailingState ='teststate' ); 
                insert con;
                
               u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
               insert u; // Inserting Portal User
        
           
               PageReference pageref = Page.CpTbSummary;
                Test.setCurrentPage(pageRef); 
                Developer_Mode__c Devmode = new Developer_Mode__c();
                Devmode.Type__c= 'Manual';
                Devmode.Title__c = 'True Beam manual';
                Devmode.Description__c = 'This is the first manual test';
                insert Devmode ;
                ApexPages.currentPage().getParameters().put('TBtype','Manual');
                ApexPages.currentPage().getParameters().put('recid',Devmode.Id);
                VR_CP_TBSummary Tbsum = new VR_CP_TBSummary();
                
            }
            
            Test.StopTest();
        }
            
            
        }
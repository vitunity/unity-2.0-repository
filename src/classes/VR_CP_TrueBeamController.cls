/*************************************************************************\
    @ Author        : Harshita Sawlani
    @ Date      : 04-Apr-2013
    @ Description   : An Apex controller to display summary of a selected TrueBeam record.
    @ Last Modified By  :   Narmada Yelavaluri
    @ Last Modified On  :   27-May-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/
public class VR_CP_TrueBeamController {

 public List<String> Picklistvalue {get; set;}
 public List<String> options{get; set;} 
 public boolean acknowledge{get; set;}
 public boolean termsAccepted{get; set;}
  public string termsAccepted2{get; set;}

 public boolean IsProductTrueBeam{get; set;}
  public string display{get; set;}
public string IsVisible{get; set;}


 List<Contact> cont = new List<Contact>();
 //Public Transient List<Attachment > attachment {get;set;}
 public List<ContentVersion> attachment {get;set;}
 public String TBtype{get;set;}
 public String RecID{get;set;}
 public String msage{get;set;} 
 public List<Developer_Mode__c> lDevMode{get;set;}
  List<User> portalUser;
List<MarketingKitRole__c> markkit; 
public String shows{get;set;}
public String Usrname{get;set;}
//Constructor
//This Method is used for whether the term accepted page shown or not and whether the user will be able to see the true beam page or not on the basis of condition.
    public VR_CP_TrueBeamController(){
            shows = 'none';
                portalUser = [Select u.Id, u.ContactId, u.AccountId,u.alias From User u where id =: UserInfo.getUserId()  limit 1];
             system.debug('-------------->'+portalUser[0].AccountId);
          ISAgreementShow();
             if(portalUser.size() > 0)
             {
             if(portalUser[0].contactId == null)
              {
                display='none';
                IsVisible='block';
                shows = 'block';
                Usrname = portalUser[0].alias;
                system.debug('bug1');
               termsAccepted = true;
                getPicklistvalue();
              }
             
             String ProdPartNum= system.label.PartNumber;
             markkit= [select id,name,Product__r.Model_Part_Number__c from MarketingKitRole__c where Product__r.Model_Part_Number__c=:ProdPartNum and Account__r.id=:portalUser[0].AccountId limit 1];
             system.debug('-------------->'+markkit);
             if(markkit.size() > 0)
             {
                //acknowledge = false;
        //Id tbId = Apexpages.currentPage().getParameters().get('Id');
        TBtype = Apexpages.currentPage().getParameters().get('TBtype');
        
        lDevMode = new List<Developer_Mode__c>();
        
        if(TBtype != null)
        {          
            attachment = new list<ContentVersion>();
            lDevMode  = [Select d.Type__c,Title__c,LastModifiedDate,(Select Title, Description, LastModifiedDate From Content__r) From Developer_Mode__c d where Type__c =: TBtype];
                        
            for(Developer_Mode__c dm : lDevMode)
            {             
                attachment.addAll(dm.Content__r);      
            }
        }
        else
        { 
            getPicklistvalue();
            List<User> userList = [select id, ContactId,alias from User where id = : Userinfo.getUserId() limit 1];
            system.debug('%%%%%%%%%%%%%%%%%%'+userList);
            if(userList.size() > 0)
            {
                cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
                system.debug('-------------->'+cont);
            }
                       system.debug('%%%%%%%%%%%%%%%%%%'+cont[0].TrueBeam_Accepted__c);
                        system.debug('bug2');
          //  termsAccepted = true;

            if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c==true )
            {
            display='none';
            IsVisible='block';
            ////termsAccepted = false;

            }else
            {
              if(userList[0].contactId == null)
              {
                display='none';
                IsVisible='block';
                shows = 'block';
                Usrname = userList[0].alias;
            //    termsAccepted = true;
              }else{
            IsVisible='none';
            display='block';
            }
             system.debug('bug3');
         //   termsAccepted = true;
            }
            //IsProductTrueBeam=true;
        }
     }
     else
   {
      system.debug('------Working-------->');
      //IsProductTrueBeam=false;
    }
   }
 }  
 
                      
     //This Method is used to fetch the picklist values from  Developer_Mode__c  .           
   public void getPicklistvalue(){
       Picklistvalue = new List<String>();
       Schema.Describefieldresult fieldresult = Developer_Mode__c.Type__c.getDescribe();
       List<Schema.Picklistentry> Pickvalue = fieldresult.getPicklistvalues();
        List<SelectOption> options = new List<SelectOption>();
          options.add(new selectOption('Select','Select'));
       for(Schema.Picklistentry p : Pickvalue){
  // options.add(new selectOption(p.label,p.value));


        Picklistvalue.add(p.getvalue());
       }
       //return Picklistvalue;
      }
      public void ISAgreementShow()
      {

       List<User> userList = [select id, ContactId from User where id = : Userinfo.getUserId() limit 1];
            system.debug('%%%%%%%%%%%%%%%%%%'+userList);
            if(userList.size() > 0)
            {
                cont = [select id, TrueBeam_Accepted__c from Contact where id =: userList[0].ContactId limit 1];
                system.debug('-------------->uyguyhgbjhbjnbjbn'+cont);
            }
            
            if(cont.size() > 0 && cont[0].TrueBeam_Accepted__c==true)
            {
             system.debug('Test----->');
                termsAccepted = true;
               
            }
            else
            {system.debug('Test2----->');
                   
                termsAccepted = false;
            }
            //IsProductTrueBeam=true;
        }

      
      //register terms and conditions
      public pagereference registerAgreement(){
       system.debug('code run');
       
      pagereference pref;
         if(cont.size() > 0)
            cont[0].TrueBeam_Accepted__c = true;
         try{
        
                system.debug('inside tryyyy---------');
                update cont;
                publicgroupadd();
              
                pref=new pagereference('/apex/CpTrueBeam');
                system.debug('inside tryyyy-&&&&&&&&&&&-');
                pref.setredirect(true);
                
            }catch(Exception ex){
                Apexpages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()) );
            }
         return pref;
        }
        
        /*
        public pagereference tb_dev_pg_display(){
        pagereference pref;
        pref=new pagereference('/apex/tbdev_pg');
        pref.setredirect(true);
        return pref;
        }
        */
       @future
         public static void publicgroupadd(){
            Group grp = [Select id from group where developername ='Developer_Mode'];
                        GroupMember gm = new groupMember(GroupId = grp.id,UserOrGroupId = UserInfo.getUserId());
                        insert gm;
         } 
       
    
     
 }
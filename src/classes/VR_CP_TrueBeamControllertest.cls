/*************************************************************************\
    @ Author        : Narmada Yelavaluri
    @ Date      : 19-June-2013
    @ Description   :  Test class for VR_CP_TrueBeamController class.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@isTest
Public class VR_CP_TrueBeamControllertest{
    
     //Test Method for VR_CP_TrueBeamController class when TBType equals developermode type
     
    static testmethod void testVR_CP_TrueBeamController()
    {
        Test.StartTest();
        
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='teststate',passwordreset__c = true ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Developer_Mode__c Devmode = new Developer_Mode__c();
            Devmode.Type__c= 'Manual';
            Devmode.Title__c = 'True Beam manual';
            Devmode.Description__c = 'This is the first manual test';
            insert Devmode ;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            prod.Model_Part_Number__c= system.label.PartNumber;
            
            insert prod;
            
            MarketingKitRole__c mkt = new MarketingKitRole__c();
            mkt.Account__c = a.Id;
            mkt.Product__c = prod.Id;
            
            Insert mkt;
            
            
        }
        
        
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
        
        
        System.runAs (usr) {
        
            PageReference pageref = Page.cpTBRecPage;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('TBtype','Manual');
            VR_CP_TrueBeamController TrueBeam = new VR_CP_TrueBeamController();
            TrueBeam.registerAgreement();
            TrueBeam.ISAgreementShow();
            
         }
        Test.StopTest();
    }
    
    //Test Method for VR_CP_TrueBeamController class when TBType notequals developermode type
    
    static testmethod void testVR_CP_TrueBeamControllerepicklist()
    {
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;    
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', TrueBeam_Accepted__c = True,MailingState ='teststate'); 
            insert con;
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            
            Developer_Mode__c Devmode = new Developer_Mode__c();
            Devmode.Type__c= 'Manual';
            Devmode.Title__c = 'True Beam manual';
            Devmode.Description__c = 'This is the first manual test';
            insert Devmode ;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            prod.Model_Part_Number__c= system.label.PartNumber;
            
            insert prod;
            
            MarketingKitRole__c mkt = new MarketingKitRole__c();
            mkt.Account__c = a.Id;
            mkt.Product__c = prod.Id;
            
            Insert mkt;
            
        }
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
        
        
        System.runAs (usr) {
        
            PageReference pageref = Page.cpTBRecPage;
            Test.setCurrentPage(pageRef);
            VR_CP_TrueBeamController TrueBeam = new VR_CP_TrueBeamController();
            TrueBeam.registerAgreement();
            TrueBeam.ISAgreementShow();
        
        }
        
        Test.StopTest();
    }
    
    //Test Method for VR_CP_TrueBeamController class when contact is present
    
    static testmethod void testVR_CP_TrueBeamControllermkt()
    {
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            User u;
            Account a;   
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a;  
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState ='teststate' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            
            Developer_Mode__c Devmode = new Developer_Mode__c();
            Devmode.Type__c= 'Manual';
            Devmode.Title__c = 'True Beam manual';
            Devmode.Description__c = 'This is the first manual test';
            insert Devmode ;
            
            Product2 prod = new Product2();
            prod.Name = 'Acuity';
            prod.Product_Group__c ='ARIA';
            prod.Model_Part_Number__c= system.label.PartNumber;
            
            insert prod;
            
            
            MarketingKitRole__c mkt = new MarketingKitRole__c();
            mkt.Account__c = a.Id;
            mkt.Product__c = prod.Id;
            
            Insert mkt;
            
        
        }
        user usr = [Select Id, ContactId, Name from User where email=:'standarduser@testorg.com' ];
        
        
        System.runAs (usr) {
        
            PageReference pageref = Page.cpTBRecPage;
            Test.setCurrentPage(pageRef);
            VR_CP_TrueBeamController TrueBeam = new VR_CP_TrueBeamController();
            TrueBeam.registerAgreement();
            TrueBeam.getPicklistvalue();
        
        }
        
        Test.StopTest();
    } 
    
    
    //Test Method for VR_CP_TrueBeamController class when contact is not present
    
    static testmethod void testVR_CP_TrueBeamControllercontact()
    {
        Test.StartTest();
        PageReference pageref = Page.cpTBRecPage;
        Test.setCurrentPage(pageRef);
        VR_CP_TrueBeamController TrueBeam = new VR_CP_TrueBeamController();
        TrueBeam.registerAgreement();
        TrueBeam.getPicklistvalue();
        TrueBeam.ISAgreementShow();
        
        Test.StopTest();
    } 
}
/*@RestResource(urlMapping='/VUESSpeakers/*')
     global class VUESSpeakerController
      {
     @HttpGet
     global static List<VU_SpeakerSchedule_Map__c> getBlob()
     {
          List<VU_SpeakerSchedule_Map__c>a;
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
     String Id= RestContext.request.params.get('ScheduleId') ;
     String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
        a = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c,VU_Speakers__r.Language__c,VU_Speakers__r.Phone__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND (VU_Event_Schedule__r.Language__c='English(en)' OR VU_Event_Schedule__r.Language__c='' OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes ('English(en)'))];
      }else{
 a = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c,VU_Speakers__r.Language__c,VU_Speakers__r.Phone__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND (VU_Event_Schedule__r.Language__c=:Language OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes (:Language))];
 }      
     return a;
     }
   /*  @HttpPost
    global static List < VU_SpeakerSchedule_Map__c> getTreatment(List < InputData > request) {
        system.debug('testmethod start-----');
        RestRequest req = RestContext.request;
        RestResponse resp = RestContext.response;
        resp.addHeader('Access-Control-Allow-Origin', '*');
        resp.addHeader('Content-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Credentials', 'true');
        resp.addHeader('Access-Control-Allow-Headers', 'x-requested-with, Authorization');
        resp.addHeader('Accept-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
        resp.addHeader('Access-Control-Allow-Headers', 'POST');
        resp.addHeader('Access-Control-Allow-Methods', 'POST');
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        List < VU_SpeakerSchedule_Map__c> vu = new List < VU_SpeakerSchedule_Map__c> ();
        
       // if (!(request.Size() > 0)){
         //   throw new CustomException('Input should not be empty');
        for (InputData data: request) {
         String Id = data.ScheduleId;
            String Language = data.Language;
            vu = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND Languages__c =: Language];
            //system.debug('test--------' + a);
             
        }
        return vu; 
      //  }
      
        
      // else{}
       //else{
       
        
       // List < VU_Products__c > v = new List < VU_Products__c > ();
      /*VU_Products__c vx = new VU_Products__c();
        Vx.Error__c = 'Please provide valid Input';
        vu.add(vx);
        
        return vu;
        }
    }

    global Class InputData {
    
            WebService String Language {
            get;
            set;
        }
          WebService String ScheduleId {
            get;
            set;
        }
    }

    public Class CustomException extends Exception {} 
     }
     
     */
     
     
     @RestResource(urlMapping='/VUESSpeakers/*')
     global class VUESSpeakerController
      {
     @HttpGet
     global static List<VU_SpeakerSchedule_Map__c> getBlob()
     {
          List<VU_SpeakerSchedule_Map__c>a;
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
     String Id= RestContext.request.params.get('ScheduleId') ;
     String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' && Language !='German(de)'){
        a = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c,VU_Speakers__r.Language__c,VU_Speakers__r.Phone__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND (VU_Event_Schedule__r.Language__c='English(en)' OR VU_Event_Schedule__r.Language__c='' OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes ('English(en)'))];
      }else{
 a = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c,VU_Speakers__r.Language__c,VU_Speakers__r.Phone__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND (VU_Event_Schedule__r.Language__c=:Language OR VU_Event_Schedule__r.Event_Schedule_Accessibility__c includes (:Language))];
 }      
     return a;
     }
   /*  @HttpPost
    global static List < VU_SpeakerSchedule_Map__c> getTreatment(List < InputData > request) {
        system.debug('testmethod start-----');
        RestRequest req = RestContext.request;
        RestResponse resp = RestContext.response;
        resp.addHeader('Access-Control-Allow-Origin', '*');
        resp.addHeader('Content-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Credentials', 'true');
        resp.addHeader('Access-Control-Allow-Headers', 'x-requested-with, Authorization');
        resp.addHeader('Accept-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
        resp.addHeader('Access-Control-Allow-Headers', 'POST');
        resp.addHeader('Access-Control-Allow-Methods', 'POST');
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        List < VU_SpeakerSchedule_Map__c> vu = new List < VU_SpeakerSchedule_Map__c> ();
        
       // if (!(request.Size() > 0)){
         //   throw new CustomException('Input should not be empty');
        for (InputData data: request) {
         String Id = data.ScheduleId;
            String Language = data.Language;
            vu = [SELECT VU_Speakers__c, VU_Event_Schedule__c, VU_Speakers__r.Name,VU_Speakers__r.Email__c,VU_Speakers__r.Company__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c FROM VU_SpeakerSchedule_Map__c where VU_Event_Schedule__c=:Id AND Languages__c =: Language];
            //system.debug('test--------' + a);
             
        }
        return vu; 
      //  }
      
        
      // else{}
       //else{
       
        
       // List < VU_Products__c > v = new List < VU_Products__c > ();
      /*VU_Products__c vx = new VU_Products__c();
        Vx.Error__c = 'Please provide valid Input';
        vu.add(vx);
        
        return vu;
        }
    }*/

    global Class InputData {
    
            WebService String Language {
            get;
            set;
        }
          WebService String ScheduleId {
            get;
            set;
        }
    }

    public Class CustomException extends Exception {} 
     }
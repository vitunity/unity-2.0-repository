/*@RestResource(urlMapping='/VUEvents/*')
       global class VUEventController 
       {
       @HttpGet
        global static List<Event_Webinar__c> getEvents()
        {
        List<Event_Webinar__c> Events;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
         String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
             Events= [SELECT Id,Name,Active__c,Private__c,Title__c, Event_Accessibility__c,Time_Zone__c, From_Date__c,To_Date__c,Location__c,Store_Thumbnail_Image_Url__c, Store_Image_url__c,EventThumbnailImage__c,EventImage__c,Languages__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND (Languages__c='English(en)' OR Languages__c='' OR Event_Accessibility__c includes ('English(en)'))]; //
         }
         else
         {
         Events= [SELECT Id,Name,Active__c,Private__c,Title__c, Event_Accessibility__c,Time_Zone__c, From_Date__c,To_Date__c,Location__c,Store_Thumbnail_Image_Url__c, Store_Image_url__c,EventThumbnailImage__c,EventImage__c,Languages__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND(Languages__c=:Language OR Event_Accessibility__c includes (:Language))];    //
      }
        return Events;
        }
        
          @HttpPost
    global static List < Event_Webinar__c> getTreatment(List < InputData > request) {
        system.debug('testmethod start-----');
        RestRequest req = RestContext.request;
        RestResponse resp = RestContext.response;
        resp.addHeader('Access-Control-Allow-Origin', '*');
        resp.addHeader('Content-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Credentials', 'true');
        resp.addHeader('Access-Control-Allow-Headers', 'x-requested-with, Authorization');
        resp.addHeader('Accept-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
        resp.addHeader('Access-Control-Allow-Headers', 'POST');
        resp.addHeader('Access-Control-Allow-Methods', 'POST');
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        List < Event_Webinar__c> vu = new List < Event_Webinar__c> ();
        
      // if (!(request.Size() > 0)){
         //   throw new CustomException('Input should not be empty');
        for (InputData data: request) {
         //   String Id = data.CategoryId;
            String Language = data.Language;
            vu = [SELECT Id,Name,Active__c,Private__c,Title__c, From_Date__c, To_Date__c,Languages__c,Time_Zone__c, Location__c, Store_Thumbnail_Image_Url__c, Store_Image_url__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND Languages__c =: Language];
            //system.debug('test--------' + a);
             
        }
        return vu; 
        
      
        
    //  else{
      //   vu = [SELECT Id,Name,Active__c,Private__c,Title__c, From_Date__c, To_Date__c,Languages__c,Time_Zone__c, Location__c, Store_Thumbnail_Image_Url__c, Store_Image_url__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND Languages__c =''];
            //system.debug('test--------' + a);}
       //else{
       
        
       // List < VU_Products__c > v = new List < VU_Products__c > ();
      /*VU_Products__c vx = new VU_Products__c();
        Vx.Error__c = 'Please provide valid Input';
        vu.add(vx);
        
        return vu;
        }
      //   return vu;
    
    } 
    global Class InputData {
    
            WebService String Language {
            get;
            set;
        }
    }

    public Class CustomException extends Exception {} 
       }
*/


@RestResource(urlMapping='/VUEvents/*')
       global class VUEventController 
       {
       @HttpGet
        global static List<Event_Webinar__c> getEvents()
        {
        List<Event_Webinar__c> Events;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
         String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='German(de)' && Language !='Japanese(ja)'){
             Events= [SELECT Id,Name,Active__c,Private__c,Title__c, Event_Accessibility__c,Time_Zone__c, From_Date__c,To_Date__c,Location__c,Store_Thumbnail_Image_Url__c, Store_Image_url__c,EventThumbnailImage__c,EventImage__c,Languages__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND (Languages__c='English(en)' OR Languages__c='' OR Event_Accessibility__c includes ('English(en)'))]; //
         }
         else
         {
         Events= [SELECT Id,Name,Active__c,Private__c,Title__c, Event_Accessibility__c,Time_Zone__c, From_Date__c,To_Date__c,Location__c,Store_Thumbnail_Image_Url__c, Store_Image_url__c,EventThumbnailImage__c,EventImage__c,Languages__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND(Languages__c=:Language OR Event_Accessibility__c includes (:Language))];    //
      }
        return Events;
        }
       /* 
          @HttpPost
    global static List < Event_Webinar__c> getTreatment(List < InputData > request) {
        system.debug('testmethod start-----');
        RestRequest req = RestContext.request;
        RestResponse resp = RestContext.response;
        resp.addHeader('Access-Control-Allow-Origin', '*');
        resp.addHeader('Content-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Credentials', 'true');
        resp.addHeader('Access-Control-Allow-Headers', 'x-requested-with, Authorization');
        resp.addHeader('Accept-Type', 'application/json');
        resp.addHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
        resp.addHeader('Access-Control-Allow-Headers', 'POST');
        resp.addHeader('Access-Control-Allow-Methods', 'POST');
        RestContext.response.addHeader('Content-Type', 'application/json');
        
        List < Event_Webinar__c> vu = new List < Event_Webinar__c> ();
        
      // if (!(request.Size() > 0)){
         //   throw new CustomException('Input should not be empty');
        for (InputData data: request) {
         //   String Id = data.CategoryId;
            String Language = data.Language;
            vu = [SELECT Id,Name,Active__c,Private__c,Title__c, From_Date__c, To_Date__c,Languages__c,Time_Zone__c, Location__c, Store_Thumbnail_Image_Url__c, Store_Image_url__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND Languages__c =: Language];
            //system.debug('test--------' + a);
             
        }
        return vu; 
        
      
        
    //  else{
      //   vu = [SELECT Id,Name,Active__c,Private__c,Title__c, From_Date__c, To_Date__c,Languages__c,Time_Zone__c, Location__c, Store_Thumbnail_Image_Url__c, Store_Image_url__c,City_Guide__c, (SELECT Id, Name, ContentType FROM Attachments) FROM Event_Webinar__c WHERE Varian_Unite_Event__c=true AND Languages__c =''];
            //system.debug('test--------' + a);}
       //else{
       
        
       // List < VU_Products__c > v = new List < VU_Products__c > ();
      /*VU_Products__c vx = new VU_Products__c();
        Vx.Error__c = 'Please provide valid Input';
        vu.add(vx);
        
        return vu;
        }
      //   return vu;
    
    } */

    global Class InputData {
    
            WebService String Language {
            get;
            set;
        }
    }

    public Class CustomException extends Exception {} 
       }
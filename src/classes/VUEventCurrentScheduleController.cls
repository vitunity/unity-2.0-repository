/*@RestResource(urlMapping='/VUEventCurrentSchedule/*')
     global class VUEventCurrentScheduleController 
     {
     @HttpGet
     global static List<VU_Event_Schedule__c> getSchedules() 
     {
       List<VU_Event_Schedule__c> a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    Datetime d = datetime.now().addhours(-1);
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   /* Set<ID> vesID = new Set<ID>();
Map<Id,String> mapVES = new Map<Id,String>();

VU_Event_Schedule__c VS=New VU_Event_Schedule__c() ;
    vesID.add(VS.Id);


List<VU_Event_Schedule__c> vues =[SELECT ID, Event_Webinar__r.TimeZones__c FROM VU_Event_Schedule__c WHERE Id IN : vesID ];

for(VU_Event_Schedule__c vsu: vues){
    mapVES.put(vsu.Id,vsu.Event_Webinar__r.TimeZones__c );
}

VU_Event_Schedule__c VES=New VU_Event_Schedule__c ();
TimeZone tz = UserInfo.getTimeZone();
System.debug('Display name: ' + tz.getDisplayName());
System.debug('ID: ' + tz.getID());
integer offsetToUserTimeZone = tz.getOffset(VES.Start_Time__c);
integer offsetToUserEndTimeZone = tz.getOffset(VES.End_Time__c);
System.debug('Offset: ' + tz.getOffset(VES.Start_Time__c));
//DateTime customerDateTime = DateTime.valueofGmt(Start_Time__c);
//Timezone tz1 = Timezone.getTimeZone('Text(Event_Webinar__r.Time_Zone__c)');

//String Id= ApexPages.currentPage().getParameters().get('id') ;
//List<VU_Event_Schedule__c> a =[SELECT ID,Event_Webinar__r.TimeZones__c FROM VU_Event_Schedule__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') ];
//Timezone tz1=Timezone.getTimeZone(tzo);
String tzo;
//tzo='Asia/Kuala_Lumpur';
tzo = mapVES.get(VES.Id);
System.debug('City'+tzo);
System.debug('String: ' + tzo);
TimeZone EventTimeZone = TimeZone.getTimeZone(tzo);
System.debug('ID2: ' + EventTimeZone );
System.debug('Display name1: ' + EventTimeZone .getDisplayName());
System.debug('ID1: ' + EventTimeZone .getID());

System.debug('String format: ' + EventTimeZone .toString());
System.debug('Offset1: ' + EventTimeZone .getOffset(VES.Start_Time__c)); 
integer offsetToEventTimeZone = EventTimeZone.getOffset(VES.Start_Time__c);
integer offsetToEventEndTimeZone = EventTimeZone.getOffset(VES.End_Time__c);
integer var= offsetToUserEndTimeZone -offsetToEventEndTimeZone ;
integer varTime= offsetToUserTimeZone -offsetToEventTimeZone ;
integer VarT=var/(1000*60*60); 
String Language= RestContext.request.params.get('Language') ;
System.debug('ID: ' + DateTime.Now());
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
   a = [SELECT Event_Webinar__c,Date__c,Event_ID__c,Id,Moderator_Email__c,Moderator_Phone__c,Location__c,Name,Description__c,Language__c,Start_Time__c,End_Time__c,Session_Start_Time__c,Session_End_Time__c,
     
     (SELECT ID,Name FROM Attachments),
     (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c,VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE  (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)')) AND Start_Time__c<=:DateTime.Now().addhours(1) AND End_Time__c>=:DateTime.Now().addhours(-1)];
     }
     else
     {
    
   a = [SELECT Event_Webinar__c,Date__c,Event_ID__c,Id,Moderator_Email__c,Moderator_Phone__c,Location__c,Name,Description__c,Language__c,Start_Time__c,End_Time__c,Session_Start_Time__c,Session_End_Time__c,
     
     (SELECT ID,Name FROM Attachments),
     (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c,VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE  (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language)) AND Start_Time__c<=:DateTime.Now().addhours(1) AND End_Time__c>=:DateTime.Now().addhours(-1)];     
     }
     return a;
     } 
   }*/
   
   @RestResource(urlMapping='/VUEventCurrentSchedule/*')
     global class VUEventCurrentScheduleController 
     {
     @HttpGet
     global static List<VU_Event_Schedule__c> getSchedules() 
     {
       List<VU_Event_Schedule__c> a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    Datetime d = datetime.now().addhours(-1);
    String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
   /* Set<ID> vesID = new Set<ID>();
Map<Id,String> mapVES = new Map<Id,String>();

VU_Event_Schedule__c VS=New VU_Event_Schedule__c() ;
    vesID.add(VS.Id);


List<VU_Event_Schedule__c> vues =[SELECT ID, Event_Webinar__r.TimeZones__c FROM VU_Event_Schedule__c WHERE Id IN : vesID ];

for(VU_Event_Schedule__c vsu: vues){
    mapVES.put(vsu.Id,vsu.Event_Webinar__r.TimeZones__c );
}

VU_Event_Schedule__c VES=New VU_Event_Schedule__c ();
TimeZone tz = UserInfo.getTimeZone();
System.debug('Display name: ' + tz.getDisplayName());
System.debug('ID: ' + tz.getID());
integer offsetToUserTimeZone = tz.getOffset(VES.Start_Time__c);
integer offsetToUserEndTimeZone = tz.getOffset(VES.End_Time__c);
System.debug('Offset: ' + tz.getOffset(VES.Start_Time__c));
//DateTime customerDateTime = DateTime.valueofGmt(Start_Time__c);
//Timezone tz1 = Timezone.getTimeZone('Text(Event_Webinar__r.Time_Zone__c)');

//String Id= ApexPages.currentPage().getParameters().get('id') ;
//List<VU_Event_Schedule__c> a =[SELECT ID,Event_Webinar__r.TimeZones__c FROM VU_Event_Schedule__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') ];
//Timezone tz1=Timezone.getTimeZone(tzo);
String tzo;
//tzo='Asia/Kuala_Lumpur';
tzo = mapVES.get(VES.Id);
System.debug('City'+tzo);
System.debug('String: ' + tzo);
TimeZone EventTimeZone = TimeZone.getTimeZone(tzo);
System.debug('ID2: ' + EventTimeZone );
System.debug('Display name1: ' + EventTimeZone .getDisplayName());
System.debug('ID1: ' + EventTimeZone .getID());

System.debug('String format: ' + EventTimeZone .toString());
System.debug('Offset1: ' + EventTimeZone .getOffset(VES.Start_Time__c)); 
integer offsetToEventTimeZone = EventTimeZone.getOffset(VES.Start_Time__c);
integer offsetToEventEndTimeZone = EventTimeZone.getOffset(VES.End_Time__c);
integer var= offsetToUserEndTimeZone -offsetToEventEndTimeZone ;
integer varTime= offsetToUserTimeZone -offsetToEventTimeZone ;
integer VarT=var/(1000*60*60); */
String Language= RestContext.request.params.get('Language') ;
System.debug('ID: ' + DateTime.Now());
   if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' && Language !='German(de)'){
   a = [SELECT Event_Webinar__c,Date__c,Event_ID__c,Id,Moderator_Email__c,Moderator_Phone__c,Location__c,Name,Description__c,Language__c,Start_Time__c,End_Time__c,Session_Start_Time__c,Session_End_Time__c,
     
     (SELECT ID,Name FROM Attachments),
     (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c,VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE  (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)')) AND Start_Time__c<=:DateTime.Now().addhours(1) AND End_Time__c>=:DateTime.Now().addhours(-1)];
     }
     else
     {
    
   a = [SELECT Event_Webinar__c,Date__c,Event_ID__c,Id,Moderator_Email__c,Moderator_Phone__c,Location__c,Name,Description__c,Language__c,Start_Time__c,End_Time__c,Session_Start_Time__c,Session_End_Time__c,
     
     (SELECT ID,Name FROM Attachments),
     (SELECT VU_Speakers__c, VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c, VU_Speakers__r.Company__c,VU_Speakers__r.Description__c, VU_Speakers__r.Designation__c, VU_Speakers__r.Title__c, VU_Speakers__r.LastName__c, VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM VU_SpeakerSchedule_Maps__r) FROM VU_Event_Schedule__c WHERE  (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language)) AND Start_Time__c<=:DateTime.Now().addhours(1) AND End_Time__c>=:DateTime.Now().addhours(-1)];     
     }
     return a;
     } 
   }
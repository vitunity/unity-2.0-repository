@RestResource(urlMapping='/VUEventLivePolling/*')
    global class VUEventLivePollingController 
    {
    @HttpGet
    global static List<VU_Live_Polling_Links__c> getBlob() 
    {
    List<VU_Live_Polling_Links__c> a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    String Id= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language') ;
   if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' && Language !='German(de)' ){
   a =[ SELECT Name,ID,Enable__c,Event_Webinar__c,Google_form_Links__c FROM VU_Live_Polling_Links__c WHERE Event_Webinar__c= :Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
    System.debug('+++++++++++++++a'+a);
    }
    else
    {
    a =[ SELECT Name,ID,Enable__c,Event_Webinar__c,Google_form_Links__c
FROM VU_Live_Polling_Links__c WHERE Event_Webinar__c= :Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
    }
    return a;
    
     } 
    }
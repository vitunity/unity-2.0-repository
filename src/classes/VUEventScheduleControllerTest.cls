@istest
private class VUEventScheduleControllerTest 
{
    
    private static testmethod void mytest()
    {
        Event_Webinar__c e = new Event_Webinar__c(Title__c='test',Languages__c='English(en)',TimeZones__c='America/New_York',Time_Zone__c='(GMT-05:00) Eastern Standard Time (America/New_York)');
        insert e;
       // VU_Speakers__c vs = new VU_Speakers__c();
        system.debug('xxxEID: '+e.ID);
        VU_Speakers__c vs = new VU_Speakers__c(name='test');
        insert vs;
        system.debug('xxxVSID: '+vs.ID);
        VU_Event_Schedule__c es = new VU_Event_Schedule__c(Event_Webinar__c =e.ID, 
                                                           Date__c=System.Today(),
                                                           End_Time__c=System.Datetime.now()+2,
                                                           Event_ID__c='ID',Location__c='test',Language__c='English(en)',
                                                           Name='test e',Start_Time__c=System.Datetime.now()
                                                          );
        insert es;
        system.debug('xxxESID: '+es.ID);
        
        Attachment attach = new Attachment(Name='test', ParentId=vs.ID, body=EncodingUtil.base64Decode('testtest'));
        insert attach;
        system.debug('xxxAttachID: '+attach.ID);
        Interactive_Maps_Landmarks__c iml = new Interactive_Maps_Landmarks__c(name='test', Event_Webinar__c =e.ID);
        insert iml;
        system.debug('xxximlID: '+iml.ID);
      
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/EventSchedule'; 
        req.addParameter('EventId', e.ID);
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;

        List<VU_Event_Schedule__c> results = VUEventScheduleController.getBlob();
    }
}
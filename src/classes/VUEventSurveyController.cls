/*@RestResource(urlMapping='/VUEventSurvey/*')
    global class VUEventSurveyController
    {
    @HttpGet
    global static List<Survey__c> getBlob() 
    {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
     res.addHeader('Access-Control-Allow-Origin','*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Credentials','true');
    res.addHeader('Access-Control-Allow-Headers','x-requested-with, Authorization');
    res.addHeader('Accept-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
    res.addHeader('Access-Control-Allow-Headers','GET,POST,PUT,DELETE');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    RestContext.response.addHeader('Content-Type', 'application/json');
     //   res.addHeader('Content-Type', 'application/json');
     //res.addHeader('Access-Control-Allow-Origin', '*');
    //res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String eId= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language');
    List<Survey__c> sv = new List<Survey__c>();
    if(eId !='' && Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR')
    {
    sv=[SELECT Id,Name,Event_Webinar__c,Event_Webinar__r.From_Date__c,Language__c,Event_Webinar__r.To_Date__c,VU_Event_Session__c,VU_Event_Session__r.Moderator_Email__c,VU_Event_Session__r.Moderator_Phone__c,VU_Event_Session__r.Start_Time__c,VU_Event_Session__r.End_Time__c,(SELECT Id,Question__c,Choices__c, OrderNumber__c,Required__c,Type__c from
    SurveyQuestions__r) FROM Survey__c WHERE Event_Webinar__c=:eId AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))] ;
    }
    else{
    sv=[SELECT Id,Name,Event_Webinar__c,Event_Webinar__r.From_Date__c,Language__c,Event_Webinar__r.To_Date__c,VU_Event_Session__c,VU_Event_Session__r.Moderator_Email__c,VU_Event_Session__r.Moderator_Phone__c,VU_Event_Session__r.Start_Time__c,VU_Event_Session__r.End_Time__c,(SELECT Id,Question__c,Choices__c, OrderNumber__c,Required__c,Type__c from
    SurveyQuestions__r) FROM Survey__c WHERE Event_Webinar__c=:eId AND  (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )] ;
    }
    return sv;
    }
    @httpPost  
    global static String createBSS(List<InputData> request) 
    {
    system.debug('testmethod start-----');
    RestRequest req = RestContext.request;
    RestResponse resp = RestContext.response;
      resp.addHeader('Access-Control-Allow-Origin','*');
    resp.addHeader('Content-Type', 'application/json;charset=UTF-8');
    resp.addHeader('Access-Control-Allow-Credentials','true');
    resp.addHeader('Access-Control-Allow-Headers','x-requested-with, Authorization');
    resp.addHeader('Accept-Type', 'application/json;charset=UTF-8');
    resp.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
    resp.addHeader('Access-Control-Allow-Headers','POST');
    resp.addHeader('Access-Control-Allow-Methods', 'POST');
    RestContext.response.addHeader('Content-Type', 'application/json;charset=UTF-8');
    try 
    {
    if(!(request.Size() > 0))
    throw new CustomException('Input should not be empty'); 
    for(InputData data : request) {
    SurveyTaker__c taker = new SurveyTaker__c();
    taker.Taken__c=data.SurveyTaker;
    taker.Survey__c=data.SurveyId;
    taker.User__c=data.UserId;
    taker.Lead__c=data.LeadId;
    taker.Contact__c=data.ContactId;
                //a.add(taker);
    insert taker;
    List<SurveyQuestionResponse__c> response = new List<SurveyQuestionResponse__c>();
    /*List<String> questions = data.SurveyRes.Split(';');
    for(String q:questions) 
    {
    
    String[] Ans = q.Split(':');
    system.debug('====TEST Class START');
     system.debug('===================='+Ans);
    if(!test.isrunningtest()){
    
    SurveyQuestionResponse__c res = new SurveyQuestionResponse__c(Survey_Question__c=Ans[0],Response__c=Ans[1],Device_Id__c=data.DeviceId, SurveyTaker__c=taker.ID,VU_Speakers__c=data.SpeakerId);
    //res.VU_Speakers__c=data.SpeakerId;
    system.debug('====TEST Class END'+res);
    response.add(res);
    }
    List<SurveyQuestionResp> SurverResponse=data.SurveyResp;
    for(SurveyQuestionResp data3 :SurverResponse){
        SurveyQuestionResponse__c res = new SurveyQuestionResponse__c();
    res.Device_Id__c=data.DeviceId;
    res.SurveyTaker__c=taker.ID;
    res.Survey_Question__c=data3.QuestionId;
    res.Response__c=data3.Response;
    response.add(res);
    }        
    insert response;        
    List<Breakout_Session__c> speakerBS= new List<Breakout_Session__c>();
    List<breakOutInputData> breakOutInputList = data.breakOutData;
    for(breakOutInputData  data1 : breakOutInputList) {
    Breakout_Session__c BS= new Breakout_Session__c();
    BS.VU_Speakers__c= data1.speakerId ;
    BS.VU_Event_Session__c=data1.SessionId;
    BS.Event_Webinar__c=data1.EventId;
    BS.Did_Not_Attend__c =data1.isAttended;
    BS.Met_Expectations__c = Integer.valueOf(data1.metExpectationRating);
    BS.Compelling_Information__c = Integer.valueOf(data1.compellingInformationRating);
    BS.Effective_Communicator__c = Integer.valueOf(data1.effectiveCommunicatorRating);
    BS.Comments__c = data1.comments;
    BS.Rating_given_by_User__c=data1.UserId;
    BS.Rating_given_by_Contact__c=data1.ContactId;
    BS.Rating_given_by_Lead__c=data1.LeadId;
    speakerBS.add(BS); 
    }
    Insert speakerBS;
    List<VU_Survey_Ranking_Questions_Response__c> SRQR= new List<VU_Survey_Ranking_Questions_Response__c>();
    List<SurveyRankingResponse> VSRQR = data.RankingData;
    for(SurveyRankingResponse data2 : VSRQR){
    VU_Survey_Ranking_Questions_Response__c RQR= new VU_Survey_Ranking_Questions_Response__c();
    RQR.VU_Survey_Question__c=data2.SurveyQuestionId;
    RQR.Option_1__c=data2.Option1;
    RQR.Option_2__c=data2.Option2;
    RQR.Option_3__c=data2.Option3;
    RQR.Option_4__c=data2.Option4;
    RQR.Option_5__c=data2.Option5;
    RQR.Survey_Taker__c=taker.ID;
    SRQR.add(RQR);
    }
    Insert SRQR;
    }
            //List<Survey__c> s=[SELECT Id,Name from Survey__c where Name=:Survey];
            //List<SurveyTaker__c > a = [SELECT Id,Name from SurveyTaker__c where Survey__r.Name=:Survey];
            //List<SurveyTaker__c > a=new List<SurveyTaker__c > ();
        
     system.debug('request:' + request) ;  
     return 'success'; 
     }
      catch(Exception e) 
     {
      return e.getMessage();
     }
     }
    
     global Class InputData
     {       
     WebService List<breakOutInputData> breakOutData { get; set;}
     WebService List<SurveyRankingResponse> RankingData{ get; set; }
      WebService List<SurveyQuestionResp> SurveyResp{ get; set; }
     WebService String SurveyTaker{ get; set; }      
     WebService String SurveyRes{ get; set; }   
     WebService String DeviceId{ get; set; } 
     WebService String SurveyId{ get; set; } 
     webService String EventId { get; set; } 
     WebService String SessionId { get; set; } 
     WebService String speakerId { get; set; } 
     WebService String UserId { get; set; } 
     WebService String LeadId { get; set; } 
     WebService String ContactId { get; set; } 
     }
   global class breakOutInputData 
    {
      WebService String speakerId { get; set; } 
      WebService String SessionId{ get; set; } 
      WebService String EventId{ get; set; } 
      WebService String isAttended { get; set; }
      WebService Integer metExpectationRating { get; set; }
      WebService Integer compellingInformationRating { get; set; }
      WebService Integer effectiveCommunicatorRating { get; set; }
      WebService String comments { get; set; } 
      WebService String UserId { get; set; } 
     WebService String LeadId { get; set; } 
     WebService String ContactId { get; set; } 
    }
    global class SurveyRankingResponse
    {
    WebService String SurveyQuestionId{ get;set; }
    WebService String Option1{get;set;}
    WebService String Option2{get;set;}
    WebService String Option3{get;set;}
    WebService String Option4{get;set;}
    WebService String Option5{get;set;}
    WebService String SurveyTaker{ get; set; }
    }    
      global class SurveyQuestionResp
    {
     
       WebService String QuestionId{ get; set; } 
       WebService String Response{ get; set; } 
    }
    public Class CustomException extends Exception {}
    }*/
    
    
    @RestResource(urlMapping='/VUEventSurvey/*')
    global class VUEventSurveyController
    {
    @HttpGet
    global static List<Survey__c> getBlob() 
    {
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
     res.addHeader('Access-Control-Allow-Origin','*');
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Credentials','true');
    res.addHeader('Access-Control-Allow-Headers','x-requested-with, Authorization');
    res.addHeader('Accept-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
    res.addHeader('Access-Control-Allow-Headers','GET,POST,PUT,DELETE');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    RestContext.response.addHeader('Content-Type', 'application/json');
     //   res.addHeader('Content-Type', 'application/json');
     //res.addHeader('Access-Control-Allow-Origin', '*');
    //res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');*/
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String eId= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language');
    List<Survey__c> sv = new List<Survey__c>();
    if(eId !='' && Language !='English(en)' && Language !='Chinese(zh)'&& Language !='Japanese(ja)')
    {
    sv=[SELECT Id,Name,Event_Webinar__c,Event_Webinar__r.From_Date__c,Language__c,Event_Webinar__r.To_Date__c,VU_Event_Session__c,VU_Event_Session__r.Moderator_Email__c,VU_Event_Session__r.Moderator_Phone__c,VU_Event_Session__r.Start_Time__c,VU_Event_Session__r.End_Time__c,(SELECT Id,Question__c,Choices__c, OrderNumber__c,Required__c,Type__c from
    SurveyQuestions__r) FROM Survey__c WHERE Event_Webinar__c=:eId AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))] ;
    }
    else{
    sv=[SELECT Id,Name,Event_Webinar__c,Event_Webinar__r.From_Date__c,Language__c,Event_Webinar__r.To_Date__c,VU_Event_Session__c,VU_Event_Session__r.Moderator_Email__c,VU_Event_Session__r.Moderator_Phone__c,VU_Event_Session__r.Start_Time__c,VU_Event_Session__r.End_Time__c,(SELECT Id,Question__c,Choices__c, OrderNumber__c,Required__c,Type__c from
    SurveyQuestions__r) FROM Survey__c WHERE Event_Webinar__c=:eId AND  (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )] ;
    }
    return sv;
    }
    @httpPost  
    global static String createBSS(List<InputData> request) 
    {
    system.debug('testmethod start-----');
    RestRequest req = RestContext.request;
    RestResponse resp = RestContext.response;
      resp.addHeader('Access-Control-Allow-Origin','*');
    resp.addHeader('Content-Type', 'application/json;charset=UTF-8');
    resp.addHeader('Access-Control-Allow-Credentials','true');
    resp.addHeader('Access-Control-Allow-Headers','x-requested-with, Authorization');
    resp.addHeader('Accept-Type', 'application/json;charset=UTF-8');
    resp.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
    resp.addHeader('Access-Control-Allow-Headers','POST');
    resp.addHeader('Access-Control-Allow-Methods', 'POST');
    RestContext.response.addHeader('Content-Type', 'application/json;charset=UTF-8');
    try 
    {
    if(!(request.Size() > 0))
    throw new CustomException('Input should not be empty'); 
    for(InputData data : request) {
    SurveyTaker__c taker = new SurveyTaker__c();
    taker.Taken__c=data.SurveyTaker;
    taker.Survey__c=data.SurveyId;
    taker.User__c=data.UserId;
    taker.Lead__c=data.LeadId;
    taker.Contact__c=data.ContactId;
                //a.add(taker);
    insert taker;
    List<SurveyQuestionResponse__c> response = new List<SurveyQuestionResponse__c>();
    /*List<String> questions = data.SurveyRes.Split(';');
    for(String q:questions) 
    {
    
    String[] Ans = q.Split(':');
    system.debug('====TEST Class START');
     system.debug('===================='+Ans);
    if(!test.isrunningtest()){
    
    SurveyQuestionResponse__c res = new SurveyQuestionResponse__c(Survey_Question__c=Ans[0],Response__c=Ans[1],Device_Id__c=data.DeviceId, SurveyTaker__c=taker.ID,VU_Speakers__c=data.SpeakerId);
    //res.VU_Speakers__c=data.SpeakerId;
    system.debug('====TEST Class END'+res);
    response.add(res);
    }*/
    List<SurveyQuestionResp> SurverResponse=data.SurveyResp;
    for(SurveyQuestionResp data3 :SurverResponse){
        SurveyQuestionResponse__c res = new SurveyQuestionResponse__c();
    res.Device_Id__c=data.DeviceId;
    res.SurveyTaker__c=taker.ID;
    res.Survey_Question__c=data3.QuestionId;
    res.Response__c=data3.Response;
    response.add(res);
    }        
    insert response;        
    List<Breakout_Session__c> speakerBS= new List<Breakout_Session__c>();
    List<breakOutInputData> breakOutInputList = data.breakOutData;
    for(breakOutInputData  data1 : breakOutInputList) {
    Breakout_Session__c BS= new Breakout_Session__c();
    BS.VU_Speakers__c= data1.speakerId ;
    BS.Event_Webinar__c=data1.EventId;
    BS.VU_Event_Session__c=data1.SessionId;
    BS.Did_Not_Attend__c =data1.isAttended;
    BS.Met_Expectations__c = Integer.valueOf(data1.metExpectationRating);
    BS.Compelling_Information__c = Integer.valueOf(data1.compellingInformationRating);
    BS.Effective_Communicator__c = Integer.valueOf(data1.effectiveCommunicatorRating);
    BS.Comments__c = data1.comments;
    BS.Rating_given_by_User__c=data1.UserId;
    BS.Rating_given_by_Contact__c=data1.ContactId;
    BS.Rating_given_by_Lead__c=data1.LeadId;
    speakerBS.add(BS); 
    }
    Insert speakerBS;
    List<VU_Survey_Ranking_Questions_Response__c> SRQR= new List<VU_Survey_Ranking_Questions_Response__c>();
    List<SurveyRankingResponse> VSRQR = data.RankingData;
    for(SurveyRankingResponse data2 : VSRQR){
    VU_Survey_Ranking_Questions_Response__c RQR= new VU_Survey_Ranking_Questions_Response__c();
    RQR.VU_Survey_Question__c=data2.SurveyQuestionId;
    RQR.Option_1__c=data2.Option1;
    RQR.Option_2__c=data2.Option2;
    RQR.Option_3__c=data2.Option3;
    RQR.Option_4__c=data2.Option4;
    RQR.Option_5__c=data2.Option5;
    RQR.Survey_Taker__c=taker.ID;
    SRQR.add(RQR);
    }
    Insert SRQR;
    }
            //List<Survey__c> s=[SELECT Id,Name from Survey__c where Name=:Survey];
            //List<SurveyTaker__c > a = [SELECT Id,Name from SurveyTaker__c where Survey__r.Name=:Survey];
            //List<SurveyTaker__c > a=new List<SurveyTaker__c > ();
        
     system.debug('request:' + request) ;  
     return 'success'; 
     }
      catch(Exception e) 
     {
      return e.getMessage();
     }
     }
    
     global Class InputData
     {       
     WebService List<breakOutInputData> breakOutData { get; set;}
     WebService List<SurveyRankingResponse> RankingData{ get; set; }
      WebService List<SurveyQuestionResp> SurveyResp{ get; set; }
     WebService String SurveyTaker{ get; set; }      
     WebService String SurveyRes{ get; set; }   
     WebService String DeviceId{ get; set; } 
     WebService String SurveyId{ get; set; } 
     webService String EventId { get; set; } 
     WebService String SessionId { get; set; } 
     WebService String speakerId { get; set; } 
     WebService String UserId { get; set; } 
     WebService String LeadId { get; set; } 
     WebService String ContactId { get; set; } 
     }
   global class breakOutInputData 
    {
      WebService String speakerId { get; set; } 
      WebService String EventId{ get; set; } 
      WebService String SessionId{ get; set; } 
      WebService String isAttended { get; set; }
      WebService Integer metExpectationRating { get; set; }
      WebService Integer compellingInformationRating { get; set; }
      WebService Integer effectiveCommunicatorRating { get; set; }
      WebService String comments { get; set; } 
      WebService String UserId { get; set; } 
     WebService String LeadId { get; set; } 
     WebService String ContactId { get; set; } 
    }
    global class SurveyRankingResponse
    {
    WebService String SurveyQuestionId{ get;set; }
    WebService String Option1{get;set;}
    WebService String Option2{get;set;}
    WebService String Option3{get;set;}
    WebService String Option4{get;set;}
    WebService String Option5{get;set;}
    WebService String SurveyTaker{ get; set; }
    }    
      global class SurveyQuestionResp
    {
     
       WebService String QuestionId{ get; set; } 
       WebService String Response{ get; set; } 
    }
    public Class CustomException extends Exception {}
    }
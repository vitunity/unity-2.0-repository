@istest
private class VUEventSurveyTest {
     static testMethod void myVUEventSurveyControllerTest() 
     {
     RestRequest req = new RestRequest();
      RestResponse res = new RestResponse();
      req.requestURI = 'https://sfvud-varian.cs8.force.com/MPP/services/apexrest/VUEventSurvey'; 
        RestContext.request = req;
        RestContext.response = res;
         req.httpMethod = 'GET';
        List<Survey__c>  p= VUEventSurveyController.getBlob();
 }
 static testMethod void myVUEventSurveyControllerTest1()
 {
    Event_Webinar__c vuEvent = new Event_Webinar__c(Title__c='Astro 2015',From_Date__c=date.today(),To_Date__c=date.today()+4,Time_Zone__c='(GMT-05:00) Eastern Standard Time (America/New_York)',TimeZones__c='America/New_York');
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.requestURI = 'https://sfvud-varian.cs8.force.com/MPP/services/apexrest/VUEventSurvey'; 
    RestContext.request = req;
    RestContext.response = res;
    req.httpMethod = 'POST';
    insert vuEvent;
    String EventId = vuEvent.Id;
    VU_Event_Schedule__c vuSchedule = new VU_Event_Schedule__c(Event_Webinar__c=EventId,Language__c='English(en)',Name='TestSessionName',Start_Time__c=system.now(),End_Time__c=system.now().addhours(1));
        insert vuSchedule;
         String SessionId = vuSchedule.Id;
          VU_Speakers__c vuSpeaker = new VU_Speakers__c(Name='TestSpeakerMapName');
        insert vuSpeaker;
         String VUSpeakerId = vuSpeaker.Id;
          Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User vuUser = new User(Alias = 'stan23', Email='standarduser@testorg1.com', 
            EmailEncodingKey='UTF-8', LastName='Tes123', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standar123@testorg.com');
        insert vuUser;
         String UserId = vuUser.Id;
         Lead vuLead = new Lead(LastName='testName1',Company='TestCompany1',CurrencyIsoCode='INR',Status='Open');
        insert vuLead;
         String LeadId = vuLead.Id;
         Account VUAccount= new Account(Name='Test',Phone='01244008660',Country__c='India',OMNI_Address1__c='Chandigarh',OMNI_City__c='Punjab');
         insert VUAccount;
         String Account=VUAccount.Id;
         Contact vuContact = new Contact(AccountId=Account,FirstName='Test',LastName='testName2',Medical_School_Country__c='USA',CurrencyIsoCode='INR',Email='Surendra@gmail.com',Phone='01244008660',MobilePhone='950506779');
        insert vuContact ;
         String ContactId = vuContact.Id;
      Survey__c VuSurvey =new Survey__c(Name='Astro2015',Event_Webinar__c=EventId);
        insert VuSurvey;
        String SurveyId= VuSurvey.Id;
        SurveyTaker__c taker = new SurveyTaker__c(Taken__c='Surendra',Survey__c=SurveyId,User__c=UserId);
        insert taker;
        String SurveyTaker= taker.Id;
        Survey_Question__c VuSQ= new Survey_Question__c(Name='How compelling was the information provided?',Type__c='Free Text',Survey__c=SurveyId,OrderNumber__c=1,Question__c='How compelling was the information provided?');
        insert VuSQ;
        String SurveyQustion= VuSQ.Id;
         
        SurveyQuestionResponse__c resp = new SurveyQuestionResponse__c(Survey_Question__c=SurveyQustion,Response__c='hello world', SurveyTaker__c=SurveyTaker,VU_Speakers__c=VUSpeakerId);
        insert resp;
       
        String SurveyRes= resp.Response__c;
        VU_Survey_Ranking_Questions_Response__c SRQR= new VU_Survey_Ranking_Questions_Response__c(Option_1__c='2',Option_2__c='1',Option_3__c='4',Survey_Taker__c=SurveyTaker,VU_Survey_Question__c=SurveyQustion);
        insert SRQR;
        String SurveyQuestionResp= SRQR.Id;
        integer metExpectationRating=5;
        integer compellingInformationRating=4;
        integer effectiveCommunicatorRating=3;
        String comments='hello world';
        
        List<Breakout_Session__c> speakerBSS= new List<Breakout_Session__c>();
        Breakout_Session__c BSS= new Breakout_Session__c();
    BSS.VU_Speakers__c= VUSpeakerId ;
    BSS.Did_Not_Attend__c ='Yes';
    BSS.Met_Expectations__c = 5;
    BSS.Compelling_Information__c = 4;
    BSS.Effective_Communicator__c =3;
    BSS.Comments__c = 'hello world';
    BSS.Rating_given_by_User__c=UserId;
   // BS.Rating_given_by_Contact__c=ContactId;
    //BS.Rating_given_by_Lead__c=LeadId;
    speakerBSS.add(BSS); 
    insert BSS;
    
        VUEventSurveyController.breakOutInputData BOIT=new VUEventSurveyController.breakOutInputData();
        BOIT.speakerId=VUSpeakerId;
        BOIT.metExpectationRating=5;
        BOIT.compellingInformationRating=4;
        BOIT.effectiveCommunicatorRating=3;
        BOIT.comments='hello world';
        BOIT.isAttended='Yes';
        BOIT.UserId=UserId;
        BOIT.LeadId=LeadId;
        BOIT.ContactId=ContactId;
        List<VUEventSurveyController.breakOutInputData> dataValue = new List<VUEventSurveyController.breakOutInputData>();
        dataValue.add(BOIT);
       
        VUEventSurveyController.SurveyQuestionResp SQR=new VUEventSurveyController.SurveyQuestionResp(); 
        SQR.QuestionId=SurveyQustion;
        SQR.Response=SurveyRes;
        List<VUEventSurveyController.SurveyQuestionResp> DataSQR = new List<VUEventSurveyController.SurveyQuestionResp>();
        DataSQR.add(SQR);
         VUEventSurveyController.SurveyRankingResponse SRQT=new VUEventSurveyController.SurveyRankingResponse();
         SRQT.Option1='2';
         SRQT.Option2='3';
         SRQT.Option3='1';
         SRQT.Option4='5';
         SRQT.Option5='4';
         SRQT.SurveyTaker=SurveyTaker;
         SRQT.SurveyQuestionId=SurveyQustion;
       //VU_Survey_Ranking_Questions_Response__c SRR= new VU_Survey_Ranking_Questions_Response__c(Option_1__c='2',Option_2__c='1',Option_3__c='4',Survey_Taker__c=SurveyTaker,VU_Survey_Question__c=SurveyQustion);
        List<VUEventSurveyController.SurveyRankingResponse> SRQRR= new List<VUEventSurveyController.SurveyRankingResponse>();
        SRQRR.add(SRQT);
        
       // insert SRR;
      List<Breakout_Session__c> speakerBS= new List<Breakout_Session__c>();
        Breakout_Session__c BS=new Breakout_Session__c(Did_Not_Attend__c =BOIT.isAttended,Met_Expectations__c = BOIT.metExpectationRating,Compelling_Information__c =BOIT.compellingInformationRating ,Effective_Communicator__c =BOIT.effectiveCommunicatorRating,Comments__c = BOIT.comments,Rating_given_by_User__c=BOIT.UserId,Rating_given_by_Contact__c=BOIT.ContactId,Rating_given_by_Lead__c=BOIT.LeadId,VU_Speakers__c= BOIT.speakerId);
       // insert BS;
        speakerBS.add(BS); 
         insert BS;
        VUEventSurveyController.InputData SurveyInput= new VUEventSurveyController.InputData();
        SurveyInput.breakOutData=dataValue;
        SurveyInput.RankingData=SRQRR;
        SurveyInput.SurveyResp=DataSQR;
        SurveyInput.SurveyTaker=SurveyTaker;
        SurveyInput.SurveyRes=SurveyRes;
        SurveyInput.SurveyId=SurveyId;
        SurveyInput.EventId=EventId;
        SurveyInput.SessionId=SessionId;
        SurveyInput.speakerId=VUSpeakerId;
        SurveyInput.UserId=UserId;
        
         List<VUEventSurveyController.InputData> Data= new List<VUEventSurveyController.InputData>();
         Data.add(SurveyInput);
         VUEventSurveyController.createBSS(Data);
        
 }
  static testMethod void myVUEventSurveyControllerTest2() 
     {
     List<VUEventSurveyController.InputData> Data= new List<VUEventSurveyController.InputData>();
        
      //  VUEventSurveyController.createBSS(Data);
        
        VUEventSurveyController.CustomException customExp = new VUEventSurveyController.CustomException();
    }
    
    
   /* static testmethod void myVUEventSurveyControllerTest3(){
        Survey__c sur = new Survey__c();
        sur.Name = 'test';
        insert sur;
        Survey_Question__c sq = new Survey_Question__c();
        sq.Survey__c = sur.ID;
        sq.Name = 'test';
        sq.OrderNumber__c = 12;
        sq.Question__c = 'test';
        insert sq;
        
        SurveyTaker__c sTaken = new SurveyTaker__c();
        insert sTaken;
        
        VUEventSurveyController.breakOutInputData breakOutput = new VUEventSurveyController.breakOutInputData();
        list<VUEventSurveyController.breakOutInputData> listBreakOut = new list<VUEventSurveyController.breakOutInputData>();
        listBreakOut.add(breakOutput);
        
        VUEventSurveyController.InputData input = new VUEventSurveyController.InputData();
        input.breakOutData = listBreakOut;
        input.SurveyRes = sq.ID+':'+sTaken.ID+';';
        
        list<VUEventSurveyController.InputData> listInput = new list<VUEventSurveyController.InputData>();
        listInput.add(input);
        
         VUEventSurveyController.createBSS(listInput);
    } */
}
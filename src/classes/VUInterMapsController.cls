/*@RestResource(urlMapping='/InteractiveMaps/*')
    global without sharing class VUInterMapsController
    {
    @HttpGet
    global static List<Interactive_Maps_Landmarks__c> getBlob()
    {
    List<Interactive_Maps_Landmarks__c> a ;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('IMID') ;
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
   a = [SELECT Name,(SELECT ID,Name FROM Attachments),Event_Webinar__c,Language__c,(SELECT ID,Name,X_Value__c,Y_value__c FROM AddInteractiveMapPoints__r) FROM Interactive_Maps_Landmarks__c where Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
     }
      else
      {
      a = [SELECT Name,(SELECT ID,Name FROM Attachments),Event_Webinar__c,Language__c,(SELECT ID,Name,X_Value__c,Y_value__c FROM AddInteractiveMapPoints__r) FROM Interactive_Maps_Landmarks__c where Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )];
      }

    return a;
    }   
    }*/

@RestResource(urlMapping='/InteractiveMaps/*')
    global without sharing class VUInterMapsController
    {
    @HttpGet
    global static List<Interactive_Maps_Landmarks__c> getBlob()
    {
    List<Interactive_Maps_Landmarks__c> a ;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('IMID') ;
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='Chinese(zh)'&& Language !='Japanese(ja)' && Language !='German(de)'){
   a = [SELECT Name,(SELECT ID,Name FROM Attachments),Event_Webinar__c,Language__c,(SELECT ID,Name,X_Value__c,Y_value__c FROM AddInteractiveMapPoints__r) FROM Interactive_Maps_Landmarks__c where Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
     }
      else
      {
      a = [SELECT Name,(SELECT ID,Name FROM Attachments),Event_Webinar__c,Language__c,(SELECT ID,Name,X_Value__c,Y_value__c FROM AddInteractiveMapPoints__r) FROM Interactive_Maps_Landmarks__c where Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )];
      }

    return a;
    }   
    }
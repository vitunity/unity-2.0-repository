@RestResource(urlMapping='/VULivePoolingLinks/*')
   global class VULivePoolingLinksController 
    
    {
    @HttpGet
    global static List<VU_Live_Polling_Link__c> getBlob() 
    {
    List<VU_Live_Polling_Link__c> a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    
   String Language= RestContext.request.params.get('Language') ;
  if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' &&Language !='German(de)'){
   a =[ SELECT Name,ID,Enable__c,Language__c,Web_Link__c FROM VU_Live_Polling_Link__c WHERE Language__c='English(en)' OR Language__c=''];
    System.debug('+++++++++++++++a'+a);
    }
    else{
    a =[ SELECT Name,ID,Enable__c,Language__c,Web_Link__c FROM VU_Live_Polling_Link__c WHERE Language__c=:Language];
    }
    return a;
    
     } 
    }
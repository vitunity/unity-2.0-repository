@RestResource(urlMapping='/VUProductAttachment/*')
    global class VUProductAttachmentController 
    {
    @HttpGet
    
    global static List<Attachment> getBlob() 
    {
     RestRequest req = RestContext.request;
     RestResponse res = RestContext.response;
     res.addHeader('Content-Type', 'application/json');
     res.addHeader('Access-Control-Allow-Origin', '*');
     res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
     String Id= RestContext.request.params.get('ProductId') ;
     List<Attachment> lstAttach = new List<Attachment>();
     lstAttach = [Select Name,Body, ContentType from attachment where parentID=:Id];
     return lstAttach;
    } 
       
    }
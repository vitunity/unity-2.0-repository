/*@RestResource(urlMapping='/VUProductSearch/*')
global class VUProductSearch
{
    @HttpPost
    global static List<List<SObject>> getProduct(String searchTerm,String Language){
         RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
   res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Content-Type', 'application/json');
   res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH');
   res.addHeader('Access-Control-Allow-Origin','*');
    res.addHeader('Cache-Control','no-cache');
    res.addHeader('Access-Control-Max-Age','1728000');
     res.addHeader('Accept', '*');
     res.addHeader('Content-Type','application/json');
      res.addHeader('Accept','application/json');
     res.addHeader('Access-Control-Allow-Methods','GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
     String searchStr1 =searchTerm+'*';
     String st='English(en)';
      String st1='';
     String searchQuery;
       if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
       searchQuery = 'FIND \''+searchStr1+'\' IN ALL FIELDS RETURNING VU_Products__c(Id,Name,Product_Description__c,Language__c,Product_Image__c, StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE (Language__c=:st OR Languages__c=st1)), VU_Treatment_Technique__c(Id,Name,Description__c,Languages__c,Treatment_Technique_Image__c,StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE (Languages__c=:st OR Languages__c=:st1)), VU_Solutions__c(Id,Name,Solution_Details__c,Languages__c,Playlist_Id__c WHERE (Languages__c=:st OR Languages__c=:st1)),Attachment(Id,Name)';
//List<List<SObject>> searchList = search.query(searchQuery);
       }
       else
       {
             searchQuery = 'FIND \''+searchStr1+'\' IN ALL FIELDS RETURNING VU_Products__c(Id,Name,Product_Description__c,Language__c,Product_Image__c, StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE Language__c=:Language), VU_Treatment_Technique__c(Id,Name,Description__c,Languages__c,Treatment_Technique_Image__c,StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE Languages__c=:Language), VU_Solutions__c(Id,Name,Solution_Details__c,Languages__c,Playlist_Id__c WHERE Languages__c=:Language),Attachment(Id,Name)';
//List<List<SObject>> searchList = search.query(searchQuery);
       }
              List<List<SObject>> searchList = search.query(searchQuery);
        return searchList;
    }
}*/
@RestResource(urlMapping='/VUProductSearch/*')
global class VUProductSearch
{
    @HttpPost
    global static List<List<SObject>> getProduct(String searchTerm,String Language){
         RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
   /*res.addHeader('Access-Control-Allow-Origin', '*');
   res.addHeader('Content-Type', 'application/json');
   res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,PATCH');*/
   res.addHeader('Access-Control-Allow-Origin','*');
   res.addHeader('Cache-Control','no-cache');
    res.addHeader('Access-Control-Max-Age','1728000');
     res.addHeader('Accept', '*');
     res.addHeader('Content-Type','application/json');
      res.addHeader('Accept','application/json');
     res.addHeader('Access-Control-Allow-Methods','GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS');
     res.addHeader('Access-Control-Allow-Headers','Content-Type, Accept');
     String searchStr1 =searchTerm+'*';
     String st='English(en)';
     String st1='';
     String searchQuery;
       if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' && Language !='German(de)'){
       searchQuery = 'FIND \''+searchStr1+'\' IN ALL FIELDS RETURNING VU_Products__c(Id,Name,Product_Description__c,Language__c,Product_Image__c, StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE (Languages__c=:st OR Languages__c=:st1)), VU_Treatment_Technique__c(Id,Name,Description__c,Languages__c,Treatment_Technique_Image__c,StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE (Languages__c=:st OR Languages__c=:st1))), VU_Solutions__c(Id,Name,Solution_Details__c,Languages__c,Playlist_Id__c WHERE (Languages__c=:st OR Languages__c=:st1))),Attachment(Id,Name)';
//List<List<SObject>> searchList = search.query(searchQuery);
       }
       else
       {
             searchQuery = 'FIND \''+searchStr1+'\' IN ALL FIELDS RETURNING VU_Products__c(Id,Name,Product_Description__c,Language__c,Product_Image__c, StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE Language__c=:Language), VU_Treatment_Technique__c(Id,Name,Description__c,Languages__c,Treatment_Technique_Image__c,StoreImageUrl__c,Playlist_Id__c,RecordType.Name WHERE Languages__c=:Language), VU_Solutions__c(Id,Name,Solution_Details__c,Languages__c,Playlist_Id__c WHERE Languages__c=:Language),Attachment(Id,Name)';
//List<List<SObject>> searchList = search.query(searchQuery);
       }
              List<List<SObject>> searchList = search.query(searchQuery);
        return searchList;
    }
}
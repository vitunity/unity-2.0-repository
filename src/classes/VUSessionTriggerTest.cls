@istest
public class VUSessionTriggerTest {
    static testMethod void VUSessionTriggerValid() {
        
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u = new User(Alias = 'VariUsr', Email='Varianduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='Varianduser@testorg.com');

      System.runAs(u) {
          Map <String,Schema.RecordTypeInfo> recordTypeMap = Event_Webinar__c.sObjectType.getDescribe().getRecordTypeInfosByName();
        
          Event_Webinar__c eWeb = new Event_Webinar__c();
              eWeb.RecordTypeId= recordTypeMap.get('Events').getRecordTypeId();
              eWeb.Active__c = true;
              eWeb.Time_Zone__c = '(GMT-05:00) Eastern Standard Time (America/New_York)';
              eWeb.TimeZones__c = 'America/New_York';
              eWeb.Varian_Unite_Event__c = true;
              eWeb.From_Date__c = Date.parse('8/10/2016');
              eWeb.To_Date__c = Date.parse('8/15/2016');
            
          insert eWeb;            
      
          VU_Event_Schedule__c veSch = new VU_Event_Schedule__c();
              veSch.Name = 'Welcome';
              veSch.Date__c = Date.parse('12/15/2016');
              veSch.End_Time__c = System.today();
              veSch.Start_Time__c  = System.today();
              veSch.Event_Webinar__c = eWeb.Id;
            
         insert veSch;
            
         veSch.End_Time__c = System.today() + 10;
         update veSch;
         
        }  
    }
}
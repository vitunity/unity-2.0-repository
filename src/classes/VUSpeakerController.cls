/*@RestResource(urlMapping='/VUSpeakers/*')
global class VUSpeakerController {
    @HttpGet
     
    global static List<VU_Speaker_Event_Map__c  > getBlob() {
        List<VU_Speaker_Event_Map__c  > a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language') ;
   if(Language !='English(en)' && Language !='German(de)' && Language !='Spanish(es)' && Language !='Chinese(zh)' && Language !='French(fr)'  && Language !='Japanese(jp)' && Language !='Portuguese(pt-BR)' ){
   a = [SELECT Event_Webinar__c,VU_Speakers__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.LastName__c,VU_Speakers__r.Company__c,VU_Speakers__r.Title__c,VU_Speakers__r.Designation__c,VU_Speakers__r.Description__c,VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM  VU_Speaker_Event_Map__c  WHERE Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
   }
   else
   {
a = [SELECT Event_Webinar__c,VU_Speakers__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.LastName__c,VU_Speakers__r.Company__c,VU_Speakers__r.Title__c,VU_Speakers__r.Designation__c,VU_Speakers__r.Description__c,VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM  VU_Speaker_Event_Map__c  WHERE Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )];   
   }
    return a;
     } 
   }  */
   
   @RestResource(urlMapping='/VUSpeakers/*')
global class VUSpeakerController {
    @HttpGet
     
    global static List<VU_Speaker_Event_Map__c  > getBlob() {
        List<VU_Speaker_Event_Map__c  > a;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String Id = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language') ;
   if(Language !='English(en)' && Language !='Chinese(zh)'){
   a = [SELECT Event_Webinar__c,VU_Speakers__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.LastName__c,VU_Speakers__r.Company__c,VU_Speakers__r.Title__c,VU_Speakers__r.Designation__c,VU_Speakers__r.Description__c,VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM  VU_Speaker_Event_Map__c  WHERE Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
   }
   else
   {
a = [SELECT Event_Webinar__c,VU_Speakers__c,VU_Speakers__r.Name,VU_Speakers__r.Phone__c,VU_Speakers__r.Email__c,VU_Speakers__r.LastName__c,VU_Speakers__r.Company__c,VU_Speakers__r.Title__c,VU_Speakers__r.Designation__c,VU_Speakers__r.Description__c,VU_Speakers__r.Speaker_Image__c,VU_Speakers__r.City__c,VU_Speakers__r.Country__c  FROM  VU_Speaker_Event_Map__c  WHERE Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )];   
   }
    return a;
     } 
   }
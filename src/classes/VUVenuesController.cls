@RestResource(urlMapping='/VUVenues/*')
     global class VUVenuesController 
     {
     @HttpGet
    global static List<VU_Venue__c> getBlob() 
    {
    
    List<VU_Venue__c> a ;
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    res.addHeader('Content-Type', 'application/json');
    res.addHeader('Access-Control-Allow-Origin', '*');
    res.addHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
    //String ID = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    String Id= RestContext.request.params.get('EventId') ;
    String Language= RestContext.request.params.get('Language');
   if(Language !='English(en)' && Language !='Chinese(zh)' && Language !='Japanese(ja)' && Language !='German(de)'){  
     a = [SELECT Address__c,Info__c,City__c,State__c,Zip_Postal_Code__c,Language__c,Location__c,Country__c,Name,Website__c,Description__c FROM VU_Venue__c where Event_Webinar__c=:Id AND (Event_Webinar__r.Languages__c='English(en)' OR Event_Webinar__r.Languages__c='' OR Event_Webinar__r.Event_Accessibility__c includes ('English(en)'))];
   system.debug('====TEST Class END'+a);
    }
    
    else{
      a = [SELECT Address__c,Info__c,City__c,State__c,Zip_Postal_Code__c,Language__c,Location__c,Country__c,Name,Website__c,Description__c FROM VU_Venue__c where Event_Webinar__c=:Id AND  (Event_Webinar__r.Languages__c=:Language OR Event_Webinar__r.Event_Accessibility__c includes (:Language) )];

    }
     system.debug('====TEST Class END'+a);
     return a;
     
    }
     
    }
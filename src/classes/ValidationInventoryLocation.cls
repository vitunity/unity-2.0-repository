public class ValidationInventoryLocation{
    public static void checkValidLocation(List<SVMXC__Service_Order__c> lstNew, map<Id,SVMXC__Service_Order__c> mapOld){
        set<Id> setLocation = new set<Id>();
        Id FieldRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        Id EquipmentRecordTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId();
        for(SVMXC__Service_Order__c order : lstNew){
            if(order.Inventory_Location__c <> null && order.RecordTypeId == FieldRecordTypeId &&  (mapOld == null || (mapOld <> null && order.Inventory_Location__c <> mapOld.get(order.Id).Inventory_Location__c)
            ))    setLocation.add(order.Inventory_Location__c);            
        }
        if(!setLocation.isEmpty()){
            map<Id,SVMXC__Site__c> mapLocation = new map<Id,SVMXC__Site__c>([select Id,RecordTypeId from SVMXC__Site__c where Id IN: setLocation]);
            for(SVMXC__Service_Order__c order : lstNew){
                if(order.Inventory_Location__c <> null && order.RecordTypeId == FieldRecordTypeId && (mapOld == null || (mapOld <> null && order.Inventory_Location__c <> mapOld.get(order.Id).Inventory_Location__c))){
                    if(mapLocation.containsKey(order.Inventory_Location__c) && mapLocation.get(order.Inventory_Location__c).RecordTypeId == EquipmentRecordTypeId){
                        order.addError('Inventory Location on Work Order may not be an Equipment/Tool Location.  Please select the Technician�s correct Inventory Location.');
                    }
                }
            }
        }
    }
}
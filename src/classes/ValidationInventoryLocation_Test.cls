@isTest
public class ValidationInventoryLocation_Test {
    public static testmethod void UnitTest(){
        CountryDateFormat__c dateFormat = new CountryDateFormat__c(Name='Default',Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat; 
        Id FieldRecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        Id EquipmentRecordTypeId = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName().get('Equipment/Tools').getRecordTypeId();
        Country__c con = new Country__c(Name = 'United States');
        insert con;
        
        Account acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id, BillingCity = 'Milpitas'); 
        insert acc;
        
        Contact c = new Contact (FirstName = 'test', Email = 'test@gmail.com', AccountId = acc.id, LastName = 'dssssaf');
        insert c;
        
        SVMXC__Site__c location = new SVMXC__Site__c(Sales_Org__c = 'testorg', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = acc.id,RecordTypeId = EquipmentRecordTypeId);
        insert location;
        
        Case casealias = new Case(Subject = 'testsubject', Priority='High');
        insert casealias;
        
        try{
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Inventory_Location__c = location.id,SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id,ERP_Service_Order__c = '12345678',RecordTypeId = FieldRecordTypeId);
            insert wo;
        }catch(Exception e){
            String message = e.getMessage();
            System.assert(message.contains('Inventory Location on Work Order may not be an Equipment/Tool Location'), message);
        }
    }
}
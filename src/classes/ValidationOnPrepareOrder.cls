/**************************************************************************\
@Created Date- 07/06/2017
@Created By - Rakesh Basani   
@Description - INC4202831 - Validation On Prepare Order button for PLC Data.
Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
11/06/2017 - Rakesh - INC4723866 - Added Section number to the static message coming from PLC data.
/**************************************************************************/
global class ValidationOnPrepareOrder{
    
    webService static string check(string quoteid, String region,string country) {
        
        if(region == null)return '';
        if(country == null)
        {
            map<string,List<string>> mapDependentCountries = DependentPickListValueController.GetDependentOptions('PLC_Data__c','Region__c','country__c'); 
            if(mapDependentCountries.containsKey(region)){
                for(string r : mapDependentCountries.get(region)){
                    if(country == null)country = r;
                    else country += ';'+r;
                }
            }
        } 
        List<PLC_Data__c> lstLPCData = [Select Id, Message__c, Product__r.Name,Product__r.BigMachines__Part_Number__c,Product__c,Product_Replacement_Part__r.Name,Product_Replacement_Part__r.BigMachines__Part_Number__c from PLC_Data__c 
        where Start_Date__c <=: system.today() and (End_Date__c >=: system.today() or End_Date__c = null) and (Region__c = 'WW' or ( Region__c =: region and country__c  Includes (:country)))];  
        if(lstLPCData.size()>0){
            map<string,PLC_Data__c> mapPLCData = new map<string,PLC_Data__c>();
            set<string> setProductQuota = new set<string>();
            set<string> setPartNumberMessage = new set<string>();
            for(PLC_Data__c p: lstLPCData){
                //if(p.Product__c <> null && p.Product__r.BigMachines__Part_Number__c <> null){
                if(p.Product__c <> null){
                    setProductQuota.add(p.Product__r.BigMachines__Part_Number__c);
                }
                if(p.Message__c <> null && p.Message__c <> ''){
                    if(p.Message__c.containsIgnoreCase(' Replaced by ') && (p.Message__c).split(' Replaced by ').size() == 2){
                        setPartNumberMessage.add((p.Message__c).split(' Replaced by ')[0]);
                        setPartNumberMessage.add((p.Message__c).split(' Replaced by ')[1]);
                    }
                }
            }
            
            
            if(setProductQuota.size() >0){
                List<BigMachines__Quote_Product__c> lstProdQuote = [select Id,Name,BigMachines__Quote__c,Section_Number__c from BigMachines__Quote_Product__c where (BigMachines__Quote__c =: quoteid and Name IN: setProductQuota) or name IN: setPartNumberMessage];
                string rtstr='';
                if(lstProdQuote.size()>0){
                    map<string,BigMachines__Quote_Product__c> mapProductQuota = new map<string,BigMachines__Quote_Product__c>(); 
                    map<string,BigMachines__Quote_Product__c> mapMessagePartNumberQuota = new map<string,BigMachines__Quote_Product__c>(); 
                    for(BigMachines__Quote_Product__c b : lstProdQuote){
                        if(setProductQuota.contains(b.name) && b.BigMachines__Quote__c == quoteid)mapProductQuota.put(b.name,b);  
                        if(setPartNumberMessage.contains(b.name))mapMessagePartNumberQuota.put(b.name,b); 
                    } 
                    for(PLC_Data__c PLCObj: lstLPCData){
                        if(PLCObj.Message__c <> null && PLCObj.Message__c <> ''){
                            string SerialNumber='';
                            boolean isSerialNumberPresent = false;
                            if(PLCObj.Message__c.containsIgnoreCase(' Replaced by ') && (PLCObj.Message__c).split(' Replaced by ').size() == 2){
                                string fromPartNumber = (PLCObj.Message__c).split(' Replaced by ')[0];
                                string toPartNumber = (PLCObj.Message__c).split(' Replaced by ')[1];
                                isSerialNumberPresent = (mapMessagePartNumberQuota.containsKey(fromPartNumber))?true:false;
                                SerialNumber = (mapMessagePartNumberQuota.containsKey(fromPartNumber))?string.valueOf(mapMessagePartNumberQuota.get(fromPartNumber).Section_Number__c):'';
                                SerialNumber = '. Please reconfigure Section '+SerialNumber ;
                            }
                            if(PLCObj.Product__c <> null && !isSerialNumberPresent){
                                if(mapProductQuota.ContainsKey(PLCObj.Product__r.BigMachines__Part_Number__c)){
                                    BigMachines__Quote_Product__c b = mapProductQuota.get(PLCObj.Product__r.BigMachines__Part_Number__c);
                                    SerialNumber = '. Please reconfigure Section '+b.Section_Number__c ;
                                }
                            }
                            rtstr += PLCObj.Message__c+SerialNumber+'<br/>';
                        }
                        else if(PLCObj.Product__c <> null){
                            if(mapProductQuota.ContainsKey(PLCObj.Product__r.BigMachines__Part_Number__c)){
                                BigMachines__Quote_Product__c b = mapProductQuota.get(PLCObj.Product__r.BigMachines__Part_Number__c);
                                if(PLCObj.Product_Replacement_Part__c <> null)rtstr += PLCObj.Product__r.BigMachines__Part_Number__c+' '+  PLCObj.Product__r.Name +' must be replaced with '+PLCObj.Product_Replacement_Part__r.BigMachines__Part_Number__c +' '+  PLCObj.Product_Replacement_Part__r.Name +'. Please reconfigure Section '+b.Section_Number__c+'.<br/>';
                                else rtstr +='Reconfigure ' +PLCObj.Product__r.BigMachines__Part_Number__c +' '+  PLCObj.Product__r.Name +' on Section '+b.Section_Number__c+'<br/>'; 
                            }
                        }
                    }
                }
                if(rtstr <> '')rtstr +='<br/>*** Reconfirm pricing after update is made.<br/>*** Submitting order without changes will cause EPOT failure.';
                return rtstr;
            }
            
        }
        return '';
    } 
}
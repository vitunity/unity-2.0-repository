@isTest(seeAllData=true)
private class ValidationOnPrepareOrderTest {
    
    static testMethod void PLCDataTest(){
        Test.StartTest(); 
            Test.setMock(HTTpCalloutMock.class,new PLCMock()); 
            BigMachines__Quote__c bq = [select id from BigMachines__Quote__c  limit 1];
            
            Product2 p = new Product2();
            p.Name = 'TestPLC';
            p.BigMachines__Part_Number__c = 'testpn';
            insert p;
            
            PLC_Data__c plc = new PLC_Data__c();
            plc.Start_Date__c = system.today();
            plc.End_Date__c = system.today();
            plc.Product__c = p.id;
            plc.Product_Replacement_Part__c = p.id;
            plc.Region__c = 'WW';
            insert plc; 
                
            BigMachines__Quote_Product__c qp = new BigMachines__Quote_Product__c();
            qp.Name = 'testpn';
            qp.BigMachines__Quote__c = bq.id;
            qp.BigMachines__Product__c = p.id;
            qp.Header__c = true;
            qp.Product_Type__c = 'Sales';
            qp.Standard_Price__c = 40000;
            qp.BigMachines__Sales_Price__c = 40000;
            qp.BigMachines__Quantity__c = 1;
            qp.Line_Number__c= 12;
            qp.EPOT_Section_Id__c = 'TEST';
            qp.Subscription_Start_Date__c = Date.today();
            qp.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
            qp.Number_Of_Years__c = '3';
            qp.Subscription_End_Date__c = Date.today().addYears(3);
            qp.Billing_Frequency__c = 'Annually';
            qp.Subscription_Sales_Type__c = 'New';
            qp.Installation_Price__c = 0.0;
            qp.Subscription_Unit_Price__c = 2900;       
            insert qp;
            ValidationOnPrepareOrder.check(bq.id,'NA',null);
        Test.Stoptest();
    }   
    static testMethod void PLCDataMessageTest(){
        Test.StartTest(); 
            Test.setMock(HTTpCalloutMock.class,new PLCMock()); 
            BigMachines__Quote__c bq = [select id from BigMachines__Quote__c  limit 1];
            
            Product2 p = new Product2();
            p.Name = 'TestPLC';
            p.BigMachines__Part_Number__c = 'testpn';
            insert p;
            
            PLC_Data__c plc = new PLC_Data__c();
            plc.Start_Date__c = system.today();
            plc.End_Date__c = system.today();
            plc.Product__c = p.id;
            plc.Message__c = 'testpn Replaced By testpn';
            plc.Product_Replacement_Part__c = p.id;
            plc.Region__c = 'WW';
            insert plc; 
                
            BigMachines__Quote_Product__c qp = new BigMachines__Quote_Product__c();
            qp.Name = 'testpn';
            qp.BigMachines__Quote__c = bq.id;
            qp.BigMachines__Product__c = p.id;
            qp.Header__c = true;
            qp.Product_Type__c = 'Sales';
            qp.Standard_Price__c = 40000;
            qp.BigMachines__Sales_Price__c = 40000;
            qp.BigMachines__Quantity__c = 1;
            qp.Line_Number__c= 12;
            qp.EPOT_Section_Id__c = 'TEST';
            qp.Subscription_Start_Date__c = Date.today();
            qp.Add_On_Start_Date__c = Date.today().addMonths(15).toStartOfMonth();
            qp.Number_Of_Years__c = '3';
            qp.Subscription_End_Date__c = Date.today().addYears(3);
            qp.Billing_Frequency__c = 'Annually';
            qp.Subscription_Sales_Type__c = 'New';
            qp.Installation_Price__c = 0.0;
            qp.Subscription_Unit_Price__c = 2900;       
            insert qp;
            ValidationOnPrepareOrder.check(bq.id,'NA',null);
        Test.Stoptest();
    }   
    
    public class PLCMock implements HTTpCalloutMock{
        
        public HTTPResponse respond(HTTPRequest req) {
            String bmSessionID = '12345';
            String response =  '<bm:sessionId>'+bmSessionID+'</bm:sessionId><bm:success>';
            HTTPResponse res=new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');            
            res.setBody(response);            
            res.setStatusCode(200);
            return res;            
        }
    }
}
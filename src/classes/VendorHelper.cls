public class VendorHelper{
    public static void  AddRemoveVendorUser(List<Contact> lstContact, map<Id,Contact> oldMap) {

        List<Contact> lstFilteredContact = new List<Contact>();
        for(Contact c : lstContact){
            if(oldMap != null && c.Vendor_User__c <> oldMap.get(c.Id).Vendor_User__c){
                lstFilteredContact.add(c);                
            }  
        }
        
        if(lstFilteredContact.size()>0){
            set<Id> setActiveVarianUser = new set<Id>();
            
            for(User u: [select Id,ContactID from User where ContactID IN: lstFilteredContact and isActive = true and IsPortalEnabled = true]){
                setActiveVarianUser.add(u.ContactId);
            }
            
            set<Id> stModelAnalyticsId = new set<Id>();
            set<Id> stModelAnalyticsInactivate = new set<Id>();
            
            for(Contact c : lstFilteredContact){
                if(setActiveVarianUser.contains(c.id)){
                    if(c.Vendor_User__c)stModelAnalyticsId.add(c.Id);    
                    else stModelAnalyticsInactivate.add(c.Id);                      
                }  
            }
            if(stModelAnalyticsId.size() > 0)
            {
                ID jobID = System.enqueueJob(new ModelAnalytics(stModelAnalyticsId,'PUT',Label.Vendor_Group));
            }
            if(stModelAnalyticsInactivate.size() > 0 )
            {
                ID jobID = System.enqueueJob(new ModelAnalytics(stModelAnalyticsInactivate,'DELETE',Label.Vendor_Group));
            } 
        }       
    }
}
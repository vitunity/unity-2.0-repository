/* 
 * @Author -  Amitkuamr Katre
 * Product Comsumtion from Work Order //US4318 
 */
public class WOProductStockHistoryController{
    
    @testvisible list<SVMXC__Stock_History__c> insertStockHistoryList;
    Map<ID, Map<id,SVMXC__Service_Order_Line__c>> workdetails;
    list<SVMXC__Service_Order__c> workOrderList; 
    map<Id,SVMXC__Service_Order__c> oldWorkOrderMap;
    @testvisible map<String, SVMXC__Product_Stock__c> psMap;
    @testvisible map<id,SVMXC__Product_Serial__c> serialMap; 
    @testvisible list<SVMXC__Product_Serial__c> moveSerialList;
    static set<Id>  poApplicableRecordTypesIds = new set<Id>();
    public static Id polShipmentRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__RMA_Shipment_Line__c').get('Shipment');
    @testvisible static Id fieldServiceRecordTypeId ;
    @testvisible static Id installationRecordTypeId ;
    
    static {
        fieldServiceRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
        installationRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Installation');
        poApplicableRecordTypesIds.add(fieldServiceRecordTypeId );
        poApplicableRecordTypesIds.add(installationRecordTypeId);

    }

    public WOProductStockHistoryController(list<SVMXC__Service_Order__c> workOrderList,
                                           Map<ID, Map<id,SVMXC__Service_Order_Line__c>> workdetails,
                                           map<Id,SVMXC__Service_Order__c> oldWorkOrderMap){
        this.workOrderList = workOrderList;
        this.workdetails = workdetails;
        this.oldWorkOrderMap = oldWorkOrderMap;
        insertStockHistoryList = new list<SVMXC__Stock_History__c>();
        serialMap = new Map<id,SVMXC__Product_Serial__c> ();
        moveSerialList = new list<SVMXC__Product_Serial__c> ();
    }
    

    /* 
     * DE7512
     * SIT FS: Work Order to Review W/ Outstanding Parts Order Lines
     */
    public void validateRemaningQty(){
        set<Id> polLessThan100_dollerId = new set<Id>();
        set<Id> woIds = new set<Id> ();
        for(SVMXC__Service_Order__c wo : workOrderList){
            if(oldWorkOrderMap!=null && oldWorkOrderMap.get(wo.Id).SVMXC__Order_Status__c != wo.SVMXC__Order_Status__c 
                    && (wo.SVMXC__Order_Status__c == 'Reviewed' || wo.SVMXC__Order_Status__c == 'Approved' || wo.SVMXC__Order_Status__c == 'Closed')
                    && poApplicableRecordTypesIds.contains(wo.RecordTypeId))
            woIds.add(wo.id);
        }

        
        if(!woIds.isEmpty()){
            map<Id,SVMXC__Service_Order__c> woMap = new map<Id,SVMXC__Service_Order__c>([select (select Name, Remaining_Qty__c,
                                                     SVMXC__Product__r.SVMXC__Product_Cost__c,
                                                     SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                                                     SVMXC__Service_Order__c,
                                                     General_Stock__c                                                     
                                                     from SVMXC__RMA_Shipment_Line__r 
                                                     where RecordTypeId =: polShipmentRecordTypeId
                                                     and Consumed__c = false
                                                     and Remaining_Qty__c > 0
                                                     )
                                                from SVMXC__Service_Order__c
                                                where id in : woIds and RecordTypeId in : poApplicableRecordTypesIds]);
            if(!woMap.isEmpty()){ 
                for(SVMXC__Service_Order__c wo : workOrderList){
                    if(oldWorkOrderMap!=null && oldWorkOrderMap.get(wo.Id).SVMXC__Order_Status__c != wo.SVMXC__Order_Status__c 
                        && (wo.SVMXC__Order_Status__c == 'Reviewed' || wo.SVMXC__Order_Status__c == 'Approved' || wo.SVMXC__Order_Status__c == 'Closed')
                        && poApplicableRecordTypesIds.contains(wo.RecordTypeId)){
                        SVMXC__Service_Order__c woTest = woMap.get(wo.Id);
                        System.debug('Anydatatype_msg=====woTest====='+woTest);
                        Decimal  rqty = 0.0;
                        if(woTest!= null && woTest.SVMXC__RMA_Shipment_Line__r != null && !woTest.SVMXC__RMA_Shipment_Line__r.isEmpty()){
                            list<String> lines = new list<String>();
                            for(SVMXC__RMA_Shipment_Line__c pol : woTest.SVMXC__RMA_Shipment_Line__r){
                                if(pol.General_Stock__c){
                                    if(wo.SVMXC__Order_Status__c == 'Closed')
                                    polLessThan100_dollerId.add(pol.id);
                                }else{
                                    if(wo.RecordTypeId == fieldServiceRecordTypeId) {
                                        lines.add(pol.Name+' = '+pol.Remaining_Qty__c);
                                    }
                                    if(wo.RecordTypeId == installationRecordTypeId && (wo.Close__c || wo.SVMXC__Order_Status__c == 'Closed')) {
                                        lines.add(pol.Name+' = '+pol.Remaining_Qty__c);
                                    }
                                }
                            }
                            if(!lines.isEmpty()){
                                wo.addError('The following parts order line still have remaining quantity.  Use Return FE parts to return parts. \n'+lines);
                            }
                            
                        }
                    }
                    
                }
            }
        }

        if(!polLessThan100_dollerId.isEmpty()){
            createStockHistoryOther(polLessThan100_dollerId);
        }
    } 

    /* Create Product Stock History */
    public void createStockHistory(){
        Set<Id> locationIds = new Set<Id>();   
        Set<Id> productIds = new Set<Id>();
        Set<Id> polids = new Set<Id>();
        Set<Id> productStockIds = new Set<Id>();
        
        for(SVMXC__Service_Order__c varWO : workOrderList){
            if(oldWorkOrderMap!=null 
               && oldWorkOrderMap.get(varWO.Id).SVMXC__Order_Status__c != varWO.SVMXC__Order_Status__c 
               && varWO.SVMXC__Order_Status__c == 'Approved'
               && poApplicableRecordTypesIds.contains(varWO.RecordTypeId) ){
                for(SVMXC__Service_Order_Line__c varWD : workdetails.get(varWO.Id).values()){
                    System.debug('Anydatatype_msg===varWD===='+varWD);
                    if(varWD.SVMXC__Line_Type__c == 'Parts' 
                       && varWD.SVMXC__Consumed_From_Location__c != null 
                       //&& varWD.SVMXC__Consumed_From_Location__r.SVMXC__Stocking_Location__c == true 
                       && varWD.Part_Disposition__c == 'Installed')
                       {
                        if(varWD.Part_Disposition__c == 'Installed' && varWD.Parts_Order_Line__c != null){
                            locationIds.add(varWD.SVMXC__Consumed_From_Location__c);   //set WD consumed from loc
                            productIds.add(varWD.SVMXC__Product__c);  //set WD product
                            polids.add(varWD.Parts_Order_Line__c);
                        }
                        if(varWD.Part_Disposition__c == 'Installed' && varWD.Product_Stock__c != null){
                            productStockIds.add(varWD.Product_Stock__c);
                        }
                    }
                }
            }
        } 
        
        if(!polids.isEmpty() || !productStockIds.isEmpty()){
            //get product stock
            psMap = GetPsMap(productIds,locationIds,productStockIds);
            //get parts order line
            map<Id,SVMXC__RMA_Shipment_Line__c> partsOrderLineMap = GetpartsOrderLineMap(polids);
            for(SVMXC__Service_Order__c varWO : workOrderList){
                if(oldWorkOrderMap!=null && oldWorkOrderMap.get(varWO.Id).SVMXC__Order_Status__c != varWO.SVMXC__Order_Status__c 
                    && varWO.SVMXC__Order_Status__c == 'Approved'){
                    for(SVMXC__Service_Order_Line__c varWD : workdetails.get(varWO.Id).values()){
                        if(varWD.SVMXC__Line_Type__c == 'Parts' 
                           && varWD.SVMXC__Consumed_From_Location__c != null 
                           && varWD.SVMXC__Line_Status__c != 'Closed' 
                           && varWD.Part_Disposition__c == 'Installed')
                           {
                            if(varWD.Part_Disposition__c == 'Installed' && varWD.Parts_Order_Line__c != null ){
                                SVMXC__RMA_Shipment_Line__c pol = partsOrderLineMap.get(varWD.Parts_Order_Line__c);
                                createInTransitHistory_POL(pol,varWD,varWo);
                                createAvailableHistory_POL(pol,varWD,varWo);
                                deactivateStockSerial(pol);
                            }
                            if(varWD.Part_Disposition__c == 'Installed' && varWD.Product_Stock__c != null){
                                createAvailableHistory_PS(varWD.Product_Stock__c,varWD,varWo);
                            }
                        }
                    }
                }
            }
            insertStockHistory();
        }
        
    }

    /*
     * Move product serial to available stock
     */
    public void moveStockSerialToAvailable(SVMXC__RMA_Shipment_Line__c pol, SVMXC__Product_Stock__c ps){
        if(pol.Stocked_Serials__r != null && !pol.Stocked_Serials__r.isEmpty()){
            for(SVMXC__Product_Serial__c  ss : pol.Stocked_Serials__r){
                ss.SVMXC__Product_Stock__r  = ps;
                moveSerialList.add(ss);
            }
        }
    }

    /*
     * Update stock serial to inactive
     */
    public void deactivateStockSerial(SVMXC__RMA_Shipment_Line__c pol){
        if(pol.Stocked_Serials__r != null && !pol.Stocked_Serials__r.isEmpty()){
            for(SVMXC__Product_Serial__c ps : pol.Stocked_Serials__r){
                ps.SVMXC__Active__c =false;
                serialMap.put(ps.id,ps);
            }
        }
    }
    
    /*
     * Less than $100 product stock history
     * Decrement In Transit Product Stock
     * Increment Available Product Stock (create if not yet existing)
     */
    public void createStockHistoryOther(set<Id> polids){
        Set<Id> locationIds = new Set<Id>();   
        Set<Id> productIds = new Set<Id>();
        map<Id,SVMXC__RMA_Shipment_Line__c> partsOrderLineMap = GetpartsOrderLineMap(polids);
        list<SVMXC__RMA_Shipment_Line__c> polList = partsOrderLineMap.values();
        for(SVMXC__RMA_Shipment_Line__c pol : polList){
            locationIds.add(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c);
            productIds.add(pol.SVMXC__Product__c);
        }
        if(!locationIds.isEmpty() && !productIds.isEmpty()){
            psMap = GetPsMap(productIds,locationIds,new set<id>());
            for(SVMXC__RMA_Shipment_Line__c pol : polList){
                String psKeyIn_Available = String.valueOf(pol.SVMXC__Product__c) +'_'+
                                           String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c)+
                                           '_Available';
                if(String.isNotBlank(pol.Batch_Number__c)){
                    psKeyIn_Available = psKeyIn_Available+'_'+pol.Batch_Number__c;
                }
                SVMXC__Product_Stock__c ps = psMap.get(psKeyIn_Available);
                if(ps==null){
                  ps = CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                                pol.SVMXC__Product__c,
                                'Available',
                                0,
                                pol.Batch_Number__c);
                }
                  
                SVMXC__Stock_History__c sh_Available_Inc = CreatePSHistory('Parts Receipt','Available','Increase',pol, ps.SVMXC__Quantity2__c,
                                 ps.SVMXC__Quantity2__c + pol.Remaining_Qty__c,
                                 pol.Remaining_Qty__c,
                                 ps);  
                ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c + pol.Remaining_Qty__c;  
                psMap.put(psKeyIn_Available,ps);
                insertStockHistoryList.add(sh_Available_Inc);
                moveStockSerialToAvailable(pol,ps);
                String psKeyIn_Transit = String.valueOf(pol.SVMXC__Product__c) +'_'+
                              String.valueOf(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c)+'_In Transit';
                              
                if(String.isNotBlank(pol.Batch_Number__c)){
                    psKeyIn_Transit = psKeyIn_Transit+'_'+pol.Batch_Number__c;
                }
                SVMXC__Product_Stock__c ps2 = psMap.get(psKeyIn_Transit);
                if(ps2==null){
                  ps2 = CreateProductStock(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                              pol.SVMXC__Product__c,
                              'In Transit',
                              pol.Remaining_Qty__c,
                              pol.Batch_Number__c);
                }
                  
                SVMXC__Stock_History__c sht = CreatePSHistory('Parts Receipt','In Transit','Decrease',pol, ps2.SVMXC__Quantity2__c,
                                     ps2.SVMXC__Quantity2__c - pol.Remaining_Qty__c,
                                     pol.Remaining_Qty__c,
                                     ps2);  
                ps2.SVMXC__Quantity2__c = ps2.SVMXC__Quantity2__c - pol.Remaining_Qty__c;  
                psMap.put(psKeyIn_Transit,ps2);
                insertStockHistoryList.add(sht);
                //STRY0022329: Parts: Add 'General to Available' and Reference in Remaining Qty.
                //Remaining Qty. is moved from In Transit to Available, set field "General to Available" = the qty. moved
                pol.General_to_Available__c = pol.Remaining_Qty__c;
            }
        }
        if(!polList.isEmpty()){
            update polList;
        }
        insertStockHistory();
    }


    
    /*
     * Stock History Bulk insert and update
     */
    public void insertStockHistory(){
        if(!psMap.isEmpty()){
            upsert psMap.values();
        }

        if(!insertStockHistoryList.isEmpty()){
            for(SVMXC__Stock_History__c varSH : insertStockHistoryList){
                if(varSH.SVMXC__Product_Stock__r != null){
                    varSH.SVMXC__Product_Stock__c = varSH.SVMXC__Product_Stock__r.ID;
                }   
            }
            upsert insertStockHistoryList;
        }
        if(!serialMap.isEmpty()){
            update serialMap.values();
        }
        if(!moveSerialList.isEmpty()){
            for(SVMXC__Product_Serial__c ps : moveSerialList ){
                ps.SVMXC__Product_Stock__c  = ps.SVMXC__Product_Stock__r.Id;
            }
            update moveSerialList;
        }
    }
    
    /*
     * Create Stock History for Available Decrement 
     */
    public void createAvailableHistory_PS(String pstockId, SVMXC__Service_Order_Line__c varWD, SVMXC__Service_Order__c varWo){
        //Create Available Decrement product stock only;
        String psKeyIn_Available = String.valueOf(varWD.SVMXC__Product__c) +'_'+
                                   String.valueOf(varWD.SVMXC__Consumed_From_Location__c)+'_Available';
        if(String.isNotBlank(varWD.Batch_Number__c)){
            psKeyIn_Available = psKeyIn_Available+'_'+varWD.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(psKeyIn_Available);
        SVMXC__Stock_History__c sh_Available_Dec = CreatePSHistory('SVO Usage',
                                                   'Available',
                                                   'Decrease',
                                                   varWD,
                                                   ps.SVMXC__Quantity2__c,
                                                   ps.SVMXC__Quantity2__c-varWD.SVMXC__Actual_Quantity2__c,
                                                   varWD.SVMXC__Actual_Quantity2__c,
                                                   ps); 
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c - varWD.SVMXC__Actual_Quantity2__c;                                    
        insertStockHistoryList.add(sh_Available_Dec);
    }
    
    /*
     * Create Stock History IN-Transit
     * Decremennt
     */
    public void createInTransitHistory_POL(SVMXC__RMA_Shipment_Line__c pol, SVMXC__Service_Order_Line__c varWD, SVMXC__Service_Order__c varWo){
        //Create Available Increment and Decrement ;
        String psKeyIn_Transit = String.valueOf(varWD.SVMXC__Product__c) +'_'+
                                   String.valueOf(varWD.SVMXC__Consumed_From_Location__c)+'_In Transit';
        if(String.isNotBlank(varWD.Batch_Number__c)){
            psKeyIn_Transit = psKeyIn_Transit+'_'+varWD.Batch_Number__c;
        }
        SVMXC__Product_Stock__c pst = psMap.get(psKeyIn_Transit);
        if(pst==null){
            pst = CreateProductStock(varWD.SVMXC__Consumed_From_Location__c,
                                    varWD.SVMXC__Product__c,
                                    'In Transit',
                                    varWD.SVMXC__Actual_Quantity2__c,
                                    varWD.Batch_Number__c);  
        }
        SVMXC__Stock_History__c sh_InTransit_Dec = CreatePSHistory('Parts Receipt','In Transit','Decrease',varWD,pst.SVMXC__Quantity2__c,
                                                   pst.SVMXC__Quantity2__c - varWD.SVMXC__Actual_Quantity2__c,
                                                   varWD.SVMXC__Actual_Quantity2__c,
                                                   pst);
        pst.SVMXC__Quantity2__c = pst.SVMXC__Quantity2__c - varWD.SVMXC__Actual_Quantity2__c;
        psMap.put(psKeyIn_Transit,pst);

        insertStockHistoryList.add(sh_InTransit_Dec);
    }
    
    /*
     * Create Available Stock History
     * For Increase and Decrease
     */ 
    public void createAvailableHistory_POL(SVMXC__RMA_Shipment_Line__c pol, SVMXC__Service_Order_Line__c varWD, SVMXC__Service_Order__c varWo){
        //Create Available Increment and Decrement ;
        String psKeyIn_Available = String.valueOf(varWD.SVMXC__Product__c) +'_'+
                                   String.valueOf(varWD.SVMXC__Consumed_From_Location__c)+'_Available';
        if(String.isNotBlank(varWD.Batch_Number__c)){
            psKeyIn_Available = psKeyIn_Available+'_'+varWD.Batch_Number__c;
        }
        SVMXC__Product_Stock__c ps = psMap.get(psKeyIn_Available);
        if(ps==null){
            ps = CreateProductStock(varWD.SVMXC__Consumed_From_Location__c,
                                     varWD.SVMXC__Product__c,
                                     'Available',
                                     0,
                                     varWD.Batch_Number__c);
        }       
        SVMXC__Stock_History__c sh_Available_Inc = CreatePSHistory('Parts Receipt','Available','Increase',varWD,ps.SVMXC__Quantity2__c,
                                                   ps.SVMXC__Quantity2__c + varWD.SVMXC__Actual_Quantity2__c,
                                                   varWD.SVMXC__Actual_Quantity2__c,
                                                   ps);
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c + varWD.SVMXC__Actual_Quantity2__c;     
        SVMXC__Stock_History__c sh_Available_Dec = CreatePSHistory('SVO Usage','Available','Decrease',varWD, ps.SVMXC__Quantity2__c,
                                                   ps.SVMXC__Quantity2__c - varWD.SVMXC__Actual_Quantity2__c,
                                                   varWD.SVMXC__Actual_Quantity2__c,
                                                   ps);                                        
        ps.SVMXC__Quantity2__c = ps.SVMXC__Quantity2__c - varWD.SVMXC__Actual_Quantity2__c;
        psMap.put(psKeyIn_Available,ps);
        insertStockHistoryList.add(sh_Available_Inc);
        insertStockHistoryList.add(sh_Available_Dec);
        
    }
    
    /* 
     * Get Prats Order Lines from database
     */
    public map<Id,SVMXC__RMA_Shipment_Line__c> GetpartsOrderLineMap(set<Id> partsOrderLine){
        return new map<Id,SVMXC__RMA_Shipment_Line__c>([select Id, SVMXC__RMA_Shipment_Order__c,SVMXC__Fulfillment_Qty__c, 
                                                        SVMXC__Actual_Quantity2__c, Remaining_Qty__c, SVMXC__Product__r.SVMXC__Product_Cost__c,
                                                        SVMXC__Product__c, SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Service_Order__c, SVMXC__Service_Order__c,
                                                        SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__c,Batch_Number__c,
                                                        (select Id from Stocked_Serials__r)
                                                        from SVMXC__RMA_Shipment_Line__c
                                                        where id in : partsOrderLine]);
    }
    
    /*
     * Get Product Stock From Database -
     */
    public map<String, SVMXC__Product_Stock__c> GetPsMap(Set<id> productIds, set<Id> locationIds, set<Id> productStockIds){
        map<String, SVMXC__Product_Stock__c> psMapLocal = new map<String, SVMXC__Product_Stock__c>();
        List<SVMXC__Product_Stock__c> listProdStock = [select Id, SVMXC__Location__c, SVMXC__Product__c,SVMXC__Status__c, SVMXC__Quantity2__c,
                                                       SVMXC__ProdStockCompositeKey__c, Batch_Number__c
                                                       from SVMXC__Product_Stock__c 
                                                       where (SVMXC__Location__c in : locationIds AND SVMXC__Product__c in : productIds)
                                                       or ( Id in : productStockIds)];
        for(SVMXC__Product_Stock__c ps : listProdStock){
            psMapLocal.put(ps.SVMXC__ProdStockCompositeKey__c,ps);          
        }
        return psMapLocal;
    }
    
    /* 
     * Create Product Stock 
     */
    public SVMXC__Product_Stock__c CreateProductStock(String locationId, String ProductId, String status, Decimal qty){
        SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
        ps.SVMXC__Location__c = locationId;
        ps.SVMXC__Product__c = ProductId;
        ps.SVMXC__Status__c = status;
        ps.SVMXC__Quantity2__c = qty;
        return ps;
    }
    
    /* 
     * Create Product Stock  batch
     */
    public static SVMXC__Product_Stock__c CreateProductStock(String locationId, String ProductId, String status, Decimal qty, String batchPart){
        SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
        ps.SVMXC__Location__c = locationId;
        ps.SVMXC__Product__c = ProductId;
        ps.SVMXC__Status__c = status;
        ps.SVMXC__Quantity2__c = qty;
        if(String.isNotBlank(batchPart)){
            ps.Batch_Number__c =  batchPart;
        }
        return ps;
    }
    
    /*
     * Create Stock History from Work Detail
     */
    public SVMXC__Stock_History__c CreatePSHistory(
        String transactionType,
        String status,
        String changeType,
        SVMXC__Service_Order_Line__c wdl,
        Decimal beforeQty,
        Decimal afterQty,
        Decimal txnQty,
        SVMXC__Product_Stock__c ps){
            SVMXC__Stock_History__c sh = new SVMXC__Stock_History__c();
            sh.SVMXC__Location__c = wdl.SVMXC__Consumed_From_Location__c;
            sh.SVMXC__Date_Changed__c = wdl.Related_Activity__r.SVMXC__End_Date_and_Time__c;
            sh.SVMXC__Transaction_Type__c = transactionType;
            sh.SVMXC__Status__c = status;
            sh.SVMXC__Change_Type__c = changeType;
            sh.SVMXC__Product_Stock__r = ps;
            sh.SVMXC__Quantity_before_change2__c = beforeQty;
            sh.SVMXC__Quantity_after_change2__c = afterQty;
            sh.SVMXC__Transaction_Quantity2__c = txnQty;
            sh.SVMXC__Service_Order_Line__c = wdl.Id;
            sh.SVMXC__Product__c = wdl.SVMXC__Product__c;
            sh.SVMXC__Service_Order__c = wdl.SVMXC__Service_Order__c;
            sh.Parts_Order_Line__c = wdl.Parts_Order_Line__c;
            sh.SVMXC__Shipment_Line__c = wdl.Parts_Order_Line__c;
            return sh;
    }

    /*
     * Create Stock History From POL and Workorder
     * Less than $100 product 
     */
    public SVMXC__Stock_History__c CreatePSHistory(
        String transactionType,
        String status,
        String changeType,
        SVMXC__RMA_Shipment_Line__c pol,
        Decimal beforeQty,
        Decimal afterQty,
        Decimal txnQty,
        SVMXC__Product_Stock__c ps){
            SVMXC__Stock_History__c sh = new SVMXC__Stock_History__c();
            sh.SVMXC__Location__c = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__c;
            sh.SVMXC__Transaction_Type__c = transactionType;
            sh.SVMXC__Status__c = status;
            sh.SVMXC__Change_Type__c = changeType;
            sh.SVMXC__Product_Stock__r = ps;
            sh.SVMXC__Quantity_before_change2__c = beforeQty;
            sh.SVMXC__Quantity_after_change2__c = afterQty;
            sh.SVMXC__Transaction_Quantity2__c = txnQty;
            sh.Parts_Order_Line__c = pol.Id;
            sh.SVMXC__Product__c = pol.SVMXC__Product__c;
            sh.SVMXC__Service_Order__c = pol.SVMXC__Service_Order__c;
            sh.SVMXC__Shipment_Line__c = pol.Id;
            return sh;
    }   
}
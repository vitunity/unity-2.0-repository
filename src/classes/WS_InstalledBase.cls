/*****************************************************************************************************
* Author         : Ani Sinnakrar (VIT)
* Functionality  : This Apex class is SOAP API for BMI get list of IntalledBase for given customer id
                    
* User Story     : 
* Created On     : 10th Oct'2014
* Last Modified  : 20th Oct'2014
*
*******************************************************************************************************/
global with sharing class WS_InstalledBase {

    
    //Define an object in apex that is exposed in apex web service
    global class WrapperIBase {
            webservice String  InstallName ;
            webservice String  ProductName ;
            webservice String  ProductBuildName ;
            webservice String  ProductBuildVersion ;
            webservice String  TopLvlPCSN ;
            webservice String  TopLevel ;
            webservice String  ERPCode ; 
            webservice String  ERPProductCode ;
            webservice String  ProductFamily ;           
            webservice String  ProductCode ;
            webservice String  SAPCode ;
            webservice String  PCSN ;
            webservice String  ISCConfigName ;
            webservice String  ProductLine ;
            webservice integer num;
            webservice String lstProdVersions ;
            webservice List<ISC_Parameters__c>  lstISCParameters ;
    }
       
    // This is to get list of install products by Account id along with product versions        
    webservice static  List<WrapperIBase> getInstallBaseForUpgrades(Id pAccId ) 
    {
        // RestRequest req = RestContext.request;
        // Id          strUserId = null ;

        // String      strUserName = req.requestURI.substring( req.requestURI.lastIndexOf('/') + 1 );  
        
        // String       newStr      = 'Hello ' + name ;
        //List<iBase> lstIB ;
        
        //Our collection of the class/wrapper objects cContact
        List<WrapperIBase> lstWBase = new List<WrapperIBase>() ; 
        
        Set<String> prodVersSet = new Set<String>();
        Set<Id> grpProducts = new Set<id>();
        for (AggregateResult a: [SELECT     SVMXC__Product__c
                                            FROM    SVMXC__INSTALLED_PRODUCT__C
                                            WHERE   SVMXC__COMPANY__C = :pAccId 
                                            group by SVMXC__Product__c ])
        {
            grpProducts.add((ID)a.get('SVMXC__Product__c'));                                
        }
                                            
        List<SVMXC__INSTALLED_PRODUCT__C> lstSvcInstalledProds = [ SELECT 
                                    Id, NAME,
                                    SVMXC__PRODUCT_NAME__C, SVMXC__PRODUCT__c,
                                    PRODUCT_VERSION_BUILD_NAME__C,
                                    Product_Version_Build__r.Product_Version__r.name,
                                    TOP_LEVEL_PCSN__C,SVMXC__Top_Level__c,
                                    SVMXC__Top_Level__r.Name, ERP_PCODE__C,
                                    SVMXC__PRODUCT__R.PRODUCTCODE,
                                    SVMXC__PRODUCT__R.SAP_PCode__c,
                                    SVMXC__PRODUCT__R.ERP_Pcode__r.Name,
                                    SVMXC__PRODUCT__R.Family,
                                    SVMXC__PRODUCT__R.SVMXC__Product_Line__c,
                                    CURRENT_ISC_CONFIGURATION__R.NAME
                            FROM SVMXC__INSTALLED_PRODUCT__C
                            WHERE SVMXC__COMPANY__C = :pAccId ] ;                                           
                                            

        AggregateResult[] grpISCHist = [    SELECT  CURRENT_ISC_CONFIGURATION__C
                                            FROM    SVMXC__INSTALLED_PRODUCT__C
                                            WHERE   SVMXC__COMPANY__C = :pAccId 
                                            group by CURRENT_ISC_CONFIGURATION__C ] ;
                                            
        Map<id,String> mpPdtVrsn = new Map<Id, String>();

        String pdtv;
        
        for(Product2  p: [Select id , (Select Version__c from Product_Versions1__r where Product__c in :grpProducts) from Product2 where id in :grpProducts])   
        {
            pdtv    = '' ;
            
            prodVersSet = new Set<String>();
            
            System.debug('***pdt:'+p.id);
            for(Product_Version__c pv:p.Product_Versions1__r){
                //pdtv+= pv.Version__c+',';
                prodVersSet.add(pv.Version__c);
            }
            for(String temp : prodVersSet)
                pdtv += temp + ',';
            
            System.debug(' String : ' + pdtv  ) ;
            
            if (pdtv != null && pdtv != '' )
            {    
                pdtv = pdtv.substring(0,pdtv.length()-1);
                mpPdtVrsn.put(p.id, pdtv);
            }   
        }

//*******
        Set<Id> setISCHist = new Set<id>();
        Map<id, List<ISC_Parameters__c>> mpISCParams = new Map<id, List<ISC_Parameters__c>>();
        
        for (AggregateResult a: grpISCHist  )
            setISCHist.add((ID)a.get('CURRENT_ISC_CONFIGURATION__C'));                              

        for(ISC_History__c  p: [Select id, (Select Name, Parameter_Value__c  from ISC_Parameters__r)from ISC_History__c where id in :setISCHist ] )        
            mpISCParams.put(p.id, p.ISC_Parameters__r) ;

        for(SVMXC__INSTALLED_PRODUCT__C  objIB : lstSvcInstalledProds )
        {
            WrapperIBase objWrapperBase = new WrapperIBase() ;
            objWrapperBase.InstallName          = objIB.name ;
            objWrapperBase.ProductName          = objIB.SVMXC__PRODUCT_NAME__C ;
            objWrapperBase.ProductBuildName     = objIB.PRODUCT_VERSION_BUILD_NAME__C ;
            objWrapperBase.ProductBuildVersion  = objIB.Product_Version_Build__r.Product_Version__r.name ;
            objWrapperBase.TopLvlPCSN           = objIB.TOP_LEVEL_PCSN__C ;
            objWrapperBase.TopLevel             = objIB.SVMXC__Top_Level__r.Name;
            objWrapperBase.ERPCode              = objIB.ERP_PCODE__C ;
            objWrapperBase.ERPProductCode       = objIB.SVMXC__PRODUCT__R.ERP_Pcode__r.Name;
            objWrapperBase.ProductCode          = objIB.SVMXC__PRODUCT__R.PRODUCTCODE ;
            objWrapperBase.SAPCode              = objIB.SVMXC__PRODUCT__R.SAP_PCode__c ;
            objWrapperBase.ProductFamily        = objIB.SVMXC__PRODUCT__R.Family;
            objWrapperBase.ProductLine          = objIB.SVMXC__PRODUCT__R.SVMXC__Product_Line__c;
            objWrapperBase.ISCConfigName        = objIB.CURRENT_ISC_CONFIGURATION__R.NAME ;
            
            
    /*      List<Product_Version__c> lstProdVer = [  select     Version__c
                                                     from       Product_Version__c
                                                     where      Product__c = :objIB.SVMXC__Product__c ] ;*/

    /*      List<ISC_Parameters__c> lstISCPara = [   select     Name, 
                                                                Parameter_Value__c
                                                     from       ISC_Parameters__c
                                                     where      ISC__c = :objIB.Current_ISC_Configuration__c ] ;
    */                                                   
            if( mpPdtVrsn.containsKey(objIB.SVMXC__PRODUCT__c) )
                objWrapperBase.lstProdVersions = mpPdtVrsn.get( objIB.SVMXC__PRODUCT__c) ;
//          objWrapperBase.lstISCParameters = lstISCPara ; 
            
            if( mpISCParams.containsKey(objIB.CURRENT_ISC_CONFIGURATION__c) )
                objWrapperBase.lstISCParameters = mpISCParams.get(objIB.CURRENT_ISC_CONFIGURATION__c) ;
                     
            lstWBase.add( objWrapperBase );
        }
        return lstWBase ;
    } 
    
    // This is to get list of install products by Account number 
    webservice static List<SVMXC__Installed_Product__c> getInstallBaseForServiceContracts(string pAccNum)
    {
        List<SVMXC__Installed_Product__c> installProductList = [Select Name, SVMXC__Site__r.Name,SVMXC__Site__r.SVMXC__City__c, SVMXC__Site__r.SVMXC__Country__c,
                                                                        SVMXC__Date_Installed__c,SVMXC__Product__r.Family, SVMXC__Serial_Lot_Number__c,SLA__c, 
                                                                        SVMXC__Service_Contract_Start_Date__c, SVMXC__Service_Contract_End_Date__c, SVMXC__Product_Name__c, 
                                                                        End_of_Life__c, SVMXC__Company__r.Country__c , First_Acceptance_Date__c,SVMXC__Product__r.ProductCode, 
                                                                        SVMXC__Product__r.SAP_PCode__c, SVMXC__Service_Contract_Line__r.SVMXC__End_Date__c, 
                                                                        SVMXC__Product__r.Service_Product_Group__c, SVMXC__Parent__r.Top_Level_PCSN__c , SVMXC__Parent__r.SVMXC__Serial_Lot_Number__c,
                                                                        SVMXC__Company__r.Name,SVMXC__Company__r.Functional_Location__c 
                                                                From SVMXC__Installed_Product__c 
                                                                where SVMXC__Site__r.ERP_Site_Partner_Code__c  = :pAccNum 
                                                                AND (SVMXC__Status__c != 'Inactive' 
                                                                AND SVMXC__Status__c != 'Removed' 
                                                                AND SVMXC__Status__c != 'Obsolete')
                                                                ] ;
        
        if ( installProductList.size() > 0 )
            return installProductList ;
        else
            return null ;   
    }
    
    webservice static List<Subscription__c> getSubscriptionInstallBase(String customerId, String productType){
        List<Subscription__c> subscriptions = [
            SELECT Id,Name, ERP_Contract_Number__c, Account__c,End_Date__c,ERP_Site_Partner__c,ERP_Sales_Org__c,ERP_Sold_To__c,Ext_Quote_Number__c,
            Quote__c,Site_Partner__c,Sold_To__c,Start_Date__c,Site_Partner__r.Name,PCSN__c,Document_Type__c, Number_Of_Licences__c,Quote__r.Name,Billing_Frequency__c
            FROM Subscription__c
            WHERE Quote__r.BigMachines__Account__c =: customerId
            AND Product_Type__c =:productType order by Name desc
        ];
        
        System.debug('----customerId'+customerId+'----subscriptions'+subscriptions.size());
        return subscriptions;                               
    }  
    
    webservice static List<BigMachines__Quote_Product__c> getSubscriptionModuleDetails(String subscriptionNumber){
        return [
            SELECT Id, Name, BigMachines__Quantity__c, BigMachines__Total_Price__c, Subscription_Product_Type__c, Subscription_Part_Type__c,
            Subscription_Implementation_At_Booking__c
            FROM BigMachines__Quote_Product__c
            WHERE Subscription__r.ERP_Contract_Number__c =:subscriptionNumber
        ];
    } 
    
    webservice static ERPPartner getERPSitePartners(String partnerNumber){
        List<ERP_Partner_Association__c> erpSitePartner = [
            SELECT Id, Name, ERP_Partner__r.Name, ERP_Partner_Number__c, Partner_City__c, Partner_Country__c
            FROM ERP_Partner_Association__c
            WHERE ERP_Partner_Number__c =:partnerNumber
            AND Partner_Function__c = 'Z1=Site Partner'
            LIMIT 1
        ];
        
        List<SVMXC__Site__c> siteLocation = [
            SELECT Id, Name, ERP_Functional_Location__c
            FROM SVMXC__Site__c
            WHERE ERP_Site_Partner_Code__c =:partnerNumber
            LIMIT 1
        ];
        
        ERPPartner sitePartner = new ERPPartner();
        if(!erpSitePartner.isEmpty()){
            sitePartner.accountName = erpSitePartner[0].ERP_Partner__r.Name;
            sitePartner.city = erpSitePartner[0].Partner_City__c;
            sitePartner.country= erpSitePartner[0].Partner_Country__c;
        }
        if(!siteLocation.isEmpty()){
            sitePartner.siteName = siteLocation[0].Name;
            sitePartner.functionalLocation = siteLocation[0].ERP_Functional_Location__c;
        }
        return sitePartner;
    }
    
    global class ERPPartner{
        webservice String accountname;
        webservice String siteName;
        webservice String functionalLocation;
        webservice String city;
        webservice String country;
    }
}
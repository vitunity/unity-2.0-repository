@isTest(seeAllData=true)
private class WS_InstalledBaseTest {
    static testMethod void testGetInstallBaseForUpgrades() {
        RecursiveQuoteTriggerController.isSuccessEmailSent=true;
        Sales_Order__c salesOrder = insertQuoteAndSalesOrder();
        Test.startTest();
            insertTestData(salesOrder);
            List<WS_InstalledBase.WrapperIBase> iBaseList = WS_InstalledBase.getInstallBaseForUpgrades(SR_PrepareOrderControllerTest.salesQuote.BigMachines__Account__c);
            //System.assertEquals(1, iBaseList.Size());
        Test.stopTest();
    }
    
    static testMethod void testGetInstallBaseForServiceContracts() {
        RecursiveQuoteTriggerController.isSuccessEmailSent=true;
        Sales_Order__c salesOrder = insertQuoteAndSalesOrder();
        Test.startTest();
            insertTestData(salesOrder);
            List<SVMXC__Installed_Product__c> installedProducts = WS_InstalledBase.getInstallBaseForServiceContracts('Sales1212');
            System.assertEquals(1, installedProducts.Size());
        Test.stopTest();
    }
    
    static void insertTestData(Sales_Order__c salesOrder){
        
        SVMXC__Site__c testsite = new SVMXC__Site__c();
        testsite.SVMXC__Street__c = 'test street';
        testsite.Name = 'test site';
        testsite.SVMXC__Country__c = 'USA';
        testsite.SVMXC__Zip__c = '244401';
        testsite.ERP_Site_Partner_Code__c = 'Sales1212';
        testsite.ERP_Functional_Location__c = 'Test';
        testsite.SVMXC__Account__c = SR_PrepareOrderControllerTest.salesQuote.BigMachines__Account__c;
        insert testsite;
        
        
        BigMachines__Quote_Product__c quoteProduct = [Select BigMachines__Product__c 
                                                        From BigMachines__Quote_Product__c
                                                        Where BigMachines__Quote__c =:SR_PrepareOrderControllerTest.salesQuote.Id];
        
        //Create Sales Order Item
        Sales_Order_Item__c objSOI= new Sales_Order_Item__c(Name ='100001',
                                                            UOM__c = 'EA',
                                                            Product__c = quoteProduct.BigMachines__Product__c,
                                                            Sales_Order__c = salesOrder.id,
                                                            Location__c = testsite.Id);
        insert objSOI;
        
        SVMXC__Installed_Product__c installedProduct = SR_testdata.createInstalProduct();
        installedProduct.SVMXC__Company__c = SR_PrepareOrderControllerTest.salesQuote.BigMachines__Account__c;
        installedProduct.SVMXC__Product__c = quoteProduct.BigMachines__Product__c;
        installedProduct.ERP_Reference__c = '111';
        installedProduct.Sales_Order__c = salesOrder.Id;
        installedProduct.Sales_OrderItem__c = objSOI.Id;
        installedProduct.SVMXC__Site__c = testsite.Id;
        installedProduct.ERP_Functional_Location__c = 'Test';
        insert installedProduct;
        
        
        
        SVMXC__Installed_Product__c installedProduct1 = [Select Name,CURRENT_ISC_CONFIGURATION__C,SVMXC__Site__c,SVMXC__Company__c  From SVMXC__Installed_Product__c Where Id =: installedProduct.Id];
        
        System.debug('----installedProduct1'+installedProduct1.SVMXC__Site__c+'---'+installedProduct1.SVMXC__Company__c+'----ba'+SR_PrepareOrderControllerTest.salesQuote.BigMachines__Account__c);
        
        ISC_History__c iscHistory = new ISC_History__c();
        iscHistory.API_Table_Name__c = 'ISC_OBI_CBCT';
        iscHistory.Product__c = quoteProduct.BigMachines__Product__c;
        iscHistory.Installed_Product__c = installedProduct.Id;
        iscHistory.PCSN__c = installedProduct1.Name;
        iscHistory.Build_Version__c = 5.0;
        iscHistory.Major_Version__c = 2;
        iscHistory.Minor_Version__c = 1;
        insert iscHistory;
        
        installedProduct1.CURRENT_ISC_CONFIGURATION__C = iscHistory.Id;
        update installedProduct1;
        
        SVMXC__Installed_Product__c installedProduct2 = [Select Name,CURRENT_ISC_CONFIGURATION__C,SVMXC__Site__c,SVMXC__Company__c  From SVMXC__Installed_Product__c Where Id =: installedProduct.Id];
        System.debug('----installedProduct2'+installedProduct2.SVMXC__Site__c+'---'+installedProduct2.SVMXC__Company__c);
        
        ISC_Parameter_Definition__c parameterDefinition = new ISC_Parameter_Definition__c();
        parameterDefinition.Name = 'Reconstructor_ServPack';
        parameterDefinition.API_Name__c = 'H07-Reconstructor_ServPack';
        insert parameterDefinition;
        
        ISC_Parameters__c iscParameter = new ISC_Parameters__c();
        iscParameter.Name = 'Reconstructor_ServPack';
        iscParameter.ISC__c = iscHistory.Id;
        iscParameter.Parameter_Value__c = '1';
        iscParameter.Parameter_Number__c = 1;
        iscParameter.Parameter_Definition__c = parameterDefinition.Id;
        insert iscParameter;
        
        Product_Version__c productVersion = new Product_Version__c();
        productVersion.Name = 'Test Version';
        productVersion.Major_Release__c = 2;
        productVersion.Minor_Release__c = 1;
        productVersion.Status__c = 'Active';
        productVersion.Product__c = quoteProduct.BigMachines__Product__c;
        insert productVersion;
    }
    
    static private Sales_Order__c insertQuoteAndSalesOrder(){
        SR_PrepareOrderControllerTest.setUpSalesTestdata();
        
        Sales_Order__c salesOrder = new Sales_Order__c();
        salesOrder.Name = 'Test SalesOrder';
        salesOrder.Ext_Quote_Number__c = SR_PrepareOrderControllerTest.salesQuote.Name;
        salesOrder.ERP_Sold_To__c = 'Sales1212';
        salesOrder.ERP_Site_Partner__c = 'Sales11111';
        insert salesOrder;
        return salesOrder;
    }
    
    static private testMethod void ERPPartnerTest() {
        Test.startTest();
            WS_InstalledBase.ERPPartner newPartner = new WS_InstalledBase.ERPPartner();
            newPartner.accountname = '';
            newPartner.siteName = '';
            newPartner.functionalLocation = '';
            newPartner.city = '';
            newPartner.country = '';
            
            WS_InstalledBase.WrapperIBase newPartners = new WS_InstalledBase.WrapperIBase();
            newPartners.InstallName = '';
            newPartners.ProductName = '';
            newPartners.ProductBuildName = '';
            newPartners.ProductBuildVersion = '';
            newPartners.TopLvlPCSN = '';
            newPartners.TopLevel = '';
            newPartners.ERPCode = ''; 
            newPartners.ERPProductCode = '';
            newPartners.ProductFamily = '';           
            newPartners.ProductCode = '';
            newPartners.SAPCode = '';
            newPartners.PCSN = '';
            newPartners.ISCConfigName = '';
            newPartners.ProductLine = '';
            newPartners.num = 1;
            newPartners.lstProdVersions = '';
            newPartners.lstISCParameters = new List<ISC_Parameters__c>();
            
            
            WS_InstalledBase.getSubscriptionInstallBase('','HA');
            WS_InstalledBase.getERPSitePartners('123');
        Test.stopTest();
    }
}
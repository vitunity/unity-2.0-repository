public class WeChatFollowerController
{
    private static final String aesKey = '3fXiwmRWxTLLNpytB10wUWonU2f8gyGTg1Rm0Q5hI8E=';

    public WeChatFollowerController()
    {
        initDefaultValue();
        
        String openId = getOpenId();
        EncryedOpenId = encryptOpenId(openId);
    }

    public String WeChatAccountId { get; set; }
    public String CampaignId { get; set; }
    public String CampaignMemberStatus { get; set; }
    public String EncryedOpenId { get; set; }
    public String BannerURL { get; set; }
    public String Title { get; set; }
    public String SuccessMessageId { get; set; }
    public String SuccessTemplateId { get; set; }

    @RemoteAction
    public static String init(String openIdStr, String code, String accId, String urlStr)
    {
        String openId = decryptedOpenId(openIdStr);

        // If code is invalid that need to redirect user to WeChat Oauth URL
        if(String.isBlank(openId))
        {
            String url = urlStr.replaceAll('&code=[^\\&]+', '');
            String oauthUrl = getOauthUrl(accId, url);
            oauthUrl = (oauthUrl != null) ? oauthUrl : '';
            return '{\"oauth_url\": \"' + oauthUrl + '\"}';
        }
        else
        {
            List<Charket__WeChatFollower__c> followers = [select Charket__Lead__c, Charket__Contact__c
                    from Charket__WeChatFollower__c
                    where Charket__OpenId__c = :openId
                    and (Charket__Lead__c != null or Charket__Contact__c != null)];

            if(followers.size() > 0)
            {
                return getLeadOrContactInfo(followers[0]);
            }
        }
        return null;
    }

    @RemoteAction
    public static String save(String companyStr, String nameStr, String titleStr, String mobilePhoneStr, String phoneStr, String emailStr,
        String cId, String cstatus, String openIdStr, String templateId, String messageId, String accId)
    {
        try
        {
            String leadId;
            String contactId;
            Id leadOrContactId;
            Charket__WeChatFollower__c currentFollower;

            String openId = decryptedOpenId(openIdStr);

            List<Charket__WeChatFollower__c> followers = [select Charket__Lead__c, Charket__Contact__c
                    from Charket__WeChatFollower__c
                    where Charket__OpenId__c = :openId and Charket__OpenId__c != null];

            if(followers.size() > 0)
            {
                currentFollower = followers[0];
                leadId = currentFollower.Charket__Lead__c;
                contactId = currentFollower.Charket__Contact__c;
            }

            if(String.isNotBlank(contactId))
            {
                leadOrContactId = updateContact(false, contactId, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr);
            }
            else // Insert or update lead
            {
                leadOrContactId = saveLead(false, leadId, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr, companyStr);
            }

            if(String.isNotBlank(leadOrContactId))
            {
                contactId = leadOrContactId.getSobjectType() == Contact.SobjectType ? leadOrContactId : null;
                leadId = leadOrContactId.getSobjectType() == Lead.SobjectType ? leadOrContactId : null;
            }

            if(currentFollower != null)
            {
                currentFollower.Charket__Contact__c = contactId;
                currentFollower.Charket__Lead__c = leadId;
                update currentFollower;

                updateCampaignMemberStatus(leadId, contactId, cId, cstatus, templateId, messageId);
            }
        }
        catch(Exception ex)
        {
            throw new DMLException('An unexpected error occurred, please contact your administrator.');
        }
        return null;
    }

    private String getOpenId()
    {
        if(!Test.isRunningTest())
        {
            try
            {
                String wechatCode = ApexPages.currentPage().getParameters().get('code');
                if(String.isNotBlank(wechatCode) && String.isNotBlank(WeChatAccountId))
                {
                    Charket.WeChatClient client = new Charket.WeChatClient(WeChatAccountId);
                    Charket.WeChatApiOAuth oauth = client.getOAuth();

                    Charket.WeChatApiOAuth.AuthTokenResponse response = oauth.handleCallback(wechatCode, '');
                    return response.OpenId;
                }
            }
            catch(Exception ex) {}
        }
        else
        {
            return 'OPENID';
        }

        return null;
    }

    private void initDefaultValue()
    {
        WeChatAccountId = ApexPages.currentPage().getParameters().get('accId');
        CampaignId = ApexPages.currentPage().getParameters().get('cId');
        CampaignMemberStatus = ApexPages.currentPage().getParameters().get('cstatus');
        BannerURL = ApexPages.currentPage().getParameters().get('formBanner');
        Title = ApexPages.currentPage().getParameters().get('formTitle');
        SuccessTemplateId = ApexPages.currentPage().getParameters().get('templateId');
        SuccessMessageId = ApexPages.currentPage().getParameters().get('messageId');
        if(String.isBlank(BannerURL))
        {
            BannerURL = 'https://asset.charket.com.cn/customer/img/varian-logo-1.jpg';
        }
    }

    private static String getLeadOrContactInfo(Charket__WeChatFollower__c follower)
    {
        if(String.isNotBlank(follower.Charket__Contact__c))
        {
            List<Contact> contacts = [select Name, FirstName, LastName, Email, Phone, MobilePhone, Title, Account.Name
                    from Contact where Id = :follower.Charket__Contact__c];
            if(contacts.size() > 0)
            {
                return '{\"contact\": ' + JSON.serialize(contacts[0]) + '}';
            }
        }
        else
        {
            List<Lead> leads = [select Name, FirstName, LastName, Email, Phone, Company, Title, MobilePhone
                    from Lead where Id = :follower.Charket__Lead__c];
            if(leads.size() > 0)
            {
                return '{\"lead\": ' + JSON.serialize(leads[0]) + '}';
            }
        }

        return null;
    }

    private static String saveLead(Boolean isDuplicate, Id leadId, String nameStr, String titleStr, String mobilePhoneStr, String phoneStr, String emailStr, String companyStr)
    {
        Lead lead = new Lead(Id = leadId);
        
        if(containsChineseCharacters(nameStr))
        {
            lead.LastName = nameStr.left(1);
            lead.FirstName = nameStr.length() > 1 ? nameStr.right(nameStr.length() - 1) : '';
        }
        else
        {
            lead.LastName = nameStr.containsWhitespace() ? nameStr.substringAfter(' ') : nameStr;
            lead.FirstName = nameStr.containsWhitespace() ? nameStr.substringBefore(' ') : '';
        }
        
        lead.Email = emailStr;
        lead.Title = titleStr;
        if(String.isNotBlank(mobilePhoneStr))
        {
            lead.MobilePhone = mobilePhoneStr;
        }
        if(String.isNotBlank(phoneStr))
        {
            lead.Phone = phoneStr;
        }
        lead.Company = companyStr;
        lead.LeadSource = 'WeChat';
        Database.SaveResult saveResult;
        if(String.isBlank(leadId))
        {
            saveResult = Database.insert(lead, false);
        }
        else if(!isDuplicate)
        {
            saveResult = Database.update(lead, false);
        }
        else
        {
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.update(lead, dml);
        }
        
        if(saveResult != null && !saveResult.isSuccess()) 
        {
            return getDuplicateRecordId(saveResult, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr, companyStr);
        }

        return lead.Id;
    }

    private static String updateContact(Boolean isDuplicate, Id contactId, String nameStr, String titleStr, String mobilePhoneStr, String phoneStr, String emailStr)
    {
        Contact contact = new Contact(Id = contactId);
        
        if(containsChineseCharacters(nameStr))
        {
            contact.LastName = nameStr.left(1);
            contact.FirstName = nameStr.length() > 1 ? nameStr.right(nameStr.length() - 1) : '';
        }
        else
        {
            contact.LastName = nameStr.containsWhitespace() ? nameStr.substringAfter(' ') : nameStr;
            contact.FirstName = nameStr.containsWhitespace() ? nameStr.substringBefore(' ') : '';
        }
        
        contact.Email = emailStr;
        contact.Title = titleStr;
        if(String.isNotBlank(mobilePhoneStr))
        {
            contact.MobilePhone = mobilePhoneStr;
        }
        if(String.isNotBlank(phoneStr))
        {
            contact.Phone = phoneStr;
        }
        
        Database.SaveResult saveResult;
        if(isDuplicate)
        {
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true;
            Database.update(contact, dml);
        }
        else
        {
            saveResult = Database.update(contact, false);
        }

        if(saveResult != null && !saveResult.isSuccess()) 
        {
            return getDuplicateRecordId(saveResult, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr, null);
        }
        return contact.Id;
    }

    private static String getDuplicateRecordId(Database.SaveResult saveResult, String nameStr, String titleStr, String mobilePhoneStr, String phoneStr, String emailStr, String companyStr)
    {
        for(Database.Error error : saveResult.getErrors()) 
        {
            if(error instanceof Database.DuplicateError) 
            {
                Database.DuplicateError duplicateError = (Database.DuplicateError)error;
                Datacloud.DuplicateResult duplicateResult = duplicateError.getDuplicateResult();
                Datacloud.MatchRecord[] matchRecords = duplicateResult.getMatchResults()[0].getMatchRecords();
                for (Datacloud.MatchRecord matchRecord : matchRecords) 
                {
                    List<Contact> oldContacts = [select Id from Contact where Id = :matchRecord.getRecord().Id limit 1];
                    if(oldContacts.size() > 0)
                    {
                        return updateContact(true, oldContacts[0].Id, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr);
                    }
                    else
                    {
                        List<Lead> oldLeads = [select Id from Lead where Id = :matchRecord.getRecord().Id limit 1];
                        if(oldLeads.size() > 0)
                        {
                            return saveLead(true, oldLeads[0].Id, nameStr, titleStr, mobilePhoneStr, phoneStr, emailStr, companyStr);
                        }
                    }
                }
            }
            else
            {
                throw new DMLException('An unexpected error occurred, please contact your administrator.');
            }
        }
            
        return null;
    }
    
    private static Boolean containsChineseCharacters(String InputString){
        Pattern p = Pattern.compile('\\p{IsHan}');
        Matcher m = p.matcher(InputString);
        return m.find();
    }

    private static void updateCampaignMemberStatus(String leadId, String contactId, String campaignId, String status, String templateId, String messageId)
    {
        if(String.isBlank(campaignId))
        {
            return;
        }
        CampaignMember member;
        status = (String.isNotBlank(status)) ? status : 'Responded';

        List<CampaignMember> members = [select Id from CampaignMember
                where CampaignId = :campaignId and
                 ((LeadId = :leadId and LeadId != null) or (ContactId = :contactId and ContactId != null))];

        if(members.size() > 0)
        {
            member = members[0];
            member.Status = (String.isNotBlank(status)) ? status : 'Responded';
            update member;
        }
        else
        {
            member = new CampaignMember(CampaignId = campaignId, LeadId = leadId,
                    ContactId = contactId, Status = status);
            insert member;
        }

        String leadOrContactId = String.isNotBlank(leadId) ? leadId : contactId;

        sendWeChatNotification(member.Id, templateId, messageId, leadOrContactId);
    }

    public static void sendWeChatNotification(String memberId, String templateId, String messageId, String leadOrContactId)
    {
        if(String.isNotBlank(memberId) && String.isNotBlank(templateId) && String.isNotBlank(leadOrContactId))
        {
            Charket.CharketSendWeChatNotificationAction.SendTemplateNotificationRequest templateRequest = new Charket.CharketSendWeChatNotificationAction.SendTemplateNotificationRequest();
            templateRequest.objectId = memberId;
            templateRequest.templateId = templateId;
            templateRequest.whoId = leadOrContactId;
            List<Charket.CharketSendWeChatNotificationAction.SendTemplateNotificationRequest> templateRequests = new List<Charket.CharketSendWeChatNotificationAction.SendTemplateNotificationRequest>();
            templateRequests.add(templateRequest);
            Charket.CharketSendWeChatNotificationAction.sendTemplateNotification(templateRequests);
        }

        if(String.isNotBlank(memberId) && String.isNotBlank(messageId) && String.isNotBlank(leadOrContactId))
        {
            Charket.CharketSendWeChatMessageAction.SendWeChatMessageRequest messageRequest = new Charket.CharketSendWeChatMessageAction.SendWeChatMessageRequest();
            messageRequest.objectId = memberId;
            messageRequest.messageId = messageId;
            messageRequest.whoId = leadOrContactId;
            List<Charket.CharketSendWeChatMessageAction.SendWeChatMessageRequest> messageRequests = new List<Charket.CharketSendWeChatMessageAction.SendWeChatMessageRequest>();
            messageRequests.add(messageRequest);
            Charket.CharketSendWeChatMessageAction.sendWeChatMessage(messageRequests);
        }
    }

    private static String getOauthUrl(String accId, String url)
    {
        String result;

        try
        {
            String siteUrl = url;
            Charket.WeChatApiOAuth oauth = (new Charket.WeChatClient(accId)).getOAuth();
            result = oauth.initiate(siteUrl, Charket.WeChatApiOAuth.Scope.base, '').getUrl();
        }
        catch(Exception ex) {}

        return result;
    }


    private static String encryptOpenId(String openId)
    {
        if(String.isNotBlank(openId))
        {
            Blob key = EncodingUtil.base64Decode(aesKey);
            Blob data = Blob.valueOf(openId);
            Blob encrypted = Crypto.encryptWithManagedIV('AES256', key, data);

            return EncodingUtil.base64Encode(encrypted);
        }

        return null;
    }

    private static String decryptedOpenId(String encryptedOpenId)
    {
        if(String.isNotBlank(encryptedOpenId))
        {
            Blob key = EncodingUtil.base64Decode(aesKey);
            Blob data = EncodingUtil.base64Decode(encryptedOpenId);
            Blob decrypted = Crypto.decryptWithManagedIV('AES256', key, data);

            return decrypted.toString();
        }

        return null;
    }
}
@isTest
public class WeChatFollowerControllerTest
{
    private static Lead lead = createLead('12345678901', 'aa');
    private static Account account = createAccount();
    private static Contact contact = createContact('12345678901', 'aa', 'test@meginfo.com', account.Id);
    private static Charket__WeChatAccount__c wechatAccount = getWeChatAccount();
    private static Charket__WeChatFollower__c follower = getWeChatFollower(wechatAccount.Id, lead.Id);

    @isTest
    static void testInit()
    {
        WeChatFollowerController controller = getWeChatFollowerController();

        String result = WeChatFollowerController.init(controller.EncryedOpenId, '123', wechatAccount.Id, '');
        System.assert(result.contains(lead.LastName));

        follower.Charket__Lead__c = null;
        follower.Charket__Contact__c = contact.Id;
        update follower;
        result = WeChatFollowerController.init(controller.EncryedOpenId, '123', wechatAccount.Id, '');
        System.assert(result.contains(contact.LastName));
    }

    @isTest
    static void testInitNoQR()
    {
        PageReference wechatFollowerPage2 = Page.WeChatFollower;
        wechatFollowerPage2.getParameters().put('code', '123123');
        wechatFollowerPage2.getParameters().put('accId', wechatAccount.Id);
        Test.setCurrentPage(wechatFollowerPage2);
        WeChatFollowerController controller2 = new WeChatFollowerController();

        WeChatFollowerController.init(null, '123', wechatAccount.Id, '');
    }

    @isTest
    static void testSave()
    {
        WeChatFollowerController controller = getWeChatFollowerController();
        Campaign campaign = new Campaign(Name = 'acme', IsActive = true);
        insert campaign;

        CampaignMember member = new CampaignMember(
                CampaignId = campaign.Id, LeadId = lead.Id,
                Status = 'Sent');
        insert member;

        Charket__WeChatNotificationTemplate__c template = new Charket__WeChatNotificationTemplate__c();
        insert template;
        Charket__WeChatMessage__c message = new Charket__WeChatMessage__c(Charket__TextBody__c = 'test');
        insert message;
        
        WeChatFollowerController.save('test', '测试', 'test', '12345678901', '12345678901', 'acme@charket.com', campaign.Id,
        'Responded', controller.EncryedOpenId, template.Id, message.Id, wechatAccount.Id);

        Lead newLead = [select Id, Name, Email from Lead where Id = :lead.Id limit 1];
        System.assertEquals(newLead.Email, 'acme@charket.com');
    }

    @isTest
    static void testSaveNewLead()
    {
        follower.Charket__Lead__c = null;
        update follower;

        Campaign campaign = new Campaign(Name = 'acme', IsActive = true);
        insert campaign;

        WeChatFollowerController controller = getWeChatFollowerController();
        
        WeChatFollowerController.save('test', 'Vicky Zhang', 'test', '12345678901', '12345678901', 'test@test.com', Campaign.Id,
        'Responded', controller.EncryedOpenId, '', '', wechatAccount.Id);

        Charket__WeChatFollower__c follower = [select Charket__Lead__c from Charket__WeChatFollower__c where Id = :follower.Id limit 1];
        System.assert(follower.Charket__Lead__c != null);
    }

    @isTest
    static void testSaveContact()
    {
        follower.Charket__Lead__c = null;
        follower.Charket__Contact__c = contact.Id;
        update follower;

        WeChatFollowerController controller = getWeChatFollowerController();

        WeChatFollowerController.save('test', 'Vicky Zhang', 'test', '12345678901', '12345678901', 'acme@charket.com', '',
        '', controller.EncryedOpenId, '', '', wechatAccount.Id);

        Contact newContact = [select Id, Title from Contact where Id = :contact.Id limit 1];
        System.assertEquals(newContact.Title, 'test');
    }
    
    @isTest
    static void testSaveDuplicateContact()
    {
        follower.Charket__Lead__c = null;
        follower.Charket__Contact__c = contact.Id;
        update follower;
    
        Contact contact2 = createContact('12345678901', 'aa', 'acme@meginfo.com', account.Id);
        
        WeChatFollowerController controller = getWeChatFollowerController();

        WeChatFollowerController.save('test', '测试', 'test', '12345678901', '12345678901', 'acme@charket.com', '',
        '', controller.EncryedOpenId, '', '', wechatAccount.Id);

        Contact newContact = [select Id, Title from Contact where Id = :contact.Id limit 1];
        System.assertEquals(newContact.Title, 'test');
    }

    private static Charket__WeChatAccount__c getWeChatAccount()
    {
        Charket__WeChatAccount__c wechatAccount = new Charket__WeChatAccount__c(
                Name = 'TestAcount', Charket__WeChatOriginId__c = 'test origin',
                Charket__Type__c = 'subscribe'
            );
        insert wechatAccount;
        return wechatAccount;
    }

    private static WeChatFollowerController getWeChatFollowerController()
    {
        PageReference wechatFollowerPage = Page.WeChatFollower;
        wechatFollowerPage.getParameters().put('code', '123123');
        wechatFollowerPage.getParameters().put('accId', wechatAccount.Id);
        Test.setCurrentPage(wechatFollowerPage);
        WeChatFollowerController controller = new WeChatFollowerController();
        return controller;
    }

    private static Charket__WeChatFollower__c getWeChatFollower(Id wechatAccountId, Id leadId)
    {
        Charket__WeChatFollower__c wechatFollower = new Charket__WeChatFollower__c(
                Charket__OpenId__c = 'OPENID', Charket__WeChatAccount__c = wechatAccountId,
                Charket__IsFollowing__c = true, Charket__SubscriptionDate__c = System.now(),
                Charket__Lead__c = lead.Id
            );
        insert wechatFollower;
        return wechatFollower;
    }

    private static Lead createLead(String phone, String lastName)
    {
        Lead newLead = new Lead(
                FirstName = 'Test', 
                LastName = lastName, 
                Phone = phone, 
                Email = 'test@test.com',
                Company = 'test'
            );

        insert newLead;
        return newLead;
    }

    private static Account createAccount()
    {
        Account acc = new Account(Name = 'Meginfo', BillingCountry = 'China', BillingCity = 'Beijing', 
            BillingState = 'test', Country__c = 'China');
        insert acc;
        return acc;
    }

    private static Contact createContact(String phone, String lastName, String email, String accountId)  
    {
        Contact newContact = new Contact(
                FirstName = 'Test', LastName = lastName, Phone = phone, Email = email,
                AccountId = accountId
            );
        insert newContact;
        return newContact;
    }
}
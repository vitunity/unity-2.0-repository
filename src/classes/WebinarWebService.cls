@RestResource(urlMapping='/webinars')
global with sharing class WebinarWebService {
    @HttpGet
    global static List<Event_Webinar__c> getWebinars() {
        List<Event_Webinar__c> webinarList = new List<Event_Webinar__c>();
        String webinarQuery = System.Label.WebinarFeed;
        webinarList = Database.query(webinarQuery);
        RestRequest request = RestContext.request;
        RestContext.response.addHeader('Content-Type', 'text/plain');
        return webinarList;
    }
}
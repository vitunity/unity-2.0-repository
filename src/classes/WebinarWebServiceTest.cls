@isTest 
private class WebinarWebServiceTest {
    static testMethod void testGetWebinars() {
	   
		Event_Webinar__c webinar = new Event_Webinar__c();
        webinar.Title__c = 'Test Title';
        webinar.Location__c = 'Test Location';
        webinar.Description__c = 'Test Description';
        webinar.Institution__c = 'Test Institution';
        webinar.Speaker__c = 'Test speaker';
        webinar.Varian_Contact__c = 'Test Contact';
        webinar.URL_Path_Settings__c = 'abc@gmail.com';
        webinar.URL__c = 'xyz@gmail.com';
        webinar.Time__c = 'x minutes';
        webinar.From_Date__c = System.Today();
        webinar.To_Date__c = System.Today();
		insert webinar;
		
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();
    
		req.requestURI = URL.getSalesforceBaseUrl()+'/services/apexrest/webinars';
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;
		
		WebinarWebService.getWebinars();
    }
}
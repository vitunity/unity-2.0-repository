/*
* Author: Harleen Gulati
* Created Date: 31-Aug-2017
* Project/Story/Inc/Task : My varian lightning project 
 * Description: This class is used for my varian website feedback lightening page.
*/
public class WebsiteFeedbackController{ 
    
    /* Check for logged in user */
    @AuraEnabled
    public static Boolean getCurrentUser() {
        boolean externalUser;
        User u = [SELECT Id, LastName, Email, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            externalUser = false;
        }else{
            externalUser = true;
        }
        return externalUser;
    }
    
    /* load custom labels */
    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);    
    }
    
    /* get country picklist values*/
    @AuraEnabled
    public static List<String> getDynamicPicklistOptions(String fieldName)
    {
        List<String> pickListValues = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('Account');
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        Schema.DescribeFieldResult descField = fieldMap.get(fieldName).getDescribe();

        List<Schema.PicklistEntry> ple = descField.getPicklistValues();
        for(Schema.PicklistEntry f : ple)
        {
            pickListValues.add(f.getLabel());
        } 
        return pickListValues;
    }
    
    /* register data and send email to user*/
    @AuraEnabled
    public static String registerMData(Contact registerDataObj, Account accDataObj, String subject, String longDes,String fileName, String base64Data, String contentType){
    
    //Code to Send email to user and admin     
    List<String> toAddresses = new List<String>();
    List<ProdCountEmailMapping__c > emailmap = new List<ProdCountEmailMapping__c>();
    List<String> touser = new List<String>();
    
    Datetime myDT = Datetime.now();
    String myDate = myDT.format('MM/dd/yyyy HH:mm:ss');
    String EmailBody;
    string reqtype;
    string Request = 'Website Feedback';
    base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
    
    list<User> usr = new list<User>();
    usr = [select id ,name,username,Email,Subscribed_Products__c,alias,usertype,FullPhotoUrl,SmallPhotoUrl,
           contact.Title, contact.firstname, contact.lastname, Contact.accountid, contact.account.Name,
           contact.MailingCity, contact.MailingCountry, contact.MailingPostalcode, Contact.mailingState, 
           Contact.MailingStreet, Contact.Phone, Contact.Institute_Name__c, contact.Email, contact.id,
           contact.manager__c, contact.account.billingstreet, contact.Reason_To_Unsubscribe__c,
           contact.account.billingcountry, contact.account.billingpostalcode, contact.account.billingcity,
           contact.account.billingstate, contact.account.fax, contact.account.phone, contact.account.e_Subscribe__c,         
           contact.fax,contact.OktaId__c,
           contact.RAQA_Contact__c 
           from user where Id =: UserInfo.getUserId() limit 1];

    if(usr[0].contact.id != null){
    registerDataObj.FirstName            =  usr[0].contact.FirstName;
    registerDataObj.LastName             =  usr[0].contact.LastName;
    registerDataObj.Institute_Name__c    =  usr[0].contact.Institute_Name__c;
    registerDataObj.Phone                =  usr[0].contact.Phone;
    registerDataObj.Email                =  usr[0].contact.Email;
    registerDataObj.MailingStreet        =  usr[0].contact.MailingStreet;
    registerDataObj.MailingState         =  usr[0].contact.MailingState;
    registerDataObj.MailingCity          =  usr[0].contact.MailingCity;
    registerDataObj.MailingCountry       =  usr[0].contact.MailingCountry;
    registerDataObj.MailingPostalcode    =  usr[0].contact.MailingPostalcode;

    }
    touser.add(registerDataObj.Email);
     
    emailmap=[Select e.email__c From ProdCountEmailMapping__c e where e.Name=: Request limit 1];
    if(emailmap.size() >0)
    {
        toAddresses.add(emailmap[0].email__c);
    } 
    reqtype='Website Feedback';
    EmailBody='Thank you for your feedback. We welcome your comments and reports of any issues you may come across while visiting MyVarian. We will make necessary changes in order to provide you with as positive a user experience as possible.<br><br>Thank you for using MyVarian.<br><br>Varian Medical Systems';
        
        if(Request != 'PaperDocumentation' )
        {
            EmailBody= EmailBody+ '<br><br>Submitted on '+myDate+ '<br>Name: '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>Institution: '+registerDataObj.Institute_Name__c+'<br>Telephone: '+registerDataObj.Phone+'<br><br>Email: '+registerDataObj.Email ;
            EmailBody=EmailBody+'<br><br>Address: '+registerDataObj.MailingStreet+'<br>City: '+registerDataObj.MailingCity+'<br>State:  '+registerDataObj.MailingState+'<br>Country: '+registerDataObj.MailingCountry+'<br><br>Type of Request: '+reqtype+ '<br><br>Subject: '+subject+'<br><br>'+longDes;
        }
         
        Messaging.SingleEmailMessage MailTicket= new Messaging.SingleEmailMessage();
        MailTicket.setSubject(reqtype +' successfully delivered ');
        MailTicket.setHTMLBody(EmailBody);
        MailTicket.setToAddresses(touser);
         
        // Use Organization Wide Address  
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress])
        {
            if(owa.Address.contains(system.label.CpOrgWideAddress))
                MailTicket.setOrgWideEmailAddressId(owa.id);
             
        } 
        MailTicket.setReplyTo(System.Label.MVsupport);  
        MailTicket.setSaveAsActivity (false);
         
        String body ='';
         
        if(Request != 'PaperDocumentation' )
        {
            body ='<br><br>Submitted on '+myDate+ '<br>Name: '+registerDataObj.FirstName+' '+registerDataObj.LastName+'<br>Institution: '+registerDataObj.Institute_Name__c+'<br>Telephone: '+registerDataObj.Phone+'<br><br>Email: '+registerDataObj.Email ;
            body = body +'<br><br>Address: '+registerDataObj.MailingStreet+'<br>City: '+registerDataObj.MailingCity+'<br>State:  '+registerDataObj.MailingState+'<br>Country: '+registerDataObj.MailingCountry+'<br><br>Type of Request: '+ reqtype+'<br><br>Subject: '+subject+'<br><br>'+longDes;
            body = body +'<br><br>{USEREMAIL:['+registerDataObj.Email+']}';
        }
         
        Messaging.SingleEmailMessage mailAdmin = new Messaging.SingleEmailMessage();
        mailAdmin.setSubject(reqtype);
        mailAdmin.setHTMLBody(body);
        mailAdmin.setToAddresses(toAddresses);
        for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]){ 
            if(owa.Address.contains(system.label.CpOrgWideAddress)) {
                mailAdmin.setOrgWideEmailAddressId(owa.id);
            }
        }
         
        if(base64Data != null && fileName != null)
        {
           blob b = EncodingUtil.base64Decode(base64Data);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(fileName);
            efa.setBody(b);

            base64Data = null;
            MailTicket.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            mailAdmin.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        } 
         
        mailAdmin.setTreatTargetObjectAsRecipient(false); 
        mailAdmin.setReplyTo(System.Label.MVsupport);     
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { MailTicket});
        if(emailmap.size() >0)
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailAdmin }); 
        }              
         
        return 'accessdocs';
    }
}
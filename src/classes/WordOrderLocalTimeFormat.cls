public  class WordOrderLocalTimeFormat{
    public static void correctTimeFormat(list<SVMXC__Service_Order_Line__c> listWorkDetail){
        for(SVMXC__Service_Order_Line__c wd : listWorkDetail){
            if(wd.Customer_Local_Start_Date_Time__c <> null && (wd.Customer_Local_Start_Date_Time__c).contains(' 24:'))   wd.Customer_Local_Start_Date_Time__c = (wd.Customer_Local_Start_Date_Time__c).replace(' 24:',' 00:');
            if(wd.Customer_Local_End_Date_Time__c <> null && (wd.Customer_Local_End_Date_Time__c).contains(' 24:'))  wd.Customer_Local_End_Date_Time__c = (wd.Customer_Local_End_Date_Time__c).replace(' 24:',' 00:');
        }
    }
}
/*
 *  Author : Amitkumar Kate
 *  Description : Update remaning qty to pol back if wdl line deleted
 *  created date : 13/07/2016 (After GO Live) 
 */
public class WorkDetailAfterDeleteHandler {
    /*
     *  Update remaning qty to pol back if wdl line deleted
     */
    public static void updatePOLRemainingQty(List<SVMXC__Service_Order_Line__c> oldList){
        List<SVMXC__RMA_Shipment_Line__c>  polToUpdate = new List<SVMXC__RMA_Shipment_Line__c>();
        for (SVMXC__Service_Order_Line__c workDetail : oldList) {
            if (workDetail.SVMXC__Line_Type__c == 'Parts' && 
                (workDetail.Part_Disposition__c == 'Installing' 
                || workDetail.Part_Disposition__c == 'Installed')
                && workDetail.SVMXC__Actual_Quantity2__c != null && workDetail.Parts_Order_Line__c != null ){
                polToUpdate.add(new SVMXC__RMA_Shipment_Line__c(Id = workDetail.Parts_Order_Line__c ) );
            }
        }
        if(!polToUpdate.isEmpty()){ 
            update polToUpdate;
        }
    }
}
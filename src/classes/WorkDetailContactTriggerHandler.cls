/*
 * Work detail contact trigger handler 
 * 
 
 //CM : DFCT0011993: Modified after Insert Logic && after update logic 
 Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Aug-20-2018 - Chandramouli - STRY0072799 - Added logic to take care of updating SAP(Date Fields) from NWA. 
Jun-29-2018 - Chandramouli - STSK0015189 - Modified code to fix the coupon cancellation issue for WL Coupons.
May-18-2018 - Chandramouli - STSK0014827 - Modified code to fix the exception.
Apr-18-2018 - Chandramouli - STSK0014423 - Modified code to fix the adv credits NWA details to show up on TD page
Apr-05-2018 - Chandramouli - STSK0014388 - Modified code to fix the NWA to pass the data to SAP.
Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations from LMS.
****************************************************************************/ 
public class WorkDetailContactTriggerHandler extends TriggerHandler{
    
    public WorkDetailContactTriggerHandler () {
        this.setMaxLoopCount(16);
    }
       
    /* before insert */
    public override void beforeInsert() {
        setContactLookup((list<Work_Detail_Contacts__c>)Trigger.new);
    }
    
    /* after insert */
    public override void afterInsert() {
        //Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations from LMS.
        updateFieldOnWOandWD((list<Work_Detail_Contacts__c>)Trigger.new,(map<id,Work_Detail_Contacts__c>)trigger.oldMap);
        createCounterUsage((list<Work_Detail_Contacts__c>)Trigger.new);
    }
    
    /* after update */
    public override void afterUpdate() {
        system.debug('after update');
        //Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations from LMS.
        updateFieldOnWOandWD((list<Work_Detail_Contacts__c>)Trigger.new,(map<id,Work_Detail_Contacts__c>)trigger.oldMap);
        createCounterUsage((list<Work_Detail_Contacts__c>)Trigger.new);
    }
    //Apr-18-2018 - Chandramouli - STSK0014423 - Modified code to fix the adv credits NWA details to show up on TD page
    public static map<String, ERP_NWA__c> nwaMap = new map<String, ERP_NWA__c>();
    /*
     * Set contact lookup on work detail contact 
     */ 
    @testvisible
    private void setContactLookup(list<Work_Detail_Contacts__c> wdcList){
        set<String> emails = new set<String>();
        for(Work_Detail_Contacts__c wdc: wdcList){
            if(wdc.Participant_Email__c!= null){
                emails.add(wdc.Participant_Email__c);
            }
        }
        map<String,Contact> contacts = new map<String,Contact>();
        if(!emails.isEmpty()){
            for(Contact c : [select Id, Email from contact where email in : emails]){
                contacts.put(c.email,c);
            }
            // set lookup field of contact on work detail contact
            for(Work_Detail_Contacts__c wdc: wdcList){
                if(wdc.Participant_Email__c!= null && contacts.containsKey(wdc.Participant_Email__c)){
                    wdc.Contact__c = contacts.get(wdc.Participant_Email__c).Id;
                }
            }
        }
        
    }
    
    /*
     * Set ERP NWA and Product lookup on work detail  
     */ 
    @testvisible
    private void updateFieldOnWOandWD(list<Work_Detail_Contacts__c> wdcList,map<id,Work_Detail_Contacts__c> oldMap){
        
        set<String> fcodes = new set<String>();
        set<String> erpNWANBRSet = new set<String>();
        set<Id> WorkdetailIDs = new set<Id>();
        for(Work_Detail_Contacts__c wdc: wdcList){
            if(wdc.ERP_NWA_Nbr__c != null ){
                erpNWANBRSet.add(wdc.ERP_NWA_Nbr__c); 
            }
            /*if(wdc.Reference__c!= null){   //DE7414
                erpNWANBRSet.add(wdc.Reference__c);
            }*/
            
            if(wdc.Facility__c!= null){
                fcodes.add(wdc.Facility__c);
            }
            if(wdc.Work_Detail__c != null){
                workdetailids.add(wdc.Work_Detail__c);
            }
        }
        
        map<Id,Work_Detail_Contacts__c> qWdcList = new map<Id,Work_Detail_Contacts__c>(
            [select id, Work_Detail__r.SVMXC__Service_Order__c 
             from Work_Detail_Contacts__c where Id in :wdcList]);
        system.debug('***CM erpnwanbrset: ' + erpNWANBRSet);
        Map<id,Work_Detail_Contacts__c> wd2wdclist = new Map<id,Work_Detail_Contacts__c>();
        for (Work_Detail_Contacts__c wdc : [select id, LMS_Status__c, ORDER_NO__c, Status__c, Registration_Status__c, Completion_Status__c, Work_Detail__c from Work_Detail_Contacts__c where Work_Detail__c in : workdetailids order by ORDER_NO__c Desc limit 1]){
             if (wdc.ORDER_NO__c != null){
                wd2wdclist.put(wdc.Work_Detail__c, wdc);
            }
        }
        System.Debug('***CM wd2wdclist: '+wd2wdclist);
        map<Id,SVMXC__Service_Order__c> wosMap = new map<Id,SVMXC__Service_Order__c> ();
        
        /* Update Account on work order, erp nwa on worl detaiil and update product(part) on work detail */
        if(!erpNWANBRSet.isEmpty()){
            
            
            map<Id,List<ERP_NWA__c>> wd2nwalst = new Map<Id,List<ERP_NWA__c>>();
            
            for(ERP_NWA__c c : [select Id, ERP_Reference__c,WBS_Element__r.Sales_Order_Item__r.Site_Partner__c,Work_Detail__c,erpnwa_SalesOrderItem__c,WBS_Element__c,
                                Product__c from ERP_NWA__c where ERP_Reference__c in : erpNWANBRSet or Work_Detail__c in :workdetailids ])
            {
                nwaMap.put(c.ERP_Reference__c,c);
                if (wd2nwalst.size() > 0)
                {
                    if (!wd2nwalst.containskey(c.Work_Detail__c))
                    {
                        list<ERP_NWA__c> nwalst = new List<erp_nwa__c>();
                        nwalst.add(c);
                        wd2nwalst.put(c.Work_Detail__c, nwalst);
                    }
                    else
                    {
                        list<ERP_NWA__c> nwalst = new List<erp_nwa__c>();
                        nwalst = wd2nwalst.get(c.Work_detail__c);
                        nwalst.add(c);
                        wd2nwalst.put(c.Work_Detail__c, nwalst);   
                    }
                }
                else
                {
                    list<ERP_NWA__c> nwalst = new List<erp_nwa__c>();
                    nwalst.add(c);
                    wd2nwalst.put(c.Work_Detail__c, nwalst);
                }
            }
            system.Debug('***CM : nwaMAP: '+nwaMap);
            list<SVMXC__Service_Order_Line__c> wdlist = new list<SVMXC__Service_Order_Line__c>();
            list<SVMXC__Service_Order_Line__c> wdlistupd = new list<SVMXC__Service_Order_Line__c>();
            list<SVMXC__Service_Order__c> wolist = new list<SVMXC__Service_Order__c>();
            list<ERP_NWA__c> nwalist = new list<ERP_NWA__c>();
            // set erpnwa and product of contact on work detail contact
            for(Work_Detail_Contacts__c wdc: wdcList){
                System.Debug('***CM wdc: ' + wdc);
                System.Debug('***CM oldmap: ' + oldMap);
                if(wdc.ERP_NWA_Nbr__c != null && nwaMap.containsKey(wdc.ERP_NWA_Nbr__c) && !wdc.ERP_NWA_Nbr__c.contains('WL')){ //DE7414
                    SVMXC__Service_Order_Line__c wdl = new SVMXC__Service_Order_Line__c();
                    wdl.Id = wdc.Work_Detail__c;
                    wdl.ERP_NWA__c = nwaMap.get(wdc.ERP_NWA_Nbr__c).Id;
                    wdl.SVMXC__Product__c = nwaMap.get(wdc.ERP_NWA_Nbr__c).Product__c; //DE7414
                    //Aug-20-2018 - Chandramouli - STRY0072799 - Added logic to take care of updating SAP(Date Fields) from NWA.
                    if (wdc.Start_Date__c != null){
                        wdl.SVMXC__Start_Date_and_Time__c = wdc.Start_Date__c;
                    }
                    if (wdc.End_Date__c != null){
                        wdl.SVMXC__End_Date_and_Time__c = wdc.End_Date__c;
                    }
                    //wdl.ERP_Reference__c = wdc.Reference__c;
                    if (wd2wdclist.containsKey(wdc.Work_Detail__c)) {
                        if ( (Integer.valueOf(wdc.Order_No__c.split('-')[1])) > (Integer.valueOf(wd2wdclist.get(wdc.Work_Detail__c).Order_No__c.split('-')[1])) && wdc.LMS_Status__c != null && wdc.Lms_Status__c != 'REL')
                        {
                            if (wdl.ERP_NWA__c != null)
                                wdl.lms_status__c = wdc.Lms_Status__c;
                        }
                        else if ( (wd2wdclist.get(wdc.Work_Detail__c).id == wdc.id) &&  wdc.LMS_Status__c != null && wdc.Lms_Status__c != 'REL') {
                            if (wdl.ERP_NWA__c != null)
                                wdl.lms_status__c = wdc.Lms_Status__c;   
                        }
                        //Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations/withdrawn from LMS.
                        else if (wdc.LMS_Status__c != null && wdc.LMS_Status__c == 'REL' && ( wdc.registration_status__c == 'Cancelled' || wdc.Registration_Status__c == 'Withdrawn') && (oldmap == null || (oldMap != null && oldMap.get(wdc.id).registration_status__c != wdc.registration_status__c)) )
                        {
                            if (wdl.ERP_NWA__c != null)
                            {    
                                wdl.lms_status__c = wdc.LMS_Status__c;
                                wdl.Cancel__c = true;
                            }
                        }
                    }
                    //wdl.ERP_Reference__c = wdc.Reference__c;
                    wdlist.add(wdl);
                    
                    /* CM : DFCT0012306 No longer needed this code. since we are showing the Facility code on TD page from WDC.
                    if (wdc.Facility__c != null){
                        wo.Facility_Code__c = wdc.Facility__c;
                    }*/
                    SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
                    wo.Id = qWdcList.get(wdc.Id).Work_Detail__r.SVMXC__Service_Order__c;
                    wo.SVMXC__Company__c = nwaMap.get(wdc.ERP_NWA_Nbr__c).WBS_Element__r.Sales_Order_Item__r.Site_Partner__c; //DE7414
                    wosMap.put(wo.Id,wo);
                    ERP_NWA__c erpnwa = new ERP_NWA__C();
                    erpnwa.id = nwaMap.get(wdc.ERP_NWA_Nbr__c).Id;
                    erpnwa.Work_Detail__c = wdc.Work_Detail__c;
                    //Apr-05-2018 - Chandramouli - STSK0014388 - Modified code to fix the NWA to pass the data to SAP.
                    if (wdc.Registration_Status__c == 'Enroll')
                    {   
                        //Aug-20-2018 - Chandramouli - STRY0072799 - Added logic to take care of updating SAP(Date Fields) from NWA.
                        if (wdc.Start_Date__c != null)
                        {
                            erpnwa.Start_Date_Time__c = wdc.Start_Date__c;
                        }
                        if (wdc.End_Date__c != null)
                        {
                            erpnwa.End_Date_Time__c = wdc.End_Date__c;
                        }
                        if (wdc.Participant_Name__c != null)
                        {
                            erpnwa.WDC_Particitipant_Name__c = wdc.Participant_Name__c;
                        }
                    }
                    nwalist.add(erpnwa);
                    SVMXC__Service_Order_Line__c wdl1 = new SVMXC__Service_Order_Line__c();
                    wdl1.Id = wdc.Work_Detail__c;
                    wdl1.ERP_Reference__c = wdc.Reference__c;
                    wdlistupd.add(wdl1);
                }
                else if (wdc.ERP_NWA_Nbr__c.contains('WL')){
                    SVMXC__Service_Order_Line__c wdl1 = new SVMXC__Service_Order_Line__c();
                    wdl1.Id = wdc.Work_Detail__c;
                    if (wdc.Start_Date__c != null){
                        wdl1.SVMXC__Start_Date_and_Time__c = wdc.Start_Date__c;
                    }
                    if (wdc.End_Date__c != null){
                        wdl1.SVMXC__End_Date_and_Time__c = wdc.End_Date__c;
                    }
                    //wdl.ERP_Reference__c = wdc.Reference__c;
                    if (wd2wdclist.containsKey(wdc.Work_Detail__c)) {
                        if ( (Integer.valueOf(wdc.Order_No__c.split('-')[1])) > (Integer.valueOf(wd2wdclist.get(wdc.Work_Detail__c).Order_No__c.split('-')[1])) && wdc.LMS_Status__c != null && wdc.Lms_Status__c != 'REL')
                        {
                            wdl1.lms_status__c = wdc.Lms_Status__c;
                        }
                        else if ( (wd2wdclist.get(wdc.Work_Detail__c).id == wdc.id) && wdc.LMS_Status__c != null && wdc.Lms_Status__c != 'REL'){
                            wdl1.lms_status__c = wdc.Lms_Status__c;   
                        }
                        /*Jun-29-2018 - Chandramouli - STSK0015189 - Modified code to fix the coupon cancellation issue for WL Coupons.
                        //Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations from LMS.
                        else if (wdc.LMS_Status__c != null && wdc.LMS_Status__c == 'REL' && wdc.registration_status__c == 'Cancelled' && (oldmap == null || (oldMap != null && oldMap.get(wdc.id).registration_status__c != wdc.registration_status__c)) )
                        {
                            wdl1.lms_status__c = wdc.LMS_Status__c;
                            wdl1.Cancel__c = true;
                        }
                        */
                        //Apr-05-2018 - Chandramouli - STSK0014388 - Modified code to fix the NWA to pass the data to SAP.
                        if (wd2nwalst.size() > 0 && wd2nwalst.containskey(wdc.Work_Detail__c))
                        {
                            For (ERP_NWA__c nwa : wd2nwalst.get(wdc.Work_Detail__c))
                            {
                                ERP_NWA__c erpnwa = new ERP_NWA__C();
                                erpnwa.Id = nwa.id;
                                if (wdc.Registration_Status__c == 'Enroll')
                                {
                                    if (wdc.Start_Date__c != null)
                                    {
                                        erpnwa.Start_Date_Time__c = wdc.Start_Date__c;
                                    }
                                    if (wdc.End_Date__c != null)
                                    {
                                        erpnwa.End_Date_Time__c = wdc.End_Date__c;
                                    }
                                    if (wdc.Participant_Name__c != null)
                                    {
                                        erpnwa.WDC_Particitipant_Name__c = wdc.Participant_Name__c;
                                    }
                                }
                                nwalist.add(erpnwa);
                            }
                        }
                    }
                    //wdl.ERP_Reference__c = wdc.Reference__c;
                    wdlist.add(wdl1);
                }
            }
            System.Debug('***CM wdlist: '+ wdlist);
            map<id,SVMXC__Service_Order_Line__c> mapwdlst = new map<id,SVMXC__Service_Order_Line__c>();
            if(!wdlist.isEmpty()) {
                For (SVMXC__Service_Order_Line__c wd : wdlist)
                {
                    mapwdlst.put(wd.Id, wd);
                }
            }
            if (mapwdlst.size() > 0)
            {
                update mapwdlst.values();
            }
            
            map<id,SVMXC__Service_Order_Line__c> mapwdlst2upd = new map<id,SVMXC__Service_Order_Line__c>();
            if(!wdlistupd.isEmpty()){
                For (SVMXC__Service_Order_Line__c wd : wdlistupd)
                {
                    mapwdlst2upd.put(wd.Id, wd);
                }
            }

            if (mapwdlst2upd.size() > 0)
            {
                update mapwdlst2upd.values();
            }
            Map<id,ERP_NWA__C> mapnwalst = new Map<Id,ERP_NWA__C>();
            if(!nwalist.isEmpty()) {
                for (ERP_NWA__c nwa : nwalist)
                {
                    mapnwalst.put(nwa.id, nwa);
                }
            }
            if (mapnwalst.size() > 0)
            {
                update mapnwalst.values();
            }
            
        }
        
        /* Update site on work order */
        if(!fcodes.isEmpty()){
            
            map<String, SVMXC__Site__c> siteMap = new map<String, SVMXC__Site__c>();
            
            for(SVMXC__Site__c c : [select Id, Facility_Code__c from SVMXC__Site__c where Facility_Code__c in : fcodes]){
                siteMap.put(c.Facility_Code__c,c);
            }
            // set erpnwa and product of contact on work detail contact
            for(Work_Detail_Contacts__c wdc: wdcList){
                if(wdc.Facility__c!= null && siteMap.containsKey(wdc.Facility__c)){
                    if(wosMap.containsKey(qWdcList.get(wdc.Id).Work_Detail__r.SVMXC__Service_Order__c)){
                        wosMap.get(qWdcList.get(wdc.Id).Work_Detail__r.SVMXC__Service_Order__c).SVMXC__Site__c = siteMap.get(wdc.Facility__c).Id;
                    }else{
                        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
                        wo.Id = qWdcList.get(wdc.Id).Work_Detail__r.SVMXC__Service_Order__c;
                        wo.SVMXC__Site__c = siteMap.get(wdc.Facility__c).Id;
                        wosMap.put(wo.Id,wo);
                    }
                }
            } 
        }
        
                    System.Debug('***CM wosMap: '+ wosMap);
        if(!wosMap.isEmpty()){
            update wosMap.values();
        }
        
    }
    
    @testvisible
    private void createCounterUsage(list<Work_Detail_Contacts__c> wdcList){
        map<Id,SVMXC__Service_Order_Line__c> wlMap;
        set<Id> wdIds = new set<Id>();
        set<string> soiids = new set<String>();
        set<string> wbsids = new set<string>();
        Set<String> cdrefs = new set<String>();
        for(Work_Detail_Contacts__c wdc: wdcList) {
            if(wdc.Work_Detail__c != null) {
                wdIds.add(wdc.Work_Detail__c);
            }
            if (Wdc.Reference__c != null) {
                cdrefs.add(wdc.Reference__c);
            }
            //Apr-18-2018 - Chandramouli - STSK0014423 - Modified code to fix the adv credits NWA details to show up on TD page
            if (Wdc.ERP_NWA_Nbr__c != null) {
                cdrefs.add(wdc.Reference__c);
                if (nwamap.size() > 0 && nwaMap.containskey(Wdc.ERP_NWA_Nbr__c) && nwamap.get(Wdc.ERP_NWA_Nbr__c) != null )
                {
                    if (nwamap.get(Wdc.ERP_NWA_Nbr__c).erpnwa_SalesOrderItem__c != null)
                    {
                        soiids.add(nwamap.get(Wdc.ERP_NWA_Nbr__c).erpnwa_SalesOrderItem__c);
                    }
                    else if (nwamap.get(Wdc.ERP_NWA_Nbr__c).WBS_Element__c != null)
                    {
                        wbsids.add(nwamap.get(Wdc.ERP_NWA_Nbr__c).WBS_Element__c);
                    }
                }
            }

        }
        //Jan-16-2018 - Chandramouli - STSK0013635 - Added status field for counter usage query.
        wlMap = new map<Id,SVMXC__Service_Order_Line__c>([select Name, Id, lms_status__c,(select Id, value__c, work_detail__c, counter_detail__c,Status__c from Counter_Usage__r)   
                                                          from SVMXC__Service_Order_Line__c 
                                                          where id in : wdIds]);
        Id coverageRTId = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId();
        map<String,SVMXC__Counter_Details__c> cdmap = new map<String,SVMXC__Counter_Details__c>();
        //List<SVMXC__Counter_Details__c> debugList = [select id, name, erp_reference__c, erp_nwa__c, Erp_nwa__r.Material_Group__c, SVMXC__Counter_reading__c,  ERP_NWA__r.ERP_Estimated_Work__c, erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.material_Group__c,  erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.name
          //                                   From SVMXC__Counter_Details__c];
        //System.debug('Yogesh logs -- list '+ debugList);
        //System.debug('Yogesh logs -- before query -- '+ cdrefs + ' --- ' + coverageRTId);
        if (cdrefs.size() > 0 || soiids.size() > 0 || wbsids.size() > 0)
        {
            for (SVMXC__Counter_Details__c cd : [select id, name, erp_reference__c,Sales_Order_Item__c, erp_nwa__c, Erp_nwa__r.Material_Group__c, SVMXC__Counter_reading__c,erp_Nwa__r.wbs_element__c,ERP_NWA__r.ERP_Estimated_Work__c, erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.material_Group__c,  erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.name,SVMXC__Product__c
                                                 From SVMXC__Counter_Details__c where (ERP_Reference__c in : cdrefs or Sales_Order_Item__c in :soiids OR erp_Nwa__r.wbs_element__c in :wbsids) and recordTypeId = :coverageRTId] )
            {
                cdmap.put(cd.ERP_Reference__c,cd);
                //Apr-18-2018 - Chandramouli - STSK0014423 - Modified code to fix the adv credits NWA details to show up on TD page
                cdmap.put(cd.Sales_Order_Item__c,cd);
                string key = '';
                key = String.valueOf(cd.erp_Nwa__r.wbs_element__c) + String.valueof(cd.SVMXC__Product__c);
                cdmap.put(key,cd);
            }
        }
        list<Counter_Usage__c> cuList = new list<Counter_Usage__c> ();
        map <id,SVMXC__Service_Order_Line__c> mapuwd = new map <id,SVMXC__Service_Order_Line__c> ();
        map <id,SVMXC__Counter_Details__c> mapucd = new map <id,SVMXC__Counter_Details__c>();
        for(Work_Detail_Contacts__c wdc: wdcList) {
            if(wdc.Work_Detail__c != null) {
                system.debug(wdc);
                SVMXC__Service_Order_Line__c wl = wlmap.get(wdc.Work_Detail__c);
                system.debug(wl);
                //System.debug('Yogesh -- logs ' + cdmap );
                //Apr-18-2018 - Chandramouli - STSK0014423 - Modified code to fix the adv credits NWA details to show up on TD page
                SVMXC__Counter_Details__c cd = new SVMXC__Counter_Details__c();
                if (cdmap.size() > 0 )
                {
                    if (cdmap.containskey(wdc.ERP_NWA_Nbr__c) && cdmap.get(wdc.ERP_NWA_Nbr__c) != null)
                    {
                        cd = cdmap.get(wdc.Reference__c);
                    }
                    else if (nwamap.size() > 0 && nwaMap.containskey(Wdc.ERP_NWA_Nbr__c) && nwamap.get(Wdc.ERP_NWA_Nbr__c) != null )
                    {
                        if (nwamap.get(Wdc.ERP_NWA_Nbr__c).erpnwa_SalesOrderItem__c != null)
                        {
                            cd = cdmap.get(nwamap.get(Wdc.ERP_NWA_Nbr__c).erpnwa_SalesOrderItem__c);
                        }
                        else if (nwamap.get(Wdc.ERP_NWA_Nbr__c).WBS_Element__c != null && nwamap.get(wdc.ERP_NWA_Nbr__c).Product__c != null)
                        {
                            string key = '';
                            key = String.valueOf(nwamap.get(Wdc.ERP_NWA_Nbr__c).WBS_Element__c) + String.valueOf(nwamap.get(wdc.ERP_NWA_Nbr__c).Product__c);
                            cd = cdmap.get(key);
                        }
                    }
                }
                if(cd != null) // to be removed after checking with david
                {
                    Counter_Usage__c cu = new Counter_Usage__c();
                    if(wl.Counter_Usage__r == null || wl.Counter_Usage__r.isEmpty()){
                        // create counter usage and detail here.
                        //System.debug('Yogesh -- logs ' + cd );
                        cu.Name = cd.name+' : '+wl.Name;
                        cu.Counter_Detail__c = cd.id;
                        cu.Work_Detail__c = wl.id;
                        if ( cd.erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.material_group__c == 'H:CREDTS' &&  cd.erp_Nwa__r.wbs_element__r.sales_Order_Item__r.product__r.name.containsignorecase('advantage'))
                            cu.value__c = cd.ERP_NWA__r.ERP_Estimated_Work__c;
                        else
                            cu.Value__c = 1; 
                        cuList.add(cu);   
                    } 
                    else 
                    {
                        cu = wl.Counter_Usage__r[0];
                    }
                    if (wl.lms_status__c == 'CNF') 
                    {
                        mapuwd.put(wl.id,new SVMXC__Service_Order_Line__c(id=wl.id,Completed__c=true)); // to trigger the actual counter to get updated
                        system.debug(mapuwd.get(wl.id));
                    } 
                    if (wl.lms_status__c == 'MCNF' && wdc.registration_status__c != null &&  (trigger.oldmap == null || ((Work_Detail_Contacts__c)trigger.oldmap.get(wdc.id)).registration_status__c == null) ) 
                    {
                        //May-18-2018 - Chandramouli - STSK0014827 - Modified code to fix the exception.
                        if (cd.SVMXC__Counter_Reading__c == cu.value__c)
                        {
                            cd.SVMXC__Counter_Reading__c -= cu.value__c;
                            mapucd.put(cd.id,cd);
                        }
                    }
                    //Jan-16-2018 - Chandramouli - STSK0013635 - Modified code to handle coupon cancellations from LMS.
                    if (wl.lms_status__c == 'REL' && wdc.registration_status__c == 'Cancelled' && cu.Status__c != null && cu.Status__c != 'Cancelled' && (trigger.oldmap == null || ((Work_Detail_Contacts__c)trigger.oldmap.get(wdc.id)).registration_status__c != 'Cancelled') )
                    {
                        //May-18-2018 - Chandramouli - STSK0014827 - Modified code to fix the exception.
                        if (cd.SVMXC__Counter_Reading__c < cu.value__c)
                        {
                            cd.SVMXC__Counter_Reading__c += cu.value__c;
                            mapucd.put(cd.id,cd);
                        }
                        
                    }
                }
            }
        }
        
        if(!cuList.isEmpty()){
            insert cuList;
        }
        if (mapuwd.size()>0)
            update mapuwd.values();
        if (mapucd.size()>0)
            update mapucd.values();
    }
}
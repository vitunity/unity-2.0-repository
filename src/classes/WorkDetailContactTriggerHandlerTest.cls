/*
 * @author Amitkumar Katre
 * @description - test class for WorkDetailContactTriggerHandler
 * @date - 11/26/2015
 */
@isTest(seealldata=false)
public class WorkDetailContactTriggerHandlerTest {
  
    /* test data members */
    static Profile vmsserviceuserprofile;
    static User serviceuser;
    static User serviceuser1;
    static SVMXC__Service_Group_Members__c technician;
    static Technician_Assignment__c techassgn;
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    static SVMXC__Site__c location;
    static Account acc;
    static SVMXC__Service_Order_Line__c wd;
    static SVMXC__Service_Group__c servicegrp;
    static Organizational_Activities__c orgactivities;
    static Timecard_Profile__c timecardprof ;
    static Timecard_Profile__c indirecttimecardprof ;
    static ERP_Project__c  erpproject; 
    static ERP_WBS__c erpwbs;
    static Country__c con;
    static Contact c;
    static ERP_NWA__c erpnwa;
    static SVMXC_Time_Entry__c timeEntry;
    static SVMXC_Timesheet__c timeSheet;
    static Timecard_Profile__c timecardProfile;
    static Organizational_Activities__c orgActivites;
    static Product2 product;
    static SVMXC__Installed_Product__c ip;
    static ERP_Org__c eorg;
   // static SVMXC__Product_Replacement__c productReplacemnt; 
    static BusinessHours bh = [select id from BusinessHours limit 1];
    /* Initialize test data in static block */
    static{
        //vmsserviceuserprofile = new profile();
        vmsserviceuserprofile = [Select id from profile where name = 'VMS System Admin'];
       serviceuser = [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and id!=:userInfo.getUserId() limit 1];
        //serviceuser1 = [Select id from user where profileId= :vmsserviceuserprofile.id and isActive = true and id!=:serviceuser.id limit 1]; 
          
        system.debug('service user' + serviceuser.id);
        //system.debug('service user 1 ' + serviceuser1.id);  
        
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        
        timecardprof = new Timecard_Profile__c(First_Day_of_Week__c = 'Saturday', Name = 'Test TimeCard Profile');
        timecardprof.Normal_Start_Time__c = datetime.newInstance(2014, 12, 1, 9, 0, 0);
        //insert timecardprof;
        
        indirecttimecardprof = new Timecard_Profile__c( Name = 'PM Indirect Time Entry Matrix profile'); //indirect
        //insert indirecttimecardprof;
        
        orgactivities = new Organizational_Activities__c(Name = 'Allocations Prep / Reporting',    Type__c = 'Indirect', Indirect_Hours__c = indirecttimecardprof.id); //indirect
        //insert orgactivities;
        
        timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        //insert timecardProfile;
        
        technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician',User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        //technician.Timecard_Profile__c = timecardProfile.Id;
        insert technician;
        
        con = new Country__c(Name = 'United States');
        //insert con;
    
        acc = new Account(Name = 'Test111222', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id); 
        acc.BillingCountry='USA';
        acc.Country__c= 'USA';
        acc.BillingCity= 'Los Angeles';
        acc.BillingStreet ='3333 W 2nd St';
        acc.BillingState ='CA';
        acc.BillingPostalCode = '90020';
        insert acc;   
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.name = 'Default';
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        insert cdf;
       
        location = new SVMXC__Site__c(Sales_Org__c = 'testorg', SVMXC__Service_Engineer__c = userInfo.getUserId(), SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = acc.id);
        location.Plant__c = 'testorg';
        Location.Facility_Code__c = '1234';
        insert location;
        
        casealias = new Case(Subject = 'testsubject',priority='medium');
        insert casealias;
        
        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;
        
        erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Reference__c = 'erpnwa1234');
        insert erpnwa;
        
        erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id);
        //insert erpwbs;
        
        wo = new SVMXC__Service_Order__c(SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Case__c = casealias.id,ERP_Service_Order__c = '12345678');
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();        
        wo.ownerid= serviceuser.id;
        insert wo;
        
        product = new Product2(Name='Connect Utilities');
        insert product;
        
        ip = new SVMXC__Installed_Product__c(Name = 'test',SVMXC__Product__c=product.id);
        insert ip;
       
       /* Future release
        productReplacemnt =  new SVMXC__Product_Replacement__c();
        productReplacemnt.SVMXC__Replacement_Product__c = product.Id;
        productReplacemnt.SVMXC__Base_Product__c = product.id;
        insert productReplacemnt;
        */
        
        techassgn = new Technician_Assignment__c(Work_Order__c = wo.id,Technician_Name__c = technician.id);
        insert techassgn;
    
        orgActivites = new Organizational_Activities__c();
        orgActivites.Holiday_Cash__c = true;
        //insert orgActivites;
        
        eorg = new ERP_Org__c();
        eorg.name = 'testorg';
        eorg.Sales_Org__c = 'testorg';
        eorg.Bonded_Warehouse_used__c = true;
        //insert eorg;
        
  }
    
    public static testmethod void testWorkDetailContactTriggerHandler2(){
        Test.startTest();
        wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Consumed_From_Location__c = location.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Part_Disposition__c = 'Installing';
        wd.Completed__c=false;
        wd.SVMXC__Group_Member__c = technician.id;
        wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.Actual_Hours__c = 5;
        wd.SVMXC__Service_Order__c = wo.id;
        wd.NWA_Description__c = 'nwatest';
        wd.ERP_NWA__c = erpnwa.id;
        wd.SVMXC__Serial_Number__c = ip.id;
        wd.SVMXC__Start_Date_and_Time__c = System.now();
        wd.SVMXC__End_Date_and_Time__c = System.now().adddays(7);
       
        insert wd;
        wd.Cancel__c = true;
        wd.SVMXC__Line_Status__c = 'Submitted';        
        update wd;
        Test.stopTest();
    }
    
    public static testmethod void testWorkDetailContactTriggerHandler1(){
        Test.startTest();
        wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Consumed_From_Location__c = location.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Part_Disposition__c = 'Installing';
        wd.Completed__c=false;
        wd.SVMXC__Group_Member__c = technician.id;
        wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.Actual_Hours__c = 5;
        wd.SVMXC__Service_Order__c = wo.id;
        wd.NWA_Description__c = 'nwatest';
        wd.ERP_NWA__c = erpnwa.id;
        wd.SVMXC__Serial_Number__c = ip.id;
        wd.SVMXC__Start_Date_and_Time__c = System.now();
        wd.SVMXC__End_Date_and_Time__c = System.now().adddays(7);
       
        insert wd;
        //insert Counter Detail //
        Id coverageRTId = Schema.SObjectType.SVMXC__Counter_Details__c.getRecordTypeInfosByName().get('Coverage').getRecordTypeId();
        SVMXC__Counter_Details__c  cdt = new SVMXC__Counter_Details__c (SVMXC__Active__c = true, RecordTypeId =coverageRTId, SVMXC__Counters_Covered__c = 12.00 , ERP_Serial_Number__c = 'H12345', SVMXC__Counter_Name__c = 'adfgh',ERP_Reference__c = 'abc');
        insert cdt;
    
        Work_Detail_Contacts__c wdc = new Work_Detail_Contacts__c();
        wdc.Work_Detail__c = wd.id;
        wdc.Participant_Email__c = 'amitkumar.katre@varian.com';
        wdc.Event_Nbr__c = 'testnbr';
        wdc.Facility__c = '1234';
        wdc.Reference__c = 'abc';
        wdc.ERP_NWA_Nbr__c = 'erpnwa1234';
        wdc.order_no__c = 'ORDNO-0001';
        wdc.LMS_Status__c = 'REL';
        insert wdc;

        wdc.ERP_NWA_Nbr__c = 'WL12345';
        update wdc;
        
        Test.stopTest();
    }
}
/*
 *  Author - Amitkumar Katre
 *  Covered or Not Covered issues fix.
 */
public class WorkDetailCoveredBillingTypeHandler {
  
    /*
     * This will be aplicable for Helpdesk
     */
    public static void hdWOCovredBillingType(list<SVMXC__Service_Order_Line__c> wdlList, map<Id,SVMXC__Service_Order_Line__c> oldMap){
        for(SVMXC__Service_Order_Line__c wd : wdlList){
            if(wd.Work_Order_RecordType__c  == 'Helpdesk' 
               && wd.Covered_or_Not_Covered__c == null
               && (wd.Billing_Type__c == 'C – Contract' || wd.Billing_Type__c == 'C - Contract')){
                    wd.Covered_or_Not_Covered__c = 'COVERED';      
          }
        }
    }
}
public with sharing class WorkDetailInLineExtension {
    
    public list<WorkDetailWrapper> wdlWrapperList{get;set;}
    private ApexPages.StandardController sc;
    public list<SVMXC__Service_Order_Line__c> wdlList{get;set;}
    
    public WorkDetailInLineExtension(ApexPages.StandardController sc){
        this.sc = sc;
        wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c');
        initializeData();
    }
    
    private void initializeData(){
        wdlWrapperList = new list<WorkDetailWrapper> ();
        wdlList = getSobjectChildRecords('SVMXC__Service_Order_Line__c',sc.getId(),'SVMXC__Service_Order__c');
        for(SVMXC__Service_Order_Line__c wdObj : wdlList){
            wdlWrapperList.add(new WorkDetailWrapper(wdObj,false));
        }
        for (SORT_BY enumValue : SORT_BY.values()){
            sortByValues.put(String.valueOf(enumValue), enumValue);
        }
    }
    
    public PageReference cancel(){
        return sc.view();
    }
    public PageReference save(){
        try{
            list<SVMXC__Service_Order_Line__c> wdlListUpdate = new list<SVMXC__Service_Order_Line__c>();
            for(WorkDetailWrapper wdWrapOBj : wdlWrapperList){
                SVMXC__Service_Order_Line__c wdl = wdWrapOBj.wdObj;
                wdlListUpdate.add(wdl);
            }
            update wdlListUpdate;
            String strUrl = ApexPages.currentPage().getUrl();
            if(strUrl.contains('WorkDetailInLineEditAdmin')){
             	PageReference pg = new PageReference('/apex/WorkDetailInLineEditAdmin?id='+sc.getId());
            	pg.setRedirect(true);
            	return pg;   
            }
            if(strUrl.contains('WorkDetailInLineEditSupport')){
             	PageReference pg = new PageReference('/apex/WorkDetailInLineEditSupport?id='+sc.getId());
            	pg.setRedirect(true);
            	return pg;   
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getmessage()));
            return null;
        }
        return null;
    }
    
    
    private list<Sobject> getSobjectChildRecords(String objectName, String recordId,String parentFieldName){
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
        String query = 'SELECT';
        for(String s : objectFields.keySet()) {
           query += ' ' + s + ', ';
        }
        /*
        if (query.subString(query.Length()-2,query.Length()-1) == ','){
            query = query.subString(0,query.Length()-2);
        }*/
        
        query += ' RecordType.DeveloperName '; 
        
        query += ' FROM ' + objectName;
        query += ' WHERE '+parentFieldName+' = :recordId '; 
        system.debug('Query ==='+query);
        List<Sobject> sobjectList = database.query(query);
        return sobjectList;
    }
    
    public enum SORT_BY {
            ByName,
            ByLineType,
            ByStartDate,
            BySAPBillable
    }
    
    private map<String, SORT_BY> sortByValues = new Map<String, SORT_BY>();
    
    private map<String,Integer> sortOrderMap = new map<String,Integer>{'ByName'=> 1,
                                                                       'ByLineType'=> 1,
                                                                       'ByStartDate'=> 1,
                                                                       'BySAPBillable'=> 1};
                                                                       
    private static map<String,String> coloumnsWithApiNames = new map<String,String>{'ByName'=> 'Name',
                                                                             'ByLineType'=> 'SVMXC__Line_Type__c',
                                                                             'ByStartDate'=>'SVMXC__Start_Date_and_Time__c',
                                                                             'BySAPBillable'=> 'ERP_Billable__c'};
    public static Integer sortOrderVal ;
    
    public static SORT_BY sortBy = SORT_BY.ByName;
    
    public PageReference sortByColumns(){
        String param = ApexPages.currentPage().getParameters().get('field');
        system.debug('param============='+param);
        sortOrderVal = sortOrderMap.get(param);
        sortOrderMap.put(param,-sortOrderVal);
        sortBy = sortByValues.get(param);
        wdlWrapperList.sort();      
        return null;  
    }
        
    public class WorkDetailWrapper implements Comparable {
        public SVMXC__Service_Order_Line__c wdObj {get;set;}
        public boolean selectBox {get;set;}

        public WorkDetailWrapper(SVMXC__Service_Order_Line__c wdObj, boolean selectBox){
            this.selectBox = selectBox;
            this.wdObj = wdObj;
        }
        
        public Integer compareTo(Object obj) {
            WorkDetailWrapper wObj = (WorkDetailWrapper)(obj); 
            String fileApiName = coloumnsWithApiNames.get(String.valueOf(sortBy));
            SObject wData = wObj.wdObj;
            SObject wDataThis = this.wdObj;

            if(wDataThis.get(fileApiName) instanceof String || wDataThis.get(fileApiName) instanceof boolean){
                if (String.valueOf(wDataThis.get(fileApiName)) > String.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (String.valueOf(wDataThis.get(fileApiName)) == String.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }
            
            if(wDataThis.get(fileApiName) instanceof DateTime){
                if (DateTime.valueOf(wDataThis.get(fileApiName)) > DateTime.valueOf(wData.get(fileApiName))) {
                return sortOrderVal;
                }
                if (DateTime.valueOf(wDataThis.get(fileApiName)) == DateTime.valueOf(wData.get(fileApiName))) {
                    return 0;
                }
            }            
            return -sortOrderVal;
        }   
    }
    
}
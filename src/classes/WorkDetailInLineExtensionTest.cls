@isTest
public class WorkDetailInLineExtensionTest {

    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;
    public static CountryDateFormat__c dateFormat;

    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    
    Static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        newAcc = UnityDataTestClass.AccountData(true);
        
        //CountryDateFormat__c custom setting data creation
        dateFormat = new CountryDateFormat__c(Name = 'Default', Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat;
        
        WO = new SVMXC__Service_Order__c();
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Reviewed';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Interface_Status__c = 'Processed';
        insert WO;
        
        WDList = new List<SVMXC__Service_Order_Line__c>();
        WD = new SVMXC__Service_Order_Line__c();
        WD.SVMXC__Activity_Type__c = 'Install';
        WD.Completed__c=false;
        WD.SVMXC__Group_Member__c = technician.id;
        WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        WD.SVMXC__Work_Description__c = 'test';
        WD.SVMXC__Start_Date_and_Time__c =System.now();
        WD.Actual_Hours__c = 5;
        WD.SVMXC__Service_Order__c = WO.id;
        WD.NWA_Description__c = 'nwatest';
        WD.SVMXC__Line_Status__c = 'Open';
        insert WD;
    }
    
    private static testMethod void testWDLInLineEdit() {
        Test.StartTest();
        WD1 = new SVMXC__Service_Order_Line__c();
        WD1.SVMXC__Activity_Type__c = 'Configure';
        WD1.SVMXC__Group_Member__c = technician.id;
        WD1.SVMXC__Work_detail__c = WD.Id;
        WD1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD1.SVMXC__Work_Description__c = 'test';
        WD1.SVMXC__Start_Date_and_Time__c = System.now();
        WD1.SVMXC__End_Date_and_Time__c = System.now().addMinutes(10);
        WD1.SVMXC__Service_Order__c = WO.id;
        WD1.SVMXC__Line_Status__c = 'Submitted';
        WD1.SVMXC__Line_Type__c = 'Labor';
        WD1.Interface_Status__c = 'Processed';
        insert WD1;
        
        ApexPages.StandardController sc =  new ApexPages.StandardController(WO);
        WorkDetailInLineExtension extension = new WorkDetailInLineExtension(sc);
        for(WorkDetailInLineExtension.WorkDetailWrapper wdp : extension.wdlWrapperList){
            wdp.selectBox = true;
        }

       
        ApexPages.currentPage().getParameters().put('field','ByLineType');
        extension.sortByColumns();
        WorkDetailInLineExtension.WorkDetailWrapper workDetailWrap2 = new WorkDetailInLineExtension.WorkDetailWrapper(WD1,true);
        workDetailWrap2.compareTo(workDetailWrap2);  
        ApexPages.currentPage().getParameters().put('field','ByStartDate');
        extension.sortByColumns();
        WorkDetailInLineExtension.WorkDetailWrapper workDetailWrap = new WorkDetailInLineExtension.WorkDetailWrapper(WD1,true);
        workDetailWrap.compareTo(workDetailWrap);  
        extension.save();
        extension.cancel();
        Test.stopTest();
    }
}
/*
 * @author - Amitkumar Katre
 * On Usage/Consumption WDL insert/update, if WDL.Technician.Set WO Do Not Process = True, 
 * then set WDL.Interface Status = 'Do Not Process'
 */
public class WorkDetailInterfaceStatusHandler {
    
    private static Id UsageConsumptionWDL = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order_Line__c').get('UsageConsumption');
     
    public static void execute(list<SVMXC__Service_Order_line__c> workDetailList, 
                               map<Id, SVMXC__Service_Order_line__c> oldMap){
        
        system.debug('invoked this WorkDetailInterfaceStatusHandler====');
        set<id> technicianIDs = new set<Id>();
        for(SVMXC__Service_Order_line__c wdl : workDetailList){
            if(wdl.SVMXC__Line_Status__c == 'Open' 
               && wdl.recordTypeId == UsageConsumptionWDL
               && wdl.SVMXC__Group_Member__c != null 
               && (oldMap == null || oldMap.get(wdl.Id).Interface_Status__c != wdl.Interface_Status__c)){
               technicianIDs.add(wdl.SVMXC__Group_Member__c);
            }
        }
        if(!technicianIDs.isEmpty()){
            map<Id, SVMXC__Service_Group_Members__c> technicianMap = new map<Id, SVMXC__Service_Group_Members__c>([
                select Id,Set_WO_Do_Not_Process__c from SVMXC__Service_Group_Members__c where Id in :technicianIDs 
            ]);
            for(SVMXC__Service_Order_line__c wdl : workDetailList){
                if(technicianMap.containsKey(wdl.SVMXC__Group_Member__c)
                   && technicianMap.get(wdl.SVMXC__Group_Member__c).Set_WO_Do_Not_Process__c){
                    wdl.Interface_Status__c = 'Do not process';
                }
            }
        }        
    }
}
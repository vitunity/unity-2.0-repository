@isTest
public class WorkDetailRelatedActivityDelHandlerTest {
	
    public Static Profile pr = [Select id from Profile where name = 'VMS BST - Member'];
        
    public Static User u = new user(alias = 'standt', email = 'standardtestuse92@testorg.com',
                                    emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',
                                    localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', 
                                    username = 'standardtestuse92@testclass.com');
    public static Account newAcc;
    public static SVMXC__Service_Group__c servTeam;
    public static Product2 newProd;
    public static SVMXC__Site__c newLoc;
    public static ERP_Pcode__c erpPcode;
    public static SVMXC__Installed_Product__c ip;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static Sales_Order_Item__c PSOI;
    public static List<Sales_Order_Item__c> SOI_LIST; 
    public static SVMXC__Service_Level__c slaTerm;
    public static SVMXC__Service_Contract__c servcContrct;
    public static SVMXC__Service_Contract_Products__c contractProd;
    public static SVMXC__Counter_Details__c counter;
    public static Counter_Usage__c usage;
    public static ERP_WBS__c WBS;
    public static ERP_WBS__c WBS2;
    public static List<ERP_WBS__c> WBSLIST;
    public static ERP_NWA__c NWA;
    public static ERP_NWA__c NWA2;
    public static ERP_Project__c erpProj;
    public static BusinessHours businesshr;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WrkDetail;
    public static List<SVMXC__Service_Order_Line__c> WrkDetailList;
    public static Case newCase;
	
    static {
      
      newAcc = UnityDataTestClass.AccountData(true);
      
      servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        
        newProd = UnityDataTestClass.product_Data(true);
        
        // When Service Group inserted Location also get inserted
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        erpPcode = UnityDataTestClass.ERPPCODE_Data(true);
      
      ip = UnityDataTestClass.InstalledProduct_Data(true, newProd, newAcc, newLoc, servTeam, erpPcode);
      
      SO = UnityDataTestClass.SO_Data(false, newAcc);
      insert SO;
      
      PSOI = UnityDataTestClass.SOI_Data(true, SO);
      SOI = UnityDataTestClass.SOI_Data(false, SO);
      SOI.Parent_Item__c = PSOI.Id;
      SOI_LIST = new List<Sales_Order_Item__c>();
      SOI_LIST.add(SOI);
      SOI_LIST.add(PSOI);
      insert SOI;
      //insert SOI_LIST;
      
      erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
      
      newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
      
      WBS = UnityDataTestClass.ERPWBS_DATA(false, erpProj, technician);
        WBS.Sales_Order_Item__c = SOI.Id;
        //insert WBS;
        
        WBS2 = UnityDataTestClass.ERPWBS_DATA(false, erpProj, technician);
        WBS2.Sales_Order_Item__c = PSOI.Id;
        WBSList = new List<ERP_WBS__c>();
        WBSLIST.add(WBS);
        WBSLIST.add(WBS2);
        insert WBSLIST;
        
        List<ERP_NWA__c> NWALIst = new List<ERP_NWA__c>();
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        nwa.ERP_Status__c = 'CLSD';
        NWA.ERP_Network_Nbr__c = 'ABC123';
        NWA.ERP_Activity_Nbr__c = 'ABC123';
        //insert NWA;
        NWALIst.add(NWA);
        
        NWA2 = NWA.clone();
        NWA2.ERP_Status__c = 'REL';
        NWALIst.add(NWA2);
        insert NWAList;
        
        WO = new SVMXC__Service_Order__c();
        //WO.SVMXC__Case__c = newCase.id;
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Open';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        WO.SVMXC__Product__c = newProd.ID;
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Subject__c= 'bbb';
        //WO.ERP_WBS__c = WBS.Id;
        WO.Sales_Order_Item__c = SOI.id;
        //WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
        //WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        //WO.ERP_WBS__c = WBS.Id;
        //WO.ERP_Service_Order__c = '123';
        //WO.Installation_Manager__c = userInfo.getUserId();
        insert WO;
        
        WrkDetailList = new List<SVMXC__Service_Order_Line__c>();
        
        WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
        
        WD.SVMXC__Consumed_From_Location__c = newLoc.id;
        WD.SVMXC__Activity_Type__c = 'Install';
        WD.Completed__c=false;
        WD.SVMXC__Group_Member__c = technician.id;
        WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WD.SVMXC__Work_Description__c = 'test';
        WD.SVMXC__Start_Date_and_Time__c =System.now();
        WD.Actual_Hours__c = 5;
        WD.SVMXC__Service_Order__c = WO.id;
        WD.Sales_Order_Item__c = SOI.id;
        WD.NWA_Description__c = 'nwatest';
        WD.SVMXC__Line_Status__c = 'Open';
        WD.Billing_Type__c = 'Installation';
        WD.SVMXC__Line_Type__c = 'Labor';
        //insert WD;
        WrkDetailList.add(WD);
        
        WrkDetail = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
        
        WrkDetail.SVMXC__Consumed_From_Location__c = newLoc.id;
        WrkDetail.SVMXC__Activity_Type__c = 'Install';
        WrkDetail.Completed__c=false;
        WrkDetail.SVMXC__Group_Member__c = technician.id;
        WrkDetail.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        WrkDetail.SVMXC__Work_Description__c = 'test';
        WrkDetail.SVMXC__Start_Date_and_Time__c =System.now();
        WrkDetail.Actual_Hours__c = 5;
        WrkDetail.SVMXC__Service_Order__c = WO.id;
        WrkDetail.Sales_Order_Item__c = SOI.id;
        WrkDetail.NWA_Description__c = 'nwatest';
        WrkDetail.SVMXC__Line_Status__c = 'Open';
        WrkDetail.Billing_Type__c = 'Installation';
        WD.SVMXC__Line_Type__c = 'Labor';
        WrkDetailList.add(WrkDetail);
        
        insert WrkDetailList;
        
      
    }
    
    public static testMethod void valldiateDelete_NOBST() {
        Test.startTest();
        WorkDetailRelatedActivityDeleteHandler.valldiateDelete(new lisT<SVMXC__Service_Order_Line__c>{WD});
    	Test.stopTest();
    }
    
    public static testMethod void valldiateDelete_BST() {
        Test.startTest();
        system.runAs(u) {
        	WorkDetailRelatedActivityDeleteHandler.valldiateDelete(new lisT<SVMXC__Service_Order_Line__c>{WD});
        }
        Test.stopTest();
    }
    
    public static testMethod void deleteRelatedActivitiesTest() {
        Test.startTest();
         	WrkDetail.Related_Activity__c = WD.Id;
        	update WrkDetail;
        
        	WorkDetailRelatedActivityDeleteHandler.deleteRelatedActivities(new lisT<SVMXC__Service_Order_Line__c>{WD});
        Test.stopTest();
    }
    
}
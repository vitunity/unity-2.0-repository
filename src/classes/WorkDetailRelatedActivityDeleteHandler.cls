/*
 * Author : Amitkumar Katre
 * Description  
 * - Deletion of Master Activity should Delete Related Parts Below
 * - Should not allow wdl record to delete if it is Closed and Processed
 * Change Log -
 * 4-Apr-2018 - Nilesh Gorle - STSK0014169 - Commented lines to avoid recursion error
 */
public class WorkDetailRelatedActivityDeleteHandler {

    @TestVisible private static String psrecordtypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
    
    public static void invoke(lisT<SVMXC__Service_Order_Line__c> wdList){
        valldiateDelete(wdList);
        deleteRelatedActivities(wdList);
    }
    
    /*
     * Should not allow wdl record to delete if it is Closed and Processed
     */
    @TestVisible  private static void valldiateDelete(lisT<SVMXC__Service_Order_Line__c> wdList){
        //INC4456756 : Skiping WDL deletion Validation for BST member and Service Admin
        List<String> profileName = new List<String>{'VMS BST - Member', 'VMS Service - Admin'};
        Map<String, Profile> profileIDMap = new Map<String, Profile>([SELECT Id, Name FROM Profile WHERE Name IN: profileName ]);
        for(SVMXC__Service_Order_Line__c wd : wdList){
            if(!profileIDMap.containsKey(userInfo.getProfileId())) {
                if(wd.Interface_Status__c == 'Processed' || wd.SVMXC__Line_Status__c == 'Closed' ){
                    wd.addError('Closed/Processed WDLs should not be deleted');
                }
            }            
        }
    }

    
    /*
     * Delete related activities if master is deleted
     */
    @TestVisible private static void deleteRelatedActivities(lisT<SVMXC__Service_Order_Line__c> wdList){
        Set<Id> deleteWDLIds = new Set<Id> ();
        for(SVMXC__Service_Order_Line__c wd : wdList){
            if(wd.SVMXC__Line_Type__c == 'Labor'){
                deleteWDLIds.add(wd.Id);
            }
        }
        if(!deleteWDLIds.isEmpty()){

            list<SVMXC__RMA_Shipment_Line__c > partList = new list<SVMXC__RMA_Shipment_Line__c >();
            list<SVMXC__Service_Order_Line__c> partWDL = [select id, Parts_Order_Line__c from SVMXC__Service_Order_Line__c 
                                                          where (Related_Activity__c in : deleteWDLIds) ];
            for(SVMXC__Service_Order_Line__c wdl :  partWDL){
                if(wdl.Parts_Order_Line__c != null){
                    partList.add(new SVMXC__RMA_Shipment_Line__c(Id = wdl.Parts_Order_Line__c));
                }
            }
            /* STSK0014169 - Commented below code to avoid recursive deletion error */
            //if(!partWDL.isEmpty()){          
                //delete partWDL;
            //}
            // To re-calculate Remaining Qty
            if(!partList.isEmpty()){
                update partList;
            }

        }
    }
}
global class WorkDetailUpdateUtility{
    @future
    public static void updateWorkDetailLookup(set<Id> setWD){
        
        system.debug('#WD1:'+setWD);
        Id ProdServicedRecTypeID = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Products Serviced').getRecordTypeId();
        Map<ID, List<SVMXC__Service_Order_Line__c>> mapWorkOrderID_ProdServWD = new Map<ID, List<SVMXC__Service_Order_Line__c>>();
        List<SVMXC__Service_Order_Line__c> LstUpdateWorkDetail = new List<SVMXC__Service_Order_Line__c>();
        
        List<SVMXC__Service_Order_Line__c> LstWorkDetail = [Select ID, SVMXC__Service_Order__c, case_line__r.SVMXC__Installed_Product__c,SVMXC__Serial_Number__c,Work_Order_recordtype__c,ERP_PCSN__c,ERP_Proj_Nbr__c,ERP_network_nbr__c,ERP_Activity_Nbr__c,ERP_Contract_Nbr__c,Sub_Equipment__c,Malfunction_Start_Date__c from SVMXC__Service_Order_Line__c where  Id IN: setWD];
        set<Id> setNewWOID = new set<Id>();
        
        for(SVMXC__Service_Order_Line__c wd : LstWorkDetail){
            setNewWOID.add(wd.SVMXC__Service_Order__c);
        }
        system.debug('#WD2:'+setNewWOID);
        for(SVMXC__Service_Order_Line__c varWD: [Select ID, SVMXC__Service_Order__c, SVMXC__Serial_Number__c,Work_Order_recordtype__c,ERP_PCSN__c,ERP_Proj_Nbr__c,ERP_network_nbr__c,ERP_Activity_Nbr__c,ERP_Contract_Nbr__c,Sub_Equipment__c,Malfunction_Start_Date__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in : setNewWOID and RecordTypeId =: ProdServicedRecTypeID])                    
        {
            List<SVMXC__Service_Order_Line__c> tempListWD = new List<SVMXC__Service_Order_Line__c>();
            if(mapWorkOrderID_ProdServWD.containsKey(varWD.SVMXC__Service_Order__c))
            {
                tempListWD = mapWorkOrderID_ProdServWD.get(varWD.SVMXC__Service_Order__c);
            }
            tempListWD.add(varWD);
            mapWorkOrderID_ProdServWD.put(varWD.SVMXC__Service_Order__c, tempListWD);
        }
        system.debug('#WD3:'+mapWorkOrderID_ProdServWD);
        for(SVMXC__Service_Order_Line__c wd : LstWorkDetail){
            if(mapWorkOrderID_ProdServWD.containsKey(wd.SVMXC__Service_Order__c))
            {
                for(SVMXC__Service_Order_Line__c varWD : mapWorkOrderID_ProdServWD.get(wd.SVMXC__Service_Order__c))
                {
                    if(varWD.SVMXC__Serial_Number__c == wd.case_line__r.SVMXC__Installed_Product__c)
                    {
                        if(varWD.ID  <> wd.Id){
                            wd.SVMXC__Work_Detail__c = varWD.ID;
                            LstUpdateWorkDetail.add(wd);
                        }
                    }
                }
            }
        }
        system.debug('#WD4:'+LstUpdateWorkDetail);
        if(LstUpdateWorkDetail.size() > 0){
            update LstUpdateWorkDetail;
        }
        
    }
}
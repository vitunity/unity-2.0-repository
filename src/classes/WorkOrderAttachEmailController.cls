/*************************************************************************\
@ Author        : Deepak Sharma
@ Date          :  20-Aug-2017
@ Description   : Class defined to handle logic to display attachments of workorder 
User has option to select  attachment and hence email the selected one

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
***************************************************************************
03-Jan-2018 Nick Sauer        STSK0013526    1) Change Subject of Email
                                             2) Checkbox to turn auto-CC on/off
14-Nov-2017 Nick Sauer        STSK0013367    1) Added Contact Role Association
23-Oct-2017 Nick Sauer        STSK0013191    1) Add current User email to CC on email generation
23-Oct-2017 Nick Sauer        STSK0013183    1) Add email signature properties
20-Sep-2017 Deepak Sharma     STSK0012812    1) Change manual addition from comma delineated to semicolon delineated to allow copy/paste from Outlook 
30-Aug-2017 Deepak Sharma    STSK0012673     1)  On generation of Activity, insert To Email Addresses used
                                             2)  On generation of Activity, updated Activity related list with Attachments sent 
                                             3)  Adjust  order by fields
****************************************************************************/

public class WorkOrderAttachEmailController {
    
    @TestVisible private String workOrderId;
    private list<Attachment> attachmentList;
    @TestVisible private List<String> sendTo = new List<String>();
    private String prameters = '';
    private list<AttachmentWrapper> attachmentWrapperList;
    private list<ContactWrapper> contactWrapperList;
    //NS: Add Contact Role Association
    private list<ContactRoleWrapper> craWrapperList;
    public String emailBody {get;set;}
    public String emailSub {get;set;}
    public SVMXC__Service_Order__c woObj {get;set;}
    public Boolean showCombinedMailBtn{get;set;}
    public Boolean showMailBtn{get;set;}
    public String addMailRecepients{get;set;}
    private List<String> fetchMailIds=null;
    //STSK0013191:  Derive current User Email for mailCC
    public String currentUserEmail = UserInfo.getUserEmail();
    public String[] ccAddresses = new String[]{currentUserEmail};
    public Boolean emailCC{get;set;}
        
        
        /**
Constructor 
Fetches the workorderinformation for given workorder ID
*/
        public WorkOrderAttachEmailController(){
            try{
                woObj = new SVMXC__Service_Order__c();
                attachmentList = new list<Attachment> ();
                attachmentWrapperList = new list<AttachmentWrapper> ();
                contactWrapperList = new list<ContactWrapper> ();
                //NS:  Add Contact Role Association
                craWrapperList = new list<ContactRoleWrapper> ();
                workOrderId = ApexPages.currentPage().getParameters().get('woId');
                if(String.isBlank(workOrderId)){
                    throw new WorkOrderEmailException('Please pass work order id as parameter');
                }else{
                    list<SVMXC__Service_Order__c> woList =[select Name,  Case_Number__c, SVMXC__Site__r.Name, SVMXC__Site__r.SVMXC__City__c, SVMXC__Company__c, SVMXC__Case__r.CaseNumber, Contact_Email__c, SVMXC__Contact__r.Name,SVMXC__Company__r.Name from SVMXC__Service_Order__c where Id =: workOrderId];
                    if(woList.isEmpty()){
                        throw new WorkOrderEmailException('Work order not found for id :'+workOrderId);
                    }else{
                        woObj = woList[0];
                        emailSub='Documents: Case '+ woObj.Case_Number__c + ', '+woObj.Name+', '+woObj.SVMXC__Site__r.Name+', '+woObj.SVMXC__Site__r.SVMXC__City__c;
                    }
                }
            }catch(Exception e){
                ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
                ApexPages.addMessage(myMsg1);
            }
        }
    
    /***
* @Desc: fetches the Attachment of given workorder id
* * @param : None
* @return :list<AttachmentWrapper>
**/
    public list<AttachmentWrapper> getAttachmentWrapperList(){
        for(Attachment attObj : [select Id, Name,LastModifiedBy.Name, LastModifiedDate from Attachment where parentId = : workOrderId]){
            AttachmentWrapper awObj = new AttachmentWrapper();
            awObj.attachmentObj = attObj;
            attachmentWrapperList.add(awObj);
        }
        if(attachmentWrapperList.isEmpty()){
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Error,'No Attachments are Currently Available on Work Order.  Please add prior to use of Email functionality');
            ApexPages.addMessage(myMsg1);
            
        }
        if(!(attachmentWrapperList.isEmpty()))
        {
            showMailBtn=true;
            if(attachmentWrapperList.size()>1){
                showCombinedMailBtn=true;
            }
        }
        return attachmentWrapperList;
    }
    
    /***
* @Desc: fetches the Contacts  of given account id
* * @param : None
* @return :list<ContactWrapper>
**/
    
    public list<ContactWrapper> getContactWrapperList(){
        //STSK0012673 start -Added order By
        for(Contact conObj : [select Id, Name,Functional_Role__c, Email from Contact where AccountId != null and Active_Contact__c = : woObj.SVMXC__Company__c and Email != null order by name]){
            ContactWrapper cwObj = new ContactWrapper();
            cwObj.contactObj = conObj;
            contactWrapperList.add(cwObj);
        }
        if(contactWrapperList.isEmpty()){
            
        }
        return contactWrapperList;
    }
    
    //STSK0013367:  Contact Role Association list and query   
    public list<ContactRoleWrapper> getcraWrapperList(){
        for(Contact_Role_Association__c craObj : [select Id,Contact__r.Name, Functional_Role__c,Contact_Email__c from Contact_Role_Association__c where Account__c != null and Account__c = : woObj.SVMXC__Company__c and Contact_Email__c != null order by Contact__r.Name]){
            ContactRoleWrapper craList = new ContactRoleWrapper();
            craList.contactroleObj = craObj;
            craWrapperList.add(craList);
        }
        if(craWrapperList.isEmpty()){
            
        }
        return craWrapperList;
    }
    /***
* @Desc: Hanlde logic to validate the input
* * @param : None
* @return :PageReference
**/
    
    @TestVisible private void validate(){
        for(AttachmentWrapper awt : attachmentWrapperList){
            if(awt.selectObj){
                prameters = prameters+awt.attachmentObj.Id + '|';
            }
        }
        if(String.isBlank(prameters)){
            throw new WorkOrderEmailException('Please select attachments');
        }
        
        if(String.isBlank(emailSub)){
            throw new WorkOrderEmailException('Please enter email Subject');
        }
        
        if(String.isBlank(emailBody)){
            throw new WorkOrderEmailException('Please enter email body');
        }
        
        for(ContactWrapper cwt : contactWrapperList){
            if(cwt.selectObj){
                sendTo.add(cwt.contactObj.Email);
            }
        }
        
        //STSK0013367:  Add Contact Role Association list to sendTo
        for(ContactRoleWrapper cra : craWrapperList){
            if(cra.selectObj){
                sendTo.add(cra.contactroleObj.Contact_Email__c);
            }
        }
        
        if(String.isNotBlank(addMailRecepients))
        {
            //STSK0012812 -Start
            List<String> tempStr=addMailRecepients.split('[;,]'); 
            //STSK0012812 -End
            for(String str:tempStr)
            {
                sendTo.add(str);
            }
        }
        if(sendTo.isEmpty()){
            throw new WorkOrderEmailException('Please select at least one Contact, or enter at least one Additional Email');
        }
        
        // Start ---STSK0013183
        else
        {
            appendSignature();
            
        }// End ---STSK0013183
    }
    /***
* @Desc: Coded to append User Signature in the Email
@Task: STSK0013183
* @param : None
* @return :void
* 
**/ 
    
    @TestVisible  private void appendSignature()
    {
        
        User userInfoDB=[Select Title,Address From User where id =:UserInfo.getUserId() LIMIT 1];
        Document doc=[select id,name  from document where name ='Email Varian Logo' Limit 1];
        String varianLogoImagePath='/servlet/servlet.ImageServer?id='+doc.id+'&oid='+UserInfo.getOrganizationId();
        String hostURL;
        if(Test.isRunningTest()){
            hostURL='https://test.com/';
        }else{
            hostURL='https://'+ApexPages.currentPage().getHeaders().get('Host').replace('.visual.', '.content.');
        }
        String varianLogoURL=hostURL+varianLogoImagePath; 
        emailBody=emailBody+'<br>'+UserInfo.getName()+'<br>' ;
        if(String.isNotBlank( userInfoDB.Title))
        {
            emailBody=emailBody+userInfoDB.Title+'<br>';
        }
        if(String.isNotBlank(userInfo.getOrganizationName()))
            
        {
            emailBody=emailBody+userInfo.getOrganizationName()+'<br>';
        }
        if(userInfoDB.Address!=null)
        {
            emailBody=emailBody+userInfoDB.Address.getCity()+', '+userInfoDB.Address.getState()+', '+userInfoDB.Address.getCountry()+'<br>';
        }
        emailBody=emailBody+UserInfo.getUserEmail()+'<br>';
        emailBody=emailBody+'<a href="https://www.varian.com">varian.com</a><br>';       
        emailBody=emailBody+'<img src="'+varianLogoURL+'" /><br>';       
        emailBody=emailBody+'This message may contain information that is confidential or otherwise protected from disclosure. If you are not the intended recipient, you are hereby notified that any use, disclosure, dissemination, distribution, or copying of this message, or any attachments, is strictly prohibited. If you have received this message in error, please advise the sender by reply e-mail, and delete the message and any attachments.';
        
    }
    
    /***
* @Desc: Hanlde logic to send  attachments in mail
* * @param : None
* @return :PageReference
**/
    
    public PageReference email(){
        try{
            sendTo = new List<String>();
            prameters = '';
            validate();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();           
            mail.setToAddresses(sendTo);
            //STSK0013526 Start
            if(emailCC == TRUE){
                mail.setCcAddresses(ccAddresses);
            }
            //STSK0013526 End
            mail.setSenderDisplayName(UserInfo.getUserName());
            mail.setSubject(emailSub );
            mail.setHtmlBody(emailBody);           
            List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
            list<Attachment> taskAttachment=new List<Attachment>();
            for(AttachmentWrapper awt : attachmentWrapperList){
                if(awt.selectObj){ 
                    list<Attachment> aaObjList = [select Name, body from Attachment where id =:awt.attachmentObj.Id limit 1];
                    if(!aaObjList.isEmpty()){
                        Messaging.Emailfileattachment eAttachment = new Messaging.Emailfileattachment();
                        eAttachment.setFileName( aaObjList[0].name);
                        eAttachment.setBody(aaObjList[0].body);
                        efaList.add(eAttachment);
                        taskAttachment.add(aaObjList[0]);
                    }
                }
            }
            mail.setFileAttachments(efaList );
            mails.add(mail);
            Messaging.sendEmail(mails);
            //STSK0012673 Changes -Start
            Task activityHistory = new Task( WhatId= workOrderId , Subject = mail.subject, Description ='Mail Body::  '+emailBody +'\n Mail Recipients ::  Attachment[s] mailed to  '+sendTo, ActivityDate = System.Today(), Status='Completed');
            //STSK0012673 Changes - End
            Database.insert(activityHistory);
            for(Attachment atch :taskAttachment)
            {
                Attachment atchForTask=new Attachment(Name=atch.Name, body=atch.Body);
                atchForTask.ParentId=activityHistory.id;
                Database.insert(atchForTask);
                
            }
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Info,'Mail Sent!');
            ApexPages.addMessage(myMsg1);
            return null;
        }catch(Exception e){
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
        return new PageReference('/'+workOrderId);
    }
    
    
    /***
* @Desc: Hanlde logic to send combined   attachments in mail
* * @param : None
* @return :PageReference
**/
    public PageReference emailWithMerge(){
        try{
            sendTo = new List<String>();
            prameters = '';
            validate();
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(sendTo);
            //STSK0013526 Start
            if(emailCC == TRUE){
                mail.setCcAddresses(ccAddresses);
            }
            //STSK0013526 End
            mail.setSenderDisplayName(UserInfo.getUserName());
            mail.setSubject(emailSub);
            mail.setHtmlBody(emailBody);
            List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
            HttpRequest mergeRequest = new HttpRequest();
            String endpointURL = Label.Heroku_App_URL;
            prameters = prameters.substring(0,prameters.length()-1);
            prameters = prameters+'.pdf';
            prameters = EncodingUtil.urlEncode(prameters, 'UTF-8');
            endpointURL = endpointURL+prameters;
            mergeRequest.setEndpoint(endpointURL);
            mergeRequest.setMethod('GET');
            http h = new Http();
            HttpResponse mergeResponse;
            if(!Test.isRunningTest()) {
                mergeResponse = h.send(mergeRequest);
            }
            else {
                mergeResponse = new HttpResponse();
                mergeResponse.setStatusCode(200); 
                mergeResponse.setBody('{"result":"test"}');
            }
            
            Blob data = Blob.valueof(mergeResponse.getBody());
            Messaging.Emailfileattachment eAttachment = new Messaging.Emailfileattachment();
            String reportName='Case '+ woObj.Case_Number__c+', '+woObj.Name+ ' Attachments'+'.pdf';
            eAttachment.setFileName( reportName); 
            eAttachment.setBody(mergeResponse.getBodyAsBlob());
            efaList.add(eAttachment);
            mail.setFileAttachments(efaList );
            mails.add(mail);
            Messaging.sendEmail(mails);
            //STSK0012673 Changes -Start
            Task activityHistory = new Task( WhatId= workOrderId , Subject = mail.subject, Description ='Mail Body::  '+emailBody +'\n Mail Recipients ::Attachment mailed to  '+sendTo, ActivityDate = System.Today(), Status='Completed');
            //STSK0012673 Changes -End
            Database.insert(activityHistory);
            Attachment atchMerged=new Attachment(Name=reportName, body=mergeResponse.getBodyAsBlob());
            atchMerged.ParentId=activityHistory.id;
            Database.insert(atchMerged);
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Info,'Mail Sent!');
            ApexPages.addMessage(myMsg1);
            return null;
        }catch(Exception e){
            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg1);
            return null;
        }
    }
    
    
    
    /***
* @Desc: Hanlde logic for Back Button
* * @param : None
* @return :PageReference
**/ 
    public PageReference back(){
        
        return new PageReference('/'+workOrderId);
    }
    
    /************************************* Wrapper Classes  *****************************************/  
    public class AttachmentWrapper{
        public Attachment attachmentObj {get;set;} 
        public boolean selectObj {get;set;}
    }
    
    public class ContactWrapper{
        public Contact contactObj {get;set;} 
        public boolean selectObj {get;set;}
    }
    
    //STSK0013367:  Contact Role Association Wrapper
    public class ContactRoleWrapper{
        public Contact_Role_Association__c contactroleObj {get;set;} 
        public boolean selectObj {get;set;}
    }
    
    public class WorkOrderEmailException extends Exception{} 
    }
@isTest
public class WorkOrderAttachEmailControllerTest {
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent1 = new SVMXC__Installed_Product__c();
    public static Product2 objProd;
    public static List<TripReportAttachmentController.OtherUserWrapper> othListE=new List<TripReportAttachmentController.OtherUserWrapper>();
    public static List<TripReportAttachmentController.OtherUserWrapper> othListNE=new List<TripReportAttachmentController.OtherUserWrapper>();
    public static List<TripReportAttachmentController.OtherUserWrapper> othListNET=new List<TripReportAttachmentController.OtherUserWrapper>();
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListE=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListNE=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<TripReportAttachmentController.ManagerUserWrapper> mngListNET=new List<TripReportAttachmentController.ManagerUserWrapper>();
    public static List<Attachment> listAttach=new List<Attachment>();
    
    
    
    
    
    Static {
        
        
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/image';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'Email Varian Logo';
        document.FolderId = UserInfo.getUserId();
        insert document;
        
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        
        //Using Existing Profile
        Profile profile = [Select id from Profile where name = 'VMS Unity 1C - Service User'];
        
        
        
        
        //newAcc = UnityDataTestClass.AccountData(true);
        //
        Account newAcc = new Account();
        newAcc.Name = 'My Varian Test Account';
        newAcc.BillingCountry='USA';
        newAcc.Country__c= 'USA';
        newAcc.BillingCity= 'Los Angeles';
        newAcc.BillingStreet ='3333 W 2nd St';
        newAcc.BillingState ='CA';
        newAcc.BillingPostalCode = '90020';
        newAcc.Agreement_Type__c = 'Master Pricing';
        newAcc.OMNI_Address1__c ='3333 W 2nd St';
        newAcc.OMNI_City__c ='Los Angeles';
        newAcc.OMNI_State__c ='CA';
        newAcc.OMNI_Country__c = 'USA';  
        newAcc.OMNI_Postal_Code__c = '90020';
        
        Account newAcc2 = new Account();
        newAcc2.Name = 'My Varian Test Account 2';
        newAcc2.BillingCountry='USA';
        newAcc2.Country__c= 'USA';
        newAcc2.BillingCity= 'Los Angeles';
        newAcc2.BillingStreet ='3333 W 2nd St';
        newAcc2.BillingState ='CA';
        newAcc2.BillingPostalCode = '90020';
        newAcc2.Agreement_Type__c = 'Master Pricing';
        newAcc2.OMNI_Address1__c ='3333 W 2nd St';
        newAcc2.OMNI_City__c ='Los Angeles';
        newAcc2.OMNI_State__c ='CA';
        newAcc2.OMNI_Country__c = 'USA';  
        newAcc2.OMNI_Postal_Code__c = '90020';
        
        insert new list<Account>{newAcc,newAcc2};
        
        Contact conObj = new Contact(FirstName='test',Lastname='Test',
                                  Email = 'bac@gfmam.com',Functional_Role__c='Account Payable Capital',
                                 AccountId = newAcc.Id,
                                    Active_Contact__c = newAcc.Id);
        insert conObj;
        
        Contact_Role_Association__c cra = new Contact_Role_Association__c();
        cra.Account__c = newAcc.id;
        cra.Contact__c = conObj.id;
        insert cra; 
        
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
       
        
        CountryDateFormat__c cdf = new CountryDateFormat__c();
        cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
        cdf.Name ='Default';
        insert cdf;
        
        WO = new SVMXC__Service_Order__c();
        WO.SVMXC__Case__c = newCase.id;
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Open';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.SVMXC__Group_Member__c=technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Subject__c= 'bbb';
        insert WO;
        
        //Insert Attachment
        //
        Attachment objAtt = new Attachment();
        objAtt.Name = 'Test';
        objAtt.body = Blob.valueof('Unit Test Attachment1');
        objAtt.ParentId = WO.Id;
        listAttach.add(objAtt);
        
        //Creating attachments
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment1'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=WO.id;
        listAttach.add(attach);
        
        attach=new Attachment(); 
        attach.Name='Unit Test Attachment2'; 
        Blob bodyBlob1=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob1; 
        attach.parentId=WO.id;
        listAttach.add(attach);
        insert listAttach;
    }
    
    @isTest public static  void withoutPageReference(){
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        System.Test.StartTest();
        try{
            WorkOrderAttachEmailController testController=new WorkOrderAttachEmailController();
        }
        catch(Exception e)
            
        {
            System.assertEquals('Please pass work order id as parameter', e.getMessage());
        }
        
        System.Test.StopTest();
    }
    
    @isTest public static  void testorkOrderAttachEmailControllerInvalidWOFail() {  
       
        Test.starttest();
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('woId', '111111111111111111');
        try{
            WorkOrderAttachEmailController ctrl = new WorkOrderAttachEmailController(); 
        }
        catch(Exception e)
        {
            System.assertEquals(true, e.getMessage().Contains('Work order not found for id :'+'111111111111111111'));
        }
        
    }     
    
    @isTest public static void attachmentNotFound()    
    {
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.TestTripAttachment')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        testClass.workOrderId='invalid23456789012';
        try{
            
            List<WorkOrderAttachEmailController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        }
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('No Attachments are Currently Available on Work Order.  Please add prior to use of Email functionality'))  ;   
        }
        
        System.Test.StopTest();
        
        
    }  
    
    @isTest public static void attachmentNotSelected()
    {
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        
        try
        {testClass.validate();}
        catch(Exception e)
        {
            System.assertEquals(true,e.getMessage().Contains('Please select attachments'));
        }
        
        
        
        System.Test.StopTest();   
    }
    
    @isTest public static void attachmentSelected()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        List<WorkOrderAttachEmailController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        for(integer i=0; i<tempList.size(); i++)
        {
            tempList[i].selectObj=true;
        }
        try{
            testClass.emailBody='';
            testClass.validate();
        }
        catch(Exception e)
        {
            //System.assertEquals(true,String.isBlank(testClass.emailBody));
            //System.assertEquals(true,e.getMessage().Contains('Please enter email body'))  ;       
        }
        System.Test.StopTest();    
    }
    
    @isTest public static void emailBobySelected()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        List<WorkOrderAttachEmailController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        for(integer i=0; i<tempList.size(); i++)
        {
            tempList[i].selectObj=true;
        }
        try{
            testClass.emailBody='Email Body Entered';
            testClass.addMailRecepients='';
            testClass.validate();
        }
        catch(Exception e) 
        {
            //System.assertEquals(true,String.isBlank(testClass.addMailRecepients)); /
            //System.assertEquals(true,e.getMessage().Contains('Please select at least one Contact or enter at least one Additional Email'))  ;       
        }
        System.Test.StopTest();
        
        
    }   
    @isTest public static void contactSelected() 
    {
        
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        List<WorkOrderAttachEmailController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        for(WorkOrderAttachEmailController.AttachmentWrapper atcWrap: testClass.getAttachmentWrapperList())
        {
            atcWrap.selectObj=true;
        }
        //Contact Selected but email id is not added to sendTo List
        for(WorkOrderAttachEmailController.ContactWrapper conWrapper: testClass.getContactWrapperList()) {
            conWrapper.selectObj = true; 
            
        }

        for(WorkOrderAttachEmailController.ContactRoleWrapper craWrapper: testClass.getcraWrapperList()) {
            craWrapper.selectObj = true; 
            
        }
        
        try{
            testClass.emailBody='Email Body Entered';
            testClass.addMailRecepients='';
            testClass.validate();
        }
        catch(Exception e)
        {
            //System.assertEquals(true,String.isBlank(testClass.addMailRecepients)); 
            //System.assertEquals(true,e.getMessage().Contains('Please select at least one Contact or enter at least one Additional Email'))  ;       
        }
        System.Test.StopTest();
        
        
    }
    
    
    @isTest public static void sendToSelected() 
    {
        
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        List<WorkOrderAttachEmailController.AttachmentWrapper> tempList=testClass.getAttachmentWrapperList();
        for(WorkOrderAttachEmailController.AttachmentWrapper atcWrap: testClass.getAttachmentWrapperList())
        {
            atcWrap.selectObj=true;
        }
        //Contact Selected but email id is not added to sendTo List
        for(WorkOrderAttachEmailController.ContactWrapper conWrapper: testClass.getContactWrapperList()) {
            conWrapper.selectObj = true;
            testClass.sendTo.add(conWrapper.contactObj.Email);
            
        }
        
        for(WorkOrderAttachEmailController.ContactRoleWrapper craWrapper: testClass.getcraWrapperList()) {
            craWrapper.selectObj = true; 
            
        }
        
        if(testClass.sendTo.isEmpty())
        {
            testClass.sendTo.add('Testing@test.com');
        }
        
        try{
            testClass.emailBody='Email Body Entered';
            testClass.addMailRecepients='Testing@test.com';
            testClass.email();
        }
        catch(Exception e)
        {
            //System.assertEquals(true,e.getMessage().Contains('Please choose at least one Contact from List or enter at least one Additional Email'))  ;       
        }
       // for(ApexPages.Message msg :  ApexPages.getMessages()) {
         //   System.assertEquals(true,msg.getSummary().contains('Mail Sent'));       }
        
        
        System.Test.StopTest();
        
        
    }
    
    
    @isTest public static  void testorkOrderAttachEmailControllerPass() {  
        
        Test.starttest();
        
        //No Work Order Id Exception
        WorkOrderAttachEmailController ctrl = new WorkOrderAttachEmailController(); 
        
        //No Work Order Found Exception
        PageReference pageRef = Page.WorkOrderAttachEmail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('woId', '11');
        ctrl = new WorkOrderAttachEmailController(); 
        
        //No Attachement Exception        
        ApexPages.currentPage().getParameters().put('woId', WO.Id);
        ctrl = new WorkOrderAttachEmailController(); 
        ctrl.getAttachmentWrapperList();
        
        ctrl = new WorkOrderAttachEmailController(); 
        
        //Sending email without attachments to throw exception
        ctrl.email(); 
        
        //Getting attachments and marking selected
        for(WorkOrderAttachEmailController.AttachmentWrapper attachWrapper: ctrl.getAttachmentWrapperList()) {
            attachWrapper.selectObj = true; 
        }
        
        //Sending email without contacts to throw exception
        ctrl.email(); 
        
        //Getting Contacts and marking selected
        for(WorkOrderAttachEmailController.ContactWrapper conWrapper: ctrl.getContactWrapperList()) {
            conWrapper.selectObj = true; 
        }
        
        for(WorkOrderAttachEmailController.ContactRoleWrapper craWrapper: ctrl.getcraWrapperList()) {
            craWrapper.selectObj = true; 
            
        }
        
        //Sending email without body to throw exception
        ctrl.email(); 
        
        //Sendinge mail with subject
        ctrl.emailBody = 'Test Email';
        ctrl.addMailRecepients = 'test1@test.com';
        ctrl.email();   
        
        //Sending email with merge
        ctrl.emailWithMerge();
        ctrl.back();
        
        Test.stoptest();
    }
    
    
    
    
    @isTest public static void backWithPGRef()
    {
        System.Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderAttachEmail')); 
        System.currentPagereference().getParameters().put('woId',WO.id);
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        testClass.back();
        System.Test.StopTest();   
    }
    @isTest public static void backWithoutPGRef()
    {
        System.Test.StartTest();
        WorkOrderAttachEmailController testClass=new WorkOrderAttachEmailController();
        testClass.back();
        System.Test.StopTest();   
    }
    
    
    
    
}
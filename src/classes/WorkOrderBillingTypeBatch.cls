//cdelfattore@forefront 10/23/2015
//US5258 TA7714
/*
    In a Batch job run daily, when the Scheduled Date Time is within 1 days and
    no Usage/Consumption lines exists under that Product Serviced based on the
    Creation Date of the Case associated and the Line Status is not Cancelled or Closed,
    set the Billing Type to null to force a re-evaluation.
    US5258 TA7714
  @Last Modified By    :   Amitkumar Katre
  @Last Modified On    :   10 OCT 2017
  @Last Modified Reason:   STSK0013111 :  Coding Task - WorkOrderBillingTypeBatch
        
*/
//last updated by Amitkuamar : STRY0021997
global class WorkOrderBillingTypeBatch implements Database.Batchable<sObject> {
        
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String fieldServiceRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
        if(Test.isRunningTest())
            return Database.getQueryLocator([SELECT Id, (SELECT Id FROM SVMXC__Work_Details__r 
                                         WHERE RecordType.Name = 'Usage/Consumption' AND SVMXC__Line_Status__c = 'Open' AND SVMXC__Service_order__r.SVMXC__Order_Status__c = 'Assigned' ) 
                                         FROM SVMXC__Service_Order_Line__c 
                                         WHERE  SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c > :Datetime.now()
                                         AND SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c < :Datetime.now().addDays(1)
                                         AND RecordType.Name = 'Products Serviced' LIMIT 200]);
                                         
         return Database.getQueryLocator([SELECT Id, (SELECT Id FROM SVMXC__Work_Details__r 
                                         WHERE RecordType.Name = 'Usage/Consumption' AND SVMXC__Line_Status__c = 'Open' AND SVMXC__Service_order__r.SVMXC__Order_Status__c = 'Assigned' ) 
                                         FROM SVMXC__Service_Order_Line__c 
                                         WHERE  SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c > :Datetime.now()
                                         AND SVMXC__Service_Order__r.SVMXC__Scheduled_Date_Time__c < :Datetime.now().addDays(1)
                                         AND RecordType.Name = 'Products Serviced'
                                         AND SVMXC__Service_Order__r.RecordTypeId =:fieldServiceRecordTypeId]);
    } 

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<SVMXC__Service_Order_Line__c> wdlListUpdate = new List<SVMXC__Service_Order_Line__c>();
        for(SVMXC__Service_Order_Line__c wdObj : (List<SVMXC__Service_Order_Line__c>) scope){
            wdObj.Billing_Type__c =  null;
            wdlListUpdate.add(wdObj);
            for(SVMXC__Service_Order_Line__c wdObjInnner : wdObj.SVMXC__Work_Details__r){
                wdObjInnner.Billing_Type__c =  null;
                wdlListUpdate.add(wdObjInnner);
            }
        }
        if(!wdlListUpdate.isEmpty())
        update wdlListUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {
        // Added by Puneet, Will run only when test class is running, as see all data is true while validating/deploying to production
        // it is gib=ving an error "System.UnexpectedException: No more than one executeBatch can be called from within a test method."
        AsyncApexJob Job;
        if(Test.isRunningTest()){
            Job = [ SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                               FROM AsyncApexJob WHERE Id =:BC.getJobId()];
            system.debug(' ===  Job Status === ' + Job.Status);
            System.abortJob(Job.Id);
            system.debug(' ===  Job Status === ' + Job.Status);
        }
    }
    
}
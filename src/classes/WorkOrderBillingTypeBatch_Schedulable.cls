global class WorkOrderBillingTypeBatch_Schedulable implements Schedulable {
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new WorkOrderBillingTypeBatch());
	}

	global static void scheduleOncePerDay(){
		String testRun = '';
		if(Test.isRunningTest()){
			testRun = 'TEST ';
		}

		//will use the timezone of the user that runs the schedualed job
		//seconds minutes hours day_of_month month
		System.schedule(testRun + 'WorkOrderBillingTypeBatch Process', '0 0 2 * * ?', new WorkOrderBillingTypeBatch_Schedulable());
	}
}
public with sharing class WorkOrderDateFieldsHandler {
    
    map<Id, Account> accountsMap = new map<Id, Account> ();
    map<String, CountryDateFormat__c> Dateformatsetting;
    String defaultDateFormat ;
    
    public WorkOrderDateFieldsHandler (){
        Dateformatsetting = CountryDateFormat__c.getAll();
        accountsMap = SR_Class_WorkOrder.mapAccount_WorkOrder;
        system.debug('accountsMap======='+accountsMap);
        system.debug(' ==== ' + Dateformatsetting);
        //system.debug(' ==== ' + Dateformatsetting.get('Default').Date_Format__c );
        defaultDateFormat = (Dateformatsetting.ContainsKey('Default'))?Dateformatsetting.get('Default').Date_Format__c:'dd-MM-yyyy kk:mm';
    }
    
    public void exceute(list<SVMXC__Service_Order__c> lstOfWo, 
                        map<Id, SVMXC__Service_Order__c> oldMap, 
                        map<Id, SVMXC__Service_Order__c> newMap){
        //Set date values in text based on customer time zone.
        for(SVMXC__Service_Order__c wo : lstOfWo){
           setMachineReleaseDate(wo);
           system.debug('Test=========================setCustomerMalfunctionDate');
            
           if(wo.Acceptance_Date1__c != null && (oldmap == null 
              || oldmap.get(wo.id).Acceptance_Date1__c != wo.Acceptance_Date1__c)){
              setCustomerMalfunctionDate(wo);
           }
            
           if(wo.Date_of_Intervention1__c != null && (oldmap == null 
              || oldmap.get(wo.id).Date_of_Intervention1__c != wo.Date_of_Intervention1__c)){
              setCustomerRequestStartDate(wo);
           } 
           
          if(wo.Next_Training_Trip_Reminder1__c != null && (oldmap == null 
              || oldmap.get(wo.id).Next_Training_Trip_Reminder1__c != wo.Next_Training_Trip_Reminder1__c)){
              setCustomerRequestEndDate(wo);
           } 
            
           if(wo.Machine_release_time__c != null && (oldmap == null 
              || oldmap.get(wo.id).Machine_release_time__c != wo.Machine_release_time__c
              || wo.ERP_Malfunction_End_Date_time__c == null)){
              setCustomerErpMalFtnDates(wo);
           } 
           
           setTimeInOutDates(wo);
           setActualRestorationDate(wo);
        }
        
    }
    
    @testvisible
    private void setMachineReleaseDate(SVMXC__Service_Order__c wo){
        if(wo.Machine_release_time__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
               //rakesh
               String str_mrlt = wo.Machine_release_time__c.format(getDateFormat(wo));
               wo.Machine_release_Local_time__c = str_mrlt;
               //wo.Machine_release_Local_time__c = wo.Machine_release_time__c.format('dd-MMM-yyyy hh:mm a', getCustomerTimezone(ct)); //DE7967 
           }
                           
        }
    }
    
    @testvisible
    private void setCustomerMalfunctionDate(SVMXC__Service_Order__c wo){
        
        if(wo.Acceptance_Date1__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           system.debug('Acceptance_Date1__c=========='+wo.Acceptance_Date1__c);
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
               String erpstart = wo.Acceptance_Date1__c.format('YYYY-MM-dd HH:mm',getCustomerTimezone(ct)); 
               //wo.ERP_Malfunction_Start_Date_Time__c =  DateTime.valueofGMT(erpstart+':00'); 
               wo.Customer_Malfunction_Start_Date_Time__c = wo.Acceptance_Date1__c.format(getDateFormat(wo), getCustomerTimezone(ct)); 
               //calculate erp time gmt 0
               String erp_s_str = wo.Acceptance_Date1__c.format('dd-MMM-yyyy hh:mm a', getCustomerTimezone(ct));
               wo.ERP_Malfunction_Start_Date_Time__c = constructDateTimeGMT(erp_s_str); 
           } 
        } 
        if(wo.Customer_Malfunction_Start_Date_Time__c <> null && (wo.Customer_Malfunction_Start_Date_Time__c).contains(' 24:'))
             wo.Customer_Malfunction_Start_Date_Time__c = (wo.Customer_Malfunction_Start_Date_Time__c).replace(' 24:',' 00:');
    }
    
    
    @testvisible
    private void setCustomerRequestStartDate(SVMXC__Service_Order__c wo){
        
        if(wo.Date_of_Intervention1__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
               wo.Customer_Requested_Start_Date_Time__c = wo.Date_of_Intervention1__c.format(getDateFormat(wo), getCustomerTimezone(ct)); 
           }
        }
        if(wo.Customer_Requested_Start_Date_Time__c <> null && (wo.Customer_Requested_Start_Date_Time__c).contains(' 24:'))
            wo.Customer_Requested_Start_Date_Time__c = (wo.Customer_Requested_Start_Date_Time__c).replace(' 24:',' 00:');
    }
    
    @testvisible
    private void setCustomerRequestEndDate(SVMXC__Service_Order__c wo){
        
        if(wo.Next_Training_Trip_Reminder1__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
              wo.Customer_Requested_End_Date_Time__c = wo.Next_Training_Trip_Reminder1__c.format(getDateFormat(wo), getCustomerTimezone(ct)); 
           }
        }
        if(wo.Customer_Requested_End_Date_Time__c <> null && (wo.Customer_Requested_End_Date_Time__c).contains(' 24:'))
              wo.Customer_Requested_End_Date_Time__c = (wo.Customer_Requested_End_Date_Time__c).replace(' 24:',' 00:');
    }
    
    @testvisible
    private void setTimeInOutDates(SVMXC__Service_Order__c wo){
        //In date time
        if(wo.Time_in__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
              wo.local_time_in__c = wo.Time_in__c.format(getDateFormat(wo), getCustomerTimezone(ct)); //DE7967 
           }
        }
        //Out date time
        if(wo.time_out__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
              wo.local_time_out__c = wo.time_out__c.format(getDateFormat(wo), getCustomerTimezone(ct)); //DE7967 
           }
        }
        
        if(wo.local_time_in__c <> null && (wo.local_time_in__c).contains(' 24:'))
            wo.local_time_in__c = (wo.local_time_in__c).replace(' 24:',' 00:');
        if(wo.local_time_out__c <> null && (wo.local_time_out__c).contains(' 24:'))
            wo.local_time_out__c = (wo.local_time_out__c).replace(' 24:',' 00:');
    }
    
    @testvisible
    private void setActualRestorationDate(SVMXC__Service_Order__c wo){
        if(wo.Machine_release_time__c != null ){
            wo.SVMXC__Actual_Restoration__c = wo.Machine_release_time__c;
        }
    }
    
    @testvisible
    private void setCustomerErpMalFtnDates(SVMXC__Service_Order__c wo){
        if(wo.Machine_release_time__c != null && accountsMap.containsKey(wo.SVMXC__Company__c)){
           String ct = accountsMap.get(wo.SVMXC__Company__c).Timezone__r.Salesforce_timezone__c;
           if(ct != null && ct != ''){
              //String erpend = wo.Machine_release_time__c.format('YYYY-MM-dd HH:mm',getCustomerTimezone(ct)); 
              wo.Customer_Malfunction_End_Date_time_Str__c = wo.Machine_release_time__c.format(getDateFormat(wo), getCustomerTimezone(ct)); 
              //wo.Customer_Malfunction_End_Date_time_Str__c = wo.Machine_release_time__c.format(getDateFormat(wo), getCustomerTimezone(ct)); 
              //wo.ERP_Malfunction_End_Date_time__c = DateTime.valueofGMT(erpend+':00'); 
        String erp_e_str = wo.Machine_release_time__c.format('dd-MMM-yyyy hh:mm a', getCustomerTimezone(ct));
              wo.ERP_Malfunction_End_Date_time__c = constructDateTimeGMT(erp_e_str);
              
           }
        }
        if(wo.Customer_Malfunction_End_Date_time_Str__c <> null && (wo.Customer_Malfunction_End_Date_time_Str__c).contains(' 24:'))
            wo.Customer_Malfunction_End_Date_time_Str__c = (wo.Customer_Malfunction_End_Date_time_Str__c).replace(' 24:',' 00:');
    }
    
    @testvisible
    private String getCustomerTimezone (String t){
        return t.substring(t.indexof('(')+ 1,t.indexof(')'));
        return null;
    }
    
    
    @testvisible
    private String getDateFormat(SVMXC__Service_Order__c wo){
        String dtformat;
        if(accountsMap.containsKey(wo.SVMXC__Company__c) && 
          accountsMap.get(wo.SVMXC__Company__c).country__c <> null ){
            if(Dateformatsetting.containsKey(accountsMap.get(wo.SVMXC__Company__c).country__c)){
                dtformat = Dateformatsetting.get(accountsMap.get(wo.SVMXC__Company__c).country__c).Date_Format__c;
            }
        }
        
        if(dtformat == null){
            return defaultDateFormat;
        }else{
            system.debug('dtformat========'+dtformat);
            return dtformat;
        }
         
    }
    
     /**
            Create GMT instance from datetime string in "dd-MMM-yyyy hh:mm AM" format, when set on field, should show the right date/time when user timezone is GMT, user has to change time zone to GMT to see right values
        */
        public DateTime constructDateTimeGMT(String strDateTime)
        {
            //system.debug(strDateTime);
            List<String> dateTimeSplitList =  strDateTime.split(' ');
            List<string> dateValueSplit = new List<string>();
            if(dateTimeSplitList[0].contains('/'))
                dateValueSplit = dateTimeSplitList[0].split('/') ;
            if(dateTimeSplitList[0].contains('-'))
                dateValueSplit = dateTimeSplitList[0].split('-') ;
            List<String> timeSplit = dateTimeSplitList[1].split(':');
            String strAMPM = dateTimeSplitList[2];
            DateTime constructedDateTime;
            if(strAMPM == 'AM' && Integer.valueof(timeSplit[0]) == 12) // need to set HOUR as ZERO for 12 AM
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),00, Integer.valueof(timeSplit[1]), 00);
            }
            else if((strAMPM == 'AM' && Integer.valueof(timeSplit[0]) != 12) || (strAMPM == 'PM' && Integer.valueof(timeSplit[0]) == 12)) // whatever HOUR comes
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0]), Integer.valueof(timeSplit[1]), 00);
            }
            else if(strAMPM == 'PM' && Integer.valueof(timeSplit[0]) != 12) // add 12 to HOUR 
            {
                constructedDateTime = dateTime.newInstanceGmt(Integer.valueof(dateValueSplit[2]),Integer.valueof(convertMonthNameToNumber(dateValueSplit[1])),Integer.valueof(dateValueSplit[0]),Integer.valueof(timeSplit[0])+12, Integer.valueof(timeSplit[1]), 00);
            }
            return constructedDateTime;
        }
        


        @TestVisible
        private String convertMonthNameToNumber(String strMonthName)
        {
      string rt = '01';
            if(strMonthName == 'Jan')                rt = '01';
            if(strMonthName == 'Feb')                rt = '02';
            if(strMonthName == 'Mar')                rt = '03';
            if(strMonthName == 'Apr')
                rt = '04';
            if(strMonthName == 'May')                rt = '05';
            if(strMonthName == 'Jun')                rt = '06';
            if(strMonthName == 'Jul')                rt = '07';
            if(strMonthName == 'Aug')                rt = '08';
            if(strMonthName == 'Sep')                rt = '09';
            if(strMonthName == 'Oct')                rt = '10';
            if(strMonthName == 'Nov')                rt = '11';
            if(strMonthName == 'Dec')                rt = '12';
      return rt;
        }
}
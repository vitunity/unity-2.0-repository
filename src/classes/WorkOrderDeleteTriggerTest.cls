/*
 *  Author : Amitkumar Kate
 *  Description : Time Entry delete trigger test
 *  created date : 18/07/2016 (After GO Live)
 */
@isTest(seealldata=true)
public class WorkOrderDeleteTriggerTest {
    /* test data members */
    static Profile vmsserviceuserprofile;
    static User serviceuser;
    static User serviceuser1;
    static SVMXC__Service_Group_Members__c technician;
    static Technician_Assignment__c techassgn;
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    static SVMXC__Site__c location;
    static Account acc;
    static SVMXC__Service_Order_Line__c wd;
    static SVMXC__Service_Group__c servicegrp;
    static Organizational_Activities__c orgactivities;
    static Timecard_Profile__c timecardprof ;
    static Timecard_Profile__c indirecttimecardprof ;
    static ERP_Project__c  erpproject; 
    static ERP_WBS__c erpwbs;
    static Country__c con;
    static ERP_NWA__c erpnwa;
    static SVMXC_Time_Entry__c timeEntry;
    static SVMXC_Timesheet__c timeSheet;
    static Timecard_Profile__c timecardProfile;
    static Organizational_Activities__c orgActivites;
    static Product2 product;
    static SVMXC__Installed_Product__c ip;
    static ERP_Org__c eorg;
    static SVMXC__Product_Replacement__c productReplacemnt; 
    
    /* Initialize test data in static block */
    static{
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
     
        technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician',User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,
        SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321');
        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert technician;
        
     
        acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States'); 
        insert acc;
  
        location = new SVMXC__Site__c(
        Sales_Org__c = 'testorg', SVMXC__Service_Engineer__c = userInfo.getUserId(),
        SVMXC__Location_Type__c = 'Field', Plant__c = 'dfgh', SVMXC__Account__c = acc.id);
        insert location;
        
        casealias = new Case(Subject = 'testsubject');
        casealias.Priority = 'High';
        insert casealias;
     
        wo = new SVMXC__Service_Order__c(SVMXC__Group_Member__c = technician.id,SVMXC__Site__c = location.id,SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id,
                                         ERP_Service_Order__c = '12345678');
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
        wo.ownerid= Userinfo.getUserId();
        insert wo;
        
        product = new Product2(Name='Connect Utilities');
        insert product;
        
        ip = new SVMXC__Installed_Product__c(Name = 'test',SVMXC__Product__c=product.id);
        insert ip;
        
    }
    
    public static testmethod void unittest1() {
      System.Test.startTest();
      wd = new SVMXC__Service_Order_Line__c(); 
      wd.SVMXC__Consumed_From_Location__c = location.id;
      wd.SVMXC__Line_Type__c = 'Labor';     
      wd.SVMXC__Group_Member__c = technician.id;
      wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
      wd.SVMXC__Work_Description__c = 'test';
      wd.SVMXC__Start_Date_and_Time__c = System.now();
      wd.SVMXC__End_Date_and_Time__c = System.now().addMinutes(30);
      wd.SVMXC__Service_Order__c = wo.id;
      wd.SVMXC__Serial_Number__c = ip.id;
      wd.Old_Part_Number__c = product.Id;
      insert wd; 
      delete wo;  
      System.Test.stopTest();
    }
}
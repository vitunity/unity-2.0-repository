/*
 * @Author - Amitkumar Katre
 * @Description - MFL SYNC issues controller
 */
public class WorkOrderMFLSyncHandler {

    private static string usageConsumptionWDRecType = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Usage/Consumption').getRecordTypeId();
    
    public static void submitHandler(SVMXC__Service_Order__c varWO,
                                                    map<Id,SVMXC__Service_Order__c> oldMapWorkorder,
                                                 Map<ID, Map<id,SVMXC__Service_Order_Line__c>> mapChildWorkDetail){
        
        // Start - added to resolve mfl syc issues while submit - Amitkumar INC4269559
        // if lines are not updating becasue of sync.
        if(varWO.SVMXC__Order_Status__c == 'Submitted' 
        && varWO.Submited_Line_Count__c != null 
        && varWO.Submitted_WD_Count__c == 0
        && (varWO.RecordTypeId == SR_Class_WorkOrder.fieldServcRecTypeID || varWO.RecordTypeId == SR_Class_WorkOrder.instlRecTypeID ) 
        ){
            if(mapChildWorkDetail.containsKey(varWO.ID) 
               && mapChildWorkDetail.get(varWO.ID) != null 
               && mapChildWorkDetail.get(varWO.ID).size() > 0  ){
                   for(SVMXC__Service_Order_Line__c objWD : mapChildWorkDetail.get(varWO.ID).values()){
                       if((objWD.SVMXC__Line_Status__c == 'Open')
                          && objWD.RecordTypeId == usageConsumptionWDRecType){
                            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                            if(SR_Class_WorkOrder.mapUpdateWorkDetail.containsKey(objWD.ID))
                            {
                                wd = SR_Class_WorkOrder.mapUpdateWorkDetail.get(objWD.ID);
                            }
                            else
                            {
                                wd = new SVMXC__Service_Order_Line__c(ID = objWD.ID);
                            }
                            objWD.SVMXC__Line_Status__c = 'Submitted';
                            wd.SVMXC__Line_Status__c = 'Submitted';
                            SR_Class_WorkOrder.mapUpdateWorkDetail.put(wd.ID, wd);
                        }
                   }
            }
        } 
        //if header is not set properly 
        if(varWO.SVMXC__Order_Status__c == 'Assigned' && oldMapWorkorder != null
         && oldMapWorkorder.get(varWO.Id).SVMXC__Order_Status__c == 'Assigned' 
         && (varWO.Submitted_WD_Count__c != null && varWO.Submitted_WD_Count__c > 0)
         && varWo.Submited_Line_Count__c == null  
         && (varWO.RecordTypeId == SR_Class_WorkOrder.fieldServcRecTypeID || varWO.RecordTypeId == SR_Class_WorkOrder.instlRecTypeID ) 
        ){
            if(mapChildWorkDetail != null
               && mapChildWorkDetail.containsKey(varWO.ID) 
               && mapChildWorkDetail.get(varWO.ID) != null 
               && mapChildWorkDetail.get(varWO.ID).size() > 0  ){
                   Integer sb_count = 0;
                   for(SVMXC__Service_Order_Line__c objWD : mapChildWorkDetail.get(varWO.ID).values()){
                       if(objWD.SVMXC__Line_Status__c == 'Submitted' 
                          && objWD.RecordTypeId == usageConsumptionWDRecType){
                              sb_count++; 
                          }
                   }
                   if(varWO.Submitted_WD_Count__c == sb_count){
                       SVMXC__Service_Order__c tempWO = new SVMXC__Service_Order__c(id = varWO.Id);
                       tempWO.SVMXC__Order_Status__c = 'Submitted'; 
                       tempWO.Submited_Line_Count__c = sb_count;
                       system.debug('setting header if conflicted by mfl '+tempWo);
                       SR_Class_WorkOrder.mapOfPWOToUpdate.put(tempWO.ID, tempWO);
                   }
               }
        }
    }
}
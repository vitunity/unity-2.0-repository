/*
 * Author Amitkumar Katre
 * DE8269: OJT Work Orders Get Stuck at Submit/Null
 */
public with sharing class WorkOrderOJTHandler {
    
    map<ID, Map<id,SVMXC__Service_Order_Line__c>> mapChildWorkDetail;
    string usageRecTypeID = SR_Class_WorkOrder.usageRecTypeID;
    string instlRecTypeID = SR_Class_WorkOrder.instlRecTypeID;
    string fieldServcRecTypeID = SR_Class_WorkOrder.fieldServcRecTypeID;
    
    public WorkOrderOJTHandler (){
        mapChildWorkDetail = SR_Class_WorkOrder.mapChildWorkDetail;
        system.debug('mapChildWorkDetail======='+mapChildWorkDetail);
    }
    
    public void exceute(list<SVMXC__Service_Order__c> lstOfWo, 
                        map<Id, SVMXC__Service_Order__c> oldMap, 
                        map<Id, SVMXC__Service_Order__c> newMap){
        if(mapChildWorkDetail == null) return;
        for(SVMXC__Service_Order__c wo : lstOfWo){
            if(mapChildWorkDetail.get(wo.Id) != null 
               &&(wo.RecordTypeId == fieldServcRecTypeID 
               || wo.RecordTypeId == instlRecTypeID )
               && wo.SVMXC__Order_Status__c == 'Submitted'
               && (oldMap == null 
               || oldMap.get(wo.Id).SVMXC__Order_Status__c != wo.SVMXC__Order_Status__c)
               && wo.Submited_Line_Count__c != null){
                Integer countUCWD = Integer.valueof(wo.Submited_Line_Count__c);
              //Submitted_WD_Count__c//Submited_Line_Count__c
              integer donotProcessUCWD = 0;
                for(SVMXC__Service_Order_Line__c objWD : mapChildWorkDetail.get(wo.ID).values()){
                    if(objWD.Interface_Status__c == 'Do not process' && objWD.SVMXC__Line_Status__c != 'Closed'
                       && obJWD.RecordTypeId == usageRecTypeID){
                       donotProcessUCWD = donotProcessUCWD + 1;
                    }
                }
                if(countUCWD == donotProcessUCWD){
                  wo.Interface_Status__c = 'Do Not Process';       
                } 
            }            
        }
    }
}
/**
 *  Controller created for Custom Validation Rules
 *  Test Class : WorkOrderPSValidatorTest
 */
public class WorkOrderPSValidator {
    
   /**
    *  Puneet Mishra, 21 September 2016
    *  DFCT0011771 : Validation Rule to stop PS WDLs being deleted if there's U?C WDLs associated to PS WDLs 
    *  Moved method to this Class, 17 Oct 2016
    */
    public static void psDeleteValidate(Map<ID, SVMXC__Service_Order_Line__c> mapOldWorkDetail) {
        if(mapOldWorkDetail.isEmpty()) // return if map is empty
            return;
        String productServicedWDLRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order_Line__c').get('UsageConsumption');
        String usageConsumptionWDLRecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order_Line__c').get('UsageConsumption');
        set<Id> productServicedIdSet = new set<Id>();
        map<Id, List<SVMXC__Service_Order_Line__c>> PS_UCMap = new map<Id, List<SVMXC__Service_Order_Line__c>>();
        for(SVMXC__Service_Order_Line__c oLine : mapOldWorkDetail.values()) {
            system.debug(' ==== oline.Work_Order_RecordType__c ==== ' + oline.Work_Order_RecordType__c);
            system.debug(' ==== oline.RecordTypeId ==== ' + oline.RecordTypeId);
            // logic should work only for product serviced work detail AND work order of type installation
            if(oline.Work_Order_RecordType__c == 'Installation' && oline.RecordTypeId == productServicedWDLrecordTypeId) {
                productServicedIdSet.add(oLine.Id);
            }
        }
        
        if(!productServicedIdSet.isEmpty()) {
            for(SVMXC__Service_Order_Line__c oLine : [  SELECT Id, RecordTypeId, SVMXC__Work_Detail__c, SVMXC__Work_Detail__r.Name, Name  
                                                            FROM SVMXC__Service_Order_line__c 
                                                            WHERE RecordTypeId =: usageConsumptionWDLRecordTypeId AND 
                                                                    SVMXC__Work_detail__c IN: productServicedIdSet]) {
                                                                        
                system.debug(' === oLine === ' + oLine);
                if(PS_UCMap.containsKey(oLine.SVMXC__Work_Detail__c)) {
                    PS_UCMap.get(oLine.SVMXC__Work_Detail__c).add(oLine);
                } else {
                    PS_UCMap.put(oLine.SVMXC__Work_Detail__c, new List<SVMXC__Service_Order_Line__c>{oLine});
                }
                system.debug(' === PS_UCMap === ' + PS_UCMap);                                                        
            }
        }
        
        if(!PS_UCMap.isEmpty()) {
            for(SVMXC__Service_Order_Line__c oLine : mapOldWorkDetail.values()) {
                system.debug(' ==== oLine ==== ' + oLine);
                system.debug(' ==== PS_UCMap.containsKey(oLine.SVMXC__Work_Detail__c) ==== ' + PS_UCMap.containsKey(oLine.SVMXC__Work_Detail__c));
                if(PS_UCMap.containsKey(oLine.Id) && PS_UCMap.get(oLine.Id).size() > 0) {
                    oLine.addError('<font color="red"><b>You cannot delete Product Serviced as the record ' + oLine.Name + 
                                                ' as Usage Consumptions associated with this Work Detail </b></font>', FALSE);
                }
            }
        }
    }
    
    /**
     *  Puneet Mishra, 21 September 2016
     *  DFCT0012091 : Component must have Location = Work Order.Site
     */
    public static void componentLocValidator(Map<ID, SVMXC__Service_Order__c> woNewMap, Map<ID, SVMXC__Service_Order__c> woOldMap) {
        system.debug(' == ' + woNewMap);
        if(woNewMap == null)
            return;
        if(woNewMap.isEmpty())
            return;
        Id fieldServcRecTypeID = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
        Id helpDeskRecTypeID = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Helpdesk');
        Map<Id, Id> componentWOIds = new Map<Id, Id>();
        List<SVMXC__Installed_Product__c> ipProd = new List<SVMXC__Installed_Product__c>();
        for(SVMXC__Service_Order__c wo : woNewMap.values()) {
            if((wo.RecordTypeId == fieldServcRecTypeID || wo.RecordTypeId == helpDeskRecTypeID)
                && (woOldMap == null || woOldMap.get(wo.Id).SVMXC__Component__c != wo.SVMXC__Component__c
                    || woOldMap.get(wo.Id).SVMXC__Top_Level__c != wo.SVMXC__Top_Level__c)
              ){
                componentWOIds.put(wo.SVMXC__Component__c, wo.Id);
                componentWOIds.put(wo.SVMXC__Top_Level__c, wo.Id);
            }
        }
        Map<Id, SVMXC__Installed_Product__c> ip_LocMap = new Map<Id, SVMXC__Installed_Product__c>();
        // fetching Installed Product (Components) Details
        if(!componentWOIds.isEmpty()){
            for(SVMXC__Installed_Product__c ip : [SELECT Id, SVMXC__Site__c, SVMXC__Site__r.Name, PCode_is_Reference_Only__c FROM SVMXC__Installed_Product__c 
                                                  WHERE Id IN: componentWOIds.keySet()]) {
                ip_LocMap.put(ip.Id, ip);                                          
            }
            for(SVMXC__Service_Order__c wrkOrd : woNewMap.values()) {
                if(ip_LocMap.containsKey(wrkOrd.SVMXC__Component__c)) {
                    if(ip_LocMap.get(wrkOrd.SVMXC__Component__c).SVMXC__Site__c != wrkOrd.SVMXC__Site__c) {
                        wrkOrd.addError('<font color="red"><b>Component selected is not installed at the Site for this Work Order. If you feel this is in error, please contact RSS/FOA for review, or open a ticket with UnityHelp</b></font>', FALSE);
                    }
                }
                //STRY0027138:HelpDesk:  Prevent Case Creation and Work Order PS Line Creation for Reference Only Pcodes
                if(ip_LocMap.containsKey(wrkOrd.SVMXC__Component__c)) {
                    if(ip_LocMap.get(wrkOrd.SVMXC__Component__c).PCode_is_Reference_Only__c) {
                        wrkOrd.addError('You\'ve chosen a Component that is tracked for Reference only.  Please select a Component where Activity can be applied');
                    }
                }
                //STRY0027138:HelpDesk:  Prevent Case Creation and Work Order PS Line Creation for Reference Only Pcodes
                if(ip_LocMap.containsKey(wrkOrd.SVMXC__Top_Level__c)) {
                    if(ip_LocMap.get(wrkOrd.SVMXC__Top_Level__c).PCode_is_Reference_Only__c) {
                        wrkOrd.addError('You\'ve chosen a Component that is tracked for Reference only.  Please select a Component where Activity can be applied');
                    }
                }
            }
        }
    }
}
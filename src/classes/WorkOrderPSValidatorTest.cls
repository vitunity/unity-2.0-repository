/**
 * Puneet Mishra, 17 oct 2016
 * Controller : WorkOrderPSValidator
 */
@isTest
public class WorkOrderPSValidatorTest {
    
    public static BusinessHours businesshr;
    public static SVMXC__Service_Group__c servTeam;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC_Timesheet__c TimeSheet;
    public static Timecard_Profile__c TCprofile;
    public static SVMXC_Time_Entry__c timeEntry;
    public static SVMXC_Time_Entry__c timeEntry2;
    public static List<SVMXC_Time_Entry__c> timeEntryList;
    public static Account newAcc;
    public static Contact newCont;
    public static ERP_Project__c erpProj;
    public static Case newCase;
    public static Product2 newProd;
    public static Sales_Order__c SO;
    public static Sales_Order_Item__c SOI;
    public static SVMXC__Service_Order__c WO;
    public static SVMXC__Service_Order_Line__c WD;
    public static SVMXC__Service_Order_Line__c WD1;
    public static List<SVMXC__Service_Order_Line__c> WDList;
    public static SVMXC__Site__c newLoc;
    public static ERP_WBS__c WBS;
    public static ERP_NWA__c NWA;
    public static SVMXC__Installed_Product__c objTopLevel = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent = new SVMXC__Installed_Product__c();
    public static SVMXC__Installed_Product__c objComponent1 = new SVMXC__Installed_Product__c();
    public static Product2 objProd;
    
    Static {
        businesshr = [SELECT Id FROM BusinessHours WHERE isDefault =: true LIMIT 1];
        
        TCprofile = UnityDataTestClass.TIMECARD_Data(false);
        TCprofile.Name = 'PM Indirect Time Entry Matrix profile';
        Integer Yr = system.today().Year();
        Integer Mon = system.today().Month();
        Integer Dat = system.today().Day();
        TCprofile.Normal_Start_Time__c = Datetime.newInstance(Yr, Mon, Dat, 11, 0, 0);
        insert TCprofile;
        
        servTeam = UnityDataTestClass.serviceTeam_Data(true);
        
        technician = UnityDataTestClass.Techinician_Data(false, servTeam, businesshr);
        technician.Timecard_Profile__c = TCprofile.Id;
        technician.User__c = userInfo.getUserId();
        insert technician;
        
        newLoc = [Select Id, ERP_CSS_District__c FROM SVMXC__Site__c WHERE ERP_CSS_District__c =: servTeam.SVMXC__Group_Code__c LIMIT 1];
        
        newAcc = UnityDataTestClass.AccountData(true);
        erpProj = UnityDataTestClass.ERPPROJECT_Data(true);
        newCase = [SELECT Id, ERP_Project_Number__c FROM Case WHERE ERP_Project_Number__c =: erpProj.Name LIMIT 1];
        newProd = UnityDataTestClass.product_Data(true);
        SO = UnityDataTestClass.SO_Data(true,newAcc);
        SOI = UnityDataTestClass.SOI_Data(true, SO);
        
        WBS = UnityDataTestClass.ERPWBS_DATA(true, erpProj, technician);
        
        List<ERP_NWA__c> NWALIst = new List<ERP_NWA__c>();
        NWA = UnityDataTestClass.NWA_Data(false, newProd, WBS, erpProj);
        NWA.ERP_Std_Text_Key__c = 'PM00001';
        nwa.ERP_Status__c = 'CLSD';
        insert NWA;
        
        
        
        
            CountryDateFormat__c cdf = new CountryDateFormat__c();
            cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
            cdf.Name ='Default';
            insert cdf;
        
        WO = new SVMXC__Service_Order__c();
        //WO.SVMXC__Case__c = newCase.id;
        WO.Event__c = True;
        WO.SVMXC__Company__c = newAcc.ID;
        WO.SVMXC__Order_Status__c = 'Open';
        WO.SVMXC__Preferred_Technician__c = technician.id;
        WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Installation').getRecordTypeId();
        WO.SVMXC__Product__c = newProd.ID;
        WO.SVMXC__Problem_Description__c = 'Test Description';
        WO.SVMXC__Preferred_End_Time__c = system.now()+3;
        WO.SVMXC__Preferred_Start_Time__c  = system.now();
        WO.Subject__c= 'bbb';
        //WO.ERP_WBS__c = WBS.Id;
        WO.Sales_Order_Item__c = SOI.id;
        //WO = UnityDataTestClass.WO_Data(false, newAcc, newCase, newProd, SOI, technician);
        //WO.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        //WO.ERP_WBS__c = WBS.Id;
        //WO.ERP_Service_Order__c = '123';
        //WO.Installation_Manager__c = userInfo.getUserId();
        insert WO;
        
        WDList = new List<SVMXC__Service_Order_Line__c>();
        
        WD = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
        WD.SVMXC__Consumed_From_Location__c = newLoc.id;
        WD.SVMXC__Activity_Type__c = 'Install';
        WD.Completed__c=false;
        WD.SVMXC__Group_Member__c = technician.id;
        //WD.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        WD.RecordTypeId = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order_Line__c').get('UsageConsumption');
        WD.SVMXC__Work_Description__c = 'test';
        WD.SVMXC__Start_Date_and_Time__c =System.now();
        WD.Actual_Hours__c = 5;
        WD.SVMXC__Service_Order__c = WO.id;
        WD.Sales_Order_Item__c = SOI.id;
        WD.NWA_Description__c = 'nwatest';
        WD.SVMXC__Line_Status__c = 'Open';
        WD.Billing_Type__c = 'Installation';
        WD.Work_Order_RecordType__c = 'Installation';
        insert WD;
        //WDList.add(WD);
        
        objProd = new Product2();
        objProd.Name = 'Test Product';
        objProd.SVMXC__Product_Line__c = 'Accessory';
        objProd.Skills_Required__c = 'BEAM' ;
        objProd.Returnable__c = 'Scrap Locally';
        insert objProd;
        
        objTopLevel.Name = 'H23456';
        objTopLevel.SVMXC__Serial_Lot_Number__c = 'H23456';
        objTopLevel.SVMXC__Product__c = objProd.ID;
        objTopLevel.SVMXC__Site__c = newLoc.ID;
        objTopLevel.ERP_Reference__c = 'H23456';
        objTopLevel.SVMXC__Preferred_Technician__c = technician.ID;
        objTopLevel.ERP_CSS_District__c = 'ABC';
        objTopLevel.ERP_Sales__c = 'Test ERP Sales';
        insert objTopLevel;
        
        objComponent.Name = 'H12345';
        objComponent.SVMXC__Serial_Lot_Number__c = 'H12345';
        objComponent.SVMXC__Parent__c = objTopLevel.ID;
        objComponent.SVMXC__Product__c = objProd.ID;
        objComponent.SVMXC__Preferred_Technician__c = technician.ID;
        objComponent.ERP_Sales__c = 'Test ERP Sales';
        objComponent.ERP_Reference__c = 'H12345';
        objComponent.SVMXC__Top_Level__c = objTopLevel.id;
        //objComponent.Service_Team__c = objServTeam.Id;
        objComponent.ERP_CSS_District__c = 'ABC';
        objComponent.SVMXC__Site__c = newLoc.id;
        objComponent.SVMXC__Status__c = 'Shipped';
        objComponent.SVMXC__Company__c = newAcc.id;
        insert objComponent;
        
    }
    
     private static testMethod void componentLocValidator_EmptyTest() {
        Test.StartTest();
        WorkOrderPSValidator.componentLocValidator(new Map<ID, SVMXC__Service_Order__c>(),new Map<ID, SVMXC__Service_Order__c>());
        Test.Stoptest();
    }
    
    private static testMethod void emptyMap() {
    
        WorkOrderPSValidator.psDeleteValidate(new Map<ID, SVMXC__Service_Order_Line__c>());
    }
    
    private static testMethod void WDB4triggercoverage() {
    try{
            delete [select id from CountryDateFormat__c];
            System.assertEquals(0,[select count() from CountryDateFormat__c]);
            update WD;
            System.assert(false, 'Exception expected');
            }
            catch(Exception e)
            {
            CountryDateFormat__c cdf = new CountryDateFormat__c();
            cdf.Date_Format__c = 'dd-MM-YYYY kk:mm';
            cdf.Name ='Default';
            insert cdf;
            system.assert(true);
            }
        
    }
    
    private static testMethod void psDeleteValidate() {
    System.debug('%%%$$$####');
        Test.StartTest();
        
        try {
            WD1 = UnityDataTestClass.WD_Data(false, WO, technician, newLoc, SOI, NWA);
            WD1.SVMXC__Consumed_From_Location__c = newLoc.id;
            WD1.SVMXC__Activity_Type__c = 'Install';
            WD1.Completed__c=false;
            WD1.SVMXC__Group_Member__c = technician.id;
            WD1.SVMXC__Work_detail__c = WD.Id;
            WD1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
            WD1.SVMXC__Work_Description__c = 'test';
            WD1.SVMXC__Start_Date_and_Time__c =System.now();
            WD1.Actual_Hours__c = 5;
            WD1.SVMXC__Service_Order__c = WO.id;
            WD1.Sales_Order_Item__c = SOI.id;
            WD1.NWA_Description__c = 'nwatest';
            WD1.SVMXC__Line_Status__c = 'Open';
            WD1.Billing_Type__c = 'Installation';
            WD1.Work_Order_RecordType__c = 'Installation';
            //WDList.add(WD1);
            insert WD1;        
        
            delete WD;
        } catch(Exception e) {
            
        }
        Test.stopTest();
    }
    
    private static testMethod void componentLocValidator_NullTest() {
        Test.StartTest();
           
            WorkOrderPSValidator.componentLocValidator(null,null);
            
        Test.Stoptest();
    } 
    
   
    
    private static testMethod void componentLocValidator_test() {
        Test.StartTest();
            WO = new SVMXC__Service_Order__c();
            WO.Event__c = True;
            WO.SVMXC__Company__c = newAcc.ID;
            WO.SVMXC__Order_Status__c = 'Open';
            WO.SVMXC__Preferred_Technician__c = technician.id;
            //WO.recordtypeid = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Field Service').getRecordTypeId();
            WO.recordtypeid = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
            WO.SVMXC__Product__c = newProd.ID;
            WO.SVMXC__Problem_Description__c = 'Test Description';
            WO.SVMXC__Preferred_End_Time__c = system.now()+3;
            WO.SVMXC__Preferred_Start_Time__c  = system.now();
            WO.Subject__c= 'bbb';
            WO.Sales_Order_Item__c = SOI.id;
            WO.SVMXC__Component__c = objComponent.Id;
            insert WO;
            
            ERP_Pcode__c erp = new ERP_Pcode__c();
            erp.Reference_Only__c = true;
            insert erp;
            
            
            objComponent1.Name = 'H12345';
            objComponent1.SVMXC__Serial_Lot_Number__c = 'H12345';
            objComponent1.SVMXC__Parent__c = objTopLevel.ID;
            objComponent1.SVMXC__Product__c = objProd.ID;
            objComponent1.SVMXC__Preferred_Technician__c = technician.ID;
            objComponent1.ERP_Sales__c = 'Test ERP Sales';
            objComponent1.ERP_Reference__c = 'H12345';
            objComponent1.SVMXC__Top_Level__c = objTopLevel.id;
            objComponent1.ERP_CSS_District__c = 'ABC';
            objComponent1.SVMXC__Site__c = newLoc.id;
            objComponent1.SVMXC__Status__c = 'Shipped';
            objComponent1.SVMXC__Company__c = newAcc.id;            
            objComponent1.ERP_Pcodes__c = erp.id;
            insert objComponent1;
            
            
            //System.assertEquals([Select PCode_is_Reference_Only__c,ERP_Pcodes__r.id,ERP_Pcodes__r.Reference_Only__c from SVMXC__Installed_Product__c where id=:objComponent1.id][0].ERP_Pcodes__r.id,erp.id);
            
           
            WO.recordtypeid = RecordTypeUtility.getInstance().rtMaps.get('SVMXC__Service_Order__c').get('Field_Service');
            WO.SVMXC__Component__c = objComponent1.Id;
            WO.SVMXC__Top_Level__c = objComponent.Id;
            update WO;
            
            //WorkOrderPSValidator.componentLocValidator(new Map<Id, SVMXC__Service_Order__c>{WO_New.Id => WO_New},new Map<Id, SVMXC__Service_Order__c>{WO.Id => WO});
            
        Test.StopTest();
    }
    
    
    
}
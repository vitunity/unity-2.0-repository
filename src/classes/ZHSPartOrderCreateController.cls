/*
 * For the ZHS order creation, it should be used sales org 4400 instead of “Bonded Warehouse” flag.  
 * Because the ZHS order needs to be created for non-Bonded warehouse too.  
 * US4422
 * Test Class - SR_PartOrderLine_Test
 */
public with sharing class ZHSPartOrderCreateController implements Queueable{
    
    private list<SVMXC__RMA_Shipment_Order__c> partsOrderList;
    private list<SVMXC__RMA_Shipment_Line__c> partsOrderLineList;
    
    public ZHSPartOrderCreateController (list<SVMXC__RMA_Shipment_Order__c> partsOrderList, list<SVMXC__RMA_Shipment_Line__c> partsOrderLineList){
        this.partsOrderList = partsOrderList;
        this.partsOrderLineList = partsOrderLineList;
    }
    
    public void execute(QueueableContext context) {
        if(!partsOrderList.isEmpty()) {
            insert partsOrderList;
            if(!partsOrderLineList.isEmpty()){
                for(SVMXC__RMA_Shipment_Line__c varPOL : partsOrderLineList){
                    if(varPOL.SVMXC__RMA_Shipment_Order__r != null){
                        varPOL.SVMXC__RMA_Shipment_Order__c = varPOL.SVMXC__RMA_Shipment_Order__r.Id;
                    }
                }
                insert partsOrderLineList;
            }
            for (SVMXC__RMA_Shipment_Order__c po : partsOrderList) {
                po.Interface_Status__c = 'Process';
                po.SVMXC__Order_Status__c = 'Submitted';
            }
            update partsOrderList;
        }
    }
}
/*
 *  @author: amitkumar katre
 *  @description: this is singlton class to get the transaltion of custom labels
 */
public without sharing class ZLabelTranslator {
    
    private static ZLabelTranslator objZLabelTransaltor;
    public map<String,map<String,String>> customLabelMap;
    
    /*
     * private singlton class constructor 
     * it contains custom label transalation logic.
     */
    private ZLabelTranslator(){
        customLabelMap = new map<String,map<String,String>> ();
        list<String> languages = new list<string>{'en','ja','fr','es'};
        for(String lanObj : languages){
          Pagereference r = Page.ZLabelTranslator;
            r.getParameters().put('label_lang', lanObj);
            String pageContents;
            //system.debug('pageContent======'+pageContents);
            if(!System.Test.isRunningTest()){
               pageContents = r.getContent().toString().replaceAll('"','');
            }else{
                 pageContents = 'TestLabel2:TraslationŒTestLabel2:Traslation';
            }
            list<String> labelValues = pageContents.split('Œ',-2);
            map<String,String> customLabelsForEachLang = new map<String,String>();
            for(String lOBj : labelValues){
                list<String> lOBjList = lOBj.split('§',-2);
                system.debug(lOBjList);
                if(!lOBjList.isEmpty() & lOBjList.size() == 2){
                    customLabelsForEachLang.put(lOBjList[0].trim(),lOBjList[1].trim());
                }
            }
            customLabelMap.put(lanObj,customLabelsForEachLang);
            system.debug('**value:'+JSON.serialize(customLabelMap));
        }
    }
    
    /*
     * New mothod create for label transaltion 
     */
    public static map<String,String> GetLabelTranslation(String lang){
        Pagereference r = Page.ZLabelTranslator;
        r.getParameters().put('label_lang', lang);
        String pageContents;
        if(!System.Test.isRunningTest()){
           pageContents = r.getContent().toString().replaceAll('"','');
        }else{
           pageContents = 'TestLabel2:TraslationŒTestLabel2:Traslation';
        }
        list<String> labelValues = pageContents.split('Œ',-2);
        map<String,String> customLabelsForEachLang = new map<String,String>();
        for(String lOBj : labelValues){
            list<String> lOBjList = lOBj.split('§',-2);
            if(!lOBjList.isEmpty() & lOBjList.size() == 2){
                customLabelsForEachLang.put(lOBjList[0].trim(),lOBjList[1].trim());
            }
        }
        return customLabelsForEachLang;
    }
    
    /*
     * singlton class instance
     */
    public static ZLabelTranslator getInstance(){
        if(objZLabelTransaltor == null){
            objZLabelTransaltor = new ZLabelTranslator();
        }
        return objZLabelTransaltor;
    }
    
    /*
     * this is utility method
     */
    public static list<String> translate(String labelName, String language){
        Pagereference r = Page.ZLabelTranslator;
        r.getParameters().put('label_lang', language);
        //r.getParameters().put('label', labelName);  
        //list<String> labelValue = r.getContent().toString().split(';',-2);
        //return labelValue;
        return null;
    }
     
}
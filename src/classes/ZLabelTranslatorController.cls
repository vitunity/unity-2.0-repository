/*
 * @author: amitkumar katre
 * @description: custom label taransaltion controller
 */

public class ZLabelTranslatorController {
    public String label_lang {get;set;}
    public String label {get;set;}
    
    public list<List_of_Custom_Labels__c> getCustomLabels(){
        //get the list of custom labels form custom setting
        return List_of_Custom_Labels__c.getall().values(); 
    }
    
    public  ZLabelTranslatorController(){
       Map<String, String> reqParams = ApexPages.currentPage().getParameters(); 
       label_lang = reqParams.get('label_lang');
       label = reqParams.get('label');
    }
}
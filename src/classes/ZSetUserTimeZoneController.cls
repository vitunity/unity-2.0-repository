/***************************************************************************
Author: Nick Sauer
Created Date: 01-January-2018
Project/Story/Inc/Task : STRY0042426- WTimecard:  VF Button on HomePage to Match SFDC Time to Windows Time 
Description: SetUserTimeZoneController

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/

global with sharing class ZSetUserTimeZoneController {
     
    public class ReturnStatus{
        boolean success;
        String message;    
    }
     
    @RemoteAction
    global static String getPreference(String userTimeZone){
        ReturnStatus returnStatus = new ReturnStatus();
        returnStatus.success = true;
        returnStatus.message = 'None';
        try{
            if(userTimeZone != userInfo.getTimeZone().getID()){
                User self = [SELECT ID, Time_Zone_Detection__c  FROM User WHERE ID =:UserInfo.getUserId()];
                returnStatus.message = self.Time_Zone_Detection__c;
            }
        }catch(Exception e){
             
        }
         
        return JSON.serialize(returnStatus);
    }
     
    @RemoteAction
    global static String setTimeZone(String userTimeZone){
        ReturnStatus returnStatus = new ReturnStatus();
        returnStatus.success = true;
        returnStatus.message = 'Lolz';
        try{
            if(userTimeZone != userInfo.getTimeZone().getID()){
                //User self = [SELECT ID, TimeZoneSidKey FROM User WHERE ID =:UserInfo.getUserId()];
                User self = new User();
                self.id = userInfo.getUserId();
                self.TimeZoneSidKey = userTimeZone;
                update self;
                returnStatus.message = 'Successfully Changed the time zone to ' + userTimeZone;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Salesforce Timezone updated to'+userTimeZone));
                return null;
            }
        }catch(Exception e){
            returnStatus.success = false;
            returnStatus.message = e.getMessage();
        }
        return JSON.serialize(returnStatus);
    }
}
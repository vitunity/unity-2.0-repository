/***************************************************************************
Author: Nick Sauer
Created Date: 11-January-2018
Project/Story/Inc/Task : STRY0042426- Timecard:  VF Button on HomePage to Match SFDC Time to Windows Time 
Description: SetUserTimeZoneController test class

Change Log: 
Date/Modified By Name/Task or Story or Inc # /Description of Change
*************************************************************************************/

@isTest
public class ZSetUserTimeZoneTest {
    
    	private string userTimeZone = 'GMT';
    
    	//Create User for Test
	    private static User InsertUserData(Boolean isInsert) {
        list<profile> serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Service - User' or Name =: 'VMS Unity 1C - Service User'];
        User serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile[0].Id, Email = 'test1c@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1c@service.com');
        if(isInsert)
            insert serviceUser;
        
        return serviceUser;
    }
    
    @isTest private static void UnitTestNoChange() {
    Test.StartTest(); 
    string userTimeZone = 'GMT';
    PageReference pageRef = Page.ZSetUserTimeZone;
    Test.setCurrentPage(pageRef);    
    ZSetUserTimeZoneController ctrl = new ZSetUserTimeZoneController();
    ZSetUserTimeZoneController.getPreference(userTimeZone);
    ZSetUserTimeZoneController.setTimeZone(userTimeZone);
    for(ApexPages.Message msg :  ApexPages.getMessages()) {
    	//System.assertEquals('Success', msg.getSummary());
    	//System.assertEquals(ApexPages.Severity.INFO, msg.getSeverity()); 
	}
    //ctrl.getPreference(userTimeZone);
    //ctrl.setTimeZone(userTimeZone);
        
    Test.stopTest();
    }
    
    @isTest private static void UnitTestChange() {
    Test.StartTest(); 
    string userTimeZone = 'America/Los_Angeles';
    PageReference pageRef = Page.ZSetUserTimeZone;
    Test.setCurrentPage(pageRef);    
    ZSetUserTimeZoneController ctrl = new ZSetUserTimeZoneController();
    ZSetUserTimeZoneController.getPreference(userTimeZone);
    ZSetUserTimeZoneController.setTimeZone(userTimeZone);
    for(ApexPages.Message msg :  ApexPages.getMessages()) {
    	//System.assertEquals('Salesforce Timezone updated toAmerica/Los_Angeles', msg.getSummary());
    	//System.assertEquals(ApexPages.Severity.INFO, msg.getSeverity()); 
	}
    //ctrl.getPreference(userTimeZone);
    //ctrl.setTimeZone(userTimeZone);
        
    Test.stopTest();
    }
}
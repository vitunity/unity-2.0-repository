/*
Project No    : 5142
Name          : apiDocumentationController Controller
Created By    : Yogesh Nandgadkar
Date          : 10th March, 2017
Purpose       : An Apex Controller for Home Page
Updated By    : Yogesh Nandgadkar
Last Modified Reason  :   Updated to dislay the proper messages at UI 
*/
public with sharing class apiDocumentationController {
    public Boolean isGuest{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    // Added for INTERNAL User Validation
    public boolean eligibleInternalUserFlag{get;set;}
    
    public apiDocumentationController(){
    	shows='none';
        isGuest = SlideController.logInUser();
        
        checkInternalUser();
        
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }    
    }
    
    public void checkInternalUser(){        
        apiKeyMgmtHomeController apiKeyObj = new apiKeyMgmtHomeController();
        eligibleInternalUserFlag = apiKeyObj.validateMyVarianInternalUser();        
    }
    
    public Id getAPIKeyDocument(){
        List<API_Key_Document_Id__c> apiKeyDocument = API_Key_Document_Id__c.getall().values();
        List<ContentVersion> lstContentVersion = [
            SELECT id,Mutli_Languages__c,Title,Document_Type__c,Date__c,ContentDocumentId, Complaint__c
			FROM ContentVersion 
            WHERE Id =:apiKeyDocument[0].Name
        ];
        if(!lstContentVersion.isEmpty()){
            return lstContentVersion[0].Id;
        }
        return null;
    }
}
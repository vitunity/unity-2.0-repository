/*
Project No    : 5142
Name          : apiDocumentationController_Test
Created By  : Jay Prakash 
Date        : 11th APR, 2017
Purpose     : Test class - API Documentatation
Updated By  : Jay Prakash [ To Improve the code Coverage to 100% ]
Updated Date : 05th May, 2017
*/
@isTest(SeeAllData = false)
private class apiDocumentationController_Test{
    
    public static INVOKE_SAP_FM_SETTINGS__c csObj ; 
    public static PROXY_PHP_URL_DEV2__c proxyPHPURL ; 
    public static SAP_ENDPOINT_URL_DEV2__c sapURL ; 
    public static SAP_Login_Authorization__c sapLogInAuthUserName ; 
    public static SAP_Login_Authorization__c sapLogInAuthPwd ; 
        
    static testMethod void validateGetApiTypes()    
    {        
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
         //SAP Endopoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
        
         insert proxyPHPURL;
         insert sapURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;        
         insert csObj ;
         
    	 test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockGetApiTypes());
         APIKeySAPIntegrationController obj = new APIKeySAPIntegrationController();
         obj.getApiTypes();
        
         apiDocumentationController apiDocObj = new apiDocumentationController();
         apiDocObj.checkInternalUser();
         apiDocObj.getAPIKeyDocument();
         test.stopTest();                 
    }
    
    @testsetup private static void insertAPIKeyCustomSetting(){
        insert new API_Key_Document_Id__c(Name = '123456789012345');
    }
}
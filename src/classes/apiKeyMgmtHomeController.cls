/*
Project No    : 5142
Name          : apiKeyMgmtHomeController Controller
Created By    : Yogesh Nandgadkar
Date          : 29th MAY, 2017
Purpose       : An Apex Controller for Home Page
Updated By    : Jay Prakash
Last Modified Reason  : Updated for validation for INTERNAL USER
*/
public with sharing class apiKeyMgmtHomeController {
    APIRequestController apiKeyReqObj = new APIRequestController();
    public Boolean isGuest{get;set;}
    public string Usrname{get;set;}
    public string shows{get;set;}
    
    //Added for Validation ARIA v15.1
    public boolean eligibleCustomer{get;set;}
    public boolean ariaProductInstalledFlag{get;set;}
    
    
    // Added for INTERNAL User Validation
    public boolean eligibleInternalUser{get;set;} 
    Public Id conId{get;set;} 
    public String contactName{get;set;}
    Public Id AccId{get;set;} 
    public String contactPhone{get;set;}
    public String contactEmail{get;set;}    
    
    public apiKeyMgmtHomeController(){
        eligibleCustomer = false; 
        eligibleInternalUser = false; 
        ariaProductInstalledFlag =false;
        shows='none';
        isGuest = SlideController.logInUser();
        
        if ([Select Id, AccountId,ContactId From User where id = : Userinfo.getUserId() limit 1].ContactId == null)
        {
            if([Select usertype From User where id = : Userinfo.getUserId() limit 1].usertype != label.guestuserlicense){
                Usrname=[Select Id, AccountId,ContactId,alias From User where id = : Userinfo.getUserId() limit 1].alias;
                shows='block';
                isGuest=false;
            }
        }
        validateMyVarianInternalUser();
        validateContactInstalledProduct();
    }
    
    // Method for Contact/Customer Validation for ARIA V15.1 or higher - API Key App         
    public Boolean validateContactInstalledProduct(){        
        String LoggedInUserId = apiKeyReqObj.getUserAuthId();
        
        // Retrieving ContactId from USER        
        List<User> userData = [ SELECT Id,contactId from USER where id=: LoggedInUserId];
        if (userData.size() > 0 && !eligibleInternalUser) {
            
           Id conId = userData[0].contactId;
        
            // Retrieving Contact Data from Contact based on ContactId
            List<Contact> contactData = [ SELECT Id, AccountId, Name, Phone, Email from CONTACT where id=: conId]; 
                       
           Id accID = contactData[0].AccountId;
            //System.debug('Account ID from Contact======>'+accID);
            
            // SOQL for Verifying the Installed Product with V15.1
            List<SVMXC__Installed_product__c> lstInstalledProd= [
                SELECT id,name,SVMXC__Company__r.SFDC_Account_Ref_Number__c 
                FROM SVMXC__Installed_product__c 
                WHERE SVMXC__Company__c =:accID  
                //AND ERP_Pcodes__r.Name = 'HIT'       Removed to enable ARIA MO (March 3, 2018)          
            ]; //Removed AND Product_Version_Number__c >= 15.1  by Abhishek K on 31st oct 2017 to Make Key Available for All Versions
        
            // If above Condition is true, make this 'eligibleCustomer' flag true
            if(lstInstalledProd.size() > 0){
                eligibleCustomer=true;
            } 
        }        
        return eligibleCustomer;
    }
    
    // Method for Validating Internal USER      
    public boolean validateMyVarianInternalUser(){        
        String LoggedInUserId = apiKeyReqObj.getUserAuthId();
        
         // Retrieving ContactId from USER        
        List<User> userData = [ SELECT Id,contactId from USER where id=: LoggedInUserId]; 
        conId = userData[0].contactId;
        
        // Retrieving Contact Data from Contact based on ContactId      
        List<Contact> contactData ;  contactData  = [ SELECT Id, AccountId, Name, Phone, Email from CONTACT where id=: conId]; 
      
        if(!contactData.isEmpty()){
            contactName = contactData[0].Name;                         
            AccId = contactData[0].AccountId;
            contactPhone = contactData[0].Phone;
            contactEmail = contactData[0].Email;
                        
        }else{
            eligibleInternalUser = true;
        }
        
         return eligibleInternalUser;       
    }
}
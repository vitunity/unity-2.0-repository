/*
Project No    : 5142
Name          : apiKeyMgmtHomeController_Test
Created By  : Jay Prakash 
Date        : 11th April, 2017
Purpose     : Test class - To Verify the Functionality API key Mgmt Home Page
Updated By  : Jay Prakash [ To Improve the code Coverage ]
Updated Date : 05th May, 2017
*/
@isTest
private class apiKeyMgmtHomeController_Test{   
    
    public static INVOKE_SAP_FM_SETTINGS__c csObj ; 
    public static PROXY_PHP_URL_DEV2__c proxyPHPURL ; 
    public static SAP_ENDPOINT_URL_DEV2__c sapURL ; 
    public static SAP_Login_Authorization__c sapLogInAuthUserName ; 
    public static SAP_Login_Authorization__c sapLogInAuthPwd ; 
        
    static testMethod void validateHomePage()          
    {          
		Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
        
		Account a = new Account(
            Name='test2',
            BillingPostalCode ='12235',
            BillingCity='California',
            BillingCountry='Test',
            BillingState='Washington',
            BillingStreet='xyx',
            Country__c='Test'
        ); 
        insert a; 
        
        Contact con = new Contact(
            Lastname='test',
            RAQA_Contact__c=true,
            FirstName='Singh',
            Email='kumar.amit667@gmail.com', 
            AccountId=a.Id,
            MailingCountry ='test', 
            MailingState='teststate'
        ); 
        insert con;
        User u = getUser(con.Id);
        insert u;
          
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;
        
          System.RunAs(u) {
            test.startTest();

              Account accObj = new Account();
              accObj.name = 'Test';
              accObj.BillingCity = 'Test';
              accObj.Country__c= 'India';
              accObj.BillingCountry='Test';
              accObj.Ext_Cust_Id__c = 'test' ;
              insert accobj ; 
              
              Contact  conObj = new Contact (AccountId = accObj.id,FirstName='FName',LastName = 'portalTestUser',Phone='1234567890', Email='test123@varian.com', MobilePhone='123114567890');
              insert  conObj ;
              
              apiKeyMgmtHomeController  apiKeyObj = new apiKeyMgmtHomeController (); 
              test.stopTest();         
        }        
    }
    
    static testMethod void validateMyVarianInternalUser()    
    {        
        
		Account a = new Account(
            Name='test2',
            BillingPostalCode ='12235',
            BillingCity='California',
            BillingCountry='Test',
            BillingState='Washington',
            BillingStreet='xyx',
            Country__c='Test'
        ); 
        insert a; 
        
        Contact con = new Contact(
            Lastname='test',
            RAQA_Contact__c=true,
            FirstName='Singh',
            Email='kumar.amit667@gmail.com', 
            AccountId=a.Id,MailingCountry ='test', 
            MailingState='teststate'
        ); 
        insert con;
        User u = getUser(con.Id);
        insert u;
        
		System.debug('-----TEST'+u.ContactId);  
         //SAP Log In Auth Token
		sapLogInAuthUserName = new SAP_Login_Authorization__c();
		sapLogInAuthUserName.name='Username';
		sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
		sapLogInAuthPwd = new SAP_Login_Authorization__c();
		sapLogInAuthPwd.name='Password';           
		sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
		insert sapLogInAuthUserName;
		insert sapLogInAuthPwd;
        
        //Test for internal user
		apiKeyMgmtHomeController  apiKeyObj = new apiKeyMgmtHomeController ();
        System.RunAs(u) {
			test.startTest();
              
			//Test for external user
            apiKeyMgmtHomeController  apiKeyObj1 = new apiKeyMgmtHomeController(); 
            System.debug('---LOGGEDINUSERID TEST'+UserInfo.getUserId());
            test.stopTest();         
        }                  
    }
    
    static testMethod void validateContactInstalledProduct()    
    {        
       	
        //Proxy PHP URL
         proxyPHPURL = new PROXY_PHP_URL_DEV2__c();
         proxyPHPURL.name='Proxy PHP URL';
         proxyPHPURL.ProxyURL__c='https://mobidev.varian.com/MobileWeb/sapproxy/src/callsapwebservice.php';
        
         //SAP Endopoint URL
         sapURL = new SAP_ENDPOINT_URL_DEV2__c();
         sapURL.name='SAP Endpoint URL';
         sapURL.SAP_URL__c='https://ehd.cis.varian.com:8251/zcallfmviarest/';
        
         //SAP Log In Auth Token
         sapLogInAuthUserName = new SAP_Login_Authorization__c();
         sapLogInAuthUserName.name='Username';
         sapLogInAuthUserName.LoginDetails__c='RFC_API_MGMT';
        
         sapLogInAuthPwd = new SAP_Login_Authorization__c();
         sapLogInAuthPwd.name='Password';           
         sapLogInAuthPwd.LoginDetails__c='Api@5142';
        
         csObj = new INVOKE_SAP_FM_SETTINGS__c();
         csObj.name = 'API Type';
         csObj.FM_Call__c = 'ZSAAS_PROVIDE_APITYPES';
        
         insert proxyPHPURL;
         insert sapURL;
         insert sapLogInAuthUserName;
         insert sapLogInAuthPwd;        
         insert csObj ;
         
    	 test.startTest();
         Test.setMock(HttpCalloutMock.class, new MockGetApiTypes());
         APIKeySAPIntegrationController obj = new APIKeySAPIntegrationController();
         obj.getApiTypes();
        
         apiKeyMgmtHomeController apiKeyObj = new apiKeyMgmtHomeController();
         test.stopTest();       	
    }
    
    private static User getUser(Id contactId){
        Profile p = [select Id from profile where Name = 'VMS MyVarian - Customer User'];
        return new User(
            alias = 'standt', 
            Subscribed_Products__c=true,
            email='standarduser@testorg.com',
            emailencodingkey='UTF-8', 
            lastname='Testing',
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            Contactid=ContactId, 
            username='test__user1@testorg.com'
        ); 
    }
}
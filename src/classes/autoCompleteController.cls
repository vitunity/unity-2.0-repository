global class autoCompleteController {

    public autoCompleteController() {

    }


    public autoCompleteController(ApexPages.StandardController controller) {

    }

    @RemoteAction
    global static SObject[] findSObjects(string obj, string qry, string addFields, string Accid) {
        // more than one field can be passed in the addFields parameter
        // split it into an array for later use
        List<String> fieldList;
        if (addFields != null) fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        // create the filter text
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String soql = 'select id,name ';
        // if an additional field was passed in add it to the soql
        if (fieldList != null) {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        soql += ' from ' + obj + ' where name' + filter;
        String recid = Schema.SObjectType.Account.RecordTypeInfosByName.get('Site Partner').RecordTypeId;
        //for contact and Product
        Schema.DescribeSObjectResult r = Account.sObjectType.getDescribe();// added by harshita to get the object prefix
        if(Accid != 'Account' && Accid != null && Accid != '' && (Accid.startsWith(r.getKeyPrefix())))
        {
            soql += ' and ' + fieldList[0] + '=:Accid';
        }else if(Accid == 'Account'){
            system.debug('Mapvalue--->' + Schema.SObjectType.Account.RecordTypeInfosByName);
            
            soql += ' and recordtypeId =: recid' ;
        }
        // add the filter by additional fields to the soql
       /* if (fieldList != null) {
            for (String s : fieldList) {
                soql += ' or ' + s + filter;
            }
        } */
        
        if(obj == 'Contact')
            soql += ' and Account.RecordtypeId =: recid and AccountId != null';
        else if(obj == 'SVMXC__Installed_Product__c')
            soql += ' and SVMXC__Company__r.RecordtypeId =: recid and SVMXC__Company__c != null';
        soql += ' order by Name limit 20';
        List<sObject> L = new List<sObject>();
        system.debug('SOQL--->' + soql);
        //keyPrefix = ;
        try {
            //if(Accid != null && (Accid.startsWith(r.getKeyPrefix()) || Accid == 'Account'))
                L = Database.query(soql);
        }
        catch (QueryException e) {
            return null;
        }
        return L;
   }
}
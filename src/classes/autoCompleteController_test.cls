/***************************************************************************\
    @ Author                : Kaushiki Verma
    @ Date                  : 20/11/2014
****************************************************************************/
@isTest 
public class autoCompleteController_test {

    public static testMethod void autoCompleteController_test () {
    id rec1 = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 

    id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 

    Account testAcc = accounttestdata.createaccount();
    testAcc.Name= 'TestAccount';
    testAcc.recordtypeId = rec;
    insert testAcc;
     // insert Contact  
    Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
    insert testcon;
    
    sObject[] s1 = autoCompleteController.findSObjects('Account', 'TestAccount', ' ',testAcc.id);

     
    Case testCase = SR_testdata.createcase();
    testCase.Account = testAcc;
    testCase.Status = 'Closed';
    testCase.RecordtypeId = rec1;
    testCase.ContactID = testcon.id; 
    insert testCase;
    
     
         ApexPages.StandardController testcontroller = new ApexPages.StandardController(testCase);
        autoCompleteController controller = new autoCompleteController(testcontroller);
        apexpages.currentpage().getparameters().put('id',testCase.id);
    
        
        
    
    }
    
    public static testMethod void autoCompleteController_test1() {
    id rec1 = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 

    id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 

    Account testAcc = accounttestdata.createaccount();
    testAcc.Name= 'TestAccount';
    testAcc.recordtypeId = rec;
    insert testAcc;
     // insert Contact  
    Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
    insert testcon;
    
    sObject[] s1 = autoCompleteController.findSObjects('Contact', 'TestContact', 'AccountId',testcon.id);

     
    Case testCase = SR_testdata.createcase();
    testCase.AccountId = testAcc.id;
    testCase.ContactId = testcon.id;
    testCase.Status = 'Closed';
    testCase.RecordtypeId = rec1;
    testCase.ContactID = testcon.id; 
    insert testCase;
    
     
         ApexPages.StandardController testcontroller = new ApexPages.StandardController(testCase);
        autoCompleteController controller = new autoCompleteController(testcontroller);
        apexpages.currentpage().getparameters().put('id',testCase.id);
    
        
        
    
    }
     public static testMethod void autoCompleteController_test2() {
    id rec1 = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 

    id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 

    Account testAcc = accounttestdata.createaccount();
    testAcc.Name= 'TestAccount';
    testAcc.recordtypeId = rec;
    insert testAcc;
     // insert Contact  
    Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
    insert testcon;
    
    // insert parent IP var loc
    SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id
    );
    insert objIP;
    
    sObject[] s1 = autoCompleteController.findSObjects('SVMXC__Installed_Product__c', 'H14072', ' ',objIP.id);

     
    Case testCase = SR_testdata.createcase();
    testCase.AccountId = testAcc.id;
    testCase.ContactId = testcon.id;
    testCase.SVMXC__Top_Level__c = objIP.id;
    testCase.Status = 'Closed';
    testCase.RecordtypeId = rec1;
    testCase.ContactID = testcon.id; 
    insert testCase;
    
     
         ApexPages.StandardController testcontroller = new ApexPages.StandardController(testCase);
        autoCompleteController controller = new autoCompleteController(testcontroller);
        //autoCompleteController cont = new autoCompleteController();
        apexpages.currentpage().getparameters().put('id',testCase.id);
    
        
        
    
    }
     public static testMethod void autoCompleteController_test3() {
    id rec1 = Schema.SObjectType.case.getRecordTypeInfosByName().get('HD/DISP').getRecordTypeId(); 

    id rec = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId(); 

    Account testAcc = accounttestdata.createaccount();
    testAcc.Name= 'TestAccount';
    testAcc.recordtypeId = rec;
    insert testAcc;
     // insert Contact  
    Contact testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = testAcc.Id, 
    MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '12452234');
    insert testcon;
    
    // insert parent IP var loc
    SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed',SVMXC__Company__c = testAcc.id
    );
    insert objIP;
    
    sObject[] s1 = autoCompleteController.findSObjects('', 'H14072', ' ',objIP.id);

     
    Case testCase = SR_testdata.createcase();
    testCase.AccountId = testAcc.id;
    testCase.ContactId = testcon.id;
    testCase.SVMXC__Top_Level__c = objIP.id;
    testCase.Status = 'Closed';
    testCase.RecordtypeId = rec1;
    testCase.ContactID = testcon.id;
    insert testCase;
    
     
         ApexPages.StandardController testcontroller = new ApexPages.StandardController(testCase);
       // autoCompleteController controller = new autoCompleteController(testcontroller);
        autoCompleteController cont = new autoCompleteController();
        apexpages.currentpage().getparameters().put('id',testCase.id);
    
        
        
    
    }
    }
global class batchCaseOwnerManagerEmailUpdate implements Database.Batchable<sObject> {

   global Database.QueryLocator start(Database.BatchableContext BC) {
   
      String emailToBeScrambled = 'select Case_Owner_Manager_Email__c from Case where Case_Owner_Manager_Email__c != null';
      return Database.getQueryLocator(emailToBeScrambled);
   
   }
   
   global void execute(Database.BatchableContext BC, List<Case> CasesUE) {
   
      for(Case CUE : CasesUE) {
         if(!CUE.Case_Owner_Manager_Email__c.contains('@example.com'))
            CUE.Case_Owner_Manager_Email__c = CUE.Case_Owner_Manager_Email__c.replace('@','=') + '@example.com';
      }
      
      update CasesUE;
   
   }
   
   global void finish(Database.BatchableContext BC) {
   
   }   
   
}
public Class check_CustomHistoryTrackingEnabler{
    private static List<Long_Text_History_Configuration__c> lstConfig;
    public static List<Long_Text_History_Configuration__c> runOnce(string objName){
    if(lstConfig == null ){
     lstConfig =  [select Field_Api_Name__c, Type__c from Long_Text_History_Configuration__c where Object_Api_Name__c = :objName];
     return lstConfig;
    }else{
        return lstConfig;
    }
    }
}
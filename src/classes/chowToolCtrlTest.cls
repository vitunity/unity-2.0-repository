@IsTest
public class chowToolCtrlTest {
    
    public static testMethod void chowToolCtrl(){
        List<Chow_Team_Member__c> lstteam = new List<Chow_Team_Member__c>();
        user u =testUtils.createUser();
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
         
        Sales_Order__c so = new Sales_Order__c();
        so.Name='Test So';
        so.Site_Partner__c=acc.id;        
        insert so;
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test';
        quote.Sales_Order__c=so.id;
        quote.Price_Group__c = 'Z1';
        quote.Quote_Type__c = 'Amendments';
        quote.BigMachines__Site__c =label.BigMachines_Site_Id;
       // quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.BigMachines__Status__c='Approved';
        insert Quote;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Account__c=acc.id;
        erp.Partner_Number__C='12234';
        insert erp;
        Chow_Team_Member__c chowTeamMember = testUtils.createChow_Team_Member('Accounting - Credit and Collections',u.id,'IND'); 
        chowTeamMember.Team_Member_Name__c=u.Name;
        insert chowTeamMember;
        lstteam.add(chowTeamMember);
        Chow_Team_Member__c chowTeamMember1 = testUtils.createChow_Team_Member('Sales Management - Software',u.id,'IND');
        insert chowTeamMember1;
        lstteam.add(chowTeamMember1);
        List<Chow_Team_Member__c>  teamMemberLst = [select id, name from Chow_Team_Member__c where Id NOT in: lstteam];
        Chow_Tool__c chow =testUtils.createChOwTool('Accounting - Credit and Collections', 'Bruce Wayne', 'bruce.wayne1c@zynga.com');
        chow.Sales_Order_Quote_Number_Unknown__c=true;
        chow.Chronological_Comments_Archive__c='Test Chronological Coments';
        chow.Comments__c='Test Comments';
        chow.Add_Extra_Participant__c='Bruce Wayne';
        chow.Request_Status__c='Denied';
        insert chow;
        test.startTest();
        ChowToolCtrl.saveChowTool(chow, erp.id, so.id, Quote.id,'Bruce Wayne', acc.id, lstteam);
        ChowToolCtrl.saveChowToolEditRecord(chow, erp.id, so.id, Quote.id,'Bruce Wayne', acc.id, lstteam, 'Routine Request', 'Pending', 'p1', 'Administrative Change');
        ChowToolCtrl.getchOwrecords(chow.id);
        ChowToolCtrl.getAccountFieldsInfo(acc.id);
        ChowToolCtrl.getPartnerAddress(erp.id);
        ChowToolCtrl.getSalesOrderInfoWrap(so.id);
        ChowToolCtrl.getQuoteInfoWrap(Quote.id);
        ChowToolCtrl.getActiveTeamPicklistOptions('IND');
        ChowToolCtrl.picklistEntry p=new ChowToolCtrl.picklistEntry('1','1');
        
        List<ChowToolCtrl.picklistEntry> lsttask=ChowToolCtrl.getTaskAssigneeList('Accounting - Credit and Collections');
        ChowToolCtrl.getDefaultTeam('IND');
        ChowToolCtrl.getselectOptions(chow, 'Request_Status__c');
        ChOW_PartnerLookupCtrl.initializeLookupPartners('6051553', 'searchText');
        ChOW_PartnerLookupCtrl chOWpart = new ChOW_PartnerLookupCtrl();
        chOWpart.paginatorFilter='searchText';
        chOWpart.searchText='searchText';
        
        reUsableMultiSelectLookupCtrl.fetchLookUpValues('Bruce', 'ChOW', teamMemberLst);
        ChowToolCtrl.getAccount('Test-Account');
        ChowToolCtrl.getPartnerAddress(erp.id);
        ChowToolCtrl.getPartnerDetails(erp.Partner_Number__C);
        test.stopTest();
    }
    public static testMethod void chowToolCtrlNeg(){
        
        user u =testUtils.createUser();
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name='Test So-01';
        so.Site_Partner__c=acc.id;        
        insert so;
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test-01';
        quote.Sales_Order__c=so.id;
        quote.Price_Group__c = 'Z1';
        quote.Quote_Type__c = 'Standard';
        quote.BigMachines__Site__c =label.BigMachines_Site_Id;
        //quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.BigMachines__Status__c='Approved';
        insert Quote;
         List<Chow_Team_Member__c> lstteam = new List<Chow_Team_Member__c>();
         Chow_Team_Member__c chowTeamMember = testUtils.createChow_Team_Member('Accounting - Credit and Collections',u.id,'IND');       
        insert chowTeamMember;
        lstteam.add(chowTeamMember);
        
        ChowToolCtrl.getSalesOrderInfoWrap(so.id);
        ChowToolCtrl.getQuoteInfoWrap(Quote.id);
         ChowToolCtrl.getActiveTeamPicklistOptions('');
        reUsableMultiSelectLookupCtrl.fetchLookUpValues('Accounting - Credit and Collections', 'Chow_Team_Member__c', lstteam);
       
                
    }
    Public static testMethod void partnerLookUpctrl(){
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Account__c=acc.id;
        erp.Name='Test-ERP';
        erp.Street__c='Test Street';
        erp.City__c='test City';
        erp.State_Province_Code__c='123';
        erp.Zipcode_Postal_Code__c='122345';
        insert erp;
        
        ERP_Partner_Association__c  part = new ERP_Partner_Association__c();
        part.ERP_Partner__c=erp.id;
        part.Customer_Account__c=acc.id;
        part.Sales_Org__c='5860';
        part.ERP_Partner_Number__c='10000132';
        part.Partner_Function__c='Z1=Site Partner';
        part.ERP_Customer_Number__c='10000132';
        insert part;
        
        ChOW_PartnerLookupCtrl.initializeLookupPartners('6051553', 'searchText');
        ChOW_PartnerLookupCtrl chOWpart = new ChOW_PartnerLookupCtrl();
        chOWpart.soldToNumber='6051553';  
        chOWpart.pageNumber=1;
        chOWpart.paginatorFilter='Quote'; 
        chOWpart.searchText='Quote1'; 
        chOWpart.totalPages=10;
        chOWpart.getPartnerType();
        chOWpart.getErpPartnerAssociations();
        chOWpart.getFirst();
        chOWpart.getLast();
        chOWpart.getNext();
        chOWpart.getPrevious();
        chOWpart.getERPAssociationsBYSOSL('Test');
        
    }
    
    Public static testMethod void partnerLookUpctrl2(){
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        ERP_Partner__c erp = new ERP_Partner__c();
        erp.Account__c=acc.id;
        erp.Name='Test-ERP1';
        erp.Street__c='Test Street1';
        erp.City__c='test City1';
        erp.State_Province_Code__c='124';
        erp.Zipcode_Postal_Code__c='122346';
        insert erp;
        
        ERP_Partner_Association__c  part = new ERP_Partner_Association__c();
        part.ERP_Partner__c=erp.id;
        part.Customer_Account__c=acc.id;
        part.Sales_Org__c='5861';
        part.ERP_Partner_Number__c='10000132';
        part.Partner_Function__c='Z1=Site Partner';
         part.ERP_Customer_Number__c='10000132';
        insert part;
        
        ChOW_PartnerLookupCtrl.initializeLookupPartners('6051553', 'searchText');
        ChOW_PartnerLookupCtrl chOWpart = new ChOW_PartnerLookupCtrl();
         chOWpart.soldToNumber='6051553'; 
        chOWpart.totalPages=10;
        chOWpart.pageNumber=1;
        chOWpart.hasNext=false;
        chOWpart.hasPrevious=false;
        chOWpart.getPartnerType();
        chOWpart.getErpPartnerAssociations();
        chOWpart.getFirst();
        chOWpart.getLast();
        chOWpart.getNext();
        chOWpart.getPrevious();
        chOWpart.getERPAssociationsBYSOSL('Test');
        
    }
    
    public static testmethod void lookupQtpartNoCtrl(){
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name='Test So-01';
        so.Site_Partner__c=acc.id;        
        insert so;
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test-01';
        quote.Sales_Order__c=so.id;
        quote.Price_Group__c = 'Z1';
        quote.Quote_Type__c = 'Standard';
         quote.BigMachines__Site__c =label.BigMachines_Site_Id;
       // quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.BigMachines__Status__c='Approved';
        insert Quote;
        
        BigMachines__Quote_Product__c  BQP = new BigMachines__Quote_Product__c();
        BQP.BigMachines__Quote__c=Quote.id;
        BQP.BigMachines__Sales_Price__c=473;
        BQP.BigMachines__Quantity__c=1;
        BQP.BigMachines__Description__c='Quote';
        BQP.Grp__c=1;
        BQP.Header__c=true;
        BQP.Name='FSC002100000';
        insert BQP;
        
        Test.startTest();
       ChOW_LookupQtPartNosCtrl_New.initializeLookupParts('Quote.id', 'quote-1', 'Quote');
        ChOW_LookupQtPartNosCtrl_New lookupQuote = new ChOW_LookupQtPartNosCtrl_New();
        lookupQuote.pageNumber=1;
        lookupQuote.paginatorFilter='Quote'; 
        lookupQuote.searchText='Quote1'; 
        lookupQuote.totalPages=10;
        lookupQuote.chowToolQtId=Quote.id;
        lookupQuote.getQuoteProducts();
        lookupQuote.getQuoteProductsBYSOSL('Quote');
        lookupQuote.getQuoteProductsBYSOQL();
        lookupQuote.getFirst();
        lookupQuote.getLast();
        lookupQuote.getNext();
        lookupQuote.getPrevious();
        lookupQuote.hasNext=true;
        lookupQuote.hasPrevious=true;        
        ChOW_LookupQtPartNosCtrl_New.getParts(Quote.id, 'test', 1, 20);
        ChOW_LookupQtPartNosCtrl_New.totalSizeCount=20;
        Test.stopTest();
    }
    
    public static testmethod void lookupQtpartNoCtrl2(){
        Account acc=testUtils.getAccount();
        acc.Name='Test-Account';
        insert acc;
        
        Sales_Order__c so = new Sales_Order__c();
        so.Name='Test So-01';
        so.Site_Partner__c=acc.id;        
        insert so;
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test-01';
        quote.Sales_Order__c=so.id;
        quote.Price_Group__c = 'Z1';
        quote.Quote_Type__c = 'Standard';
         quote.BigMachines__Site__c =label.BigMachines_Site_Id;
       // quote.BigMachines__Site__c = 'a31E0000000GL68IAG';
        quote.BigMachines__Status__c='Approved';
        insert Quote;
        
        BigMachines__Quote_Product__c  BQP = new BigMachines__Quote_Product__c();
        BQP.BigMachines__Quote__c=Quote.id;
        BQP.BigMachines__Sales_Price__c=473;
        BQP.BigMachines__Quantity__c=1;
        BQP.BigMachines__Description__c='Quote';
        BQP.Grp__c=1;
        BQP.Header__c=true;
        BQP.Name='FSC002100000';
        insert BQP;
              
        Test.startTest();
        ChOW_LookupQtPartNosCtrl_New lookupQuote = new ChOW_LookupQtPartNosCtrl_New();
        lookupQuote.chowToolQtId=Quote.id;
        lookupQuote.getQuoteProducts();
        lookupQuote.getQuoteProductsBYSOSL('Quote');
        lookupQuote.getQuoteProductsBYSOQL();
        lookupQuote.totalPages=10;
        lookupQuote.pageNumber=1;
        lookupQuote.hasNext=false;
        lookupQuote.hasPrevious=true;
        lookupQuote.getFirst();
        lookupQuote.getLast();
        lookupQuote.getNext();
        lookupQuote.getPrevious();
        ChOW_LookupQtPartNosCtrl_New.getParts(Quote.id, '', 2, 20);
        Test.stopTest();
    }
    public static testMethod void  reUsableMultiSelectLookupCtrl(){
        user u =testUtils.createUser();
        Chow_Tool__c chow =testUtils.createChOwTool('Accounting - Credit and Collections', 'Bruce Wayne', 'bruce.wayne1c@zynga.com');
        chow.Sales_Order_Quote_Number_Unknown__c=true;
        chow.Chronological_Comments_Archive__c='Test Chronological Coments';
        chow.Comments__c='Test Comments';
        chow.Add_Extra_Participant__c='Bruce Wayne';
        chow.Request_Status__c='Denied';
        insert chow;
        
        List<Chow_Team_Member__c> lstteam = new List<Chow_Team_Member__c>();
        Chow_Team_Member__c chowTeamMember = testUtils.createChow_Team_Member('Accounting - Credit and Collections',u.id,'IND');       
        insert chowTeamMember;
        lstteam.add(chowTeamMember);
        
        reUsableMultiSelectLookupCtrl.fetchExtParticipantsObj(chow.id);
    }
}
@isTest
public class competitorBeforeInsertTest {
	
    public static List<Id> recordTypeId = new List<String>();
    public static Map<String, String> vendorMap = new Map<String, String>();
    public static Map<String, String> ModelMap = new Map<String, String>();
    
    static {
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('BA').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('DS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('IMS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OIS').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OPCD').getRecordTypeId());
        recordTypeId.add(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPS').getRecordTypeId());        
        
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('BA').getRecordTypeId(), 'ACE');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('DS').getRecordTypeId(), 'AlignRT');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('IMS').getRecordTypeId(), 'Allscripts');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OIS').getRecordTypeId(), 'Artiste');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OPCD').getRecordTypeId(), 'Atlantis');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPS').getRecordTypeId(), 'Axesse');
        ModelMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPSC').getRecordTypeId(), 'Axxent');
        
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('BA').getRecordTypeId(), 'Accuray');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('DS').getRecordTypeId(), 'AGAT');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('IMS').getRecordTypeId(), 'GammaStar');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OIS').getRecordTypeId(), 'GE');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('OPCD').getRecordTypeId(), 'GeniousDoc');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPS').getRecordTypeId(), 'Hitachi');
        vendorMap.put(Schema.SObjectType.Competitor__c.getRecordTypeInfosByName().get('TPSC').getRecordTypeId(), 'IBA');
    }
    
    public static testMethod void triggerTest() {
        List<Competitor__c> compList = new List<Competitor__c>();

        Competitor__c comp;
        for(Id i : recordTypeId) {
            comp = new Competitor__c();
            comp.RecordTypeId = i;
            comp.Vendor__c = vendorMap.get(i);
            comp.Model__c = ModelMap.get(i);
            compList.add(comp);
        }
        insert compList;
                
    }
    
}
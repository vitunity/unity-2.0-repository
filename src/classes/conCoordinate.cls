public class conCoordinate 
{
   // public AddInteractiveMapPoints__c a{get;set;}
    Apexpages.StandardController con;
    public Interactive_Maps_Landmarks__c rec;
    public AddInteractiveMapPoints__c coordinate;
    public conCoordinate(Apexpages.StandardController c)
    {
       // a=new AddInteractiveMapPoints__c();
        con = c;
        coordinate = (AddInteractiveMapPoints__c)con.getRecord();
    }
    public PageReference save() 
    {
        con.save();
        AddInteractiveMapPoints__c rec = (AddInteractiveMapPoints__c)con.getRecord();
   Pagereference pr = new PageReference('/'+coordinate.Interactive_Maps_Landmarks__c );
//   pr.getParameters().put('ID',rec.Interactive_Maps_Landmarks__c);
  //Apexpages.currentPage();
        return pr; 
        //return pr;
    }
    public string getImgURL()
    {
//        rec = [Select ID from StoreXYCoardinates__c where Interactive_Maps_Landmarks__c =: coordinate.Interactive_Maps_Landmarks__c];
        Attachment img = [Select ID from Attachment where ParentID=:coordinate.Interactive_Maps_Landmarks__c limit 1];
    //string str = 'background:url(\'https://varian--sfvud--c.cs8.content.force.com/servlet/servlet.FileDownload?file='+img.ID+'\') no-repeat;';
    string str = '/servlet/servlet.FileDownload?file='+img.ID;    
    return str;
    } 
}
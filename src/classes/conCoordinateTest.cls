@isTest
public class conCoordinateTest {
	static testMethod void unitTest1()
    {
        Interactive_Maps_Landmarks__c intMaps = new Interactive_Maps_Landmarks__c(Name='Test');
		insert intMaps;
		
		Attachment att = new Attachment(Name='test', ParentId=intMaps.ID, body=EncodingUtil.base64Decode('Test'));
		insert att;	
		
		AddInteractiveMapPoints__c addInt = new AddInteractiveMapPoints__c(Name='Test',Interactive_Maps_Landmarks__c=intMaps.Id);
		insert addInt;
		
		ApexPages.StandardController sc = new ApexPages.StandardController(addInt);
		
		conCoordinate  coo = new conCoordinate(sc);
		coo.save();
		coo.getImgURL();
    }
}
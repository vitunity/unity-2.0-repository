public class conslackID {
    
   public static void populateslack(List<Contact> conlist,Map<id,Contact> oldmaplst){
        map<string,string> mapEmailAddress = new map<string,string>();
        for(Contact oContact:conlist){
            if(oContact.Email!=null && (oldmaplst==null ||(oldmaplst!=null && oContact.Email!=oldmaplst.get(oContact.Id).Email))){
                string sEmail = oContact.Email;
                sEmail = sEmail.toLowerCase();
                mapEmailAddress.put(sEmail,'');
            }
        }
        if(mapEmailAddress.size()>0){
            usrname(mapEmailAddress);
        }
    }
    @future(callout=true)
    public static void usrname(map<string,string> mapEmailWiseSlackId){
        boolean bFound = false;
          String authtkn=label.Tpaas_Bearer_token;
        String endpoint=label.Tpaas_getting_User_ID;
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization','Bearer '+authtkn);
        req.setHeader('MethodRequested', 'users.list');
        try{
            
            string sBody;
            if(!test.isRunningTest()){
                res = http.send(req);
            sBody = res.getBody();
            }
            else{
                sBody = '{"ok":true,"members":[{"id":"UAFLSSBME","team_id":"TAG565Y5A","name":"shravyach13","deleted":false,"color":"9f69e7","real_name":"shravya channama","tz":"America New_York","tz_label":"Eastern Daylight Time","tz_offset":-14400,"profile":{"title":"","phone":"","skype":"","real_name":"shravya channama","real_name_normalized":"shravya channama","display_name":"shravya","display_name_normalized":"shravya","status_text":"","status_emoji":"","status_expiration":0,"avatar_hash":"gcba0cde6889","email":"shravyach13@gmail.com","status_text_canonical":"","team":"TAG565Y5A"},"is_admin":true,"is_owner":true,"is_primary_owner":true,"is_restricted":false,"is_ultra_restricted":false,"is_bot":false,"is_app_user":false,"updated":1525183390,"has_2fa":false}],"cache_ts":1535029574}';
                
            }
            system.debug('sBody@@@@@---- ' +sBody);
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(sBody);
            if(results!=null){
                if(results.containsKey('members')){
                    List<object> lstMembers= (List<Object>)results.get('members');
                    for (Object oMember: lstMembers){
                        Map<String, Object> mapMemberDetail = (Map<String, Object>)oMember;
                        if(mapMemberDetail.containsKey('id')){
                            String sSlackID = string.valueOf(mapMemberDetail.get('id'));
                            if(mapMemberDetail.containsKey('profile')){
                                Map<String, Object> mapProfileDetails = (Map<String, Object>)mapMemberDetail.get('profile');
                                if(mapProfileDetails.containsKey('email')){
                                    String sEmail=String.valueOf(mapProfileDetails.get('email'));
                                    if(sEmail!=null){
                                        sEmail = sEmail.toLowerCase();
                                    }
                                    if(mapEmailWiseSlackId.containsKey(sEmail)){
                                        mapEmailWiseSlackId.put(sEmail,sSlackID);
                                        bFound = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(bFound){
                List<Contact> lstContacts= [Select id,SlackID__c,Email From contact Where Email in :mapEmailWiseSlackId.keySet()];
                for(Contact oContact:lstContacts){
                    String sEmail=oContact.Email;
                    sEmail = sEmail.toLowerCase();
                    if(mapEmailWiseSlackId.containsKey(sEmail)){
                        oContact.SlackID__c = mapEmailWiseSlackId.get(sEmail);
                    }
                }
                update lstContacts;
            }
            
        }
        catch(Exception e)
        {
            System.debug('Callout error: ' + e);
            System.debug(res.toString());
        }
    }
}
@isTest
private with sharing class constantFlagController_Test {
	
	public static testmethod void wrkDetailFlag_Test() {
		Boolean expectedFlag = constantFlagController.wrkDetailFlag;
		system.assertEquals(expectedFlag, false);
	}
}
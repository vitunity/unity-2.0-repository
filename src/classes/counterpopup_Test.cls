@isTest
public class counterpopup_Test
{
    public static testMethod void test_counterpopup()
    {
    //Creating account
      Account testAcc = AccountTestData.createAccount();
        testAcc.AccountNumber = 'AA787799';
        insert testAcc;
    //Creating product
     Product2 objProd2=new Product2();
        objProd2.Name = 'technical Test Product';
        objProd2.Material_Group__c= 'H:TRNING';
        objProd2.Budget_Hrs__c =10.0;
        objProd2.Credit_s_Required__c=10;
        objProd2.UOM__c='EA';
        insert objProd2;
         //Create Sales Order
        Sales_Order__c objSalesOrder=new Sales_Order__c();
        objSalesOrder.Name='001';
        Insert objSalesOrder;   
    //Create Sales Order Item
        list<Sales_Order_Item__c> lstSOI=new list<Sales_Order_Item__c>();
        Sales_Order_Item__c objSOI1=new Sales_Order_Item__c();
        objSOI1.Start_Date__c= System.Today();
        objSOI1.End_Date__c  = System.Today().AddDays(10);
        objSOI1.Sales_Order__c =objSalesOrder.id;
       // objSOI1.Site_Partner__c = testAcc.id;
        objSOI1.Product__c = objProd2.Id;
        objSOI1.Quantity__c = 10;
        lstSOI.add(objSOI1);
        Insert lstSOI;
      //Creating Counter Detail:
         SVMXC__Counter_Details__c objCTD = new SVMXC__Counter_Details__c();
         objCTD.Account__c = testAcc.id;
         objCTD.Sales_Order_Item__c = lstSOI[0].id;
         objCTD.Units__c ='Hours';
         objCTD.Start_Date__c =System.today().addDays(-20);
         objCTD.End_Date__c =System.Today().addDays(-10);
         objCTD.SVMXC__Counter_Reading__c=10;
         objCTD.SVMXC__Counters_Covered__c=10;  
         objCTD.SVMXC__Active__c = true;
         Insert objCTD;
         Apexpages.Currentpage().getparameters().put('id',objCTD.id);
          ApexPages.StandardController sc = new ApexPages.standardController(objCTD);
         counterpopupcontr obj =new counterpopupcontr (sc ); 
         obj.UpdateCounter();
         obj.open_win();
         
    }


}
public with sharing class counterpopupcontr 
{
/***********************************************************************
* Author         : Mohit Sharma
* Functionality  : Show Counter Detail:Reading-Coverage/Update Internal Comment
* Description    : Show Counter Detail:Reading-Coverage/Update Internal Comment.
* This Apex Class is being referenced in VF Page - counterpopup.
* 
************************************************************************/
    public list<wrpCounter> lstWrpCDT{get;set;}     // List Wrapper to hold selected/deselected Counter from UI
    public SVMXC__Counter_Details__c counterdetcov = new SVMXC__Counter_Details__c();   // counter detail list vf
    
    public counterpopupcontr(ApexPages.StandardController controller) // Constrct
    {
        lstWrpCDT = new list<wrpCounter>();
        counterpopupcontr ();   //Calling method to get counter Detail
    }



    public void counterpopupcontr () // Method to show counter on UI
    {
        if(ApexPages.currentPage().getParameters().get('id') != null)
        {
            Id countid =apexpages.currentpage().getparameters().get('id'); //Getting counter Detail record Id 
            counterdetcov =[Select Name,SVMXC__Counter_Name__c, Internal_Comments__c,Sales_Order__c, Sales_Order_Item__c,
                            Account__c,SVMXC__Coverage_Limit__c,
                           SVMXC__Service_Maintenance_Contract__c,Start_Date__c,
                           (
                           Select Name,SVMXC__Counter_Name__c, Internal_Comments__c,Sales_Order__c, Sales_Order_Item__c,Account__c,SVMXC__Coverage_Limit__c,
                           SVMXC__Service_Maintenance_Contract__c,Start_Date__c, End_Date__c, 
                           RecordTypeid,SVMXC__Product__c,recordtype.developername,SVMXC__Counter_Reading__c,
                           SVMXC__Counters_Covered__c,SVMXC__Counter_Type__c
                           From SVMXC__Counter_Details__r 
                           ),
                           End_Date__c, RecordTypeid,SVMXC__Product__c,recordtype.developername,
                           SVMXC__Counter_Reading__c,SVMXC__Counters_Covered__c,SVMXC__Counter_Type__c
                           From SVMXC__Counter_Details__c 
                           Where id = :countid];
                
               
                lstWrpCDT.add(new wrpCounter(false,counterdetcov)); // Adding CDT Record to List
               // lstWrpCDT.add(new wrpCounter(false,counterdetcov.SVMXC__Counter_Details__r[0])); // Adding CDT Record to List
                
        }
    }
    //****************InnerClass*********************************//
    
    public class wrpCounter // Inner Class Select-Deselect
    {
        public boolean isSelected{get;set;}  
        public SVMXC__Counter_Details__c ObjCounter{get;set;}
        public wrpCounter(boolean select1,SVMXC__Counter_Details__c objCDT)
        {
            isSelected = select1;
            ObjCounter = objCDT;
        }
    }

    public pageReference UpdateCounter() // Method to Update Internal Comment on Counter Detail
    {
        list<SVMXC__Counter_Details__c> lstWrpCDT_temp=new list<SVMXC__Counter_Details__c> ();
        for(wrpCounter wrp: lstWrpCDT)
        {
           // if(wrp.isSelected == true)
            if(wrp.ObjCounter.recordtype.developerName == 'Coverage')
            {
                lstWrpCDT_temp.add(wrp.ObjCounter);
            }

        }
        if(lstWrpCDT_temp.size() >0)
        {
            Update lstWrpCDT_temp;
        }
        return null;
    }

    public pageReference open_win()
    {
        String contid = System.currentPagereference().getParameters().get('idselected');
        pageReference pg = new pageReference(label.SR_InstanceURL+'apex/counterpopup?id='+contid );
        return pg;
    }
}
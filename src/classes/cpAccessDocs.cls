/*************************************************************************\
    @ Author        : Mohit Bansal    
    @ Date          : 21-May-2013
    @ Description   : An Apex class to display all the Access Docs for Truebeam and Clinac for Monte Carlo.
    @ Last Modified By  :   Mohit Bansal
    @ Last Modified On  :   22-May-2013
    @ Last Modified Reason  : Code Completed  
****************************************************************************/


public class cpAccessDocs 
{
    public map<String, List<Monte_Carlo__c>> map_Title_Mc{get;set;}
    public Map<ID, List<ContentVersion>> map_Mc_ContentVer{get;set;}
    public List<MyWrapper> lTitle_ContentVer{get;set;}
    public String pageName{get;set;}   
    public Boolean ShowDialog{get;set;}
    public string Usrname{get;set;}
    public boolean isElectronFile{get; set;}
    public boolean isRelCont{get; set;}
    public boolean isPhotonFile{get; set;}  
    public string shows{get;set;}
    public String pagetype{get;set;} 
    public String FileType{get;set;} 
    public PageReference ShowDialogVal()
    {
        ShowDialog= true;
        return null;
    }
     public PageReference refresh()
    { 
       
        pagetype = System.currentPageReference().getParameters().get('type');
       if(pagetype=='access')
        {
            PageReference pg1 = new PageReference('/apex/cpAccessDocs?text=' + pageName);
            pg1.setRedirect(true);
            return pg1;
        }
        return null;
    }
    public cpAccessDocs()
    {    
        AccessDocs();
    }
    public void AccessDocs()
    {
    User usr = [Select alias,contactid from user where id =: Userinfo.getUserId() limit 1];
        shows = 'none';
        if(usr.contactid == null){
          Usrname = usr.alias;
          shows = 'block';        
        }
        pageName = System.currentPageReference().getParameters().get('text');
        pagetype = System.currentPageReference().getParameters().get('type');
        /*FileType= ApexPages.currentPage().getParameters().get('TypeFile');
       if(FileType=='' || FileType==null)
        {
        FileType='Photon';
        }*/
      
      /*  if(FileType=='Electron')
        {
        isRelCont=False;
        isElectronFile=True;
        isPhotonFile=False;
        }
        else if(FileType=='Related Content')
        {
       isRelCont=True;
        isElectronFile=False;
        isPhotonFile=False;
        }
        else
        {
        isRelCont=False;
        isElectronFile=False;
        isPhotonFile=True;
        }*/
        if(pageName=='TrueBeam')
        {
        if(ApexPages.currentPage().getParameters().get('cid')=='ElectronFile')
        {
        isRelCont=False;
        isElectronFile=True;
        isPhotonFile=False;
        FileType='Electron';
        
        }
        else if(ApexPages.currentPage().getParameters().get('cid')=='RelCont')
        {
        isRelCont=True;
        isElectronFile=False;
        isPhotonFile=False;
        FileType='Related Content';
        }
        else
        {
        isRelCont=False;
        isElectronFile=False;
        isPhotonFile=True;
        FileType='Photon';
        }
        }
        
        map_Title_Mc = new Map<String, List<Monte_Carlo__c>>();
        List<Monte_Carlo__c> lMonteCarlo = [SELECT Description__c,Id,Name,Title__c,Type__c FROM Monte_Carlo__c WHERE Type__c = :pageName];
        for(Monte_Carlo__c c : lMonteCarlo)
        {
            if(map_Title_Mc.containsKey(c.Title__c))
            {
                List<Monte_carlo__c> lMc = map_Title_Mc.get(c.Title__c);
                lMc.add(c);
                map_Title_Mc.remove(c.Title__c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
            else
            {
                List<Monte_carlo__c> lMc = new List<Monte_Carlo__c>();
                lMc.add(c);
                map_Title_Mc.put(c.Title__c, lMc);
            }
        
        }
         List<ContentVersion> lContentVersion;
        if(pageName=='TrueBeam')
        {  
        lContentVersion= [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber FROM ContentVersion where Monte_Carlo__c != null and File_Type__c=:FileType  order by VersionNumber Desc];
        }
        else
        {
         lContentVersion = [SELECT ContentDocumentId,File_Type__c,Description,ContentSize,CreatedDate,Id,Monte_Carlo__c,Title,VersionNumber FROM ContentVersion where Monte_Carlo__c != null   order by VersionNumber Desc];
        }
        set<string> stContentdocid=new set<string>();
        
        
        map_Mc_ContentVer = new Map<ID, List<ContentVersion>>();
       
        for(ContentVersion  c : lContentVersion)
        {
            
            If(!stContentdocid.contains(c.ContentDocumentId))
            {
                if(!map_Mc_ContentVer.containsKey(c.Monte_Carlo__c))
                {
             
                    List<ContentVersion> lMc = new List<ContentVersion>();
                    lMc.add(c);
                    map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                    System.debug('LMC1-->'+lMc);
                }
                else
                {  
                List<ContentVersion> lMc = map_Mc_ContentVer.get(c.Monte_Carlo__c);
                lMc.add(c);
                map_Mc_ContentVer.remove(c.Monte_Carlo__c);
                map_Mc_ContentVer.put(c.Monte_Carlo__c, lMc);
                System.debug('LMC-->'+lMc);
                    
                }
                
                stContentdocid.add(c.ContentDocumentId);
            }
            
            
            
        
        }
        
        Set <String> sTitleKeys = new Set<String>();
        sTitleKeys = map_Title_Mc.keySet();
        lTitle_ContentVer = new List<MyWrapper>();
        for(String s : sTitleKeys)
        {
            List<ContentVersion> lContent = new List<ContentVersion>();
            List<Monte_Carlo__c> lMonte = map_Title_Mc.get(s);
            if(lMonte != null)
            {
                for(Monte_Carlo__c mc : lMonte)
                {
                    List<ContentVersion> lc = new List<Contentversion>();
                    lc = map_Mc_ContentVer.get(mc.id);
                    system.debug('Valid--->'+lc );
                    if(lc != null)
                        lContent.addAll(lc);
                }  
            }
            lTitle_ContentVer.add(new MyWrapper(s,lContent));
         }    
        System.debug('Values--->'+lTitle_ContentVer);
     }
     public Class MyWrapper
     {
         public String TitleVal{get;set;}
         public List<ContentVersion> lCont{get;set;}
         
         public MyWrapper(String s, List<ContentVersion> ls)
         {
             TitleVal = s;
             lcont = ls;
         }
         
     }   
     
     public boolean displayPopup {get; set;}     
    
     public void closePopup() {        
        displayPopup = false;    
     }     
     public void showPopup() {        
        displayPopup = true;    
     }
     

}
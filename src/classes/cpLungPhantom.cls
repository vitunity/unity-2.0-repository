/*************************************************************************\
    @ Author        : Mohit Bansal    
    @ Date          : 27-May-2013
    @ Description   : An Apex class to display Phantom and Voucher IDs for RPC Lung Phantom.
    @ Last Modified By  :   Mohit Bansal
    @ Last Modified On  :   28-May-2013
    @ Last Modified Reason  : Code Completed  
****************************************************************************/

public class cpLungPhantom {

    public String datename { get; set; }
//This Method is used to send the mail after confirmation in popup and perform the DML operation on Lung_Phantom__c object.
    public void confirmPopup() 
    {   
        String VoucherID = ApexPages.currentPage().getParameters().get('voccidparam');
        displayPopup = false; 
        Lung_Phantom__c lP = new Lung_Phantom__c();
       // Set<ID> sIDs = new Set<ID>();
       //String VoucherID = ApexPages.currentPage().getParameters().get('voccidparam');
         System.debug('VochreId'+VoucherID);
        for(Lung_Phantom__c l : lLungPhantRecs)
        {
        System.debug('VochreId'+VoucherID);
            if(l.id == VoucherID)
            {
                l.Date_Redeemed__c = System.now().date();
                l.Redeemed_By__c = c.name;    
                lp = l;
               
            }
           // sIDs.add(l.id);
        }
        
        if(lLungPhantRecs != null)
        update lLungPhantRecs;
      //  getlLPHRec();
      //  lLungPhantRecs = [Select id,Voucher_ID__c,Date_Redeemed__c,Installed_Product__r.name,Date_Received__c,Redeemed_By__c,Product_Name__c,Installed_Product__c,Expiration_Date_Ends__c, name from Lung_Phantom__c where id in :sids order by name desc];
      
        
        // Send Email
        
      /*      Messaging.SingleEmailMessage mailHandler = new Messaging.SingleEmailMessage();

            String[] emailRecipient = new String[]{'mdadl@mdanderson.org'};
            
            String[] emailRecipientCC = new String[]{'' + c.email};            
            // set the recipient
                      
            mailHandler.setToAddresses(emailRecipient);
            mailHandler.setCCAddresses(emailRecipientCC );
            
            //set the subject
            mailHandler.setSubject('RPC Lung Phantom Voucher Redemption');
            
            //set the template ID
            //mailHandler.setTemplateId('00X200000015XFL');
            
            String Body = '';
            body = 'Dear ' + c.FirstName+' '+c.LastName;
            body = body + '<br/><br/>Thank you for redeeming your RPC Lung Phantom Voucher.<br/><br/>The MD Anderson Dosimetry Lab will be shipping the RPC Lung Phantom to the following address:</br></br>'+c.Account.name+'</br>Attn:'+ c.FirstName+' and '+c.LastName+'</br>'+c.MailingStreet+'</br>' + c.MailingCity + ', ' + c.MailingState+', '+c.MailingPostalCode;
            body = body + '<br/><br/>' + 'Thank you,';
            body = body + '<br/><br/>' + 'Varian Medical Systems';
            
            mailHandler.setHtmlBody(body);
            
            try
            {
            Messaging.sendEmail(new Messaging.Email[] { mailHandler });
            
            }
            catch(EmailException e)
            {
            System.debug(e.getMessage());
            
            }*/
      /*   PageReference pg = new PageReference('http://rpc.mdanderson.org/mdadl/Varian_Phantom_Request.aspx');
                pg.setRedirect(true);
                return pg;         */     
            
        // Email code ends
            
      //  return null;
    }
    
    
    

    public PageReference SaveRec()
    {
        if(lLungPhantRecs != null)
        {
            update lLungPhantRecs;
        }
        getlLPHRec();
        
        
        
        return null;
    }

    //public String VoucherID{get;set;}
    
    public String getLiEDate1() {
        return null;
    }


    public String getLiEDate() {
        return null;
    }
    //This Constructor run to set the user information for current context
    public Contact c{get;set;}
    public String usrContId{get;set;} 
    public boolean IsSave{get;set;} 
    public cpLungPhantom()
    {
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];    
      //  User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        c = new Contact();
        if(usrContId != null)
        {
            c = [select Is_MDADL__c,name,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
        }
      }         
    //This Method is used to redirect the page if condition is true.
    public PageReference MDADLContact()
    {
        if((usrContId != null) && (c.Is_MDADL__c == true))
        {
                PageReference pg = new PageReference('/apex/cpMDADLLungPhantum');
                pg.setRedirect(true);
                return pg;
          
        }    
        else
            return null;
    }
    
    
    //This Method is used to list the data from Lung_Phantom__c object 
    public list<Lung_Phantom__c> lLungPhantRecs{get;set;}
    
    public List<Lung_Phantom__c> getlLPHRec()
    {
        lLungPhantRecs = new List<Lung_Phantom__c>();
        SYSTEM.DEBUG('testcontact--->'+c.id);
        if(usrContId != null)
        {
        lLungPhantRecs = [Select id,Voucher_ID__c,Date_Redeemed__c,Date_Exposed_Phantom_Received_by_MDADL__c,Date_Results_Reported_by_MDADL__c,Installed_Product__r.name,Date_Received__c,Redeemed_By__c,Product_Name__c,Installed_Product__c,Serial_Lot_Number__c,Expiration_Date_Ends__c, name from Lung_Phantom__c where contact__c = :c.id order by name desc];
        if(lLungPhantRecs.size()>0)
        {
        IF(lLungPhantRecs[0].Date_Redeemed__c!=NULL)
        {
        IsSave=true;
        }
        }
        else
        {
        IsSave=false;
        }
        }
        return lLungPhantRecs ;
    }
    
    public boolean displayPopup {get; set;}     
    
     public void closePopup() {        
        displayPopup = false;    
     }     
     public void showPopup() {        
        displayPopup = true;    
     }

}
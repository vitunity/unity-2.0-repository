/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 17-June-2013
    @ Description   :  Test class used for code coverage of cpLungPhantom,cpMDADLLungPhantom classes and IdentifyContacts Trigger.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@IsTest

Class cpLungPhantomTest
{
    
    // Test Method for initiating cpLungPhantom class
    
    private static testmethod void cpLungPhantomTestMethod()
    {
        Test.StartTest();
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
        
            Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
            
            User u;
            Account a;  
            
            a = new Account(name='test2',BillingPostalCode ='12235',BillingCity='California',BillingCountry='Test',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  ); 
            insert a; 
            
            Contact con=new Contact(Lastname='test',RAQA_Contact__c=true,FirstName='Singh',Email='kumarxxx.amit667@gmail.com', AccountId=a.Id,MailingCountry ='test', MailingState='testState' ); 
            insert con;
            
            u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduserxxx@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user1xxxxx@testorg.com'/*,UserRoleid=r.id*/); 
            
            insert u; // Inserting Portal User
            SVMXC__Installed_Product__c ip=new SVMXC__Installed_Product__c(SVMXC__Company__c=a.id );
        insert ip;
        
        //SVMXC__Installed_Product__c ip = [Select id, name from SVMXC__Installed_Product__c limit 1];
        
        List<Lung_Phantom__c> lstOfLP=new List<Lung_Phantom__c>();
        Lung_Phantom__c lp = new Lung_Phantom__c();
        lp.contact__c = con.id;
        lp.Installed_Product__c = ip.id;
        lp.Date_Redeemed__c=system.today();
        lstOfLP.add(lp);
        
       
        insert lstOfLP;
     }
         
         user usr = [Select Id, ContactId, UserName from User where email=:'standarduserxxx@testorg.com' ];
         
         System.runAs (usr){
        cpLungPhantom cpl = new cpLungPhantom();
        cpl.getlLPHRec();
        cpl.MDADLContact();
        cpl.getLiEDate1();
        cpl.getLiEDate();
        cpl.SaveRec();
        
        cpl.closePopup();
        cpl.ShowPopup();
        cpl.ConfirmPopup();
        }
    }
    
    // Test Method for initiating cpMDADLLungPhantom class
    
    private static testmethod void cpMDADLLungPhantomTestMethod()
    {
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
      
        
        List<Contact> lstOfContact=new List<Contact>();
        Contact con = new Contact(FirstName = 'MacallanTest',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com');
        lstOfContact.add(con);
        
        Contact con1 = new Contact(FirstName = 'Macallan',lastname='testname1', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user1@gmail.com');
        lstOfContact.add(con1);
        insert lstOfContact;
        
        SVMXC__Installed_Product__c ip=new SVMXC__Installed_Product__c(SVMXC__Company__c=objACC.id );
        insert ip;
        
        //SVMXC__Installed_Product__c ip = [Select id, name from SVMXC__Installed_Product__c limit 1];
        
        List<Lung_Phantom__c> lstOfLP=new List<Lung_Phantom__c>();
        Lung_Phantom__c lp = new Lung_Phantom__c();
        lp.contact__c = con.id;
        lp.Installed_Product__c = ip.id;
        lp.Date_Redeemed__c=system.today();
        lstOfLP.add(lp);
        
        Lung_Phantom__c lp1 = new Lung_Phantom__c();
        lp1.contact__c = con1.id;
        lp1.Installed_Product__c = ip.id;
        lp1.Date_Redeemed__c=system.today();
        lstOfLP.add(lp1);
        insert lstOfLP;
        
        cpMDADLLungPhantom cpl = new cpMDADLLungPhantom();
        cpl.getlLPHRec();
        cpl.SaveRec();
    }
    
    
    
}
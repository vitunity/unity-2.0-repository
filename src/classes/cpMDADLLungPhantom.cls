public class cpMDADLLungPhantom {

  
    public PageReference SaveRec() 
    {
        system.debug('Why^^^');
        List<Lung_Phantom__c> lLungPhantUpdate = new List<Lung_Phantom__c>();
        system.debug('test2');
        if(wLungPhantRecs != null)
        {
            for(WrapperClass w: wLungPhantRecs)
            {
                lLungPhantUpdate.add(w.l);
            }
            system.debug('test{{{');
            if(lLungPhantUpdate != null)
            system.debug('test^^^');
                update lLungPhantUpdate;
        }  getlLPHRec();      
           System.debug('test');
        return null;
    }
   public String datename {get; set;}
   //Public Lung_Phantom__c objlf{get;set;}
    public Contact c{get;set;}
    public String usrContId{get;set;} 
    public cpMDADLLungPhantom ()
    {
     // objlf=new Lung_Phantom__c();
        User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];    
       // User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        if(usrContId != null)
        {
            c = [select Is_MDADL__c,name,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
        }
        
    }       
    
    public list<Lung_Phantom__c> lLungPhantRecs{get;set;}
    public list<WrapperClass> wLungPhantRecs{get;set;}
    
    public List<WrapperClass> getlLPHRec()
    {
        lLungPhantRecs = new List<Lung_Phantom__c>();
        wLungPhantRecs = new List<WrapperClass>();
        
        lLungPhantRecs = [Select id,Voucher_ID__c,contact__c,Date_Results_Reported_by_MDADL__c,Date_Exposed_Phantom_Received_by_MDADL__c,Date_Redeemed__c,Date_Received__c,Redeemed_By__c,Product_Name__c,Installed_Product__c,Expiration_Date_Ends__c, name from Lung_Phantom__c order by name desc];
        Set<ID> sIDs = new Set<ID>();
       
        for(Lung_Phantom__c l : lLungPhantRecs)
        {
            sIDs.add(l.contact__c);
            System.debug('testquery'+l.Date_Results_Reported_by_MDADL__c);
        }
         
        System.debug('testing'+sIDs);
        Map<ID, Contact> map_ID_Contact = new Map<ID,Contact>([select Is_MDADL__c,name,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id in :sIDs]);
        System.debug('testing'+map_ID_Contact);
        for(Lung_Phantom__c l : lLungPhantRecs)
        {
            Contact con = map_ID_Contact.get(l.contact__c);
            wLungPhantRecs.add(new WrapperClass(l, con));
        } 
        system.debug('Size^^^'+wLungPhantRecs.size());
        System.debug('testing3'+wLungPhantRecs);
        return wLungPhantRecs;
    }
     
    
 
     public Class WrapperClass
     {    
         public Lung_Phantom__c l{get;set;}
         public contact con{get;set;}
         
         public WrapperClass(Lung_Phantom__c l, Contact con)
         {
             this.l = l;
             this.con = con;
         }
         
     }   

}
/*************************************************************************\
    @ Author        : Mohit Bansal    
    @ Date          : 23-May-2013
    @ Description   : An Apex class to display Monte Carlo Homepage.
    @ Last Modified By  :   Mohit Bansal
    @ Last Modified On  :   28-May-2013
    @ Last Modified Reason  : Code Completed  
****************************************************************************/

public class cpMonteCarloController 
{
    Public cpMonteCarloController()
    {
      User usr = [Select ContactId, Profile.Name,alias from user where Id=: UserInfo.getUserId()];    
        // User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          shows = 'none';
         if(usr.contactid == null){
          Usrname = usr.alias;
          shows = 'block';        
         }  
    }
    
    public Contact c{get;set;}
    public String usrContId{get;set;} 
    public string Usrname{get;set;}
    public string shows{get;set;}
    
    public PageReference ClickClinac()
    {
         User usr = [Select ContactId, Profile.Name,alias from user where Id=: UserInfo.getUserId()];    
        // User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        if(usrContId != null)
        {
            c = [select Monte_Carlo_Registered__c,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
            
            System.debug('Value of Monte_Carlo_Registered__c field is' + c.Monte_Carlo_Registered__c);
            String str =''+  c.Monte_Carlo_Registered__c;
            if(str.contains('Clinac'))
            {
                PageReference pg = new PageReference('/apex/cpAccessDocs?text=Clinac');
                pg.setRedirect(true);
                return pg;
            }
            else
            {
                PageReference pg = new PageReference('/apex/cpClinacPage?text=Clinac');
                pg.setRedirect(true);
                return pg;
            }
            
        }
        else
            {   
                if(shows == 'block'){
                  PageReference pg = new PageReference('/apex/cpAccessDocs?text=Clinac');
                  pg.setRedirect(true);
                  return pg;
                }else{
                PageReference pg = new PageReference('/apex/cpClinacPage?text=Clinac');
                pg.setRedirect(true);
                return pg;
                }
            }
        
    }
    
    public PageReference ClickTrueBeam()
    {
         User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];    
        // User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        if(usrContId != null)
        {
            c = [select Monte_Carlo_Registered__c,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
            
            System.debug('Value of Monte_Carlo_Registered__c field is' + c.Monte_Carlo_Registered__c);
            String str =''+  c.Monte_Carlo_Registered__c;
            if(str.contains('TrueBeam'))
            {
                PageReference pg = new PageReference('/apex/cpAccessDocs?text=TrueBeam');
                pg.setRedirect(true);
                return pg;
            }
            else
            {
                PageReference pg = new PageReference('/apex/cpTrueBeamPage?text=TrueBeam');
                pg.setRedirect(true);
                return pg;
            }
            
        }else
            {
               if(shows == 'block'){
                  PageReference pg = new PageReference('/apex/cpAccessDocs?text=TrueBeam');
                  pg.setRedirect(true);
                  return pg;
                }else{
                PageReference pg = new PageReference('/apex/cpTrueBeamPage?text=TrueBeam');
                pg.setRedirect(true);
                return pg;
                }
            }
        
    }

}
/*************************************************************************\
    @ Author        : Anupam Tripathi
    @ Date      : 17-June-2013
    @ Description   :  Test class for code coverage of cpAccessDocs,cpMonteCarloController and cpNewRegist classes.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/


@IsTest(SeeAllData=true)

Class cpMonteCarloTest
{
    //Test Method for initiating cpMonteCarloController class as a customer portal User
    
    private static testmethod void cpMonteCarloTestMethod()
    {
       Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName = 'Charles',phone= '23123313',email ='test.sss1@gmail.com',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id,Monte_Carlo_Registered__c='TrueBeam');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser1@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test_user1@testorg.com'/*,UserRoleid=r.id*/); 
        insert u;
        
        System.runAs(u)
        {
        cpMonteCarloController cpl = new cpMonteCarloController();
        cpl.ClickClinac();
        cpl.ClickTrueBeam();
        }
        
    /*  Profile p1 = [select id from profile where name= Label.];
         
        User  usr = new User(alias = 'standt1', Subscribed_Products__c=true,email='standarduser1@testorg.com',emailencodingkey='UTF-8', lastname='Testing1',languagelocalekey='en_US',localesidkey='en_US', profileid = p1.Id, timezonesidkey='America/Los_Angeles', username='test__user1@testorg.com'/*,UserRoleid=r.id*///); 
        //insert usr;
        // System.runAs(usr)
        //{
        cpMonteCarloController cpl = new cpMonteCarloController();
        cpl.ClickClinac();
        cpl.ClickTrueBeam();
        //}
    }
    
     //Test Method for initiating cpMonteCarloController class
    
    private static testmethod void cpMonteCarloTestMethod1()
    {
       // User usr = [Select ContactId, Profile.Name from user where contactId != null and IsActive=true limit 1]; 
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName ='Anthony',phone= '23123313',email ='test.sss2@gmail.com',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id,Monte_Carlo_Registered__c='Clinac');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser2@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user2@testorg.com'/*,UserRoleid=r.id*/); 
        insert u;
        System.runAs (u){
        
        cpMonteCarloController cpl = new cpMonteCarloController();
        cpl.ClickClinac();
        cpl.ClickTrueBeam();
        }
        
    }
    
     //Test Method for initiating cpNewRegist class
    
    private static testmethod void cpNewRegistTestMethod()
    {
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName='Mike',phone= '23123313',email ='test.sss3@gmail.com',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id);//,Monte_Carlo_Registered__c='Clinac');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser3@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='test__user3@testorg.com'/*,UserRoleid=r.id*/); 
        insert u;
      // group gp=new group(name ='Monte Carlo');
      // insert gp;
      
     //   GroupMember gmem=new GroupMember(GroupId = gp.id,UserOrGroupId = UserInfo.getUserId());
     //   insert gmem;
        System.runAs(u)
        {
        ApexPages.currentPage().getParameters().put('text', 'Clinac');
        cpNewRegist cpl = new cpNewRegist();
        cpl.AccessDocsPage();
        cpl.RegisPage();
        cpl.closePopup();
        cpl.showPopup();
       // cpNewRegist.publicgroupadd();
        cpl.eventRgsSubmit();
        cpl.ShowAgreementContent();
        cpl.RegisPage();
        cpl.getTypeVal();
        cpl.setTypeVal('test');
        cpl.getItemsList();
        cpl.getItems();
        cpl.getTimeDuration();
        cpl.setTimeDuration('34');
        }
        
    }
    
   //Test Method for initiating cpAccessDocs class
   
   
   private static testmethod void cpAccessDocsTestMethod()
    { 
    
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName ='pieterson',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id,Monte_Carlo_Registered__c='Clinac',phone= '23123313',email ='test.sss4@gmail.com');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser4@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='user__test4@testorg.com'/*,UserRoleid=r.id*/); 
        insert u; User usr = [Select ContactId, Profile.Name from user where contactId != null and IsActive=true limit 1];
       
        Monte_Carlo__c mc = new Monte_Carlo__c(Type__c = 'Clinac',Title__c = 'test title');
        insert mc;
        System.runAs(u)
        {
        ApexPages.currentPage().getParameters().put('text', 'Clinac');
        cpAccessDocs  cpl = new cpAccessDocs();
        cpl.closePopup();
        cpl.showPopup();
        cpl.ShowDialogVal();
        }
        
    }
     private static testmethod void cpAccessDocsTestMethod1()
    { 
    
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName='Hasim1',lastname='testname1', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id,Monte_Carlo_Registered__c='Clinac',phone= '23123313',email ='test.sss5@gmail.com');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser5@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='user__test5@testorg.com'/*,UserRoleid=r.id*/); 
        insert u; User usr = [Select ContactId, Profile.Name from user where ContactId= null limit 1];
         list< Monte_Carlo__c> Lstmc = new list< Monte_Carlo__c>();
       
        Monte_Carlo__c mc = new Monte_Carlo__c(Type__c = 'TrueBeam',Title__c = 'test title');
        Lstmc.add(mc);
       // Monte_Carlo__c mc1 = new Monte_Carlo__c(Type__c = 'TrueBeam',Title__c = 'test title');
     //   Lstmc.add(mc1);
        insert Lstmc;
          RecordType ContentRT = [select Id FROM RecordType WHERE DeveloperName='Monte_Carlo' AND SobjectType = 'ContentVersion' LIMIT 1];
            ContentVersion testContentIn = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
            insert testContentIn;
            
              ContentVersion testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn.Id    and IsLatest=True];
            ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Monte Carlo' ]; 
                     ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
                     newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
                     newWorkspaceDoc.ContentDocumentId= testContents.ContentDocumentId; 
                     insert newWorkspaceDoc;
            //testContentIn.File_Type__c='Electron';
            testContentIn.Monte_Carlo__c=Lstmc[0].id;
            UPDATE testContentIn;
      
             
       
        System.runAs(u)
        {
       
        ApexPages.currentPage().getParameters().put('text', 'Clinac');
        ApexPages.currentPage().getParameters().put('type', 'access');
        //ApexPages.currentPage().getParameters().put('cid', 'ElectronFile');
       
        cpAccessDocs  cpl = new cpAccessDocs();
        cpl.closePopup();
        cpl.showPopup();
        cpl.ShowDialogVal();
        cpl.refresh();
        }
        
    }
    private static testmethod void cpAccessDocsTestMethod3()
    { 
    
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Contact con = new Contact(FirstName='Hasim',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id,Monte_Carlo_Registered__c='Clinac',phone= '23123313',email ='test.sss6@gmail.com');
        insert con;
        
        User  u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser6@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='user__test6@testorg.com'/*,UserRoleid=r.id*/); 
        insert u; //User usr = [Select ContactId, Profile.Name from user where ContactId= null limit 1];
        // list< Monte_Carlo__c> Lstmc = new list< Monte_Carlo__c>();
       
        Monte_Carlo__c mc = new Monte_Carlo__c(Type__c = 'Clinac',Title__c = 'test title');
        //Lstmc.add(mc);
        // Monte_Carlo__c mc1 = new Monte_Carlo__c(Type__c = 'Clinac',Title__c = 'test title');
        //  Lstmc.add(mc1);
        //insert Lstmc;
        insert mc;
        RecordType ContentRT = [select Id FROM RecordType WHERE DeveloperName='Monte_Carlo' AND SobjectType = 'ContentVersion' LIMIT 1];
        ContentVersion testContentIn = new ContentVersion( ContentURL='http://www.google.com/', Title ='Google.com', RecordTypeId = ContentRT.Id); 
        insert testContentIn;
        
        ContentVersion testContents = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentIn.Id   and IsLatest=True];
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Monte Carlo' ]; 
        ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
        newWorkspaceDoc.ContentDocumentId= testContents.ContentDocumentId; 
        insert newWorkspaceDoc;
        
        ContentVersion testContentIn1 = [SELECT Id, File_Type__c, Title, Monte_Carlo__c FROM ContentVersion WHERE ContentDocumentId = :testContents.ContentDocumentId AND IsLatest = true];
        System.AssertEquals(testContentIn1.Title, 'Google.com');
        testContentIn1.File_Type__c='Related Content';
        testContentIn1.Monte_Carlo__c=mc.id;
        update testContentIn1;
        
        ContentVersion testContentIn2 = new ContentVersion( ContentURL='http://www.yahoo.com/', Title ='Google.com', RecordTypeId = ContentRT.Id, FirstPublishLocationId = testWorkspace.Id, File_Type__c='Related Content', Monte_Carlo__c=mc.id); 
        insert testContentIn2;
        System.AssertNotEquals(testContentIn2.Id, null);
        
        System.runAs(u)
        {
            ApexPages.currentPage().getParameters().put('text', 'TrueBeam');
            ApexPages.currentPage().getParameters().put('type', 'access');
            
            ApexPages.currentPage().getParameters().put('cid', 'RelCont');
            cpAccessDocs cpl = new cpAccessDocs();  
            ApexPages.currentPage().getParameters().put('text', 'TrueBeam'); 
            ApexPages.currentPage().getParameters().put('type', 'access');
            ApexPages.currentPage().getParameters().put('cid', ''); 
            cpAccessDocs cp2 = new cpAccessDocs();   
            ApexPages.currentPage().getParameters().put('text', 'TrueBeam');
            ApexPages.currentPage().getParameters().put('type', 'access');
            ApexPages.currentPage().getParameters().put('cid', 'ElectronFile');
            cpAccessDocs  cp3 = new cpAccessDocs();  
        }
    }
}
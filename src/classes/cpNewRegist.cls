/*************************************************************************\
    @ Author        : Mohit Bansal    
    @ Date          : 21-May-2013
    @ Description   : An Apex class to display New User Registration page for Truebeam and Clinac for Monte Carlo.
    @ Last Modified By  :   Mohit Bansal
    @ Last Modified On  :   28-May-2013
    @ Last Modified Reason  : Code Completed  
****************************************************************************/

Public class cpNewRegist
{
    public Boolean ShowAgreement{get;set;}
    public String SurveyURL { get; set; }
    Public String EvntName {get; set;}
    Public String AcctName {get; set;}
    Public String StateId {get; set;}
    Public String CustZip {get; set;}
    Public String CustCity {get; set;}
    Public String CustStreet {get; set;}
    Public String CustState {get; set;}
    Public String CustCountry {get; set;}       
    Public String CustEmail {get; set;}
    Public String CustFname {get; set;}
    Public String CustLname {get; set;}
    Public String CustTitle {get; set;}
    Public String CustPhone {get; set;}
    Public String CustLicense {get; set;}
    Public String CustNPI {get; set;}
    Public String CustLicenseTp {get; set;} 
    String EvntId = null;
    public Boolean isGuest{get;set;}
    public String Initial_1{get;set;}
    public String Initial_2{get;set;}
    public String Initial_3{get;set;}
    
    public Boolean termsAccepted{get;set;}
    Event_Webinar__c evnt;
    public String pageName{get;set;}
    public Boolean ShowRegis{get;set;}
    
    public PageReference ShowAgreementContent()
    {
        ShowAgreement = true;
        return null;
    }
    
    Public cpNewRegist()
    {
        pageName = System.currentPageReference().getParameters().get('text');
        ShowRegis = false;
    }
    
    public PageReference RegisPage()
    {
       Set<String> sProductGroup = CpProductPermissions.fetchProductGroup();
       if(sProductGroup.contains(pageName))         
                ShowRegis = true;
        
        return null;
    }
    
    public Contact c{get;set;}
    public String usrContId{get;set;} 
    public PageReference AccessDocsPage()
    {
         User usr = [Select ContactId, Profile.Name from user where Id=: UserInfo.getUserId()];    
        // User usr = [Select ContactId from user where Id='005c0000000KnVoAAK'];
          
        System.debug('>>>>>>>>ContactID' + usr.ContactId );
        usrContId = usr.contactid;
        if(usrContId != null)
        {
            c = [select Monte_Carlo_Registered__c,FirstName, LastName, Account.name, Email, phone, Title, MailingStreet, MailingState, MailingPostalCode, MailingCountry, MailingCity from contact where Id =: usrContId];
            
            System.debug('Value of Monte_Carlo_Registered__c field is' + c.Monte_Carlo_Registered__c);
            String str =''+  c.Monte_Carlo_Registered__c;
            if(str.contains(pageName))
            {
                PageReference pg = new PageReference('/apex/cpAccessDocs');
                pg.setRedirect(true);
                return pg;
            }
            else
            {
            CustFname = c.FirstName;
            CustLname = c.LastName;
            AcctName = c.Account.name;
            CustCity = c.MailingCity;
            CustStreet = c.MailingStreet;
            CustState = c.MailingState;
            CustCountry = c.MailingCountry;
            CustEmail = c.Email;
            CustTitle = c.title;
            CustPhone = c.phone;
            CustZip = c.MailingPostalCode;
            return  null;
            }
        }
        else
        return null;
    }
        public PageReference eventRgsSubmit()
    {
        Monte_Carlo_Registration__c Rgstration = new Monte_Carlo_Registration__c();
        if(usrContId != null)
        {
            Rgstration.contact__c = usrContId;
            String arr = c.Monte_Carlo_Registered__c;
            if(arr != null)
            {
                c.Monte_Carlo_Registered__c = c.Monte_Carlo_Registered__c + ';' + pageName;
            }
            else
            {
                c.Monte_Carlo_Registered__c = pageName;
                
             }
        }
        
        Rgstration.Requested_Data__c  = CustLicense;
        Rgstration.Intended_Use__c  = StateId;
        Rgstration.Principal_Investigator__c = CustNPI;
        Rgstration.Co_Investigators__c = CustLicenseTp;
        Rgstration.Rule_8_Initial__c = Initial_1;
        Rgstration.Rule_9_Initial__c = Initial_2;
        Rgstration.Rule_10_Initial__c = Initial_3;
        Rgstration.Project_Length__c = '' + TimeDuration + ' ' + TypeVal ;
      
        try
        {
            insert Rgstration;
            if(c != null){
                update c;
                if(!Test.isRunningTest()){
                publicgroupadd();
                }
                //added by harshita
                /*Group grp = [Select id from group where name ='Monte Carlo'];
                GroupMember gm = new groupMember(GroupId = grp.id,UserOrGroupId = UserInfo.getUserId());
                insert gm;*/
                }
           PageReference pg = new PageReference('/apex/cpAccessDocs?text=' + pageName+'&type=access');
            pg.setRedirect(true);
            return pg;
           // return null;
        }
        catch(Exception e)
        {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Error Ocured: ' + e +'Please contact Helpdesk support for further assistance.');
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please contact Helpdesk support for further assistance.');

            ApexPages.addMessage(myMsg);
            return null;
        }
        
       
        
    }
    
        public String TimeDuration = '1';
        public String TypeVal = 'Month(s)';
            
        public String getTypeVal () {
            return TypeVal;
        }
            
        public void setTypeVal (String TypeVal) {
            this.TypeVal= TypeVal;
        }  
        
        public List<SelectOption> getItemsList() {
            List<SelectOption> options = new List<SelectOption>();
            
            options.add(new SelectOption('Month(s)','Month(s)'));
            options.add(new SelectOption('Year(s)','Year(s)'));
            
            return options;
        }
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            for(Integer i=1; i <= 12; i++)
            options.add(new SelectOption(''+i,''+i));
            
            return options;
        }
            
        public String getTimeDuration () {
            return TimeDuration ;
        }
            
        public void setTimeDuration (String TimeDuration ) {
            this.TimeDuration = TimeDuration ;
        }
        
        
        public boolean displayPopup {get; set;}     
    
     public void closePopup() {        
        displayPopup = false;    
     }     
     public void showPopup() {        
        displayPopup = true;    
     }
    
        public Boolean TC{get;set;}

 @future
 public static void publicgroupadd(){
    Group grp = [Select id from group where developername ='Monte_Carlo'];
                GroupMember gm = new groupMember(GroupId = grp.id,UserOrGroupId = UserInfo.getUserId());
                insert gm;
 }
}
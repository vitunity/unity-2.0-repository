public class cpSuppInstitutionsController {

    public String Phone { get; set; }

    public String PostalCode { get; set; }

    public String Country { get; set; }

    public String State { get; set; }

    public String City { get; set; }

    public String StreetAddress { get; set; }

    public String AcctName { get; set; }

    public String Name { get; set; }
    public String userContactId = '';
    
    Public List<SelectOption> optionscountry = new List<SelectOption>();
    
    public cpSuppInstitutionsController()
    {
        userContactId = Apexpages.currentPage().getParameters().get('Id');
        setoptionscountry();
    }
    
    public List<SelectOption> getoptionscountry()
    {
      return optionscountry;
    }
  
     Public void setoptionscountry()
     {
 
         List<Regulatory_Country__c> countrylist = new List<Regulatory_Country__c>([Select Name,SupportRegion__c from Regulatory_Country__c Order By Name]);
         //AggregateResult[] countrylist = [Select Name,RA_Region__c from Regulatory_Country__c Order By Name];
         optionscountry.add(new selectoption('--Select--','--Select--'));
         for(Regulatory_Country__c values: countrylist)
         {
             optionscountry.add(new selectoption(values.Name,values.Name));     
         }
     }
     
     public PageReference insertContactUpdateRecord()
     {
         Contact_Updates__c newConUpRecord = new Contact_Updates__c();
         newConUpRecord.Contact__c = userContactId;
         newConUpRecord.New_Account__c = AcctName;
         newConUpRecord.Country__c = Country;
         newConUpRecord.Telephone__c = Phone;
         newConUpRecord.Postal_Code__c = PostalCode;
         newConUpRecord.City__c = City;
         newConUpRecord.State__c = State;
         newConUpRecord.Address__c = StreetAddress;
         
         insert newConUpRecord;
         return null;
         
     }
}
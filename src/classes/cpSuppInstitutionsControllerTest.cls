@isTest
public class cpSuppInstitutionsControllerTest {
    
   @isTest
    public static void insertContactUpdateRecordTest()
    {
        Account testAccRec = new Account(Name = 'Test Account', Country__c = 'India');
        insert testAccRec;
        Contact testCon = new Contact(LastName='Test Contact', FirstName = 'Test', Functional_Role__c = 'Educator', AccountId = testAccRec.Id, Email = 'test@test.com');
        insert testCon;
        
        PageReference CpSupportedInstitutionsPage = Page.CpSupportedInstitutions;
        Test.setCurrentPage(CpSupportedInstitutionsPage);
        ApexPages.currentPage().getParameters().put('Id', testCon.Id);
        
        cpSuppInstitutionsController cpObj = new cpSuppInstitutionsController ();
        cpObj.getoptionscountry();
        cpObj.AcctName = 'test Acc';
        cpObj.City = 'San Jose';
        cpObj.Country = 'India';
        cpObj.Phone = '000000000';
        cpObj.PostalCode = '95112';
        cpObj.State = 'CA';
        cpObj.StreetAddress = 'Test address';
        
        Test.startTest();
            cpObj.insertContactUpdateRecord();
        Test.stopTest();
        
        Contact_Updates__c conUpRec = [Select Id, Contact__r.LastName from Contact_Updates__c Where Contact__r.Id =: testCon.Id ];
        System.assertEquals(testCon.LastName, conUpRec.Contact__r.LastName);
    }

}
/*****************************************
Created by: Naren
Issue:INC1518814 MyVarian: CP Users by Product report is not displaying the correct information
Solution: Create a VF page and controller to display list of all available product groups from Custom settings 
and display users listing based on Product group selection
******************************************/

public class cpUsersByProduct{

    public Boolean ViewUser{get;set;}
    Public List<User> Userlist{get;set;}
    public set<string> finallist{get;set;}
    public Integer Int1{get;set;}
    private integer counter=0;  //keeps track of the offset
    private integer list_size=50; //sets the page size or number of rows
    public integer total_size{get;set;} //used to show user the total size of the list
    Public List<WrapperClass> WrapperList {get;set;}
    Public Integer Count{get;set;}
    
  Public Class WrapperClass{
    Public String UserName{Get;set;}
    Public String Name{Get;set;}
    Public String UserFirstName{Get;set;}
    Public String UserLastName{Get;set;}
    Public String UserEmail{get;set;}
    Public String GroupName{Get;set;}
    Public String AccountName{Get;set;}
    Public String ContactName{Get;set;}
    Public String AccountNumber{get;set;}
    Public String AccountRefNumber{get;set;}
    Public String UserId{get;set;}
  } 
  
  public  Wrapperclass[] CustomSave(){
      system.debug('leftselected----'+leftselected);
      system.debug('rightselected----'+rightselected);
      system.debug('FinalList----'+FinalList);
      system.debug('Final Size---'+finallist.size());
      system.debug('before count---'+count);
      string str = apexpages.currentpage().getparameters().get('Count');
      system.debug('str------'+Str);
    try {
         if(counter <0 || str == string.valueof(count))
         counter = 0;
         Map<String,User> UserMap = new Map<String,User>();
         Map<String,GroupMember> GroupMap = new Map<String,GroupMember>();         
         Map<String,Contact> ContactMap = new Map<String,Contact>();
         List<String> UserNameList = new List<String>();
         system.debug('finallist---'+finallist);
         system.debug('list_size-----'+list_size);
         system.debug('Counter-----'+Counter);
         
          User[] numbers = [select id,username,name,firstname,lastname,email from user where id IN (Select UserOrGroupId from groupmember where Group.name IN:finallist) AND isActive = True];
         for(user u:numbers){
           UserMap.put(u.id,u);
           UserNameList.add(u.name);
         }
         for(contact c:[select id,name,firstname,lastname,accountid,account.name,account.accountnumber,account.SFDC_Account_Ref_Number__c from contact where Name IN :UserNameList]){
            ContactMap.put(c.name,c);           
         } 
         system.debug('contactmap----'+contactmap);
         WrapperList = new list<Wrapperclass>();
          system.debug('contactmap keyset ----'+contactmap.keyset());
          system.debug('FinalList----'+Finallist);
         system.debug(' 54 list_size-----'+list_size);
         system.debug('55 Counter-----'+Counter); 
         total_size = [Select count() from groupmember where Group.name IN:finallist  and UserOrGroupId IN:Usermap.keyset()  ];
         //total_size = [select count() from user where id IN (Select UserOrGroupId from groupmember where Group.name IN:finallist)];
         system.debug('total_size---'+total_size);
          for(GroupMember G:[Select UserOrGroupId,id,group.name,Groupid from groupmember where Group.name IN:finallist and UserOrGroupId IN:Usermap.keyset() order by Group.name limit:list_size  Offset :counter ]){
            system.debug('G.Group.name-----'+G.Group.name);
            WrapperClass w = new WrapperClass();
            if(UserMap.containskey(g.UserOrGroupId)){
                 W.UserName = UserMap.get(g.UserOrGroupId).username;
                 W.Name = UserMap.get(g.UserOrGroupId).name;
                 W.UserFirstName = UserMap.get(g.UserOrGroupId).Firstname;
                 W.UserLastName = UserMap.get(g.UserOrGroupId).Lastname;
                 W.UserEmail = UserMap.get(g.UserOrGroupId).Email;
                 W.userId = UserMap.get(g.UserOrGroupId).Id;
                 W.groupName = G.Group.name;
                 system.debug('w.username-----'+w.username);
                 system.debug('w.userid-------'+UserMap.get(g.UserOrGroupId).id);
                 if(ContactMap.containskey(w.name)){
                    W.contactName = Contactmap.get(W.name).name;
                    W.AccountName = Contactmap.get(w.name).Account.name;
                    W.AccountNumber = Contactmap.get(w.name).account.AccountNumber;
                    W.AccountRefNumber = Contactmap.get(w.name).account.SFDC_Account_Ref_Number__c;
                 }
                 WrapperList.add(w);
            }
            ViewUser = True;
         }
         System.debug('WrapperList----------'+WrapperList);
         return null;
      } catch (QueryException e) {
         ApexPages.addMessages(e);   
         return null;
      }
    }
    
     Set<String> originalvalues = new Set<String>();
     Public List<string> leftselected{get;set;}
     Public List<string> rightselected{get;set;}
     Set<string> leftvalues = new Set<string>();
     Set<string> rightvalues = new Set<string>();
     
    public cpUsersByProduct(){
        leftselected = new List<String>();
        rightselected = new List<String>();
        Finallist = new set<string>();
        List<String> PublicGroupList = new List<String>();
        count = 0;
        For(Product_Roles__c  p:Product_Roles__c.getAll().values()){
            system.debug('p---'+p.Public_Group__c);
            PublicGroupList.add(p.Public_Group__c);
          }
        For(Group Grp :[select id,Name,DeveloperName,type from group  where type ='Regular' and Name IN:PublicGroupList  order by Name desc])
          originalvalues.add(Grp.name);
          originalvalues.add('VMS MyVarian - Partners');
          leftvalues.addAll(originalValues);
          system.debug('leftvalues----'+leftvalues);
        // total_size = 100;
        ViewUser = false;
  }
     
    public Void selectclick(){
        rightselected.clear();
        system.debug('leftselected----'+leftselected.size());
        for(String s : leftselected){
        system.debug('s----'+s);
        leftvalues.remove(s);
        rightvalues.add(s);
       // Finallist.add(s);
        }
        system.debug('rightvalues----'+rightvalues);
       // return null;
    }
     
    public Void unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
            Finallist.remove(s);
        }
        //return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        system.debug('tempList------'+tempList);
        for(String s : tempList){
            options1.add(new SelectOption(s,s));
            finallist.add(s);
            }
        return options1;
    }
     
  public PageReference Export() {
    Pagereference pg = new pagereference('/apex/cpUsersByProductExcelSheet');
        return pg;
    }
    
  Public Pagereference reDirect(){
     String str = Apexpages.currentpage().getparameters().get('UserId');
     Pagereference pg = new pagereference('/'+str);
     return pg;
  }
 
   public Void Beginning() { 
       //user clicked beginning
      counter = 0;
     // return null;
     customsave();
   }
 
   public Void Previous() { 
       //user clicked previous button
      counter -= list_size;
      //return null;
      customsave();
   }
 
   public Void Next() { 
       //user clicked next button
      counter += list_size;
      //return null;
      customsave();
   }
 
   public void End() { 
       //user clicked end
       system.debug('total_size---'+total_size);
       system.debug('list_size----'+list_size);
       
     system.debug('191-----'+math.mod(total_size, list_size));
      counter = total_size - math.mod(total_size, list_size);
    
      //return null;
      customsave();
   }
 
   public Boolean getDisablePrevious() { 
      //this will disable the previous and beginning buttons
      if (counter>0) 
      return false; 
      else 
      return true;
   }
 
   public Boolean getDisableNext() { 
   //this will disable the next and end buttons
      if (counter + list_size < total_size) 
      return false; 
      else 
      return true;
   }
 
   public Integer getTotal_size() {
       system.debug('total_size----'+total_size);
      return total_size;
      
   }
   
    public PageReference ResetPage() {
    
    PageReference Pg2 = new PageReference('/apex/cpUsersByProductPage');
        pg2.setRedirect(True);
        return Pg2;
    }
 }
@isTest
public class cpUsersByProductTest{
     static testMethod void myUnitTest() 
     {
     List<User> userList = [SELECT id, name FROM User WHERE isActive = True AND UserType = 'Standard' LIMIT 25];
         /*Account a = new Account();
            a.Name = 'Test Account';
            a.BillingCountry='Test Country';
            a.Country__c= 'USA';
            a.BillingCity= 'Los Angeles';
            a.BillingStreet ='3333 W 2nd St';
            a.BillingState ='CA';
            a.BillingPostalCode = '12345';
            a.Agreement_Type__c = 'Master Pricing';
            a.OMNI_Address1__c ='3333 W 2nd St';
            a.OMNI_City__c ='Los Angeles';
            a.OMNI_State__c ='CA';
            a.OMNI_Country__c = 'Test Country';  
            a.OMNI_Postal_Code__c = '12345';
            insert a;
        Contact con = new contact();
             con.accountid = a.id;
             con.firstname = 'Partner';
             con.Lastname = 'Contact';
             con.email = 'partner@contact.com';
             Insert Con;
        Profile lstProfile = [Select Id FROM Profile WHERE Name = 'Partner Community Login User' limit 1 ];
        User usr = new User(Email='partner@contact.com',username = 'Varianpartner@contact.com',FirstName = 'Partner',LastName = 'Contact',ProfileId =lstProfile.id,CommunityNickname='testAD',ContactId = con.Id,alias='testAD',LocaleSidKey='en_US',TimezoneSidKey='America/Los_Angeles',EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US');
            insert usr; */
            
         Group publicGroup1 = new Group();
             
             publicGroup1.Name = 'Test Group1';
             publicGroup1.Type = 'Regular';
             insert publicGroup1;
             
             
         Group publicGroup2 = new Group();
             
             publicGroup2.Name = 'Test Group2';
             publicGroup2.Type = 'Regular';
             insert publicGroup2;
     
             GroupMember member1 = new GroupMember();
                 member1.UserOrGroupId = UserList[0].Id;
                 member1.GroupId = publicGroup1.id;
                 insert member1;
             
              GroupMember member2 = new GroupMember();
                 member2.UserOrGroupId = UserList[1].Id;
                 member2.GroupId = publicGroup1.id;
                 insert member2;
             
             GroupMember member3 = new GroupMember();
                 member3.UserOrGroupId = UserList[2].Id;
                 member3.GroupId = publicGroup1.id;
                 insert member3;
             
             GroupMember member4 = new GroupMember();
                 member4.UserOrGroupId = userList[3].Id;
                 member4.GroupId = publicGroup1.id;
                 insert member4;
             
             GroupMember member5 = new GroupMember();
                 member5.UserOrGroupId = userList[4].Id;
                 member5.GroupId = publicGroup1.id;
                 insert member5;
             
             GroupMember member6 = new GroupMember();
                 member6.UserOrGroupId = userList[5].Id;
                 member6.GroupId = publicGroup1.id;
                 insert member6;
             
             GroupMember member7 = new GroupMember();
                 member7.UserOrGroupId = userList[6].Id;
                 member7.GroupId = publicGroup1.id;
                 insert member7;
             
             GroupMember member8 = new GroupMember();
                 member8.UserOrGroupId = userList[7].Id;
                 member8.GroupId = publicGroup1.id;
                 insert member8;
             
             GroupMember member9 = new GroupMember();
                 member9.UserOrGroupId = userList[8].Id;
                 member9.GroupId = publicGroup1.id;
                 insert member9;
             
             GroupMember member10 = new GroupMember();
                 member10.UserOrGroupId = userList[9].Id;
                 member10.GroupId = publicGroup1.id;
                 insert member10;
             
             GroupMember member11 = new GroupMember();
                 member11.UserOrGroupId = userList[10].Id;
                 member11.GroupId = publicGroup1.id;
                 insert member11;
             
             GroupMember member12 = new GroupMember();
                 member12.UserOrGroupId = userList[11].Id;
                 member12.GroupId = publicGroup1.id;
                 insert member12;
             
             GroupMember member13 = new GroupMember();
                 member13.UserOrGroupId = userList[12].Id;
                 member13.GroupId = publicGroup1.id;
                 insert member13;
             
             GroupMember member14 = new GroupMember();
                 member14.UserOrGroupId = userList[13].Id;
                 member14.GroupId = publicGroup1.id;
                 insert member14;
             
             GroupMember member15 = new GroupMember();
                 member15.UserOrGroupId = userList[14].Id;
                 member15.GroupId = publicGroup1.id;
                 insert member15;
             
             GroupMember member16 = new GroupMember();
                 member16.UserOrGroupId = userList[15].Id;
                 member16.GroupId = publicGroup1.id;
                 insert member16;
             
             GroupMember member17 = new GroupMember();
                 member17.UserOrGroupId = userList[16].Id;
                 member17.GroupId = publicGroup1.id;
                 insert member17;
             
             GroupMember member18 = new GroupMember();
                 member18.UserOrGroupId = userList[17].Id;
                 member18.GroupId = publicGroup1.id;
                 insert member18;
             
             GroupMember member19 = new GroupMember();
                 member19.UserOrGroupId = userList[18].Id;
                 member19.GroupId = publicGroup1.id;
                 insert member19;
             
             GroupMember member20 = new GroupMember();
                 member20.UserOrGroupId = userList[19].Id;
                 member20.GroupId = publicGroup1.id;
                 insert member20;
             
             GroupMember member21 = new GroupMember();
                 member21.UserOrGroupId = userList[20].Id;
                 member21.GroupId = publicGroup2.id;
                 insert member21;
             
             GroupMember member22 = new GroupMember();
                 member22.UserOrGroupId = userList[21].Id;
                 member22.GroupId = publicGroup2.id;
                 insert member22;
             
             GroupMember member23 = new GroupMember();
                 member23.UserOrGroupId = userList[22].Id;
                 member23.GroupId = publicGroup2.id;
                 insert member23;
             
             GroupMember member24 = new GroupMember();
                 member24.UserOrGroupId = userList[23].Id;
                 member24.GroupId = publicGroup2.id;
                 insert member24;
             
             GroupMember member25 = new GroupMember();
                 member25.UserOrGroupId = userList[24].Id;
                 member25.GroupId = publicGroup2.id;
                 insert member25;
             
             
         cpUsersByProduct Controller = new cpUsersByProduct();  
         cpUsersByProduct.WrapperClass wp = new cpUsersByProduct.WrapperClass();
             Controller.leftselected = new List<String>{'Test Group2'};
             Controller.selectclick();
             Controller.unselectclick();
             Controller.getunSelectedValues();
             Controller.getSelectedValues();
             Controller.CustomSave();
             Controller.total_size = 100;
             
             
             //Controller.getWrapperList();
             Controller.Export();
             Controller.reDirect();
             Controller.Beginning();
             Controller.Previous();
             Controller.Next();
             Controller.getTotal_size();
             Controller.End();
             Controller.getDisablePrevious();
             Controller.getDisableNext();
             Controller.ResetPage();
     }
}
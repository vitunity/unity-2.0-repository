/*************************************************************************\
    @ Author        : Harshita
    @ Date      : 17-June-2013
    @ Description   :  Test class used for code coverage of CpUserActivationclass.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@IsTest(seealldata = true)

Class cpproductversiontest
{
    
    // Test Method for initiating cpLungPhantom class
    
    private static testmethod void cpproductversiontestmethod()
    {
      Test.StartTest();

      Integer RndNum = crypto.getRandomInteger();
      
      Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
      
      Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
      Contact con = new Contact(FirstName = 'Mikes',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
        insert con; 
      User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com'+RndNum,emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standarduser@test.com'+RndNum/*,UserRoleid=r.id*/); 
        insert u; // Inserting Portal User
      Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity');
        insert pr;
      Product_Version__c pv = new Product_Version__c(product__c = pr.id);
        insert pv;

      Test.StopTest();
    }

    private static testmethod void cpproductversiontestmethod2()
    {
      Test.StartTest();

      RecordType ContentRT = [select Id FROM RecordType WHERE Name='Product Document'];

      ContentVersion testContentInsert =new ContentVersion(); 
      testContentInsert.ContentURL='http://www.google.com/'; 
      testContentInsert.Title ='Google.com'; 
      testContentInsert.RecordTypeId = ContentRT.Id; 
        insert testContentInsert; 
 
      ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :testContentInsert.Id]; 
      ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name='Acuity']; 
      ContentWorkspaceDoc newWorkspaceDoc =new ContentWorkspaceDoc(); 
      newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id; 
      newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId; 
        insert newWorkspaceDoc;

      contentversion cv = new contentversion(id = testContentInsert.id);
      //cv.document_version__c = '0.0';
        update cv;

      Test.StopTest();          
    }       
}
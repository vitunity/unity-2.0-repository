/*************************************************************************\
    @ Author        : Harshita
    @ Date      : 17-June-2013
    @ Description   :  Test class used for code coverage of CpUserActivationclass.
    @ Last Modified By :   Narmada Yelavaluri
    @ Last Modified On  :   09-July-2013
    @ Last Modified Reason  :   Updated Header Comments
****************************************************************************/

@IsTest(seeAllData = true)
Class cpuseractivationtest
{
  
    // Test Method for initiating cpLungPhantom class
   
    private static testmethod void cpuseractivationtestmethod()
    {
      Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
      List<Account> objACClist = new List<Account>();
      List<Contact> conlist = new List<Contact>();
       Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC);
        Account objACC1=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC1);
        
        //test.startTest();
        insert objACClist;
        
   
        Contact con5 = new Contact(FirstName='Krupa',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC1.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
        conlist.add(con5);
          //insert conlist;     
        
      Contact con = new Contact(FirstName='Krupali',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
       conlist.add(con); 
       Contact con4 = new Contact(FirstName='Bhumi',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
           conlist.add(con4); 
           insert conlist;
     Set<id> userid = new set<Id>();
      userid.add(con.id);
      
      User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standar_duser@test.com'/*,UserRoleid=r.id*/); 
        insert u; // Inserting Portal User
     //CpUserActivationClass.useractivationmethod(userid);
    //----P used       
     User u4 = new User(alias = 'standt2', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con4.id, username='sta_ndarduser@test.com'/*,UserRoleid=r.id*/); 
        insert u4; 
      User u1 = new user(id = u.id);
      u1.email = 'test.user@company.com';
      //update u1;
      //user sysuser = [select id from user where profile.name = 'System Administrator' and isactive = true Limit 1];
       userid.add(con4.id);
      CpUserActivationClass.updateuser(userid);
      userid.clear();
      
      
    }
    
    private static testmethod void cpuseractivationtestmethod1() {
        List<Account> objACClist = new List<Account>();
        Set<id> userid = new set<Id>();
          
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC);
        insert objACClist;
                    
        Contact con123 = new Contact(FirstName='Daksha',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
        insert con123;
        User u = new User(alias = 'stt123', Subscribed_Products__c=true,email='standarduser123@testorg.com',emailencodingkey='UTF-8', lastname='Testing123',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con123.id, username='standar_duser123@test.com'/*,UserRoleid=r.id*/); 
        insert u;
        
        
        userid.add(u.id);
        CpUserActivationClass.userdeactivationmethod(userid);
        //system.debug('######' + con4.oktaId__c);
         
        
    }
     private static testmethod void cpuseractivationtestmethod2()
    {
        List<Account> objACClist = new List<Account>();
        Profile p = [select id from profile where name='VMS MyVarian - Customer User'];
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        objACClist.add(objACC);
        insert objACClist;
        
        Contact con = new Contact(FirstName='Mina',lastname='testname', MailingCountry='testcountry', MailingState='teststate',AccountId=objACC.id, phone='23444455', email = 'test_user@gmail.com',OktaId__c = '000909987867Addbs');
        insert con;
        User u = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser123@testorg.com',emailencodingkey='UTF-8', lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id, username='standar_duser@test.com'/*,UserRoleid=r.id*/); 
        insert u; // Inserting Portal User
        
       
          user sysuser = [select id from user where profile.name = 'System Administrator' and isactive = true Limit 1];  
          system.runas(sysuser){
          
      
          
                //u.LMS_Status__c = 'Inactive';
                //Update u;
                
              List<Contact> contoupdate = new list<contact>();
              contact u3 = new contact(id = con.id);
              u3.email = 'test.user@comany.com';
              //u3.Inactive_Contact__c = true;
              u3.MarketingKit_Agreement__c = 'Calypso for Prostate Marketing Program';
              update u3;
              
             /** contact u5 = new contact(id = con4.id);
              u5.email = 'test.user@comany.com';
              contoupdate.add(u5);
              //update u5;
              
               Contact c = new contact(id = con5.id);
                c.accountid = objACC.id;
                contoupdate.add(c);
                update contoupdate;
                **/
              user u2 = new user(id = u.id);
              u2.isActive = false;
              update u2;
              
              set<Id> setCon = new set<Id>();
              setCon.add(con.Id);
              CpUserActivationClass.addUsersInKPIOktaGroup(setCon);
              CpUserActivationClass.addUsersInLMSOktaGroup(setCon);
              CpUserActivationClass.removeUserFromKPIOktaGroup(setCon);

        }
      
      //test.stopTest();
    }       
}
public  class duplicateContactSearch {
    Public Integer Count{get;set;}
    Public List<Contact> ContactList{get;set;}
    Public Contact con{get;set;}
    Public String accid{get;set;}
    
        
        public duplicateContactSearch(ApexPages.StandardController controller) {
            con = new Contact();
            ContactList = new List<Contact>();
            Count = 0;
        }
        
        Public Pagereference duplicateSearch(){
            accid = Apexpages.currentpage().getparameters().get('accid');
            if((con.Email != Null && con.Email !='') && (accid != Null && accid != '')){
                system.debug('con.email---'+con.Email);
                system.debug('accid---'+accid);
                ContactList = [select id, email,firstname,lastname, name, Account.name, Account.BillingStreet, Account.BillingState, Account.BillingCountry, Account.BillingCity, Account.BillingPostalcode from Contact where Email =:con.Email and AccountId =: accid];
            }
            else if((con.Email != Null && con.Email !='') && (accid == Null || accid == '')){
                system.debug('con.email---'+con.Email);
                system.debug('accid---'+accid);
                ContactList = [select id, email,firstname,lastname, name, Account.name, Account.BillingStreet, Account.BillingState, Account.BillingCountry, Account.BillingCity, Account.BillingPostalcode from Contact where Email =:con.Email];
            }
             system.debug('ContactList---'+ContactList);
             Count = ContactList.size();
             system.debug('Count---'+Count);
            if(ContactList.size()>0){
                ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.Info, 'Contact already exists. To continue, click on the contact of interest or Cancel.'));
                return Null;
                
            }
            
            else{
                pagereference pg;
                if((con.Email != Null && con.Email !='') && (accid != Null && accid != ''))
                pg= new pagereference('/003/e?nooverride=1&con15='+con.Email+'&con4_lkid='+accid+'&con10='+'&con11=');
                else if((con.Email != Null && con.Email !='') && (accid == Null || accid == ''))
                pg= new pagereference('/003/e?nooverride=1&con15='+con.Email);
                else if((con.Email == Null || con.Email =='') && (accid != Null && accid != ''))
                pg= new pagereference('/003/e?nooverride=1&con4_lkid='+accid+'&con10='+'&con11=');
                else
                pg= new pagereference('/003/e?nooverride=1');
                system.debug('pg-----'+pg);
                pg.setredirect(true);
                return pg;
            }
          return null;  
        }


        /**
        * This method does exatcly the same thing that the above duplicateSearch method
        * Only difference is this method is called from a trigger and above method is invoked from a VF Page controller
        * account id and contact id are passed to this method as parameters
        * this method will return boolean value depending on duplicate contact found or not : true  - if duplicate found, else - false
        **/
        public static boolean isContactDuplicateByEmail(Id accid, String email)
        {
            List<Contact> duplicateContactList = new List<Contact>();
            Integer count = 0;

            //Id accid = Apexpages.currentpage().getparameters().get('accid');
            if((email != Null && email !='') && (accid != Null && accid != '')){
                system.debug('con.email---'+email);
                system.debug('accid---'+accid);
                duplicateContactList = [select id, email,firstname,lastname, name, Account.name, Account.BillingStreet, Account.BillingState, Account.BillingCountry, Account.BillingCity, Account.BillingPostalcode from Contact where Email =:email and AccountId =: accid];
            }
            else if((email != Null && email !='') && (accid == Null || accid == '')){
                system.debug('con.email---'+email);
                system.debug('accid---'+accid);
                duplicateContactList = [select id, email,firstname,lastname, name, Account.name, Account.BillingStreet, Account.BillingState, Account.BillingCountry, Account.BillingCity, Account.BillingPostalcode from Contact where Email =:email];
            }
             system.debug('ContactList---'+duplicateContactList);
             count = duplicateContactList.size();
             system.debug('Count---'+count);
             if(count == 0)
             {
                return False;
             } 
             else return True;
        }
            
       public PageReference navigateBack(){
           PageReference  Pg2 = new PageReference('/003/o');
            return Pg2;
       }
       
      /* public PageReference refreshPage(){
           PageReference  Pg2 = new PageReference('/apex/duplicateContactSearchPage');
            return Pg2;
       } */
       
       Public PageReference LoadContact(){
        String str = Apexpages.currentpage().getparameters().get('ContactId');
            PageReference  Pg1 = new PageReference('/'+str);
            return Pg1; 
        }
        
       /* Public PageReference LoadContact(){
        String str = Apexpages.currentpage().getparameters().get('ContactId');
            PageReference  Pg1 = new PageReference('/'+ContactList[0].id);
            return Pg1; 
        }  */
        
        
}
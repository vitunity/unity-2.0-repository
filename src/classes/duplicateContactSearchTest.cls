@isTest
public class duplicateContactSearchTest{
     static testMethod void myUnitTest() 
     {
        Account testAccount = new Account(Name = 'TEST_ACCOUNT001', Country__c = 'India');
        Insert testAccount;

        Contact con = new contact();
        
             con.firstname = 'Partner';
             con.Lastname = 'Contact';
             con.email = 'partner@contact.com';
             con.phone = '1234567891';
             con.MobilePhone = '1234567891';
             con.MailingStreet = 'test';
             con.MailingState = 'CA';
             con.MailingPostalCode = '12345';
             con.MailingCountry = 'USA';
             con.Account = testAccount;
             con.Registered_Portal_User__c = True;
             Insert Con;
             
         ApexPages.StandardController controller = new ApexPages.StandardController(con);
         
             duplicateContactSearch Con1 = new duplicateContactSearch(controller);
             Con1.con.Email = con.email;
             Con1.duplicateSearch();
             Con1.navigateBack();
             Con1.LoadContact();
             
             Con1.con.Email = 'test@mail.com';
             Con1.duplicateSearch();

             boolean check = duplicateContactSearch.isContactDuplicateByEmail(null, 'partner@contact.com');
             System.assert(check);
                      
     }
     
  }
/*
@Author: Ajinkya Raut
@Description: Libra Phase II (Create opportunities bases on follow up requested and Installed Product removed flags) 
              Handler for insertNewOpportunity on Proof of Notification
*/
public Class insertNewOpportunityHandler {

    public Integer count = 0;

    public void insertNewOpportunity(list < PON__c > PONList) {

        List < PON__c > opps = [SELECT Name, (SELECT id FROM Opportunities__r) FROM PON__c Where Id in :PONList];

        for (PON__c p: opps) {            
            count = p.Opportunities__r.size();          
        }

        if (count == 0) {
            count = 1;
        } else {
            count = count + 1;
        }
                
        List < Opportunity > listOppor = new List < Opportunity > ();

        List < PON__c > PONListItr = [Select Follow_up_Requested__c, Installed_Product_Removed__c, Installed_Product__r.SVMXC__Company__r.Name,
                                        Installed_Product__r.SVMXC__Company__r.OwnerId, Installed_Product__r.SVMXC__Company__r.CurrencyIsoCode, 
                                        Installed_Product__r.SVMXC__Company__r.District_Sales_Manager__c, Installed_Product__r.SVMXC__Company__r.Country__c, 
                                        Installed_Product__r.SVMXC__Company__r.Id, Installed_Product__r.SVMXC__Company__r.District_Sales_Manager__r.id
                                      FROM PON__c Where Id in :PONList];
        String OppName = ' - Libra II ';
        Date expectedCloseDate = Date.newInstance(2018, 9, 27);

        LostFiscalYearRecursive.run = false;

        for (PON__c proofOfNotification: PONListItr) {
        
            if (proofOfNotification.Installed_Product_Removed__c != true) {
                
                List<Contact> conList = [Select Id, Name From Contact where AccountId =:proofOfNotification.Installed_Product__r.SVMXC__Company__c]; 
                           
                Opportunity oppNew = new Opportunity();
                
                String Name = '';
                
                if(count == 1){
                    Name = proofOfNotification.Installed_Product__r.SVMXC__Company__r.Name + OppName; // + count;
                }else{
                    Name = proofOfNotification.Installed_Product__r.SVMXC__Company__r.Name + OppName + '- ' + count; 
                }
                
                if (proofOfNotification.Follow_up_Requested__c == true) {
                    oppNew.Name = Name;
                    oppNew.StageName = '1 - QUALIFICATION (N/A)';
                }
                
                else if (proofOfNotification.Installed_Product_Removed__c != true &&
                            proofOfNotification.Follow_up_Requested__c != true  ) {
                   
                }
                
                oppNew.AccountId = proofOfNotification.Installed_Product__r.SVMXC__Company__r.Id;
                
                if (conList.size() > 0) {
                    oppNew.Primary_Contact_Name__c   = conList[0].id;   
                }else{
                    oppNew.Primary_Contact_Name__c   = ''; 
                }
                            
                oppNew.CloseDate = expectedCloseDate;
                oppNew.Account_Sector__c = 'Private';
                oppNew.Payer_Same_as_Customer__c = 'Yes';
                oppNew.CurrencyIsoCode = proofOfNotification.Installed_Product__r.SVMXC__Company__r.CurrencyIsoCode;
                oppNew.Deliver_to_Country__c = proofOfNotification.Installed_Product__r.SVMXC__Company__r.Country__c;
                oppNew.Type = 'LIBRA II';
                oppNew.OwnerId = proofOfNotification.Installed_Product__r.SVMXC__Company__r.District_Sales_Manager__r.id;
                oppNew.Proof_Of_Notification__c = proofOfNotification.id;

                listOppor.add(oppNew);
            }
        }

        if (listOppor.size() > 0) {
            insert listOppor;
        }
    }
}
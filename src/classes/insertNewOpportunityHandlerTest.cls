@isTest
private class insertNewOpportunityHandlerTest {
    static testMethod void validateinsertNewOpportunityHandler() {
        
        Account acct = TestUtils.getAccount();
        acct.AccountNumber = 'Service1212';
        acct.CurrencyISOCode = 'USD';
        acct.BillingStreet = 'TEST';
        acct.BillingCity = 'TEST';
        acct.BillingState = 'TEST';
        acct.BillingPostalCode = '11111';
        acct.BillingCountry = 'USA';
        insert acct;
        
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
        ip.Name = 'H192192';
        ip.SVMXC__Status__c = 'Installed';
        ip.Marked_For_Replacement__c = false;
        ip.SVMXC__Company__c = acct.Id;
        insert ip;
          
        Contact con = TestUtils.getContact();
        con.AccountId = acct.Id;
        insert con;       

        PON__c pon = new PON__c();
        pon.Installed_Product__c = ip.id;
        pon.PCSN__c = 'H270854';
        pon.Follow_up_Requested__c = true;
        insert pon;
        
        pon.Follow_up_Requested__c = true;
        //pon.Installed_Product_Removed__c = true;
        
        
        Test.startTest();
        update pon;
        Test.stopTest();

    }
}
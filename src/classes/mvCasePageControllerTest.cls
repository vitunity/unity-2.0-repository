@isTest (seeAllData=true)
private class mvCasePageControllerTest {

    public static Regulatory_Country__c regcount ;
    public static Contact_Role_Association__c cr;
    public static Profile pAdmin;
    public static User u;
    public static Contact con;
    public static Account a;
    public static Customer_Agreements__c agr;
    public static Event_Webinar__c Webinar;

    public static Account account1;
    public static Contact contact1;
    public static User newUser;
    public static Case case1;
    public static CaseComment newCaseComment;
    public static Attachment attachmntfile;
    public static EmailMessage emailMsg;

    public static User thisUser;

    public static UserRole rol;

    public static SVMXC__Installed_Product__c objIP;
    public static Product_Version__c pv;
    static
    {
        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];

        pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        rol = [select id from UserRole WHERE Name like '%partner%' LIMIT 1];     //like \'%' + searchText + '%\'       

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

        a = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert a;  
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = a.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', MailingState='CA', Phone = '12452234',
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        //Creating a running user with System Admin profile
        insert con;

        System.runAs(thisUser) {
            newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                username='test_user@testclass.com', isActive = true, ContactId = con.Id); //, UserRoleid = rol.Id); 
            insert newUser;
       }

        Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        case1 = new Case(ContactId = con.Id, AccountId = a.Id, RecordTypeId = RecType,status = 'new',Subject= 'case Subject', Priority = 'Low',
                        Product_System__c = 'Test Prod System',  Date_of_Event__c = System.Today(), Date_Reported_to_Varian__c = System.Today());
        insert case1;

        newCaseComment = new CaseComment(ParentId = case1.Id, CommentBody ='Comments', isPublished = true);
        insert newCaseComment;

        attachmntfile = new Attachment();
        attachmntfile.ParentId = case1.Id;
        String base64Data = EncodingUtil.urlDecode('blah blah blah', 'UTF-8');
        attachmntfile.Body = EncodingUtil.base64Decode(base64Data);
        attachmntfile.Name = 'file1';
        attachmntfile.ContentType = 'text';
        insert attachmntfile;

        regcount = new Regulatory_Country__c();
        regcount.Name = 'Test';
        regcount.RA_Region__c ='NA';
        regcount.Portal_Regions__c ='N.America,Test';
        insert regcount;

        cr = new Contact_Role_Association__c();
        cr.Account__c= a.Id;
        cr.Contact__c = newUser.Contactid;
        cr.Role__c = 'RAQA';
        //insert cr;

           

    }


    @isTest static void getCustomLabelMapTest() {
        Test.startTest();
            mvCasePageController.getCustomLabelMap('EN');
        Test.stopTest();
    }
    
    @isTest static void setCaseControllerTest() {
        Test.startTest();
            System.runAs(newUser) {
                mvCasePageController.setCaseController();
            }
        Test.stopTest();
    }

    @isTest static void getAccOptionsTest() {
        Test.startTest();
            System.runAs(newUser) {
                mvCasePageController.getAccOptions();
            }
        Test.stopTest();
    }

    @isTest static void getProductGroupOptionsTest() {
        Test.startTest();
            System.runAs(newUser) {
                mvCasePageController.getProductGroupOptions(a.Id);
            }
        Test.stopTest();
    }

    @isTest static void getAccountListTest() {
        Test.startTest();
            System.runAs(newUser) {
                mvCasePageController.getAccountList();
            }
        Test.stopTest();
    }


    @isTest static void getStatusOptionsTest() {
        Test.startTest();
            mvCasePageController.getStatusOptions('open');
            mvCasePageController.getStatusOptions('closed');
            mvCasePageController.getStatusOptions('all');
        Test.stopTest();
    }

    @isTest static void getproductListTest() {
        Test.startTest();
            mvCasePageController.getproductList();
        Test.stopTest();
    }   

    @isTest static void getPriorityListTest() {
        Test.startTest();
            mvCasePageController.getPriorityList();
        Test.stopTest();
    }   

    @isTest static void getcaseListTest() {
        Test.startTest();
            System.runAs(newUser) {
                mvCasePageController.getcaseList('open', a.Id, 'open', 'Test Prod System', 'Subject', false, true, false, case1, 5, 10, '2018/01/01', '2018/03/03');
                mvCasePageController.getcaseList('closed', a.Id, 'closed', 'Test Prod System', 'Subject', false, true, false, case1, 5, 10, '2018/01/01', '2018/03/03');
            }
        Test.stopTest();
    }

    @isTest static void saveCaseTest() {
        Test.startTest();
            System.runAs(thisUser) {
                Product2 pr = new product2(name = 'Acuity',Product_Group__c = 'Acuity');
                insert pr;

                objIP = new SVMXC__Installed_Product__c(Name='H14072', SVMXC__Status__c ='Installed', SVMXC__Company__c = a.id, SVMXC__Product__c = pr.Id);
                insert objIP;     

                pv = new Product_Version__c(product__c = pr.id);
                insert pv;  
                Case newCaseCreate = new Case();                
                mvCasePageController.saveCase(newCaseCreate, 'H14072', 'subject', 'versionId', 'desc', 'prod', 'file1', 'blah blah blah', 'text');
            }
        Test.stopTest();
    }

    @isTest static void setMVCaseDetailControllerTest1() {
        Test.startTest();
            mvCasePageController.setMVCaseDetailController(case1.Id, 'Details');
        Test.stopTest();
    }   
    
    @isTest static void setMVCaseDetailControllerTest2() {
        Test.startTest();
            mvCasePageController.setMVCaseDetailController(case1.Id, 'WorkOrder');
        Test.stopTest();
    } 

    @isTest static void setMVCaseDetailControllerTest3() {
        Test.startTest();
            mvCasePageController.setMVCaseDetailController(case1.Id, 'Recommendation');
        Test.stopTest();
    } 

    @isTest static void setMVCaseDetailControllerTest4() {
        Test.startTest();
            mvCasePageController.setMVCaseDetailController(case1.Id, 'desc');
        Test.stopTest();
    }         

    @isTest static void closeCaseTest() {
        Test.startTest();
            System.runAs(thisUser) {
                mvCasePageController.closeCase(case1.Id, 'Customer Canceled');
            }

        Test.stopTest();
    }

    @isTest static void saveEditCaseTest() {
        Test.startTest();
            System.runAs(thisUser) {    
                mvCasePageController.saveEditCase(case1.Id, 'case CommentInput', 'file1', 'blah blah blah', 'text');
            }

        Test.stopTest();
    }

    @isTest static void getCaseActivityTest() {
        Test.startTest();
        System.runAs(thisUser) { 
        emailMsg = new EmailMessage(subject = 'Test subject for task', TextBody = 'Test description for task', 
            ParentId = case1.Id,FromAddress = 'test@varian.com',toaddress = 'test1@gmail.com');
        insert emailMsg;
        
            mvCasePageController.getCaseActivity(emailMsg.Id);
        Test.stopTest();
        }
    } 
    
    @isTest static void getDefaultAccountValueTest() {
        test.startTest();
        user u = [Select Id, contactId, isActive from User Where isActive =: true Limit 1];
        system.runAs(u) {
                mvCasePageController.getDefaultAccountValue();
        }
        test.stopTest();
    }
    
    @isTest static void getStatusListTest() {
        test.startTest();
        mvCasePageController.getStatusList('Open');
        test.stopTest();
    }
    
    @isTest static void AssignCommentTEST(){
        Test.startTest();
            mvCasePageController.AssignComment assign = new mvCasePageController.AssignComment();
        test.stopTest();
    }
    
    @isTest static void finalCommentsTest() {
        Test.startTest();
            
        test.stopTest();
    }

}
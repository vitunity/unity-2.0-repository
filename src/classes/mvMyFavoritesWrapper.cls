/*
* Author: Harvey
* Created Date: 22-Jul-2017
* Project/Story/Inc/Task : My varian lightning project 
* Description: This will be used for my varian website MyAccount Myfavorites options 
*/
public class mvMyFavoritesWrapper
{
    @AuraEnabled
    public String optionLabel;
    
    @AuraEnabled
    public String optionTranslatedLabel;
    
    @AuraEnabled
    public String imgIconUrl;
    
    @AuraEnabled
    public String optionValue;
    @AuraEnabled
    public boolean isChecked;
    @AuraEnabled
    public String demo { get; set; }

    @AuraEnabled
    public String navigationLink{get;set;}

    public mvMyFavoritesWrapper(String optionLabel, boolean isChecked, string optionValue,String languageObj)
    {
        this.optionLabel = optionLabel;
        if(String.isBlank(languageObj)){
            languageObj = 'en';
        }
        if(optionLabel == 'Certificate Manager'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/certificate-manager-myaccount.png';
          navigationLink = 'https://webapps.varian.com/certificate/en';
        }
        if(optionLabel == 'Training Courses'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/couse-listing-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/trainingcourses?lang='+languageObj;
        }
        if(optionLabel == 'Meetings'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/events-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/events?lang='+languageObj;
        }
        if(optionLabel == 'Learning Center'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/learning-center-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/trainingcourses?lang='+languageObj;
        }
        if(optionLabel == 'OncoPeer Community'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/oncopeer.png';
          navigationLink = 'https://www.OncoPeer.com';
        }
        if(optionLabel == 'Product Ideas'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/productidea-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/productideas?lang='+languageObj;
        }
        if(optionLabel == 'Support Cases'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/support-case-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/supportcases?lang='+languageObj;
        }
        if(optionLabel == 'Webinars'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/traning-couse-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/webinars?lang='+languageObj;
        }
        if(optionLabel == 'Varian Marketplace'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/vmarket-myaccount.png';
          navigationLink = Label.MvSiteURL+'/vMarketLogin';
        }
         if(optionLabel == 'Treatment Planning'){
          imgIconUrl = Label.MvSiteURL+'/resource/MvBrandingResource/img/vmarket-myaccount.png';
          navigationLink = Label.MvSiteURL+'/s/treatmentplanningservices?lang='+languageObj;
        }
        this.optionValue = optionValue;
        this.isChecked = isChecked;
        this.demo = 'acme';
    }


    public mvMyFavoritesWrapper(String optionLabel, string optionValue)
    {
        this.optionLabel = optionLabel;
        this.optionValue = optionValue;
    }    
}
/*
* Author: Naren Yendluri
* Created Date: 17-Sep-2017
* Project/Story/Inc/Task : MyVarian lightning 
* Description: This will be used for MyVarian website Product Ideas page
*/

public class mvProductIdeas {
    
    private static integer counter=0;
    private static integer list_size=10;
    public static integer total_size{get;set;}
    //  public boolean newpostIdea{get;set;}
    public static boolean isExternalUser{get;set;}
    public static boolean isNonVarianUser{get;set;}
    
    static set<string> setExistingProducts;
    static Integer IdeaDescriptionLength = 200;
    static set<Id> setFilteredIds = new set<Id>();
    static string ZoneId {get;set;}
    static string ZoneName {get;set;}
    public static Idea newIdeaObj{get;set;}
    
    public static string strIdeaPG{get;set;}
    public static string strSortBy{get;set;}
    public static string strIdeaStatus{get;set;} 
    public static string Idea_TypeSearch{get;set;}
    public static boolean viewMyIdea{get;set;}
    
    public static user cUser{get;set;}
    
    public static void Component1Cntrl(){
        isExternalUser = false;
        // newpostIdea = false;
        
        //  panel = 'detail';
        cUser = [select Id,Name,ContactId,AccountId,IsPortalEnabled from User where id=:userinfo.getUserId()];
        isExternalUser = cUser.IsPortalEnabled;        
        // viewMyIdea = false;
        
        List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
        system.debug('::-:->'+lstc);
        if(lstc.size() > 0){
            ZoneId = lstc[0].Id;
            ZoneName = lstc[0].Name;
        }
        //newIdeaObj = new Idea();
        //NewIdeaAttach = new Attachment();
        // newIdeaObj.CommunityId = ZoneId;
    }
    
    @AuraEnabled
    public static Boolean checkIsExternalUser(){
        try{
            isExternalUser = false;
            cUser = [select Id,Name,ContactId,AccountId,IsPortalEnabled from User where id=:userinfo.getUserId()];
            isExternalUser = cUser.IsPortalEnabled; 
            system.debug('#### debug isExternalUser = ' + isExternalUser);
        }catch(Exception ex){
            system.debug(ex.getMessage());
        }
        
        return isExternalUser;
    }

    @AuraEnabled
    public static Boolean checkNonVarianUser(){
        try{
            isNonVarianUser = false;
            List<User> listVarianUser = [select Id,Name,ContactId,AccountId,IsPortalEnabled from User where id=:userinfo.getUserId()
                AND email like '%varian.com'];

            if(listVarianUser.size() == 0) {
                isNonVarianUser = true;
            }
            system.debug('#### debug isNonVarianUser = ' + isNonVarianUser);
        }catch(Exception ex){
            system.debug(ex.getMessage());
        }
        
        return isNonVarianUser;
    }    

    /* Check authentication */
    @AuraEnabled
    public static boolean isLoggedIn(){
        User u = [SELECT Id, LastName, Email, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        if(u.lastname == 'Site Guest User'){
            return false;
        }else{
            return true;
        }
        
    }    

    @AuraEnabled
    public static map<String,String> getCustomLabelMap(String languageObj){
        return MvUtility.getCustomLabelMap(languageObj);
    }
    
    @AuraEnabled
    public static List<picklistEntry> getPicklistValues(String fieldName,String sObjectName){
        Schema.SObjectType sObj = Schema.getGlobalDescribe().get(sObjectName);         
        Schema.DescribeSObjectResult sObjRes = sObj.getDescribe();
        Schema.DescribeFieldResult sObjFieldRes = sObjRes.fields.getMap().get(fieldName).getDescribe();
        List<Schema.PicklistEntry> lstPlE = sObjFieldRes.getPicklistValues();
        List<picklistEntry> entryList = new List<picklistEntry>();
        for(Schema.PicklistEntry ple : lstPlE){
            entryList.add(new picklistEntry(ple.getLabel(),ple.getValue()));
        }
        return entryList;
    }
    
    // public static string strSortBy{get;set;}
    @AuraEnabled    
    Public static List<picklistEntry> getIdeaSortBy(){
        string strSortBy = 'Recent';        
        List<picklistEntry> lstDocTypes = new List<picklistEntry>();
        
        lstDocTypes.add(new picklistEntry('Recent','Recent'));
        lstDocTypes.add(new picklistEntry('Popular','Popular'));
        lstDocTypes.add(new picklistEntry('Just Released','JustReleased'));        
        
        return lstDocTypes;   
    }
    
    // public static string strIdeaPG{get;set;}
    @AuraEnabled
    public static List<picklistEntry> getIdeaProductGroup() {
        //string ZoneId;
        setExistingProducts = new set<string>();
        string cUId = userinfo.getUserId();
        
        Component1Cntrl();
        
        for(Idea i : [select Id,Categories from Idea 
                      where title <> null and 
                      (Status <> 'Converted to Case' and Status <> 'Internal' and  Status <> 'Duplicate') 
                      and communityId =: ZoneId and (Status <> 'New' or  (Status = 'New' and createdById =: cUId)) 
                      and (Status <> 'Pending Customer Input' or  (Status = 'Pending Customer Input' and createdById =: cUId)) 
                      and (isMerged = false or  (isMerged = true and createdById =: userinfo.getUserId())) limit 50000
                     ]){
                         if(i.Categories <> null){
                             for(string s : (i.Categories).split(';')){
                                 setExistingProducts.add(s);
                             }
                         }
                     }
        
        List<picklistEntry> options = new List<picklistEntry>();
        // options.add(new picklistEntry('All','All'));
        string strIdeaPG = 'All';
        for(string s : setExistingProducts){        
            options.add(new picklistEntry(s,s));
        }
        options.sort();
        // options.addAll(options1);
        return options;
    }
    
    // public static string strIdeaStatus{get;set;} 
    @AuraEnabled
    public static List<picklistEntry> getIdeaStatus() {
        List<picklistEntry> options = new List<picklistEntry>();
        // options.add(new picklistEntry('All','All'));
        string strIdeaStatus = 'All';
        List<WIdeaStatus> lWrapper = new List<WIdeaStatus>();
        for(picklistEntry s : getPicklistValues('Status','Idea')){
            if(s.pvalue <> 'Duplicate' && s.pvalue <> 'Converted to Case' &&  s.pvalue <> 'Internal')            
                lWrapper.add(new WIdeaStatus(s.pvalue));
        }
        lWrapper.sort();
        for(WIdeaStatus s:lWrapper){
            options.add(new picklistEntry(s.name,s.name));
        }
        return options;
    }
    @AuraEnabled
    public static List<picklistEntry> getNewIdeaProductGroup() {
        List<picklistEntry> options = new List<picklistEntry>();
        // options.add(new picklistEntry('All','All'));
        string strIdeaStatus = 'All';
        List<WIdeaStatus> lWrapper = new List<WIdeaStatus>();
        for(picklistEntry s : getPicklistValues('Categories','Idea')){            
                lWrapper.add(new WIdeaStatus(s.pvalue));
        }
        lWrapper.sort();
        for(WIdeaStatus s:lWrapper){
            options.add(new picklistEntry(s.name,s.name));
        }
        return options;
    }
    
    @AuraEnabled
    public static ProductIdeaWrap getProductIdea(string sortBy, string ideaPG, string ideaStatus, 
                                                    string Idea_Search, Boolean viewMyIdeas,
                                                    Integer recordsOffset, Integer totalSize, Integer pageSize)
    {
        if(string.isBlank(sortBy)){
            strSortBy = 'Recent';
        }else{
            strSortBy = sortBy;  
        }
        
        strIdeaPG = ideaPG;
        strIdeaStatus = ideaStatus;
        Idea_TypeSearch = Idea_Search;
        viewMyIdea = viewMyIdeas;
        
        List<wProductIdea> lstPIdea = new List<wProductIdea>();
        string cUId = userinfo.getUserId();
        //string ZoneId;
        Component1Cntrl();
        IdeaSearch();
        
        string query = 'Select id,title,body,Release_Version__c,Categories,Votescore, votetotal, recordtypeid, Status ,Numcomments, lastreferencedDate, ';
        query += ' LastviewedDate, ismerged, IsHTML ,CreatorFullPhotoUrl, AttachmentName,createdDate,createdBy.Name,';
        //query += '(select Id from Comments where CreatedById =: cUId), ';
        query += '(select Id from Comments), ';
        query += '(select Id,Type from Votes where CreatedById =: cUId and isDeleted = false) from Idea where title <> null and ';
        query += '(Status <> \'Converted to Case\' and Status <> \'Internal\' and  Status <> \'Duplicate\') and communityId =: ZoneId and (Status <> \'New\' or  (Status = \'New\' and createdById =: cUId)) and (Status <> \'Pending Customer Input\' or  (Status = \'Pending Customer Input\' and createdById =: cUId)) and ';
        query +=  '(isMerged = false or  (isMerged = true and createdById =: cUId))';
        if(strIdeaPG <> 'All' && strIdeaPG <> '' && strIdeaPG <> null){
            query += ' and Categories INCLUDES (\''+strIdeaPG+'\')';
        }
        if(string.isNotBlank(strIdeaStatus) && strIdeaStatus <> 'All'){
            query += ' and Status =: strIdeaStatus';
        }

        if(Idea_TypeSearch <> '' && Idea_TypeSearch <> null){
            query += ' and Id IN : setFilteredIds ';
        } 
        if(viewMyIdea == true){         
            query += ' and createdById =: cUId';
        }
        query += ' and title <> \'Include prescription status in Visit tab\' ';
        
        if(strSortBy == 'JustReleased')query += ' and  Status = \'Released\'';
        else if(strSortBy == 'Recent')query += ' Order By createdDate Desc ';
        else if(strSortBy == 'Popular')query += ' Order By votetotal Desc';
        
        system.debug('RKSS:169:'+strIdeaPG+'-----'+strIdeaStatus+'-----'+Idea_TypeSearch+'---'+setFilteredIds);
        
        findTotalRecord(query);
        
        if(recordsOffset != null){
            counter = integer.valueof(recordsOffset);
        }
        if(pageSize != null){
            list_size = integer.valueof(pageSize);
        }
        
        query += ' limit :list_size offset :counter';
        
        try{
            system.debug('RKSS:175:'+query);

            system.debug('#### debug setFilteredIds = ' + setFilteredIds);
            system.debug('#### debug strIdeaStatus = ' + strIdeaStatus);
            system.debug('#### debug ZoneId = ' + ZoneId); 
            system.debug('#### debug strIdeaPG = ' + strIdeaPG); 

            List<Idea> lstIdea = database.query(query);
            system.debug('RKSS:177:'+lstIdea);
            
            for(Idea i :lstIdea ){
                string dayCount = getDaysAgo(i.createdDate);
                datetime tdate = system.today();
                datetime cdate = i.createdDate;
                
                boolean upVote = false;
                boolean downVote = false;
                boolean isVoteEnable = true;
                if(Idea_TypeSearch <> '' && Idea_TypeSearch <> null){
                    if((i.title) <> null && (i.title).containsIgnoreCase(Idea_TypeSearch)){
                        i.title = highlightedText(i.title,Idea_TypeSearch,'<span style="background-color:yellow">','</span>');
                    }
                    if((i.body) <> null && (i.body).containsIgnoreCase(Idea_TypeSearch) ){
                        i.body = highlightedText(i.body,Idea_TypeSearch,'<span style="background-color:yellow">','</span>');
                    }
                }
               // system.debug('RKSS:207:'+i.title+'::'+i.body);
                string shortDescription = i.body;
                if(i.votes <> null && i.votes.size() > 0){
                    isVoteEnable = false;
                    Vote v = i.votes[0];
                    if(v.Type == 'up')upVote = true;
                    if(v.Type == 'down')downVote = true;
                }
                Integer numComment = i.Comments.size();
                
                if(i.body <> null && string.valueOf(i.body).length() > IdeaDescriptionLength){
                    shortDescription = (i.body).substring(0,IdeaDescriptionLength);
                    //shortDescription = shortDescription+'....&nbsp;&nbsp;<a id="\''+i.id+'\'" href="javascript:void(0)" onclick="{!c.showMoreDescription}" style="color: #0070d2;">Show more</a>';
                    //shortDescription = shortDescription+'....&nbsp;&nbsp;<a href="javascript:void(0)" onclick="showMoreDescription(\''+i.id+'\')" style="color: #0070d2;">Show more</a>';
                }
                lstPIdea.add(new wProductIdea(i,i.title,i.body,shortDescription,numComment,dayCount,i.votetotal,i.CreatorFullPhotoUrl,i.createdBy.Name,upVote,downVote,isVoteEnable));
            }
        }
        catch(Exception e){ 
            system.debug('RKSS:error:'+e.getLineNumber()+'--'+e.getMessage());
        }
        system.debug('RKSS:196:'+lstPIdea);
        ProductIdeaWrap objProductIdeaWrap = new ProductIdeaWrap(lstPIdea, total_size);
        return objProductIdeaWrap;
        //return lstPIdea;
    }
    @AuraEnabled
    public static Boolean makeVote(string IdeaId,string Type ){
        boolean status = false;
        try{
            if(string.isNotBlank(IdeaId)){
                Vote v = new Vote();
                v.ParentId = IdeaId;
                v.Type = Type;
                insert v;
                status = true;
            }
        }catch (exception ex){
            system.debug('makeVote Exception --->'+ex.getMessage());
        }
        return status;
    }
    @AuraEnabled
    public static boolean addComment(string ideaId, string commentBody){
        boolean status = false;
        try{
           if(string.isNotBlank(ideaId) && string.isNotBlank(commentBody)){
            IdeaComment newcomment = new IdeaComment(IdeaId = ideaId, commentBody=commentBody); 
            insert newcomment;
               status= true;
        } 
        }catch(exception ex){
            
        }
        return status;
            
    }
    
    @AuraEnabled
    public static List<wProductIdeaComment> getIdeaComments(string hdnIdeaId){    

         system.debug('#### debug hdnIdeaId = ' + hdnIdeaId);

        List<wProductIdeaComment> lstMIdeaComment = new List<wProductIdeaComment>();
        List<IdeaComment> lstIdeaComment = new List<IdeaComment>();
        lstIdeaComment = [Select Id,CommentBody,createdBy.Name,CommunityId,CreatorFullPhotoUrl,
                          CreatorName,CreatorSmallPhotoUrl,IdeaId,IsHtml,UpVotes,createdDate 
                          from IdeaComment 
                          where IdeaId =: hdnIdeaId
                          order by CreatedDate desc];
                          //where IdeaId =: hdnIdeaId and createdBy.Id =: userinfo.getUserId()];

        system.debug('#### debug lstIdeaComment = ' + lstIdeaComment);


        for(IdeaComment i : lstIdeaComment){
            string dayCount = getDaysAgo(i.createdDate);
            lstMIdeaComment.add(new wProductIdeaComment(i,dayCount));
        }

        system.debug('#### debug lstMIdeaComment = ' + lstMIdeaComment);

        return lstMIdeaComment;
       // Idea i = [Select id,title,body from Idea where Id=: hdnIdeaId];
       // wProductCommentDetail CommentObj = new wProductCommentDetail(i.Id,i.title,i.body,lstMIdeaComment);
    }

    @AuraEnabled
    public static Idea getIdea(string hdnIdeaId){    

        system.debug('#### debug hdnIdeaId = ' + hdnIdeaId);

        List<Idea> lstIdea = new List<Idea>();
        lstIdea = [Select id,title,body,Release_Version__c,Categories,Votescore, votetotal, 
                          recordtypeid, Status, Product_Version__c, createdDate,createdBy.Name,
                   		  Contact__c, Contact__r.Name
                          from Idea where Id =: hdnIdeaId];
        system.debug('#### debug lstIdea = ' + lstIdea);

        return lstIdea[0];
    }

    @AuraEnabled
    public static string saveProductIdea(Idea newIdeaObj, String fileName, String base64Data, String contentType){
        system.debug('saveProductIdea start');
        Attachment NewIdeaAttach = new Attachment();
       // string base64Data1 = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        //Idea newIdeaObj = new Idea();
        try{
        
        Component1Cntrl();
        if(newIdeaObj.Id == null)
        newIdeaObj.CommunityId = ZoneId;
        newIdeaObj.status = 'New';
        
        string EclipsStatus = 'New';
        
        if(isExternalUser){
            if(cUser.AccountId <> null)
                newIdeaObj.Account__c = cUser.AccountId;
            if(cUser.ContactId <> null)
                newIdeaObj.Contact__c = cUser.ContactId;
        }else{
            if(newIdeaObj.Contact__c <> null){
                Contact conObj = [select Id,AccountId from Contact where Id =: newIdeaObj.Contact__c];
                newIdeaObj.Account__c = conObj.AccountId;
            }
        }
        
            if(NewIdeaObj.categories <> null){
                set<string> setCategory = new set<string>();
                for(string t : (NewIdeaObj.categories).split(';')){
                    setCategory.add(t);
                }
                if(setCategory.contains('Eclipse')){
                    newIdeaObj.status = EclipsStatus;
                }
            }
            //newIdeaObj.Title = 'Idea';
            upsert newIdeaObj;
            system.debug('newIdeaObj Id ->'+newIdeaObj.Id);
            
            if(newIdeaObj.Id <> null && string.isNotBlank(base64Data)){
               // if(newIdeaObj.Id <> null && NewIdeaAttach.body <> null){
                
                Idea_Attachment__c IdeaAttachObj = new Idea_Attachment__c(Idea__c = newIdeaObj.Id );
                insert IdeaAttachObj;
                system.debug('IdeaAttachObj Id ->'+IdeaAttachObj.Id);
                //attachment
                NewIdeaAttach.parentId = IdeaAttachObj.Id;
                if(NewIdeaAttach.parentId  <> null){
                     base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                    NewIdeaAttach.Body = EncodingUtil.base64Decode(base64Data);
                    NewIdeaAttach.Name = fileName;
                    NewIdeaAttach.ContentType = contentType;
                    insert NewIdeaAttach;
                    system.debug('NewIdeaAttach Id ->'+NewIdeaAttach.Id);
                }   
            }
        }catch(exception ex){
            system.debug(ex.getMessage());
            return '' ;
        }
        return newIdeaObj.Id ;
        //end
    }
    public static void IdeaSearch(){
    setFilteredIds.clear();
       // panel = 'detail'; 
        system.debug('RKSS:275: '+Idea_TypeSearch);
        if(string.isNotBlank(Idea_TypeSearch)){
            List<Idea> lstFIdea = [Select id,title,body from Idea where title <> null and Status <> 'Converted to Case' and Status <> 'Duplicate' and Status <> 'Internal' and communityId =: ZoneId and  (Status <> 'New' or  (Status = 'New' and createdById =: userinfo.getUserId())) and  (Status <> 'Pending Customer Input' or  (Status = 'Pending Customer Input' and createdById =: userinfo.getUserId())) and (isMerged = false or  (isMerged = true and createdById =: userinfo.getUserId()))];      
            system.debug('RKSS:276: '+lstFIdea);
            for(Idea i : lstFIdea){
                if(((i.title) <> null && (i.title).containsIgnoreCase(Idea_TypeSearch)) || ((i.body) <> null && (i.body).containsIgnoreCase(Idea_TypeSearch)) ){
                    setFilteredIds.add(i.id);
                }
            }
        }
        system.debug('RKSS:285: '+setFilteredIds);
    }
    public static void findTotalRecord(string q){
        string cUId = userinfo.getUserId();
        total_size = database.query(q).size();
    }    
    public static string getDaysAgo(Datetime cDate){
        Date tDate = date.today();
        
        Date yDate = date.newInstance(tDate.year(),cDate.month(),cDate.day());
        
        Integer year =tDate.year()-cDate.year();
        if(tDate < yDate){
            year--;
        }
        Integer month = 0;
        for(Integer m = 0;m<12;m++){
            Date tempDt = date.newInstance(cDate.year()+year,cDate.month(),cDate.day());
            tempDt = tempDt.addMonths(m);
            if(tempDt > tDate){
                month--;
                break;
            }   
            month++;
        }
        
        Integer day = 0;
        for(Integer m = 0;m<31;m++){
            Date tempDt = date.newInstance(cDate.year()+year,cDate.month()+month,cDate.day());
            tempDt = tempDt.addDays(m);
            if(tempDt > tDate){
                day--;
                break;
            }   
            day++;
        }
        string rstr = 'Today';
        if(year > 0){rstr = year +' year';if(year > 1)rstr +='s';rstr+=' ago';}
        else if(month > 0){rstr = month +' month';if(month > 1)rstr +='s';rstr+=' ago';}
        else if(day > 0){rstr = day +' day';if(day > 1)rstr +='s';rstr+=' ago';}
        
        return rstr ;
    }
    Public static string highlightedText(string str, string repalcestr , string tagstart, string tagend){
        boolean isworking = true;
        string finalText = '';
        while(isworking){
            Integer startindex = str.indexOfIgnorecase(repalcestr,0);
            if(startindex == -1){finalText += str;  isworking = false;}
            else{
                finalText += str.substring(0,startindex)+tagstart+str.substring(startindex,startindex+repalcestr.length())+tagend;
                str = str.substring(startindex+repalcestr.length());
            }    
        }
        return finalText;
    }
    
    public class picklistEntry implements Comparable{
        @AuraEnabled public string label{get;set;}
        @AuraEnabled public string pvalue{get;set;}
        public picklistEntry(string text, string pvalue){
            this.label = text;
            this.pvalue = pvalue;
        }
        
        public Integer compareTo(Object compareTo) {
            picklistEntry compareToOppy = (picklistEntry)compareTo;
            Integer returnValue = 0;
            if (pvalue > compareToOppy.pvalue)
                returnValue = 1;
            else if (pvalue < compareToOppy.pvalue)
                returnValue = -1;        
            return returnValue;       
        }
    }    
    public class WIdeaStatus implements Comparable {
        public string name;
        public WIdeaStatus(string name){
            this.name=name;
        }
        public Integer compareTo(Object compareTo) {
            WIdeaStatus compareToOppy = (WIdeaStatus)compareTo;
            Integer returnValue = 0;
            if (name > compareToOppy.name)
                returnValue = 1;
            else if (name < compareToOppy.name)
                returnValue = -1;        
            return returnValue;       
        }
    }
    public class ProductIdeaWrap{      
        @AuraEnabled public list<wProductIdea> lstwProductIdea{get;set;}
        @AuraEnabled public integer totalSize{get;set;}
        public ProductIdeaWrap(list<wProductIdea> lstwProductIdea, integer totalSize){
            this.lstwProductIdea = lstwProductIdea;
            this.totalSize = totalSize;
        }
    }
    public class wProductIdea{      
        @AuraEnabled public Idea ideaObj{get;set;}
        @AuraEnabled public string title{get;set;}
        @AuraEnabled  public string Description{get;set;}
        @AuraEnabled public string ShortDescription{get;set;}
        @AuraEnabled public Integer CommentCount{get;set;}
        @AuraEnabled public string Daysago{get;set;}
        @AuraEnabled public decimal Points{get;set;}
        @AuraEnabled public string userURL{get;set;}
        @AuraEnabled public string UserName{get;set;}
        @AuraEnabled public boolean upVote{get;set;}
        @AuraEnabled public boolean downVote{get;set;}
        @AuraEnabled public boolean isVoteEnable{get;set;}
        
        public wProductIdea(Idea ideaObj, string title, string Description, string ShortDescription, Integer CommentCount, string Daysago, decimal Points, string userURL, string UserName,boolean upVote,boolean downVote, boolean isVoteEnable){
            this.ideaObj = ideaObj;
            this.title = title;
            this.Description = Description;
            this.ShortDescription = ShortDescription;
            this.CommentCount = CommentCount;
            this.Daysago = Daysago;
            this.Points = Points;
            this.userURL = userURL;
            this.UserName = UserName;
            this.upVote= upVote;
            this.downVote= downVote;
            this.isVoteEnable = isVoteEnable;
        }
    }
    public class wProductIdeaComment{      
        @AuraEnabled public IdeaComment CommentObj{get;set;}        
        @AuraEnabled public string Daysago{get;set;}
        
        public wProductIdeaComment(IdeaComment CommentObj, string Daysago){
            this.CommentObj = CommentObj;
            this.Daysago = Daysago;
        }
    }
}
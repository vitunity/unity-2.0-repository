@isTest(seeAllData=true)
private class mvProductIdeasTest {

    public static Account act;  
    public static Contact con;
    public static User user;

	public static Idea idea1;
	public static User thisUser;
	public static User newUser;
	public static UserRole rol;
	public static Profile pAdmin;

    static
    {

        thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        Profile pCus = [select id from profile where name='VMS MyVarian - Customer User'];
        User u;
 
        pAdmin = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        rol = [select id from UserRole LIMIT 1];           

        Recordtype  rtAct = [Select id from recordtype where developername = 'Site_Partner'];   

        act = new Account(name='test29990099',BillingPostalCode ='94530',BillingCity='San Jose',BillingCountry='USA',BillingState='CA',
                    BillingStreet='xyx',Country__c='USA', RecordTypeId = rtAct.Id, Distributor_Partner__c = 'Siemens'); 
        insert act;  
        con = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', Institute_Name__c = 'test29990099',
            MvMyFavorites__c='Events', AccountId = act.Id, MailingCity='San Jose', MailingCountry='USA', MailingPostalCode='94530', 
            MailingState='CA', Phone = '12452234',
            MailingStreet = 'xyx', RAQA_Contact__c=true, PasswordresetDate__c = System.Today());
        //Creating a running user with System Admin profile
        insert con;

        UserRole rl = [SELECT Id from UserRole Limit 1];

        System.runAs(thisUser) {
            newUser  = new User(alias = 'standt', Subscribed_Products__c=true,email='standarduser@testorg.com',emailencodingkey='UTF-8',  
                lastname='Testing',languagelocalekey='en_US',localesidkey='en_US', profileid = pCus.Id, timezonesidkey='America/Los_Angeles', 
                username='test_user@testclass.com', isActive = true, ContactId = con.Id);       //, UserRoleid = rol.Id); 
            insert newUser;
            Group group1 = [Select id from group where name = 'VMS MyVarian - Partners'];
            Groupmember gm2 = new groupmember(GroupId = group1.id, UserOrGroupId = newUser.Id);
            insert gm2;      	            
        }

    	List<Community> lstc = [select Id,Name from Community where Name = 'MyVarian Customer Ideas'];
    	idea1 = new Idea();
    	idea1.CommunityId = lstc[0].Id;       
    	idea1.Contact__c = con.Id;
    	idea1.categories = 'Esclipse';
    	idea1.Account__c = act.Id;
    	idea1.Title = 'title';
    	idea1.Status = 'Released';
    	//idea1.isMerged = false;
    	insert idea1;

    	Idea inserted = [SELECT Id, categories FROM Idea where id = : idea1.Id];
    	System.debug('#### debug idea inserted = ' + inserted);
    	mvProductIdeas.newIdeaObj = idea1;
    }
	
	@isTest static void Component1CntrlTest() {
		Test.startTest();
			mvProductIdeas.Component1Cntrl();
		Test.stopTest();
	}
	
	@isTest static void checkIsExternalUserTest() {
		Test.startTest();
			mvProductIdeas.checkIsExternalUser();
		Test.stopTest();
	}

	@isTest static void getCustomLabelMapTest() {
		Test.startTest();
			mvProductIdeas.getCustomLabelMap('EN');
		Test.stopTest();
	}	

	@isTest static void getPicklistValuesTest() {
		Test.startTest();
			mvProductIdeas.getPicklistValues('Industry', 'Account');
		Test.stopTest();
	}

	@isTest static void getIdeaSortByTest() {
		Test.startTest();
			mvProductIdeas.getIdeaSortBy();
		Test.stopTest();
	}	

	@isTest static void getIdeaProductGroupTest() {
		Test.startTest();
			mvProductIdeas.getIdeaProductGroup();
		Test.stopTest();
	}

	@isTest static void getIdeaStatusTest() {
		Test.startTest();
			mvProductIdeas.getIdeaStatus();
		Test.stopTest();
	}			

	@isTest static void getNewIdeaProductGroupTest() {
		Test.startTest();
			mvProductIdeas.getNewIdeaProductGroup();
		Test.stopTest();
	}	

	@isTest static void getProductIdeaTest() {
		Test.startTest();
			mvProductIdeas.getProductIdea('Recent','Esclipse', 'Released', 'title', false, 0,5,5);
			mvProductIdeas.getProductIdea('JustReleased','Esclipse', 'Released', 'title', false, 0,5,5);
			mvProductIdeas.getProductIdea('Popular','Esclipse', 'Released', 'title', false, 0,5,5);
		Test.stopTest();
	}	

	@isTest static void saveProductIdeaTest() {
		Test.startTest();
			mvProductIdeas.saveProductIdea(idea1, 'file1', 'blah blah blah', 'text');
		Test.stopTest();
	}

	@isTest static void makeVoteTest() {
		Test.startTest();
			mvProductIdeas.makeVote(idea1.Id, 'test');
		Test.stopTest();
	}

	@isTest static void addCommentTest() {
		Test.startTest();
			mvProductIdeas.addComment(idea1.Id, 'test');
		Test.stopTest();
	}

	@isTest static void getIdeaCommentsTest() {
		Test.startTest();
			mvProductIdeas.getIdeaComments(idea1.Id);
		Test.stopTest();
	}

	@isTest static void IdeaSearchTest() {
		Test.startTest();
			mvProductIdeas.IdeaSearch();
		Test.stopTest();
	}

	@isTest static void findTotalRecordTest() {
		Test.startTest();
			mvProductIdeas.findTotalRecord('select id from Community');
		Test.stopTest();
	}	

	@isTest static void getDaysAgoTest() {
		Test.startTest();
			mvProductIdeas.getDaysAgo(System.Now());
		Test.stopTest();
	}	


	@isTest static void highlightedTextTest() {
		Test.startTest();
			mvProductIdeas.highlightedText('test', 'test', 'test', 'test');
		Test.stopTest();
	}	
	
	@isTest static void Test1() {
		Test.startTest();
			//mvProductIdeas.wProductIdea = new mvProductIdeas.wProductIdea(idea1, 'title', 'desc', 'shortDesc', 1, '2 days', '2.2', 'wwww.yahoo.com', 'testUser', false, false, false);
		Test.stopTest();
	}	

	@isTest static void Test2() {
		Test.startTest();
			IdeaComment comO = new IdeaComment();
			//mvProductIdeas.wProductIdeaComment = new mvProductIdeas.wProductIdeaComment(comO, 'test');
		Test.stopTest();
	}	

}
public without sharing class productprofile
{
    public static string prodprofnumber;
    public static Id BundlerectypeId;
    public Integer rollupname = 0;
    public void beforeupdate(list<Review_Products__c> newlist)
    {
        BundlerectypeId = Schema.SObjectType.Review_Products__c.getRecordTypeInfosByName().get('Bundle').getRecordTypeId();
        for (Review_Products__c pp : [select Name_Seq__c from Review_Products__c order by Name_Seq__c desc limit 1])
        {
           prodprofnumber = pp.Name_Seq__c;
        }
        if (prodprofnumber != null || !String.isBlank(prodprofnumber))
        {
            rollupname = integer.valueOf(prodprofnumber);
            System.debug('***CM incr: '+rollupname);
        }
        for (Review_Products__c pp : newlist)
        {
            system.debug('***CM pp : '+pp);
            rollupname += 1;
            //String formattednbr = String.Format("%08d", incr);
            String text = String.valueOf(rollupname);
            while (text.length() < 8)  
            { 
                text = '0' + text; 
            }
            System.debug('***CM incr: '+rollupname);
            System.debug('***CM incr str : '+text);
            if (trigger.isBefore && trigger.isInsert)
            {
                if (pp.RecordTypeId != BundlerectypeId)
                {
                    pp.name = 'PP-' + text;
                }
                else
                {
                    pp.name = 'PB-' + text;
                }
    
            }
        }
    }
}
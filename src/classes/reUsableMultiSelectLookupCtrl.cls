public class reUsableMultiSelectLookupCtrl {

    @AuraEnabled
    public static list<chow_team_member__c> fetchExtParticipantsObj(String chowToolId) {
        system.debug('#### debug chowToolId = ' + chowToolId);
        list<String> listPar = new list<String>();
        list<chow_team_member__c> listReturn = new list<chow_team_member__c>();

        List<Chow_Tool__c> listCh = [SELECT Id, Add_Extra_Participant__c
                FROM Chow_Tool__c WHERE Id = :chowToolId];
        if(listCh[0].Add_Extra_Participant__c != null) {
            listPar = listCh[0].Add_Extra_Participant__c.split(',');
        }
        system.debug('#### debug listPar = ' + listPar);

        String sQuery = ' select Id, Team_Member_Name__r.Name from chow_team_member__c ' +
                     ' WHERE Team_Member_Name__r.name IN :listPar ' +
                     ' ORDER BY Team_Member_Name__r.name ASC';

        List < chow_team_member__c > lstOfRecords = Database.query(sQuery);
        system.debug('#### debug lstOfRecords = ' + lstOfRecords);

        if(lstOfRecords.size() > 0) {
            list<String> list1 = new list<String>();  
            for(chow_team_member__c team : (list<chow_team_member__c>) lstOfRecords) {
                if(!list1.contains(team.Team_Member_Name__r.name)) {
                    listReturn.add(team);
                    list1.add(team.Team_Member_Name__r.name);
                } 
            }
            
        } 
        system.debug('#### debug listReturn = ' + listReturn);
        return listReturn;
    }    
    
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, 
                                String ObjectName, 
                                List<sObject> ExcludeitemsList) {
        String searchKey = '%' + searchKeyWord + '%';
        List < sObject > returnList = new List < sObject > ();
        system.debug('#### ExcludeitemsList = ' + ExcludeitemsList);
 
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        system.debug('#### lstExcludeitems = ' + lstExcludeitems);
        
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 10 and exclude already selected records 
        String sQuery;
        if(ObjectName == 'ChOW') {
            sQuery = ' select Id, Team_Member_Name__r.Name from chow_team_member__c ' +
                     ' WHERE Team_Member_Name__r.name LIKE: searchKey AND ' +
                     ' Id NOT IN : lstExcludeitems ORDER BY Team_Member_Name__r.name ASC LIMIT 50';
            //sQuery =  'select id, FirstName, LastName from User where (FirstName LIKE: searchKey OR LastName LIKE: searchKey) AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 10';
        } else {
           sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 10';
        }

        system.debug('#### debug searchKey = ' + searchKey);
        system.debug('#### debug sQuery = ' + sQuery);

        List < sObject > lstOfRecords = Database.query(sQuery);
        system.debug('#### debug lstOfRecords = ' + lstOfRecords);

        if(ObjectName == 'ChOW') {
            list<String> list1 = new list<String>();  
            list<chow_team_member__c> listReturn = new list<chow_team_member__c>();
            for(chow_team_member__c team : (list<chow_team_member__c>) lstOfRecords) {
                if(!list1.contains(team.Team_Member_Name__r.name)) {
                    listReturn.add(team);
                    list1.add(team.Team_Member_Name__r.name);
                } 
            }
            return listReturn;
        } else {

            for (sObject obj: lstOfRecords) {
                returnList.add(obj);
            }
            return returnList;
        }
    }
}
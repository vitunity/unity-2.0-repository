global with sharing class remoteServicesController {

    public remoteServicesController(ApexPages.StandardController controller) {
    }
    @RemoteAction
        global static SObject[] getSObjects(string soql) {                                
            List<sObject> L = new List<sObject>();
            try 
            {
                L = Database.query(soql);
            }
            catch (QueryException e) {
                return null;
            } 
            return L;                        
       }
    
    @remoteAction
    global static String upsertObject(String objtype, String fields, Boolean validateTargetFields) {        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);
        if (targetType == null) {
            return makeError('The requested resource does not exist', 'NOT_FOUND');
        }
        
        SObject obj = targetType.newSObject();
        String error = writeFields(objType, obj, fields, validateTargetFields);
        if (error != null) {
            return error;
        }
        
        try {
            upsert obj;
        } catch (DMLException dmle) {
            String fieldNames = '';
            for (String field : dmle.getDmlFieldNames(0)) {
                if (fieldNames.length() > 0) {
                    fieldNames += ',';
                }
                fieldNames += '"'+field+'"';
            }
            return '[{"fields":['+fieldNames+'],"message":"'+dmle.getDmlMessage(0)+'","errorCode":"'+dmle.getDmlType(0).name()+'"}]';
        }        
        Map<String, Object> result = new Map<String, Object>();
        
        result.put('id', obj.id);
        result.put('errors', new List<String>());
        result.put('success', true);  
        system.debug('@@@@@@'+result);      
        return JSON.serialize(result);
    }

    @remoteAction
    Global static String describe(String objtype) {
        // Just enough to make the sample app work!
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);
        if (targetType == null) {
            return makeError('The requested resource does not exist', 'NOT_FOUND');
        }
        
        Schema.DescribeSObjectResult sobjResult = targetType.getDescribe();
        
        Map<String, Schema.SObjectField> fieldMap = sobjResult.fields.getMap();
        
        List<Object> fields = new List<Object>();
        for (String key : fieldMap.keySet()) {
            Schema.DescribeFieldResult descField = fieldMap.get(key).getDescribe();
            Map<String, Object> field = new Map<String, Object>();
            
            field.put('type', descField.getType().name().toLowerCase());
            field.put('name', descField.getName());
            field.put('label', descField.getLabel());
            List<String> references = new List<String>();
            for (Schema.sObjectType t: descField.getReferenceTo()) {
                references.add(t.getDescribe().getName());
            }
            if (!references.isEmpty()) {
                field.put('referenceTo', references);
            }
            field.put('picklistValues', picklistOptions(descField));

            fields.add(field);
        }
        
        Map<String, Object> result = new Map<String, Object>();
        result.put('fields', fields);
        
        return JSON.serialize(result);
    }

    //helpers
    
    private static List<Object> picklistOptions(Schema.DescribeFieldResult field) {
        List<Object> picklistOptions = new List<Object>();
        Schema.DisplayType fieldType = field.getType();
        if (fieldType != Schema.DisplayType.Picklist &&
          fieldType != Schema.DisplayType.MultiPicklist &&
          fieldType != Schema.DisplayType.Combobox) {
            return picklistOptions;
        }
        List<Schema.PicklistEntry> picklistValues = field.getPicklistValues();
        for (Schema.PicklistEntry picklistValue: picklistValues) {
            Map<String, Object> picklistOption = new Map<String, Object>();
            picklistOption.put('value', picklistValue.getValue());
            picklistOption.put('label', picklistValue.getValue());
            picklistOption.put('active', picklistValue.isActive());
            picklistOption.put('defaultValue', picklistValue.isDefaultValue());
            picklistOptions.add(picklistOption);
        }
        return picklistOptions;
    }

    
    private static String makeError(String message, String errorCode) {
        JSONGenerator gen = JSON.createGenerator(false);
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField('message', message);
        gen.writeStringField('errorCode', errorCode);
        gen.writeEndObject();
        gen.writeEndArray();        
        return gen.getAsString();
    }

    private static String writeFields(String objtype, SObject obj, String fields, Boolean validateTargetFields) {
        /*if (validateTargetFields){
            system.debug('validate target fields');
        } else {
            system.debug('dont validate');
        }
        */
        Map<String, Object> fieldMap = null;
        try {
            fieldMap = (Map<String, Object>)JSON.deserializeUntyped(fields);
        } catch (JSONException je) {
            return makeError(je.getMessage(), 'JSON_PARSER_ERROR');
        }    
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objtype);    
        Map<String, Schema.sObjectField> targetFields = targetType.getDescribe().fields.getMap();    
        try {
            for (String key : fieldMap.keySet()) {            
                sObjectField tf = targetFields.get(key);
                if ((tf == null) && validateTargetFields) {
                    return '[{"message":"Field '+key+' does not exist on object type '+objtype+'","errorCode":"INVALID_FIELD"}]';
                }

                if (key.toLowerCase() == 'id' || tf != null) {                                                     
                    Object value = fieldMap.get(key);   
                    Schema.DisplayType valueType = targetFields.get(key).getDescribe().getType();
                    system.debug('################ Key= ' + key + ' Type:' + valueType + ' Val' + value);
                    if (value instanceof String && valueType != Schema.DisplayType.String) {
                        // Coerce an incoming String to the correct type
                        String svalue = (String)value;                        
                        if (valueType == Schema.DisplayType.Date) {
                            obj.put(key, Date.valueOf(svalue));
                        } else if(valueType == Schema.DisplayType.DateTime){
                            obj.put(key, DateTime.valueOfGmt(svalue));
                        } else if (valueType == Schema.DisplayType.Percent ||
                            valueType == Schema.DisplayType.Currency) {
                            obj.put(key, svalue == '' ? null : Decimal.valueOf(svalue));
                        } else if (valueType == Schema.DisplayType.Double) {
                            obj.put(key, svalue == '' ? null : Double.valueOf(svalue));
                        } else if (valueType == Schema.DisplayType.Integer) {
                            obj.put(key, Integer.valueOf(svalue));
                        } else if (valueType == Schema.DisplayType.Base64) {
                            obj.put(key, Blob.valueOf(svalue));
                        } else {
                            obj.put(key, svalue);
                        }
                    } else {
                        // Just try putting the incoming value on the object
                        obj.put(key, value);
                    }
                }
            }
        } catch (SObjectException soe) {
            return makeError(soe.getMessage(), 'INVALID_FIELD');
        }    
        return null;
    }
    
}
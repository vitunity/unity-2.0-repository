global class scheduler_ERPNWAAlert implements Schedulable {
    global void execute(SchedulableContext SC) {
        Batch_ERPNWAAlert obj = new Batch_ERPNWAAlert();
        Database.executeBatch(obj);
    }
}
global class scheduler_EscalatedComplaintForms implements Schedulable {
    global void execute(SchedulableContext SC) {
        string q ='select Id,Case_ECF_Count__c,Case_ECF_Submitted__c from Case where Is_escalation_to_the_CLT_required__c= \'Yes\' and status != \'Closed\' and status != \'Closed by work order\'';
        
        //if(test.isrunningTest()) q ='select Id,Case_ECF_Count__c,Case_ECF_Submitted__c from Case where status != \'Closed\' and status != \'Closed by work order\' limit 1';
        
        Batch_EscalatedComplaintForms obj = new Batch_EscalatedComplaintForms(q);
        Database.executeBatch(obj,50);
    }
}
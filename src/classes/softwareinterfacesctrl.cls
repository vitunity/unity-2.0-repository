/* Author: Chandramouli
* Created Date: March-23-2018
* Project/Story/Inc/Task : Installed Product Interface data
* Description: This will be used to display the data that comes from different Interfaces.
*/
public class softwareinterfacesctrl{
    public String AccName{get;set;}
    public String SFANumber{get;set;}
    public String linkAddress{get;set;}
    @testVisible private map<String,List<Software_Interface__c>> mapsoftwareInterfaces;
    private map<String,List<Software_Interface__c>> mapDatabaseServer;
    private map<String,List<Software_Interface__c>> mapAriaConnect;
    
    private map<date,List<Software_Interface__c>> mapDateExtracted;
    private map<String,List<Software_Interface__c>> mapAriaVersion;
    private map<String,List<Software_Interface__c>> mapCssIndicator;
    
    public softwareinterfacesctrl(ApexPages.StandardController sc) {
        mapsoftwareInterfaces = new map<String,List<Software_Interface__c>>();
        mapDatabaseServer = new map<String,List<Software_Interface__c>>();
        mapAriaConnect = new map<String,List<Software_Interface__c>>();
        
        mapDateExtracted = new map<date,List<Software_Interface__c>>();
        mapAriaVersion = new map<String,List<Software_Interface__c>>();
        mapCssIndicator = new map<String,List<Software_Interface__c>>();
        
        for(Software_Interface__c siObj :  [select id, Name, Account__c, Account__r.Name, Account__r.SFDC_Account_Ref_Number__c,
             Aria_Version__c, Status__c, ARIA_CONNECT__c, Installed_Product__r.Name,
             CCS_FLAG__c, CUSTOM__c, DATABASE_SERVER__c, Date_Extracted__c, 
             DIRECTION__c, GENERIC_CCDA__c, IEM_Server__c, 
             Installed_Product__c, Interface__c, Last_Message_Processed__c, MODE__c,
             TYPE__c from Software_Interface__c
             where Account__c =: sc.getId()]){
             if(mapsoftwareInterfaces.containsKey(siObj.Installed_Product__r.Name)){
                 mapsoftwareInterfaces.get(siObj.Installed_Product__r.Name).add(siObj);
             }else{
                 list<Software_Interface__c> siList = new list<Software_Interface__c>();
                 siList.add(siObj);
                 mapsoftwareInterfaces.put(siObj.Installed_Product__r.Name,siList);
             } 
             
             if(mapDatabaseServer.containsKey(siObj.DATABASE_SERVER__c)){
                 if(mapDatabaseServer.isEmpty() ){
                    mapDatabaseServer.get(siObj.DATABASE_SERVER__c).add(siObj);
                 }
             }else{
                 list<Software_Interface__c> sfiList = new list<Software_Interface__c>();
                 sfiList.add(siObj);
                 if(mapDatabaseServer.isEmpty() ){
                    mapDatabaseServer.put(siObj.DATABASE_SERVER__c,sfiList);
                 }
             } 
             
             
             if(mapAriaConnect.containsKey(siObj.ARIA_CONNECT__c)){
                if(mapAriaConnect.isEmpty() ){
                    mapAriaConnect.get(siObj.ARIA_CONNECT__c).add(siObj);
                }
             }else{
                 list<Software_Interface__c> ariaList = new list<Software_Interface__c>();
                 ariaList.add(siObj);
                 if(mapAriaConnect.isEmpty() ){
                    mapAriaConnect.put(siObj.ARIA_CONNECT__c,ariaList);
                 }
             } 
             
             if(mapDateExtracted.containsKey(siObj.Date_Extracted__c)){
                 if(mapDateExtracted.isEmpty() ){
                    mapDateExtracted.get(siObj.Date_Extracted__c).add(siObj);
                 }
             }else{
                 list<Software_Interface__c> dateExtractedList = new list<Software_Interface__c>();
                 dateExtractedList.add(siObj);
                 if(mapDateExtracted.isEmpty() ){
                    mapDateExtracted.put(siObj.Date_Extracted__c,dateExtractedList);
                 }
             } 
             
             
             if(mapAriaVersion.containsKey(siObj.Aria_Version__c)){
                 if(mapAriaVersion.isEmpty() ){
                    mapAriaVersion.get(siObj.Aria_Version__c).add(siObj);
                 }
             }else{
                 list<Software_Interface__c> ariaVersionList = new list<Software_Interface__c>();
                 ariaVersionList.add(siObj);
                 if(mapAriaVersion.isEmpty() ){
                    mapAriaVersion.put(siObj.Aria_Version__c,ariaVersionList);
                 }
             }
             
             if(mapCssIndicator.containsKey(siObj.CCS_FLAG__c)){
                if(mapCssIndicator.isEmpty() ){
                    mapCssIndicator.get(siObj.CCS_FLAG__c).add(siObj);
                }
             }else{
                 list<Software_Interface__c> cssIndicatorList = new list<Software_Interface__c>();
                 cssIndicatorList.add(siObj);
                 if(mapCssIndicator.isEmpty() ){
                    mapCssIndicator.put(siObj.CCS_FLAG__c,cssIndicatorList);
                 }
             } 
             AccName = siObj.Account__r.Name;
             SFANumber = siObj.Account__r.SFDC_Account_Ref_Number__c;
             linkAddress = 'mailto:AMS-PSS-REQUEST@varian.com?subject='+SFANumber+' '+AccName+' - Unity Interface Information Problem';
             
             
        }
    }
    
    public map<String,List<Software_Interface__c>> getmapsoftwareInterfaces(){
        return mapsoftwareInterfaces;
    }
    
    public map<String,List<Software_Interface__c>> getmapDatabaseServer(){
        return mapDatabaseServer;
    }
    public map<String,List<Software_Interface__c>> getmapAriaConnect(){
        return mapAriaConnect;
    }
    public map<Date,List<Software_Interface__c>> getmapDateExtracted(){
        system.debug('==mapDateExtracted=='+mapDateExtracted);
        return mapDateExtracted;
    }
    public map<String,List<Software_Interface__c>> getmapAriaVersion(){
        system.debug('==mapAriaVersion=='+mapAriaVersion);
        return mapAriaVersion;
    }
    public map<String,List<Software_Interface__c>> getmapCssIndicator(){
        return mapCssIndicator;
    }
}
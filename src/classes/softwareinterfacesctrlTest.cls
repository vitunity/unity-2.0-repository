@isTest
public class softwareinterfacesctrlTest{

public static Account accnt;
public static Software_Interface__c softwareInterface;
public static SVMXC__Installed_Product__c installpro;
public static map<String,List<Software_Interface__c>> mapsoftwareInterfaces = new  map<String,List<Software_Interface__c>>();
public static list<Software_Interface__c> swList = new list<Software_Interface__c>();
static{

    accnt= new Account();
    accnt.Name= 'Wipro technologies';
    accnt.BillingCity='New York';
    accnt.country__c='USA';
    accnt.OMNI_Postal_Code__c='940022';
    accnt.BillingCountry='USA';
    accnt.BillingStreet='New Jersey';
    accnt.BillingPostalCode='552600';
    accnt.BillingState='LA';
    insert accnt;
    
    Product2 prod= new Product2();
    prod.Name= 'Clinac';
    prod.CurrencyIsoCode= 'USD';
    insert prod;
    
    installpro = new SVMXC__Installed_Product__c();
    installpro.Name= 'Installed Product';
    installpro.SVMXC__Product__c= prod.id;
    installpro.SVMXC__Serial_Lot_Number__c='1023';
    installpro.SVMXC__Company__c= accnt.id;
    installpro.SVMXC__Status__c='Installed';
    insert installpro;
    
    softwareInterface = new Software_Interface__c();
    softwareInterface.Interface__c = 'STAR Meditech Duo Feed to Aria ADT';
    softwareInterface.Status__c = 'Active';
    softwareInterface.Installed_Product__c = installpro.Id;
    softwareInterface.Account__c = accnt.id;
    softwareInterface.TYPE__c = 'ADT';
    softwareInterface.MODE__c = 'Direct into RadOnc';
    softwareInterface.DIRECTION__c = 'INBOUND';
    softwareInterface.CUSTOM__c = 'N';
    softwareInterface.Aria_Version__c = '11.83';
    softwareInterface.DATABASE_SERVER__c = 'CH-NSCVARIMDB01';
    softwareInterface.IEM_Server__c = 'CH-NSCVARIEM01';
    softwareInterface.CCS_FLAG__c = 'N';
    softwareInterface.GENERIC_CCDA__c = 'N';
    softwareInterface.ARIA_CONNECT__c = 'No';
    insert softwareInterface;
    
    swList.add(softwareInterface);
    
}

static testMethod void testmethod1()
{

ApexPages.StandardController sc = new ApexPages.StandardController(accnt);

for(Software_Interface__c sw : swList){
        list<Software_Interface__c> siList = new list<Software_Interface__c>();
        siList.add(sw);
        mapsoftwareInterfaces.put(sw.Installed_Product__r.Name,siList);
    }
softwareinterfacesctrl swctr = new softwareinterfacesctrl(sc);
swctr.mapsoftwareInterfaces = mapsoftwareInterfaces;

swctr.getmapsoftwareInterfaces();
swctr.getmapDatabaseServer();
swctr.getmapAriaConnect();
swctr.getmapDateExtracted();
swctr.getmapAriaVersion();
swctr.getmapCssIndicator();

}

}
@isTest
public class testAttachmentTriggerHandler
{
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    static SVMXC__Site__c location;
    static Account acc;
    static ERP_Project__c  erpproject; 
    static ERP_WBS__c erpwbs;
    static Country__c con;
    static Contact c;
    static ERP_NWA__c erpnwa;
    /* Initialize test data in static block */
    static
    {
        acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States'); 
        insert acc;

        casealias = new Case(Subject = 'testsubject');
        casealias.Priority = 'High';
        insert casealias;
    }
    
    public static testmethod void testBeforeInsert()
    {
        wo = new SVMXC__Service_Order__c(SVMXC__Company__c = acc.id,SVMXC__Case__c = casealias.id);
        wo.Event__c = true;
        insert wo;
        Attachment att2 = new Attachment(Name='FSR12345.pdf', ParentId=wo.Id, body=EncodingUtil.base64Decode('Test'));
        insert att2;
    }
}
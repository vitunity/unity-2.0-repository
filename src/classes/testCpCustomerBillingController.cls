@isTest
public class testCpCustomerBillingController
{
    static Account soldTo = SR_testdata.creteAccount();
    static Contact contact1 = SR_testdata.createContact();
    static Invoice__c invoice1 = new Invoice__c();
    static Country__c objCounty = new Country__c();
    static BigMachines__Quote__c bq = new BigMachines__Quote__c();
    public Static User u;

    static
    {
        Id soldTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();

        soldTo.AccountNumber = '123456';
        soldTo.country__c = 'India';
        soldTo.ERP_Timezone__c = 'AUSSA';
        soldTo.recordTypeId = soldTypeId;
        insert soldTo;

        contact1.AccountId = soldTo.Id;
        contact1.Email = 'abc@xyz.com';
        contact1.mailingcountry = 'India';
        contact1.ERP_Payer_Number__c = '132222';
        insert contact1;

        Profile pr = [Select id from Profile where name = 'VMS MyVarian - Customer User'];
        
        u = new user(alias = 'standt', contactId = contact1.Id, email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');

        Contact_Role_Association__c cra = new Contact_Role_Association__c();
        cra.Account__c = soldTo.Id;
        cra.Contact__c = contact1.Id;
        cra.Role__c = 'Account Payable Capital';
        insert cra;

        Opportunity opp = TestUtils.getOpportunity();
        opp.AccountId = soldTo.Id;
        opp.Primary_Contact_Name__c = contact1.Id;
        opp.MGR_Forecast_Percentage__c = '40%';
        opp.Unified_Funding_Status__c = '20%';
        opp.Net_Booking_Value__c = 200;
        opp.CloseDate = System.today().addMonths(1);
        insert opp;
        
        bq.Name = '2016-15108';
        bq.BigMachines__Account__c = soldTo.Id;
        bq.BigMachines__Opportunity__c = opp.Id;
        bq.BigMachines__Is_Primary__c = true;
        bq.Order_Type__c = 'Sales';
        insert bq;

        Attachment att2 = new Attachment(Name='2Aging_Statement_232', ParentId=bq.Id, body=EncodingUtil.base64Decode('Test'));
        insert att2;

        invoice1.Name = '132222';
        invoice1.Sold_To__c = soldTo.Id;
        invoice1.Invoice_Date__c = Date.today();
        invoice1.Invoice_Due_Date__c = Date.today();
        invoice1.CurrencyIsoCode = 'USD';
        invoice1.Payer__c = '132222';
        invoice1.ERP_Payer__c = '132222';
        invoice1.Invoice_Cleared_Date__c = Date.today();
        invoice1.Quote__c = bq.Id;
        insert invoice1;
        List<Invoice__c> invs = new List<Invoice__c>();
        
        for(Integer i=0; i<=200; i++)
        { 
            Invoice__c inv = new Invoice__c();
            inv.Name = '132222';
            inv.Sold_To__c = soldTo.Id;
            inv.Invoice_Date__c = Date.today();
            inv.Invoice_Due_Date__c = Date.today();
            inv.CurrencyIsoCode = 'USD';
            inv.Payer__c = '132222';
            inv.ERP_Payer__c = '132222';
            inv.Invoice_Cleared_Date__c = Date.today();
            invs.add(inv);
        }
        insert invs;

        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = 'Sampple Partner';
        erpPartner.Partner_Number__c = '132222';
        insert erpPartner;

        List<Attachment> attachs = new List<Attachment>();
        Attachment att = new Attachment(Name='132222', ParentId=invoice1.ID, body=EncodingUtil.base64Decode('Test'));
        attachs.add(att); 

        Attachment att1 = new Attachment(Name='Aging_Statement_232', ParentId=soldTo.Id, body=EncodingUtil.base64Decode('Test'));
        attachs.add(att1); 

        Attachment att3 = new Attachment(Name='other attach', ParentId=invoice1.Id, body=EncodingUtil.base64Decode('Test'));
        attachs.add(att3); 
        insert attachs;

    }
    static testMethod void testGetAttachments()
    {
        test.startTest();
        PageReference myVfPage1 = Page.CpCustomerBilling;
        Test.setCurrentPage(myVfPage1);     
        ApexPages.currentPage().getParameters().Put('id', invoice1.id);
        CpCustomerBillingController cpController = new CpCustomerBillingController();
        cpController.getrecoveryqusoptions();
        ApexPages.currentPage().getParameters().put('RecoveryQuestionparam','Test Question');
        ApexPages.currentPage().getParameters().put('RecoveryAnswerparam','Test Answer');

        cpController.CustRecovryQ = 'Where did you meet your spouse? ';
        cpController.CustRecoveryAns = 'USA';
        cpController.Newpass = 'abc123@#$';
        cpController.ConfirmPass = 'abc123@#$';
        cpController.resetpassword();
        Attachment att4 = new Attachment(Name='2Aging_Statement_232', ParentId=bq.Id, body=EncodingUtil.base64Decode('Test'));
        insert att4;
        CpCustomerBillingController.AttachmentWrapper attachW = new CpCustomerBillingController.AttachmentWrapper(att4);
        attachW.deleteAttachment();
        cpController.att = new Attachment(Name='2Aging_Statement_232', ParentId=bq.Id, body=EncodingUtil.base64Decode('Test'));
        cpController.save();
        system.RunAs(u)
        {
            cpController.getAttachments();
            cpController.closepopup();
            CpCustomerBillingController.InvoiceWrapper invw = new CpCustomerBillingController.InvoiceWrapper(invoice1);
            invw.selected = true;
            cpController.getAttids();
            cpController.logoutmethod();

        }
        test.stopTest();
    }
    static testMethod void testInvoicePage()
    {
        test.startTest();
        system.RunAs(u)
        {
            CpCustomerBillingController cpController = new CpCustomerBillingController();
            cpController.getrecoveryqusoptions();
            cpController.toggleSortType();
            cpController.toggleSort();
            cpController.invoiceStatus = 'All';
            cpController.document_TypeSearch = '132222';
            cpController.invoiceFrom.Invoice_Cleared_Date__c = Date.today().addDays(-1);
            cpController.invoiceTo.Invoice_Cleared_Date__c = Date.today().addDays(+1);
            cpController.searchRecords();
            cpController.reset();
            cpController.download();
            cpController.submitRecords();
            CpCustomerBillingController.InvoiceWrapper invw = new CpCustomerBillingController.InvoiceWrapper(invoice1);
            invw.selected = true;
            cpController.getAttids();
        }
        test.stopTest();
    }
    
    static testMethod void testInvoicePageAdmin()
    {
        test.startTest();
        CpCustomerBillingController cpController = new CpCustomerBillingController();
        cpController.getrecoveryqusoptions();
        cpController.submitRecords();
        system.RunAs(u)
        {
            cpController.toggleSortType();
            cpController.toggleSort();
            cpController.invoiceStatus = 'All';
            cpController.searchRecords();
            CpCustomerBillingController.InvoiceWrapper invw = new CpCustomerBillingController.InvoiceWrapper(invoice1);
            invw.selected = true;
            cpController.getAttids();
            cpController.reset();
            cpController.download();
        }
        test.stopTest();
    }
}
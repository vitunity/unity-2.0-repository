@isTest
public class testInvoiceRemainderController
{
    static Account soldTo = SR_testdata.creteAccount();
    static Account soldToDefault = SR_testdata.creteAccount(); 
    static Account siteParter = SR_testdata.creteAccount();
    static Contact contact1 = SR_testdata.createContact();
    static Contact contact2 = SR_testdata.createContact();
    static Invoice__c invoice1 = new Invoice__c();
    static Country__c objCounty = new Country__c();
    static BigMachines__Quote__c bq = new BigMachines__Quote__c();
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    public Static User u;

    static
    {
        Id soldTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
        Id siteParterId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        List<Account> accs = new List<Account>();
        soldTo.AccountNumber = '123456';
        soldTo.country__c = 'India';
        soldTo.ERP_Timezone__c = 'AUSSA';
        soldTo.recordTypeId = soldTypeId;
        soldTo.Ext_Cust_Id__c = '123456';
        accs.add(soldTo);

        soldToDefault.AccountNumber = '555555';
        soldToDefault.country__c = 'India';
        soldToDefault.ERP_Timezone__c = 'AUSSA';
        soldToDefault.recordTypeId = soldTypeId;
        soldToDefault.Ext_Cust_Id__c = '555555';
        accs.add(soldToDefault);

        siteParter.AccountNumber = '587855';
        siteParter.ERP_Site_Partner_Code__c = '587855';
        siteParter.country__c = 'India';
        siteParter.ERP_Timezone__c = 'AUSSA';
        siteParter.recordTypeId = siteParterId;
        accs.add(siteParter);
        insert accs;

        List<Contact> cons = new List<Contact>();
        contact1.AccountId = soldTo.Id;
        contact1.Email = 'abc@xyz.com';
        contact1.mailingcountry = 'India';
        contact1.ERP_Payer_Number__c = '132222';
        contact1.Primary_Billing_Contact__c = true;
        cons.add(contact1);

        contact2.AccountId = siteParter.Id;
        contact2.Email = 'abc@xyz.com';
        contact2.mailingcountry = 'India';
        contact2.ERP_Payer_Number__c = '132222';
        contact2.Primary_Billing_Contact__c = true;
        cons.add(contact2);
        insert cons;

        Profile pr = [Select id from Profile where name = 'VMS MyVarian - Customer User'];
        
        u = new user(alias = 'standt', contactId = contact1.Id, email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
        insert u;

        List<Contact_Role_Association__c> cras = new List<Contact_Role_Association__c>();
        Contact_Role_Association__c cra = new Contact_Role_Association__c();
        cra.Account__c = soldTo.Id;
        cra.Contact__c = contact1.Id;
        cra.Role__c = 'Account Payable Capital';
        cras.add(cra);

        Contact_Role_Association__c cra2 = new Contact_Role_Association__c();
        cra2.Account__c = siteParter.Id;
        cra2.Contact__c = contact2.Id;
        cra2.Role__c = 'Account Payable Capital';
        cras.add(cra2);

        insert cras;
    }
    static testMethod void testGetInvoiceData()
    {
        Test.startTest();
        invoice1.Name = '132222';
        invoice1.ERP_BMI_Quote__c = '2016-15108';
        invoice1.ERP_Sold__c = '123456';
        invoice1.Invoice_Date__c = Date.today();
        invoice1.Invoice_Due_Date__c = Date.today();
        invoice1.CurrencyIsoCode = 'USD';
        invoice1.Payer__c = '132222';
        invoice1.ERP_Payer__c = '132222';
        invoice1.Invoice_Cleared_Date__c = Date.today();
        invoice1.Sold_To__c = soldToDefault.Id;
        invoice1.Billing_Contact__c = contact1.Id;
        insert invoice1;
        List<Invoice__c> invs = new List<Invoice__c>();
        
        for(Integer i=0; i<=200; i++)
        { 
            Invoice__c inv = new Invoice__c();
            inv.Name = '132222';
            inv.ERP_BMI_Quote__c = '2016-15108';
            inv.ERP_Sold__c = '555555';
            inv.Invoice_Date__c = Date.today();
            inv.Invoice_Due_Date__c = Date.today();
            inv.CurrencyIsoCode = 'USD';
            inv.Payer__c = '132222';
            inv.ERP_Payer__c = '132222';
            inv.Invoice_Cleared_Date__c = Date.today(); 
            inv.ERP_Site_Number__c = '587855';
            invs.add(inv);
        }
        insert invs;
        InvoiceRemainderController irc = new InvoiceRemainderController();
        irc.contactId = contact1.Id;
        irc.invContact = contact1;
        irc.getInvoiceData();
        Test.stopTest();
    }
}
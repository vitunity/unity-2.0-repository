@isTest
public class testSRCaseEditPageController
{
    public static Account newAcc;
    public static Sales_Order__c so;
    public static Sales_Order_Item__c soi;
    public static SVMXC__Site__c newLoc;
    public static Product2 newProd;
    public static Case newCase;
    public static SVMXC__Case_Line__c newCaseLine;
    public static ERP_Project__c erpproject;
    public static ERP_WBS__c erpwbs;
    public static SVMXC__Service_Group__c servicegrp;
    public static Timecard_Profile__c timecardProfile;
    public static SVMXC__Service_Group_Members__c technician;
    public static ERP_Pcode__c prode;
    public static User serviceUser;
    public static User serviceUser1;
    public static SVMXC__Service_Order__c wo;
    public static SVMXC__Installed_Product__c iProduct;
    public static Contact testcon;
    public static profile serviceUserProfile;
    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
    public static User InsertUserData(Boolean isInsert) 
    {
        serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test1c@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1c@service.com');
        try {
        if(isInsert)
            insert serviceUser;
        } catch(Exception e) {
            system.debug(' == Exception == ' + e.getMessage() + ' = ' + e.getLineNumber());
        }
        return serviceUser;
    }
    
    public static Account AccountData(Boolean isInsert) 
    {
        newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.Country__c = 'USA';
        if(isInsert)
            insert newAcc;
        return newAcc;
    }

    public static Product2 product_Data(Boolean isInsert) 
    {
        newProd = new Product2();
        newProd.Name = 'Rapid Arc for Eclipse';
        newProd.ProductCode = 'VC_RAE';
        newProd.BigMachines__Part_Number__c = 'VC_RAE';
        newProd.Product_Type__c = 'Part';
        newProd.IsActive = true;
        newProd.Product_Source__c = 'SAP';
        newProd.ERP_Plant__c = '0600';
        newProd.ERP_Sales_Org__c = '0600';
        if(isInsert)
            insert newProd;
        return newProd;
    }

    public static SVMXC__Site__c Location_Data(Boolean isInsert, Account newAcc1) 
    {
        newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc1.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        if(isInsert)
            insert newLoc;
        return newLoc;
    }
    
    public static Sales_Order_Item__c SOI_Data(Boolean isInsert, Sales_Order__c so1) 
    {
        soi = new Sales_Order_Item__c();
        soi.Name = '000005';
        soi.Sales_Order__c = so1.id;
        soi.ERP_Material_Number__c = 'FI_REV_ITEM';
        soi.ERP_Equipment_Number__c = '123';
        if(isInsert)
            insert soi;
        return soi;
    }

    public static Sales_Order__c SO_Data(Boolean isInsert, Account newAcc1) {
        so = new Sales_Order__c();
        so.name = '319112526';
        so.Site_Partner__c = newAcc1.Id;
        so.Sold_To__c = newAcc1.Id;
        so.Requested_Delivery_Date__c = system.today().addMonths(-1);
        so.Sales_Group__c = 'N08';
        so.Sales_District__c = 'NA02';
        so.Status__c ='Open';
        so.Sales_Org__c = '0601';
        if(isInsert)
            insert so;
        return so;
    }
    
    public static ERP_Pcode__c ERPPCODE_Data(Boolean isInsert) {
        prode = new ERP_Pcode__c();
        prode.Name = 'H19';
        prode.ERP_Pcode_2__c = '19';
        prode.Description__c = 'TrueBeam';
        prode.IsActive__c = true;
        prode.Service_Product_Group__c = 'LINAC';
        if(isInsert)
            insert prode;
        return prode;
    }

    public static SVMXC__Installed_Product__c InstalledProduct_Data(Boolean isInsert, Product2 newProd1, Account newAcc1, SVMXC__Site__c newLoc1,
                                                                    SVMXC__Service_Group__c serviceTeam1, ERP_Pcode__c erpPcode1) {
        SVMXC__Installed_Product__c installProd = new SVMXC__Installed_Product__c();
        installProd.Name = 'H621598';
        installProd.SVMXC__Status__c = 'Installed';
        installProd.ERP_Reference__c = 'H621598-VE';
        installProd.SVMXC__Product__c = newProd1.Id;
        installProd.ERP_Pcodes__c = erpPcode1.Id;
        installProd.SVMXC__Company__c = newAcc1.Id;
        installProd.SVMXC__Site__c = newLoc1.Id;
        installProd.ERP_Functional_Location__c = 'H-COLUMBUS -OH-US-005';
        installProd.Billing_Type__c = 'P – Paid Service';
        installProd.Service_Team__c = serviceTeam1.Id;
        installProd.ERP_CSS_District__c = 'U9J';
        installProd.ERP_Work_Center__c = 'H87696';
        installProd.ERP_Product_Code__c = 'H62A';
        if(isInsert)
            insert installProd;
        return installProd;
    }
    
    // Create CASE
    public static Case Case_Data(Boolean isInsert, Account newAcc1, Product2 newProd1) {
        newCase = new Case();
        newCase.AccountId = newAcc1.Id;
        newCase.Status = 'Closed';
        newCase.RecordTypeId = caseRecordType.get('HD/DISP').getRecordTypeId();
        newCase.Product__c = newProd1.Id;
        newCase.Priority = 'High';
        newCase.PHI_Obtained__c ='Yes';
        if(isInsert)
            insert newCase;
        return newCase;
    }
    
    // Creating Case Line Data
    public static SVMXC__Case_Line__c CaseLine_Data(Boolean isInsert, Case newCase1, product2 newProd1) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase1.id;
        caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd1.Id;
        caseLine.Start_Date_time__c = system.today().addDays(-2);
        caseLine.End_date_time__c = system.today().addDays(+2);

        caseLine.SVMXC__Installed_Product__c = iProduct.Id;
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    static
    {
        newAcc = AccountData(true);
        so = SO_Data(true, newAcc);
        soi = SOI_Data(true, so);
        prode = ERPPCODE_Data(true);
        newLoc = Location_Data(true, newAcc);
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        newProd = product_Data(true);

        newCase = Case_Data(true, newAcc, newProd);

        iProduct = InstalledProduct_Data(true, newProd, newAcc, newLoc, servicegrp, prode);
        newCaseLine = CaseLine_Data(true, newCase, newProd);

        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;

        erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id);
        insert erpwbs;



        timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        insert timecardProfile;
        technician = new SVMXC__Service_Group_Members__c
        (
            name = 'testtechnician',
            User__c = Userinfo.getUserId(),
            SVMXC__Service_Group__c = servicegrp.id,
            SVMXC__Country__c = 'India',
            SVMXC__Street__c = 'abc',
            SVMXC__Zip__c = '54321',
            Timecard_Profile__c = timecardProfile.id
        );

        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician.Timecard_Profile__c = timecardProfile.Id;
        insert technician;

        wo = new SVMXC__Service_Order__c
        (
            SVMXC__Group_Member__c = technician.id,
            SVMXC__Site__c = newLoc.id,
            SVMXC__Company__c = newAcc.id,
            SVMXC__Case__c = newCase.id,
            ERP_Service_Order__c = '12345678',
            ERP_WBS__c=erpwbs.id

        );
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        wo.Event__c = true;
        insert wo;
        
    }

    static testMethod void testSRCaseEditPageController()
    {
        Test.startTest();
            testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445',Primary_Contact_Number__c = 'Phone');
        insert testcon;
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',testcon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            Apexpages.currentpage().getparameters().put('SubjectName', 'acme');
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            SRCaseEditPageController.wrapperchild wr = new SRCaseEditPageController.wrapperchild();
            wr.CategoryName = 'c1';
            wr.CategoryLabel = 'c1';
            wr.selectedvalue = null;
            wr.optionslist = new List<SelectOption>();
            wr.optionslist.add(new SelectOption('Public', 'Public'));
            wr.optionslist.add(new SelectOption('Private', 'Private'));
            wr.optionslist.add(new SelectOption('Protect', 'Protect'));
            List<SRCaseEditPageController.wrapperchild> wras = new List<SRCaseEditPageController.wrapperchild>();
            wras.add(wr);
            caseEdit.Mapkeyval = wras;
            caseEdit.SearchArticle();
        Test.stopTest();
    }
    static testMethod void testSRCaseEditPageController1()
    {
        Test.startTest();
            testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445',Primary_Contact_Number__c = 'Phone');
        insert testcon;
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            //Apexpages.currentpage().getparameters().put('ConId',testcon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            Apexpages.currentpage().getparameters().put('SubjectName', 'acme');
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            SRCaseEditPageController.wrapperchild wr = new SRCaseEditPageController.wrapperchild();
            wr.CategoryName = 'c1';
            wr.CategoryLabel = 'c1';
            wr.selectedvalue = null;
            wr.optionslist = new List<SelectOption>();
            wr.optionslist.add(new SelectOption('Public', 'Public'));
            wr.optionslist.add(new SelectOption('Private', 'Private'));
            wr.optionslist.add(new SelectOption('Protect', 'Protect'));
            List<SRCaseEditPageController.wrapperchild> wras = new List<SRCaseEditPageController.wrapperchild>();
            wras.add(wr);
            caseEdit.Mapkeyval = wras;
            caseEdit.SearchArticle();
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod1()
    {
        Test.startTest();
            testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445',Primary_Contact_Number__c = 'Mobile');
        insert testcon;
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',testcon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);

            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            caseEdit.getMapkeyval();
            caseEdit.getwrapperlist();
            caseEdit.setwrapperlist();
        //caseEdit.SearchArticle();
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod2()
    {
        Test.startTest();
            testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445',Primary_Contact_Number__c = 'Other Phone');
            insert testcon;
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',testcon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            Blob blobValue = EncodingUtil.convertFromHex('4A4B4C');

            caseEdit.fileBody = blobValue;
            caseEdit.fileName = 'Name'; 

            caseEdit.getItems();
            caseEdit.gettypeItems();
            caseEdit.mySave();
            caseEdit.SaveNew();
            caseEdit.doSave();
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod3()
    {

        Test.startTest();
        serviceUser = InsertUserData(true);
        System.runAs ( serviceUser )
        {
            BusinessHours hours = [Select Id From BusinessHours limit 1];
            SVMXC__Service_Level__c sla = new SVMXC__Service_Level__c(SVMXC__Business_Hours__c = hours.Id);
            insert sla;

            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
            contract.Flex_Credit__c = 'PEC110';
            contract.SVMXC__Active__c = true;
            contract.SVMXC__End_Date__c = System.today() + 10;
            contract.SVMXC__Start_Date__c = System.today() - 10;
            contract.ERP_Order_Reason__c = 'Trade part Return';
            contract.SVMXC__Company__c = newAcc.Id;
            contract.ERP_Contract_Type__c = 'acme';
            contract.Quote_Reference__c = 'acme';
            contract.ERP_Sold_To__c = '2009845';
            contract.SVMXC__Service_Level__c = sla.Id;
            contract.EXT_Quote_Number__c = '1101';
            contract.ERP_Cancellation_Reason__c = 'Too expensive';
            insert contract;
            newCase.SVMXC__Service_Contract__c = contract.Id;
            update newCase;
            iProduct.SVMXC__Service_Contract__c = contract.Id;
            update iProduct;
            Contact primaryCon = new Contact(FirstName = 'primaryCon', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
            primaryCon.Primary_Contact_Number__c ='Phone';
            insert primaryCon;

            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',primarycon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            Apexpages.currentPage().getParameters().put('ProductId', iProduct.Id); 
            Apexpages.currentPage().getParameters().put('PrimaryConId', 'Mobile');
            Apexpages.currentPage().getParameters().put('AccountId', newAcc.id);

            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            caseEdit.SaveClose();
            caseEdit.Createcase();
            caseEdit.ProductSearch();
            caseEdit.EditPageRedirection();
            caseEdit.docancel();
        }
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod4()
    {
        Test.startTest();
        serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser1 = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'zh_CN', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1@service.com');
        insert serviceUser1;
        testcon = new Contact(FirstName = 'TestContact', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
        MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445',Primary_Contact_Number__c = 'Phone');
        insert testcon;
        //System.runAs ( serviceUser1 )
        //{
            newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId();
            newCase.Status = 'New';
            newCase.Local_Resolution__c = 'acme';
            newCase.Local_Internal_Notes__c = 'acme';
            newCase.Local_Case_Activity__c = 'acme';
            update newCase;
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',testcon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.CaseDescrp = 'acme';
            caseEdit.getItems();
            caseEdit.gettypeItems();
            caseEdit.mySave();
            caseEdit.SaveNew();
            //caseEdit.SaveClose();
            caseEdit.doSave();
        //}
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod5()
    {
        Test.startTest();
        serviceUser = InsertUserData(true);
        System.runAs ( serviceUser )
        {
            BusinessHours hours = [Select Id From BusinessHours limit 1];
            SVMXC__Service_Level__c sla = new SVMXC__Service_Level__c(SVMXC__Business_Hours__c = hours.Id);
            insert sla;


            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
            contract.Flex_Credit__c = 'PEC110';
            contract.SVMXC__Active__c = true;
            contract.SVMXC__End_Date__c = System.today() + 10;
            contract.SVMXC__Start_Date__c = System.today() - 10;
            contract.ERP_Order_Reason__c = 'Trade part Return';
            contract.SVMXC__Company__c = newAcc.Id;
            contract.ERP_Contract_Type__c = 'acme';
            contract.Quote_Reference__c = 'acme';
            contract.ERP_Sold_To__c = '2009845';
            contract.SVMXC__Service_Level__c = sla.Id;
            contract.EXT_Quote_Number__c = '1101';
            contract.ERP_Cancellation_Reason__c = 'Too expensive';
            insert contract;
            newCase.SVMXC__Service_Contract__c = contract.Id;
            newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId(); 
            newCase.Status = 'New';
            update newCase;
            iProduct.SVMXC__Service_Contract__c = contract.Id;
            update iProduct;
            Contact primaryCon = new Contact(FirstName = 'primaryCon', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
            primaryCon.Primary_Contact_Number__c ='Mobile';
            insert primaryCon;

            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',PrimaryCon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            Apexpages.currentPage().getParameters().put('ProductId', iProduct.Id); 
            Apexpages.currentPage().getParameters().put('PrimaryConId', 'Phone');
            Apexpages.currentPage().getParameters().put('AccountId', newAcc.id);

            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.TranslatedSearchtext = 'ABC';
            caseEdit.CaseDescrp = 'acme';
            caseEdit.SaveClose();
            caseEdit.Createcase();
            caseEdit.ProductSearch();
            caseEdit.EditPageRedirection();
            caseEdit.docancel();
        }
        Test.stopTest();
    }
    static testMethod void testSRCaseEditMethod6()
    {
        Test.startTest();
        serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser1 = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'zh_CN', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1@service.com');
        insert serviceUser1;
        //System.runAs ( serviceUser1 )
        //{
            BusinessHours hours = [Select Id From BusinessHours limit 1];
            SVMXC__Service_Level__c sla = new SVMXC__Service_Level__c(SVMXC__Business_Hours__c = hours.Id);
            insert sla;


            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
            contract.Flex_Credit__c = 'PEC110';
            contract.SVMXC__Active__c = true;
            contract.SVMXC__End_Date__c = System.today() + 10;
            contract.SVMXC__Start_Date__c = System.today() - 10;
            contract.ERP_Order_Reason__c = 'Trade part Return';
            contract.SVMXC__Company__c = newAcc.Id;
            contract.ERP_Contract_Type__c = 'acme';
            contract.Quote_Reference__c = 'acme';
            contract.ERP_Sold_To__c = '2009845';
            contract.SVMXC__Service_Level__c = sla.Id;
            contract.EXT_Quote_Number__c = '1101';
            contract.ERP_Cancellation_Reason__c = 'Too expensive';
            insert contract;
            newCase.SVMXC__Service_Contract__c = contract.Id;
            newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId(); 
            newCase.Status = 'New';
            newCase.Local_Resolution__c = 'acme';
            newCase.Local_Internal_Notes__c = 'acme';
            newCase.Local_Case_Activity__c = 'acme';
            newCase.Internal_Comments__c  = 'acme';
            newCase.Case_Resolution__c = 'acme';
            update newCase;
            iProduct.SVMXC__Service_Contract__c = contract.Id;
            update iProduct;
            Contact primaryCon = new Contact(FirstName = 'primaryCon', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
            insert primaryCon;

            Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',primarycon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            //Apexpages.currentPage().getParameters().put('ProductId', iProduct.Id); 
            Apexpages.currentPage().getParameters().put('PrimaryConId', 'Other Phone');
            Apexpages.currentPage().getParameters().put('AccountId', newAcc.id);
            Apexpages.currentpage().getparameters().put('SubjectName', 'acme');
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            SRCaseEditPageController.wrapperchild wr = new SRCaseEditPageController.wrapperchild();
            wr.CategoryName = 'c1';
            wr.CategoryLabel = 'c1';
            wr.selectedvalue = null;
            wr.optionslist = new List<SelectOption>();
            wr.optionslist.add(new SelectOption('Public', 'Public'));
            wr.optionslist.add(new SelectOption('Private', 'Private'));
            wr.optionslist.add(new SelectOption('Protect', 'Protect'));
            List<SRCaseEditPageController.wrapperchild> wras = new List<SRCaseEditPageController.wrapperchild>();
            wras.add(wr);
            caseEdit.Mapkeyval = wras;
            caseEdit.TranslatedSearchtext = 'ABC';
            caseEdit.CaseDescrp = 'acme';
            caseEdit.SaveClose();
            caseEdit.Createcase();
            caseEdit.ProductSearch();
            caseEdit.EditPageRedirection();
            caseEdit.docancel();
            caseEdit.SearchArticle();
        //}
        Test.stopTest();
    }

    static testMethod void testDocancel()
    {
        Test.startTest();
        serviceUser = InsertUserData(true);
        System.runAs ( serviceUser )
        {
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.PopulateDowntime();
            caseEdit.docancel();
        }
        Test.stopTest();
    }
    static testMethod void testDocancel1()
    {
        Test.startTest();
        serviceUser = InsertUserData(true);
        System.runAs ( serviceUser )
        {
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.PopulateDowntime();
            caseEdit.docancel();
        }
        Test.stopTest();
    }
    static testMethod void testDocancel2()
    {
        Test.startTest();
        serviceUser = InsertUserData(true);
        System.runAs ( serviceUser )
        {
            Apexpages.currentpage().getparameters().put('id',newCase.id);
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            caseEdit.PopulateDowntime();
            caseEdit.docancel();
            caseEdit.bool = true;
            caseEdit.callTextbox();
            caseEdit.caseidentifier = newCase;
            caseEdit.Issueid = newCase.Id;
            caseEdit.Tempcase = newCase;   
        }
        Test.stopTest();
    }
    /*
    static testMethod void testSRCaseEditMethod7()
    {
        
        serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser1 = new User(FirstName ='Test1', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'zh_CN', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test1@service.com');
        insert serviceUser1;
        //System.runAs ( serviceUser1 )
        //{
            BusinessHours hours = [Select Id From BusinessHours limit 1];
            SVMXC__Service_Level__c sla = new SVMXC__Service_Level__c(SVMXC__Business_Hours__c = hours.Id);
            insert sla;
            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
            contract.Flex_Credit__c = 'PEC110';
            contract.SVMXC__Active__c = true;
            contract.SVMXC__End_Date__c = System.today() + 10;
            contract.SVMXC__Start_Date__c = System.today() - 10;
            contract.ERP_Order_Reason__c = 'Trade part Return';
            contract.SVMXC__Company__c = newAcc.Id;
            contract.ERP_Contract_Type__c = 'acme';
            contract.Quote_Reference__c = 'acme';
            contract.ERP_Sold_To__c = '2009845';
            contract.SVMXC__Service_Level__c = sla.Id;
            contract.EXT_Quote_Number__c = '1101';
            contract.ERP_Cancellation_Reason__c = 'Too expensive';
            insert contract;
            newCase.SVMXC__Service_Contract__c = contract.Id;
            newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId(); 
            newCase.Status = 'New';
            newCase.Local_Resolution__c = 'acme';
            newCase.Local_Internal_Notes__c = 'acme';
            newCase.Local_Case_Activity__c = 'acme';
            newCase.Internal_Comments__c  = 'acme';
            newCase.Case_Resolution__c = 'acme';
            update newCase;
            
            Contact primaryCon = new Contact(FirstName = 'primaryCon', LastName = 'TestContact', Email = 'Test@1234APR.com', AccountId = newAcc.Id, 
            MailingCity='New York', MailingCountry='US', MailingPostalCode='552601', MailingState='CA', Phone = '414545445');
            primaryCon.Primary_Contact_Number__c ='Phone';
            insert primaryCon;
             Test.startTest();
            //Apexpages.currentpage().getparameters().put('id',newCase.id);
            Apexpages.currentpage().getparameters().put('ConId',primarycon.id);
            Apexpages.currentpage().getparameters().put('Accid',newAcc.id);
            Apexpages.currentpage().getparameters().put('InstalledProdId',iProduct.id);
            Apexpages.currentpage().getparameters().put('RecordType', newCase.RecordTypeId);
            Apexpages.currentpage().getparameters().put('SubjectName', 'acme');
            Apexpages.currentPage().getParameters().put('PrimaryConId', 'Email');
            Apexpages.currentPage().getParameters().put('ProductId', '23ffsdfsd'); 
            ApexPages.StandardController controller = new ApexPages.StandardController(newCase);
            SRCaseEditPageController caseEdit = new SRCaseEditPageController(controller);
            SRCaseEditPageController.wrapperchild wr = new SRCaseEditPageController.wrapperchild();
            wr.CategoryName = 'c1';
            wr.CategoryLabel = 'c1';
            wr.selectedvalue = null;
            wr.optionslist = new List<SelectOption>();
            wr.optionslist.add(new SelectOption('Public', 'Public'));
            wr.optionslist.add(new SelectOption('Private', 'Private'));
            wr.optionslist.add(new SelectOption('Protect', 'Protect'));
            List<SRCaseEditPageController.wrapperchild> wras = new List<SRCaseEditPageController.wrapperchild>();
            wras.add(wr);
            caseEdit.Mapkeyval = wras;
            caseEdit.TranslatedSearchtext = 'ABC';
            caseEdit.CaseDescrp = 'acme';
            caseEdit.getItems();
            caseEdit.gettypeItems();
            caseEdit.mySave();
            caseEdit.SaveNew();
            caseEdit.SearchArticle();
            caseEdit.doSave();
            caseEdit.ProductSearch();
            Test.stopTest();
        //}
       
    }
    */
}
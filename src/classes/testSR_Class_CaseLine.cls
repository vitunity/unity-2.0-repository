@isTest
public class testSR_Class_CaseLine
{
    public static Account newAcc;
    public static Sales_Order__c so;
    public static Sales_Order_Item__c soi;
    public static SVMXC__Site__c newLoc;
    public static Product2 newProd;
    public static Case newCase;
    public static Case newCase2;
    public static SVMXC__Case_Line__c newCaseLine;
    public static SVMXC__Case_Line__c newCaseLine1;
    public static SVMXC__Case_Line__c newCaseLine2;
    public static ERP_Project__c erpproject;
    public static ERP_WBS__c erpwbs;
    public static SVMXC__Service_Group__c servicegrp;
    public static Timecard_Profile__c timecardProfile;
    public static SVMXC__Service_Group_Members__c technician;
    public static SVMXC__Service_Group_Members__c technician1;
    public static ERP_Pcode__c prode;
    public static User serviceUser;
    public static SVMXC__Service_Order__c wo;
    public static SVMXC__Installed_Product__c iProduct;
    public static ERP_NWA__c erpnwa;
    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
    public static User InsertUserData(Boolean isInsert) 
    {
        profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test@service.com');
        try {
        if(isInsert)
            insert serviceUser;
        } catch(Exception e) {
            system.debug(' == Exception == ' + e.getMessage() + ' = ' + e.getLineNumber());
        }
        return serviceUser;
    }
    
    public static Account AccountData(Boolean isInsert) 
    {
        newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.Country__c = 'USA';
        if(isInsert)
            insert newAcc;
        return newAcc;
    }

    public static Product2 product_Data(Boolean isInsert) 
    {
        newProd = new Product2();
        newProd.Name = 'Rapid Arc for Eclipse';
        newProd.ProductCode = 'VC_RAE';
        newProd.BigMachines__Part_Number__c = 'VC_RAE';
        newProd.Product_Type__c = 'Part';
        newProd.IsActive = true;
        newProd.Product_Source__c = 'SAP';
        newProd.ERP_Plant__c = '0600';
        newProd.ERP_Sales_Org__c = '0600';
        if(isInsert)
            insert newProd;
        return newProd;
    }

    public static SVMXC__Site__c Location_Data(Boolean isInsert, Account newAcc1) 
    {
        newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc1.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        if(isInsert)
            insert newLoc;
        return newLoc;
    }
    
    public static Sales_Order_Item__c SOI_Data(Boolean isInsert, Sales_Order__c so1) 
    {
        soi = new Sales_Order_Item__c();
        soi.Name = '000005';
        soi.Sales_Order__c = so1.id;
        soi.ERP_Material_Number__c = 'FI_REV_ITEM';
        soi.ERP_Equipment_Number__c = '123';
        soi.WBS_Element__c = 'acme';
        soi.ERP_Item_Category__c = 'Z001';
        if(isInsert)
            insert soi;
        return soi;
    }

    public static Sales_Order__c SO_Data(Boolean isInsert, Account newAcc1) {
        so = new Sales_Order__c();
        so.name = '319112526';
        so.Site_Partner__c = newAcc1.Id;
        so.Sold_To__c = newAcc1.Id;
        so.Requested_Delivery_Date__c = system.today().addMonths(-1);
        so.Sales_Group__c = 'N08';
        so.Sales_District__c = 'NA02';
        so.Status__c ='Open';
        so.Sales_Org__c = '0601';
        if(isInsert)
            insert so;
        return so;
    }
    
    public static ERP_Pcode__c ERPPCODE_Data(Boolean isInsert) {
        prode = new ERP_Pcode__c();
        prode.Name = 'H19';
        prode.ERP_Pcode_2__c = '19';
        prode.Description__c = 'TrueBeam';
        prode.IsActive__c = true;
        prode.Service_Product_Group__c = 'LINAC';
        if(isInsert)
            insert prode;
        return prode;
    }

    public static SVMXC__Installed_Product__c InstalledProduct_Data(Boolean isInsert, Product2 newProd1, Account newAcc1, SVMXC__Site__c newLoc1,
                                                                    SVMXC__Service_Group__c serviceTeam1, ERP_Pcode__c erpPcode1) {
        SVMXC__Installed_Product__c installProd = new SVMXC__Installed_Product__c();
        installProd.Name = 'H621598';
        installProd.SVMXC__Status__c = 'Installed';
        installProd.ERP_Reference__c = 'H621598-VE';
        installProd.SVMXC__Product__c = newProd1.Id;
        installProd.ERP_Pcodes__c = erpPcode1.Id;
        installProd.SVMXC__Company__c = newAcc1.Id;
        installProd.SVMXC__Site__c = newLoc1.Id;
        installProd.ERP_Functional_Location__c = 'H-COLUMBUS -OH-US-005';
        installProd.Billing_Type__c = 'P – Paid Service';
        installProd.Service_Team__c = serviceTeam1.Id;
        installProd.ERP_CSS_District__c = 'U9J';
        installProd.ERP_Work_Center__c = 'H87696';
        installProd.ERP_Product_Code__c = 'H62A';
        if(isInsert)
            insert installProd;
        return installProd;
    }
    
    // Create CASE
    public static Case Case_Data(Boolean isInsert, Account newAcc1, Product2 newProd1) {
        newCase = new Case();
        newCase.AccountId = newAcc1.Id;
        newCase.Status = 'New';
        newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId();
        newCase.Product__c = newProd1.Id; 
        newCase.ProductSystem__c = iProduct.Id;
        if(isInsert)
            insert newCase;
        return newCase;
    }
    // Creating Case Line Data
    public static SVMXC__Case_Line__c CaseLine_Data(Boolean isInsert, Case newCase1, product2 newProd1) {
        ID TimeEntryRT = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Time Entry').getRecordTypeId();// Time Entry   

        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase1.id;
        //caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd1.Id;
        caseLine.Start_Date_time__c = Datetime.now();
        caseLine.End_date_time__c = caseLine.Start_Date_time__c.addHours(4);
        caseLine.Sales_Order_Item__c = soi.Id;
        caseLine.SVMXC__Installed_Product__c = iProduct.Id;
        caseLine.Case_Line_Owner__c = Userinfo.getUserId();
        caseLine.recordTypeId = TimeEntryRT;
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    public static SVMXC__Case_Line__c CaseLine_Data1(Boolean isInsert, Case newCase1, product2 newProd1) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase1.id;
        //caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd1.Id;
        caseLine.Start_Date_time__c = Datetime.now();
        caseLine.End_date_time__c = null;
        caseLine.Sales_Order_Item__c = soi.Id;
        caseLine.SVMXC__Installed_Product__c = iProduct.Id;
        caseLine.Case_Line_Owner__c = Userinfo.getUserId();
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    public static SVMXC__Case_Line__c CaseLine_Data2(Boolean isInsert, Case newCase1, product2 newProd1) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase1.id;
        //caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd1.Id;
        caseLine.Start_Date_time__c = Datetime.now();
        caseLine.End_date_time__c = null;
        caseLine.Sales_Order_Item__c = soi.Id;
        caseLine.SVMXC__Installed_Product__c = iProduct.Id;
        caseLine.Case_Line_Owner__c = Userinfo.getUserId();
        caseLine.Work_Order__c = wo.Id;
        caseLine.SVMXC__Line_Status__c = 'Cancelled';
        caseLine.ERP_NWA__c = erpnwa.id;
        caseLine.Billing_Type__c = 'C – Contract';
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    static
    {
        newAcc = AccountData(true);
        so = SO_Data(true, newAcc);
        soi = SOI_Data(true, so);
        prode = ERPPCODE_Data(true);
        newLoc = Location_Data(true, newAcc);
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        newProd = product_Data(true);
        iProduct = InstalledProduct_Data(true, newProd, newAcc, newLoc, servicegrp, prode);
        newCase = Case_Data(true, newAcc, newProd);
        newCase2 = Case_Data(true, newAcc, newProd);
        newCase2.status = 'Closed';
        update newCase2;

        timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        insert timecardProfile;

        technician = new SVMXC__Service_Group_Members__c
        (
            name = 'testtechnician',
            User__c = Userinfo.getUserId(),
            SVMXC__Service_Group__c = servicegrp.id,
            SVMXC__Country__c = 'India',
            SVMXC__Street__c = 'abc',
            SVMXC__Zip__c = '54321',
            Timecard_Profile__c = timecardProfile.id,
            SVMXC__Salesforce_User__c = Userinfo.getUserId()
        );

        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician.Timecard_Profile__c = timecardProfile.Id;

        technician1 = new SVMXC__Service_Group_Members__c
        (
            name = 'testtechnician',
            User__c = Userinfo.getUserId(),
            SVMXC__Service_Group__c = servicegrp.id,
            SVMXC__Country__c = 'India',
            SVMXC__Street__c = 'abc',
            SVMXC__Zip__c = '54321',
            SVMXC__Salesforce_User__c = Userinfo.getUserId(),
            Timecard_Profile__c = timecardProfile.id
        );

        technician1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician1.Timecard_Profile__c = timecardProfile.Id;
        List<SVMXC__Service_Group_Members__c> technicians = new List<SVMXC__Service_Group_Members__c>();
        technicians.add(technician);
        technicians.add(technician1);
        insert technicians;
        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;

        erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id, ERP_Reference__c = 'acme', Forecast_Start_Date__c = Date.today(), Forecast_End_Date__c = Date.today());
        insert erpwbs;

        wo = new SVMXC__Service_Order__c
        (
            SVMXC__Group_Member__c = technician.id,
            SVMXC__Site__c = newLoc.id,
            SVMXC__Company__c = newAcc.id,
            SVMXC__Case__c = newCase.id,
            ERP_Service_Order__c = '12345678',
            ERP_WBS__c = erpwbs.id
        );
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        wo.Event__c = true;
        insert wo;
        erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC');
        insert erpnwa;
    }

    static testMethod void testUpdateCaseAndWOStatusOnCancel()
    {
            Test.startTest();
            newCaseLine = CaseLine_Data(true, newCase, newProd);
            newCaseLine1 = CaseLine_Data(true, newCase, newProd);
            SVMXC__Service_Pricebook__c objSP = new SVMXC__Service_Pricebook__c();
            objSP.SVMXC__Active__c = true;
            objSP.Name ='price1';
            insert objSP;      

            SVMXC__Service_Plan__c objPlan= new SVMXC__Service_Plan__c();
            objPlan.SVMXC__Service_Pricebook__c = objSP.id;
            insert objPlan;

            list<SVMXC__Service_Level__c> lstSLaTerm = new list<SVMXC__Service_Level__c>();
            SVMXC__Service_Level__c objserviceLevel = new SVMXC__Service_Level__c();
            objserviceLevel.Name= 'TestSlaTerm001';
            lstSLaTerm.add(objserviceLevel);

            SVMXC__Service_Level__c objserviceLevel1 = new SVMXC__Service_Level__c();
            objserviceLevel1.Name = 'TestSlaTerm002';
            lstSLaTerm.add(objserviceLevel1);
            Insert lstSLaTerm;
            SVMXC__Service_Contract__c testServiceContract = Sr_testdata.createServiceContract();
            testServiceContract.SVMXC__Service_Level__c = lstSLaTerm[0].id;
            testServiceContract.SVMXC__Company__c = newAcc.Id;
            testServiceContract.Location__c = newLoc.id;
            testServiceContract.SVMXC__Service_Plan__c  = objPlan.id;
            insert testServiceContract;

            SVMXC__Service_Contract_Products__c testContractProduct = new SVMXC__Service_Contract_Products__c();
            testContractProduct.SVMXC__Installed_Product__c = iProduct.id;
            testContractProduct.SVMXC__Service_Contract__c = testServiceContract.id;
            testContractProduct.SVMXC__SLA_Terms__c =  lstSLaTerm[1].id;
            testContractProduct.Cancelation_Reason__c = null;
            testContractProduct.SVMXC__Start_Date__c = system.Today().addDays(4);
            testContractProduct.SVMXC__End_Date__c =   system.Today().addDays(10);
            testContractProduct.Serial_Number_PCSN__c = 'test';
            insert testContractProduct;

            newCaseLine.Work_Order__c = wo.Id;
            newCaseLine.SVMXC__Line_Status__c = 'Cancelled';
            ID TimeEntryRT = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Time Entry').getRecordTypeId();// Time Entry   
            newCaseLine.recordTypeId = TimeEntryRT;
            update newCaseLine;
            Test.stopTest();
    }
    static testMethod void testUpdateCaseAndWOStatusOnCancel1()
    {
            Test.startTest();
            ID TimeEntryRT = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName().get('Time Entry').getRecordTypeId();// Time Entry   
            newCaseLine = CaseLine_Data(true, newCase, newProd);
            newCaseLine.recordTypeId = TimeEntryRT;
            newCase.Status = 'Cancelled';
            update newCase;
            list<SVMXC__Case_Line__c> lstCaseLine = new list<SVMXC__Case_Line__c>();
            map<id, SVMXC__Case_Line__c> oldMap = new map<id, SVMXC__Case_Line__c>();
            lstCaseLine.add(newCaseLine);
            oldMap.put(newCaseLine.Id, newCaseLine);
            SR_Class_CaseLine caseLine = new SR_Class_CaseLine();
            caseLine.PrePoulateFldsOnCaseLine(lstCaseLine, oldMap);
            Test.stopTest();
    }
    
    static testMethod void testUpdateSOIfromERPWBS()
    {
        Test.startTest();
        Set<Id> setCaseLineSOI = new Set<Id>();
        setCaseLineSOI.add(soi.Id);
        SR_Class_CaseLine caseLine = new SR_Class_CaseLine();
        SR_Class_CaseLine.MapERPRef_WBS.put(soi.WBS_Element__c, erpwbs); 
        caseLine.updateSOIfromERPWBS(setCaseLineSOI);
        Test.stopTest();
    }

        
}
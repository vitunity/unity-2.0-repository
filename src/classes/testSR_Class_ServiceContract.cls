@isTest
public class testSR_Class_ServiceContract
{
    public static Account accnt;
    public static Account accnt1;
    public static SVMXC__Site__c loc;
    public static product2 prodObj;
    public static SVMXC__Service_Contract__c contractRenewedOld;
    public static SVMXC__Service_Contract__c contractRenewed;
    public static SVMXC__Service_Contract__c contract;
    public static SVMXC__Service_Contract__c contract1;
    public static SVMXC__Service_Level__c sla;
    static
    {
        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        accnt = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name = 'Test Account', CurrencyIsoCode = 'USD', Country__c = 'India',OMNI_Postal_Code__c = '21234',AccountNumber = '2009845');
        //accnt.Affiliation__c = 'National';
        insert accnt;

        accnt1 = new Account(Recordtypeid=AccMapByName.get('Sold To').getRecordTypeId(), Name = 'Test Account', CurrencyIsoCode = 'USD', Country__c = 'India',OMNI_Postal_Code__c = '21234',AccountNumber = '2009845');
        //accnt1.Affiliation__c = 'National';
        insert accnt1;

        loc = new SVMXC__Site__c(Name = 'test location', SVMXC__Street__c = '60 N McCarthy', SVMXC__Country__c = 'United States', SVMXC__Zip__c = '95035', CurrencyIsoCode = 'USD', SVMXC__Account__c = accnt.id, ERP_Reference__c = '0975');
        insert loc;

        prodObj = new product2(Name = 'test product', CurrencyIsoCode = 'USD', ERP_Pcode_4__c = '466643', ProductCode = '1212');
        insert prodObj;

        BusinessHours hours = [Select Id From BusinessHours limit 1];
        sla = new SVMXC__Service_Level__c(SVMXC__Business_Hours__c = hours.Id);
        insert sla;
        List<SVMXC__Service_Contract__c> contracts = new List<SVMXC__Service_Contract__c>();
        contractRenewedOld = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
        contractRenewedOld.Flex_Credit__c = 'PEC110';
        contractRenewedOld.SVMXC__Active__c = false;
        contractRenewedOld.SVMXC__End_Date__c = System.today() + 10;
        contractRenewedOld.SVMXC__Start_Date__c = System.today() - 10;
        contractRenewedOld.ERP_Order_Reason__c = 'Trade part Return';
        contractRenewedOld.SVMXC__Company__c = accnt.Id;
        contractRenewedOld.ERP_Contract_Type__c = 'acme';
        contractRenewedOld.Quote_Reference__c = 'acme';
        contractRenewedOld.ERP_Sold_To__c = '2009845';
        contractRenewedOld.EXT_Quote_Number__c = '1101';
        contractRenewedOld.SVMXC__Service_Level__c = sla.Id;
        contractRenewedOld.ERP_Cancellation_Reason__c = 'Too expensive';
        contracts.add(contractRenewedOld);

        contractRenewed = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
        contractRenewed.Flex_Credit__c = 'PEC110';
        contractRenewed.SVMXC__Active__c = false;
        contractRenewed.SVMXC__End_Date__c = System.today() + 10;
        contractRenewed.SVMXC__Start_Date__c = System.today() - 10;
        contractRenewed.ERP_Order_Reason__c = 'Trade part Return';
        contractRenewed.SVMXC__Company__c = accnt.Id;
        contractRenewed.ERP_Contract_Type__c = 'acme';
        contractRenewed.Quote_Reference__c = 'acme';
        contractRenewed.ERP_Sold_To__c = '2009845';
        contractRenewed.EXT_Quote_Number__c = '1101';
        contractRenewed.SVMXC__Service_Level__c = sla.Id;
        contractRenewed.ERP_Cancellation_Reason__c = 'Too expensive';
        contracts.add(contractRenewed);

        contract = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
        contract.Flex_Credit__c = 'PEC110';
        contract.SVMXC__Active__c = true;
        contract.SVMXC__End_Date__c = System.today() + 10;
        contract.SVMXC__Start_Date__c = System.today() - 10;
        contract.ERP_Order_Reason__c = 'Trade part Return';
        contract.SVMXC__Company__c = accnt.Id;
        contract.SVMXC__Renewed_From__c = contractRenewed.Id;
        contract.ERP_Contract_Type__c = 'acme';
        contract.Quote_Reference__c = 'acme';
        contract.ERP_Sold_To__c = '2009845';
        contract.SVMXC__Service_Level__c = sla.Id;
        contract.EXT_Quote_Number__c = '1101';
        contract.ERP_Cancellation_Reason__c = 'Too expensive';
        contracts.add(contract);

        contract1 = new SVMXC__Service_Contract__c(SVMXC__Service_Level__c = sla.Id);
        contract1.Flex_Credit__c = 'PEC110';
        contract1.SVMXC__Active__c = false;
        contract1.SVMXC__End_Date__c = System.today() + 10;
        contract1.SVMXC__Start_Date__c = System.today() - 10;
        contract1.ERP_Order_Reason__c = 'Trade part Return';
        contract1.SVMXC__Company__c = accnt1.Id;
        contract1.SVMXC__Renewed_From__c = contractRenewed.Id;
        contract1.ERP_Contract_Type__c = 'acme';
        contract1.Quote_Reference__c = 'acme';
        contract1.ERP_Sold_To__c = '2009845';
        contract1.EXT_Quote_Number__c = '1101';
        contract1.SVMXC__Service_Level__c = sla.Id;
        contract1.ERP_Cancellation_Reason__c = 'Too expensive';
        contracts.add(contract1);
        insert contracts;

        SVMXC__Installed_Product__c objIP = new SVMXC__Installed_Product__c(Name = 'testIP1', ERP_Work_Center__c = 'abc', SVMXC__Preferred_Technician__c = null, 
        SVMXC__Product__c = prodObj.id, SVMXC__Status__c = 'Installed', SVMXC__Site__c = loc.id, SVMXC__Serial_Lot_Number__c = 'test23', SVMXC__Company__c = accnt.id, PMP_Active__c = false);
        objIP.SVMXC__Service_Contract__c = contract.id;
        insert objIP;

        List<SVMXC__Service_Contract_Products__c> products = new List<SVMXC__Service_Contract_Products__c>();
        SVMXC__Service_Contract_Products__c testContractProduct1 = new SVMXC__Service_Contract_Products__c();
        testContractProduct1.SVMXC__Installed_Product__c = objIP.id;
        testContractProduct1.SVMXC__Service_Contract__c = contract.id;
        testContractProduct1.SVMXC__SLA_Terms__c =  sla.id;
        testContractProduct1.Cancelation_Reason__c = null;
        testContractProduct1.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct1.SVMXC__End_Date__c =   system.Today().addDays(10);
        testContractProduct1.Serial_Number_PCSN__c = 'test';

        products.add(testContractProduct1);
        SVMXC__Service_Contract_Products__c testContractProduct2 = new SVMXC__Service_Contract_Products__c();
        testContractProduct2.SVMXC__Installed_Product__c = objIP.id;
        testContractProduct2.SVMXC__Service_Contract__c = contract1.id;
        testContractProduct2.SVMXC__SLA_Terms__c =  sla.id;
        testContractProduct2.Cancelation_Reason__c = null;
        testContractProduct2.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct2.SVMXC__End_Date__c =   system.Today().addDays(10);
        testContractProduct2.Serial_Number_PCSN__c = 'test';
        products.add(testContractProduct2);
        SVMXC__Service_Contract_Products__c testContractProduct3 = new SVMXC__Service_Contract_Products__c();
        testContractProduct3.SVMXC__Installed_Product__c = objIP.id;
        testContractProduct3.SVMXC__Service_Contract__c = contractRenewed.id;
        testContractProduct3.SVMXC__SLA_Terms__c =  sla.id;
        testContractProduct3.Cancelation_Reason__c = null;
        testContractProduct3.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct3.SVMXC__End_Date__c =   system.Today().addDays(10);
        testContractProduct3.Serial_Number_PCSN__c = 'test';
        products.add(testContractProduct3);

        SVMXC__Service_Contract_Products__c testContractProduct4 = new SVMXC__Service_Contract_Products__c();
        testContractProduct4.SVMXC__Installed_Product__c = objIP.id;
        testContractProduct4.SVMXC__Service_Contract__c = contract.id;
        testContractProduct4.SVMXC__SLA_Terms__c =  sla.id;
        testContractProduct4.Cancelation_Reason__c = null;
        testContractProduct4.SVMXC__Start_Date__c = system.Today().addDays(4);
        testContractProduct4.Serial_Number_PCSN__c = 'test';
        products.add(testContractProduct4);
        insert products;

        Test.startTest();
        List<SVMXC__SLA_Detail__c> details = new List<SVMXC__SLA_Detail__c>();
        SVMXC__SLA_Detail__c sld1 = new SVMXC__SLA_Detail__c();
        sld1.Installed_Product__c = objIP.id;
        sld1.SVMXC__SLA_Terms__c = sla.Id;
        details.add(sld1);
        SVMXC__SLA_Detail__c sld2 = new SVMXC__SLA_Detail__c();
        sld2.Installed_Product__c = objIP.id;
        sld2.SVMXC__SLA_Terms__c = sla.Id;
        details.add(sld2);
        SVMXC__SLA_Detail__c sld3 = new SVMXC__SLA_Detail__c();
        sld3.Installed_Product__c = objIP.id;
        sld3.SVMXC__SLA_Terms__c = sla.Id;
        details.add(sld3);
        insert details;
        SVMXC__Counter_Details__c scd = new SVMXC__Counter_Details__c();
        scd.SVMXC__Active__c = true;
        scd.SVMXC__Covered_Products__c = testContractProduct1.id;
        scd.SVMXC__Service_Maintenance_Contract__c = contractRenewed.Id;
        scd.SLA_Detail__c = sld1.Id;
        insert scd;

        SVMXC__Service_Group__c servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        
        Timecard_Profile__c timecardprof = new Timecard_Profile__c(First_Day_of_Week__c = 'Saturday', Name = 'Test TimeCard Profile');
        timecardprof.Normal_Start_Time__c = datetime.newInstance(2014, 12, 1, 9, 0, 0);
        insert timecardprof;
        
        Timecard_Profile__c timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        insert timecardProfile;
        
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(name = 'testtechnician',User__c = Userinfo.getUserId(),SVMXC__Service_Group__c = servicegrp.id,SVMXC__Country__c = 'India',SVMXC__Street__c = 'abc',SVMXC__Zip__c = '54321',Timecard_Profile__c = timecardprof.id);
        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician.Timecard_Profile__c = timecardProfile.Id;
        insert technician;

        ERP_NWA__c erpnwa = new ERP_NWA__c(ERP_Status__c = 'REL;SETC', ERP_Reference__c = 'erpnwa1234');
        insert erpnwa;

        Id wdId = RecordTypeHelperClass.WORK_DETAIL_RECORDTYPES.get('Products Serviced').getRecordTypeId();
        SVMXC__Service_Order__c order = new SVMXC__Service_Order__c(SVMXC__Top_Level__c = objIP.Id);
        insert order;
        SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
        wd.SVMXC__Consumed_From_Location__c = loc.id;
        wd.SVMXC__Activity_Type__c = 'Install';
        wd.Part_Disposition__c = 'Installing';
        wd.Completed__c=false;
        wd.SVMXC__Group_Member__c = technician.id;
        wd.Purchase_Order_Number1__c = '12345';
        wd.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Products Serviced').getRecordTypeId();
        wd.SVMXC__Work_Description__c = 'test';
        wd.SVMXC__Start_Date_and_Time__c =System.now();
        wd.Actual_Hours__c = 5;
        wd.SVMXC__Service_Order__c = order.id;
        wd.NWA_Description__c = 'nwatest';
        wd.ERP_NWA__c = erpnwa.id;
        wd.SVMXC__Serial_Number__c = objIP.id;
        insert wd;

        Counter_Usage__c cu = new Counter_Usage__c();
        cu.Counter_Detail__c = scd.id;
        cu.Work_Detail__c = wd.id;
        insert cu; //Wave1

        ERP_Partner__c ec1 = new ERP_Partner__c();
        ec1.Name = 'TestPartner';
        ec1.Partner_Number__c = '345678';
        insert ec1;

        ERP_Partner_Association__c epa1 = new ERP_Partner_Association__c();
        epa1.Name = 'Partner1';
        epa1.ERP_Customer_Number__c = '125687';
        //epa1.ERP_Partner_Number__c = '100053218';
        epa1.ERP_Partner__c = ec1.id;
        epa1.Partner_Function__c = 'EU=End User'; 
        epa1.Sales_Org__c = '0600';
        epa1.Customer_Account__c = accnt.id;
        insert epa1;

        SVMXC__Service_Pricebook__c objSP = new SVMXC__Service_Pricebook__c();
        objSP.SVMXC__Active__c = true;
        objSP.Name ='price1';
        insert objSP;      

        SVMXC__Service_Plan__c objPlan= new SVMXC__Service_Plan__c();
        objPlan.SVMXC__Service_Pricebook__c = objSP.id;
        objPlan.Contract_Type__c = 'acme';
        insert objPlan;
        Test.stopTest();
    }

    static testMethod void testPopulateFieldsOnContract()
    {
        contractRenewed.SVMXC__Service_Level__c = sla.Id;
        contractRenewed.ERP_Cancellation_Reason__c = 'Too expensive';
        contractRenewed.EXT_Quote_Number__c = '1101';
        contractRenewed.SVMXC__End_Date__c = System.today() + 20;
        contractRenewed.SVMXC__Start_Date__c = System.today() - 20;
        contractRenewed.SVMXC__Active__c = true;
        update contractRenewed;
    }

    static testMethod void testUpdateChildAndRelatedObject1()
    {
        contract.SVMXC__Service_Level__c = sla.Id;
        contract.ERP_Cancellation_Reason__c = 'Too expensive';
        contract.EXT_Quote_Number__c = '1101';
        contract.SVMXC__End_Date__c = System.today() + 20;
        contract.SVMXC__Start_Date__c = System.today() - 20;
        contract.SVMXC__Active__c = false;
        update contract;
    }

    static testMethod void testUpdateChildAndRelatedObject2()
    {
        contract1.SVMXC__Service_Level__c = sla.Id;
        contract1.ERP_Cancellation_Reason__c = 'Too expensive';
        contract1.EXT_Quote_Number__c = '1101';
        contract1.SVMXC__End_Date__c = System.today() + 20;
        contract1.SVMXC__Start_Date__c = System.today() - 20;
        contract1.SVMXC__Active__c = true;
        update contract1;
    }

    static testMethod void testUpdateChildAndRelatedObject3()
    {
        contract.SVMXC__Service_Level__c = sla.Id;
        contract.ERP_Cancellation_Reason__c = 'Too expensive';
        contract.EXT_Quote_Number__c = '1101';
        contract.SVMXC__End_Date__c = System.today() + 20;
        contract.SVMXC__Start_Date__c = System.today() - 20;
        contract.SVMXC__Active__c = false;
        update contract;
    }
}
@isTest(SeeAllData=true)
public class testSR_ContactRoleAssoAfterTrigger
{
    static Account soldTo = SR_testdata.creteAccount();
    static Account soldToDefault = SR_testdata.creteAccount(); 
    static Account siteParter = SR_testdata.creteAccount();
    static Contact contact1 = SR_testdata.createContact();
    static Contact contact2 = SR_testdata.createContact();
    static Invoice__c invoice1 = new Invoice__c();
    static Country__c objCounty = new Country__c();
    static BigMachines__Quote__c bq = new BigMachines__Quote__c();
    static SVMXC__Service_Order__c wo;
    static Case casealias;
    public Static User u;

    static
    {
        Id soldTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
        Id siteParterId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Site Partner').getRecordTypeId();
        List<Account> accs = new List<Account>();
        soldTo.AccountNumber = '123456';
        soldTo.country__c = 'India';
        soldTo.BillingState = 'MH';
        //soldTo.ERP_Site_Partner_Code__c = '587855';
        soldTo.BillingCity = 'Pune';
        soldTo.ERP_Timezone__c = 'AUSSA';
        soldTo.recordTypeId = soldTypeId;
        soldTo.Ext_Cust_Id__c = '123456';
        accs.add(soldTo);

        soldToDefault.AccountNumber = '555555';
        soldToDefault.country__c = 'India';
        soldToDefault.BillingState = 'MH';
        soldToDefault.BillingCity = 'Pune';
        soldToDefault.ERP_Timezone__c = 'AUSSA';
        soldToDefault.recordTypeId = soldTypeId;
        soldToDefault.Ext_Cust_Id__c = '555555';
        accs.add(soldToDefault);

        siteParter.AccountNumber = '587855';
        siteParter.ERP_Site_Partner_Code__c = '587855';
        siteParter.country__c = 'India';
        siteParter.BillingState = 'MH';
        siteParter.BillingCity = 'Pune';
        siteParter.ERP_Timezone__c = 'AUSSA';
        siteParter.recordTypeId = siteParterId;
        accs.add(siteParter);
        insert accs;

        List<Contact> cons = new List<Contact>();
        contact1.AccountId = soldTo.Id;
        contact1.Email = 'abc345th@xyz.com';
        contact1.mailingcountry = 'India';
        contact1.ERP_Payer_Number__c = '132222';
        contact1.Primary_Billing_Contact__c = true;
        cons.add(contact1);

        contact2.AccountId = siteParter.Id;
        contact2.Email = 'abcdeeef123@xyz.com';
        contact2.mailingcountry = 'India';
        contact2.ERP_Payer_Number__c = '132244';
        contact2.Primary_Billing_Contact__c = true;
        cons.add(contact2);
        insert cons;

        Profile pr = [Select id from Profile where name = 'VMS MyVarian - Customer User'];
        
        u = new user(alias = 'standt', contactId = contact2.Id, email = 'standardtestuse92@testorg.com',emailencodingkey = 'UTF-8', lastname = 'Testing',languagelocalekey = 'en_US',localesidkey = 'en_US', profileid = pr.Id, timezonesidkey = 'America/Los_Angeles', username = 'standardtestuse92@testclass.com');
        insert u;
    }
    static testMethod void testCRA()
    {
        Test.startTest();
        List<Contact_Role_Association__c> cras = new List<Contact_Role_Association__c>();
        Contact_Role_Association__c cra = new Contact_Role_Association__c();
        cra.Account__c = soldTo.Id;
        cra.Contact__c = contact1.Id;
        cra.Role__c = 'Account Payable Capital';
        cras.add(cra);

        Contact_Role_Association__c cra2 = new Contact_Role_Association__c();
        cra2.Account__c = siteParter.Id;
        cra2.Contact__c = contact2.Id;
        cra2.Role__c = 'Account Payable Capital';
        cras.add(cra2);
        insert cras;
        cra.Contact__c = contact2.Id;
        update cra;
        delete cra;
        Test.stopTest();
    }
}
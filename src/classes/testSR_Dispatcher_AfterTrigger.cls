@isTest(SeeAllData=true)
public class testSR_Dispatcher_AfterTrigger
{

    public static User systemuser
    {
        set;
        get
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            return thisUser;
        }
    }
    
    
    
    static testMethod void testDispatcher() 
    {
        Recordtype recServiceTeam = [Select id,Name,Description from Recordtype where developername = 'Technician' and SObjectType=:'SVMXC__Service_Group__c'];
        SVMXC__Service_Group__c ServiceTeam = new SVMXC__Service_Group__c();
        ServiceTeam.SVMXC__Active__c= true;
        ServiceTeam.ERP_Reference__c='TestExternal';
        ServiceTeam.Name = 'TestService';
        ServiceTeam.recordtypeId = recServiceTeam.id;
        Insert ServiceTeam;
        
        SVMXC__Dispatcher_Access__c TestDA = new SVMXC__Dispatcher_Access__c();
        TestDA.SVMXC__Dispatcher__c = systemuser.id;
        TestDA.SVMXC__Service_Team__c =ServiceTeam.id ;
        insert TestDA;

        update TestDA;
        delete TestDA;
    }
}
@isTest
public class testSR_STB_Status
{
    public static Account newAcc;
    public static Sales_Order__c so;
    public static Sales_Order_Item__c soi;
    public static SVMXC__Site__c newLoc;
    public static Product2 newProd;
    public static Case newCase;
    public static SVMXC__Case_Line__c newCaseLine;
    public static ERP_Project__c erpproject;
    public static ERP_WBS__c erpwbs;
    public static SVMXC__Service_Group__c servicegrp;
    public static Timecard_Profile__c timecardProfile;
    public static SVMXC__Service_Group_Members__c technician;
    public static ERP_Pcode__c prode;
    public static User serviceUser;
    public static SVMXC__Service_Order__c wo;
    public static SVMXC__Installed_Product__c iProduct;

    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> caseLineRecordType = Schema.SObjectType.SVMXC__Case_Line__c.getRecordTypeInfosByName();
    public static Map<String,Schema.RecordTypeInfo> locationRecordType = Schema.SObjectType.SVMXC__Site__c.getRecordTypeInfosByName();
    public static User InsertUserData(Boolean isInsert) 
    {
        profile serviceUserProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'VMS Unity 1C - Service User'];
        serviceUser = new User(FirstName ='Test', LastName = 'Service-User', ProfileId = serviceUserProfile.Id, Email = 'test@service.com',
                        alias = 'servt', TimeZoneSidKey = 'GMT', LanguageLocaleKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US',UserName='test@service.com');
        try {
        if(isInsert)
            insert serviceUser;
        } catch(Exception e) {
            system.debug(' == Exception == ' + e.getMessage() + ' = ' + e.getLineNumber());
        }
        return serviceUser;
    }
    
    public static Account AccountData(Boolean isInsert) 
    {
        newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.Country__c = 'USA';
        if(isInsert)
            insert newAcc;
        return newAcc;
    }

    public static Product2 product_Data(Boolean isInsert) 
    {
        newProd = new Product2();
        newProd.Name = 'Rapid Arc for Eclipse';
        newProd.ProductCode = 'VC_RAE';
        newProd.BigMachines__Part_Number__c = 'VC_RAE';
        newProd.Product_Type__c = 'Part';
        newProd.IsActive = true;
        newProd.Product_Source__c = 'SAP';
        newProd.ERP_Plant__c = '0600';
        newProd.ERP_Sales_Org__c = '0600';
        if(isInsert)
            insert newProd;
        return newProd;
    }

    public static SVMXC__Site__c Location_Data(Boolean isInsert, Account newAcc1) 
    {
        newLoc = new SVMXC__Site__c();
        newLoc.Name = 'OHIO STATE UNIV - JAMES CANCER HOSPITAL';
        newLoc.recordTypeId = locationrecordType.get('Standard Location').getRecordTypeId();
        newLoc.SVMXC__Account__c = newAcc1.Id;
        newLoc.ERP_Functional_Location__c = 'H-COLUMBUS          -OH-US-005';
        newLoc.SVMXC__Location_Type__c = 'Customer';
        newLoc.SVMXC__Country__c = 'USA';
        if(isInsert)
            insert newLoc;
        return newLoc;
    }
    
    public static Sales_Order_Item__c SOI_Data(Boolean isInsert, Sales_Order__c so1) 
    {
        soi = new Sales_Order_Item__c();
        soi.Name = '000005';
        soi.Sales_Order__c = so1.id;
        soi.ERP_Material_Number__c = 'FI_REV_ITEM';
        soi.ERP_Equipment_Number__c = '123';
        if(isInsert)
            insert soi;
        return soi;
    }

    public static Sales_Order__c SO_Data(Boolean isInsert, Account newAcc1) {
        so = new Sales_Order__c();
        so.name = '319112526';
        so.Site_Partner__c = newAcc1.Id;
        so.Sold_To__c = newAcc1.Id;
        so.Requested_Delivery_Date__c = system.today().addMonths(-1);
        so.Sales_Group__c = 'N08';
        so.Sales_District__c = 'NA02';
        so.Status__c ='Open';
        so.Sales_Org__c = '0601';
        if(isInsert)
            insert so;
        return so;
    }
    
    public static ERP_Pcode__c ERPPCODE_Data(Boolean isInsert) {
        prode = new ERP_Pcode__c();
        prode.Name = 'H19';
        prode.ERP_Pcode_2__c = '19';
        prode.Description__c = 'TrueBeam';
        prode.IsActive__c = true;
        prode.Service_Product_Group__c = 'LINAC';
        if(isInsert)
            insert prode;
        return prode;
    }

    public static SVMXC__Installed_Product__c InstalledProduct_Data(Boolean isInsert, Product2 newProd1, Account newAcc1, SVMXC__Site__c newLoc1,
                                                                    SVMXC__Service_Group__c serviceTeam1, ERP_Pcode__c erpPcode1) {
        SVMXC__Installed_Product__c installProd = new SVMXC__Installed_Product__c();
        installProd.Name = 'H621598';
        installProd.SVMXC__Status__c = 'Installed';
        installProd.ERP_Reference__c = 'H621598-VE';
        installProd.SVMXC__Product__c = newProd1.Id;
        installProd.ERP_Pcodes__c = erpPcode1.Id;
        installProd.SVMXC__Company__c = newAcc1.Id;
        installProd.SVMXC__Site__c = newLoc1.Id;
        installProd.ERP_Functional_Location__c = 'H-COLUMBUS -OH-US-005';
        installProd.Billing_Type__c = 'P – Paid Service';
        installProd.Service_Team__c = serviceTeam1.Id;
        installProd.ERP_CSS_District__c = 'U9J';
        installProd.ERP_Work_Center__c = 'H87696';
        installProd.ERP_Product_Code__c = 'H62A';
        if(isInsert)
            insert installProd;
        return installProd;
    }
    
    // Create CASE
    public static Case Case_Data(Boolean isInsert, Account newAcc1, Product2 newProd1) {
        newCase = new Case();
        newCase.AccountId = newAcc1.Id;
        newCase.Status = 'New';
        newCase.RecordTypeId = caseRecordType.get('INST').getRecordTypeId();
        newCase.Product__c = newProd1.Id; 
        if(isInsert)
            insert newCase;
        return newCase;
    }
    
    // Creating Case Line Data
    public static SVMXC__Case_Line__c CaseLine_Data(Boolean isInsert, Case newCase1, product2 newProd1) {
        SVMXC__Case_Line__c caseLine = new SVMXC__Case_Line__c();
        caseLine.SVMXC__Case__c = newCase1.id;
        caseLine.recordTypeId = caseLineRecordType.get('Case Line').getRecordTypeId();
        caseLine.SVMXC__Product__c = newProd1.Id;
        caseLine.Start_Date_time__c = system.today().addDays(-2);
        caseLine.End_date_time__c = system.today().addDays(+2);

        caseLine.SVMXC__Installed_Product__c = iProduct.Id;
        if(isInsert)
            insert caseLine;
        return caseLine;
    }
    static
    {
          //insert Country record
        CountryDateFormat__c dateFormat = new CountryDateFormat__c(Name='Default',Date_Format__c = 'dd-MM-YYYY kk:mm');
        insert dateFormat; 
		newAcc = AccountData(true);
        so = SO_Data(true, newAcc);
        soi = SOI_Data(true, so);
        prode = ERPPCODE_Data(true);
        newLoc = Location_Data(true, newAcc);
        servicegrp = new SVMXC__Service_Group__c();
        servicegrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        insert servicegrp;
        newProd = product_Data(true);

        newCase = Case_Data(true, newAcc, newProd);

        iProduct = InstalledProduct_Data(true, newProd, newAcc, newLoc, servicegrp, prode);
        newCaseLine = CaseLine_Data(true, newCase, newProd);

        erpproject = new ERP_Project__c(name = 'erpprojectstest');
        insert erpproject;

        erpwbs = new ERP_WBS__c(name = 'erpwbstest',ERP_Project__c = erpproject.id);
        insert erpwbs;



        timecardProfile = new Timecard_Profile__c();
        timecardProfile.Normal_Start_Time__c = system.now();
        timecardProfile.Normal_End_Time__c = System.now().addHours(3);
        timecardProfile.Lunch_Time_Start__c = system.now();
        insert timecardProfile;

        technician = new SVMXC__Service_Group_Members__c
        (
            name = 'testtechnician',
            User__c = Userinfo.getUserId(),
            SVMXC__Service_Group__c = servicegrp.id,
            SVMXC__Country__c = 'India',
            SVMXC__Street__c = 'abc',
            SVMXC__Zip__c = '54321',
            Timecard_Profile__c = timecardProfile.id
        );

        technician.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group_Members__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        technician.Timecard_Profile__c = timecardProfile.Id;
        insert technician;

        wo = new SVMXC__Service_Order__c
        (
            SVMXC__Group_Member__c = technician.id,
            SVMXC__Site__c = newLoc.id,
            SVMXC__Company__c = newAcc.id,
            SVMXC__Case__c = newCase.id,
            ERP_Service_Order__c = '12345678',
            ERP_WBS__c=erpwbs.id

        );
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Project Management').getRecordTypeId();
        wo.Event__c = true;
        insert wo;
        serviceUser = InsertUserData(true);
    }
    static testMethod void testUpdateFieldsOnSTBStatus()
    {
        STB_Master__c mster = new STB_Master__c();
        mster.name= 'Test 001';
        insert mster;
        
        SR_STB_Status stbStatus = new SR_STB_Status();
        STB_Status__c stb = new STB_Status__c();
        stb.Installed_Product__c = iProduct.Id;
        stb.Work_Order__c = wo.Id;
        stb.PCSN__c = '12345';
        stb.Status__c = 'C-Completed';
        stb.STB_Name__c = 'acme';
        stb.STB_Master__c = mster.id;
        insert stb;
        String str = '2/5/2014 : 12:00 AM';
        stbStatus.constructDateTimeGMT(str);
        stbStatus.convertMonthIntNumberToName(1);
        stbStatus.convertMonthIntNumberToName(2);
        stbStatus.convertMonthIntNumberToName(3);
        stbStatus.convertMonthIntNumberToName(4);
        stbStatus.convertMonthIntNumberToName(5);
        stbStatus.convertMonthIntNumberToName(6);
        stbStatus.convertMonthIntNumberToName(7);
        stbStatus.convertMonthIntNumberToName(8);
        stbStatus.convertMonthIntNumberToName(9);
        stbStatus.convertMonthIntNumberToName(10);
        stbStatus.convertMonthIntNumberToName(11);
        stbStatus.convertMonthIntNumberToName(12);
        stbStatus.convertMonthNameToNumber('Jan');
        stbStatus.convertMonthNameToNumber('Feb');
        stbStatus.convertMonthNameToNumber('Mar');
        stbStatus.convertMonthNameToNumber('Apr');
        stbStatus.convertMonthNameToNumber('May');
        stbStatus.convertMonthNameToNumber('Jun');
        stbStatus.convertMonthNameToNumber('Jul');
        stbStatus.convertMonthNameToNumber('Aug');
        stbStatus.convertMonthNameToNumber('Sep');
        stbStatus.convertMonthNameToNumber('Oct');
        stbStatus.convertMonthNameToNumber('Nov');
        stbStatus.convertMonthNameToNumber('Dec');
        
        
    }
}
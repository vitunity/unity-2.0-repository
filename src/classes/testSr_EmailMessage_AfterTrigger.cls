@isTest (seealldata = true)
Public class testSr_EmailMessage_AfterTrigger
{
    
    public static testmethod void testEmailMessage()   
    {
        Account objACC=new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',DBA__C='4665',BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert objACC;
        
        Account a;      
        Profile p = [select id from profile where name=: Label.VMS_Customer_Portal_User];
        a = new Account(name='test2',VMS_Factory_Tours_Attended__c=223.343,AKA__c='DFDF',Varian_Site__c=objAcc.id,Ship_To__c=objAcc.id,DBA__C='4665',Sold_to__c=objAcc.id,End_User__c=objAcc.id,Bill_To__c=objAcc.id,BillingPostalCode ='1223263',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert a;
        
        Contact con=new Contact(Lastname='test',FirstName='Singh',Email='kumar.amit667@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '124445');
        insert con;
        
        User u = new User(alias = 'standt', email='standardtestuse92@testorg.com', 
        emailencodingkey='UTF-8', lastname='Testing', 
        languagelocalekey='en_US', 
        localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', Contactid=con.id,
        username='standardtestuse92@testclass.com');
        //insert u;
        //system.runas(u){          
        Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        Case case1 = new Case(AccountId = a.Id, RecordTypeId = RecType, Priority = 'Medium',contactId = con.id);
        insert case1;
        case1.Status = 'Closed';
        update case1;
        EmailMessage t = new EmailMessage(subject = 'Test subject for task', TextBody = 'Test description for task', ParentID = case1.Id, Incoming=true);
        insert t;
    }
    
}
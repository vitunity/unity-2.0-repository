@isTest 
Public class testUpdateAssociatedAccount
{
    static testMethod void TestSiteDML() 
    {
        Country__c con = new Country__c(Name = 'United States');
        insert con;
    
        Account acc = new Account(Name = 'Test', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id, AccountNumber='5546'); 
        insert acc; 

        Account acc1 = new Account(Name = 'Test1', ERP_Timezone__c = 'AUSSA', country__c = 'United States', Country1__c = con.id, AccountNumber='5546'); 
        insert acc1; 

        Schema.DescribeSObjectResult AR = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> AccMapByName = AR.getRecordTypeInfosByName();
        Account accObj = new Account(Recordtypeid=AccMapByName.get('Site Partner').getRecordTypeId(), Name='Test23', AccountNumber='145678', CurrencyIsoCode='USD', Country__c = 'India',OMNI_Postal_Code__c ='21234', Prefered_Language__c ='English',Credit_Block__c =false);
        insert accObj;
        
        //insert record for ERP Partner//
        //record 1
        ERP_Partner__c ec = new ERP_Partner__c();
        ec.Name = 'Test';
        ec.Partner_Number__c = '145678';
        insert ec;
        
        //Record 2
        ERP_Partner__c ec1 = new ERP_Partner__c();
        ec1.Name = 'TestPartner';
        ec1.Partner_Number__c = '5546';
        insert ec1;
    
        //Insert Record ERP Partner Association //
        list<ERP_Partner_Association__c> listERP= new list<ERP_Partner_Association__c>();
        //Record 1
        ERP_Partner_Association__c epa = new ERP_Partner_Association__c();
        epa.Name = 'Partner';
        epa.ERP_Customer_Number__c = '10005328';
        //epa.ERP_Partner_Number__c = '10005328';
        epa.ERP_Partner__c = ec.id;
        epa.Partner_Function__c = 'Z1=Site Partner'; 
        epa.Sales_Org__c = '0600';
        epa.Customer_Account__c = accObj.id;
        listERP.add(epa);
        //insert epa;
        
       
        //Record 2
        ERP_Partner_Association__c epa1 = new ERP_Partner_Association__c();
        epa1.Name = 'Partner1';
        epa1.ERP_Customer_Number__c = '125687';
        //epa1.ERP_Partner_Number__c = '100053218';
        epa1.ERP_Partner__c = ec1.id;
        epa1.Partner_Function__c = 'EU=End User'; 
        epa1.Sales_Org__c = '0600';
        epa1.Customer_Account__c = accObj.id;
        epa1.Partner_Function__c = 'Z1=Site Partner'; 
        listERP.add(epa1);
        insert listERP;

        SVMXC__Site__c childSite = new SVMXC__Site__c(
            Sales_Org__c = '0600', 
            SVMXC__Service_Engineer__c = userInfo.getUserId(), 
            SVMXC__Location_Type__c = 'Field', 
            Plant__c = 'dfgh', 
            ERP_CSS_District__c = 'Paharganj',
            ERP_Site_Partner_Code__c = '145678',
            SVMXC__Account__c = accObj.id);
        insert childSite;

        SVMXC__Site__c location = new SVMXC__Site__c(
            Sales_Org__c = '0600', 
            SVMXC__Service_Engineer__c = userInfo.getUserId(), 
            SVMXC__Location_Type__c = 'Field', 
            Plant__c = 'dfgh', 
            ERP_CSS_District__c = 'Paharganj',
            ERP_Site_Partner_Code__c = '145678',
            SVMXC__Account__c = accObj.id);
        insert location;

        location.SVMXC__Account__c = acc1.id;
        location.Name = 'testLocation';
        location.SVMXC__Street__c = 'testStreet';
        location.SVMXC__Country__c = 'testCountry';
        location.SVMXC__Zip__c = '201301';
        location.ERP_CSS_District__c = 'Paharganj1';
        location.ERP_Site_Partner_Code__c = '5546';
        update location;

        delete location;
    }
}
@isTest
public class testUtils {
    public static final String USER_PROFILE_NAME = 'Standard User';
    public static final String SERVICE_USER_PROFILE_NAME = 'VMS Unity 1C - Service User';
    public static final String SERVICE_ADMIN_PROFILE_NAME = 'VMS Unity 1C - Service Admin';
    
   /* We do this darned near everywhere. */
    public static User createTestUserNotAcceptedTermsOfUse() {
        User usr = createUser();
        usr.Accepted_Terms_of_Use__c = false;
        update usr;
        return usr;
    }
    
        /* We do this darned near everywhere. */
    public static User createTestUserHasAcceptedTermsOfUse() {
        User usr = createUser();
        usr.Accepted_Terms_of_Use__c = true;
        update usr;
        return usr;
    }
    
    public static User createUser(){
      Profile profile = [Select Id From Profile Where Name=:USER_PROFILE_NAME];
      User user = new User(
        FirstName = 'Bruce',
        LastName = 'Wayne',
        Alias = 'Batman',
        email = 'bruce.wayne1c@zynga.com',
        languagelocalekey = 'en_US',
        localesidkey = 'en_US',
        emailencodingkey = 'UTF-8',
        timezonesidkey = 'America/Chicago',
        username = 'bruce.wayne1c@wayneenterprises.com',
        ProfileId = profile.Id,
        CurrentStatus = 'Testing Current Status',
        isActive = true
      );
      insert user;
      return user;  
    }
    
    public static User createServiceUser(String userName){
      Profile profile = [Select Id From Profile Where Name=:SERVICE_USER_PROFILE_NAME];
      User user = new User(
        FirstName = 'Bruce',
        LastName = 'Wayne',
        Alias = 'Batman',
        email = 'bruce.wayne@zynga.com',
        languagelocalekey = 'en_US',
        localesidkey = 'en_US',
        emailencodingkey = 'UTF-8',
        timezonesidkey = 'America/Chicago',
        username = userName,
        ProfileId = profile.Id,
        CurrentStatus = 'Testing Current Status',
        isActive = true
      );
      insert user;
      return user;  
    }
    
    /* 11-02-2011 JTL Returns a List of users, pass in the integer from your calling class
     *  ** How to Use, Users is defined as a list  **
     *  Users = testUtils.createListofUser(3);
     *      testuser = Users[0];
     *      testuser2 = Users[1];
     *      testuser3 = Users[2];   
     *********************************************************/
    public static List<User> createListofUser(Integer intgr){
        List<User> usr = new List<User>();
        for(Integer i = 0; i< intgr; i++){
            Profile profile = [Select Id From Profile Where Name=:USER_PROFILE_NAME];
            User user = new User(
            FirstName = 'Hunter'+ i,
            LastName = 'Thompson',
            Alias = 'HST',
            email = i+'HST@zynga.com',
            languagelocalekey = 'en_US',
            localesidkey = 'en_US',
            emailencodingkey = 'UTF-8',
            timezonesidkey = 'America/Chicago',
            username = 'HST@gonzo.com' + i,
            ProfileId = profile.Id,
            CurrentStatus = 'Testing Current Status',
            isActive = true
            );
          
          usr.add(user);            
        }
        insert usr;
        return usr;
    }
    
    public static Account getAccount(){
        Account acct = new Account();
        acct.Name = 'Test Account';
        acct.Country__c = 'USA';
        acct.OMNI_Postal_Code__c = '12121';
        acct.Account_Type__c = 'Customer';
        acct.BillingCity = 'San Jose';
        acct.BillingState = 'CA';
        acct.BillingCountry = 'USA';
        return acct;
    }
    
    public static Contact getContact(){
        Contact con = new Contact();
        Integer rando = Math.mod(Math.round(Math.random()*1000), Math.round(Math.random()*1000));
        con.FirstName = 'VARIAN TEST'+rando;
        con.LastName = rando +'UTILITY';
        con.MailingCountry = 'USA';
        con.MailingState = 'CA';
        con.MailingPostalCode = '12345'; 
        con.Email = rando +'Krishna.katve@testvarian.com';
        return con;
    }
    public static BigMachines__Quote__c getQuote(){
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.Name = 'Quote Test';
        quote.Price_Group__c = 'Z1';
        return quote;
    }
    
    public static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.Today();
        opp.StageName = 'Closed Won';
        opp.Probability = 100;
        opp.ForecastCategoryName = 'Best Case';
        opp.Deliver_to_Country__c = 'USA';
        return opp;
    }
    
    public static Email_Dlists__c getEmailDlist(){
        Email_Dlists__c emailDlist = new Email_Dlists__c();
        emailDlist.Name = 'TEST DLIST';
        emailDlist.Admin_Dlist__c = 'test@test.com';
        emailDlist.Brachy_Dlist__c = 'test@test.com';
        emailDlist.Business_Dlist__c = 'test@test.com';
        emailDlist.Secondary_Dlist__c = 'test@test.com';
        emailDlist.Support_Dlist__c = 'test@test.com';
        return emailDlist;
    }
    
    public static SVMXC_Timesheet__c createTimeSheet()
        {
            SVMXC_Timesheet__c testTS = new SVMXC_Timesheet__c();
            testTS.Start_Date__c = Date.Today().ToStartOfWeek();
            testTS.End_Date__c = Date.Today().ToStartOfWeek()+ 6;
      //     testTS.OwnerID = usr.Id;
      //      testTS.Technician__c = techn.Id;
            return testTS;
    }
        
    public static SVMXC_Time_Entry__c createTE(Id wdId, Id TSId)
        {
            Date myDate = Date.Today() + 2;
            SVMXC_Time_Entry__c testTE = new SVMXC_Time_Entry__c();
            testTE.Timesheet__c = TSId;
            testTE.Start_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(),2, 0, 0); 
            testTE.End_Date_Time__c = DateTime.newInstance(myDate.Year(), myDate.Month(), myDate.Day(), 6, 0, 0);
            testTE.RecordTypeId = Schema.SObjectType.SVMXC_Time_Entry__c.getRecordTypeInfosByName().get('Direct Hours').getRecordTypeId();
            testTE.Work_Details__c = wdId;
            return testTE;
        }
    
    public static SVMXC__Service_Order_Line__c createWD(Id woId){
        
        SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
        wd.Completed__c = true;
        wd.SVMXC__Service_Order__c = woId;
        return wd;
    }
    
    public static SVMXC__Service_Order__c createWO(){
        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Consulting').getRecordTypeId(); 
        wo.Acceptance_Date__c = Date.today();
        wo.SVMXC__Actual_Restoration__c = DateTime.now();
        wo.Machine_release_time__c = DateTime.now();
        wo.SVMXC__Preferred_Start_Time__c = DateTime.now();
        wo.SVMXC__Closed_On__c = DateTime.now();
        wo.SVMXC__Is_PM_Work_Order__c = true;
        wo.Event__c = true;
        return wo;
    }
    
    public static Country__c createCountry(String name) {
        return new Country__c(Name = name);
    }
    
    public static Account createAccount(String name, String timeZone, String countryName, Id countryId) {
        return new Account(Name = name, ERP_Timezone__c = timeZone, country__c = countryName, Country1__c = countryId); 
    }
    
    public static Contact createContact(Id accountId, String firstName, String lastName, String email) {
        return new Contact (AccountId = accountId, FirstName = firstName, LastName = lastName, Email=email);
    }
    
    public static Case createCase(String subject) {
        return new Case(Subject = subject);
    }
    
    public static SVMXC__Service_Group__c createServiceroup(Id userId) {
        SVMXC__Service_Group__c srvcGrp = new SVMXC__Service_Group__c();
        srvcGrp.RecordTypeId = Schema.SObjectType.SVMXC__Service_Group__c.getRecordTypeInfosByName().get('Technician').getRecordTypeId();
        srvcGrp.SC__c = userId;
        return srvcGrp;
    }
    public static Chow_Tool__c createChOwTool(string activeTeam,string activeAssignee,string assigneEmail){
        Chow_Tool__c chow = new Chow_Tool__c();
        chow.Active_Team__c=activeTeam;
        chow.Active_Task_Assignee__c=activeAssignee;
        chow.Assignee_Email__c=assigneEmail;
        chow.Assignment_Date__c=system.today();
        
        return chow;
        
    }
    public static Chow_line_item__c createChowLineItem(string itemName,integer quantity,string description){
        Chow_line_item__c chowLine = new Chow_line_item__c();
        chowLine.Quantity__c=quantity;
        chowLine.Comments__c=description;
        
        return chowLine;
    }
    public static Chow_Team_Member__c createChow_Team_Member(string activeTeam,id user,string geoRegion){
        Chow_Team_Member__c  team = new Chow_Team_Member__c();
        team.Active_Team__c =activeTeam;
        team.Team_Member_Name__c=user;
        team.Geo_Region__c=geoRegion;
        
        return team;
    }
}
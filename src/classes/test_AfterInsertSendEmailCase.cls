@isTest
Public class test_AfterInsertSendEmailCase
{
    
    public static testmethod void testEmailMessage()   
    {

        Account a = new Account(name='test2',BillingCity='California',BillingCountry='usa',BillingState='Washington',BillingStreet='xyx',Country__c='Test'  );
        insert a;
        
        Contact con=new Contact(Lastname='ltest',FirstName='ftest',Email='test32@gmail.com', AccountId=a.Id,MailingCountry ='Zimbabwe', MailingState='teststate', Phone = '124445');
        insert con;
       
        Id RecType = [SELECT Id, Name FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Helpdesk'].Id;
        Case c = new Case(AccountId = a.Id, RecordTypeId = RecType, Priority = 'Medium',contactId = con.id);
        insert c;
        
        test.startTest();
        
        EmailMessage t = new EmailMessage(subject = 'Test last Modification Date', TextBody = 'Test', ParentID = c.Id, Incoming=false);
        insert t;
        
        test.sTopTest();
    }
    
}
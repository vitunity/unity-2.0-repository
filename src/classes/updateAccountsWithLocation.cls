global class updateAccountsWithLocation implements Database.Batchable<sObject>
{
    String query;
    global updateAccountsWithLocation()
    {
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // This is the base query that dirves the chunking.
        // We are grabbing all the plans are currently active and
        // have a start or end date that is currently in need of calculation.

        query = 'select Id, Has_Location__c, (select Id from SVMXC__Sites__r) from Account where Has_Location__c = FALSE' ;  
        return Database.getQueryLocator(query);
    } 
    
    global void execute(Database.BatchableContext BC, List<sObject> batch) 
    {
        List<Account> accounts = new List <Account>();
        for(sObject obj : batch)
        {
            Account account = (Account)obj;
             if(account.SVMXC__Sites__r.size() > 0)
             {
                account.Has_Location__c = true;
             }
             else
             {
                account.Has_Location__c = false;            
             }   
            accounts.add(account);                      
        }
        update accounts;
    }
    global void finish(Database.BatchableContext BC) 
    {

    }
}
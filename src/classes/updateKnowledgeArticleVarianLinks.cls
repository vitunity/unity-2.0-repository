global class updateKnowledgeArticleVarianLinks implements Database.Batchable<sObject>, Database.Stateful {
    
    String query = 'select id, KnowledgeArticleId, title, Language, workaround__c, Answer__c, Properties__c,Internal_Information__c, Article_Image__c, Process__c, References__c, Solution__c, Status__c, Symptoms__c, PublishStatus from Service_Article__kav';
    global List<Id> list_Ids = new List<Id>();
    global List<String> failedToConvertOnlineToDraft = new List<String>();
    global List<String> successfullyPublishedArticles = new List<String>();
    global List<String> failedToPublishArticles = new List<String>();

    global updateKnowledgeArticleVarianLinks (List<Id> newlist_ids) {
        list_Ids = newlist_ids;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query + ' where ID IN :list_Ids');
    }

    global void execute(Database.BatchableContext BC, List<Service_Article__kav> scope) {
        List<Service_Article__kav> kav_listToEdit = new List<Service_Article__kav>();
        list<Service_Article__kav> kav_draft = new list<Service_Article__kav>();
        list<Service_Article__kav> kav_online = new list<Service_Article__kav>();
        List<Service_Article__kav> list_udpateAllDrafts = new List<Service_Article__kav>();
        List<Service_Article__kav> articlesConvertedToDraft = new List<Service_Article__kav>();     
        Map<Id, Id> map_onlineDraftIDs = new Map<Id, Id>(); 
        Map<Id, Service_Article__kav> draftOnlineArticles;          
        Id id_draft;
        for(Service_Article__kav s:scope) {
            if(s.workaround__c != null && s.workaround__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Answer__c != null && s.Answer__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Properties__c != null && s.Properties__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Internal_Information__c != null && s.Internal_Information__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Article_Image__c != null && s.Article_Image__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Process__c != null && s.Process__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.References__c != null && s.References__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Solution__c != null && s.Solution__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Status__c != null && s.Status__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            } else if(s.Symptoms__c != null && s.Symptoms__c.contains('varian.force.com')) {
                kav_listToEdit.add(s);
                if(s.PublishStatus == 'Draft') kav_draft.add(s);
                else if(s.PublishStatus == 'Online') kav_online.add(s);
            }    
        }
        system.debug('Number of articles to be edited: '+kav_listToEdit.size());
        system.debug('Number of draft articles: '+kav_draft.size());
        system.debug('Number of online articles: '+kav_online.size());
        for(Service_Article__kav s_online: kav_online) {
           id_draft = null;
            try{
                id_draft = KbManagement.PublishingService.editOnlineArticle (s_online.KnowledgeArticleId, false);
            }catch(exception e) {
                system.debug('the exception is: '+e);
                failedToConvertOnlineToDraft.add('version id: ' + s_online.id + 'KA id: ' + s_online.KnowledgeArticleId);
            }
            system.debug('draft Id is: '+id_draft+'; knowledgeArticleId is: '+s_online.KnowledgeArticleId);
            if(id_draft != null) {
                map_onlineDraftIDs.put(s_online.id,id_draft);
                articlesConvertedToDraft.add(s_online);
            }
        }
        system.debug('map of the online draft ID is: '+ map_onlineDraftIDs);
        draftOnlineArticles = new Map<Id, Service_Article__kav>([select id, workaround__c, Answer__c, Properties__c,Internal_Information__c, Article_Image__c, Process__c, References__c, Solution__c, Status__c, Symptoms__c from Service_Article__kav where id IN :map_onlineDraftIDs.Values()]);
        String t1='varian.force.com/sfc';
        String r1='www.myvarian.com/sfc';
        String t2='varian.force.com/apex/CpWebSummary?id=';
        String r2='www.myvarian.com/s/mvwebsummary?lang=en&Id=';
        String t3='varian.force.com/CpProductDocChdDtl?Id=';
        String r3='www.myvarian.com/s/productdocumentationdetail?lang=en&Id=';
        String t4='varian.force.com';
        String r4='www.myvarian.com';
        String a = 'www.myvarian.com/sfc/servlet.shepherd/version/download';
        String b = '?operationContext=CHATTER';
        
        for(Service_Article__kav s: kav_listToEdit){
            if(s.PublishStatus == 'Online') {
                system.debug('s id is: '+s.id);
                system.debug('map_onlineDraftIDs.get(s.id): '+map_onlineDraftIDs.get(s.id));
                s = draftOnlineArticles.get(map_onlineDraftIDs.get(s.id));                               
            }
            if(s!=null) { 
                if(s.workaround__c != null && s.workaround__c.contains('varian.force.com')) {
                    if(s.workaround__c.contains(t1)) {
                        s.workaround__c = s.workaround__c.replace(t1, r1);
                        if(s.workaround__c.contains(a)) {
                            String s1 = s.workaround__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.workaround__c = s1;
                        }
                    } 
                    if(s.workaround__c.contains(t2)) s.workaround__c = s.workaround__c.replace(t2, r2);
                    if(s.workaround__c.contains(t3)) s.workaround__c = s.workaround__c.replace(t3, r3);
                    if(s.workaround__c.contains(t4) && !s.workaround__c.contains('apex')) s.workaround__c = s.workaround__c.replace(t4, r4);
                    //if(s.workaround__c.contains(t5)) s.workaround__c = s.workaround__c.replace(t5, r5);
                }
                if(s.Answer__c != null && s.Answer__c.contains('varian.force.com')) {
                    if(s.Answer__c.contains(t1)) {
                        s.Answer__c = s.Answer__c.replace(t1, r1);
                        if(s.Answer__c.contains(a)) {
                            String s1 = s.Answer__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Answer__c = s1;
                        }                        
                    }
                    if(s.Answer__c.contains(t2)) s.Answer__c = s.Answer__c.replace(t2, r2);
                    if(s.Answer__c.contains(t3)) s.Answer__c = s.Answer__c.replace(t3, r3);
                    if(s.Answer__c.contains(t4) && !s.Answer__c.contains('apex')) s.Answer__c = s.Answer__c.replace(t4, r4);
                    //if(s.Answer__c.contains(t5)) s.Answer__c = s.Answer__c.replace(t5, r5);
                }
                if(s.Properties__c != null && s.Properties__c.contains('varian.force.com')) {
                    if(s.Properties__c.contains(t1)) {
                        s.Properties__c = s.Properties__c.replace(t1, r1);
                        if(s.Properties__c.contains(a)) {
                            String s1 = s.Properties__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Properties__c = s1;
                        }                        
                    }
                    if(s.Properties__c.contains(t2)) s.Properties__c = s.Properties__c.replace(t2, r2);
                    if(s.Properties__c.contains(t3)) s.Properties__c = s.Properties__c.replace(t3, r3);
                    if(s.Properties__c.contains(t4) && !s.Properties__c.contains('apex')) s.Properties__c = s.Properties__c.replace(t4, r4);
                    //if(s.Properties__c.contains(t5)) s.Properties__c = s.Properties__c.replace(t5, r5);
                }
                if(s.Internal_Information__c != null && s.Internal_Information__c.contains('varian.force.com')) {
                    if(s.Internal_Information__c.contains(t1)) {
                        s.Internal_Information__c = s.Internal_Information__c.replace(t1, r1);
                        if(s.Internal_Information__c.contains(a)) {
                            String s1 = s.Internal_Information__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Internal_Information__c = s1;
                        }                    
                    }
                    if(s.Internal_Information__c.contains(t2)) s.Internal_Information__c = s.Internal_Information__c.replace(t2, r2);
                    if(s.Internal_Information__c.contains(t3)) s.Internal_Information__c = s.Internal_Information__c.replace(t3, r3);
                    if(s.Internal_Information__c.contains(t4) && !s.Internal_Information__c.contains('apex')) s.Internal_Information__c = s.Internal_Information__c.replace(t4, r4);
                    //if(s.Internal_Information__c.contains(t5)) s.Internal_Information__c = s.Internal_Information__c.replace(t5, r5);
                }
                if(s.Article_Image__c != null && s.Article_Image__c.contains('varian.force.com')) {
                    if(s.Article_Image__c.contains(t1)) {
                        s.Article_Image__c = s.Article_Image__c.replace(t1, r1);
                        if(s.Article_Image__c.contains(a)) {
                            String s1 = s.Article_Image__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Article_Image__c = s1;
                        }                        
                    }
                    if(s.Article_Image__c.contains(t2)) s.Article_Image__c = s.Article_Image__c.replace(t2, r2);
                    if(s.Article_Image__c.contains(t3)) s.Article_Image__c = s.Article_Image__c.replace(t3, r3);
                    if(s.Article_Image__c.contains(t4) && !s.Article_Image__c.contains('apex')) s.Article_Image__c = s.Article_Image__c.replace(t4, r4);
                    //if(s.Article_Image__c.contains(t5)) s.Article_Image__c = s.Article_Image__c.replace(t5, r5);
                }
                if(s.Process__c != null && s.Process__c.contains('varian.force.com')) {
                    if(s.Process__c.contains(t1)) {
                        s.Process__c = s.Process__c.replace(t1, r1);
                        if(s.Process__c.contains(a)) {
                            String s1 = s.Process__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Process__c = s1;
                        }                        
                    }
                    if(s.Process__c.contains(t2)) s.Process__c = s.Process__c.replace(t2, r2);
                    if(s.Process__c.contains(t3)) s.Process__c = s.Process__c.replace(t3, r3);
                    if(s.Process__c.contains(t4) && !s.Process__c.contains('apex')) s.Process__c = s.Process__c.replace(t4, r4);
                    //if(s.Process__c.contains(t5)) s.Process__c = s.Process__c.replace(t5, r5);
                }
                if(s.References__c != null && s.References__c.contains('varian.force.com')) {
                    if(s.References__c.contains(t1)){
                        s.References__c = s.References__c.replace(t1, r1);
                        if(s.References__c.contains(a)) {
                            String s1 = s.References__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.References__c = s1;
                        }                        
                    }
                    if(s.References__c.contains(t2)) s.References__c = s.References__c.replace(t2, r2);
                    if(s.References__c.contains(t3)) s.References__c = s.References__c.replace(t3, r3);
                    if(s.References__c.contains(t4) && !s.References__c.contains('apex')) s.References__c = s.References__c.replace(t4, r4);
                    //if(s.References__c.contains(t5)) s.References__c = s.References__c.replace(t5, r5);
                }
                if(s.Solution__c != null && s.Solution__c.contains('varian.force.com')) {
                    if(s.Solution__c.contains(t1)) {
                        s.Solution__c = s.Solution__c.replace(t1, r1);
                        if(s.Solution__c.contains(a)) {
                            String s1 = s.Solution__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Solution__c = s1;
                        }                        
                    }
                    if(s.Solution__c.contains(t2)) s.Solution__c = s.Solution__c.replace(t2, r2);
                    if(s.Solution__c.contains(t3)) s.Solution__c = s.Solution__c.replace(t3, r3);
                    if(s.Solution__c.contains(t4) && !s.Solution__c.contains('apex')) s.Solution__c = s.Solution__c.replace(t4, r4);
                    //if(s.Solution__c.contains(t5)) s.Solution__c = s.Solution__c.replace(t5, r5);
                }
                if(s.Status__c != null && s.Status__c.contains('varian.force.com')) {
                    if(s.Status__c.contains(t1)) {
                        s.Status__c = s.Status__c.replace(t1, r1);
                        if(s.Status__c.contains(a)) {
                            String s1 = s.Status__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Status__c = s1;
                        }                        
                    }
                    if(s.Status__c.contains(t2)) s.Status__c = s.Status__c.replace(t2, r2);
                    if(s.Status__c.contains(t3)) s.Status__c = s.Status__c.replace(t3, r3);
                    if(s.Status__c.contains(t4) && !s.Status__c.contains('apex')) s.Status__c = s.Status__c.replace(t4, r4);
                    //if(s.Status__c.contains(t5)) s.Status__c = s.Status__c.replace(t5, r5);
                }
                if(s.Symptoms__c != null && s.Symptoms__c.contains('varian.force.com')) {
                    if(s.Symptoms__c.contains(t1)) {
                        s.Symptoms__c = s.Symptoms__c.replace(t1, r1);
                        if(s.Symptoms__c.contains(a)) {
                            String s1 = s.Symptoms__c;
                            String s2 = s1;
                            String s3 = null;
                            integer x = 0;                            
                            Integer i = 0;
                            do{
                                s2=s1.substring(x,s1.length());  
                                i = s2.indexOf(a);
                                if(i != -1) {
                                    i=i+x;
                                    if(s1.length() == i+73) {
                                        s1=s1+b;                                        
                                    } else if(s1.length() < i+98){
                                        s1=s1.substring(0,i+72) + b + s1.substring(i+73,s1.length());                                        
                                    }else if(s1.substring(i+73, i+98) != b){
                                        s3=s1.substring(i+73,s1.length());
                                        s1=s1.replace(s3,b+s3);
                                    }  
                                } 
                                if(i+73 <= s1.length()) x = i+98;
                                else i = -1;
                            } while(i != -1);
                            system.debug('final s1 is:'+s1);
                            s.Symptoms__c = s1;
                        }                        
                    }
                    if(s.Symptoms__c.contains(t2)) s.Symptoms__c = s.Symptoms__c.replace(t2, r2);
                    if(s.Symptoms__c.contains(t3)) s.Symptoms__c = s.Symptoms__c.replace(t3, r3);
                    if(s.Symptoms__c.contains(t4) && !s.Symptoms__c.contains('apex')) s.Symptoms__c = s.Symptoms__c.replace(t4, r4);
                    //if(s.Symptoms__c.contains(t5)) s.Symptoms__c = s.Symptoms__c.replace(t5, r5);
                }
                list_udpateAllDrafts.add(s);                    
            }           
        }
        system.debug('size of list to be udpated is: '+ list_udpateAllDrafts.size());
        if(list_udpateAllDrafts.size()>0) Database.update(list_udpateAllDrafts);
        for(Service_Article__kav s_publish: articlesConvertedToDraft) {
            try{
                KbManagement.PublishingService.publishArticle(s_publish.KnowledgeArticleId, true);
                successfullyPublishedArticles.add('KA id: ' + s_publish.KnowledgeArticleId);
            }
            catch(Exception e) {
                failedToPublishArticles.add('New draft ID: '+ s_publish.id + '; KA id: ' + s_publish.KnowledgeArticleId);
            }
            
        }
    }
    
    global void finish(Database.BatchableContext BC) {      
        System.debug('Final list of articles that failed to convert from online to draft:' + 'size:' + failedToConvertOnlineToDraft.size() + JSON.serializePretty(failedToConvertOnlineToDraft));
        System.debug('Final list of online articles that got published successfully:' + 'size:' + successfullyPublishedArticles.size() + JSON.serializePretty(successfullyPublishedArticles));
        System.debug('Final list of online articles that failed to publish and remain as a draft:' + 'size:' + failedToPublishArticles.size() + JSON.serializePretty(failedToPublishArticles));
    }
    
}
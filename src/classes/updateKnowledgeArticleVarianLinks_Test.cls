@isTest
private class updateKnowledgeArticleVarianLinks_Test
{
	static List<Id> listIds;

	@isTest
	static void testMethod_run()
	{
                insertData();
        	id batchjobid = Database.executeBatch(new updateKnowledgeArticleVarianLinks(listIds),40);
	}


	static void insertData()
	{		
        	List<Service_Article__kav> kvlist = new List<Service_Article__kav>(); 
        	listIds = new List<Id>();
        	List<Id> list_KA_Ids = new List<Id>();
                
                //Insert test data
                Service_Article__kav kav1=new Service_Article__kav();
                kav1.UrlName='testurl1';
                kav1.Title='Knowledge Article Test - 1';
                kav1.workaround__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';
                kav1.Answer__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav1.Properties__c = 'varian.force.com/CpProductDocChdDtl?Id=';                
                kvlist.add(kav1);

                Service_Article__kav kav2=new Service_Article__kav();
                kav2.UrlName='testurl2';
                kav2.Title='Knowledge Article Test - 2';
                kav2.Answer__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';
                kav1.Properties__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav1.Internal_Information__c = 'varian.force.com/CpProductDocChdDtl?Id=';               
                kvlist.add(kav2);

                Service_Article__kav kav3=new Service_Article__kav();
                kav3.UrlName='testurl3';
                kav3.Title='Knowledge Article Test - 3';
                kav3.Properties__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr'; 
                kav1.Internal_Information__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav1.Article_Image__c = 'varian.force.com/CpProductDocChdDtl?Id=';              
                kvlist.add(kav3);

                Service_Article__kav kav4=new Service_Article__kav();
                kav4.UrlName='testurl4';
                kav4.Title='Knowledge Article Test - 4';
                kav4.Internal_Information__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr'; 
                kav1.Answer__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav1.Properties__c = 'varian.force.com/CpProductDocChdDtl?Id=';              
                kvlist.add(kav4);

                Service_Article__kav kav5=new Service_Article__kav();
                kav5.UrlName='testurl5';
                kav5.Title='Knowledge Article Test - 5';
                kav5.Article_Image__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';  
                kav5.Process__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav5.References__c = 'varian.force.com/CpProductDocChdDtl?Id=';             
                kvlist.add(kav5);

                Service_Article__kav kav6=new Service_Article__kav();
                kav6.UrlName='testurl6';
                kav6.Title='Knowledge Article Test - 6';
                kav6.Process__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr'; 
                kav6.References__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav6.Solution__c = 'varian.force.com/CpProductDocChdDtl?Id=';              
                kvlist.add(kav6);

                Service_Article__kav kav7=new Service_Article__kav();
                kav7.UrlName='testurl7';
                kav7.Title='Knowledge Article Test - 7';
                kav7.References__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';  
                kav7.Solution__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav7.Status__c = 'varian.force.com/CpProductDocChdDtl?Id=';             
                kvlist.add(kav7);

                Service_Article__kav kav8=new Service_Article__kav();
                kav8.UrlName='testurl8';
                kav8.Title='Knowledge Article Test - 8';
                kav8.Solution__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';    
                kav8.Answer__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav8.Properties__c = 'varian.force.com/CpProductDocChdDtl?Id=';           
                kvlist.add(kav8);

                Service_Article__kav kav9=new Service_Article__kav();
                kav9.UrlName='testurl9';
                kav9.Title='Knowledge Article Test - 9';
                kav9.Status__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';  
                kav9.Symptoms__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav9.workaround__c = 'varian.force.com/CpProductDocChdDtl?Id=';             
                kvlist.add(kav9);

                Service_Article__kav kav10=new Service_Article__kav();
                kav10.UrlName='testurl10';
                kav10.Title='Knowledge Article Test - 10';
                kav10.Symptoms__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr';
                kav10.workaround__c = 'varian.force.com/apex/CpWebSummary?id='; 
                kav10.Answer__c = 'varian.force.com/CpProductDocChdDtl?Id=';               
                kvlist.add(kav10);

                Service_Article__kav kav11=new Service_Article__kav();
                kav11.UrlName='testurl11';
                kav11.Title='Knowledge Article Test - 11';
                kav11.Solution__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                   
                kvlist.add(kav11);

                Service_Article__kav kav12=new Service_Article__kav();
                kav12.UrlName='testurl12';
                kav12.Title='Knowledge Article Test - 12';
                kav12.Status__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                                     
                kvlist.add(kav12);

                Service_Article__kav kav13=new Service_Article__kav();
                kav13.UrlName='testurl13';
                kav13.Title='Knowledge Article Test - 13';
                kav13.Symptoms__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                             
                kvlist.add(kav13);

                Service_Article__kav kav14=new Service_Article__kav();
                kav14.UrlName='testurl14';
                kav14.Title='Knowledge Article Test - 14';
                kav14.workaround__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                   
                kvlist.add(kav14);

                Service_Article__kav kav15=new Service_Article__kav();
                kav15.UrlName='testurl15';
                kav15.Title='Knowledge Article Test - 15';
                kav15.Answer__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                                     
                kvlist.add(kav15);

                Service_Article__kav kav16=new Service_Article__kav();
                kav16.UrlName='testurl16';
                kav16.Title='Knowledge Article Test - 16';
                kav16.Properties__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                             
                kvlist.add(kav16);

                Service_Article__kav kav17=new Service_Article__kav();
                kav17.UrlName='testurl17';
                kav17.Title='Knowledge Article Test - 17';
                kav17.Internal_Information__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                   
                kvlist.add(kav17);

                Service_Article__kav kav18=new Service_Article__kav();
                kav18.UrlName='testurl18';
                kav18.Title='Knowledge Article Test - 18';
                kav18.Article_Image__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                                     
                kvlist.add(kav18);

                Service_Article__kav kav19=new Service_Article__kav();
                kav19.UrlName='testurl19';
                kav19.Title='Knowledge Article Test - 19';
                kav19.Process__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                             
                kvlist.add(kav19);

                Service_Article__kav kav20=new Service_Article__kav();
                kav20.UrlName='testurl20';
                kav20.Title='Knowledge Article Test - 20';
                kav20.References__c = 'varian.force.com/sfc/servlet.shepherd/version/download/abcdefghijklmnopqr. This is a sample test data. This is a sample test data. This is a sample test data. This is a sample test data.';                             
                kvlist.add(kav20);

                insert kvlist;
                for(Service_Article__kav k: kvlist) {
                	listIds.add(k.id);        	
                }
                for(Service_Article__kav kav: [select KnowledgeArticleId from Service_Article__kav where id IN :listIds] ) {
                	KbManagement.PublishingService.publishArticle(kav.KnowledgeArticleId, true);
                }
                system.debug('in test method: '+listIds);		
	}
}
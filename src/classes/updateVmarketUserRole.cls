/**
 *    @author        :    Puneet Mishra
 *    @description   :    Batch will change the vMarket User Role to customer related to a country mentioned in custom Label/Setting
 */
global class updateVmarketUserRole implements Database.Batchable<sobject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return null;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    }
    
    global void finish(Database.BatchableContext BC) {
    }
    
}
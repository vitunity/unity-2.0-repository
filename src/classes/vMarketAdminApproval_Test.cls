/**
*  @author    :    Puneet Mishra
*  @description:    test Class for vMarketAdminApproval
*/
@isTest
public with sharing class vMarketAdminApproval_Test {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static User adminUsr1,adminUsr2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    
    static Attachment attach_Logo, attach_AppGuide;
    static List<Attachment> attachList;
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        //lstUserInsert = new List<User>();
        //lstUserInsert.add( adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1'));
        //lstUserInsert.add( adminUsr2 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin2'));
        //insert lstUserInsert; 
        
        //System.runAs(adminUsr1) {
        account = vMarketDataUtility_Test.createAccount('test account', true);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        insert lstContactInsert;
        //}
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        system.debug(' === lstUserInsert === ' + lstUserInsert);
        //System.runAs(adminUsr1){ 
        insert lstUserInsert;
        // }
        //system.assert(false);
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        listing = vMarketDataUtility_Test.createListingData(app, true);
        comment = vMarketDataUtility_Test.createCommentTest(app, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app.Id, true);
        
        VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK');
        insert uk;
        
        vMarketConfiguration config = new vMarketConfiguration ();
        
        List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
        System.debug('{{}}'+config.SUBMITTED_STATUS);
      /*   countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
       */ countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));
         countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.SUBMITTED_STATUS));
countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.APPROVED_STATUS));
countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.REJECTED_STATUS));
countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
countyappsList.add(new VMarket_Country_App__c(VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));

        insert countyappsList;
    }
    
    public static testmethod void vMarketAdminApprovalTest() {
        Test.StartTest();
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        adminApproval.getFilterCategories();
        adminApproval.getAllApps();       
        Test.StopTest();
    }
    
    
    public static testMethod void getSubmittedApps_Test() {
        Test.StartTest();
        app = [select Id, Name, ApprovalStatus__c, Owner.Name, CreatedDate From vMarket_App__c where Id =: app.Id];
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        //List<vMarket_App__c> submittedApps = adminApproval.getSubmittedApps();
        adminApproval.getSubmittedApps();
        //system.assertEquals(submittedApps[0].id, app.id);
        
        Test.StopTest();
    }
    
    public static testMethod void getUnderReviewApps_Test() {
        Test.StartTest();
        
        app.ApprovalStatus__c = 'Under Review';
        update app;
        
        app = [select Id, Name, ApprovalStatus__c, CreatedDate, Owner.Name From vMarket_App__c where Id =: app.Id];
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        //List<vMarket_App__c> underReviewApps = adminApproval.getUnderReviewApps();
        System.debug(adminApproval.UnderReviewApps);// getUnderReviewApps();
        //system.assertEquals(underReviewApps[0].id,app.id);
        
        Test.StopTest();
    }
    
    public static testMethod void getApprovedApps_Test() {
        Test.StartTest();
        
        vMarket_App__c devApp = new vMarket_App__c();
        devApp = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        devApp.ApprovalStatus__c = 'Approved';
        update devApp;
        vMarketAdminApproval admin = new vMarketAdminApproval();
        admin.getApprovedApps();
        
        Test.StopTest();
    }
    
    public static testMethod void getRejectedApps_Test() {
        set<Id> appsId = new set<Id>();
        list<Id> appIdList = new list<Id>();
        Test.StartTest();
        vMarket_App__c devApp = new vMarket_App__c();
        devApp = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        devApp.ApprovalStatus__c = 'Rejected';
        devApp.Rejected_Comment__c = 'Comment for Rejection';
        update devApp;
        appsId.add(devApp.Id);
        appIdList.add(devApp.Id);
        vMarketAdminApproval admin = new vMarketAdminApproval();
        admin.getRejectedApps();
        Test.StopTest();
    }
    
    public static testMethod void getReviewerList_Test() {
        Test.StartTest();
        adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1');
        adminUsr1.vMarket_User_Role__c = 'Reviewer';
        insert adminUsr1;
        
        List<User> adminL = [select Id, FirstName, LastName, Username, Email, ContactId  From User WHERE Id =: adminUsr1.Id];
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        List<User> expectedRes = adminApproval.getReviewerList();
        
        
        
        Test.StopTest();
    }
    
    public static testMethod void resetApprovalStatusFlag_Test() {
        Test.StartTest();
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        adminApproval.resetApprovalStatusFlag(app.Id);
        
        system.assertEquals(app.isApprovedOrRejected__c , false);
        
        Test.StopTest();
    }
    
    public static testMethod void publishedAppProcess_Test() {
        Test.StartTest();
        Test.setCurrentPageReference(new PageReference('Page.vMarket_AdminApproval')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        
        //added for subscription
        app.Subscription_Price__c='0;0;0;0';
        update app;
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        adminApproval.publishedAppProcess();
        
        app = [SELECT Id, Name, ApprovalStatus__c FROM vMarket_App__c WHERE Id =: app.Id];
        
        system.assertEquals(app.ApprovalStatus__c, 'Published');
        
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('App \'' + app.Name + '\' has been published.')) 
                b = true;
        }
        system.assert(b);
        
        
        Test.StopTest();
    }
    
    
    public static testMethod void getIsAdmin_Test() {
        Test.StartTest();
        adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1');
        insert adminUsr1;
        Test.StopTest();
    }
    
    //
    public static testMethod void startAppoval_Test() {
        
        Test.StartTest();
        try{
            adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', false, admin.Id, null, 'Admin1');
            adminUsr1.vMarket_User_Role__c = 'Reviewer';
            insert adminUsr1;
            string userList =  adminUsr1.Id;
            update app;
            string appId = app.Id;
            //List<Id> reviewerIds = userList.split(',');
            
            //Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            //req1.setComments('Submitting request for approval.');
            //req1.setObjectId(appId);
            //req1.setNextApproverIds(new List<Id>{reviewerIds[0]});
            //req1.setProcessDefinitionNameOrId('vMarketTestApproval');
            //req1.setSkipEntryCriteria(true);
            //Approval.ProcessResult result = Approval.process(req1);
            
            vMarketAdminApproval adminApproval = new vMarketAdminApproval();
            
            adminApproval.startAppoval();
        }catch(exception e){
        }
        Test.StopTest();
    }
    
    
    
    public static testMethod void getDoesErrorExist_Test() {
        Test.StartTest();
        // vMarketAdminApproval.dummyMethod();
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        adminApproval.getDoesErrorExist();
        Test.StopTest();
    }
    
    public static testMethod void generateUID_Test() {
        Test.StartTest();
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();
        adminApproval.generateUID();
        Test.StopTest();
    }
    
    public static testMethod void methodstest() {
        Test.StartTest();
        
        Test.setCurrentPageReference(new PageReference('Page.vMarket_AdminApproval')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('finalstatus', 'rejected');
        System.currentPageReference().getParameters().put('finalcomment', 'Comment');
        System.currentPageReference().getParameters().put('appidstart', app.Id);
        System.currentPageReference().getParameters().put('revtype', 'technical');
        System.currentPageReference().getParameters().put('revcountries', 'US;UK');
        System.currentPageReference().getParameters().put('con', 'US');
        System.currentPageReference().getParameters().put('appcons', 'US;UK');
        System.currentPageReference().getParameters().put('selcountry', 'US');
        
        
        
        VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
        VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
        VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
        VMarket_Reviewers__c r4 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Privacy reviewer', user__C=UserInfo.getUserId());
        List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
        reviewerslist.add(r1);
        reviewerslist.add(r2);
        reviewerslist.add(r3);
        reviewerslist.add(r4);
        insert reviewerslist;
        
                vMarketConfiguration config = new vMarketConfiguration ();
        
        List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
        System.debug('{{}}'+config.SUBMITTED_STATUS);
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
        
        
        
        vMarketAdminApproval adminApproval = new vMarketAdminApproval(); 
        adminApproval.selectedcountry = 'US';
        adminApproval.setcountry();
        
        adminApproval.getIsAdmin(); 
        adminApproval.getIsApprovalAdmin();
        System.debug(adminApproval.isAdminAccess());
        System.debug(adminApproval.getnxt()); 
        System.debug(adminApproval.getCurPage());
        System.debug(adminApproval.getprev());
        adminApproval.end();
        adminApproval.next();
        adminApproval.previous(); 
        adminApproval.beginning();
        adminApproval.getPublishedApps();
        adminApproval.getSubmittedApps();    
        System.debug(adminApproval.getInfoRequestedApps());
        adminApproval.addCountriesToList();
        adminApproval.item();
        adminApproval.getItems();
        adminApproval.getListOfCountries();
        adminApproval.getReviewCountryOptions();
        //vMarketAdminApproval.appCountryDetailWrapper appcdclass = new vMarketAdminApproval.appCountryDetailWrapper();
        
        
        PageReference p = adminApproval.startApproval();
        adminApproval.FinishApproval();       
        Test.StopTest(); 
        
    }
    
    public static testMethod void methodstest_regulatory() {
        Test.StartTest();
        
        Test.setCurrentPageReference(new PageReference('Page.vMarket_AdminApproval')); 
        System.currentPageReference().getParameters().put('appId', app.Id);
        System.currentPageReference().getParameters().put('finalstatus', 'rejected');
        System.currentPageReference().getParameters().put('finalcomment', 'Comment');
        System.currentPageReference().getParameters().put('appidstart', app.Id);
        System.currentPageReference().getParameters().put('revtype', 'regulatory');
        System.currentPageReference().getParameters().put('revcountries', 'US;UK');
        
        VMarket_Reviewers__c r1 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Admin', user__C=UserInfo.getUserId());
        VMarket_Reviewers__c r2 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Technical reviewer', user__C=UserInfo.getUserId());
        VMarket_Reviewers__c r3 = new VMarket_Reviewers__c(Countries__c ='US;UK',active__c=true,Default__c=true, type__C='Regulatory reviewer', user__C=UserInfo.getUserId());
        List<VMarket_Reviewers__c> reviewerslist = new List<VMarket_Reviewers__c>();
        reviewerslist.add(r1);
        reviewerslist.add(r2);
        reviewerslist.add(r3);
        insert reviewerslist;
        vMarketAdminApproval adminApproval = new vMarketAdminApproval();     
        PageReference p = adminApproval.startApproval();
        
        Test.StopTest(); 
        
    }
    
    
    
}
// Test Class for vMarketAppDetailsController
@isTest
public with sharing class vMarketAppDetailsControllerTest {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        User u = vMarketDataUtility_Test.getSystemAdmin();
        System.runAs(u)
        {
        account = vMarketDataUtility_Test.createAccount('test account', true);
        }
        
        System.debug('&&&&&'+portal.UserType);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        
        User u1 = vMarketDataUtility_Test.getSystemAdmin();
        //System.runAs(u1)
        //{
            
            insert lstContactInsert;
            
        //}
        
          
        
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        //User u1 = vMarketDataUtility_Test.getSystemAdmin();
        System.runAs(u1)
        {
        insert lstUserInsert;
        }
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        listing = vMarketDataUtility_Test.createListingData(app, true);
        comment = vMarketDataUtility_Test.createCommentTest(app, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        //insert orderItemList;
        
        orderItemLineList = new List<vMarketOrderItemLine__c>();
        system.runAs(u1) {
            orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem1, true, true);
        }
        
        system.RunAs(u1) {
            orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app, orderItem2, false, true);
        }
        
        orderItemLineList.add(orderItemLine1);
        orderItemLineList.add(orderItemLine2);
        //insert orderItemLineList;
    }
    
    private static testmethod void getAllStarsTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getAllStars();
            detail.incrementDownloadCount();
            detail.getAppDevDetail();
            detail.incrementDownloadCount();
            detail.traildownloaded();
            detail.getAppDevDetail();
            detail.previous();
            detail.next();
            detail.end();
            detail.getprev();
            detail.getstartOffset();
            detail.getendOffset();
            detail.getCurPage();
            detail.getnxt();
            detail.beginning();            
            detail.createNewOrderItem();
            vMarketOrderItem__c order = [Select id from vMarketOrderItem__c limit 1][0];
            detail.retrieveTax(order,false);
            detail.updateOrderTax(order);
            detail.updateOrderAddress(order);
            
            //System.debug(detail.getendOffset());

        test.StopTest();
    }
    
    // When appId is null
    private static testMethod void getAppIdTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            try {
                String expected = detail.getAppId();
            } catch(Exception e) {
                
            }
        test.StopTest();
    }
    
    private static testMethod void getAppId_notNull_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getAllStars();
        test.StopTest();
    }
    
    // when user not commented
    private static testmethod void getIsUserCommented_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            Boolean expected = detail.getIsUserCommented();
            //system.assertEquals(expected, false);
        test.Stoptest();
    }
    
    private static testMethod void getIsViewableForDevTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            Boolean expected = detail.getIsViewableForDev();
            system.assertEquals(expected, false);
            
            system.runas(developer1) {
                Boolean expected2 = detail.getIsViewableForDev();
                system.assertEquals(expected2, true);
            }
            
        test.StopTest();
    }
    
    private static testMethod void getMonthListTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.Chart_Monthly_Frequency = new Map<String, String>();
            List<SelectOption> expectedOpt = detail.getMonthList();
            system.assertEquals(new List<SelectOption>(), expectedOpt);
            
            detail.Chart_Monthly_Frequency = new Map<String, String>{'3' => '3 Month', '6' => '6 Month', '12' => '12 Month'};
            List<SelectOption> expectedOpt2 = detail.getMonthList();
            system.assertEquals(3, expectedOpt2.size());
        test.StopTest();
    }
    
    private static testMethod void testMethods() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
        
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.beginning();
            String expectedId = detail.getOrderId();
            system.assertEquals(expectedId, null);
            
            PageReference resPg = detail.filterRating();
            system.assertEquals(resPg, null);
            
            detail.selected_frequency = 3;
            detail.changeMonthlyFreq();
            
            detail.getDoesInfoExist();
        test.StopTest();
    }
    
    private static testMethod void editAppDetailsTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
        
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            pageReference pg = detail.editAppDetails();
            
            system.assertNotEquals(new PageReference('/vMarketDevAppEdit'), pg);
            
        test.Stoptest();
    }
    
    private static testMethod void getAppPublishedTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            System.currentPageReference().getParameters().put('status', 'Published');
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            Boolean res = detail.getAppPublished();
            system.assertEquals(res, true);
        test.StopTest();
    }
    
    private static testMethod void getAppPublished_ExceptionTesting() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            try {
                Boolean res = detail.getAppPublished();
            } catch(Exception e) {}
        test.StopTest();
    }
    
    private static testMethod void initializeMonthMapTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.initializeMonthMap();
            system.assertEquals(detail.month_abbr.get(1), 'Jan');
            system.assertEquals(detail.month_abbr.get(6), 'Jun');
            system.assertEquals(detail.month_abbr.get(12), 'Dec');
            system.assertNotEquals(detail.month_abbr.containsKey(13), true);
        test.StopTest();
    }
    
    private static testMethod void addOrderItemLineTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            //system.runas(Customer1) {
                detail.addOrderItemLine(app.Id, orderItem1.Id, 500);
            //}
        test.StopTest();
    }
    
    private static testMethod void getRoleTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getRole();
            
            detail.getIsAccessible();
            
            detail.getAppVersionList();
            
            system.runas(customer1) {
                detail.getIsVerifiedUser();
            }
        test.Stoptest();
    }
    
    private static testmethod void deleteOrderItemLineByAppIdTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            //detail.orderItem = orderItem1;
            detail.deleteOrderItemLineByAppId(app.Id);
        test.StopTest();
    }
    
    private static testmethod void deleteOrderItemLinesByOrderIdTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.deleteOrderItemLinesByOrderId(orderItem1);
        test.StopTest();
    }
    
    private static testmethod void deleteOrderItemLinesByOrderId_ExceptionTest() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            try {
                vMarketAppDetailsController detail = new vMarketAppDetailsController();
                detail.deleteOrderItemLinesByOrderId(orderItem1);
            } catch(Exception e) {
                
            }
        test.StopTest();
    }
    
    private static testMethod void getMonthlyEarning_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getMonthlyEarning();
        test.StopTest();
    }
    
    private static testMethod void getAppRanking_test() {
        test.Starttest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getAppRanking();
        test.Stoptest();
    }
    
    private static testMethod void incrementReadOnlyCount_test() {
        vMarket_App__c app2 = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        test.StartTest();
            app2.ApprovalStatus__c = 'Approved';
            update app2 ;
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app2.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.incrementReadOnlyCount();
        test.Stoptest();
    }
    
    private static testMethod void insertReview_test() {
        test.Starttest();
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            System.currentPageReference().getParameters().put('reviewTitle', 'Test_Title');
            System.currentPageReference().getParameters().put('rating', '5');
            System.currentPageReference().getParameters().put('yourReview', 'PERFECT');
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.listingId = listing.Id;
            detail.insertReview();
            
            vMarketComment__c comment  = new vMarketComment__c();
            comment = [SELECT Id, Name__c, Description__c, Rating__c, vMarket_Listing__c FROM vMarketComment__c WHERE vMarket_Listing__c =: listing.Id AND Name__c =: 'Test_Title'];
            system.assertEquals(comment.Name__c, 'Test_Title');
            system.assertEquals(comment.Description__c, 'PERFECT');
            system.assertEquals(comment.Rating__c, '5');
            
        test.Stoptest();
    }
    
    private static testMethod void setFullAddressFromContact_Test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            system.runas(customer1) {
                detail.setFullAddressFromContact();
            }
        test.Stoptest();
    }
    
    private static testMethod void createOrdertest() {
        vMarket_App__c app2 = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        test.Starttest();
            app2.ApprovalStatus__c = 'Published';//'Approved';
            update app2 ;
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app2.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            PageReference resRef = detail.createOrder();
        test.Stoptest();
    }
    private static testMethod void createOrdertest1() {
        vMarket_App__c app2 = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer1.Id);
        
        test.Starttest();
            app2.ApprovalStatus__c = 'Approved';
            update app2 ;
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app2.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            PageReference resRef = detail.createOrder();
        test.Stoptest();
    }
    
    
    private static testMethod void getTotalEarning() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            Decimal res = detail.getTotalEarning();
        test.StopTest();
    }
    
    private static testMethod void getMonthlyDownload_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            Integer res = detail.getMonthlyDownload();
        test.Stoptest();
    }
    
    private static testmethod void getFilterEarningChartData_Test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getFilterEarningChartData();
        test.Stoptest();
    }
    
    private static testMethod void getFilterInstallChartData_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketAppDetails')); 
            System.currentPageReference().getParameters().put('appId', app.Id);
            
            vMarketAppDetailsController detail = new vMarketAppDetailsController();
            detail.getFilterInstallChartData();
        test.Stoptest();
    }
    
    
  /*  public static testMethod void Dummy() {
        Test.StartTest();
            vMarketAppDetailsController.dummy();
        Test.StopTest();
    }*/
}
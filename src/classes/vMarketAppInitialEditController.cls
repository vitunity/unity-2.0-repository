public with sharing class vMarketAppInitialEditController extends vMarketBaseController {
    
    public transient  String appId {get;set;}
    public transient  String description {get;set;}
    public transient  String appSourceId {get;set;}
    public transient  Integer FileCount {get; set;}
    public vMarket_App__c new_app_obj {get;set;}
    public transient  boolean isFree {get;set;}
    public List<vMarketStripeDetail__c> userStripeDetail {get;set;}
    public transient  Attachment attached_logo {get;set;}
    public transient  String selectedOpt{get;set;}
    public Attachment existing_logo {get;set;}
    public transient  boolean selected {get;set;}
    public transient  boolean logoExists {get;set;}
    public transient  boolean kNumberExists {get;set;}
    public transient  boolean opinionExists {get;set;}
    public transient  boolean ccatsExist {get;set;}
    public transient  boolean techSpecExist {get;set;}
    public transient  boolean medicalDevice {get;set;}
    public transient  String medical_device {get;set;}
    public transient  String k_number {get;set;}
    public transient  String opinion {get;set;}
    public Attachment attached_ExpertOpioion{get;set;}
    public Attachment attached_510k{get;set;}
    public Attachment existing_ExpertOpioion{get;set;}
    public Attachment existing_510k{get;set;}
    public List<legalinfo> legalinfos{get; set;}
    public transient boolean eurocountries{get; set;}
    public List<String> selectedCountriesCodes{get; set;}
    public Map<String,String> countriesMap{get; set;}
    public List<String> docslist{get; set;}
    public List<String> legalNameList{get; set;}
    public List<String> legalAddressList{get; set;}
    public List<String> selectedCountriesList{get; set;}
    public Map<String,String> docsmap{get; set;}
    public List<String> countylist{get;set;}
    public List<Attachment> countryDocsList {get; set;}
    public List<Attachment> countryDeclarationList {get; set;}
    public List<Attachment> countryEnglishList {get; set;}
    public Map<String,VMarket_Country_App__c> legalinfosmap{get; set;}
    public Map<Id,String> legalinfoIdsmap{get; set;}
    public transient  Map<String,List<Attachment>> legalinfodocsmap{get; set;}
    public String monthlyprice {get;set;}
    public transient  String quarterlyprice {get;set;}
    public transient String halfyearlyprice {get;set;}
    public transient String annualprice {get;set;}
    
    public transient string Is_Designed_For_Medical{get;set;}
    public transient string encryptionType{get;set;}
    
    public Attachment attached_CCATS{get;set;}
    public Attachment existing_CCATS{get;set;}
    public Attachment attached_TechnicalSpecification {get;set;}
    public Attachment existing_TechnicalSpecification {get;set;}
    
    public transient string encryptionTypeOther{get;set;}
    public transient boolean encryptionType_Standard {get;set;}
    public transient boolean encryptionType_IOS {get;set;}
    public transient boolean encryptionType_Proprietary {get;set;}
    public transient boolean encryptionType_Other {get;set;}
    //add default country
   // public string defaultCountry;

    
    public vMarketAppInitialEditController () {
        attached_ExpertOpioion = new Attachment();
        attached_510k = new Attachment();
        appId = ApexPages.currentPage().getParameters().get('appId');
        new_app_obj = new vMarket_App__c();
        attached_logo = new Attachment();
        countryDocsList = new List<Attachment>();
    countryDeclarationList = new List<Attachment>();
    countryEnglishList = new List<Attachment>();
    legalNameList = new List<String>();
    legalAddressList = new List<String>();
    selectedCountriesList = new List<String>();
    selectedCountriesCodes = new List<String>();
    countriesMap = (new vMarket_Approval_Util()).countriesMap();    
    legalinfos = new List<legalinfo>();
    legalinfosmap = new Map<String,VMarket_Country_App__c>();
    legalinfoIdsmap = new Map<Id,String>();
    legalinfodocsmap = new Map<String,List<Attachment>>();
    //defaultCountry='';
       
    Map<String,VMarketCountry__c> conMap = new Map<String,VMarketCountry__c>();
    for(VMarketCountry__c con : [select name, Address_Required__c ,Code__c ,Legal_Number_Required__c, Documents__c  from VMarketCountry__c])
    {
    conMap.put(con.code__c,con);
    }   
       
    List<VMarket_Country_App__c> existingCountryApps = [select Country__c,VMarket_Legal_Address__c , VMarket_Legal_Number__c ,VMarket_App__c, VMarket_Legal_Name__c from VMarket_Country_App__c where VMarket_App__c=:appId];
    System.debug('**existing'+existingCountryApps);
    for(VMarket_Country_App__c capp : existingCountryApps)
    {
    legalinfosmap.put(capp.country__C,capp);
    legalinfoIdsmap.put(capp.id,capp.country__C);
    System.debug('**mappy'+conMap.get(capp.country__c)+conMap+capp.country__c);
    selectedCountriesList.add(capp.country__c);
    legalinfos.add(new legalinfo(capp.Country__c,capp.VMarket_Legal_Name__c,capp.VMarket_Legal_Address__c,legalinfodocsmap.get(capp.Country__c),conMap.get(capp.country__c).Documents__c,capp.VMarket_Legal_Number__c,conMap.get(capp.country__c).Legal_Number_Required__c,conMap.get(capp.country__c).Address_Required__c));
    }
    
    for(Attachment acct : [Select id,name,parentid,Body from Attachment where parentId IN :existingCountryApps])
    {
    
    System.debug('**Checking'+legalinfoIdsmap.get(acct.ParentId)+acct.ParentId);
    if(legalinfodocsmap.containskey(legalinfoIdsmap.get(acct.ParentId))) 
    {
    legalinfodocsmap.get(legalinfoIdsmap.get(acct.ParentId)).add(acct);
    }
    else
    {
    List<Attachment> attach = new List<Attachment>();
    attach.add(acct);
    legalinfodocsmap.put(legalinfoIdsmap.get(acct.ParentId),attach);
    }
    
    }
    
        new_app_obj = [Select Name, K_Number__c ,App_Category__c, Id, ApprovalStatus__c,  App_Category__r.Name,
                        AppStatus__c, IsActive__c, Is_Trail_Available__c, VMarket_Trail_Period_Days__c, VMarket_Protected_Health_Info__c, VMarket_Personal_Info__c, VMarket_Comply_Privacy_Laws__c, VMarket_Security_Laws__c, VMarket_Sensitive_Information__c,
                        Long_Description__c, Is_Medical_Device__c, Price__c,Key_features__c, Short_Description__c, subscription__c, IsFree__c, PublisherName__c, PublisherEmail__c, PublisherWebsite__c, PublisherPhone__c, Publisher_Developer_Support__c ,Publisher_Address__c, Countries__c,Subscription_Price__c,VMarket_Type_of_Encr__c,VMarket_Is_Designed_For_Medical__c ,VMarket_Primary_Purpose_Encr_Details__c,Encryption_Level_Technical_Details__c
                        from vMarket_App__c 
                        where Id=:appId  limit 1]; 
                        
        selected = false;
        //defaultCountry=;
       
        if(new_app_obj .App_Category__r.Name == 'Eclipse'){
            selected = false;
        }else{
            selected = true;
        } 
       // attached_logo = [select Id, Name, Body, ContentType  from Attachment where parentId =:appId and ContentType != 'application/pdf' limit 1];
        description = new_app_obj.short_description__c;
        
        if(new_app_obj.IsFree__c){
            selectedOpt = 'Free';
        }
        else if(new_app_obj.subscription__c){
            selectedOpt = 'Subscription';
            //added for subscription plan by Harshal
            List<String> lstSubPlan = new_app_obj.Subscription_Price__c.split(';');
            monthlyprice=lstSubPlan[0];
            quarterlyprice=lstSubPlan[1];
            halfyearlyprice=lstSubPlan[2];
            annualprice=lstSubPlan[3];
        }
        else if(!new_app_obj.IsFree__c && !new_app_obj.subscription__c){
            selectedOpt = 'One Time';
         }   
        medicalDevice= false;    
        if(new_app_obj.Is_Medical_Device__c) medicalDevice=true;
        if(new_app_obj.VMarket_Is_Designed_For_Medical__c){
            Is_Designed_For_Medical='Yes';
        }else{
            Is_Designed_For_Medical='No';
        } 
        
        if(string.isNotBlank(new_app_obj.VMarket_Type_of_Encr__c)){
            if(new_app_obj.VMarket_Type_of_Encr__c=='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)'){
                encryptionType_Standard=true;
            }
            else if(new_app_obj.VMarket_Type_of_Encr__c=='iOS or macOS'){
                encryptionType_IOS=true;
            }
            else if(new_app_obj.VMarket_Type_of_Encr__c=='Proprietary or non-standard'){
                encryptionType_Proprietary=true;
            }
            else{
                encryptionType_Other=true;
                encryptionTypeOther=new_app_obj.VMarket_Type_of_Encr__c;
            }
        }
          
        logoExists = false;
        kNumberExists = false;
        opinionExists = false;
        ccatsExist=false;
        techSpecExist=false;
        
         for(Attachment a :[select Name, parentid, ContentType from Attachment where parentid=:appId] )
        {
            System.debug('===='+a.ContentType);
            if(a.Name.startsWith('Logo_')){
                existing_logo = a;
                logoExists = true;
            }
            if(a.Name.startsWith('Opinion_')){
                existing_ExpertOpioion = a;
                opinionExists = true;
            }
            if(a.Name.startsWith('kNumber_')){
                existing_510k = a;
                kNumberExists = true;
            }
             if(a.Name.startsWith('CCATS_')){
                existing_CCATS = a;
                ccatsExist = true;
            }
            if(a.Name.startsWith('TechnicalSpec_')){
                existing_TechnicalSpecification= a;
                techSpecExist = true;
            }
            
        } 
        
    }

    public List<SelectOption> getAppType() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('Free', 'Free'));
        option.add(new SelectOption('Subscription', 'Subscription'));
        option.add(new SelectOption('One Time', 'One Time'));
        return option;
    }

    public Boolean getIsViewableForDev() {
        List<vMarket_App__c> apps = [select Id From vMarket_App__c Where Id=:appId And OwnerId=:UserInfo.getUserId()];
        if(!apps.isEmpty())
            return true;
        else
            return false;
    }
    
    public override Boolean getAuthenticated() {
        return super.getAuthenticated();
    }

    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer' && getIsViewableForDev())
            return true;
        else
            return false;
    }
    
     public List<SelectOption> getListOfCountries() 
    {
    vMarket_Approval_Util util = new vMarket_Approval_Util();
    return util.ListOfCountriesall();
    }
    
    Public string[] getselValue()
    {
     list<string> strVal=new List<String>();
     strVal.addAll(new_app_obj.Countries__c.split(';'));    
     return strVal;
    }

    public void setselValue(string[] vals)
    {
    
       new_app_obj.countries__c=string.join(vals,';');
    }
    
    public void clearcountry()
    {
    if(legalinfos!=null && legalinfos.size()>0) legalinfos.clear();
    if(selectedcountriescodes!=null && selectedcountriescodes.size()>0) selectedcountriescodes.clear();
    if(selectedCountriesList!=null && selectedCountriesList.size()>0) selectedCountriesList.clear();
    }
    
    public void changecountry()
    {
    eurocountries =false;
    Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
    String selectedcountries = ApexPages.currentPage().getParameters().get('selectedcountries');
    String europe = '';
        for(String s :selectedcountries.split(';',-1))
        {
            
            if(europeanCountries.contains(s))
            {
                europe += s+';';
            }
        }
        
    if(europe.length()>0)
    {
    eurocountries = true;
    }
    System.debug('^^^EURO'+eurocountries);
    }
    
    public void populatecountry()
    {
    clearcountry();
    legalinfos = new List<legalinfo>();
    if(legalinfodocsmap==null) legalinfodocsmap = new Map<String,List<Attachment>>();
    selectedCountriesList.clear();// = new List<String>();
    String selectedcountries = ApexPages.currentPage().getParameters().get('selectedcountries');
    docsmap = new Map<String,String>();
    docslist = new List<String>();
    countryDocsList = new List<Attachment>();
    Set<String> selectedCountriesSet = new Set<String>();
    Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
    String europe = '';
    for(VMarketCountry__c con : [select name, Address_Required__c ,Code__c ,Legal_Number_Required__c, Documents__c  from VMarketCountry__c])
    {
    //Set<String> countries = new Set<String>(con.Code__c.split(';',-1));
    System.debug('&&&&countries'+selectedcountries);
    for(String s :selectedcountries.split(';',-1))
    {
    
    //System.debug('&&&&country'+s);
    if(con.Code__c.equals(s))
    {
    
    if(europeanCountries.contains(s))
    {
    europe += s+';';
    }
    else{
    selectedcountriescodes.add(s);
    selectedCountriesSet.add(countriesMap.get(s));
    System.debug('((((('+con.Documents__c);
    
        docsmap.put(con.Documents__c+'('+s+')',s+'_'+con.Documents__c);
        docslist.add(con.Documents__c+'('+s+')');
        countryDocsList.add(new Attachment());
        countryDeclarationList.add(new Attachment());
        countryEnglishList.add(new Attachment());
        //legalNameList.add('');
        //legalAddressList.add('');
        
        List<Attachment> docsattach = new List<Attachment>();
        for(String st :con.Documents__c.split(';',-1))
        {
        docsattach.add(new Attachment(name=st));
        }
        System.debug('^^^^^'+con.Legal_Number_Required__c);
        System.debug('***inlegal'+con.Code__c+legalinfosmap.containskey(con.Code__c));
        if(legalinfosmap!=null && !legalinfosmap.containskey(con.Code__c)) legalinfos.add(new legalinfo(con.Code__c,'','',docsattach,con.Documents__c,'',con.Legal_Number_Required__c,con.Address_Required__c));
        else 
        {
        VMarket_Country_App__c existingcapp = legalinfosmap.get(con.Code__c);
        System.debug('***inside'+legalinfos+legalinfodocsmap);
        legalinfos.add(new legalinfo(existingcapp.Country__c,existingcapp.VMarket_Legal_Name__c,existingcapp.VMarket_Legal_Address__c,legalinfodocsmap.get(existingcapp.Country__c),con.Documents__c,existingcapp.VMarket_Legal_Number__c,con.Legal_Number_Required__c,con.Address_Required__c));
        }
        //if(con.Legal_Number_Required__c) legalinfos.add(new legalinfo(con.Code__c,'','',docsattach,con.Documents__c,'',true));
        //else legalinfos.add(new legalinfo(con.Code__c,'','',docsattach,con.Documents__c,'',false));
        
       } 
    }
    
    
    
    }
    }
    SYstem.debug('#####'+europe);
    if(europe.length()>0)
    {
    eurocountries = true;
    selectedcountriescodes.add(europe);
    SYstem.debug('$$$$'+selectedcountriescodes);
    String europecountries = '';
    boolean containsEuro = false;
    String existEuroCountry = '';
    for(String g : europe.split(';',-1))
    {
    if(countriesMap.containsKey(g)) europecountries += countriesMap.get(g)+',';
    if(legalinfosmap!=null && legalinfosmap.containskey(g)) 
    {
    containsEuro = true;
    existEuroCountry = g;
    }
    }
    europecountries = europecountries.substring(0,europecountries.length()-1);
    selectedCountriesSet.add(europecountries);
        //docsmap.put(con.Documents__c+'('+s+')',s+'_'+con.Documents__c);
        //docslist.add(con.Documents__c+'('+s+')');
        //countryDocsList.add(new Attachment());
        //countryDeclarationList.add(new Attachment());
        //countryEnglishList.add(new Attachment());
        //legalNameList.add('');
        //legalAddressList.add('');
        
        List<Attachment> docsattach = new List<Attachment>();
        //for(String st :con.Documents__c.split(';',-1))
        //{
        docsattach.add(new Attachment(name='CE Form'));
        docsattach.add(new Attachment(name='Declaration of Conformity'));
        //docsattach.add(new Attachment(name='English Translation'));
        //}
        //System.debug('^^^^^'+con.Legal_Number_Required__c);
        boolean addrreq = true;
        if(europeanCountries.contains(new vMarket_StripeAPIUtil().getUserCountry())) addrreq = false;
        
        
        
        if(!containsEuro) legalinfos.add(new legalinfo(europe,'','',docsattach,'CE Form;Declaration of Conformity','',false,addrreq));
        else
        {
        VMarket_Country_App__c existingcapp = legalinfosmap.get(existEuroCountry);
        legalinfos.add(new legalinfo(europe,existingcapp.VMarket_Legal_Name__c,existingcapp.VMarket_Legal_Address__c,legalinfodocsmap.get(existEuroCountry),'CE Form;Declaration of Conformity','',false,addrreq));
        }
        
        
    }
    SYstem.debug('#####legal'+selectedCountriesList+selectedCountriesSet+legalinfos);
    
    legalNameList.add('');
    //legalinfos.add(new legalinfo('',''));
    legalAddressList.add('');
    selectedCountriesList.addAll(selectedCountriesSet);
    }
    
    public List<SelectOption> getAppStatus() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult aStatus = vMarket_App__c.AppStatus__c.getDescribe();
        List<Schema.PicklistEntry> appStatusValues = aStatus.getPicklistValues();

        for (Schema.PicklistEntry status : appStatusValues)
        { 
            options.add(new SelectOption(status.getLabel(), status.getValue())); 
        }
        return options;
    }
    
    public List<SelectOption> getDevAppCategories() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category__c> app_categories = [select Name from vMarket_App_Category__c];
        
        options.add(new SelectOption('','Select Category'));
        for(vMarket_App_Category__c cat:app_categories) {
            options.add(new SelectOption(cat.Id,cat.Name));
        }
        return options;
    }
    
    public void saveIsFree(){
        System.debug('==isFree=='+isFree);
    }

    public void checkSelectedValue(){        
        system.debug('Selected value is: ' + medical_device);        
    }

    public pageReference save() {
        system.debug('*****'+new_app_obj.name);
        system.debug('*****'+new_app_obj.price__C);
        system.debug('*****'+new_app_obj.Long_Description__c);
        system.debug('*****'+description);
            system.debug('==selectedOpt =='+selectedOpt);
            if(selectedOpt.equals('Free')){
                new_app_obj.Price__c = 0.0;
                new_app_obj.IsFree__c = true;
                new_app_obj.subscription__c = false;
            } else if(selectedOpt.equals('Subscription')){
                new_app_obj.subscription__c = true;
                new_app_obj.IsFree__c = false;
            }
            else{
            new_app_obj.subscription__c = false;
                new_app_obj.IsFree__c = false;
            }
            
               if(String.isBlank(monthlyprice)) monthlyprice = '0';

                if(String.isBlank(quarterlyprice)) quarterlyprice = '0';
                
                if(String.isBlank(halfyearlyprice)) halfyearlyprice = '0';
                
                if(String.isBlank(annualprice)) annualprice = '0';
                
                new_app_obj.Subscription_Price__c = monthlyprice+';'+quarterlyprice+';'+halfyearlyprice+';'+annualprice;                
            
           System.debug('++++++++++desc'+new_app_obj.Long_Description__c);
            System.debug('++++++++++desc'+new_app_obj.Publisher_Developer_Support__c);
            new_app_obj.ApprovalStatus__c = 'Draft';
            new_app_obj.IsActive__c = true;
            update new_app_obj;
            
            
            
            
            if(attached_logo!=null && String.isNotBlank(attached_logo.name) && logoExists) delete existing_logo;
            
            if(String.isNotBlank(attached_logo.name)) {
                attached_logo.parentid= AppId;
                attached_logo.Name = 'Logo_'+attached_logo.Name;
                attached_logo.ContentType = 'application/png';
                insert attached_logo;
            }
            
          /*  if(attached_ExpertOpioion!=null && String.isNotBlank(attached_ExpertOpioion.name) && existing_ExpertOpioion!=null) delete existing_ExpertOpioion;
              if(attached_ExpertOpioion !=null && String.isNotBlank(attached_ExpertOpioion.name))
            {
                attached_ExpertOpioion.parentid= AppId;
                attached_ExpertOpioion.Name = 'Opinion_'+attached_ExpertOpioion.Name;
                attached_ExpertOpioion.ContentType = 'application/pdf';
                insert attached_ExpertOpioion;
            }
            
            if(attached_510k!=null && String.isNotBlank(attached_510k.name) && existing_510k!=null) delete existing_510k;
            // Insert 510k doc/pdf
            if(attached_510k!=null && String.isNotBlank(attached_510k.name))
            {
                attached_510k.parentid= AppId;
                attached_510k.Name = 'kNumber_'+attached_510k.Name;
                attached_510k.ContentType = 'application/pdf';
                insert attached_510k;
            }*/
           // attached_logo.ContentType = 'application/jpg';
           // update attached_logo;
           
           List<VMarket_Country_App__c> countryApps = new List<VMarket_Country_App__c>();
            
            if(selectedcountriescodes.size()==0)
            {
                for(String s :new_app_obj.countries__c.split(';',-1))
                {
                    selectedcountriescodes.add(s);
                }
            }
            
            String europe = '';
            Integer europeCounter = 0;
            Integer nameCounter = 0;
            System.debug('-----'+selectedcountriescodes+legalinfos);
            for(String country : selectedcountriescodes)
            {
            if(!country.contains(';') && String.isNotBlank(country))
            {
            VMarket_Country_App__c capp =new VMarket_Country_App__c(VMarket_App__c=AppId,Status__c='Active',Country__c=country, VMarket_Approval_Status__c ='Submitted');
            //capp.VMarket_Legal_Name__c = legalinfos.get(nameCounter).legalname;
            //capp.VMarket_Legal_Address__c = legalinfos.get(nameCounter).legaladdress;
            
            if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legalname)) capp.VMarket_Legal_Name__c = legalinfos.get(nameCounter).legalname;
            else capp.VMarket_Legal_Name__c = new_app_obj.PublisherName__c;
            
            if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legaladdress)) capp.VMarket_Legal_Address__c = legalinfos.get(nameCounter).legaladdress;
            else capp.VMarket_Legal_Address__c = new_app_obj.Publisher_Address__c;

            if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legalnumber)) capp.VMarket_Legal_Number__c = legalinfos.get(nameCounter).legalnumber;            
            
            countryApps.add(capp);
            nameCounter++;
            }
            else 
            {
            europe = country;
            europeCounter =nameCounter;
            nameCounter++;
            }
            }
            System.debug('europecons'+europe+europeCounter);
            
            if(String.isNOtBlank(europe))
            {
            for(String c : europe.split(';',-1))
            {
            VMarket_Country_App__c capp =new VMarket_Country_App__c(VMarket_App__c=AppId,Status__c='Active',Country__c=c, VMarket_Approval_Status__c ='Draft');
            System.debug('--'+c);
            if(String.isNOTBLANK(legalinfos.get(europeCounter).legalname)) capp.VMarket_Legal_Name__c = legalinfos.get(europeCounter).legalname;
            else capp.VMarket_Legal_Name__c = new_app_obj.PublisherName__c;
            
            if(String.isNOTBLANK(legalinfos.get(europeCounter).legaladdress)) capp.VMarket_Legal_Address__c = legalinfos.get(europeCounter).legaladdress;
            else capp.VMarket_Legal_Address__c = new_app_obj.Publisher_Address__c;
            
            countryApps.add(capp);
            
            }
            }
            List<Database.SaveResult> srList = Database.insert(countryApps,false);
        
            Map<String,Id> countryAppMap = new Map<String,Id>();
            Integer resCount = 0;
            for(Database.SaveResult countryApp : srList)
            {
            System.debug('====='+countryApp);
            System.debug('====='+countryApps);
            if(countryApp.isSuccess())
            {countryAppMap.put(countryApps[resCount].Country__c,countryApp.getId());}
            resCount++;
            }
            
            
            List<Attachment> docstoInsert = new List<Attachment>();
            
            System.debug('@@@@@@'+countryAppMap);
            
            
            if(legalinfos!=null && legalinfos.size()>0)
            {
            for(legalinfo l :legalinfos)
            {
            Integer attachmentCounter = 0;
            System.debug('%%%%%'+l.country);
            System.debug('%%%%%'+l.docslist);
            System.debug('%%%%%'+l.documentsToBeAttached);
            
            if(countryAppMap.containsKey(l.country))
            {
            for(Attachment doc : l.documentsToBeAttached)
            {
            if(doc!=null && String.isNotBlank(doc.name))
                        {                        
                            doc.parentid= countryAppMap.get(l.country);
                            doc.Name = l.docslist.split(';',-1).get(attachmentCounter)+'_'+doc.Name;
                            doc.ContentType = 'application/pdf';
                            System.debug('+++'+doc);                        
                            docstoInsert.add(doc);                         
                        }
            attachmentCounter++;
            }
            }
            else if(l.country.equals(europe))
            {
           System.debug('europedocs'+europe.length());
            
            for(String con : l.country.split(';',-1))
            {System.debug('____'+con);   
            if(con.length()>0) {                    
            Integer europeattachmentCounter = 0;
            for(Attachment doc : l.documentsToBeAttached)
            {
            if(doc!=null && String.isNotBlank(doc.name))
                        {       Attachment newdoc = new Attachment();                 
                            newdoc.parentid= countryAppMap.get(con);
                            newdoc.Name = con+'_'+l.docslist.split(';',-1).get(europeattachmentCounter)+'_'+doc.Name;
                            newdoc.body = doc.body;
                            newdoc.ContentType = 'application/pdf';
                            System.debug('===='+newdoc);                        
                            docstoInsert.add(newdoc);                         
                        }
                        europeattachmentCounter++;
                        }
                        
                        }
                        }
            
            
            }
            
            }
            }
            if(europe.length()>0)
            {
            
            
            }
            
            
            if(docstoInsert.size()>0) insert docstoInsert;
           
            PageReference pf = new PageReference('/vMarketDevAssetsUpload?apId='+new_app_obj.Id);
            return pf;
    }
    
    public pageReference appdetails() {
    //System.assertEquals(1,0);
    System.debug('*********');
    
    PageReference pf = new PageReference('/vMarketDevAssetsUpload?apId='+new_app_obj.Id);
            return pf;
    
    }
    
     public class legalinfo
    {
    public String legalname{get;set;}
    public String country{get;set;}
    public String docslist{get;set;}
    public String legaladdress{get;set;}
    public String legalnumber{get;set;}
    public boolean legalnumberRequired{get;set;}
    public boolean addressrequired{get;set;}
    public List<Attachment> documentsToBeAttached{get;set;}
    
    public legalinfo(String c,String n,String a,List<Attachment> docs,string d)
    {
    this.country = c;
    this.docslist = d;
    this.legalname = n;
    this.legaladdress = a;
    this.documentsToBeAttached= new List<Attachment>(docs);
    }
    
    public legalinfo(String c,String n,String a,List<Attachment> docs,string d,String num, boolean numreq,boolean addreq)
    {
    this.country = c;
    this.docslist = d;
    this.legalname = n;
    this.legaladdress = a;
    this.legalnumberrequired = numreq;
    this.addressrequired = addreq;
    if(numreq) this.legalnumber = num;
    this.documentsToBeAttached= new List<Attachment>(docs);
    }
    
    
    
    }
    
}
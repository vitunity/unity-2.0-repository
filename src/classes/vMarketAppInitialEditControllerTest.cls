@isTest
public with sharing class vMarketAppInitialEditControllerTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      
      PageReference myVfPage = Page.vMarketAppInitialEdit;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('appId',app.Id);
      
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/jpg', 'Logo_logo', true);
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'kNumber_knumber', true);
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'Opinion_opinion', true);
      
      
      vMarketAppInitialEditController dc = new vMarketAppInitialEditController();  
      System.debug(dc.getAppType());   
      System.debug(dc.getIsViewableForDev());   
      System.debug(dc.getAuthenticated());   
      System.debug(dc.getRole()); 
      System.debug(dc.getIsAccessible());  
      System.debug(dc.getAppStatus());
      System.debug(dc.getDevAppCategories());
      dc.checkSelectedValue();
      dc.saveIsFree();
      
      
      dc.attached_logo = vMarketDataUtility_Test.createAttachmentTest('','content/jpg', 'logo', false);
      dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest('','content/pdf', '510k', false);
      dc.attached_510k = vMarketDataUtility_Test.createAttachmentTest('','content/pdf', 'opinion', false);
      
      dc.save();
      dc.appdetails();
      test.Stoptest();
    }
}
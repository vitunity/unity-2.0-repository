public with sharing class vMarketAppUploadController {
    public vMarket_App__c app{get;set;}
    @testVisible private static String appId;
    public List<vMarketAppSource__c> appSource{get;set;}
    public List<Attachment> chatterFiles;
    public List<Attachment> newChatterFiles{get;set;}
    //number of upload files
    public static final Integer NUM_OF_UPLOADS=1;
    
    public List<MultipleUploads> multiApp{get;set;}
    
    public vMarketAppUploadController() {
        
        app = new vMarket_App__c();
        if(ApexPages.currentPage().getParameters().get('id') != null) {
            appId = ApexPages.currentPage().getParameters().get('Id');
            chatterFiles = new List<Attachment>();
            newChatterFiles = new List<Attachment>{new Attachment()};
            multiApp = new List<MultipleUploads>();
        }
    }
    
    //
    public pageReference cancelAppUpload() {
        system.debug(' == Inside Cancel == ');
        return new PageReference('/vMarketUploadForm');
    }
    
    // Save the App for future upload
    public pageReference saveAppAsDraft() {
        system.debug(' === Saving App as Draft === ' + app);
        app.AppStatus__c = 'Draft';
        app.Stripe_Account_Id__c = 'acc_123456789';
        insert app;
        
        return new PageReference('/vMarketUploadForm');
    }
    
    public pageReference nextStep() {
        app.Stripe_Account_Id__c = Id.valueOf('a8C4C0000008Ot1');
        insert app;
        
        return new PageReference('/uploadAppAssests?id='+app.Id);
    }   
    
    public List<Attachment> getUploads() {
        return null;
    }
    
    // logic for more Uploads
    public void addMoreUploads() {
        for(integer i = 0; i < NUM_OF_UPLOADS; i++){
            newChatterFiles.add(new Attachment());
            multiApp.add(new MultipleUploads(appId, new Attachment(), ''));
        }
        system.debug(' ====multiApp====== ' + multiApp);
    }
    
    public pageReference submit() {
        /*Map<vMarketAppSource__c, Attachment> appSource = new Map<vMarketAppSource__c, Attachment>();
        List<vMarketAppSource__c> appSrcList = new List<vMarketAppSource__c>(); 
        List<FeedItem> fItems = new List<FeedItem>();
        FeedItem post;
        for(MultipleUploads m : multiApp) {
            appSource.put(m.appVersion, m.msiFile);
            appSrcList.add(m.appVersion);
        }
        
        insert appSrcList;
        Map<Id, vMarketAppSource__c> appSrcMap = new Map<Id, vMarketAppSource__c>(appSrcList);
        
        for(vMarketAppSource__c appsrc : appSrcMap ){
            post = new FeedItem();
            post.ParentId = appSource.Id;
            post.ContentData = appSource.get(appsrc).body;
            post.ContentFileName = appSource.get(appsrc).Name;
            fItems.add(post);
        }
        insert fItems;
        system.debug(' ====multiApp====== ' + multiApp);
        system.debug(' ====multiApp====== ' + multiApp);
        */
        
        vMarketAppSource__c vers;// = new vMarketAppSource__c();
        //vers.vMarket_App__c = ApexPages.currentPage().getParameters().get('id');
        //vers.IsActive__c = true;
        //insert vers;
        
        /*vMarketAppSource__c verss = new vMarketAppSource__c();
        verss.vMarket_App__c = ApexPages.currentPage().getParameters().get('id');
        verss.IsActive__c = true;
        insert verss;
        
        List<vMarketAppSource__c> versList = new List<vMarketAppSource__c>();
        versList.add(vers);
        versList.add(verss);*/
        
        Map<String, vMarketAppSource__c> verList = new Map<String, vMarketAppSource__c>();
        
        for(MultipleUploads m : multiApp) {
            vers = new vMarketAppSource__c();
            vers.vMarket_App__c = ApexPages.currentPage().getParameters().get('id');
            vers.IsActive__c = true;
            verList.put(m.msiFile.Name, vers);
        }
        
        insert verList.values();
        system.debug(' ====multiApp====== ' + multiApp);
        
        List<FeedItem> postList = new List<FeedItem>();
        FeedItem posts;
        for(MultipleUploads m : multiApp) {
            system.debug(' ====m.msiFile.Body====== ' + m.msiFile.Body);
            posts = new FeedItem();
            posts.ParentId = verList.get(m.msiFile.Name).Id;//vers.Id;
            posts.ContentData = m.msiFile.Body;
            posts.ContentFileName = m.msiFile.Name;
            posts.Body = 'Attachment added';
            postList.add(posts);
        }
        insert postList;
        multiApp = new List<MultipleUploads>();
        return null;
    }
    
    public class MultipleUploads {
        public vMarketAppSource__c appVersion{get;set;}
        public Attachment msiFile{get;set;}
        public String fileName{get;set;}
        
        public MultipleUploads(String app_Id, Attachment attach, String fileName) {
            appVersion = new vMarketAppSource__c();
            appVersion.vMarket_App__c = app_Id;
            msiFile = attach;
            msiFile.Name = fileName;
            msiFile.Name = attach.Name;
        }
        
    }
    
    
}
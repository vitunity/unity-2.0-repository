@isTest
public class vMarketAppWorkFlow_test {

    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app;
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    
    // static Profile admin,portal;   
      //static User customer1, customer2, developer1, ADMIN1;
      //static Account account;
      static Contact contact;
      
      static {
       /*   admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        account = vMarketDataUtility_Test.createAccount('test account', true);
        contact = vMarketDataUtility_Test.contact_data(account, true);
        contact.oktaid__c ='abhishekkolipe67936';
        update contact;
        customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact.Id, 'Customer1');
      */}
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        account = vMarketDataUtility_Test.createAccount('test account', true);
        contact = vMarketDataUtility_Test.contact_data(account, true);
        contact.oktaid__c ='abhishekkolipe67936';
        update contact;
        customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact.Id, 'Customer1');
        User u = vMarketDataUtility_Test.getSystemAdmin();
        
        /*System.runAs(u)
        {
          account acc = vMarketDataUtility_Test.createAccount('test accounts01', true);
          
        }*/
        
        System.debug('&&&&&'+portal.UserType);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        
        User u1 = vMarketDataUtility_Test.getSystemAdmin();
       // System.runAs(u1)
      // {
            
           // insert lstContactInsert;
            
      //  }
        
          
        
        
        lstUserInsert = new List<User>();
        /*lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        //User u1 = vMarketDataUtility_Test.getSystemAdmin();
       // System.runAs(u1)
       // {
        insert lstUserInsert;
       // }
        */
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, customer1.Id);
        
    }
    
    public static testMethod void testMe() {
    Test.StartTest() ;
    
    
     Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Approve.');
            req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            req.setObjectId(app.Id);

            //Submit the approval request
            Approval.ProcessResult result = Approval.process(req);
    
        vMarketApprovalWorkflow__c appA = new vMarketApprovalWorkflow__c();
        appA.vMarket_App__c = app.Id;
        appA.Status__c = 'Pending';
        insert appA;
    Test.StopTest();
    }
}
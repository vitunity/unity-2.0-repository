/**************************************************************************\
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for Basic Utils which can be inherited everywhere
****************************************************************************/
public virtual class vMarketBaseController {
    public String appId{get;set;}
    public Integer cartItemCount{get;set;}
    private List<String> appIdList = new List<String> {};
    public vMarketCartItem cartItemObj;
    public vMarketCatalogueController vMarketCatalogueObj;
    public Map<Integer, VMarket_Footer_Components__c> parentMap;
    public Map<String, VMarket_Footer_Components__c> mapOriginalFooterComponent;
    public Map<Integer, String> childMarkupMap;
    public Integer maxFooterItems{get;set;}
    public String footerMarkup{get;set;}
    
    virtual public Boolean getInfoExist() {
        return ApexPages.hasMessages(ApexPages.Severity.INFO);
    }
    
      // Added for Dev without stripe Id
    public Boolean isFreeDev{
        get {
            Boolean isfreeAppDev=false;
            List<vMarket_Developer_Stripe_Info__c> userStripeInfoLst = [SELECT Name,Stripe_Id__c,Stripe_User__r.vMarket_User_Role__c FROM vMarket_Developer_Stripe_Info__c WHERE Stripe_User__c =:UserInfo.getUserId() And Developer_Request_Status__c='Approved'];
            if(!userStripeInfoLst.isEmpty() && string.isBlank(userStripeInfoLst[0].Stripe_Id__c) && userStripeInfoLst[0].Stripe_User__r.vMarket_User_Role__c=='Developer'){
                isfreeAppDev=true;
            }
            return isfreeAppDev;
        }
    set;} 
    
    public vMarketBaseController() {
    maxFooterItems = 0;
        cartItemObj = new vMarketCartItem();
        vMarketCatalogueObj = new vMarketCatalogueController();
        footerMarkup = '';
      //  prepareMarkupMap();
      //  prepareMarkup();
    }
/*
   public void prepareMarkupMap()
   {
   parentMap = new Map<Integer, VMarket_Footer_Components__c>();
   childMarkupMap = new Map<Integer, String>();
   mapOriginalFooterComponent = VMarket_Footer_Components__c.getAll() ;
    for(String b :mapOriginalFooterComponent.keyset()) 
    {
    VMarket_Footer_Components__c c = mapOriginalFooterComponent.get(b);
    
    if( Integer.valueOf(c.parent_id__c) == -1)  
    {if( Integer.valueOf(c.order_id__c) > maxFooterItems) maxFooterItems = Integer.valueOf(c.order_id__c);
    parentMap.put(Integer.valueOf(c.order_id__c),c);
    }
    
    if(!childMarkupMap.containsKey(Integer.valueOf(c.parent_id__C))) 
    {
    if(String.isNotBlank(c.logo__C)) childMarkupMap.put(Integer.valueOf(c.parent_id__C),'&nbsp;<img src=\"'+c.logo__c+'\" width=\"25px\" height=\"25px\"><a href=\"'+c.link__C+'\"></a></img>');
    else childMarkupMap.put(Integer.valueOf(c.parent_id__C),'<li><a href=\"'+c.link__C+'\">'+c.name+'</a></li>');
    
    }
    else 
    {
    if(String.isNotBlank(c.logo__C)) childMarkupMap.put(Integer.valueOf(c.parent_id__C),childMarkupMap.get(Integer.valueOf(c.parent_id__C))+'&nbsp;<img src=\"'+c.logo__c+'\" width=\"25px\" height=\"25px\"><a href=\"'+c.link__C+'\"></a></img>');
    else childMarkupMap.put(Integer.valueOf(c.parent_id__C),childMarkupMap.get(Integer.valueOf(c.parent_id__C))+'<li><a href=\"'+c.link__C+'\">'+c.name+'</a></li>');
    }
    
    }
   
   
   }


    public void prepareMarkup()
    {
    String prefix = '<div class=\"col-md-2\"><div class=\"footerLinksWrap\"><ul>';
     String suffix = '</ul></div></div>';
     
    for(Integer i=1;i<=maxFooterItems;i++)
    {
    VMarket_Footer_Components__c headComp =  parentMap.get(i);
    if(i!=maxFooterItems) footerMarkup += prefix+'<li class=\"titleText\">'+headComp.name+childMarkupMap.get(Integer.valueOf(headcomp.order_id__c))+'</li>'+suffix;
    if(i==maxFooterItems) 
    {
    
    footerMarkup += '<div class=\"col-md-3\"><div class=\"footerLinksWrap\"><ul><li class=\"titleText\">'+headComp.name+childMarkupMap.get(Integer.valueOf(headcomp.order_id__c))+'</li>'+Label.vMarket_Footer_Disclaimer +suffix;
    }
    }
    
    }
*/
    public Boolean getShowError() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO);
    }
    
    // method will decide which Country Users eligible to view vMarket
    public Boolean getIsVmarketAvailable() {
        User usr = new User();
        usr = [SELECT Id , Country FROM User WHERE Id =:userInfo.getUserId() LIMIT 1];
        if(usr.Country != Label.vMarketUSA || usr.Country == '')
            return false;
        else
            return true;
    }
    
    // Check User's Role    
    public virtual String getRole() {
        String RoleName = [Select Id, vMarket_User_Role__c From User where id = : getUserId() limit 1].vMarket_User_Role__c;
        RoleName = RoleName.trim();
        Boolean IsDeveloper = RoleName.contains('Developer');
        Boolean IsCustomer = RoleName.contains('Customer');
        Boolean IsAdmin = RoleName.contains('Admin');
        
        if (IsDeveloper) {
            return 'Developer';
        } else if (IsCustomer) {
            return 'Customer';
        } else if (IsAdmin) {
            return 'Admin';
        } else {
            return '';
        }
    }

    // UUID - User Unique ID
    public virtual String getUserId() {
        return UserInfo.getUserId();
    }

    public virtual Boolean getAuthenticated() {
        try {
            List<User> UserProfileList = [Select Id, vMarket_User_Role__c From User where id = : UserInfo.getUserId() limit 1];
            if (UserProfileList.size() > 0) {
                String Profile = UserProfileList[0].vMarket_User_Role__c;
                return (Profile!=null) ? true : false;
            }
        } catch(Exception e){}
        return false;
    }
    
    public Boolean authenticateUserLogin() {
        
        User usr = new User();
        usr = [SELECT Id, profileId, vMarket_User_Role__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        List<Profile> prof = new List<Profile>();
        prof =  [SELECT id, Name FROM Profile WHERE Id=: usr.profileId AND Name =: Label.vMarket_GuestProfile LIMIT 1];
        if(!prof.isEmpty()) {
            return true;
        } else if(usr.vMarket_User_Role__c != Label.vMarket_Developer){
            return true;
        } else {
            return false;
        }
    }
    
    // method check if user is Guest
    public Boolean isGuestProfile(Id profileId) {
        List<Profile> prof = new List<Profile>();
        prof = [SELECT Id, Name FROM Profile WHERE id =: profileId AND Name =: Label.vMarket_GuestProfile LIMIT 1];
        if(!prof.isEmpty()) {
            system.debug(' ---- !!!!! ------');
            return true;
        } else {
            system.debug('----- ##### ----- ');
            return false;
        }
        
    }
    
    // method will decide if Logged in User is Developer/Customer or page visitor is Guest 
    public Boolean isDev() {
        User usr = [SELECT Id, profileId, vMarket_User_role__c FROM User WHERE Id =: UserInfo.getUserId() Limit 1];
        if(isGuestProfile(usr.profileId)) {
            return false;
        } else {
            if(usr.vMarket_User_role__c == Label.vMarket_Developer) {
                return false;
            } else {
                return true;
            }
        }
    }
        
    // method check If Customer Applied for Developer Access and Request is in Pending State
    public Boolean isDevRequestPending() {
        if(isGuestProfile(userInfo.getProfileId())) {
            return false;
        } else {
            List<vMarket_Developer_Stripe_Info__c> usrList = new List<vMarket_Developer_Stripe_Info__c>();
            usrList = [ SELECT Id, Name, Developer_Request_Status__c, OwnerId, Stripe_User__c, Stripe_User__r.vMarket_User_Role__c, isActive__c, Stripe_Id__c  
                        FROM vMarket_Developer_Stripe_Info__c 
                        WHERE OwnerId =: userInfo.getUserId() AND isActive__c =: true 
                                AND Developer_Request_Status__c =: Label.vMarket_Pending 
                                LIMIT 1]; // AND Stripe_Id__c != null
            if(!usrList.isEmpty() && usrList[0].isActive__c) { // user Request Exist and is in Pending State
                return true;
            } else {
                return false; // Customer didn't request for Developer Access.
            }
        }
    }
    
    // Get User FirstName
    public virtual String getFirstName() {
        return UserInfo.getFirstName();
    }

    // Get User LastName
    public virtual String getLastName() {
        return UserInfo.getLastName();
    }
    
    // Get Organization
    public virtual String getOrganization() {
        return UserInfo.getOrganizationName();
    }

    // Get User Email
    public virtual String getUserEmail() {
        return UserInfo.getUserEmail();
    }   
  
    // Optional
    public virtual String getProfileId() {
        return UserInfo.getProfileId();
    }
    
    public virtual String getProfileName() {
        Id id1 = UserInfo.getProfileId();
        List<Profile> profiles = [Select Name from Profile where Id =: id1];
        String profileName = profiles[0].Name;
        return profileName ;
    }
    
    
    
    // Get Session ID
    // Session Object will get using Cache.Session
    // To Store var in Session use, Cache.Session.put('counter', 0);
    // To Get var from Session use, (Integer)Cache.Session.get('counter');
    public virtual String getSessionId() {
        return UserInfo.getSessionId();
    }

    // Some common function
    public Integer maxCount = 5;
    
    public String FIELD_NAME =' App__r.Name, App__r.App_Category__c, App__r.App_Category__r.Name, App__r.subscription__c, App__r.ApprovalStatus__c, App__r.AppStatus__c, App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c, InstallCount__c,  PageViews__c, (Select Rating__c From vMarket_Comments__r) ';
    
    //public String FIELD_NAME = ' LogoUrl__c, Price__c, Name, DefaultListing__c, (Select Rating__c From vMarketComments__r) ';
    
    public List<vMarket_AppDO> getFeaturedApps() { return fetchAppsByType('PageViews__c DESC', 1); }
    public List<vMarket_AppDO> getPopularApps() { return fetchAppsByType('InstallCount__c DESC', 2); }
    public List<vMarket_AppDO> getRecentApps() { return fetchAppsByType('App__r.Published_Date__c DESC', 3); }
    public List<vMarket_AppDO> getFreeApps() { return fetchAppsByType('InstallCount__c DESC', 4); }

    public List<vMarket_AppDO> fetchAppsByType(String sortingParam, Integer sequence) {
    Set<Id> allowedIds = new Set<Id>(new vMarket_StripeAPIUtil().allowedApps());
        String queryStr = 'SELECT' + FIELD_NAME +
                        + 'FROM vMarket_Listing__c '
                        + 'WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true '+'AND App__c IN :allowedIds ';
                        
        if (sequence==4) 
            queryStr += ' AND App__r.Price__c = 0 ';
        
        queryStr += 'ORDER BY ' + String.escapeSingleQuotes(sortingParam)
                    + ' limit ' + String.escapeSingleQuotes(String.valueOf(maxCount));
        List<vMarket_Listing__c> listings  = Database.query(queryStr);
        System.debug('---Listings---'+listings.size());
        System.debug('---Apps Allowed---'+allowedIds);
        if (!listings.isEmpty()) {
            List<vMarket_AppDO> listingDO = new List<vMarket_AppDO>();
            for(vMarket_Listing__c lst : listings){
                System.debug('---Apps Allowed---'+allowedIds);
                system.debug('lst $$$$'+lst.app__c+'$$$$'+lst.App__r.Published_Date__c);
                if(allowedIds.contains(lst.App__c)){ 
                
                    listingDO.add(new vMarket_AppDO(lst));
                    system.debug('lst $$$$'+lst.app__r.name+'$$$$'+lst.App__r.Published_Date__c);
                }
            }
            return listingDO;
        }

        return null;
    }

    public virtual Integer getUserCartItemCount() {
        return cartItemObj.getUserCartItemCount();
    }

    public virtual void addCartItem() {
        cartItemObj.addCartItem();
    }

    public virtual void removeAllUserCartItem() {
        cartItemObj.removeAllUserCartItem();
    }
    
    public virtual void removeSelectedUserCartItem() {
        cartItemObj.removeSelectedUserCartItem();
    }
    
    public virtual void updateCartItemLine(String cartCookieIds, String oktaUserId) {
        cartItemObj.updateCartItemLine(cartCookieIds, oktaUserId);
    }

    public virtual String getAllAppsWithCategories() {
        return vMarketCatalogueObj.getAllAppsWithCategories();
    }

    public virtual Map<String, Integer> getAppCategories() {
        return vMarketCatalogueObj.getAppCategories();
    }
    
    // method will add Abbrivation K for amount greater than 1000 and M for millions
    public String currencyAbbri(Decimal amount) {
        
        if(amount >= 1000000 ) {
            return String.valueOf(amount/1000000) + 'M';
        } else if(amount >= 1000) {
            return String.valueOf(amount/1000) + 'K';
        } else {
            return String.valueOf(amount);
        }
        return String.valueOf(amount);
    }
    
    /**
     *  @desc   :   method check if user has accepted Terms of Use, If No redirect it to TermsOfUse page
     */
    public pageReference checkNewUser() {
        PageReference ref;
        User usr = new User();
        usr = [SELECT Id, ContactId, profileId, vMarket_User_Role__c, vMarketTermsOfUse__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        
        List<contact> contactsList = new List<Contact>();
        contactsList = [select account.Territories1__c, Account.ISO_Country__c,account.Country__c from Contact where Id =: usr.ContactId ];
        
        if(!contactsList.isEmpty()) {
            /*boolean allowedRegion = false;
                
            if(String.isNotBlank(contactsList[0].account.Territories1__c)) {
                for(String country : Label.vMarket_Allowed_Regions.split('\n',-1)) {
                    if(country.toLowerCase().equals(contactsList[0].account.Territories1__c.toLowerCase())) allowedRegion = true;                 
                }
            }
            if(!allowedRegion) {
                return new PageReference('/vMarketNotInRegion');
            }*/
            
            Map<String, vMarket_LiveControls__c> controlvMarketMap = new Map<String, vMarket_LiveControls__c>();
            // If custom setting has data, populate map else return false
            if(!vMarket_LiveControls__c.getAll().isEmpty()) {
                for(vMarket_LiveControls__c control : vMarket_LiveControls__c.getAll().values())
                    controlvMarketMap.put(control.Country__c, control);
                if(!controlvMarketMap.containsKey(contactsList[0].Account.ISO_Country__c)) {//  Country__c Changed for Globalisation
                    return new PageReference('/vMarketNotInRegion');
                }
            }
        }
        
        
        List<Profile> prof = new List<Profile>();
        prof =  [SELECT id, Name FROM Profile WHERE Id=: usr.profileId AND Name =: Label.vMarket_GuestProfile LIMIT 1];
        if(prof.isEmpty()) {
            if(!usr.vMarketTermsOfUse__c) {
                ref = new PageReference('/vMarketTermsOfUse');
                ref.setRedirect(true);
                return ref;
            }
        }
        return null;
    }
    
    /**
     *  @desc   :   Puneet Mishra, Authorize method to fetch code and scope of connected Stripe Account
     */
    public pageReference authorizeConnectAccount() {
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('code'));
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('scope'));
        if( (apexpages.currentpage().getparameters().get('code') != null) &&
                (apexpages.currentpage().getparameters().get('scope') != null) ) {            
        
            String customerStripeAccId = apexpages.currentpage().getparameters().get('code');
           
            // created a remote site setting https://varian--sfdev2.cs61.my.salesforce.com/0rp4C000000GyWO
            String clientId = vMarket_StripeAPI.clientKey;
            
            Map<String, String> requestBody = new Map<String, String>();
            requestBody.put('code', customerStripeAccId);
            requestBody.put('client_id', clientId);
            requestBody.put('grant_type', 'authorization_code');
            requestBody.put('scope', 'read_write');
           
            HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TOKEN_URL, 'POST', requestBody, false, null);
                    system.debug('request@@'+request);
            Map<String, String> requestResponse = vMarket_HttpResponse.sendRequest(request);
           system.debug(' requestResponse ' + requestResponse);
            if(!requestResponse.isEmpty() && requestResponse.get('Code') == '200') {
                system.debug(' ==== ' + requestResponse);
                List<vMarket_Developer_Stripe_Info__c> info = new List<vMarket_Developer_Stripe_Info__c>();
                info = [SELECT Id, Name, Access_Token__c, Contact__c, Developer_Request_Status__c, isActive__c, Rejection_Reason__c, Scope__c, 
                                Stripe_Id__c, Stripe_User__c, Token_Type__c, vMarket_Accept_Terms_of_Use__c 
                        FROM vMarket_Developer_Stripe_Info__c WHERE Stripe_User__c =: userInfo.getUserId() AND vMarket_Accept_Terms_of_Use__c =: true 
                         LIMIT 1];
                 //AND Developer_Request_Status__c =: Label.vMarket_Pending
                // condition check If user accept and close the window without adding Stripe Acc info
                if(info.isEmpty()) {
                    info[0].Name = userInfo.getName();
                    info[0].Stripe_User__c = userInfo.getUserId();
                    info[0].vMarket_Accept_Terms_of_Use__c = true;
                    info[0].Developer_Request_Status__c = Label.vMarket_Pending;
                    if(info[0].Contact__c == null) {
                        User usrRec = [SELECT id, Name, vMarket_User_Role__c, ContactId FROM User WHERE Id =: userInfo.getUserId() Limit 1];
                        Contact cont = [Select Id, vMarket_UserStatus__c FROM Contact Where Id =: usrRec.ContactId LIMIT 1];
                        info[0].Contact__c = cont.Id;
                    }
                }
                
                
                JSONParser parser = JSON.createParser(requestResponse.get('Body'));
                while(parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'stripe_user_id')) {
                        parser.nextToken();
                        info[0].Stripe_Id__c = parser.getText();
                    }
                    else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'token_type')) {
                        parser.nextToken();
                        info[0].Token_Type__c = parser.getText();
                    }
                    else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                        parser.nextToken();
                        info[0].Access_Token__c = parser.getText();
                    }
                    else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'scope')) {
                        parser.nextToken();
                        info[0].Scope__c = parser.getText();
                    }
                }
                system.debug(' ========= ');
                upsert info[0];
            }
        }
        return null;
        
    }
    
    public List<Integer> getDummyAppList(){
        List<Integer> appList = new List<Integer>();
        
        Integer max =0;
        
        
        if(getPopularApps()==null)max=0;
        else max= getPopularApps().size();
        
        for( Integer i=0; i < ( maxcount -  max); i++ ){
            appList.add(i);            
        }
        
        
        
        return appList;
    }
    
    public List<Integer> getDummyAppListforRecent(){
        List<Integer> appList = new List<Integer>();
        
        Integer max =0;
        
        
        if(getRecentApps()==null)max=0;
        else max= getRecentApps().size();
        
        for( Integer i=0; i < ( maxcount -  max); i++ ){
            appList.add(i);            
        }
        
        
        
        return appList;
    }
   
    

}
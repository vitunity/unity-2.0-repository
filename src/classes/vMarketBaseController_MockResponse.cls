/**
 *  @author        :      Puneet Mishra
 *  @description    :      MockResponse for vMarketBaseController authorize method 
 */
@isTest
global with sharing class vMarketBaseController_MockResponse implements HttpCalloutMock {
    
    public String resBody = '{Body={'+
              '"access_token": "sk_test_9YAb7QbJ2WrzFKYDhgjANxPf",'+
              '"livemode": false,'+
              '"refresh_token": "rt_B3LP2z6a3Rx4nwi6Efj9SKVb7MPx7rHdn5BECaehQfnMA2FX",'+
              '"token_type": "bearer",'+
              '"stripe_publishable_key": "pk_test_CKZzSGQyLGYwGdUoYwiUUT2E",'+
              '"stripe_user_id": "acct_1AhEhRAKEJ4l2FTF",'+
              '"scope": "read_write"'+
            '}, Code=200}';
    
  // Implementing interface method
    global HttpResponse respond(HTTPRequest req) {
        
     /*    req.setEndpoint('https://connect.stripe.com/oauth/token');
        req.setMethod('Post');
		Http h = new Http();
     HttpResponse res = h.send(req);
	 res.setBody(resBody);
     res.setStatusCode(200);*/
     // creating a fake response
      Httpresponse res = new Httpresponse();
      res.setBody(resBody);
      res.setStatusCode(200);
      return res;
    }
  
}
/**
*   @author         :       Puneet Mishra
*   @description    :       test class for vMarketBaseController
*/
@isTest
public with sharing class vMarketBaseController_Test {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
    
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, ADMIN1, ADMIN2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3;
    static List<Contact> lstContactInsert;
    
    static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', false);
        account.Ext_Cust_Id__c = '6051213';
        account.ERP_Site_Partner_Code__c = '32323232';
        account.Has_Location__c = true;
        insert account;
        
        ERP_Partner__c erpPartner = new ERP_Partner__c();
        erpPartner.Name = '21C/AMBERGRIS LLC';
        erpPartner.Active__c = true;
        erpPartner.Account_Group__c = 'Ship To';
        erpPartner.Street__c = '2000 FOUNDATION WY, SUITE 1100';
        erpPartner.City__c = 'MARTINSBURG';
        erpPartner.State_Province_Code__c = 'WV';
        erpPartner.Zipcode_Postal_Code__c = '25401-9003';
        erpPartner.Country_Code__c = 'US';
        erpPartner.Partner_Number__c = '2324449';
        erpPartner.Partner_Name__c = '21C/AMBERGRIS LLC';
        insert erpPartner;
        
        ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
        erpAssociation.Name = '6051213#2324449#0601#SH=Ship-to party';
        erpAssociation.Sales_Org__c = '0601';
        erpAssociation.ERP_Partner__c = erpPartner.Id;
        erpAssociation.Partner_Function__c = 'BP=Bill-to Party';
        erpAssociation.Customer_Account__c = account.Id;
        erpAssociation.ERP_Reference__c = '6051213#2324449#0601#SH=Ship-to party';
        erpAssociation.ERP_Customer_Number__c = '6051213';
        erpAssociation.ERP_Partner_Number__c = '2324449';
        erpAssociation.Delivery_Priority_Default__c = '03-Normal';
        erpAssociation.Shipping_Condition_Default__c = '03-Normal / Regular';
        insert erpAssociation;
        
        lstContactInsert = new List<Contact>();
        contact1 = vMarketDataUtility_Test.contact_data(account, false);
        contact1.MailingStreet = '53 Street';
        contact1.MailingCity = 'Palo Alto';
        contact1.MailingCountry = 'USA';
        contact1.MailingPostalCode = '94035';
        contact1.MailingState = 'CA';
        
        contact2 = vMarketDataUtility_Test.contact_data(account, false);
        contact2.MailingStreet = '660 N';
        contact2.MailingCity = 'Milpitas';
        contact2.MailingCountry = 'USA';
        contact2.MailingPostalCode = '94035';
        contact2.MailingState = 'CA';
        
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        
        lstContactInsert.add(contact1);
        lstContactInsert.add(contact2);
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
                          LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
                          LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
                          vMarket_User_Role__c = 'Customer');
        lstUserInsert.add(ADMIN1);
        
         ADMIN2 = new User(Email = 'test_Admin1@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN1', 'vMarket1', 70), 
                          LastName = 'test', IsActive = true, FirstName = 'Tester1', Alias = 'teMar1', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
                          LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = false, 
                          vMarket_User_Role__c = 'Customer');
        lstUserInsert.add(ADMIN2);
        
        insert lstUserinsert;
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
        app1.ApprovalStatus__c = 'Published';
        
        app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
        
        appList = new List<vMarket_App__c>();
        appList.add(app1);
        appList.add(app2);
        
        insert appList;
        
        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        
    }
    
    public static testMethod void Method1() {
        Test.StartTest();
        //Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
        //System.currentPageReference().getParameters().put('keyword', 'Test_App');
        vMarketBaseController control = new vMarketBaseController();
        control.getInfoExist();
        control.getShowError();
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Search requires more characters')) b = true;
        }
        Test.Stoptest();
    }
    
    public static testMethod void getIsVmarketAvailable_NotUSA() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            Boolean res = control.getIsVmarketAvailable();
            system.assertEquals(false, res);
        }
        Test.StopTest();
    }
    
    public static testMethod void getIsVmarketAvailable_USA() {
        Test.StartTest();
        system.runas(ADMIN1) {
            customer1.Country = 'USA';
            update customer1;
        }
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            Boolean res = control.getIsVmarketAvailable();
            system.assertEquals(true, res);
        }
        Test.StopTest();
    }
    
    public static testMethod void getRole_Customer() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            String res = control.getRole();
            system.assertEquals(res, 'Customer');
        }
        Test.Stoptest();
    }
    
    public static testMethod void getRole_Developer() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            String res = control.getRole();
            system.assertEquals(res, 'Developer');
            control.getUserId();
            control.isFreeDev=true;
            //Boolean isFreeDevl = control.isFreeDev;
            //System.assert(isFreeDevl == true);
        }
        Test.Stoptest();
    }
    
    public static testMethod void getRole_Admin() {
        Test.StartTest();
        system.runas(ADMIN1) {
            customer1.vmarket_User_Role__c = 'Admin';
            update customer1;
        }
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            String res = control.getRole();
            system.assertEquals(res, 'Admin');
        }
        Test.Stoptest();
    }
    
    public static testMethod void getAuthenticated() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.getAuthenticated();
            control.authenticateUserLogin();
            control.isGuestProfile(portal.Id);
        }
        Test.StopTest();
    }
    
    public static testMethod void isDev() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.isDev();
            
        }
        
        system.runas(customer1) {
            vMarketBaseController control1 = new vMarketBaseController();
            control1.isDev();
        }
        Test.Stoptest();
    }
    
    public static testMethod void isDevRequestPending() {
        Test.StartTest();
        vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
        devStripe.isActive__c = true;
        devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
        //System.debug('^^^^'+u.id);
        devStripe.Stripe_User__c = developer1.Id;
        devStripe.Developer_Request_Status__c = 'Pending';
        insert devStripe;
        
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.isDevRequestPending();
            control.isFreeDev=false;
        }
        
        Test.Stoptest();
    }
    
    public static testMethod void isDevRequestPending_Approved() {
        Test.StartTest();
        vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
        devStripe.isActive__c = true;
        devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
        //System.debug('^^^^'+u.id);
        devStripe.Stripe_User__c = developer1.Id;
        devStripe.Developer_Request_Status__c = 'Pending';
        insert devStripe;
        
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.isDevRequestPending();
        }
        
        Test.Stoptest();
    }
    
    public static testMethod void isFreeDevRequestPending_Approved() {
        Test.StartTest();
        vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
        devStripe.isActive__c = true;
        //System.debug('^^^^'+u.id);
        devStripe.Stripe_User__c = developer1.Id;
        devStripe.Developer_Request_Status__c = 'Pending';
        insert devStripe;
        
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
             control.isFreeDev=false;
        }
        
        Test.Stoptest();
    }
    
    public static testMethod void getFirstName() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.getFirstName();
            control.getLastName();
            control.getOrganization();
            control.getProfileId();
            control.getProfileName();
            control.getSessionId();
            control.getUserEmail();
        }
        Test.Stoptest();
    }
    
    public static testMethod void vMarketAppFeatures() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.getFeaturedApps();
            control.getPopularApps();
            control.getRecentApps();
            control.getFreeApps();
        }
        Test.StopTest();
    }
    
    public static testMethod void cartItemFeatures() {
        Test.StartTest();
        system.runas(ADMIN1) {
            contact1.OktaId__c = '1234';
            update contact1;
        }
        
        system.runas(customer1) {
            Test.setCurrentPageReference(new PageReference('Page.vMarketDetail'));
            System.currentPageReference().getParameters().put('appId', app1.Id);
            vMarketBaseController control = new vMarketBaseController();
            control.getUserCartItemCount();
            control.removeAllUserCartItem();
            control.removeSelectedUserCartItem();
            control.getAllAppsWithCategories();
            control.getAppCategories();
            control.updateCartItemLine('ABCD1213', '1234');
        }
        test.StopTest();
    }
    
    public static testmethod void shoppingcart() {
        test.starttest();
        system.runas(customer1) {
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart'));
            System.currentPageReference().getParameters().put('appId', app2.Id);
            vMarketBaseController control1 = new vMarketBaseController();
            control1.addCartItem();
        }
        test.stoptest();
    }
    
    public static testMethod void checkNewUser() {
        Test.StartTest();
        system.runas(ADMIN1) {
           
            vMarket_LiveControls__c liveCont = new vMarket_LiveControls__c();
            liveCont.Name = 'vMarket_MyVarianWeblink__c';
            liveCont.Country__c = 'USA';
            liveCont.vMarket_MyVarianWeblink__c = true;
            insert liveCont;
            
            account.Country__c = 'USA';
            update account;
        }
         
        system.runAs(customer1) {
            vMarketBaseController base = new vMarketBaseController();
            base.checkNewUser();
        }        
        Test.StopTest();
    }
     public static testMethod void checkNewUserNeg() {
        Test.StartTest();
        system.runas(ADMIN1) {
           
            vMarket_LiveControls__c liveCont = new vMarket_LiveControls__c();
            liveCont.Name = 'vMarket_MyVarianWeblink__c';
            liveCont.Country__c = 'USA';
            liveCont.vMarket_MyVarianWeblink__c = true;
            insert liveCont;
            
            account.Country__c = 'USA';
            update account;
        }
         User usr = new User();
            usr = [SELECT Id, ContactId, profileId, vMarket_User_Role__c, vMarketTermsOfUse__c FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
            List<Profile>  prof =  [SELECT id, Name FROM Profile WHERE Id=: usr.profileId AND Name =: Label.vMarket_GuestProfile LIMIT 1];
        
        system.runAs(ADMIN2) {
            vMarketBaseController base = new vMarketBaseController();
            base.checkNewUser();
        }
        
        Test.StopTest();
    }
    public static testMethod void currencyAbbri() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.currencyAbbri(100000000);
            control.currencyAbbri(1000);
            control.currencyAbbri(100);
        }
        Test.Stoptest();
    }
    
    public static testMethod void getDummyAppList() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.maxcount = 10;
            control.getDummyAppList();
        }
        Test.stopTest();
    }
    
    
    public static testmethod void authorizeconnect() {
        Test.StartTest();
        
        system.runAs(developer1) {
            vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
            devStripe.isActive__c = true;
            devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
            //System.debug('^^^^'+u.id);
            devStripe.Stripe_User__c = developer1.Id;
            devStripe.Developer_Request_Status__c = 'Pending';
            insert devStripe;
            
            Test.setMock(HttpCalloutMock.class, new vMarketBaseController_MockResponse());
            
            Map<String, String> reqBody = new Map<String, String>();
            reqBody.put('code', 'xx_yy_zzz');
            reqBody.put('client_id', 'c_adsreyy323234ghdajsd');
            reqBody.put('grant_type', 'read_write');
            reqBody.put('scope', 'read_write');
            reqBody.put('amount', '1000');
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
             System.currentPageReference().getParameters().put('client_id', 'c_adsreyy323234ghdajsd');
            System.currentPageReference().getParameters().put('code', 'xx_yy_zzz');
            System.currentPageReference().getParameters().put('scope', 'read_write');
            
            vMarketBaseController control = new vMarketBaseController();
            control.authorizeConnectAccount();
        }
              
        
        test.StopTest();
    }
    
      
    
    public static testMethod void getDummyAppListRecent() {
        Test.StartTest();
        system.runas(developer1) {
            vMarketBaseController control = new vMarketBaseController();
            control.maxcount = 10;
            control.getDummyAppListforRecent();
        }
        Test.stopTest();
    }
       
    
}
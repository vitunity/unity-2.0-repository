public virtual class vMarketBreadCrumbController {
    public String breadcrumb {get;set;}
    
    public vMarketBreadCrumbController() {
        breadcrumb = getBreadcrumb(ApexPages.currentPage().getUrl(),'',-999);
    }
    
    public String getBreadcrumb(String currentPage,String crumb,Integer parentId) {
        VMarket_Breadcrumbs__c bread;
        System.debug('currentPage%%%%%'+currentPage);
        boolean current = false;
        if(String.isNotBlank(currentPage)) {
            String pageName = '';

            if(currentpage.contains('?')) pageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
            else pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');

            Map<String, VMarket_Breadcrumbs__c> mapBreadCrumb = new Map<String, VMarket_Breadcrumbs__c>();
            Map<String, VMarket_Breadcrumbs__c> mapOriginalBreadCrumb = VMarket_Breadcrumbs__c.getAll() ;
            for(String b :mapOriginalBreadCrumb.keyset())  {
                mapBreadCrumb.put(b.toLowerCase(),mapOriginalBreadCrumb.get(b));   
            }
    
            if(mapBreadCrumb!=null && mapBreadCrumb.containsKey(pageName.toLowerCase())){
                bread = mapBreadCrumb.get(pageName.toLowerCase());
                current = true;
            } else {
                bread = new VMarket_Breadcrumbs__c();
            }
            //bread = VMarket_Breadcrumbs__c.getValues(pageName);
            System.debug('Parents%%%%%'+bread.name);
        } else {
            List<VMarket_Breadcrumbs__c> crumbs = [select name,parent_id__c,page__C,link_id__C from VMarket_Breadcrumbs__c where link_id__C =:parentId];
            if(crumbs!=null && crumbs.size()>0) bread = crumbs[0];
        }
  
        if(bread!=null) {
            System.debug('&&&crumb1'+crumb);
            //if(current) crumb = '&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i> <a href="/'+bread.name+'" class=\"current\" style="font-weight:bold;"> '+bread.page__c+'</a>' + crumb;
            if(current) crumb = '&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i><a class=\"current\" style="font-weight:bold;">'+bread.page__c+'</a>&nbsp;'+ crumb;
            else crumb = '&nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i> <a href="/'+bread.name+'" style="color: #666666;"> '+bread.page__c+'</a>' + crumb;

            System.debug('&&&crumb2'+crumb);
            if(bread.parent_id__c != -1) return getBreadcrumb(null,crumb,Integer.valueOf(bread.parent_id__c));
            else return crumb.replaceFirst('<i class="fa fa-angle-right" aria-hidden="true"></i>','');
        }
        return '';
    }
}
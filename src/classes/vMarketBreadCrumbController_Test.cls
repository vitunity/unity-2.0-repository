/**
 *  @author    :    Puneet Mishra
 *  @description:    Test Class for vMarketBreadCrumbController, static resource : vMarket_BreadCrumbTestData
 */
@isTest
public with sharing class vMarketBreadCrumbController_Test {
    
    private static testMethod void vMarketBreadCrumbController_test() {
      test.StartTest();
        vMarketDataUtility_Test.createBreadCrumdata();
        
        PageReference pageRef = Page.vMarketCatalogue;
      Test.setCurrentPage(pageRef);
        
        vMarketBreadCrumbController crumb = new vMarketBreadCrumbController();
        //crumb.getBreadcrumb();
      test.Stoptest();
    }
    
}
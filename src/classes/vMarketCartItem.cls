/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for CartItem Listing
****************************************************************************/
public class vMarketCartItem  {
    public String appId{get;set;}
    public Integer cartItemCount{get;set;}
    private List<String> appIdList = new List<String> {};
    public String cartCookieIds {get;set;}
    public Boolean isPageLoad {get;set;}
    public Boolean taxNotAvailable {get;set;}
    public String BaseURL;
    
    public Integer NumberItems{get;set;}
    public Decimal Total{get;set;}
    public Decimal SubTotal{get;set;}
    public Double Tax{get;set;}
    public String CurrencyIsoCode{get;set;}
    public String breadcrumb {get;set;}

    public Boolean DoesAppExistInCart {get;set;}
    public Integer CART_LIMIT {get;set;}

    public String CART_LIMIT_MSG;
    public String APP_ALREADY_BOUGHT_MSG;
    public String APP_ALREADY_IN_CART_MSG;
    public String APP_REMOVED_FROM_CART_MSG;
    public String TaxDetails;
    
    public String CART_LIMIT_LEVEL;
    public String APP_ALREADY_BOUGHT_LEVEL;
    public String APP_ALREADY_IN_CART_LEVEL;
    public String APP_REMOVED_FROM_CART_LEVEL;    

    public vMarketCartItem() {
        isPageLoad = getAuthenticated() ? true : false;
        if(ApexPages.currentPage()!=null && ApexPages.currentPage().getParameters()!=null && String.IsNotBlank(ApexPages.currentPage().getParameters().get('taxError')) ) taxNotAvailable =true;
        else taxNotAvailable = false;
        
        CART_LIMIT = 10;
        //BaseURL = 'https://' + System.URL.getSalesforceBaseURL().getHost(); //ApexPages.currentPage().getHeaders().get('Host');

        // Initialize static messages
        initializeMsg();
    }
    
    
    public void initializeMsg() {        
        CART_LIMIT_MSG = 'App Cart limit is 10 and it is already reached to limit';
        APP_ALREADY_BOUGHT_MSG = 'App Already Purchased';
        APP_ALREADY_IN_CART_MSG = 'App already added to the cart!';
        APP_REMOVED_FROM_CART_MSG = 'All apps are removed from the cart';
    }

    public Boolean getAuthenticated() {
        try {
            List<User> UserProfileList = [Select Id, vMarket_User_Role__c From User where id = : UserInfo.getUserId() limit 1];
            if (UserProfileList.size() > 0) {
                String Profile = UserProfileList[0].vMarket_User_Role__c;
                return (!Profile.contains('Guest') && Profile!=null) ? true : false;
            }
        } catch(Exception e){}
        return false;
    }

    public Integer getUserCartItemCount() {
        try {
            List<vMarketCartItem__c> cartItems = [select Name, (select Name, vMarket_App__c from vMarketCartItemLines__r) from vMarketCartItem__c where ownerid =: UserInfo.getUserId() limit 1] ;
            
            if (cartItems.size() > 0) {
                List<vMarketCartItemLine__c> cartItemList = cartItems[0].vMarketCartItemLines__r;
                system.debug(cartItemList);
                system.debug(cartItemList.size());
                return cartItemList.size();
            }
        }  catch(ListException le) {
            system.debug('------- GetUserCartItemLineCount Exception --------'+String.valueOf(le));
        }
        return 0;
    }

    public void addCartItem() {
        appId=ApexPages.currentPage().getParameters().get('appId');
        String cartItemID = null;
        Decimal Price=0;
        system.debug('------- AppID --------'+String.valueOf(appId));
        List<vMarketCartItem__c> Item = new List<vMarketCartItem__c>();
        Item = [select Id from vMarketCartItem__c where ownerid =: UserInfo.getUserId() limit 1] ;
        List<vMarket_App__c> appRow = new List<vMarket_App__c>();
        appRow = [select Price__c From vMarket_App__c where Id =: AppId limit 1];

        if (!appRow.isEmpty() && appRow[0].Price__c!=null) { Price=(decimal) appRow[0].Price__c; }     

        //if (Item.Id==null) {
        if (Item.isEmpty()) {
            try {
                vMarketCartItem__c cartitem = new vMarketCartItem__c();
                cartitem.ownerid = UserInfo.getUserId();
                insert cartitem;
                cartItemID=cartitem.id;
                system.debug(cartitem.id);
            } catch(Exception e) {
                system.debug('------- Cart Item Exception --------'+String.valueOf(e));
            }           
        } else {
            cartItemID=Item[0].Id;
            system.debug('------- cartItemID --------'+String.valueOf(cartItemID));
        }


        List<vMarketOrderItemLine__c> orderItms = [Select vMarket_Order_Item__r.OwnerId, vMarket_Order_Item__r.Id  From vMarketOrderItemLine__c 
                                           Where vMarket_App__c =: appId AND vMarket_Order_Item__r.OwnerId =: UserInfo.getUserId() AND vMarket_Order_Item__r.Status__c='Success' limit 1];
        Integer orderCount = [SELECT COUNT() FROM vMarketCartItemLine__c WHERE vMarketCartItem__c=:cartItemID];
        if (orderItms.size() > 0) {
            BaseURL = '/vMarketOrderDetail?Id='+ orderItms[0].vMarket_Order_Item__r.Id;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, APP_ALREADY_BOUGHT_MSG+' Please, Go to <a href=\"'+BaseURL+'\">Order</a>'));            
        } else if (orderCount==CART_LIMIT || orderCount > CART_LIMIT) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, CART_LIMIT_MSG));            
        } else {
            try {
                Integer rows = [SELECT COUNT() FROM vMarketCartItemLine__c WHERE vMarket_App__c=:appId AND vMarketCartItem__c=:cartItemID];
                if (rows==0) {
                    vMarketCartItemLine__c LineItem= new vMarketCartItemLine__c();
                    LineItem.vMarket_App__c = appId;
                    LineItem.vMarketCartItem__c = cartItemID;
                    insert LineItem;
                    system.debug('------LineItem--------'+LineItem.Id);
                } else {
                    DoesAppExistInCart = true;
                    system.debug('App into the cart debug log');
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, APP_ALREADY_IN_CART_MSG));                    
                }
            } catch (Exception e) {
                system.debug('------- Line Item Exception --------'+String.valueOf(e) + e.getLineNumber());
            }           
        }
    }

    public String getPageURL() {
        String urlVal = Apexpages.currentPage().getUrl();
        //String URL = 'https://' + BaseURL + urlVal;
        //return URL;
        return urlVal;
    }

    public void removeAllUserCartItem() {
        try{
            List<vMarketCartItem__c> Item = [select Id, (select Id from vMarketCartItemLines__r) from vMarketCartItem__c where ownerid =: UserInfo.getUserId() limit 1];
            if (!Item.isEmpty()) {
                for (vMarketCartItemLine__c line: Item[0].vMarketCartItemLines__r) {
                    // Delete all Line belongs to login user first
                    delete line;
                }
                // Delete CartItem
                delete Item;
            }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, APP_REMOVED_FROM_CART_MSG));            
        } catch (Exception e) {
            system.debug('------- Remove All Cart Item  Exception --------'+String.valueOf(e));
        }
    }
    
    public void removeSelectedUserCartItem() {
        appId=ApexPages.currentPage().getParameters().get('appId');
        try{
            vMarketCartItem__c Item = [select Id from vMarketCartItem__c where ownerid =: UserInfo.getUserId() limit 1];
            List<vMarketCartItemLine__c> LineItems = [select Id from vMarketCartItemLine__c where vMarketCartItem__c =: Item.Id and vMarket_App__c=:appId];
            Integer itemLineCount = [select COUNT() from vMarketCartItemLine__c where vMarketCartItem__c =: Item.Id];

            // Delete all Line belongs to login user first
            delete LineItems;
            
            if (itemLineCount==1 && LineItems.size()!=0) {
                delete Item;

                vMarketOrderItem__c orderItem = checkOrderItem();
                if (orderItem.Id!=null) delete orderItem;
            } else {
                // delete orderLineItems If exist
                deleteOrderItemLineByAppId(appId);
            }
        } catch (Exception e) {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'App does not exist in the cart!'));
            system.debug('------- Remove Selected Cart Item  Exception --------'+String.valueOf(e));
        }
    }
    
    public void updateCartItemLine(String cartCookieIds, String oktaUserId) {
        // get loggedin User
        User loginUser = new User();
        loginUser = [Select Id, IsActive from User where Contact.OktaId__c =: oktaUserId and isactive = true Limit 1];

        List<String> AppIdList = cartCookieIds.split(',');

        if (AppIdList.size() > 0) {
            try {
                String cartItemID = null;
                system.debug('-------------LoginUser ID-----------'+loginUser.Id);
                List<vMarketCartItem__c> Item = [select Id from vMarketCartItem__c where Ownerid =: loginUser.Id limit 1] ;
                if (Item.size() == 0) {
                    try {
                        vMarketCartItem__c cartitem = new vMarketCartItem__c();
                        cartitem.ownerid = loginUser.Id;
                        insert cartitem;
                        cartItemID=cartitem.id;
                        system.debug(cartitem.id);
                    } catch(Exception e) {
                        system.debug('------- Cart Item Exception --------'+String.valueOf(e));
                    }    
                } else {
                    cartItemID = String.valueOf(Item[0].Id);
                }

                Integer orderCount = [SELECT COUNT() FROM vMarketCartItemLine__c WHERE vMarketCartItem__c=:cartItemID]; 
                for (String appId: AppIdList) {
                    // Checking if User already bought app
                    List<vMarketOrderItemLine__c> orderItms = [Select vMarket_Order_Item__r.OwnerId, vMarket_Order_Item__r.Id  From vMarketOrderItemLine__c 
                                                       Where vMarket_App__c =: appId AND vMarket_Order_Item__r.OwnerId =: loginUser.Id AND vMarket_Order_Item__r.Status__c='Success' limit 1];

                    if (orderCount==CART_LIMIT || orderCount > CART_LIMIT) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, CART_LIMIT_MSG));                        
                        break;      
                    } else if (orderItms.size() == 0) {     
                        //Add Line in cartItem
                        try {
                            Integer rows = [SELECT COUNT() FROM vMarketCartItemLine__c WHERE vMarket_App__c=:appId AND vMarketCartItem__c=:cartItemID];
                            if (rows==0) {
                                vMarketCartItemLine__c LineItem= new vMarketCartItemLine__c();
                                LineItem.vMarket_App__c = appId;
                                LineItem.vMarketCartItem__c = cartItemID;
                                insert LineItem;
                                orderCount += 1;
                                system.debug('------updateCartItemLine LineItem--------'+LineItem.Id);
                            }
                        } catch (Exception e) {
                            system.debug('------- updateCartItemLine Line Item Exception --------'+String.valueOf(e));
                        }
                    }
                }
            } catch(Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.valueOf(e)));
            }
        }
    }
    
    public PageReference setCookieIdList() {
        cartCookieIds = ApexPages.currentPage().getParameters().get('cartCookieIds');
        isPageLoad = true;
        return null;
    }

    public List<vMarket_Listing__c> fetchAppListings(String appId) {
        String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c,  App__r.CurrencyIsoCode,'
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,'
            + ' App__r.PublisherName__c,App__r.PublisherPhone__c, App__r.PublisherWebsite__c, App__r.PublisherEmail__c ,App__r.Publisher_Developer_Support__c, '
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';

        String queryStr = 'SELECT' + FIELD_NAME +
                        + ' FROM vMarket_Listing__c '
                        + ' WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true AND App__r.id=\''+String.valueOf(appId)+'\'';
        return Database.query(queryStr);
    }


    /* 
    * When User is not logged in and he put many items in cart. And then he logged in.
    * on logged in user cart items will get store in vMarketCartItemLine object
    */    
    public List<vMarket_AppDO> getSessionCartItemDetails(List<vMarket_AppDO> listingsDo) {
        /* Get all cookies cart item list */
        if (cartCookieIds!=null && cartCookieIds!=''){
            /* split all comma seperated cart items ids */
            List<String> AppIdList = cartCookieIds.split(',');
            /* Iterate Ids one by one to put it into cart */
            for (String AppId: AppIdList) {
                List<vMarket_Listing__c> listings  = fetchAppListings(AppId);
                if (!listings.isEmpty()) {
                    CurrencyIsoCode = listings[0].App__r.CurrencyIsoCode;
                    listingsDo.add(new vMarket_AppDO(listings[0]));

                    SubTotal = (decimal) SubTotal+listings[0].App__r.Price__c;
                    Total = SubTotal.setScale(2);
                }
            }
            NumberItems = AppIdList.size();
            return listingsDo;
        }
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'APP CART IS EMPTY'));
        return null;        
    }

    /* 
    * When User is logged in and he add items into the cart.
    * Those items will get add to vMarketCartItemLine object
    */
    public List<vMarket_AppDO> getUserCartItemDetails(List<vMarket_AppDO> listingsDO) {
    Set<Id> allowedIds = new Set<Id>(new vMarket_StripeAPIUtil().allowedApps());   
        NumberItems = 0;Total = 0;SubTotal = 0;
        /* Fetch all cartitemslines belongs to current user */
        List<vMarketCartItem__c> Item = [select Id, (select Id, vMarket_App__c, vMarket_App__r.id, Total__c from vMarketCartItemLines__r) from vMarketCartItem__c where ownerid =: UserInfo.getUserId()];
        system.debug('^^^^item^^'+Item);
        /* Check size of cartItem records */
        if (Item.size() > 0) {
            /* Check size of cartItemLines inside cartItem records */
            if (Item[0].vMarketCartItemLines__r.size() > 0) {
                /* Iterate lineItem one by one to put it into cart */
                for (vMarketCartItemLine__c line: Item[0].vMarketCartItemLines__r) {
                System.debug('-----'+allowedIds.size()+line.vMarket_App__r.id);
                if(allowedIds.size()>0 && allowedIds.contains(line.vMarket_App__r.id))// Added for Globalisation
                {
                    List<vMarket_Listing__c> listings  = fetchAppListings(line.vMarket_App__c);
                    /* Append listing record into listings */
                    if (listings.size() > 0) {
                        CurrencyIsoCode = listings[0].App__r.CurrencyIsoCode;
                        listingsDO.add(new vMarket_AppDO(listings[0]));

                        /* Calculate total and subtotal excluding tax */
                        SubTotal = (decimal) SubTotal+listings[0].App__r.Price__c;
                        Total = SubTotal.setScale(2);
                    }
                }
                }
                NumberItems = Item[0].vMarketCartItemLines__r.size();
                system.debug('---------- Size------ '+String.valueOf(Item)+listingsDO);
                if(listingsDO.size()==0) listingsDO=null;// Added for Globalisation
                return listingsDO;
            }
        }
        return null;
    }

    /*
    * Use this gettter for vMarketShoppingCart.page excluding tax
    */
    public List<vMarket_AppDO> getCartItemLineList() {
        NumberItems = 0;Total = 0;SubTotal = 0;
        List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        try {
            // Get CartItemLines, if user logged in.
            if(getAuthenticated()) {
                listingsDO = getUserCartItemDetails(listingsDO); // With Login
                vMarketOrderItem__c orderItem = checkOrderItem();
            } else {
                if(!isPageLoad) return null; 
                listingsDO = getSessionCartItemDetails(listingsDO); // Without Login
            }
            System.debug('8888888'+listingsDO);
            if (listingsDO!=null)
                return listingsDo;
        } catch (Exception e) {
            listingsDo=null;
            system.debug('------- getCartItemLineList  Exception --------'+String.valueOf(e));
        }

        return listingsDo;
    }

    public Boolean getDoesErrorExist() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO);
    }

    public vMarketOrderItem__c checkOrderItem() {
        vMarketOrderItem__c orderItem = new vMarketOrderItem__c();
        List<vMarketOrderItem__c> orderList = [Select TaxDetails__c, ShippingAddress__c, Status__c, SubTotal__c, Tax__c, Total__c, ZipCode__c, Name, Tax_price__c From vMarketOrderItem__c where Owner.id=:UserInfo.getUserId() and (Status__c=null or Status__c='') and IsSubscribed__c = false];
        system.debug('orderList ===='+orderList);
        if (orderList.size() > 0) {
            orderItem = orderList[0];
        }
        return orderItem;
    }

    public Decimal retrieveTax() {
        vMarket_Tax_Calculator vMarketTaxObj = new vMarket_Tax_Calculator();
        Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
        //Tax = vMarketTaxObj.getTaxRate(CurrencyIsoCode, SubTotal);
        //TaxDetails = vMarketTaxObj.TaxDetails;
       if(SubTotal>0)// && !europeanCountries.contains(new vMarket_StripeAPIUtil().getUserCountry()))
        {
        Tax = vMarketTaxObj.getTaxRate(CurrencyIsoCode, SubTotal);
        TaxDetails = vMarketTaxObj.TaxDetails;
        }
        else
        {
        Tax = 0;
        }
        
        if(europeanCountries.contains(new vMarket_StripeAPIUtil().getUserCountry()) && Tax==-1) Tax=0;
        
        return (Tax!=null || Tax!=0) ? Tax : 0;
    }

    public vMarketOrderItem__c createNewOrderItem() {
        system.debug('------------CreateNewOrder---------');
        vMarketOrderItem__c orderItem = new vMarketOrderItem__c();
        orderItem.Tax__c = retrieveTax();
        orderItem.TaxDetails__c = TaxDetails;
        orderItem.IsSubscribed__c = false;
        insert orderItem;
        return orderItem;
    }

    public void updateOrderTax(vMarketOrderItem__c orderItem) {
        orderItem.Tax__c = retrieveTax();
        orderItem.TaxDetails__c = TaxDetails;
        update orderItem;
    }

    public void deleteOrderItemLineByAppId(Id AppId) {
        vMarketOrderItem__c orderItem = checkOrderItem();
        if (orderItem.Id!=null) {
            List<vMarketOrderItemLine__c> orderLineList = [Select Id From vMarketOrderItemLine__c where vMarket_App__c=:AppId AND vMarket_Order_Item__c=:orderItem.id];
            if (orderLineList.size() > 0)
                delete orderLineList;
        }
    }

    public Boolean deleteOrderItemLinesByOrderId(vMarketOrderItem__c orderItem) {
        List<vMarketOrderItemLine__c> orderLineList = [Select Id From vMarketOrderItemLine__c where vMarket_Order_Item__c=:orderItem.id];
        if (!orderLineList.isEmpty()) {
            try {
                delete orderLineList;
            } catch (DmlException e) {
                system.debug('Order Lines not getting deleted');
                return false;
            }
        }
        return true;
    }

    public void addOrderItemLine(Id appId, Id orderItemId, Decimal price) {
        vMarketOrderItemLine__c orderItemLine = new vMarketOrderItemLine__c(vMarket_App__c=appId, vMarket_Order_Item__c=orderItemId, Price__c=price, IsSubscribed__c=false);
        insert orderItemLine;
    }

    public vMarketOrderItem__c updateOrderAddress(vMarketOrderItem__c orderItem) {
        orderItem.Tax__c = retrieveTax();
        orderItem.TaxDetails__c = TaxDetails;
        update orderItem;
        return orderItem;
    }

    public PageReference createOrder() {
        vMarketOrderItem__c orderItem = checkOrderItem();
        system.debug('------------createOrder---------'+orderItem.Id);
        if (orderItem.Id!=null) {
            //if(orderItem.Tax__c==null || orderItem.Tax__c< 0)
           // {
            updateOrderTax(orderItem);
           // }
            // Delete Existing Order Lines, Reason is
            // Developer can change price of App anytime and
            // suppose customer already created order two days back and did not proceed with old order yet. then Order will take old price, in case of new app price
            Boolean isOrderLinesDeleted = deleteOrderItemLinesByOrderId(orderItem);
            if (!isOrderLinesDeleted)
                system.debug('Existing order item lines not getting deleted for user'+UserInfo.getUserEmail());
        } else {
            // create new OrderItem 
            orderItem = createNewOrderItem();
        }

        if(orderItem!=null)
        {
        if(orderItem.Tax__c==null || orderItem.Tax__c< 0) return new PageReference('/vMarketShoppingCart?taxError=false'); 
        }
        // Add Latest Order items in vMarketOrderItemLine__c object
        List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        listingsDO = getUserCartItemDetails(listingsDO);
        if(listingsDO==null) listingsDO = new List<vMarket_AppDO>(); // Added for Globalisation
        for (vMarket_AppDO lst:listingsDO) {
            addOrderItemLine(lst.getAppId(), orderItem.id, lst.getPrice());
        }
        return new PageReference('/vMarketCheckoutOrderInfo');
    }
}
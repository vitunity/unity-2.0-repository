/**
 *  @author     :       Puneet Mishra
 *  @description:       test class for vMarketCartItem
 */

@isTest
public with sharing class vMarketCartItem_Test {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app3;
    static List<vMarket_App__c> appList;
    
    static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
     static vMarketCartItemLine__c cartLineitm;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1, developer2;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3, contact4;
    static List<Contact> lstContactInsert;
    static User adminUsr1;
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        adminUsr1 = vMarketDataUtility_Test.createStandardUser(true, 'Admin', true, admin.Id, null, 'Admin1');
        account = vMarketDataUtility_Test.createAccount('test account', true);
        
        lstContactInsert = new List<Contact>();
        lstContactInsert.add(contact1 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact2 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        lstContactInsert.add(contact4 = vMarketDataUtility_Test.contact_data(account, false));
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        lstUserInsert.add(developer2 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact4.Id, 'Developer2'));
        insert lstUserInsert;
        
    
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
        app1.ApprovalStatus__c = 'Published';
        app1.IsActive__c = true;
        app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
        app3 = vMarketDataUtility_Test.createAppTest('Test Application 3', cate, false, developer1.Id);
        appList = new List<vMarket_App__c>();
        appList.add(app1);
        appList.add(app2);
        appList.add(app3);
        insert appList;
        
        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
         VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK');
        insert uk;
        
        vMarketConfiguration config = new vMarketConfiguration ();
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        //insert orderItemList;
        
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        /*orderItemLineList = new List<vMarketOrderItemLine__c>();
        system.runAs(Customer1) {
            orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        }
        
        system.RunAs(Customer2) {
            orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        }
        
        orderItemLineList.add(orderItemLine1);
        orderItemLineList.add(orderItemLine2);*/
        //insert orderItemLineList;
        system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart1, false);
        cartLine3 = vMarketDataUtility_Test.createCartItemLines(app3, cart2, false);
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
            insert cartLineList;
        //}
    }
    
    
     private static testMethod void vMarketCartItem() {
        test.StartTest();
            vMarketCartItem item = new vMarketCartItem();
            system.assertEquals(item.CART_LIMIT_MSG, 'App Cart limit is 10 and it is already reached to limit');
            system.assertEquals(item.APP_ALREADY_BOUGHT_MSG, 'App Already Purchased');
            system.assertEquals(item.APP_ALREADY_IN_CART_MSG, 'App already added to the cart!');
            system.assertEquals(item.APP_REMOVED_FROM_CART_MSG, 'All apps are removed from the cart');
            system.assertEquals(item.CART_LIMIT, 10);
            //system.assertEquals(item.isPageLoad, true);
        test.StopTest();
    }
    
   private static testMethod void addOrderItemLine_Test() {
        test.StartTest();
            system.runAs(adminUsr1) {
                vMarketCartItem item = new vMarketCartItem();
                item.addOrderItemLine(app1.Id, orderItem1.Id, 400);
                vMarketOrderItemLine__c orderLine = new vMarketOrderItemLine__c();
                orderLine = [SELECT Id, vMarket_App__c, vMarket_Order_Item__c, Price__c FROM vMarketOrderItemLine__c Limit 1];
                system.assertEquals(orderLine.vMarket_App__c, app1.Id);
                system.assertEquals(orderLine.vMarket_Order_Item__c, orderItem1.Id);
                //system.assertEquals(orderLine.Price__c, 400);
            }
        test.Stoptest();
    }
    
    private static testMethod void getUserCartItemCount_test() {
        test.StartTest();
            vMarketCartItem item = new vMarketCartItem();
            Integer res;
            system.runas(customer1) {
                res = item.getUserCartItemCount();
                system.assertEquals(2, res);
            }
            system.runas(customer2) {
                res = item.getUserCartItemCount();
                //system.assertEquals(1, res);
            }
        test.Stoptest();
    }
        
    private static testMethod void addCartItem_WithOrderItemLine_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            
            system.runas(adminUsr1) {
                orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
                orderItem1.Status__c='Success';
                update orderItem1;
                
                orderItemLine1 = [Select vMarket_Order_Item__r.OwnerId, vMarket_Order_Item__r.Id  From vMarketOrderItemLine__c Where Id =: orderItemLine1.Id LIMIT 1];
                
                item.addCartItem();
                
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                Boolean isMsg = false;
                String baseURL = '/vMarketOrderDetail?Id='+ orderItemLine1.vMarket_Order_Item__r.Id;
                for(Apexpages.Message msg:msgs){
                    if (msg.getDetail().contains('App Already Purchased' + ' Please, Go to <a href=\"' + baseURL + '\">Order</a>'))
                        isMsg = true;
                }
                //system.assert(isMsg);
            }
            
        test.Stoptest();
    }
    
    private static testMethod void addCartItem_WithOUTOrderItemLine_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            item.addCartItem();
            system.runas(adminUsr1) {
                //item.addCartItem();
            }
            
        test.Stoptest();
    }
    
    // Test method for 10 OR more than 10 orders
    private static testMethod void addCartItem_LIMITREACHED_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            
            system.runas(adminUsr1) {
                vMarketCartItemLine__c newCart;
                List<vMarketCartItemLine__c> cartList = new List<vMarketCartItemLine__c>();
                for(Integer i = 0; i < 12; i++) {
                     newCart = new vMarketCartItemLine__c();
                     newCart = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
                    cartList.add(newCart);
                }
                insert cartList;
                
                item.addCartItem();
                
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                Boolean isApexPageMsg = false;
                for(Apexpages.Message msg:msgs){
                    if (msg.getDetail().contains('App Cart limit is 10 and it is already reached to limit'))
                        isApexPageMsg = true;
                }
                
                //system.assert(isApexPageMsg);
            }
            
        test.Stoptest();
    }
    
    // test method for App already in Cart
    private static testMethod void addCartItem_APPALREADYADDED_Test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            Id AppId = appList[0].Id;
            
            vMarketCartItem item = new vMarketCartItem();
            //system.runas(customer1) {
                item.addCartItem();
            
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                Boolean isApexPageMsg = false;
                for(Apexpages.Message msg:msgs){
                    if (msg.getDetail().contains('App already added to the cart!'))
                        isApexPageMsg = true;
                }
                
                //system.assert(isApexPageMsg);
            
            //}
        test.STopTest();
    }
    
    // test method for removeAllUserCartItem
    private static testMethod void removeAllUserCartItem_Test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            system.runas(customer1) {
                item.removeAllUserCartItem();
                
                List<Apexpages.Message> msgs = ApexPages.getMessages();
                Boolean isApexPageMsg = false;
                for(Apexpages.Message msg:msgs){
                    if (msg.getDetail().contains('App already added to the cart!'))
                        isApexPageMsg = true;
                }
                
                //system.assert(isApexPageMsg);
            }
        test.Stoptest();
    }
    
    // testMethod for removeSelectedUserCartItem
    private static testMethod void removeSelectedUserCartItem_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            system.runas(customer1) {
                item.removeSelectedUserCartItem();
            }
        test.Stoptest();
    }
    
    // testMethod for removeSelectedUserCartItem when one item is in cart
    private static testMethod void removeSelectedUserCartItem_ONEITEMCOUNT_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app3.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            system.runas(customer2) {
                item.removeSelectedUserCartItem();
            }
        test.Stoptest();
    }
    
    // test method for updateCartItemLine, More than one CartItemLine
    private static testmethod void updateCartItemLine_MORETHANONECARTITEMLINE_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            String cookieIds = app1.Id+','+app2.Id;
            vMarketCartItem item = new vMarketCartItem();
            
            Contact cont = new Contact();
            String contId = [SELECT Id, ContactId FROM User WHERE Id =: customer1.id Limit 1].ContactId;
            cont = [SELECT Id, oktaId__c FROM Contact WHERE Id =: contId];
            cont.OktaId__c = customer1.Id;
            update cont;
            
            system.runas(customer1) {
                String oktaUserId = customer1.Id;
                
                item.updateCartItemLine(cookieIds, oktaUserId);
            }
        test.Stoptest();
    }
    
    // test method for updateCartItemLine, More than one CartItemLine
    private static testmethod void updateCartItemLine_ONECARTITEMLINE_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app3.Id);
            
            String cookieIds = app3.Id+',';
            vMarketCartItem item = new vMarketCartItem();
            
            Contact cont = new Contact();
            String contId = [SELECT Id, ContactId FROM User WHERE Id =: customer1.id Limit 1].ContactId;
            cont = [SELECT Id, oktaId__c FROM Contact WHERE Id =: contId];
            cont.OktaId__c = customer2.Id;
            update cont;
            
            system.runas(customer2) {
                String oktaUserId = customer2.Id;
                
                item.updateCartItemLine(cookieIds, oktaUserId);
            }
        test.Stoptest();
    }
    
    private static testMethod void getUserCartItemDetails_test() {
        Test.StartTest();
        cartLineitm = vMarketDataUtility_Test.createCartItemLines(app1, cart1, true);
        vMarket_Listing__c listing  = new vMarket_Listing__c();
        listing.isActive__c = true;
        listing.App__c = app1.Id;
         List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        vMarketCartItem itm = new vMarketCartItem();
        itm.getUserCartItemDetails(listingsDO);
        
        Test.StopTest();
    }
    
    //test method for orderCOUNT = CART_LIMIT
    private static testMethod void updateCartItemLine_ONEORDER_test() {
        test.StartTest();
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app3.Id);
            
            String cookieIds = app3.Id+',';
            vMarketCartItem item = new vMarketCartItem();
            
            Contact cont = new Contact();
            String contId = [SELECT Id, ContactId FROM User WHERE Id =: customer1.id Limit 1].ContactId;
            cont = [SELECT Id, oktaId__c FROM Contact WHERE Id =: contId];
            cont.OktaId__c = customer2.Id;
            update cont;
            
            system.runas(adminUsr1) {
                orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem1, false, true);
                String oktaUserId = customer2.Id;
                
                item.updateCartItemLine(cookieIds, oktaUserId);
            }
            
        test.StopTest();
    }
    
    // testMethod for getSessionCartItemDetails
    private static testMethod void getSessionCartItemDetails_Test() {
        
    }
    
    // testMethod for createOrder
    private static testMethod void createOrder_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            system.runas(customer1) {
                orderItem1.Status__c = '';
                update orderItem1;
                
                vMarketCartItem item = new vMarketCartItem();
                item.createOrder();
            }
            
        test.Stoptest();
    }
    
    // testmethod for deleteOrderItemLinesByOrderId
    private static testMethod void deleteOrderItemLinesByOrderId_test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            system.runas(customer1) {
                Boolean res = item.deleteOrderItemLinesByOrderId(orderItem1);
                List<vMarketOrderItemLine__c> orderLineList = new List<vMarketOrderItemLine__c>();
                orderLineList = [Select Id From vMarketOrderItemLine__c where vMarket_Order_Item__c=:orderItem1.id];
                
                //system.assertEquals(new List<vMarketOrderItemLine__c>(), orderLineList);
                //system.assertEquals(res, true);
            }
        test.Stoptest();
    }
    
    // test method for setCookieIdList
    private static testMethod void setCookieIdList_Test() {
        test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            System.currentPageReference().getParameters().put('cartCookieIds', '12345');
            
            vMarketCartItem item = new vMarketCartItem();
            item.setCookieIdList();
            system.assertEquals(item.cartCookieIds, '12345');
            system.assertEquals(item.isPageLoad, true);
        test.Stoptest();
    }
        
    // testmethod for fetchAppListings when Listing exists for App
    private static testMethod void fetchAppListings_Test() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app2.Id);
            
            app2.ApprovalStatus__c = 'Published';
            update app2;
            
            String appId = app2.Id;
            
            String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,'
            + ' App__r.PublisherName__c,App__r.PublisherPhone__c, App__r.PublisherWebsite__c, App__r.PublisherEmail__c ,App__r.Publisher_Developer_Support__c, '
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';

            String queryStr = 'SELECT' + FIELD_NAME + ' FROM vMarket_Listing__c WHERE App__r.id=\''+String.valueOf(appId)+'\'';
            List<vMarket_Listing__c> actual = Database.query(queryStr);
            
            vMarketCartItem item = new vMarketCartItem();
            List<vMarket_Listing__c> reslisting = item.fetchAppListings(app2.Id);
            
            //system.assertEquals(actual, reslisting);
        Test.StopTest();
    }
    
    // testmethod for fetchAppListings when Listing exists for App
    private static testMethod void fetchAppListings_NOLISTING_Test() {
        Test.StartTest();
            
            vMarketCartItem item = new vMarketCartItem();
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            //System.currentPageReference().getParameters().put('appId', app2.Id);
            
            
            ApexPages.currentPage().getParameters().put('appId',app2.Id);
            
            String appId = app2.Id;
            system.debug('$$$apid='+appId);
            
            String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,'
            + ' App__r.PublisherName__c,App__r.PublisherPhone__c, App__r.PublisherWebsite__c, App__r.PublisherEmail__c ,App__r.Publisher_Developer_Support__c, '
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';

            String queryStr = 'SELECT' + FIELD_NAME + ' FROM vMarket_Listing__c WHERE App__r.id=\''+String.valueOf(appId)+'\'';
            List<vMarket_Listing__c> actual = Database.query(queryStr);
            
            
            List<vMarket_Listing__c> reslisting = item.fetchAppListings(appId);
            
            system.assertEquals(actual, reslisting);
        Test.StopTest();
    }
    
    private static testMethod void getPageURL_test() {
        test.StartTest();
            
            Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
            System.currentPageReference().getParameters().put('appId', app1.Id);
            
            vMarketCartItem item = new vMarketCartItem();
            String res = item.getPageURL();
            system.debug(' ########## ' + res);
        test.StopTest();
    }
    
    private static testMethod void getDoesErrorExist() {
        Test.StartTest();
            vMarketCartItem item = new vMarketCartItem();
            item.getDoesErrorExist();
        Test.StopTest();
    }
    
    private static testMethod void getCartItemLineList() {
        Test.StarTTest();
             
             VMarket_Country_App__c conApp=new VMarket_Country_App__c(VMarket_App__c=app1.Id, country__C='US', status__C='Active', VMarket_Approval_Status__c='Published');
             insert conApp;
             
             app1.Price__c=100;
             update app1;
        	
            system.runas(customer1){   
               	Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
                System.currentPageReference().getParameters().put('appId', app1.Id);
                
                String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, App__r.CurrencyIsoCode,'
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.Owner.firstname,App__r.Owner.lastname, InstallCount__c,'
            + ' App__r.PublisherName__c,App__r.PublisherPhone__c, App__r.PublisherWebsite__c, App__r.PublisherEmail__c ,App__r.Publisher_Developer_Support__c, '
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';

            String queryStr = 'SELECT' + FIELD_NAME + ' FROM vMarket_Listing__c ';
            listing = Database.query(queryStr);
               
                vMarketCartItem item = new vMarketCartItem();
                item.cartCookieIds = app1.Id+','+app2.Id+','+app3.Id;
                List<vMarket_AppDO> appDoList = new List<vMarket_AppDO>();                
                appDOList = item.getCartItemLineList();
                system.debug('appDOList==='+appDOList);
               // item.getSessionCartItemDetails(appDOList);
            }
        Test.StopTest();
    }
    
    private static testMethod void createNewOrderItem() {
        Test.STartTest();
            system.runas(customer1) {
                Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
                System.currentPageReference().getParameters().put('appId', app1.Id);
                
                vMarketCartItem item = new vMarketCartItem();
                item.createNewOrderItem();
            }
        Test.StopTest();
    }
    
    private static testMethod void deleteOrderItemLineByAppId() {
        Test.STartTest();
            system.runas(customer1) {
                Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
                System.currentPageReference().getParameters().put('appId', app1.Id);
                
                vMarketCartItem item = new vMarketCartItem();
                item.deleteOrderItemLineByAppId(app1.Id);
                item.updateOrderTax(orderItem1);
            }
        Test.StopTest();
    }
    
    private static testMethod void deleteOrderItemLinesByOrderId() {
        Test.STartTest();
            system.runas(customer1) {
                Test.setCurrentPageReference(new PageReference('Page.vMarketShoppingCart')); 
                System.currentPageReference().getParameters().put('appId', app1.Id);
                
                vMarketCartItem item = new vMarketCartItem();
                item.deleteOrderItemLinesByOrderId(orderItem1);
                item.updateOrderAddress(orderItem1);
            }
        Test.StopTest();
    }
}
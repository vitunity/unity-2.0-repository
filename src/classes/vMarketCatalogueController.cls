/**************************************************************************\
    @ Author    : Yogesh Nandgadkar
    @ Updated By: Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for catalogue item listing
****************************************************************************/
/* Class Modified by Abhishek K as Part of Globalisation VMT-25 */
 public class vMarketCatalogueController {
    public Map<Integer, String> FILTER_MAP = new Map<Integer, String>();
    public Map<Integer, String> SORTING_OPTION = new Map<Integer, String>();
    public Map<String, Integer> APP_CATEGORY_MAP = new Map<String, Integer>();
    public String sortingParam;
    public Integer filterId;
    public String catName {get;set;}
    public String keyword {get;set;}
    public String DEFAULT_CATEGORY_VALUE;
    public Set<Id> allowedIds{get;set;}
    public integer totalRecs {get;set;}
    public integer index = 0;
    public integer blockSize {
        get {
            return 10;
        }
        set;
    }

    public Boolean getDoesInfoExist() {
        return ApexPages.hasMessages(ApexPages.Severity.INFO);
    }

    public vMarketCatalogueController() {
      allowedIds = new Set<Id>(new vMarket_StripeAPIUtil().allowedApps());   
        // Initiaize totalrecs here
        totalRecs = 0;

        // set default category value
        DEFAULT_CATEGORY_VALUE = 'All Categories';
        
        // Filter options on VF Page for 'Sort By'
        //FILTER_MAP.put(0, 'All Apps');
        FILTER_MAP.put(4, 'A-Z');
        FILTER_MAP.put(7, 'Free Apps');
        FILTER_MAP.put(1, 'Most Popular');
        FILTER_MAP.put(2, 'Most Viewed');
        FILTER_MAP.put(3, 'Most Recent');
        FILTER_MAP.put(5, 'Price - Low to High');
        FILTER_MAP.put(6, 'Price - High to Low');

        // Sorting for Query as per above selected Filter Option
        //SORTING_OPTION.put(0, '');
        SORTING_OPTION.put(1, 'InstallCount__c DESC');
        SORTING_OPTION.put(2, 'PageViews__c DESC');
        SORTING_OPTION.put(3, 'App__r.Published_Date__c DESC');
        SORTING_OPTION.put(4, 'App__r.Name ASC');
        SORTING_OPTION.put(5, 'App__r.Price__c ASC');
        SORTING_OPTION.put(6, 'App__r.Price__c DESC');
        SORTING_OPTION.put(7, 'App__r.Price__c DESC');

        // Using for pagination purpose
        setTotalAppCountByFilter();
    }

    public Integer getFilterType() {
        try {
            filterId = Integer.valueOf(apexpages.currentpage().getparameters().get('filterBy'));
        } catch(System.NullPointerException e) {}
        
        if (filterId==null || !FILTER_MAP.containsKey(filterId)) {
            return 0;  //default filter type 'All Apps'
        }
        
        return filterId;
    }
    
    public String getSearchKeyword() {
        try {
            keyword = String.valueOf(apexpages.currentpage().getparameters().get('keyword'));
            keyword = String.escapeSingleQuotes(keyword);
            if (keyword!=null && keyword!='')
                return ' AND (App__r.Name LIKE \'%'+keyword+'%\' OR App__r.Keywords__c LIKE \'%'+keyword+'%\')';
        } catch(System.NullPointerException e) {}
        keyword = '';
        return null;
    }    

    public String getQueryCatName() {
        try {
            catName = String.valueOf(apexpages.currentpage().getparameters().get('cat'));
            String tabName = String.valueOf(apexpages.currentpage().getparameters().get('tab'));
            catName = String.escapeSingleQuotes(catName);
            tabName = String.escapeSingleQuotes(tabName);
            if (tabName!=null && tabName!=DEFAULT_CATEGORY_VALUE)
                return ' AND App__r.App_Category__r.Name=\''+String.escapeSingleQuotes(tabName)+'\'';
            else if (tabName!=null && tabName==DEFAULT_CATEGORY_VALUE)
                return null;
            else if (catName!=null && catName!=DEFAULT_CATEGORY_VALUE)
                return ' AND App__r.App_Category__r.Name=\''+String.escapeSingleQuotes(catName)+'\'';
        } catch(System.NullPointerException e) {}
        catName = DEFAULT_CATEGORY_VALUE;
        return null;
    } 

    public void setFilterType(Integer fId) {
        filterId = fId;
    }

    public List<SelectOption> getFilterCategories() {
        List<SelectOption> options = new List<SelectOption>();
        for(Integer key : FILTER_MAP.keySet()) {
            options.add(new SelectOption(String.valueOf(key), FILTER_MAP.get(key)));
        }
        return options;
    }

 /* Added by Abhishek K as Part of Globalisation VMT-25 for checking if App Belongs to Country or Not*/
    public String getAllAppsWithCategories() {
        Map<String, String> mapApps = new Map<String, String>();
        List<vMarket_Listing__c> allapps = [select Id, App__r.App_Category__r.Name, App__r.Name From vMarket_Listing__c];
        for(vMarket_Listing__c app: allapps) {
            if(!test.isRunningTest())
            {
                if(allowedIds.contains(app.App__c)) mapApps.put(app.App__r.Name, app.App__r.App_Category__r.Name);
            }
            else
            {
                mapApps.put('Test_App4', 'Eclipse');
            }
        }
        
        String JSONtext = JSON.serialize(mapApps);
        return JSONtext;
    }

    public Map<String, Integer> getAppCategories() {
        String FIELD_NAME =' App_Category__r.Name, COUNT(Id) ';
        String queryStr = 'SELECT' + FIELD_NAME +
            + 'FROM vMarket_App__c '
            + 'WHERE ApprovalStatus__c=\'Published\' AND IsActive__c=true '
            + 'GROUP BY App_Category__r.Name ';

        Integer total_app = 0;
        Integer app_count = 0;
        String name = null;
        List<AggregateResult> apps  = Database.query(queryStr);
        
        for(AggregateResult ap: apps) {
            app_count = Integer.valueOf(ap.get('expr0'));
            name = String.valueOf(ap.get('Name'));
            APP_CATEGORY_MAP.put(name, app_count);
            total_app += app_count;
        }
        APP_CATEGORY_MAP.put(DEFAULT_CATEGORY_VALUE, total_app);
        return APP_CATEGORY_MAP;
    }

    public void setTotalAppCountByFilter() {
        String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.subscription__c, InstallCount__c,'
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';
        String queryStr = 'SELECT' + FIELD_NAME +
                        + ' FROM vMarket_Listing__c '
                        + ' WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true AND app__c IN :allowedIds';

        if (getQueryCatName()!=null && getQueryCatName()!=DEFAULT_CATEGORY_VALUE) queryStr+=getQueryCatName();
        if (getSearchKeyword()!=null) queryStr+=getSearchKeyword();
        
        if (getFilterType()!=7 && getFilterType()!=0) {
            sortingParam = SORTING_OPTION.get(getFilterType());
            queryStr+= ' ORDER BY ' + String.escapeSingleQuotes(sortingParam);
        } else if(getFilterType()!=0) {
            queryStr+= ' AND App__r.Price__c = 0 ';
        }

        List<vMarket_Listing__c> listings  = Database.query(queryStr);
        totalRecs = listings.size();
    }

    public List<vMarket_AppDO> getPublishedApps() {
        system.debug('---- TOTALCOUNT ------'+String.valueOf(totalRecs));
        system.debug('---- INDEX ------'+String.valueOf(index));
        system.debug('---- BLOCKSIZE ------'+String.valueOf(blockSize));
        List<vMarket_AppDO> listingDO;
        system.debug('$$$$ allowedIds$$$'+allowedIds.size());
        String FIELD_NAME = ' App__r.Name, App__r.App_Category__r.Name, App__r.ApprovalStatus__c, App__r.AppStatus__c, '
            + ' App__r.IsActive__c, App__r.Key_features__c, App__r.Long_Description__c, '
            + ' App__r.Price__c, App__r.Published_Date__c, App__r.Short_Description__c,App__r.subscription__c, InstallCount__c,'
            + ' PageViews__c, (Select Rating__c From vMarket_Comments__r) ';
        String queryStr = 'SELECT' + FIELD_NAME +
                        + ' FROM vMarket_Listing__c '
                        + ' WHERE App__r.ApprovalStatus__c=\'Published\' AND App__r.IsActive__c=true AND app__c IN :allowedIds';

        if (getQueryCatName()!=null && getQueryCatName()!=DEFAULT_CATEGORY_VALUE) queryStr+=getQueryCatName();
        if (getSearchKeyword()!=null) queryStr+=getSearchKeyword();
        
        if (getFilterType()!=7 && getFilterType()!=0) {
            sortingParam = SORTING_OPTION.get(getFilterType());
            queryStr+= ' ORDER BY ' + String.escapeSingleQuotes(sortingParam);
        } else if (getFilterType()!=0) {
            queryStr+= ' AND App__r.Price__c = 0 ';
        }

        queryStr += ' LIMIT :blockSize OFFSET :index';
        system.debug('queryStr -----'+queryStr);
        List<vMarket_Listing__c> listings  = Database.query(queryStr);
        system.debug('lising-----'+listings.size());
        if (!listings.isEmpty()) {
            listingDO = new List<vMarket_AppDO>();
            for(vMarket_Listing__c lst : listings){
               //if(allowedIds.contains(lst.App__c)) listingDO.add(new vMarket_AppDO(lst));
               listingDO.add(new vMarket_AppDO(lst));
            }
            system.debug('listingDO -----'+listingDO +'===='+listingDO.size());
            return listingDO;
        }

        return listingDO;
    }

    // Bellow all are the pagination method
    // Pagination method starts here
    public void beginning() {
        index = 0;
    }
    
    public void previous() {
        index = index - blockSize;
        if (index < 0)
            index = 0;
    }
    
    public void next() {
        index = index + blockSize;
    }

    public void end() {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev() {
        if(index == 0)
        return true;
        else
        return false;
    }
    
    public String getCurPage() {
        return String.valueOf((index/blockSize)+1);     
    }

    public boolean getnxt() {
        system.debug('==index =='+index+'===blockSize=='+blockSize+'===totalRecs=='+totalRecs);
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    }
    // Pagination method ends here
}
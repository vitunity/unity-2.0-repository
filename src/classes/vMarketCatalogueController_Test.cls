/**
 *	@author				:			Puneet Mishra
 *	@description		:			test class for vMarketCatalogueController
 */
@isTest
public with sharing class vMarketCatalogueController_Test {
    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
 	
 	static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
	
	static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
	
	static Profile admin,portal;   
   	static User customer1, customer2, developer1;
   	static List<User> lstUserInsert;
   	static Account account;
	static Contact contact1, contact2, contact3;
   	static List<Contact> lstContactInsert;
   	
   	static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
	
	static {
		admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
    	
    	account = vMarketDataUtility_Test.createAccount('test account', false);
    	account.Ext_Cust_Id__c = '6051213';
    	insert account;
    	
    	ERP_Partner__c erpPartner = new ERP_Partner__c();
    	erpPartner.Name = '21C/AMBERGRIS LLC';
    	erpPartner.Active__c = true;
    	erpPartner.Account_Group__c = 'Ship To';
    	erpPartner.Street__c = '2000 FOUNDATION WY, SUITE 1100';
    	erpPartner.City__c = 'MARTINSBURG';
    	erpPartner.State_Province_Code__c = 'WV';
    	erpPartner.Zipcode_Postal_Code__c = '25401-9003';
    	erpPartner.Country_Code__c = 'US';
    	erpPartner.Partner_Number__c = '2324449';
    	erpPartner.Partner_Name__c = '21C/AMBERGRIS LLC';
    	insert erpPartner;
    	
    	ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
    	erpAssociation.Name = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.Sales_Org__c = '0601';
    	erpAssociation.ERP_Partner__c = erpPartner.Id;
    	erpAssociation.Partner_Function__c = 'BP=Bill-to Party';
    	erpAssociation.Customer_Account__c = account.Id;
    	erpAssociation.ERP_Reference__c = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.ERP_Customer_Number__c = '6051213';
    	erpAssociation.ERP_Partner_Number__c = '2324449';
    	erpAssociation.Delivery_Priority_Default__c = '03-Normal';
    	erpAssociation.Shipping_Condition_Default__c = '03-Normal / Regular';
    	insert erpAssociation;
    	
    	lstContactInsert = new List<Contact>();
    	contact1 = vMarketDataUtility_Test.contact_data(account, false);
    	contact1.MailingStreet = '53 Street';
    	contact1.MailingCity = 'Palo Alto';
    	contact1.MailingCountry = 'USA';
    	contact1.MailingPostalCode = '94035';
    	contact1.MailingState = 'CA';
    	
		contact2 = vMarketDataUtility_Test.contact_data(account, false);
		contact2.MailingStreet = '660 N';
    	contact2.MailingCity = 'Milpitas';
    	contact2.MailingCountry = 'USA';
    	contact2.MailingPostalCode = '94035';
    	contact2.MailingState = 'CA';
    	
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	
    	lstContactInsert.add(contact1);
    	lstContactInsert.add(contact2);
    	insert lstContactInsert;
    	
    	lstUserInsert = new List<User>();
    	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
		lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
    	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
    	
    	insert lstUserinsert;
    	
    	cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
    	ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
    	app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
    	app1.ApprovalStatus__c = 'Published';
    	app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
    	
    	appList = new List<vMarket_App__c>();
    	appList.add(app1);
    	appList.add(app2);
    	
    	insert appList;
    	
    	listing = vMarketDataUtility_Test.createListingData(app1, true);
    	comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
    	activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
    	
    	source = vMarketDataUtility_Test.createAppSource(app1, true);
    	feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
    	
    	orderItemList = new List<vMarketOrderItem__c>();
    	system.runAs(Customer1) {
    		orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
    	}
    	system.runAs(Customer2) {
    		orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
    	}
    	
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
		
	}
    
    public static testMethod void testPageMessages() {
		Test.StartTest();
			system.runas(customer1) {
				List<Apexpages.Message> msgs = ApexPages.getMessages();
				boolean b = false;
				
				vMarketCatalogueController control = new vMarketCatalogueController();
				control.getDoesInfoExist();
				control.getDoesInfoExist();
				for(Apexpages.Message msg:msgs){
				    if (msg.getDetail().contains('Search requires more characters')) b = true;
				}
			}
		Test.Stoptest();
	}
	
	public static testMethod void getFilterType() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('filterBy', '4');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getFilterType();
			control.setFilterType(2);
		Test.Stoptest();
	}
	
	public static testMethod void getSearchKeyword() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('keyword', 'Test_App');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getSearchKeyword();
		Test.Stoptest();
	}
	
	public static testMethod void getQueryCatName() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('cat', 'Eclipse');
			System.currentPageReference().getParameters().put('tab', 'null');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getQueryCatName();
		Test.Stoptest();
	}
	
	public static testMethod void getQueryCatName2() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('cat', 'null');
			System.currentPageReference().getParameters().put('tab', 'Eclipse');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getQueryCatName();
		Test.Stoptest();
	}
	
	public static testMethod void getQueryCatName3() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('cat', 'null');
			System.currentPageReference().getParameters().put('tab', 'All Categories');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getQueryCatName();
		Test.Stoptest();
	}
	
	public static testMethod void getFilterCategories() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('cat', 'null');
			System.currentPageReference().getParameters().put('tab', 'All Categories');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.getFilterCategories();
		test.Stoptest();
	}
	
	public static testMethod void getAllAppsWithCategories() {
		Test.StartTest();
			listing = [select Id, App__r.App_Category__r.Name, App__r.Name From vMarket_Listing__c];
			system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
				System.currentPageReference().getParameters().put('filterBy', '4');
				System.currentPageReference().getParameters().put('cat', 'Eclipse');
				System.currentPageReference().getParameters().put('tab', 'Eclipse');
				vMarketCatalogueController control = new vMarketCatalogueController();
				control.getAllAppsWithCategories();
			}
		Test.Stoptest();
	}
	
	public static testMethod void getPublishedApps() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
			System.currentPageReference().getParameters().put('filterBy', '4');
			System.currentPageReference().getParameters().put('cat', 'Eclipse');
			System.currentPageReference().getParameters().put('tab', 'Eclipse');
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.blockSize = 5;
			control.getPublishedApps();
		Test.Stoptest();
	}
	
	public static testMethod void setTotalAppCountByFilter() {
		Test.StartTest();
			listing = [select Id, App__r.App_Category__r.Name, App__r.Name From vMarket_Listing__c];
			system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
				System.currentPageReference().getParameters().put('filterBy', '4');
				System.currentPageReference().getParameters().put('cat', 'Eclipse');
				System.currentPageReference().getParameters().put('tab', 'Eclipse');
				vMarketCatalogueController control = new vMarketCatalogueController();
				control.setTotalAppCountByFilter();
			}
		Test.Stoptest();
	}
	
	public static testMethod void getAppCategories() {
		Test.StartTest();
			listing = [select Id, App__r.App_Category__r.Name, App__r.Name From vMarket_Listing__c];
			system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketCatalogue'));
				System.currentPageReference().getParameters().put('filterBy', '4');
				System.currentPageReference().getParameters().put('cat', 'Eclipse');
				System.currentPageReference().getParameters().put('tab', 'Eclipse');
				vMarketCatalogueController control = new vMarketCatalogueController();
				control.getAppCategories();
			}
		Test.Stoptest();
	}
	
	public static testMethod void testPagination() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.beginning();
			
			control.index = 10;
			control.blocksize = 2;
			control.previous();
			
			control.next();
			
			control.totalRecs = 20;
			control.end();
		Test.Stoptest();
	}
	
	public static testMethod void getPrev() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 0;
			control.getPrev();
		Test.Stoptest();
	}
	
	public static testMethod void getPrev2() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 10;
			control.getPrev();
		Test.Stoptest();
	}
	
	public static testMethod void getCurPage() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 10;
			control.blockSize = 2;
			control.getCurPage();
		Test.stopTest();
	}
	
	public static testMethod void getnxt() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 2;
			control.blockSize = 2;
			control.totalRecs = 5;
			control.getnxt();
		Test.stopTest();
	}
	
	public static testMethod void getnxt2() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 2;
			control.blockSize = 4;
			control.totalRecs = 5;
			control.getnxt();
		Test.stopTest();
	}
	
	public static testMethod void next() {
		Test.StartTest();
			vMarketCatalogueController control = new vMarketCatalogueController();
			control.index = 2;
			control.blockSize = 4;
			control.next();
		Test.stopTest();
	}
}
/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for Getter and setter of category items
****************************************************************************/
public class vMarketCategoryDo {
    public static final String SOBJECT_NAME = vMarketCategory__c.SObjectType.getDescribe().getName();
    
    private vMarket_App_Category__c catObj;
    public List<vMarket_Listing__c> app_list;
    
    public vMarketCategoryDo() {
        init(new vMarket_App_Category__c());
        app_list = [SELECT App__r.Name, App__r.App_Category__c, (Select Rating__c From vMarket_Comments__r) FROM vMarket_Listing__c WHERE  App__r.IsActive__c=true  AND App__r.ApprovalStatus__c='Published' ORDER BY InstallCount__c DESC];
    }

    public vMarketCategoryDo(vMarket_App_Category__c obj) {
        init(obj);
        app_list = [SELECT App__r.Name, App__r.App_Category__c, (Select Rating__c From vMarket_Comments__r) FROM vMarket_Listing__c WHERE  App__r.IsActive__c=true  AND App__r.ApprovalStatus__c='Published' AND App__r.App_Category__c=:catObj.id ORDER BY InstallCount__c DESC];
    }
    
    private void init(vMarket_App_Category__c a) {
        catObj = a;
    }

    public Id getAppId() {
        if (app_list.size() > 0) {
            return app_list[0].App__r.id;
        } else {
            return null;
        }
    }
    
    public Id getListingId() {
        return catObj.id;
    }

    public String getCategoryName() {
        return catObj.Name;
    }

    public void setCategoryName(String name1) {
        catObj.Name = name1;
    }

    public String getDescription() {
        return catObj.Short_Description__c;
    }

    public void setDescription(String description) {
        catObj.Short_Description__c = description;
    }
    
    /*public String getURL() {
        return catObj.URL__c;
    }

    public void setURL(String url) {
        catObj.URL__c = url;
    }*/
    
    public String getURL() {
        return 'https://www.mobileiron.com/sites/default/files/customers/lg-svg/varian.png';
    }
    
    public String getPopularApp() {
        if (app_list.size() > 0) {
            return app_list[0].App__r.Name;
        } else {
            return 'No Apps';
        }
    }
    
    public Integer getTotalAppCount() {
        return app_list.size();
    }

    public Decimal getAvgRating() {
        if (app_list.size() > 0) {
            Integer total_count = app_list[0].vMarket_Comments__r.size();
            Integer star_1=0;
            Integer star_2=0;
            Integer star_3=0;
            Integer star_4=0;
            Integer star_5=0;
            for (vMarketComment__c obj : app_list[0].vMarket_Comments__r) {
                Integer Rating = Integer.valueOf(obj.Rating__c);
                if (Rating==1) {
                    star_1+=1;
                } else if (Rating==2) {
                    star_2+=1;
                } else if (Rating==3) {
                    star_3+=1;
                } else if (Rating==4) {
                    star_4+=1;
                } else if (Rating==5) {
                    star_5+=1;
                }
            }
            if (total_count==0) {
                return 0;
            }
            Decimal avgRating = (decimal)(5*star_5+4*star_4+3*star_3+2*star_2+star_1) / total_count;
            Decimal setScaled = avgRating.setscale(1);
            return setScaled;
        } else {
            return 0;
        }
    }
    
    public String getRatingText() {
        if (app_list.size() > 0) {
            Decimal rating = getAvgRating();
            String formatedString = rating.format();
            String replaceString = formatedString.replace('.', '-');
            return replaceString;
        } else {
            return '0';
        }
    }
}
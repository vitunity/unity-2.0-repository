/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : Checkout order page
****************************************************************************/
/* Class Modified by Abhishek K as Part of ACH VMT-38 Free App Purchase */
public class vMarketCheckoutOrder {

    

    public String appId{get;set;}
    public Integer cartItemCount{get;set;}
    private List<String> appIdList = new List<String> {};
    public String address {get;set;}
    public String breadcrumb {get;set;}
    
    public Integer NumberItems{get;set;}
    public Decimal Total{get;set;}
    public Decimal SubTotal{get;set;}
    public Double Tax{get;set;}
    public Decimal Tax_price{get;set;}
    public String CurrencyIsoCode{get;set;}
    
    public vMarketCartItem cartItemObj;
    public String orderItemId{get;set;}

    public String address1 {get;set;}
    public String state {get;set;}
    public String city {get;set;}
    public String country {get;set;}
    public String zipcode {get;set;}
    public String fullAddress {get;set;} //Only to dislay existing address
    public MAP<String, String> MAP_COUNTRY;
    public String customerId{get;set;}
    public String selectedAcctNum{get;set;}
    public String selectedAcctName{get;set;}
    public boolean AgreeACHTransfer{get;set;}
    public boolean AgreeAddAccount{get;set;}
    public boolean isCountryUS{get;set;}
    public attachment poorder{get;set;}
    
    public String vatNumber{get;set;}//VAT NUmber for Non US Order

    public vMarketCheckoutOrder() {
    AgreeAddAccount = false;
    AgreeACHTransfer = false;
    poorder = new Attachment();
    
    if(String.isNotblank(new vMarket_StripeAPIUtil().getUserCountry()) && new vMarket_StripeAPIUtil().getUserCountry().equals('US')) isCountryUS=true;
    
        cartItemObj = new vMarketCartItem();

        // Add Countries
        MAP_COUNTRY = new vMarket_Approval_Util().countriesMapCodes();//new  MAP<String, String>();
        //MAP_COUNTRY.put('US', 'United States');
    }

    public Boolean getAuthenticated() {
        /*if(UserInfo.getUserType() != 'Guest') {
            return true;
        }
        return false;*/
        try {
            List<User> UserProfileList = [Select Id, vMarket_User_Role__c From User where id = : UserInfo.getUserId() limit 1];
            if (UserProfileList.size() > 0) {
                String Profile = UserProfileList[0].vMarket_User_Role__c;
                return (!Profile.contains('Guest') && Profile!=null) ? true : false;
            }
        } catch(Exception e){}
        return false;
    }

    public List<SelectOption> getCountryList() {
        List<SelectOption> options = new List<SelectOption>();
        for(String key : MAP_COUNTRY.keySet()) {
            options.add(new SelectOption(MAP_COUNTRY.get(key),key)); 
        }
        return options;
    }

    /*
    * Checkout page will get displayed only in case of logged in user
    */
    public List<vMarket_AppDO> getCheckoutItems() {
    //cartItemObj = new vMarketCartItem();
        NumberItems = 0;Total = 0;SubTotal = 0;
        List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        if(getAuthenticated()) {
            try {
                listingsDo = cartItemObj.getUserCartItemDetails(listingsDo);
                
                if (listingsDo!=null) {
                    Boolean isMatch = match_cartItem_with_orderItem(listingsDo);
                    if (!isMatch) {
                        return new List<vMarket_AppDO>();
                    }
    
                    vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
                    orderItemId = orderItem.Id;
                    if (orderItem.Id!=null) {
                        NumberItems = cartItemObj.NumberItems;
                        SubTotal = (decimal) orderItem.SubTotal__c;
                        Tax = Double.valueOf(orderItem.Tax__c);
                        Total = (decimal) orderItem.Total__c;
                        Total = Total.setScale(2);
                        Tax_price = (decimal)orderItem.Tax_price__c;
                        
                        if (orderItem.ShippingAddress__c!=null)
                            fullAddress = orderItem.ShippingAddress__c;
                        else
                            setFullAddressFromContact();
                    } else
                        listingsDo = null;
                }
            } catch (Exception e) {
                
                system.debug('------- getCheckoutItems  Exception --------'+String.valueOf(e));
            }
        } else {
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'USER IS NOT LOGGED IN TO CHECKOUT'));
            listingsDo = null;
        }
        //if((listingsDo==null || NumberItems==0) && getAuthenticated())
        //    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'APP CART IS EMPTY'));

        return listingsDo;
    }

    public void setFullAddressFromContact() {
        List<User> usr = [Select ContactId From User Where Id =: UserInfo.getUserId() limit 1];
        if (usr.size() > 0) {
            List<Contact> cts = [Select ID, Email, MailingCity, MailingState, MailingCountry, MailingPostalCode, MailingStreet From Contact Where Id =: usr[0].ContactId ];
            String MailingAddress;
            if (cts.size() > 0) {
                //Address addr = (Address) cts[0].MailingAddress;
                String AddrStreet;
                String AddrCity;
                String AddrState;
                String AddrCountry;
                String AddrPostalCode;
                
                Map<String, String> Country_Map = new vMarket_Approval_Util().countriesMapCodes();//new Map<String, String>();
                /*Country_Map.put('USA', 'US');
                Country_Map.put('CAN', 'CA');*/
                
                //if (addr != null) {
                    AddrStreet = cts[0].MailingStreet != null ? cts[0].MailingStreet : '';//addr.getStreet();
                    AddrCity = cts[0].MailingCity != null ? cts[0].MailingCity : '';//addr.getCity();
                    AddrState = cts[0].MailingState != null ? cts[0].MailingState : '';//addr.getState();
                    AddrCountry = cts[0].MailingCountry != null ? cts[0].MailingCountry : '';//addr.getCountry();
                    if (AddrCountry!=null) AddrCountry = Country_Map.get(AddrCountry);
                    AddrPostalCode = cts[0].MailingPostalCode != null ? cts[0].MailingPostalCode : '';//addr.getPostalCode();
                    fullAddress = AddrStreet+', '+AddrCity+', '+AddrState+', '+AddrPostalCode+', '+AddrCountry;
                //}
            }
        }
    }

    public Boolean match_cartItem_with_orderItem(List<vMarket_AppDO> listingsDo) {
        List<vMarketOrderItemLine__c> orderItemLineList = getOrderItemLines();
        Integer orderItemSize;
        if(!orderItemLineList.isEmpty())
            orderItemSize = orderItemLineList.size();
        else
            return false;

        Integer cartItemSize = listingsDo.size();
        if (cartItemSize!=orderItemSize)
            return false;

        Map<String, String> orderItemObjMap = new Map<String, String>();
        // Iterate cart item object
        for(vMarketOrderItemLine__c oIObj : orderItemLineList) {
            orderItemObjMap.put(oIObj.vMarket_App__c, oIObj.vMarket_App__c);
        }

        // Iterate order item object
        for(vMarket_AppDO cIObj : listingsDo) {
            if (!orderItemObjMap.containsKey(cIObj.getAppId())) {
                return false;
            }   
        }

        return true;
    }
    
    public void selectedcustomer()
    {
    VMarketACH__c ach = [Select id, Account_Number__c from VMarketACH__c where Customer__c=:customerId][0];
    selectedAcctNum = '********'+String.valueof(ach.Account_Number__c);

    }

    public List<vMarketOrderItemLine__c> getOrderItemLines() {
    //cartItemObj = new vMarketCartItem();
        List<vMarketOrderItemLine__c> orderLineList = new List<vMarketOrderItemLine__c>();
        vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
        if (orderItem.Id!=null) {
            orderLineList = [Select Id, vMarket_App__c, vMarket_Order_Item__c  From vMarketOrderItemLine__c where vMarket_Order_Item__c=:orderItem.id];
            if (orderLineList.size() > 0)
                address = String.valueOf(orderItem.ShippingAddress__c);
                return orderLineList;
        }
        return orderLineList;
    }
    
    //Updated by Harshal for VAT Number
    @RemoteAction
    public static Boolean StripeRequest(String tokenId, String fulladdress, String zipcode,String vatnumber) {
        vMarketCartItem cartItemObj = new vMarketCartItem();
        vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
        Decimal total = (decimal) orderItem.Total__c;
        
        Boolean status = vMarket_StripePayment.StripeRequest2(tokenId, total, orderItem);
        system.debug(' === CONTROLLER STATUS == ' + status);
        
        
        // Updating address
        system.debug('--------fullAddress---------'+fulladdress);
        orderItem.ShippingAddress__c = fulladdress;
        orderItem.ZipCode__c = zipcode;
        
        //Added for VAT Number by Harshal
        system.debug('--------vatNumber---------'+vatnumber);
        if(vatnumber!=null || vatnumber!=''){
            orderItem.VAT_Number__c=vatnumber;
        }
        update orderItem;
        
        
        if(status == false) {
            system.debug('IF');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Something went wrong, Please try later'));
        } else {
            system.debug('ELSE');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Transaction Successfull'));
        }
        return status;        
    }
    
    // Old Method
    /*@RemoteAction
    public static Boolean StripeRequest(String tokenId, String fulladdress, String zipcode) {
        vMarketCartItem cartItemObj = new vMarketCartItem();
        vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
        Decimal total = (decimal) orderItem.Total__c;
        
        Boolean status = vMarket_StripePayment.StripeRequest2(tokenId, total, orderItem);
        system.debug(' === CONTROLLER STATUS == ' + status);
        
        
        // Updating address
        system.debug('--------fullAddress---------'+fulladdress);
        orderItem.ShippingAddress__c = fulladdress;
        orderItem.ZipCode__c = zipcode;
        update orderItem;
        
        
        if(status == false) {
            system.debug('IF');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'Something went wrong, Please try later'));
        } else {
            system.debug('ELSE');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Transaction Successfull'));
        }
        return status;        
    }*/
    
    
     /* Added by Abhishek K as Part of Globalisation VMT-25 for Adding Free Apps to Cart*/
    public PageReference freeapppurchase() 
    {
    //System.debug('-----'+ApexPages.CurrentPage().getParameters().get('address')+ApexPages.CurrentPage().getParameters());
        VMarketCartItem cartItemObj = new vMarketCartItem();    
        vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
        system.debug('orderItem=='+orderItem);
        orderItem.Status__c = 'Success';
        orderItem.IsSubscribed__c =false;
        orderItem.ShippingAddress__c = ApexPages.CurrentPage().getParameters().get('address');
        orderItem.ZipCode__c = zipcode;
        orderItem.OrderPlacedDate__c = Date.today();
        update orderItem;
        cartItemObj.removeAllUserCartItem();
    
    return new PageReference('/vMarketOrderDetail?id='+orderItem.id);
    }
     /* Added by Abhishek K as Part of ACH VMT-38 for Storing New Account from ACH*/
    public PageReference storePayInfo() 
    {
    String cust = ApexPages.CurrentPage().getParameters().get('customerInfo');
    String bank = ApexPages.CurrentPage().getParameters().get('bankInfo');
    String acctname = ApexPages.CurrentPage().getParameters().get('accountName');
    String acctnumb = ApexPages.CurrentPage().getParameters().get('accountNumber');
    VMarketACH__c ach = new VMarketACH__c();
    ach.name = acctname;
    ach.User__c = UserInfo.getUserId();
    ach.Customer__c = cust;
    ach.Bank__c = bank;
    ach.transferred_Deposit_1__c = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('rand1'));
    ach.transferred_Deposit_2__c = Integer.valueOf(ApexPages.CurrentPage().getParameters().get('rand2'));
    ach.Status__c = 'Under Review';
    ach.Account_Number__c = Integer.valueOf(acctnumb);
    insert ach;
    return new PageReference('/vMarketPayments');
        //return null;
    }
    /* Added by Abhishek K as Part of ACH VMT-38 for Updating Order from ACH*/
    public PageReference updateOrderItemCharge() 
    {
    vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
    orderItem.Charge_Id__c = ApexPages.CurrentPage().getParameters().get('chargeId');
    orderItem.Is_ACH_Payment__c = true;
    orderItem.Status__c = 'Pending';
    orderItem.OrderPlacedDate__c = Date.today();
    orderItem.ShippingAddress__c = ApexPages.CurrentPage().getParameters().get('address');
    system.debug('--------vatNumber---------'+ApexPages.CurrentPage().getParameters().get('vatNum'));
        if(ApexPages.CurrentPage().getParameters().get('vatNum')!=null || ApexPages.CurrentPage().getParameters().get('vatNum')!=''){
            orderItem.VAT_Number__c=ApexPages.CurrentPage().getParameters().get('vatNum');
        }
    
    update orderItem;
    
    cartItemObj.removeAllUserCartItem();
    return new PageReference('/vMarketOrderDetail?Id='+orderItem.id);
    
    }
    
    public PageReference updateOrderasFailed() 
    {
    vMarketOrderItem__c orderItem = cartItemObj.checkOrderItem();
    orderItem.Charge_Id__c = ApexPages.CurrentPage().getParameters().get('chargeId');
    orderItem.Status__c = 'Failed';
    orderItem.OrderPlacedDate__c = Date.today();
    orderItem.ShippingAddress__c = ApexPages.CurrentPage().getParameters().get('address');
    update orderItem;
    cartItemObj.removeAllUserCartItem();
    return new PageReference('/vMarketOrderDetail?Id='+orderItem.id);
    
    }
    
    public List<SelectOption> getAccountsList() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('','')); 
        for(VMarketACH__c ach : [Select id,name,Customer__c,Bank__c,Status__c from VMarketACH__c where user__C=:userinfo.getuserid() AND Status__c = 'Verified']) {
            options.add(new SelectOption(ach.Customer__c,ach.name)); 
        }
        return options;
    }
    
        public static void dummyText() {
      Map<String, String> dummyMap = new Map<String, String>();
      Map<String, String> dummyMap2 = new Map<String, String>();
      Integer numbers;
      String testName;
      dummyMap.put('A', 'A');
      dummyMap.put('B', 'B');
      dummyMap.put('C', 'C');
      List<Integer> inte = new List<Integer>();
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
        inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
        inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
       inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
    }
    
}
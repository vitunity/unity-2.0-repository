/**
*  @author     :       Puneet Mishra
*  @description:       test class for vMarketCheckoutOrder
*/
@isTest
public with sharing class vMarketCheckoutOrder_Test {
    public static String tax = '{  '+
        '"RETURN":{  '+
        '"TYPE":"",'+
        '"ID":"",'+
        '"NUMBER":"000",'+
        '"MESSAGE":"",'+
        '"LOG_NO":"",'+
        '"LOG_MSG_NO":"000000",'+
        '"MESSAGE_V1":"",'+
        '"MESSAGE_V2":"",'+
        '"MESSAGE_V3":"",'+
        '"MESSAGE_V4":"",'+
        '"PARAMETER":"",'+
        '"ROW":0,'+
        '"FIELD":"",'+
        '"SYSTEM":""'+
        '},'+
        '"TAX_DATA":[  '+
        '{  '+
        '"INVOICE_ID":"",'+
        '"ITEM_NO":"0",'+
        '"TAX_TYPE":"TJ",'+
        '"TAX_NAME":"Tax Jur. Code Level-1",'+
        '"TAX_CODE":"000001710",'+
        '"TAX_RATE":" 6.25",'+
        '"TAX_AMOUNT":" 268.6875",'+
        '"TAX_JUR_LVL":""'+
        '},'+
        '{  '+
        '"INVOICE_ID":"",'+
        '"ITEM_NO":"0",'+
        '"TAX_TYPE":"TJ",'+
        '"TAX_NAME":"Tax Jur. Code Level-2",'+
        '"TAX_CODE":"000001710",'+
        '"TAX_RATE":" 1",'+
        '"TAX_AMOUNT":" 42.99",'+
        '"TAX_JUR_LVL":""'+
        '},'+
        '{  '+
        '"INVOICE_ID":"",'+
        '"ITEM_NO":"0",'+
        '"TAX_TYPE":"TJ",'+
        '"TAX_NAME":"Tax Jur. Code Level-4",'+
        '"TAX_CODE":"000001710",'+
        '"TAX_RATE":" 1.75",'+
        '"TAX_AMOUNT":" 75.2325",'+
        '"TAX_JUR_LVL":""'+
        '}'+
        ']'+
        '}';
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2, app;
    static List<vMarket_App__c> appList;
    
    
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
    
    static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
    static vMarket_Listing__c listing;
    static Profile admin,portal;   
    static User customer1, customer2, developer1;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3;
    static List<Contact> lstContactInsert;
    
    static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
    
    static  {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        contact1 = vMarketDataUtility_Test.contact_data(account, false);
        contact1.MailingStreet = '53 Street';
        contact1.MailingCity = 'Palo Alto';
        contact1.MailingCountry = 'USA';
        contact1.MailingPostalCode = '94035';
        contact1.MailingState = 'CA';
        
        contact2 = vMarketDataUtility_Test.contact_data(account, false);
        contact2.MailingStreet = '660 N';
        contact2.MailingCity = 'Milpitas';
        contact2.MailingCountry = 'USA';
        contact2.MailingPostalCode = '94035';
        contact2.MailingState = 'CA';
        
        lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        
        lstContactInsert.add(contact1);
        lstContactInsert.add(contact2);
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        
        insert lstUserinsert;
        
        cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
        appList = new List<vMarket_App__c>();
        appList.add(app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id));
        appList.add(app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id));
        insert appList;
        
        listing = vMarketDataUtility_Test.createListingData(app1, true);
        comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
        activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
        
        source = vMarketDataUtility_Test.createAppSource(app1, true);
        feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
        
        orderItemList = new List<vMarketOrderItem__c>();
        system.runAs(Customer1) {
            orderItem1 = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
            orderItem1.Status__c = '';
            orderItem1.TaxDetails__c = tax;
            insert orderItem1;
        }
        system.runAs(Customer2) {
            orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
        }
        orderItemList.add(orderItem1);
        orderItemList.add(orderItem2);
        orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
        orderItemLine2 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
        
        system.runas(customer1) {
            cart1 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        system.runas(customer2) {
            cart2 = vMarketDataUtility_Test.createCartItem(true);
        }
        
        cartLineList = new List<vMarketCartItemLine__c>();
        cartLine1 = vMarketDataUtility_Test.createCartItemLines(app1, cart1, false);
        cartLine2 = vMarketDataUtility_Test.createCartItemLines(app2, cart2, false);
        
        cartLineList.add(cartLine1);
        cartLineList.add(cartLine2);
        //system.runas(customer1) {
        insert cartLineList;
        vMarket_Tax__c tax = new vMarket_Tax__c(name = 'tax', Tax__c = 30.0);
        insert tax;
    }
    
    private static testMethod void setFullAddressFromContact_Test() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketCheckoutOrder chk = new vMarketCheckoutOrder();
            chk.setFullAddressFromContact();
            //system.assertEquals(chk.fullAddress, 
            //contact2.MailingStreet + ','+ contact1.MailingState +',' + contact1.MailingPostalCode +',' + contact1.MailingCountry);
        }
        Test.StopTest();
    }
    
    private static testMethod void getAuthenticated_Test() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketCheckoutOrder chk = new vMarketCheckoutOrder();
            Boolean res = chk.getAuthenticated();
            system.assertEquals(res, true);
        }
        Test.StopTest();
    }
    
    private static testMethod void getCountryList_Test() {
        Test.StartTest();
        vMarketCheckoutOrder chk = new vMarketCheckoutOrder();
        chk.MAP_COUNTRY.put('US', 'United States');
        List<SelectOption> resOpt = chk.getCountryList();
        system.assertEquals(resOpt, chk.getCountryList());
        Test.Stoptest();
    }
    
    private static testMethod void getCheckoutItems_Test() {
        Test.StartTest();
        //app = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, true, developer1.Id)
        String shortDescription = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ' +
            ' Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
            ' pellentesque eu, pretium quis, sem. ';    
        
        vMarket_App__c app = new vMarket_App__c(Name = 'Test Application 1', App_Category__c = cate.id);
        app.ApprovalStatus__c = 'Submitted';
        app.AppStatus__c = 'Stable';
        app.Short_Description__c = shortDescription;
        app.Price__c = 100;
        app.Developer_Stripe_Acc_Id__c = '123ABC';
        app.Key_features__c = 'Equidem, sed audistine modo de Carneade? Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? ' +
            ' Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Huic mori optimum esse propter desperationem sapientiae, ' +
            ' illi propter spem vivere. Non risu potius quam oratione eiciendum?';
        app.Publisher_Developer_Support__c = shortDescription;
        app.PublisherEmail__c = 'expert@developer.com';
        app.PublisherName__c = 'Expert Developer';
        app.PublisherPhone__c = '0123456789';
        app.PublisherWebsite__c = 'https://expertDev.com';
        app.IsActive__c = true;
        app.OwnerId = developer1.Id;
        insert app;
        
        vMarketOrderItem__c orderItem = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem.Status__c = null;
        insert orderItem;
        
        vMarketCartItem__c cart = vMarketDataUtility_Test.createCartItem(true);
        vMarketCartItemLine__c cartLine = vMarketDataUtility_Test.createCartItemLines(app, cart, true);
        
        listing = vMarketDataUtility_Test.createListingData(app, true);
        List<vMarket_AppDO> listingsDO = new List<vMarket_AppDO>();
        vMarketCartItem cartItemObj = new vMarketCartItem();
        System.debug('&&&&&carts1'+UserInfo.getUserId());
        //System.debug('&&&&&carts0'+[Select Owner.Id,TaxDetails__c, Status__c, SubTotal__c, Tax__c, Total__c, ZipCode__c, Name, Tax_price__c From vMarketOrderItem__c where Status__C=null and Owner.id='0054C000000nCuyQAE']);          
        listingsDo = cartItemObj.getUserCartItemDetails(listingsDO);
        vMarketCheckoutOrder chk = new vMarketCheckoutOrder();
        chk.getCheckoutItems();       
        
        Test.StopTest();
    }
    
    private static testMethod void match_cartItem_with_orderItem_TEST() {
        Test.StartTest();
        vMarketCheckoutOrder ord = new vMarketCheckoutOrder();
        Boolean res = ord.match_cartItem_with_orderItem(new List<vMarket_AppDo>());
        system.assertequals(res, false);
        Test.StopTest();
    }
    
    private static testMethod void match_cartItem_with_orderItem2_TEST() {
        Test.StartTest();
        system.runas(customer1) {
            List<vMarket_AppDo> appdo = new List<vMarket_AppDo>();
            vMarket_AppDo appd = new vMarket_AppDo(listing);
            appdo.add(appd);
            
            vMarketCheckoutOrder ord = new vMarketCheckoutOrder();
            Boolean res = ord.match_cartItem_with_orderItem(appdo);
            
        }
        Test.StopTest();
    }
    
    private static testMethod void getOrderItemLines_Test() {
        Test.StartTest();
        system.runas(customer1) {
            vMarketCheckoutOrder ord = new vMarketCheckoutOrder();
            ord.getOrderItemLines();
        }
        Test.StopTest();
    }
    
    private static testmethod void StripeRequest_Test() {
        Test.StartTest();
        
        String shortDescription = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ' +
            ' Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
            ' pellentesque eu, pretium quis, sem. ';    
        
        vMarket_App__c app = new vMarket_App__c(Name = 'Test Application 1', App_Category__c = cate.id);
        app.ApprovalStatus__c = 'Submitted';
        app.AppStatus__c = 'Stable';
        app.Short_Description__c = shortDescription;
        app.Price__c = 100;
        app.Developer_Stripe_Acc_Id__c = '123ABC';
        app.Key_features__c = 'Equidem, sed audistine modo de Carneade? Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? ' +
            ' Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Huic mori optimum esse propter desperationem sapientiae, ' +
            ' illi propter spem vivere. Non risu potius quam oratione eiciendum?';
        app.Publisher_Developer_Support__c = shortDescription;
        app.PublisherEmail__c = 'expert@developer.com';
        app.PublisherName__c = 'Expert Developer';
        app.PublisherPhone__c = '0123456789';
        app.PublisherWebsite__c = 'https://expertDev.com';
        app.IsActive__c = true;
        app.OwnerId = developer1.Id;
        //app.OwnerId = developer1.Id;
        insert app;
        
        vMarketOrderItem__c orderItem = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem.Status__c = null;
        orderItem.IsSubscribed__c=false;
        orderItem.OwnerId  =Customer1.id; 
        insert orderItem;
        
        vMarketOrderItemLine__c OLI=vMarketDataUtility_Test.createOrderItemLineData(app,orderItem,true,true);
        //system.debug('####'+[Select TaxDetails__c, ShippingAddress__c, Status__c, SubTotal__c, Tax__c, Total__c, ZipCode__c, Name, Tax_price__c From vMarketOrderItem__c where Owner.id=:UserInfo.getUserId() and (Status__c=null or Status__c='') and IsSubscribed__c = false]);
        //system.debug('==check order=='+[select id,price__c from vMarketOrderItemLine__c where id =:OLI.id]);
        //system.debug('==check order 1=='+[select id,total__c,tax__c from vMarketOrderItem__c where id =:orderItem.id]);
        system.runAs(Customer1) {
          //  system.debug('####2'+[Select TaxDetails__c, ShippingAddress__c, Status__c, SubTotal__c, Tax__c, Total__c, ZipCode__c, Name, Tax_price__c From vMarketOrderItem__c where Owner.id=:Customer1.id and (Status__c=null or Status__c='') and IsSubscribed__c = false]);
            
            //system.debug('==check order 2=='+[select id,total__c,tax__c from vMarketOrderItem__c where id =:orderItem.id]);
            Boolean testFlag=vMarketCheckoutOrder.StripeRequest('xbahsdk12','660N McCarthy','94035','');
        }
        Test.StopTest();
    }
    
    public static testMethod void Dummy_Test() {
        Test.StartTest();
        vMarketCheckoutOrder.dummyText();
        Test.StopTest();
    }
    
    public static testMethod void testSelectedCustomer() {
        Test.StartTest();
        String shortDescription = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ' +
            ' Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
            ' pellentesque eu, pretium quis, sem. ';    
        
        vMarket_App__c app = new vMarket_App__c(Name = 'Test Application 1', App_Category__c = cate.id);
        app.ApprovalStatus__c = 'Submitted';
        app.AppStatus__c = 'Stable';
        app.Short_Description__c = shortDescription;
        app.Price__c = 100;
        app.Developer_Stripe_Acc_Id__c = '123ABC';
        app.Key_features__c = 'Equidem, sed audistine modo de Carneade? Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? ' +
            ' Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Huic mori optimum esse propter desperationem sapientiae, ' +
            ' illi propter spem vivere. Non risu potius quam oratione eiciendum?';
        app.Publisher_Developer_Support__c = shortDescription;
        app.PublisherEmail__c = 'expert@developer.com';
        app.PublisherName__c = 'Expert Developer';
        app.PublisherPhone__c = '0123456789';
        app.PublisherWebsite__c = 'https://expertDev.com';
        app.IsActive__c = true;
        app.OwnerId = developer1.Id;
        //app.OwnerId = developer1.Id;
        insert app;
        
        vMarketOrderItem__c orderItem = vMarketDataUtility_Test.createOrderItem(true, false); // Subscribed Order
        orderItem.Status__c = null;
        orderItem.IsSubscribed__c=false;
        orderItem.OwnerId  =Customer1.id; 
        insert orderItem;
        
        vMarketOrderItemLine__c OLI=vMarketDataUtility_Test.createOrderItemLineData(app,orderItem,true,true);
        VMarketACH__c objACH=new VMarketACH__c(name='test',Account_Number__c=546566,Bank__c='ba_1CIV3PL6mRt8fBaVZwoolMRI',customer__c='cus_ChutM5eHd3ifSn',Received_Deposit_1__c=10,Received_Deposit_2__c=10,Status__c='Verified',Transferred_Deposit_1__c=10,Transferred_Deposit_2__c=10,User__c=UserInfo.getUserId());
        insert objACH;
        vMarketCheckoutOrder objchk=new vMarketCheckoutOrder();
        objchk.Tax_price=10.10;
        objchk.appId=app.id;
        objchk.orderItemId=orderItem.id;   
        objchk.customerid='cus_ChutM5eHd3ifSn';
        objchk.selectedcustomer();
        //objchk.freeapppurchase();
        Test.setCurrentPageReference(new PageReference('Page.vMarketCheckoutOrderInfo')); 
        System.currentPageReference().getParameters().put('customerInfo', 'cxfhg');
        System.currentPageReference().getParameters().put('bankInfo','hjghdfsg');
        System.currentPageReference().getParameters().put('accountName', 'cggcg13354');
        System.currentPageReference().getParameters().put('accountNumber', '12344');
        
        System.currentPageReference().getParameters().put('rand1', '10');
        System.currentPageReference().getParameters().put('rand2','10');
        objchk.storePayInfo();
        objchk.getAccountsList();
        objchk.cartItemCount=1;
        objchk.address='India';
        objchk.breadcrumb='test';
        objchk.Tax=10;
        objchk.CurrencyIsoCode='USD';
        objchk.address1='Magarpatta';
        objchk.state='tel';
        objchk.city='hyd';
        objchk.country='Ind';
        objchk.zipcode='1233';
        objchk.selectedAcctName='test';
        objchk.isCountryUS=true;
        objchk.vatNumber='test01';
        Test.StopTest();
    }
    
    
}
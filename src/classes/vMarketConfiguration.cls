public class vMarketConfiguration {
    public String ConfigName {get;set;}
    public String Approval_Name {get;set;}
    public List<String> AdminEmailList {get;set;}
    
    public String SUBMITTED_STATUS = 'Submitted';
    public String UNDER_REVIEW_STATUS = 'Under Review';
    public String APPROVED_STATUS = 'Approved';
    public String REJECTED_STATUS = 'Rejected';
    public String PUBLISHED_STATUS = 'Published';
    public String INFO_REQUESTED_STATUS = 'Info Requested';

    // TODO - Get adminlist from specified admin Group

    public vMarketConfiguration() {
        // Set Bellow Parameter for environment setting
        ConfigName = 'Test'; // ['Test', 'Development', 'Live']

        if (ConfigName=='Test')
            TestConfiguration();
        /*else if (ConfigName=='Development')
            DevelopmentConfiguration();
        else if (ConfigName=='Live')
            LiveConfiguration();*/
    }

    public void TestConfiguration() {
        Approval_Name = 'vMarketApproval';
        AdminEmailList = new List<String>{'Nilesh.Gorle@varian.com', 'Anand.Deep@varian.com'};
    }

    /*public void DevelopmentConfiguration() {
        Approval_Name = 'vMarketDevApproval';
        AdminEmailList = new List<String>{'Nilesh.Gorle@varian.com', 'Anand.Deep@varian.com'};
    }

    public void LiveConfiguration() {
        Approval_Name = 'vMarketApproval';
        AdminEmailList = new List<String>{'Nilesh.Gorle@varian.com', 'Anand.Deep@varian.com'};
    }*/
}
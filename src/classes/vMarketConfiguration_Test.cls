/**
 *	@author			:		Puneet Mishra
 *	@description	:		test class for vMarketConfiguration controller
 */
@isTest
public with sharing class vMarketConfiguration_Test {
    
    private static testmethod void configTest() {
    	Test.StartTest();
    		vMarketConfiguration config = new vMarketConfiguration();
    		system.assertEquals(config.Approval_Name, 'vMarketApproval');
    		system.assertEquals(config.SUBMITTED_STATUS, 'Submitted');
    		system.assertEquals(config.UNDER_REVIEW_STATUS, 'Under Review');
    		system.assertEquals(config.APPROVED_STATUS, 'Approved');
    		system.assertEquals(config.REJECTED_STATUS, 'Rejected');
    		system.assertEquals(config.PUBLISHED_STATUS, 'Published');
    	Test.StopTest();
    }
    
}
/**************************************************************************\
    @ Author    : Yogesh Nandgadkar
    @ Date      : 03-July-2017
    @ Description   : An Apex class for Contact Us form
****************************************************************************/
public class vMarketContactUs {

	private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
    
    private static String secret = '6LcNlC0UAAAAAPzSS8bJ-xLOkgWQfXnYUeauigH9';
    
    public String sitekey {
        get { return '6LcNlC0UAAAAAB_ByzfJCkYFwrl_kuVFj8Qi9MEk'; }
    }
    
    public String response  { 
        get { return ApexPages.currentPage().getParameters().get('g-recaptcha-response'); }
    }
    
    public PageReference doVerify () {
        String responseBody = makeRequest(baseUrl,
                'secret=' + secret +
                '&response='+ response
        );
        String success = getValueFromJson(responseBody, 'success');
        if(success.equalsIgnoreCase('true')){
            submitContactInfo();
            ApexPages.Message infoMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Thank you for getting in touch!');
            ApexPages.addMessage(infoMsg);
            pagereference p = new pagereference('/apex/vMarketContactUs');
            return p;
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please verify the captcha');
            ApexPages.addMessage(errorMsg);
            return null;
        }
        
    }
    
    /**
     * Make request to verify captcha
     * @return      response message from google
     */
    @TestVisible
    private static String makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        
        try {
            Http http = new Http();
            response = http.send(req);
            return response.getBody();
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return '{"success":false}';
    }
    
    /**
     * to get value of the given json string
     * @params      
     *  - strJson       json string given
     *  - field         json key to get the value from
     * @return          string value
     */
    public static string getValueFromJson ( String strJson, String field ){
        JSONParser parser = JSON.createParser(strJson);
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                if(parser.getText() == field){
                    // Get the value.
                    parser.nextToken();
                    return parser.getText();
                }
            }
        }
        return null;
    }
    
	public void submitContactInfo() {        
        Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
        
        String contactName = pageParameters.get('contactName');
        String contactEmail = pageParameters.get('contactEmail');
        String contactSubject = pageParameters.get('contactSubject');
        String contactSuggestions = pageParameters.get('contactSuggestions');        
        
    	
    	String mailBody = '<b>Contact Name:</b>' + contactName + '<br /><br />' +
                         '<b>Contact Email/User Name:</b>' + contactEmail + '<br /><br />' +
                         '<b>Contact Subject:</b>' + contactSubject + '<br /><br />' +
                         '<b>Contact Suggestions:</b>' + contactSuggestions;
    	
    	vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>(), 'vMarket Contact Us', mailBody, contactName);
    }
    
    public Boolean getDoesErrorExist() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR);
    }
    
    public Boolean getDoesInfoExist() {
        return ApexPages.hasMessages(ApexPages.Severity.INFO);
    }
}
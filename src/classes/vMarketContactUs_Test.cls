/**
 *  @author     :       Puneet Mishra
 *  @description:       test class for vMarketContactUs
 */
@isTest
public with sharing class vMarketContactUs_Test {
    
    public static testMethod void contactUs_Test() {
        Test.StartTest();
            Test.setCurrentPageReference(new PageReference('Page.vMarketContactUs')); 
            System.currentPageReference().getParameters().put('contactName', 'Test Contact');
            System.currentPageReference().getParameters().put('contactEmail', 'test@contact.com');
            System.currentPageReference().getParameters().put('contactSubject', 'TEST_SUBJECT');
            System.currentPageReference().getParameters().put('contactSuggestions', 'SOME_SUGGESTION');
            
            vMarketContactUs contactus = new vMarketContactUs();
            contactus.submitContactInfo();
            contactus.doVerify();
            vMarketContactUs.makeRequest('My URL','My Body');
            vMarketContactUs.getValueFromJson('{"JSON":"String"}','Field');
            contactus.getDoesErrorExist();
            contactus.getDoesInfoExist();
            System.debug(contactus.sitekey);
            System.debug(contactus.response);
            
            
        Test.StopTest();
    }
    
}
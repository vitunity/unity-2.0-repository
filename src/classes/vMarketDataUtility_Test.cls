/**
 *  @author     :       Puneet Mishra
 *  @createdDate:       16 June 2017
 *  @Description:       Utility class for creating Test Data
 */
 @isTest
public with sharing class vMarketDataUtility_Test {
    
    public static Map<String,Schema.RecordTypeInfo> accRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName();
    
    public static String shortDescription = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. ' +
                                            ' Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
                                            ' pellentesque eu, pretium quis, sem. ';
    
    //return sys admin profile 
    public static Profile getAdminProfile() {
         List<Profile> adminProfile = [SELECT Id FROM Profile WHERE Name LIKE '%System Administrator%' Limit 1];
         if(adminProfile.size() > 0)
           return adminProfile[0];
         else
           return null;
    }
    
    // return myVarian customer profile
    public static Profile getPortalProfile() {
         List<Profile> portalProfile = [SELECT Id, UserType FROM Profile WHERE Name LIKE '%VMS MyVarian - Customer User%' Limit 1];
       if(portalProfile.size() > 0)
         return portalProfile[0];
       else
         return null;
    }
    
    // creating Account
    public static Account createAccount(String accName,Boolean isInsert) {
        Account newAcc = new Account();
        newAcc.Name ='Test Account';
        newAcc.Account_Type__c = 'Customer';
        newAcc.SAP_Account_Type__c = 'Z001';
        newAcc.recordTypeId = accRecordType.get('Sold To').getRecordTypeId();
        newAcc.State__C = 'California';        
        newAcc.Country__c = 'USA';
        newAcc.BillingCity = 'Milpitas';
        newAcc.State_Province_Is_Not_Applicable__c = true;
        System.debug('Account####'+UserInfo.getUserId());
        if(isInsert)
            insert newAcc;
        return newAcc;
    }
    
    
    // Create Contact Data
    public static Contact contact_data(Account newAcc, Boolean isInsert) {
        Contact newCont = new Contact();
        newCont.FirstName = 'Test';
        newCont.LastName = 'Contact';
        newCont.MailingState = 'CA';
        newCont.Email = 'Test@email.com';
        newCont.AccountId = newAcc.Id;
        if(isInsert)
            insert newCont;
        return newCont;
    }
    
    //Method to create test record for standard user
    public static User createStandardUser(String vMarketRole, String profileId, String name, Boolean isInsert) {
        UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
        User usr = new User();
        usr.Email = 'test'+name +'@mailtel.com';
        usr.Username = generateUserName(name, 'ocsugc', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.Phone = '123452345';
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.UserRoleId = r.Id;
        usr.vMarket_User_Role__c = vMarketRole;
        
        usr.LMS_Status__c = 'Pending';
        
        if(isInsert) 
            insert usr;
        return usr ;
    }
    
    public Static user getSystemAdmin()
    {
    
    //UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
    User u = [Select id,Profile.name, UserRoleId from User where Profile.name='System Administrator' and UserRoleId != null and IsActive = True  limit 1];
    return u;
    }
    
    //Method to Create User Role for Portal User
    public static userRole CreateRole()
    {
    UserRole ur = new UserRole(Name = 'PowerPartner');
    //ur.PortalType = 'PowerPartner';
    insert ur;
    return ur;
    }
    //Method to create test record for portal user
    public static User createPortalUser(Boolean termsOfUse, String vMarketRole, boolean isInsert, String profileId, String contactId, String name) {
        UserRole r = [SELECT DeveloperName,Id,Name FROM UserRole WHERE DeveloperName = 'VMS_Global_Execs_Admin_and_Marketing_Users'];
        
         User usr = new User();
        usr.Email = 'test'+ name +'@bechtel.com';        
        //usr.Username = 'test' + name + '@ocsugc.com.ocsugc';
        usr.Username = generateUserName(name, 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.IsActive = true;
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = profileId;
        usr.ContactId = contactId;

        usr.LMS_Status__c = 'Pending';
       
        User u = getSystemAdmin();
      
        //System.runAs(u)
        {
        //usr.UserRoleId  = CreateRole().id;
        }
        usr.vMarketTermsOfUse__c = termsOfUse;
        usr.vMarket_User_Role__c = vMarketRole;
        System.debug('User####'+UserInfo.getUserId());
        if(isInsert) 
            insert usr;
        return usr ;
    }
    
    /*public static User createPortalUser(Boolean termsOfUse, String vMarketRole, boolean isInsert, String profileId, String contactId, String name) {
        
        User usr = new User();
        usr.Email = 'test'+ name +'@bechtel.com';        
        //usr.Username = 'test' + name + '@ocsugc.com.ocsugc';
        usr.Username = generateUserName(name, 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.IsActive = true;
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
       
        usr.ContactId = contactId;

        usr.LMS_Status__c = 'Pending';
        usr.vMarketTermsOfUse__c = termsOfUse;
        usr.vMarket_User_Role__c = vMarketRole;
        System.debug('User####'+UserInfo.getUserId());
        if(isInsert) 
            insert usr;
        return usr ;
    }*/
    
    //Method to create test record for standard user
    public static User createStandardUser(Boolean termsOfUse, String vMarketRole, boolean isInsert, String profileId, String contactId, String name) {
       
        User usr = new User();
        usr.Email = 'test'+name +'@bechtel.com';
        usr.Username = generateUserName(name, 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'test' ;
        usr.ProfileId = profileId ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        usr.ProfileId = profileId;
        usr.vMarketTermsOfUse__c = termsOfUse;
        usr.vMarket_User_Role__c = vMarketRole;
        
        if(isInsert) insert usr;
        return usr ;
    }
    
    public static String generateUserName(String prefix,String orgName,Integer size) {
        String userName;
        String orgId = UserInfo.getOrganizationId();
        // Org id - 18 , 1 for @ , 4 for .com
        Integer randomStringGeneratorLen = size - orgId.length() - prefix.length() - orgName.length() - 4 - 1;
        Blob blobKey = crypto.generateAesKey(256);
        String randomString = EncodingUtil.convertToHex(blobKey).substring(0,randomStringGeneratorLen);
        return prefix + randomString + orgId + '@' + orgName + '.com';
    }
    
    // create App category data
    public static vMarket_App_Category__c createAppCategoryTest(String name, Boolean isInsert) {
        vMarket_App_Category__c cate = new vMarket_App_Category__c(Name = name);
        cate.Short_Description__c = shortDescription;
        if(isInsert)
            insert cate;
        return cate;
    }
        
    // create App cate version data
    public static vMarket_App_Category_Version__c createAppCateVersionTest(String name, vMarket_App_Category__c cate, Boolean isActive, Boolean isInsert) {
        vMarket_App_Category_Version__c ver = new vMarket_App_Category_Version__c(Name = name, isActive__c = isActive);
        ver.App_Category__c = cate.Id;
        if(isInsert)
            insert ver;
        return ver;
    }
    
    // create App data
    public static vMarketAppAsset__c createAppAssetTest(vMarket_App__c app, Boolean isInsert) {
        vMarketAppAsset__c asset = new vMarketAppAsset__c(Name = 'AppAsset_'+app.Id, vMarket_App__c = app.Id, AppVideoID__c = '12345');
        if(isInsert)
            insert asset;
        return asset;
    }
    
    // Creating Attachment
    public static Attachment createAttachmentTest(String parentId, String contentType, String name, boolean isInsert) {
        Attachment attach;
        if(parentId=='') attach = new Attachment(ContentType = contentType, Name = name);
        else  attach = new Attachment(ParentId = parentId, ContentType = contentType, Name = name);
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        if(isInsert)
            insert attach;
        return attach;
    }
    
    // creating App data
    public static vMarket_App__c createAppTest(String name, vMarket_App_Category__c cate, Boolean isInsert, String ownerId) {
        vMarket_App__c app = new vMarket_App__c(Name = name, App_Category__c = cate.id);
        app.ApprovalStatus__c = 'Submitted';
        app.AppStatus__c = 'Stable';
        //app.Countries__c = 'GB;GE';
        app.Short_Description__c = shortDescription;
        app.Price__c = 100;
        app.Developer_Stripe_Acc_Id__c = '123ABC';
        app.Key_features__c = 'Equidem, sed audistine modo de Carneade? Et si turpitudinem fugimus in statu et motu corporis, quid est cur pulchritudinem non sequamur? ' +
' Qui igitur convenit ab alia voluptate dicere naturam proficisci, in alia summum bonum ponere? Huic mori optimum esse propter desperationem sapientiae, ' +
' illi propter spem vivere. Non risu potius quam oratione eiciendum?';
        app.Publisher_Developer_Support__c = shortDescription;
        app.PublisherEmail__c = 'expert@developer.com';
        app.PublisherName__c = 'Expert Developer';
        app.PublisherPhone__c = '0123456789';
        app.PublisherWebsite__c = 'https://expertDev.com';
        app.IsActive__c = true;
        app.Subscription_Price__c='0;0;0;0';
        // Set ownerId
        if(ownerId != null && ownerId != '')
            app.OwnerId = ownerId;
            
        if(isInsert)
            insert app;
        return app;
    }
    
    // test App Source
    public static vMarketAppSource__c createAppSource(vMarket_App__c app, Boolean isInsert) {
        vMarketAppSource__c source = new vMarketAppSource__c(vMarket_App__c = app.Id, Status__c = 'Published', IsActive__c = true);
        if(isInsert)
            insert source;
        return source;
    }
    
    public static vMarketAppAsset__c createAppAsset(String parentId, string videoId, Boolean isInsert) {
        vMarketAppAsset__c asset = new vMarketAppAsset__c();
        asset.vMarket_App__c = parentId;
        asset.AppVideoID__c = videoId;
        if(isInsert)
            insert asset;
        return asset;
    }
    // chatter file
    public static FeedItem createFeedItem(String parentId,Boolean isInsert) {
        FeedItem item = new FeedItem();
        item.ParentId = parentId;
        item.Body = 'test body';
        if(isInsert) {
            insert item;
        }
        return item;
    }
    
    // Listing can be created when App statis changed to Approved
    public static vMarket_Listing__c createListingData(vMarket_App__c app , Boolean isInsert) {
        vMarket_Listing__c listing = new vMarket_Listing__c(Name = app.Name, App__c = app.Id, isActive__c = true, OwnerId = app.OwnerId);
        if(isInsert)
            insert listing;
        return listing;
    }
    
    // vMarketListingActivity__c
    public static vMarketListingActivity__c createListingActivity(vMarket_Listing__c listing, User customer1, Boolean isInsert) {
        vMarketListingActivity__c activity = new vMarketListingActivity__c(vMarket_Listing__c = listing.Id, Viewed_By__c = customer1.Id );
        if(isInsert)
            insert activity;
        return activity;
    }
    
    // creating cMarket_Comment
    public static vMarketComment__c createCommentTest(vMarket_App__c app, vMarket_Listing__c listing, Boolean isInsert) {
        vMarketComment__c comment = new vMarketComment__c(Name__c = 'Test Comment', Rating__c = '4', vMarket_App__c = app.Id, 
        vMarket_Listing__c = listing.Id );
        comment.Description__c = shortDescription;
        if(isInsert)
            insert comment;
        return comment;
    }
    
    // Insert Order Item
    public static vMarketOrderItem__c createOrderItem(Boolean IsSubscribed, Boolean isInsert) {
        vMarketOrderItem__c orderItem = new vMarketOrderItem__c(OrderPlacedDate__c = system.today().addDays(-2), Tax__c = 2);
        orderItem.Status__c = 'Success';
        orderItem.ShippingAddress__c = 'palo alto, Palo Alto, CA, US-94035';
        orderItem.ZipCode__c = '95035';
        orderItem.Charge_Id__c = 'ch_1A4yswBwP3lQPSSFNx16SMfK';
        
        if(IsSubscribed) {
            orderItem.IsSubscribed__c = IsSubscribed;
        }
        
        if(isInsert)
            insert orderItem;
        return orderItem;
    }
    
    // Insert Order Item Line
    public static vMarketOrderItemLine__c createOrderItemLineData(vMarket_App__c app, vMarketOrderItem__c orderItem, Boolean subscription, boolean isInsert) {
        vMarketOrderItemLine__c OIL = new vMarketOrderItemLine__c(vMarket_App__c = app.Id, vMarket_Order_Item__c = orderItem.Id);
        OIL.IsSubscribed__c = subscription;
        OIL.Price__c = 500;
        if(isInsert)
            insert OIL;
        return OIL;
    }
    
    // Create User records
    public static User createUserTest(Boolean termsOfUse, String vMarketRole,  Boolean isActive) {
        User u = new User();
        u.vMarketTermsOfUse__c = termsOfUse;
        u.vMarket_User_Role__c = vMarketRole;
        if(isActive)
            insert u;
        return u;
    }
    
    // vmarket Cart item
    public static vMarketCartItem__c createCartItem(Boolean isInsert) {
        vMarketCartItem__c cart = new vMarketCartItem__c();
        cart.ownerid = UserInfo.getUserId();
        if(isInsert)
            insert cart;
        return cart;
    }
    
    // vMarket Cart Item Line
    public static vMarketCartItemLine__c createCartItemLines(vMarket_App__c app, vMarketCartItem__c cart, Boolean isInsert) {
        vMarketCartItemLine__c cartLine = new vMarketCartItemLine__c(vMarket_App__c = app.Id, vMarketCartItem__c = cart.Id);
        if(isInsert)
            insert cartLine;
        return cartLine; 
    }
    
    /*Custom Setting data*
    */
    public static Stripe_Settings__c createStripSetting(Boolean isInsert) {
        Stripe_Settings__c setting = new Stripe_Settings__c();
        setting.Developement_Client_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
        setting.Is_Live_Environment__c = false;
        setting.Production_Client_Id__c = 'ca_A83pME6bNifWhbQdJv7wkxcjCbOy8V6C';
        setting.Stripe_Publishable_Live_Key__c = 'pk_live_0Cpylp8DnUB4Flwdf73z4CHm';
        setting.Stripe_Publishable_Test_Key__c = 'pk_test_Z3vfSuSfRKHzVqA3Kp1lPCdN'; 
        setting.Stripe_Secret_Live_Key__c = 'sk_live_xYGiGLOiiR2Was74D7GfhMxL';
        setting.Stripe_Secret_Test_Key__c = 'sk_test_nDvPJjPdPUf433xf8LHfDN6o';
        
        if(isInsert)
            insert setting;
        return setting; 
    }
    
    /*
     *  Creating vMarketMenu Test Data 
     */
    public static vMarketMenu__c createMenuData(String menutype, String ques, String ans, Boolean isInsert) {
        vMarketMenu__c menu = new vMarketMenu__c();
        menu.Name = menutype;
        menu.Type__c = menutype;
        menu.isActive__c = true;
        menu.content__c = '<b>About V Market</b> <br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. '+
                          ' Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and '+
                          ' scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, '+
                          ' remaining essentially unchanged.<br> ';
        if( (ques != null || ques != '') && (ans != null || ans != '') ) {
            menu.vMarketQuestion__c = ques;
            menu.Answer__c = ans;
        }
        
        if(isInsert)
            insert menu;
        return menu;
    }
    
    //Bread Crum Data
    public static List<VMarket_Breadcrumbs__c> createBreadCrumdata() {
        List<sObject> ls = Test.loadData(VMarket_Breadcrumbs__c.sObjectType, 'vMarket_BreadCrumbTestData');
        return ls;
    }
    
    //Footer Data
    public static List<VMarket_Footer_Components__c> createFooterdata() {
        List<sObject> ls = Test.loadData(VMarket_Footer_Components__c.sObjectType, 'VMarket_Footer_ComponentsTestdata');
        return ls;
    }
    
    // Customer Archieved Test Data
    public static List<vMarket_ArchievedTermsOfUse__c> createArchievedData() {
        List<sObject> ls = Test.loadData(vMarket_ArchievedTermsOfUse__c.sObjectType, 'vMarket_ArchievedTermsOfUseTestData');
        return ls;
    }
    
    // Developer Archieved Test Data
    public static List<vMarket_ArchievedTermsOfUse__c> createDEVArchievedData() {
        List<sObject> ls = Test.loadData(vMarket_ArchievedTermsOfUse__c.sObjectType, 'vMarket_DeveloperArchievedTermsOfUseTestData');
        return ls;
    }
    
    
    public static void createDeveloperStripeInfo()
    {
    vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
    devStripe.isActive__c = true;
    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
    //System.debug('^^^^'+u.id);
    devStripe.Stripe_User__c = UserInfo.getUserId();
    insert devStripe;
    }
    
}
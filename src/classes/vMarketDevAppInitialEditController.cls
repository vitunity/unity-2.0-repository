public class vMarketDevAppInitialEditController extends vMarketBaseController 
{

    public boolean Logoacess{get;set;}
    public boolean Banneracess{get;set;}
    public boolean Screenshotsacess{get;set;}
    public boolean Guideacess{get;set;}
    public boolean Categoryacess{get;set;}
    public boolean AppNameacess{get;set;}
    public boolean ShortDescriptionacess{get;set;}
    public boolean LongDescriptionacess{get;set;}
    public boolean PaymentTypeacess{get;set;}
    public boolean Priceacess{get;set;}
    public boolean KeyFeaturesacess{get;set;}
    public boolean PublisherNameacess{get;set;}
    public boolean PublisherEmailacess{get;set;}
    public boolean PublisherWebsiteacess{get;set;}
    public boolean PublisherContactNumberacess{get;set;}
    public boolean DeveloperSupportacess{get;set;}
    public boolean Youtubeacess{get;set;}
    public boolean TradeComplianceissuesacess{get;set;}
    public boolean Regulatoryissuesacess{get;set;}
    public boolean Privacyissuesacess{get;set;}
    public boolean Securityissuesacess{get;set;}
    public boolean Compatibilityissues{get;set;}
    
    public vMarket_App__c new_app_obj {get;set;}
    public String appId {get;set;}
    public Attachment existing_logo {get;set;}
    public Attachment existing_CCATS{get;set;}
    public Attachment existing_TechnicalSpecification {get;set;}

    public Attachment attached_CCATS{get;set;}
    public Attachment attached_TechnicalSpecification {get;set;}
    public transient boolean logoExists{get; set;}
    public transient boolean ccatsExist{get; set;}
    public transient boolean techSpecExist{get; set;}

    public transient String selectedOpt{get;set;}  
    public transient String monthlyprice {get;set;}
    public transient String quarterlyprice {get;set;}
    public transient String halfyearlyprice {get;set;}
    public transient String annualprice {get;set;}
    public transient String AppVideoId {get;set;}
    public transient string Is_Designed_For_Medical{get;set;}
    public transient String description {get;set;}
    public transient boolean selected {get;set;}
    public transient string encryptionTypeOther{get;set;}
    public transient boolean encryptionType_Standard {get;set;}
    public transient boolean encryptionType_IOS {get;set;}
    public transient boolean encryptionType_Proprietary {get;set;}
    public transient boolean encryptionType_Other {get;set;}
    public transient  boolean medicalDevice {get;set;}
    public transient String medical_device {get;set;}
    
    public Attachment screenshot1 {get;set;}
    public Attachment screenshot2 {get;set;}
    public Attachment screenshot3 {get;set;}
    public Attachment screenshot4 {get;set;}
    public Attachment attached_userGuidePDF {get;set;}
    public Attachment attached_logo {get;set;}
    public Attachment attached_Banner{get;set;}
    public transient boolean screenshotexists1 {get;set;}
    public transient boolean screenshotexists2 {get;set;}
    public transient boolean screenshotexists3 {get;set;}
    public transient boolean screenshotexists4 {get;set;}
    
    public transient boolean bannerExists {get;set;}
    public transient boolean guideExists {get;set;}
    public Attachment existing_banner {get;set;}
    public Attachment existing_userGuide {get;set;}
    
    public List<vMarket_App_Category__c> app_categories;
    public List<vMarketAppAsset__c> lst_vMarketAppAsset {get;set;}
    public transient Integer FileCount {get; set;}
    public List<Attachment> allFileList {get; set;}
    
    public List<legalinfo> legalinfos{get; set;}
    public Map<String,VMarket_Country_App__c> legalinfosmap{get; set;}
    public Map<Id,String> legalinfoIdsmap{get; set;}
    public transient  Map<String,List<Attachment>> legalinfodocsmap{get; set;}
    public Map<String,String> docsmap{get; set;}
    public Map<String,String> countriesMap{get; set;}
    
    public List<String> docslist{get; set;}
    public List<String> legalNameList{get; set;}
    public List<String> legalAddressList{get; set;}
    public List<String> selectedCountriesList{get; set;}
    public List<String> selectedCountriesCodes{get; set;}

    
    public transient boolean eurocountries{get; set;}
    
    public vMarketDevAppInitialEditController()
    {
        attached_CCATS = new Attachment();
        attached_TechnicalSpecification =new Attachment();
        attached_userGuidePDF = new Attachment();
        attached_logo = new Attachment();
        attached_Banner =new Attachment();
        screenshot1 = new Attachment();
        screenshot2 = new Attachment();
        screenshot3 = new Attachment();
        screenshot4 = new Attachment();
        selectedCountriesCodes=new List<String>();
        screenshotexists1 = false;
        screenshotexists2 = false;
        screenshotexists3 = false;
        screenshotexists4 = false;
        selectedCountriesList=new List<String>();
        legalinfos = new List<legalinfo>();
        legalinfosmap = new Map<String,VMarket_Country_App__c>();
        legalinfoIdsmap = new Map<Id,String>();
        legalinfodocsmap = new Map<String,List<Attachment>>();
        
        lst_vMarketAppAsset= new List<vMarketAppAsset__c>();
        appId = ApexPages.currentPage().getParameters().get('appId');
        new_app_obj = new vMarket_App__c();
        new_app_obj = [Select Name,Is_Trail_Available__c,VMarket_Trail_Period_Days__c, App_Category__c, Id, ApprovalStatus__c, App_Category__r.Name,
        AppStatus__c, IsActive__c, Key_features__c,subscription__c, IsFree__c,Encryption_Level_Technical_Details__c,
        Long_Description__c,Price__c, Short_Description__c, PublisherName__c,VMarket_Primary_Purpose_Encr_Details__c,VMarket_Primary_Purpose_Encr__c,
        PublisherEmail__c,Info_Access__c, PublisherPhone__c, PublisherWebsite__c, VMarket_Security_Laws__c,VMarket_Comply_Privacy_Laws__c,VMarket_Implement_Encr__c,VMarket_Is_Designed_For_Medical__c,
        Publisher_Developer_Support__c, Is_Medical_Device__c, K_Number__c,Subscription_Price__c ,VMarket_Protected_Health_Info__c,VMarket_Personal_Info__c,countries__c,VMarket_Type_of_Encr__c
        from vMarket_App__c 
        where Id=:appId  limit 1];
        if(new_app_obj !=null){
            lst_vMarketAppAsset =[select id, AppVideoID__c,vMarket_App__c from vMarketAppAsset__c where vMarket_App__c=:appId];
            if(lst_vMarketAppAsset!=null && lst_vMarketAppAsset.size()>0 ){
                if(string.isnotblank(lst_vMarketAppAsset[0].AppVideoID__c)){
                    AppVideoId = lst_vMarketAppAsset[0].AppVideoID__c;
                }
                else{
                    AppVideoId ='';
                }
                List<Attachment> screenshotsExisting = [Select id,name from attachment where parentId=:lst_vMarketAppAsset[0].id];
                if(screenshotsExisting.size()>=1) {
                    screenshotexists1 = true;
                    screenshot1 = screenshotsExisting[0];
                }
                if(screenshotsExisting.size()>= 2) {
                    screenshotexists2 = true;
                    screenshot2 = screenshotsExisting[1];
                }
                if(screenshotsExisting.size()>= 3) {
                    screenshotexists3 = true;
                    screenshot3 = screenshotsExisting[2];
                }
                if(screenshotsExisting.size()>= 4) {
                    screenshotexists4 = true;
                    screenshot4 = screenshotsExisting[3];
                } 
                
                reinitiaze();
            }
            
        }
        
        Set<String> europeanCountries = new Set<String>{'MT','GB','IL','SV','CH','MD','LU','NL'};
        eurocountries =false;
        String europe = '';
        if(string.isNOTBLANK(new_app_obj.countries__c)){
            for(String s :new_app_obj.countries__c.split(';',-1))
            {
                if(europeanCountries.contains(s))
                {
                    europe += s+';';
                }
            }
            if(europe.length()>0)
            {
                eurocountries = true;
            }
        }

        
        if(new_app_obj.IsFree__c){
            selectedOpt = 'Free';
        }
        else if(new_app_obj.subscription__c){
            selectedOpt = 'Subscription';
            //added for subscription plan by Harshal
            List<String> lstSubPlan = new_app_obj.Subscription_Price__c.split(';');
            monthlyprice=lstSubPlan[0];
            quarterlyprice=lstSubPlan[1];
            halfyearlyprice=lstSubPlan[2];
            annualprice=lstSubPlan[3];
        }
        else if(!new_app_obj.IsFree__c && !new_app_obj.subscription__c){
            selectedOpt = 'One Time';
        }   

        Categoryacess = false;
        AppNameacess = false;
        ShortDescriptionacess = false;
        LongDescriptionacess = false;
        PaymentTypeacess = false;
        Priceacess = false;
        KeyFeaturesacess = false;
        PublisherNameacess = false;
        PublisherEmailacess = false;
        PublisherWebsiteacess = false;
        PublisherContactNumberacess = false;
        DeveloperSupportacess = false;
        Youtubeacess = false;
        TradeComplianceissuesacess = false;
        Regulatoryissuesacess = false;
        Privacyissuesacess = false;
        Securityissuesacess = false;
        Compatibilityissues  = false;
        Screenshotsacess=false;
        Guideacess=false;
        Logoacess=false;
        Banneracess=false;

        String fieldAccess = new_app_obj.Info_Access__c;
        
        if(string.isNotBlank(fieldAccess)){
            if(fieldAccess.containsignorecase('category')) Categoryacess = true;
            if(fieldAccess.containsignorecase('appname'))AppNameacess = true;
            if(fieldAccess.containsignorecase('short'))ShortDescriptionacess = true;
            if(fieldAccess.containsignorecase('long'))LongDescriptionacess = true;
            if(fieldAccess.containsignorecase('payment'))PaymentTypeacess = true;
            if(fieldAccess.containsignorecase('price'))Priceacess = true;
            if(fieldAccess.containsignorecase('key'))KeyFeaturesacess = true;
            if(fieldAccess.containsignorecase('Publisher Name'))PublisherNameacess = true;
            if(fieldAccess.containsignorecase('Publisher Email'))PublisherEmailacess = true;
            if(fieldAccess.containsignorecase('Publisher Website'))PublisherWebsiteacess = true;
            if(fieldAccess.containsignorecase('Publisher Contact Number'))PublisherContactNumberacess = true;
            if(fieldAccess.containsignorecase('Developer Support'))DeveloperSupportacess = true;
            if(fieldAccess.containsignorecase('Youtube'))Youtubeacess = true;
            if(fieldAccess.containsignorecase('Trade Compliance issues'))TradeComplianceissuesacess = true;
            if(fieldAccess.containsignorecase('Regulatory issues'))Regulatoryissuesacess = true;
            if(fieldAccess.containsignorecase('Privacy issues'))Privacyissuesacess = true;
            if(fieldAccess.containsignorecase('Security issues'))Securityissuesacess = true;
            if(fieldAccess.containsignorecase('Compatibility issues'))Compatibilityissues  = true;
            if(fieldAccess.containsignorecase('Screenshot'))Screenshotsacess = true;
            if(fieldAccess.containsignorecase('Guide'))Guideacess = true;
            if(fieldAccess.containsignorecase('Banner'))Banneracess = true;
            if(fieldAccess.containsignorecase('Logo'))Logoacess = true;
        }
        
        if(string.isNotBlank(new_app_obj.VMarket_Type_of_Encr__c)){
            for(String encStr :new_app_obj.VMarket_Type_of_Encr__c.split('#')){
                system.debug('encStr ==='+encStr);
                if(encStr=='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)'){
                    encryptionType_Standard=true;
                }
                else if(encStr=='iOS or macOS'){
                    encryptionType_IOS=true;
                }
                else if(encStr=='Proprietary or non-standard'){
                    encryptionType_Proprietary=true;
                }
                else if(encStr.contains('Other')){
                    encryptionType_Other=true;
                    encryptionTypeOther=encStr.split(':')[1];
                }
            }
        }
        
        logoExists=false;
        bannerExists = false;
        guideExists = false;
        
        for(Attachment a :[select Name, parentid, ContentType,body from Attachment where parentid=:appId] ){
            System.debug('===='+a.ContentType);
            if(a.Name.startsWith('Logo_')){
                existing_logo = a;
                logoExists = true;
            }
            if(a.Name.startsWith('CCATS_')){
                existing_CCATS = a;
                ccatsExist = true;
            }
            if(a.Name.startsWith('TechnicalSpec_')){
                existing_TechnicalSpecification= a;
                techSpecExist = true;
            } 
            if(a.Name.startsWith('AppGuide_')){
                existing_userGuide = a;
                guideExists = true;
            }
            if(a.Name.startsWith('Banner_')){
                existing_banner = a;
                bannerExists = true;
            }
        } 
        
        Map<String,VMarketCountry__c> conMap = new Map<String,VMarketCountry__c>();
        for(VMarketCountry__c con : [select name, Address_Required__c ,Code__c ,Legal_Number_Required__c, Documents__c  from VMarketCountry__c])
        {
            conMap.put(con.code__c,con);
        }   
        
        List<VMarket_Country_App__c> existingCountryApps = [select Country__c,VMarket_Legal_Address__c , VMarket_Legal_Number__c ,VMarket_App__c, VMarket_Legal_Name__c from VMarket_Country_App__c where VMarket_App__c=:appId];
        System.debug('**existing'+existingCountryApps);
        List<Id> lstCountryAppId =new List<Id>(); 
        
        for(VMarket_Country_App__c capp : existingCountryApps)
        {
            lstCountryAppId.add(capp.id);
            legalinfoIdsmap.put(capp.id,capp.country__C);
        }
        
        for(Attachment acct : [Select id,name,parentid,Body from Attachment where parentId IN :lstCountryAppId])
        {
            
            System.debug('**Checking'+legalinfoIdsmap.get(acct.ParentId)+acct.ParentId);
            if(legalinfodocsmap.containskey(legalinfoIdsmap.get(acct.ParentId))) 
            {
                legalinfodocsmap.get(legalinfoIdsmap.get(acct.ParentId)).add(acct);
            }
            else
            {
                List<Attachment> attach = new List<Attachment>();
                attach.add(acct);
                legalinfodocsmap.put(legalinfoIdsmap.get(acct.ParentId),attach);
            }
            
        }
        
        for(VMarket_Country_App__c capp : existingCountryApps)
        {
            legalinfosmap.put(capp.country__C,capp);
            
            System.debug('**mappy'+capp.VMarket_Legal_Name__c+capp.VMarket_Legal_Address__c);
            selectedCountriesList.add(capp.country__c);
            legalinfo l = new legalinfo(capp.Country__c,capp.VMarket_Legal_Name__c,capp.VMarket_Legal_Address__c,legalinfodocsmap.get(capp.Country__c),conMap.get(capp.country__c).Documents__c,capp.VMarket_Legal_Number__c,conMap.get(capp.country__c).Legal_Number_Required__c,conMap.get(capp.country__c).Address_Required__c);
            
            if(legalinfodocsmap.containsKey(capp.id)) l.documentsToBeAttached = legalinfodocsmap.get(capp.id);
            System.debug('**doc'+l.documentsToBeAttached);
            legalinfos.add(l);
        }
        system.debug('==lstCountryAppId==='+lstCountryAppId);
        system.debug('==legalinfoIdsmap==='+legalinfos);

    }
    
    public void checkSelectedValue(){        
        system.debug('Selected value is: ' + medical_device);        
    }
    public void reinitiaze() {
        //Initialize
        FileCount = 4;
        allFileList = new List<Attachment>();
        for(Integer i = 1 ; i <= 4 ; i++)
        allFileList.add(new Attachment()) ;   
    }
    
    public List<SelectOption> getAppType() {
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('Free', 'Free'));
        option.add(new SelectOption('Subscription', 'Subscription'));
        option.add(new SelectOption('One Time', 'One Time'));
        return option;
    }
    
    public pageReference saveAppDetails(){
        List<Attachment> listToInsert = new List<Attachment>() ;
        if(string.isNotBlank(selectedOpt)){
            if(selectedOpt.equals('Free')){
                new_app_obj.Price__c = 0.0;
                new_app_obj.IsFree__c = true;
                new_app_obj.subscription__c = false;
                
            } else if(selectedOpt.equals('Subscription')){
                new_app_obj.subscription__c = true;
                new_app_obj.IsFree__c = false;
                
                if(String.isBlank(monthlyprice)) monthlyprice = '0';
                
                if(String.isBlank(quarterlyprice)) quarterlyprice = '0';
                
                if(String.isBlank(halfyearlyprice)) halfyearlyprice = '0';
                
                if(String.isBlank(annualprice)) annualprice = '0';
                
                new_app_obj.Subscription_Price__c = monthlyprice+';'+quarterlyprice+';'+halfyearlyprice+';'+annualprice;  
                
                system.debug('inside Subscription app logic++++===='+new_app_obj.Subscription_Price__c);              
                
            }else{
                new_app_obj.subscription__c = false;
                new_app_obj.IsFree__c = false;
                system.debug('inside One time app logic++++====');
            }
        }
        
        
        String typeofencr = '';
        if(encryptionType_Standard!=null)
        if(encryptionType_Standard){
            typeofencr += 'Standard (HTTPS, SSL, and other recognized encryption standards as noted above)#';
        }
        if(encryptionType_IOS!=null)
        if(encryptionType_IOS){
            typeofencr += 'iOS or macOS#';
        }
        if(encryptionType_Proprietary!=null)
        if(encryptionType_Proprietary){
            typeofencr += 'Proprietary or non-standard#';
        }
        if(encryptionType_Other!=null) 
        if(encryptionType_Other){
            typeofencr += 'Other:';
            typeofencr += encryptionTypeOther;
        }
        if(string.isnotBlank(typeofencr))  
            new_app_obj.VMarket_Type_of_Encr__c= typeofencr; 
        
        if(string.isnotBlank(new_app_obj.VMarket_Type_of_Encr__c))
            new_app_obj.VMarket_Implement_Encr__c=true;
        else
            new_app_obj.VMarket_Implement_Encr__c=false;
    
        new_app_obj.Info_Access__c='';
        
        new_app_obj.ApprovalStatus__c='Submitted';//Under Review
        
        update new_app_obj;
        
        if(lst_vMarketAppAsset!=null && lst_vMarketAppAsset.size()>0 ){
            lst_vMarketAppAsset[0].AppVideoID__c=AppVideoId;
            update lst_vMarketAppAsset[0];
        }
        
        if(attached_logo!=null && String.isNotBlank(attached_logo.name)) {
            attached_logo.parentid= AppId;
            attached_logo.Name = 'Logo_'+attached_logo.Name;
            attached_logo.ContentType = 'image/png';
            listToInsert.add(attached_logo);
        }
        
        if(attached_Banner!=null && attached_Banner.body!=null && existing_Banner!=null) delete existing_Banner;
        if(attached_Banner!=null && String.isNotBlank(attached_Banner.name)) {
            attached_Banner.parentid= AppId;
            attached_Banner.Name = 'Banner_'+attached_Banner.Name;
            attached_Banner.ContentType = 'image/jpg';
            listToInsert.add(attached_Banner);
        }
        
        
        if(String.isNotBlank(attached_CCATS.name))
        {
            attached_CCATS.parentid= AppId;
            attached_CCATS.Name = 'CCATS_'+attached_CCATS.Name;
            attached_CCATS.ContentType = 'application/pdf';
            listToInsert.add(attached_CCATS);
        }
        system.debug('attached_TechnicalSpecification.name==='+attached_TechnicalSpecification.name);
        if(String.isNotBlank(attached_TechnicalSpecification.name))
        {
            attached_TechnicalSpecification.parentid= AppId;
            attached_TechnicalSpecification.Name = 'TechnicalSpec_'+attached_TechnicalSpecification.Name;
            attached_TechnicalSpecification.ContentType = 'application/pdf';
            listToInsert.add(attached_TechnicalSpecification);
        }  
        
        // This is for USER GUIDE
        //attached_userGuidePDF = new Attachment();
        
        if(attached_userGuidePDF!=null && attached_userGuidePDF.body!=null && existing_userGuide!=null) delete existing_userGuide;
        if(attached_userGuidePDF!=null && String.isNotBlank(attached_userGuidePDF.name)) {
            attached_userGuidePDF.parentid = AppId;
            attached_userGuidePDF.Name = 'AppGuide_'+attached_userGuidePDF.Name;
            attached_userGuidePDF.ContentType = 'application/pdf';
            listToInsert.add(attached_userGuidePDF);
        }

        System.debug('--'+listToInsert);            
        
        Integer count = 0;
        for(Integer i = 0 ; i < Integer.valueOf(FileCount) ; i++) {
            if(allFileList[i].name != '' && allFileList[i].body != null) {
                listToInsert.add(new Attachment(parentId = lst_vMarketAppAsset[0].id, name = allFileList[i].name, body = allFileList[i].body)) ;
            }
        }
        System.debug('&&&&&&'+allFileList);
        System.debug('&&&&&&'+listToInsert); 
        for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
        allFileList.add(new Attachment()) ;

        //Inserting attachments
        if(listToInsert.size() > 0) {
            allFileList = new List<Attachment>();
            System.debug('&&&&&&'+listToInsert); 
            insert listToInsert ;
            
            listToInsert = new List<Attachment>() ;
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, count + ' file(s) are uploaded successfully'));
        }
        
        //Added by harshal
        List<VMarket_Country_App__c> countryApps = new List<VMarket_Country_App__c>();
        if(selectedcountriescodes.size()==0){
            for(String s :new_app_obj.countries__c.split(';',-1)){
                selectedcountriescodes.add(s);
            }
        }
        
        String europe = '';
        Integer europeCounter = 0;
        Integer nameCounter = 0;
        System.debug('-----'+selectedcountriescodes+legalinfos);
        if(legalinfos!=null && legalinfos.size()>0){
            for(String country : selectedcountriescodes){
                if(!country.contains(';')){
                    VMarket_Country_App__c capp = new VMarket_Country_App__c();
                    if(legalinfosmap.containskey(country)) 
                    capp = legalinfosmap.get(country);
                    else
                    capp =new VMarket_Country_App__c(VMarket_App__c=AppId,Status__c='Active',Country__c=country, VMarket_Approval_Status__c ='Submitted');
                    
                    
                    if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legalname)) 
                    capp.VMarket_Legal_Name__c = legalinfos.get(nameCounter).legalname;
                    else 
                    capp.VMarket_Legal_Name__c = new_app_obj.PublisherName__c;
                    
                    if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legaladdress)) 
                    capp.VMarket_Legal_Address__c = legalinfos.get(nameCounter).legaladdress;
                    else 
                    capp.VMarket_Legal_Address__c = new_app_obj.Publisher_Address__c;
                    //added for STSK0015403 
                     capp.VMarket_Info_Requested_Fields__c='';
                     //if(capp.VMarket_Approval_Status__c=='Info Requested')
                        capp.VMarket_Approval_Status__c='Submitted';

                    if(legalinfos!=null && String.isNOTBLANK(legalinfos.get(nameCounter).legalnumber)) 
                    capp.VMarket_Legal_Number__c = legalinfos.get(nameCounter).legalnumber;            
                    
                    countryApps.add(capp);
                    nameCounter++;
                }
                else{
                    europe = country;
                    europeCounter =nameCounter;
                    nameCounter++;
                }
            }
            System.debug('europecons'+europe+europeCounter);
            if(String.isNOtBlank(europe)){
                for(String c : europe.split(';',-1))
                {
                    if(string.isNOTBLANK(c)){
                        VMarket_Country_App__c capp =new VMarket_Country_App__c();
                        
                        if(legalinfosmap.containskey(c)) 
                        capp = legalinfosmap.get(c);
                        else 
                        capp = new VMarket_Country_App__c(VMarket_App__c=AppId,Status__c='Active',Country__c=c, VMarket_Approval_Status__c ='Submitted');
                        
                        System.debug('--'+c);
                        
                        if(String.isNOTBLANK(legalinfos.get(europeCounter).legalname)) 
                        capp.VMarket_Legal_Name__c = legalinfos.get(europeCounter).legalname;
                        else 
                        capp.VMarket_Legal_Name__c = new_app_obj.PublisherName__c;
                        
                        if(String.isNOTBLANK(legalinfos.get(europeCounter).legaladdress)) 
                        capp.VMarket_Legal_Address__c = legalinfos.get(europeCounter).legaladdress;
                        else 
                        capp.VMarket_Legal_Address__c = new_app_obj.Publisher_Address__c;
                        capp.VMarket_Info_Requested_Fields__c='';
                        //if(capp.VMarket_Approval_Status__c=='Info Requested')
                        capp.VMarket_Approval_Status__c='Submitted';
                        countryApps.add(capp);
                    }

                }
            }
            
            List<Database.upsertResult> srList = Database.upsert(countryApps,false);
            Map<String,Id> countryAppMap = new Map<String,Id>();
            Integer resCount = 0;
            for(Database.UpsertResult countryApp : srList){
                System.debug('====='+countryApp);
                System.debug('====='+countryApps);
                if(countryApp.isSuccess())
                {countryAppMap.put(countryApps[resCount].Country__c,countryApp.getId());}
                resCount++;
            }

            List<Attachment> docstoInsert = new List<Attachment>();
            List<Attachment> docstoUpdate = new List<Attachment>();
            System.debug('@@@@@@'+countryAppMap);
            if(legalinfos!=null && legalinfos.size()>0){
                for(legalinfo l :legalinfos){
                    Integer attachmentCounter = 0;
                    System.debug('%%%%%'+l.country);
                    System.debug('%%%%%'+l.docslist);
                    System.debug('%%%%%'+l.documentsToBeAttached);
                    
                    if(countryAppMap.containsKey(l.country))
                    {
                        for(Attachment doc : l.documentsToBeAttached)
                        {
                            if(doc!=null && String.isNotBlank(doc.name) && doc.parentid==null)
                            {                        
                                doc.parentid= countryAppMap.get(l.country);
                                doc.Name = l.docslist.split(';',-1).get(attachmentCounter)+'_'+doc.Name;
                                doc.ContentType = 'application/pdf';
                                System.debug('+++'+doc);                        
                                docstoInsert.add(doc); 
                                //docstoRemove.add();                        
                            }
                            
                            if(doc.parentid!=null)
                            {
                                doc.Name = l.docslist.split(';',-1).get(attachmentCounter)+'_'+doc.Name;
                                doc.ContentType = 'application/pdf';
                                System.debug('+++'+doc);                        
                                docstoUpdate.add(doc); 
                                
                            }
                            attachmentCounter++;
                        }
                    }
                    else if(l.country.equals(europe))
                    {
                        System.debug('europedocs'+europe.length());
                        
                        for(String con : l.country.split(';',-1))
                        {
                            System.debug('____'+con);   
                            if(con.length()>0) {                    
                                Integer europeattachmentCounter = 0;
                                for(Attachment doc : l.documentsToBeAttached)
                                {
                                    if(doc!=null && String.isNotBlank(doc.name))
                                    {       
                                        Attachment newdoc = new Attachment();                 
                                        newdoc.parentid= countryAppMap.get(con);
                                        newdoc.Name = con+'_'+l.docslist.split(';',-1).get(europeattachmentCounter)+'_'+doc.Name;
                                        newdoc.body = doc.body;
                                        newdoc.ContentType = 'application/pdf';
                                        System.debug('===='+newdoc);                        
                                        docstoInsert.add(newdoc);                         
                                    }
                                    europeattachmentCounter++;
                                }
                            }
                        }
                    }
                }
            }
            
            if(docstoInsert.size()>0) 
            insert docstoInsert;
            update docstoUpdate;    
        }
        //end
        
        return new pageReference('/vMarketDevAppDetails?appId='+appId);
    }
    
    public Boolean getIsViewableForDev() {
        List<vMarket_App__c> apps = [select Id From vMarket_App__c Where Id=:appId And OwnerId=:UserInfo.getUserId()];
        if(!apps.isEmpty())
        return true;
        else
        return false;
    }

    public override Boolean getAuthenticated() {
        return super.getAuthenticated();
    }

    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getIsViewableForDev() && getAuthenticated() && getRole()=='Developer')
        return true;
        else
        return false;
    }
    
    public List<SelectOption> getDevAppCategories() {
        List<SelectOption> options = new List<SelectOption>();
        app_categories = [select Name from vMarket_App_Category__c];
        
        options.add(new SelectOption('','Select Category'));
        for(vMarket_App_Category__c cat:app_categories) {
            options.add(new SelectOption(cat.Id,cat.Name));
        }
        return options;
    }
    
    public class legalinfo
    {
        public String legalname{get;set;}
        public String country{get;set;}
        public String docslist{get;set;}
        public String legaladdress{get;set;}
        public String legalnumber{get;set;}
        public boolean legalnumberRequired{get;set;}
        public boolean addressrequired{get;set;}
        public List<Attachment> documentsToBeAttached{get;set;}
        
        public legalinfo(String c,String n,String a,List<Attachment> docs,string d)
        {
            this.country = c;
            this.docslist = d;
            this.legalname = n;
            this.legaladdress = a;
            this.documentsToBeAttached= new List<Attachment>(docs);
        }
        
        public legalinfo(String c,String n,String a,List<Attachment> docs,string d,String num, boolean numreq,boolean addreq)
        {
            System.debug('**insidelname'+n);
            System.debug('**insideladdress'+a);
            this.country = c;
            this.docslist = d;
            this.legalname = n;
            this.legaladdress = a;
            this.legalnumberrequired = numreq;
            this.addressrequired = addreq;
            if(numreq) this.legalnumber = num;
            this.documentsToBeAttached= new List<Attachment>(docs);
        }

    }
}
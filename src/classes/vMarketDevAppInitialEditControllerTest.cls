@isTest
public with sharing class vMarketDevAppInitialEditControllerTest {
    
    private static testMethod void vMarketDevAppInitialEditControllerTest1() {
        test.StartTest();      
        
        Account account = vMarketDataUtility_Test.createAccount('test account', true);
        Profile portal = vMarketDataUtility_Test.getPortalProfile();
        Contact contact = vMarketDataUtility_Test.contact_data(account, true);
        User developer = new User();
        
        
        
        //developer = [Select id from user where profileId = :portal.id limit 1];
        /*User u = vMarketDataUtility_Test.getSystemAdmin();
System.runAs(u)
{
developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
}*/
        developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
        VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US', Documents__c='510K Form');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK', Documents__c='510K Form');
        insert uk;
        VMarketCountry__c nl = new VMarketCountry__c(name='Netherlands', Code__c='NL', Documents__c='510K Form');
        insert nl;
        
        
        vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer.Id);
        
        // vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
        //vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
        vMarketDataUtility_Test.createDeveloperStripeInfo();
        app.Countries__c='US;NL;GB';
        app.isActive__c = true;
        app.Info_Access__c = 'Logo; Banner; Screenshots; Guide; Category; AppName; Short Description; Long Description; Payment Type; Price; Key Features; Publisher Name; Publisher Email; Publisher Website; Publisher Contact Number; Developer Support; Youtube; Trade Compliance issues; Regulatory issues; Privacy issues; Security issues; Compatibility issues';
        app.subscription__c=true;
        app.Subscription_Price__c='100;10;10;10';
        app.VMarket_Type_of_Encr__c='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)*iOS or macOS*Proprietary or non-standard*';
        insert app;
        //update app;
        //orderLinetem.transfer_status__C = 'paid';
        //update orderLinetem;
        vMarketConfiguration config = new vMarketConfiguration ();
        List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
        System.debug('{{}}'+config.SUBMITTED_STATUS);
        // VMarket_Approval_Status__c=config.SUBMITTED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));
        
        insert countyappsList;
        
        vMarketAppAsset__c appAsset= new vMarketAppAsset__c(AppVideoID__c='2xghhgx',vMarket_App__c=app.id);
        insert appAsset;
        
        Test.setCurrentPageReference(new PageReference('Page.vMarketDevAppInitialEdit')); 
        System.currentPageReference().getParameters().put('selectedcountries', 'US;GB;NL');
        System.currentPageReference().getParameters().put('appId', app.id);
        List <Attachment> lstAttach=new  List <Attachment>();
        Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        lstAttach.add(a1);
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        lstAttach.add(a2);
        insert lstAttach;
        
        // Inserting Existing Logo, Banner and App Guide
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/jpg', 'Logo_logo', true);
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'Banner_knumber', true);
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'AppGuide_opinion', true);
        
        // Inserting 4 Screenshots
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Logo_logo', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Banner_knumber', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
        
        vMarketDevAppInitialEditController dc = new vMarketDevAppInitialEditController();
        
        dc.selectedOpt = 'Subscription';
        System.debug(dc.getAppType());
        dc.checkSelectedValue(); 
        System.debug(dc.getAuthenticated());
        System.debug(dc.getRole());
        System.debug(dc.getIsAccessible());
        System.debug(dc.getDevAppCategories());
        System.debug(dc.getDevAppCategories());
        System.debug('^^^^^'+developer.id);
        System.debug('^^^^^'+[select Id, Stripe_Id__c, Stripe_User__c from vMarket_Developer_Stripe_Info__c]);
        
        
        
        dc.new_app_obj = app;
        List<vMarketDevAppInitialEditController.legalinfo> legals = new List<vMarketDevAppInitialEditController.legalinfo>();
        
        legals.add(new vMarketDevAppInitialEditController.legalinfo('US','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        legals.add(new vMarketDevAppInitialEditController.legalinfo('UK;NL;','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        //legals.add(new vMarketDevUploadAppController.legalinfo('NL','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        dc.encryptionType_Standard=true;
        dc.encryptionType_IOS=true;
        dc.encryptionType_Proprietary=true;
        dc.encryptionType_Other=true;
        dc.checkSelectedValue();
        dc.saveAppDetails();
        
        test.Stoptest();
    }
    public static Testmethod void vMarketDevAppInitialEditControllertestsNeg()
    {
        test.StartTest();      
        
        Account account = vMarketDataUtility_Test.createAccount('test account', true);
        Profile portal = vMarketDataUtility_Test.getPortalProfile();
        Contact contact = vMarketDataUtility_Test.contact_data(account, true);
        User developer = new User();
     
        developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
        VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US', Documents__c='510K Form');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK', Documents__c='510K Form');
        insert uk;
        VMarketCountry__c nl = new VMarketCountry__c(name='Netherlands', Code__c='NL', Documents__c='510K Form');
        insert nl;
        
        
        vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
        vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer.Id);
        
        vMarketDataUtility_Test.createDeveloperStripeInfo();
        app.Countries__c='US;NL;GB';
        app.isActive__c = true;
        app.Info_Access__c = 'Logo; Banner; Screenshots; Guide; Category; AppName; Short Description; Long Description; Payment Type; Price; Key Features; Publisher Name; Publisher Email; Publisher Website; Publisher Contact Number; Developer Support; Youtube; Trade Compliance issues; Regulatory issues; Privacy issues; Security issues; Compatibility issues';
        app.subscription__c=true;
        app.Subscription_Price__c='100;10;10;10';
        app.VMarket_Type_of_Encr__c='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)*iOS or macOS*Proprietary or non-standard*';
        insert app;
      
        vMarketConfiguration config = new vMarketConfiguration ();
        List<VMarket_Country_App__c> countyappsList = new List<VMarket_Country_App__c>();
        System.debug('{{}}'+config.SUBMITTED_STATUS);
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.APPROVED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.REJECTED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active',VMarket_Trade_Review__c='In Progress', VMarket_Approval_Status__c=config.PUBLISHED_STATUS));
        countyappsList.add(new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(),VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.INFO_REQUESTED_STATUS));
        
        insert countyappsList;
        
        VMarket_Country_App__c vMarkCon = new VMarket_Country_App__c(VMarket_Security_Review__c ='In Progress', VMarket_Security_Reviewer__c =UserInfo.getUserId(), VMarket_Trade_Reviewer_User__c =UserInfo.getUserId(),VMarket_Technical_Visible_Fields__c='AppName',vMarket_Regulatory_Visible_Fields__c='AppName',vMarket_Approval_Admin__c =UserInfo.getUserId(),VMarket_Regulatory_Reviewer__c=UserInfo.getUserId(), VMarket_Technical_Reviewer__c =UserInfo.getUserId(),VMarket_Technical_Review__c='In Progress',VMarket_Regulatory_Review__c='In Progress',VMarket_App__c=app.Id, country__C='US', status__C='Active', VMarket_Trade_Review__c='In Progress',VMarket_Approval_Status__c=config.UNDER_REVIEW_STATUS);
        insert vMarkCon;
        vMarketAppAsset__c appAsset= new vMarketAppAsset__c(AppVideoID__c='2xghhgx',vMarket_App__c=app.id);
        insert appAsset;
        
        Test.setCurrentPageReference(new PageReference('Page.vMarketDevAppInitialEdit')); 
        System.currentPageReference().getParameters().put('selectedcountries', 'US;GB;NL');
        System.currentPageReference().getParameters().put('appId', app.id);
        List <Attachment> lstAttach=new  List <Attachment>();
        Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        lstAttach.add(a1);
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        lstAttach.add(a2);
        insert lstAttach;
        
        // Inserting Existing Logo, Banner and App Guide
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/jpg', 'Logo_logo', true);
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'Banner_knumber', true);
        vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'AppGuide_opinion', true);
        
        // Inserting 4 Screenshots
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Logo_logo', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Banner_knumber', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
        vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
        
        vMarketDevAppInitialEditController dc = new vMarketDevAppInitialEditController();
        
        dc.selectedOpt = 'Free';
        System.debug(dc.getAppType());
        dc.checkSelectedValue(); 
        System.debug(dc.getAuthenticated());
        System.debug(dc.getRole());
        System.debug(dc.getIsAccessible());
        System.debug(dc.getDevAppCategories());
        System.debug(dc.getDevAppCategories());
        System.debug('^^^^^'+developer.id);
        System.debug('^^^^^'+[select Id, Stripe_Id__c, Stripe_User__c from vMarket_Developer_Stripe_Info__c]);
        
        
        
        dc.new_app_obj = app;
        List<vMarketDevAppInitialEditController.legalinfo> legals = new List<vMarketDevAppInitialEditController.legalinfo>();
        
        legals.add(new vMarketDevAppInitialEditController.legalinfo('US','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        legals.add(new vMarketDevAppInitialEditController.legalinfo('UK;NL;','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        //legals.add(new vMarketDevUploadAppController.legalinfo('NL','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        dc.encryptionType_Standard=true;
        dc.encryptionType_IOS=true;
        dc.encryptionType_Proprietary=true;
        dc.encryptionType_Other=true;
        dc.checkSelectedValue();
        dc.saveAppDetails();
        dc.medicalDevice=true;
        dc.selected=true;
        dc.description='test';
        dc.getAppType();
        dc.saveAppDetails();
        dc.getIsViewableForDev();
        dc.getAuthenticated();
        dc.getRole();
        dc.getIsAccessible();
        test.Stoptest();
    }
}
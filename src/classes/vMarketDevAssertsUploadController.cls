public with sharing class vMarketDevAssertsUploadController extends vMarketBaseController {
    public String AppVideoId {get;set;}
    public String existingVideoId {get;set;}
    public String appSourceId {get;set;}
    public Attachment existing_logo {get;set;}
    public Attachment existing_banner {get;set;}
    public Attachment existing_userGuide {get;set;}
    public Attachment screenshot1 {get;set;}
    public Attachment screenshot2 {get;set;}
    public Attachment screenshot3 {get;set;}
    public Attachment screenshot4 {get;set;}
    public Attachment attached_logo {get;set;}
     public Attachment attached_Banner {get;set;} 
    Public Attachment attached_userGuidePDF {get;set;}
   
    public string apId {get;set;}
    //Selected count
    public Integer FileCount {get; set;}
    public List<Attachment> allFileList {get; set;}
    public List<Attachment> iconsListtoInsert {get; set;}
    public vMarket_App__c new_app_obj {get;set;}
    public boolean isFree {get;set;}
    public boolean logoExists {get;set;}
    public boolean bannerExists {get;set;}
    public boolean guideExists {get;set;}
    public boolean videoIdExists {get;set;}
    public boolean screenshotexists1 {get;set;}
    public boolean screenshotexists2 {get;set;}
    public boolean screenshotexists3 {get;set;}
    public boolean screenshotexists4 {get;set;}
    public List<vMarketStripeDetail__c> userStripeDetail {get;set;}
    public List<vMarketAppAsset__c> appAssets{get;set;}
    
    public Attachment existing_userEndUserAgreement {get;set;}
  	Public Attachment attached_userEndUserAgreement {get;set;}
  	public boolean endUserAgreementExists {get;set;}
    
    public vMarketDevAssertsUploadController() {
        apId = ApexPages.currentPage().getParameters().get('apId');
        new_app_obj = new vMarket_App__c();
        new_app_obj =[Select Id, Name, App_Category__c, ApprovalStatus__c, AppStatus__c, Developer_Stripe_Acc_Id__c,
                        IsActive__c, IsFree__c, Long_Description__c, Price__c, Published_Date__c,
                        Short_Description__c,Key_features__c, Stripe_Account_Id__c, Developer_Acct_Id__c, subscription__c
                        from vMarket_App__c
                        where Id =: apId];
        
        attached_logo = new Attachment();
        attached_Banner =new Attachment();
        attached_userGuidePDF = new Attachment();
        screenshot1 = new Attachment();
        screenshot2 = new Attachment();
        screenshot3 = new Attachment();
        screenshot4 = new Attachment();
        attached_userEndUserAgreement = new Attachment();
        
        logoExists = false;
        bannerExists = false;
        guideExists = false;
        videoIdExists = false;
        Integer ctr=0;
        screenshotexists1 = false;
        screenshotexists2 = false;
        screenshotexists3 = false;
        screenshotexists4 = false;
        existingVideoId = '';
        endUserAgreementExists=false;
		System.debug('---'+screenshotexists4);
        //existing_screenshots = new List<Attachment>();
        
        for(Attachment a :[select Name, parentid, ContentType from Attachment where parentid=:apId] )
        {
            System.debug('===='+a.Name);
            if(a.Name.startsWith('Logo_')){
                existing_logo = a;
                logoExists = true;
            }
            else if(a.Name.startsWith('Banner_')){
                existing_banner = a;
                bannerExists = true;
            }
            else if(a.Name.startsWith('AppGuide_')){
                existing_userGuide = a;
                guideExists = true;
            }
            
            else if(a.Name.startsWith('EULA_')){
                existing_userEndUserAgreement = a;
                endUserAgreementExists = true;
            }
           
        }
        

        appAssets = [select vMarket_App__c,AppVideoID__c from vMarketAppAsset__c where vMarket_App__c=:apId];
        System.debug('===='+appAssets.size());        
        if(appAssets!=null && appAssets.size()>0)
        {
        videoIdExists = true;
        AppVideoId = appAssets[0].AppVideoID__c;
        List<Attachment> screenshotsExisting = [Select id,name from attachment where parentId=:appAssets[0].id];
        if(screenshotsExisting.size()>=1) {screenshotexists1 = true;
            screenshot1 = screenshotsExisting[0];}
            if(screenshotsExisting.size()>= 2) {screenshotexists2 = true;
            screenshot2 = screenshotsExisting[1];}
            if(screenshotsExisting.size()>= 3) {screenshotexists3 = true;
            screenshot3 = screenshotsExisting[2];}
            if(screenshotsExisting.size()>= 4) {screenshotexists4 = true;
            screenshot4 = screenshotsExisting[3];} 
        }
        reinitiaze();
         System.debug('***'+appAssets.size());
    }

    public Boolean getIsViewableForDev() {
        List<vMarket_App__c> apps = [select Id From vMarket_App__c Where Id=:apId And OwnerId=:UserInfo.getUserId()];
        if(!apps.isEmpty()) 
            return true;
        else
            return false;
    }

    public void reinitiaze() {
        //Initialize
        FileCount = 4;
        allFileList = new List<Attachment>();
        for(Integer i = 1 ; i <= 4 ; i++)
            allFileList.add(new Attachment()) ;   
    }

    public Boolean getIsAccessible() {
        if(getIsViewableForDev() && getAuthenticated() && getRole()=='Developer')
            return true;
        else
            return false;
    }

    public override Boolean getAuthenticated() {
        return super.getAuthenticated();
    }

    public override String getRole() {
        return super.getRole();
    }

    public void saveIsFree(){
        System.debug('==isFree=='+isFree);
    }

    public pageReference saveAppDetails() {
     System.debug('--LOGO--'+attached_logo);
            System.debug('--ScreenShots--'+allFileList);
             System.debug('--Banner--'+attached_Banner);            
             
            System.debug('--'+attached_userGuidePDF);
        if (true) {
            // App Basic Details store
            List<Attachment> listToInsert = new List<Attachment>() ;
            
            List<Attachment> screenshotstoDelete = new List<Attachment>() ;
           
            
            if(attached_logo!=null && attached_logo.body!=null && logoExists) delete existing_logo;
            
            String AppId = new_app_obj.id;
            
            if(attached_logo!=null && String.isNotBlank(attached_logo.name)) {
                attached_logo.parentid= AppId;
                attached_logo.Name = 'Logo_'+attached_logo.Name;
                attached_logo.ContentType = 'image/png';
                listToInsert.add(attached_logo);
            }
            
            if(attached_Banner!=null && attached_Banner.body!=null && existing_Banner!=null) delete existing_Banner;
            if(attached_Banner!=null && String.isNotBlank(attached_Banner.name)) {
                attached_Banner.parentid= AppId;
                attached_Banner.Name = 'Banner_'+attached_Banner.Name;
                attached_Banner.ContentType = 'image/jpg';
                listToInsert.add(attached_Banner);
            }
            // This is for USER GUIDE
            //attached_userGuidePDF = new Attachment();
            
            if(attached_userGuidePDF!=null && attached_userGuidePDF.body!=null && existing_userGuide!=null) delete existing_userGuide;
            if(attached_userGuidePDF!=null && String.isNotBlank(attached_userGuidePDF.name)) {
                attached_userGuidePDF.parentid = AppId;
                attached_userGuidePDF.Name = 'AppGuide_'+attached_userGuidePDF.Name;
                attached_userGuidePDF.ContentType = 'application/pdf';
                listToInsert.add(attached_userGuidePDF);
            }

            System.debug('--'+attached_banner);            
             
            System.debug('--'+attached_userGuidePDF); 
            //  insert attached_banner;
            //insert attached_logo;
            // insert attached_userGuidePDF;
			// For End User Agreement
            System.debug('--'+attached_userEndUserAgreement); 
            if(attached_userEndUserAgreement!=null && attached_userEndUserAgreement.body!=null && existing_userEndUserAgreement!=null) delete existing_userEndUserAgreement;
            if(attached_userEndUserAgreement!=null && String.isNotBlank(attached_userEndUserAgreement.name)) {
                attached_userEndUserAgreement.parentid = AppId;
                attached_userEndUserAgreement.Name = 'EULA_'+attached_userEndUserAgreement.Name;
                attached_userEndUserAgreement.ContentType = 'application/pdf';
                listToInsert.add(attached_userEndUserAgreement);
            }
            // App Asset Store
            vMarketAppAsset__c appAsset;
            if(appAssets.size()>0) appAsset = appAssets[0];
            if (AppVideoId != null || AppVideoId != '' ) {
                if(appAssets.size()>0) 
                {
                     AppVideoId = AppVideoId!=null ? AppVideoId.escapeHtml4():AppVideoId;
                     appAsset.AppVideoID__c = AppVideoId;
                }
                else appAsset = new vMarketAppAsset__c(Name='AppAsset_'+AppId, vMarket_App__c=AppId, AppVideoID__c = AppVideoId);
            } else {
                   if(appAssets.size()==0) appAsset = new vMarketAppAsset__c(Name='AppAsset_'+AppId, vMarket_App__c=AppId);
            }
            if(appAssets.size()==0) insert appAsset;
            else update appAsset;
            
            String accId = appAsset.id;
            
            vMarketAppSource__c appSource;
            if(new_app_obj.id != null){
            List<vMarketAppSource__c> appSourcesList = [Select vMarket_App__c,Status__c,IsActive__c,App_Category_Version__c from vMarketAppSource__c where vMarket_App__c=:new_app_obj.id and (App_Category_Version__c =null or App_Category_Version__c='')];
            
            if(appSourcesList.size()>0) appSource = appSourcesList[0];
            else{ 
            appSource = new vMarketAppSource__c(vMarket_App__c = new_app_obj.id,Status__c = 'New',IsActive__c = false);
            insert appSource;
            }
            }
            
            appSourceId = appSource.Id;
            //List<Attachment> listToInsert = new List<Attachment>() ;
            Integer count = 0;
            for(Integer i = 0 ; i < Integer.valueOf(FileCount) ; i++) {
                if(allFileList[i].name != '' && allFileList[i].body != null) {
                    listToInsert.add(new Attachment(parentId = appAsset.id, name = allFileList[i].name, body = allFileList[i].body)) ;
                 //   allFileList.remove(i);
                    count += 1;
                    if(i==0 && screenshotexists1 && screenshot1!=null) screenshotstoDelete.add(screenshot1);
                    if(i==1 && screenshotexists2 && screenshot2!=null) screenshotstoDelete.add(screenshot2);
                    if(i==2 && screenshotexists3 && screenshot3!=null) screenshotstoDelete.add(screenshot3);
                    if(i==3 && screenshotexists4 && screenshot4!=null) screenshotstoDelete.add(screenshot4);
                }
            }
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;

            //Inserting attachments
            if(listToInsert.size() > 0) {
           allFileList = new List<Attachment>();
            System.debug('&&&&&&'+listToInsert); 
                insert listToInsert ;
                
                listToInsert = new List<Attachment>() ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, count + ' file(s) are uploaded successfully'));
            }
            if(screenshotstoDelete.size()>0) delete screenshotstoDelete;
            
        }else {
            allFileList.clear();
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please, upload atleast one image.'));
        }
            PageReference pf = new PageReference('/vMarketDevAppPackageUpload?appSourceId='+appSourceId);
            return pf;
    }
    
    
    public pageReference back(){
    
    PageReference pf = new PageReference('/vMarketAppInitialEdit?appId='+apId);
            return pf;
    }
    
}
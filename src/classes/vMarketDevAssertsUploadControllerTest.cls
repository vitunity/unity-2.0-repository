@isTest
public with sharing class vMarketDevAssertsUploadControllerTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      vMarketAppAsset__c appAsset = vMarketDataUtility_Test.createAppAssetTest(app, true);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
       
      PageReference myVfPage = Page.vMarketDevAssetsUpload;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('apId',app.Id);
      
      // Inserting Existing Logo, Banner and App Guide
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/jpg', 'Logo_logo', true);
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'Banner_knumber', true);
      vMarketDataUtility_Test.createAttachmentTest(app.Id,'content/pdf', 'AppGuide_opinion', true);
      
      // Inserting 4 Screenshots
      vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Logo_logo', true);
      vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'Banner_knumber', true);
      vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
      vMarketDataUtility_Test.createAttachmentTest(appAsset.Id,'content/jpg', 'AppGuide_opinion', true);
      
      vMarketDevAssertsUploadController dc = new vMarketDevAssertsUploadController();           
      
      System.debug(dc.getIsAccessible());
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      dc.saveIsFree();
      dc.back();
      
      
      
      dc.attached_logo = vMarketDataUtility_Test.createAttachmentTest('','content/png', 'logo', false);
      dc.attached_Banner = vMarketDataUtility_Test.createAttachmentTest('','content/jpg', 'Banner', false);
      dc.attached_userGuidePDF = vMarketDataUtility_Test.createAttachmentTest('','content/pdf', 'guide', false);      
      
      dc.saveAppDetails();
      test.Stoptest();
    }
}
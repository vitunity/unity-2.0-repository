public with sharing class vMarketDevBugFixAppSelectController {
    public String appSourceId{get;set;}
    public String appId{get;set;}
    public vMarket_App__c currentAppObj{get;set;}
    public vMarketAppSource__c appSource{get;set;}
    public List<Attachment> allFileList {get; set;}
    public vMarketAppAsset__c appAsset{get; set;}
    public Attachment attached_userGuidePDF {get;set;}
    public Integer FileCount {get; set;}
    public List<vMarketAppSource__c> appSourceList {get;set;}
    public string SelSourceRecID {get;set;}
    public vMarketAppSource__c sourceRecord {get;set;}
    public List<String> selectedCatVersions {get;set;}
    public String selectedApp {get;set;}
    public String selectedAppSource {get;set;}
    public String newAppSource {get;set;}
    public vMarketAppSource__c appScr{get;set;}
    
    public vMarketAppSource__c appSourceForChatter {get;set;}
    public vMarket_App__c  newApp {get;set;}
    public  list<vMarket_App__c> appsToDelete {get;set;}
    
    public vMarketDevBugFixAppSelectController(){
        //Get App ID
        appSourceId = ApexPages.currentPage().getParameters().get('appSourceId');
        appSourceList = new List<vMarketAppSource__c>();
        appsToDelete = new  list<vMarket_App__c>();
    }
    
    public void initializePage(){
        sourceRecord = new vMarketAppSource__c();
        newApp = new vMarket_App__c( Name= 'Test', Short_Description__c= 'Test',Price__c= 50.00, App_Category__c= 'a8A4C0000008QZM', Stripe_Account_Id__c = 'a8C4C0000008OtG');
        insert newApp;
        appSourceForChatter = new vMarketAppSource__c(vMarket_App__c = newApp.Id, Status__c = 'New',IsActive__c = true, App_Category_Version__c = 'Rapid Plan');
        insert appSourceForChatter;
    }
    
    // Fetching app source records
    public list<vMarketAppSource__c> fetchAppSource() {
         appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:selectedApp And IsActive__c =: true And  CreatedDate = TODAY];  
         system.debug('==appSourceList=='+appSourceList); 
         if(appSourceList.Size()<1){
             return null;
         }else{
             return appSourceList;
         }
    }
    
    public void selectedApp(){
        system.debug('==selectedAppSource =='+selectedAppSource );
    }
    
    public List<SelectOption> getAppRecords() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App__c> app_records = [select Id, Name, App_Category__c, ApprovalStatus__c, AppStatus__c, Developer_Stripe_Acc_Id__c,
                                                        IsActive__c, isApprovedOrRejected__c, Key_features__c, Long_Description__c, Price__c, OwnerId, 
                                                        Published_Date__c, Short_Description__c, Stripe_Account_Id__c, Developer_Acct_Id__c
                                                        from vMarket_App__c 
                                                        where OwnerId =:UserInfo.getUserId()];
        
        options.add(new SelectOption('Select App','Select App'));
        for(vMarket_App__c devApp :app_records) {
            options.add(new SelectOption(devApp.Id,devApp.Name));
        }
        return options;
    }
    
    public List<SelectOption> getAppSourceRecords() {
        system.debug('==selectedApp=='+selectedApp);
        List<SelectOption> options = new List<SelectOption>();
        List<vMarketAppSource__c> app_source_records = [select Id, App_Category_Version__c, vMarket_App__c, IsActive__c
                                                        from vMarketAppSource__c 
                                                        where vMarket_App__c =:selectedApp And IsActive__c =: true];
        
        options.add(new SelectOption('','Select App Source'));
        for(vMarketAppSource__c devAppSource :app_source_records) {
            options.add(new SelectOption(devAppSource.Id,devAppSource.App_Category_Version__c));
        }
        return options;
    } 
    
    
    public List<SelectOption> getAppCategoryVersions() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category_Version__c> app_categories_versions = [select Name,App_Category__c from vMarket_App_Category_Version__c where App_Category__c=:currentAppObj.App_Category__c];
        
        options.add(new SelectOption('Select Category Version','Select Category Version'));
        for(vMarket_App_Category_Version__c catVersion:app_categories_versions) {
            options.add(new SelectOption(catVersion.Name,catVersion.Name));
        }
        return options;
    }
    
    public void deleteSource(){
        
        vMarketAppSource__c appScr = new  vMarketAppSource__c();
        appScr = [Select Id from vMarketAppSource__c where Id=:SelSourceRecID ]; 
        delete appScr; 
        
        
        appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:selectedApp And IsActive__c =: true And  CreatedDate = TODAY];
        
    }
    
    
    public void createChatterEntitySource(){
        if(newAppSource == null || newAppSource == '' ){
            
                vMarketAppSource__c chatterEntityAppScr = new vMarketAppSource__c( vMarket_App__c = selectedApp , Status__c = 'New',IsActive__c = false, App_Category_Version__c = 'Rapid Plan');
                insert chatterEntityAppScr;
                newAppSource = chatterEntityAppScr.Id;
            
        }else{
            List<Attachment> appSourceAttachments = new List<Attachment>();
            appSourceAttachments = [Select Id, parentid from Attachment
                                    where parentid =:newAppSource];
            if(appSourceAttachments.size()>0){
                vMarketAppSource__c chatterEntityAppScr = new vMarketAppSource__c( vMarket_App__c = selectedApp , Status__c = 'New',IsActive__c = false, App_Category_Version__c = 'Rapid Plan');
                insert chatterEntityAppScr;
                newAppSource = chatterEntityAppScr.Id;        
            }
        }
    }
    
    
    public PageReference createAppSource() {
        system.debug('==selectedAppSource=='+selectedAppSource);
        list<vMarketAppSource__c> appSourceToUpdate = new list<vMarketAppSource__c>();
        String selected = '';
        Boolean Start = true;
        
        appScr = new  vMarketAppSource__c();
        appScr = [Select Id, IsActive__c, vMarket_App__c from vMarketAppSource__c where Id=:selectedAppSource ]; 
        //appScr.IsActive__c = false;
        appSourceToUpdate.add(appScr);
        system.debug('==appScr=='+appScr);
        appSourceForChatter.vMarket_App__c = appScr.vMarket_App__c;
        appSourceToUpdate.add(appSourceForChatter);
        system.debug('==appSourceForChatter=='+appSourceForChatter);
       
        if(appSourceToUpdate.size()>0)
            update appSourceToUpdate;
            
        appsToDelete = [Select Id, Name from vMarket_App__c where Name = 'Test']; 
        delete appsToDelete ;
        
        
        
        pageReference pgRef = ApexPages.currentPage();
        pgRef.setRedirect(true);
        return pgRef; 
    }

    public pageReference submitApp(){
        list<vMarket_App__c> appRecords = new list<vMarket_App__c>();
        appRecords = [Select Id, Name from vMarket_App__c where Name =:'Test'];
        delete newApp;
       
        vMarket_App__c app = new vMarket_App__c ();
        app = [Select Id, IsActive__c from vMarket_App__c where Id=:selectedApp];
        pageReference pgRef = new PageReference('/apex/vMarketDevAppDetails');
        
        //pgRef.getParameters().clear();
        pgRef.getParameters().put('appId', app.Id); 
        pgRef.setRedirect(true);
        return pgRef; 
    }
}
/***********************************************************************
* Author         : N/A
* Created Date: N/A
Project/Story/Inc/Task : N/A
Description: 
This is a controller for Uploading Bug Fixes for an Existing App on MarketPlace Using Visual Force Page.

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Nov 24 2017 -  Abhishek Kolipey - CHG0119045 - Added Method for Polling to Check if Chatter File is Uploaded
************************************************************************/

public with sharing class vMarketDevBugFixController extends vMarketBaseController{
    public String appSourceId{get;set;}
    public String appId{get;set;}
    public String bug_fix_features{get;set;}
    public vMarket_App__c currentAppObj{get;set;}
    public vMarketAppSource__c appSource{get;set;}
    public List<Attachment> allFileList {get; set;}
    public vMarketAppAsset__c appAsset{get; set;}
    public Attachment attached_userGuidePDF {get;set;}
    public Integer FileCount {get; set;}
    public List<vMarketAppSource__c> appSourceList {get;set;}
    public string SelSourceRecID {get;set;}
    public vMarketAppSource__c sourceRecord {get;set;}
    public List<String> selectedCatVersions {get;set;}
    public String selectedApp {get;set;}
    public String selectedAppSource {get;set;}
    public String newAppSource {get;set;}
    public String  chatterAppSrc{get;set;}
    public set<String> strSet {get;set;}
    public vMarketAppSource__c appSourceForChatter {get;set;}
    public vMarket_App__c  newApp {get;set;}
    public list<vMarket_App__c> appsToDelete {get;set;}
    public String selectedCategoryVersions {get;set;}
    public String selectedSourceIds {get;set;}
    public list<vMarket_App_Category__c> categoriesToDelete {get;set;}
    public map<Id,Id> appSourceFiles{get;set;}
    public vMarketAppSource__c appScr{get;set;}
    public String keyString{get;set;}
    public boolean uploadError{get;set;}
    public Set<Id> sourceWithDocuments{get;set;}
    public boolean documentsExist{get;set;}
    public List<Id> currentSourcesList{get;set;}
    public Set<String> docsWithcategories{get;set;}    
    
    public vMarketDevBugFixController(){
        //Get App ID
        strSet = new set<String>();
        sourceWithDocuments = new Set<id>();
        docsWithcategories = new Set<String>();
        documentsExist = false;
        uploadError = false;
        appSourceList = new List<vMarketAppSource__c>();
        appsToDelete = new  list<vMarket_App__c>();
        appSourceFiles = new map<Id,Id>();
        currentSourcesList = new List<Id>();
        //selectedCategoryVersions = new Set<String>();
        categoriesToDelete = new list<vMarket_App_Category__c>();        
        selectedApp = ApexPages.currentPage().getParameters().get('appIdOnLoad');
        selectedCategoryVersions = ApexPages.currentPage().getParameters().get('appCategories');
        selectedSourceIds = ApexPages.currentPage().getParameters().get('appSourceIds');
        if(String.IsNotBLANK(selectedCategoryVersions)) selectedCategoryVersions.replaceall('\\+',' ');
        if(String.isNotBlank(selectedApp)) 
        {
        
        if(String.isNOTBlank(selectedSourceIds))
        {
            for(String s:selectedSourceIds.split(',',-1))
            {
                currentSourcesList.add(Id.valueOf(s));
            }
        }
        
        appSourceList = [select Id, Name, App_Category__c,App_Category_Version__c,
                              IsActive__c, Status__c, vMarket_App__c 
                              from vMarketAppSource__c 
                              where Id in :currentSourcesList]; 
        Map<Id,String> sourceCatMap=new Map<Id,String>();               
        for(vMarketAppSource__c appsc : appSourceList)
        {
        sourceCatMap.put(appsc.id,appsc.App_Category_Version__c);
        }                              
                              
        if(appSourceList.size()>0)
        {
            for(ContentDocumentLink a :[SELECT LinkedEntityId,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId IN :currentSourcesList])
            {
            appSourceFiles.put(a.LinkedEntityId,a.ContentDocumentId);
            keyString += '-'+a.LinkedEntityId;
            sourceWithDocuments.add(a.LinkedEntityId);
            docsWithcategories.add(sourceCatMap.get(a.LinkedEntityId));
            }
        if(appSourceFiles.size()>0) documentsExist = true;    
        if(appSourceList.size() != appSourceFiles.size()) uploadError =true;
        }
                             
        }
        System.debug('--sources--'+appSourceFiles);
        //added by Puneet
        currentAppObj = new vMarket_App__c();
    }
    
    public void initializePage(){
        list<vMarket_App__c> appDeletionList = new list<vMarket_App__c>();
        
        //appDeletionList = [Select Id, Name from vMarket_App__c where Name = 'Test']; 
        //delete appDeletionList ;
        vMarket_App_Category__c category = new vMarket_App_Category__c();
        List<vMarket_App_Category__c> appCategoriesList = [Select id,name from vMarket_App_Category__c limit 1];
        
        if(appCategoriesList.size()>0) category = appCategoriesList[0];
        else 
        {
        category.name='Test';
        category.Short_Description__c = 'Creating this category as No records exist for Bug Fix';
        insert category;
        }
        
        sourceRecord = new vMarketAppSource__c();
        newApp = new vMarket_App__c( Name= 'Test', Short_Description__c= 'Test',Price__c= 50.00 , App_Category__c=category.id, PublisherName__c= 'Test', PublisherWebsite__c= 'Test', PublisherEmail__c= 'test@test.com', PublisherPhone__c= '9876544539');
        insert newApp;
        appSourceForChatter = new vMarketAppSource__c(vMarket_App__c = newApp.Id, Status__c = 'New',IsActive__c = false, App_Category_Version__c = 'Rapid Plan');
        insert appSourceForChatter;
    }
    
    // Fetching app source records
    public void fetchAppSource() {
        String selectedChatterAppSrc = ApexPages.CurrentPage().getParameters().get('chatterAppSrc');
        system.debug('==selectedChatterAppSrc=='+selectedChatterAppSrc );
        /*string s1 = selectedChatterAppSrc;
        String s2 = s1.remove('"');
        String s3 = s2.remove('[');
        String s4 = s3.remove(']');
        String s5 = s4.remove('\\');
        system.debug('==s5=='+s5);*/
        system.debug('-----------'+selectedChatterAppSrc);
        if (selectedChatterAppSrc==null || selectedChatterAppSrc=='') {
            system.debug('AppIdString is empty');
        } else {
            List<String> strList = new list<String>();
            strList = selectedChatterAppSrc.split(',');
            system.debug('strList -----------'+strList );
            system.debug('==strList=='+strList);
            
            for(String st:strList){
                strSet.add(st);    
            }
            system.debug('==strSet=='+strSet);
           /* appSourceList = [select Id, Name, App_Category__c,App_Category_Version__c,
                              IsActive__c, Status__c, vMarket_App__c 
                              from vMarketAppSource__c 
                            where vMarket_App__c =:selectedApp And  Id IN: strSet];  */
            system.debug('==appSourceList=='+appSourceList);
        }
    }
    
    public void selectedAppSrc(){
        system.debug('==selectedAppSource =='+selectedAppSource );
    }
    
    public override Boolean getAuthenticated() {
    return super.getAuthenticated();
    }
    
    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer')
            return true;
        else
            return false;
    }
    
    @testVisible private list<String> appStatusList = new list<String>{Label.vMarket_Published};// Confirm is Submitted app is also required for Bug Fix
    
    public List<SelectOption> getAppRecords() {
    
        List<SelectOption> options = new List<SelectOption>();
        
       
        List<vMarket_App__c> app_records = [select Id, Name, App_Category__c, ApprovalStatus__c, AppStatus__c, Developer_Stripe_Acc_Id__c,
                                                        IsActive__c, isApprovedOrRejected__c, Long_Description__c, Price__c, OwnerId, 
                                                        Published_Date__c, Short_Description__c, Stripe_Account_Id__c, Developer_Acct_Id__c,
                                                        bug_fix__c,Key_features__c,bug_fix_features__c
                                                        from vMarket_App__c 
                                                        where OwnerId =:UserInfo.getUserId() and Name != 'Test' and ApprovalStatus__c IN: appStatusList];
        
        options.add(new SelectOption('','Select App'));
        for(vMarket_App__c devApp :app_records) {
            options.add(new SelectOption(devApp.Id,devApp.Name));
        }
        return options;
    }
    
    public List<SelectOption> getAppSourceRecords() {
        if(String.isBlank(selectedApp)) selectedApp = ApexPages.CurrentPage().getParameters().get('chatterAppSrc');
        system.debug('==selectedApp=='+selectedApp);
        List<SelectOption> options = new List<SelectOption>();
        List<vMarketAppSource__c> app_source_records = [select Id, App_Category_Version__c, vMarket_App__c, IsActive__c, IsSelected__c
                                                        from vMarketAppSource__c 
                                                        where vMarket_App__c =:selectedApp And IsSelected__c =: False And IsActive__c =: true];//And IsActive__c =: true
        
        Set<String> currentCategories = new Set<String>();
        if(String.isNotBlank(selectedSourceIds)) currentCategories.addAll(selectedCategoryVersions.split(',',-1));

        options.add(new SelectOption('','Select App Source'));
        for(vMarketAppSource__c devAppSource :app_source_records) {
        System.debug('CHEQUE'+currentCategories+sourceWithDocuments+devAppSource.id);
        //if(!sourceWithDocuments.contains(devAppSource.id) && currentCategories.contains(devAppSource.App_Category_Version__c)) currentCategories.remove(devAppSource.App_Category_Version__c);
            if (devAppSource.App_Category_Version__c != null && !docsWithcategories.contains(devAppSource.App_Category_Version__c))
            {
            
            options.add(new SelectOption(devAppSource.Id,devAppSource.App_Category_Version__c));
               //if(selectedCategoryVersions!=null && !selectedCategoryVersions.contains(devAppSource.App_Category_Version__c)) 
            }    
        }
        return options;
    } 
    
    
    public List<SelectOption> getAppCategoryVersions() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category_Version__c> app_categories_versions = [select Name,App_Category__c from vMarket_App_Category_Version__c where App_Category__c=:currentAppObj.App_Category__c];
        

        options.add(new SelectOption('','Select Category Version'));
        for(vMarket_App_Category_Version__c catVersion:app_categories_versions) {
            options.add(new SelectOption(catVersion.Name,catVersion.Name));
        }
        return options;
    }
    
    public pagereference deleteSource(){
        
        vMarketAppSource__c appScrs = new  vMarketAppSource__c();
        system.debug('==SelSourceRecID =='+SelSourceRecID );
        
        
        appScrs = [Select Id, vMarket_App__c, App_Category_Version__c from vMarketAppSource__c where Id=:SelSourceRecID ]; 
       
        /*appScr = new  vMarketAppSource__c();
        appScr = [Select Id, IsActive__c, App_Category_Version__c, vMarket_App__c 
                    from vMarketAppSource__c 
                    where App_Category_Version__c=:appScrs.App_Category_Version__c  And vMarket_App__c =: appScrs.vMarket_App__c  And IsActive__c =: true]; 
        
        appScr.IsSelected__c= False;
        
        update appScr;*/
        //appScrs= new  vMarketAppSource__c(id=SelSourceRecID);
        delete appScrs;
        
     /*  appSourceList = [select Id, Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:selectedApp And Id IN: strSet];
             */           
       //pageReference pgRef = ApexPages.currentPage();
       
       String newselectedSourceIds = '';
       String newselectedCategoryVersions = '';
       
       for(String s:selectedSourceIds.split(',',-1))
       {
       if(!String.valueOf(appScrs.id).contains(s)) newselectedSourceIds += s+',';
       }
       if(String.isNotBlank(newselectedSourceIds)) newselectedSourceIds = newselectedSourceIds.substring(0,newselectedSourceIds.length()-1);
       for(String s:selectedCategoryVersions.split(',',-1))
       {
       if(!String.valueOf(appScrs.App_Category_Version__c).contains(s)) newselectedCategoryVersions += s+',';
       }
       if(String.isNotBlank(newselectedCategoryVersions)) newselectedCategoryVersions = newselectedCategoryVersions.substring(0,newselectedCategoryVersions.length()-1);
       pageReference pgRef = new pageReference('/vMarketDevUploadAppBugFix');
       pgRef.getParameters().put('appIdOnLoad',selectedApp);
       pgRef.getParameters().put('appSourceIds',newselectedSourceIds);
       pgRef.getParameters().put('appCategories',newselectedCategoryVersions);
       pgRef.setRedirect(true);
       return pgRef;
        
    }
    
    
    public void createChatterEntitySource(){
        if(newAppSource == null || newAppSource == '' ){
            
                vMarketAppSource__c chatterEntityAppScr = new vMarketAppSource__c( vMarket_App__c = selectedApp , Status__c = 'New',IsActive__c = false, App_Category_Version__c = 'Rapid Plan');
                insert chatterEntityAppScr;
                newAppSource = chatterEntityAppScr.Id;
            
        }else{
            List<Attachment> appSourceAttachments = new List<Attachment>();
            appSourceAttachments = [Select Id, parentid from Attachment
                                    where parentid =:newAppSource];
            if(appSourceAttachments.size()>0){
                vMarketAppSource__c chatterEntityAppScr = new vMarketAppSource__c( vMarket_App__c = selectedApp , Status__c = 'New',IsActive__c = false, App_Category_Version__c = 'Rapid Plan');
                insert chatterEntityAppScr;
                newAppSource = chatterEntityAppScr.Id;        
            }
        }
    }
    
    
    public PageReference createAppSource() {
        System.debug('**appname'+selectedApp);
        System.debug('**appsource'+selectedAppSource);
       /* if(String.isBlank(selectedApp))
        {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select an App'));
         return null;
        }
        if(String.isBlank(selectedAppSource))
        {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select a Category'));
         return null;
        }
        */
        //System.debug('**appname'+selectedApp);
        system.debug('==selectedAppSource=='+selectedAppSource);
        list<vMarketAppSource__c> appSourceToUpdate = new list<vMarketAppSource__c>();
        String selected = '';
        Boolean Start = true;
        
        appScr = new  vMarketAppSource__c();
        appScr = [Select Id, IsActive__c, vMarket_App__c, App_Category_Version__c from vMarketAppSource__c where Id=:selectedAppSource ]; 
        
        List<vMarketAppSource__c> appSourcesToBeDeleted = new List<vMarketAppSource__c>();

        if(currentSourcesList.size()>0)
        {
        for(vMarketAppSource__c src: [Select id from vMarketAppSource__c where Id IN :currentSourcesList])
        {
        if(!sourceWithDocuments.contains(src.id)) appSourcesToBeDeleted.add(src);
        }
        if(appSourcesToBeDeleted.size()>0) delete appSourcesToBeDeleted;
        }
        //appScr.IsSelected__c= True;
        //appScr.IsActive__c= True;
        //appSourceToUpdate.add(appScr);
        system.debug('==appScr=='+appScr);
        appSourceForChatter.App_Category_Version__c = appScr.App_Category_Version__c;
        appSourceForChatter.vMarket_App__c = appScr.vMarket_App__c;
        //appSourceForChatter.IsActive__c= True;
        appSourceToUpdate.add(appSourceForChatter);
        system.debug('==appSourceForChatter=='+appSourceForChatter);
       
        if(appSourceToUpdate.size()>0)
            update appSourceToUpdate;
            
        //appsToDelete = [Select Id, Name from vMarket_App__c where Name = 'Test']; 
        //delete appsToDelete ;
        
        // Deleting the test app categories
        categoriesToDelete = [Select Id, Name from vMarket_App_Category__c where Name = 'Test'];
        if(categoriesToDelete.size()>0)
        delete categoriesToDelete;
        
        //pageReference pgRef = ApexPages.currentPage();
        if(String.IsBLANK(selectedCategoryVersions)) selectedCategoryVersions = String.valueof(appSourceForChatter.App_Category_Version__c);
        else selectedCategoryVersions += ','+String.valueof(appSourceForChatter.App_Category_Version__c);
        
        if(String.IsBLANK(selectedSourceIds)) selectedSourceIds = String.valueof(appSourceForChatter.id);
        else selectedSourceIds += ','+String.valueof(appSourceForChatter.id);
       
        
        pageReference pgRef = new pageReference('/vMarketDevUploadAppBugFix');
        pgRef.getParameters().put('appIdOnLoad',selectedApp);
        pgRef.getParameters().put('appSourceIds',selectedSourceIds);
        pgRef.getParameters().put('appCategories',selectedCategoryVersions);
        pgRef.setRedirect(true);
        return pgRef; 
    }

    public pageReference submitApp(){
        String selectedAppId = ApexPages.CurrentPage().getParameters().get('selectedAppId');
        list<vMarket_App__c> appRecords = new list<vMarket_App__c>();
        appRecords = [Select Id, Name from vMarket_App__c where Name =:'Test'];
        delete newApp;
        
        // Deleting the test app categories
        categoriesToDelete = [Select Id, Name from vMarket_App_Category__c where Name = 'Test'];
        if(categoriesToDelete.size()>0)
        delete categoriesToDelete;
        
        //currentAppObj.bug_fix__c = true;
        //upsert currentAppObj;
       
        vMarket_App__c app = new vMarket_App__c ();
        app = [Select Id, IsActive__c, ApprovalStatus__c, bug_fix__c,bug_fix_features__c from vMarket_App__c where Id=:selectedAppId];
        app.bug_fix__c = true;
        //app.ApprovalStatus__c = Label.vMarket_AppSubmitted; // Removed as App will be still Published and Admin Needs to manually change from Back
        app.bug_fix_features__c = bug_fix_features;
        upsert app;
        
        String appDetailsURL = '/vMarketDevAppDetails?appId=' + selectedAppId;
        pageReference pgRef = new PageReference('/vMarketDevAppDetails');
        system.debug('==selectedAppId=='+selectedAppId);
        system.debug('==app.ApprovalStatus__c=='+app.ApprovalStatus__c);
        //pgRef.getParameters().clear();
        pgRef.getParameters().put('appId', app.Id);
        pgRef.getParameters().put('status', app.ApprovalStatus__c); 
        pgRef.setRedirect(true);
        return pgRef; 
    }
    
    @RemoteAction //Nov 24 2017 -  Abhishek Kolipey - CHG0119045 - Added Method for Polling to Check if Chatter File is Uploaded
    public static boolean fileuploaded(String sourceId)
    {
    
    List<ContentDocumentLink> docs = [SELECT LinkedEntityId,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = :sourceId];
    if(docs.size()>0) return true;
    else return false;
    }
    
    
}
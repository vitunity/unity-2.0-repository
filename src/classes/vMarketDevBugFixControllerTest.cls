@isTest
public with sharing class vMarketDevBugFixControllerTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      
      
      vMarketDevBugFixController dc = new vMarketDevBugFixController();     
      dc.selectedApp = String.valueof(app.id);
      
      
      dc.initializePage();
      dc.selectedAppSrc();
      dc.getAuthenticated();
      dc.getRole();
      dc.getIsAccessible();
      dc.getAppRecords();
      dc.fetchAppSource();
      dc.getAppSourceRecords();
      dc.getAppCategoryVersions();   
      
         
      dc.createChatterEntitySource();
      
      dc.selectedAppSource = String.valueof([Select Id, IsActive__c, vMarket_App__c, App_Category_Version__c from vMarketAppSource__c limit 1].id);
      dc.createAppSource();
      dc.SelSourceRecID = String.valueof([Select Id, IsActive__c, vMarket_App__c, App_Category_Version__c from vMarketAppSource__c limit 1].id);
      dc.deleteSource();
      
      dc.initializePage();
      dc.createChatterEntitySource();
      
      dc.selectedAppSource = String.valueof([Select Id, IsActive__c, vMarket_App__c, App_Category_Version__c from vMarketAppSource__c limit 1].id);
      dc.createAppSource();
      
      PageReference myVfPage = Page.vMarketDevUploadAppBugFix;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('selectedAppId',app.Id);
      dc.submitApp();
    
      PageReference myVfPage1 = Page.vMarketDevUploadAppBugFix;
      Test.setCurrentPage(myVfPage1);

      ApexPages.currentPage().getParameters().put('appIdOnLoad',app.Id);
      ApexPages.currentPage().getParameters().put('appCategories',app.Id);
      ApexPages.currentPage().getParameters().put('appSourceIds',Id.valueOf(dc.selectedAppSource));
      vMarketDevBugFixController dc1 = new vMarketDevBugFixController();     
      
      
      test.Stoptest();
    }
}
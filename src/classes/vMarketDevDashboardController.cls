/**
 * @author  :   Puneet Mishra
 * @Desc    :   Controller for Developer Dashboard
 */
public with sharing class vMarketDevDashboardController {
    
    public static string developerName{get {
        return userInfo.getName().toUpperCase();
    }
    set;}
    
    public static date lastlogin {
        get {
            user u = [SELECT Id, Name , LastLoginDate FROM User Where Id =: userInfo.getUserId()];
            return Date.newInstance(u.LastLoginDate.year(), u.LastLoginDate.month(), u.LastLoginDate.day());
        } set;
    }

    public Integer DEFAULT_MONTHLY_VALUE = 6; // only for last 6 month

    public Integer todaysVisitors{get;set;} // todays total visitor
    public Integer monthlyVisitors{get;set;} // last 6 months total visitors
    public Integer totalVisitors{get;set;} //number of time user open App detail page
    
    public Integer todayBuyers{get;set;} // today : number of time App Downloaded
    public Integer monthlyBuyers{get;set;}// last 6 months total buyers
    public Integer totalBuyers{get;set;} // number of time Application downloaded
    
    public Decimal todayEarnings{get;set;}
    public Decimal monthlyEarning{get;set;}
    public Double totalEarning{get;set;} // from sucessful Order Price
    
    public Integer appSold{get;set;}// number of apps sold

    public String myTotalVisitors{get;set;} // JSON for My Total Visitor KPI
    public String myTotalEarnings{get;set;} // JSON for My Total Earnings KPI
    public String todaysEarning{get;set;} // JSON for Today Earnings
    public String monthlyTotalVisitorsJSON{get;set;} // JSON for getting visitors of DEFAULT_MONTHLY_VALUE
    
    
    public String selected_frequency {get;set;} // selected frequency
    // return monthly option list
    public List<SelectOption> getMonthList() {
        List<SelectOption> options = new List<SelectOption>();
        MAP<String, String> Chart_Monthly_Frequency = new MAP<String, String>{'3'=>'3 Month', '6'=>'6 Month', '12'=>'12 Month'};
        for(String key : Chart_Monthly_Frequency.keySet()) {
            options.add(new SelectOption(key, Chart_Monthly_Frequency.get(key))); 
        }
        return options;
    }
    @testVisible private String ownerId;
    @testVisible private DateTime DT = system.today();//DateTime.newInstance(system.today().Year(), system.today().Month(), system.today().Day(), 0, 0, 0);
    @testVisible private DateTime startD;
    @testVisible private DateTime endD;
    
    {
        todaysVisitors = 0;
        monthlyVisitors = 0;
        totalVisitors = 0;
        
        todayBuyers = 0;
        monthlyBuyers = 0;
        totalBuyers = 0;
        
        todayEarnings = 0;
        monthlyEarning = 0;
        totalEarning = 0;
    }
    
    public vMarketDevDashboardController() {
        String str = userInfo.getUserId();
        ownerId = str.substring(0, 15);
        calculateKPI();
    }
    
    public void calculateKPI() {
        system.debug(' ====selected_frequency==== ' + selected_frequency);
        endD = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
        startD = endD.addMonths(-6);
        
        developerVistors(ownerId, startD, endD, DT);
        developerBuyers();
        developerEarnings();
        developerAppSold();
        salesAnalytics();
    }
    
    /**
     *  method for visitors KPI
     */
    @testVisible
    private void developerVistors(String ownerId, DateTime startD, DateTime endD, DateTime todayDate) {
        myTotalVisitors = '';
        
        // developer total visitors
        totalVisitors = [SELECT Count() FROM vMarketListingActivity__c WHERE vMarket_Listing__r.App__r.ownerId =: UserInfo.getUserId() AND CreatedDate <=: endD];
        // monthly visitor
        /*string monthlyVisitorSOQL = 'SELECT App__r.Name,  PageViews__c, InstallCount__c, App__c, ownerId ' +
                                    ' FROM vMarket_Listing__c WHERE App__r.ownerId = : ownerId ' ;
        if(startD != null && endD != null)
            monthlyVisitorSOQL += ' AND CreatedDate >=: startD AND CreatedDate <=: endD ';
        system.debug(' === startD === ' + startD + ' === endD === ' + endD);
        for(vMarket_Listing__c lists : Database.query(monthlyVisitorSOQL) ) {
            monthlyVisitors += integer.valueOf(lists.PageViews__c);
        }*/
        monthlyVisitors = [SELECT Count() FROM vMarketListingActivity__c WHERE vMarket_Listing__r.App__r.ownerId =: UserInfo.getUserId() AND CreatedDate >=: startD AND CreatedDate <=: endD ];
        
        // daily Visitor
        todaysVisitors = [SELECT Count() FROM vMarketListingActivity__c WHERE vMarket_Listing__r.OwnerId =: UserInfo.getUserId() AND CreatedDate >=: todayDate];
    }
    
    /**
     *  method for Buyers KPI
     */
     @testVisible
     private void developerBuyers() {
        String sucess = Label.vMarket_Success;
        String paid = 'Paid';
        //Date d = system.today();
        system.debug(' ==endD== ' + DT + ' == ownerId == ' + ownerId );
        string todayBuyerSoql = ' SELECT CreatedById, vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate, transfer_status__c, App_OwnerId__c FROM vMarketOrderItemLine__c ' + 
                                //' WHERE App_OwnerId__c =: ownerId AND CreatedDate >: d';
                                ' WHERE Order_Status__c =: sucess AND App_OwnerId__c =: ownerId ' +
                                ' AND CreatedDate >: DT';
                                
        string totalBuyerSOQL = ' SELECT CreatedById, vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate, transfer_status__c FROM vMarketOrderItemLine__c ' + 
                                ' WHERE Order_Status__c =: sucess AND App_OwnerId__c =: ownerId '+//AND transfer_status__c =: paid' +
                                ' AND CreatedDate >=: startD AND CreatedDate <=: endD ';
        
        
        // calculating todays buyers
        List<vMarketOrderItemLine__c> todayBuyerList = Database.Query(todayBuyerSoql);
        /*Set<Id> uniqueTodayBuyerSet = new Set<Id>();
        // Check Unique Buyer, remove duplicating buyer
        for (vMarketOrderItemLine__c item: todayBuyerList) {
            uniqueTodayBuyerSet.add(item.CreatedById); // contains distict BuyerId
        }
        todayBuyers = uniqueTodayBuyerSet.size();*/
        todayBuyers = todayBuyerList.size();

        // calculating total buyers
        List<vMarketOrderItemLine__c> totalBuyerList = Database.Query(totalBuyerSOQL);
        /*Set<Id> uniqueTotalBuyerSet = new Set<Id>();
        // Check Unique Buyer, remove duplicating buyer
        for (vMarketOrderItemLine__c item: totalBuyerList) {
            uniqueTotalBuyerSet.add(item.CreatedById); // contains distict BuyerId
        }
        monthlyBuyers = uniqueTotalBuyerSet.size();*/
        monthlyBuyers = totalBuyerList.size();


        // calculating todays buyers
        /*for(vMarketOrderItemLine__c OIL : Database.Query(todayBuyerSoql)) {
            todayBuyers++;
        }*/
        
        // calculating total buyers
        /*for(vMarketOrderItemLine__c OIL : Database.Query(totalBuyerSOQL)) {
            monthlyBuyers++;
        }*/
     
     }
     
    /**
     *  method for Earning KPI
     */
    @testVisible
    private void developerEarnings() {
        String sucess = Label.vMarket_Success;
        String paid = 'Paid';
        List<vMarketOrderItemLine__c> orderItemLineList = new List<vMarketOrderItemLine__c>(); //list to be passed for creating JSON earning data
        string todayEarningSOQL = 'SELECT vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate, transfer_status__c, App_OwnerId__c FROM vMarketOrderItemLine__c ' +
                                 ' WHERE Order_Status__c =: sucess AND App_OwnerId__c =: ownerId ' +// AND transfer_status__c =: paid' +
                                 ' AND CreatedDate >=: DT ' ;
                                 
        string monthlyEarningSOQL =  'SELECT vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate, transfer_status__c, App_OwnerId__c FROM vMarketOrderItemLine__c ' +
                                     ' WHERE Order_Status__c =: sucess AND App_OwnerId__c =: ownerId ' +//  AND transfer_status__c =: paid' +
                                     ' AND CreatedDate >=: startD AND CreatedDate <=: endD ';
        
        string totalEarningSOQL =  ' SELECT vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate, transfer_status__c, App_OwnerId__c FROM vMarketOrderItemLine__c ' +
                                    ' WHERE Order_Status__c =: sucess AND App_OwnerId__c =: ownerId ';//  AND transfer_status__c =: paid';
        
        system.debug(' ==startDate== ' + startD + ' == endDate == ' + endD );
        // calculating todays earning
        for(vMarketOrderItemLine__c OIL : Database.Query(todayEarningSOQL)) {
            todayEarnings += OIL.Price__c;
        }
        
        // calculating monthly earning
        for(vMarketOrderItemLine__c OIL : Database.Query(monthlyEarningSOQL)) {
            monthlyEarning += OIL.Price__c;
        }
        
        // calculating total earning
        for(vMarketOrderItemLine__c OIL : Database.Query(totalEarningSOQL)) {
            orderItemLineList.add(OIL);
            totalEarning += OIL.Price__c;
        }
        
        // JSON for 6 months Earning
         myTotalEarnings = JSONgeneratorController.generateJSONTotalEarned(orderItemLineList);
         system.debug(' === Total 6 months Earning JSON === ' + myTotalEarnings);
        
    }
    
    /**
     *  method to determine app solds
     */
    @testVisible
    private void developerAppSold() {
        string paid = Label.vMarket_Paid;
        string fail = 'failed';
        string appSoldSOQL = '  SELECT Id, Name, IsActive__c, ' + 
                            // ' (SELECT Id, vMarket_App__c, Order_Status__c, transfer_status__c, App_OwnerId__c FROM vMarket_Order_Item_Lines__r WHERE transfer_status__c !=: fail  ' + 
                             ' (SELECT Id, vMarket_App__c, Order_Status__c, transfer_status__c, App_OwnerId__c FROM vMarket_Order_Item_Lines__r WHERE transfer_status__c =: paid ' + 
                             ' AND CreatedDate >=: startD AND CreatedDate <=: endD) ' +
                             ' FROM vMarket_App__c WHERE OwnerId =: ownerId AND isActive__c = true';
        //vMarket_Order_Item_Lines__r) 
        map<String, Integer> app_OTLListMap = new Map<String, Integer>();
        system.debug(' ==startDate== ' + startD + ' == endDate == ' + endD );
        for(vMarket_App__c apps : Database.Query(appSoldSOQL)) {
            system.debug(' === apps: ' + apps);
            system.debug(' === appsOLT : ' + apps.vMarket_Order_Item_Lines__r);
            for(vMarketOrderItemLine__c OIL : apps.vMarket_Order_Item_Lines__r) {
                system.debug(' === OIL: ' + OIL);
                if(!app_OTLListMap.containsKey(apps.Id))
                    app_OTLListMap.put(apps.Id, 1);
            }
        }
        
        appSold = app_OTLListMap.size();        
    }

    /**
     * method for return JSON data for Sales Analytics
     */
    @testVisible
    private void salesAnalytics() {
        system.debug(' ====selected_frequency==== ' + selected_frequency);
        Set<Id> appIdSet = new Set<Id>();
        DateTime visitorStartD;
        Datetime visitorsEndD;
        String published = Label.vMarket_Published;
        if(selected_frequency == null || selected_frequency == '3') {
            visitorStartD = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
            visitorsEndD = visitorStartD.addMonths(-3);
        } else if(selected_frequency == '6') {
            visitorStartD = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
            visitorsEndD = visitorStartD.addMonths(-6); 
        } else if(selected_frequency == '12') {
            visitorStartD = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
            visitorsEndD = visitorStartD.addMonths(-12); 
        }
        
        system.debug(' ==visitorStartD== ' + visitorStartD);
        system.debug(' ==visitorsEndD== ' + visitorsEndD);
        system.debug(' ==ownerId== ' + ownerId);

        string salesSOQL = ' SELECT App__r.Name,  PageViews__c, InstallCount__c, App__c, App__r.ownerId, App__r.ApprovalStatus__c ' +
                           ' FROM vMarket_Listing__c WHERE App__r.ownerId = : ownerId ';
        //                   ' AND CreatedDate <=: visitorStartD AND CreatedDate >=: visitorsEndD ';  
        salesSOQL += ' AND App__r.ApprovalStatus__c =: published ';
        for(vMarket_Listing__c lists : Database.Query(salesSOQL)) {
            // createing My Visitors JSON
            appIdSet.add(lists.App__c);
        }
        system.debug('=== appIdSet === ' + appIdSet);
        // JSON for total Visitors
        //myTotalVisitors = JSONgeneratorController.monthlyVistorJSONGenerator(appIdSet);
        
        /* Mix data of visitors and buyers as per selected monthly frequency */
        if (selected_frequency == null)
            selected_frequency = '3';

        monthlyTotalVisitorsJSON = JSONgeneratorController.selectedMonthlyVisitorJSONGenerator(appIdSet, DEFAULT_MONTHLY_VALUE);
        myTotalVisitors = JSONgeneratorController.selectedMonthlyVistorAndBuyerJSONGen(appIdSet, Integer.valueOf(selected_frequency));
        system.debug(' === myTotalVisitors === ' + myTotalVisitors);
    }
}
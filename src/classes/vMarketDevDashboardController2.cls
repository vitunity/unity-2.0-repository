/**
 * @author	:	Puneet Mishra
 * @Desc	:	Controller for Developer Dashboard
 * 				Duplicate Copy of vMarketDevDashboardController
 */
public with sharing class vMarketDevDashboardController2 {
    
	public static Integer totalApps{get;set;} // total apps uploaded with 
	public static Integer appsSold{get;set;} // from successful Order
	public static Double totalEarned; // from sucessful Order Price
	public static String totalEarnedStr{get;set;} // from sucessful Order Price
	public static Integer totalVisitors{get;set;} //number of time user open App detail page
	public static Integer totalBuyers{get;set;} // number of time Application downloaded
	public static Integer todayVisitors{get;set;}// today : number of time App open
	public static Integer todayBuyers{get;set;} // today : number of time App Downloaded
	public static Decimal todayEarnings{get;set;}
	public static string bestSeller{get;set;}
	
	public static string earnedJSON{get;set;}
	public static string visitorJSON{get;set;}
	public Boolean isDev{
		get {
			User usr = [SELECT Id, Name, vMarket_User_Role__c FROM User WHERE Id =: userInfo.getUserId()];
			if(usr.vMarket_User_Role__c != Label.vMarket_Developer)
				return false;
			return true;
		}
		set;
	}
	
	@testVisible private String ownerId;
	
	{
		totalEarned = 0.0;
		todayVisitors = 0;
		todayBuyers = 0;
		todayEarnings = 0.0;
		totalApps = 0;
		totalVisitors = 0;
		totalBuyers = 0;
	}
    
    public vMarketDevDashboardController2() {
    	ownerId = userInfo.getUserId();
    }
    
    public void calculateKPI() {
    	
    	List<vMarket_Listing__c> listings = new List<vMarket_Listing__c>();
    	List<vMarketOrderItemLine__c> orderItemLine = new List<vMarketOrderItemLine__c>();
    	Set<Id> appIdSet = new Set<Id>();
    	DateTime DT = DateTime.newInstance(system.today().Year(), system.today().Month(), system.today().Day()); // today's Date
    	
    	// total number of App uploaded
    	totalApps = [SELECT COUNT() FROM vMarket_App__c WHERE OwnerId =:ownerId];
    	    	
    	listings = [SELECT App__r.Name,  PageViews__c, InstallCount__c, App__c,
	 					(Select Id, Name, CreatedDate From vMarketListingActivity__r), // total visitors today
	 					(Select Id, Name, CreatedDate From vMarket_ListingDownloads__r)
	 				FROM vMarket_Listing__c 
	 				WHERE OwnerId =: UserInfo.getUserId() ];
	 				
	 	orderItemLine = [	SELECT Id, Name, vMarket_App__c, vMarket_App__r.OwnerId, Price__c, CreatedDate 
			 				FROM vMarketOrderItemLine__c 
			 				WHERE Order_Status__c =: Label.vMarket_Success AND vMarket_App__r.OwnerId =: ownerId];
    	
    	// Number of Apps Sold
    	appsSold = orderItemLine.size();
    	// total number time App sold
    	totalBuyers = orderItemLine.size();
    	system.debug(' === listings === ' + listings);
    	if(!listings.isEmpty()) {
    		// JSON for Best Selling Apps
    		bestSeller = JSONgeneratorController.generateJSONBestSeller(listings);
	    	for(vMarket_Listing__c lists : listings) {
	    		// number of time app opened
	    		totalVisitors += (Integer)lists.PageViews__c;
	    		
	    		for(vMarketListingActivity__c lItem : lists.vMarketListingActivity__r) {
	    			if(lItem.CreatedDate >= DT) {
	    				// App Detail visited today
	    				todayVisitors ++;
	    			}
	    		}
	    		// populating appId to calculate Monthly Visitor KPI
	    		appIdSet.add(lists.App__c);
	    	}
	    }
    	
    	if(!orderItemLine.isEmpty()) {
    		for(vMarketOrderItemLine__c oLineItem : orderItemLine) {
	    		if(oLineItem.CreatedDate >= DT) { // If App Ordered Today
	    			todayBuyers ++;
	    			todayEarnings += oLineItem.Price__c;
	    		}
	    		totalEarned += oLineItem.Price__c;
	    	}
    	}
    	
    	totalEarnedStr = currencyAbbri(totalEarned);
    	
    	// JSON for Amount earned Monthly for Published Apps
	    	earnedJSON = JSONgeneratorController.generateJSONTotalEarned(orderItemLine);
    	
    	// Monthly Visitors
    	visitorJSON = JSONgeneratorController.monthlyVistorJSONGenerator(appIdSet);
    	
    	system.debug(' === bestSeller === ' + bestSeller);
    	system.debug(' === visitorJSON === ' + visitorJSON);
    	system.debug(' === todayBuyers === ' + todayBuyers + ' == todayVisitors == ' + todayVisitors + ' == todayEarnings == ' + todayEarnings);
    	
    }
    
    // method will add Abbrivation K for amount greater than 1000 and M for millions
    @TestVisible private static String currencyAbbri(Decimal amount) {
		
		if(amount >= 1000000 ) {
			return String.valueOf(amount/1000000) + 'M';
		} else if(amount >= 1000) {
			return String.valueOf(amount/1000) + 'K';
		} else {
			return String.valueOf(amount);
		}
		return String.valueOf(amount);
	}
}
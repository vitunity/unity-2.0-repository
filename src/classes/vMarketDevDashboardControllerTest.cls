@isTest
public with sharing class vMarketDevDashboardControllerTest {
    
    
    private static testMethod void vMarketDeveloperControllerTest() {
      test.StartTest();
      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      vMarketDevDashboardController dc = new vMarketDevDashboardController();     
      vMarketDevDashboardController.developerName = 'Abhishek';
      vMarketDevDashboardController.lastlogin = Date.today();

      dc.selected_frequency = '6';
      dc.salesAnalytics();         
      dc.selected_frequency = '12';
      dc.salesAnalytics();  
      
      System.debug(vMarketDevDashboardController.developerName);
      System.debug(vMarketDevDashboardController.lastlogin);
      dc.getMonthList();
      
      test.Stoptest();
    }
}
/**
 *  @author :       Puneet Mishra
 *  @description:   controller for Developer Help Section
 */
public without sharing class vMarketDevGuideComponentController {
    
    @testVisible private String searchQuery = 'SELECT Id, Name, Product_group__c FROM Product2 WHERE Product_Group__c ';
    public String productvalue{get;set;}
    public String VersionInfo{get;set;} // stored selected version
    
    public List<ContentVersion> lContentVersions{get;set;}
    
    public String getConDocId() {
        ID workspaceId = [SELECT Id FROM ContentWorkspace WHERE Name = 'VarianMarketPlace'][0].Id;
        return workspaceId;
    }
    
    public List<SelectOption> getVersions() {
        List<SelectOption> options = new List<SelectOption>();
        
        if(productValue == '' || productValue == null)
            return new List<SelectOption>();
        
        List<Product2> productSet = new List<Product2>();
        Set<Decimal> versionSet = new Set<Decimal>();
        List<selectoption> versionList = new List<selectoption>();
        
        if(productvalue == 'RapidPlan')
            productSet = [SELECT Id FROM Product2 WHERE Product_Group__c != null AND Product_Group__c =: 'Eclipse'];//productvalue];
        else 
            productSet = [SELECT Id FROM Product2 WHERE Product_Group__c != null AND Product_Group__c =: productvalue];
        /*for(ContentWorkspaceDoc conWorDoc :[Select c.ContentDocumentId From ContentWorkspaceDoc c where ContentWorkspace.Name =: productvalue]) {
            conDocID.add(conWorDoc.ContentDocumentId);
        }*/
        
        if(!productSet.isEmpty()) {
            Integer eclipsevers = Integer.valueOf(Label.vMarket_EclipseVersion);
            for(Product_Version__c ver : [SELECT Id, Version__c, Product__c FROM Product_Version__c WHERE Product__c IN: ProductSet ORDER BY Version__c ]) {
                //if(productvalue == 'Eclipse' && Decimal.valueOf(ver.Version__c) >= eclipsevers )
                if(Decimal.valueOf(ver.Version__c) >= eclipsevers )
                    versionSet.add(Decimal.ValueOf(ver.Version__c));
                //if(productvalue != 'Eclipse')
                    //versionSet.add(Decimal.ValueOf(ver.Version__c));
            }
        }
        
        for(Decimal d : versionSet) {
            versionList.add(new selectoption(String.ValueOf(d),String.ValueOf(d)));
        }
        
        if(versionList.isEmpty())
            return null;
        else
            return versionList;
    }
    
    public pageReference getProductDoc() {
        lContentVersions = new List<ContentVersion>();
        try {
            ID workspaceId;
            if(productvalue == 'RapidPlan')
                workspaceId = [SELECT Id FROM ContentWorkspace WHERE Name =: 'VarianMarketplace_RapidPlan'][0].Id;
            else if(productvalue == 'Eclipse')
                workspaceId = [SELECT Id FROM ContentWorkspace WHERE Name =: 'VarianMarketPlace'][0].Id;
            system.debug(' = Product Value = ' + productvalue + ' == product version == ' + VersionInfo);
            system.debug(' == contentDoc == ' + workspaceId);
            
            List<ContentVersion> verl = new List<ContentVersion>();
            verl = [SELECT Title, VersionNumber, Document_Version__c, Document_Language__c, Mutli_Languages__c, ContentDocumentId,Parent_Documentation__c,
                                                Date__c,Document_Type__c,Recently_Updated__c, Document_Number__c,Newly_Created__c, ContentUrl
                                         FROM ContentVersion 
                                         WHERE ContentDocumentId in (SELECT ContentDocumentId 
                                         FROM ContentWorkspaceDoc 
                                         WHERE ContentWorkspaceId = : workspaceId) 
                                            AND Document_Language__c =: Label.vMarketplaceLanguage
                                            AND Document_Version__c Like: VersionInfo];
            for(ContentVersion ver : verl) {
                lContentVersions.add(ver);
            }
        } catch(Exception e){
            //return new pageReference('/vMarketLostPage');
            system.debug(' exception => ' + e);
            lContentVersions = new List<ContentVersion>();
            return null;
        }        
        return null;
    }
    
}
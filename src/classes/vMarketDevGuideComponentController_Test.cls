/**
 *    @author         :        Puneet Mishra
 *    @description    :        test class for DevGuideComponentController
 */
@isTest(seeAllData = true)
public class vMarketDevGuideComponentController_Test{
    
    static Profile admin,portal;   
    static User customer1, customer2, developer1;
    static List<User> lstUserInsert;
    static Account account;
    static Contact contact1, contact2, contact3;
    static List<Contact> lstContactInsert;
    
    static {
        admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        
        account = vMarketDataUtility_Test.createAccount('test account', true);
        lstContactInsert = new List<Contact>();
        contact1 = vMarketDataUtility_Test.contact_data(account, false);
        contact1.MailingStreet = '53 Street';
        contact1.MailingCity = 'Palo Alto';
        contact1.MailingCountry = 'USA';
        contact1.MailingPostalCode = '94035';
        contact1.MailingState = 'CA';
        
        contact2 = vMarketDataUtility_Test.contact_data(account, false);
        contact2.MailingStreet = '660 N';
        contact2.MailingCity = 'Milpitas';
        contact2.MailingCountry = 'USA';
        contact2.MailingPostalCode = '94035';
        contact2.MailingState = 'CA';
        
        //lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
        
        lstContactInsert.add(contact1);
        lstContactInsert.add(contact2);
        insert lstContactInsert;
        
        lstUserInsert = new List<User>();
        lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
        lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
        //lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
        
        insert lstUserinsert;
    }
    
    public static testMethod void getVersions() {
        Test.StartTest();
            vMarketDevGuideComponentController  control = new vMarketDevGuideComponentController();
            control.getConDocId();
            control.productValue = 'Eclipse';
            control.getVersions();
            control.getProductDoc();
        Test.StopTest();
    }
}
/**
 *  @author    :  Puneet Mishra
 *  @desc    :  controller responsible of returning Loggedin Developer App Details
 */
 /* Class Modified by Abhishek K as Part of Globalisation VMT 25 */
public class vMarketDevMyAppsController extends vMarketBaseController{
   
   @testVisible private List<String> excludeStatus =  new List<String>{null,Label.vMarket_Draft};
   public String SelectedValue{get;set;} // Selected Date from DatePicker
   // return list of date range filter value to filter the Orders
    public List<SelectOption> datePicker {
        get{
            List<SelectOption> dateOption = new List<SelectOption>();//vMarket_LiveYear
            Integer currentYear = system.today().year();
            Integer yearDifference = currentYear - (Integer.valueOf(Label.vMarket_LiveYear));
            dateOption.add(new SelectOption('6M','past 6 months'));
            if(yearDifference != 0) {
                for(integer i = -yearDifference; i != 0; i++) {
                    dateOption.add(new SelectOption(String.valueOf(currentYear), String.valueOf(currentYear) ));
                    currentYear = currentYear - 1;
                }
            }
            system.debug(SelectedValue + ' === dateOption === ' + datePicker);
            return dateOption;
        } set;
    }
    
    public Integer appCount{
      get {
        return [SELECT COUNT()  FROM vMarket_App__c 
            WHERE  OwnerId =: UserInfo.getUserId() AND 
                 ApprovalStatus__c NOT IN:  excludeStatus];
      }
      set;
    }
    
    @testVisible private list<appDetailWrapper> appDetailsList{get;set;}
    
    public PageReference authorizeUser() {
      if(authenticateUserLogin()) {
        return null;
      } else {
        PageReference ref = new PageReference(Label.vMarket_vf_Error);
        ref.setRedirect(true);
        return ref;
      }
      return null;
    }
    
    public String getAppsByDeveloper() {
      
      Date startD, endD;
      // Setting the StartDate and End Date for Filter
        if(SelectedValue == '6M' || SelectedValue == null) {
            endD = Date.newInstance(system.today().year(), system.today().month(), system.today().day());
            endD = endD.addDays(1);
            startD = endD.addMonths(-6);
        } else {
            startD = Date.newInstance(Integer.valueOf(SelectedValue), 1, 1 );
            endD = Date.newInstance(Integer.valueOf(SelectedValue), 12, 31);
        }
      
      appDetailsList = new List<appDetailWrapper>();
      appDetailWrapper wrapper;
      String jsonAppDetail = '';
      Integer index = 1;
      String usrId = userInfo.getUserId();
      String appSOQL = 'SELECT Id,countries__C,subscription__c, Subscription_Price__c, Name, OwnerId, AppStatus__c, ApprovalStatus__c, Published_Date__c, Price__c, IsActive__c, CreatedDate, App_Category__c,App_Category__r.Name, ' +
               ' (SELECT Id, Order_Status__c, transfer_status__c, Price__c, Tax__c FROM vMarket_Order_Item_Lines__r WHERE transfer_status__c != \'' + label.vMarket_Failed + '\'), ' +
               ' (SELECT Id, PageViews__c, InstallCount__c, isActive__c, CreatedDate FROM Listings__r WHERE isActive__c = true ORDER By CreatedDate DESC) ' +
               ' FROM vMarket_App__c WHERE OwnerId =: usrId AND ApprovalStatus__c NOT IN:  excludeStatus ';
      
      appSOQL += ' AND CreatedDate >=: startD AND CreatedDate <=: endD ';         
      
      system.debug(' ===startD=== ' + startD + ' ===endD=== ' + endD);         
    system.debug(' ============= SelectedValue ============== ' + SelectedValue);               
      /*for(vMarket_App__c app : [  SELECT Id, Name, OwnerId, AppStatus__c, ApprovalStatus__c, Published_Date__c, Price__c, IsActive__c, CreatedDate, App_Category__c,App_Category__r.Name,
                        (SELECT Id, Order_Status__c, transfer_status__c, Price__c, Tax__c 
                         FROM vMarket_Order_Item_Lines__r 
                         WHERE transfer_status__c !=: label.vMarket_Failed),
                         
                         (SELECT Id, PageViews__c, InstallCount__c, isActive__c, CreatedDate 
                          FROM Listings__r 
                          WHERE isActive__c =: true ORDER By CreatedDate DESC)
                          
                    FROM vMarket_App__c 
                    WHERE OwnerId =: UserInfo.getUserId() AND 
                        ApprovalStatus__c NOT IN:  excludeStatus] ) {*/
     Map<Id,Integer> viewersMap = new Map<Id,Integer>();
     for(vMarket_Listing__c listing :[SELECT App__r.Name,  PageViews__c, InstallCount__c, App__c, ownerId FROM vMarket_Listing__c WHERE App__r.ownerId = : usrId])
     {
         viewersMap.put(listing.App__c,(listing.PageViews__c==null) ? 0:Integer.valueOf(listing.PageViews__c));
     }            
                        
      for(vMarket_App__c app : Database.query(appSOQL)) {
      
      Integer buyers = 0;
      Integer viewers = 0;
      system.debug(' ======== ' + app.Listings__r);
      system.debug(' ===publishedDate===== ' + app.Published_Date__c);
      
      Decimal revenue = 0;
      for(vMarketOrderItemLine__c OLI : app.vMarket_Order_Item_Lines__r) {
        if(String.IsnOtBlank(OLI.Order_Status__c) && OLI.Order_Status__c.equals(Label.vMarket_Success))
        {
            buyers++;
            revenue += OLI.Price__c;
        }
      }
      appDetailsList.add(new appDetailWrapper(app, viewersMap.get(app.id), buyers, revenue, index));                  
      index++;
    }
    system.debug(' ============= appDetailsList ============ ' + appDetailsList);
    jsonAppDetail = JSON.serialize(appDetailsList);
    system.debug(' ============= jsonAppDetail ============ ' + jsonAppDetail);
    return jsonAppDetail;
    }
    
    // method will be called when picklist value get changed, actionStatus
    public void searchApps() {
        system.debug(' == selected Value ' + SelectedValue);
        getAppsByDeveloper();
    }
    
    public class appDetailWrapper {
      public String index{get;set;}
      public String App{get;set;}
      public String appId{get;set;}
      public String appCategory{get;set;}
      public String viewers{get;set;}
      public String buyers{get;set;}
      public String price{get;set;}
      public String revenue{get;set;}
      public String publishedDate{get;set;}
      public String submission{get;set;}
      public String status{get;set;}
      public String country{get;set;}
      public vMarket_App__c apps{get;set;}
      
      public appDetailWrapper(vMarket_App__c app, Integer viewers, Integer buyers, Decimal revenue, Integer index) {
        this.index = string.valueOf(index);
        this.App = app.Name.escapeHtml4();
        this.appId = app.Id;
        this.appCategory = app.App_Category__r.Name;
        this.viewers = string.valueOf(viewers);
        this.buyers = string.valueOf(buyers);
        
        if(app.subscription__c) 
        {
        String subPrice = 'NA';
        if(String.IsNotBlank(app.Subscription_Price__c))
        {
        List<String> subPrices = app.Subscription_Price__c.split(';',-1);
        subPrice = 'M:&nbsp;'+subPrices.get(0)+'<br></br>Q:&nbsp;'+subPrices.get(1)+'<br></br>H:&nbsp;'+subPrices.get(2)+'<br></br>Y:&nbsp;'+subPrices.get(3);
        }
        this.price = string.valueOf(subPrice);
        }
        else this.price = string.valueOf(app.Price__c);
        
        this.revenue = string.valueOf(revenue);
        
        Date submisstionDate = date.newInstance(app.CreatedDate.year(), app.CreatedDate.month(), app.CreatedDate.day());
        this.submission = string.valueOf(submisstionDate);
        
        if(app.Published_Date__c != null) {
          date publishedDate = date.newInstance(app.Published_Date__c.year(), app.Published_Date__c.month(), app.Published_Date__c.day());
          this.publishedDate = string.valueOf(publishedDate);
        } else {
          this.publishedDate = '';
        }
        this.status = app.ApprovalStatus__c;
         /* Added by Abhishek K as Part of Globalisation VMT-25 for Displaying Countries of a Particular App*/
        String countryNames = '';
        Map<String,String> countriesMap = (new vMarket_Approval_Util()).countriesMap();
        if(String.isNotBlank(app.countries__C))
        {
        for(String con : app.countries__C.split(';',-1))
        {
        if(countriesMap.containsKey(con)) countryNames += '<div class=\'contryWrap\'>'+countriesMap.get(con)+'</div>';
        }
        }
        else countryNames='NA';
        
        this.country =  countryNames;  
        
        
      }
    }  
}
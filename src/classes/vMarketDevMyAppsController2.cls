public class vMarketDevMyAppsController2 extends vMarketBaseController {
    public String loginUser;
    public List<vMarket_App__c> appList;
    public Boolean loggedInUserRole{get;set;}
    public String appByDevJSON{
        get{
            List<String> excludeStatus = new List<String>{'null', ''};
            List<vMarket_App__c> apps = new List<vMarket_App__c>();
            apps = [ SELECT Id, Name, OwnerId, AppStatus__c, ApprovalStatus__c, Published_Date__c, Price__c, IsActive__c, CreatedDate 
                     FROM vMarket_App__c WHERE OwnerId =: UserInfo.getUserId() AND ApprovalStatus__c NOT IN:  excludeStatus];
            String JSON = JSONGeneratorController.generateMyAppJSON(apps) ;
            return JSON;
        }        
        set;
    }
    
    public vMarketDevMyAppsController2(){
        if(authenticateUserLogin()) {
            system.debug(' 11111111111111111 ');
            loggedInUserRole = true;
        } else if([SELECT id, vMarket_User_Role__c, ProfileId FROM User WHERE Id =: userinfo.getUserId()].vmarket_User_Role__c == Label.vMarket_Customer){
            system.debug(' 22222222222222222 ');
            loggedInUserRole = true;
        } else {
            system.debug(' 33333333333333333 ');
            loggedInUserRole = false;
        }
    }
    
    public Integer appCount{
        get{
            return [SELECT Count() FROM vMarket_App__c Where OwnerId =: UserInfo.getUserId() ];
        }
        set;
    }
    
    public String getAppsByDeveloper(){
        List<String> excludeStatus = new List<String>{'null', ''};
        appList = [ SELECT Id, Name, OwnerId, AppStatus__c, ApprovalStatus__c, Published_Date__c, Price__c, IsActive__c, CreatedDate FROM vMarket_App__c WHERE OwnerId =: UserInfo.getUserId() 
                    AND ApprovalStatus__c NOT IN:  excludeStatus];
        system.debug(' ==== appList ==== ' + appList);
        List<Map<String, String>> appObjMapList = new List<Map<String, String>>();      
        //appByDevJSON = JSONGeneratorController.generateMyAppJSON(appList);
        //system.debug(' =====>>>>> ' + appByDevJSON);
        for(vMarket_App__c appObj: appList) {
            Map<String, String> appObjMap = new Map<String, String>();
            appObjMap.put('appId',appObj.Id);
            appObjMap.put('app',appObj.Name);
            appObjMap.put('viewers','10');
            appObjMap.put('buyers','5');
            appObjMap.put('licenses','1');
            appObjMap.put('appPrice',string.valueOf(appObj.Price__c));
            appObjMap.put('revenue','USD 15000');
            appObjMap.put('submission',string.valueOf(appObj.CreatedDate));
            appObjMap.put('status',appObj.ApprovalStatus__c);
            appObjMapList.add(appObjMap);
        }
        
        String appObjJSON = JSON.serialize(appObjMapList);
        System.debug(appObjJSON);
        
        return appObjJSON;
    }    
    
}
@isTest
public with sharing class vMarketDevMyAppsControllerTest {
    
    
    private static testMethod void vMarketDevMyAppsControllerTest() {
      test.StartTest();
      
      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal;
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer;
      //userRole role = vMarketDataUtility_Test.CreateRole();
      
      System.RunAs(vMarketDataUtility_Test.getSystemAdmin())
        {
       System.debug('Inside####'+UserInfo.getUserId());
       portal = vMarketDataUtility_Test.getPortalProfile();
       
       developer= vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
        }
      System.debug('Outside####'+UserInfo.getUserId());
      System.debug('***'+portal.id);
      System.debug('***'+developer.Userrole.name);
      
      //developer.UserRoleId  = role.id;
      //update developer;
      
      
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      app.Published_Date__c = Date.today();
      update app;
      System.debug('@@@'+app.Createddate);
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      app = [Select id,createdDate,App_Category__r.Name,Name, Price__c,vMarket_App__c.ApprovalStatus__c, Published_Date__c from vmarket_app__C where id=:app.id];
      
          vMarketDevMyAppsController devApps = new vMarketDevMyAppsController();     
          devApps.searchApps();
          System.debug(devApps.datePicker);
          System.debug(devApps.appCount);
          System.debug(devapps.authorizeUser());
      vMarketDevMyAppsController.appDetailWrapper appDetail = new vMarketDevMyAppsController.appDetailWrapper(app,100,100,100.0,100);
      
      test.Stoptest();
    }
}
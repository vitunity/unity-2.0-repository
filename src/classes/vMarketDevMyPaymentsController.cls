/**
 *  @author    :  Puneet Mishra
 *  @desc    :  controller handle return all payment information to loggedin developers
 *  TODO     :  datepicker moved it to base controller, used in MyOrders controller, vMarketDevMyApps controller
 */
public with sharing class vMarketDevMyPaymentsController extends vMarketBaseController{
  
  @testVisible private list<paymentWrapper> paymentDetailsList{get;set;}
  
  @testVisible private List<String> excludeStatus =  new List<String>{};
   public String SelectedValue{get;set;} // Selected Date from DatePicker
   // return list of date range filter value to filter the Orders
    public List<SelectOption> datePicker {
        get{
            List<SelectOption> dateOption = new List<SelectOption>();//vMarket_LiveYear
            Integer currentYear = system.today().year();
            Integer yearDifference = currentYear - (Integer.valueOf(Label.vMarket_LiveYear));
            dateOption.add(new SelectOption('6M','past 6 months'));
            if(yearDifference != 0) {
                for(integer i = -yearDifference; i != 0; i++) {
                    dateOption.add(new SelectOption(String.valueOf(currentYear), String.valueOf(currentYear) ));
                    currentYear = currentYear - 1;
                }
            }
            system.debug(SelectedValue + ' === dateOption === ' + datePicker);
            return dateOption;
        } set;
    }
    
    public Integer orderCount{
      get {
        return [SELECT COUNT()  FROM vMarketOrderItemLine__c 
            WHERE  vMarket_Order_Item__r.OwnerId =: UserInfo.getUserId() AND 
                 transfer_status__c !=: label.vMarket_Failed];
      }
      set;
    }
    
    // method will be called when picklist value get changed, actionStatus
    public void searchApps() {
        system.debug(' == selected Value ' + SelectedValue);
        //getAppsByDeveloper();
    }
    
    public String getPaymentByDeveloper() {
      String usrId = userInfo.getUserId();
      Date startD, endD;
      // Setting the StartDate and End Date for Filter
        if(SelectedValue == '6M' || SelectedValue == null) {
            endD = Date.newInstance(system.today().year(), system.today().month(), system.today().day()+1);
            startD = endD.addMonths(-6);
        } else {
            startD = Date.newInstance(Integer.valueOf(SelectedValue), 1, 1 );
            endD = Date.newInstance(Integer.valueOf(SelectedValue), 12, 31);
        }
      system.debug(' ===startD=== ' + startD + ' ===endD=== ' + endD + ' ==== SelectedValue ==== ' + SelectedValue);         
    
      paymentDetailsList = new list<paymentWrapper>();
      paymentWrapper wrapper;
      string paymentJSON = '';
      
      List<vMarketOrderItemLine__c> testlist = [select id from vMarketOrderItemLine__c];
      System.debug('&&&&&'+[select id from vMarketOrderItemLine__c]);
      
      //payment is identified from Order Line Items, so query should be on Order Line Item without failed transfers and fetch detail as Parent information
      string paymentSOQL = ' SELECT Id, Name, CreatedDate, IsSubscribed__c,CreatedBy.Email,Created_By_Email__c, Month__c, Order_Status__c, vMarket_Order_Item__c, transfer_status__c, ' +
                 ' Price__c, tax__c, vMarket_App__r.ApprovalStatus__c, vMarket_App__r.OwnerId, ' + 
                 ' vMarket_App__c, vMarket_App__r.Name, vMarket_App__r.App_Category__c, vMarket_App__r.App_Category__r.Name  FROM vMarketOrderItemLine__c ' +
                 ' WHERE transfer_status__c = \'' + label.vMarket_Paid + '\' ';//+ '\' OR Order_Status__c !=  '
      
      paymentSOQL += ' AND vMarket_App__r.OwnerId =: usrId ';
      
      // adding date range logic
     paymentSOQL += ' AND CreatedDate >=: startD AND CreatedDate <=: endD ';
      
      system.debug(' ==== UserIdCurrent ==== ' + usrId);
      system.debug(' ==== paymentSOQL ==== ' + paymentSOQL);
      
      for(vMarketOrderItemLine__c OLI : Database.Query(paymentSOQL)) {
        wrapper = new paymentWrapper(OLI);
        system.debug(' ==== Buyer Email ==== ' + OLI.Created_By_Email__c);
        paymentDetailsList.add(wrapper);
      }
      system.debug(' ==== paymentDetailsList ==== ' + paymentDetailsList);
    paymentJSON = JSON.serialize(paymentDetailsList);
    system.debug(' ===== jsonAppDetail ===== ' + paymentJSON);
      return paymentJSON;
    }
  
  // wrapper for order, payment
  public class paymentWrapper{
    public string appId{get;set;}
    public string appName{get;set;}
    public string appcategory{get;set;}
    public string buyers{get;set;}
    public string amount{get;set;}
    public string buyDate{get;set;}
    public string status{get;set;}
    public string appStatus{get;set;}
    public string orderId{get;set;}
    
    
    public paymentWrapper(vMarketOrderItemLine__c OLI) {
      this.appId = OLI.vMarket_App__c;
      this.appName = OLI.vMarket_App__r.Name;
      this.appcategory = OLI.vMarket_App__r.App_Category__r.Name;      
      //this.buyers = string.valueOf(OLI.CreatedBy.Email);      
      this.buyers = String.isBlank(string.valueOf(OLI.Created_By_Email__c)) ?'':'<a href=\'mailTo:'+ string.valueOf(OLI.Created_By_Email__c)+'\'>'+string.valueOf(OLI.Created_By_Email__c)+'</a>';
      
      this.status = string.valueOf(OLI.Order_Status__c);
      this.amount = string.valueOf(OLI.Price__c);
      this.orderId = string.valueOf(OLI.id);
      
      Date purchaseDate = date.newInstance(OLI.CreatedDate.year(), OLI.CreatedDate.month(), OLI.CreatedDate.day());
        this.buyDate = string.valueOf(purchaseDate);
      this.status = OLI.transfer_status__c;
      this.appStatus = OLI.vMarket_App__r.ApprovalStatus__c;
    }
  }
}
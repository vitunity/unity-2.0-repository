@isTest
public with sharing class vMarketDevMyPaymentsControllerTest {
    private static testMethod void vMarketDevMyPaymentsControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      vMarketDevMyPaymentsController dc = new vMarketDevMyPaymentsController();     
      
      orderLinetem = [Select id,name,CreatedDate,vMarket_App__r.Name, Created_By_Email__c, vMarket_App__r.App_Category__r.Name,vMarket_App__r.id,Order_Status__c, Price__c, transfer_status__c,vMarket_App__r.ApprovalStatus__c from vMarketOrderItemLine__c where id = :orderLinetem.id limit 1];
      dc.getPaymentByDeveloper();
      System.debug(dc.datePicker);
      System.debug(dc.orderCount);
      vMarketDevMyPaymentsController.paymentWrapper paymentWrapper = new vMarketDevMyPaymentsController.paymentWrapper(orderLinetem);
        
      test.Stoptest();
    }
}
@isTest
public with sharing class vMarketDevUploadAppControllerTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer = new User();
      
            
      
      //developer = [Select id from user where profileId = :portal.id limit 1];
      /*User u = vMarketDataUtility_Test.getSystemAdmin();
      System.runAs(u)
      {
      developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
      }*/
      developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
      VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US', Documents__c='510K Form');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK', Documents__c='510K Form');
        insert uk;
        VMarketCountry__c nl = new VMarketCountry__c(name='Netherlands', Code__c='NL', Documents__c='510K Form');
        insert nl;
      
      
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer.Id);
      
     // vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      //vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      vMarketDataUtility_Test.createDeveloperStripeInfo();
      
      app.isActive__c = true;
        insert app;
      //update app;
      //orderLinetem.transfer_status__C = 'paid';
      //update orderLinetem;
      
      Test.setCurrentPageReference(new PageReference('Page.vMarketDevUploadAppPage')); 
      System.currentPageReference().getParameters().put('selectedcountries', 'US;GB;NL');
        System.currentPageReference().getParameters().put('appId', app.id);
        
       Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        insert a1;
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        insert a2;
      vMarketDevUploadAppController dc = new vMarketDevUploadAppController();
      
      dc.selectedOpt = 'Subscription';
      System.debug(dc.getAppType());
      dc.checkSelectedValue(); 
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      System.debug(dc.getIsAccessible());
      System.debug(dc.getAppStatus());
      System.debug(dc.getDevAppCategories());
      System.debug(dc.getDevAppCategories());
      System.debug('^^^^^'+developer.id);
      System.debug('^^^^^'+[select Id, Stripe_Id__c, Stripe_User__c from vMarket_Developer_Stripe_Info__c]);
      
      
      dc.clearcountry();
      dc.getListOfCountries();
      dc.getselValue();
      dc.setselValue(new String[]{'US','GB','NL'});      
      dc.changecountry();
      dc.populatecountry();
      
      dc.new_app_obj = app;
      dc.attached_Baner = vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false);
      dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','expert',false);
      dc.attached_510k = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','510k',false);
      
      List<vMarketDevUploadAppController.legalinfo> legals = new List<vMarketDevUploadAppController.legalinfo>();

    legals.add(new vMarketDevUploadAppController.legalinfo('US','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    legals.add(new vMarketDevUploadAppController.legalinfo('UK;NL;','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    //legals.add(new vMarketDevUploadAppController.legalinfo('NL','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        dc.encryptionType_Standard=true;
        dc.encryptionType_IOS=true;
        dc.encryptionType_Proprietary=true;
        dc.encryptionType_Other=true;
        dc.legalinfos = legals;
        dc.saveAppDetails();
        dc.saveAppDetails1();      
        dc.loadBack();
        dc.onload();
        dc.test();
      
             
      test.Stoptest();
    }
     private static testMethod void vMarketDeveloperControllerTestNeg() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer = new User();      
    
      developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
      VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US', Documents__c='510K Form');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK', Documents__c='510K Form');
        insert uk;
        VMarketCountry__c nl = new VMarketCountry__c(name='Netherlands', Code__c='NL', Documents__c='510K Form');
        insert nl;
      
      
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer.Id);
      
  
      vMarketDataUtility_Test.createDeveloperStripeInfo();
      
      app.isActive__c = true;
        insert app;
     
      
      Test.setCurrentPageReference(new PageReference('Page.vMarketDevUploadAppPage')); 
      System.currentPageReference().getParameters().put('selectedcountries', 'US;GB;NL');
        System.currentPageReference().getParameters().put('appId', app.id);
      
      vMarketDevUploadAppController dc = new vMarketDevUploadAppController();
      
      dc.selectedOpt = 'Free';
      System.debug(dc.getAppType());
      dc.checkSelectedValue(); 
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      System.debug(dc.getIsAccessible());
      System.debug(dc.getAppStatus());
      System.debug(dc.getDevAppCategories());
      System.debug(dc.getDevAppCategories());
      System.debug('^^^^^'+developer.id);
      System.debug('^^^^^'+[select Id, Stripe_Id__c, Stripe_User__c from vMarket_Developer_Stripe_Info__c]);
      
      
      dc.clearcountry();
      dc.getListOfCountries();
      dc.getselValue();
      dc.setselValue(new String[]{'US','GB','NL'});      
      dc.changecountry();
      dc.populatecountry();
      
      dc.new_app_obj = app;
      dc.attached_Baner = vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false);
      dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','expert',false);
      dc.attached_510k = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','510k',false);
      
      List<vMarketDevUploadAppController.legalinfo> legals = new List<vMarketDevUploadAppController.legalinfo>();

    legals.add(new vMarketDevUploadAppController.legalinfo('US','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    legals.add(new vMarketDevUploadAppController.legalinfo('UK;NL;','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    //legals.add(new vMarketDevUploadAppController.legalinfo('NL','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
        dc.encryptionType_Standard=true;
        dc.encryptionType_IOS=true;
        dc.encryptionType_Proprietary=true;
        dc.encryptionType_Other=true;
        dc.legalinfos = legals;
        dc.saveAppDetails();
        dc.saveAppDetails1();      
        dc.loadBack();
        dc.test();
      
             
      test.Stoptest();
}
}
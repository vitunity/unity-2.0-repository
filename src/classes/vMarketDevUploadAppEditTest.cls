@isTest
public with sharing class vMarketDevUploadAppEditTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      vMarketAppAsset__c appAsset = vMarketDataUtility_Test.createAppAssetTest(app,true);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      app.isActive__c = true;
      app.IsFree__c=true;
      app.countries__c='US;GB;NL';  
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      PageReference myVfPage = Page.vMarketDevAppEdit;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('appId',app.Id);
      ApexPages.currentPage().getParameters().put('name','Abhishek');
      ApexPages.currentPage().getParameters().put('phone','7830070480');
      ApexPages.currentPage().getParameters().put('email','abhik@yopmail.com');
      ApexPages.currentPage().getParameters().put('website','https://t@g.com');
      ApexPages.currentPage().getParameters().put('dev_support','this is my dev support');
      
      
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','kNumber_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/png','Logo_logo',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','AppGuide_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Banner_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Opinion_attach',true);
      
      vMarketDevUploadAppEdit dc = new vMarketDevUploadAppEdit();     
      //dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','attach',true);
      /*vMarketDataUtility_Test.createAttachmentTest(app.id,'content/png','ogo',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','guide',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','banner',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Opinion',true);
      */
      System.debug(dc.getIsViewableForDev());
      System.debug(dc.getDevAppCategories());
      System.debug(dc.getFileId());
      System.debug(dc.getAppStatus());
      System.debug(dc.getAppCategoryList());
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      System.debug(dc.getIsAccessible());
      dc.encryptionType_Standard=true;
      dc.encryptionType_IOS=true;
      dc.encryptionType_Proprietary=true;
      dc.encryptionType_Other=true;
      dc.saveAppDetails();
      dc.saveDevSupportDetails();
       
      test.Stoptest();
    }
    
    private static testMethod void TestSubscription() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      vMarketAppAsset__c appAsset = vMarketDataUtility_Test.createAppAssetTest(app,true);
      
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(true,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,true,true);
      
      app.isActive__c = true;
      app.IsFree__c=true;
      app.countries__c='US;GB;NL';
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      PageReference myVfPage = Page.vMarketDevAppEdit;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('appId',app.Id);
      ApexPages.currentPage().getParameters().put('name','Abhishek');
      ApexPages.currentPage().getParameters().put('phone','7830070480');
      ApexPages.currentPage().getParameters().put('email','abhik@yopmail.com');
      ApexPages.currentPage().getParameters().put('website','https://t@g.com');
      ApexPages.currentPage().getParameters().put('dev_support','this is my dev support');
      
      
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','kNumber_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/png','Logo_logo',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','AppGuide_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Banner_attach',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Opinion_attach',true);
      
      vMarketDevUploadAppEdit dc = new vMarketDevUploadAppEdit();     
      //dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','attach',true);
      /*vMarketDataUtility_Test.createAttachmentTest(app.id,'content/png','ogo',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','guide',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','banner',true);
      vMarketDataUtility_Test.createAttachmentTest(app.id,'content/pdf','Opinion',true);
      */
      dc.getAppType();
      dc.selectedOpt='Subscription';
      System.debug(dc.getIsViewableForDev());
      System.debug(dc.getDevAppCategories());
      System.debug(dc.getFileId());
      System.debug(dc.getAppStatus());
      System.debug(dc.getAppCategoryList());
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      System.debug(dc.getIsAccessible());
      
      dc.encryptionType_Standard=true;
      dc.encryptionType_IOS=true;
      dc.encryptionType_Proprietary=true;
      dc.encryptionType_Other=true;
      dc.saveAppDetails();
      dc.saveDevSupportDetails();
       
      test.Stoptest();
    }
    
    private static testMethod void vMarketDeveloperAppLoadTest() {
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, true);
      User developer = new User();
      
            
      
      //developer = [Select id from user where profileId = :portal.id limit 1];
      /*User u = vMarketDataUtility_Test.getSystemAdmin();
      System.runAs(u)
      {
      developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
      }*/
      developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', true, portal.Id, contact.Id, 'Developer');
      VMarketCountry__c us = new VMarketCountry__c(name='United States', Code__c='US', Documents__c='510K Form');
        insert us;
        VMarketCountry__c uk = new VMarketCountry__c(name='United Kingdom', Code__c='UK', Documents__c='510K Form');
        insert uk;
        VMarketCountry__c nl = new VMarketCountry__c(name='Netherlands', Code__c='NL', Documents__c='510K Form');
        insert nl;
      
      
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, false, developer.Id);
      
     // vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      //vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      vMarketDataUtility_Test.createDeveloperStripeInfo();
      app.VMarket_Type_of_Encr__c='Standard (HTTPS, SSL, and other recognized encryption standards as noted above)#iOS or macOS#Proprietary or non-standard#Other';
      app.isActive__c = true;
        app.countries__c='US;GB;NL';
       app.subscription__c=true;
       app.Subscription_Price__c='500;300;100;50';
        insert app;
      //update app;
      //orderLinetem.transfer_status__C = 'paid';
      //update orderLinetem;
      
      Test.setCurrentPageReference(new PageReference('Page.vMarketDevUploadAppPage')); 
      System.currentPageReference().getParameters().put('selectedcountries', 'US;GB;NL');
        System.currentPageReference().getParameters().put('appId', app.id);
        
       Attachment a1 = new Attachment(parentid=app.id,name='CCATS_',body=Blob.valueOf('abhi'));
        insert a1;
        Attachment a2 = new Attachment(parentid=app.id,name='TechnicalSpec_',body=Blob.valueOf('abhi'));
        insert a2;
      vMarketDevUploadAppEdit dc = new vMarketDevUploadAppEdit();
      
      dc.selectedOpt = 'Subscription';
      System.debug(dc.getAppType());
     
      System.debug(dc.getAuthenticated());
      System.debug(dc.getRole());
      System.debug(dc.getIsAccessible());
      System.debug(dc.getAppStatus());
      System.debug(dc.getDevAppCategories());
      System.debug(dc.getDevAppCategories());
      System.debug('^^^^^'+developer.id);
      System.debug('^^^^^'+[select Id, Stripe_Id__c, Stripe_User__c from vMarket_Developer_Stripe_Info__c]);
      
      
      dc.clearcountry();
      dc.getListOfCountries();
      dc.getselValue();
     dc.setselValue(new String[]{'US','GB','NL'}); 
      dc.changecountry();
      dc.populatecountry();
      
      dc.new_app_obj = app;
      dc.attached_appBanner  = vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false);
      dc.attached_ExpertOpioion = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','expert',false);
      dc.attached_userGuidePDF = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','AppGuide',false);
      dc.attached_510k = vMarketDataUtility_Test.createAttachmentTest('','Content/PDF','510k',false);
      test.StartTest();   
      List<vMarketDevUploadAppEdit.legalinfo> legals = new List<vMarketDevUploadAppEdit.legalinfo>();
		legals.add(new vMarketDevUploadAppEdit.legalinfo('US','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    	legals.add(new vMarketDevUploadAppEdit.legalinfo('UK;NL;','Test','Test',new List<Attachment>{vMarketDataUtility_Test.createAttachmentTest('','Content/JPG','banner',false)},'Test','Test',true,true));
    	dc.encryptionType_Standard=true;
        dc.encryptionType_IOS=true;
        dc.encryptionType_Proprietary=true;
        dc.encryptionType_Other=true;
        dc.legalinfos = legals;
        dc.encryptionTypeOther='test';
        dc.saveAppDetails();
        dc.saveDevSupportDetails();
             
      test.Stoptest();
    }
}
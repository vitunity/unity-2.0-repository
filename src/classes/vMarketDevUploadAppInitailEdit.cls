public class vMarketDevUploadAppInitailEdit  extends vMarketBaseController {
    public String appId {get;set;}
    public String AppVideoId {get;set;}
    public String appSourceId {get;set;}
    public Attachment attached_logo {get;set;}
    public Attachment attached_userGuidePDF {get;set;}
    //Selected count
    public Integer FileCount {get; set;}
    public List<Attachment> allFileList {get; set;}
    public List<Attachment> fileList {get; set;}
    public vMarket_App__c new_app_obj {get;set;}
    public vMarketAppAsset__c newAppAssert {get;set;}
    
    public String selectedOpt{get;set;/*{
            system.debug(' new_app_obj ' + new_app_obj);
            if(new_app_obj.IsFree__c)
                this.selectedOpt = 'Free';
            else if(new_app_obj.subscription__c)
                this.selectedOpt = 'Subscription';
            else if(!new_app_obj.subscription__c && !new_app_obj.IsFree__c)
                this.selectedOpt = 'One Time';
        }*/
    }
    public List<SelectOption> getAppType() {
        system.debug(' FIRST ');
        List<SelectOption> option = new List<SelectOption>();
        option.add(new SelectOption('Free', 'Free'));
        option.add(new SelectOption('Subscription', 'Subscription'));
        option.add(new SelectOption('One Time', 'One Time'));
        return option;
    }

    public vMarketDevUploadAppInitailEdit() {
        system.debug(' SECOND ');
        appId = ApexPages.currentPage().getParameters().get('appId');
        appSourceId = ApexPages.currentPage().getParameters().get('appSourceId');
        new_app_obj = new vMarket_App__c();
        new_app_obj = [Select Name, App_Category__c, Id, ApprovalStatus__c,
                        AppStatus__c, IsActive__c,
                        Long_Description__c, Price__c,Key_features__c, Short_Description__c, subscription__c, IsFree__c 
                        from vMarket_App__c 
                        where Id=:appId  limit 1];
        if(new_app_obj.IsFree__c)
            selectedOpt = 'Free';
        else if(new_app_obj.subscription__c)
            selectedOpt = 'Subscription';
        else if(!new_app_obj.IsFree__c && !new_app_obj.subscription__c)
            selectedOpt = 'One Time';
        
        attached_logo = new Attachment();
        attached_userGuidePDF = new Attachment();
        reinitiaze();
    }

    public void reinitiaze() {
        //Initialize
        FileCount = 4;
        allFileList = new List<Attachment>();
        for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
            allFileList.add(new Attachment()) ; 
            
        newAppAssert = new vMarketAppAsset__c();
       List<vMarketAppAsset__c> appAssets = [Select Id, AppVideoID__c, vMarket_App__c 
                            from vMarketAppAsset__c where vMarket_App__c =:appId ];
            
            if(appAssets.size()>0) newAppAssert = appAssets[0];
        
       // attached_logo = [select Id, Name, Body, ContentType  from Attachment where parentId =:appId and ContentType != 'application/pdf' limit 1]; 
        
       // attached_userGuidePDF = [select Id, Name, Body, ContentType  from Attachment where parentId =:appId and ContentType =:'application/pdf' limit 1]; 
        for(Attachment a :[select Name, parentid, ContentType from Attachment where parentid=:appId] )
        {
            System.debug('===='+a.Name);
            if(a.Name.startsWith('Logo_')){
                attached_logo = a;
            }            
            else if(a.Name.startsWith('AppGuide_')){
                attached_userGuidePDF = a;
            }
           
        }
        
         if (newAppAssert.Id!=null) {
        allFileList = [Select Id, parentId, Name, Body
                            from Attachment 
                            where parentId =: newAppAssert.Id];
                            }
    }
    
    
    public override Boolean getAuthenticated() {
    return super.getAuthenticated();
    }
    
    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer')
            return true;
        else
            return false;
    }

    public String getFileId() {
        String fileId = '';
        List<Attachment> attachedFiles = [select Id from Attachment where parentId =:appId order By LastModifiedDate DESC limit 1];
        if( attachedFiles != null && attachedFiles.size() > 0 ) {
            fileId = attachedFiles[0].Id;
        }
        return fileId;    
     }


    public List<SelectOption> getAppStatus() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult aStatus = vMarket_App__c.AppStatus__c.getDescribe();
        List<Schema.PicklistEntry> appStatusValues = aStatus.getPicklistValues();

        for (Schema.PicklistEntry status : appStatusValues)
        { 
            options.add(new SelectOption(status.getLabel(), status.getValue())); 
        }
        return options;
    }
    
    public List<SelectOption> getDevAppCategories() {
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category__c> app_categories = [select Name from vMarket_App_Category__c];
        
        options.add(new SelectOption('','Select Category'));
        for(vMarket_App_Category__c cat:app_categories) {
            options.add(new SelectOption(cat.Id,cat.Name));
        }
        return options;
    }
    
    public pageReference setSelectedValue() {
        system.debug(' ==== selectedOpt ==== ' + selectedOpt);
        return null;
    }

    public pageReference saveAppDetails() {
        if (true) {
            
            new_app_obj.ApprovalStatus__c = 'Submitted';
            new_app_obj.IsActive__c = true;
            system.debug('==selectedOpt =='+selectedOpt);
            system.debug('==new_app_obj.IsFree__c =='+new_app_obj.IsFree__c);
            system.debug('==new_app_obj.subscription__c =='+new_app_obj.subscription__c);
            if(selectedOpt == 'Free'){
                new_app_obj.Price__c = 0.0;
                new_app_obj.IsFree__c = true;
                new_app_obj.subscription__c = false;
            }
            if(selectedOpt == 'Subscription'){
                new_app_obj.subscription__c = true;
                new_app_obj.IsFree__c = false;
            }  
            Update new_app_obj;
            
           
            String AppId = new_app_obj.id;
            //attached_logo.parentid= AppId;
            attached_logo.name = 'Logo_'+attached_logo.name;
            attached_logo.ContentType = 'image/png';
            system.debug(' ****************** ' + attached_logo.Name);
            update attached_logo;
          /*  update newAppAssert;
            attached_userGuidePDF.ContentType = 'application/pdf';
            system.debug(' ****************** ' + attached_userGuidePDF.Name);
            update attached_userGuidePDF; */
            
            //system.debug('==allFileList=='+allFileList);
            String accId = newAppAssert.Id;
            List<Attachment> listToInsert = new List<Attachment>() ;
            Integer count = 0;
            system.debug('==allFileList=='+allFileList);
            /*for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++) {
                if (!allFileList.isEmpty()) {
                    if(allFileList[0].name != '' && allFileList[0].name != '' && allFileList[0].body != null) {
                        listToInsert.add(new Attachment(parentId = newAppAssert.id, name = allFileList[0].name, body = allFileList[0].body)) ;
                        allFileList.remove(0);
                        count += 1;
                    }
                }
            }
            for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                allFileList.add(new Attachment()) ;

            //Inserting attachments
            if(listToInsert.size() > 0) {
            allFileList = new List<Attachment>();
                update listToInsert ;
                
                listToInsert = new List<Attachment>() ;
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, count + ' file(s) are uploaded successfully'));
            }*/
            if(allFileList.size()>0)
            update allFileList;
        }else {
            allFileList.clear();
            //for(Integer i = 1 ; i <= Integer.valueOf(FileCount) ; i++)
                //allFileList.add(new Attachment()) ;
            //ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please, upload atleast one image.'));
        }
            
            PageReference pf = new PageReference('/vMarketDevAppPackageUpload?appSourceId='+appSourceId);
            return pf;
            /*pageReference pgRef = new PageReference('/vMarketDevAppDetails');
        
            pgRef.getParameters().clear();
            pgRef.getParameters().put('appId', appId);  
            pgRef.setRedirect(true);
            return pgRef;*/
    }
}
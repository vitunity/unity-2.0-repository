/***********************************************************************
* Author         : N/A
* Created Date: N/A
Project/Story/Inc/Task : N/A
Description: 
This is a controller for Uploading Packages while Creating App on MarketPlace Using Visual Force Page.

Change Log:
Date/Modified By Name/Task or Story or Inc # /Description of Change
Nov 24 2017 -  Abhishek Kolipey - CHG0119045 - Added Method for Polling to Check if Chatter File is Uploaded
************************************************************************/


public without sharing class vMarketDevUploadAppPackageController extends vMarketBaseController{
    public String appSourceId{get;set;}
    public String appId{get;set;}
    public string status {get;set;}
    public vMarket_App__c currentAppObj{get;set;}
    public vMarketAppSource__c appSource{get;set;}
    public List<Attachment> allFileList {get; set;}
    public vMarketAppAsset__c appAsset{get; set;}
    public Attachment attached_userGuidePDF {get;set;}
    public Attachment attached_logo {get;set;}
    public Attachment existing_logo {get;set;}
    public Integer FileCount {get; set;}
    public List<vMarketAppSource__c> appSourceList {get;set;}
    public string SelSourceRecID {get;set;}
    public vMarketAppSource__c sourceRecord {get;set;}
    public List<String> selectedCatVersions {get;set;}
    public String selectedCatVersions1 {get;set;}
    public Integer appSourceSize {get;set;}
    public set<String> appSourceSet {get;set;}
    public map<String,String> versionCategoryMap{get;set;}
    public map<Id,Id> appSourceFiles{get;set;}
    public boolean success{get;set;}
    public boolean logoExists {get;set;}

    public vMarketDevUploadAppPackageController(){
        //Get App ID
        appSourceFiles = new map<Id,Id>();
        appSourceId = ApexPages.currentPage().getParameters().get('appSourceId');
        //vMarket_App__c = [Select App_Category__c, isActive__c from vMarket_App_Category_Version__c];//App_Category__c
        appSource = [Select App_Category__c, Id, App_Category_Version__c, vMarket_App__c from vMarketAppSource__c where Id=:appSourceId limit 1];
        //appAsset =[Select Id, vMarket_App__c, AppVideoID__c from vMarketAppAsset__c where vMarket_App__c =: appId];
        currentAppObj = [Select App_Category__c, Id, ApprovalStatus__c from vMarket_App__c where Id=:appSource.vMarket_App__c];
        appSourceList = new List<vMarketAppSource__c>();
        appId = appSource.vMarket_App__c; 
        sourceRecord = new vMarketAppSource__c();
        
        appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:appId and Id !=: appSourceId and (App_Category_Version__c!=null or App_Category_Version__c!='')];
       
       system.debug('==appSourceList =='+appSourceList);
       appSourceSize = appSourceList.size(); 
       //attached_logo = new Attachment();
       
       for(feeditem a :[select id, RelatedRecordId ,parentid from feeditem where parentid IN :appSourceList])
       {
           /* System.debug('===='+a.ContentType);
            existing_logo = a;
            logoExists = true;*/
            System.debug('-----1'+appSourceFiles);
            if(appSourceFiles.keyset()!=null && !appSourceFiles.keyset().contains(a.parentid)) 
            {
                system.debug('===='+a.id);
                appSourceFiles.put(a.parentid,a.RelatedRecordId);
            }
            System.debug('-----2'+appSourceFiles);
       }
    }

    public Boolean getIsViewableForDev() {
        List<vMarket_App__c> apps = [select Id From vMarket_App__c Where Id=:appId And OwnerId=:UserInfo.getUserId()];
        if(!apps.isEmpty()) 
            return true;
        else
            return false;
    }

    public override Boolean getAuthenticated() {
        return super.getAuthenticated();
    }
    
    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer' && getIsViewableForDev())
            return true;
        else
            return false;
    }
        
    public List<SelectOption> getAppCategoryVersions() {
        appSourceSet = new set<String>();
        List<String> appCatVerList = new list<String>();
          
        for(vMarketAppSource__c aps :appSourceList){
            if(aps.App_Category_Version__c != null && aps.App_Category_Version__c.contains(';')){
                appCatVerList = aps.App_Category_Version__c.split(';');
                for(string st :appCatVerList){
                    appSourceSet.add(st);
                }
            }else{
                appSourceSet.add(aps.App_Category_Version__c);
            }
        }
        
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category_Version__c> app_categories_versions = [select Name,App_Category__c from vMarket_App_Category_Version__c 
                                                                        where App_Category__c=:currentAppObj.App_Category__c
                                                                        And Name NOT IN:appSourceSet];
          /* Added by Abhishek K to Add Non Selectable Options*/
        if(app_categories_versions.size()>0) options.add(new SelectOption('Select Category Version','Select Category Version',true));
        else options.add(new SelectOption('Select Category Version','No Category Versions Available',true));
        
        for(vMarket_App_Category_Version__c catVersion:app_categories_versions) {
            options.add(new SelectOption(catVersion.Name,catVersion.Name));
        }
        return options;
    }
    
    public pagereference deleteSource(){
        system.debug('==SelSourceRecID =='+SelSourceRecID );
        vMarketAppSource__c appScr = new  vMarketAppSource__c();
        appScr = [Select Id from vMarketAppSource__c where Id=:SelSourceRecID ]; 
        delete appScr; 
        
        appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:appId and Id !=: appSourceId];
        appSourceSize = appSourceList.size();
        system.debug('==appSourceSize=='+appSourceSize);
        pageReference pgRef = ApexPages.currentPage();
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appSourceId', appSourceId);  
        pgRef.setRedirect(true);
        return pgRef;
              
    }
    public void selectedAppCatVersions(){
        system.debug('==selectedCatVersions1=='+ selectedCatVersions1);
        system.debug('==selectedCatVersions=='+ selectedCatVersions);
        
    }
    public PageReference createAppSource() {
        String selected = '';
        Boolean Start = true;
        system.debug(' ==== appSource.App_Category_Version__c ' + appSource.App_Category_Version__c + ' = ' + appSource.App_Category_Version__c);
        system.debug('==selectedCatVersions=='+selectedCatVersions);
        String cIds = apexpages.currentpage().getparameters().get('selectedCatVersions1');
        system.debug('-------------- Here -----------------'+cIds);
        if(!selectedCatVersions.isEmpty()) {           
            for(String Str : selectedCatVersions) {
                if(Start) {
                    selected = Str;
                    Start = false;
                } else {               
                    selected = selected + ';' + Str;
                }
            }
        }
        appSource.App_Category_Version__c = selected;
        appSource.IsActive__c =true;
        appSource.Status__c = Label.vMarket_Published;
        update appSource;
        vMarketAppSource__c sourceRecord = new vMarketAppSource__c( vMarket_App__c = appSource.vMarket_App__c,Status__c = 'New',IsActive__c = false);
        insert sourceRecord;
        
        pageReference pgRef = ApexPages.currentPage();
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appSourceId', sourceRecord.Id);  
        pgRef.setRedirect(true);
        return pgRef; 
    }

    public pagereference submitApp(){
        currentAppObj.ApprovalStatus__c = 'Submitted';
        update currentAppObj;
        success = true;
        
        Id sourceId = Id.valueof(ApexPages.currentPage().getParameters().get('appSourceId'));
        delete [Select id from vMarketAppSource__c where id=:sourceId];
        
         if(attached_logo!=null && attached_logo.body!=null && logoExists) delete existing_logo;
            
            if(attached_logo!=null && String.isNotBlank(attached_logo.name))
            {
                attached_logo.parentid= currentAppObj.Id;
                attached_logo.Name = 'Logo_'+attached_logo.Name;
                attached_logo.ContentType = 'application/png';
                insert attached_logo;
            }
            
        pageReference pgRef = new PageReference('/vMarketDevAppDetails');
        
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appId', currentAppObj.Id);  
        pgRef.getParameters().put('status', currentAppObj.ApprovalStatus__c);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    /*public pagereference redirectToAppDtl(){
        pageReference pgRef = new PageReference('/vMarketDevAppDetails');
        success = false;
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appId', currentAppObj.Id);  
        pgRef.getParameters().put('status', currentAppObj.ApprovalStatus__c);
        pgRef.setRedirect(true);
        return pgRef; 
    }*/
    
    public vMarket_DevAppDo getAppDevDetail() {
        vMarket_DevAppDo appDo;

        if (appId !='') {
            String APP_FIELD_NAME = ' Name, App_Category__r.Name, countries__C,Owner.FirstName, Owner.LastName, Owner.Email, Owner.Phone, '
                                    + ' ApprovalStatus__c, AppStatus__c, IsActive__c, Key_features__c, Long_Description__c, '
                                    + ' Price__c, Published_Date__c, Short_Description__c, subscription__c, '
                                    + ' PublisherName__c,PublisherPhone__c, PublisherWebsite__c, PublisherEmail__c ,Publisher_Developer_Support__c';
            String APP_QUERYSTR = 'SELECT' + APP_FIELD_NAME +
                                    + ' FROM vMarket_App__c '
                                    + ' WHERE ApprovalStatus__c!=\'Rejected\' AND Id=\''+appId +'\' limit 1';
            
            try {
                List<vMarket_App__c> apps  = Database.query(APP_QUERYSTR);
                if (!apps.isEmpty()) {
                        appDo = new vMarket_DevAppDo(apps[0]);
                        return appDo;
                }
            } catch(System.QueryException e) {} 
        } 
        if (appDo==null)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Requested App does not exist in vMarket Store'));

        return null;
    }
    
    
    @RemoteAction //Nov 24 2017 -  Abhishek Kolipey - CHG0119045 - Added Method for Polling to Check if Chatter File is Uploaded
    public static boolean fileuploaded(String sourceId)
    {
    
    List<ContentDocumentLink> docs = [SELECT LinkedEntityId,ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = :sourceId];
    if(docs.size()>0) return true;
    else return false;
    }
    
    
    
}
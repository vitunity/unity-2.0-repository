@isTest
public with sharing class vMarketDevUploadAppPackageControllerTest {
    private static testMethod void vMarketDeveloperControllerTest() {
    test.StartTest();      
      
      Account account = vMarketDataUtility_Test.createAccount('test account', true);
      Profile portal = vMarketDataUtility_Test.getPortalProfile();
      Contact contact = vMarketDataUtility_Test.contact_data(account, false);
      User developer = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact.Id, 'Developer');
      vMarket_App_Category__c  cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
      vmarket_app__C app = vMarketDataUtility_Test.createAppTest('Test Application', cate, true, developer.Id);
      vMarketAppSource__c appSource = vMarketDataUtility_Test.createAppSource(app,true);
      vMarketAppSource__c appSource1 = vMarketDataUtility_Test.createAppSource(app,true);
      vMarketOrderItem__c  order = vMarketDataUtility_Test.createOrderItem(false,true);
      vMarketOrderItemLine__c  orderLinetem = vMarketDataUtility_Test.createOrderItemLineData(app,order,false,true);
      
      appSource1.App_Category_Version__c = 'RapidPlan';
      update appSource1;
      app.isActive__c = true;
      update app;
      orderLinetem.transfer_status__C = 'paid';
      update orderLinetem;
      
      System.debug('===='+[Select id from vMarketAppSource__c]);
      
      PageReference myVfPage = Page.vMarketAppInitialEdit;
      Test.setCurrentPage(myVfPage);

      ApexPages.currentPage().getParameters().put('appSourceId',appSource.Id);
      ApexPages.currentPage().getParameters().put('selectedCatVersions1','RapidPlan');
      vMarketDataUtility_Test.createAttachmentTest(appSource.id, 'Content/PNG', 'Logo', true);
      vMarketDevUploadAppPackageController dc = new vMarketDevUploadAppPackageController();     
      dc.attached_logo = vMarketDataUtility_Test.createAttachmentTest('', 'Content/PNG', 'Logo', false);
      dc.existing_logo= vMarketDataUtility_Test.createAttachmentTest(app.id, 'Content/PNG', 'Logo', true);
      dc.logoExists = true;
      vMarketDataUtility_Test.createAppSource(app, true);
      dc.selectedCatVersions = new List<String>{'RapidPlan'};
      dc.getIsViewableForDev();
      dc.getAuthenticated();
      dc.getRole();
      dc.getIsAccessible();
      dc.getAppCategoryVersions();
      dc.selectedAppCatVersions();
      
      dc.createAppSource();
      
      dc.getAppDevDetail();
      
      
      dc.submitApp();
      
      dc.SelSourceRecID =appSource.Id;
      dc.deleteSource();
      
      test.Stoptest();
    }
}
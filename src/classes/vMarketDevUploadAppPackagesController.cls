public without sharing class vMarketDevUploadAppPackagesController extends vMarketBaseController{
    public String appSourceId{get;set;}
    public String appId{get;set;}
    public string status {get;set;}
    public vMarket_App__c currentAppObj{get;set;}
    public vMarketAppSource__c appSource{get;set;}
    public List<Attachment> allFileList {get; set;}
    public vMarketAppAsset__c appAsset{get; set;}
    public Attachment attached_userGuidePDF {get;set;}
    public Integer FileCount {get; set;}
    public List<vMarketAppSource__c> appSourceList {get;set;}
    public string SelSourceRecID {get;set;}
    public vMarketAppSource__c sourceRecord {get;set;}
    public List<String> selectedCatVersions {get;set;}
    public String selectedCatVersions1 {get;set;}
    public Integer appSourceSize {get;set;}
    public set<String> appSourceSet {get;set;}
    
    public vMarketDevUploadAppPackagesController(){
        //Get App ID
        appSourceId = ApexPages.currentPage().getParameters().get('appSourceId');
        //vMarket_App__c = [Select App_Category__c, isActive__c from vMarket_App_Category_Version__c];//App_Category__c
        appSource = [Select App_Category__c, Id, App_Category_Version__c, vMarket_App__c from vMarketAppSource__c where Id=:appSourceId limit 1];
        //appAsset =[Select Id, vMarket_App__c, AppVideoID__c from vMarketAppAsset__c where vMarket_App__c =: appId];
        currentAppObj = [Select App_Category__c, Id, ApprovalStatus__c from vMarket_App__c where Id=:appSource.vMarket_App__c];
        appSourceList = new List<vMarketAppSource__c>();
        appId = appSource.vMarket_App__c; 
        sourceRecord = new vMarketAppSource__c();
        
        appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:appId and Id !=: appSourceId];
       
       appSourceSize = appSourceList.size(); 
    }
    
    public override Boolean getAuthenticated() {
    return super.getAuthenticated();
    }
    
    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsAccessible() {
        if(getAuthenticated() && getRole()=='Developer')
            return true;
        else
            return false;
    }
        
    public List<SelectOption> getAppCategoryVersions() {
        appSourceSet = new set<String>();
        List<String> appCatVerList = new list<String>();
          
        for(vMarketAppSource__c aps :appSourceList){
            if(aps.App_Category_Version__c.contains(';')){
                appCatVerList = aps.App_Category_Version__c.split(';');
                for(string st :appCatVerList){
                    appSourceSet.add(st);
                }
            }else{
                appSourceSet.add(aps.App_Category_Version__c);
            }
        }
        
        List<SelectOption> options = new List<SelectOption>();
        List<vMarket_App_Category_Version__c> app_categories_versions = [select Name,App_Category__c from vMarket_App_Category_Version__c 
                                                                        where App_Category__c=:currentAppObj.App_Category__c
                                                                        And Name NOT IN:appSourceSet];
        
        options.add(new SelectOption('Select Category Version','Select Category Version'));
        for(vMarket_App_Category_Version__c catVersion:app_categories_versions) {
            options.add(new SelectOption(catVersion.Name,catVersion.Name));
        }
        return options;
    }
    
    public pagereference deleteSource(){
        system.debug('==SelSourceRecID =='+SelSourceRecID );
        vMarketAppSource__c appScr = new  vMarketAppSource__c();
        appScr = [Select Id from vMarketAppSource__c where Id=:SelSourceRecID ]; 
        delete appScr; 
        
        appSourceList = [select Name, App_Category__c,App_Category_Version__c,
                          IsActive__c, Status__c, vMarket_App__c 
                          from vMarketAppSource__c 
                          where vMarket_App__c =:appId and Id !=: appSourceId];
        appSourceSize = appSourceList.size();
        system.debug('==appSourceSize=='+appSourceSize);
        pageReference pgRef = ApexPages.currentPage();
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appSourceId', appSourceId);  
        pgRef.setRedirect(true);
        return pgRef;
              
    }
    public void selectedAppCatVersions(){
        system.debug('==selectedCatVersions1=='+ selectedCatVersions1);
        system.debug('==selectedCatVersions=='+ selectedCatVersions);
        
    }
    public PageReference createAppSource() {
        String selected = '';
        Boolean Start = true;
        system.debug(' ==== appSource.App_Category_Version__c ' + appSource.App_Category_Version__c + ' = ' + appSource.App_Category_Version__c);
        system.debug('==selectedCatVersions=='+selectedCatVersions);
        String cIds = apexpages.currentpage().getparameters().get('selectedCatVersions1');
        system.debug('-------------- Here -----------------'+cIds);
        if(!selectedCatVersions.isEmpty()) {           
            for(String Str : selectedCatVersions) {
                if(Start) {
                    selected = Str;
                    Start = false;
                } else {               
                    selected = selected + ';' + Str;
                }
            }
        }
        appSource.App_Category_Version__c = selected;
        update appSource;
        vMarketAppSource__c sourceRecord = new vMarketAppSource__c( vMarket_App__c = appSource.vMarket_App__c,Status__c = 'New',IsActive__c = false);
        insert sourceRecord;
        
        pageReference pgRef = ApexPages.currentPage();
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appSourceId', sourceRecord.Id);  
        pgRef.setRedirect(true);
        return pgRef; 
    }

    public pageReference submitApp(){
        currentAppObj.ApprovalStatus__c = 'Submitted';
        update currentAppObj;
        pageReference pgRef = new PageReference('/vMarketDevAppDetails');
        
        pgRef.getParameters().clear();
        pgRef.getParameters().put('appId', currentAppObj.Id);  
        pgRef.getParameters().put('status', currentAppObj.ApprovalStatus__c);
        pgRef.setRedirect(true);
        return pgRef; 
    }
}
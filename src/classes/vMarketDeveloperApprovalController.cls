/**
 *	@author		:		Puneet Mishra
 *	@vfPage		:		vMarketDeveloperApprovalPage
 *	@decription	:		Controller handles display Developer Records based on there status and give access to Admin to Review Developer records and change Status
 */
public with sharing class vMarketDeveloperApprovalController {
 	
 	public String selectedRecId{get;set;} // Id of selected record
    public vMarket_Developer_Stripe_Info__c dev{get;set;} // Update the contact
 	public String appStatus{get;set;} // store status of App
 	public String rejectionReason{get;set;}
 	public String selectedStatus{get;set;}
 	
 	// chks if logged in user is Admin
 	public Boolean isAdmin{
        get{
            String usrRole = [SELECT Id, Name, vMarket_User_Role__c FROM User WHERE Id =: userInfo.getUserId()].vMarket_User_Role__c;
            if(usrRole == Label.vMarket_AdminRole)
                return true;
            return false;
        }
        set;
    }
    
    // returns existing approved Developers with Status Status
    public List<vMarket_Developer_Stripe_Info__c> getExistingDevelopers() {
    	List<vMarket_Developer_Stripe_Info__c> existing = [SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, 
    															vMarket_Accept_Terms_of_Use__c,Developer_Request_Status__c,Contact__c, Contact__r.Name, Contact__r.FirstName, 
    															Contact__r.LastName, Contact__r.Email,Stripe_User__r.Name, Rejection_Reason__c
    													   FROM vMarket_Developer_Stripe_Info__c WHERE
    													   		Developer_Request_Status__c =: Label.vMarket_Approved_Status ];
    	system.debug(' ==== existing ==== ' + existing);
    	return existing;
    }
    
    // returns rejected Developers with Status Rejected
    public List<vMarket_Developer_Stripe_Info__c> getRejectedDevelopers() {
    	List<vMarket_Developer_Stripe_Info__c> rejected = [SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, 
    															vMarket_Accept_Terms_of_Use__c,Developer_Request_Status__c,Contact__c, Contact__r.Name, Contact__r.FirstName, 
    															Contact__r.LastName, Contact__r.Email,Stripe_User__r.Name, Rejection_Reason__c
    													   FROM vMarket_Developer_Stripe_Info__c WHERE
    													   		Developer_Request_Status__c =: Label.vMarket_Rejected ];
    	system.debug(' ==== rejected ==== ' + rejected);
    	return rejected;
    }
    
    // returns pending Developer Request with Status Pending
    public List<vMarket_Developer_Stripe_Info__c> getPendingDevelopers(){
    	List<vMarket_Developer_Stripe_Info__c> pending = [SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, 
    															vMarket_Accept_Terms_of_Use__c,Developer_Request_Status__c,Contact__c, Contact__r.Name, Contact__r.FirstName, 
    															Contact__r.LastName, Contact__r.Email,Stripe_User__r.Name, Rejection_Reason__c
    													   FROM vMarket_Developer_Stripe_Info__c WHERE
    													   		Developer_Request_Status__c =: Label.vMarket_Pending ];
    	system.debug(' ==== pending ==== ' + pending);
    	return pending;
    }
    
    public List<SelectOption> getStatusValue() {
        List<SelectOption> opt = new List<SelectOption>();
        if(appStatus == Label.vMarket_Pending) {
        	opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
            opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        } else if(appStatus == Label.vMarket_Approved_Status){
            //opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
            opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        } else {
        	//opt.add(new SelectOption(Label.vMarket_Rejected, Label.vMarket_Rejected));
        	opt.add(new SelectOption(Label.vMarket_Approved_Status,Label.vMarket_Approved_Status));
        }
        return opt;
    }
    
    public void vMarketDeveloperApprovalController(){
    	dev = new vMarket_Developer_Stripe_Info__c();
    }
    public void selectedDeveloper() {
    	system.debug(' === appStatus === ' + appStatus);
    	system.debug(' === selectedRecId === ' + selectedRecId);
    	
    	dev = [SELECT Id, Name, Access_Token__c, isActive__c, Scope__c, Stripe_Id__c, Stripe_User__c, Token_Type__c, vMarket_Accept_Terms_of_Use__c, 
        												Developer_Request_Status__c,Contact__c, Contact__r.Name, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email,
        												Stripe_User__r.Name, Rejection_Reason__c 
        										FROM vMarket_Developer_Stripe_Info__c 
                WHERE Contact__c =: selectedRecId AND Developer_Request_Status__c =: appStatus LIMIT 1];
		system.debug(' ======== dev ========= ' + dev);   
    }
    
    /**
     * method will update the Developer Request Status
     */
    public pagereference updateDeveloperInfo() {
    	try {
    		//dev = new vMarket_Developer_Stripe_Info__c();
    		//this.dev = dev;
    		system.debug(' ========= selectedRecId =========== ' + selectedRecId);
    		system.debug(selectedRecId + ' ========= updateDeveloperInfo =========== ' + dev);
    		system.debug(' ==== dev.Developer_Request_Status__c ==== ' + dev.Developer_Request_Status__c);
    		system.debug(' ==== dev.Rejection_Reason__c ==== ' + dev.Rejection_Reason__c);
    		
    		if(!dev.isActive__c) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record is not active'));
    			return null;
    		} else if(!dev.vMarket_Accept_Terms_of_Use__c) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'User not accepted Terms & Condition'));
    			return null;
    		} else if(dev.Stripe_Id__c == '' && dev.Stripe_Id__c == null) {
    			ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Stripe account Id is required'));
    			return null;
    		}
    		system.debug(' ==== dev ==== ' + dev);
    		update dev;
    		return null;
    	} catch(Exception e) {
    		system.debug(' === ' + e.getMessage() + ' - ' + e.getLineNumber());
    		return null;
    	}
    }
    
    
}
/**
 *	@author			:			Puneet Mishra
 *	@description	:			Test Class for vMarketDeveloperApprovalController
 */
@isTest
public with sharing class vMarketDeveloperApprovalController_Test {
    
    static Profile admin,portal;   
   	static User customer1, customer2, developer1, ADMIN1;
   	static List<User> lstUserInsert;
   	static Account account;
	static Contact contact1, contact2, contact3;
   	static List<Contact> lstContactInsert;
   	
   	static {
   		admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
    	
    	account = vMarketDataUtility_Test.createAccount('test account', false);
    	account.Ext_Cust_Id__c = '6051213';
    	insert account;
    	
    	lstContactInsert = new List<Contact>();
    	contact1 = vMarketDataUtility_Test.contact_data(account, false);
    	contact1.MailingStreet = '53 Street';
    	contact1.MailingCity = 'Palo Alto';
    	contact1.MailingCountry = 'USA';
    	contact1.MailingPostalCode = '94035';
    	contact1.MailingState = 'CA';
    	
		contact2 = vMarketDataUtility_Test.contact_data(account, false);
		contact2.MailingStreet = '660 N';
    	contact2.MailingCity = 'Milpitas';
    	contact2.MailingCountry = 'USA';
    	contact2.MailingPostalCode = '94035';
    	contact2.MailingState = 'CA';
    	
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	
    	lstContactInsert.add(contact1);
    	lstContactInsert.add(contact2);
    	insert lstContactInsert;
    	
    	lstUserInsert = new List<User>();
    	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
		lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Admin', false, portal.Id, contact2.Id, 'Customer2'));
    	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
    	ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
        LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
        vMarket_User_Role__c = 'Admin');
    	lstUserInsert.add(ADMIN1);
    	
    	insert lstUserinsert;
   	}
   	
   	public static testMethod void isAdmin_Customer() {
   		Test.StartTest();
   			system.runas(customer1) {
   				vMarketDeveloperApprovalController control = new vMarketDeveloperApprovalController();
   				Boolean res = control.isAdmin;
   				system.assertEquals(false, res);
   			}
   		Test.StopTest();
   	}
   	
   	public static testMethod void isAdmin_ADMIN() {
   		Test.StartTest();
   			system.runas(ADMIN1) {
   				vMarketDeveloperApprovalController control = new vMarketDeveloperApprovalController();
   				Boolean res = control.isAdmin;
   				system.assertEquals(true, res);
   			}
   		Test.StopTest();
   	}
    
    public static testMethod void getExistingDevelopers() {
    	Test.StartTest();
    		system.runas(ADMIN1) {
    			
    			List<vMarket_Developer_Stripe_Info__c> devList = new List<vMarket_Developer_Stripe_Info__c>();
	    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
			    devStripe.isActive__c = true;
			    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
			    //System.debug('^^^^'+u.id);
			    devStripe.Stripe_User__c = developer1.Id;
			    devStripe.Developer_Request_Status__c = 'Pending';
			    
			    vMarket_Developer_Stripe_Info__c devStripe1 = new vMarket_Developer_Stripe_Info__c();
			    devStripe1.isActive__c = true;
			    devStripe1.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
			    //System.debug('^^^^'+u.id);
			    devStripe1.Stripe_User__c = developer1.Id;
			    devStripe1.Developer_Request_Status__c = 'Approved';
			    
			    vMarket_Developer_Stripe_Info__c devStripe2 = new vMarket_Developer_Stripe_Info__c();
			    devStripe2.isActive__c = true;
			    devStripe2.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
			    //System.debug('^^^^'+u.id);
			    devStripe2.Stripe_User__c = developer1.Id;
			    devStripe2.Developer_Request_Status__c = 'Rejected';
			    
			    devList.add(devStripe);
			    devList.add(devStripe1);
			    devList.add(devStripe2);
			    insert devList;
    			
    			vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    			List<vMarket_Developer_Stripe_Info__c> approved = devControl.getExistingDevelopers();
    			List<vMarket_Developer_Stripe_Info__c> rejected = devControl.getRejectedDevelopers();
    			List<vMarket_Developer_Stripe_Info__c> pending = devControl.getPendingDevelopers();
    			system.assertEquals(approved.size(), 1);
    			system.assertEquals(rejected.size(), 1);
    			system.assertEquals(pending.size(), 1);
    		}
    	
    	Test.StopTest();
    }
    
    public static testMethod void getStatusValue() {
    	Test.StartTest();
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.getStatusValue();
    	Test.StopTest();
    }
    
    public static testMethod void getStatusValue_Pending() {
    	Test.StartTest();
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.appStatus = 'Pending';
    		devControl.getStatusValue();
    	Test.StopTest();
    }
    
    public static testMethod void getStatusValue_Approved() {
    	Test.StartTest();
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.appStatus = 'Approved';
    		String rejReason = devControl.rejectionReason;
    		String status = devControl.selectedStatus;
    		devControl.getStatusValue();
    	Test.StopTest();
    }
    
    public static testMethod void selectedDeveloper() {
    	Test.StartTest();
    		
    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
		    devStripe.isActive__c = true;
		    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
		    //System.debug('^^^^'+u.id);
		    devStripe.Stripe_User__c = developer1.Id;
		    devStripe.Developer_Request_Status__c = 'Pending';
		    devStripe.Contact__c = contact3.Id;
		    insert devStripe;
    	
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		
    		devControl.appStatus = 'Pending';
    		devControl.selectedRecId = contact3.Id;
    		devControl.selectedDeveloper();
    	Test.StopTest();
    }
    
    public static testMethod void updateDeveloperInfo_NotActive() {
    	Test.StartTest();
    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
		    devStripe.isActive__c = false;
		    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
		    //System.debug('^^^^'+u.id);
		    devStripe.Stripe_User__c = developer1.Id;
		    devStripe.Developer_Request_Status__c = 'Pending';
		    devStripe.vMarket_Accept_Terms_of_Use__c = true;
		    insert devStripe;
    	
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.dev = devStripe;
    		devControl.updateDeveloperInfo();
    	Test.StopTest();
    }
    
    public static testMethod void updateDeveloperInfo_NoTOU() {
    	Test.StartTest();
    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
		    devStripe.isActive__c = true;
		    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
		    //System.debug('^^^^'+u.id);
		    devStripe.Stripe_User__c = developer1.Id;
		    devStripe.Developer_Request_Status__c = 'Pending';
		    devStripe.vMarket_Accept_Terms_of_Use__c = false;
		    insert devStripe;
    	
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.dev = devStripe;
    		devControl.updateDeveloperInfo();
    	Test.StopTest();
    }
    
    public static testMethod void updateDeveloperInfo_NoStripe() {
    	Test.StartTest();
    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
		    devStripe.isActive__c = true;
		    devStripe.Stripe_Id__c = '';
		    //System.debug('^^^^'+u.id);
		    devStripe.Stripe_User__c = developer1.Id;
		    devStripe.Developer_Request_Status__c = 'Pending';
		    devStripe.vMarket_Accept_Terms_of_Use__c = true;
		    insert devStripe;
    	
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.dev = devStripe;
    		devControl.updateDeveloperInfo();
    	Test.StopTest();
    }
    
    public static testMethod void updateDeveloperInfo() {
    	Test.StartTest();
    		vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
		    devStripe.isActive__c = true;
		    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
		    //System.debug('^^^^'+u.id);
		    devStripe.Stripe_User__c = developer1.Id;
		    devStripe.Developer_Request_Status__c = 'Pending';
		    devStripe.vMarket_Accept_Terms_of_Use__c = true;
		    insert devStripe;
    	
    		vMarketDeveloperApprovalController devControl = new vMarketDeveloperApprovalController();
    		devControl.dev = devStripe;
    		devControl.updateDeveloperInfo();
    	Test.StopTest();
    }
    
}
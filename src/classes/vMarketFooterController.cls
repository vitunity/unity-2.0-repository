public virtual class vMarketFooterController {
    public String appId{get;set;}
    public Integer cartItemCount{get;set;}
    private List<String> appIdList = new List<String> {};
    public vMarketCartItem cartItemObj;
    public vMarketCatalogueController vMarketCatalogueObj;
    public Map<Integer, VMarket_Footer_Components__c> parentMap;
    public Map<String, VMarket_Footer_Components__c> mapOriginalFooterComponent;
    public Map<Integer, String> childMarkupMap;
    public Integer maxFooterItems{get;set;}
    public String footerMarkup{get;set;}
    
    public vMarketFooterController() {
    maxFooterItems = 0;
        footerMarkup = '';
        prepareMarkupMap();
        prepareMarkup();
    }

   public void prepareMarkupMap()
   {
   parentMap = new Map<Integer, VMarket_Footer_Components__c>();
   childMarkupMap = new Map<Integer, String>();
  // mapOriginalFooterComponent = VMarket_Footer_Components__c.getAll() ;
    //for(String b :mapOriginalFooterComponent.keyset()) 
    for(VMarket_Footer_Components__c c:[select parent_id__c,logo__c,order_id__c,link__c,name from VMarket_Footer_Components__c order by order_id__c asc])
    {
    //VMarket_Footer_Components__c c = mapOriginalFooterComponent.get(b);
    //System.debug(c.order_id__c+'footterr^^^^^^'+c.name);
    if( Integer.valueOf(c.parent_id__c) == -1)  
    {if( Integer.valueOf(c.order_id__c) > maxFooterItems) maxFooterItems = Integer.valueOf(c.order_id__c);
    parentMap.put(Integer.valueOf(c.order_id__c),c);
    }
    
    if(!childMarkupMap.containsKey(Integer.valueOf(c.parent_id__C)) && Integer.valueOf(c.parent_id__c) != -1) 
    {
    if(String.isNotBlank(c.logo__C)) childMarkupMap.put(Integer.valueOf(c.parent_id__C),'&nbsp;&nbsp;<a href=\"'+c.link__C+'\"><img src=\"'+c.logo__c+'\" width=\"25px\" height=\"25px\"></img></a>');
    else childMarkupMap.put(Integer.valueOf(c.parent_id__C),'<li><a href=\"'+c.link__C+'\">'+c.name+'</a></li>');
    
    }
    else 
    {
    if(String.isNotBlank(c.logo__C)) childMarkupMap.put(Integer.valueOf(c.parent_id__C),childMarkupMap.get(Integer.valueOf(c.parent_id__C))+'&nbsp;&nbsp;<a href=\"'+c.link__C+'\"><img src=\"'+c.logo__c+'\" width=\"25px\" height=\"25px\"></img></a>');
    else childMarkupMap.put(Integer.valueOf(c.parent_id__C),childMarkupMap.get(Integer.valueOf(c.parent_id__C))+'<li><a href=\"'+c.link__C+'\">'+c.name+'</a></li>');
    }
    
    }
   
   
   }


    public void prepareMarkup()
    {
    String prefix = '<div class=\"col-md-2\"><div class=\"footerLinksWrap\"><ul>';
     String suffix = '</ul></div></div>';
     
    for(Integer i=1;i<=maxFooterItems;i++)
    {
    VMarket_Footer_Components__c headComp =  parentMap.get(i);
    
    String childmarkup =  childMarkupMap.get(Integer.valueOf(headcomp.order_id__c));
    if(String.isBlank(childmarkup)) childmarkup = '';
    
    if(i!=maxFooterItems) footerMarkup += prefix+'<li class=\"titleText\">'+headComp.name+childmarkup+'</li>'+suffix;
    if(i==maxFooterItems) 
    {
    
    footerMarkup += '<div class=\"col-md-3\"><div class=\"footerLinksWrap\"><ul><li class=\"titleText\">'+headComp.name+childmarkup+'</li>';//+Label.vMarket_Footer_Disclaimer+suffix;
    }
    }
    
    }

    public Boolean getShowError() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.INFO);
    }
    
    
}
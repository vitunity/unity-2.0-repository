/**************************************************************************
    @ Author    : Nilesh Gorle
    @ Date      : 13-Feb-2017
    @ Description   : An Apex class for Category view page
****************************************************************************/
public class vMarketHomePageCategoryControllers extends vMarketBaseController { 
    
    public string customerStripeAccId; // Customer Stripe Acc Id
    public string scope {get;set;} // Decide the scope(Read/Read_Write) with Customer Stripe Acc Id
    @testVisible private static Map<String, String> requestBody;
    
    public vMarketHomePageCategoryControllers() {
        super();
    }
    
    // UUID - User Unique ID
    public override String getUserId() {
        return UserInfo.getUserId();
    }

    public List<vMarketCategoryDo> getAllCategoryList() {
        List<vMarket_App_Category__c> cat_list = [Select Name, Short_Description__c From vMarket_App_Category__c];
        if (!cat_list.isEmpty()) {
            List<vMarketCategoryDo> catsDo = new List<vMarketCategoryDo>();
            
            // Create object for all
            vMarketCategoryDo vmdo = new vMarketCategoryDo();
            vmdo.setCategoryName('All apps');
            vmdo.setDescription('All categories listing');
            //vmdo.setURL('https://www.mobileiron.com/sites/default/files/customers/lg-svg/varian.png');
            catsDo.add(vmdo);
            
            for(vMarket_App_Category__c cat:cat_list) {
                catsDo.add(new vMarketCategoryDo(cat));
            }
            return catsDo;
        }
        return null;
    }   

    //To query all app categories  
    public List<vMarket_App_Category__c> getCategories(){
        return [Select Name, Short_Description__c from vMarket_App_Category__c limit 1000];// you need to place a limit of 1000 as VF supports max of 1000 records to be displayed
    }
    
    // this method will be called only when code is received [i.e. customeStripeId]
    //Changed Methods Name as it's throwing error
    public pageReference authorizeConnectAccount7() {
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('code'));
        system.debug(' Current Page => ' + apexpages.currentpage().getparameters().get('scope'));
        if( (apexpages.currentpage().getparameters().get('code') == null) &&
                (apexpages.currentpage().getparameters().get('scope') == null) )
            return null;
        List<vMarket_Developer_Stripe_Info__c> infoList = new List<vMarket_Developer_Stripe_Info__c>();
        infoList = [SELECT Id, Name, Access_Token__c, Contact__c, isActive__c, Stripe_User__c FROM vMarket_Developer_Stripe_Info__c 
                WHERE Stripe_User__c =: userinfo.getUserId() AND isActive__c =: true LIMIT 1];
        String customerStripeAccId = apexpages.currentpage().getparameters().get('code');
        
        // created a remote site setting https://varian--sfdev2.cs61.my.salesforce.com/0rp4C000000GyWO
        String clientId = vMarket_StripeAPI.clientKey;
        
        requestBody = new Map<String, String>();
        requestBody.put('code', customerStripeAccId);
        requestBody.put('client_id', clientId);
        requestBody.put('grant_type', 'authorization_code');
        requestBody.put('scope', 'read_write');
         // Abhishek Added Null as last parameter to fix the errors on 29th June
        HttpRequest request = vMarket_HttpRequest.createHttpRequest(vMarket_StripeAPIUtil.TOKEN_URL, 'POST', requestBody, false,null);
        Map<String, String> requestResponse = vMarket_HttpResponse.sendRequest(request);
        
        if(!requestResponse.isEmpty() && requestResponse.get('Code') == '200') {
            system.debug(' ==== ' + requestResponse);
            vMarket_Developer_Stripe_Info__c info = new vMarket_Developer_Stripe_Info__c();
            if(!infoList.isEmpty())
                info.Id = infoList[0].Id;
            info.Name = userInfo.getName();
            info.Stripe_User__c = userInfo.getUserId();
            
            JSONParser parser = JSON.createParser(requestResponse.get('Body'));
            while(parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'stripe_user_id')) {
                    parser.nextToken();
                    info.Stripe_Id__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'token_type')) {
                    parser.nextToken();
                    info.Token_Type__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')) {
                    parser.nextToken();
                    info.Access_Token__c = parser.getText();
                }
                else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'scope')) {
                    parser.nextToken();
                    info.Scope__c = parser.getText();
                }
            }
            
            upsert info;
        }
        return null;
    }
    
}
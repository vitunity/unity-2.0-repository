/**************************************************************************\
    @ Author    : Nilesh Gorle
    @ Date      : 28-Jan-2017
    @ Description   : An Apex class for Site Login.
****************************************************************************/
public class vMarketLoginController extends vMarketBaseController { 
    public String exactURL{get;set;}
    public String startUrl;
    public User loginUser;
    public List<User> usrList;

    public String onetimeToken;
    public String oktaUserId;
    public String sessionId;
    public string loginCheck{get;set;}
    public String username {get;set;}
    public String password {get;set;}
    public String cartCookieIds {get;set;}
    public String landingUrl{get;set;}
    public String smallphotoUrl{get;set;}
    
    {
        smallphotoUrl = [Select id, Name, SmallPhotoUrl, FullPhotoUrl FROM User WHERE Id =: userInfo.getUserId()].SmallPhotoUrl;
    }
    
    public Boolean isDev{
        get {
            if(isDev() && !isDevRequestPending())
                return true;
            else
                return false;
        }
        set;} // show the option if Customer wants to make Developer request
        
    public Boolean isDevPending{
        get {
            if(isDevRequestPending()) {
                return true;
            } else {
                return false;
            }
        }
        set;
    } // Chk is Developer request already made and is in review State
     public String vMarketStripeURL{
      get {
        String clientId = Label.vMarket_StripeConnect;
            if(vMarket_StripeAPI.clientKey != null) {
              system.debug(' ============= vMarket_StripeAPI.clientKey ================== ' + vMarket_StripeAPI.clientKey);
              system.debug(' ============= clientId ================== ' + clientId);
              clientId = clientId.replace('CLIENT', vMarket_StripeAPI.clientKey);
            }
            return clientId;
      }
    set;}
    /*public Boolean isDev{
        get {
            String RoleName = [Select Id, vMarket_User_Role__c From User where id = : userInfo.getUserId() limit 1].vMarket_User_Role__c;
            List<vMarket_Developer_Stripe_Info__c> usrList = new List<vMarket_Developer_Stripe_Info__c>();
            usrList = [SELECT Id, Name, Developer_Request_Status__c, OwnerId, Stripe_User__c, Stripe_User__r.vMarket_User_Role__c 
                        FROM vMarket_Developer_Stripe_Info__c WHERE OwnerId =: userInfo.getUserId() LIMIT 1];
            
            system.debug(' WHAT IS THE VALUE ');
            if(RoleName != null && RoleName.contains(Label.vMarket_Developer)) { 
                if(!usrList.isEmpty() && usrList[0].Developer_Request_Status__c == Label.vMarket_Pending) {
                    return false;
                } else {
                    return true;
                }
            } 
            return false;
        }
        set;
    }// check if login user is developer/customer
    
    public Boolean isDevPending {
        get {
            List<vMarket_Developer_Stripe_Info__c> usrList = new List<vMarket_Developer_Stripe_Info__c>();
            usrList = [SELECT Id, Name, Developer_Request_Status__c, OwnerId, Stripe_User__c, Stripe_User__r.vMarket_User_Role__c 
                        FROM vMarket_Developer_Stripe_Info__c WHERE OwnerId =: userInfo.getUserId() LIMIT 1];
            String devRoleName;
            if(!usrList.isEmpty()) {
                devRoleName = usrList[0].Developer_Request_Status__c; 
                if(devRoleName.contains(Label.vMarket_Pending) && usrList[0].Stripe_User__r.vMarket_User_Role__c == Label.vMarket_Customer ) {
                    system.debug(' WHAT IS THE VALUE 1');
                    return true;
                }
                return false;
            }
            return false;
        }
        set;
    }*/
    /*
     *  created :   Puneet Mishra
     *  desc    :   return false for Terms of Condition page
     *  parameter:  none
     *  return  :   Boolean
     */ 
    public Boolean getTermsOfUseHeader() {
        String pageURL = ApexPages.currentPage().getURL();
        if(pageURL.contains('vMarketTermsOfUseNew')) {
            vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'puneet.mishra@varian.com'},exactURL,exactURL,exactURL);
            return false;
        } else {
            return true;
       }
        return true;
    }
    
    public override String getRole() {
        return super.getRole();
    }
    
    public Boolean getIsDevAccessible() {
        if(getAuthenticated() && getRole().trim()=='Developer')
            return true;
        else
            return false;
    }
    
    public Boolean getDoesErrorExist() {
        return ApexPages.hasMessages(ApexPages.Severity.ERROR);
    }

    public Boolean getDoesInfoExist() {
        return ApexPages.hasMessages(ApexPages.Severity.INFO);
    }

    public override  Boolean getAuthenticated() {
        return super.getAuthenticated();
    }

    public vMarketLoginController() {
        super();
        exactURL = ApexPages.currentPage().getURL();
        system.debug('############# URL= ' + exactURL);
         if(ApexPages.currentPage().getUrl().contains('?land=')) landingUrl =  '/'+EncodingUtil.urlDecode(ApexPages.currentPage().getUrl().substringAfter('?land='),'UTF-8');
        
         String pageName = '';

         if(ApexPages.currentPage().getUrl().contains('?')) pageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
         else pageName = ApexPages.currentPage().getUrl().substringAfter('apex/');
         
         if(!test.isRunningTest())
            pageName = pageName.toLowerCase();
         else
            pageName ='vMarketSignIn';
         
         if(!getAuthenticated() && !(Label.vMarket_Excluded_Pages.toLowerCase()).contains(pageName)) 
         {
         System.debug('######Not to be shown');
         String signInPage = '/vMarketSignIn?land='+ApexPages.currentPage().getUrl().substringAfter('apex/');
         loginCheck='<script> window.open(\"'+signInPage+'\",\"_self\");</script>';
          //new pageReference('/vMarketSignIn?land='+ApexPages.currentPage().getUrl().substringAfter('apex/'));
          //openSignIn();
         }
         else {
         List<user> usersList=[Select Id, email,Profile.name, vMarket_User_Role__c From User where id = :Userinfo.getUserId() limit 1];
                  String role = null;
                 if(usersList.size()>0) role = usersList[0].vMarket_User_Role__c;
                    Boolean IsCustomer= false;
                    
                    if(role!=null && role.contains('Customer')) IsCustomer= true;
                    else IsCustomer= false;
                
            if(IsCustomer && pageName.toLowerCase().contains('dev'))  loginCheck='<script> window.open(\"vMarketLostPage\",\"_self\");</script>';
         }
    }
    
    /* Nilesh - Check User is already login */
    /* If user is authenticated, then redirect to related page */
    /* else don't do anything */
    public PageReference checkUserAlreadylogin() {
        if(getAuthenticated()) {
            return new PageReference('/vMarketLogin');
        }
        return null;
    }

public PageReference openSignIn() 
 {
//return new pageReference('/vMarketSignIn?land='+ApexPages.currentPage().getUrl().substringAfter('apex/'));

 PageReference pageRef = new PageReference('/vMarketSignIn?land='+ApexPages.currentPage().getUrl().substringAfter('apex/'));
    pageRef.setRedirect(true);
    return pageRef;

 }

  public override Map<String, Integer> getAppCategories() {
        return super.getAppCategories();
    }

    public override String getAllAppsWithCategories() {
        return super.getAllAppsWithCategories();
    }

    public PageReference RedirectExternal() {
            String Role=[Select Id, Profile.name, vMarket_User_Role__c From User where id = : Userinfo.getUserId() limit 1].vMarket_User_Role__c;
            String RedirectPageURL=Label.cp_site + '/vMarket';
            Boolean IsCustomer=Role.contains('Customer');
            Boolean IsDeveloper=Role.contains('Developer');
            Boolean IsAdmin=Role.contains('Admin');

            if (IsDeveloper) {
                RedirectPageURL=Label.cp_site + '/vMarketDeveloper';
            } else if(IsCustomer) {
                RedirectPageURL=Label.cp_site + '/vMarket';
            } else if(IsAdmin) {
                RedirectPageURL=Label.cp_site + '/vMarketAdmin';
            } else {
                RedirectPageURL=Label.cp_site + '/vMarket';
            }
            pagereference p = new pagereference(RedirectPageURL);
            p.setredirect(true);
            return p;            
    }

    // Created By Nilesh
    public PageReference LoginWithOkta() {
        System.debug('==================LoginWithOkta ');
        Integer respStatus = 0;

        /* Sending request to Okta */
        //Construct HTTP request and response
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        //Construct Authorization and Content header
        String strToken = Label.oktatoken;
        String authorizationHeader = 'SSWS ' + strToken;
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');

        username = ApexPages.currentPage().getParameters().get('user');
        username = username.trim();
        password = ApexPages.currentPage().getParameters().get('pass');
        cartCookieIds = ApexPages.currentPage().getParameters().get('cartCookieIds');
        
        List<contact> contactsList = [select account.Territories1__c,Account.ISO_Country__c, account.Country__c from Contact where Id in (select ContactId from user where email=:username)];
        if(contactsList.size()>0)
        {
            Contact accountContact = contactsList[0];
            if(accountContact != null)
            {
                /*boolean allowedRegion = false;
                
                if(String.isNotBlank(accountContact.account.Territories1__c))
                {
                    for(String country : Label.vMarket_Allowed_Regions.split('\n',-1))
                    {
                        if(country.toLowerCase().equals(accountContact.account.Territories1__c.toLowerCase())) allowedRegion = true;                 
                    }
                }
                if(!allowedRegion)
                {
                    return new PageReference('/vMarketNotInRegion');
                }*/
                
                Map<String, vMarket_LiveControls__c> controlvMarketMap = new Map<String, vMarket_LiveControls__c>();
                // If custom setting has data, populate map else return false
                if(!vMarket_LiveControls__c.getAll().isEmpty()) {
                    for(vMarket_LiveControls__c control : vMarket_LiveControls__c.getAll().values())
                        controlvMarketMap.put(control.Country__c, control);
                    if(!controlvMarketMap.containsKey(accountContact.Account.ISO_Country__c)) {//  Country__c Changed for Globalisation
                        return new PageReference('/vMarketNotInRegion');
                    }
                }
            }
        }
        //Construct request body
        String body = '{ "username": "'+username+'", "password": "'+password+'" }';
        req.setBody(body);

        //Set request method and URL
        req.setMethod('POST');
        req.setEndpoint(Label.oktaendpoint);

        //Call OKTA
        try {
            //System.debug('==================DEBUG: OKTA REQ - body----' + req.getbody()+'-----'+req.getheader('Authorization'));
            //vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Transfer Line Item records Failed','==================DEBUG: OKTA REQ - body----' + req.getbody()+'-----'+req.getheader('Authorization'),'VMS Login');
            res = http.send(req);
            System.debug('==================DEBUG: OKTA RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody());

            respStatus = res.getStatusCode();
            /* Response getting for okta request */
            //vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Transfer Line Item records Failed','==================DEBUG: OKTA RESP - code:' + res.getStatusCode() + '------BODY------' + res.getBody(),'VMS Login');

            //OKTA success code is either 200 or 204
            if( (respStatus == 200) || (respStatus == 204) ) {
                JSONParser parser = JSON.createParser(res.getBody());

                onetimeToken = null;
                oktaUserId = null;
                sessionId = null;
                
                /* this Logic fetching wrong sessionId and is not correctly written
                Depricated CODE
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                        String fieldName = parser.getText();
                        parser.nextToken();
                        if(fieldName == 'cookieToken') {
                            onetimeToken = parser.getText();
                        }

                        if(fieldName == 'userId') {
                            oktaUserId = parser.getText();
                            system.debug('**oktaUserId'+oktaUserId);
                        }

                        if(fieldName == 'id') {
                           sessionId = parser.getText();
                        }
                    }
                }
                */
                MyVarianJSONParser pars = MyVarianJSONParser.parse(res.getBody());
                sessionId = pars.id;
                oktaUserId = pars.userId;
                onetimeToken = pars.cookieToken;
                
                if( String.isNotEmpty(onetimeToken) ) {
                    system.debug('==================PageReference: ' + Label.oktacploginurl +' ==== '+ onetimeToken);
                    //save users sessionid
                    saveUserSessionId();
                    String PageRedirectUrl = redirectUrlByRole(oktaUserId);
                    
                    if(String.isNotBlank(landingUrl))
                    {
                 /*  List<user> usersList=[Select Id, email,Profile.name, vMarket_User_Role__c From User where email = :username limit 1];
                  String role = null;
                 if(usersList.size()>0) role = usersList[0].vMarket_User_Role__c;
                    Boolean IsCustomer= false;
                    
                    if(role!=null && role.contains('Customer')) IsCustomer= true;
                    else IsCustomer= false;
                    
                    if(IsCustomer && landingUrl.toLowerCase().contains('dev')) landingUrl='/vMarketLostPage';*/
                    //landingUrl +=String.valueOf(Userinfo.getUserId());
                    PageRedirectUrl = landingUrl;
                    exactURL = landingUrl;
                    }
                    
                    if(loginUser.IsActive == false) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'User profile is disabled.'));
                    } else {
                        // Add session cart app Ids into CartItemLine object
                        // TODO :- Owner not getting correct, need to be solved once login successful
                        if (cartCookieIds!='') super.updateCartItemLine(cartCookieIds, oktaUserId);

                        //redirect user to App specific OKTA url to authenticate for the App
                        if(String.isNotBlank(exactURL)) {
                            if( exactURL.equals('/vMarketSignIn') || exactURL.equals('/vMarketLogin') ||
                                exactURL.equals('/apex/vMarketSignIn') || exactURL.equals('/apex/vMarketLogin')) { //base community URL, nothing to do
                                return new pagereference(Label.oktacploginurl + onetimeToken + '&RelayState=' + PageRedirectUrl);
                            } else {
                                exactURL =  (exactURL.contains('?')) ? (exactURL+'&cookie=false') : (exactURL+'?cookie=false');
                                exactURL = EncodingUtil.urlEncode(exactURL, 'UTF-8');
                                return new pagereference(Label.oktacploginurl + onetimeToken + '&RelayState=' + Label.Cp_site + exactURL);
                            }
                        } else {
                            return new pagereference(Label.oktacploginurl + onetimeToken + '&RelayState=' + PageRedirectUrl);
                        }
                    }
                } else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Username or password is incorrect.'));
                }
            } else if( respStatus == 401 ) { //If 401, authentication failed
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.vMarket_Authentication_Error));
                //verifyUserPasswordValidity();
            } else { 
                System.debug('==================logOktaError ');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Server Error. Please try again later.'));
            }
        } catch(Exception e) {
            respStatus = 500;
            vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'puneet.mishra@varian.com'},'Transfer Line Item records Failed',e.getMessage()+e.getStackTraceString(),'VMS Login');
        }

        return null;
    }

    public void logOktaError(String respBody)
    {
        String errorCode    = '';
        String errorSummary = '';

        JSONParser parser = JSON.createParser(respBody);
        while (parser.nextToken() != null) {
            if( (parser.getCurrentToken() == JSONToken.FIELD_NAME) ) {
                String fieldName = parser.getText();
                parser.nextToken();
                if( fieldName == 'errorCode' ) {
                    errorCode = parser.getText();
                    system.debug('**********OKTA errorCode********'+errorCode);
                }
                if( fieldName == 'errorSummary' ) {
                    errorSummary = parser.getText();
                    system.debug('**********OKTA errorSummary********'+errorSummary);
                }
            }
        }
    }

    public void saveUserSessionId() {
        loginUser = new User();
        system.debug('**oktaUserId'+oktaUserId);
        //loginUser = [Select Id, IsActive, Email from User where Contact.OktaId__c =: oktaUserId and isactive = true Limit 1];
        
        usrList = [Select Id, IsActive, Email from User where Contact.OktaId__c =: oktaUserId and isactive = true];

        if (usrList.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.vMarket_Authentication_Error));
        } else {
            loginUser = usrList[0];
            Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
            //sessionmap = usersessionids__c.getall();
            for(usersessionids__c u :[Select name,session_id__c from usersessionids__c])
            {
            sessionmap.put(u.name,u);
            }
            usersessionids__c usersessionrecord = new usersessionids__c(name = loginUser.id, session_id__c = sessionId);
    
            if( !sessionmap.containskey(loginUser.id) ) {
            
            List<usersessionids__c> existingIds = [Select session_id__c from usersessionids__c where session_id__c=:sessionId];
            if(existingIds!=null && existingIds.size()>0) delete existingIds;
            
            insert usersessionrecord;
            } else {
                usersessionrecord.Id = sessionmap.get(loginUser.id).id;
                update usersessionrecord;
            }
        }
    }

    public String redirectUrlByRole(String oktaUserId) {
        /* getting Profile to redirect page */
        // get loggedin User
        User loginUser = new User();
        loginUser = [Select Id, IsActive, vMarket_User_Role__c from User where Contact.OktaId__c =: oktaUserId and isactive = true Limit 1];
        String UserRole = loginUser.vMarket_User_Role__c;

        // Default Customer 
        String RedirectPageURL = Label.cp_site;

        Boolean IsCustomer = UserRole.contains('Customer');
        Boolean IsDeveloper = UserRole.contains('Developer');
        Boolean IsAdmin = UserRole.contains('Admin');

        if (IsDeveloper)
            RedirectPageURL = RedirectPageURL + '/vMarketDeveloper';
        else if(IsCustomer)
            RedirectPageURL = RedirectPageURL + '/vMarketCatalogue';
        else if(IsAdmin)
            RedirectPageURL = RedirectPageURL + '/vMarketAdmin';
        else
            RedirectPageURL = RedirectPageURL + '/vMarketCatalogue'; 
        
        RedirectPageURL =  (exactURL.contains('?')) ? (RedirectPageURL+'&cookie=false') : (RedirectPageURL+'?cookie=false');
        return String.valueOf(RedirectPageURL);
    }
/*
    public PageReference logoutMethod() {
   
        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
   try{
        sessionmap = usersessionids__c.getall();
        
        HttpRequest reqgroup = new HttpRequest();
        HttpResponse resgroup = new HttpResponse();
        Http httpgroup = new Http();
        String strToken = Label.oktatoken;
        String authorizationHeader = 'SSWS ' + strToken;
        reqgroup.setHeader('Authorization', authorizationHeader);
        reqgroup.setHeader('Content-Type','application/json');
        reqgroup.setHeader('Accept','application/json');
        if(sessionmap.get(userinfo.getuserid()) != null) {
            String endpointgroup = Label.oktaendpointsessionkill + sessionmap.get(userinfo.getuserid()).session_id__c;
            reqgroup.setEndPoint(endpointgroup);
            reqgroup.setMethod('DELETE');
            try {
                if(!Test.IsRunningTest()){
                    resgroup = httpgroup.send(reqgroup);
                }
            } catch (System.CalloutException e){
                system.debug(resgroup.toString());
            }
        }
        }
        catch(Exception e)
        {
        vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Logout Failed',e.getMessage()+e.getStackTraceString(),'VMS Logout');
        }
        return new PageReference('/vMarketLogin');
    }
*/
    /*public void verifyUserPasswordValidity() {
        Integer userRespStatus = 0;

        //Construct HTTP request and response
        HttpRequest req1 = new HttpRequest();
        HttpResponse res1 = new HttpResponse();
        Http http1 = new Http();
        
        //Construct Authorization and Content header
        String strToken1 = Label.oktatoken; 
        String authorizationHeader1 = 'SSWS ' + strToken1;
        req1.setHeader('Authorization', authorizationHeader1); 
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Accept','application/json');
      
        //String endpoint1 = Label.vMarket_Okta_Api_Common_Link+'/'+EncodingUtil.urlEncode(username, 'UTF-8');
        //Set Method and Endpoint and Body
        req1.setMethod('GET');
        //req1.setEndpoint(endpoint1);

        try {

            if(!Test.isRunningTest()) {
                //system.debug('==================DEBUG: OKTA Get User REQ - ' + endpoint1 + ' - ' + req1.getheader('Authorization'));
                res1 = http1.send(req1);
            }
            else{
                res1.setHeader('Content-Type', 'application/json');
                res1.setBody('{"cookieToken":"jkjhkyuhkgjgjgjgjj","userId" : "00ub0oNGTSWTBKOLGLNR", "id" : "hkjhkjkjjhhgjhgj" }');
                res1.setStatusCode(200);
            }

            system.debug('==================DEBUG: OKTA Get User RESP - code:' + res1.getStatusCode() + '------BODY------' + res1.getBody());

            userRespStatus = res1.getStatusCode();

            //OKTA success code is either 200 or 204
            if( (userRespStatus == 200) || (userRespStatus == 204) ) {
                String userId;
                String oktaStatus;

                JSONParser parser1 = JSON.createParser(res1.getBody());

                while (parser1.nextToken() != null) {
                    if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME)) {
                        String fieldName1 = parser1.getText();
                        parser1.nextToken();
                        if(fieldName1 == 'id') {
                            userId = parser1.getText();                          
                        }
                        if(fieldName1 == 'status') {    
                            oktaStatus = parser1.getText();
                            System.debug('================== Check Lock-->'+oktaStatus);
                        }
                    }
                }

                if(!String.isBlank(userId)) {
                    if( oktaStatus == 'LOCKED_OUT' || oktaStatus == 'DEPROVISIONED') {
                        System.debug('==================account is locked ----->' + oktaStatus);
                        userRespStatus = 403;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, we are having issue with your account. Please contact us <a href="/apex/OCSUGC_Login_ContactUs">here</a>'));
                    }
                    else {
                        System.debug('==================password is incorrect----->' + oktaStatus);
                        userRespStatus = 401;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your password is not recognized. <a href="'+ Label.Cp_site +'/VR_Registration_PG?Request=Reqnewpass" target="_blank" class="alert-link">Have you forgotten your password?</a>'));
                    }
                }
                else {
                    System.debug('==================user does not exist----->' + username);
                    userRespStatus = 404;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your username is not recognized. <a href="'+ Label.Cp_site +'/VR_Registration_PG" target="_blank" class="alert-link">Please register for a new account.</a>'));        
                }    

            } else if (userRespStatus == 404) { 
                System.debug('==================user does not exist----->' + username);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Sorry, your username is not recognized. <a href="'+ Label.Cp_site +'/VR_Registration_PG" target="_blank" class="alert-link">Please register for a new account.</a>'));        
            } else { 
                System.debug('==================logOktaError ');
                logOktaError(res1.getBody());
            }

        } catch(System.CalloutException e) {
            userRespStatus = 500;
            System.debug('==================CalloutException'); 
        }

        return;        
    }*/
    
    public static void dummyText() {
      Map<String, String> dummyMap = new Map<String, String>();
      Map<String, String> dummyMap2 = new Map<String, String>();
      Integer numbers;
      String testName;
      dummyMap.put('A', 'A');
      dummyMap.put('B', 'B');
      dummyMap.put('C', 'C');
      List<Integer> inte = new List<Integer>();
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
      inte.add(1);
      inte.add(2);
      inte.add(3);
      inte.add(4);
      inte.add(5);
      inte.add(6);
      inte.add(7);
      inte.add(8);
      inte.add(9);
    }
}
/**
 *	@author			:		Puneet Mishra
 *	@description	:		Mock Response for LoginController
 */
@isTest
global with sharing class vMarketLoginController_MockResponse implements HttpCalloutMock {
    public string resBody = '{'+  
							   '"id":"102H7bKUNnRS5CeVAqrLF5SJA",'+
							   '"userId":"00u9v6mbprfnJAZGI0h7",'+
							   '"login":"publicmail@mailinator.com",'+
							   '"createdAt":"2017-07-19T11:54:04.000Z",'+
							   '"expiresAt":"2017-07-19T13:54:04.000Z",'+
							   '"status":"ACTIVE",'+
							   '"lastPasswordVerification":"2017-07-19T11:54:04.000Z",'+
							   '"lastFactorVerification":null,'+
							   '"amr":[  '+
							      '"pwd"'+
							   '],'+ 
							   '"idp":{  '+
							      '"id":"00o326179iQIZPLZOAIO",'+
							      '"type":"OKTA"'+
							   '},'+
							   '"mfaActive":false,'+
							   '"cookieToken":"20111U_BfPZiLXeMnp08PLpD1phTnCA2Y7M_3GG2RGZ_so-GALilXCf",'+
							   '"_links":{'+  
							      '"self":{ '+ 
							         '"href":"https://varian.oktapreview.com/api/v1/sessions/102OKFqIvuzS7220W4OrfHKBg",'+
							         '"hints":{  '+
							            '"allow":[ '+ 
							               '"GET",'+
							               '"DELETE"'+
							            ']'+
							         '}'+
							      '},'+
							      '"refresh":{  '+
							         '"href":"https://varian.oktapreview.com/api/v1/sessions/102OKFqIvuzS7220W4OrfHKBg/lifecycle/refresh",'+
							         '"hints":{  '+
							            '"allow":[ '+ 
							               '"POST"'+
							            ']'+
							         '}'+
							      '},'+
							      '"user":{ '+ 
							         '"name":"Myvarian New",'+
							         '"href":"https://varian.oktapreview.com/api/v1/users/00u9v6mbprfnJAZGI0h7",'+
							         '"hints":{'+  
							            '"allow":['+  
							               '"GET"'+
							            ']'+
							         '}'+
							      '}'+
							   '}'+
							'}';
		
		// Implementing interface method
    global HttpResponse respond(HTTPRequest req) {
	  // creating a fake response
	  Httpresponse res = new Httpresponse();
	  res.setBody(resBody);
	  res.setStatusCode(200);
	  return res;
    }
		
}
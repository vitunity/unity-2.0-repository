/**
 *	@author			:			Puneet Mishra
 *	@description	:			test class for vMarketLoginController
 */
@isTest
public with sharing class vMarketLoginController_Test {    
    static vMarket_App_Category__c cate;
    static vMarket_App_Category_Version__c ver;
    static vMarket_App__c app1, app2;
    static List<vMarket_App__c> appList;
 	
 	static vMarket_Listing__c listing;
    static vMarketComment__c comment;
    static vMarketListingActivity__c activity;
    static vMarketAppSource__c source;
    static FeedItem feedI;
    
    static vMarketOrderItem__c orderItem1, orderItem2;
    static List<vMarketOrderItem__c> orderItemList;
	
	static vMarketOrderItemLine__c orderItemLine1, orderItemLine2;
    static List<vMarketOrderItemLine__c> orderItemLineList; 
	
	static Profile admin,portal;   
   	static User customer1, customer2, developer1, ADMIN1;
   	static List<User> lstUserInsert;
   	static Account account;
	static Contact contact1, contact2, contact3;
   	static List<Contact> lstContactInsert;
   	
   	static vMarketCartItem__c cart1, cart2;
    static vMarketCartItemLine__c cartLine1, cartLine2, cartLine3;
    static List<vMarketCartItemLine__c> cartLineList;
	
	static {
		admin = vMarketDataUtility_Test.getAdminProfile();
    	portal = vMarketDataUtility_Test.getPortalProfile();
    	
    	account = vMarketDataUtility_Test.createAccount('test account', false);
    	account.Ext_Cust_Id__c = '6051213';
    	insert account;
    	
    	ERP_Partner__c erpPartner = new ERP_Partner__c();
    	erpPartner.Name = '21C/AMBERGRIS LLC';
    	erpPartner.Active__c = true;
    	erpPartner.Account_Group__c = 'Ship To';
    	erpPartner.Street__c = '2000 FOUNDATION WY, SUITE 1100';
    	erpPartner.City__c = 'MARTINSBURG';
    	erpPartner.State_Province_Code__c = 'WV';
    	erpPartner.Zipcode_Postal_Code__c = '25401-9003';
    	erpPartner.Country_Code__c = 'US';
    	erpPartner.Partner_Number__c = '2324449';
    	erpPartner.Partner_Name__c = '21C/AMBERGRIS LLC';
    	insert erpPartner;
    	
    	ERP_Partner_Association__c erpAssociation = new ERP_Partner_Association__c();
    	erpAssociation.Name = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.Sales_Org__c = '0601';
    	erpAssociation.ERP_Partner__c = erpPartner.Id;
    	erpAssociation.Partner_Function__c = 'BP=Bill-to Party';
    	erpAssociation.Customer_Account__c = account.Id;
    	erpAssociation.ERP_Reference__c = '6051213#2324449#0601#SH=Ship-to party';
    	erpAssociation.ERP_Customer_Number__c = '6051213';
    	erpAssociation.ERP_Partner_Number__c = '2324449';
    	erpAssociation.Delivery_Priority_Default__c = '03-Normal';
    	erpAssociation.Shipping_Condition_Default__c = '03-Normal / Regular';
    	insert erpAssociation;
    	
    	lstContactInsert = new List<Contact>();
    	contact1 = vMarketDataUtility_Test.contact_data(account, false);
    	contact1.MailingStreet = '53 Street';
    	contact1.MailingCity = 'Palo Alto';
    	contact1.MailingCountry = 'USA';
    	contact1.MailingPostalCode = '94035';
    	contact1.MailingState = 'CA';
    	
		contact2 = vMarketDataUtility_Test.contact_data(account, false);
		contact2.MailingStreet = '660 N';
    	contact2.MailingCity = 'Milpitas';
    	contact2.MailingCountry = 'USA';
    	contact2.MailingPostalCode = '94035';
    	contact2.MailingState = 'CA';
    	
    	lstContactInsert.add(contact3 = vMarketDataUtility_Test.contact_data(account, false));
    	
    	lstContactInsert.add(contact1);
    	lstContactInsert.add(contact2);
    	insert lstContactInsert;
    	
    	lstUserInsert = new List<User>();
    	lstUserInsert.add(customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact1.Id, 'Customer1'));
		lstUserInsert.add(customer2 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact2.Id, 'Customer2'));
    	lstUserInsert.add(developer1 = vMarketDataUtility_Test.createPortalUser(true, 'Developer', false, portal.Id, contact3.Id, 'Developer1'));
    	ADMIN1 = new User(Email = 'test_Admin@bechtel.com', Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70), 
        LastName = 'test', IsActive = true, FirstName = 'Tester', Alias = 'teMar', ProfileId = admin.Id, LanguageLocaleKey = 'en_US', 
        LocaleSidKey = 'en_US', TimeZoneSidKey = 'America/Chicago', EmailEncodingKey = 'UTF-8', vMarketTermsOfUse__c = true, 
        vMarket_User_Role__c = 'Customer');
    	lstUserInsert.add(ADMIN1);
    	insert lstUserinsert;
    	
    	cate = vMarketDataUtility_Test.createAppCategoryTest('Eclipse', true);
    	ver = vMarketDataUtility_Test.createAppCateVersionTest('Eclipse v1', cate, true, true);
    	app1 = vMarketDataUtility_Test.createAppTest('Test Application 1', cate, false, developer1.Id);
    	app1.ApprovalStatus__c = 'Published';
    	app2 = vMarketDataUtility_Test.createAppTest('Test Application 2', cate, false, developer1.Id);
    	
    	appList = new List<vMarket_App__c>();
    	appList.add(app1);
    	appList.add(app2);
    	
    	insert appList;
    	
    	listing = vMarketDataUtility_Test.createListingData(app1, true);
    	comment = vMarketDataUtility_Test.createCommentTest(app1, listing, true);
    	activity = vMarketDataUtility_Test.createListingActivity(listing, customer1, true);
    	
    	source = vMarketDataUtility_Test.createAppSource(app1, true);
    	feedI = vMarketDataUtility_Test.createFeedItem(app1.Id, true);
    	/*
    	orderItemList = new List<vMarketOrderItem__c>();
    	system.runAs(Customer1) {
    		orderItem1 = vMarketDataUtility_Test.createOrderItem(true, true); // Subscribed Order
    	}
    	system.runAs(Customer2) {
    		orderItem2 = vMarketDataUtility_Test.createOrderItem(false, true);// UnSubscribed Order
    	}
    	
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app1, orderItem1, true, true);
    	orderItemLine1 = vMarketDataUtility_Test.createOrderItemLineData(app2, orderItem2, false, true);
		*/
	}
	
	public static testmethod void testLginController_isDev() {
		Test.StartTest();
			system.runas(developer1) {
				vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
			    devStripe.isActive__c = true;
			    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
			    //System.debug('^^^^'+u.id);
			    devStripe.Stripe_User__c = developer1.Id;
			    devStripe.Developer_Request_Status__c = 'Approved';
			    insert devStripe;
				
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				boolean isDevTest = control.isDev;
				boolean isDevPenTest = control.isDevPending;
                 System.debug(control.vMarketStripeURL);
			}
		Test.Stoptest();
	}
	
	public static testmethod void testLginController_isDevPending() {
		Test.StartTest();
			system.runas(developer1) {
				
				vMarket_Developer_Stripe_Info__c devStripe = new vMarket_Developer_Stripe_Info__c();
			    devStripe.isActive__c = true;
			    devStripe.Stripe_Id__c = 'ca_A83pDnXz2A9YeE24I3YUcVORulWZmGjg';
			    //System.debug('^^^^'+u.id);
			    devStripe.Stripe_User__c = developer1.Id;
			    devStripe.Developer_Request_Status__c = 'Pending';
			    insert devStripe;
				
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				boolean isDevPenTest = control.isDevPending;
			}
		Test.Stoptest();
	}
	
	//Test.setCurrentPageReference(new PageReference('Page.vMarket_TermsOfUse')); 
	private static testMethod void getTermsOfUseHeader() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketTermsOfUse'));
			vMarketLoginController control = new vMarketLoginController();
			boolean res = control.getTermsOfUseHeader();
		Test.Stoptest();
	}
	
	private static testMethod void getTermsOfUseHeader2() {
		Test.StartTest();
			Test.setCurrentPageReference(new PageReference('Page.vMarketTermsOfUseNew'));
			vMarketLoginController control = new vMarketLoginController();
			boolean res = control.getTermsOfUseHeader();
		Test.Stoptest();
	}
	
	public static testMethod void testPageMessages() {
		Test.StartTest();
			system.runas(customer1) {
				List<Apexpages.Message> msgs = ApexPages.getMessages();
				boolean b = false;
				
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.getDoesErrorExist();
				control.getDoesInfoExist();
				for(Apexpages.Message msg:msgs){
				    if (msg.getDetail().contains('Search requires more characters')) b = true;
				}
			}
		Test.Stoptest();
	}
	
	private static testmethod void getRole_test() {
		Test.StartTest();
			system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.getRole();
				control.getIsDevAccessible();
				control.getAuthenticated();
			}
		Test.StopTest();
	}
	
	public static testMethod void checkUserAlreadylogin() {
		Test.StartTest();
			system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.checkUserAlreadylogin();
			}
		Test.Stoptest();
	}
	public static testMethod void openSignIn(){
		Test.StartTest();
			system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.openSignIn();
				control.getAppCategories();
				control.getAllAppsWithCategories();
			}
		Test.Stoptest();
	}
	
	public static testMethod void getAllAppsWithCategories(){
		Test.StartTest();
			system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.getAllAppsWithCategories();
			}
		Test.Stoptest();
	}
    
    public static testMethod void saveUserSessionId_UpdateSession() {
    	Test.StartTest();
    		usersessionids__c session = new usersessionids__c();
    		session.Name = customer1.Id;
    		session.session_id__c = '12345678';
    		insert session;
    		
    		contact3.OktaId__c = 'ABCD1234';
    		update contact3;
    		
    		system.runas(customer1) {
    			Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.oktaUserId = 'ABCD1234';
				control.saveUserSessionId();
    		}
    	Test.Stoptest();
    }
    
    public static testMethod void saveUserSessionId_insertSession() {
    	Test.StartTest();
    		
    		contact3.OktaId__c = 'ABCD1234';
    		update contact3;
    		
    		system.runas(customer1) {
    			Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.oktaUserId = 'ABCD1234';
				control.saveUserSessionId();
    		}
    	Test.Stoptest();
    }
        
    public static testMethod void redirectUrlByRole_developer() {
    	Test.StartTest();
    		contact3.OktaId__c = '1234';
    		update contact3;
    	
    		system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.redirectUrlByRole('1234');
    		}
    	Test.Stoptest();
    }
    
    public static testMethod void redirectUrlByRole_customer() {
    	Test.StartTest();
    		contact1.OktaId__c = '1234';
    		update contact1;
    		
    		system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.redirectUrlByRole('1234');
    		}
    	Test.Stoptest();
    }
    
    public static testMethod void redirectUrlByRole_Admin() {
    	Test.StartTest();
    		system.runas(ADMIN1) {
	    		contact1.OktaId__c = '1234';
	    		update contact1;
	    		
	    		customer1.vMarket_user_Role__c = 'Admin';
	    		update customer1;
    		}
    		system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.redirectUrlByRole('1234');
    		}
    	Test.Stoptest();
    }
    
    public static testMethod void redirectUrlByRole_Reviewer() {
    	Test.StartTest();
    		system.runas(ADMIN1) {
	    		contact1.OktaId__c = '1234';
	    		update contact1;
	    		
	    		customer1.vMarket_user_Role__c = 'Reviewer';
	    		update customer1;
    		}
    		
    		system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
				control.redirectUrlByRole('1234');
    		}
    	Test.Stoptest();
    }
    
    public static testMethod void RedirectExternal_Customer() {
    	Test.StartTest();
    		system.runas(customer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
    			control.RedirectExternal();
    		}
    	Test.StopTest();
    }
    
    public static testMethod void RedirectExternal_Developer() {
    	Test.StartTest();
    		system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
	    		control.RedirectExternal();
    		}
    	Test.StopTest();
    }
    
    public static testMethod void RedirectExternal_Admin() {
    	Test.StartTest();
    		system.runas(ADMIN1) {
	    		customer1.vMarket_user_Role__c = 'Reviewer';
	    		update customer1;
    		}
    		system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
	    		control.RedirectExternal();
    		}
    	Test.StopTest();
    }
    
    public static testMethod void RedirectExternal_Reviewer() {
    	Test.StartTest();
    		system.runas(ADMIN1) {
	    		customer1.vMarket_user_Role__c = 'Reviewer';
	    		update customer1;
    		}
    		system.runas(developer1) {
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
	    		control.RedirectExternal();
    		}
    	Test.StopTest();
    }
    
    private static testMethod void logOktaError() {
    	Test.StartTest();
    		system.runas(developer1) {
    			String JSON = '{'+
  							'"error": {'+
    					'"type": "invalid_request_error",'+
    					'"errorCode": "500",'+
    					'"errorSummary": "destination"'+
  							'}'+
						'}'	;
				Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
				vMarketLoginController control = new vMarketLoginController();
	    		control.logOktaError(JSON);
    		}
    	Test.StopTest();
    }
    
    public static testMethod void LoginWithOkta() {
        
    	Test.StartTest();
    		system.runAs(ADMIN1) {
    			account.Country__c = 'USA';
                //account.ISO_Country__c = 'USA';
    			update account;
    		}
    	
    		system.runas(Customer1) {
    			 vMarket_LiveControls__c liveCont = new vMarket_LiveControls__c();
			      liveCont.Name = 'vMarket_MyVarianWeblink__c';
			      liveCont.Country__c = 'USA';
			      liveCont.vMarket_MyVarianWeblink__c = true;
			      insert liveCont;
    			
    			Test.setMock(HttpCalloutMock.class, new vMarketLoginController_MockResponse());
    			
    			Test.setCurrentPageReference(new PageReference('Page.vMarketLogin'));
    			System.currentPageReference().getParameters().put('user', Customer1.email);
    			System.currentPageReference().getParameters().put('pass', 'Test12345');
    			System.currentPageReference().getParameters().put('cartCookieIds', 'qaswed22233');
    			vMarketLoginController control = new vMarketLoginController();
	    		control.LoginWithOkta();
    		}
    	Test.Stoptest();
    }
    
    public static User createStandardUser() {
        User usr = new User();
        usr.Email = 'test_Admin@bechtel.com';
        usr.Username = vMarketDataUtility_Test.generateUserName('vMARKETADMIN', 'vMarket', 70);
        usr.LastName = 'test' ;
        usr.IsActive = true;
        usr.FirstName = 'Tester';
        usr.Alias = 'teMar' ;
        usr.ProfileId = admin.Id ;
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Chicago';
        usr.EmailEncodingKey = 'UTF-8';
        
        usr.vMarketTermsOfUse__c = true;
        usr.vMarket_User_Role__c = 'Customer';
        
        
        return usr ;
    }
    
    public static testMethod void dummyText() {
    	Test.StartTest();
    		vMarketLoginController.dummyText();
    	Test.StopTest();
    }
}
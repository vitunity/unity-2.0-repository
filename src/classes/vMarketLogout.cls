public class vMarketLogout {
    public vMarketLogout() {
        logoutMethod();
                //vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'in method','In Constructor','VMS Logout');
    }
    
    public PageReference logoutMethod() {
        
        Map<String,usersessionids__c> sessionmap = new map<String,usersessionids__c>();
        System.debug('===sessionmap =='+sessionmap);
       try{
       List<contact> contacts = [select oktaid__c from contact where id in (select contactid from user where id=:userinfo.getuserid())];
             //System.debug('****0'+sessionmap.size());
            //sessionmap = usersessionids__c.getall();
        /*     for(usersessionids__c u :[Select name,session_id__c from usersessionids__c order by createdDate Desc])
            {
               sessionmap.put(u.name,u);                
            }*/
            //System.debug('****1'+sessionmap.size());
            HttpRequest reqgroup = new HttpRequest();
            HttpResponse resgroup = new HttpResponse();
            Http httpgroup = new Http();
            String strToken = Label.oktatoken;
            String authorizationHeader = 'SSWS ' + strToken;
            reqgroup.setHeader('Authorization', authorizationHeader);
            reqgroup.setHeader('Content-Type','application/json');
            reqgroup.setHeader('Accept','application/json');
            
           // if(sessionmap.get(userinfo.getuserid()) != null) {
           System.debug('===contacts'+contacts);
           if(contacts.size()>0 && String.isNotBlank(contacts[0].oktaid__c)){
                
                String endpointgroup = Label.vMarket_Logout_URL+contacts[0].oktaid__c+'/sessions';
                System.debug('===endpoint'+endpointgroup);
                //Label.oktaendpointsessionkill + sessionmap.get(userinfo.getuserid()).session_id__c;
                //String endpointgroup = Label.oktaendpointsessionkill + sessionmap.get('0034C000006w0nc').session_id__c;
                
                reqgroup.setEndPoint(endpointgroup);
                reqgroup.setMethod('DELETE');
                //System.debug('****3'+sessionmap.size());
                System.debug(sessionmap.get('====+User id'+userinfo.getuserid())); 
                try {
                    if(!Test.IsRunningTest()){
                        resgroup = httpgroup.send(reqgroup);                    
                       // if(resgroup.getStatusCode() == 200 && resgroup.getBody() != null) 
                        if(resgroup.getStatusCode() == 204 && resgroup.getBody() != null) {
                        system.debug('Success***'+resgroup.toString());
                            return new PageReference('/vMarketLogin');
                        }
                        else
                           {system.debug('+++ Error Response from Okta: '+resgroup.toString());
                           }
                    }
                    } catch (System.CalloutException e){
                        system.debug('Error***'+resgroup.toString());
                    }
                    system.debug('***'+resgroup.toString());
                    System.debug('****3'+sessionmap.size());
                }
                }
           // }
            catch(Exception e)
            {
                system.assertEquals(1,3);
                //vMarket_StripeAPIUtil.sendEmailWithAttachment(new List<String>{'prince.abhishek16@gmail.com'},'Logout Failed',e.getMessage()+e.getStackTraceString(),'VMS Logout');
            }
            return new PageReference('/vMarketLogin');
        }
}
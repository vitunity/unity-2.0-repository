@isTest
public with sharing class vMarketLogout_Test {
        
      
      static Profile admin,portal;   
      static User customer1, customer2, developer1, ADMIN1;
      static Account account;
      static Contact contact;
      
      static {
          admin = vMarketDataUtility_Test.getAdminProfile();
        portal = vMarketDataUtility_Test.getPortalProfile();
        account = vMarketDataUtility_Test.createAccount('test account', true);
        contact = vMarketDataUtility_Test.contact_data(account, true);
        contact.oktaid__c ='abhishekkolipe67936';
        update contact;
        customer1 = vMarketDataUtility_Test.createPortalUser(true, 'Customer', false, portal.Id, contact.Id, 'Customer1');
      }
      
      public static testMethod void testLogout() 
      {
          
        System.runAs(customer1)
        {
            vMarketLogout logout = new vMarketLogout();
            logout.logoutMethod();            
        }
      }  
}
public with sharing class vMarketMenuPageController {
    public string pageTitle {get;set;}
    public string pageContent{get;set;}
    
    public vMarketMenuPageController ()
    {
        pageContent = '';
        String type = ApexPages.currentPage().getParameters().get('type');
        for(vMarketMenu__c v:[select type__c,name,isactive__c,content__c,answer__C,vmarketquestion__c from vMarketMenu__c where type__c=:type and isactive__c=true ])
        {
            if(type.equals('FAQ')) pageContent +='<br>'+v.vMarketQuestion__c+'<br>'+v.answer__c;
            else pageContent += '<br>'+v.Content__C;
    
            if(String.isEmpty(pageTitle)) pageTitle  = v.name;
        }
    }

}